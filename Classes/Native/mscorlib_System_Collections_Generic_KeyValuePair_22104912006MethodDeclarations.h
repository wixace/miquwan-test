﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21209901952MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m885888368(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2104912006 *, uint8_t, IBehavior_t770859129 *, const MethodInfo*))KeyValuePair_2__ctor_m907882396_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::get_Key()
#define KeyValuePair_2_get_Key_m1880940536(__this, method) ((  uint8_t (*) (KeyValuePair_2_t2104912006 *, const MethodInfo*))KeyValuePair_2_get_Key_m3205006412_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2052464313(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2104912006 *, uint8_t, const MethodInfo*))KeyValuePair_2_set_Key_m614586637_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::get_Value()
#define KeyValuePair_2_get_Value_m804780060(__this, method) ((  IBehavior_t770859129 * (*) (KeyValuePair_2_t2104912006 *, const MethodInfo*))KeyValuePair_2_get_Value_m1857364592_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m77688377(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2104912006 *, IBehavior_t770859129 *, const MethodInfo*))KeyValuePair_2_set_Value_m3542081165_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::ToString()
#define KeyValuePair_2_ToString_m3121256751(__this, method) ((  String_t* (*) (KeyValuePair_2_t2104912006 *, const MethodInfo*))KeyValuePair_2_ToString_m3384335323_gshared)(__this, method)
