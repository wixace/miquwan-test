﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIPlayTweenGenerated
struct UIPlayTweenGenerated_t933687660;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UIPlayTweenGenerated::.ctor()
extern "C"  void UIPlayTweenGenerated__ctor_m1287406255 (UIPlayTweenGenerated_t933687660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPlayTweenGenerated::UIPlayTween_UIPlayTween1(JSVCall,System.Int32)
extern "C"  bool UIPlayTweenGenerated_UIPlayTween_UIPlayTween1_m1424207459 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTweenGenerated::UIPlayTween_current(JSVCall)
extern "C"  void UIPlayTweenGenerated_UIPlayTween_current_m2553615901 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTweenGenerated::UIPlayTween_tweenTarget(JSVCall)
extern "C"  void UIPlayTweenGenerated_UIPlayTween_tweenTarget_m1133671194 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTweenGenerated::UIPlayTween_tweenGroup(JSVCall)
extern "C"  void UIPlayTweenGenerated_UIPlayTween_tweenGroup_m144670018 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTweenGenerated::UIPlayTween_trigger(JSVCall)
extern "C"  void UIPlayTweenGenerated_UIPlayTween_trigger_m1068192958 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTweenGenerated::UIPlayTween_playDirection(JSVCall)
extern "C"  void UIPlayTweenGenerated_UIPlayTween_playDirection_m1980621099 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTweenGenerated::UIPlayTween_resetOnPlay(JSVCall)
extern "C"  void UIPlayTweenGenerated_UIPlayTween_resetOnPlay_m2845927892 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTweenGenerated::UIPlayTween_resetIfDisabled(JSVCall)
extern "C"  void UIPlayTweenGenerated_UIPlayTween_resetIfDisabled_m576503566 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTweenGenerated::UIPlayTween_ifDisabledOnPlay(JSVCall)
extern "C"  void UIPlayTweenGenerated_UIPlayTween_ifDisabledOnPlay_m3290501450 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTweenGenerated::UIPlayTween_disableWhenFinished(JSVCall)
extern "C"  void UIPlayTweenGenerated_UIPlayTween_disableWhenFinished_m3672551714 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTweenGenerated::UIPlayTween_includeChildren(JSVCall)
extern "C"  void UIPlayTweenGenerated_UIPlayTween_includeChildren_m1001426063 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTweenGenerated::UIPlayTween_onFinished(JSVCall)
extern "C"  void UIPlayTweenGenerated_UIPlayTween_onFinished_m3544188229 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPlayTweenGenerated::UIPlayTween_Play__Boolean(JSVCall,System.Int32)
extern "C"  bool UIPlayTweenGenerated_UIPlayTween_Play__Boolean_m3544419137 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTweenGenerated::__Register()
extern "C"  void UIPlayTweenGenerated___Register_m2027851640 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPlayTweenGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UIPlayTweenGenerated_ilo_attachFinalizerObject1_m1043811600 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTweenGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UIPlayTweenGenerated_ilo_addJSCSRel2_m3386659436 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIPlayTweenGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIPlayTweenGenerated_ilo_setObject3_m3129038013 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIPlayTweenGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIPlayTweenGenerated_ilo_getObject4_m1144069603 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTweenGenerated::ilo_setInt325(System.Int32,System.Int32)
extern "C"  void UIPlayTweenGenerated_ilo_setInt325_m855556227 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTweenGenerated::ilo_setEnum6(System.Int32,System.Int32)
extern "C"  void UIPlayTweenGenerated_ilo_setEnum6_m2150168875 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlayTweenGenerated::ilo_setBooleanS7(System.Int32,System.Boolean)
extern "C"  void UIPlayTweenGenerated_ilo_setBooleanS7_m3682545562 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPlayTweenGenerated::ilo_getBooleanS8(System.Int32)
extern "C"  bool UIPlayTweenGenerated_ilo_getBooleanS8_m1267814148 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
