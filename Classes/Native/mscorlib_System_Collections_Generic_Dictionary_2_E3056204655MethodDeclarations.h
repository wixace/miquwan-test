﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>
struct Dictionary_2_t1738881263;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3056204655.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21637661969.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3840966209_gshared (Enumerator_t3056204655 * __this, Dictionary_2_t1738881263 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m3840966209(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3056204655 *, Dictionary_2_t1738881263 *, const MethodInfo*))Enumerator__ctor_m3840966209_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2950322816_gshared (Enumerator_t3056204655 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2950322816(__this, method) ((  Il2CppObject * (*) (Enumerator_t3056204655 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2950322816_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1533905876_gshared (Enumerator_t3056204655 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1533905876(__this, method) ((  void (*) (Enumerator_t3056204655 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1533905876_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3264954205_gshared (Enumerator_t3056204655 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3264954205(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t3056204655 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3264954205_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m366720860_gshared (Enumerator_t3056204655 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m366720860(__this, method) ((  Il2CppObject * (*) (Enumerator_t3056204655 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m366720860_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1279864558_gshared (Enumerator_t3056204655 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1279864558(__this, method) ((  Il2CppObject * (*) (Enumerator_t3056204655 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1279864558_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2899515072_gshared (Enumerator_t3056204655 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2899515072(__this, method) ((  bool (*) (Enumerator_t3056204655 *, const MethodInfo*))Enumerator_MoveNext_m2899515072_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t1637661969  Enumerator_get_Current_m1432355696_gshared (Enumerator_t3056204655 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1432355696(__this, method) ((  KeyValuePair_2_t1637661969  (*) (Enumerator_t3056204655 *, const MethodInfo*))Enumerator_get_Current_m1432355696_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.Object>::get_CurrentKey()
extern "C"  uint16_t Enumerator_get_CurrentKey_m633733773_gshared (Enumerator_t3056204655 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m633733773(__this, method) ((  uint16_t (*) (Enumerator_t3056204655 *, const MethodInfo*))Enumerator_get_CurrentKey_m633733773_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3337467889_gshared (Enumerator_t3056204655 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m3337467889(__this, method) ((  Il2CppObject * (*) (Enumerator_t3056204655 *, const MethodInfo*))Enumerator_get_CurrentValue_m3337467889_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m1828931347_gshared (Enumerator_t3056204655 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1828931347(__this, method) ((  void (*) (Enumerator_t3056204655 *, const MethodInfo*))Enumerator_Reset_m1828931347_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1482407132_gshared (Enumerator_t3056204655 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1482407132(__this, method) ((  void (*) (Enumerator_t3056204655 *, const MethodInfo*))Enumerator_VerifyState_m1482407132_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m1408983364_gshared (Enumerator_t3056204655 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1408983364(__this, method) ((  void (*) (Enumerator_t3056204655 *, const MethodInfo*))Enumerator_VerifyCurrent_m1408983364_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt16,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3484829475_gshared (Enumerator_t3056204655 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3484829475(__this, method) ((  void (*) (Enumerator_t3056204655 *, const MethodInfo*))Enumerator_Dispose_m3484829475_gshared)(__this, method)
