﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Appegg/ViewConfig
struct ViewConfig_t3906840306;

#include "codegen/il2cpp-codegen.h"

// System.Void Appegg/ViewConfig::.ctor()
extern "C"  void ViewConfig__ctor_m3831346873 (ViewConfig_t3906840306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
