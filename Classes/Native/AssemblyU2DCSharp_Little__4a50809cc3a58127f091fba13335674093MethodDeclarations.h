﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._4a50809cc3a58127f091fba147ca7253
struct _4a50809cc3a58127f091fba147ca7253_t3335674093;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__4a50809cc3a58127f091fba13335674093.h"

// System.Void Little._4a50809cc3a58127f091fba147ca7253::.ctor()
extern "C"  void _4a50809cc3a58127f091fba147ca7253__ctor_m3893463008 (_4a50809cc3a58127f091fba147ca7253_t3335674093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._4a50809cc3a58127f091fba147ca7253::_4a50809cc3a58127f091fba147ca7253m2(System.Int32)
extern "C"  int32_t _4a50809cc3a58127f091fba147ca7253__4a50809cc3a58127f091fba147ca7253m2_m2674492025 (_4a50809cc3a58127f091fba147ca7253_t3335674093 * __this, int32_t ____4a50809cc3a58127f091fba147ca7253a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._4a50809cc3a58127f091fba147ca7253::_4a50809cc3a58127f091fba147ca7253m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _4a50809cc3a58127f091fba147ca7253__4a50809cc3a58127f091fba147ca7253m_m3634487389 (_4a50809cc3a58127f091fba147ca7253_t3335674093 * __this, int32_t ____4a50809cc3a58127f091fba147ca7253a0, int32_t ____4a50809cc3a58127f091fba147ca725361, int32_t ____4a50809cc3a58127f091fba147ca7253c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._4a50809cc3a58127f091fba147ca7253::ilo__4a50809cc3a58127f091fba147ca7253m21(Little._4a50809cc3a58127f091fba147ca7253,System.Int32)
extern "C"  int32_t _4a50809cc3a58127f091fba147ca7253_ilo__4a50809cc3a58127f091fba147ca7253m21_m3855049556 (Il2CppObject * __this /* static, unused */, _4a50809cc3a58127f091fba147ca7253_t3335674093 * ____this0, int32_t ____4a50809cc3a58127f091fba147ca7253a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
