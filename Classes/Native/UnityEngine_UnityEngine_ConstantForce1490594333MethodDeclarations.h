﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ConstantForce
struct ConstantForce_t1490594333;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine.ConstantForce::.ctor()
extern "C"  void ConstantForce__ctor_m4182161426 (ConstantForce_t1490594333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ConstantForce::get_force()
extern "C"  Vector3_t4282066566  ConstantForce_get_force_m4090357004 (ConstantForce_t1490594333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConstantForce::set_force(UnityEngine.Vector3)
extern "C"  void ConstantForce_set_force_m3500166139 (ConstantForce_t1490594333 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConstantForce::INTERNAL_get_force(UnityEngine.Vector3&)
extern "C"  void ConstantForce_INTERNAL_get_force_m4140681445 (ConstantForce_t1490594333 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConstantForce::INTERNAL_set_force(UnityEngine.Vector3&)
extern "C"  void ConstantForce_INTERNAL_set_force_m4055313393 (ConstantForce_t1490594333 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ConstantForce::get_relativeForce()
extern "C"  Vector3_t4282066566  ConstantForce_get_relativeForce_m1304634528 (ConstantForce_t1490594333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConstantForce::set_relativeForce(UnityEngine.Vector3)
extern "C"  void ConstantForce_set_relativeForce_m2953580007 (ConstantForce_t1490594333 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConstantForce::INTERNAL_get_relativeForce(UnityEngine.Vector3&)
extern "C"  void ConstantForce_INTERNAL_get_relativeForce_m1972588665 (ConstantForce_t1490594333 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConstantForce::INTERNAL_set_relativeForce(UnityEngine.Vector3&)
extern "C"  void ConstantForce_INTERNAL_set_relativeForce_m3785287557 (ConstantForce_t1490594333 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ConstantForce::get_torque()
extern "C"  Vector3_t4282066566  ConstantForce_get_torque_m890021035 (ConstantForce_t1490594333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConstantForce::set_torque(UnityEngine.Vector3)
extern "C"  void ConstantForce_set_torque_m1660927584 (ConstantForce_t1490594333 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConstantForce::INTERNAL_get_torque(UnityEngine.Vector3&)
extern "C"  void ConstantForce_INTERNAL_get_torque_m3055955512 (ConstantForce_t1490594333 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConstantForce::INTERNAL_set_torque(UnityEngine.Vector3&)
extern "C"  void ConstantForce_INTERNAL_set_torque_m409545900 (ConstantForce_t1490594333 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ConstantForce::get_relativeTorque()
extern "C"  Vector3_t4282066566  ConstantForce_get_relativeTorque_m431970199 (ConstantForce_t1490594333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConstantForce::set_relativeTorque(UnityEngine.Vector3)
extern "C"  void ConstantForce_set_relativeTorque_m1896626676 (ConstantForce_t1490594333 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConstantForce::INTERNAL_get_relativeTorque(UnityEngine.Vector3&)
extern "C"  void ConstantForce_INTERNAL_get_relativeTorque_m269588772 (ConstantForce_t1490594333 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ConstantForce::INTERNAL_set_relativeTorque(UnityEngine.Vector3&)
extern "C"  void ConstantForce_INTERNAL_set_relativeTorque_m628679576 (ConstantForce_t1490594333 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
