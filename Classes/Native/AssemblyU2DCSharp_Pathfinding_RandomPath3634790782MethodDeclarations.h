﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RandomPath
struct RandomPath_t3634790782;
// OnPathDelegate
struct OnPathDelegate_t598607977;
// Pathfinding.Path
struct Path_t1974241691;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.NNConstraint
struct NNConstraint_t758567699;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// System.String
struct String_t;
// Pathfinding.PathNode
struct PathNode_t417131581;
// Pathfinding.PathHandler
struct PathHandler_t918952263;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_OnPathDelegate598607977.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Pathfinding_NNInfo1570625892.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_Pathfinding_NNConstraint758567699.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Pathfinding_PathHandler918952263.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"
#include "AssemblyU2DCSharp_PathCompleteState1625108115.h"

// System.Void Pathfinding.RandomPath::.ctor()
extern "C"  void RandomPath__ctor_m614267049 (RandomPath_t3634790782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RandomPath::.ctor(UnityEngine.Vector3,System.Int32,OnPathDelegate)
extern "C"  void RandomPath__ctor_m4278689110 (RandomPath_t3634790782 * __this, Vector3_t4282066566  ___start0, int32_t ___length1, OnPathDelegate_t598607977 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RandomPath::get_FloodingPath()
extern "C"  bool RandomPath_get_FloodingPath_m1686271329 (RandomPath_t3634790782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RandomPath::Reset()
extern "C"  void RandomPath_Reset_m2555667286 (RandomPath_t3634790782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RandomPath::Recycle()
extern "C"  void RandomPath_Recycle_m2813746714 (RandomPath_t3634790782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RandomPath Pathfinding.RandomPath::Construct(UnityEngine.Vector3,System.Int32,OnPathDelegate)
extern "C"  RandomPath_t3634790782 * RandomPath_Construct_m163803596 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, int32_t ___length1, OnPathDelegate_t598607977 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RandomPath Pathfinding.RandomPath::Setup(UnityEngine.Vector3,System.Int32,OnPathDelegate)
extern "C"  RandomPath_t3634790782 * RandomPath_Setup_m1405763174 (RandomPath_t3634790782 * __this, Vector3_t4282066566  ___start0, int32_t ___length1, OnPathDelegate_t598607977 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RandomPath::ReturnPath()
extern "C"  void RandomPath_ReturnPath_m646884400 (RandomPath_t3634790782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RandomPath::Prepare()
extern "C"  void RandomPath_Prepare_m536095630 (RandomPath_t3634790782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RandomPath::Initialize()
extern "C"  void RandomPath_Initialize_m3817693067 (RandomPath_t3634790782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RandomPath::CalculateStep(System.Int64)
extern "C"  void RandomPath_CalculateStep_m2615716331 (RandomPath_t3634790782 * __this, int64_t ___targetTick0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RandomPath::ilo_Invoke1(OnPathDelegate,Pathfinding.Path)
extern "C"  void RandomPath_ilo_Invoke1_m2372175347 (Il2CppObject * __this /* static, unused */, OnPathDelegate_t598607977 * ____this0, Path_t1974241691 * ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.RandomPath::ilo_GetNearest2(AstarPath,UnityEngine.Vector3,Pathfinding.NNConstraint,Pathfinding.GraphNode)
extern "C"  NNInfo_t1570625892  RandomPath_ilo_GetNearest2_m1122398513 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, Vector3_t4282066566  ___position1, NNConstraint_t758567699 * ___constraint2, GraphNode_t23612370 * ___hint3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.RandomPath::ilo_op_Explicit3(UnityEngine.Vector3)
extern "C"  Int3_t1974045594  RandomPath_ilo_op_Explicit3_m1579721243 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RandomPath::ilo_LogError4(Pathfinding.Path,System.String)
extern "C"  void RandomPath_ilo_LogError4_m3687905873 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, String_t* ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RandomPath::ilo_Error5(Pathfinding.Path)
extern "C"  void RandomPath_ilo_Error5_m3721438380 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathNode Pathfinding.RandomPath::ilo_GetPathNode6(Pathfinding.PathHandler,Pathfinding.GraphNode)
extern "C"  PathNode_t417131581 * RandomPath_ilo_GetPathNode6_m2133093482 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RandomPath::ilo_set_cost7(Pathfinding.PathNode,System.UInt32)
extern "C"  void RandomPath_ilo_set_cost7_m396619236 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathNode Pathfinding.RandomPath::ilo_PopNode8(Pathfinding.PathHandler)
extern "C"  PathNode_t417131581 * RandomPath_ilo_PopNode8_m2723528248 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RandomPath::ilo_HeapEmpty9(Pathfinding.PathHandler)
extern "C"  bool RandomPath_ilo_HeapEmpty9_m4044566941 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RandomPath::ilo_set_CompleteState10(Pathfinding.Path,PathCompleteState)
extern "C"  void RandomPath_ilo_set_CompleteState10_m2263689624 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
