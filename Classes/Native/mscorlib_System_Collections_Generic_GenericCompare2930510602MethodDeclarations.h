﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<Pathfinding.AdvancedSmooth/Turn>
struct GenericComparer_1_t2930510602;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_AdvancedSmooth_Turn465195378.h"

// System.Void System.Collections.Generic.GenericComparer`1<Pathfinding.AdvancedSmooth/Turn>::.ctor()
extern "C"  void GenericComparer_1__ctor_m134379058_gshared (GenericComparer_1_t2930510602 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m134379058(__this, method) ((  void (*) (GenericComparer_1_t2930510602 *, const MethodInfo*))GenericComparer_1__ctor_m134379058_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<Pathfinding.AdvancedSmooth/Turn>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m2628184709_gshared (GenericComparer_1_t2930510602 * __this, Turn_t465195378  ___x0, Turn_t465195378  ___y1, const MethodInfo* method);
#define GenericComparer_1_Compare_m2628184709(__this, ___x0, ___y1, method) ((  int32_t (*) (GenericComparer_1_t2930510602 *, Turn_t465195378 , Turn_t465195378 , const MethodInfo*))GenericComparer_1_Compare_m2628184709_gshared)(__this, ___x0, ___y1, method)
