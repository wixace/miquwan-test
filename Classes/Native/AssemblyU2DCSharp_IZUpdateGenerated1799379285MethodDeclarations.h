﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IZUpdateGenerated
struct IZUpdateGenerated_t1799379285;
// JSVCall
struct JSVCall_t3708497963;
// IZUpdate
struct IZUpdate_t3482043738;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void IZUpdateGenerated::.ctor()
extern "C"  void IZUpdateGenerated__ctor_m2034893750 (IZUpdateGenerated_t1799379285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IZUpdateGenerated::IZUpdate_ZUpdate(JSVCall,System.Int32)
extern "C"  bool IZUpdateGenerated_IZUpdate_ZUpdate_m1812739048 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IZUpdateGenerated::__Register()
extern "C"  void IZUpdateGenerated___Register_m2142460625 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IZUpdateGenerated::ilo_ZUpdate1(IZUpdate)
extern "C"  void IZUpdateGenerated_ilo_ZUpdate1_m2519396181 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
