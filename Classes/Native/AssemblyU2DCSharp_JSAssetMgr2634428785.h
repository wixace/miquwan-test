﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t3134621005;
// JSEngine
struct JSEngine_t2847479019;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.TextAsset>
struct Dictionary_2_t361581051;
// System.Action
struct Action_t3771233898;

#include "AssemblyU2DCSharp_Singleton_1_gen2887244178.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSAssetMgr
struct  JSAssetMgr_t2634428785  : public Singleton_1_t2887244178
{
public:
	// UnityEngine.WWW JSAssetMgr::www
	WWW_t3134621005 * ___www_1;
	// System.Int32 JSAssetMgr::totalLoadCount
	int32_t ___totalLoadCount_2;
	// System.Single JSAssetMgr::progress_www
	float ___progress_www_3;
	// JSEngine JSAssetMgr::jsEngine
	JSEngine_t2847479019 * ___jsEngine_4;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.TextAsset> JSAssetMgr::dataDic
	Dictionary_2_t361581051 * ___dataDic_5;
	// System.Action JSAssetMgr::onInitJSEngine
	Action_t3771233898 * ___onInitJSEngine_6;
	// System.Boolean JSAssetMgr::<isLoaded>k__BackingField
	bool ___U3CisLoadedU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_www_1() { return static_cast<int32_t>(offsetof(JSAssetMgr_t2634428785, ___www_1)); }
	inline WWW_t3134621005 * get_www_1() const { return ___www_1; }
	inline WWW_t3134621005 ** get_address_of_www_1() { return &___www_1; }
	inline void set_www_1(WWW_t3134621005 * value)
	{
		___www_1 = value;
		Il2CppCodeGenWriteBarrier(&___www_1, value);
	}

	inline static int32_t get_offset_of_totalLoadCount_2() { return static_cast<int32_t>(offsetof(JSAssetMgr_t2634428785, ___totalLoadCount_2)); }
	inline int32_t get_totalLoadCount_2() const { return ___totalLoadCount_2; }
	inline int32_t* get_address_of_totalLoadCount_2() { return &___totalLoadCount_2; }
	inline void set_totalLoadCount_2(int32_t value)
	{
		___totalLoadCount_2 = value;
	}

	inline static int32_t get_offset_of_progress_www_3() { return static_cast<int32_t>(offsetof(JSAssetMgr_t2634428785, ___progress_www_3)); }
	inline float get_progress_www_3() const { return ___progress_www_3; }
	inline float* get_address_of_progress_www_3() { return &___progress_www_3; }
	inline void set_progress_www_3(float value)
	{
		___progress_www_3 = value;
	}

	inline static int32_t get_offset_of_jsEngine_4() { return static_cast<int32_t>(offsetof(JSAssetMgr_t2634428785, ___jsEngine_4)); }
	inline JSEngine_t2847479019 * get_jsEngine_4() const { return ___jsEngine_4; }
	inline JSEngine_t2847479019 ** get_address_of_jsEngine_4() { return &___jsEngine_4; }
	inline void set_jsEngine_4(JSEngine_t2847479019 * value)
	{
		___jsEngine_4 = value;
		Il2CppCodeGenWriteBarrier(&___jsEngine_4, value);
	}

	inline static int32_t get_offset_of_dataDic_5() { return static_cast<int32_t>(offsetof(JSAssetMgr_t2634428785, ___dataDic_5)); }
	inline Dictionary_2_t361581051 * get_dataDic_5() const { return ___dataDic_5; }
	inline Dictionary_2_t361581051 ** get_address_of_dataDic_5() { return &___dataDic_5; }
	inline void set_dataDic_5(Dictionary_2_t361581051 * value)
	{
		___dataDic_5 = value;
		Il2CppCodeGenWriteBarrier(&___dataDic_5, value);
	}

	inline static int32_t get_offset_of_onInitJSEngine_6() { return static_cast<int32_t>(offsetof(JSAssetMgr_t2634428785, ___onInitJSEngine_6)); }
	inline Action_t3771233898 * get_onInitJSEngine_6() const { return ___onInitJSEngine_6; }
	inline Action_t3771233898 ** get_address_of_onInitJSEngine_6() { return &___onInitJSEngine_6; }
	inline void set_onInitJSEngine_6(Action_t3771233898 * value)
	{
		___onInitJSEngine_6 = value;
		Il2CppCodeGenWriteBarrier(&___onInitJSEngine_6, value);
	}

	inline static int32_t get_offset_of_U3CisLoadedU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(JSAssetMgr_t2634428785, ___U3CisLoadedU3Ek__BackingField_7)); }
	inline bool get_U3CisLoadedU3Ek__BackingField_7() const { return ___U3CisLoadedU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CisLoadedU3Ek__BackingField_7() { return &___U3CisLoadedU3Ek__BackingField_7; }
	inline void set_U3CisLoadedU3Ek__BackingField_7(bool value)
	{
		___U3CisLoadedU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
