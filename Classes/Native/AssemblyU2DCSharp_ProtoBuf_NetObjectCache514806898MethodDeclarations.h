﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.NetObjectCache
struct NetObjectCache_t514806898;
// ProtoBuf.Meta.MutableList
struct MutableList_t4210537526;
// System.Object
struct Il2CppObject;
// ProtoBuf.Meta.BasicList
struct BasicList_t528018366;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_BasicList528018366.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_MutableList4210537526.h"

// System.Void ProtoBuf.NetObjectCache::.ctor()
extern "C"  void NetObjectCache__ctor_m2904559362 (NetObjectCache_t514806898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MutableList ProtoBuf.NetObjectCache::get_List()
extern "C"  MutableList_t4210537526 * NetObjectCache_get_List_m3683362292 (NetObjectCache_t514806898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.NetObjectCache::GetKeyedObject(System.Int32)
extern "C"  Il2CppObject * NetObjectCache_GetKeyedObject_m1037882405 (NetObjectCache_t514806898 * __this, int32_t ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.NetObjectCache::SetKeyedObject(System.Int32,System.Object)
extern "C"  void NetObjectCache_SetKeyedObject_m3048334460 (NetObjectCache_t514806898 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.NetObjectCache::AddObjectKey(System.Object,System.Boolean&)
extern "C"  int32_t NetObjectCache_AddObjectKey_m47435760 (NetObjectCache_t514806898 * __this, Il2CppObject * ___value0, bool* ___existing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.NetObjectCache::RegisterTrappedObject(System.Object)
extern "C"  void NetObjectCache_RegisterTrappedObject_m1470685430 (NetObjectCache_t514806898 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.NetObjectCache::Clear()
extern "C"  void NetObjectCache_Clear_m310692653 (NetObjectCache_t514806898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.NetObjectCache::ilo_get_Item1(ProtoBuf.Meta.BasicList,System.Int32)
extern "C"  Il2CppObject * NetObjectCache_ilo_get_Item1_m1179205762 (Il2CppObject * __this /* static, unused */, BasicList_t528018366 * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.NetObjectCache::ilo_get_Item2(ProtoBuf.Meta.MutableList,System.Int32)
extern "C"  Il2CppObject * NetObjectCache_ilo_get_Item2_m1974685307 (Il2CppObject * __this /* static, unused */, MutableList_t4210537526 * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.NetObjectCache::ilo_set_Item3(ProtoBuf.Meta.MutableList,System.Int32,System.Object)
extern "C"  void NetObjectCache_ilo_set_Item3_m3544869665 (Il2CppObject * __this /* static, unused */, MutableList_t4210537526 * ____this0, int32_t ___index1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.NetObjectCache::ilo_get_Count4(ProtoBuf.Meta.BasicList)
extern "C"  int32_t NetObjectCache_ilo_get_Count4_m3919529687 (Il2CppObject * __this /* static, unused */, BasicList_t528018366 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
