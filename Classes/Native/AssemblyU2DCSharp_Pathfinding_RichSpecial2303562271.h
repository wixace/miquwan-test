﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.NodeLink2
struct NodeLink2_t1645404664;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "AssemblyU2DCSharp_Pathfinding_RichPathPart2520985130.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RichSpecial
struct  RichSpecial_t2303562271  : public RichPathPart_t2520985130
{
public:
	// Pathfinding.NodeLink2 Pathfinding.RichSpecial::nodeLink
	NodeLink2_t1645404664 * ___nodeLink_0;
	// UnityEngine.Transform Pathfinding.RichSpecial::first
	Transform_t1659122786 * ___first_1;
	// UnityEngine.Transform Pathfinding.RichSpecial::second
	Transform_t1659122786 * ___second_2;
	// System.Boolean Pathfinding.RichSpecial::reverse
	bool ___reverse_3;

public:
	inline static int32_t get_offset_of_nodeLink_0() { return static_cast<int32_t>(offsetof(RichSpecial_t2303562271, ___nodeLink_0)); }
	inline NodeLink2_t1645404664 * get_nodeLink_0() const { return ___nodeLink_0; }
	inline NodeLink2_t1645404664 ** get_address_of_nodeLink_0() { return &___nodeLink_0; }
	inline void set_nodeLink_0(NodeLink2_t1645404664 * value)
	{
		___nodeLink_0 = value;
		Il2CppCodeGenWriteBarrier(&___nodeLink_0, value);
	}

	inline static int32_t get_offset_of_first_1() { return static_cast<int32_t>(offsetof(RichSpecial_t2303562271, ___first_1)); }
	inline Transform_t1659122786 * get_first_1() const { return ___first_1; }
	inline Transform_t1659122786 ** get_address_of_first_1() { return &___first_1; }
	inline void set_first_1(Transform_t1659122786 * value)
	{
		___first_1 = value;
		Il2CppCodeGenWriteBarrier(&___first_1, value);
	}

	inline static int32_t get_offset_of_second_2() { return static_cast<int32_t>(offsetof(RichSpecial_t2303562271, ___second_2)); }
	inline Transform_t1659122786 * get_second_2() const { return ___second_2; }
	inline Transform_t1659122786 ** get_address_of_second_2() { return &___second_2; }
	inline void set_second_2(Transform_t1659122786 * value)
	{
		___second_2 = value;
		Il2CppCodeGenWriteBarrier(&___second_2, value);
	}

	inline static int32_t get_offset_of_reverse_3() { return static_cast<int32_t>(offsetof(RichSpecial_t2303562271, ___reverse_3)); }
	inline bool get_reverse_3() const { return ___reverse_3; }
	inline bool* get_address_of_reverse_3() { return &___reverse_3; }
	inline void set_reverse_3(bool value)
	{
		___reverse_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
