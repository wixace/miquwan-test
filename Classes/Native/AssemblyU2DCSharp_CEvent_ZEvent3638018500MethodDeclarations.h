﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void CEvent.ZEvent::.ctor(System.String)
extern "C"  void ZEvent__ctor_m2065206350 (ZEvent_t3638018500 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent.ZEvent::.ctor(System.String,System.Object[])
extern "C"  void ZEvent__ctor_m1301399226 (ZEvent_t3638018500 * __this, String_t* ___name0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent.ZEvent::.ctor(System.String,System.Object)
extern "C"  void ZEvent__ctor_m2915317404 (ZEvent_t3638018500 * __this, String_t* ___name0, Il2CppObject * ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent.ZEvent::.ctor(System.String,System.Object,System.Object)
extern "C"  void ZEvent__ctor_m1855504490 (ZEvent_t3638018500 * __this, String_t* ___name0, Il2CppObject * ___arg1, Il2CppObject * ___arg12, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent.ZEvent::.ctor(System.String,System.Object,System.Object,System.Object)
extern "C"  void ZEvent__ctor_m3872983480 (ZEvent_t3638018500 * __this, String_t* ___name0, Il2CppObject * ___arg1, Il2CppObject * ___arg12, Il2CppObject * ___arg23, const MethodInfo* method) IL2CPP_METHOD_ATTR;
