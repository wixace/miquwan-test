﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIScrollViewGenerated
struct UIScrollViewGenerated_t4150891369;
// JSVCall
struct JSVCall_t3708497963;
// UIScrollView/OnDragNotification
struct OnDragNotification_t2323474503;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Delegate
struct Delegate_t3310234105;
// UIPanel
struct UIPanel_t295209936;
// UIScrollView
struct UIScrollView_t2113479878;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "AssemblyU2DCSharp_UIScrollView2113479878.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UIScrollViewGenerated::.ctor()
extern "C"  void UIScrollViewGenerated__ctor_m4029819938 (UIScrollViewGenerated_t4150891369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::UIScrollView_UIScrollView1(JSVCall,System.Int32)
extern "C"  bool UIScrollViewGenerated_UIScrollView_UIScrollView1_m781005136 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_list(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_list_m1190431792 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_topSpace(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_topSpace_m630149501 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_movement(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_movement_m4144347967 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_dragEffect(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_dragEffect_m1284676297 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_restrictWithinPanel(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_restrictWithinPanel_m1640315809 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_disableDragIfFits(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_disableDragIfFits_m1846749891 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_smoothDragStart(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_smoothDragStart_m2630280574 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_iOSDragEmulation(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_iOSDragEmulation_m1362439033 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_scrollWheelFactor(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_scrollWheelFactor_m3131140641 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_momentumAmount(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_momentumAmount_m3799521278 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_dampenStrength(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_dampenStrength_m1361340772 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_horizontalScrollBar(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_horizontalScrollBar_m3330084604 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_verticalScrollBar(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_verticalScrollBar_m628651982 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_showScrollBars(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_showScrollBars_m1969732164 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_customMovement(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_customMovement_m3412089294 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_contentPivot(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_contentPivot_m431782373 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIScrollView/OnDragNotification UIScrollViewGenerated::UIScrollView_onDragStarted_GetDelegate_member16_arg0(CSRepresentedObject)
extern "C"  OnDragNotification_t2323474503 * UIScrollViewGenerated_UIScrollView_onDragStarted_GetDelegate_member16_arg0_m2080065658 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_onDragStarted(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_onDragStarted_m420974448 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIScrollView/OnDragNotification UIScrollViewGenerated::UIScrollView_onDragFinished_GetDelegate_member17_arg0(CSRepresentedObject)
extern "C"  OnDragNotification_t2323474503 * UIScrollViewGenerated_UIScrollView_onDragFinished_GetDelegate_member17_arg0_m2727694492 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_onDragFinished(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_onDragFinished_m101143913 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIScrollView/OnDragNotification UIScrollViewGenerated::UIScrollView_onMomentumMove_GetDelegate_member18_arg0(CSRepresentedObject)
extern "C"  OnDragNotification_t2323474503 * UIScrollViewGenerated_UIScrollView_onMomentumMove_GetDelegate_member18_arg0_m2068518970 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_onMomentumMove(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_onMomentumMove_m3380832774 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIScrollView/OnDragNotification UIScrollViewGenerated::UIScrollView_onStoppedMoving_GetDelegate_member19_arg0(CSRepresentedObject)
extern "C"  OnDragNotification_t2323474503 * UIScrollViewGenerated_UIScrollView_onStoppedMoving_GetDelegate_member19_arg0_m3954499695 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_onStoppedMoving(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_onStoppedMoving_m2933892514 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_yueJieFanWei(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_yueJieFanWei_m3129559433 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_centerOnChild(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_centerOnChild_m3523158422 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_panel(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_panel_m3601570682 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_isDragging(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_isDragging_m1690221717 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_bounds(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_bounds_m1277685241 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_canMoveHorizontally(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_canMoveHorizontally_m2314395692 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_canMoveVertically(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_canMoveVertically_m388218714 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_shouldMoveHorizontally(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_shouldMoveHorizontally_m2597620153 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_shouldMoveVertically(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_shouldMoveVertically_m2676779047 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_shouldMove(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_shouldMove_m379705706 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::UIScrollView_currentMomentum(JSVCall)
extern "C"  void UIScrollViewGenerated_UIScrollView_currentMomentum_m3093743853 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::UIScrollView_DisableSpring(JSVCall,System.Int32)
extern "C"  bool UIScrollViewGenerated_UIScrollView_DisableSpring_m1888853146 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::UIScrollView_Drag(JSVCall,System.Int32)
extern "C"  bool UIScrollViewGenerated_UIScrollView_Drag_m2171809425 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::UIScrollView_InvalidateBounds(JSVCall,System.Int32)
extern "C"  bool UIScrollViewGenerated_UIScrollView_InvalidateBounds_m3557576237 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::UIScrollView_MoveAbsolute__Vector3(JSVCall,System.Int32)
extern "C"  bool UIScrollViewGenerated_UIScrollView_MoveAbsolute__Vector3_m2605126957 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::UIScrollView_MoveRelative__Vector3(JSVCall,System.Int32)
extern "C"  bool UIScrollViewGenerated_UIScrollView_MoveRelative__Vector3_m2285616440 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::UIScrollView_OnPan__Vector2(JSVCall,System.Int32)
extern "C"  bool UIScrollViewGenerated_UIScrollView_OnPan__Vector2_m400982382 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::UIScrollView_OnScrollBar(JSVCall,System.Int32)
extern "C"  bool UIScrollViewGenerated_UIScrollView_OnScrollBar_m3228604396 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::UIScrollView_Press__Boolean(JSVCall,System.Int32)
extern "C"  bool UIScrollViewGenerated_UIScrollView_Press__Boolean_m4191245026 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::UIScrollView_ResetPosition(JSVCall,System.Int32)
extern "C"  bool UIScrollViewGenerated_UIScrollView_ResetPosition_m3477414109 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::UIScrollView_ResetPositionDown(JSVCall,System.Int32)
extern "C"  bool UIScrollViewGenerated_UIScrollView_ResetPositionDown_m51975711 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::UIScrollView_RestrictWithinBounds__Boolean__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UIScrollViewGenerated_UIScrollView_RestrictWithinBounds__Boolean__Boolean__Boolean_m3867016977 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::UIScrollView_RestrictWithinBounds__Boolean(JSVCall,System.Int32)
extern "C"  bool UIScrollViewGenerated_UIScrollView_RestrictWithinBounds__Boolean_m1142477329 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::UIScrollView_Scroll__Single(JSVCall,System.Int32)
extern "C"  bool UIScrollViewGenerated_UIScrollView_Scroll__Single_m222945714 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::UIScrollView_SetDragAmount__Single__Single__Boolean(JSVCall,System.Int32)
extern "C"  bool UIScrollViewGenerated_UIScrollView_SetDragAmount__Single__Single__Boolean_m1761048199 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::UIScrollView_UpdatePosition(JSVCall,System.Int32)
extern "C"  bool UIScrollViewGenerated_UIScrollView_UpdatePosition_m3970061551 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::UIScrollView_UpdateScrollbars__Boolean(JSVCall,System.Int32)
extern "C"  bool UIScrollViewGenerated_UIScrollView_UpdateScrollbars__Boolean_m1069147127 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::UIScrollView_UpdateScrollbars(JSVCall,System.Int32)
extern "C"  bool UIScrollViewGenerated_UIScrollView_UpdateScrollbars_m3483643539 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::__Register()
extern "C"  void UIScrollViewGenerated___Register_m334468581 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIScrollView/OnDragNotification UIScrollViewGenerated::<UIScrollView_onDragStarted>m__15C()
extern "C"  OnDragNotification_t2323474503 * UIScrollViewGenerated_U3CUIScrollView_onDragStartedU3Em__15C_m1543687509 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIScrollView/OnDragNotification UIScrollViewGenerated::<UIScrollView_onDragFinished>m__15E()
extern "C"  OnDragNotification_t2323474503 * UIScrollViewGenerated_U3CUIScrollView_onDragFinishedU3Em__15E_m3190322588 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIScrollView/OnDragNotification UIScrollViewGenerated::<UIScrollView_onMomentumMove>m__160()
extern "C"  OnDragNotification_t2323474503 * UIScrollViewGenerated_U3CUIScrollView_onMomentumMoveU3Em__160_m2175053763 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIScrollView/OnDragNotification UIScrollViewGenerated::<UIScrollView_onStoppedMoving>m__162()
extern "C"  OnDragNotification_t2323474503 * UIScrollViewGenerated_U3CUIScrollView_onStoppedMovingU3Em__162_m2558947733 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIScrollViewGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UIScrollViewGenerated_ilo_getObject1_m21467604 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UIScrollViewGenerated_ilo_addJSCSRel2_m716553823 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::ilo_setEnum3(System.Int32,System.Int32)
extern "C"  void UIScrollViewGenerated_ilo_setEnum3_m523321473 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIScrollViewGenerated::ilo_getEnum4(System.Int32)
extern "C"  int32_t UIScrollViewGenerated_ilo_getEnum4_m276434581 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void UIScrollViewGenerated_ilo_setBooleanS5_m1783894927 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::ilo_getBooleanS6(System.Int32)
extern "C"  bool UIScrollViewGenerated_ilo_getBooleanS6_m2835463367 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::ilo_setSingle7(System.Int32,System.Single)
extern "C"  void UIScrollViewGenerated_ilo_setSingle7_m2569683992 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIScrollViewGenerated::ilo_getObject8(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIScrollViewGenerated_ilo_getObject8_m452254538 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIScrollViewGenerated::ilo_setObject9(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIScrollViewGenerated_ilo_setObject9_m1682575188 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UIScrollViewGenerated::ilo_getVector2S10(System.Int32)
extern "C"  Vector2_t4282066565  UIScrollViewGenerated_ilo_getVector2S10_m2016802162 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::ilo_addJSFunCSDelegateRel11(System.Int32,System.Delegate)
extern "C"  void UIScrollViewGenerated_ilo_addJSFunCSDelegateRel11_m587647605 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIPanel UIScrollViewGenerated::ilo_get_panel12(UIScrollView)
extern "C"  UIPanel_t295209936 * UIScrollViewGenerated_ilo_get_panel12_m2596520774 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UIScrollViewGenerated::ilo_get_bounds13(UIScrollView)
extern "C"  Bounds_t2711641849  UIScrollViewGenerated_ilo_get_bounds13_m4173906910 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::ilo_get_shouldMoveHorizontally14(UIScrollView)
extern "C"  bool UIScrollViewGenerated_ilo_get_shouldMoveHorizontally14_m2395563740 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::ilo_get_shouldMoveVertically15(UIScrollView)
extern "C"  bool UIScrollViewGenerated_ilo_get_shouldMoveVertically15_m189098799 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::ilo_setVector3S16(System.Int32,UnityEngine.Vector3)
extern "C"  void UIScrollViewGenerated_ilo_setVector3S16_m912010395 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::ilo_DisableSpring17(UIScrollView)
extern "C"  void UIScrollViewGenerated_ilo_DisableSpring17_m3653625826 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::ilo_Drag18(UIScrollView)
extern "C"  void UIScrollViewGenerated_ilo_Drag18_m1466228842 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::ilo_MoveRelative19(UIScrollView,UnityEngine.Vector3)
extern "C"  void UIScrollViewGenerated_ilo_MoveRelative19_m3347535047 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, Vector3_t4282066566  ___relative1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::ilo_Press20(UIScrollView,System.Boolean)
extern "C"  void UIScrollViewGenerated_ilo_Press20_m3516731221 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, bool ___pressed1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::ilo_ResetPositionDown21(UIScrollView)
extern "C"  void UIScrollViewGenerated_ilo_ResetPositionDown21_m1853902208 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::ilo_RestrictWithinBounds22(UIScrollView,System.Boolean)
extern "C"  bool UIScrollViewGenerated_ilo_RestrictWithinBounds22_m3550378086 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, bool ___instant1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIScrollViewGenerated::ilo_getSingle23(System.Int32)
extern "C"  float UIScrollViewGenerated_ilo_getSingle23_m2612428713 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated::ilo_UpdateScrollbars24(UIScrollView)
extern "C"  void UIScrollViewGenerated_ilo_UpdateScrollbars24_m407564999 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollViewGenerated::ilo_isFunctionS25(System.Int32)
extern "C"  bool UIScrollViewGenerated_ilo_isFunctionS25_m3622182470 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIScrollView/OnDragNotification UIScrollViewGenerated::ilo_UIScrollView_onMomentumMove_GetDelegate_member18_arg026(CSRepresentedObject)
extern "C"  OnDragNotification_t2323474503 * UIScrollViewGenerated_ilo_UIScrollView_onMomentumMove_GetDelegate_member18_arg026_m740548585 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject UIScrollViewGenerated::ilo_getFunctionS27(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * UIScrollViewGenerated_ilo_getFunctionS27_m618835635 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIScrollView/OnDragNotification UIScrollViewGenerated::ilo_UIScrollView_onStoppedMoving_GetDelegate_member19_arg028(CSRepresentedObject)
extern "C"  OnDragNotification_t2323474503 * UIScrollViewGenerated_ilo_UIScrollView_onStoppedMoving_GetDelegate_member19_arg028_m2746190870 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
