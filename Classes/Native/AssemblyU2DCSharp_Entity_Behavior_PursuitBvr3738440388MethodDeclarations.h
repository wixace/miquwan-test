﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.PursuitBvr
struct PursuitBvr_t3738440388;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// NpcMgr
struct NpcMgr_t2339534743;
// CombatEntity
struct CombatEntity_t684137495;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// AnimationRunner
struct AnimationRunner_t1015409588;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_AnimationRunner1015409588.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void Entity.Behavior.PursuitBvr::.ctor()
extern "C"  void PursuitBvr__ctor_m922525926 (PursuitBvr_t3738440388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.PursuitBvr::get_id()
extern "C"  uint8_t PursuitBvr_get_id_m3240423924 (PursuitBvr_t3738440388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.PursuitBvr::Reason()
extern "C"  void PursuitBvr_Reason_m2379888034 (PursuitBvr_t3738440388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.PursuitBvr::Action()
extern "C"  void PursuitBvr_Action_m1576594708 (PursuitBvr_t3738440388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.PursuitBvr::DoEntering()
extern "C"  void PursuitBvr_DoEntering_m2043268179 (PursuitBvr_t3738440388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.PursuitBvr::DoLeaving()
extern "C"  void PursuitBvr_DoLeaving_m1065011437 (PursuitBvr_t3738440388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.PursuitBvr::SetParams(System.Object[])
extern "C"  void PursuitBvr_SetParams_m324755238 (PursuitBvr_t3738440388 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NpcMgr Entity.Behavior.PursuitBvr::ilo_get_instance1()
extern "C"  NpcMgr_t2339534743 * PursuitBvr_ilo_get_instance1_m844775646 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.PursuitBvr::ilo_set_atkTarget2(CombatEntity,CombatEntity)
extern "C"  void PursuitBvr_ilo_set_atkTarget2_m2665833359 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, CombatEntity_t684137495 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.PursuitBvr::ilo_get_entity3(Entity.Behavior.IBehavior)
extern "C"  CombatEntity_t684137495 * PursuitBvr_ilo_get_entity3_m1176659414 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.PursuitBvr::ilo_TranBehavior4(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void PursuitBvr_ilo_TranBehavior4_m3477857388 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___id1, ObjectU5BU5D_t1108656482* ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.PursuitBvr::ilo_Play5(AnimationRunner,AnimationRunner/AniType,System.Single,System.Boolean)
extern "C"  void PursuitBvr_ilo_Play5_m3873208955 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, int32_t ___aniType1, float ___playTime2, bool ___isskill3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Entity.Behavior.PursuitBvr::ilo_get_InitPosition6(CombatEntity)
extern "C"  Vector3_t4282066566  PursuitBvr_ilo_get_InitPosition6_m668360148 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
