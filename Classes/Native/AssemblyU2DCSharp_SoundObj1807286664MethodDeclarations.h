﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SoundObj
struct SoundObj_t1807286664;

#include "codegen/il2cpp-codegen.h"

// System.Void SoundObj::.ctor()
extern "C"  void SoundObj__ctor_m3445421587 (SoundObj_t1807286664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
