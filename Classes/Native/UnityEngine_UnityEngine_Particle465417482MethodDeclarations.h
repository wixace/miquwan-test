﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Particle
struct Particle_t465417482;
struct Particle_t465417482_marshaled_pinvoke;
struct Particle_t465417482_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Particle465417482.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// UnityEngine.Vector3 UnityEngine.Particle::get_position()
extern "C"  Vector3_t4282066566  Particle_get_position_m123839369 (Particle_t465417482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Particle::set_position(UnityEngine.Vector3)
extern "C"  void Particle_set_position_m1704412534 (Particle_t465417482 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Particle::get_velocity()
extern "C"  Vector3_t4282066566  Particle_get_velocity_m1068182077 (Particle_t465417482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Particle::set_velocity(UnityEngine.Vector3)
extern "C"  void Particle_set_velocity_m3803101762 (Particle_t465417482 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Particle::get_energy()
extern "C"  float Particle_get_energy_m703280378 (Particle_t465417482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Particle::set_energy(System.Single)
extern "C"  void Particle_set_energy_m3596059529 (Particle_t465417482 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Particle::get_startEnergy()
extern "C"  float Particle_get_startEnergy_m4007451450 (Particle_t465417482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Particle::set_startEnergy(System.Single)
extern "C"  void Particle_set_startEnergy_m3923772185 (Particle_t465417482 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Particle::get_size()
extern "C"  float Particle_get_size_m2355073139 (Particle_t465417482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Particle::set_size(System.Single)
extern "C"  void Particle_set_size_m3933117104 (Particle_t465417482 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Particle::get_rotation()
extern "C"  float Particle_get_rotation_m4690992 (Particle_t465417482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Particle::set_rotation(System.Single)
extern "C"  void Particle_set_rotation_m474236435 (Particle_t465417482 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Particle::get_angularVelocity()
extern "C"  float Particle_get_angularVelocity_m998049231 (Particle_t465417482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Particle::set_angularVelocity(System.Single)
extern "C"  void Particle_set_angularVelocity_m206372516 (Particle_t465417482 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Particle::get_color()
extern "C"  Color_t4194546905  Particle_get_color_m641254776 (Particle_t465417482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Particle::set_color(UnityEngine.Color)
extern "C"  void Particle_set_color_m892839131 (Particle_t465417482 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Particle_t465417482;
struct Particle_t465417482_marshaled_pinvoke;

extern "C" void Particle_t465417482_marshal_pinvoke(const Particle_t465417482& unmarshaled, Particle_t465417482_marshaled_pinvoke& marshaled);
extern "C" void Particle_t465417482_marshal_pinvoke_back(const Particle_t465417482_marshaled_pinvoke& marshaled, Particle_t465417482& unmarshaled);
extern "C" void Particle_t465417482_marshal_pinvoke_cleanup(Particle_t465417482_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Particle_t465417482;
struct Particle_t465417482_marshaled_com;

extern "C" void Particle_t465417482_marshal_com(const Particle_t465417482& unmarshaled, Particle_t465417482_marshaled_com& marshaled);
extern "C" void Particle_t465417482_marshal_com_back(const Particle_t465417482_marshaled_com& marshaled, Particle_t465417482& unmarshaled);
extern "C" void Particle_t465417482_marshal_com_cleanup(Particle_t465417482_marshaled_com& marshaled);
