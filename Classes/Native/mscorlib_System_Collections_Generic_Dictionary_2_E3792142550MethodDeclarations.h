﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,GenericTypeCache/TypeMembers>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3380756035(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3792142550 *, Dictionary_2_t2474819158 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,GenericTypeCache/TypeMembers>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m901646664(__this, method) ((  Il2CppObject * (*) (Enumerator_t3792142550 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,GenericTypeCache/TypeMembers>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1745981074(__this, method) ((  void (*) (Enumerator_t3792142550 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,GenericTypeCache/TypeMembers>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4097713545(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t3792142550 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,GenericTypeCache/TypeMembers>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m899887140(__this, method) ((  Il2CppObject * (*) (Enumerator_t3792142550 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,GenericTypeCache/TypeMembers>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2551551414(__this, method) ((  Il2CppObject * (*) (Enumerator_t3792142550 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,GenericTypeCache/TypeMembers>::MoveNext()
#define Enumerator_MoveNext_m1547519938(__this, method) ((  bool (*) (Enumerator_t3792142550 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,GenericTypeCache/TypeMembers>::get_Current()
#define Enumerator_get_Current_m2961139514(__this, method) ((  KeyValuePair_2_t2373599864  (*) (Enumerator_t3792142550 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,GenericTypeCache/TypeMembers>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3506458699(__this, method) ((  Type_t * (*) (Enumerator_t3792142550 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,GenericTypeCache/TypeMembers>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1329917771(__this, method) ((  TypeMembers_t2369396673 * (*) (Enumerator_t3792142550 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,GenericTypeCache/TypeMembers>::Reset()
#define Enumerator_Reset_m1004807189(__this, method) ((  void (*) (Enumerator_t3792142550 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,GenericTypeCache/TypeMembers>::VerifyState()
#define Enumerator_VerifyState_m2855227486(__this, method) ((  void (*) (Enumerator_t3792142550 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,GenericTypeCache/TypeMembers>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2134383686(__this, method) ((  void (*) (Enumerator_t3792142550 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,GenericTypeCache/TypeMembers>::Dispose()
#define Enumerator_Dispose_m1775496101(__this, method) ((  void (*) (Enumerator_t3792142550 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
