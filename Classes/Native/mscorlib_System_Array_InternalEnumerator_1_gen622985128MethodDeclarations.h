﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen622985128.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_PushType1840642452.h"

// System.Void System.Array/InternalEnumerator`1<PushType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2849606053_gshared (InternalEnumerator_1_t622985128 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2849606053(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t622985128 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2849606053_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<PushType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2161981403_gshared (InternalEnumerator_1_t622985128 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2161981403(__this, method) ((  void (*) (InternalEnumerator_1_t622985128 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2161981403_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<PushType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1493933767_gshared (InternalEnumerator_1_t622985128 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1493933767(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t622985128 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1493933767_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<PushType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1682107836_gshared (InternalEnumerator_1_t622985128 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1682107836(__this, method) ((  void (*) (InternalEnumerator_1_t622985128 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1682107836_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<PushType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m92752455_gshared (InternalEnumerator_1_t622985128 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m92752455(__this, method) ((  bool (*) (InternalEnumerator_1_t622985128 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m92752455_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<PushType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m849994988_gshared (InternalEnumerator_1_t622985128 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m849994988(__this, method) ((  int32_t (*) (InternalEnumerator_1_t622985128 *, const MethodInfo*))InternalEnumerator_1_get_Current_m849994988_gshared)(__this, method)
