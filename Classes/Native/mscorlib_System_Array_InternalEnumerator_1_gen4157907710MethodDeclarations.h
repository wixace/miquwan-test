﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4157907710.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeB1080597738.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m948831972_gshared (InternalEnumerator_1_t4157907710 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m948831972(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4157907710 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m948831972_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4096351292_gshared (InternalEnumerator_1_t4157907710 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4096351292(__this, method) ((  void (*) (InternalEnumerator_1_t4157907710 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4096351292_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m184909352_gshared (InternalEnumerator_1_t4157907710 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m184909352(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4157907710 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m184909352_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1285037499_gshared (InternalEnumerator_1_t4157907710 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1285037499(__this, method) ((  void (*) (InternalEnumerator_1_t4157907710 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1285037499_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m173218728_gshared (InternalEnumerator_1_t4157907710 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m173218728(__this, method) ((  bool (*) (InternalEnumerator_1_t4157907710 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m173218728_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::get_Current()
extern "C"  ReflectionProbeBlendInfo_t1080597738  InternalEnumerator_1_get_Current_m4088890923_gshared (InternalEnumerator_1_t4157907710 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m4088890923(__this, method) ((  ReflectionProbeBlendInfo_t1080597738  (*) (InternalEnumerator_1_t4157907710 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4088890923_gshared)(__this, method)
