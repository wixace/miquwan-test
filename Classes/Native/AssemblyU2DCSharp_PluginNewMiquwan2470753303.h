﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginNewMiquwan
struct  PluginNewMiquwan_t2470753303  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginNewMiquwan::userId
	String_t* ___userId_2;
	// System.String PluginNewMiquwan::token
	String_t* ___token_3;
	// System.String[] PluginNewMiquwan::sdkInfos
	StringU5BU5D_t4054002952* ___sdkInfos_4;
	// System.String PluginNewMiquwan::configId
	String_t* ___configId_5;
	// System.String PluginNewMiquwan::appId
	String_t* ___appId_6;

public:
	inline static int32_t get_offset_of_userId_2() { return static_cast<int32_t>(offsetof(PluginNewMiquwan_t2470753303, ___userId_2)); }
	inline String_t* get_userId_2() const { return ___userId_2; }
	inline String_t** get_address_of_userId_2() { return &___userId_2; }
	inline void set_userId_2(String_t* value)
	{
		___userId_2 = value;
		Il2CppCodeGenWriteBarrier(&___userId_2, value);
	}

	inline static int32_t get_offset_of_token_3() { return static_cast<int32_t>(offsetof(PluginNewMiquwan_t2470753303, ___token_3)); }
	inline String_t* get_token_3() const { return ___token_3; }
	inline String_t** get_address_of_token_3() { return &___token_3; }
	inline void set_token_3(String_t* value)
	{
		___token_3 = value;
		Il2CppCodeGenWriteBarrier(&___token_3, value);
	}

	inline static int32_t get_offset_of_sdkInfos_4() { return static_cast<int32_t>(offsetof(PluginNewMiquwan_t2470753303, ___sdkInfos_4)); }
	inline StringU5BU5D_t4054002952* get_sdkInfos_4() const { return ___sdkInfos_4; }
	inline StringU5BU5D_t4054002952** get_address_of_sdkInfos_4() { return &___sdkInfos_4; }
	inline void set_sdkInfos_4(StringU5BU5D_t4054002952* value)
	{
		___sdkInfos_4 = value;
		Il2CppCodeGenWriteBarrier(&___sdkInfos_4, value);
	}

	inline static int32_t get_offset_of_configId_5() { return static_cast<int32_t>(offsetof(PluginNewMiquwan_t2470753303, ___configId_5)); }
	inline String_t* get_configId_5() const { return ___configId_5; }
	inline String_t** get_address_of_configId_5() { return &___configId_5; }
	inline void set_configId_5(String_t* value)
	{
		___configId_5 = value;
		Il2CppCodeGenWriteBarrier(&___configId_5, value);
	}

	inline static int32_t get_offset_of_appId_6() { return static_cast<int32_t>(offsetof(PluginNewMiquwan_t2470753303, ___appId_6)); }
	inline String_t* get_appId_6() const { return ___appId_6; }
	inline String_t** get_address_of_appId_6() { return &___appId_6; }
	inline void set_appId_6(String_t* value)
	{
		___appId_6 = value;
		Il2CppCodeGenWriteBarrier(&___appId_6, value);
	}
};

struct PluginNewMiquwan_t2470753303_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PluginNewMiquwan::<>f__switch$map24
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map24_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map24_7() { return static_cast<int32_t>(offsetof(PluginNewMiquwan_t2470753303_StaticFields, ___U3CU3Ef__switchU24map24_7)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map24_7() const { return ___U3CU3Ef__switchU24map24_7; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map24_7() { return &___U3CU3Ef__switchU24map24_7; }
	inline void set_U3CU3Ef__switchU24map24_7(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map24_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map24_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
