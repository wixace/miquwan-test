﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ColliderGenerated
struct UnityEngine_ColliderGenerated_t3162883987;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_ColliderGenerated::.ctor()
extern "C"  void UnityEngine_ColliderGenerated__ctor_m3157368632 (UnityEngine_ColliderGenerated_t3162883987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColliderGenerated::Collider_Collider1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColliderGenerated_Collider_Collider1_m1974638010 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColliderGenerated::Collider_enabled(JSVCall)
extern "C"  void UnityEngine_ColliderGenerated_Collider_enabled_m1615044709 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColliderGenerated::Collider_attachedRigidbody(JSVCall)
extern "C"  void UnityEngine_ColliderGenerated_Collider_attachedRigidbody_m1597522845 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColliderGenerated::Collider_isTrigger(JSVCall)
extern "C"  void UnityEngine_ColliderGenerated_Collider_isTrigger_m2492082456 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColliderGenerated::Collider_contactOffset(JSVCall)
extern "C"  void UnityEngine_ColliderGenerated_Collider_contactOffset_m3654365971 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColliderGenerated::Collider_material(JSVCall)
extern "C"  void UnityEngine_ColliderGenerated_Collider_material_m213016383 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColliderGenerated::Collider_sharedMaterial(JSVCall)
extern "C"  void UnityEngine_ColliderGenerated_Collider_sharedMaterial_m2807392602 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColliderGenerated::Collider_bounds(JSVCall)
extern "C"  void UnityEngine_ColliderGenerated_Collider_bounds_m2965007729 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColliderGenerated::Collider_ClosestPointOnBounds__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColliderGenerated_Collider_ClosestPointOnBounds__Vector3_m1721515362 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColliderGenerated::Collider_Raycast__Ray__RaycastHit__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColliderGenerated_Collider_Raycast__Ray__RaycastHit__Single_m2346721592 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColliderGenerated::__Register()
extern "C"  void UnityEngine_ColliderGenerated___Register_m1378525327 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColliderGenerated::ilo_getBooleanS1(System.Int32)
extern "C"  bool UnityEngine_ColliderGenerated_ilo_getBooleanS1_m1707708844 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColliderGenerated::ilo_setBooleanS2(System.Int32,System.Boolean)
extern "C"  void UnityEngine_ColliderGenerated_ilo_setBooleanS2_m4063527752 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_ColliderGenerated::ilo_getVector3S3(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_ColliderGenerated_ilo_getVector3S3_m3880779856 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_ColliderGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_ColliderGenerated_ilo_getObject4_m2397319280 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ColliderGenerated::ilo_setObject5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_ColliderGenerated_ilo_setObject5_m665285370 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
