﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pomelo.DotNetClient.HandShakeService
struct HandShakeService_t4068105274;
// Pomelo.DotNetClient.Protocol
struct Protocol_t400511732;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.Action`1<Newtonsoft.Json.Linq.JObject>
struct Action_1_t2194360335;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_Protocol400511732.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_PackageType3964380262.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"

// System.Void Pomelo.DotNetClient.HandShakeService::.ctor(Pomelo.DotNetClient.Protocol)
extern "C"  void HandShakeService__ctor_m445104239 (HandShakeService_t4068105274 * __this, Protocol_t400511732 * ___protocol0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.HandShakeService::request(Newtonsoft.Json.Linq.JObject,System.Action`1<Newtonsoft.Json.Linq.JObject>)
extern "C"  void HandShakeService_request_m2011686931 (HandShakeService_t4068105274 * __this, JObject_t1798544199 * ___user0, Action_1_t2194360335 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.HandShakeService::invokeCallback(Newtonsoft.Json.Linq.JObject)
extern "C"  void HandShakeService_invokeCallback_m2494075503 (HandShakeService_t4068105274 * __this, JObject_t1798544199 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.HandShakeService::ack()
extern "C"  void HandShakeService_ack_m204557513 (HandShakeService_t4068105274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject Pomelo.DotNetClient.HandShakeService::buildMsg(Newtonsoft.Json.Linq.JObject)
extern "C"  JObject_t1798544199 * HandShakeService_buildMsg_m366498418 (HandShakeService_t4068105274 * __this, JObject_t1798544199 * ___user0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.HandShakeService::ilo_send1(Pomelo.DotNetClient.Protocol,Pomelo.DotNetClient.PackageType,System.Byte[])
extern "C"  void HandShakeService_ilo_send1_m4131756269 (Il2CppObject * __this /* static, unused */, Protocol_t400511732 * ____this0, int32_t ___type1, ByteU5BU5D_t4260760469* ___body2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.HandShakeService::ilo_set_Item2(Newtonsoft.Json.Linq.JObject,System.String,Newtonsoft.Json.Linq.JToken)
extern "C"  void HandShakeService_ilo_set_Item2_m784417919 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, JToken_t3412245951 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
