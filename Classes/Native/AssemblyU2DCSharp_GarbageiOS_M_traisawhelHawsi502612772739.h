﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_traisawhelHawsi50
struct  M_traisawhelHawsi50_t2612772739  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_traisawhelHawsi50::_cemsaitre
	uint32_t ____cemsaitre_0;
	// System.Single GarbageiOS.M_traisawhelHawsi50::_touseLurmemsai
	float ____touseLurmemsai_1;
	// System.String GarbageiOS.M_traisawhelHawsi50::_timupas
	String_t* ____timupas_2;
	// System.Int32 GarbageiOS.M_traisawhelHawsi50::_soomurcar
	int32_t ____soomurcar_3;
	// System.Int32 GarbageiOS.M_traisawhelHawsi50::_gameresis
	int32_t ____gameresis_4;
	// System.Int32 GarbageiOS.M_traisawhelHawsi50::_lisnipo
	int32_t ____lisnipo_5;
	// System.Int32 GarbageiOS.M_traisawhelHawsi50::_dresee
	int32_t ____dresee_6;
	// System.Int32 GarbageiOS.M_traisawhelHawsi50::_temrawjelSana
	int32_t ____temrawjelSana_7;
	// System.String GarbageiOS.M_traisawhelHawsi50::_xichaysi
	String_t* ____xichaysi_8;
	// System.Int32 GarbageiOS.M_traisawhelHawsi50::_tawbeWhurtri
	int32_t ____tawbeWhurtri_9;
	// System.Int32 GarbageiOS.M_traisawhelHawsi50::_xirpeehou
	int32_t ____xirpeehou_10;
	// System.UInt32 GarbageiOS.M_traisawhelHawsi50::_kasfal
	uint32_t ____kasfal_11;
	// System.UInt32 GarbageiOS.M_traisawhelHawsi50::_licirHamou
	uint32_t ____licirHamou_12;
	// System.Single GarbageiOS.M_traisawhelHawsi50::_sorchaDereche
	float ____sorchaDereche_13;
	// System.UInt32 GarbageiOS.M_traisawhelHawsi50::_talllere
	uint32_t ____talllere_14;
	// System.String GarbageiOS.M_traisawhelHawsi50::_durpaWhatrurtow
	String_t* ____durpaWhatrurtow_15;
	// System.Single GarbageiOS.M_traisawhelHawsi50::_dirse
	float ____dirse_16;
	// System.Single GarbageiOS.M_traisawhelHawsi50::_neepalyereHemnorsti
	float ____neepalyereHemnorsti_17;
	// System.Boolean GarbageiOS.M_traisawhelHawsi50::_sirfaxooRecar
	bool ____sirfaxooRecar_18;
	// System.UInt32 GarbageiOS.M_traisawhelHawsi50::_zurhayRelpoyer
	uint32_t ____zurhayRelpoyer_19;
	// System.Int32 GarbageiOS.M_traisawhelHawsi50::_couzorStalgekal
	int32_t ____couzorStalgekal_20;
	// System.Single GarbageiOS.M_traisawhelHawsi50::_reqou
	float ____reqou_21;
	// System.UInt32 GarbageiOS.M_traisawhelHawsi50::_tesouDayyailo
	uint32_t ____tesouDayyailo_22;
	// System.String GarbageiOS.M_traisawhelHawsi50::_jemasFepay
	String_t* ____jemasFepay_23;

public:
	inline static int32_t get_offset_of__cemsaitre_0() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____cemsaitre_0)); }
	inline uint32_t get__cemsaitre_0() const { return ____cemsaitre_0; }
	inline uint32_t* get_address_of__cemsaitre_0() { return &____cemsaitre_0; }
	inline void set__cemsaitre_0(uint32_t value)
	{
		____cemsaitre_0 = value;
	}

	inline static int32_t get_offset_of__touseLurmemsai_1() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____touseLurmemsai_1)); }
	inline float get__touseLurmemsai_1() const { return ____touseLurmemsai_1; }
	inline float* get_address_of__touseLurmemsai_1() { return &____touseLurmemsai_1; }
	inline void set__touseLurmemsai_1(float value)
	{
		____touseLurmemsai_1 = value;
	}

	inline static int32_t get_offset_of__timupas_2() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____timupas_2)); }
	inline String_t* get__timupas_2() const { return ____timupas_2; }
	inline String_t** get_address_of__timupas_2() { return &____timupas_2; }
	inline void set__timupas_2(String_t* value)
	{
		____timupas_2 = value;
		Il2CppCodeGenWriteBarrier(&____timupas_2, value);
	}

	inline static int32_t get_offset_of__soomurcar_3() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____soomurcar_3)); }
	inline int32_t get__soomurcar_3() const { return ____soomurcar_3; }
	inline int32_t* get_address_of__soomurcar_3() { return &____soomurcar_3; }
	inline void set__soomurcar_3(int32_t value)
	{
		____soomurcar_3 = value;
	}

	inline static int32_t get_offset_of__gameresis_4() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____gameresis_4)); }
	inline int32_t get__gameresis_4() const { return ____gameresis_4; }
	inline int32_t* get_address_of__gameresis_4() { return &____gameresis_4; }
	inline void set__gameresis_4(int32_t value)
	{
		____gameresis_4 = value;
	}

	inline static int32_t get_offset_of__lisnipo_5() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____lisnipo_5)); }
	inline int32_t get__lisnipo_5() const { return ____lisnipo_5; }
	inline int32_t* get_address_of__lisnipo_5() { return &____lisnipo_5; }
	inline void set__lisnipo_5(int32_t value)
	{
		____lisnipo_5 = value;
	}

	inline static int32_t get_offset_of__dresee_6() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____dresee_6)); }
	inline int32_t get__dresee_6() const { return ____dresee_6; }
	inline int32_t* get_address_of__dresee_6() { return &____dresee_6; }
	inline void set__dresee_6(int32_t value)
	{
		____dresee_6 = value;
	}

	inline static int32_t get_offset_of__temrawjelSana_7() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____temrawjelSana_7)); }
	inline int32_t get__temrawjelSana_7() const { return ____temrawjelSana_7; }
	inline int32_t* get_address_of__temrawjelSana_7() { return &____temrawjelSana_7; }
	inline void set__temrawjelSana_7(int32_t value)
	{
		____temrawjelSana_7 = value;
	}

	inline static int32_t get_offset_of__xichaysi_8() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____xichaysi_8)); }
	inline String_t* get__xichaysi_8() const { return ____xichaysi_8; }
	inline String_t** get_address_of__xichaysi_8() { return &____xichaysi_8; }
	inline void set__xichaysi_8(String_t* value)
	{
		____xichaysi_8 = value;
		Il2CppCodeGenWriteBarrier(&____xichaysi_8, value);
	}

	inline static int32_t get_offset_of__tawbeWhurtri_9() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____tawbeWhurtri_9)); }
	inline int32_t get__tawbeWhurtri_9() const { return ____tawbeWhurtri_9; }
	inline int32_t* get_address_of__tawbeWhurtri_9() { return &____tawbeWhurtri_9; }
	inline void set__tawbeWhurtri_9(int32_t value)
	{
		____tawbeWhurtri_9 = value;
	}

	inline static int32_t get_offset_of__xirpeehou_10() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____xirpeehou_10)); }
	inline int32_t get__xirpeehou_10() const { return ____xirpeehou_10; }
	inline int32_t* get_address_of__xirpeehou_10() { return &____xirpeehou_10; }
	inline void set__xirpeehou_10(int32_t value)
	{
		____xirpeehou_10 = value;
	}

	inline static int32_t get_offset_of__kasfal_11() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____kasfal_11)); }
	inline uint32_t get__kasfal_11() const { return ____kasfal_11; }
	inline uint32_t* get_address_of__kasfal_11() { return &____kasfal_11; }
	inline void set__kasfal_11(uint32_t value)
	{
		____kasfal_11 = value;
	}

	inline static int32_t get_offset_of__licirHamou_12() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____licirHamou_12)); }
	inline uint32_t get__licirHamou_12() const { return ____licirHamou_12; }
	inline uint32_t* get_address_of__licirHamou_12() { return &____licirHamou_12; }
	inline void set__licirHamou_12(uint32_t value)
	{
		____licirHamou_12 = value;
	}

	inline static int32_t get_offset_of__sorchaDereche_13() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____sorchaDereche_13)); }
	inline float get__sorchaDereche_13() const { return ____sorchaDereche_13; }
	inline float* get_address_of__sorchaDereche_13() { return &____sorchaDereche_13; }
	inline void set__sorchaDereche_13(float value)
	{
		____sorchaDereche_13 = value;
	}

	inline static int32_t get_offset_of__talllere_14() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____talllere_14)); }
	inline uint32_t get__talllere_14() const { return ____talllere_14; }
	inline uint32_t* get_address_of__talllere_14() { return &____talllere_14; }
	inline void set__talllere_14(uint32_t value)
	{
		____talllere_14 = value;
	}

	inline static int32_t get_offset_of__durpaWhatrurtow_15() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____durpaWhatrurtow_15)); }
	inline String_t* get__durpaWhatrurtow_15() const { return ____durpaWhatrurtow_15; }
	inline String_t** get_address_of__durpaWhatrurtow_15() { return &____durpaWhatrurtow_15; }
	inline void set__durpaWhatrurtow_15(String_t* value)
	{
		____durpaWhatrurtow_15 = value;
		Il2CppCodeGenWriteBarrier(&____durpaWhatrurtow_15, value);
	}

	inline static int32_t get_offset_of__dirse_16() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____dirse_16)); }
	inline float get__dirse_16() const { return ____dirse_16; }
	inline float* get_address_of__dirse_16() { return &____dirse_16; }
	inline void set__dirse_16(float value)
	{
		____dirse_16 = value;
	}

	inline static int32_t get_offset_of__neepalyereHemnorsti_17() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____neepalyereHemnorsti_17)); }
	inline float get__neepalyereHemnorsti_17() const { return ____neepalyereHemnorsti_17; }
	inline float* get_address_of__neepalyereHemnorsti_17() { return &____neepalyereHemnorsti_17; }
	inline void set__neepalyereHemnorsti_17(float value)
	{
		____neepalyereHemnorsti_17 = value;
	}

	inline static int32_t get_offset_of__sirfaxooRecar_18() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____sirfaxooRecar_18)); }
	inline bool get__sirfaxooRecar_18() const { return ____sirfaxooRecar_18; }
	inline bool* get_address_of__sirfaxooRecar_18() { return &____sirfaxooRecar_18; }
	inline void set__sirfaxooRecar_18(bool value)
	{
		____sirfaxooRecar_18 = value;
	}

	inline static int32_t get_offset_of__zurhayRelpoyer_19() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____zurhayRelpoyer_19)); }
	inline uint32_t get__zurhayRelpoyer_19() const { return ____zurhayRelpoyer_19; }
	inline uint32_t* get_address_of__zurhayRelpoyer_19() { return &____zurhayRelpoyer_19; }
	inline void set__zurhayRelpoyer_19(uint32_t value)
	{
		____zurhayRelpoyer_19 = value;
	}

	inline static int32_t get_offset_of__couzorStalgekal_20() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____couzorStalgekal_20)); }
	inline int32_t get__couzorStalgekal_20() const { return ____couzorStalgekal_20; }
	inline int32_t* get_address_of__couzorStalgekal_20() { return &____couzorStalgekal_20; }
	inline void set__couzorStalgekal_20(int32_t value)
	{
		____couzorStalgekal_20 = value;
	}

	inline static int32_t get_offset_of__reqou_21() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____reqou_21)); }
	inline float get__reqou_21() const { return ____reqou_21; }
	inline float* get_address_of__reqou_21() { return &____reqou_21; }
	inline void set__reqou_21(float value)
	{
		____reqou_21 = value;
	}

	inline static int32_t get_offset_of__tesouDayyailo_22() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____tesouDayyailo_22)); }
	inline uint32_t get__tesouDayyailo_22() const { return ____tesouDayyailo_22; }
	inline uint32_t* get_address_of__tesouDayyailo_22() { return &____tesouDayyailo_22; }
	inline void set__tesouDayyailo_22(uint32_t value)
	{
		____tesouDayyailo_22 = value;
	}

	inline static int32_t get_offset_of__jemasFepay_23() { return static_cast<int32_t>(offsetof(M_traisawhelHawsi50_t2612772739, ____jemasFepay_23)); }
	inline String_t* get__jemasFepay_23() const { return ____jemasFepay_23; }
	inline String_t** get_address_of__jemasFepay_23() { return &____jemasFepay_23; }
	inline void set__jemasFepay_23(String_t* value)
	{
		____jemasFepay_23 = value;
		Il2CppCodeGenWriteBarrier(&____jemasFepay_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
