﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimeUpdateMgrGenerated
struct TimeUpdateMgrGenerated_t3330447245;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// TimeUpdateMgr
struct TimeUpdateMgr_t880289826;
// TimeUpdateVo
struct TimeUpdateVo_t1829512047;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_TimeUpdateMgr880289826.h"
#include "AssemblyU2DCSharp_TimeUpdateVo1829512047.h"

// System.Void TimeUpdateMgrGenerated::.ctor()
extern "C"  void TimeUpdateMgrGenerated__ctor_m3907439534 (TimeUpdateMgrGenerated_t3330447245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimeUpdateMgrGenerated::TimeUpdateMgr_TimeUpdateMgr1(JSVCall,System.Int32)
extern "C"  bool TimeUpdateMgrGenerated_TimeUpdateMgr_TimeUpdateMgr1_m226141958 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimeUpdateMgrGenerated::TimeUpdateMgr_AddTimeVo__TimeUpdateVo(JSVCall,System.Int32)
extern "C"  bool TimeUpdateMgrGenerated_TimeUpdateMgr_AddTimeVo__TimeUpdateVo_m438226369 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimeUpdateMgrGenerated::TimeUpdateMgr_Clear(JSVCall,System.Int32)
extern "C"  bool TimeUpdateMgrGenerated_TimeUpdateMgr_Clear_m1230532056 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimeUpdateMgrGenerated::TimeUpdateMgr_Init(JSVCall,System.Int32)
extern "C"  bool TimeUpdateMgrGenerated_TimeUpdateMgr_Init_m2571476135 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimeUpdateMgrGenerated::TimeUpdateMgr_RemoveVo__TimeUpdateVo(JSVCall,System.Int32)
extern "C"  bool TimeUpdateMgrGenerated_TimeUpdateMgr_RemoveVo__TimeUpdateVo_m3494985731 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimeUpdateMgrGenerated::TimeUpdateMgr_ZUpdate(JSVCall,System.Int32)
extern "C"  bool TimeUpdateMgrGenerated_TimeUpdateMgr_ZUpdate_m2633301614 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeUpdateMgrGenerated::__Register()
extern "C"  void TimeUpdateMgrGenerated___Register_m2905394649 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TimeUpdateMgrGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t TimeUpdateMgrGenerated_ilo_getObject1_m619981220 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimeUpdateMgrGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool TimeUpdateMgrGenerated_ilo_attachFinalizerObject2_m840002866 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeUpdateMgrGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void TimeUpdateMgrGenerated_ilo_addJSCSRel3_m4275989612 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeUpdateMgrGenerated::ilo_AddTimeVo4(TimeUpdateMgr,TimeUpdateVo)
extern "C"  void TimeUpdateMgrGenerated_ilo_AddTimeVo4_m3241797341 (Il2CppObject * __this /* static, unused */, TimeUpdateMgr_t880289826 * ____this0, TimeUpdateVo_t1829512047 * ___vo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeUpdateMgrGenerated::ilo_Clear5(TimeUpdateMgr)
extern "C"  void TimeUpdateMgrGenerated_ilo_Clear5_m39800991 (Il2CppObject * __this /* static, unused */, TimeUpdateMgr_t880289826 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
