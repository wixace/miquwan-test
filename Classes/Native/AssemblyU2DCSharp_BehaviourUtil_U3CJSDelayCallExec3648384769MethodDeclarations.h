﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<JSDelayCallExec>c__Iterator3D
struct U3CJSDelayCallExecU3Ec__Iterator3D_t3648384769;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<JSDelayCallExec>c__Iterator3D::.ctor()
extern "C"  void U3CJSDelayCallExecU3Ec__Iterator3D__ctor_m1927935162 (U3CJSDelayCallExecU3Ec__Iterator3D_t3648384769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BehaviourUtil/<JSDelayCallExec>c__Iterator3D::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CJSDelayCallExecU3Ec__Iterator3D_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1023040994 (U3CJSDelayCallExecU3Ec__Iterator3D_t3648384769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BehaviourUtil/<JSDelayCallExec>c__Iterator3D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CJSDelayCallExecU3Ec__Iterator3D_System_Collections_IEnumerator_get_Current_m2467849590 (U3CJSDelayCallExecU3Ec__Iterator3D_t3648384769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtil/<JSDelayCallExec>c__Iterator3D::MoveNext()
extern "C"  bool U3CJSDelayCallExecU3Ec__Iterator3D_MoveNext_m2244808930 (U3CJSDelayCallExecU3Ec__Iterator3D_t3648384769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/<JSDelayCallExec>c__Iterator3D::Dispose()
extern "C"  void U3CJSDelayCallExecU3Ec__Iterator3D_Dispose_m1513069175 (U3CJSDelayCallExecU3Ec__Iterator3D_t3648384769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/<JSDelayCallExec>c__Iterator3D::Reset()
extern "C"  void U3CJSDelayCallExecU3Ec__Iterator3D_Reset_m3869335399 (U3CJSDelayCallExecU3Ec__Iterator3D_t3648384769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
