﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// System.IO.Stream
struct Stream_t1561764144;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// ProtoBuf.SerializationContext
struct SerializationContext_t3997850667;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// ProtoBuf.NetObjectCache
struct NetObjectCache_t514806898;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;
// System.Exception
struct Exception_t3991598821;
// ProtoBuf.IExtensible
struct IExtensible_t1056931882;
// ProtoBuf.Meta.BasicList
struct BasicList_t528018366;
// ProtoBuf.Meta.MutableList
struct MutableList_t4210537526;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "AssemblyU2DCSharp_ProtoBuf_SerializationContext3997850667.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "AssemblyU2DCSharp_ProtoBuf_PrefixStyle1974492709.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_ProtoBuf_WireType2355646059.h"
#include "AssemblyU2DCSharp_ProtoBuf_SubItemToken3365128146.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ProtoBuf_DataFormat274207885.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_BasicList528018366.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_MutableList4210537526.h"
#include "AssemblyU2DCSharp_ProtoBuf_NetObjectCache514806898.h"

// System.Void ProtoBuf.ProtoWriter::.ctor(System.IO.Stream,ProtoBuf.Meta.TypeModel,ProtoBuf.SerializationContext)
extern "C"  void ProtoWriter__ctor_m1119262727 (ProtoWriter_t4117914721 * __this, Stream_t1561764144 * ___dest0, TypeModel_t2730011105 * ___model1, SerializationContext_t3997850667 * ___context2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::.cctor()
extern "C"  void ProtoWriter__cctor_m2202679530 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::System.IDisposable.Dispose()
extern "C"  void ProtoWriter_System_IDisposable_Dispose_m759839836 (ProtoWriter_t4117914721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteObject(System.Object,System.Int32,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_WriteObject_m4094881292 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, int32_t ___key1, ProtoWriter_t4117914721 * ___writer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteRecursionSafeObject(System.Object,System.Int32,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_WriteRecursionSafeObject_m1189616859 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, int32_t ___key1, ProtoWriter_t4117914721 * ___writer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteObject(System.Object,System.Int32,ProtoBuf.ProtoWriter,ProtoBuf.PrefixStyle,System.Int32)
extern "C"  void ProtoWriter_WriteObject_m2380995801 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, int32_t ___key1, ProtoWriter_t4117914721 * ___writer2, int32_t ___style3, int32_t ___fieldNumber4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoWriter::GetTypeKey(System.Type&)
extern "C"  int32_t ProtoWriter_GetTypeKey_m3037982609 (ProtoWriter_t4117914721 * __this, Type_t ** ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.NetObjectCache ProtoBuf.ProtoWriter::get_NetCache()
extern "C"  NetObjectCache_t514806898 * ProtoWriter_get_NetCache_m3596757401 (ProtoWriter_t4117914721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.WireType ProtoBuf.ProtoWriter::get_WireType()
extern "C"  int32_t ProtoWriter_get_WireType_m3862178426 (ProtoWriter_t4117914721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteFieldHeader(System.Int32,ProtoBuf.WireType,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_WriteFieldHeader_m2247348746 (Il2CppObject * __this /* static, unused */, int32_t ___fieldNumber0, int32_t ___wireType1, ProtoWriter_t4117914721 * ___writer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteHeaderCore(System.Int32,ProtoBuf.WireType,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_WriteHeaderCore_m4105087271 (Il2CppObject * __this /* static, unused */, int32_t ___fieldNumber0, int32_t ___wireType1, ProtoWriter_t4117914721 * ___writer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteBytes(System.Byte[],ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_WriteBytes_m1920718770 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___data0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteBytes(System.Byte[],System.Int32,System.Int32,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_WriteBytes_m420460882 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___data0, int32_t ___offset1, int32_t ___length2, ProtoWriter_t4117914721 * ___writer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::CopyRawFromStream(System.IO.Stream,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_CopyRawFromStream_m899510137 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::IncrementedAndReset(System.Int32,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_IncrementedAndReset_m2480626134 (Il2CppObject * __this /* static, unused */, int32_t ___length0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.SubItemToken ProtoBuf.ProtoWriter::StartSubItem(System.Object,ProtoBuf.ProtoWriter)
extern "C"  SubItemToken_t3365128146  ProtoWriter_StartSubItem_m1795402934 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___instance0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::CheckRecursionStackAndPush(System.Object)
extern "C"  void ProtoWriter_CheckRecursionStackAndPush_m2425890000 (ProtoWriter_t4117914721 * __this, Il2CppObject * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::PopRecursionStack()
extern "C"  void ProtoWriter_PopRecursionStack_m497185656 (ProtoWriter_t4117914721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.SubItemToken ProtoBuf.ProtoWriter::StartSubItem(System.Object,ProtoBuf.ProtoWriter,System.Boolean)
extern "C"  SubItemToken_t3365128146  ProtoWriter_StartSubItem_m950199303 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___instance0, ProtoWriter_t4117914721 * ___writer1, bool ___allowFixed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::EndSubItem(ProtoBuf.SubItemToken,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_EndSubItem_m2549937374 (Il2CppObject * __this /* static, unused */, SubItemToken_t3365128146  ___token0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::EndSubItem(ProtoBuf.SubItemToken,ProtoBuf.ProtoWriter,ProtoBuf.PrefixStyle)
extern "C"  void ProtoWriter_EndSubItem_m3042374956 (Il2CppObject * __this /* static, unused */, SubItemToken_t3365128146  ___token0, ProtoWriter_t4117914721 * ___writer1, int32_t ___style2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.SerializationContext ProtoBuf.ProtoWriter::get_Context()
extern "C"  SerializationContext_t3997850667 * ProtoWriter_get_Context_m130506262 (ProtoWriter_t4117914721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::Dispose()
extern "C"  void ProtoWriter_Dispose_m1521952416 (ProtoWriter_t4117914721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoWriter::GetPosition(ProtoBuf.ProtoWriter)
extern "C"  int32_t ProtoWriter_GetPosition_m263012442 (Il2CppObject * __this /* static, unused */, ProtoWriter_t4117914721 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::DemandSpace(System.Int32,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_DemandSpace_m1798886497 (Il2CppObject * __this /* static, unused */, int32_t ___required0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::Close()
extern "C"  void ProtoWriter_Close_m3875675193 (ProtoWriter_t4117914721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::CheckDepthFlushlock()
extern "C"  void ProtoWriter_CheckDepthFlushlock_m2678974741 (ProtoWriter_t4117914721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.TypeModel ProtoBuf.ProtoWriter::get_Model()
extern "C"  TypeModel_t2730011105 * ProtoWriter_get_Model_m844322423 (ProtoWriter_t4117914721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::Flush(ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_Flush_m2029937869 (Il2CppObject * __this /* static, unused */, ProtoWriter_t4117914721 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteUInt32Variant(System.UInt32,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_WriteUInt32Variant_m253557858 (Il2CppObject * __this /* static, unused */, uint32_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ProtoBuf.ProtoWriter::Zig(System.Int32)
extern "C"  uint32_t ProtoWriter_Zig_m3009278127 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 ProtoBuf.ProtoWriter::Zig(System.Int64)
extern "C"  uint64_t ProtoWriter_Zig_m3805147535 (Il2CppObject * __this /* static, unused */, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteUInt64Variant(System.UInt64,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_WriteUInt64Variant_m3980391874 (Il2CppObject * __this /* static, unused */, uint64_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteString(System.String,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_WriteString_m332496797 (Il2CppObject * __this /* static, unused */, String_t* ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteUInt64(System.UInt64,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_WriteUInt64_m2503974827 (Il2CppObject * __this /* static, unused */, uint64_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteInt64(System.Int64,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_WriteInt64_m2335323213 (Il2CppObject * __this /* static, unused */, int64_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteUInt32(System.UInt32,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_WriteUInt32_m3247457197 (Il2CppObject * __this /* static, unused */, uint32_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteInt16(System.Int16,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_WriteInt16_m3116690349 (Il2CppObject * __this /* static, unused */, int16_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteUInt16(System.UInt16,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_WriteUInt16_m2119016377 (Il2CppObject * __this /* static, unused */, uint16_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteByte(System.Byte,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_WriteByte_m1207847435 (Il2CppObject * __this /* static, unused */, uint8_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteSByte(System.SByte,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_WriteSByte_m204472077 (Il2CppObject * __this /* static, unused */, int8_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteInt32ToBuffer(System.Int32,System.Byte[],System.Int32)
extern "C"  void ProtoWriter_WriteInt32ToBuffer_m3331409272 (Il2CppObject * __this /* static, unused */, int32_t ___value0, ByteU5BU5D_t4260760469* ___buffer1, int32_t ___index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteInt32(System.Int32,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_WriteInt32_m3746851309 (Il2CppObject * __this /* static, unused */, int32_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteDouble(System.Double,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_WriteDouble_m589268765 (Il2CppObject * __this /* static, unused */, double ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteSingle(System.Single,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_WriteSingle_m500770123 (Il2CppObject * __this /* static, unused */, float ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::ThrowEnumException(ProtoBuf.ProtoWriter,System.Object)
extern "C"  void ProtoWriter_ThrowEnumException_m3748818015 (Il2CppObject * __this /* static, unused */, ProtoWriter_t4117914721 * ___writer0, Il2CppObject * ___enumValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception ProtoBuf.ProtoWriter::CreateException(ProtoBuf.ProtoWriter)
extern "C"  Exception_t3991598821 * ProtoWriter_CreateException_m2156570799 (Il2CppObject * __this /* static, unused */, ProtoWriter_t4117914721 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteBoolean(System.Boolean,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_WriteBoolean_m3516727405 (Il2CppObject * __this /* static, unused */, bool ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::AppendExtensionData(ProtoBuf.IExtensible,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_AppendExtensionData_m897888127 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___instance0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::SetPackedField(System.Int32,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_SetPackedField_m1586586780 (Il2CppObject * __this /* static, unused */, int32_t ___fieldNumber0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.ProtoWriter::SerializeType(System.Type)
extern "C"  String_t* ProtoWriter_SerializeType_m2523438273 (ProtoWriter_t4117914721 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::SetRootObject(System.Object)
extern "C"  void ProtoWriter_SetRootObject_m2579690448 (ProtoWriter_t4117914721 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::WriteType(System.Type,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_WriteType_m2148665455 (Il2CppObject * __this /* static, unused */, Type_t * ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::ilo_Dispose1(ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_ilo_Dispose1_m2640346542 (Il2CppObject * __this /* static, unused */, ProtoWriter_t4117914721 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::ilo_ThrowUnexpectedType2(System.Type)
extern "C"  void ProtoWriter_ilo_ThrowUnexpectedType2_m2807212612 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception ProtoBuf.ProtoWriter::ilo_CreateException3(ProtoBuf.ProtoWriter)
extern "C"  Exception_t3991598821 * ProtoWriter_ilo_CreateException3_m3514134665 (Il2CppObject * __this /* static, unused */, ProtoWriter_t4117914721 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.SubItemToken ProtoBuf.ProtoWriter::ilo_StartSubItem4(System.Object,ProtoBuf.ProtoWriter,System.Boolean)
extern "C"  SubItemToken_t3365128146  ProtoWriter_ilo_StartSubItem4_m3377959132 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___instance0, ProtoWriter_t4117914721 * ___writer1, bool ___allowFixed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoWriter::ilo_TrySerializeAuxiliaryType5(ProtoBuf.Meta.TypeModel,ProtoBuf.ProtoWriter,System.Type,ProtoBuf.DataFormat,System.Int32,System.Object,System.Boolean)
extern "C"  bool ProtoWriter_ilo_TrySerializeAuxiliaryType5_m1981818746 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, ProtoWriter_t4117914721 * ___writer1, Type_t * ___type2, int32_t ___format3, int32_t ___tag4, Il2CppObject * ___value5, bool ___isInsideList6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::ilo_EndSubItem6(ProtoBuf.SubItemToken,ProtoBuf.ProtoWriter,ProtoBuf.PrefixStyle)
extern "C"  void ProtoWriter_ilo_EndSubItem6_m97074713 (Il2CppObject * __this /* static, unused */, SubItemToken_t3365128146  ___token0, ProtoWriter_t4117914721 * ___writer1, int32_t ___style2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::ilo_WriteHeaderCore7(System.Int32,ProtoBuf.WireType,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_ilo_WriteHeaderCore7_m581443699 (Il2CppObject * __this /* static, unused */, int32_t ___fieldNumber0, int32_t ___wireType1, ProtoWriter_t4117914721 * ___writer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::ilo_WriteUInt32Variant8(System.UInt32,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_ilo_WriteUInt32Variant8_m3786524429 (Il2CppObject * __this /* static, unused */, uint32_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::ilo_WriteBytes9(System.Byte[],System.Int32,System.Int32,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_ilo_WriteBytes9_m473559392 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___data0, int32_t ___offset1, int32_t ___length2, ProtoWriter_t4117914721 * ___writer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::ilo_Flush10(ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_ilo_Flush10_m914201433 (Il2CppObject * __this /* static, unused */, ProtoWriter_t4117914721 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::ilo_DemandSpace11(System.Int32,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_ilo_DemandSpace11_m2436698420 (Il2CppObject * __this /* static, unused */, int32_t ___required0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::ilo_BlockCopy12(System.Byte[],System.Int32,System.Byte[],System.Int32,System.Int32)
extern "C"  void ProtoWriter_ilo_BlockCopy12_m1190181250 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___from0, int32_t ___fromIndex1, ByteU5BU5D_t4260760469* ___to2, int32_t ___toIndex3, int32_t ___count4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoWriter::ilo_IndexOfReference13(ProtoBuf.Meta.BasicList,System.Object)
extern "C"  int32_t ProtoWriter_ilo_IndexOfReference13_m329132308 (Il2CppObject * __this /* static, unused */, BasicList_t528018366 * ____this0, Il2CppObject * ___instance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::ilo_RemoveLast14(ProtoBuf.Meta.MutableList)
extern "C"  void ProtoWriter_ilo_RemoveLast14_m1399262787 (Il2CppObject * __this /* static, unused */, MutableList_t4210537526 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::ilo_IncrementedAndReset15(System.Int32,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_ilo_IncrementedAndReset15_m4168551397 (Il2CppObject * __this /* static, unused */, int32_t ___length0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::ilo_WriteInt6416(System.Int64,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_ilo_WriteInt6416_m222576885 (Il2CppObject * __this /* static, unused */, int64_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::ilo_WriteUInt64Variant17(System.UInt64,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_ilo_WriteUInt64Variant17_m99675803 (Il2CppObject * __this /* static, unused */, uint64_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::ilo_WriteInt3218(System.Int32,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_ilo_WriteInt3218_m1337111443 (Il2CppObject * __this /* static, unused */, int32_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::ilo_WriteUInt3219(System.UInt32,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_ilo_WriteUInt3219_m3077290498 (Il2CppObject * __this /* static, unused */, uint32_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::ilo_WriteInt32ToBuffer20(System.Int32,System.Byte[],System.Int32)
extern "C"  void ProtoWriter_ilo_WriteInt32ToBuffer20_m906817063 (Il2CppObject * __this /* static, unused */, int32_t ___value0, ByteU5BU5D_t4260760469* ___buffer1, int32_t ___index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ProtoBuf.ProtoWriter::ilo_Zig21(System.Int32)
extern "C"  uint32_t ProtoWriter_ilo_Zig21_m2575324635 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoWriter::ilo_IsInfinity22(System.Single)
extern "C"  bool ProtoWriter_ilo_IsInfinity22_m1856082897 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoWriter::ilo_IsInfinity23(System.Double)
extern "C"  bool ProtoWriter_ilo_IsInfinity23_m1095985991 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::ilo_WriteSingle24(System.Single,ProtoBuf.ProtoWriter)
extern "C"  void ProtoWriter_ilo_WriteSingle24_m2613864186 (Il2CppObject * __this /* static, unused */, float ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream ProtoBuf.ProtoWriter::ilo_BeginQuery25(ProtoBuf.IExtension)
extern "C"  Stream_t1561764144 * ProtoWriter_ilo_BeginQuery25_m2067950319 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.ProtoWriter::ilo_SerializeType26(ProtoBuf.Meta.TypeModel,System.Type)
extern "C"  String_t* ProtoWriter_ilo_SerializeType26_m1317741971 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.NetObjectCache ProtoBuf.ProtoWriter::ilo_get_NetCache27(ProtoBuf.ProtoWriter)
extern "C"  NetObjectCache_t514806898 * ProtoWriter_ilo_get_NetCache27_m2854998105 (Il2CppObject * __this /* static, unused */, ProtoWriter_t4117914721 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoWriter::ilo_SetKeyedObject28(ProtoBuf.NetObjectCache,System.Int32,System.Object)
extern "C"  void ProtoWriter_ilo_SetKeyedObject28_m929609809 (Il2CppObject * __this /* static, unused */, NetObjectCache_t514806898 * ____this0, int32_t ___key1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
