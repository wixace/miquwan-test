﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ReplayBase779703160.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayStopMove
struct  ReplayStopMove_t2938734618  : public ReplayBase_t779703160
{
public:
	// System.Int32 ReplayStopMove::isPlayAnim
	int32_t ___isPlayAnim_8;
	// System.Single ReplayStopMove::rotX
	float ___rotX_9;
	// System.Single ReplayStopMove::rotY
	float ___rotY_10;
	// System.Single ReplayStopMove::rotZ
	float ___rotZ_11;

public:
	inline static int32_t get_offset_of_isPlayAnim_8() { return static_cast<int32_t>(offsetof(ReplayStopMove_t2938734618, ___isPlayAnim_8)); }
	inline int32_t get_isPlayAnim_8() const { return ___isPlayAnim_8; }
	inline int32_t* get_address_of_isPlayAnim_8() { return &___isPlayAnim_8; }
	inline void set_isPlayAnim_8(int32_t value)
	{
		___isPlayAnim_8 = value;
	}

	inline static int32_t get_offset_of_rotX_9() { return static_cast<int32_t>(offsetof(ReplayStopMove_t2938734618, ___rotX_9)); }
	inline float get_rotX_9() const { return ___rotX_9; }
	inline float* get_address_of_rotX_9() { return &___rotX_9; }
	inline void set_rotX_9(float value)
	{
		___rotX_9 = value;
	}

	inline static int32_t get_offset_of_rotY_10() { return static_cast<int32_t>(offsetof(ReplayStopMove_t2938734618, ___rotY_10)); }
	inline float get_rotY_10() const { return ___rotY_10; }
	inline float* get_address_of_rotY_10() { return &___rotY_10; }
	inline void set_rotY_10(float value)
	{
		___rotY_10 = value;
	}

	inline static int32_t get_offset_of_rotZ_11() { return static_cast<int32_t>(offsetof(ReplayStopMove_t2938734618, ___rotZ_11)); }
	inline float get_rotZ_11() const { return ___rotZ_11; }
	inline float* get_address_of_rotZ_11() { return &___rotZ_11; }
	inline void set_rotZ_11(float value)
	{
		___rotZ_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
