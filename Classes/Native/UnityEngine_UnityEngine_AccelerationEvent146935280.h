﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AccelerationEvent
struct  AccelerationEvent_t146935280 
{
public:
	// System.Single UnityEngine.AccelerationEvent::x
	float ___x_0;
	// System.Single UnityEngine.AccelerationEvent::y
	float ___y_1;
	// System.Single UnityEngine.AccelerationEvent::z
	float ___z_2;
	// System.Single UnityEngine.AccelerationEvent::m_TimeDelta
	float ___m_TimeDelta_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(AccelerationEvent_t146935280, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(AccelerationEvent_t146935280, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(AccelerationEvent_t146935280, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_3() { return static_cast<int32_t>(offsetof(AccelerationEvent_t146935280, ___m_TimeDelta_3)); }
	inline float get_m_TimeDelta_3() const { return ___m_TimeDelta_3; }
	inline float* get_address_of_m_TimeDelta_3() { return &___m_TimeDelta_3; }
	inline void set_m_TimeDelta_3(float value)
	{
		___m_TimeDelta_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: UnityEngine.AccelerationEvent
struct AccelerationEvent_t146935280_marshaled_pinvoke
{
	float ___x_0;
	float ___y_1;
	float ___z_2;
	float ___m_TimeDelta_3;
};
// Native definition for marshalling of: UnityEngine.AccelerationEvent
struct AccelerationEvent_t146935280_marshaled_com
{
	float ___x_0;
	float ___y_1;
	float ___z_2;
	float ___m_TimeDelta_3;
};
