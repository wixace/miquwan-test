﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Serialization.JsonFx.EcmaScriptIdentifier
struct EcmaScriptIdentifier_t4023709608;
// Pathfinding.Serialization.JsonFx.JsonWriter
struct JsonWriter_t541860733;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonFx541860733.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Pathfinding.Serialization.JsonFx.EcmaScriptIdentifier::Pathfinding.Serialization.JsonFx.IJsonSerializable.WriteJson(Pathfinding.Serialization.JsonFx.JsonWriter)
extern "C"  void EcmaScriptIdentifier_Pathfinding_Serialization_JsonFx_IJsonSerializable_WriteJson_m3353525581 (EcmaScriptIdentifier_t4023709608 * __this, JsonWriter_t541860733 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.Serialization.JsonFx.EcmaScriptIdentifier::EnsureValidIdentifier(System.String,System.Boolean)
extern "C"  String_t* EcmaScriptIdentifier_EnsureValidIdentifier_m1404313670 (Il2CppObject * __this /* static, unused */, String_t* ___varExpr0, bool ___nested1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.Serialization.JsonFx.EcmaScriptIdentifier::EnsureValidIdentifier(System.String,System.Boolean,System.Boolean)
extern "C"  String_t* EcmaScriptIdentifier_EnsureValidIdentifier_m1118494327 (Il2CppObject * __this /* static, unused */, String_t* ___varExpr0, bool ___nested1, bool ___throwOnEmpty2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Serialization.JsonFx.EcmaScriptIdentifier::IsValidIdentifier(System.String,System.Boolean)
extern "C"  bool EcmaScriptIdentifier_IsValidIdentifier_m1345170647 (Il2CppObject * __this /* static, unused */, String_t* ___varExpr0, bool ___nested1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Serialization.JsonFx.EcmaScriptIdentifier::IsReservedWord(System.String)
extern "C"  bool EcmaScriptIdentifier_IsReservedWord_m1103390885 (Il2CppObject * __this /* static, unused */, String_t* ___varExpr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Serialization.JsonFx.EcmaScriptIdentifier::Equals(System.Object)
extern "C"  bool EcmaScriptIdentifier_Equals_m1423181236 (EcmaScriptIdentifier_t4023709608 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.Serialization.JsonFx.EcmaScriptIdentifier::ToString()
extern "C"  String_t* EcmaScriptIdentifier_ToString_m2780159466 (EcmaScriptIdentifier_t4023709608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Serialization.JsonFx.EcmaScriptIdentifier::GetHashCode()
extern "C"  int32_t EcmaScriptIdentifier_GetHashCode_m2065690444 (EcmaScriptIdentifier_t4023709608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
