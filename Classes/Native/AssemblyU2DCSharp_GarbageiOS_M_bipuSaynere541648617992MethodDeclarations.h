﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_bipuSaynere54
struct M_bipuSaynere54_t1648617992;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_bipuSaynere54::.ctor()
extern "C"  void M_bipuSaynere54__ctor_m3119527003 (M_bipuSaynere54_t1648617992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_bipuSaynere54::M_sorweRuhairdou0(System.String[],System.Int32)
extern "C"  void M_bipuSaynere54_M_sorweRuhairdou0_m3154569453 (M_bipuSaynere54_t1648617992 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_bipuSaynere54::M_pajeyurNirje1(System.String[],System.Int32)
extern "C"  void M_bipuSaynere54_M_pajeyurNirje1_m2381379719 (M_bipuSaynere54_t1648617992 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_bipuSaynere54::M_cemellawMerserjow2(System.String[],System.Int32)
extern "C"  void M_bipuSaynere54_M_cemellawMerserjow2_m2962955912 (M_bipuSaynere54_t1648617992 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
