﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.QualitySettings
struct QualitySettings_t719345784;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ShadowProjection4063319827.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_AnisotropicFiltering503731965.h"
#include "UnityEngine_UnityEngine_ColorSpace161844263.h"
#include "UnityEngine_UnityEngine_BlendWeights3700066926.h"

// System.Void UnityEngine.QualitySettings::.ctor()
extern "C"  void QualitySettings__ctor_m2119214167 (QualitySettings_t719345784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UnityEngine.QualitySettings::get_names()
extern "C"  StringU5BU5D_t4054002952* QualitySettings_get_names_m284012725 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.QualitySettings::GetQualityLevel()
extern "C"  int32_t QualitySettings_GetQualityLevel_m1740241520 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::SetQualityLevel(System.Int32,System.Boolean)
extern "C"  void QualitySettings_SetQualityLevel_m1369916048 (Il2CppObject * __this /* static, unused */, int32_t ___index0, bool ___applyExpensiveChanges1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::SetQualityLevel(System.Int32)
extern "C"  void QualitySettings_SetQualityLevel_m962145613 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::IncreaseLevel(System.Boolean)
extern "C"  void QualitySettings_IncreaseLevel_m3712064654 (Il2CppObject * __this /* static, unused */, bool ___applyExpensiveChanges0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::IncreaseLevel()
extern "C"  void QualitySettings_IncreaseLevel_m3950578519 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::DecreaseLevel(System.Boolean)
extern "C"  void QualitySettings_DecreaseLevel_m2539185074 (Il2CppObject * __this /* static, unused */, bool ___applyExpensiveChanges0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::DecreaseLevel()
extern "C"  void QualitySettings_DecreaseLevel_m962847099 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.QualitySettings::get_pixelLightCount()
extern "C"  int32_t QualitySettings_get_pixelLightCount_m516387691 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_pixelLightCount(System.Int32)
extern "C"  void QualitySettings_set_pixelLightCount_m751067080 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ShadowProjection UnityEngine.QualitySettings::get_shadowProjection()
extern "C"  int32_t QualitySettings_get_shadowProjection_m2809772028 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_shadowProjection(UnityEngine.ShadowProjection)
extern "C"  void QualitySettings_set_shadowProjection_m3501455027 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.QualitySettings::get_shadowCascades()
extern "C"  int32_t QualitySettings_get_shadowCascades_m2367020021 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_shadowCascades(System.Int32)
extern "C"  void QualitySettings_set_shadowCascades_m4032047290 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.QualitySettings::get_shadowDistance()
extern "C"  float QualitySettings_get_shadowDistance_m2372428063 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_shadowDistance(System.Single)
extern "C"  void QualitySettings_set_shadowDistance_m3102645484 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.QualitySettings::get_shadowNearPlaneOffset()
extern "C"  float QualitySettings_get_shadowNearPlaneOffset_m2379033215 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_shadowNearPlaneOffset(System.Single)
extern "C"  void QualitySettings_set_shadowNearPlaneOffset_m1951001484 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.QualitySettings::get_shadowCascade2Split()
extern "C"  float QualitySettings_get_shadowCascade2Split_m2965549044 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_shadowCascade2Split(System.Single)
extern "C"  void QualitySettings_set_shadowCascade2Split_m4285344823 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.QualitySettings::get_shadowCascade4Split()
extern "C"  Vector3_t4282066566  QualitySettings_get_shadowCascade4Split_m4077801120 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_shadowCascade4Split(UnityEngine.Vector3)
extern "C"  void QualitySettings_set_shadowCascade4Split_m2216977127 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::INTERNAL_get_shadowCascade4Split(UnityEngine.Vector3&)
extern "C"  void QualitySettings_INTERNAL_get_shadowCascade4Split_m1025321231 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::INTERNAL_set_shadowCascade4Split(UnityEngine.Vector3&)
extern "C"  void QualitySettings_INTERNAL_set_shadowCascade4Split_m2091271963 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.QualitySettings::get_masterTextureLimit()
extern "C"  int32_t QualitySettings_get_masterTextureLimit_m2907593240 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_masterTextureLimit(System.Int32)
extern "C"  void QualitySettings_set_masterTextureLimit_m1173942365 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnisotropicFiltering UnityEngine.QualitySettings::get_anisotropicFiltering()
extern "C"  int32_t QualitySettings_get_anisotropicFiltering_m2086675408 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_anisotropicFiltering(UnityEngine.AnisotropicFiltering)
extern "C"  void QualitySettings_set_anisotropicFiltering_m2841520243 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.QualitySettings::get_lodBias()
extern "C"  float QualitySettings_get_lodBias_m1207867410 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_lodBias(System.Single)
extern "C"  void QualitySettings_set_lodBias_m1212998873 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.QualitySettings::get_maximumLODLevel()
extern "C"  int32_t QualitySettings_get_maximumLODLevel_m933671087 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_maximumLODLevel(System.Int32)
extern "C"  void QualitySettings_set_maximumLODLevel_m2189285132 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.QualitySettings::get_particleRaycastBudget()
extern "C"  int32_t QualitySettings_get_particleRaycastBudget_m1146460756 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_particleRaycastBudget(System.Int32)
extern "C"  void QualitySettings_set_particleRaycastBudget_m221747249 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.QualitySettings::get_softVegetation()
extern "C"  bool QualitySettings_get_softVegetation_m4089244520 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_softVegetation(System.Boolean)
extern "C"  void QualitySettings_set_softVegetation_m3407837177 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.QualitySettings::get_realtimeReflectionProbes()
extern "C"  bool QualitySettings_get_realtimeReflectionProbes_m2548464105 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_realtimeReflectionProbes(System.Boolean)
extern "C"  void QualitySettings_set_realtimeReflectionProbes_m4060018746 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.QualitySettings::get_billboardsFaceCameraPosition()
extern "C"  bool QualitySettings_get_billboardsFaceCameraPosition_m1402270191 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_billboardsFaceCameraPosition(System.Boolean)
extern "C"  void QualitySettings_set_billboardsFaceCameraPosition_m2744127680 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.QualitySettings::get_maxQueuedFrames()
extern "C"  int32_t QualitySettings_get_maxQueuedFrames_m1382379177 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_maxQueuedFrames(System.Int32)
extern "C"  void QualitySettings_set_maxQueuedFrames_m402484742 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.QualitySettings::get_vSyncCount()
extern "C"  int32_t QualitySettings_get_vSyncCount_m1698965652 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_vSyncCount(System.Int32)
extern "C"  void QualitySettings_set_vSyncCount_m2698975449 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.QualitySettings::get_antiAliasing()
extern "C"  int32_t QualitySettings_get_antiAliasing_m2055981962 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_antiAliasing(System.Int32)
extern "C"  void QualitySettings_set_antiAliasing_m3006369231 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ColorSpace UnityEngine.QualitySettings::get_desiredColorSpace()
extern "C"  int32_t QualitySettings_get_desiredColorSpace_m377960350 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
extern "C"  int32_t QualitySettings_get_activeColorSpace_m2993616266 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.BlendWeights UnityEngine.QualitySettings::get_blendWeights()
extern "C"  int32_t QualitySettings_get_blendWeights_m1992404338 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_blendWeights(UnityEngine.BlendWeights)
extern "C"  void QualitySettings_set_blendWeights_m1547724627 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.QualitySettings::get_asyncUploadTimeSlice()
extern "C"  int32_t QualitySettings_get_asyncUploadTimeSlice_m2441990174 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_asyncUploadTimeSlice(System.Int32)
extern "C"  void QualitySettings_set_asyncUploadTimeSlice_m1242308707 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.QualitySettings::get_asyncUploadBufferSize()
extern "C"  int32_t QualitySettings_get_asyncUploadBufferSize_m1555576458 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_asyncUploadBufferSize(System.Int32)
extern "C"  void QualitySettings_set_asyncUploadBufferSize_m3392377703 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
