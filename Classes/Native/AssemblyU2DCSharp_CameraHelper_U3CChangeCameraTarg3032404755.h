﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CameraHelper
struct CameraHelper_t3196871507;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraHelper/<ChangeCameraTarget>c__AnonStorey14E
struct  U3CChangeCameraTargetU3Ec__AnonStorey14E_t3032404755  : public Il2CppObject
{
public:
	// System.Boolean CameraHelper/<ChangeCameraTarget>c__AnonStorey14E::isMove
	bool ___isMove_0;
	// CameraHelper CameraHelper/<ChangeCameraTarget>c__AnonStorey14E::<>f__this
	CameraHelper_t3196871507 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_isMove_0() { return static_cast<int32_t>(offsetof(U3CChangeCameraTargetU3Ec__AnonStorey14E_t3032404755, ___isMove_0)); }
	inline bool get_isMove_0() const { return ___isMove_0; }
	inline bool* get_address_of_isMove_0() { return &___isMove_0; }
	inline void set_isMove_0(bool value)
	{
		___isMove_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CChangeCameraTargetU3Ec__AnonStorey14E_t3032404755, ___U3CU3Ef__this_1)); }
	inline CameraHelper_t3196871507 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline CameraHelper_t3196871507 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(CameraHelper_t3196871507 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
