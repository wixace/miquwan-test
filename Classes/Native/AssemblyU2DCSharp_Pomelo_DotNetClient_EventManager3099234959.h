﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Pomelo.DotNetClient.MsgEvent>
struct List_1_t1450508997;
// Pomelo.DotNetClient.ObjPool`1<Pomelo.DotNetClient.MsgEvent>
struct ObjPool_1_t493003188;
// Pomelo.DotNetClient.PomeloClient
struct PomeloClient_t4215271137;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pomelo.DotNetClient.EventManager
struct  EventManager_t3099234959  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<Pomelo.DotNetClient.MsgEvent> Pomelo.DotNetClient.EventManager::msgList
	List_1_t1450508997 * ___msgList_0;
	// Pomelo.DotNetClient.ObjPool`1<Pomelo.DotNetClient.MsgEvent> Pomelo.DotNetClient.EventManager::pool
	ObjPool_1_t493003188 * ___pool_1;
	// Pomelo.DotNetClient.PomeloClient Pomelo.DotNetClient.EventManager::pc
	PomeloClient_t4215271137 * ___pc_2;

public:
	inline static int32_t get_offset_of_msgList_0() { return static_cast<int32_t>(offsetof(EventManager_t3099234959, ___msgList_0)); }
	inline List_1_t1450508997 * get_msgList_0() const { return ___msgList_0; }
	inline List_1_t1450508997 ** get_address_of_msgList_0() { return &___msgList_0; }
	inline void set_msgList_0(List_1_t1450508997 * value)
	{
		___msgList_0 = value;
		Il2CppCodeGenWriteBarrier(&___msgList_0, value);
	}

	inline static int32_t get_offset_of_pool_1() { return static_cast<int32_t>(offsetof(EventManager_t3099234959, ___pool_1)); }
	inline ObjPool_1_t493003188 * get_pool_1() const { return ___pool_1; }
	inline ObjPool_1_t493003188 ** get_address_of_pool_1() { return &___pool_1; }
	inline void set_pool_1(ObjPool_1_t493003188 * value)
	{
		___pool_1 = value;
		Il2CppCodeGenWriteBarrier(&___pool_1, value);
	}

	inline static int32_t get_offset_of_pc_2() { return static_cast<int32_t>(offsetof(EventManager_t3099234959, ___pc_2)); }
	inline PomeloClient_t4215271137 * get_pc_2() const { return ___pc_2; }
	inline PomeloClient_t4215271137 ** get_address_of_pc_2() { return &___pc_2; }
	inline void set_pc_2(PomeloClient_t4215271137 * value)
	{
		___pc_2 = value;
		Il2CppCodeGenWriteBarrier(&___pc_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
