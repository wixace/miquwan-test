﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2045888271MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::.ctor()
#define Dictionary_2__ctor_m1320406133(__this, method) ((  void (*) (Dictionary_2_t253816784 *, const MethodInfo*))Dictionary_2__ctor_m3794638399_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m1040005827(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t253816784 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m273898294_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m2191135436(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t253816784 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m4211605689_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::.ctor(System.Int32)
#define Dictionary_2__ctor_m3500873117(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t253816784 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2504582416_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m1751258289(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t253816784 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2042790116_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m2188028301(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t253816784 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m4162067200_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2166199106(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t253816784 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1029455407_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1894631554(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t253816784 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1376055727_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m3426503698(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t253816784 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1780508229_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.IDictionary.get_Values()
#define Dictionary_2_System_Collections_IDictionary_get_Values_m997692544(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t253816784 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m4038979059_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.IDictionary.get_IsFixedSize()
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m344591893(__this, method) ((  bool (*) (Dictionary_2_t253816784 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m824729858_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.IDictionary.get_IsReadOnly()
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3928808004(__this, method) ((  bool (*) (Dictionary_2_t253816784 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1311897015_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m156418812(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t253816784 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1551250025_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3841706593(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t253816784 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1049066318_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m2918955856(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t253816784 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m781609539_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m68045350(__this, ___key0, method) ((  bool (*) (Dictionary_2_t253816784 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1783363411_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m1546200735(__this, ___key0, method) ((  void (*) (Dictionary_2_t253816784 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2215006604_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m277219054(__this, method) ((  bool (*) (Dictionary_2_t253816784 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1826238689_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2615097626(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t253816784 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2075478797_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3485129138(__this, method) ((  bool (*) (Dictionary_2_t253816784 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1433083365_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2733488053(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t253816784 *, KeyValuePair_2_t152597490 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3257059362_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3364679213(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t253816784 *, KeyValuePair_2_t152597490 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2744049760_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2753692761(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t253816784 *, KeyValuePair_2U5BU5D_t4087239687*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2008986502_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2699738258(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t253816784 *, KeyValuePair_2_t152597490 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2350489477_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1089063032(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t253816784 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3550803941_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3431963635(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t253816784 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m4115711264_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1578049392(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t253816784 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m52259357_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3148431371(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t253816784 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1395730616_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::get_Count()
#define Dictionary_2_get_Count_m2018810036(__this, method) ((  int32_t (*) (Dictionary_2_t253816784 *, const MethodInfo*))Dictionary_2_get_Count_m1232250407_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::get_Item(TKey)
#define Dictionary_2_get_Item_m2860880649(__this, ___key0, method) ((  FightPoint_t4216057832 * (*) (Dictionary_2_t253816784 *, CombatEntity_t684137495 *, const MethodInfo*))Dictionary_2_get_Item_m2285357284_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m2285548300(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t253816784 *, CombatEntity_t684137495 *, FightPoint_t4216057832 *, const MethodInfo*))Dictionary_2_set_Item_m2627891647_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m701426116(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t253816784 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2966484407_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m3915975283(__this, ___size0, method) ((  void (*) (Dictionary_2_t253816784 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2119297760_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m2028151087(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t253816784 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2536521436_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m3836774779(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t152597490  (*) (Il2CppObject * /* static, unused */, CombatEntity_t684137495 *, FightPoint_t4216057832 *, const MethodInfo*))Dictionary_2_make_pair_m2083407400_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m4193483291(__this /* static, unused */, ___key0, ___value1, method) ((  CombatEntity_t684137495 * (*) (Il2CppObject * /* static, unused */, CombatEntity_t684137495 *, FightPoint_t4216057832 *, const MethodInfo*))Dictionary_2_pick_key_m3909093582_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m3661216923(__this /* static, unused */, ___key0, ___value1, method) ((  FightPoint_t4216057832 * (*) (Il2CppObject * /* static, unused */, CombatEntity_t684137495 *, FightPoint_t4216057832 *, const MethodInfo*))Dictionary_2_pick_value_m3477594126_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m4062366656(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t253816784 *, KeyValuePair_2U5BU5D_t4087239687*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3401241971_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::Resize()
#define Dictionary_2_Resize_m2959982444(__this, method) ((  void (*) (Dictionary_2_t253816784 *, const MethodInfo*))Dictionary_2_Resize_m1727470041_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::Add(TKey,TValue)
#define Dictionary_2_Add_m2942471057(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t253816784 *, CombatEntity_t684137495 *, FightPoint_t4216057832 *, const MethodInfo*))Dictionary_2_Add_m3537188182_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::Clear()
#define Dictionary_2_Clear_m1656172151(__this, method) ((  void (*) (Dictionary_2_t253816784 *, const MethodInfo*))Dictionary_2_Clear_m1200771690_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m2833740752(__this, ___key0, method) ((  bool (*) (Dictionary_2_t253816784 *, CombatEntity_t684137495 *, const MethodInfo*))Dictionary_2_ContainsKey_m3006991056_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m4012470685(__this, ___value0, method) ((  bool (*) (Dictionary_2_t253816784 *, FightPoint_t4216057832 *, const MethodInfo*))Dictionary_2_ContainsValue_m712275664_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m3483636714(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t253816784 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m1544184413_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m846879930(__this, ___sender0, method) ((  void (*) (Dictionary_2_t253816784 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1638301735_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::Remove(TKey)
#define Dictionary_2_Remove_m3845458200(__this, ___key0, method) ((  bool (*) (Dictionary_2_t253816784 *, CombatEntity_t684137495 *, const MethodInfo*))Dictionary_2_Remove_m2155719712_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m440026140(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t253816784 *, CombatEntity_t684137495 *, FightPoint_t4216057832 **, const MethodInfo*))Dictionary_2_TryGetValue_m2075628329_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::get_Keys()
#define Dictionary_2_get_Keys_m1898650697(__this, method) ((  KeyCollection_t1880576235 * (*) (Dictionary_2_t253816784 *, const MethodInfo*))Dictionary_2_get_Keys_m2624609910_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::get_Values()
#define Dictionary_2_get_Values_m3049062665(__this, method) ((  ValueCollection_t3249389793 * (*) (Dictionary_2_t253816784 *, const MethodInfo*))Dictionary_2_get_Values_m2070602102_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m3643342198(__this, ___key0, method) ((  CombatEntity_t684137495 * (*) (Dictionary_2_t253816784 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3358952489_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m1973531062(__this, ___value0, method) ((  FightPoint_t4216057832 * (*) (Dictionary_2_t253816784 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1789908265_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m3177708694(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t253816784 *, KeyValuePair_2_t152597490 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m3073235459_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m2472683665(__this, method) ((  Enumerator_t1571140176  (*) (Dictionary_2_t253816784 *, const MethodInfo*))Dictionary_2_GetEnumerator_m65675076_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m1343196832(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, CombatEntity_t684137495 *, FightPoint_t4216057832 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m3818730131_gshared)(__this /* static, unused */, ___key0, ___value1, method)
