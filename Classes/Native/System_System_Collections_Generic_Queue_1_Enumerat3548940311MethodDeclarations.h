﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat3401177016MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<Pathfinding.GraphNode>::.ctor(System.Collections.Generic.Queue`1<T>)
#define Enumerator__ctor_m2952314967(__this, ___q0, method) ((  void (*) (Enumerator_t3548940311 *, Queue_1_t2259854799 *, const MethodInfo*))Enumerator__ctor_m3846579967_gshared)(__this, ___q0, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<Pathfinding.GraphNode>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2490160978(__this, method) ((  void (*) (Enumerator_t3548940311 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2396412922_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<Pathfinding.GraphNode>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m989508488(__this, method) ((  Il2CppObject * (*) (Enumerator_t3548940311 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m280731440_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<Pathfinding.GraphNode>::Dispose()
#define Enumerator_Dispose_m779610853(__this, method) ((  void (*) (Enumerator_t3548940311 *, const MethodInfo*))Enumerator_Dispose_m3069945149_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<Pathfinding.GraphNode>::MoveNext()
#define Enumerator_MoveNext_m2288244758(__this, method) ((  bool (*) (Enumerator_t3548940311 *, const MethodInfo*))Enumerator_MoveNext_m262168254_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<Pathfinding.GraphNode>::get_Current()
#define Enumerator_get_Current_m3796738807(__this, method) ((  GraphNode_t23612370 * (*) (Enumerator_t3548940311 *, const MethodInfo*))Enumerator_get_Current_m3381813839_gshared)(__this, method)
