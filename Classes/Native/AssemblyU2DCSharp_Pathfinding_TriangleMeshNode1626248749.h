﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.INavmeshHolder[]
struct INavmeshHolderU5BU5D_t3899225556;

#include "AssemblyU2DCSharp_Pathfinding_MeshNode3005053445.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.TriangleMeshNode
struct  TriangleMeshNode_t1626248749  : public MeshNode_t3005053445
{
public:
	// System.Int32 Pathfinding.TriangleMeshNode::v0
	int32_t ___v0_17;
	// System.Int32 Pathfinding.TriangleMeshNode::v1
	int32_t ___v1_18;
	// System.Int32 Pathfinding.TriangleMeshNode::v2
	int32_t ___v2_19;

public:
	inline static int32_t get_offset_of_v0_17() { return static_cast<int32_t>(offsetof(TriangleMeshNode_t1626248749, ___v0_17)); }
	inline int32_t get_v0_17() const { return ___v0_17; }
	inline int32_t* get_address_of_v0_17() { return &___v0_17; }
	inline void set_v0_17(int32_t value)
	{
		___v0_17 = value;
	}

	inline static int32_t get_offset_of_v1_18() { return static_cast<int32_t>(offsetof(TriangleMeshNode_t1626248749, ___v1_18)); }
	inline int32_t get_v1_18() const { return ___v1_18; }
	inline int32_t* get_address_of_v1_18() { return &___v1_18; }
	inline void set_v1_18(int32_t value)
	{
		___v1_18 = value;
	}

	inline static int32_t get_offset_of_v2_19() { return static_cast<int32_t>(offsetof(TriangleMeshNode_t1626248749, ___v2_19)); }
	inline int32_t get_v2_19() const { return ___v2_19; }
	inline int32_t* get_address_of_v2_19() { return &___v2_19; }
	inline void set_v2_19(int32_t value)
	{
		___v2_19 = value;
	}
};

struct TriangleMeshNode_t1626248749_StaticFields
{
public:
	// Pathfinding.INavmeshHolder[] Pathfinding.TriangleMeshNode::_navmeshHolders
	INavmeshHolderU5BU5D_t3899225556* ____navmeshHolders_20;

public:
	inline static int32_t get_offset_of__navmeshHolders_20() { return static_cast<int32_t>(offsetof(TriangleMeshNode_t1626248749_StaticFields, ____navmeshHolders_20)); }
	inline INavmeshHolderU5BU5D_t3899225556* get__navmeshHolders_20() const { return ____navmeshHolders_20; }
	inline INavmeshHolderU5BU5D_t3899225556** get_address_of__navmeshHolders_20() { return &____navmeshHolders_20; }
	inline void set__navmeshHolders_20(INavmeshHolderU5BU5D_t3899225556* value)
	{
		____navmeshHolders_20 = value;
		Il2CppCodeGenWriteBarrier(&____navmeshHolders_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
