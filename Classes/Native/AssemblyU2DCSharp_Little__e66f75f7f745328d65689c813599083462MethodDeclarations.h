﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._e66f75f7f745328d65689c818f77fd2b
struct _e66f75f7f745328d65689c818f77fd2b_t3599083462;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._e66f75f7f745328d65689c818f77fd2b::.ctor()
extern "C"  void _e66f75f7f745328d65689c818f77fd2b__ctor_m1148640551 (_e66f75f7f745328d65689c818f77fd2b_t3599083462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e66f75f7f745328d65689c818f77fd2b::_e66f75f7f745328d65689c818f77fd2bm2(System.Int32)
extern "C"  int32_t _e66f75f7f745328d65689c818f77fd2b__e66f75f7f745328d65689c818f77fd2bm2_m1741605849 (_e66f75f7f745328d65689c818f77fd2b_t3599083462 * __this, int32_t ____e66f75f7f745328d65689c818f77fd2ba0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e66f75f7f745328d65689c818f77fd2b::_e66f75f7f745328d65689c818f77fd2bm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _e66f75f7f745328d65689c818f77fd2b__e66f75f7f745328d65689c818f77fd2bm_m2249188093 (_e66f75f7f745328d65689c818f77fd2b_t3599083462 * __this, int32_t ____e66f75f7f745328d65689c818f77fd2ba0, int32_t ____e66f75f7f745328d65689c818f77fd2b571, int32_t ____e66f75f7f745328d65689c818f77fd2bc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
