﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweenColor
struct TweenColor_t2922258392;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UIWidget
struct UIWidget_t769069560;
// UITweener
struct UITweener_t105489188;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_TweenColor2922258392.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UITweener105489188.h"

// System.Void TweenColor::.ctor()
extern "C"  void TweenColor__ctor_m67619779 (TweenColor_t2922258392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenColor::Cache()
extern "C"  void TweenColor_Cache_m1452148707 (TweenColor_t2922258392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color TweenColor::get_color()
extern "C"  Color_t4194546905  TweenColor_get_color_m2471677844 (TweenColor_t2922258392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenColor::set_color(UnityEngine.Color)
extern "C"  void TweenColor_set_color_m1029665855 (TweenColor_t2922258392 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color TweenColor::get_value()
extern "C"  Color_t4194546905  TweenColor_get_value_m1753736738 (TweenColor_t2922258392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenColor::set_value(UnityEngine.Color)
extern "C"  void TweenColor_set_value_m1797557105 (TweenColor_t2922258392 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenColor::OnUpdate(System.Single,System.Boolean)
extern "C"  void TweenColor_OnUpdate_m1902336411 (TweenColor_t2922258392 * __this, float ___factor0, bool ___isFinished1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TweenColor TweenColor::Begin(UnityEngine.GameObject,System.Single,UnityEngine.Color)
extern "C"  TweenColor_t2922258392 * TweenColor_Begin_m397250626 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, float ___duration1, Color_t4194546905  ___color2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenColor::SetStartToCurrentValue()
extern "C"  void TweenColor_SetStartToCurrentValue_m1446997812 (TweenColor_t2922258392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenColor::SetEndToCurrentValue()
extern "C"  void TweenColor_SetEndToCurrentValue_m4180918573 (TweenColor_t2922258392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenColor::SetCurrentValueToStart()
extern "C"  void TweenColor_SetCurrentValueToStart_m692700718 (TweenColor_t2922258392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenColor::SetCurrentValueToEnd()
extern "C"  void TweenColor_SetCurrentValueToEnd_m2892637991 (TweenColor_t2922258392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color TweenColor::ilo_get_value1(TweenColor)
extern "C"  Color_t4194546905  TweenColor_ilo_get_value1_m1579649932 (Il2CppObject * __this /* static, unused */, TweenColor_t2922258392 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenColor::ilo_Cache2(TweenColor)
extern "C"  void TweenColor_ilo_Cache2_m2486241964 (Il2CppObject * __this /* static, unused */, TweenColor_t2922258392 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenColor::ilo_set_color3(UIWidget,UnityEngine.Color)
extern "C"  void TweenColor_ilo_set_color3_m3292484169 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, Color_t4194546905  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenColor::ilo_Sample4(UITweener,System.Single,System.Boolean)
extern "C"  void TweenColor_ilo_Sample4_m3248670850 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, float ___factor1, bool ___isFinished2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
