﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._af795452c438d0ca3a16961a608667dc
struct _af795452c438d0ca3a16961a608667dc_t1894050612;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._af795452c438d0ca3a16961a608667dc::.ctor()
extern "C"  void _af795452c438d0ca3a16961a608667dc__ctor_m354522489 (_af795452c438d0ca3a16961a608667dc_t1894050612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._af795452c438d0ca3a16961a608667dc::_af795452c438d0ca3a16961a608667dcm2(System.Int32)
extern "C"  int32_t _af795452c438d0ca3a16961a608667dc__af795452c438d0ca3a16961a608667dcm2_m1426040089 (_af795452c438d0ca3a16961a608667dc_t1894050612 * __this, int32_t ____af795452c438d0ca3a16961a608667dca0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._af795452c438d0ca3a16961a608667dc::_af795452c438d0ca3a16961a608667dcm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _af795452c438d0ca3a16961a608667dc__af795452c438d0ca3a16961a608667dcm_m2091512253 (_af795452c438d0ca3a16961a608667dc_t1894050612 * __this, int32_t ____af795452c438d0ca3a16961a608667dca0, int32_t ____af795452c438d0ca3a16961a608667dc501, int32_t ____af795452c438d0ca3a16961a608667dcc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
