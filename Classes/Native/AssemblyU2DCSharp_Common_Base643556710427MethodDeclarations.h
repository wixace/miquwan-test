﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Common.Base64
struct Base64_t3556710427;
// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t2012439129;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Text_Encoding2012439129.h"
#include "mscorlib_System_String7231557.h"

// System.Void Common.Base64::.ctor()
extern "C"  void Base64__ctor_m615154425 (Base64_t3556710427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.Base64::EncodeBase64(System.Text.Encoding,System.String)
extern "C"  String_t* Base64_EncodeBase64_m4157703924 (Il2CppObject * __this /* static, unused */, Encoding_t2012439129 * ___encode0, String_t* ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.Base64::EncodeBase64(System.String)
extern "C"  String_t* Base64_EncodeBase64_m1487697013 (Il2CppObject * __this /* static, unused */, String_t* ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.Base64::DecodeBase64(System.Text.Encoding,System.String)
extern "C"  String_t* Base64_DecodeBase64_m2629536972 (Il2CppObject * __this /* static, unused */, Encoding_t2012439129 * ___encode0, String_t* ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.Base64::DecodeBase64(System.String)
extern "C"  String_t* Base64_DecodeBase64_m246904733 (Il2CppObject * __this /* static, unused */, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
