﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginOasis
struct PluginOasis_t2549995064;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// VersionMgr
struct VersionMgr_t1322950208;
// System.Object
struct Il2CppObject;
// VersionInfo
struct VersionInfo_t2356638086;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_FloatTextMgr630384591.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"

// System.Void PluginOasis::.ctor()
extern "C"  void PluginOasis__ctor_m1013916723 (PluginOasis_t2549995064 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::Init()
extern "C"  void PluginOasis_Init_m1092622273 (PluginOasis_t2549995064 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginOasis::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginOasis_ReqSDKHttpLogin_m4151885284 (PluginOasis_t2549995064 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginOasis::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginOasis_IsLoginSuccess_m2838497752 (PluginOasis_t2549995064 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::OpenUserLogin()
extern "C"  void PluginOasis_OpenUserLogin_m3227935621 (PluginOasis_t2549995064 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::RoleEnterGame(CEvent.ZEvent)
extern "C"  void PluginOasis_RoleEnterGame_m549969206 (PluginOasis_t2549995064 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::CreatRole(CEvent.ZEvent)
extern "C"  void PluginOasis_CreatRole_m1671099467 (PluginOasis_t2549995064 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginOasis_RoleUpgrade_m1739566404 (PluginOasis_t2549995064 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::UserPay(CEvent.ZEvent)
extern "C"  void PluginOasis_UserPay_m43262029 (PluginOasis_t2549995064 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginOasis_SignCallBack_m2632527462 (PluginOasis_t2549995064 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginOasis::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginOasis_BuildOrderParam2WWWForm_m3663665424 (PluginOasis_t2549995064 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::OnLoginSuccess(System.String)
extern "C"  void PluginOasis_OnLoginSuccess_m490447608 (PluginOasis_t2549995064 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::OnLoginFail(System.String)
extern "C"  void PluginOasis_OnLoginFail_m2271572297 (PluginOasis_t2549995064 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::OnSwitchSuccess(System.String)
extern "C"  void PluginOasis_OnSwitchSuccess_m1357082817 (PluginOasis_t2549995064 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::OnPaySuccess(System.String)
extern "C"  void PluginOasis_OnPaySuccess_m1914594103 (PluginOasis_t2549995064 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::OnPayFail(System.String)
extern "C"  void PluginOasis_OnPayFail_m3374952810 (PluginOasis_t2549995064 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::OnPayCancel(System.String)
extern "C"  void PluginOasis_OnPayCancel_m2773199054 (PluginOasis_t2549995064 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::OnLogoutSuccess(System.String)
extern "C"  void PluginOasis_OnLogoutSuccess_m2322516631 (PluginOasis_t2549995064 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::init(System.String)
extern "C"  void PluginOasis_init_m1407197441 (PluginOasis_t2549995064 * __this, String_t* ___language0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::login()
extern "C"  void PluginOasis_login_m535931546 (PluginOasis_t2549995064 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::setUserInfo(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginOasis_setUserInfo_m1683489574 (PluginOasis_t2549995064 * __this, String_t* ___serverID0, String_t* ___serverName1, String_t* ___serverType2, String_t* ___roleName3, String_t* ___roleID4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::pay(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginOasis_pay_m3963434873 (PluginOasis_t2549995064 * __this, String_t* ___goodsId0, String_t* ___price1, String_t* ___serverID2, String_t* ___roleID3, String_t* ___extraData4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::showMenu()
extern "C"  void PluginOasis_showMenu_m3941372301 (PluginOasis_t2549995064 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::<OnLogoutSuccess>m__447()
extern "C"  void PluginOasis_U3COnLogoutSuccessU3Em__447_m1300171995 (PluginOasis_t2549995064 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginOasis::ilo_get_Instance1()
extern "C"  VersionMgr_t1322950208 * PluginOasis_ilo_get_Instance1_m2419480526 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::ilo_Log2(System.Object,System.Boolean)
extern "C"  void PluginOasis_ilo_Log2_m49825019 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginOasis::ilo_get_isSdkLogin3(VersionMgr)
extern "C"  bool PluginOasis_ilo_get_isSdkLogin3_m1338517411 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginOasis::ilo_get_currentVS4(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginOasis_ilo_get_currentVS4_m3920037086 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginOasis::ilo_get_ProductsCfgMgr5()
extern "C"  ProductsCfgMgr_t2493714872 * PluginOasis_ilo_get_ProductsCfgMgr5_m3093826871 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr PluginOasis::ilo_get_FloatTextMgr6()
extern "C"  FloatTextMgr_t630384591 * PluginOasis_ilo_get_FloatTextMgr6_m4012286794 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::ilo_DispatchEvent7(CEvent.ZEvent)
extern "C"  void PluginOasis_ilo_DispatchEvent7_m3375620416 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOasis::ilo_FloatText8(FloatTextMgr,FLOAT_TEXT_ID,System.Int32,System.Object[])
extern "C"  void PluginOasis_ilo_FloatText8_m4108451037 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, int32_t ___textId1, int32_t ___offsetY2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
