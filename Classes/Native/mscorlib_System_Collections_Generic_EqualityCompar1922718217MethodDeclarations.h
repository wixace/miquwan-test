﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<MScrollView/MoveWay>
struct DefaultComparer_t1922718217;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MScrollView_MoveWay3474941550.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<MScrollView/MoveWay>::.ctor()
extern "C"  void DefaultComparer__ctor_m865225644_gshared (DefaultComparer_t1922718217 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m865225644(__this, method) ((  void (*) (DefaultComparer_t1922718217 *, const MethodInfo*))DefaultComparer__ctor_m865225644_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<MScrollView/MoveWay>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3464095359_gshared (DefaultComparer_t1922718217 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3464095359(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1922718217 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m3464095359_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<MScrollView/MoveWay>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1974139517_gshared (DefaultComparer_t1922718217 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1974139517(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1922718217 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m1974139517_gshared)(__this, ___x0, ___y1, method)
