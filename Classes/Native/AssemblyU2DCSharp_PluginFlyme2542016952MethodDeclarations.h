﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginFlyme
struct PluginFlyme_t2542016952;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// VersionInfo
struct VersionInfo_t2356638086;
// System.Object
struct Il2CppObject;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"

// System.Void PluginFlyme::.ctor()
extern "C"  void PluginFlyme__ctor_m2969358515 (PluginFlyme_t2542016952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFlyme::Init()
extern "C"  void PluginFlyme_Init_m1709890369 (PluginFlyme_t2542016952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFlyme::OnDestory()
extern "C"  void PluginFlyme_OnDestory_m3751291590 (PluginFlyme_t2542016952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFlyme::Clear()
extern "C"  void PluginFlyme_Clear_m375491806 (PluginFlyme_t2542016952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFlyme::RoleEnterGame(CEvent.ZEvent)
extern "C"  void PluginFlyme_RoleEnterGame_m312148150 (PluginFlyme_t2542016952 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFlyme::UserPay(CEvent.ZEvent)
extern "C"  void PluginFlyme_UserPay_m208135117 (PluginFlyme_t2542016952 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFlyme::onFlymeLoginSuccess(System.String)
extern "C"  void PluginFlyme_onFlymeLoginSuccess_m2111267531 (PluginFlyme_t2542016952 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFlyme::onFlymeLoginError(System.String)
extern "C"  void PluginFlyme_onFlymeLoginError_m1385751910 (PluginFlyme_t2542016952 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFlyme::OnPayFlymeSuccess(System.String)
extern "C"  void PluginFlyme_OnPayFlymeSuccess_m2550966672 (PluginFlyme_t2542016952 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFlyme::OnPayFlymeError(System.String)
extern "C"  void PluginFlyme_OnPayFlymeError_m545986923 (PluginFlyme_t2542016952 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFlyme::OnExitGameSuccess(System.String)
extern "C"  void PluginFlyme_OnExitGameSuccess_m3713999485 (PluginFlyme_t2542016952 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFlyme::OnLogoutFlyme(System.String)
extern "C"  void PluginFlyme_OnLogoutFlyme_m3203902991 (PluginFlyme_t2542016952 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginFlyme::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginFlyme_ReqSDKHttpLogin_m4198025828 (PluginFlyme_t2542016952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFlyme::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginFlyme_IsLoginSuccess_m1925729624 (PluginFlyme_t2542016952 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFlyme::LoginFlymeMgr()
extern "C"  void PluginFlyme_LoginFlymeMgr_m2038374311 (PluginFlyme_t2542016952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFlyme::LogoutFlymeMgr()
extern "C"  void PluginFlyme_LogoutFlymeMgr_m659302632 (PluginFlyme_t2542016952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFlyme::PlayFlymeMgr(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.Int32,System.String,System.String,System.String,System.String)
extern "C"  void PluginFlyme_PlayFlymeMgr_m2596293747 (PluginFlyme_t2542016952 * __this, String_t* ___price0, String_t* ___roleId1, String_t* ___roleName2, String_t* ___roleLevel3, String_t* ___serverId4, String_t* ___serverName5, String_t* ___productId6, String_t* ___productName7, int32_t ___buyNum8, String_t* ___exts9, String_t* ___productDes10, String_t* ___notifyUrl11, String_t* ___orderUrl12, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFlyme::EnterGameFlymeMgr(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginFlyme_EnterGameFlymeMgr_m2019571466 (PluginFlyme_t2542016952 * __this, String_t* ___roleId0, String_t* ___roleName1, String_t* ___roleLevel2, String_t* ___serverId3, String_t* ___serverName4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFlyme::<OnExitGameSuccess>m__41D()
extern "C"  void PluginFlyme_U3COnExitGameSuccessU3Em__41D_m532786929 (PluginFlyme_t2542016952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFlyme::<OnLogoutFlyme>m__41E()
extern "C"  void PluginFlyme_U3COnLogoutFlymeU3Em__41E_m1129431492 (PluginFlyme_t2542016952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFlyme::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginFlyme_ilo_AddEventListener1_m198366689 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginFlyme::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginFlyme_ilo_get_Instance2_m3810871311 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginFlyme::ilo_get_currentVS3(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginFlyme_ilo_get_currentVS3_m3300671965 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFlyme::ilo_Log4(System.Object,System.Boolean)
extern "C"  void PluginFlyme_ilo_Log4_m3920009469 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginFlyme::ilo_get_PluginsSdkMgr5()
extern "C"  PluginsSdkMgr_t3884624670 * PluginFlyme_ilo_get_PluginsSdkMgr5_m588375375 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFlyme::ilo_DispatchEvent6(CEvent.ZEvent)
extern "C"  void PluginFlyme_ilo_DispatchEvent6_m3048804321 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginFlyme::ilo_Logout7(System.Action)
extern "C"  void PluginFlyme_ilo_Logout7_m742066610 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginFlyme::ilo_get_DeviceID8(PluginsSdkMgr)
extern "C"  String_t* PluginFlyme_ilo_get_DeviceID8_m3980907541 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginFlyme::ilo_get_isSdkLogin9(VersionMgr)
extern "C"  bool PluginFlyme_ilo_get_isSdkLogin9_m1347058473 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
