﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900;
// System.String[]
struct StringU5BU5D_t4054002952;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1151101739;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleShopManager
struct  SimpleShopManager_t1021689509  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Sprite[] SimpleShopManager::sprites
	SpriteU5BU5D_t2761310900* ___sprites_2;
	// System.String[] SimpleShopManager::titles
	StringU5BU5D_t4054002952* ___titles_3;
	// System.String[] SimpleShopManager::prices
	StringU5BU5D_t4054002952* ___prices_4;
	// UnityEngine.GameObject SimpleShopManager::itemPrefab
	GameObject_t3674682005 * ___itemPrefab_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> SimpleShopManager::_bag
	Dictionary_2_t1151101739 * ____bag_6;

public:
	inline static int32_t get_offset_of_sprites_2() { return static_cast<int32_t>(offsetof(SimpleShopManager_t1021689509, ___sprites_2)); }
	inline SpriteU5BU5D_t2761310900* get_sprites_2() const { return ___sprites_2; }
	inline SpriteU5BU5D_t2761310900** get_address_of_sprites_2() { return &___sprites_2; }
	inline void set_sprites_2(SpriteU5BU5D_t2761310900* value)
	{
		___sprites_2 = value;
		Il2CppCodeGenWriteBarrier(&___sprites_2, value);
	}

	inline static int32_t get_offset_of_titles_3() { return static_cast<int32_t>(offsetof(SimpleShopManager_t1021689509, ___titles_3)); }
	inline StringU5BU5D_t4054002952* get_titles_3() const { return ___titles_3; }
	inline StringU5BU5D_t4054002952** get_address_of_titles_3() { return &___titles_3; }
	inline void set_titles_3(StringU5BU5D_t4054002952* value)
	{
		___titles_3 = value;
		Il2CppCodeGenWriteBarrier(&___titles_3, value);
	}

	inline static int32_t get_offset_of_prices_4() { return static_cast<int32_t>(offsetof(SimpleShopManager_t1021689509, ___prices_4)); }
	inline StringU5BU5D_t4054002952* get_prices_4() const { return ___prices_4; }
	inline StringU5BU5D_t4054002952** get_address_of_prices_4() { return &___prices_4; }
	inline void set_prices_4(StringU5BU5D_t4054002952* value)
	{
		___prices_4 = value;
		Il2CppCodeGenWriteBarrier(&___prices_4, value);
	}

	inline static int32_t get_offset_of_itemPrefab_5() { return static_cast<int32_t>(offsetof(SimpleShopManager_t1021689509, ___itemPrefab_5)); }
	inline GameObject_t3674682005 * get_itemPrefab_5() const { return ___itemPrefab_5; }
	inline GameObject_t3674682005 ** get_address_of_itemPrefab_5() { return &___itemPrefab_5; }
	inline void set_itemPrefab_5(GameObject_t3674682005 * value)
	{
		___itemPrefab_5 = value;
		Il2CppCodeGenWriteBarrier(&___itemPrefab_5, value);
	}

	inline static int32_t get_offset_of__bag_6() { return static_cast<int32_t>(offsetof(SimpleShopManager_t1021689509, ____bag_6)); }
	inline Dictionary_2_t1151101739 * get__bag_6() const { return ____bag_6; }
	inline Dictionary_2_t1151101739 ** get_address_of__bag_6() { return &____bag_6; }
	inline void set__bag_6(Dictionary_2_t1151101739 * value)
	{
		____bag_6 = value;
		Il2CppCodeGenWriteBarrier(&____bag_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
