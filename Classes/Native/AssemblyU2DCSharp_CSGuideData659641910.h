﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CSGuideVO[]
struct CSGuideVOU5BU5D_t2582593960;
// System.Collections.Generic.List`1<CSGuideVO>
struct List_1_t1190101749;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSGuideData
struct  CSGuideData_t659641910  : public Il2CppObject
{
public:
	// CSGuideVO[] CSGuideData::guideList
	CSGuideVOU5BU5D_t2582593960* ___guideList_0;
	// System.Collections.Generic.List`1<CSGuideVO> CSGuideData::finishList
	List_1_t1190101749 * ___finishList_1;

public:
	inline static int32_t get_offset_of_guideList_0() { return static_cast<int32_t>(offsetof(CSGuideData_t659641910, ___guideList_0)); }
	inline CSGuideVOU5BU5D_t2582593960* get_guideList_0() const { return ___guideList_0; }
	inline CSGuideVOU5BU5D_t2582593960** get_address_of_guideList_0() { return &___guideList_0; }
	inline void set_guideList_0(CSGuideVOU5BU5D_t2582593960* value)
	{
		___guideList_0 = value;
		Il2CppCodeGenWriteBarrier(&___guideList_0, value);
	}

	inline static int32_t get_offset_of_finishList_1() { return static_cast<int32_t>(offsetof(CSGuideData_t659641910, ___finishList_1)); }
	inline List_1_t1190101749 * get_finishList_1() const { return ___finishList_1; }
	inline List_1_t1190101749 ** get_address_of_finishList_1() { return &___finishList_1; }
	inline void set_finishList_1(List_1_t1190101749 * value)
	{
		___finishList_1 = value;
		Il2CppCodeGenWriteBarrier(&___finishList_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
