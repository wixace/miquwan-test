﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HeadUpBloodMgr
struct HeadUpBloodMgr_t2330866585;
// NumEffect
struct NumEffect_t2824221847;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UIFont
struct UIFont_t2503090435;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// Mihua.Asset.OnLoadAsset
struct OnLoadAsset_t4181543125;
// SceneMgr
struct SceneMgr_t3584105996;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NumEffect2824221847.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_HeadUpBloodMgr_LabelType4067300664.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_Mihua_Asset_OnLoadAsset4181543125.h"
#include "AssemblyU2DCSharp_HeadUpBloodMgr2330866585.h"

// System.Void HeadUpBloodMgr::.ctor()
extern "C"  void HeadUpBloodMgr__ctor_m3897922850 (HeadUpBloodMgr_t2330866585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeadUpBloodMgr::.cctor()
extern "C"  void HeadUpBloodMgr__cctor_m94427851 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeadUpBloodMgr HeadUpBloodMgr::get_Instance()
extern "C"  HeadUpBloodMgr_t2330866585 * HeadUpBloodMgr_get_Instance_m986773572 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeadUpBloodMgr::Clear()
extern "C"  void HeadUpBloodMgr_Clear_m1304056141 (HeadUpBloodMgr_t2330866585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NumEffect HeadUpBloodMgr::GetNumEffect()
extern "C"  NumEffect_t2824221847 * HeadUpBloodMgr_GetNumEffect_m2639511615 (HeadUpBloodMgr_t2330866585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeadUpBloodMgr::ResetNumEffect(NumEffect)
extern "C"  void HeadUpBloodMgr_ResetNumEffect_m4279453425 (HeadUpBloodMgr_t2330866585 * __this, NumEffect_t2824221847 * ___numEff0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeadUpBloodMgr::Init()
extern "C"  void HeadUpBloodMgr_Init_m2571128050 (HeadUpBloodMgr_t2330866585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeadUpBloodMgr::Clear(CEvent.ZEvent)
extern "C"  void HeadUpBloodMgr_Clear_m3613362254 (HeadUpBloodMgr_t2330866585 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeadUpBloodMgr::AddBlood(System.Boolean,System.String,HeadUpBloodMgr/LabelType,UnityEngine.Vector3,System.Int32,System.Boolean,System.Int32)
extern "C"  void HeadUpBloodMgr_AddBlood_m4139066238 (HeadUpBloodMgr_t2330866585 * __this, bool ___isHero0, String_t* ___value1, int32_t ___type2, Vector3_t4282066566  ___pos3, int32_t ___id4, bool ___isbuff5, int32_t ___index6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HeadUpBloodMgr::GetValueTxt(HeadUpBloodMgr/LabelType,System.String)
extern "C"  String_t* HeadUpBloodMgr_GetValueTxt_m407804086 (HeadUpBloodMgr_t2330866585 * __this, int32_t ___type0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIFont HeadUpBloodMgr::GetFont(System.Boolean,HeadUpBloodMgr/LabelType)
extern "C"  UIFont_t2503090435 * HeadUpBloodMgr_GetFont_m94046572 (HeadUpBloodMgr_t2330866585 * __this, bool ___isHero0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeadUpBloodMgr::ZUpdate()
extern "C"  void HeadUpBloodMgr_ZUpdate_m260905891 (HeadUpBloodMgr_t2330866585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeadUpBloodMgr::<Init>m__3D0(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  void HeadUpBloodMgr_U3CInitU3Em__3D0_m477008941 (HeadUpBloodMgr_t2330866585 * __this, AssetOperation_t778728221 * ___ao0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeadUpBloodMgr::ilo_Clear1(NumEffect)
extern "C"  void HeadUpBloodMgr_ilo_Clear1_m2811975618 (Il2CppObject * __this /* static, unused */, NumEffect_t2824221847 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HeadUpBloodMgr::ilo_Instantiate2(UnityEngine.GameObject)
extern "C"  GameObject_t3674682005 * HeadUpBloodMgr_ilo_Instantiate2_m1513588554 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Asset.ABLoadOperation.AssetOperation HeadUpBloodMgr::ilo_LoadAssetAsync3(System.String,Mihua.Asset.OnLoadAsset)
extern "C"  AssetOperation_t778728221 * HeadUpBloodMgr_ilo_LoadAssetAsync3_m2352422253 (Il2CppObject * __this /* static, unused */, String_t* ___assetName0, OnLoadAsset_t4181543125 * ___onLoadAsset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SceneMgr HeadUpBloodMgr::ilo_get_SceneMgr4()
extern "C"  SceneMgr_t3584105996 * HeadUpBloodMgr_ilo_get_SceneMgr4_m209899963 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NumEffect HeadUpBloodMgr::ilo_GetNumEffect5(HeadUpBloodMgr)
extern "C"  NumEffect_t2824221847 * HeadUpBloodMgr_ilo_GetNumEffect5_m3882998508 (Il2CppObject * __this /* static, unused */, HeadUpBloodMgr_t2330866585 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HeadUpBloodMgr::ilo_WorldToNgui6(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  HeadUpBloodMgr_ilo_WorldToNgui6_m1576973454 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ____pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HeadUpBloodMgr::ilo_GetValueTxt7(HeadUpBloodMgr,HeadUpBloodMgr/LabelType,System.String)
extern "C"  String_t* HeadUpBloodMgr_ilo_GetValueTxt7_m1741962185 (Il2CppObject * __this /* static, unused */, HeadUpBloodMgr_t2330866585 * ____this0, int32_t ___type1, String_t* ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeadUpBloodMgr::ilo_Send8(NumEffect)
extern "C"  void HeadUpBloodMgr_ilo_Send8_m1426632254 (Il2CppObject * __this /* static, unused */, NumEffect_t2824221847 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeadUpBloodMgr::ilo_AddEventListener9(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void HeadUpBloodMgr_ilo_AddEventListener9_m3922686090 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
