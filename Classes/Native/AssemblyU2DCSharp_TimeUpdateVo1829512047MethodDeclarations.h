﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimeUpdateVo
struct TimeUpdateVo_t1829512047;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3771233898.h"

// System.Void TimeUpdateVo::.ctor(System.Single,System.Action)
extern "C"  void TimeUpdateVo__ctor_m1083942294 (TimeUpdateVo_t1829512047 * __this, float ___delay0, Action_t3771233898 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeUpdateVo::SetDelay(System.Double)
extern "C"  void TimeUpdateVo_SetDelay_m4061504937 (TimeUpdateVo_t1829512047 * __this, double ___delay0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeUpdateVo::Update()
extern "C"  void TimeUpdateVo_Update_m969545857 (TimeUpdateVo_t1829512047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeUpdateVo::Clear()
extern "C"  void TimeUpdateVo_Clear_m1676671031 (TimeUpdateVo_t1829512047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
