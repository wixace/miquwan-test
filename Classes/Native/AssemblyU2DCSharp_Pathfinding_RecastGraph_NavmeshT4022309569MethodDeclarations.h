﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RecastGraph/NavmeshTile
struct NavmeshTile_t4022309569;
// Pathfinding.GraphNodeDelegateCancelable
struct GraphNodeDelegateCancelable_t3591372971;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNodeDelegateCan3591372971.h"

// System.Void Pathfinding.RecastGraph/NavmeshTile::.ctor()
extern "C"  void NavmeshTile__ctor_m3971889610 (NavmeshTile_t4022309569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph/NavmeshTile::GetTileCoordinates(System.Int32,System.Int32&,System.Int32&)
extern "C"  void NavmeshTile_GetTileCoordinates_m3717984428 (NavmeshTile_t4022309569 * __this, int32_t ___tileIndex0, int32_t* ___x1, int32_t* ___z2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.RecastGraph/NavmeshTile::GetVertexArrayIndex(System.Int32)
extern "C"  int32_t NavmeshTile_GetVertexArrayIndex_m514678906 (NavmeshTile_t4022309569 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.RecastGraph/NavmeshTile::GetVertex(System.Int32)
extern "C"  Int3_t1974045594  NavmeshTile_GetVertex_m964896386 (NavmeshTile_t4022309569 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph/NavmeshTile::GetNodes(Pathfinding.GraphNodeDelegateCancelable)
extern "C"  void NavmeshTile_GetNodes_m1962506078 (NavmeshTile_t4022309569 * __this, GraphNodeDelegateCancelable_t3591372971 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
