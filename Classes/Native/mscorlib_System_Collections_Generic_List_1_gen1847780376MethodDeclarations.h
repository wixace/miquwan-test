﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Core.RpsResult>
struct List_1_t1847780376;
// System.Collections.Generic.IEnumerable`1<Core.RpsResult>
struct IEnumerable_1_t3780507781;
// System.Collections.Generic.IEnumerator`1<Core.RpsResult>
struct IEnumerator_1_t2391459873;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Core.RpsResult>
struct ICollection_1_t1374184811;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>
struct ReadOnlyCollection_1_t2036672360;
// Core.RpsResult[]
struct RpsResultU5BU5D_t1451851929;
// System.Predicate`1<Core.RpsResult>
struct Predicate_1_t90651707;
// System.Collections.Generic.IComparer`1<Core.RpsResult>
struct IComparer_1_t3054608866;
// System.Comparison`1<Core.RpsResult>
struct Comparison_1_t3490923307;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Core_RpsResult479594824.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1867453146.h"

// System.Void System.Collections.Generic.List`1<Core.RpsResult>::.ctor()
extern "C"  void List_1__ctor_m3675798094_gshared (List_1_t1847780376 * __this, const MethodInfo* method);
#define List_1__ctor_m3675798094(__this, method) ((  void (*) (List_1_t1847780376 *, const MethodInfo*))List_1__ctor_m3675798094_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m4065135496_gshared (List_1_t1847780376 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m4065135496(__this, ___collection0, method) ((  void (*) (List_1_t1847780376 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m4065135496_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3556807528_gshared (List_1_t1847780376 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m3556807528(__this, ___capacity0, method) ((  void (*) (List_1_t1847780376 *, int32_t, const MethodInfo*))List_1__ctor_m3556807528_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::.cctor()
extern "C"  void List_1__cctor_m185521654_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m185521654(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m185521654_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Core.RpsResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3396479969_gshared (List_1_t1847780376 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3396479969(__this, method) ((  Il2CppObject* (*) (List_1_t1847780376 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3396479969_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2510862477_gshared (List_1_t1847780376 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m2510862477(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1847780376 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2510862477_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Core.RpsResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1696098396_gshared (List_1_t1847780376 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1696098396(__this, method) ((  Il2CppObject * (*) (List_1_t1847780376 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1696098396_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Core.RpsResult>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2164273377_gshared (List_1_t1847780376 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2164273377(__this, ___item0, method) ((  int32_t (*) (List_1_t1847780376 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2164273377_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Core.RpsResult>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1547318655_gshared (List_1_t1847780376 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1547318655(__this, ___item0, method) ((  bool (*) (List_1_t1847780376 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1547318655_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Core.RpsResult>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1628114617_gshared (List_1_t1847780376 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1628114617(__this, ___item0, method) ((  int32_t (*) (List_1_t1847780376 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1628114617_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3575015340_gshared (List_1_t1847780376 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3575015340(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1847780376 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3575015340_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m1670527484_gshared (List_1_t1847780376 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1670527484(__this, ___item0, method) ((  void (*) (List_1_t1847780376 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1670527484_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Core.RpsResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2984022720_gshared (List_1_t1847780376 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2984022720(__this, method) ((  bool (*) (List_1_t1847780376 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2984022720_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Core.RpsResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m2576006525_gshared (List_1_t1847780376 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2576006525(__this, method) ((  bool (*) (List_1_t1847780376 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m2576006525_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Core.RpsResult>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1868514927_gshared (List_1_t1847780376 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1868514927(__this, method) ((  Il2CppObject * (*) (List_1_t1847780376 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1868514927_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Core.RpsResult>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3380326446_gshared (List_1_t1847780376 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3380326446(__this, method) ((  bool (*) (List_1_t1847780376 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3380326446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Core.RpsResult>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1809977611_gshared (List_1_t1847780376 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1809977611(__this, method) ((  bool (*) (List_1_t1847780376 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1809977611_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Core.RpsResult>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1412126774_gshared (List_1_t1847780376 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1412126774(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1847780376 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1412126774_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m229857411_gshared (List_1_t1847780376 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m229857411(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1847780376 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m229857411_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::Add(T)
extern "C"  void List_1_Add_m3370158910_gshared (List_1_t1847780376 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m3370158910(__this, ___item0, method) ((  void (*) (List_1_t1847780376 *, int32_t, const MethodInfo*))List_1_Add_m3370158910_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1738433347_gshared (List_1_t1847780376 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1738433347(__this, ___newCount0, method) ((  void (*) (List_1_t1847780376 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1738433347_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m3497928900_gshared (List_1_t1847780376 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m3497928900(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t1847780376 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m3497928900_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m4189646401_gshared (List_1_t1847780376 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m4189646401(__this, ___collection0, method) ((  void (*) (List_1_t1847780376 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m4189646401_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3450618625_gshared (List_1_t1847780376 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3450618625(__this, ___enumerable0, method) ((  void (*) (List_1_t1847780376 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3450618625_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3309637366_gshared (List_1_t1847780376 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3309637366(__this, ___collection0, method) ((  void (*) (List_1_t1847780376 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3309637366_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Core.RpsResult>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2036672360 * List_1_AsReadOnly_m3410400463_gshared (List_1_t1847780376 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3410400463(__this, method) ((  ReadOnlyCollection_1_t2036672360 * (*) (List_1_t1847780376 *, const MethodInfo*))List_1_AsReadOnly_m3410400463_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Core.RpsResult>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m626923684_gshared (List_1_t1847780376 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m626923684(__this, ___item0, method) ((  int32_t (*) (List_1_t1847780376 *, int32_t, const MethodInfo*))List_1_BinarySearch_m626923684_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::Clear()
extern "C"  void List_1_Clear_m475710658_gshared (List_1_t1847780376 * __this, const MethodInfo* method);
#define List_1_Clear_m475710658(__this, method) ((  void (*) (List_1_t1847780376 *, const MethodInfo*))List_1_Clear_m475710658_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Core.RpsResult>::Contains(T)
extern "C"  bool List_1_Contains_m2834995252_gshared (List_1_t1847780376 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m2834995252(__this, ___item0, method) ((  bool (*) (List_1_t1847780376 *, int32_t, const MethodInfo*))List_1_Contains_m2834995252_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3874143992_gshared (List_1_t1847780376 * __this, RpsResultU5BU5D_t1451851929* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m3874143992(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1847780376 *, RpsResultU5BU5D_t1451851929*, int32_t, const MethodInfo*))List_1_CopyTo_m3874143992_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Core.RpsResult>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m4198318798_gshared (List_1_t1847780376 * __this, Predicate_1_t90651707 * ___match0, const MethodInfo* method);
#define List_1_Find_m4198318798(__this, ___match0, method) ((  int32_t (*) (List_1_t1847780376 *, Predicate_1_t90651707 *, const MethodInfo*))List_1_Find_m4198318798_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3618813675_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t90651707 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3618813675(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t90651707 *, const MethodInfo*))List_1_CheckMatch_m3618813675_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Core.RpsResult>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3366595784_gshared (List_1_t1847780376 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t90651707 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3366595784(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1847780376 *, int32_t, int32_t, Predicate_1_t90651707 *, const MethodInfo*))List_1_GetIndex_m3366595784_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Core.RpsResult>::GetEnumerator()
extern "C"  Enumerator_t1867453146  List_1_GetEnumerator_m975347569_gshared (List_1_t1847780376 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m975347569(__this, method) ((  Enumerator_t1867453146  (*) (List_1_t1847780376 *, const MethodInfo*))List_1_GetEnumerator_m975347569_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Core.RpsResult>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1038896580_gshared (List_1_t1847780376 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m1038896580(__this, ___item0, method) ((  int32_t (*) (List_1_t1847780376 *, int32_t, const MethodInfo*))List_1_IndexOf_m1038896580_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m904928591_gshared (List_1_t1847780376 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m904928591(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1847780376 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m904928591_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3620511176_gshared (List_1_t1847780376 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3620511176(__this, ___index0, method) ((  void (*) (List_1_t1847780376 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3620511176_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m1134310255_gshared (List_1_t1847780376 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m1134310255(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1847780376 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m1134310255_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2392503460_gshared (List_1_t1847780376 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2392503460(__this, ___collection0, method) ((  void (*) (List_1_t1847780376 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2392503460_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Core.RpsResult>::Remove(T)
extern "C"  bool List_1_Remove_m815781167_gshared (List_1_t1847780376 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m815781167(__this, ___item0, method) ((  bool (*) (List_1_t1847780376 *, int32_t, const MethodInfo*))List_1_Remove_m815781167_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Core.RpsResult>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m3138955911_gshared (List_1_t1847780376 * __this, Predicate_1_t90651707 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m3138955911(__this, ___match0, method) ((  int32_t (*) (List_1_t1847780376 *, Predicate_1_t90651707 *, const MethodInfo*))List_1_RemoveAll_m3138955911_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3303130421_gshared (List_1_t1847780376 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3303130421(__this, ___index0, method) ((  void (*) (List_1_t1847780376 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3303130421_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m796825432_gshared (List_1_t1847780376 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m796825432(__this, ___index0, ___count1, method) ((  void (*) (List_1_t1847780376 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m796825432_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::Reverse()
extern "C"  void List_1_Reverse_m3554095895_gshared (List_1_t1847780376 * __this, const MethodInfo* method);
#define List_1_Reverse_m3554095895(__this, method) ((  void (*) (List_1_t1847780376 *, const MethodInfo*))List_1_Reverse_m3554095895_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::Sort()
extern "C"  void List_1_Sort_m3940269035_gshared (List_1_t1847780376 * __this, const MethodInfo* method);
#define List_1_Sort_m3940269035(__this, method) ((  void (*) (List_1_t1847780376 *, const MethodInfo*))List_1_Sort_m3940269035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m249540185_gshared (List_1_t1847780376 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m249540185(__this, ___comparer0, method) ((  void (*) (List_1_t1847780376 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m249540185_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2224764414_gshared (List_1_t1847780376 * __this, Comparison_1_t3490923307 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2224764414(__this, ___comparison0, method) ((  void (*) (List_1_t1847780376 *, Comparison_1_t3490923307 *, const MethodInfo*))List_1_Sort_m2224764414_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Core.RpsResult>::ToArray()
extern "C"  RpsResultU5BU5D_t1451851929* List_1_ToArray_m3361425392_gshared (List_1_t1847780376 * __this, const MethodInfo* method);
#define List_1_ToArray_m3361425392(__this, method) ((  RpsResultU5BU5D_t1451851929* (*) (List_1_t1847780376 *, const MethodInfo*))List_1_ToArray_m3361425392_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::TrimExcess()
extern "C"  void List_1_TrimExcess_m3211460420_gshared (List_1_t1847780376 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m3211460420(__this, method) ((  void (*) (List_1_t1847780376 *, const MethodInfo*))List_1_TrimExcess_m3211460420_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Core.RpsResult>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2414330868_gshared (List_1_t1847780376 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2414330868(__this, method) ((  int32_t (*) (List_1_t1847780376 *, const MethodInfo*))List_1_get_Capacity_m2414330868_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3243589461_gshared (List_1_t1847780376 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3243589461(__this, ___value0, method) ((  void (*) (List_1_t1847780376 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3243589461_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Core.RpsResult>::get_Count()
extern "C"  int32_t List_1_get_Count_m1804929248_gshared (List_1_t1847780376 * __this, const MethodInfo* method);
#define List_1_get_Count_m1804929248(__this, method) ((  int32_t (*) (List_1_t1847780376 *, const MethodInfo*))List_1_get_Count_m1804929248_gshared)(__this, method)
// T System.Collections.Generic.List`1<Core.RpsResult>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m4172331509_gshared (List_1_t1847780376 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m4172331509(__this, ___index0, method) ((  int32_t (*) (List_1_t1847780376 *, int32_t, const MethodInfo*))List_1_get_Item_m4172331509_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Core.RpsResult>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m960058502_gshared (List_1_t1847780376 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m960058502(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1847780376 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m960058502_gshared)(__this, ___index0, ___value1, method)
