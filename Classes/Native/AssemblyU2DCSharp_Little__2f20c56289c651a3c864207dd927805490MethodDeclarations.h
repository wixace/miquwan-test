﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._2f20c56289c651a3c864207dd9e5fda7
struct _2f20c56289c651a3c864207dd9e5fda7_t27805490;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__2f20c56289c651a3c864207dd927805490.h"

// System.Void Little._2f20c56289c651a3c864207dd9e5fda7::.ctor()
extern "C"  void _2f20c56289c651a3c864207dd9e5fda7__ctor_m735237947 (_2f20c56289c651a3c864207dd9e5fda7_t27805490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._2f20c56289c651a3c864207dd9e5fda7::_2f20c56289c651a3c864207dd9e5fda7m2(System.Int32)
extern "C"  int32_t _2f20c56289c651a3c864207dd9e5fda7__2f20c56289c651a3c864207dd9e5fda7m2_m3965531225 (_2f20c56289c651a3c864207dd9e5fda7_t27805490 * __this, int32_t ____2f20c56289c651a3c864207dd9e5fda7a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._2f20c56289c651a3c864207dd9e5fda7::_2f20c56289c651a3c864207dd9e5fda7m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _2f20c56289c651a3c864207dd9e5fda7__2f20c56289c651a3c864207dd9e5fda7m_m3551529597 (_2f20c56289c651a3c864207dd9e5fda7_t27805490 * __this, int32_t ____2f20c56289c651a3c864207dd9e5fda7a0, int32_t ____2f20c56289c651a3c864207dd9e5fda7351, int32_t ____2f20c56289c651a3c864207dd9e5fda7c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._2f20c56289c651a3c864207dd9e5fda7::ilo__2f20c56289c651a3c864207dd9e5fda7m21(Little._2f20c56289c651a3c864207dd9e5fda7,System.Int32)
extern "C"  int32_t _2f20c56289c651a3c864207dd9e5fda7_ilo__2f20c56289c651a3c864207dd9e5fda7m21_m3708315961 (Il2CppObject * __this /* static, unused */, _2f20c56289c651a3c864207dd9e5fda7_t27805490 * ____this0, int32_t ____2f20c56289c651a3c864207dd9e5fda7a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
