﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// CSHeroUnit[]
struct CSHeroUnitU5BU5D_t1342235227;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSAISPlayerData
struct  CSAISPlayerData_t3122557766  : public Il2CppObject
{
public:
	// System.UInt32 CSAISPlayerData::roleId
	uint32_t ___roleId_0;
	// System.String CSAISPlayerData::name
	String_t* ___name_1;
	// System.String CSAISPlayerData::unionName
	String_t* ___unionName_2;
	// System.String CSAISPlayerData::icon
	String_t* ___icon_3;
	// System.UInt32 CSAISPlayerData::rank
	uint32_t ___rank_4;
	// System.UInt32 CSAISPlayerData::lv
	uint32_t ___lv_5;
	// System.UInt32 CSAISPlayerData::vipLv
	uint32_t ___vipLv_6;
	// System.UInt32 CSAISPlayerData::exp
	uint32_t ___exp_7;
	// System.UInt32 CSAISPlayerData::power
	uint32_t ___power_8;
	// System.UInt32 CSAISPlayerData::serviceId
	uint32_t ___serviceId_9;
	// System.String CSAISPlayerData::serviceName
	String_t* ___serviceName_10;
	// CSHeroUnit[] CSAISPlayerData::heroGroups
	CSHeroUnitU5BU5D_t1342235227* ___heroGroups_11;

public:
	inline static int32_t get_offset_of_roleId_0() { return static_cast<int32_t>(offsetof(CSAISPlayerData_t3122557766, ___roleId_0)); }
	inline uint32_t get_roleId_0() const { return ___roleId_0; }
	inline uint32_t* get_address_of_roleId_0() { return &___roleId_0; }
	inline void set_roleId_0(uint32_t value)
	{
		___roleId_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(CSAISPlayerData_t3122557766, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_unionName_2() { return static_cast<int32_t>(offsetof(CSAISPlayerData_t3122557766, ___unionName_2)); }
	inline String_t* get_unionName_2() const { return ___unionName_2; }
	inline String_t** get_address_of_unionName_2() { return &___unionName_2; }
	inline void set_unionName_2(String_t* value)
	{
		___unionName_2 = value;
		Il2CppCodeGenWriteBarrier(&___unionName_2, value);
	}

	inline static int32_t get_offset_of_icon_3() { return static_cast<int32_t>(offsetof(CSAISPlayerData_t3122557766, ___icon_3)); }
	inline String_t* get_icon_3() const { return ___icon_3; }
	inline String_t** get_address_of_icon_3() { return &___icon_3; }
	inline void set_icon_3(String_t* value)
	{
		___icon_3 = value;
		Il2CppCodeGenWriteBarrier(&___icon_3, value);
	}

	inline static int32_t get_offset_of_rank_4() { return static_cast<int32_t>(offsetof(CSAISPlayerData_t3122557766, ___rank_4)); }
	inline uint32_t get_rank_4() const { return ___rank_4; }
	inline uint32_t* get_address_of_rank_4() { return &___rank_4; }
	inline void set_rank_4(uint32_t value)
	{
		___rank_4 = value;
	}

	inline static int32_t get_offset_of_lv_5() { return static_cast<int32_t>(offsetof(CSAISPlayerData_t3122557766, ___lv_5)); }
	inline uint32_t get_lv_5() const { return ___lv_5; }
	inline uint32_t* get_address_of_lv_5() { return &___lv_5; }
	inline void set_lv_5(uint32_t value)
	{
		___lv_5 = value;
	}

	inline static int32_t get_offset_of_vipLv_6() { return static_cast<int32_t>(offsetof(CSAISPlayerData_t3122557766, ___vipLv_6)); }
	inline uint32_t get_vipLv_6() const { return ___vipLv_6; }
	inline uint32_t* get_address_of_vipLv_6() { return &___vipLv_6; }
	inline void set_vipLv_6(uint32_t value)
	{
		___vipLv_6 = value;
	}

	inline static int32_t get_offset_of_exp_7() { return static_cast<int32_t>(offsetof(CSAISPlayerData_t3122557766, ___exp_7)); }
	inline uint32_t get_exp_7() const { return ___exp_7; }
	inline uint32_t* get_address_of_exp_7() { return &___exp_7; }
	inline void set_exp_7(uint32_t value)
	{
		___exp_7 = value;
	}

	inline static int32_t get_offset_of_power_8() { return static_cast<int32_t>(offsetof(CSAISPlayerData_t3122557766, ___power_8)); }
	inline uint32_t get_power_8() const { return ___power_8; }
	inline uint32_t* get_address_of_power_8() { return &___power_8; }
	inline void set_power_8(uint32_t value)
	{
		___power_8 = value;
	}

	inline static int32_t get_offset_of_serviceId_9() { return static_cast<int32_t>(offsetof(CSAISPlayerData_t3122557766, ___serviceId_9)); }
	inline uint32_t get_serviceId_9() const { return ___serviceId_9; }
	inline uint32_t* get_address_of_serviceId_9() { return &___serviceId_9; }
	inline void set_serviceId_9(uint32_t value)
	{
		___serviceId_9 = value;
	}

	inline static int32_t get_offset_of_serviceName_10() { return static_cast<int32_t>(offsetof(CSAISPlayerData_t3122557766, ___serviceName_10)); }
	inline String_t* get_serviceName_10() const { return ___serviceName_10; }
	inline String_t** get_address_of_serviceName_10() { return &___serviceName_10; }
	inline void set_serviceName_10(String_t* value)
	{
		___serviceName_10 = value;
		Il2CppCodeGenWriteBarrier(&___serviceName_10, value);
	}

	inline static int32_t get_offset_of_heroGroups_11() { return static_cast<int32_t>(offsetof(CSAISPlayerData_t3122557766, ___heroGroups_11)); }
	inline CSHeroUnitU5BU5D_t1342235227* get_heroGroups_11() const { return ___heroGroups_11; }
	inline CSHeroUnitU5BU5D_t1342235227** get_address_of_heroGroups_11() { return &___heroGroups_11; }
	inline void set_heroGroups_11(CSHeroUnitU5BU5D_t1342235227* value)
	{
		___heroGroups_11 = value;
		Il2CppCodeGenWriteBarrier(&___heroGroups_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
