﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3080275847MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,Pathfinding.PointNode>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m3160906674(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1671273256 *, Dictionary_2_t44513805 *, const MethodInfo*))KeyCollection__ctor_m2083279176_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1943254628(__this, ___item0, method) ((  void (*) (KeyCollection_t1671273256 *, Int3_t1974045594 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1015896846_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1771840731(__this, method) ((  void (*) (KeyCollection_t1671273256 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1256909829_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3576437482(__this, ___item0, method) ((  bool (*) (KeyCollection_t1671273256 *, Int3_t1974045594 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m177369728_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3400941263(__this, ___item0, method) ((  bool (*) (KeyCollection_t1671273256 *, Int3_t1974045594 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3151594469_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2186242733(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1671273256 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3383333591_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m385784013(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1671273256 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1859776759_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3754082716(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1671273256 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2111535686_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3863642059(__this, method) ((  bool (*) (KeyCollection_t1671273256 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1534675425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3444095549(__this, method) ((  bool (*) (KeyCollection_t1671273256 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m389220307_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3343168175(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1671273256 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3741191109_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,Pathfinding.PointNode>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m2661304935(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1671273256 *, Int3U5BU5D_t516284607*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m259053565_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,Pathfinding.PointNode>::GetEnumerator()
#define KeyCollection_GetEnumerator_m3360230324(__this, method) ((  Enumerator_t659449859  (*) (KeyCollection_t1671273256 *, const MethodInfo*))KeyCollection_GetEnumerator_m1917886282_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,Pathfinding.PointNode>::get_Count()
#define KeyCollection_get_Count_m3624140279(__this, method) ((  int32_t (*) (KeyCollection_t1671273256 *, const MethodInfo*))KeyCollection_get_Count_m3630650253_gshared)(__this, method)
