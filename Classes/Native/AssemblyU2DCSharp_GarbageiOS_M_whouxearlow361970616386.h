﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_whouxearlow36
struct  M_whouxearlow36_t1970616386  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_whouxearlow36::_halpenoNelrou
	int32_t ____halpenoNelrou_0;
	// System.Single GarbageiOS.M_whouxearlow36::_moujer
	float ____moujer_1;
	// System.UInt32 GarbageiOS.M_whouxearlow36::_chera
	uint32_t ____chera_2;
	// System.UInt32 GarbageiOS.M_whouxearlow36::_pomelsoo
	uint32_t ____pomelsoo_3;
	// System.String GarbageiOS.M_whouxearlow36::_sexaiCucear
	String_t* ____sexaiCucear_4;
	// System.Boolean GarbageiOS.M_whouxearlow36::_taichuDrenaisay
	bool ____taichuDrenaisay_5;
	// System.Int32 GarbageiOS.M_whouxearlow36::_libersawJowfousas
	int32_t ____libersawJowfousas_6;
	// System.Single GarbageiOS.M_whouxearlow36::_bersornaXaire
	float ____bersornaXaire_7;
	// System.Int32 GarbageiOS.M_whouxearlow36::_silor
	int32_t ____silor_8;
	// System.UInt32 GarbageiOS.M_whouxearlow36::_coral
	uint32_t ____coral_9;
	// System.Boolean GarbageiOS.M_whouxearlow36::_mebeTrerekou
	bool ____mebeTrerekou_10;
	// System.String GarbageiOS.M_whouxearlow36::_seawall
	String_t* ____seawall_11;
	// System.UInt32 GarbageiOS.M_whouxearlow36::_drouzurCichall
	uint32_t ____drouzurCichall_12;
	// System.Boolean GarbageiOS.M_whouxearlow36::_totuNejall
	bool ____totuNejall_13;
	// System.Boolean GarbageiOS.M_whouxearlow36::_tejarmawDursostair
	bool ____tejarmawDursostair_14;
	// System.Int32 GarbageiOS.M_whouxearlow36::_rapis
	int32_t ____rapis_15;
	// System.UInt32 GarbageiOS.M_whouxearlow36::_hopegasLismu
	uint32_t ____hopegasLismu_16;
	// System.Single GarbageiOS.M_whouxearlow36::_qereleepem
	float ____qereleepem_17;
	// System.UInt32 GarbageiOS.M_whouxearlow36::_rallnereceDelta
	uint32_t ____rallnereceDelta_18;
	// System.Single GarbageiOS.M_whouxearlow36::_cormaiJuci
	float ____cormaiJuci_19;
	// System.String GarbageiOS.M_whouxearlow36::_mawbow
	String_t* ____mawbow_20;
	// System.Boolean GarbageiOS.M_whouxearlow36::_saixeesi
	bool ____saixeesi_21;
	// System.UInt32 GarbageiOS.M_whouxearlow36::_drarjai
	uint32_t ____drarjai_22;
	// System.Boolean GarbageiOS.M_whouxearlow36::_hesow
	bool ____hesow_23;
	// System.Single GarbageiOS.M_whouxearlow36::_nama
	float ____nama_24;
	// System.Boolean GarbageiOS.M_whouxearlow36::_fina
	bool ____fina_25;
	// System.Int32 GarbageiOS.M_whouxearlow36::_kouhe
	int32_t ____kouhe_26;
	// System.String GarbageiOS.M_whouxearlow36::_fasaw
	String_t* ____fasaw_27;
	// System.String GarbageiOS.M_whouxearlow36::_keqa
	String_t* ____keqa_28;
	// System.Int32 GarbageiOS.M_whouxearlow36::_nemstaChersaja
	int32_t ____nemstaChersaja_29;
	// System.Int32 GarbageiOS.M_whouxearlow36::_piswhemlai
	int32_t ____piswhemlai_30;
	// System.String GarbageiOS.M_whouxearlow36::_tomavere
	String_t* ____tomavere_31;
	// System.Single GarbageiOS.M_whouxearlow36::_cichoo
	float ____cichoo_32;
	// System.String GarbageiOS.M_whouxearlow36::_raleecaJowsis
	String_t* ____raleecaJowsis_33;
	// System.Single GarbageiOS.M_whouxearlow36::_mowjas
	float ____mowjas_34;

public:
	inline static int32_t get_offset_of__halpenoNelrou_0() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____halpenoNelrou_0)); }
	inline int32_t get__halpenoNelrou_0() const { return ____halpenoNelrou_0; }
	inline int32_t* get_address_of__halpenoNelrou_0() { return &____halpenoNelrou_0; }
	inline void set__halpenoNelrou_0(int32_t value)
	{
		____halpenoNelrou_0 = value;
	}

	inline static int32_t get_offset_of__moujer_1() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____moujer_1)); }
	inline float get__moujer_1() const { return ____moujer_1; }
	inline float* get_address_of__moujer_1() { return &____moujer_1; }
	inline void set__moujer_1(float value)
	{
		____moujer_1 = value;
	}

	inline static int32_t get_offset_of__chera_2() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____chera_2)); }
	inline uint32_t get__chera_2() const { return ____chera_2; }
	inline uint32_t* get_address_of__chera_2() { return &____chera_2; }
	inline void set__chera_2(uint32_t value)
	{
		____chera_2 = value;
	}

	inline static int32_t get_offset_of__pomelsoo_3() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____pomelsoo_3)); }
	inline uint32_t get__pomelsoo_3() const { return ____pomelsoo_3; }
	inline uint32_t* get_address_of__pomelsoo_3() { return &____pomelsoo_3; }
	inline void set__pomelsoo_3(uint32_t value)
	{
		____pomelsoo_3 = value;
	}

	inline static int32_t get_offset_of__sexaiCucear_4() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____sexaiCucear_4)); }
	inline String_t* get__sexaiCucear_4() const { return ____sexaiCucear_4; }
	inline String_t** get_address_of__sexaiCucear_4() { return &____sexaiCucear_4; }
	inline void set__sexaiCucear_4(String_t* value)
	{
		____sexaiCucear_4 = value;
		Il2CppCodeGenWriteBarrier(&____sexaiCucear_4, value);
	}

	inline static int32_t get_offset_of__taichuDrenaisay_5() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____taichuDrenaisay_5)); }
	inline bool get__taichuDrenaisay_5() const { return ____taichuDrenaisay_5; }
	inline bool* get_address_of__taichuDrenaisay_5() { return &____taichuDrenaisay_5; }
	inline void set__taichuDrenaisay_5(bool value)
	{
		____taichuDrenaisay_5 = value;
	}

	inline static int32_t get_offset_of__libersawJowfousas_6() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____libersawJowfousas_6)); }
	inline int32_t get__libersawJowfousas_6() const { return ____libersawJowfousas_6; }
	inline int32_t* get_address_of__libersawJowfousas_6() { return &____libersawJowfousas_6; }
	inline void set__libersawJowfousas_6(int32_t value)
	{
		____libersawJowfousas_6 = value;
	}

	inline static int32_t get_offset_of__bersornaXaire_7() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____bersornaXaire_7)); }
	inline float get__bersornaXaire_7() const { return ____bersornaXaire_7; }
	inline float* get_address_of__bersornaXaire_7() { return &____bersornaXaire_7; }
	inline void set__bersornaXaire_7(float value)
	{
		____bersornaXaire_7 = value;
	}

	inline static int32_t get_offset_of__silor_8() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____silor_8)); }
	inline int32_t get__silor_8() const { return ____silor_8; }
	inline int32_t* get_address_of__silor_8() { return &____silor_8; }
	inline void set__silor_8(int32_t value)
	{
		____silor_8 = value;
	}

	inline static int32_t get_offset_of__coral_9() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____coral_9)); }
	inline uint32_t get__coral_9() const { return ____coral_9; }
	inline uint32_t* get_address_of__coral_9() { return &____coral_9; }
	inline void set__coral_9(uint32_t value)
	{
		____coral_9 = value;
	}

	inline static int32_t get_offset_of__mebeTrerekou_10() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____mebeTrerekou_10)); }
	inline bool get__mebeTrerekou_10() const { return ____mebeTrerekou_10; }
	inline bool* get_address_of__mebeTrerekou_10() { return &____mebeTrerekou_10; }
	inline void set__mebeTrerekou_10(bool value)
	{
		____mebeTrerekou_10 = value;
	}

	inline static int32_t get_offset_of__seawall_11() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____seawall_11)); }
	inline String_t* get__seawall_11() const { return ____seawall_11; }
	inline String_t** get_address_of__seawall_11() { return &____seawall_11; }
	inline void set__seawall_11(String_t* value)
	{
		____seawall_11 = value;
		Il2CppCodeGenWriteBarrier(&____seawall_11, value);
	}

	inline static int32_t get_offset_of__drouzurCichall_12() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____drouzurCichall_12)); }
	inline uint32_t get__drouzurCichall_12() const { return ____drouzurCichall_12; }
	inline uint32_t* get_address_of__drouzurCichall_12() { return &____drouzurCichall_12; }
	inline void set__drouzurCichall_12(uint32_t value)
	{
		____drouzurCichall_12 = value;
	}

	inline static int32_t get_offset_of__totuNejall_13() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____totuNejall_13)); }
	inline bool get__totuNejall_13() const { return ____totuNejall_13; }
	inline bool* get_address_of__totuNejall_13() { return &____totuNejall_13; }
	inline void set__totuNejall_13(bool value)
	{
		____totuNejall_13 = value;
	}

	inline static int32_t get_offset_of__tejarmawDursostair_14() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____tejarmawDursostair_14)); }
	inline bool get__tejarmawDursostair_14() const { return ____tejarmawDursostair_14; }
	inline bool* get_address_of__tejarmawDursostair_14() { return &____tejarmawDursostair_14; }
	inline void set__tejarmawDursostair_14(bool value)
	{
		____tejarmawDursostair_14 = value;
	}

	inline static int32_t get_offset_of__rapis_15() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____rapis_15)); }
	inline int32_t get__rapis_15() const { return ____rapis_15; }
	inline int32_t* get_address_of__rapis_15() { return &____rapis_15; }
	inline void set__rapis_15(int32_t value)
	{
		____rapis_15 = value;
	}

	inline static int32_t get_offset_of__hopegasLismu_16() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____hopegasLismu_16)); }
	inline uint32_t get__hopegasLismu_16() const { return ____hopegasLismu_16; }
	inline uint32_t* get_address_of__hopegasLismu_16() { return &____hopegasLismu_16; }
	inline void set__hopegasLismu_16(uint32_t value)
	{
		____hopegasLismu_16 = value;
	}

	inline static int32_t get_offset_of__qereleepem_17() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____qereleepem_17)); }
	inline float get__qereleepem_17() const { return ____qereleepem_17; }
	inline float* get_address_of__qereleepem_17() { return &____qereleepem_17; }
	inline void set__qereleepem_17(float value)
	{
		____qereleepem_17 = value;
	}

	inline static int32_t get_offset_of__rallnereceDelta_18() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____rallnereceDelta_18)); }
	inline uint32_t get__rallnereceDelta_18() const { return ____rallnereceDelta_18; }
	inline uint32_t* get_address_of__rallnereceDelta_18() { return &____rallnereceDelta_18; }
	inline void set__rallnereceDelta_18(uint32_t value)
	{
		____rallnereceDelta_18 = value;
	}

	inline static int32_t get_offset_of__cormaiJuci_19() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____cormaiJuci_19)); }
	inline float get__cormaiJuci_19() const { return ____cormaiJuci_19; }
	inline float* get_address_of__cormaiJuci_19() { return &____cormaiJuci_19; }
	inline void set__cormaiJuci_19(float value)
	{
		____cormaiJuci_19 = value;
	}

	inline static int32_t get_offset_of__mawbow_20() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____mawbow_20)); }
	inline String_t* get__mawbow_20() const { return ____mawbow_20; }
	inline String_t** get_address_of__mawbow_20() { return &____mawbow_20; }
	inline void set__mawbow_20(String_t* value)
	{
		____mawbow_20 = value;
		Il2CppCodeGenWriteBarrier(&____mawbow_20, value);
	}

	inline static int32_t get_offset_of__saixeesi_21() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____saixeesi_21)); }
	inline bool get__saixeesi_21() const { return ____saixeesi_21; }
	inline bool* get_address_of__saixeesi_21() { return &____saixeesi_21; }
	inline void set__saixeesi_21(bool value)
	{
		____saixeesi_21 = value;
	}

	inline static int32_t get_offset_of__drarjai_22() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____drarjai_22)); }
	inline uint32_t get__drarjai_22() const { return ____drarjai_22; }
	inline uint32_t* get_address_of__drarjai_22() { return &____drarjai_22; }
	inline void set__drarjai_22(uint32_t value)
	{
		____drarjai_22 = value;
	}

	inline static int32_t get_offset_of__hesow_23() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____hesow_23)); }
	inline bool get__hesow_23() const { return ____hesow_23; }
	inline bool* get_address_of__hesow_23() { return &____hesow_23; }
	inline void set__hesow_23(bool value)
	{
		____hesow_23 = value;
	}

	inline static int32_t get_offset_of__nama_24() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____nama_24)); }
	inline float get__nama_24() const { return ____nama_24; }
	inline float* get_address_of__nama_24() { return &____nama_24; }
	inline void set__nama_24(float value)
	{
		____nama_24 = value;
	}

	inline static int32_t get_offset_of__fina_25() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____fina_25)); }
	inline bool get__fina_25() const { return ____fina_25; }
	inline bool* get_address_of__fina_25() { return &____fina_25; }
	inline void set__fina_25(bool value)
	{
		____fina_25 = value;
	}

	inline static int32_t get_offset_of__kouhe_26() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____kouhe_26)); }
	inline int32_t get__kouhe_26() const { return ____kouhe_26; }
	inline int32_t* get_address_of__kouhe_26() { return &____kouhe_26; }
	inline void set__kouhe_26(int32_t value)
	{
		____kouhe_26 = value;
	}

	inline static int32_t get_offset_of__fasaw_27() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____fasaw_27)); }
	inline String_t* get__fasaw_27() const { return ____fasaw_27; }
	inline String_t** get_address_of__fasaw_27() { return &____fasaw_27; }
	inline void set__fasaw_27(String_t* value)
	{
		____fasaw_27 = value;
		Il2CppCodeGenWriteBarrier(&____fasaw_27, value);
	}

	inline static int32_t get_offset_of__keqa_28() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____keqa_28)); }
	inline String_t* get__keqa_28() const { return ____keqa_28; }
	inline String_t** get_address_of__keqa_28() { return &____keqa_28; }
	inline void set__keqa_28(String_t* value)
	{
		____keqa_28 = value;
		Il2CppCodeGenWriteBarrier(&____keqa_28, value);
	}

	inline static int32_t get_offset_of__nemstaChersaja_29() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____nemstaChersaja_29)); }
	inline int32_t get__nemstaChersaja_29() const { return ____nemstaChersaja_29; }
	inline int32_t* get_address_of__nemstaChersaja_29() { return &____nemstaChersaja_29; }
	inline void set__nemstaChersaja_29(int32_t value)
	{
		____nemstaChersaja_29 = value;
	}

	inline static int32_t get_offset_of__piswhemlai_30() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____piswhemlai_30)); }
	inline int32_t get__piswhemlai_30() const { return ____piswhemlai_30; }
	inline int32_t* get_address_of__piswhemlai_30() { return &____piswhemlai_30; }
	inline void set__piswhemlai_30(int32_t value)
	{
		____piswhemlai_30 = value;
	}

	inline static int32_t get_offset_of__tomavere_31() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____tomavere_31)); }
	inline String_t* get__tomavere_31() const { return ____tomavere_31; }
	inline String_t** get_address_of__tomavere_31() { return &____tomavere_31; }
	inline void set__tomavere_31(String_t* value)
	{
		____tomavere_31 = value;
		Il2CppCodeGenWriteBarrier(&____tomavere_31, value);
	}

	inline static int32_t get_offset_of__cichoo_32() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____cichoo_32)); }
	inline float get__cichoo_32() const { return ____cichoo_32; }
	inline float* get_address_of__cichoo_32() { return &____cichoo_32; }
	inline void set__cichoo_32(float value)
	{
		____cichoo_32 = value;
	}

	inline static int32_t get_offset_of__raleecaJowsis_33() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____raleecaJowsis_33)); }
	inline String_t* get__raleecaJowsis_33() const { return ____raleecaJowsis_33; }
	inline String_t** get_address_of__raleecaJowsis_33() { return &____raleecaJowsis_33; }
	inline void set__raleecaJowsis_33(String_t* value)
	{
		____raleecaJowsis_33 = value;
		Il2CppCodeGenWriteBarrier(&____raleecaJowsis_33, value);
	}

	inline static int32_t get_offset_of__mowjas_34() { return static_cast<int32_t>(offsetof(M_whouxearlow36_t1970616386, ____mowjas_34)); }
	inline float get__mowjas_34() const { return ____mowjas_34; }
	inline float* get_address_of__mowjas_34() { return &____mowjas_34; }
	inline void set__mowjas_34(float value)
	{
		____mowjas_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
