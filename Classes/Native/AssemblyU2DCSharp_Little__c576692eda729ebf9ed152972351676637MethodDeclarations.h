﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._c576692eda729ebf9ed15297ba410467
struct _c576692eda729ebf9ed15297ba410467_t2351676637;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__c576692eda729ebf9ed152972351676637.h"

// System.Void Little._c576692eda729ebf9ed15297ba410467::.ctor()
extern "C"  void _c576692eda729ebf9ed15297ba410467__ctor_m2188481008 (_c576692eda729ebf9ed15297ba410467_t2351676637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c576692eda729ebf9ed15297ba410467::_c576692eda729ebf9ed15297ba410467m2(System.Int32)
extern "C"  int32_t _c576692eda729ebf9ed15297ba410467__c576692eda729ebf9ed15297ba410467m2_m1739029625 (_c576692eda729ebf9ed15297ba410467_t2351676637 * __this, int32_t ____c576692eda729ebf9ed15297ba410467a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c576692eda729ebf9ed15297ba410467::_c576692eda729ebf9ed15297ba410467m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _c576692eda729ebf9ed15297ba410467__c576692eda729ebf9ed15297ba410467m_m439428701 (_c576692eda729ebf9ed15297ba410467_t2351676637 * __this, int32_t ____c576692eda729ebf9ed15297ba410467a0, int32_t ____c576692eda729ebf9ed15297ba41046741, int32_t ____c576692eda729ebf9ed15297ba410467c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c576692eda729ebf9ed15297ba410467::ilo__c576692eda729ebf9ed15297ba410467m21(Little._c576692eda729ebf9ed15297ba410467,System.Int32)
extern "C"  int32_t _c576692eda729ebf9ed15297ba410467_ilo__c576692eda729ebf9ed15297ba410467m21_m4168160580 (Il2CppObject * __this /* static, unused */, _c576692eda729ebf9ed15297ba410467_t2351676637 * ____this0, int32_t ____c576692eda729ebf9ed15297ba410467a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
