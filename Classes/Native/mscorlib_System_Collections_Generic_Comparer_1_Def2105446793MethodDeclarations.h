﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.Voxels.VoxelContour>
struct DefaultComparer_t2105446793;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_VoxelContour3597201016.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.Voxels.VoxelContour>::.ctor()
extern "C"  void DefaultComparer__ctor_m1019252527_gshared (DefaultComparer_t2105446793 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1019252527(__this, method) ((  void (*) (DefaultComparer_t2105446793 *, const MethodInfo*))DefaultComparer__ctor_m1019252527_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.Voxels.VoxelContour>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1364407136_gshared (DefaultComparer_t2105446793 * __this, VoxelContour_t3597201016  ___x0, VoxelContour_t3597201016  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m1364407136(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t2105446793 *, VoxelContour_t3597201016 , VoxelContour_t3597201016 , const MethodInfo*))DefaultComparer_Compare_m1364407136_gshared)(__this, ___x0, ___y1, method)
