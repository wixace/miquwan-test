﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ResourcesGenerated
struct UnityEngine_ResourcesGenerated_t700159058;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_ResourcesGenerated::.ctor()
extern "C"  void UnityEngine_ResourcesGenerated__ctor_m2894840969 (UnityEngine_ResourcesGenerated_t700159058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ResourcesGenerated::.cctor()
extern "C"  void UnityEngine_ResourcesGenerated__cctor_m3358627908 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ResourcesGenerated::Resources_Resources1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ResourcesGenerated_Resources_Resources1_m1456054533 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ResourcesGenerated::Resources_FindObjectsOfTypeAll__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ResourcesGenerated_Resources_FindObjectsOfTypeAll__Type_m188550376 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ResourcesGenerated::Resources_FindObjectsOfTypeAllT1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ResourcesGenerated_Resources_FindObjectsOfTypeAllT1_m1956135499 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ResourcesGenerated::Resources_GetBuiltinResource__Type__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ResourcesGenerated_Resources_GetBuiltinResource__Type__String_m1165113855 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ResourcesGenerated::Resources_GetBuiltinResourceT1__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ResourcesGenerated_Resources_GetBuiltinResourceT1__String_m1519759202 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ResourcesGenerated::Resources_Load__String__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ResourcesGenerated_Resources_Load__String__Type_m2107431306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ResourcesGenerated::Resources_Load__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ResourcesGenerated_Resources_Load__String_m284392720 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ResourcesGenerated::Resources_LoadT1__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ResourcesGenerated_Resources_LoadT1__String_m868209453 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ResourcesGenerated::Resources_LoadAll__String__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ResourcesGenerated_Resources_LoadAll__String__Type_m3768008559 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ResourcesGenerated::Resources_LoadAll__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ResourcesGenerated_Resources_LoadAll__String_m3247796917 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ResourcesGenerated::Resources_LoadAllT1__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ResourcesGenerated_Resources_LoadAllT1__String_m1136325522 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ResourcesGenerated::Resources_LoadAsync__String__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ResourcesGenerated_Resources_LoadAsync__String__Type_m2338800202 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ResourcesGenerated::Resources_LoadAsync__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ResourcesGenerated_Resources_LoadAsync__String_m1038841296 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ResourcesGenerated::Resources_LoadAsyncT1__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ResourcesGenerated_Resources_LoadAsyncT1__String_m43817965 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ResourcesGenerated::Resources_UnloadAsset__UEObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ResourcesGenerated_Resources_UnloadAsset__UEObject_m1788261769 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ResourcesGenerated::Resources_UnloadUnusedAssets(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ResourcesGenerated_Resources_UnloadUnusedAssets_m1260155633 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ResourcesGenerated::__Register()
extern "C"  void UnityEngine_ResourcesGenerated___Register_m932718046 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ResourcesGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_ResourcesGenerated_ilo_attachFinalizerObject1_m1093459318 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_ResourcesGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_ResourcesGenerated_ilo_getObject2_m2710348935 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ResourcesGenerated::ilo_moveSaveID2Arr3(System.Int32)
extern "C"  void UnityEngine_ResourcesGenerated_ilo_moveSaveID2Arr3_m1995111904 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ResourcesGenerated::ilo_setArrayS4(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_ResourcesGenerated_ilo_setArrayS4_m3916480477 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ResourcesGenerated::ilo_setWhatever5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  void UnityEngine_ResourcesGenerated_ilo_setWhatever5_m748033364 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_ResourcesGenerated::ilo_getStringS6(System.Int32)
extern "C"  String_t* UnityEngine_ResourcesGenerated_ilo_getStringS6_m3304062220 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ResourcesGenerated::ilo_setObject7(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_ResourcesGenerated_ilo_setObject7_m1214875303 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
