﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimationPoint
struct AnimationPoint_t1831839884;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AnimationPoint1831839884.h"

// System.Void AnimationPoint::.ctor()
extern "C"  void AnimationPoint__ctor_m674038159 (AnimationPoint_t1831839884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationPoint::ToObject(AnimationPoint)
extern "C"  void AnimationPoint_ToObject_m375972195 (AnimationPoint_t1831839884 * __this, AnimationPoint_t1831839884 * ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationPoint::ToJson(AnimationPoint)
extern "C"  void AnimationPoint_ToJson_m1201535180 (AnimationPoint_t1831839884 * __this, AnimationPoint_t1831839884 * ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
