﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.SoftJointLimitSpring
struct SoftJointLimitSpring_t4258895628;
struct SoftJointLimitSpring_t4258895628_marshaled_pinvoke;
struct SoftJointLimitSpring_t4258895628_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct SoftJointLimitSpring_t4258895628;
struct SoftJointLimitSpring_t4258895628_marshaled_pinvoke;

extern "C" void SoftJointLimitSpring_t4258895628_marshal_pinvoke(const SoftJointLimitSpring_t4258895628& unmarshaled, SoftJointLimitSpring_t4258895628_marshaled_pinvoke& marshaled);
extern "C" void SoftJointLimitSpring_t4258895628_marshal_pinvoke_back(const SoftJointLimitSpring_t4258895628_marshaled_pinvoke& marshaled, SoftJointLimitSpring_t4258895628& unmarshaled);
extern "C" void SoftJointLimitSpring_t4258895628_marshal_pinvoke_cleanup(SoftJointLimitSpring_t4258895628_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct SoftJointLimitSpring_t4258895628;
struct SoftJointLimitSpring_t4258895628_marshaled_com;

extern "C" void SoftJointLimitSpring_t4258895628_marshal_com(const SoftJointLimitSpring_t4258895628& unmarshaled, SoftJointLimitSpring_t4258895628_marshaled_com& marshaled);
extern "C" void SoftJointLimitSpring_t4258895628_marshal_com_back(const SoftJointLimitSpring_t4258895628_marshaled_com& marshaled, SoftJointLimitSpring_t4258895628& unmarshaled);
extern "C" void SoftJointLimitSpring_t4258895628_marshal_com_cleanup(SoftJointLimitSpring_t4258895628_marshaled_com& marshaled);
