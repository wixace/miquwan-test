﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>
struct ValueCollection_t768230707;
// System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>
struct SortedDictionary_2_t3586408994;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3065703549;
// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "System_System_Collections_Generic_SortedDictionary3450288660.h"

// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m929790650_gshared (ValueCollection_t768230707 * __this, SortedDictionary_2_t3586408994 * ___dic0, const MethodInfo* method);
#define ValueCollection__ctor_m929790650(__this, ___dic0, method) ((  void (*) (ValueCollection_t768230707 *, SortedDictionary_2_t3586408994 *, const MethodInfo*))ValueCollection__ctor_m929790650_gshared)(__this, ___dic0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m671582325_gshared (ValueCollection_t768230707 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m671582325(__this, ___item0, method) ((  void (*) (ValueCollection_t768230707 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m671582325_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4195743678_gshared (ValueCollection_t768230707 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4195743678(__this, method) ((  void (*) (ValueCollection_t768230707 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4195743678_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1118411453_gshared (ValueCollection_t768230707 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1118411453(__this, ___item0, method) ((  bool (*) (ValueCollection_t768230707 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1118411453_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3688554608_gshared (ValueCollection_t768230707 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3688554608(__this, method) ((  bool (*) (ValueCollection_t768230707 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3688554608_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2545172130_gshared (ValueCollection_t768230707 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2545172130(__this, ___item0, method) ((  bool (*) (ValueCollection_t768230707 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2545172130_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3096401504_gshared (ValueCollection_t768230707 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3096401504(__this, method) ((  Il2CppObject* (*) (ValueCollection_t768230707 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3096401504_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2587919170_gshared (ValueCollection_t768230707 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m2587919170(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t768230707 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2587919170_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3503967824_gshared (ValueCollection_t768230707 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3503967824(__this, method) ((  bool (*) (ValueCollection_t768230707 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3503967824_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2692445392_gshared (ValueCollection_t768230707 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2692445392(__this, method) ((  Il2CppObject * (*) (ValueCollection_t768230707 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2692445392_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2733840465_gshared (ValueCollection_t768230707 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2733840465(__this, method) ((  Il2CppObject * (*) (ValueCollection_t768230707 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2733840465_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m452141348_gshared (ValueCollection_t768230707 * __this, Int32U5BU5D_t3230847821* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define ValueCollection_CopyTo_m452141348(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ValueCollection_t768230707 *, Int32U5BU5D_t3230847821*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m452141348_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m4154846486_gshared (ValueCollection_t768230707 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m4154846486(__this, method) ((  int32_t (*) (ValueCollection_t768230707 *, const MethodInfo*))ValueCollection_get_Count_m4154846486_gshared)(__this, method)
// System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Int32,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t3450288660  ValueCollection_GetEnumerator_m3591908324_gshared (ValueCollection_t768230707 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m3591908324(__this, method) ((  Enumerator_t3450288660  (*) (ValueCollection_t768230707 *, const MethodInfo*))ValueCollection_GetEnumerator_m3591908324_gshared)(__this, method)
