﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_SoftJointLimitGenerated
struct UnityEngine_SoftJointLimitGenerated_t2746395724;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_SoftJointLimitGenerated::.ctor()
extern "C"  void UnityEngine_SoftJointLimitGenerated__ctor_m3390549151 (UnityEngine_SoftJointLimitGenerated_t2746395724 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SoftJointLimitGenerated::.cctor()
extern "C"  void UnityEngine_SoftJointLimitGenerated__cctor_m1545712366 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SoftJointLimitGenerated::SoftJointLimit_SoftJointLimit1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SoftJointLimitGenerated_SoftJointLimit_SoftJointLimit1_m1458682707 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SoftJointLimitGenerated::SoftJointLimit_limit(JSVCall)
extern "C"  void UnityEngine_SoftJointLimitGenerated_SoftJointLimit_limit_m1998730123 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SoftJointLimitGenerated::SoftJointLimit_bounciness(JSVCall)
extern "C"  void UnityEngine_SoftJointLimitGenerated_SoftJointLimit_bounciness_m2613096099 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SoftJointLimitGenerated::SoftJointLimit_contactDistance(JSVCall)
extern "C"  void UnityEngine_SoftJointLimitGenerated_SoftJointLimit_contactDistance_m3219242065 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SoftJointLimitGenerated::__Register()
extern "C"  void UnityEngine_SoftJointLimitGenerated___Register_m3804748680 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SoftJointLimitGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_SoftJointLimitGenerated_ilo_getObject1_m3870470647 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SoftJointLimitGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_SoftJointLimitGenerated_ilo_addJSCSRel2_m952992860 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SoftJointLimitGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_SoftJointLimitGenerated_ilo_setSingle3_m1190377591 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_SoftJointLimitGenerated::ilo_getSingle4(System.Int32)
extern "C"  float UnityEngine_SoftJointLimitGenerated_ilo_getSingle4_m1209141651 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
