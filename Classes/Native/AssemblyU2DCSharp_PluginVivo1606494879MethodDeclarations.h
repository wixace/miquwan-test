﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginVivo
struct PluginVivo_t1606494879;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionInfo
struct VersionInfo_t2356638086;
// VersionMgr
struct VersionMgr_t1322950208;
// System.Object
struct Il2CppObject;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_PluginVivo1606494879.h"

// System.Void PluginVivo::.ctor()
extern "C"  void PluginVivo__ctor_m727118044 (PluginVivo_t1606494879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginVivo::Init()
extern "C"  void PluginVivo_Init_m2468844024 (PluginVivo_t1606494879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginVivo::OnDestory()
extern "C"  void PluginVivo_OnDestory_m1646530159 (PluginVivo_t1606494879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginVivo::Clear()
extern "C"  void PluginVivo_Clear_m2428218631 (PluginVivo_t1606494879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginVivo::RoleEnterGame(CEvent.ZEvent)
extern "C"  void PluginVivo_RoleEnterGame_m3074409517 (PluginVivo_t1606494879 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginVivo::UserPay(CEvent.ZEvent)
extern "C"  void PluginVivo_UserPay_m2934328452 (PluginVivo_t1606494879 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginVivo::GetChannelIdSuccess(System.String)
extern "C"  void PluginVivo_GetChannelIdSuccess_m1756651917 (PluginVivo_t1606494879 * __this, String_t* ___channelId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginVivo::onVivoAccountLogin(System.String)
extern "C"  void PluginVivo_onVivoAccountLogin_m260752531 (PluginVivo_t1606494879 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginVivo::OnPayVivoSuccess(System.String)
extern "C"  void PluginVivo_OnPayVivoSuccess_m2544713772 (PluginVivo_t1606494879 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginVivo::OnPayVivoFail(System.String)
extern "C"  void PluginVivo_OnPayVivoFail_m2161639573 (PluginVivo_t1606494879 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginVivo::OnExitGameSuccess(System.String)
extern "C"  void PluginVivo_OnExitGameSuccess_m1383551604 (PluginVivo_t1606494879 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginVivo::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginVivo_ReqSDKHttpLogin_m2074751665 (PluginVivo_t1606494879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginVivo::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginVivo_IsLoginSuccess_m2664756215 (PluginVivo_t1606494879 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginVivo::LoginVivoMgr()
extern "C"  void PluginVivo_LoginVivoMgr_m2611359563 (PluginVivo_t1606494879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginVivo::PlayVivoMgr(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.Int32,System.String,System.String,System.String)
extern "C"  void PluginVivo_PlayVivoMgr_m537595799 (PluginVivo_t1606494879 * __this, String_t* ___price0, String_t* ___roleId1, String_t* ___roleName2, String_t* ___roleLevel3, String_t* ___serverId4, String_t* ___serverName5, String_t* ___productId6, String_t* ___productName7, int32_t ___buyNum8, String_t* ___exts9, String_t* ___productDes10, String_t* ___notifyUrl11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginVivo::EnterGameVivoMgr(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginVivo_EnterGameVivoMgr_m4108272840 (PluginVivo_t1606494879 * __this, String_t* ___roleId0, String_t* ___roleName1, String_t* ___roleLevel2, String_t* ___serverId3, String_t* ___serverName4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginVivo::<OnExitGameSuccess>m__460()
extern "C"  void PluginVivo_U3COnExitGameSuccessU3Em__460_m206379873 (PluginVivo_t1606494879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginVivo::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginVivo_ilo_AddEventListener1_m205411480 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginVivo::ilo_get_currentVS2(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginVivo_ilo_get_currentVS2_m849969293 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginVivo::ilo_Log3(System.Object,System.Boolean)
extern "C"  void PluginVivo_ilo_Log3_m1725557619 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginVivo::ilo_ReqSDKHttpLogin4(PluginsSdkMgr)
extern "C"  void PluginVivo_ilo_ReqSDKHttpLogin4_m2876980106 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginVivo::ilo_get_Instance5()
extern "C"  VersionMgr_t1322950208 * PluginVivo_ilo_get_Instance5_m1497872353 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginVivo::ilo_LoginVivoMgr6(PluginVivo)
extern "C"  void PluginVivo_ilo_LoginVivoMgr6_m2926485403 (Il2CppObject * __this /* static, unused */, PluginVivo_t1606494879 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
