﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<PushType>
struct Collection_1_t1026100610;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// PushType[]
struct PushTypeU5BU5D_t3978570141;
// System.Collections.Generic.IEnumerator`1<PushType>
struct IEnumerator_1_t3752507501;
// System.Collections.Generic.IList`1<PushType>
struct IList_1_t240322359;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PushType1840642452.h"

// System.Void System.Collections.ObjectModel.Collection`1<PushType>::.ctor()
extern "C"  void Collection_1__ctor_m3645228699_gshared (Collection_1_t1026100610 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3645228699(__this, method) ((  void (*) (Collection_1_t1026100610 *, const MethodInfo*))Collection_1__ctor_m3645228699_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PushType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4036374332_gshared (Collection_1_t1026100610 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4036374332(__this, method) ((  bool (*) (Collection_1_t1026100610 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4036374332_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<PushType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m18290441_gshared (Collection_1_t1026100610 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m18290441(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1026100610 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m18290441_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<PushType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m3637086424_gshared (Collection_1_t1026100610 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m3637086424(__this, method) ((  Il2CppObject * (*) (Collection_1_t1026100610 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m3637086424_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<PushType>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2800307941_gshared (Collection_1_t1026100610 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m2800307941(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1026100610 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m2800307941_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PushType>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m402259195_gshared (Collection_1_t1026100610 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m402259195(__this, ___value0, method) ((  bool (*) (Collection_1_t1026100610 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m402259195_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<PushType>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m292391613_gshared (Collection_1_t1026100610 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m292391613(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1026100610 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m292391613_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PushType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2190338992_gshared (Collection_1_t1026100610 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2190338992(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1026100610 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2190338992_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<PushType>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3953164152_gshared (Collection_1_t1026100610 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3953164152(__this, ___value0, method) ((  void (*) (Collection_1_t1026100610 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3953164152_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PushType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m739898497_gshared (Collection_1_t1026100610 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m739898497(__this, method) ((  bool (*) (Collection_1_t1026100610 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m739898497_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<PushType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m119807731_gshared (Collection_1_t1026100610 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m119807731(__this, method) ((  Il2CppObject * (*) (Collection_1_t1026100610 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m119807731_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PushType>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m613794986_gshared (Collection_1_t1026100610 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m613794986(__this, method) ((  bool (*) (Collection_1_t1026100610 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m613794986_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PushType>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m4214586639_gshared (Collection_1_t1026100610 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m4214586639(__this, method) ((  bool (*) (Collection_1_t1026100610 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m4214586639_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<PushType>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m236724154_gshared (Collection_1_t1026100610 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m236724154(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1026100610 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m236724154_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PushType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m995748743_gshared (Collection_1_t1026100610 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m995748743(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1026100610 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m995748743_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<PushType>::Add(T)
extern "C"  void Collection_1_Add_m1493203588_gshared (Collection_1_t1026100610 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m1493203588(__this, ___item0, method) ((  void (*) (Collection_1_t1026100610 *, int32_t, const MethodInfo*))Collection_1_Add_m1493203588_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PushType>::Clear()
extern "C"  void Collection_1_Clear_m1051361990_gshared (Collection_1_t1026100610 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1051361990(__this, method) ((  void (*) (Collection_1_t1026100610 *, const MethodInfo*))Collection_1_Clear_m1051361990_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<PushType>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3672238588_gshared (Collection_1_t1026100610 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3672238588(__this, method) ((  void (*) (Collection_1_t1026100610 *, const MethodInfo*))Collection_1_ClearItems_m3672238588_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PushType>::Contains(T)
extern "C"  bool Collection_1_Contains_m2863670072_gshared (Collection_1_t1026100610 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m2863670072(__this, ___item0, method) ((  bool (*) (Collection_1_t1026100610 *, int32_t, const MethodInfo*))Collection_1_Contains_m2863670072_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PushType>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m613828980_gshared (Collection_1_t1026100610 * __this, PushTypeU5BU5D_t3978570141* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m613828980(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1026100610 *, PushTypeU5BU5D_t3978570141*, int32_t, const MethodInfo*))Collection_1_CopyTo_m613828980_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<PushType>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m4078921487_gshared (Collection_1_t1026100610 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m4078921487(__this, method) ((  Il2CppObject* (*) (Collection_1_t1026100610 *, const MethodInfo*))Collection_1_GetEnumerator_m4078921487_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<PushType>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m3941224256_gshared (Collection_1_t1026100610 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m3941224256(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1026100610 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m3941224256_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PushType>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m576728299_gshared (Collection_1_t1026100610 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m576728299(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1026100610 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m576728299_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<PushType>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m3440438686_gshared (Collection_1_t1026100610 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m3440438686(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1026100610 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m3440438686_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<PushType>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m829375750_gshared (Collection_1_t1026100610 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m829375750(__this, method) ((  Il2CppObject* (*) (Collection_1_t1026100610 *, const MethodInfo*))Collection_1_get_Items_m829375750_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PushType>::Remove(T)
extern "C"  bool Collection_1_Remove_m3649327411_gshared (Collection_1_t1026100610 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m3649327411(__this, ___item0, method) ((  bool (*) (Collection_1_t1026100610 *, int32_t, const MethodInfo*))Collection_1_Remove_m3649327411_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PushType>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m2745548465_gshared (Collection_1_t1026100610 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m2745548465(__this, ___index0, method) ((  void (*) (Collection_1_t1026100610 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m2745548465_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PushType>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m4261780113_gshared (Collection_1_t1026100610 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m4261780113(__this, ___index0, method) ((  void (*) (Collection_1_t1026100610 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m4261780113_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<PushType>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1736175163_gshared (Collection_1_t1026100610 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1736175163(__this, method) ((  int32_t (*) (Collection_1_t1026100610 *, const MethodInfo*))Collection_1_get_Count_m1736175163_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<PushType>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m17117207_gshared (Collection_1_t1026100610 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m17117207(__this, ___index0, method) ((  int32_t (*) (Collection_1_t1026100610 *, int32_t, const MethodInfo*))Collection_1_get_Item_m17117207_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PushType>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m1994710786_gshared (Collection_1_t1026100610 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m1994710786(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1026100610 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m1994710786_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<PushType>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2015297847_gshared (Collection_1_t1026100610 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2015297847(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1026100610 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m2015297847_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PushType>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2613999604_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2613999604(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2613999604_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<PushType>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m4016765814_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m4016765814(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m4016765814_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PushType>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m596532532_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m596532532(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m596532532_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PushType>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m2889937488_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m2889937488(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2889937488_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PushType>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m134936143_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m134936143(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m134936143_gshared)(__this /* static, unused */, ___list0, method)
