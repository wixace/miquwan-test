﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIToggledComponents
struct UIToggledComponents_t3531934354;
// EventDelegate
struct EventDelegate_t4004424223;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t1077642479;
// EventDelegate/Callback
struct Callback_t1094463061;
// UIToggle
struct UIToggle_t688812808;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_UIToggle688812808.h"

// System.Void UIToggledComponents::.ctor()
extern "C"  void UIToggledComponents__ctor_m92720409 (UIToggledComponents_t3531934354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggledComponents::Awake()
extern "C"  void UIToggledComponents_Awake_m330325628 (UIToggledComponents_t3531934354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggledComponents::Toggle()
extern "C"  void UIToggledComponents_Toggle_m2062071007 (UIToggledComponents_t3531934354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate UIToggledComponents::ilo_Add1(System.Collections.Generic.List`1<EventDelegate>,EventDelegate/Callback)
extern "C"  EventDelegate_t4004424223 * UIToggledComponents_ilo_Add1_m1396983796 (Il2CppObject * __this /* static, unused */, List_1_t1077642479 * ___list0, Callback_t1094463061 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggledComponents::ilo_get_value2(UIToggle)
extern "C"  bool UIToggledComponents_ilo_get_value2_m895199820 (Il2CppObject * __this /* static, unused */, UIToggle_t688812808 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
