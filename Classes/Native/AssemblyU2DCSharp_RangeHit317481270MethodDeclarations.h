﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CombatEntity
struct CombatEntity_t684137495;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// GameMgr
struct GameMgr_t1469029542;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitType643359572.h"

// System.Void RangeHit::CircleHit(UnityEngine.Vector3,System.Single,CombatEntity,System.Collections.Generic.List`1<CombatEntity>,System.Boolean)
extern "C"  void RangeHit_CircleHit_m296160067 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___centre0, float ___radius1, CombatEntity_t684137495 * ___entity2, List_1_t2052323047 * ___hitList3, bool ___iswish4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RangeHit::SectorHit(System.Single,System.Single,CombatEntity,System.Collections.Generic.List`1<CombatEntity>,System.Boolean)
extern "C"  void RangeHit_SectorHit_m2705188779 (Il2CppObject * __this /* static, unused */, float ___radius0, float ___angle1, CombatEntity_t684137495 * ___entity2, List_1_t2052323047 * ___hitList3, bool ___iswish4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RangeHit::RectHit(System.Single,System.Single,CombatEntity,System.Collections.Generic.List`1<CombatEntity>,System.Boolean)
extern "C"  void RangeHit_RectHit_m1940110505 (Il2CppObject * __this /* static, unused */, float ___length0, float ___width1, CombatEntity_t684137495 * ___entity2, List_1_t2052323047 * ___hitList3, bool ___iswish4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RangeHit::CrossHit(System.Single,System.Single,System.Single,CombatEntity,System.Collections.Generic.List`1<CombatEntity>,System.Boolean)
extern "C"  void RangeHit_CrossHit_m134961070 (Il2CppObject * __this /* static, unused */, float ___radius0, float ___length1, float ___width2, CombatEntity_t684137495 * ___entity3, List_1_t2052323047 * ___hitList4, bool ___iswish5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RangeHit::EquantHit(System.Single,System.Single,System.Single,CombatEntity,System.Collections.Generic.List`1<CombatEntity>,System.Boolean)
extern "C"  void RangeHit_EquantHit_m2032211368 (Il2CppObject * __this /* static, unused */, float ___radius0, float ___length1, float ___width2, CombatEntity_t684137495 * ___entity3, List_1_t2052323047 * ___hitList4, bool ___iswish5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RangeHit::ilo_get_isNotSelected1(CombatEntity)
extern "C"  bool RangeHit_ilo_get_isNotSelected1_m3931403717 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single RangeHit::ilo_get_halfwidth2(CombatEntity)
extern "C"  float RangeHit_ilo_get_halfwidth2_m268771535 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> RangeHit::ilo_GetAllAlly3(GameMgr,CombatEntity)
extern "C"  List_1_t2052323047 * RangeHit_ilo_GetAllAlly3_m267941337 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, CombatEntity_t684137495 * ___entity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitType RangeHit::ilo_get_unitType4(CombatEntity)
extern "C"  int32_t RangeHit_ilo_get_unitType4_m3900941211 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 RangeHit::ilo_get_position5(CombatEntity)
extern "C"  Vector3_t4282066566  RangeHit_ilo_get_position5_m3349093154 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
