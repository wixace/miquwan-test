﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginSanXiang
struct PluginSanXiang_t38647228;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// VersionInfo
struct VersionInfo_t2356638086;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// Mihua.SDK.EnterGameInfo
struct EnterGameInfo_t1897532954;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_PluginSanXiang38647228.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"

// System.Void PluginSanXiang::.ctor()
extern "C"  void PluginSanXiang__ctor_m4250061407 (PluginSanXiang_t38647228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::Init()
extern "C"  void PluginSanXiang_Init_m1197014037 (PluginSanXiang_t38647228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::GameLoginSuccess(CEvent.ZEvent)
extern "C"  void PluginSanXiang_GameLoginSuccess_m3493147818 (PluginSanXiang_t38647228 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::JsonParse(System.String)
extern "C"  void PluginSanXiang_JsonParse_m44205722 (PluginSanXiang_t38647228 * __this, String_t* ___arg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginSanXiang::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginSanXiang_ReqSDKHttpLogin_m3595668340 (PluginSanXiang_t38647228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginSanXiang::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginSanXiang_IsLoginSuccess_m2338004244 (PluginSanXiang_t38647228 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::OpenUserLogin()
extern "C"  void PluginSanXiang_OpenUserLogin_m3389463985 (PluginSanXiang_t38647228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::UserPay(CEvent.ZEvent)
extern "C"  void PluginSanXiang_UserPay_m1514265761 (PluginSanXiang_t38647228 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginSanXiang_SignCallBack_m3532748986 (PluginSanXiang_t38647228 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginSanXiang::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginSanXiang_BuildOrderParam2WWWForm_m446069218 (PluginSanXiang_t38647228 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::CreateRole(CEvent.ZEvent)
extern "C"  void PluginSanXiang_CreateRole_m3584913092 (PluginSanXiang_t38647228 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::EnterGame(CEvent.ZEvent)
extern "C"  void PluginSanXiang_EnterGame_m1432029684 (PluginSanXiang_t38647228 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginSanXiang_RoleUpgrade_m2126454680 (PluginSanXiang_t38647228 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::OnLoginSuccess(System.String)
extern "C"  void PluginSanXiang_OnLoginSuccess_m2881822756 (PluginSanXiang_t38647228 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::OnLogoutSuccess(System.String)
extern "C"  void PluginSanXiang_OnLogoutSuccess_m3440702187 (PluginSanXiang_t38647228 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::OnLogout(System.String)
extern "C"  void PluginSanXiang_OnLogout_m1436487348 (PluginSanXiang_t38647228 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::initSdk(System.String,System.String,System.String,System.String)
extern "C"  void PluginSanXiang_initSdk_m2255789871 (PluginSanXiang_t38647228 * __this, String_t* ___appid0, String_t* ___key1, String_t* ___gameversion2, String_t* ___gameid3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::login()
extern "C"  void PluginSanXiang_login_m3772076230 (PluginSanXiang_t38647228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::logout()
extern "C"  void PluginSanXiang_logout_m976068847 (PluginSanXiang_t38647228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::creatRole(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginSanXiang_creatRole_m3219971438 (PluginSanXiang_t38647228 * __this, String_t* ___openuid0, String_t* ___roleId1, String_t* ___roleName2, String_t* ___gameid3, String_t* ___serverId4, String_t* ___serverName5, String_t* ___roleLv6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::roleUpgrade(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginSanXiang_roleUpgrade_m4258044647 (PluginSanXiang_t38647228 * __this, String_t* ___openuid0, String_t* ___roleId1, String_t* ___roleName2, String_t* ___gameid3, String_t* ___serverId4, String_t* ___serverName5, String_t* ___roleLv6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::pay(System.String)
extern "C"  void PluginSanXiang_pay_m1498767837 (PluginSanXiang_t38647228 * __this, String_t* ___paydata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::<OnLogoutSuccess>m__452()
extern "C"  void PluginSanXiang_U3COnLogoutSuccessU3Em__452_m2421055905 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::<OnLogout>m__453()
extern "C"  void PluginSanXiang_U3COnLogoutU3Em__453_m303827075 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginSanXiang_ilo_AddEventListener1_m3469760181 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginSanXiang::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginSanXiang_ilo_get_Instance2_m773853217 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::ilo_Log3(System.Object,System.Boolean)
extern "C"  void PluginSanXiang_ilo_Log3_m2891162000 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken PluginSanXiang::ilo_get_Item4(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * PluginSanXiang_ilo_get_Item4_m3837359683 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginSanXiang::ilo_get_isSdkLogin5(VersionMgr)
extern "C"  bool PluginSanXiang_ilo_get_isSdkLogin5_m2416281833 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginSanXiang::ilo_get_PluginsSdkMgr6()
extern "C"  PluginsSdkMgr_t3884624670 * PluginSanXiang_ilo_get_PluginsSdkMgr6_m46629294 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginSanXiang::ilo_get_DeviceID7(PluginsSdkMgr)
extern "C"  String_t* PluginSanXiang_ilo_get_DeviceID7_m970906480 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::ilo_login8(PluginSanXiang)
extern "C"  void PluginSanXiang_ilo_login8_m649339531 (Il2CppObject * __this /* static, unused */, PluginSanXiang_t38647228 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginSanXiang::ilo_get_currentVS9(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginSanXiang_ilo_get_currentVS9_m3649618609 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginSanXiang::ilo_GetProductID10(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginSanXiang_ilo_GetProductID10_m496411410 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.EnterGameInfo PluginSanXiang::ilo_Parse11(System.Object[])
extern "C"  EnterGameInfo_t1897532954 * PluginSanXiang_ilo_Parse11_m1718592858 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::ilo_creatRole12(PluginSanXiang,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginSanXiang_ilo_creatRole12_m4099023218 (Il2CppObject * __this /* static, unused */, PluginSanXiang_t38647228 * ____this0, String_t* ___openuid1, String_t* ___roleId2, String_t* ___roleName3, String_t* ___gameid4, String_t* ___serverId5, String_t* ___serverName6, String_t* ___roleLv7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginSanXiang::ilo_ReqSDKHttpLogin13(PluginsSdkMgr)
extern "C"  void PluginSanXiang_ilo_ReqSDKHttpLogin13_m829730819 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
