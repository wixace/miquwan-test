﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DelayDestroy
struct DelayDestroy_t3754862871;
// MonoBehaviourEx
struct MonoBehaviourEx_t1076555597;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoBehaviourEx1076555597.h"

// System.Void DelayDestroy::.ctor()
extern "C"  void DelayDestroy__ctor_m4261450596 (DelayDestroy_t3754862871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DelayDestroy::Start()
extern "C"  void DelayDestroy_Start_m3208588388 (DelayDestroy_t3754862871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DelayDestroy::Update()
extern "C"  void DelayDestroy_Update_m687844393 (DelayDestroy_t3754862871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DelayDestroy::ilo_get_time1(MonoBehaviourEx)
extern "C"  float DelayDestroy_ilo_get_time1_m3673876903 (Il2CppObject * __this /* static, unused */, MonoBehaviourEx_t1076555597 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
