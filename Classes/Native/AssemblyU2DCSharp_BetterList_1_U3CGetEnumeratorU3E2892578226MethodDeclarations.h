﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator29<UICamera/DepthEntry>
struct U3CGetEnumeratorU3Ec__Iterator29_t2892578226;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UICamera_DepthEntry1145614469.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator29<UICamera/DepthEntry>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29__ctor_m131657870_gshared (U3CGetEnumeratorU3Ec__Iterator29_t2892578226 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29__ctor_m131657870(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t2892578226 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29__ctor_m131657870_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator29<UICamera/DepthEntry>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  DepthEntry_t1145614469  U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1427705015_gshared (U3CGetEnumeratorU3Ec__Iterator29_t2892578226 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1427705015(__this, method) ((  DepthEntry_t1145614469  (*) (U3CGetEnumeratorU3Ec__Iterator29_t2892578226 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1427705015_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator29<UICamera/DepthEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m1756867298_gshared (U3CGetEnumeratorU3Ec__Iterator29_t2892578226 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m1756867298(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator29_t2892578226 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m1756867298_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator29<UICamera/DepthEntry>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m3759722382_gshared (U3CGetEnumeratorU3Ec__Iterator29_t2892578226 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m3759722382(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator29_t2892578226 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m3759722382_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator29<UICamera/DepthEntry>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29_Dispose_m1867444555_gshared (U3CGetEnumeratorU3Ec__Iterator29_t2892578226 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_Dispose_m1867444555(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t2892578226 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_Dispose_m1867444555_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator29<UICamera/DepthEntry>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29_Reset_m2073058107_gshared (U3CGetEnumeratorU3Ec__Iterator29_t2892578226 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_Reset_m2073058107(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t2892578226 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_Reset_m2073058107_gshared)(__this, method)
