﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_toqow109
struct M_toqow109_t1689188556;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_toqow1091689188556.h"

// System.Void GarbageiOS.M_toqow109::.ctor()
extern "C"  void M_toqow109__ctor_m2820543207 (M_toqow109_t1689188556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_toqow109::M_dikalljar0(System.String[],System.Int32)
extern "C"  void M_toqow109_M_dikalljar0_m1244812822 (M_toqow109_t1689188556 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_toqow109::M_tearfaileGemrere1(System.String[],System.Int32)
extern "C"  void M_toqow109_M_tearfaileGemrere1_m1885919627 (M_toqow109_t1689188556 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_toqow109::ilo_M_dikalljar01(GarbageiOS.M_toqow109,System.String[],System.Int32)
extern "C"  void M_toqow109_ilo_M_dikalljar01_m1738177232 (Il2CppObject * __this /* static, unused */, M_toqow109_t1689188556 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
