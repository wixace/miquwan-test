﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._2b0841d2beaa343275e0f45893f75a17
struct _2b0841d2beaa343275e0f45893f75a17_t213419414;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__2b0841d2beaa343275e0f4589213419414.h"

// System.Void Little._2b0841d2beaa343275e0f45893f75a17::.ctor()
extern "C"  void _2b0841d2beaa343275e0f45893f75a17__ctor_m3369634135 (_2b0841d2beaa343275e0f45893f75a17_t213419414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._2b0841d2beaa343275e0f45893f75a17::_2b0841d2beaa343275e0f45893f75a17m2(System.Int32)
extern "C"  int32_t _2b0841d2beaa343275e0f45893f75a17__2b0841d2beaa343275e0f45893f75a17m2_m2524674521 (_2b0841d2beaa343275e0f45893f75a17_t213419414 * __this, int32_t ____2b0841d2beaa343275e0f45893f75a17a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._2b0841d2beaa343275e0f45893f75a17::_2b0841d2beaa343275e0f45893f75a17m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _2b0841d2beaa343275e0f45893f75a17__2b0841d2beaa343275e0f45893f75a17m_m2808280317 (_2b0841d2beaa343275e0f45893f75a17_t213419414 * __this, int32_t ____2b0841d2beaa343275e0f45893f75a17a0, int32_t ____2b0841d2beaa343275e0f45893f75a17621, int32_t ____2b0841d2beaa343275e0f45893f75a17c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._2b0841d2beaa343275e0f45893f75a17::ilo__2b0841d2beaa343275e0f45893f75a17m21(Little._2b0841d2beaa343275e0f45893f75a17,System.Int32)
extern "C"  int32_t _2b0841d2beaa343275e0f45893f75a17_ilo__2b0841d2beaa343275e0f45893f75a17m21_m2299371549 (Il2CppObject * __this /* static, unused */, _2b0841d2beaa343275e0f45893f75a17_t213419414 * ____this0, int32_t ____2b0841d2beaa343275e0f45893f75a17a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
