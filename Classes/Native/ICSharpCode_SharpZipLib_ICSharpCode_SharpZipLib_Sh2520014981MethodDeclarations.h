﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.SharpZipBaseException
struct SharpZipBaseException_t2520014981;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_String7231557.h"

// System.Void ICSharpCode.SharpZipLib.SharpZipBaseException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SharpZipBaseException__ctor_m3034235335 (SharpZipBaseException_t2520014981 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.SharpZipBaseException::.ctor()
extern "C"  void SharpZipBaseException__ctor_m730469766 (SharpZipBaseException_t2520014981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.SharpZipBaseException::.ctor(System.String)
extern "C"  void SharpZipBaseException__ctor_m2797155132 (SharpZipBaseException_t2520014981 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
