﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_gurweajeePasjounu48
struct M_gurweajeePasjounu48_t1465778100;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_gurweajeePasjounu481465778100.h"

// System.Void GarbageiOS.M_gurweajeePasjounu48::.ctor()
extern "C"  void M_gurweajeePasjounu48__ctor_m1566789167 (M_gurweajeePasjounu48_t1465778100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gurweajeePasjounu48::M_todea0(System.String[],System.Int32)
extern "C"  void M_gurweajeePasjounu48_M_todea0_m2665539945 (M_gurweajeePasjounu48_t1465778100 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gurweajeePasjounu48::M_nallstade1(System.String[],System.Int32)
extern "C"  void M_gurweajeePasjounu48_M_nallstade1_m631167265 (M_gurweajeePasjounu48_t1465778100 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gurweajeePasjounu48::M_gowsaw2(System.String[],System.Int32)
extern "C"  void M_gurweajeePasjounu48_M_gowsaw2_m3997610282 (M_gurweajeePasjounu48_t1465778100 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gurweajeePasjounu48::M_dratem3(System.String[],System.Int32)
extern "C"  void M_gurweajeePasjounu48_M_dratem3_m821125852 (M_gurweajeePasjounu48_t1465778100 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gurweajeePasjounu48::M_heecar4(System.String[],System.Int32)
extern "C"  void M_gurweajeePasjounu48_M_heecar4_m3944539290 (M_gurweajeePasjounu48_t1465778100 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gurweajeePasjounu48::ilo_M_gowsaw21(GarbageiOS.M_gurweajeePasjounu48,System.String[],System.Int32)
extern "C"  void M_gurweajeePasjounu48_ilo_M_gowsaw21_m294487318 (Il2CppObject * __this /* static, unused */, M_gurweajeePasjounu48_t1465778100 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
