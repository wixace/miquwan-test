﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<HatredCtrl/stValue>
struct DefaultComparer_t1873722077;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<HatredCtrl/stValue>::.ctor()
extern "C"  void DefaultComparer__ctor_m3125470958_gshared (DefaultComparer_t1873722077 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3125470958(__this, method) ((  void (*) (DefaultComparer_t1873722077 *, const MethodInfo*))DefaultComparer__ctor_m3125470958_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<HatredCtrl/stValue>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2201616197_gshared (DefaultComparer_t1873722077 * __this, stValue_t3425945410  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2201616197(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1873722077 *, stValue_t3425945410 , const MethodInfo*))DefaultComparer_GetHashCode_m2201616197_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<HatredCtrl/stValue>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2252174211_gshared (DefaultComparer_t1873722077 * __this, stValue_t3425945410  ___x0, stValue_t3425945410  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2252174211(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1873722077 *, stValue_t3425945410 , stValue_t3425945410 , const MethodInfo*))DefaultComparer_Equals_m2252174211_gshared)(__this, ___x0, ___y1, method)
