﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectMgr
struct EffectMgr_t535289511;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// EffectCtrl
struct EffectCtrl_t3708787644;
// effectCfg
struct effectCfg_t2826279187;
// CombatEntity
struct CombatEntity_t684137495;
// EffectPara
struct EffectPara_t3709156657;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// System.String
struct String_t;
// GameQutilyCtr
struct GameQutilyCtr_t3963256169;
// ConfigAssetMgr
struct ConfigAssetMgr_t4036193930;
// ReplayMgr
struct ReplayMgr_t1549183121;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// CameraHelper
struct CameraHelper_t3196871507;
// ActorLine
struct ActorLine_t2375805801;
// UnityEngine.LineRenderer
struct LineRenderer_t1892709339;
// ActorBullet
struct ActorBullet_t2246481719;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_effectCfg2826279187.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_EffectCtrl3708787644.h"
#include "AssemblyU2DCSharp_EffectPara3709156657.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitTag299076696.h"
#include "AssemblyU2DCSharp_GameQutilyCtr3963256169.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ConfigAssetMgr4036193930.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"
#include "AssemblyU2DCSharp_EffectMgr535289511.h"
#include "AssemblyU2DCSharp_ReplayMgr1549183121.h"
#include "AssemblyU2DCSharp_ReplayExecuteType3106517448.h"
#include "AssemblyU2DCSharp_CameraHelper3196871507.h"
#include "AssemblyU2DCSharp_ActorLine2375805801.h"
#include "UnityEngine_UnityEngine_LineRenderer1892709339.h"

// System.Void EffectMgr::.ctor()
extern "C"  void EffectMgr__ctor_m1561707172 (EffectMgr_t535289511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::.cctor()
extern "C"  void EffectMgr__cctor_m686185865 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform EffectMgr::get_effectParent()
extern "C"  Transform_t1659122786 * EffectMgr_get_effectParent_m4272515782 (EffectMgr_t535289511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::set_effectParent(UnityEngine.Transform)
extern "C"  void EffectMgr_set_effectParent_m973991525 (EffectMgr_t535289511 * __this, Transform_t1659122786 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::PlaySceneEffect(System.Int32,UnityEngine.Vector3)
extern "C"  void EffectMgr_PlaySceneEffect_m2576255503 (EffectMgr_t535289511 * __this, int32_t ___id0, Vector3_t4282066566  ___pos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::PlaySceneEffect(UnityEngine.GameObject,UnityEngine.Vector3)
extern "C"  void EffectMgr_PlaySceneEffect_m1079628022 (EffectMgr_t535289511 * __this, GameObject_t3674682005 * ___obj0, Vector3_t4282066566  ___pos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject EffectMgr::GetEffectObj(System.Int32)
extern "C"  GameObject_t3674682005 * EffectMgr_GetEffectObj_m1547121100 (EffectMgr_t535289511 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::ClearSceneEffect()
extern "C"  void EffectMgr_ClearSceneEffect_m1066596432 (EffectMgr_t535289511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::OnEntityDead(System.Int32)
extern "C"  void EffectMgr_OnEntityDead_m4283223063 (EffectMgr_t535289511 * __this, int32_t ___srcID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::SetViewEffect(System.Int32,System.Boolean)
extern "C"  void EffectMgr_SetViewEffect_m42549874 (EffectMgr_t535289511 * __this, int32_t ___srcID0, bool ___isShow1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject EffectMgr::SetCameraShotViewEffect(System.Int32,System.Int32)
extern "C"  GameObject_t3674682005 * EffectMgr_SetCameraShotViewEffect_m616982968 (EffectMgr_t535289511 * __this, int32_t ___srcID0, int32_t ___effid1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::ClearLoopEffect(System.Int32,System.Int32)
extern "C"  void EffectMgr_ClearLoopEffect_m3886810658 (EffectMgr_t535289511 * __this, int32_t ___srcID0, int32_t ___effectID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::SetEffectLayer(System.Int32)
extern "C"  void EffectMgr_SetEffectLayer_m3966473391 (EffectMgr_t535289511 * __this, int32_t ___layer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::Clear()
extern "C"  void EffectMgr_Clear_m3262807759 (EffectMgr_t535289511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EffectCtrl EffectMgr::AddEffect(System.Int32,UnityEngine.GameObject,effectCfg,CombatEntity)
extern "C"  EffectCtrl_t3708787644 * EffectMgr_AddEffect_m3684316574 (EffectMgr_t535289511 * __this, int32_t ___id0, GameObject_t3674682005 * ___effectObj1, effectCfg_t2826279187 * ___effectVO2, CombatEntity_t684137495 * ____entity3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EffectCtrl EffectMgr::GetEffectGameObject(System.Int32)
extern "C"  EffectCtrl_t3708787644 * EffectMgr_GetEffectGameObject_m247552460 (EffectMgr_t535289511 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::ResetEffectGameObject(EffectCtrl)
extern "C"  void EffectMgr_ResetEffectGameObject_m634784375 (EffectMgr_t535289511 * __this, EffectCtrl_t3708787644 * ___effectObj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::InitParent()
extern "C"  void EffectMgr_InitParent_m3313283642 (EffectMgr_t535289511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::PlayEffect(System.Int32,CombatEntity,CombatEntity)
extern "C"  void EffectMgr_PlayEffect_m900097590 (EffectMgr_t535289511 * __this, int32_t ___id0, CombatEntity_t684137495 * ___srcTf1, CombatEntity_t684137495 * ___host2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::PlayEffect(System.Int32,CombatEntity)
extern "C"  void EffectMgr_PlayEffect_m1398047289 (EffectMgr_t535289511 * __this, int32_t ___id0, CombatEntity_t684137495 * ___srcTf1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::PlayEffect(System.Int32,CombatEntity,UnityEngine.Vector3)
extern "C"  void EffectMgr_PlayEffect_m3961229644 (EffectMgr_t535289511 * __this, int32_t ___id0, CombatEntity_t684137495 * ___srcTf1, Vector3_t4282066566  ___pos2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::PlayEffect(EffectPara)
extern "C"  void EffectMgr_PlayEffect_m4243746164 (EffectMgr_t535289511 * __this, EffectPara_t3709156657 * ___paras0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::PlayCameraShotEffect(System.Int32,UnityEngine.GameObject,CombatEntity,System.Single,UnityEngine.Vector3,CombatEntity,System.Boolean,System.Single)
extern "C"  void EffectMgr_PlayCameraShotEffect_m485048753 (EffectMgr_t535289511 * __this, int32_t ___id0, GameObject_t3674682005 * ___obj1, CombatEntity_t684137495 * ___srcTf2, float ___lifetime3, Vector3_t4282066566  ___pos4, CombatEntity_t684137495 * ___host5, bool ___isShowInBlack6, float ___playSpeed7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::PlayEffect(System.Int32,CombatEntity,System.Single,UnityEngine.Vector3,CombatEntity,System.Boolean,System.Single)
extern "C"  void EffectMgr_PlayEffect_m1448617188 (EffectMgr_t535289511 * __this, int32_t ___id0, CombatEntity_t684137495 * ___srcTf1, float ___lifetime2, Vector3_t4282066566  ___pos3, CombatEntity_t684137495 * ___host4, bool ___isShowInBlack5, float ___playSpeed6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::DoPlayEffect(EffectCtrl,effectCfg,UnityEngine.Transform,System.Single,UnityEngine.Vector3,UnityEngine.Transform,System.Boolean,System.Single)
extern "C"  void EffectMgr_DoPlayEffect_m1255021999 (EffectMgr_t535289511 * __this, EffectCtrl_t3708787644 * ___effectObj0, effectCfg_t2826279187 * ___effectCfg1, Transform_t1659122786 * ___srcTf2, float ___lifetime3, Vector3_t4282066566  ___pos4, Transform_t1659122786 * ___targetTr5, bool ___isShowInBlack6, float ___playSpeed7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::SetEffectPause(System.Int32)
extern "C"  void EffectMgr_SetEffectPause_m2789433492 (EffectMgr_t535289511 * __this, int32_t ___scrID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::SetEffectCancelPause()
extern "C"  void EffectMgr_SetEffectCancelPause_m2796313321 (EffectMgr_t535289511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::PlayCameraShotShoot(System.Int32,UnityEngine.GameObject,CombatEntity,CombatEntity,System.Single,System.Boolean,UnityEngine.Vector3,EntityEnum.UnitTag,System.Int32)
extern "C"  void EffectMgr_PlayCameraShotShoot_m672706473 (EffectMgr_t535289511 * __this, int32_t ___id0, GameObject_t3674682005 * ___obj1, CombatEntity_t684137495 * ___srcEntity2, CombatEntity_t684137495 * ___targetTf3, float ___speed4, bool ___isatk5, Vector3_t4282066566  ___targetPosition6, int32_t ___tag7, int32_t ___skillID8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::PlayShoot(System.Int32,CombatEntity,CombatEntity,System.Single,System.Boolean,UnityEngine.Vector3,EntityEnum.UnitTag,System.Int32)
extern "C"  void EffectMgr_PlayShoot_m4032809514 (EffectMgr_t535289511 * __this, int32_t ___id0, CombatEntity_t684137495 * ___srcEntity1, CombatEntity_t684137495 * ___targetTf2, float ___speed3, bool ___isatk4, Vector3_t4282066566  ___targetPosition5, int32_t ___tag6, int32_t ___skillID7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::DoPlayShoot(EffectCtrl,effectCfg,CombatEntity,CombatEntity,System.Single,System.Boolean,UnityEngine.Vector3,EntityEnum.UnitTag,System.Int32)
extern "C"  void EffectMgr_DoPlayShoot_m3483059305 (EffectMgr_t535289511 * __this, EffectCtrl_t3708787644 * ___effectObj0, effectCfg_t2826279187 * ___effectCfg1, CombatEntity_t684137495 * ___srcEntity2, CombatEntity_t684137495 * ___targetTf3, float ___speed4, bool ___isatk5, Vector3_t4282066566  ___targetPosition6, int32_t ___tag7, int32_t ___skillID8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::SacleParticleSystem(EffectCtrl,System.Single,System.Boolean,System.Boolean)
extern "C"  void EffectMgr_SacleParticleSystem_m2223506190 (EffectMgr_t535289511 * __this, EffectCtrl_t3708787644 * ___effectObj0, float ___scale1, bool ___isScale2, bool ___alsoScaleGameobject3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::RemoveSacleParticleSystem(EffectCtrl,System.Single,System.Boolean,System.Boolean)
extern "C"  void EffectMgr_RemoveSacleParticleSystem_m2916066090 (EffectMgr_t535289511 * __this, EffectCtrl_t3708787644 * ___effectObj0, float ___scale1, bool ___isScale2, bool ___alsoScaleGameobject3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSDatacfgManager EffectMgr::ilo_get_CfgDataMgr1()
extern "C"  CSDatacfgManager_t1565254243 * EffectMgr_ilo_get_CfgDataMgr1_m3881540455 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EffectMgr::ilo_GetTrackParticleName2(GameQutilyCtr,System.String)
extern "C"  String_t* EffectMgr_ilo_GetTrackParticleName2_m3933804039 (Il2CppObject * __this /* static, unused */, GameQutilyCtr_t3963256169 * ____this0, String_t* ___effectName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EffectMgr::ilo_IsNullAsset3(ConfigAssetMgr,System.String)
extern "C"  bool EffectMgr_ilo_IsNullAsset3_m1978535813 (Il2CppObject * __this /* static, unused */, ConfigAssetMgr_t4036193930 * ____this0, String_t* ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// effectCfg EffectMgr::ilo_GeteffectCfg4(CSDatacfgManager,System.Int32)
extern "C"  effectCfg_t2826279187 * EffectMgr_ilo_GeteffectCfg4_m1469877972 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameQutilyCtr EffectMgr::ilo_get_GameQutilyCtr5()
extern "C"  GameQutilyCtr_t3963256169 * EffectMgr_ilo_get_GameQutilyCtr5_m3486429406 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject EffectMgr::ilo_get_effGO6(EffectCtrl)
extern "C"  GameObject_t3674682005 * EffectMgr_ilo_get_effGO6_m179214068 (Il2CppObject * __this /* static, unused */, EffectCtrl_t3708787644 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::ilo_SetLayer7(UnityEngine.GameObject,System.Int32)
extern "C"  void EffectMgr_ilo_SetLayer7_m2123921480 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, int32_t ___layer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::ilo_Stop8(EffectCtrl)
extern "C"  void EffectMgr_ilo_Stop8_m2767963497 (Il2CppObject * __this /* static, unused */, EffectCtrl_t3708787644 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::ilo_Init9(EffectCtrl,UnityEngine.GameObject)
extern "C"  void EffectMgr_ilo_Init9_m1509196386 (Il2CppObject * __this /* static, unused */, EffectCtrl_t3708787644 * ____this0, GameObject_t3674682005 * ___go1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::ilo_PlayEffect10(EffectMgr,System.Int32,CombatEntity,System.Single,UnityEngine.Vector3,CombatEntity,System.Boolean,System.Single)
extern "C"  void EffectMgr_ilo_PlayEffect10_m853860727 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, int32_t ___id1, CombatEntity_t684137495 * ___srcTf2, float ___lifetime3, Vector3_t4282066566  ___pos4, CombatEntity_t684137495 * ___host5, bool ___isShowInBlack6, float ___playSpeed7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReplayMgr EffectMgr::ilo_get_Instance11()
extern "C"  ReplayMgr_t1549183121 * EffectMgr_ilo_get_Instance11_m3568195183 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EffectMgr::ilo_get_isDeath12(CombatEntity)
extern "C"  bool EffectMgr_ilo_get_isDeath12_m3808861486 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::ilo_DoPlayEffect13(EffectMgr,EffectCtrl,effectCfg,UnityEngine.Transform,System.Single,UnityEngine.Vector3,UnityEngine.Transform,System.Boolean,System.Single)
extern "C"  void EffectMgr_ilo_DoPlayEffect13_m2411702751 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, EffectCtrl_t3708787644 * ___effectObj1, effectCfg_t2826279187 * ___effectCfg2, Transform_t1659122786 * ___srcTf3, float ___lifetime4, Vector3_t4282066566  ___pos5, Transform_t1659122786 * ___targetTr6, bool ___isShowInBlack7, float ___playSpeed8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::ilo_AddData14(ReplayMgr,ReplayExecuteType,System.Object[])
extern "C"  void EffectMgr_ilo_AddData14_m2841669620 (Il2CppObject * __this /* static, unused */, ReplayMgr_t1549183121 * ____this0, int32_t ___type1, ObjectU5BU5D_t1108656482* ___datas2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform EffectMgr::ilo_get_effectParent15(EffectMgr)
extern "C"  Transform_t1659122786 * EffectMgr_ilo_get_effectParent15_m3214759982 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::ilo_ChangeParent16(UnityEngine.Transform,UnityEngine.Transform,System.Boolean)
extern "C"  void EffectMgr_ilo_ChangeParent16_m2559302907 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ____this0, Transform_t1659122786 * ___parent1, bool ___resetMat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EffectMgr::ilo_get_inScaler17(CameraHelper)
extern "C"  bool EffectMgr_ilo_get_inScaler17_m1074930984 (Il2CppObject * __this /* static, unused */, CameraHelper_t3196871507 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single EffectMgr::ilo_get_scale18(CombatEntity)
extern "C"  float EffectMgr_ilo_get_scale18_m3734105324 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::ilo_SacleParticleSystem19(EffectMgr,EffectCtrl,System.Single,System.Boolean,System.Boolean)
extern "C"  void EffectMgr_ilo_SacleParticleSystem19_m5865246 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, EffectCtrl_t3708787644 * ___effectObj1, float ___scale2, bool ___isScale3, bool ___alsoScaleGameobject4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::ilo_SetLine20(ActorLine,UnityEngine.Transform,UnityEngine.Transform,System.Single,UnityEngine.LineRenderer)
extern "C"  void EffectMgr_ilo_SetLine20_m892809183 (Il2CppObject * __this /* static, unused */, ActorLine_t2375805801 * ____this0, Transform_t1659122786 * ___targetTr1, Transform_t1659122786 * ___host2, float ___lifetime3, LineRenderer_t1892709339 * ___lineRenderer4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::ilo_SetPause21(EffectCtrl)
extern "C"  void EffectMgr_ilo_SetPause21_m1518936010 (Il2CppObject * __this /* static, unused */, EffectCtrl_t3708787644 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::ilo_PlayEffect22(EffectMgr,System.Int32,CombatEntity)
extern "C"  void EffectMgr_ilo_PlayEffect22_m536447947 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, int32_t ___id1, CombatEntity_t684137495 * ___srcTf2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr::ilo_DoPlayShoot23(EffectMgr,EffectCtrl,effectCfg,CombatEntity,CombatEntity,System.Single,System.Boolean,UnityEngine.Vector3,EntityEnum.UnitTag,System.Int32)
extern "C"  void EffectMgr_ilo_DoPlayShoot23_m1890424608 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, EffectCtrl_t3708787644 * ___effectObj1, effectCfg_t2826279187 * ___effectCfg2, CombatEntity_t684137495 * ___srcEntity3, CombatEntity_t684137495 * ___targetTf4, float ___speed5, bool ___isatk6, Vector3_t4282066566  ___targetPosition7, int32_t ___tag8, int32_t ___skillID9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject EffectMgr::ilo_FindChild24(UnityEngine.GameObject,System.String)
extern "C"  GameObject_t3674682005 * EffectMgr_ilo_FindChild24_m776336573 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ActorBullet EffectMgr::ilo_get_actorBullet25(EffectCtrl)
extern "C"  ActorBullet_t2246481719 * EffectMgr_ilo_get_actorBullet25_m3275634560 (Il2CppObject * __this /* static, unused */, EffectCtrl_t3708787644 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
