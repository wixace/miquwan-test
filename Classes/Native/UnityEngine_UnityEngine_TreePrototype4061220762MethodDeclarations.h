﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.TreePrototype
struct TreePrototype_t4061220762;
// UnityEngine.GameObject
struct GameObject_t3674682005;
struct TreePrototype_t4061220762_marshaled_pinvoke;
struct TreePrototype_t4061220762_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UnityEngine.TreePrototype::.ctor()
extern "C"  void TreePrototype__ctor_m3704876853 (TreePrototype_t4061220762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.TreePrototype::get_prefab()
extern "C"  GameObject_t3674682005 * TreePrototype_get_prefab_m123508821 (TreePrototype_t4061220762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TreePrototype::set_prefab(UnityEngine.GameObject)
extern "C"  void TreePrototype_set_prefab_m1279093768 (TreePrototype_t4061220762 * __this, GameObject_t3674682005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.TreePrototype::get_bendFactor()
extern "C"  float TreePrototype_get_bendFactor_m3290807156 (TreePrototype_t4061220762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TreePrototype::set_bendFactor(System.Single)
extern "C"  void TreePrototype_set_bendFactor_m1248256439 (TreePrototype_t4061220762 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct TreePrototype_t4061220762;
struct TreePrototype_t4061220762_marshaled_pinvoke;

extern "C" void TreePrototype_t4061220762_marshal_pinvoke(const TreePrototype_t4061220762& unmarshaled, TreePrototype_t4061220762_marshaled_pinvoke& marshaled);
extern "C" void TreePrototype_t4061220762_marshal_pinvoke_back(const TreePrototype_t4061220762_marshaled_pinvoke& marshaled, TreePrototype_t4061220762& unmarshaled);
extern "C" void TreePrototype_t4061220762_marshal_pinvoke_cleanup(TreePrototype_t4061220762_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct TreePrototype_t4061220762;
struct TreePrototype_t4061220762_marshaled_com;

extern "C" void TreePrototype_t4061220762_marshal_com(const TreePrototype_t4061220762& unmarshaled, TreePrototype_t4061220762_marshaled_com& marshaled);
extern "C" void TreePrototype_t4061220762_marshal_com_back(const TreePrototype_t4061220762_marshaled_com& marshaled, TreePrototype_t4061220762& unmarshaled);
extern "C" void TreePrototype_t4061220762_marshal_com_cleanup(TreePrototype_t4061220762_marshaled_com& marshaled);
