﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<FLOAT_TEXT_ID>
struct DefaultComparer_t647122437;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<FLOAT_TEXT_ID>::.ctor()
extern "C"  void DefaultComparer__ctor_m2404234088_gshared (DefaultComparer_t647122437 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2404234088(__this, method) ((  void (*) (DefaultComparer_t647122437 *, const MethodInfo*))DefaultComparer__ctor_m2404234088_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<FLOAT_TEXT_ID>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2928580675_gshared (DefaultComparer_t647122437 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2928580675(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t647122437 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m2928580675_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<FLOAT_TEXT_ID>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1303841465_gshared (DefaultComparer_t647122437 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1303841465(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t647122437 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m1303841465_gshared)(__this, ___x0, ___y1, method)
