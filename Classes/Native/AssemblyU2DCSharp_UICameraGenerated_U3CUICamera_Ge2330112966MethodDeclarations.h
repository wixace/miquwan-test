﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICameraGenerated/<UICamera_GetAxis_GetDelegate_member4_arg0>c__AnonStoreyA0
struct U3CUICamera_GetAxis_GetDelegate_member4_arg0U3Ec__AnonStoreyA0_t2330112966;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void UICameraGenerated/<UICamera_GetAxis_GetDelegate_member4_arg0>c__AnonStoreyA0::.ctor()
extern "C"  void U3CUICamera_GetAxis_GetDelegate_member4_arg0U3Ec__AnonStoreyA0__ctor_m2410634645 (U3CUICamera_GetAxis_GetDelegate_member4_arg0U3Ec__AnonStoreyA0_t2330112966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UICameraGenerated/<UICamera_GetAxis_GetDelegate_member4_arg0>c__AnonStoreyA0::<>m__104(System.String)
extern "C"  float U3CUICamera_GetAxis_GetDelegate_member4_arg0U3Ec__AnonStoreyA0_U3CU3Em__104_m3637168925 (U3CUICamera_GetAxis_GetDelegate_member4_arg0U3Ec__AnonStoreyA0_t2330112966 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
