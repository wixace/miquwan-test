﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Matrix4x4Generated
struct UnityEngine_Matrix4x4Generated_t2236912424;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine_Matrix4x4Generated::.ctor()
extern "C"  void UnityEngine_Matrix4x4Generated__ctor_m4019525235 (UnityEngine_Matrix4x4Generated_t2236912424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::.cctor()
extern "C"  void UnityEngine_Matrix4x4Generated__cctor_m3864101786 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_Matrix4x41(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_Matrix4x41_m4103005191 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_m00(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_m00_m964107289 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_m10(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_m10_m3462123226 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_m20(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_m20_m1665171867 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_m30(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_m30_m4163187804 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_m01(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_m01_m767593784 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_m11(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_m11_m3265609721 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_m21(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_m21_m1468658362 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_m31(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_m31_m3966674299 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_m02(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_m02_m571080279 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_m12(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_m12_m3069096216 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_m22(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_m22_m1272144857 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_m32(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_m32_m3770160794 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_m03(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_m03_m374566774 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_m13(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_m13_m2872582711 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_m23(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_m23_m1075631352 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_m33(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_m33_m3573647289 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_Item_Int32_Int32(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_Item_Int32_Int32_m2792338229 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_Item_Int32(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_Item_Int32_m433413700 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_inverse(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_inverse_m3765183350 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_transpose(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_transpose_m200096141 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_isIdentity(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_isIdentity_m3672969054 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_determinant(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_determinant_m3603985905 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_zero(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_zero_m4114334910 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::Matrix4x4_identity(JSVCall)
extern "C"  void UnityEngine_Matrix4x4Generated_Matrix4x4_identity_m325376872 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_Equals__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_Equals__Object_m2477378947 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_GetColumn__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_GetColumn__Int32_m2413796423 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_GetHashCode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_GetHashCode_m3017781614 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_GetRow__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_GetRow__Int32_m2715918215 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_MultiplyPoint__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_MultiplyPoint__Vector3_m1725559817 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_MultiplyPoint3x4__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_MultiplyPoint3x4__Vector3_m1157046186 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_MultiplyVector__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_MultiplyVector__Vector3_m3536095878 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_SetColumn__Int32__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_SetColumn__Int32__Vector4_m1681429048 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_SetRow__Int32__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_SetRow__Int32__Vector4_m75586656 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_SetTRS__Vector3__Quaternion__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_SetTRS__Vector3__Quaternion__Vector3_m3389149850 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_ToString__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_ToString__String_m380500418 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_ToString_m1105607793 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_Determinant__Matrix4x4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_Determinant__Matrix4x4_m177182111 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_Inverse__Matrix4x4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_Inverse__Matrix4x4_m1025461796 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_op_Equality__Matrix4x4__Matrix4x4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_op_Equality__Matrix4x4__Matrix4x4_m2938646549 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_op_Inequality__Matrix4x4__Matrix4x4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_op_Inequality__Matrix4x4__Matrix4x4_m458682938 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_op_Multiply__Matrix4x4__Matrix4x4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_op_Multiply__Matrix4x4__Matrix4x4_m1637075519 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_op_Multiply__Matrix4x4__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_op_Multiply__Matrix4x4__Vector4_m426051905 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_Ortho__Single__Single__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_Ortho__Single__Single__Single__Single__Single__Single_m3765551877 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_Perspective__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_Perspective__Single__Single__Single__Single_m3158395129 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_Scale__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_Scale__Vector3_m920986827 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_Transpose__Matrix4x4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_Transpose__Matrix4x4_m3555797563 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Matrix4x4Generated::Matrix4x4_TRS__Vector3__Quaternion__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Matrix4x4Generated_Matrix4x4_TRS__Vector3__Quaternion__Vector3_m1367423476 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::__Register()
extern "C"  void UnityEngine_Matrix4x4Generated___Register_m973567540 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Matrix4x4Generated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_Matrix4x4Generated_ilo_getObject1_m4258778623 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_Matrix4x4Generated_ilo_addJSCSRel2_m1082496048 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_Matrix4x4Generated_ilo_setSingle3_m3134217763 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_Matrix4x4Generated::ilo_getSingle4(System.Int32)
extern "C"  float UnityEngine_Matrix4x4Generated_ilo_getSingle4_m1003680343 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::ilo_changeJSObj5(System.Int32,System.Object)
extern "C"  void UnityEngine_Matrix4x4Generated_ilo_changeJSObj5_m2691153114 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Matrix4x4Generated::ilo_getInt326(System.Int32)
extern "C"  int32_t UnityEngine_Matrix4x4Generated_ilo_getInt326_m2672957183 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Matrix4x4Generated::ilo_setObject7(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_Matrix4x4Generated_ilo_setObject7_m3912405757 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::ilo_setVector3S8(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_Matrix4x4Generated_ilo_setVector3S8_m1618850875 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_Matrix4x4Generated::ilo_getObject9(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_Matrix4x4Generated_ilo_getObject9_m31359844 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_Matrix4x4Generated::ilo_getVector3S10(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_Matrix4x4Generated_ilo_getVector3S10_m1263132429 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_Matrix4x4Generated::ilo_getStringS11(System.Int32)
extern "C"  String_t* UnityEngine_Matrix4x4Generated_ilo_getStringS11_m2162701252 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::ilo_setStringS12(System.Int32,System.String)
extern "C"  void UnityEngine_Matrix4x4Generated_ilo_setStringS12_m2244386386 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Matrix4x4Generated::ilo_setBooleanS13(System.Int32,System.Boolean)
extern "C"  void UnityEngine_Matrix4x4Generated_ilo_setBooleanS13_m3526366719 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
