﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationSpriteSheet1
struct  AnimationSpriteSheet1_t2485186395  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 AnimationSpriteSheet1::uvX
	int32_t ___uvX_2;
	// System.Int32 AnimationSpriteSheet1::uvY
	int32_t ___uvY_3;
	// System.Single AnimationSpriteSheet1::fps
	float ___fps_4;

public:
	inline static int32_t get_offset_of_uvX_2() { return static_cast<int32_t>(offsetof(AnimationSpriteSheet1_t2485186395, ___uvX_2)); }
	inline int32_t get_uvX_2() const { return ___uvX_2; }
	inline int32_t* get_address_of_uvX_2() { return &___uvX_2; }
	inline void set_uvX_2(int32_t value)
	{
		___uvX_2 = value;
	}

	inline static int32_t get_offset_of_uvY_3() { return static_cast<int32_t>(offsetof(AnimationSpriteSheet1_t2485186395, ___uvY_3)); }
	inline int32_t get_uvY_3() const { return ___uvY_3; }
	inline int32_t* get_address_of_uvY_3() { return &___uvY_3; }
	inline void set_uvY_3(int32_t value)
	{
		___uvY_3 = value;
	}

	inline static int32_t get_offset_of_fps_4() { return static_cast<int32_t>(offsetof(AnimationSpriteSheet1_t2485186395, ___fps_4)); }
	inline float get_fps_4() const { return ___fps_4; }
	inline float* get_address_of_fps_4() { return &___fps_4; }
	inline void set_fps_4(float value)
	{
		___fps_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
