﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul2946406228.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Poly2Tri.P2T
struct  P2T_t3302343464  : public Il2CppObject
{
public:

public:
};

struct P2T_t3302343464_StaticFields
{
public:
	// Pathfinding.Poly2Tri.TriangulationAlgorithm Pathfinding.Poly2Tri.P2T::_defaultAlgorithm
	int32_t ____defaultAlgorithm_0;

public:
	inline static int32_t get_offset_of__defaultAlgorithm_0() { return static_cast<int32_t>(offsetof(P2T_t3302343464_StaticFields, ____defaultAlgorithm_0)); }
	inline int32_t get__defaultAlgorithm_0() const { return ____defaultAlgorithm_0; }
	inline int32_t* get_address_of__defaultAlgorithm_0() { return &____defaultAlgorithm_0; }
	inline void set__defaultAlgorithm_0(int32_t value)
	{
		____defaultAlgorithm_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
