﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.BoxCollider2D
struct BoxCollider2D_t2212926951;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine.BoxCollider2D::.ctor()
extern "C"  void BoxCollider2D__ctor_m430206856 (BoxCollider2D_t2212926951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.BoxCollider2D::get_size()
extern "C"  Vector2_t4282066565  BoxCollider2D_get_size_m1527127659 (BoxCollider2D_t2212926951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BoxCollider2D::set_size(UnityEngine.Vector2)
extern "C"  void BoxCollider2D_set_size_m1211670560 (BoxCollider2D_t2212926951 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BoxCollider2D::INTERNAL_get_size(UnityEngine.Vector2&)
extern "C"  void BoxCollider2D_INTERNAL_get_size_m2529780516 (BoxCollider2D_t2212926951 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BoxCollider2D::INTERNAL_set_size(UnityEngine.Vector2&)
extern "C"  void BoxCollider2D_INTERNAL_set_size_m2665574040 (BoxCollider2D_t2212926951 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
