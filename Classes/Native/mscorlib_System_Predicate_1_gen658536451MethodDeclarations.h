﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen3781873254MethodDeclarations.h"

// System.Void System.Predicate`1<Pathfinding.Poly2Tri.Polygon>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m1755623456(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t658536451 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m231416842_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<Pathfinding.Poly2Tri.Polygon>::Invoke(T)
#define Predicate_1_Invoke_m2400921570(__this, ___obj0, method) ((  bool (*) (Predicate_1_t658536451 *, Polygon_t1047479568 *, const MethodInfo*))Predicate_1_Invoke_m1328901537_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<Pathfinding.Poly2Tri.Polygon>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m1732380145(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t658536451 *, Polygon_t1047479568 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2038073176_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<Pathfinding.Poly2Tri.Polygon>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m3759553842(__this, ___result0, method) ((  bool (*) (Predicate_1_t658536451 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3970497007_gshared)(__this, ___result0, method)
