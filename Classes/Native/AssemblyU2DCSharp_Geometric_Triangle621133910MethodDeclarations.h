﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Geometric/Triangle
struct Triangle_t621133910;
struct Triangle_t621133910_marshaled_pinvoke;
struct Triangle_t621133910_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Geometric_Triangle621133910.h"

// System.Void Geometric/Triangle::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void Triangle__ctor_m525155135 (Triangle_t621133910 * __this, float ____x10, float ____y11, float ____x22, float ____y23, float ____x34, float ____y35, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Triangle_t621133910;
struct Triangle_t621133910_marshaled_pinvoke;

extern "C" void Triangle_t621133910_marshal_pinvoke(const Triangle_t621133910& unmarshaled, Triangle_t621133910_marshaled_pinvoke& marshaled);
extern "C" void Triangle_t621133910_marshal_pinvoke_back(const Triangle_t621133910_marshaled_pinvoke& marshaled, Triangle_t621133910& unmarshaled);
extern "C" void Triangle_t621133910_marshal_pinvoke_cleanup(Triangle_t621133910_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Triangle_t621133910;
struct Triangle_t621133910_marshaled_com;

extern "C" void Triangle_t621133910_marshal_com(const Triangle_t621133910& unmarshaled, Triangle_t621133910_marshaled_com& marshaled);
extern "C" void Triangle_t621133910_marshal_com_back(const Triangle_t621133910_marshaled_com& marshaled, Triangle_t621133910& unmarshaled);
extern "C" void Triangle_t621133910_marshal_com_cleanup(Triangle_t621133910_marshaled_com& marshaled);
