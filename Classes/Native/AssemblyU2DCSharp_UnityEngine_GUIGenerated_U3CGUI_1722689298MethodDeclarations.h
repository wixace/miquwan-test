﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUIGenerated/<GUI_ModalWindow_GetDelegate_member58_arg2>c__AnonStoreyEC
struct U3CGUI_ModalWindow_GetDelegate_member58_arg2U3Ec__AnonStoreyEC_t1722689298;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_GUIGenerated/<GUI_ModalWindow_GetDelegate_member58_arg2>c__AnonStoreyEC::.ctor()
extern "C"  void U3CGUI_ModalWindow_GetDelegate_member58_arg2U3Ec__AnonStoreyEC__ctor_m3502932121 (U3CGUI_ModalWindow_GetDelegate_member58_arg2U3Ec__AnonStoreyEC_t1722689298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated/<GUI_ModalWindow_GetDelegate_member58_arg2>c__AnonStoreyEC::<>m__1C2(System.Int32)
extern "C"  void U3CGUI_ModalWindow_GetDelegate_member58_arg2U3Ec__AnonStoreyEC_U3CU3Em__1C2_m2738252433 (U3CGUI_ModalWindow_GetDelegate_member58_arg2U3Ec__AnonStoreyEC_t1722689298 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
