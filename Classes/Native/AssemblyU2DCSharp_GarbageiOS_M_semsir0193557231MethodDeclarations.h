﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_semsir0
struct M_semsir0_t193557231;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_semsir0::.ctor()
extern "C"  void M_semsir0__ctor_m1345892692 (M_semsir0_t193557231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_semsir0::M_chouhermairFewiball0(System.String[],System.Int32)
extern "C"  void M_semsir0_M_chouhermairFewiball0_m781388786 (M_semsir0_t193557231 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
