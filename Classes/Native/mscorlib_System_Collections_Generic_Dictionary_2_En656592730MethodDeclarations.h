﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,Pathfinding.NodeLink3>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2780958168(__this, ___dictionary0, method) ((  void (*) (Enumerator_t656592730 *, Dictionary_2_t3634236634 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,Pathfinding.NodeLink3>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m97630665(__this, method) ((  Il2CppObject * (*) (Enumerator_t656592730 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,Pathfinding.NodeLink3>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4270820317(__this, method) ((  void (*) (Enumerator_t656592730 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,Pathfinding.NodeLink3>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2121410982(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t656592730 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,Pathfinding.NodeLink3>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1547090917(__this, method) ((  Il2CppObject * (*) (Enumerator_t656592730 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,Pathfinding.NodeLink3>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1744123191(__this, method) ((  Il2CppObject * (*) (Enumerator_t656592730 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,Pathfinding.NodeLink3>::MoveNext()
#define Enumerator_MoveNext_m789647945(__this, method) ((  bool (*) (Enumerator_t656592730 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,Pathfinding.NodeLink3>::get_Current()
#define Enumerator_get_Current_m4281137991(__this, method) ((  KeyValuePair_2_t3533017340  (*) (Enumerator_t656592730 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,Pathfinding.NodeLink3>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2874683222(__this, method) ((  GraphNode_t23612370 * (*) (Enumerator_t656592730 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,Pathfinding.NodeLink3>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m256733818(__this, method) ((  NodeLink3_t1645404665 * (*) (Enumerator_t656592730 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,Pathfinding.NodeLink3>::Reset()
#define Enumerator_Reset_m796091434(__this, method) ((  void (*) (Enumerator_t656592730 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,Pathfinding.NodeLink3>::VerifyState()
#define Enumerator_VerifyState_m1546742963(__this, method) ((  void (*) (Enumerator_t656592730 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,Pathfinding.NodeLink3>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3106174811(__this, method) ((  void (*) (Enumerator_t656592730 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.GraphNode,Pathfinding.NodeLink3>::Dispose()
#define Enumerator_Dispose_m3063118458(__this, method) ((  void (*) (Enumerator_t656592730 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
