﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3771233898;
// VersionMgr
struct VersionMgr_t1322950208;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VersionMgr/<LoadAllVSCfg>c__AnonStorey15E
struct  U3CLoadAllVSCfgU3Ec__AnonStorey15E_t2462933471  : public Il2CppObject
{
public:
	// System.Action VersionMgr/<LoadAllVSCfg>c__AnonStorey15E::call
	Action_t3771233898 * ___call_0;
	// VersionMgr VersionMgr/<LoadAllVSCfg>c__AnonStorey15E::<>f__this
	VersionMgr_t1322950208 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_call_0() { return static_cast<int32_t>(offsetof(U3CLoadAllVSCfgU3Ec__AnonStorey15E_t2462933471, ___call_0)); }
	inline Action_t3771233898 * get_call_0() const { return ___call_0; }
	inline Action_t3771233898 ** get_address_of_call_0() { return &___call_0; }
	inline void set_call_0(Action_t3771233898 * value)
	{
		___call_0 = value;
		Il2CppCodeGenWriteBarrier(&___call_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CLoadAllVSCfgU3Ec__AnonStorey15E_t2462933471, ___U3CU3Ef__this_1)); }
	inline VersionMgr_t1322950208 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline VersionMgr_t1322950208 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(VersionMgr_t1322950208 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
