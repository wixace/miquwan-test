﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Object,System.Single>
struct U3CDelayCallExecU3Ec__Iterator40_2_t1213861913;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Object,System.Single>::.ctor()
extern "C"  void U3CDelayCallExecU3Ec__Iterator40_2__ctor_m4276111569_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t1213861913 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2__ctor_m4276111569(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator40_2_t1213861913 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2__ctor_m4276111569_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Object,System.Single>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m46175585_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t1213861913 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m46175585(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator40_2_t1213861913 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m46175585_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Object,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_IEnumerator_get_Current_m1533246197_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t1213861913 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_IEnumerator_get_Current_m1533246197(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator40_2_t1213861913 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_IEnumerator_get_Current_m1533246197_gshared)(__this, method)
// System.Boolean BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Object,System.Single>::MoveNext()
extern "C"  bool U3CDelayCallExecU3Ec__Iterator40_2_MoveNext_m2305562755_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t1213861913 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_MoveNext_m2305562755(__this, method) ((  bool (*) (U3CDelayCallExecU3Ec__Iterator40_2_t1213861913 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_MoveNext_m2305562755_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Object,System.Single>::Dispose()
extern "C"  void U3CDelayCallExecU3Ec__Iterator40_2_Dispose_m3252765902_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t1213861913 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_Dispose_m3252765902(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator40_2_t1213861913 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_Dispose_m3252765902_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Object,System.Single>::Reset()
extern "C"  void U3CDelayCallExecU3Ec__Iterator40_2_Reset_m1922544510_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t1213861913 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_Reset_m1922544510(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator40_2_t1213861913 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_Reset_m1922544510_gshared)(__this, method)
