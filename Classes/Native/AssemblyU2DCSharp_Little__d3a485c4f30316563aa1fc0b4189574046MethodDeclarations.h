﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._d3a485c4f30316563aa1fc0b2c65c78f
struct _d3a485c4f30316563aa1fc0b2c65c78f_t4189574046;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._d3a485c4f30316563aa1fc0b2c65c78f::.ctor()
extern "C"  void _d3a485c4f30316563aa1fc0b2c65c78f__ctor_m226264143 (_d3a485c4f30316563aa1fc0b2c65c78f_t4189574046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._d3a485c4f30316563aa1fc0b2c65c78f::_d3a485c4f30316563aa1fc0b2c65c78fm2(System.Int32)
extern "C"  int32_t _d3a485c4f30316563aa1fc0b2c65c78f__d3a485c4f30316563aa1fc0b2c65c78fm2_m3000794329 (_d3a485c4f30316563aa1fc0b2c65c78f_t4189574046 * __this, int32_t ____d3a485c4f30316563aa1fc0b2c65c78fa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._d3a485c4f30316563aa1fc0b2c65c78f::_d3a485c4f30316563aa1fc0b2c65c78fm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _d3a485c4f30316563aa1fc0b2c65c78f__d3a485c4f30316563aa1fc0b2c65c78fm_m370889213 (_d3a485c4f30316563aa1fc0b2c65c78f_t4189574046 * __this, int32_t ____d3a485c4f30316563aa1fc0b2c65c78fa0, int32_t ____d3a485c4f30316563aa1fc0b2c65c78f481, int32_t ____d3a485c4f30316563aa1fc0b2c65c78fc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
