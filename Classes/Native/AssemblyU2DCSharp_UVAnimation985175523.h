﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Renderer
struct Renderer_t3076687687;
// UnityEngine.Material
struct Material_t3870600107;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UVAnimation
struct  UVAnimation_t985175523  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean UVAnimation::pauseTimer
	bool ___pauseTimer_2;
	// UnityEngine.Vector2 UVAnimation::uvDefaultTiling
	Vector2_t4282066565  ___uvDefaultTiling_3;
	// UnityEngine.Vector2 UVAnimation::uvDefaultOffset
	Vector2_t4282066565  ___uvDefaultOffset_4;
	// UnityEngine.Vector2 UVAnimation::uvOffsetSpeed
	Vector2_t4282066565  ___uvOffsetSpeed_5;
	// UnityEngine.Color UVAnimation::matDefaultColor
	Color_t4194546905  ___matDefaultColor_6;
	// UnityEngine.Renderer UVAnimation::_renderer
	Renderer_t3076687687 * ____renderer_7;
	// UnityEngine.Material UVAnimation::_mat
	Material_t3870600107 * ____mat_8;
	// UnityEngine.Vector2 UVAnimation::m_offset
	Vector2_t4282066565  ___m_offset_9;
	// UnityEngine.Vector2 UVAnimation::m_tiling
	Vector2_t4282066565  ___m_tiling_10;

public:
	inline static int32_t get_offset_of_pauseTimer_2() { return static_cast<int32_t>(offsetof(UVAnimation_t985175523, ___pauseTimer_2)); }
	inline bool get_pauseTimer_2() const { return ___pauseTimer_2; }
	inline bool* get_address_of_pauseTimer_2() { return &___pauseTimer_2; }
	inline void set_pauseTimer_2(bool value)
	{
		___pauseTimer_2 = value;
	}

	inline static int32_t get_offset_of_uvDefaultTiling_3() { return static_cast<int32_t>(offsetof(UVAnimation_t985175523, ___uvDefaultTiling_3)); }
	inline Vector2_t4282066565  get_uvDefaultTiling_3() const { return ___uvDefaultTiling_3; }
	inline Vector2_t4282066565 * get_address_of_uvDefaultTiling_3() { return &___uvDefaultTiling_3; }
	inline void set_uvDefaultTiling_3(Vector2_t4282066565  value)
	{
		___uvDefaultTiling_3 = value;
	}

	inline static int32_t get_offset_of_uvDefaultOffset_4() { return static_cast<int32_t>(offsetof(UVAnimation_t985175523, ___uvDefaultOffset_4)); }
	inline Vector2_t4282066565  get_uvDefaultOffset_4() const { return ___uvDefaultOffset_4; }
	inline Vector2_t4282066565 * get_address_of_uvDefaultOffset_4() { return &___uvDefaultOffset_4; }
	inline void set_uvDefaultOffset_4(Vector2_t4282066565  value)
	{
		___uvDefaultOffset_4 = value;
	}

	inline static int32_t get_offset_of_uvOffsetSpeed_5() { return static_cast<int32_t>(offsetof(UVAnimation_t985175523, ___uvOffsetSpeed_5)); }
	inline Vector2_t4282066565  get_uvOffsetSpeed_5() const { return ___uvOffsetSpeed_5; }
	inline Vector2_t4282066565 * get_address_of_uvOffsetSpeed_5() { return &___uvOffsetSpeed_5; }
	inline void set_uvOffsetSpeed_5(Vector2_t4282066565  value)
	{
		___uvOffsetSpeed_5 = value;
	}

	inline static int32_t get_offset_of_matDefaultColor_6() { return static_cast<int32_t>(offsetof(UVAnimation_t985175523, ___matDefaultColor_6)); }
	inline Color_t4194546905  get_matDefaultColor_6() const { return ___matDefaultColor_6; }
	inline Color_t4194546905 * get_address_of_matDefaultColor_6() { return &___matDefaultColor_6; }
	inline void set_matDefaultColor_6(Color_t4194546905  value)
	{
		___matDefaultColor_6 = value;
	}

	inline static int32_t get_offset_of__renderer_7() { return static_cast<int32_t>(offsetof(UVAnimation_t985175523, ____renderer_7)); }
	inline Renderer_t3076687687 * get__renderer_7() const { return ____renderer_7; }
	inline Renderer_t3076687687 ** get_address_of__renderer_7() { return &____renderer_7; }
	inline void set__renderer_7(Renderer_t3076687687 * value)
	{
		____renderer_7 = value;
		Il2CppCodeGenWriteBarrier(&____renderer_7, value);
	}

	inline static int32_t get_offset_of__mat_8() { return static_cast<int32_t>(offsetof(UVAnimation_t985175523, ____mat_8)); }
	inline Material_t3870600107 * get__mat_8() const { return ____mat_8; }
	inline Material_t3870600107 ** get_address_of__mat_8() { return &____mat_8; }
	inline void set__mat_8(Material_t3870600107 * value)
	{
		____mat_8 = value;
		Il2CppCodeGenWriteBarrier(&____mat_8, value);
	}

	inline static int32_t get_offset_of_m_offset_9() { return static_cast<int32_t>(offsetof(UVAnimation_t985175523, ___m_offset_9)); }
	inline Vector2_t4282066565  get_m_offset_9() const { return ___m_offset_9; }
	inline Vector2_t4282066565 * get_address_of_m_offset_9() { return &___m_offset_9; }
	inline void set_m_offset_9(Vector2_t4282066565  value)
	{
		___m_offset_9 = value;
	}

	inline static int32_t get_offset_of_m_tiling_10() { return static_cast<int32_t>(offsetof(UVAnimation_t985175523, ___m_tiling_10)); }
	inline Vector2_t4282066565  get_m_tiling_10() const { return ___m_tiling_10; }
	inline Vector2_t4282066565 * get_address_of_m_tiling_10() { return &___m_tiling_10; }
	inline void set_m_tiling_10(Vector2_t4282066565  value)
	{
		___m_tiling_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
