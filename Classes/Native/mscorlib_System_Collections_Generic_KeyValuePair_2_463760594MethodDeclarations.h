﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23529636437MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m690653619(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t463760594 *, uint8_t, List_1_t1104940528 *, const MethodInfo*))KeyValuePair_2__ctor_m1845025713_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::get_Key()
#define KeyValuePair_2_get_Key_m3905785045(__this, method) ((  uint8_t (*) (KeyValuePair_2_t463760594 *, const MethodInfo*))KeyValuePair_2_get_Key_m2103164055_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m921455766(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t463760594 *, uint8_t, const MethodInfo*))KeyValuePair_2_set_Key_m1003893720_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::get_Value()
#define KeyValuePair_2_get_Value_m4012412885(__this, method) ((  List_1_t1104940528 * (*) (KeyValuePair_2_t463760594 *, const MethodInfo*))KeyValuePair_2_get_Value_m3702007291_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m209937814(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t463760594 *, List_1_t1104940528 *, const MethodInfo*))KeyValuePair_2_set_Value_m801364952_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::ToString()
#define KeyValuePair_2_ToString_m3629490828(__this, method) ((  String_t* (*) (KeyValuePair_2_t463760594 *, const MethodInfo*))KeyValuePair_2_ToString_m2025062384_gshared)(__this, method)
