﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// shaderBuff
struct  shaderBuff_t2982920664  : public Il2CppObject
{
public:
	// System.Single shaderBuff::duration
	float ___duration_0;
	// System.Single shaderBuff::interval
	float ___interval_1;
	// System.Single shaderBuff::Intension
	float ___Intension_2;
	// UnityEngine.Color shaderBuff::_RimColor
	Color_t4194546905  ____RimColor_3;
	// System.Single shaderBuff::susanooScale
	float ___susanooScale_4;
	// UnityEngine.Vector3 shaderBuff::offset
	Vector3_t4282066566  ___offset_5;
	// UnityEngine.Vector2 shaderBuff::_blurCenter
	Vector2_t4282066565  ____blurCenter_6;
	// UnityEngine.Color shaderBuff::_bloomColor
	Color_t4194546905  ____bloomColor_7;

public:
	inline static int32_t get_offset_of_duration_0() { return static_cast<int32_t>(offsetof(shaderBuff_t2982920664, ___duration_0)); }
	inline float get_duration_0() const { return ___duration_0; }
	inline float* get_address_of_duration_0() { return &___duration_0; }
	inline void set_duration_0(float value)
	{
		___duration_0 = value;
	}

	inline static int32_t get_offset_of_interval_1() { return static_cast<int32_t>(offsetof(shaderBuff_t2982920664, ___interval_1)); }
	inline float get_interval_1() const { return ___interval_1; }
	inline float* get_address_of_interval_1() { return &___interval_1; }
	inline void set_interval_1(float value)
	{
		___interval_1 = value;
	}

	inline static int32_t get_offset_of_Intension_2() { return static_cast<int32_t>(offsetof(shaderBuff_t2982920664, ___Intension_2)); }
	inline float get_Intension_2() const { return ___Intension_2; }
	inline float* get_address_of_Intension_2() { return &___Intension_2; }
	inline void set_Intension_2(float value)
	{
		___Intension_2 = value;
	}

	inline static int32_t get_offset_of__RimColor_3() { return static_cast<int32_t>(offsetof(shaderBuff_t2982920664, ____RimColor_3)); }
	inline Color_t4194546905  get__RimColor_3() const { return ____RimColor_3; }
	inline Color_t4194546905 * get_address_of__RimColor_3() { return &____RimColor_3; }
	inline void set__RimColor_3(Color_t4194546905  value)
	{
		____RimColor_3 = value;
	}

	inline static int32_t get_offset_of_susanooScale_4() { return static_cast<int32_t>(offsetof(shaderBuff_t2982920664, ___susanooScale_4)); }
	inline float get_susanooScale_4() const { return ___susanooScale_4; }
	inline float* get_address_of_susanooScale_4() { return &___susanooScale_4; }
	inline void set_susanooScale_4(float value)
	{
		___susanooScale_4 = value;
	}

	inline static int32_t get_offset_of_offset_5() { return static_cast<int32_t>(offsetof(shaderBuff_t2982920664, ___offset_5)); }
	inline Vector3_t4282066566  get_offset_5() const { return ___offset_5; }
	inline Vector3_t4282066566 * get_address_of_offset_5() { return &___offset_5; }
	inline void set_offset_5(Vector3_t4282066566  value)
	{
		___offset_5 = value;
	}

	inline static int32_t get_offset_of__blurCenter_6() { return static_cast<int32_t>(offsetof(shaderBuff_t2982920664, ____blurCenter_6)); }
	inline Vector2_t4282066565  get__blurCenter_6() const { return ____blurCenter_6; }
	inline Vector2_t4282066565 * get_address_of__blurCenter_6() { return &____blurCenter_6; }
	inline void set__blurCenter_6(Vector2_t4282066565  value)
	{
		____blurCenter_6 = value;
	}

	inline static int32_t get_offset_of__bloomColor_7() { return static_cast<int32_t>(offsetof(shaderBuff_t2982920664, ____bloomColor_7)); }
	inline Color_t4194546905  get__bloomColor_7() const { return ____bloomColor_7; }
	inline Color_t4194546905 * get_address_of__bloomColor_7() { return &____bloomColor_7; }
	inline void set__bloomColor_7(Color_t4194546905  value)
	{
		____bloomColor_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
