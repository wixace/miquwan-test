﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t667441552;
// System.Collections.Generic.Dictionary`2<System.UInt32,Mihua.Utils.WaitForSeconds>
struct Dictionary_2_t3214597841;
// System.Collections.Generic.Dictionary`2<System.UInt32,System.Collections.IEnumerator>
struct Dictionary_2_t3461725185;
// System.Collections.Generic.List`1<Mihua.Utils.WaitForSeconds>
struct List_1_t290666119;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BehaviourUtil
struct  BehaviourUtil_t1147831871  : public MonoBehaviour_t667441552
{
public:

public:
};

struct BehaviourUtil_t1147831871_StaticFields
{
public:
	// UnityEngine.MonoBehaviour BehaviourUtil::globalCoroutine
	MonoBehaviour_t667441552 * ___globalCoroutine_2;
	// System.UInt32 BehaviourUtil::curID
	uint32_t ___curID_3;
	// System.Collections.Generic.Dictionary`2<System.UInt32,Mihua.Utils.WaitForSeconds> BehaviourUtil::allWaitSecondDic
	Dictionary_2_t3214597841 * ___allWaitSecondDic_4;
	// System.Collections.Generic.Dictionary`2<System.UInt32,System.Collections.IEnumerator> BehaviourUtil::allCoroutineDic
	Dictionary_2_t3461725185 * ___allCoroutineDic_5;
	// System.Collections.Generic.List`1<Mihua.Utils.WaitForSeconds> BehaviourUtil::allCoroutineList
	List_1_t290666119 * ___allCoroutineList_6;

public:
	inline static int32_t get_offset_of_globalCoroutine_2() { return static_cast<int32_t>(offsetof(BehaviourUtil_t1147831871_StaticFields, ___globalCoroutine_2)); }
	inline MonoBehaviour_t667441552 * get_globalCoroutine_2() const { return ___globalCoroutine_2; }
	inline MonoBehaviour_t667441552 ** get_address_of_globalCoroutine_2() { return &___globalCoroutine_2; }
	inline void set_globalCoroutine_2(MonoBehaviour_t667441552 * value)
	{
		___globalCoroutine_2 = value;
		Il2CppCodeGenWriteBarrier(&___globalCoroutine_2, value);
	}

	inline static int32_t get_offset_of_curID_3() { return static_cast<int32_t>(offsetof(BehaviourUtil_t1147831871_StaticFields, ___curID_3)); }
	inline uint32_t get_curID_3() const { return ___curID_3; }
	inline uint32_t* get_address_of_curID_3() { return &___curID_3; }
	inline void set_curID_3(uint32_t value)
	{
		___curID_3 = value;
	}

	inline static int32_t get_offset_of_allWaitSecondDic_4() { return static_cast<int32_t>(offsetof(BehaviourUtil_t1147831871_StaticFields, ___allWaitSecondDic_4)); }
	inline Dictionary_2_t3214597841 * get_allWaitSecondDic_4() const { return ___allWaitSecondDic_4; }
	inline Dictionary_2_t3214597841 ** get_address_of_allWaitSecondDic_4() { return &___allWaitSecondDic_4; }
	inline void set_allWaitSecondDic_4(Dictionary_2_t3214597841 * value)
	{
		___allWaitSecondDic_4 = value;
		Il2CppCodeGenWriteBarrier(&___allWaitSecondDic_4, value);
	}

	inline static int32_t get_offset_of_allCoroutineDic_5() { return static_cast<int32_t>(offsetof(BehaviourUtil_t1147831871_StaticFields, ___allCoroutineDic_5)); }
	inline Dictionary_2_t3461725185 * get_allCoroutineDic_5() const { return ___allCoroutineDic_5; }
	inline Dictionary_2_t3461725185 ** get_address_of_allCoroutineDic_5() { return &___allCoroutineDic_5; }
	inline void set_allCoroutineDic_5(Dictionary_2_t3461725185 * value)
	{
		___allCoroutineDic_5 = value;
		Il2CppCodeGenWriteBarrier(&___allCoroutineDic_5, value);
	}

	inline static int32_t get_offset_of_allCoroutineList_6() { return static_cast<int32_t>(offsetof(BehaviourUtil_t1147831871_StaticFields, ___allCoroutineList_6)); }
	inline List_1_t290666119 * get_allCoroutineList_6() const { return ___allCoroutineList_6; }
	inline List_1_t290666119 ** get_address_of_allCoroutineList_6() { return &___allCoroutineList_6; }
	inline void set_allCoroutineList_6(List_1_t290666119 * value)
	{
		___allCoroutineList_6 = value;
		Il2CppCodeGenWriteBarrier(&___allCoroutineList_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
