﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_meryaizuJemrou217
struct M_meryaizuJemrou217_t3998547888;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_meryaizuJemrou217::.ctor()
extern "C"  void M_meryaizuJemrou217__ctor_m4275777459 (M_meryaizuJemrou217_t3998547888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_meryaizuJemrou217::M_mepe0(System.String[],System.Int32)
extern "C"  void M_meryaizuJemrou217_M_mepe0_m4018994873 (M_meryaizuJemrou217_t3998547888 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_meryaizuJemrou217::M_trelemlorSaycibe1(System.String[],System.Int32)
extern "C"  void M_meryaizuJemrou217_M_trelemlorSaycibe1_m1065135317 (M_meryaizuJemrou217_t3998547888 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
