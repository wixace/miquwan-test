﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameObjectVisitor/<FindChildren>c__AnonStorey164
struct U3CFindChildrenU3Ec__AnonStorey164_t1031132260;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void GameObjectVisitor/<FindChildren>c__AnonStorey164::.ctor()
extern "C"  void U3CFindChildrenU3Ec__AnonStorey164__ctor_m119513783 (U3CFindChildrenU3Ec__AnonStorey164_t1031132260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameObjectVisitor/<FindChildren>c__AnonStorey164::<>m__48E(UnityEngine.GameObject)
extern "C"  bool U3CFindChildrenU3Ec__AnonStorey164_U3CU3Em__48E_m1278497039 (U3CFindChildrenU3Ec__AnonStorey164_t1031132260 * __this, GameObject_t3674682005 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
