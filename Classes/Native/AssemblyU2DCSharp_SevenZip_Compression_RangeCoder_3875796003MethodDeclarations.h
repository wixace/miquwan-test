﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Compression.RangeCoder.Encoder
struct Encoder_t2248006694;
// SevenZip.Compression.RangeCoder.BitEncoder[]
struct BitEncoderU5BU5D_t2970556732;
// SevenZip.Compression.RangeCoder.BitTreeEncoder
struct BitTreeEncoder_t3875796003;
struct BitTreeEncoder_t3875796003_marshaled_pinvoke;
struct BitTreeEncoder_t3875796003_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_3875796003.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_2248006694.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_1052492065.h"

// System.Void SevenZip.Compression.RangeCoder.BitTreeEncoder::.ctor(System.Int32)
extern "C"  void BitTreeEncoder__ctor_m2113982359 (BitTreeEncoder_t3875796003 * __this, int32_t ___numBitLevels0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.BitTreeEncoder::Init()
extern "C"  void BitTreeEncoder_Init_m331332174 (BitTreeEncoder_t3875796003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.BitTreeEncoder::Encode(SevenZip.Compression.RangeCoder.Encoder,System.UInt32)
extern "C"  void BitTreeEncoder_Encode_m410093850 (BitTreeEncoder_t3875796003 * __this, Encoder_t2248006694 * ___rangeEncoder0, uint32_t ___symbol1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.BitTreeEncoder::ReverseEncode(SevenZip.Compression.RangeCoder.Encoder,System.UInt32)
extern "C"  void BitTreeEncoder_ReverseEncode_m1812585906 (BitTreeEncoder_t3875796003 * __this, Encoder_t2248006694 * ___rangeEncoder0, uint32_t ___symbol1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.RangeCoder.BitTreeEncoder::GetPrice(System.UInt32)
extern "C"  uint32_t BitTreeEncoder_GetPrice_m3105069710 (BitTreeEncoder_t3875796003 * __this, uint32_t ___symbol0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.RangeCoder.BitTreeEncoder::ReverseGetPrice(System.UInt32)
extern "C"  uint32_t BitTreeEncoder_ReverseGetPrice_m1074266748 (BitTreeEncoder_t3875796003 * __this, uint32_t ___symbol0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.RangeCoder.BitTreeEncoder::ReverseGetPrice(SevenZip.Compression.RangeCoder.BitEncoder[],System.UInt32,System.Int32,System.UInt32)
extern "C"  uint32_t BitTreeEncoder_ReverseGetPrice_m3377875054 (Il2CppObject * __this /* static, unused */, BitEncoderU5BU5D_t2970556732* ___Models0, uint32_t ___startIndex1, int32_t ___NumBitLevels2, uint32_t ___symbol3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.BitTreeEncoder::ReverseEncode(SevenZip.Compression.RangeCoder.BitEncoder[],System.UInt32,SevenZip.Compression.RangeCoder.Encoder,System.Int32,System.UInt32)
extern "C"  void BitTreeEncoder_ReverseEncode_m2698759868 (Il2CppObject * __this /* static, unused */, BitEncoderU5BU5D_t2970556732* ___Models0, uint32_t ___startIndex1, Encoder_t2248006694 * ___rangeEncoder2, int32_t ___NumBitLevels3, uint32_t ___symbol4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.BitTreeEncoder::ilo_Encode1(SevenZip.Compression.RangeCoder.BitEncoder&,SevenZip.Compression.RangeCoder.Encoder,System.UInt32)
extern "C"  void BitTreeEncoder_ilo_Encode1_m3415943419 (Il2CppObject * __this /* static, unused */, BitEncoder_t1052492065 * ____this0, Encoder_t2248006694 * ___encoder1, uint32_t ___symbol2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.RangeCoder.BitTreeEncoder::ilo_GetPrice2(SevenZip.Compression.RangeCoder.BitEncoder&,System.UInt32)
extern "C"  uint32_t BitTreeEncoder_ilo_GetPrice2_m460817982 (Il2CppObject * __this /* static, unused */, BitEncoder_t1052492065 * ____this0, uint32_t ___symbol1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct BitTreeEncoder_t3875796003;
struct BitTreeEncoder_t3875796003_marshaled_pinvoke;

extern "C" void BitTreeEncoder_t3875796003_marshal_pinvoke(const BitTreeEncoder_t3875796003& unmarshaled, BitTreeEncoder_t3875796003_marshaled_pinvoke& marshaled);
extern "C" void BitTreeEncoder_t3875796003_marshal_pinvoke_back(const BitTreeEncoder_t3875796003_marshaled_pinvoke& marshaled, BitTreeEncoder_t3875796003& unmarshaled);
extern "C" void BitTreeEncoder_t3875796003_marshal_pinvoke_cleanup(BitTreeEncoder_t3875796003_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct BitTreeEncoder_t3875796003;
struct BitTreeEncoder_t3875796003_marshaled_com;

extern "C" void BitTreeEncoder_t3875796003_marshal_com(const BitTreeEncoder_t3875796003& unmarshaled, BitTreeEncoder_t3875796003_marshaled_com& marshaled);
extern "C" void BitTreeEncoder_t3875796003_marshal_com_back(const BitTreeEncoder_t3875796003_marshaled_com& marshaled, BitTreeEncoder_t3875796003& unmarshaled);
extern "C" void BitTreeEncoder_t3875796003_marshal_com_cleanup(BitTreeEncoder_t3875796003_marshaled_com& marshaled);
