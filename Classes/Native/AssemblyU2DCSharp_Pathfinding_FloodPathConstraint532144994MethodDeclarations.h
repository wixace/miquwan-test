﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.FloodPathConstraint
struct FloodPathConstraint_t532144994;
// Pathfinding.FloodPath
struct FloodPath_t3766979749;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// Pathfinding.NNConstraint
struct NNConstraint_t758567699;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_FloodPath3766979749.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_NNConstraint758567699.h"

// System.Void Pathfinding.FloodPathConstraint::.ctor(Pathfinding.FloodPath)
extern "C"  void FloodPathConstraint__ctor_m557350276 (FloodPathConstraint_t532144994 * __this, FloodPath_t3766979749 * ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.FloodPathConstraint::Suitable(Pathfinding.GraphNode)
extern "C"  bool FloodPathConstraint_Suitable_m294949290 (FloodPathConstraint_t532144994 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.FloodPathConstraint::ilo_Suitable1(Pathfinding.NNConstraint,Pathfinding.GraphNode)
extern "C"  bool FloodPathConstraint_ilo_Suitable1_m3832846205 (Il2CppObject * __this /* static, unused */, NNConstraint_t758567699 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
