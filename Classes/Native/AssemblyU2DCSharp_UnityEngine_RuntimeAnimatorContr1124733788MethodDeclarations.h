﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_RuntimeAnimatorControllerGenerated
struct UnityEngine_RuntimeAnimatorControllerGenerated_t1124733788;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_RuntimeAnimatorControllerGenerated::.ctor()
extern "C"  void UnityEngine_RuntimeAnimatorControllerGenerated__ctor_m3661586623 (UnityEngine_RuntimeAnimatorControllerGenerated_t1124733788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RuntimeAnimatorControllerGenerated::RuntimeAnimatorController_RuntimeAnimatorController1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RuntimeAnimatorControllerGenerated_RuntimeAnimatorController_RuntimeAnimatorController1_m773791651 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RuntimeAnimatorControllerGenerated::RuntimeAnimatorController_animationClips(JSVCall)
extern "C"  void UnityEngine_RuntimeAnimatorControllerGenerated_RuntimeAnimatorController_animationClips_m262831071 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RuntimeAnimatorControllerGenerated::__Register()
extern "C"  void UnityEngine_RuntimeAnimatorControllerGenerated___Register_m837532520 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
