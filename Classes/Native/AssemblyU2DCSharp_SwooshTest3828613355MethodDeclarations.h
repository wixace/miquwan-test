﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SwooshTest
struct SwooshTest_t3828613355;
// MeleeWeaponTrail
struct MeleeWeaponTrail_t72048470;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MeleeWeaponTrail72048470.h"

// System.Void SwooshTest::.ctor()
extern "C"  void SwooshTest__ctor_m1904373776 (SwooshTest_t3828613355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwooshTest::Start()
extern "C"  void SwooshTest_Start_m851511568 (SwooshTest_t3828613355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwooshTest::Update()
extern "C"  void SwooshTest_Update_m632907005 (SwooshTest_t3828613355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwooshTest::ilo_set_Emit1(MeleeWeaponTrail,System.Boolean)
extern "C"  void SwooshTest_ilo_set_Emit1_m2561795287 (Il2CppObject * __this /* static, unused */, MeleeWeaponTrail_t72048470 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
