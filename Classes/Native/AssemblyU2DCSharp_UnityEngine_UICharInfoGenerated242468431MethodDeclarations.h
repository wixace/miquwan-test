﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_UICharInfoGenerated
struct UnityEngine_UICharInfoGenerated_t242468431;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine_UICharInfoGenerated::.ctor()
extern "C"  void UnityEngine_UICharInfoGenerated__ctor_m2834422268 (UnityEngine_UICharInfoGenerated_t242468431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UICharInfoGenerated::.cctor()
extern "C"  void UnityEngine_UICharInfoGenerated__cctor_m1485648177 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_UICharInfoGenerated::UICharInfo_UICharInfo1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_UICharInfoGenerated_UICharInfo_UICharInfo1_m1873453110 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UICharInfoGenerated::UICharInfo_cursorPos(JSVCall)
extern "C"  void UnityEngine_UICharInfoGenerated_UICharInfo_cursorPos_m947129704 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UICharInfoGenerated::UICharInfo_charWidth(JSVCall)
extern "C"  void UnityEngine_UICharInfoGenerated_UICharInfo_charWidth_m1058213046 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UICharInfoGenerated::__Register()
extern "C"  void UnityEngine_UICharInfoGenerated___Register_m2767487051 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_UICharInfoGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_UICharInfoGenerated_ilo_getObject1_m311105978 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UICharInfoGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_UICharInfoGenerated_ilo_addJSCSRel2_m3603760953 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_UICharInfoGenerated::ilo_getVector2S3(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_UICharInfoGenerated_ilo_getVector2S3_m559053194 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
