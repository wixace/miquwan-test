﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlowControl.FlowContainerAnd
struct FlowContainerAnd_t1365439512;
// FlowControl.FlowContainerBase
struct FlowContainerBase_t4203254394;
// FlowControl.FlowBase
struct FlowBase_t3680091731;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FlowControl_FlowContainerBase4203254394.h"
#include "AssemblyU2DCSharp_FlowControl_FlowBase3680091731.h"

// System.Void FlowControl.FlowContainerAnd::.ctor()
extern "C"  void FlowContainerAnd__ctor_m3480928280 (FlowContainerAnd_t1365439512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowContainerAnd::Activate()
extern "C"  void FlowContainerAnd_Activate_m2344640735 (FlowContainerAnd_t1365439512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowContainerAnd::FlowUpdate(System.Single)
extern "C"  void FlowContainerAnd_FlowUpdate_m3615536840 (FlowContainerAnd_t1365439512 * __this, float ___duringTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FlowControl.FlowContainerAnd::ilo_get_NumChildren1(FlowControl.FlowContainerBase)
extern "C"  int32_t FlowContainerAnd_ilo_get_NumChildren1_m2283209323 (Il2CppObject * __this /* static, unused */, FlowContainerBase_t4203254394 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FlowControl.FlowContainerAnd::ilo_get_isComplete2(FlowControl.FlowBase)
extern "C"  bool FlowContainerAnd_ilo_get_isComplete2_m789900375 (Il2CppObject * __this /* static, unused */, FlowBase_t3680091731 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowContainerAnd::ilo_set_isComplete3(FlowControl.FlowBase,System.Boolean)
extern "C"  void FlowContainerAnd_ilo_set_isComplete3_m2018416549 (Il2CppObject * __this /* static, unused */, FlowBase_t3680091731 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
