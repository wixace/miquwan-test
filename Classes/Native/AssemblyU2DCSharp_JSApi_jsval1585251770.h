﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSApi/jsval
struct  jsval_t1585251770 
{
public:
	union
	{
		struct
		{
		};
		uint8_t jsval_t1585251770__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: JSApi/jsval
struct jsval_t1585251770_marshaled_pinvoke
{
	union
	{
		struct
		{
		};
		uint8_t jsval_t1585251770__padding[1];
	};
};
// Native definition for marshalling of: JSApi/jsval
struct jsval_t1585251770_marshaled_com
{
	union
	{
		struct
		{
		};
		uint8_t jsval_t1585251770__padding[1];
	};
};
