﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RVO.RVOQuadtree
struct RVOQuadtree_t4291712126;
// Pathfinding.RVO.Sampled.Agent
struct Agent_t1290054243;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_Sampled_Agent1290054243.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_RVOQuadtree_Node2108618958.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_RVOQuadtree4291712126.h"

// System.Void Pathfinding.RVO.RVOQuadtree::.ctor()
extern "C"  void RVOQuadtree__ctor_m2861492968 (RVOQuadtree_t4291712126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOQuadtree::Clear()
extern "C"  void RVOQuadtree_Clear_m267626259 (RVOQuadtree_t4291712126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOQuadtree::SetBounds(UnityEngine.Rect)
extern "C"  void RVOQuadtree_SetBounds_m1160830274 (RVOQuadtree_t4291712126 * __this, Rect_t4241904616  ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.RVO.RVOQuadtree::GetNodeIndex()
extern "C"  int32_t RVOQuadtree_GetNodeIndex_m2835653192 (RVOQuadtree_t4291712126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOQuadtree::Insert(Pathfinding.RVO.Sampled.Agent)
extern "C"  void RVOQuadtree_Insert_m3254734681 (RVOQuadtree_t4291712126 * __this, Agent_t1290054243 * ___agent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOQuadtree::Query(UnityEngine.Vector2,System.Single,Pathfinding.RVO.Sampled.Agent)
extern "C"  void RVOQuadtree_Query_m186546137 (RVOQuadtree_t4291712126 * __this, Vector2_t4282066565  ___p0, float ___radius1, Agent_t1290054243 * ___agent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RVO.RVOQuadtree::QueryRec(System.Int32,UnityEngine.Vector2,System.Single,Pathfinding.RVO.Sampled.Agent,UnityEngine.Rect)
extern "C"  float RVOQuadtree_QueryRec_m2396472545 (RVOQuadtree_t4291712126 * __this, int32_t ___i0, Vector2_t4282066565  ___p1, float ___radius2, Agent_t1290054243 * ___agent3, Rect_t4241904616  ___r4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOQuadtree::DebugDraw()
extern "C"  void RVOQuadtree_DebugDraw_m1184262653 (RVOQuadtree_t4291712126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOQuadtree::DebugDrawRec(System.Int32,UnityEngine.Rect)
extern "C"  void RVOQuadtree_DebugDrawRec_m1396379205 (RVOQuadtree_t4291712126 * __this, int32_t ___i0, Rect_t4241904616  ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOQuadtree::ilo_Add1(Pathfinding.RVO.RVOQuadtree/Node&,Pathfinding.RVO.Sampled.Agent)
extern "C"  void RVOQuadtree_ilo_Add1_m1603187651 (Il2CppObject * __this /* static, unused */, Node_t2108618958 * ____this0, Agent_t1290054243 * ___agent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RVO.RVOQuadtree::ilo_QueryRec2(Pathfinding.RVO.RVOQuadtree,System.Int32,UnityEngine.Vector2,System.Single,Pathfinding.RVO.Sampled.Agent,UnityEngine.Rect)
extern "C"  float RVOQuadtree_ilo_QueryRec2_m2883642841 (Il2CppObject * __this /* static, unused */, RVOQuadtree_t4291712126 * ____this0, int32_t ___i1, Vector2_t4282066565  ___p2, float ___radius3, Agent_t1290054243 * ___agent4, Rect_t4241904616  ___r5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.RVOQuadtree::ilo_DebugDrawRec3(Pathfinding.RVO.RVOQuadtree,System.Int32,UnityEngine.Rect)
extern "C"  void RVOQuadtree_ilo_DebugDrawRec3_m2681930038 (Il2CppObject * __this /* static, unused */, RVOQuadtree_t4291712126 * ____this0, int32_t ___i1, Rect_t4241904616  ___r2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
