﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.Shader
struct Shader_t3191267369;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iOSUIScreenEffect
struct  iOSUIScreenEffect_t1658945310  : public MonoBehaviour_t667441552
{
public:
	// System.Single iOSUIScreenEffect::brightness
	float ___brightness_2;
	// System.Single iOSUIScreenEffect::saturation
	float ___saturation_3;
	// UnityEngine.Material iOSUIScreenEffect::_material
	Material_t3870600107 * ____material_4;
	// UnityEngine.Shader iOSUIScreenEffect::_shader
	Shader_t3191267369 * ____shader_5;

public:
	inline static int32_t get_offset_of_brightness_2() { return static_cast<int32_t>(offsetof(iOSUIScreenEffect_t1658945310, ___brightness_2)); }
	inline float get_brightness_2() const { return ___brightness_2; }
	inline float* get_address_of_brightness_2() { return &___brightness_2; }
	inline void set_brightness_2(float value)
	{
		___brightness_2 = value;
	}

	inline static int32_t get_offset_of_saturation_3() { return static_cast<int32_t>(offsetof(iOSUIScreenEffect_t1658945310, ___saturation_3)); }
	inline float get_saturation_3() const { return ___saturation_3; }
	inline float* get_address_of_saturation_3() { return &___saturation_3; }
	inline void set_saturation_3(float value)
	{
		___saturation_3 = value;
	}

	inline static int32_t get_offset_of__material_4() { return static_cast<int32_t>(offsetof(iOSUIScreenEffect_t1658945310, ____material_4)); }
	inline Material_t3870600107 * get__material_4() const { return ____material_4; }
	inline Material_t3870600107 ** get_address_of__material_4() { return &____material_4; }
	inline void set__material_4(Material_t3870600107 * value)
	{
		____material_4 = value;
		Il2CppCodeGenWriteBarrier(&____material_4, value);
	}

	inline static int32_t get_offset_of__shader_5() { return static_cast<int32_t>(offsetof(iOSUIScreenEffect_t1658945310, ____shader_5)); }
	inline Shader_t3191267369 * get__shader_5() const { return ____shader_5; }
	inline Shader_t3191267369 ** get_address_of__shader_5() { return &____shader_5; }
	inline void set__shader_5(Shader_t3191267369 * value)
	{
		____shader_5 = value;
		Il2CppCodeGenWriteBarrier(&____shader_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
