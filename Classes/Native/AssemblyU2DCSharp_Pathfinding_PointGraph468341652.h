﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;
// System.String
struct String_t;
// Pathfinding.Int3[]
struct Int3U5BU5D_t516284607;
// System.Collections.Generic.Dictionary`2<Pathfinding.Int3,Pathfinding.PointNode>
struct Dictionary_2_t44513805;
// Pathfinding.PointNode[]
struct PointNodeU5BU5D_t1844325917;

#include "AssemblyU2DCSharp_Pathfinding_NavGraph1254319713.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.PointGraph
struct  PointGraph_t468341652  : public NavGraph_t1254319713
{
public:
	// UnityEngine.Transform Pathfinding.PointGraph::root
	Transform_t1659122786 * ___root_11;
	// System.String Pathfinding.PointGraph::searchTag
	String_t* ___searchTag_12;
	// System.Single Pathfinding.PointGraph::maxDistance
	float ___maxDistance_13;
	// UnityEngine.Vector3 Pathfinding.PointGraph::limits
	Vector3_t4282066566  ___limits_14;
	// System.Boolean Pathfinding.PointGraph::raycast
	bool ___raycast_15;
	// System.Boolean Pathfinding.PointGraph::use2DPhysics
	bool ___use2DPhysics_16;
	// System.Boolean Pathfinding.PointGraph::thickRaycast
	bool ___thickRaycast_17;
	// System.Single Pathfinding.PointGraph::thickRaycastRadius
	float ___thickRaycastRadius_18;
	// System.Boolean Pathfinding.PointGraph::recursive
	bool ___recursive_19;
	// System.Boolean Pathfinding.PointGraph::autoLinkNodes
	bool ___autoLinkNodes_20;
	// UnityEngine.LayerMask Pathfinding.PointGraph::mask
	LayerMask_t3236759763  ___mask_21;
	// System.Boolean Pathfinding.PointGraph::optimizeForSparseGraph
	bool ___optimizeForSparseGraph_22;
	// System.Boolean Pathfinding.PointGraph::optimizeFor2D
	bool ___optimizeFor2D_23;
	// System.Collections.Generic.Dictionary`2<Pathfinding.Int3,Pathfinding.PointNode> Pathfinding.PointGraph::nodeLookup
	Dictionary_2_t44513805 * ___nodeLookup_25;
	// Pathfinding.Int3 Pathfinding.PointGraph::minLookup
	Int3_t1974045594  ___minLookup_26;
	// Pathfinding.Int3 Pathfinding.PointGraph::maxLookup
	Int3_t1974045594  ___maxLookup_27;
	// Pathfinding.Int3 Pathfinding.PointGraph::lookupCellSize
	Int3_t1974045594  ___lookupCellSize_28;
	// Pathfinding.PointNode[] Pathfinding.PointGraph::nodes
	PointNodeU5BU5D_t1844325917* ___nodes_29;
	// System.Int32 Pathfinding.PointGraph::nodeCount
	int32_t ___nodeCount_30;

public:
	inline static int32_t get_offset_of_root_11() { return static_cast<int32_t>(offsetof(PointGraph_t468341652, ___root_11)); }
	inline Transform_t1659122786 * get_root_11() const { return ___root_11; }
	inline Transform_t1659122786 ** get_address_of_root_11() { return &___root_11; }
	inline void set_root_11(Transform_t1659122786 * value)
	{
		___root_11 = value;
		Il2CppCodeGenWriteBarrier(&___root_11, value);
	}

	inline static int32_t get_offset_of_searchTag_12() { return static_cast<int32_t>(offsetof(PointGraph_t468341652, ___searchTag_12)); }
	inline String_t* get_searchTag_12() const { return ___searchTag_12; }
	inline String_t** get_address_of_searchTag_12() { return &___searchTag_12; }
	inline void set_searchTag_12(String_t* value)
	{
		___searchTag_12 = value;
		Il2CppCodeGenWriteBarrier(&___searchTag_12, value);
	}

	inline static int32_t get_offset_of_maxDistance_13() { return static_cast<int32_t>(offsetof(PointGraph_t468341652, ___maxDistance_13)); }
	inline float get_maxDistance_13() const { return ___maxDistance_13; }
	inline float* get_address_of_maxDistance_13() { return &___maxDistance_13; }
	inline void set_maxDistance_13(float value)
	{
		___maxDistance_13 = value;
	}

	inline static int32_t get_offset_of_limits_14() { return static_cast<int32_t>(offsetof(PointGraph_t468341652, ___limits_14)); }
	inline Vector3_t4282066566  get_limits_14() const { return ___limits_14; }
	inline Vector3_t4282066566 * get_address_of_limits_14() { return &___limits_14; }
	inline void set_limits_14(Vector3_t4282066566  value)
	{
		___limits_14 = value;
	}

	inline static int32_t get_offset_of_raycast_15() { return static_cast<int32_t>(offsetof(PointGraph_t468341652, ___raycast_15)); }
	inline bool get_raycast_15() const { return ___raycast_15; }
	inline bool* get_address_of_raycast_15() { return &___raycast_15; }
	inline void set_raycast_15(bool value)
	{
		___raycast_15 = value;
	}

	inline static int32_t get_offset_of_use2DPhysics_16() { return static_cast<int32_t>(offsetof(PointGraph_t468341652, ___use2DPhysics_16)); }
	inline bool get_use2DPhysics_16() const { return ___use2DPhysics_16; }
	inline bool* get_address_of_use2DPhysics_16() { return &___use2DPhysics_16; }
	inline void set_use2DPhysics_16(bool value)
	{
		___use2DPhysics_16 = value;
	}

	inline static int32_t get_offset_of_thickRaycast_17() { return static_cast<int32_t>(offsetof(PointGraph_t468341652, ___thickRaycast_17)); }
	inline bool get_thickRaycast_17() const { return ___thickRaycast_17; }
	inline bool* get_address_of_thickRaycast_17() { return &___thickRaycast_17; }
	inline void set_thickRaycast_17(bool value)
	{
		___thickRaycast_17 = value;
	}

	inline static int32_t get_offset_of_thickRaycastRadius_18() { return static_cast<int32_t>(offsetof(PointGraph_t468341652, ___thickRaycastRadius_18)); }
	inline float get_thickRaycastRadius_18() const { return ___thickRaycastRadius_18; }
	inline float* get_address_of_thickRaycastRadius_18() { return &___thickRaycastRadius_18; }
	inline void set_thickRaycastRadius_18(float value)
	{
		___thickRaycastRadius_18 = value;
	}

	inline static int32_t get_offset_of_recursive_19() { return static_cast<int32_t>(offsetof(PointGraph_t468341652, ___recursive_19)); }
	inline bool get_recursive_19() const { return ___recursive_19; }
	inline bool* get_address_of_recursive_19() { return &___recursive_19; }
	inline void set_recursive_19(bool value)
	{
		___recursive_19 = value;
	}

	inline static int32_t get_offset_of_autoLinkNodes_20() { return static_cast<int32_t>(offsetof(PointGraph_t468341652, ___autoLinkNodes_20)); }
	inline bool get_autoLinkNodes_20() const { return ___autoLinkNodes_20; }
	inline bool* get_address_of_autoLinkNodes_20() { return &___autoLinkNodes_20; }
	inline void set_autoLinkNodes_20(bool value)
	{
		___autoLinkNodes_20 = value;
	}

	inline static int32_t get_offset_of_mask_21() { return static_cast<int32_t>(offsetof(PointGraph_t468341652, ___mask_21)); }
	inline LayerMask_t3236759763  get_mask_21() const { return ___mask_21; }
	inline LayerMask_t3236759763 * get_address_of_mask_21() { return &___mask_21; }
	inline void set_mask_21(LayerMask_t3236759763  value)
	{
		___mask_21 = value;
	}

	inline static int32_t get_offset_of_optimizeForSparseGraph_22() { return static_cast<int32_t>(offsetof(PointGraph_t468341652, ___optimizeForSparseGraph_22)); }
	inline bool get_optimizeForSparseGraph_22() const { return ___optimizeForSparseGraph_22; }
	inline bool* get_address_of_optimizeForSparseGraph_22() { return &___optimizeForSparseGraph_22; }
	inline void set_optimizeForSparseGraph_22(bool value)
	{
		___optimizeForSparseGraph_22 = value;
	}

	inline static int32_t get_offset_of_optimizeFor2D_23() { return static_cast<int32_t>(offsetof(PointGraph_t468341652, ___optimizeFor2D_23)); }
	inline bool get_optimizeFor2D_23() const { return ___optimizeFor2D_23; }
	inline bool* get_address_of_optimizeFor2D_23() { return &___optimizeFor2D_23; }
	inline void set_optimizeFor2D_23(bool value)
	{
		___optimizeFor2D_23 = value;
	}

	inline static int32_t get_offset_of_nodeLookup_25() { return static_cast<int32_t>(offsetof(PointGraph_t468341652, ___nodeLookup_25)); }
	inline Dictionary_2_t44513805 * get_nodeLookup_25() const { return ___nodeLookup_25; }
	inline Dictionary_2_t44513805 ** get_address_of_nodeLookup_25() { return &___nodeLookup_25; }
	inline void set_nodeLookup_25(Dictionary_2_t44513805 * value)
	{
		___nodeLookup_25 = value;
		Il2CppCodeGenWriteBarrier(&___nodeLookup_25, value);
	}

	inline static int32_t get_offset_of_minLookup_26() { return static_cast<int32_t>(offsetof(PointGraph_t468341652, ___minLookup_26)); }
	inline Int3_t1974045594  get_minLookup_26() const { return ___minLookup_26; }
	inline Int3_t1974045594 * get_address_of_minLookup_26() { return &___minLookup_26; }
	inline void set_minLookup_26(Int3_t1974045594  value)
	{
		___minLookup_26 = value;
	}

	inline static int32_t get_offset_of_maxLookup_27() { return static_cast<int32_t>(offsetof(PointGraph_t468341652, ___maxLookup_27)); }
	inline Int3_t1974045594  get_maxLookup_27() const { return ___maxLookup_27; }
	inline Int3_t1974045594 * get_address_of_maxLookup_27() { return &___maxLookup_27; }
	inline void set_maxLookup_27(Int3_t1974045594  value)
	{
		___maxLookup_27 = value;
	}

	inline static int32_t get_offset_of_lookupCellSize_28() { return static_cast<int32_t>(offsetof(PointGraph_t468341652, ___lookupCellSize_28)); }
	inline Int3_t1974045594  get_lookupCellSize_28() const { return ___lookupCellSize_28; }
	inline Int3_t1974045594 * get_address_of_lookupCellSize_28() { return &___lookupCellSize_28; }
	inline void set_lookupCellSize_28(Int3_t1974045594  value)
	{
		___lookupCellSize_28 = value;
	}

	inline static int32_t get_offset_of_nodes_29() { return static_cast<int32_t>(offsetof(PointGraph_t468341652, ___nodes_29)); }
	inline PointNodeU5BU5D_t1844325917* get_nodes_29() const { return ___nodes_29; }
	inline PointNodeU5BU5D_t1844325917** get_address_of_nodes_29() { return &___nodes_29; }
	inline void set_nodes_29(PointNodeU5BU5D_t1844325917* value)
	{
		___nodes_29 = value;
		Il2CppCodeGenWriteBarrier(&___nodes_29, value);
	}

	inline static int32_t get_offset_of_nodeCount_30() { return static_cast<int32_t>(offsetof(PointGraph_t468341652, ___nodeCount_30)); }
	inline int32_t get_nodeCount_30() const { return ___nodeCount_30; }
	inline int32_t* get_address_of_nodeCount_30() { return &___nodeCount_30; }
	inline void set_nodeCount_30(int32_t value)
	{
		___nodeCount_30 = value;
	}
};

struct PointGraph_t468341652_StaticFields
{
public:
	// Pathfinding.Int3[] Pathfinding.PointGraph::ThreeDNeighbours
	Int3U5BU5D_t516284607* ___ThreeDNeighbours_24;

public:
	inline static int32_t get_offset_of_ThreeDNeighbours_24() { return static_cast<int32_t>(offsetof(PointGraph_t468341652_StaticFields, ___ThreeDNeighbours_24)); }
	inline Int3U5BU5D_t516284607* get_ThreeDNeighbours_24() const { return ___ThreeDNeighbours_24; }
	inline Int3U5BU5D_t516284607** get_address_of_ThreeDNeighbours_24() { return &___ThreeDNeighbours_24; }
	inline void set_ThreeDNeighbours_24(Int3U5BU5D_t516284607* value)
	{
		___ThreeDNeighbours_24 = value;
		Il2CppCodeGenWriteBarrier(&___ThreeDNeighbours_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
