﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Object>
struct U3CDelayCallExecU3Ec__Iterator3F_1_t486923181;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Object>::.ctor()
extern "C"  void U3CDelayCallExecU3Ec__Iterator3F_1__ctor_m3333958732_gshared (U3CDelayCallExecU3Ec__Iterator3F_1_t486923181 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator3F_1__ctor_m3333958732(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator3F_1_t486923181 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator3F_1__ctor_m3333958732_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator3F_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m439017478_gshared (U3CDelayCallExecU3Ec__Iterator3F_1_t486923181 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator3F_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m439017478(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator3F_1_t486923181 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator3F_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m439017478_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator3F_1_System_Collections_IEnumerator_get_Current_m1614393242_gshared (U3CDelayCallExecU3Ec__Iterator3F_1_t486923181 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator3F_1_System_Collections_IEnumerator_get_Current_m1614393242(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator3F_1_t486923181 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator3F_1_System_Collections_IEnumerator_get_Current_m1614393242_gshared)(__this, method)
// System.Boolean BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Object>::MoveNext()
extern "C"  bool U3CDelayCallExecU3Ec__Iterator3F_1_MoveNext_m1933120552_gshared (U3CDelayCallExecU3Ec__Iterator3F_1_t486923181 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator3F_1_MoveNext_m1933120552(__this, method) ((  bool (*) (U3CDelayCallExecU3Ec__Iterator3F_1_t486923181 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator3F_1_MoveNext_m1933120552_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Object>::Dispose()
extern "C"  void U3CDelayCallExecU3Ec__Iterator3F_1_Dispose_m4081989001_gshared (U3CDelayCallExecU3Ec__Iterator3F_1_t486923181 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator3F_1_Dispose_m4081989001(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator3F_1_t486923181 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator3F_1_Dispose_m4081989001_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Object>::Reset()
extern "C"  void U3CDelayCallExecU3Ec__Iterator3F_1_Reset_m980391673_gshared (U3CDelayCallExecU3Ec__Iterator3F_1_t486923181 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator3F_1_Reset_m980391673(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator3F_1_t486923181 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator3F_1_Reset_m980391673_gshared)(__this, method)
