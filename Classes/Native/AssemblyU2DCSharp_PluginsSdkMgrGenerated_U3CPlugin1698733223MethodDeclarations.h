﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginsSdkMgrGenerated/<PluginsSdkMgr_ShowDialog_GetDelegate_member16_arg5>c__AnonStorey75
struct U3CPluginsSdkMgr_ShowDialog_GetDelegate_member16_arg5U3Ec__AnonStorey75_t1698733223;

#include "codegen/il2cpp-codegen.h"

// System.Void PluginsSdkMgrGenerated/<PluginsSdkMgr_ShowDialog_GetDelegate_member16_arg5>c__AnonStorey75::.ctor()
extern "C"  void U3CPluginsSdkMgr_ShowDialog_GetDelegate_member16_arg5U3Ec__AnonStorey75__ctor_m1914918868 (U3CPluginsSdkMgr_ShowDialog_GetDelegate_member16_arg5U3Ec__AnonStorey75_t1698733223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated/<PluginsSdkMgr_ShowDialog_GetDelegate_member16_arg5>c__AnonStorey75::<>m__8E()
extern "C"  void U3CPluginsSdkMgr_ShowDialog_GetDelegate_member16_arg5U3Ec__AnonStorey75_U3CU3Em__8E_m4330858 (U3CPluginsSdkMgr_ShowDialog_GetDelegate_member16_arg5U3Ec__AnonStorey75_t1698733223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
