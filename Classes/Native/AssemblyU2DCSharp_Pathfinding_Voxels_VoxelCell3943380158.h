﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Voxels.VoxelSpan
struct VoxelSpan_t3943867046;

#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Voxels.VoxelCell
struct  VoxelCell_t3943380158 
{
public:
	// Pathfinding.Voxels.VoxelSpan Pathfinding.Voxels.VoxelCell::firstSpan
	VoxelSpan_t3943867046 * ___firstSpan_0;

public:
	inline static int32_t get_offset_of_firstSpan_0() { return static_cast<int32_t>(offsetof(VoxelCell_t3943380158, ___firstSpan_0)); }
	inline VoxelSpan_t3943867046 * get_firstSpan_0() const { return ___firstSpan_0; }
	inline VoxelSpan_t3943867046 ** get_address_of_firstSpan_0() { return &___firstSpan_0; }
	inline void set_firstSpan_0(VoxelSpan_t3943867046 * value)
	{
		___firstSpan_0 = value;
		Il2CppCodeGenWriteBarrier(&___firstSpan_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Pathfinding.Voxels.VoxelCell
struct VoxelCell_t3943380158_marshaled_pinvoke
{
	VoxelSpan_t3943867046 * ___firstSpan_0;
};
// Native definition for marshalling of: Pathfinding.Voxels.VoxelCell
struct VoxelCell_t3943380158_marshaled_com
{
	VoxelSpan_t3943867046 * ___firstSpan_0;
};
