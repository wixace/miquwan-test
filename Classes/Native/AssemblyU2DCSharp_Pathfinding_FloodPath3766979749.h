﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.GraphNode
struct GraphNode_t23612370;
// System.Collections.Generic.Dictionary`2<Pathfinding.GraphNode,Pathfinding.GraphNode>
struct Dictionary_2_t2012444339;

#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.FloodPath
struct  FloodPath_t3766979749  : public Path_t1974241691
{
public:
	// UnityEngine.Vector3 Pathfinding.FloodPath::originalStartPoint
	Vector3_t4282066566  ___originalStartPoint_38;
	// UnityEngine.Vector3 Pathfinding.FloodPath::startPoint
	Vector3_t4282066566  ___startPoint_39;
	// Pathfinding.GraphNode Pathfinding.FloodPath::startNode
	GraphNode_t23612370 * ___startNode_40;
	// System.Boolean Pathfinding.FloodPath::saveParents
	bool ___saveParents_41;
	// System.Collections.Generic.Dictionary`2<Pathfinding.GraphNode,Pathfinding.GraphNode> Pathfinding.FloodPath::parents
	Dictionary_2_t2012444339 * ___parents_42;

public:
	inline static int32_t get_offset_of_originalStartPoint_38() { return static_cast<int32_t>(offsetof(FloodPath_t3766979749, ___originalStartPoint_38)); }
	inline Vector3_t4282066566  get_originalStartPoint_38() const { return ___originalStartPoint_38; }
	inline Vector3_t4282066566 * get_address_of_originalStartPoint_38() { return &___originalStartPoint_38; }
	inline void set_originalStartPoint_38(Vector3_t4282066566  value)
	{
		___originalStartPoint_38 = value;
	}

	inline static int32_t get_offset_of_startPoint_39() { return static_cast<int32_t>(offsetof(FloodPath_t3766979749, ___startPoint_39)); }
	inline Vector3_t4282066566  get_startPoint_39() const { return ___startPoint_39; }
	inline Vector3_t4282066566 * get_address_of_startPoint_39() { return &___startPoint_39; }
	inline void set_startPoint_39(Vector3_t4282066566  value)
	{
		___startPoint_39 = value;
	}

	inline static int32_t get_offset_of_startNode_40() { return static_cast<int32_t>(offsetof(FloodPath_t3766979749, ___startNode_40)); }
	inline GraphNode_t23612370 * get_startNode_40() const { return ___startNode_40; }
	inline GraphNode_t23612370 ** get_address_of_startNode_40() { return &___startNode_40; }
	inline void set_startNode_40(GraphNode_t23612370 * value)
	{
		___startNode_40 = value;
		Il2CppCodeGenWriteBarrier(&___startNode_40, value);
	}

	inline static int32_t get_offset_of_saveParents_41() { return static_cast<int32_t>(offsetof(FloodPath_t3766979749, ___saveParents_41)); }
	inline bool get_saveParents_41() const { return ___saveParents_41; }
	inline bool* get_address_of_saveParents_41() { return &___saveParents_41; }
	inline void set_saveParents_41(bool value)
	{
		___saveParents_41 = value;
	}

	inline static int32_t get_offset_of_parents_42() { return static_cast<int32_t>(offsetof(FloodPath_t3766979749, ___parents_42)); }
	inline Dictionary_2_t2012444339 * get_parents_42() const { return ___parents_42; }
	inline Dictionary_2_t2012444339 ** get_address_of_parents_42() { return &___parents_42; }
	inline void set_parents_42(Dictionary_2_t2012444339 * value)
	{
		___parents_42 = value;
		Il2CppCodeGenWriteBarrier(&___parents_42, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
