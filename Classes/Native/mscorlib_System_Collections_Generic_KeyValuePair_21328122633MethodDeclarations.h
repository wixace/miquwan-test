﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22545618620MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.AnimationClip,System.Boolean>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1177838263(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1328122633 *, AnimationClip_t2007702890 *, bool, const MethodInfo*))KeyValuePair_2__ctor_m2040323320_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.AnimationClip,System.Boolean>::get_Key()
#define KeyValuePair_2_get_Key_m2983366481(__this, method) ((  AnimationClip_t2007702890 * (*) (KeyValuePair_2_t1328122633 *, const MethodInfo*))KeyValuePair_2_get_Key_m700889072_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.AnimationClip,System.Boolean>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3986153746(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1328122633 *, AnimationClip_t2007702890 *, const MethodInfo*))KeyValuePair_2_set_Key_m1751794225_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.AnimationClip,System.Boolean>::get_Value()
#define KeyValuePair_2_get_Value_m1944297681(__this, method) ((  bool (*) (KeyValuePair_2_t1328122633 *, const MethodInfo*))KeyValuePair_2_get_Value_m3809014448_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.AnimationClip,System.Boolean>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2719505426(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1328122633 *, bool, const MethodInfo*))KeyValuePair_2_set_Value_m3162969521_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.AnimationClip,System.Boolean>::ToString()
#define KeyValuePair_2_ToString_m1426727376(__this, method) ((  String_t* (*) (KeyValuePair_2_t1328122633 *, const MethodInfo*))KeyValuePair_2_ToString_m3396952209_gshared)(__this, method)
