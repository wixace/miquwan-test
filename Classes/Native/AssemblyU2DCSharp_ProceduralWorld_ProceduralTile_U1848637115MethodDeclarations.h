﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProceduralWorld/ProceduralTile/<Generate>c__Iterator12
struct U3CGenerateU3Ec__Iterator12_t1848637115;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ProceduralWorld/ProceduralTile/<Generate>c__Iterator12::.ctor()
extern "C"  void U3CGenerateU3Ec__Iterator12__ctor_m4103094336 (U3CGenerateU3Ec__Iterator12_t1848637115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProceduralWorld/ProceduralTile/<Generate>c__Iterator12::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGenerateU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3342625756 (U3CGenerateU3Ec__Iterator12_t1848637115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProceduralWorld/ProceduralTile/<Generate>c__Iterator12::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGenerateU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m1478076272 (U3CGenerateU3Ec__Iterator12_t1848637115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProceduralWorld/ProceduralTile/<Generate>c__Iterator12::MoveNext()
extern "C"  bool U3CGenerateU3Ec__Iterator12_MoveNext_m3961454236 (U3CGenerateU3Ec__Iterator12_t1848637115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProceduralWorld/ProceduralTile/<Generate>c__Iterator12::Dispose()
extern "C"  void U3CGenerateU3Ec__Iterator12_Dispose_m191962237 (U3CGenerateU3Ec__Iterator12_t1848637115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProceduralWorld/ProceduralTile/<Generate>c__Iterator12::Reset()
extern "C"  void U3CGenerateU3Ec__Iterator12_Reset_m1749527277 (U3CGenerateU3Ec__Iterator12_t1848637115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
