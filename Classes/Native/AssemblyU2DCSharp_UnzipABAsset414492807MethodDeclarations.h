﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnzipABAsset
struct UnzipABAsset_t414492807;
// FileInfoRes
struct FileInfoRes_t1217059798;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// IZUpdate
struct IZUpdate_t3482043738;
// Mihua.Assets.UnzipAssetMgr
struct UnzipAssetMgr_t1276665662;
// System.Action`2<System.Byte[],System.String>
struct Action_2_t1442129143;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FileInfoRes1217059798.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Mihua_Assets_UnzipAssetMgr1276665662.h"

// System.Void UnzipABAsset::.ctor()
extern "C"  void UnzipABAsset__ctor_m4270518772 (UnzipABAsset_t414492807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnzipABAsset UnzipABAsset::get_Instance()
extern "C"  UnzipABAsset_t414492807 * UnzipABAsset_get_Instance_m591782724 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnzipABAsset::AddAsset(FileInfoRes)
extern "C"  void UnzipABAsset_AddAsset_m2129497373 (UnzipABAsset_t414492807 * __this, FileInfoRes_t1217059798 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnzipABAsset::Start()
extern "C"  void UnzipABAsset_Start_m3217656564 (UnzipABAsset_t414492807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnzipABAsset::Check()
extern "C"  void UnzipABAsset_Check_m1562188314 (UnzipABAsset_t414492807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnzipABAsset::Unzip()
extern "C"  void UnzipABAsset_Unzip_m548737786 (UnzipABAsset_t414492807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnzipABAsset::get_Progress()
extern "C"  float UnzipABAsset_get_Progress_m3669463058 (UnzipABAsset_t414492807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnzipABAsset::ZUpdate()
extern "C"  void UnzipABAsset_ZUpdate_m1843301365 (UnzipABAsset_t414492807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnzipABAsset::OnUnzip(System.Byte[],System.String)
extern "C"  void UnzipABAsset_OnUnzip_m3517312906 (UnzipABAsset_t414492807 * __this, ByteU5BU5D_t4260760469* ___bytes0, String_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnzipABAsset::SaveBytesToFile(System.Byte[],System.String)
extern "C"  void UnzipABAsset_SaveBytesToFile_m471711790 (UnzipABAsset_t414492807 * __this, ByteU5BU5D_t4260760469* ___bytes0, String_t* ____index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnzipABAsset::<SaveBytesToFile>m__3F8()
extern "C"  void UnzipABAsset_U3CSaveBytesToFileU3Em__3F8_m180884991 (UnzipABAsset_t414492807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnzipABAsset::ilo_Log1(System.Object,System.Boolean)
extern "C"  void UnzipABAsset_ilo_Log1_m4162467545 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnzipABAsset::ilo_AddUpdate2(IZUpdate)
extern "C"  void UnzipABAsset_ilo_AddUpdate2_m1856004689 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnzipABAsset::ilo_RemoveUpdate3(IZUpdate)
extern "C"  void UnzipABAsset_ilo_RemoveUpdate3_m1806407371 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnzipABAsset::ilo_AddUnzip4(Mihua.Assets.UnzipAssetMgr,System.Byte[],System.String,System.Action`2<System.Byte[],System.String>,System.Boolean)
extern "C"  void UnzipABAsset_ilo_AddUnzip4_m1743966285 (Il2CppObject * __this /* static, unused */, UnzipAssetMgr_t1276665662 * ____this0, ByteU5BU5D_t4260760469* ___bytes1, String_t* ___key2, Action_2_t1442129143 * ___callBack3, bool ___isCompress4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
