﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.TupleSerializer
struct TupleSerializer_t1096852276;
// ProtoBuf.Meta.RuntimeTypeModel
struct RuntimeTypeModel_t242172789;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t4136801618;
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t674955999;
// System.Object
struct Il2CppObject;
// ProtoBuf.SerializationContext
struct SerializationContext_t3997850667;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// System.Type
struct Type_t;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_RuntimeTypeModel242172789.h"
#include "mscorlib_System_Reflection_ConstructorInfo4136801618.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel_Callback2866957669.h"
#include "AssemblyU2DCSharp_ProtoBuf_SerializationContext3997850667.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "AssemblyU2DCSharp_ProtoBuf_Serializers_TupleSerial1096852276.h"

// System.Void ProtoBuf.Serializers.TupleSerializer::.ctor(ProtoBuf.Meta.RuntimeTypeModel,System.Reflection.ConstructorInfo,System.Reflection.MemberInfo[])
extern "C"  void TupleSerializer__ctor_m109950684 (TupleSerializer_t1096852276 * __this, RuntimeTypeModel_t242172789 * ___model0, ConstructorInfo_t4136801618 * ___ctor1, MemberInfoU5BU5D_t674955999* ___members2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.TupleSerializer::ProtoBuf.Serializers.IProtoTypeSerializer.Callback(System.Object,ProtoBuf.Meta.TypeModel/CallbackType,ProtoBuf.SerializationContext)
extern "C"  void TupleSerializer_ProtoBuf_Serializers_IProtoTypeSerializer_Callback_m14552898 (TupleSerializer_t1096852276 * __this, Il2CppObject * ___value0, int32_t ___callbackType1, SerializationContext_t3997850667 * ___context2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.TupleSerializer::ProtoBuf.Serializers.IProtoTypeSerializer.CreateInstance(ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * TupleSerializer_ProtoBuf_Serializers_IProtoTypeSerializer_CreateInstance_m2168883460 (TupleSerializer_t1096852276 * __this, ProtoReader_t3962509489 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.TupleSerializer::ProtoBuf.Serializers.IProtoTypeSerializer.CanCreateInstance()
extern "C"  bool TupleSerializer_ProtoBuf_Serializers_IProtoTypeSerializer_CanCreateInstance_m74498537 (TupleSerializer_t1096852276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.TupleSerializer::HasCallbacks(ProtoBuf.Meta.TypeModel/CallbackType)
extern "C"  bool TupleSerializer_HasCallbacks_m975045910 (TupleSerializer_t1096852276 * __this, int32_t ___callbackType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.TupleSerializer::get_ExpectedType()
extern "C"  Type_t * TupleSerializer_get_ExpectedType_m3542797704 (TupleSerializer_t1096852276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.TupleSerializer::GetValue(System.Object,System.Int32)
extern "C"  Il2CppObject * TupleSerializer_GetValue_m323348102 (TupleSerializer_t1096852276 * __this, Il2CppObject * ___obj0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.TupleSerializer::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * TupleSerializer_Read_m2077352136 (TupleSerializer_t1096852276 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.TupleSerializer::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void TupleSerializer_Write_m4068370056 (TupleSerializer_t1096852276 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.TupleSerializer::get_RequiresOldValue()
extern "C"  bool TupleSerializer_get_RequiresOldValue_m754949480 (TupleSerializer_t1096852276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.TupleSerializer::get_ReturnsValue()
extern "C"  bool TupleSerializer_get_ReturnsValue_m3266479102 (TupleSerializer_t1096852276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.TupleSerializer::GetMemberType(System.Int32)
extern "C"  Type_t * TupleSerializer_GetMemberType_m4251620048 (TupleSerializer_t1096852276 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Serializers.TupleSerializer::ilo_ReadFieldHeader1(ProtoBuf.ProtoReader)
extern "C"  int32_t TupleSerializer_ilo_ReadFieldHeader1_m1776381612 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.TupleSerializer::ilo_GetValue2(ProtoBuf.Serializers.TupleSerializer,System.Object,System.Int32)
extern "C"  Il2CppObject * TupleSerializer_ilo_GetValue2_m306905397 (Il2CppObject * __this /* static, unused */, TupleSerializer_t1096852276 * ____this0, Il2CppObject * ___obj1, int32_t ___index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
