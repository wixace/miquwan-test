﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._575226f174acc9f0d947fb1a18a522d1
struct _575226f174acc9f0d947fb1a18a522d1_t1038676210;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__575226f174acc9f0d947fb1a1038676210.h"

// System.Void Little._575226f174acc9f0d947fb1a18a522d1::.ctor()
extern "C"  void _575226f174acc9f0d947fb1a18a522d1__ctor_m1783965051 (_575226f174acc9f0d947fb1a18a522d1_t1038676210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._575226f174acc9f0d947fb1a18a522d1::_575226f174acc9f0d947fb1a18a522d1m2(System.Int32)
extern "C"  int32_t _575226f174acc9f0d947fb1a18a522d1__575226f174acc9f0d947fb1a18a522d1m2_m2276383833 (_575226f174acc9f0d947fb1a18a522d1_t1038676210 * __this, int32_t ____575226f174acc9f0d947fb1a18a522d1a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._575226f174acc9f0d947fb1a18a522d1::_575226f174acc9f0d947fb1a18a522d1m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _575226f174acc9f0d947fb1a18a522d1__575226f174acc9f0d947fb1a18a522d1m_m2215338621 (_575226f174acc9f0d947fb1a18a522d1_t1038676210 * __this, int32_t ____575226f174acc9f0d947fb1a18a522d1a0, int32_t ____575226f174acc9f0d947fb1a18a522d1251, int32_t ____575226f174acc9f0d947fb1a18a522d1c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._575226f174acc9f0d947fb1a18a522d1::ilo__575226f174acc9f0d947fb1a18a522d1m21(Little._575226f174acc9f0d947fb1a18a522d1,System.Int32)
extern "C"  int32_t _575226f174acc9f0d947fb1a18a522d1_ilo__575226f174acc9f0d947fb1a18a522d1m21_m3359345401 (Il2CppObject * __this /* static, unused */, _575226f174acc9f0d947fb1a18a522d1_t1038676210 * ____this0, int32_t ____575226f174acc9f0d947fb1a18a522d1a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
