﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginCaoHua
struct PluginCaoHua_t1396735126;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.String
struct String_t;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// VersionMgr
struct VersionMgr_t1322950208;
// VersionInfo
struct VersionInfo_t2356638086;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// Mihua.SDK.LvUpInfo
struct LvUpInfo_t411687177;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginCaoHua1396735126.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"

// System.Void PluginCaoHua::.ctor()
extern "C"  void PluginCaoHua__ctor_m2455453381 (PluginCaoHua_t1396735126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::Init()
extern "C"  void PluginCaoHua_Init_m584934127 (PluginCaoHua_t1396735126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginCaoHua::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginCaoHua_ReqSDKHttpLogin_m667157722 (PluginCaoHua_t1396735126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginCaoHua::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginCaoHua_IsLoginSuccess_m3076027118 (PluginCaoHua_t1396735126 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::OpenUserLogin()
extern "C"  void PluginCaoHua_OpenUserLogin_m3969895447 (PluginCaoHua_t1396735126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::OnLoginSuccess(System.String)
extern "C"  void PluginCaoHua_OnLoginSuccess_m1409393674 (PluginCaoHua_t1396735126 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::UserPay(CEvent.ZEvent)
extern "C"  void PluginCaoHua_UserPay_m216092539 (PluginCaoHua_t1396735126 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::OnLogoutSuccess(System.String)
extern "C"  void PluginCaoHua_OnLogoutSuccess_m745073605 (PluginCaoHua_t1396735126 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::OnLogout(System.String)
extern "C"  void PluginCaoHua_OnLogout_m4142790426 (PluginCaoHua_t1396735126 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::OnSDKLogoutSuccess(System.String)
extern "C"  void PluginCaoHua_OnSDKLogoutSuccess_m3358647109 (PluginCaoHua_t1396735126 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::OnPaySuccess(System.String)
extern "C"  void PluginCaoHua_OnPaySuccess_m1379238089 (PluginCaoHua_t1396735126 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::OnPayFail(System.String)
extern "C"  void PluginCaoHua_OnPayFail_m1961348376 (PluginCaoHua_t1396735126 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::EnterGame(CEvent.ZEvent)
extern "C"  void PluginCaoHua_EnterGame_m3723046478 (PluginCaoHua_t1396735126 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginCaoHua_RoleUpgrade_m475370866 (PluginCaoHua_t1396735126 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::initSdk(System.String,System.String)
extern "C"  void PluginCaoHua_initSdk_m1337048465 (PluginCaoHua_t1396735126 * __this, String_t* ___gameid0, String_t* ___gameExtra1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::login()
extern "C"  void PluginCaoHua_login_m1977468204 (PluginCaoHua_t1396735126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::logout()
extern "C"  void PluginCaoHua_logout_m1177794889 (PluginCaoHua_t1396735126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::creatRole(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginCaoHua_creatRole_m3376046416 (PluginCaoHua_t1396735126 * __this, String_t* ___serverName0, String_t* ___serverID1, String_t* ___roleName2, String_t* ___roleID3, String_t* ___roleLevel4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::roleUpgrade(System.String)
extern "C"  void PluginCaoHua_roleUpgrade_m3280536537 (PluginCaoHua_t1396735126 * __this, String_t* ___role_level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginCaoHua_pay_m4108503443 (PluginCaoHua_t1396735126 * __this, String_t* ___oderid0, String_t* ___roleLevel1, String_t* ___roleID2, String_t* ___roleName3, String_t* ___extra4, String_t* ___productid5, String_t* ___price6, String_t* ___productname7, String_t* ___gameid8, String_t* ___pburl9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::<OnLogoutSuccess>m__411()
extern "C"  void PluginCaoHua_U3COnLogoutSuccessU3Em__411_m428012362 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::<OnLogout>m__412()
extern "C"  void PluginCaoHua_U3COnLogoutU3Em__412_m399057696 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::<OnSDKLogoutSuccess>m__413()
extern "C"  void PluginCaoHua_U3COnSDKLogoutSuccessU3Em__413_m1927098188 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginCaoHua::ilo_get_Instance1()
extern "C"  VersionMgr_t1322950208 * PluginCaoHua_ilo_get_Instance1_m4177142918 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginCaoHua::ilo_get_currentVS2(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginCaoHua_ilo_get_currentVS2_m1052323140 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::ilo_initSdk3(PluginCaoHua,System.String,System.String)
extern "C"  void PluginCaoHua_ilo_initSdk3_m3520520573 (Il2CppObject * __this /* static, unused */, PluginCaoHua_t1396735126 * ____this0, String_t* ___gameid1, String_t* ___gameExtra2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginCaoHua::ilo_get_PluginsSdkMgr4()
extern "C"  PluginsSdkMgr_t3884624670 * PluginCaoHua_ilo_get_PluginsSdkMgr4_m2334209990 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginCaoHua::ilo_get_DeviceID5(PluginsSdkMgr)
extern "C"  String_t* PluginCaoHua_ilo_get_DeviceID5_m670955212 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginCaoHua::ilo_get_isSdkLogin6(VersionMgr)
extern "C"  bool PluginCaoHua_ilo_get_isSdkLogin6_m3526751440 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::ilo_login7(PluginCaoHua)
extern "C"  void PluginCaoHua_ilo_login7_m3585638538 (Il2CppObject * __this /* static, unused */, PluginCaoHua_t1396735126 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.PayInfo PluginCaoHua::ilo_Parse8(System.Object[])
extern "C"  PayInfo_t1775308120 * PluginCaoHua_ilo_Parse8_m414322932 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___payOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginCaoHua::ilo_GetProductID9(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginCaoHua_ilo_GetProductID9_m587812534 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::ilo_logout10(PluginCaoHua)
extern "C"  void PluginCaoHua_ilo_logout10_m3177736261 (Il2CppObject * __this /* static, unused */, PluginCaoHua_t1396735126 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.LvUpInfo PluginCaoHua::ilo_Parse11(System.Object[])
extern "C"  LvUpInfo_t411687177 * PluginCaoHua_ilo_Parse11_m2188790415 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCaoHua::ilo_ReqSDKHttpLogin12(PluginsSdkMgr)
extern "C"  void PluginCaoHua_ilo_ReqSDKHttpLogin12_m3127590782 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
