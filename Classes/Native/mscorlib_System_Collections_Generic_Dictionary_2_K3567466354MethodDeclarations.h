﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>
struct Dictionary_2_t2952530300;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3567466354.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UIModelDisplayType,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m496079127_gshared (Enumerator_t3567466354 * __this, Dictionary_2_t2952530300 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m496079127(__this, ___host0, method) ((  void (*) (Enumerator_t3567466354 *, Dictionary_2_t2952530300 *, const MethodInfo*))Enumerator__ctor_m496079127_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UIModelDisplayType,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1564656042_gshared (Enumerator_t3567466354 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1564656042(__this, method) ((  Il2CppObject * (*) (Enumerator_t3567466354 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1564656042_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UIModelDisplayType,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1376100158_gshared (Enumerator_t3567466354 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1376100158(__this, method) ((  void (*) (Enumerator_t3567466354 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1376100158_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UIModelDisplayType,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m3788004217_gshared (Enumerator_t3567466354 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3788004217(__this, method) ((  void (*) (Enumerator_t3567466354 *, const MethodInfo*))Enumerator_Dispose_m3788004217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UIModelDisplayType,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m511596970_gshared (Enumerator_t3567466354 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m511596970(__this, method) ((  bool (*) (Enumerator_t3567466354 *, const MethodInfo*))Enumerator_MoveNext_m511596970_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UIModelDisplayType,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2858198634_gshared (Enumerator_t3567466354 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2858198634(__this, method) ((  int32_t (*) (Enumerator_t3567466354 *, const MethodInfo*))Enumerator_get_Current_m2858198634_gshared)(__this, method)
