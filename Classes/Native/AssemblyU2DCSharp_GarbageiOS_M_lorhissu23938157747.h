﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_lorhissu239
struct  M_lorhissu239_t38157747  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_lorhissu239::_telhooze
	int32_t ____telhooze_0;
	// System.Single GarbageiOS.M_lorhissu239::_dearir
	float ____dearir_1;
	// System.String GarbageiOS.M_lorhissu239::_beeboo
	String_t* ____beeboo_2;
	// System.Single GarbageiOS.M_lorhissu239::_safebeWemfoo
	float ____safebeWemfoo_3;
	// System.Boolean GarbageiOS.M_lorhissu239::_fakisBorfawpem
	bool ____fakisBorfawpem_4;
	// System.UInt32 GarbageiOS.M_lorhissu239::_coomalteeDarwaiti
	uint32_t ____coomalteeDarwaiti_5;
	// System.UInt32 GarbageiOS.M_lorhissu239::_whimairVowsasmow
	uint32_t ____whimairVowsasmow_6;
	// System.String GarbageiOS.M_lorhissu239::_vebaniTunarsas
	String_t* ____vebaniTunarsas_7;
	// System.Boolean GarbageiOS.M_lorhissu239::_raikerdaYutifur
	bool ____raikerdaYutifur_8;
	// System.Int32 GarbageiOS.M_lorhissu239::_qearteListahea
	int32_t ____qearteListahea_9;
	// System.Single GarbageiOS.M_lorhissu239::_lorgolayNirfeyer
	float ____lorgolayNirfeyer_10;
	// System.Int32 GarbageiOS.M_lorhissu239::_vekow
	int32_t ____vekow_11;
	// System.String GarbageiOS.M_lorhissu239::_vurtriteMiscu
	String_t* ____vurtriteMiscu_12;
	// System.UInt32 GarbageiOS.M_lorhissu239::_teecorji
	uint32_t ____teecorji_13;
	// System.Boolean GarbageiOS.M_lorhissu239::_histeRisca
	bool ____histeRisca_14;
	// System.Single GarbageiOS.M_lorhissu239::_malljereNumor
	float ____malljereNumor_15;
	// System.Int32 GarbageiOS.M_lorhissu239::_noka
	int32_t ____noka_16;
	// System.Single GarbageiOS.M_lorhissu239::_dupabem
	float ____dupabem_17;
	// System.String GarbageiOS.M_lorhissu239::_jemnorzaw
	String_t* ____jemnorzaw_18;
	// System.String GarbageiOS.M_lorhissu239::_heetiskayWhorsirwe
	String_t* ____heetiskayWhorsirwe_19;
	// System.String GarbageiOS.M_lorhissu239::_wheqe
	String_t* ____wheqe_20;
	// System.UInt32 GarbageiOS.M_lorhissu239::_tallkirnere
	uint32_t ____tallkirnere_21;
	// System.String GarbageiOS.M_lorhissu239::_meqo
	String_t* ____meqo_22;
	// System.String GarbageiOS.M_lorhissu239::_nidemyiYeevise
	String_t* ____nidemyiYeevise_23;
	// System.Boolean GarbageiOS.M_lorhissu239::_sewisSirha
	bool ____sewisSirha_24;
	// System.Boolean GarbageiOS.M_lorhissu239::_nasanorStowlearmi
	bool ____nasanorStowlearmi_25;
	// System.UInt32 GarbageiOS.M_lorhissu239::_raipomuGurow
	uint32_t ____raipomuGurow_26;
	// System.Boolean GarbageiOS.M_lorhissu239::_truse
	bool ____truse_27;
	// System.String GarbageiOS.M_lorhissu239::_tallmeTresel
	String_t* ____tallmeTresel_28;
	// System.String GarbageiOS.M_lorhissu239::_kobeljerXerebas
	String_t* ____kobeljerXerebas_29;
	// System.Single GarbageiOS.M_lorhissu239::_juzor
	float ____juzor_30;
	// System.Single GarbageiOS.M_lorhissu239::_qelor
	float ____qelor_31;
	// System.Single GarbageiOS.M_lorhissu239::_terchojur
	float ____terchojur_32;
	// System.String GarbageiOS.M_lorhissu239::_couchisi
	String_t* ____couchisi_33;
	// System.String GarbageiOS.M_lorhissu239::_qowqewhaCipane
	String_t* ____qowqewhaCipane_34;
	// System.Int32 GarbageiOS.M_lorhissu239::_mounasRirdovor
	int32_t ____mounasRirdovor_35;
	// System.String GarbageiOS.M_lorhissu239::_nagairner
	String_t* ____nagairner_36;
	// System.UInt32 GarbageiOS.M_lorhissu239::_herewarfisMayjoo
	uint32_t ____herewarfisMayjoo_37;

public:
	inline static int32_t get_offset_of__telhooze_0() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____telhooze_0)); }
	inline int32_t get__telhooze_0() const { return ____telhooze_0; }
	inline int32_t* get_address_of__telhooze_0() { return &____telhooze_0; }
	inline void set__telhooze_0(int32_t value)
	{
		____telhooze_0 = value;
	}

	inline static int32_t get_offset_of__dearir_1() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____dearir_1)); }
	inline float get__dearir_1() const { return ____dearir_1; }
	inline float* get_address_of__dearir_1() { return &____dearir_1; }
	inline void set__dearir_1(float value)
	{
		____dearir_1 = value;
	}

	inline static int32_t get_offset_of__beeboo_2() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____beeboo_2)); }
	inline String_t* get__beeboo_2() const { return ____beeboo_2; }
	inline String_t** get_address_of__beeboo_2() { return &____beeboo_2; }
	inline void set__beeboo_2(String_t* value)
	{
		____beeboo_2 = value;
		Il2CppCodeGenWriteBarrier(&____beeboo_2, value);
	}

	inline static int32_t get_offset_of__safebeWemfoo_3() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____safebeWemfoo_3)); }
	inline float get__safebeWemfoo_3() const { return ____safebeWemfoo_3; }
	inline float* get_address_of__safebeWemfoo_3() { return &____safebeWemfoo_3; }
	inline void set__safebeWemfoo_3(float value)
	{
		____safebeWemfoo_3 = value;
	}

	inline static int32_t get_offset_of__fakisBorfawpem_4() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____fakisBorfawpem_4)); }
	inline bool get__fakisBorfawpem_4() const { return ____fakisBorfawpem_4; }
	inline bool* get_address_of__fakisBorfawpem_4() { return &____fakisBorfawpem_4; }
	inline void set__fakisBorfawpem_4(bool value)
	{
		____fakisBorfawpem_4 = value;
	}

	inline static int32_t get_offset_of__coomalteeDarwaiti_5() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____coomalteeDarwaiti_5)); }
	inline uint32_t get__coomalteeDarwaiti_5() const { return ____coomalteeDarwaiti_5; }
	inline uint32_t* get_address_of__coomalteeDarwaiti_5() { return &____coomalteeDarwaiti_5; }
	inline void set__coomalteeDarwaiti_5(uint32_t value)
	{
		____coomalteeDarwaiti_5 = value;
	}

	inline static int32_t get_offset_of__whimairVowsasmow_6() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____whimairVowsasmow_6)); }
	inline uint32_t get__whimairVowsasmow_6() const { return ____whimairVowsasmow_6; }
	inline uint32_t* get_address_of__whimairVowsasmow_6() { return &____whimairVowsasmow_6; }
	inline void set__whimairVowsasmow_6(uint32_t value)
	{
		____whimairVowsasmow_6 = value;
	}

	inline static int32_t get_offset_of__vebaniTunarsas_7() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____vebaniTunarsas_7)); }
	inline String_t* get__vebaniTunarsas_7() const { return ____vebaniTunarsas_7; }
	inline String_t** get_address_of__vebaniTunarsas_7() { return &____vebaniTunarsas_7; }
	inline void set__vebaniTunarsas_7(String_t* value)
	{
		____vebaniTunarsas_7 = value;
		Il2CppCodeGenWriteBarrier(&____vebaniTunarsas_7, value);
	}

	inline static int32_t get_offset_of__raikerdaYutifur_8() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____raikerdaYutifur_8)); }
	inline bool get__raikerdaYutifur_8() const { return ____raikerdaYutifur_8; }
	inline bool* get_address_of__raikerdaYutifur_8() { return &____raikerdaYutifur_8; }
	inline void set__raikerdaYutifur_8(bool value)
	{
		____raikerdaYutifur_8 = value;
	}

	inline static int32_t get_offset_of__qearteListahea_9() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____qearteListahea_9)); }
	inline int32_t get__qearteListahea_9() const { return ____qearteListahea_9; }
	inline int32_t* get_address_of__qearteListahea_9() { return &____qearteListahea_9; }
	inline void set__qearteListahea_9(int32_t value)
	{
		____qearteListahea_9 = value;
	}

	inline static int32_t get_offset_of__lorgolayNirfeyer_10() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____lorgolayNirfeyer_10)); }
	inline float get__lorgolayNirfeyer_10() const { return ____lorgolayNirfeyer_10; }
	inline float* get_address_of__lorgolayNirfeyer_10() { return &____lorgolayNirfeyer_10; }
	inline void set__lorgolayNirfeyer_10(float value)
	{
		____lorgolayNirfeyer_10 = value;
	}

	inline static int32_t get_offset_of__vekow_11() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____vekow_11)); }
	inline int32_t get__vekow_11() const { return ____vekow_11; }
	inline int32_t* get_address_of__vekow_11() { return &____vekow_11; }
	inline void set__vekow_11(int32_t value)
	{
		____vekow_11 = value;
	}

	inline static int32_t get_offset_of__vurtriteMiscu_12() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____vurtriteMiscu_12)); }
	inline String_t* get__vurtriteMiscu_12() const { return ____vurtriteMiscu_12; }
	inline String_t** get_address_of__vurtriteMiscu_12() { return &____vurtriteMiscu_12; }
	inline void set__vurtriteMiscu_12(String_t* value)
	{
		____vurtriteMiscu_12 = value;
		Il2CppCodeGenWriteBarrier(&____vurtriteMiscu_12, value);
	}

	inline static int32_t get_offset_of__teecorji_13() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____teecorji_13)); }
	inline uint32_t get__teecorji_13() const { return ____teecorji_13; }
	inline uint32_t* get_address_of__teecorji_13() { return &____teecorji_13; }
	inline void set__teecorji_13(uint32_t value)
	{
		____teecorji_13 = value;
	}

	inline static int32_t get_offset_of__histeRisca_14() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____histeRisca_14)); }
	inline bool get__histeRisca_14() const { return ____histeRisca_14; }
	inline bool* get_address_of__histeRisca_14() { return &____histeRisca_14; }
	inline void set__histeRisca_14(bool value)
	{
		____histeRisca_14 = value;
	}

	inline static int32_t get_offset_of__malljereNumor_15() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____malljereNumor_15)); }
	inline float get__malljereNumor_15() const { return ____malljereNumor_15; }
	inline float* get_address_of__malljereNumor_15() { return &____malljereNumor_15; }
	inline void set__malljereNumor_15(float value)
	{
		____malljereNumor_15 = value;
	}

	inline static int32_t get_offset_of__noka_16() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____noka_16)); }
	inline int32_t get__noka_16() const { return ____noka_16; }
	inline int32_t* get_address_of__noka_16() { return &____noka_16; }
	inline void set__noka_16(int32_t value)
	{
		____noka_16 = value;
	}

	inline static int32_t get_offset_of__dupabem_17() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____dupabem_17)); }
	inline float get__dupabem_17() const { return ____dupabem_17; }
	inline float* get_address_of__dupabem_17() { return &____dupabem_17; }
	inline void set__dupabem_17(float value)
	{
		____dupabem_17 = value;
	}

	inline static int32_t get_offset_of__jemnorzaw_18() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____jemnorzaw_18)); }
	inline String_t* get__jemnorzaw_18() const { return ____jemnorzaw_18; }
	inline String_t** get_address_of__jemnorzaw_18() { return &____jemnorzaw_18; }
	inline void set__jemnorzaw_18(String_t* value)
	{
		____jemnorzaw_18 = value;
		Il2CppCodeGenWriteBarrier(&____jemnorzaw_18, value);
	}

	inline static int32_t get_offset_of__heetiskayWhorsirwe_19() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____heetiskayWhorsirwe_19)); }
	inline String_t* get__heetiskayWhorsirwe_19() const { return ____heetiskayWhorsirwe_19; }
	inline String_t** get_address_of__heetiskayWhorsirwe_19() { return &____heetiskayWhorsirwe_19; }
	inline void set__heetiskayWhorsirwe_19(String_t* value)
	{
		____heetiskayWhorsirwe_19 = value;
		Il2CppCodeGenWriteBarrier(&____heetiskayWhorsirwe_19, value);
	}

	inline static int32_t get_offset_of__wheqe_20() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____wheqe_20)); }
	inline String_t* get__wheqe_20() const { return ____wheqe_20; }
	inline String_t** get_address_of__wheqe_20() { return &____wheqe_20; }
	inline void set__wheqe_20(String_t* value)
	{
		____wheqe_20 = value;
		Il2CppCodeGenWriteBarrier(&____wheqe_20, value);
	}

	inline static int32_t get_offset_of__tallkirnere_21() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____tallkirnere_21)); }
	inline uint32_t get__tallkirnere_21() const { return ____tallkirnere_21; }
	inline uint32_t* get_address_of__tallkirnere_21() { return &____tallkirnere_21; }
	inline void set__tallkirnere_21(uint32_t value)
	{
		____tallkirnere_21 = value;
	}

	inline static int32_t get_offset_of__meqo_22() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____meqo_22)); }
	inline String_t* get__meqo_22() const { return ____meqo_22; }
	inline String_t** get_address_of__meqo_22() { return &____meqo_22; }
	inline void set__meqo_22(String_t* value)
	{
		____meqo_22 = value;
		Il2CppCodeGenWriteBarrier(&____meqo_22, value);
	}

	inline static int32_t get_offset_of__nidemyiYeevise_23() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____nidemyiYeevise_23)); }
	inline String_t* get__nidemyiYeevise_23() const { return ____nidemyiYeevise_23; }
	inline String_t** get_address_of__nidemyiYeevise_23() { return &____nidemyiYeevise_23; }
	inline void set__nidemyiYeevise_23(String_t* value)
	{
		____nidemyiYeevise_23 = value;
		Il2CppCodeGenWriteBarrier(&____nidemyiYeevise_23, value);
	}

	inline static int32_t get_offset_of__sewisSirha_24() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____sewisSirha_24)); }
	inline bool get__sewisSirha_24() const { return ____sewisSirha_24; }
	inline bool* get_address_of__sewisSirha_24() { return &____sewisSirha_24; }
	inline void set__sewisSirha_24(bool value)
	{
		____sewisSirha_24 = value;
	}

	inline static int32_t get_offset_of__nasanorStowlearmi_25() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____nasanorStowlearmi_25)); }
	inline bool get__nasanorStowlearmi_25() const { return ____nasanorStowlearmi_25; }
	inline bool* get_address_of__nasanorStowlearmi_25() { return &____nasanorStowlearmi_25; }
	inline void set__nasanorStowlearmi_25(bool value)
	{
		____nasanorStowlearmi_25 = value;
	}

	inline static int32_t get_offset_of__raipomuGurow_26() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____raipomuGurow_26)); }
	inline uint32_t get__raipomuGurow_26() const { return ____raipomuGurow_26; }
	inline uint32_t* get_address_of__raipomuGurow_26() { return &____raipomuGurow_26; }
	inline void set__raipomuGurow_26(uint32_t value)
	{
		____raipomuGurow_26 = value;
	}

	inline static int32_t get_offset_of__truse_27() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____truse_27)); }
	inline bool get__truse_27() const { return ____truse_27; }
	inline bool* get_address_of__truse_27() { return &____truse_27; }
	inline void set__truse_27(bool value)
	{
		____truse_27 = value;
	}

	inline static int32_t get_offset_of__tallmeTresel_28() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____tallmeTresel_28)); }
	inline String_t* get__tallmeTresel_28() const { return ____tallmeTresel_28; }
	inline String_t** get_address_of__tallmeTresel_28() { return &____tallmeTresel_28; }
	inline void set__tallmeTresel_28(String_t* value)
	{
		____tallmeTresel_28 = value;
		Il2CppCodeGenWriteBarrier(&____tallmeTresel_28, value);
	}

	inline static int32_t get_offset_of__kobeljerXerebas_29() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____kobeljerXerebas_29)); }
	inline String_t* get__kobeljerXerebas_29() const { return ____kobeljerXerebas_29; }
	inline String_t** get_address_of__kobeljerXerebas_29() { return &____kobeljerXerebas_29; }
	inline void set__kobeljerXerebas_29(String_t* value)
	{
		____kobeljerXerebas_29 = value;
		Il2CppCodeGenWriteBarrier(&____kobeljerXerebas_29, value);
	}

	inline static int32_t get_offset_of__juzor_30() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____juzor_30)); }
	inline float get__juzor_30() const { return ____juzor_30; }
	inline float* get_address_of__juzor_30() { return &____juzor_30; }
	inline void set__juzor_30(float value)
	{
		____juzor_30 = value;
	}

	inline static int32_t get_offset_of__qelor_31() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____qelor_31)); }
	inline float get__qelor_31() const { return ____qelor_31; }
	inline float* get_address_of__qelor_31() { return &____qelor_31; }
	inline void set__qelor_31(float value)
	{
		____qelor_31 = value;
	}

	inline static int32_t get_offset_of__terchojur_32() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____terchojur_32)); }
	inline float get__terchojur_32() const { return ____terchojur_32; }
	inline float* get_address_of__terchojur_32() { return &____terchojur_32; }
	inline void set__terchojur_32(float value)
	{
		____terchojur_32 = value;
	}

	inline static int32_t get_offset_of__couchisi_33() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____couchisi_33)); }
	inline String_t* get__couchisi_33() const { return ____couchisi_33; }
	inline String_t** get_address_of__couchisi_33() { return &____couchisi_33; }
	inline void set__couchisi_33(String_t* value)
	{
		____couchisi_33 = value;
		Il2CppCodeGenWriteBarrier(&____couchisi_33, value);
	}

	inline static int32_t get_offset_of__qowqewhaCipane_34() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____qowqewhaCipane_34)); }
	inline String_t* get__qowqewhaCipane_34() const { return ____qowqewhaCipane_34; }
	inline String_t** get_address_of__qowqewhaCipane_34() { return &____qowqewhaCipane_34; }
	inline void set__qowqewhaCipane_34(String_t* value)
	{
		____qowqewhaCipane_34 = value;
		Il2CppCodeGenWriteBarrier(&____qowqewhaCipane_34, value);
	}

	inline static int32_t get_offset_of__mounasRirdovor_35() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____mounasRirdovor_35)); }
	inline int32_t get__mounasRirdovor_35() const { return ____mounasRirdovor_35; }
	inline int32_t* get_address_of__mounasRirdovor_35() { return &____mounasRirdovor_35; }
	inline void set__mounasRirdovor_35(int32_t value)
	{
		____mounasRirdovor_35 = value;
	}

	inline static int32_t get_offset_of__nagairner_36() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____nagairner_36)); }
	inline String_t* get__nagairner_36() const { return ____nagairner_36; }
	inline String_t** get_address_of__nagairner_36() { return &____nagairner_36; }
	inline void set__nagairner_36(String_t* value)
	{
		____nagairner_36 = value;
		Il2CppCodeGenWriteBarrier(&____nagairner_36, value);
	}

	inline static int32_t get_offset_of__herewarfisMayjoo_37() { return static_cast<int32_t>(offsetof(M_lorhissu239_t38157747, ____herewarfisMayjoo_37)); }
	inline uint32_t get__herewarfisMayjoo_37() const { return ____herewarfisMayjoo_37; }
	inline uint32_t* get_address_of__herewarfisMayjoo_37() { return &____herewarfisMayjoo_37; }
	inline void set__herewarfisMayjoo_37(uint32_t value)
	{
		____herewarfisMayjoo_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
