﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIButtonOffset
struct UIButtonOffset_t1019800345;
// TweenPosition
struct TweenPosition_t3684358292;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TweenPosition3684358292.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_UIButtonOffset1019800345.h"

// System.Void UIButtonOffset::.ctor()
extern "C"  void UIButtonOffset__ctor_m1347505570 (UIButtonOffset_t1019800345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonOffset::Start()
extern "C"  void UIButtonOffset_Start_m294643362 (UIButtonOffset_t1019800345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonOffset::OnEnable()
extern "C"  void UIButtonOffset_OnEnable_m550698660 (UIButtonOffset_t1019800345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonOffset::OnDisable()
extern "C"  void UIButtonOffset_OnDisable_m332726537 (UIButtonOffset_t1019800345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonOffset::OnPress(System.Boolean)
extern "C"  void UIButtonOffset_OnPress_m379741723 (UIButtonOffset_t1019800345 * __this, bool ___isPressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonOffset::OnHover(System.Boolean)
extern "C"  void UIButtonOffset_OnHover_m2460055828 (UIButtonOffset_t1019800345 * __this, bool ___isOver0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonOffset::OnSelect(System.Boolean)
extern "C"  void UIButtonOffset_OnSelect_m2808824468 (UIButtonOffset_t1019800345 * __this, bool ___isSelected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonOffset::ilo_set_value1(TweenPosition,UnityEngine.Vector3)
extern "C"  void UIButtonOffset_ilo_set_value1_m1123070111 (Il2CppObject * __this /* static, unused */, TweenPosition_t3684358292 * ____this0, Vector3_t4282066566  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TweenPosition UIButtonOffset::ilo_Begin2(UnityEngine.GameObject,System.Single,UnityEngine.Vector3)
extern "C"  TweenPosition_t3684358292 * UIButtonOffset_ilo_Begin2_m1035780333 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, float ___duration1, Vector3_t4282066566  ___pos2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonOffset::ilo_OnHover3(UIButtonOffset,System.Boolean)
extern "C"  void UIButtonOffset_ilo_OnHover3_m2512137259 (Il2CppObject * __this /* static, unused */, UIButtonOffset_t1019800345 * ____this0, bool ___isOver1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
