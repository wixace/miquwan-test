﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtilGenerated/<BehaviourUtil_DelayCall_GetDelegate_member1_arg1>c__AnonStorey43`4<System.Object,System.Object,System.Object,System.Object>
struct U3CBehaviourUtil_DelayCall_GetDelegate_member1_arg1U3Ec__AnonStorey43_4_t3682947393;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void BehaviourUtilGenerated/<BehaviourUtil_DelayCall_GetDelegate_member1_arg1>c__AnonStorey43`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CBehaviourUtil_DelayCall_GetDelegate_member1_arg1U3Ec__AnonStorey43_4__ctor_m2305584835_gshared (U3CBehaviourUtil_DelayCall_GetDelegate_member1_arg1U3Ec__AnonStorey43_4_t3682947393 * __this, const MethodInfo* method);
#define U3CBehaviourUtil_DelayCall_GetDelegate_member1_arg1U3Ec__AnonStorey43_4__ctor_m2305584835(__this, method) ((  void (*) (U3CBehaviourUtil_DelayCall_GetDelegate_member1_arg1U3Ec__AnonStorey43_4_t3682947393 *, const MethodInfo*))U3CBehaviourUtil_DelayCall_GetDelegate_member1_arg1U3Ec__AnonStorey43_4__ctor_m2305584835_gshared)(__this, method)
// System.Void BehaviourUtilGenerated/<BehaviourUtil_DelayCall_GetDelegate_member1_arg1>c__AnonStorey43`4<System.Object,System.Object,System.Object,System.Object>::<>m__2(T1,T2,T3,T4)
extern "C"  void U3CBehaviourUtil_DelayCall_GetDelegate_member1_arg1U3Ec__AnonStorey43_4_U3CU3Em__2_m541614780_gshared (U3CBehaviourUtil_DelayCall_GetDelegate_member1_arg1U3Ec__AnonStorey43_4_t3682947393 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, const MethodInfo* method);
#define U3CBehaviourUtil_DelayCall_GetDelegate_member1_arg1U3Ec__AnonStorey43_4_U3CU3Em__2_m541614780(__this, ___arg10, ___arg21, ___arg32, ___arg43, method) ((  void (*) (U3CBehaviourUtil_DelayCall_GetDelegate_member1_arg1U3Ec__AnonStorey43_4_t3682947393 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CBehaviourUtil_DelayCall_GetDelegate_member1_arg1U3Ec__AnonStorey43_4_U3CU3Em__2_m541614780_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, method)
