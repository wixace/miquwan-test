﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MonsterDeadFadeTimed
struct MonsterDeadFadeTimed_t1196879997;

#include "codegen/il2cpp-codegen.h"

// System.Void MonsterDeadFadeTimed::.ctor()
extern "C"  void MonsterDeadFadeTimed__ctor_m3531318974 (MonsterDeadFadeTimed_t1196879997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterDeadFadeTimed::Start_time(System.Single)
extern "C"  void MonsterDeadFadeTimed_Start_time_m1632858779 (MonsterDeadFadeTimed_t1196879997 * __this, float ___Fadetime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterDeadFadeTimed::Update()
extern "C"  void MonsterDeadFadeTimed_Update_m3823567887 (MonsterDeadFadeTimed_t1196879997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
