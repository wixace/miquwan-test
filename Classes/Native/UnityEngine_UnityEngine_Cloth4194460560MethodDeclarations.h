﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Cloth
struct Cloth_t4194460560;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.ClothSkinningCoefficient[]
struct ClothSkinningCoefficientU5BU5D_t3357176891;
// UnityEngine.CapsuleCollider[]
struct CapsuleColliderU5BU5D_t4217061582;
// UnityEngine.ClothSphereColliderPair[]
struct ClothSphereColliderPairU5BU5D_t3700479018;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Cloth4194460560.h"

// System.Void UnityEngine.Cloth::.ctor()
extern "C"  void Cloth__ctor_m2716843775 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Cloth::get_sleepThreshold()
extern "C"  float Cloth_get_sleepThreshold_m262134486 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::set_sleepThreshold(System.Single)
extern "C"  void Cloth_set_sleepThreshold_m23004565 (Cloth_t4194460560 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Cloth::get_bendingStiffness()
extern "C"  float Cloth_get_bendingStiffness_m3544284152 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::set_bendingStiffness(System.Single)
extern "C"  void Cloth_set_bendingStiffness_m1780943795 (Cloth_t4194460560 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Cloth::get_stretchingStiffness()
extern "C"  float Cloth_get_stretchingStiffness_m1957795490 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::set_stretchingStiffness(System.Single)
extern "C"  void Cloth_set_stretchingStiffness_m3701444425 (Cloth_t4194460560 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Cloth::get_damping()
extern "C"  float Cloth_get_damping_m3315025186 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::set_damping(System.Single)
extern "C"  void Cloth_set_damping_m2530537673 (Cloth_t4194460560 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Cloth::get_externalAcceleration()
extern "C"  Vector3_t4282066566  Cloth_get_externalAcceleration_m3984629823 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::set_externalAcceleration(UnityEngine.Vector3)
extern "C"  void Cloth_set_externalAcceleration_m4133646540 (Cloth_t4194460560 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::INTERNAL_get_externalAcceleration(UnityEngine.Vector3&)
extern "C"  void Cloth_INTERNAL_get_externalAcceleration_m1473172678 (Cloth_t4194460560 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::INTERNAL_set_externalAcceleration(UnityEngine.Vector3&)
extern "C"  void Cloth_INTERNAL_set_externalAcceleration_m157907002 (Cloth_t4194460560 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Cloth::get_randomAcceleration()
extern "C"  Vector3_t4282066566  Cloth_get_randomAcceleration_m2620749815 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::set_randomAcceleration(UnityEngine.Vector3)
extern "C"  void Cloth_set_randomAcceleration_m3755018900 (Cloth_t4194460560 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::INTERNAL_get_randomAcceleration(UnityEngine.Vector3&)
extern "C"  void Cloth_INTERNAL_get_randomAcceleration_m2977811838 (Cloth_t4194460560 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::INTERNAL_set_randomAcceleration(UnityEngine.Vector3&)
extern "C"  void Cloth_INTERNAL_set_randomAcceleration_m3566386674 (Cloth_t4194460560 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Cloth::get_useGravity()
extern "C"  bool Cloth_get_useGravity_m1303707631 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::set_useGravity(System.Boolean)
extern "C"  void Cloth_set_useGravity_m3278448832 (Cloth_t4194460560 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Cloth::get_enabled()
extern "C"  bool Cloth_get_enabled_m2909348731 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::set_enabled(System.Boolean)
extern "C"  void Cloth_set_enabled_m134879832 (Cloth_t4194460560 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine.Cloth::get_vertices()
extern "C"  Vector3U5BU5D_t215400611* Cloth_get_vertices_m1629729391 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine.Cloth::get_normals()
extern "C"  Vector3U5BU5D_t215400611* Cloth_get_normals_m1113837592 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Cloth::get_friction()
extern "C"  float Cloth_get_friction_m1908239804 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::set_friction(System.Single)
extern "C"  void Cloth_set_friction_m4046198895 (Cloth_t4194460560 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Cloth::get_collisionMassScale()
extern "C"  float Cloth_get_collisionMassScale_m243786406 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::set_collisionMassScale(System.Single)
extern "C"  void Cloth_set_collisionMassScale_m1382854597 (Cloth_t4194460560 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Cloth::get_useContinuousCollision()
extern "C"  float Cloth_get_useContinuousCollision_m809153630 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::set_useContinuousCollision(System.Single)
extern "C"  void Cloth_set_useContinuousCollision_m1778182413 (Cloth_t4194460560 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Cloth::get_useVirtualParticles()
extern "C"  float Cloth_get_useVirtualParticles_m3618490217 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::set_useVirtualParticles(System.Single)
extern "C"  void Cloth_set_useVirtualParticles_m1549343202 (Cloth_t4194460560 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::ClearTransformMotion()
extern "C"  void Cloth_ClearTransformMotion_m4027573594 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::INTERNAL_CALL_ClearTransformMotion(UnityEngine.Cloth)
extern "C"  void Cloth_INTERNAL_CALL_ClearTransformMotion_m2454059158 (Il2CppObject * __this /* static, unused */, Cloth_t4194460560 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ClothSkinningCoefficient[] UnityEngine.Cloth::get_coefficients()
extern "C"  ClothSkinningCoefficientU5BU5D_t3357176891* Cloth_get_coefficients_m1844471360 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::set_coefficients(UnityEngine.ClothSkinningCoefficient[])
extern "C"  void Cloth_set_coefficients_m181206941 (Cloth_t4194460560 * __this, ClothSkinningCoefficientU5BU5D_t3357176891* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Cloth::get_worldVelocityScale()
extern "C"  float Cloth_get_worldVelocityScale_m1393777981 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::set_worldVelocityScale(System.Single)
extern "C"  void Cloth_set_worldVelocityScale_m4087964302 (Cloth_t4194460560 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Cloth::get_worldAccelerationScale()
extern "C"  float Cloth_get_worldAccelerationScale_m934992058 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::set_worldAccelerationScale(System.Single)
extern "C"  void Cloth_set_worldAccelerationScale_m4002134577 (Cloth_t4194460560 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::SetEnabledFading(System.Boolean,System.Single)
extern "C"  void Cloth_SetEnabledFading_m2682025625 (Cloth_t4194460560 * __this, bool ___enabled0, float ___interpolationTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::SetEnabledFading(System.Boolean)
extern "C"  void Cloth_SetEnabledFading_m3629233844 (Cloth_t4194460560 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Cloth::get_solverFrequency()
extern "C"  bool Cloth_get_solverFrequency_m472975299 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::set_solverFrequency(System.Boolean)
extern "C"  void Cloth_set_solverFrequency_m1855249056 (Cloth_t4194460560 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CapsuleCollider[] UnityEngine.Cloth::get_capsuleColliders()
extern "C"  CapsuleColliderU5BU5D_t4217061582* Cloth_get_capsuleColliders_m3438430201 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::set_capsuleColliders(UnityEngine.CapsuleCollider[])
extern "C"  void Cloth_set_capsuleColliders_m1792945554 (Cloth_t4194460560 * __this, CapsuleColliderU5BU5D_t4217061582* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ClothSphereColliderPair[] UnityEngine.Cloth::get_sphereColliders()
extern "C"  ClothSphereColliderPairU5BU5D_t3700479018* Cloth_get_sphereColliders_m2146742265 (Cloth_t4194460560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::set_sphereColliders(UnityEngine.ClothSphereColliderPair[])
extern "C"  void Cloth_set_sphereColliders_m1789047360 (Cloth_t4194460560 * __this, ClothSphereColliderPairU5BU5D_t3700479018* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
