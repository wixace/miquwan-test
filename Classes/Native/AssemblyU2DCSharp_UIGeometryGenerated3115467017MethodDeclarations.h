﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIGeometryGenerated
struct UIGeometryGenerated_t3115467017;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// UIGeometry
struct UIGeometry_t3586695974;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_UIGeometry3586695974.h"

// System.Void UIGeometryGenerated::.ctor()
extern "C"  void UIGeometryGenerated__ctor_m3975801986 (UIGeometryGenerated_t3115467017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGeometryGenerated::UIGeometry_UIGeometry1(JSVCall,System.Int32)
extern "C"  bool UIGeometryGenerated_UIGeometry_UIGeometry1_m2775937072 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGeometryGenerated::UIGeometry_verts(JSVCall)
extern "C"  void UIGeometryGenerated_UIGeometry_verts_m4197588892 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGeometryGenerated::UIGeometry_uvs(JSVCall)
extern "C"  void UIGeometryGenerated_UIGeometry_uvs_m2494685964 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGeometryGenerated::UIGeometry_cols(JSVCall)
extern "C"  void UIGeometryGenerated_UIGeometry_cols_m2915572667 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGeometryGenerated::UIGeometry_hasVertices(JSVCall)
extern "C"  void UIGeometryGenerated_UIGeometry_hasVertices_m944231787 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGeometryGenerated::UIGeometry_hasTransformed(JSVCall)
extern "C"  void UIGeometryGenerated_UIGeometry_hasTransformed_m133510237 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGeometryGenerated::UIGeometry_ApplyTransform__Matrix4x4(JSVCall,System.Int32)
extern "C"  bool UIGeometryGenerated_UIGeometry_ApplyTransform__Matrix4x4_m4087863030 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGeometryGenerated::UIGeometry_Clear(JSVCall,System.Int32)
extern "C"  bool UIGeometryGenerated_UIGeometry_Clear_m4113980946 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGeometryGenerated::UIGeometry_WriteToBuffers__BetterListT1_Vector3__BetterListT1_Vector2__BetterListT1_Color32__BetterListT1_Vector3__BetterListT1_Vector4(JSVCall,System.Int32)
extern "C"  bool UIGeometryGenerated_UIGeometry_WriteToBuffers__BetterListT1_Vector3__BetterListT1_Vector2__BetterListT1_Color32__BetterListT1_Vector3__BetterListT1_Vector4_m3478159508 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGeometryGenerated::__Register()
extern "C"  void UIGeometryGenerated___Register_m1104220549 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIGeometryGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UIGeometryGenerated_ilo_getObject1_m4047237620 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGeometryGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UIGeometryGenerated_ilo_attachFinalizerObject2_m1827255798 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGeometryGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UIGeometryGenerated_ilo_addJSCSRel3_m4141658944 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIGeometryGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIGeometryGenerated_ilo_getObject4_m1296604582 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIGeometryGenerated::ilo_setObject5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIGeometryGenerated_ilo_setObject5_m1338857072 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGeometryGenerated::ilo_get_hasTransformed6(UIGeometry)
extern "C"  bool UIGeometryGenerated_ilo_get_hasTransformed6_m4265744983 (Il2CppObject * __this /* static, unused */, UIGeometry_t3586695974 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGeometryGenerated::ilo_setBooleanS7(System.Int32,System.Boolean)
extern "C"  void UIGeometryGenerated_ilo_setBooleanS7_m3036799789 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
