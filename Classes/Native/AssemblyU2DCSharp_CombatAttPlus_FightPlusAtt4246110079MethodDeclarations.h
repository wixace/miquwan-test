﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CombatAttPlus/FightPlusAtt
struct FightPlusAtt_t4246110079;

#include "codegen/il2cpp-codegen.h"

// System.Void CombatAttPlus/FightPlusAtt::.ctor()
extern "C"  void FightPlusAtt__ctor_m3805276156 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_attack_per(System.UInt32)
extern "C"  void FightPlusAtt_set_attack_per_m837769231 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_attack_per()
extern "C"  uint32_t FightPlusAtt_get_attack_per_m1705042930 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_phy_def_per(System.UInt32)
extern "C"  void FightPlusAtt_set_phy_def_per_m2946634616 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_phy_def_per()
extern "C"  uint32_t FightPlusAtt_get_phy_def_per_m1721148539 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_mag_def_per(System.UInt32)
extern "C"  void FightPlusAtt_set_mag_def_per_m1420292966 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_mag_def_per()
extern "C"  uint32_t FightPlusAtt_get_mag_def_per_m3978131917 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_hp_per(System.UInt32)
extern "C"  void FightPlusAtt_set_hp_per_m1280806735 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_hp_per()
extern "C"  uint32_t FightPlusAtt_get_hp_per_m570986546 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_hitrate_per(System.UInt32)
extern "C"  void FightPlusAtt_set_hitrate_per_m4235311436 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_hitrate_per()
extern "C"  uint32_t FightPlusAtt_get_hitrate_per_m1470764583 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_dodge_per(System.UInt32)
extern "C"  void FightPlusAtt_set_dodge_per_m2044622568 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_dodge_per()
extern "C"  uint32_t FightPlusAtt_get_dodge_per_m1555760459 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_destroy_per(System.UInt32)
extern "C"  void FightPlusAtt_set_destroy_per_m1161180389 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_destroy_per()
extern "C"  uint32_t FightPlusAtt_get_destroy_per_m3316102638 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_block_per(System.UInt32)
extern "C"  void FightPlusAtt_set_block_per_m2792145522 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_block_per()
extern "C"  uint32_t FightPlusAtt_get_block_per_m1390854529 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_crit_per(System.UInt32)
extern "C"  void FightPlusAtt_set_crit_per_m4070291005 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_crit_per()
extern "C"  uint32_t FightPlusAtt_get_crit_per_m4251398340 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_anticrit_per(System.UInt32)
extern "C"  void FightPlusAtt_set_anticrit_per_m3119347355 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_anticrit_per()
extern "C"  uint32_t FightPlusAtt_get_anticrit_per_m1997693990 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_attack(System.UInt32)
extern "C"  void FightPlusAtt_set_attack_m550316589 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_attack()
extern "C"  uint32_t FightPlusAtt_get_attack_m1098556564 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_phy_def(System.UInt32)
extern "C"  void FightPlusAtt_set_phy_def_m2632974870 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_phy_def()
extern "C"  uint32_t FightPlusAtt_get_phy_def_m3295678877 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_mag_def(System.UInt32)
extern "C"  void FightPlusAtt_set_mag_def_m1741621508 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_mag_def()
extern "C"  uint32_t FightPlusAtt_get_mag_def_m2018140143 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_hp(System.UInt32)
extern "C"  void FightPlusAtt_set_hp_m3161653613 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_hp()
extern "C"  uint32_t FightPlusAtt_get_hp_m3944902868 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_hitrate(System.UInt32)
extern "C"  void FightPlusAtt_set_hitrate_m3179524586 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_hitrate()
extern "C"  uint32_t FightPlusAtt_get_hitrate_m2717333833 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_dodge(System.UInt32)
extern "C"  void FightPlusAtt_set_dodge_m1548197254 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_dodge()
extern "C"  uint32_t FightPlusAtt_get_dodge_m184900205 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_destroy(System.UInt32)
extern "C"  void FightPlusAtt_set_destroy_m1048568323 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_destroy()
extern "C"  uint32_t FightPlusAtt_get_destroy_m2161235088 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_block(System.UInt32)
extern "C"  void FightPlusAtt_set_block_m2634890768 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_block()
extern "C"  uint32_t FightPlusAtt_get_block_m2629018019 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_crit(System.UInt32)
extern "C"  void FightPlusAtt_set_crit_m4174106459 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_crit()
extern "C"  uint32_t FightPlusAtt_get_crit_m2751212134 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_anticrit(System.UInt32)
extern "C"  void FightPlusAtt_set_anticrit_m2690150073 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_anticrit()
extern "C"  uint32_t FightPlusAtt_get_anticrit_m4219231944 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_damage_bonus(System.UInt32)
extern "C"  void FightPlusAtt_set_damage_bonus_m2597074246 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_damage_bonus()
extern "C"  uint32_t FightPlusAtt_get_damage_bonus_m3236703131 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_damage_reduce(System.UInt32)
extern "C"  void FightPlusAtt_set_damage_reduce_m354564935 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_damage_reduce()
extern "C"  uint32_t FightPlusAtt_get_damage_reduce_m3096899084 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::set_crit_hurt(System.UInt32)
extern "C"  void FightPlusAtt_set_crit_hurt_m547530345 (FightPlusAtt_t4246110079 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus/FightPlusAtt::get_crit_hurt()
extern "C"  uint32_t FightPlusAtt_get_crit_hurt_m2730145834 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus/FightPlusAtt::Clear()
extern "C"  void FightPlusAtt_Clear_m1211409447 (FightPlusAtt_t4246110079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
