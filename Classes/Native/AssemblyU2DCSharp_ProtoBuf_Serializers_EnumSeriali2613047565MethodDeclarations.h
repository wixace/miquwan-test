﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.EnumSerializer
struct EnumSerializer_t2613047565;
// System.Type
struct Type_t;
// ProtoBuf.Serializers.EnumSerializer/EnumPair[]
struct EnumPairU5BU5D_t754245438;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoTypeCode2227754741.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"

// System.Void ProtoBuf.Serializers.EnumSerializer::.ctor(System.Type,ProtoBuf.Serializers.EnumSerializer/EnumPair[])
extern "C"  void EnumSerializer__ctor_m3360549196 (EnumSerializer_t2613047565 * __this, Type_t * ___enumType0, EnumPairU5BU5D_t754245438* ___map1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.EnumSerializer::ProtoBuf.Serializers.IProtoSerializer.get_RequiresOldValue()
extern "C"  bool EnumSerializer_ProtoBuf_Serializers_IProtoSerializer_get_RequiresOldValue_m654927350 (EnumSerializer_t2613047565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.EnumSerializer::ProtoBuf.Serializers.IProtoSerializer.get_ReturnsValue()
extern "C"  bool EnumSerializer_ProtoBuf_Serializers_IProtoSerializer_get_ReturnsValue_m4112696204 (EnumSerializer_t2613047565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.ProtoTypeCode ProtoBuf.Serializers.EnumSerializer::GetTypeCode()
extern "C"  int32_t EnumSerializer_GetTypeCode_m1294252660 (EnumSerializer_t2613047565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.EnumSerializer::get_ExpectedType()
extern "C"  Type_t * EnumSerializer_get_ExpectedType_m4103646365 (EnumSerializer_t2613047565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Serializers.EnumSerializer::EnumToWire(System.Object)
extern "C"  int32_t EnumSerializer_EnumToWire_m2932822245 (EnumSerializer_t2613047565 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.EnumSerializer::WireToEnum(System.Int32)
extern "C"  Il2CppObject * EnumSerializer_WireToEnum_m2733005721 (EnumSerializer_t2613047565 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.EnumSerializer::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * EnumSerializer_Read_m24614407 (EnumSerializer_t2613047565 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.EnumSerializer::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void EnumSerializer_Write_m720777183 (EnumSerializer_t2613047565 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.EnumSerializer::ilo_ThrowEnumException1(ProtoBuf.ProtoReader,System.Type,System.Int32)
extern "C"  void EnumSerializer_ilo_ThrowEnumException1_m2744792688 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, Type_t * ___type1, int32_t ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
