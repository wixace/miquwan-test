﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Mihua.Update.VSTools::.cctor()
extern "C"  void VSTools__cctor_m1695122881 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua.Update.VSTools::GetCRC32(System.String)
extern "C"  String_t* VSTools_GetCRC32_m4147509450 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua.Update.VSTools::GetCRC32(System.Byte[])
extern "C"  String_t* VSTools_GetCRC32_m2089703825 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mihua.Update.VSTools::GetCRC32Uint(System.Byte[])
extern "C"  uint32_t VSTools_GetCRC32Uint_m1923939167 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua.Update.VSTools::GetMD5(System.Byte[])
extern "C"  String_t* VSTools_GetMD5_m1764922054 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua.Update.VSTools::GetMD5(System.String)
extern "C"  String_t* VSTools_GetMD5_m3822727679 (Il2CppObject * __this /* static, unused */, String_t* ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
