﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CaptureScreenshotMgr
struct CaptureScreenshotMgr_t3951887692;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;
// UnityEngine.Coroutine
struct Coroutine_t3621161934;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"

// System.Void CaptureScreenshotMgr::.ctor()
extern "C"  void CaptureScreenshotMgr__ctor_m3476982479 (CaptureScreenshotMgr_t3951887692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureScreenshotMgr::OnCapture(CEvent.ZEvent)
extern "C"  void CaptureScreenshotMgr_OnCapture_m3434332263 (CaptureScreenshotMgr_t3951887692 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CaptureScreenshotMgr::CutImage(System.String)
extern "C"  Il2CppObject * CaptureScreenshotMgr_CutImage_m4085728236 (CaptureScreenshotMgr_t3951887692 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CaptureScreenshotMgr::iOSCutImage(System.String)
extern "C"  Il2CppObject * CaptureScreenshotMgr_iOSCutImage_m313214775 (CaptureScreenshotMgr_t3951887692 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CaptureScreenshotMgr::GetCurTime()
extern "C"  String_t* CaptureScreenshotMgr_GetCurTime_m754418223 (CaptureScreenshotMgr_t3951887692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureScreenshotMgr::ScanFile(System.String)
extern "C"  void CaptureScreenshotMgr_ScanFile_m4167493588 (CaptureScreenshotMgr_t3951887692 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureScreenshotMgr::__ICaptureImage(System.String)
extern "C"  void CaptureScreenshotMgr___ICaptureImage_m4215583383 (CaptureScreenshotMgr_t3951887692 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine CaptureScreenshotMgr::ilo_StartCoroutine1(System.Collections.IEnumerator)
extern "C"  Coroutine_t3621161934 * CaptureScreenshotMgr_ilo_StartCoroutine1_m3559331022 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___routine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
