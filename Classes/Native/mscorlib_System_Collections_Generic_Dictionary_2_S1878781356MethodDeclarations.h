﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>
struct ShimEnumerator_t1878781356;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>
struct Dictionary_2_t2163003329;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2394286663_gshared (ShimEnumerator_t1878781356 * __this, Dictionary_2_t2163003329 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m2394286663(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1878781356 *, Dictionary_2_t2163003329 *, const MethodInfo*))ShimEnumerator__ctor_m2394286663_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1923534074_gshared (ShimEnumerator_t1878781356 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1923534074(__this, method) ((  bool (*) (ShimEnumerator_t1878781356 *, const MethodInfo*))ShimEnumerator_MoveNext_m1923534074_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m461601562_gshared (ShimEnumerator_t1878781356 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m461601562(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1878781356 *, const MethodInfo*))ShimEnumerator_get_Entry_m461601562_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m73247605_gshared (ShimEnumerator_t1878781356 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m73247605(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1878781356 *, const MethodInfo*))ShimEnumerator_get_Key_m73247605_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2719908039_gshared (ShimEnumerator_t1878781356 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m2719908039(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1878781356 *, const MethodInfo*))ShimEnumerator_get_Value_m2719908039_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3916200335_gshared (ShimEnumerator_t1878781356 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m3916200335(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1878781356 *, const MethodInfo*))ShimEnumerator_get_Current_m3916200335_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::Reset()
extern "C"  void ShimEnumerator_Reset_m1244924441_gshared (ShimEnumerator_t1878781356 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1244924441(__this, method) ((  void (*) (ShimEnumerator_t1878781356 *, const MethodInfo*))ShimEnumerator_Reset_m1244924441_gshared)(__this, method)
