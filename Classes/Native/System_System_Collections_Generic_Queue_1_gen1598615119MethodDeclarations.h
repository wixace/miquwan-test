﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<AstarPath/GUOSingle>
struct Queue_1_t1598615119;
// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<AstarPath/GUOSingle>
struct IEnumerator_1_t1274237739;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// AstarPath/GUOSingle[]
struct GUOSingleU5BU5D_t1723385383;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_AstarPath_GUOSingle3657339986.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat2887700631.h"

// System.Void System.Collections.Generic.Queue`1<AstarPath/GUOSingle>::.ctor()
extern "C"  void Queue_1__ctor_m248747638_gshared (Queue_1_t1598615119 * __this, const MethodInfo* method);
#define Queue_1__ctor_m248747638(__this, method) ((  void (*) (Queue_1_t1598615119 *, const MethodInfo*))Queue_1__ctor_m248747638_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<AstarPath/GUOSingle>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Queue_1_System_Collections_ICollection_CopyTo_m3057924495_gshared (Queue_1_t1598615119 * __this, Il2CppArray * ___array0, int32_t ___idx1, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_CopyTo_m3057924495(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t1598615119 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m3057924495_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<AstarPath/GUOSingle>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Queue_1_System_Collections_ICollection_get_IsSynchronized_m3555652431_gshared (Queue_1_t1598615119 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m3555652431(__this, method) ((  bool (*) (Queue_1_t1598615119 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m3555652431_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<AstarPath/GUOSingle>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Queue_1_System_Collections_ICollection_get_SyncRoot_m2442569581_gshared (Queue_1_t1598615119 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m2442569581(__this, method) ((  Il2CppObject * (*) (Queue_1_t1598615119 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m2442569581_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<AstarPath/GUOSingle>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3733376031_gshared (Queue_1_t1598615119 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3733376031(__this, method) ((  Il2CppObject* (*) (Queue_1_t1598615119 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3733376031_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<AstarPath/GUOSingle>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Queue_1_System_Collections_IEnumerable_GetEnumerator_m4293560906_gshared (Queue_1_t1598615119 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m4293560906(__this, method) ((  Il2CppObject * (*) (Queue_1_t1598615119 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m4293560906_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<AstarPath/GUOSingle>::Clear()
extern "C"  void Queue_1_Clear_m1949848225_gshared (Queue_1_t1598615119 * __this, const MethodInfo* method);
#define Queue_1_Clear_m1949848225(__this, method) ((  void (*) (Queue_1_t1598615119 *, const MethodInfo*))Queue_1_Clear_m1949848225_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<AstarPath/GUOSingle>::CopyTo(T[],System.Int32)
extern "C"  void Queue_1_CopyTo_m1359800826_gshared (Queue_1_t1598615119 * __this, GUOSingleU5BU5D_t1723385383* ___array0, int32_t ___idx1, const MethodInfo* method);
#define Queue_1_CopyTo_m1359800826(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t1598615119 *, GUOSingleU5BU5D_t1723385383*, int32_t, const MethodInfo*))Queue_1_CopyTo_m1359800826_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<AstarPath/GUOSingle>::Dequeue()
extern "C"  GUOSingle_t3657339986  Queue_1_Dequeue_m2232483506_gshared (Queue_1_t1598615119 * __this, const MethodInfo* method);
#define Queue_1_Dequeue_m2232483506(__this, method) ((  GUOSingle_t3657339986  (*) (Queue_1_t1598615119 *, const MethodInfo*))Queue_1_Dequeue_m2232483506_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<AstarPath/GUOSingle>::Peek()
extern "C"  GUOSingle_t3657339986  Queue_1_Peek_m565998267_gshared (Queue_1_t1598615119 * __this, const MethodInfo* method);
#define Queue_1_Peek_m565998267(__this, method) ((  GUOSingle_t3657339986  (*) (Queue_1_t1598615119 *, const MethodInfo*))Queue_1_Peek_m565998267_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<AstarPath/GUOSingle>::Enqueue(T)
extern "C"  void Queue_1_Enqueue_m517566413_gshared (Queue_1_t1598615119 * __this, GUOSingle_t3657339986  ___item0, const MethodInfo* method);
#define Queue_1_Enqueue_m517566413(__this, ___item0, method) ((  void (*) (Queue_1_t1598615119 *, GUOSingle_t3657339986 , const MethodInfo*))Queue_1_Enqueue_m517566413_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<AstarPath/GUOSingle>::SetCapacity(System.Int32)
extern "C"  void Queue_1_SetCapacity_m1071641600_gshared (Queue_1_t1598615119 * __this, int32_t ___new_size0, const MethodInfo* method);
#define Queue_1_SetCapacity_m1071641600(__this, ___new_size0, method) ((  void (*) (Queue_1_t1598615119 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m1071641600_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<AstarPath/GUOSingle>::get_Count()
extern "C"  int32_t Queue_1_get_Count_m609300104_gshared (Queue_1_t1598615119 * __this, const MethodInfo* method);
#define Queue_1_get_Count_m609300104(__this, method) ((  int32_t (*) (Queue_1_t1598615119 *, const MethodInfo*))Queue_1_get_Count_m609300104_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<AstarPath/GUOSingle>::GetEnumerator()
extern "C"  Enumerator_t2887700631  Queue_1_GetEnumerator_m2755566822_gshared (Queue_1_t1598615119 * __this, const MethodInfo* method);
#define Queue_1_GetEnumerator_m2755566822(__this, method) ((  Enumerator_t2887700631  (*) (Queue_1_t1598615119 *, const MethodInfo*))Queue_1_GetEnumerator_m2755566822_gshared)(__this, method)
