﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_BaseInputModuleGenerated
struct UnityEngine_EventSystems_BaseInputModuleGenerated_t4286620151;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_EventSystems_BaseInputModuleGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_BaseInputModuleGenerated__ctor_m1766240596 (UnityEngine_EventSystems_BaseInputModuleGenerated_t4286620151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_BaseInputModuleGenerated::BaseInputModule_ActivateModule(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_BaseInputModuleGenerated_BaseInputModule_ActivateModule_m835800805 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_BaseInputModuleGenerated::BaseInputModule_DeactivateModule(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_BaseInputModuleGenerated_BaseInputModule_DeactivateModule_m3421359526 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_BaseInputModuleGenerated::BaseInputModule_IsModuleSupported(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_BaseInputModuleGenerated_BaseInputModule_IsModuleSupported_m3474491956 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_BaseInputModuleGenerated::BaseInputModule_IsPointerOverGameObject__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_BaseInputModuleGenerated_BaseInputModule_IsPointerOverGameObject__Int32_m3923402140 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_BaseInputModuleGenerated::BaseInputModule_Process(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_BaseInputModuleGenerated_BaseInputModule_Process_m1773061643 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_BaseInputModuleGenerated::BaseInputModule_ShouldActivateModule(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_BaseInputModuleGenerated_BaseInputModule_ShouldActivateModule_m1231046520 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_BaseInputModuleGenerated::BaseInputModule_UpdateModule(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_BaseInputModuleGenerated_BaseInputModule_UpdateModule_m2581767931 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_BaseInputModuleGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_BaseInputModuleGenerated___Register_m1899528179 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_BaseInputModuleGenerated::ilo_setBooleanS1(System.Int32,System.Boolean)
extern "C"  void UnityEngine_EventSystems_BaseInputModuleGenerated_ilo_setBooleanS1_m878343365 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
