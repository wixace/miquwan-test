﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Poly2Tri.TriangulationPoint
struct TriangulationPoint_t3810082933;
// System.Collections.Generic.List`1<Pathfinding.Poly2Tri.DTSweepConstraint>
struct List_1_t433497279;
// System.String
struct String_t;
// Pathfinding.Poly2Tri.DTSweepConstraint
struct DTSweepConstraint_t3360279023;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_DTSweepC3360279023.h"

// System.Void Pathfinding.Poly2Tri.TriangulationPoint::.ctor(System.Double,System.Double)
extern "C"  void TriangulationPoint__ctor_m1454340884 (TriangulationPoint_t3810082933 * __this, double ___x0, double ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Pathfinding.Poly2Tri.DTSweepConstraint> Pathfinding.Poly2Tri.TriangulationPoint::get_Edges()
extern "C"  List_1_t433497279 * TriangulationPoint_get_Edges_m2952612962 (TriangulationPoint_t3810082933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.TriangulationPoint::set_Edges(System.Collections.Generic.List`1<Pathfinding.Poly2Tri.DTSweepConstraint>)
extern "C"  void TriangulationPoint_set_Edges_m47341099 (TriangulationPoint_t3810082933 * __this, List_1_t433497279 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.Poly2Tri.TriangulationPoint::ToString()
extern "C"  String_t* TriangulationPoint_ToString_m2084010179 (TriangulationPoint_t3810082933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.TriangulationPoint::AddEdge(Pathfinding.Poly2Tri.DTSweepConstraint)
extern "C"  void TriangulationPoint_AddEdge_m2282912300 (TriangulationPoint_t3810082933 * __this, DTSweepConstraint_t3360279023 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Poly2Tri.TriangulationPoint::get_HasEdges()
extern "C"  bool TriangulationPoint_get_HasEdges_m1603684009 (TriangulationPoint_t3810082933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
