﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_IEnumeratorGenerated
struct System_Collections_IEnumeratorGenerated_t2179705756;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void System_Collections_IEnumeratorGenerated::.ctor()
extern "C"  void System_Collections_IEnumeratorGenerated__ctor_m2337878863 (System_Collections_IEnumeratorGenerated_t2179705756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_IEnumeratorGenerated::IEnumerator_Current(JSVCall)
extern "C"  void System_Collections_IEnumeratorGenerated_IEnumerator_Current_m564481363 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_IEnumeratorGenerated::IEnumerator_MoveNext(JSVCall,System.Int32)
extern "C"  bool System_Collections_IEnumeratorGenerated_IEnumerator_MoveNext_m1221613543 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_IEnumeratorGenerated::IEnumerator_Reset(JSVCall,System.Int32)
extern "C"  bool System_Collections_IEnumeratorGenerated_IEnumerator_Reset_m2341359342 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_IEnumeratorGenerated::__Register()
extern "C"  void System_Collections_IEnumeratorGenerated___Register_m2547974872 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_IEnumeratorGenerated::ilo_setBooleanS1(System.Int32,System.Boolean)
extern "C"  void System_Collections_IEnumeratorGenerated_ilo_setBooleanS1_m3379995264 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
