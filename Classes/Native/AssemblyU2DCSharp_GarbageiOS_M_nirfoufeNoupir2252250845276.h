﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_nirfoufeNoupir225
struct  M_nirfoufeNoupir225_t2250845276  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_nirfoufeNoupir225::_jawzalllasDerejacu
	uint32_t ____jawzalllasDerejacu_0;
	// System.Single GarbageiOS.M_nirfoufeNoupir225::_dawkiqayChaifere
	float ____dawkiqayChaifere_1;
	// System.Boolean GarbageiOS.M_nirfoufeNoupir225::_turtaiNarrea
	bool ____turtaiNarrea_2;
	// System.String GarbageiOS.M_nirfoufeNoupir225::_towne
	String_t* ____towne_3;
	// System.Boolean GarbageiOS.M_nirfoufeNoupir225::_magi
	bool ____magi_4;
	// System.UInt32 GarbageiOS.M_nirfoufeNoupir225::_sutur
	uint32_t ____sutur_5;
	// System.Single GarbageiOS.M_nirfoufeNoupir225::_reni
	float ____reni_6;
	// System.Boolean GarbageiOS.M_nirfoufeNoupir225::_setem
	bool ____setem_7;
	// System.String GarbageiOS.M_nirfoufeNoupir225::_jorgisgemDoustishee
	String_t* ____jorgisgemDoustishee_8;
	// System.Boolean GarbageiOS.M_nirfoufeNoupir225::_dertalFemtaw
	bool ____dertalFemtaw_9;
	// System.String GarbageiOS.M_nirfoufeNoupir225::_casmorba
	String_t* ____casmorba_10;
	// System.Int32 GarbageiOS.M_nirfoufeNoupir225::_medee
	int32_t ____medee_11;
	// System.Int32 GarbageiOS.M_nirfoufeNoupir225::_pisar
	int32_t ____pisar_12;
	// System.UInt32 GarbageiOS.M_nirfoufeNoupir225::_jeardirbawPobuxal
	uint32_t ____jeardirbawPobuxal_13;
	// System.UInt32 GarbageiOS.M_nirfoufeNoupir225::_fallpesairSouna
	uint32_t ____fallpesairSouna_14;
	// System.Single GarbageiOS.M_nirfoufeNoupir225::_rormabea
	float ____rormabea_15;
	// System.Boolean GarbageiOS.M_nirfoufeNoupir225::_hurbalSalcho
	bool ____hurbalSalcho_16;
	// System.Int32 GarbageiOS.M_nirfoufeNoupir225::_houdro
	int32_t ____houdro_17;
	// System.UInt32 GarbageiOS.M_nirfoufeNoupir225::_beanurDrelem
	uint32_t ____beanurDrelem_18;
	// System.String GarbageiOS.M_nirfoufeNoupir225::_sidiCallsere
	String_t* ____sidiCallsere_19;
	// System.Boolean GarbageiOS.M_nirfoufeNoupir225::_jeasteCouwouzi
	bool ____jeasteCouwouzi_20;
	// System.Int32 GarbageiOS.M_nirfoufeNoupir225::_mestusay
	int32_t ____mestusay_21;
	// System.String GarbageiOS.M_nirfoufeNoupir225::_werebowcawSakee
	String_t* ____werebowcawSakee_22;
	// System.Boolean GarbageiOS.M_nirfoufeNoupir225::_kiwalru
	bool ____kiwalru_23;
	// System.String GarbageiOS.M_nirfoufeNoupir225::_rembefouRawdow
	String_t* ____rembefouRawdow_24;
	// System.UInt32 GarbageiOS.M_nirfoufeNoupir225::_dejawhawCeferece
	uint32_t ____dejawhawCeferece_25;
	// System.Boolean GarbageiOS.M_nirfoufeNoupir225::_kiscemea
	bool ____kiscemea_26;

public:
	inline static int32_t get_offset_of__jawzalllasDerejacu_0() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____jawzalllasDerejacu_0)); }
	inline uint32_t get__jawzalllasDerejacu_0() const { return ____jawzalllasDerejacu_0; }
	inline uint32_t* get_address_of__jawzalllasDerejacu_0() { return &____jawzalllasDerejacu_0; }
	inline void set__jawzalllasDerejacu_0(uint32_t value)
	{
		____jawzalllasDerejacu_0 = value;
	}

	inline static int32_t get_offset_of__dawkiqayChaifere_1() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____dawkiqayChaifere_1)); }
	inline float get__dawkiqayChaifere_1() const { return ____dawkiqayChaifere_1; }
	inline float* get_address_of__dawkiqayChaifere_1() { return &____dawkiqayChaifere_1; }
	inline void set__dawkiqayChaifere_1(float value)
	{
		____dawkiqayChaifere_1 = value;
	}

	inline static int32_t get_offset_of__turtaiNarrea_2() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____turtaiNarrea_2)); }
	inline bool get__turtaiNarrea_2() const { return ____turtaiNarrea_2; }
	inline bool* get_address_of__turtaiNarrea_2() { return &____turtaiNarrea_2; }
	inline void set__turtaiNarrea_2(bool value)
	{
		____turtaiNarrea_2 = value;
	}

	inline static int32_t get_offset_of__towne_3() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____towne_3)); }
	inline String_t* get__towne_3() const { return ____towne_3; }
	inline String_t** get_address_of__towne_3() { return &____towne_3; }
	inline void set__towne_3(String_t* value)
	{
		____towne_3 = value;
		Il2CppCodeGenWriteBarrier(&____towne_3, value);
	}

	inline static int32_t get_offset_of__magi_4() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____magi_4)); }
	inline bool get__magi_4() const { return ____magi_4; }
	inline bool* get_address_of__magi_4() { return &____magi_4; }
	inline void set__magi_4(bool value)
	{
		____magi_4 = value;
	}

	inline static int32_t get_offset_of__sutur_5() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____sutur_5)); }
	inline uint32_t get__sutur_5() const { return ____sutur_5; }
	inline uint32_t* get_address_of__sutur_5() { return &____sutur_5; }
	inline void set__sutur_5(uint32_t value)
	{
		____sutur_5 = value;
	}

	inline static int32_t get_offset_of__reni_6() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____reni_6)); }
	inline float get__reni_6() const { return ____reni_6; }
	inline float* get_address_of__reni_6() { return &____reni_6; }
	inline void set__reni_6(float value)
	{
		____reni_6 = value;
	}

	inline static int32_t get_offset_of__setem_7() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____setem_7)); }
	inline bool get__setem_7() const { return ____setem_7; }
	inline bool* get_address_of__setem_7() { return &____setem_7; }
	inline void set__setem_7(bool value)
	{
		____setem_7 = value;
	}

	inline static int32_t get_offset_of__jorgisgemDoustishee_8() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____jorgisgemDoustishee_8)); }
	inline String_t* get__jorgisgemDoustishee_8() const { return ____jorgisgemDoustishee_8; }
	inline String_t** get_address_of__jorgisgemDoustishee_8() { return &____jorgisgemDoustishee_8; }
	inline void set__jorgisgemDoustishee_8(String_t* value)
	{
		____jorgisgemDoustishee_8 = value;
		Il2CppCodeGenWriteBarrier(&____jorgisgemDoustishee_8, value);
	}

	inline static int32_t get_offset_of__dertalFemtaw_9() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____dertalFemtaw_9)); }
	inline bool get__dertalFemtaw_9() const { return ____dertalFemtaw_9; }
	inline bool* get_address_of__dertalFemtaw_9() { return &____dertalFemtaw_9; }
	inline void set__dertalFemtaw_9(bool value)
	{
		____dertalFemtaw_9 = value;
	}

	inline static int32_t get_offset_of__casmorba_10() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____casmorba_10)); }
	inline String_t* get__casmorba_10() const { return ____casmorba_10; }
	inline String_t** get_address_of__casmorba_10() { return &____casmorba_10; }
	inline void set__casmorba_10(String_t* value)
	{
		____casmorba_10 = value;
		Il2CppCodeGenWriteBarrier(&____casmorba_10, value);
	}

	inline static int32_t get_offset_of__medee_11() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____medee_11)); }
	inline int32_t get__medee_11() const { return ____medee_11; }
	inline int32_t* get_address_of__medee_11() { return &____medee_11; }
	inline void set__medee_11(int32_t value)
	{
		____medee_11 = value;
	}

	inline static int32_t get_offset_of__pisar_12() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____pisar_12)); }
	inline int32_t get__pisar_12() const { return ____pisar_12; }
	inline int32_t* get_address_of__pisar_12() { return &____pisar_12; }
	inline void set__pisar_12(int32_t value)
	{
		____pisar_12 = value;
	}

	inline static int32_t get_offset_of__jeardirbawPobuxal_13() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____jeardirbawPobuxal_13)); }
	inline uint32_t get__jeardirbawPobuxal_13() const { return ____jeardirbawPobuxal_13; }
	inline uint32_t* get_address_of__jeardirbawPobuxal_13() { return &____jeardirbawPobuxal_13; }
	inline void set__jeardirbawPobuxal_13(uint32_t value)
	{
		____jeardirbawPobuxal_13 = value;
	}

	inline static int32_t get_offset_of__fallpesairSouna_14() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____fallpesairSouna_14)); }
	inline uint32_t get__fallpesairSouna_14() const { return ____fallpesairSouna_14; }
	inline uint32_t* get_address_of__fallpesairSouna_14() { return &____fallpesairSouna_14; }
	inline void set__fallpesairSouna_14(uint32_t value)
	{
		____fallpesairSouna_14 = value;
	}

	inline static int32_t get_offset_of__rormabea_15() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____rormabea_15)); }
	inline float get__rormabea_15() const { return ____rormabea_15; }
	inline float* get_address_of__rormabea_15() { return &____rormabea_15; }
	inline void set__rormabea_15(float value)
	{
		____rormabea_15 = value;
	}

	inline static int32_t get_offset_of__hurbalSalcho_16() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____hurbalSalcho_16)); }
	inline bool get__hurbalSalcho_16() const { return ____hurbalSalcho_16; }
	inline bool* get_address_of__hurbalSalcho_16() { return &____hurbalSalcho_16; }
	inline void set__hurbalSalcho_16(bool value)
	{
		____hurbalSalcho_16 = value;
	}

	inline static int32_t get_offset_of__houdro_17() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____houdro_17)); }
	inline int32_t get__houdro_17() const { return ____houdro_17; }
	inline int32_t* get_address_of__houdro_17() { return &____houdro_17; }
	inline void set__houdro_17(int32_t value)
	{
		____houdro_17 = value;
	}

	inline static int32_t get_offset_of__beanurDrelem_18() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____beanurDrelem_18)); }
	inline uint32_t get__beanurDrelem_18() const { return ____beanurDrelem_18; }
	inline uint32_t* get_address_of__beanurDrelem_18() { return &____beanurDrelem_18; }
	inline void set__beanurDrelem_18(uint32_t value)
	{
		____beanurDrelem_18 = value;
	}

	inline static int32_t get_offset_of__sidiCallsere_19() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____sidiCallsere_19)); }
	inline String_t* get__sidiCallsere_19() const { return ____sidiCallsere_19; }
	inline String_t** get_address_of__sidiCallsere_19() { return &____sidiCallsere_19; }
	inline void set__sidiCallsere_19(String_t* value)
	{
		____sidiCallsere_19 = value;
		Il2CppCodeGenWriteBarrier(&____sidiCallsere_19, value);
	}

	inline static int32_t get_offset_of__jeasteCouwouzi_20() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____jeasteCouwouzi_20)); }
	inline bool get__jeasteCouwouzi_20() const { return ____jeasteCouwouzi_20; }
	inline bool* get_address_of__jeasteCouwouzi_20() { return &____jeasteCouwouzi_20; }
	inline void set__jeasteCouwouzi_20(bool value)
	{
		____jeasteCouwouzi_20 = value;
	}

	inline static int32_t get_offset_of__mestusay_21() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____mestusay_21)); }
	inline int32_t get__mestusay_21() const { return ____mestusay_21; }
	inline int32_t* get_address_of__mestusay_21() { return &____mestusay_21; }
	inline void set__mestusay_21(int32_t value)
	{
		____mestusay_21 = value;
	}

	inline static int32_t get_offset_of__werebowcawSakee_22() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____werebowcawSakee_22)); }
	inline String_t* get__werebowcawSakee_22() const { return ____werebowcawSakee_22; }
	inline String_t** get_address_of__werebowcawSakee_22() { return &____werebowcawSakee_22; }
	inline void set__werebowcawSakee_22(String_t* value)
	{
		____werebowcawSakee_22 = value;
		Il2CppCodeGenWriteBarrier(&____werebowcawSakee_22, value);
	}

	inline static int32_t get_offset_of__kiwalru_23() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____kiwalru_23)); }
	inline bool get__kiwalru_23() const { return ____kiwalru_23; }
	inline bool* get_address_of__kiwalru_23() { return &____kiwalru_23; }
	inline void set__kiwalru_23(bool value)
	{
		____kiwalru_23 = value;
	}

	inline static int32_t get_offset_of__rembefouRawdow_24() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____rembefouRawdow_24)); }
	inline String_t* get__rembefouRawdow_24() const { return ____rembefouRawdow_24; }
	inline String_t** get_address_of__rembefouRawdow_24() { return &____rembefouRawdow_24; }
	inline void set__rembefouRawdow_24(String_t* value)
	{
		____rembefouRawdow_24 = value;
		Il2CppCodeGenWriteBarrier(&____rembefouRawdow_24, value);
	}

	inline static int32_t get_offset_of__dejawhawCeferece_25() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____dejawhawCeferece_25)); }
	inline uint32_t get__dejawhawCeferece_25() const { return ____dejawhawCeferece_25; }
	inline uint32_t* get_address_of__dejawhawCeferece_25() { return &____dejawhawCeferece_25; }
	inline void set__dejawhawCeferece_25(uint32_t value)
	{
		____dejawhawCeferece_25 = value;
	}

	inline static int32_t get_offset_of__kiscemea_26() { return static_cast<int32_t>(offsetof(M_nirfoufeNoupir225_t2250845276, ____kiscemea_26)); }
	inline bool get__kiscemea_26() const { return ____kiscemea_26; }
	inline bool* get_address_of__kiscemea_26() { return &____kiscemea_26; }
	inline void set__kiscemea_26(bool value)
	{
		____kiscemea_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
