﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua_Asset_AssetMgrGenerated
struct Mihua_Asset_AssetMgrGenerated_t1229231121;
// JSVCall
struct JSVCall_t3708497963;
// Mihua.Asset.OnLoadAsset
struct OnLoadAsset_t4181543125;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// UnityEngine.AssetBundle
struct AssetBundle_t2070959688;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_Asset_OnLoadAsset4181543125.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Mihua_Asset_AssetMgrGenerated::.ctor()
extern "C"  void Mihua_Asset_AssetMgrGenerated__ctor_m1736310906 (Mihua_Asset_AssetMgrGenerated_t1229231121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_AssetMgrGenerated::.cctor()
extern "C"  void Mihua_Asset_AssetMgrGenerated__cctor_m1803934323 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::AssetMgr_AssetMgr1(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_AssetMgr_AssetMgr1_m3104251448 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_AssetMgrGenerated::AssetMgr_maxTime(JSVCall)
extern "C"  void Mihua_Asset_AssetMgrGenerated_AssetMgr_maxTime_m3183746435 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_AssetMgrGenerated::AssetMgr_isClearAll(JSVCall)
extern "C"  void Mihua_Asset_AssetMgrGenerated_AssetMgr_isClearAll_m4270681594 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_AssetMgrGenerated::AssetMgr_unloading(JSVCall)
extern "C"  void Mihua_Asset_AssetMgrGenerated_AssetMgr_unloading_m85170193 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::AssetMgr_AddCache__String__UEObject(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_AssetMgr_AddCache__String__UEObject_m763467284 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::AssetMgr_Clear(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_AssetMgr_Clear_m2107567196 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::AssetMgr_ClearAll(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_AssetMgr_ClearAll_m3321231527 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::AssetMgr_DownLoad__String(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_AssetMgr_DownLoad__String_m2921502092 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::AssetMgr_DownLoad__ListT1_String(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_AssetMgr_DownLoad__ListT1_String_m52846172 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::AssetMgr_Exists__String(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_AssetMgr_Exists__String_m1114051264 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::AssetMgr_Exists__ListT1_String(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_AssetMgr_Exists__ListT1_String_m3876085416 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::AssetMgr_GetAssetBundles__String(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_AssetMgr_GetAssetBundles__String_m2975700727 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::AssetMgr_GetLoadedAssetT1__String(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_AssetMgr_GetLoadedAssetT1__String_m1165545270 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::AssetMgr_Initialize(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_AssetMgr_Initialize_m485989027 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Asset.OnLoadAsset Mihua_Asset_AssetMgrGenerated::AssetMgr_LoadAssetAsync_GetDelegate_member10_arg1(CSRepresentedObject)
extern "C"  OnLoadAsset_t4181543125 * Mihua_Asset_AssetMgrGenerated_AssetMgr_LoadAssetAsync_GetDelegate_member10_arg1_m1392894689 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::AssetMgr_LoadAssetAsync__String__OnLoadAsset__Boolean(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_AssetMgr_LoadAssetAsync__String__OnLoadAsset__Boolean_m4092686675 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Asset.OnLoadAsset Mihua_Asset_AssetMgrGenerated::AssetMgr_LoadAssetAsync_GetDelegate_member11_arg1(CSRepresentedObject)
extern "C"  OnLoadAsset_t4181543125 * Mihua_Asset_AssetMgrGenerated_AssetMgr_LoadAssetAsync_GetDelegate_member11_arg1_m2354508706 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::AssetMgr_LoadAssetAsync__String__OnLoadAsset(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_AssetMgr_LoadAssetAsync__String__OnLoadAsset_m3911379383 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::AssetMgr_LoadAssetAsync__String(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_AssetMgr_LoadAssetAsync__String_m1427212502 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::AssetMgr_LoadAssetBundles__String(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_AssetMgr_LoadAssetBundles__String_m4059028939 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::AssetMgr_LoadLevelAsync__String__String(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_AssetMgr_LoadLevelAsync__String__String_m3268889619 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::AssetMgr_LoadResourceT1__String(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_AssetMgr_LoadResourceT1__String_m1997973013 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::AssetMgr_PreloadingAsync__ListT1_String(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_AssetMgr_PreloadingAsync__ListT1_String_m3650703077 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::AssetMgr_SetNextNeedAssets__ListT1_String__Boolean(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_AssetMgr_SetNextNeedAssets__ListT1_String__Boolean_m3304233072 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::AssetMgr_UnloadUnusedAssets(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_AssetMgr_UnloadUnusedAssets_m1102696619 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_AssetMgrGenerated::__Register()
extern "C"  void Mihua_Asset_AssetMgrGenerated___Register_m3440556173 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Asset.OnLoadAsset Mihua_Asset_AssetMgrGenerated::<AssetMgr_LoadAssetAsync__String__OnLoadAsset__Boolean>m__7B()
extern "C"  OnLoadAsset_t4181543125 * Mihua_Asset_AssetMgrGenerated_U3CAssetMgr_LoadAssetAsync__String__OnLoadAsset__BooleanU3Em__7B_m571374371 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Asset.OnLoadAsset Mihua_Asset_AssetMgrGenerated::<AssetMgr_LoadAssetAsync__String__OnLoadAsset>m__7D()
extern "C"  OnLoadAsset_t4181543125 * Mihua_Asset_AssetMgrGenerated_U3CAssetMgr_LoadAssetAsync__String__OnLoadAssetU3Em__7D_m2282306557 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mihua_Asset_AssetMgrGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t Mihua_Asset_AssetMgrGenerated_ilo_getObject1_m3040809596 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::ilo_getBooleanS2(System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_ilo_getBooleanS2_m2925678699 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua_Asset_AssetMgrGenerated::ilo_getStringS3(System.Int32)
extern "C"  String_t* Mihua_Asset_AssetMgrGenerated_ilo_getStringS3_m1491919456 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_AssetMgrGenerated::ilo_Clear4()
extern "C"  void Mihua_Asset_AssetMgrGenerated_ilo_Clear4_m3329573828 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_AssetMgrGenerated::ilo_DownLoad5(System.Collections.Generic.List`1<System.String>)
extern "C"  void Mihua_Asset_AssetMgrGenerated_ilo_DownLoad5_m12054534 (Il2CppObject * __this /* static, unused */, List_1_t1375417109 * ___assetNames0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::ilo_Exists6(System.Collections.Generic.List`1<System.String>)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_ilo_Exists6_m925502279 (Il2CppObject * __this /* static, unused */, List_1_t1375417109 * ___assetNames0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundle Mihua_Asset_AssetMgrGenerated::ilo_GetAssetBundles7(System.String)
extern "C"  AssetBundle_t2070959688 * Mihua_Asset_AssetMgrGenerated_ilo_GetAssetBundles7_m779313181 (Il2CppObject * __this /* static, unused */, String_t* ____assetName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Asset.ABLoadOperation.AssetOperation Mihua_Asset_AssetMgrGenerated::ilo_LoadAssetAsync8(System.String,Mihua.Asset.OnLoadAsset,System.Boolean)
extern "C"  AssetOperation_t778728221 * Mihua_Asset_AssetMgrGenerated_ilo_LoadAssetAsync8_m520180949 (Il2CppObject * __this /* static, unused */, String_t* ___assetName0, OnLoadAsset_t4181543125 * ___onLoadAsset1, bool ___isAsync2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mihua_Asset_AssetMgrGenerated::ilo_setObject9(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t Mihua_Asset_AssetMgrGenerated_ilo_setObject9_m4023709692 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Asset.ABLoadOperation.AssetOperation Mihua_Asset_AssetMgrGenerated::ilo_LoadAssetAsync10(System.String,Mihua.Asset.OnLoadAsset)
extern "C"  AssetOperation_t778728221 * Mihua_Asset_AssetMgrGenerated_ilo_LoadAssetAsync10_m3976542093 (Il2CppObject * __this /* static, unused */, String_t* ___assetName0, OnLoadAsset_t4181543125 * ___onLoadAsset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mihua_Asset_AssetMgrGenerated::ilo_getObject11(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * Mihua_Asset_AssetMgrGenerated_ilo_getObject11_m2982809014 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_AssetMgrGenerated::ilo_isFunctionS12(System.Int32)
extern "C"  bool Mihua_Asset_AssetMgrGenerated_ilo_isFunctionS12_m364599804 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Asset.OnLoadAsset Mihua_Asset_AssetMgrGenerated::ilo_AssetMgr_LoadAssetAsync_GetDelegate_member10_arg113(CSRepresentedObject)
extern "C"  OnLoadAsset_t4181543125 * Mihua_Asset_AssetMgrGenerated_ilo_AssetMgr_LoadAssetAsync_GetDelegate_member10_arg113_m706488626 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
