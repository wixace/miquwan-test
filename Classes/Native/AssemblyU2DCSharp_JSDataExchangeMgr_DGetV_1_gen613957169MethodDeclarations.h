﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr_DGetV_1_gen4048467712MethodDeclarations.h"

// System.Void JSDataExchangeMgr/DGetV`1<UICamera/OnCustomInput>::.ctor(System.Object,System.IntPtr)
#define DGetV_1__ctor_m1646993789(__this, ___object0, ___method1, method) ((  void (*) (DGetV_1_t613957169 *, Il2CppObject *, IntPtr_t, const MethodInfo*))DGetV_1__ctor_m3356914363_gshared)(__this, ___object0, ___method1, method)
// T JSDataExchangeMgr/DGetV`1<UICamera/OnCustomInput>::Invoke()
#define DGetV_1_Invoke_m2550270264(__this, method) ((  OnCustomInput_t736305828 * (*) (DGetV_1_t613957169 *, const MethodInfo*))DGetV_1_Invoke_m1509146772_gshared)(__this, method)
// System.IAsyncResult JSDataExchangeMgr/DGetV`1<UICamera/OnCustomInput>::BeginInvoke(System.AsyncCallback,System.Object)
#define DGetV_1_BeginInvoke_m3264691860(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (DGetV_1_t613957169 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))DGetV_1_BeginInvoke_m2681184398_gshared)(__this, ___callback0, ___object1, method)
// T JSDataExchangeMgr/DGetV`1<UICamera/OnCustomInput>::EndInvoke(System.IAsyncResult)
#define DGetV_1_EndInvoke_m72529006(__this, ___result0, method) ((  OnCustomInput_t736305828 * (*) (DGetV_1_t613957169 *, Il2CppObject *, const MethodInfo*))DGetV_1_EndInvoke_m1776565962_gshared)(__this, ___result0, method)
