﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSDebugMessages
struct JSDebugMessages_t3442359798;
// System.String
struct String_t;
// JSDebugMessages/Message
struct Message_t903789774;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void JSDebugMessages::.ctor()
extern "C"  void JSDebugMessages__ctor_m926998069 (JSDebugMessages_t3442359798 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSDebugMessages::.cctor()
extern "C"  void JSDebugMessages__cctor_m2485040152 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSDebugMessages::Add(System.String)
extern "C"  void JSDebugMessages_Add_m917213006 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSDebugMessages/Message JSDebugMessages::ilo_Obtain1()
extern "C"  Message_t903789774 * JSDebugMessages_ilo_Obtain1_m1691698033 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
