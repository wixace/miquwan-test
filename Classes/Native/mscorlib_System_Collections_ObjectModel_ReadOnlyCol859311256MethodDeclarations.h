﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>
struct ReadOnlyCollection_1_t859311256;
// System.Collections.Generic.IList`1<Pathfinding.Voxels.VoxelContour>
struct IList_1_t1996880923;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.Voxels.VoxelContour[]
struct VoxelContourU5BU5D_t3554406569;
// System.Collections.Generic.IEnumerator`1<Pathfinding.Voxels.VoxelContour>
struct IEnumerator_1_t1214098769;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_VoxelContour3597201016.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m1000295237_gshared (ReadOnlyCollection_1_t859311256 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1000295237(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t859311256 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1000295237_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1857180335_gshared (ReadOnlyCollection_1_t859311256 * __this, VoxelContour_t3597201016  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1857180335(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t859311256 *, VoxelContour_t3597201016 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1857180335_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3744706555_gshared (ReadOnlyCollection_1_t859311256 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3744706555(__this, method) ((  void (*) (ReadOnlyCollection_1_t859311256 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3744706555_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3813050070_gshared (ReadOnlyCollection_1_t859311256 * __this, int32_t ___index0, VoxelContour_t3597201016  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3813050070(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t859311256 *, int32_t, VoxelContour_t3597201016 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3813050070_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3225631076_gshared (ReadOnlyCollection_1_t859311256 * __this, VoxelContour_t3597201016  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3225631076(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t859311256 *, VoxelContour_t3597201016 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3225631076_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1686902940_gshared (ReadOnlyCollection_1_t859311256 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1686902940(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t859311256 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1686902940_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  VoxelContour_t3597201016  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1868479264_gshared (ReadOnlyCollection_1_t859311256 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1868479264(__this, ___index0, method) ((  VoxelContour_t3597201016  (*) (ReadOnlyCollection_1_t859311256 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1868479264_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2543610413_gshared (ReadOnlyCollection_1_t859311256 * __this, int32_t ___index0, VoxelContour_t3597201016  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2543610413(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t859311256 *, int32_t, VoxelContour_t3597201016 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2543610413_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m575138731_gshared (ReadOnlyCollection_1_t859311256 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m575138731(__this, method) ((  bool (*) (ReadOnlyCollection_1_t859311256 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m575138731_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2273823092_gshared (ReadOnlyCollection_1_t859311256 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2273823092(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t859311256 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2273823092_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m684802159_gshared (ReadOnlyCollection_1_t859311256 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m684802159(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t859311256 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m684802159_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2954265058_gshared (ReadOnlyCollection_1_t859311256 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m2954265058(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t859311256 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m2954265058_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m4074219650_gshared (ReadOnlyCollection_1_t859311256 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m4074219650(__this, method) ((  void (*) (ReadOnlyCollection_1_t859311256 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m4074219650_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2580064298_gshared (ReadOnlyCollection_1_t859311256 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2580064298(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t859311256 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2580064298_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2325673786_gshared (ReadOnlyCollection_1_t859311256 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2325673786(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t859311256 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2325673786_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m4050395749_gshared (ReadOnlyCollection_1_t859311256 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m4050395749(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t859311256 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m4050395749_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2605090467_gshared (ReadOnlyCollection_1_t859311256 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2605090467(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t859311256 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2605090467_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m738781557_gshared (ReadOnlyCollection_1_t859311256 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m738781557(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t859311256 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m738781557_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m4250715890_gshared (ReadOnlyCollection_1_t859311256 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m4250715890(__this, method) ((  bool (*) (ReadOnlyCollection_1_t859311256 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m4250715890_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3744139230_gshared (ReadOnlyCollection_1_t859311256 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3744139230(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t859311256 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3744139230_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2765373209_gshared (ReadOnlyCollection_1_t859311256 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2765373209(__this, method) ((  bool (*) (ReadOnlyCollection_1_t859311256 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2765373209_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m4145445056_gshared (ReadOnlyCollection_1_t859311256 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m4145445056(__this, method) ((  bool (*) (ReadOnlyCollection_1_t859311256 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m4145445056_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m817637_gshared (ReadOnlyCollection_1_t859311256 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m817637(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t859311256 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m817637_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1803897084_gshared (ReadOnlyCollection_1_t859311256 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1803897084(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t859311256 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1803897084_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3206740649_gshared (ReadOnlyCollection_1_t859311256 * __this, VoxelContour_t3597201016  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3206740649(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t859311256 *, VoxelContour_t3597201016 , const MethodInfo*))ReadOnlyCollection_1_Contains_m3206740649_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m738329311_gshared (ReadOnlyCollection_1_t859311256 * __this, VoxelContourU5BU5D_t3554406569* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m738329311(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t859311256 *, VoxelContourU5BU5D_t3554406569*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m738329311_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2210414988_gshared (ReadOnlyCollection_1_t859311256 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2210414988(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t859311256 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2210414988_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m333616291_gshared (ReadOnlyCollection_1_t859311256 * __this, VoxelContour_t3597201016  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m333616291(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t859311256 *, VoxelContour_t3597201016 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m333616291_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1569477944_gshared (ReadOnlyCollection_1_t859311256 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1569477944(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t859311256 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1569477944_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>::get_Item(System.Int32)
extern "C"  VoxelContour_t3597201016  ReadOnlyCollection_1_get_Item_m907570400_gshared (ReadOnlyCollection_1_t859311256 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m907570400(__this, ___index0, method) ((  VoxelContour_t3597201016  (*) (ReadOnlyCollection_1_t859311256 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m907570400_gshared)(__this, ___index0, method)
