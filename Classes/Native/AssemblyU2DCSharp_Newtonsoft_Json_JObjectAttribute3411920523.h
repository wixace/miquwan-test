﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonContainerAtt1917602971.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_MemberSerializat1550301796.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JObjectAttribute
struct  JObjectAttribute_t3411920523  : public JsonContainerAttribute_t1917602971
{
public:
	// Newtonsoft.Json.MemberSerialization Newtonsoft.Json.JObjectAttribute::_memberSerialization
	int32_t ____memberSerialization_4;

public:
	inline static int32_t get_offset_of__memberSerialization_4() { return static_cast<int32_t>(offsetof(JObjectAttribute_t3411920523, ____memberSerialization_4)); }
	inline int32_t get__memberSerialization_4() const { return ____memberSerialization_4; }
	inline int32_t* get_address_of__memberSerialization_4() { return &____memberSerialization_4; }
	inline void set__memberSerialization_4(int32_t value)
	{
		____memberSerialization_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
