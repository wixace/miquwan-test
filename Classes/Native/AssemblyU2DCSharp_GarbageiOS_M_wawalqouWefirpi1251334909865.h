﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_wawalqouWefirpi125
struct  M_wawalqouWefirpi125_t1334909865  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_wawalqouWefirpi125::_jucor
	String_t* ____jucor_0;
	// System.Boolean GarbageiOS.M_wawalqouWefirpi125::_calbecaJasallfi
	bool ____calbecaJasallfi_1;
	// System.Boolean GarbageiOS.M_wawalqouWefirpi125::_kousarou
	bool ____kousarou_2;
	// System.Int32 GarbageiOS.M_wawalqouWefirpi125::_teniJasxo
	int32_t ____teniJasxo_3;
	// System.UInt32 GarbageiOS.M_wawalqouWefirpi125::_maspair
	uint32_t ____maspair_4;
	// System.Boolean GarbageiOS.M_wawalqouWefirpi125::_meartair
	bool ____meartair_5;
	// System.UInt32 GarbageiOS.M_wawalqouWefirpi125::_maireMeawirchou
	uint32_t ____maireMeawirchou_6;
	// System.Boolean GarbageiOS.M_wawalqouWefirpi125::_chukuSearmi
	bool ____chukuSearmi_7;
	// System.Int32 GarbageiOS.M_wawalqouWefirpi125::_hawna
	int32_t ____hawna_8;
	// System.UInt32 GarbageiOS.M_wawalqouWefirpi125::_rooli
	uint32_t ____rooli_9;
	// System.Boolean GarbageiOS.M_wawalqouWefirpi125::_qaspemsi
	bool ____qaspemsi_10;
	// System.Single GarbageiOS.M_wawalqouWefirpi125::_bemtretir
	float ____bemtretir_11;
	// System.UInt32 GarbageiOS.M_wawalqouWefirpi125::_fosoujoo
	uint32_t ____fosoujoo_12;
	// System.Single GarbageiOS.M_wawalqouWefirpi125::_lairnedal
	float ____lairnedal_13;

public:
	inline static int32_t get_offset_of__jucor_0() { return static_cast<int32_t>(offsetof(M_wawalqouWefirpi125_t1334909865, ____jucor_0)); }
	inline String_t* get__jucor_0() const { return ____jucor_0; }
	inline String_t** get_address_of__jucor_0() { return &____jucor_0; }
	inline void set__jucor_0(String_t* value)
	{
		____jucor_0 = value;
		Il2CppCodeGenWriteBarrier(&____jucor_0, value);
	}

	inline static int32_t get_offset_of__calbecaJasallfi_1() { return static_cast<int32_t>(offsetof(M_wawalqouWefirpi125_t1334909865, ____calbecaJasallfi_1)); }
	inline bool get__calbecaJasallfi_1() const { return ____calbecaJasallfi_1; }
	inline bool* get_address_of__calbecaJasallfi_1() { return &____calbecaJasallfi_1; }
	inline void set__calbecaJasallfi_1(bool value)
	{
		____calbecaJasallfi_1 = value;
	}

	inline static int32_t get_offset_of__kousarou_2() { return static_cast<int32_t>(offsetof(M_wawalqouWefirpi125_t1334909865, ____kousarou_2)); }
	inline bool get__kousarou_2() const { return ____kousarou_2; }
	inline bool* get_address_of__kousarou_2() { return &____kousarou_2; }
	inline void set__kousarou_2(bool value)
	{
		____kousarou_2 = value;
	}

	inline static int32_t get_offset_of__teniJasxo_3() { return static_cast<int32_t>(offsetof(M_wawalqouWefirpi125_t1334909865, ____teniJasxo_3)); }
	inline int32_t get__teniJasxo_3() const { return ____teniJasxo_3; }
	inline int32_t* get_address_of__teniJasxo_3() { return &____teniJasxo_3; }
	inline void set__teniJasxo_3(int32_t value)
	{
		____teniJasxo_3 = value;
	}

	inline static int32_t get_offset_of__maspair_4() { return static_cast<int32_t>(offsetof(M_wawalqouWefirpi125_t1334909865, ____maspair_4)); }
	inline uint32_t get__maspair_4() const { return ____maspair_4; }
	inline uint32_t* get_address_of__maspair_4() { return &____maspair_4; }
	inline void set__maspair_4(uint32_t value)
	{
		____maspair_4 = value;
	}

	inline static int32_t get_offset_of__meartair_5() { return static_cast<int32_t>(offsetof(M_wawalqouWefirpi125_t1334909865, ____meartair_5)); }
	inline bool get__meartair_5() const { return ____meartair_5; }
	inline bool* get_address_of__meartair_5() { return &____meartair_5; }
	inline void set__meartair_5(bool value)
	{
		____meartair_5 = value;
	}

	inline static int32_t get_offset_of__maireMeawirchou_6() { return static_cast<int32_t>(offsetof(M_wawalqouWefirpi125_t1334909865, ____maireMeawirchou_6)); }
	inline uint32_t get__maireMeawirchou_6() const { return ____maireMeawirchou_6; }
	inline uint32_t* get_address_of__maireMeawirchou_6() { return &____maireMeawirchou_6; }
	inline void set__maireMeawirchou_6(uint32_t value)
	{
		____maireMeawirchou_6 = value;
	}

	inline static int32_t get_offset_of__chukuSearmi_7() { return static_cast<int32_t>(offsetof(M_wawalqouWefirpi125_t1334909865, ____chukuSearmi_7)); }
	inline bool get__chukuSearmi_7() const { return ____chukuSearmi_7; }
	inline bool* get_address_of__chukuSearmi_7() { return &____chukuSearmi_7; }
	inline void set__chukuSearmi_7(bool value)
	{
		____chukuSearmi_7 = value;
	}

	inline static int32_t get_offset_of__hawna_8() { return static_cast<int32_t>(offsetof(M_wawalqouWefirpi125_t1334909865, ____hawna_8)); }
	inline int32_t get__hawna_8() const { return ____hawna_8; }
	inline int32_t* get_address_of__hawna_8() { return &____hawna_8; }
	inline void set__hawna_8(int32_t value)
	{
		____hawna_8 = value;
	}

	inline static int32_t get_offset_of__rooli_9() { return static_cast<int32_t>(offsetof(M_wawalqouWefirpi125_t1334909865, ____rooli_9)); }
	inline uint32_t get__rooli_9() const { return ____rooli_9; }
	inline uint32_t* get_address_of__rooli_9() { return &____rooli_9; }
	inline void set__rooli_9(uint32_t value)
	{
		____rooli_9 = value;
	}

	inline static int32_t get_offset_of__qaspemsi_10() { return static_cast<int32_t>(offsetof(M_wawalqouWefirpi125_t1334909865, ____qaspemsi_10)); }
	inline bool get__qaspemsi_10() const { return ____qaspemsi_10; }
	inline bool* get_address_of__qaspemsi_10() { return &____qaspemsi_10; }
	inline void set__qaspemsi_10(bool value)
	{
		____qaspemsi_10 = value;
	}

	inline static int32_t get_offset_of__bemtretir_11() { return static_cast<int32_t>(offsetof(M_wawalqouWefirpi125_t1334909865, ____bemtretir_11)); }
	inline float get__bemtretir_11() const { return ____bemtretir_11; }
	inline float* get_address_of__bemtretir_11() { return &____bemtretir_11; }
	inline void set__bemtretir_11(float value)
	{
		____bemtretir_11 = value;
	}

	inline static int32_t get_offset_of__fosoujoo_12() { return static_cast<int32_t>(offsetof(M_wawalqouWefirpi125_t1334909865, ____fosoujoo_12)); }
	inline uint32_t get__fosoujoo_12() const { return ____fosoujoo_12; }
	inline uint32_t* get_address_of__fosoujoo_12() { return &____fosoujoo_12; }
	inline void set__fosoujoo_12(uint32_t value)
	{
		____fosoujoo_12 = value;
	}

	inline static int32_t get_offset_of__lairnedal_13() { return static_cast<int32_t>(offsetof(M_wawalqouWefirpi125_t1334909865, ____lairnedal_13)); }
	inline float get__lairnedal_13() const { return ____lairnedal_13; }
	inline float* get_address_of__lairnedal_13() { return &____lairnedal_13; }
	inline void set__lairnedal_13(float value)
	{
		____lairnedal_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
