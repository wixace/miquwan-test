﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEventListenerGenerated/<UIEventListener_onKey_GetDelegate_member14_arg0>c__AnonStoreyC2
struct U3CUIEventListener_onKey_GetDelegate_member14_arg0U3Ec__AnonStoreyC2_t3354147370;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"

// System.Void UIEventListenerGenerated/<UIEventListener_onKey_GetDelegate_member14_arg0>c__AnonStoreyC2::.ctor()
extern "C"  void U3CUIEventListener_onKey_GetDelegate_member14_arg0U3Ec__AnonStoreyC2__ctor_m2136644225 (U3CUIEventListener_onKey_GetDelegate_member14_arg0U3Ec__AnonStoreyC2_t3354147370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated/<UIEventListener_onKey_GetDelegate_member14_arg0>c__AnonStoreyC2::<>m__148(UnityEngine.GameObject,UnityEngine.KeyCode)
extern "C"  void U3CUIEventListener_onKey_GetDelegate_member14_arg0U3Ec__AnonStoreyC2_U3CU3Em__148_m1647612412 (U3CUIEventListener_onKey_GetDelegate_member14_arg0U3Ec__AnonStoreyC2_t3354147370 * __this, GameObject_t3674682005 * ___go0, int32_t ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
