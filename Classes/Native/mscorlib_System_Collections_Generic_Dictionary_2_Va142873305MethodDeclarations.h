﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,System.Object>
struct ValueCollection_t142873305;
// System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>
struct Dictionary_2_t1442267592;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3669068296.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m203935594_gshared (ValueCollection_t142873305 * __this, Dictionary_2_t1442267592 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m203935594(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t142873305 *, Dictionary_2_t1442267592 *, const MethodInfo*))ValueCollection__ctor_m203935594_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2924074888_gshared (ValueCollection_t142873305 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2924074888(__this, ___item0, method) ((  void (*) (ValueCollection_t142873305 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2924074888_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1579731025_gshared (ValueCollection_t142873305 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1579731025(__this, method) ((  void (*) (ValueCollection_t142873305 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1579731025_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1877017374_gshared (ValueCollection_t142873305 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1877017374(__this, ___item0, method) ((  bool (*) (ValueCollection_t142873305 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1877017374_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1723616067_gshared (ValueCollection_t142873305 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1723616067(__this, ___item0, method) ((  bool (*) (ValueCollection_t142873305 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1723616067_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m663527199_gshared (ValueCollection_t142873305 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m663527199(__this, method) ((  Il2CppObject* (*) (ValueCollection_t142873305 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m663527199_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3764516885_gshared (ValueCollection_t142873305 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3764516885(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t142873305 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3764516885_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2066971856_gshared (ValueCollection_t142873305 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2066971856(__this, method) ((  Il2CppObject * (*) (ValueCollection_t142873305 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2066971856_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m152193233_gshared (ValueCollection_t142873305 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m152193233(__this, method) ((  bool (*) (ValueCollection_t142873305 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m152193233_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2158657585_gshared (ValueCollection_t142873305 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2158657585(__this, method) ((  bool (*) (ValueCollection_t142873305 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2158657585_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2764646045_gshared (ValueCollection_t142873305 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2764646045(__this, method) ((  Il2CppObject * (*) (ValueCollection_t142873305 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2764646045_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m487491889_gshared (ValueCollection_t142873305 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m487491889(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t142873305 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m487491889_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3669068296  ValueCollection_GetEnumerator_m119970324_gshared (ValueCollection_t142873305 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m119970324(__this, method) ((  Enumerator_t3669068296  (*) (ValueCollection_t142873305 *, const MethodInfo*))ValueCollection_GetEnumerator_m119970324_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1354876151_gshared (ValueCollection_t142873305 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1354876151(__this, method) ((  int32_t (*) (ValueCollection_t142873305 *, const MethodInfo*))ValueCollection_get_Count_m1354876151_gshared)(__this, method)
