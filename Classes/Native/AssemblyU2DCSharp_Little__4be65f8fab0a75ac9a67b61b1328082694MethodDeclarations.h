﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._4be65f8fab0a75ac9a67b61b1cb6edf0
struct _4be65f8fab0a75ac9a67b61b1cb6edf0_t1328082694;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._4be65f8fab0a75ac9a67b61b1cb6edf0::.ctor()
extern "C"  void _4be65f8fab0a75ac9a67b61b1cb6edf0__ctor_m1139785191 (_4be65f8fab0a75ac9a67b61b1cb6edf0_t1328082694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._4be65f8fab0a75ac9a67b61b1cb6edf0::_4be65f8fab0a75ac9a67b61b1cb6edf0m2(System.Int32)
extern "C"  int32_t _4be65f8fab0a75ac9a67b61b1cb6edf0__4be65f8fab0a75ac9a67b61b1cb6edf0m2_m3706684377 (_4be65f8fab0a75ac9a67b61b1cb6edf0_t1328082694 * __this, int32_t ____4be65f8fab0a75ac9a67b61b1cb6edf0a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._4be65f8fab0a75ac9a67b61b1cb6edf0::_4be65f8fab0a75ac9a67b61b1cb6edf0m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _4be65f8fab0a75ac9a67b61b1cb6edf0__4be65f8fab0a75ac9a67b61b1cb6edf0m_m1219373821 (_4be65f8fab0a75ac9a67b61b1cb6edf0_t1328082694 * __this, int32_t ____4be65f8fab0a75ac9a67b61b1cb6edf0a0, int32_t ____4be65f8fab0a75ac9a67b61b1cb6edf021, int32_t ____4be65f8fab0a75ac9a67b61b1cb6edf0c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
