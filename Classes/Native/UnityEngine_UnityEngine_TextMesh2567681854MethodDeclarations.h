﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.TextMesh
struct TextMesh_t2567681854;
// System.String
struct String_t;
// UnityEngine.Font
struct Font_t4241557075;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Font4241557075.h"
#include "UnityEngine_UnityEngine_FontStyle3350479768.h"
#include "UnityEngine_UnityEngine_TextAlignment2019773356.h"
#include "UnityEngine_UnityEngine_TextAnchor213922566.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Void UnityEngine.TextMesh::.ctor()
extern "C"  void TextMesh__ctor_m3932301811 (TextMesh_t2567681854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.TextMesh::get_text()
extern "C"  String_t* TextMesh_get_text_m2892967978 (TextMesh_t2567681854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::set_text(System.String)
extern "C"  void TextMesh_set_text_m3628430759 (TextMesh_t2567681854 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Font UnityEngine.TextMesh::get_font()
extern "C"  Font_t4241557075 * TextMesh_get_font_m4027752146 (TextMesh_t2567681854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::set_font(UnityEngine.Font)
extern "C"  void TextMesh_set_font_m3143270039 (TextMesh_t2567681854 * __this, Font_t4241557075 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.TextMesh::get_fontSize()
extern "C"  int32_t TextMesh_get_fontSize_m1738065610 (TextMesh_t2567681854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::set_fontSize(System.Int32)
extern "C"  void TextMesh_set_fontSize_m3177501199 (TextMesh_t2567681854 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.FontStyle UnityEngine.TextMesh::get_fontStyle()
extern "C"  int32_t TextMesh_get_fontStyle_m467214274 (TextMesh_t2567681854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::set_fontStyle(UnityEngine.FontStyle)
extern "C"  void TextMesh_set_fontStyle_m3496985489 (TextMesh_t2567681854 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.TextMesh::get_offsetZ()
extern "C"  float TextMesh_get_offsetZ_m1206551395 (TextMesh_t2567681854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::set_offsetZ(System.Single)
extern "C"  void TextMesh_set_offsetZ_m96475536 (TextMesh_t2567681854 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TextAlignment UnityEngine.TextMesh::get_alignment()
extern "C"  int32_t TextMesh_get_alignment_m242781399 (TextMesh_t2567681854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::set_alignment(UnityEngine.TextAlignment)
extern "C"  void TextMesh_set_alignment_m4109382812 (TextMesh_t2567681854 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TextAnchor UnityEngine.TextMesh::get_anchor()
extern "C"  int32_t TextMesh_get_anchor_m3055071013 (TextMesh_t2567681854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::set_anchor(UnityEngine.TextAnchor)
extern "C"  void TextMesh_set_anchor_m2427405930 (TextMesh_t2567681854 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.TextMesh::get_characterSize()
extern "C"  float TextMesh_get_characterSize_m1063805702 (TextMesh_t2567681854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::set_characterSize(System.Single)
extern "C"  void TextMesh_set_characterSize_m1995082957 (TextMesh_t2567681854 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.TextMesh::get_lineSpacing()
extern "C"  float TextMesh_get_lineSpacing_m3815755627 (TextMesh_t2567681854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::set_lineSpacing(System.Single)
extern "C"  void TextMesh_set_lineSpacing_m304948872 (TextMesh_t2567681854 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.TextMesh::get_tabSize()
extern "C"  float TextMesh_get_tabSize_m467799730 (TextMesh_t2567681854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::set_tabSize(System.Single)
extern "C"  void TextMesh_set_tabSize_m3138627233 (TextMesh_t2567681854 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.TextMesh::get_richText()
extern "C"  bool TextMesh_get_richText_m358456105 (TextMesh_t2567681854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::set_richText(System.Boolean)
extern "C"  void TextMesh_set_richText_m3853564910 (TextMesh_t2567681854 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.TextMesh::get_color()
extern "C"  Color_t4194546905  TextMesh_get_color_m3561682884 (TextMesh_t2567681854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::set_color(UnityEngine.Color)
extern "C"  void TextMesh_set_color_m115266575 (TextMesh_t2567681854 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::INTERNAL_get_color(UnityEngine.Color&)
extern "C"  void TextMesh_INTERNAL_get_color_m2973257519 (TextMesh_t2567681854 * __this, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::INTERNAL_set_color(UnityEngine.Color&)
extern "C"  void TextMesh_INTERNAL_set_color_m2700543291 (TextMesh_t2567681854 * __this, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
