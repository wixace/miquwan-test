﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil
struct BehaviourUtil_t1147831871;
// UnityEngine.Coroutine
struct Coroutine_t3621161934;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// Mihua.Utils.WaitForSeconds
struct WaitForSeconds_t3217447863;
// BehaviourUtil/JSDelayFun0
struct JSDelayFun0_t1074711451;
// BehaviourUtil/JSDelayFun1
struct JSDelayFun1_t1074711452;
// System.Object
struct Il2CppObject;
// BehaviourUtil/JSDelayFun2
struct JSDelayFun2_t1074711453;
// BehaviourUtil/JSDelayFun3
struct JSDelayFun3_t1074711454;
// BehaviourUtil/JSDelayFun4
struct JSDelayFun4_t1074711455;
// System.Action
struct Action_t3771233898;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t667441552;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mihua_Utils_WaitForSeconds3217447863.h"
#include "AssemblyU2DCSharp_BehaviourUtil_JSDelayFun01074711451.h"
#include "AssemblyU2DCSharp_BehaviourUtil_JSDelayFun11074711452.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_BehaviourUtil_JSDelayFun21074711453.h"
#include "AssemblyU2DCSharp_BehaviourUtil_JSDelayFun31074711454.h"
#include "AssemblyU2DCSharp_BehaviourUtil_JSDelayFun41074711455.h"
#include "System_Core_System_Action3771233898.h"

// System.Void BehaviourUtil::.ctor()
extern "C"  void BehaviourUtil__ctor_m2295951884 (BehaviourUtil_t1147831871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil::.cctor()
extern "C"  void BehaviourUtil__cctor_m1972935457 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil::Update()
extern "C"  void BehaviourUtil_Update_m4181893761 (BehaviourUtil_t1147831871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine BehaviourUtil::StartCoroutine(System.Collections.IEnumerator,System.UInt32)
extern "C"  Coroutine_t3621161934 * BehaviourUtil_StartCoroutine_m2546279771 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___routine0, uint32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine BehaviourUtil::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t3621161934 * BehaviourUtil_StartCoroutine_m2366876519 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___routine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine BehaviourUtil::StartCoroutine(Mihua.Utils.WaitForSeconds)
extern "C"  Coroutine_t3621161934 * BehaviourUtil_StartCoroutine_m3751679742 (Il2CppObject * __this /* static, unused */, WaitForSeconds_t3217447863 * ___routine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil::StopAllCoroutine()
extern "C"  void BehaviourUtil_StopAllCoroutine_m3836482641 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil::StopCoroutine(System.UInt32)
extern "C"  void BehaviourUtil_StopCoroutine_m1083154266 (Il2CppObject * __this /* static, unused */, uint32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil::SetPause(System.UInt32,System.Boolean)
extern "C"  void BehaviourUtil_SetPause_m1763661103 (Il2CppObject * __this /* static, unused */, uint32_t ___id0, bool ___isPause1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil::StopCoroutine(System.Collections.IEnumerator)
extern "C"  void BehaviourUtil_StopCoroutine_m2478554795 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___routine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 BehaviourUtil::JSDelayCall(System.Single,BehaviourUtil/JSDelayFun0)
extern "C"  uint32_t BehaviourUtil_JSDelayCall_m903866911 (Il2CppObject * __this /* static, unused */, float ___delaytime0, JSDelayFun0_t1074711451 * ___act1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BehaviourUtil::JSDelayCallExec(System.Single,BehaviourUtil/JSDelayFun0)
extern "C"  Il2CppObject * BehaviourUtil_JSDelayCallExec_m1761217 (Il2CppObject * __this /* static, unused */, float ___delaytime0, JSDelayFun0_t1074711451 * ___act1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 BehaviourUtil::JSDelayCall(System.Single,BehaviourUtil/JSDelayFun1,System.Object)
extern "C"  uint32_t BehaviourUtil_JSDelayCall_m1939370124 (Il2CppObject * __this /* static, unused */, float ___delaytime0, JSDelayFun1_t1074711452 * ___act1, Il2CppObject * ___arg12, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BehaviourUtil::JSDelayCallExec(System.Single,BehaviourUtil/JSDelayFun1,System.Object)
extern "C"  Il2CppObject * BehaviourUtil_JSDelayCallExec_m2849759918 (Il2CppObject * __this /* static, unused */, float ___delaytime0, JSDelayFun1_t1074711452 * ___act1, Il2CppObject * ___arg12, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 BehaviourUtil::JSDelayCall(System.Single,BehaviourUtil/JSDelayFun2,System.Object,System.Object)
extern "C"  uint32_t BehaviourUtil_JSDelayCall_m2322956793 (Il2CppObject * __this /* static, unused */, float ___delaytime0, JSDelayFun2_t1074711453 * ___act1, Il2CppObject * ___arg12, Il2CppObject * ___arg23, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BehaviourUtil::JSDelayCallExec(System.Single,BehaviourUtil/JSDelayFun2,System.Object,System.Object)
extern "C"  Il2CppObject * BehaviourUtil_JSDelayCallExec_m3018690715 (Il2CppObject * __this /* static, unused */, float ___delaytime0, JSDelayFun2_t1074711453 * ___act1, Il2CppObject * ___arg12, Il2CppObject * ___arg23, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 BehaviourUtil::JSDelayCall(System.Single,BehaviourUtil/JSDelayFun3,System.Object,System.Object,System.Object)
extern "C"  uint32_t BehaviourUtil_JSDelayCall_m1900858982 (Il2CppObject * __this /* static, unused */, float ___delaytime0, JSDelayFun3_t1074711454 * ___act1, Il2CppObject * ___arg12, Il2CppObject * ___arg23, Il2CppObject * ___arg34, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BehaviourUtil::JSDelayCallExec(System.Single,BehaviourUtil/JSDelayFun3,System.Object,System.Object,System.Object)
extern "C"  Il2CppObject * BehaviourUtil_JSDelayCallExec_m2238552456 (Il2CppObject * __this /* static, unused */, float ___delaytime0, JSDelayFun3_t1074711454 * ___act1, Il2CppObject * ___arg12, Il2CppObject * ___arg23, Il2CppObject * ___arg34, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 BehaviourUtil::JSDelayCall(System.Single,BehaviourUtil/JSDelayFun4,System.Object,System.Object,System.Object,System.Object)
extern "C"  uint32_t BehaviourUtil_JSDelayCall_m1138492883 (Il2CppObject * __this /* static, unused */, float ___delaytime0, JSDelayFun4_t1074711455 * ___act1, Il2CppObject * ___arg12, Il2CppObject * ___arg23, Il2CppObject * ___arg34, Il2CppObject * ___arg45, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BehaviourUtil::JSDelayCallExec(System.Single,BehaviourUtil/JSDelayFun4,System.Object,System.Object,System.Object,System.Object)
extern "C"  Il2CppObject * BehaviourUtil_JSDelayCallExec_m2355211637 (Il2CppObject * __this /* static, unused */, float ___delaytime0, JSDelayFun4_t1074711455 * ___act1, Il2CppObject * ___arg12, Il2CppObject * ___arg23, Il2CppObject * ___arg34, Il2CppObject * ___arg45, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 BehaviourUtil::DelayCall(System.Single,System.Action)
extern "C"  uint32_t BehaviourUtil_DelayCall_m2000187420 (Il2CppObject * __this /* static, unused */, float ___delaytime0, Action_t3771233898 * ___act1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BehaviourUtil::DelayCallExec(System.Single,System.Action)
extern "C"  Il2CppObject * BehaviourUtil_DelayCallExec_m311035390 (Il2CppObject * __this /* static, unused */, float ___delaytime0, Action_t3771233898 * ___act1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Utils.WaitForSeconds BehaviourUtil::GetWaitSeconds(System.Single)
extern "C"  WaitForSeconds_t3217447863 * BehaviourUtil_GetWaitSeconds_m1247145854 (Il2CppObject * __this /* static, unused */, float ___delaytime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil::DelayPause(System.Single)
extern "C"  void BehaviourUtil_DelayPause_m568352160 (Il2CppObject * __this /* static, unused */, float ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MonoBehaviour BehaviourUtil::get_GlobalCoroutine()
extern "C"  MonoBehaviour_t667441552 * BehaviourUtil_get_GlobalCoroutine_m3559674726 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil::ilo_Update1(Mihua.Utils.WaitForSeconds)
extern "C"  void BehaviourUtil_ilo_Update1_m252335105 (Il2CppObject * __this /* static, unused */, WaitForSeconds_t3217447863 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MonoBehaviour BehaviourUtil::ilo_get_GlobalCoroutine2()
extern "C"  MonoBehaviour_t667441552 * BehaviourUtil_ilo_get_GlobalCoroutine2_m1555413345 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BehaviourUtil::ilo_JSDelayCallExec3(System.Single,BehaviourUtil/JSDelayFun3,System.Object,System.Object,System.Object)
extern "C"  Il2CppObject * BehaviourUtil_ilo_JSDelayCallExec3_m856970006 (Il2CppObject * __this /* static, unused */, float ___delaytime0, JSDelayFun3_t1074711454 * ___act1, Il2CppObject * ___arg12, Il2CppObject * ___arg23, Il2CppObject * ___arg34, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BehaviourUtil::ilo_JSDelayCallExec4(System.Single,BehaviourUtil/JSDelayFun4,System.Object,System.Object,System.Object,System.Object)
extern "C"  Il2CppObject * BehaviourUtil_ilo_JSDelayCallExec4_m2385830562 (Il2CppObject * __this /* static, unused */, float ___delaytime0, JSDelayFun4_t1074711455 * ___act1, Il2CppObject * ___arg12, Il2CppObject * ___arg23, Il2CppObject * ___arg34, Il2CppObject * ___arg45, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BehaviourUtil::ilo_DelayCallExec5(System.Single,System.Action)
extern "C"  Il2CppObject * BehaviourUtil_ilo_DelayCallExec5_m1406015484 (Il2CppObject * __this /* static, unused */, float ___delaytime0, Action_t3771233898 * ___act1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine BehaviourUtil::ilo_StartCoroutine6(System.Collections.IEnumerator,System.UInt32)
extern "C"  Coroutine_t3621161934 * BehaviourUtil_ilo_StartCoroutine6_m1157543912 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___routine0, uint32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
