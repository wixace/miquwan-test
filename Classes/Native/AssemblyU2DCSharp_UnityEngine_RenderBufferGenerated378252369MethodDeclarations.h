﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_RenderBufferGenerated
struct UnityEngine_RenderBufferGenerated_t378252369;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_RenderBufferGenerated::.ctor()
extern "C"  void UnityEngine_RenderBufferGenerated__ctor_m1544518714 (UnityEngine_RenderBufferGenerated_t378252369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderBufferGenerated::.cctor()
extern "C"  void UnityEngine_RenderBufferGenerated__cctor_m153343667 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderBufferGenerated::RenderBuffer_RenderBuffer1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RenderBufferGenerated_RenderBuffer_RenderBuffer1_m434763832 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RenderBufferGenerated::RenderBuffer_GetNativeRenderBufferPtr(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RenderBufferGenerated_RenderBuffer_GetNativeRenderBufferPtr_m693459504 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RenderBufferGenerated::__Register()
extern "C"  void UnityEngine_RenderBufferGenerated___Register_m920163533 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
