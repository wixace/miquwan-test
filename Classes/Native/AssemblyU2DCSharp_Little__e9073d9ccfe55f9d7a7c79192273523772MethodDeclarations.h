﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._e9073d9ccfe55f9d7a7c79199f47cb3b
struct _e9073d9ccfe55f9d7a7c79199f47cb3b_t2273523772;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._e9073d9ccfe55f9d7a7c79199f47cb3b::.ctor()
extern "C"  void _e9073d9ccfe55f9d7a7c79199f47cb3b__ctor_m1234530673 (_e9073d9ccfe55f9d7a7c79199f47cb3b_t2273523772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e9073d9ccfe55f9d7a7c79199f47cb3b::_e9073d9ccfe55f9d7a7c79199f47cb3bm2(System.Int32)
extern "C"  int32_t _e9073d9ccfe55f9d7a7c79199f47cb3b__e9073d9ccfe55f9d7a7c79199f47cb3bm2_m310921241 (_e9073d9ccfe55f9d7a7c79199f47cb3b_t2273523772 * __this, int32_t ____e9073d9ccfe55f9d7a7c79199f47cb3ba0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e9073d9ccfe55f9d7a7c79199f47cb3b::_e9073d9ccfe55f9d7a7c79199f47cb3bm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _e9073d9ccfe55f9d7a7c79199f47cb3b__e9073d9ccfe55f9d7a7c79199f47cb3bm_m3893276349 (_e9073d9ccfe55f9d7a7c79199f47cb3b_t2273523772 * __this, int32_t ____e9073d9ccfe55f9d7a7c79199f47cb3ba0, int32_t ____e9073d9ccfe55f9d7a7c79199f47cb3b621, int32_t ____e9073d9ccfe55f9d7a7c79199f47cb3bc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
