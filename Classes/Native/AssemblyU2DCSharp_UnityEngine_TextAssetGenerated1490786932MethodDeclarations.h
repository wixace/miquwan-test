﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_TextAssetGenerated
struct UnityEngine_TextAssetGenerated_t1490786932;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_TextAssetGenerated::.ctor()
extern "C"  void UnityEngine_TextAssetGenerated__ctor_m2882765991 (UnityEngine_TextAssetGenerated_t1490786932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextAssetGenerated::TextAsset_TextAsset1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextAssetGenerated_TextAsset_TextAsset1_m1844207339 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextAssetGenerated::TextAsset_text(JSVCall)
extern "C"  void UnityEngine_TextAssetGenerated_TextAsset_text_m1032729153 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextAssetGenerated::TextAsset_bytes(JSVCall)
extern "C"  void UnityEngine_TextAssetGenerated_TextAsset_bytes_m373282611 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextAssetGenerated::TextAsset_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextAssetGenerated_TextAsset_ToString_m1548921609 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextAssetGenerated::__Register()
extern "C"  void UnityEngine_TextAssetGenerated___Register_m2186922112 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_TextAssetGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_TextAssetGenerated_ilo_getObject1_m3182692427 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextAssetGenerated::ilo_moveSaveID2Arr2(System.Int32)
extern "C"  void UnityEngine_TextAssetGenerated_ilo_moveSaveID2Arr2_m2242333245 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
