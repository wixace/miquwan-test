﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Voxels.CompactVoxelCell
struct  CompactVoxelCell_t3431095593 
{
public:
	// System.UInt32 Pathfinding.Voxels.CompactVoxelCell::index
	uint32_t ___index_0;
	// System.UInt32 Pathfinding.Voxels.CompactVoxelCell::count
	uint32_t ___count_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(CompactVoxelCell_t3431095593, ___index_0)); }
	inline uint32_t get_index_0() const { return ___index_0; }
	inline uint32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(uint32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(CompactVoxelCell_t3431095593, ___count_1)); }
	inline uint32_t get_count_1() const { return ___count_1; }
	inline uint32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(uint32_t value)
	{
		___count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Pathfinding.Voxels.CompactVoxelCell
struct CompactVoxelCell_t3431095593_marshaled_pinvoke
{
	uint32_t ___index_0;
	uint32_t ___count_1;
};
// Native definition for marshalling of: Pathfinding.Voxels.CompactVoxelCell
struct CompactVoxelCell_t3431095593_marshaled_com
{
	uint32_t ___index_0;
	uint32_t ___count_1;
};
