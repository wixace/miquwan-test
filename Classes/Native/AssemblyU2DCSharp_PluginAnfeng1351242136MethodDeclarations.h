﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginAnfeng
struct PluginAnfeng_t1351242136;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// System.Object
struct Il2CppObject;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// Mihua.SDK.EnterGameInfo
struct EnterGameInfo_t1897532954;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Action
struct Action_t3771233898;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_PluginAnfeng1351242136.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"

// System.Void PluginAnfeng::.ctor()
extern "C"  void PluginAnfeng__ctor_m2061757443 (PluginAnfeng_t1351242136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::Init()
extern "C"  void PluginAnfeng_Init_m3758822897 (PluginAnfeng_t1351242136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginAnfeng::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginAnfeng_ReqSDKHttpLogin_m2726018200 (PluginAnfeng_t1351242136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginAnfeng::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginAnfeng_IsLoginSuccess_m3418210288 (PluginAnfeng_t1351242136 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::OpenUserLogin()
extern "C"  void PluginAnfeng_OpenUserLogin_m4106115925 (PluginAnfeng_t1351242136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::UserPay(CEvent.ZEvent)
extern "C"  void PluginAnfeng_UserPay_m933019261 (PluginAnfeng_t1351242136 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginAnfeng_SignCallBack_m3257193622 (PluginAnfeng_t1351242136 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginAnfeng::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginAnfeng_BuildOrderParam2WWWForm_m3907888510 (PluginAnfeng_t1351242136 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::EnterGame(CEvent.ZEvent)
extern "C"  void PluginAnfeng_EnterGame_m1199891664 (PluginAnfeng_t1351242136 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginAnfeng_RoleUpgrade_m2380116852 (PluginAnfeng_t1351242136 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::OnLoginSuccess(System.String)
extern "C"  void PluginAnfeng_OnLoginSuccess_m589147848 (PluginAnfeng_t1351242136 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::OnLogoutSuccess(System.String)
extern "C"  void PluginAnfeng_OnLogoutSuccess_m1087256775 (PluginAnfeng_t1351242136 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::OnLogout(System.String)
extern "C"  void PluginAnfeng_OnLogout_m597715032 (PluginAnfeng_t1351242136 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::initSdk(System.String,System.String,System.String,System.String)
extern "C"  void PluginAnfeng_initSdk_m2581687819 (PluginAnfeng_t1351242136 * __this, String_t* ___anAppid0, String_t* ___anScrectkey1, String_t* ___dataeyeAppid2, String_t* ___trackingIOAppid3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::login()
extern "C"  void PluginAnfeng_login_m1583772266 (PluginAnfeng_t1351242136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::logout()
extern "C"  void PluginAnfeng_logout_m1858122699 (PluginAnfeng_t1351242136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::userInfo(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginAnfeng_userInfo_m640585368 (PluginAnfeng_t1351242136 * __this, String_t* ___serverid0, String_t* ___servername1, String_t* ___roleid2, String_t* ___rolelv3, String_t* ___rolename4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginAnfeng_pay_m2678959521 (PluginAnfeng_t1351242136 * __this, String_t* ___orderId0, String_t* ___serverid1, String_t* ___servername2, String_t* ___roleid3, String_t* ___rolelv4, String_t* ___rolename5, String_t* ___productid6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::<OnLogoutSuccess>m__40B()
extern "C"  void PluginAnfeng_U3COnLogoutSuccessU3Em__40B_m3814578298 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::<OnLogout>m__40C()
extern "C"  void PluginAnfeng_U3COnLogoutU3Em__40C_m4094176916 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginAnfeng_ilo_AddEventListener1_m1522897041 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::ilo_initSdk2(PluginAnfeng,System.String,System.String,System.String,System.String)
extern "C"  void PluginAnfeng_ilo_initSdk2_m4251427188 (Il2CppObject * __this /* static, unused */, PluginAnfeng_t1351242136 * ____this0, String_t* ___anAppid1, String_t* ___anScrectkey2, String_t* ___dataeyeAppid3, String_t* ___trackingIOAppid4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginAnfeng::ilo_get_Instance3()
extern "C"  VersionMgr_t1322950208 * PluginAnfeng_ilo_get_Instance3_m2762140742 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginAnfeng::ilo_get_isSdkLogin4(VersionMgr)
extern "C"  bool PluginAnfeng_ilo_get_isSdkLogin4_m1836333836 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::ilo_Log5(System.Object,System.Boolean)
extern "C"  void PluginAnfeng_ilo_Log5_m1570298862 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::ilo_LogError6(System.Object,System.Boolean)
extern "C"  void PluginAnfeng_ilo_LogError6_m2474318729 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginAnfeng::ilo_GetProductID7(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginAnfeng_ilo_GetProductID7_m93967730 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginAnfeng::ilo_get_ProductsCfgMgr8()
extern "C"  ProductsCfgMgr_t2493714872 * PluginAnfeng_ilo_get_ProductsCfgMgr8_m1204641760 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.EnterGameInfo PluginAnfeng::ilo_Parse9(System.Object[])
extern "C"  EnterGameInfo_t1897532954 * PluginAnfeng_ilo_Parse9_m61504467 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::ilo_userInfo10(PluginAnfeng,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginAnfeng_ilo_userInfo10_m3280708736 (Il2CppObject * __this /* static, unused */, PluginAnfeng_t1351242136 * ____this0, String_t* ___serverid1, String_t* ___servername2, String_t* ___roleid3, String_t* ___rolelv4, String_t* ___rolename5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::ilo_logout11(PluginAnfeng)
extern "C"  void PluginAnfeng_ilo_logout11_m884784070 (Il2CppObject * __this /* static, unused */, PluginAnfeng_t1351242136 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::ilo_Logout12(System.Action)
extern "C"  void PluginAnfeng_ilo_Logout12_m3570250270 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginAnfeng::ilo_get_PluginsSdkMgr13()
extern "C"  PluginsSdkMgr_t3884624670 * PluginAnfeng_ilo_get_PluginsSdkMgr13_m2732566256 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnfeng::ilo_ReqSDKHttpLogin14(PluginsSdkMgr)
extern "C"  void PluginAnfeng_ilo_ReqSDKHttpLogin14_m2614296254 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
