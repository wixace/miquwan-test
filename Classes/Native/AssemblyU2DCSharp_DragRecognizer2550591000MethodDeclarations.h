﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DragRecognizer
struct DragRecognizer_t2550591000;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// DragGesture
struct DragGesture_t2914643285;
// FingerGestures/IFingerList
struct IFingerList_t1026994100;
// Gesture
struct Gesture_t1589572905;
// GestureRecognizer
struct GestureRecognizer_t3512875949;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DragGesture2914643285.h"
#include "AssemblyU2DCSharp_GestureRecognitionState3604239971.h"
#include "AssemblyU2DCSharp_Gesture1589572905.h"
#include "AssemblyU2DCSharp_GestureRecognizer3512875949.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void DragRecognizer::.ctor()
extern "C"  void DragRecognizer__ctor_m2732407171 (DragRecognizer_t2550591000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DragRecognizer::GetDefaultEventMessageName()
extern "C"  String_t* DragRecognizer_GetDefaultEventMessageName_m3146602663 (DragRecognizer_t2550591000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject DragRecognizer::GetDefaultSelectionForSendMessage(DragGesture)
extern "C"  GameObject_t3674682005 * DragRecognizer_GetDefaultSelectionForSendMessage_m873514074 (DragRecognizer_t2550591000 * __this, DragGesture_t2914643285 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DragRecognizer::CanBegin(DragGesture,FingerGestures/IFingerList)
extern "C"  bool DragRecognizer_CanBegin_m2023555267 (DragRecognizer_t2550591000 * __this, DragGesture_t2914643285 * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragRecognizer::OnBegin(DragGesture,FingerGestures/IFingerList)
extern "C"  void DragRecognizer_OnBegin_m37351328 (DragRecognizer_t2550591000 * __this, DragGesture_t2914643285 * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GestureRecognitionState DragRecognizer::OnRecognize(DragGesture,FingerGestures/IFingerList)
extern "C"  int32_t DragRecognizer_OnRecognize_m2646045429 (DragRecognizer_t2550591000 * __this, DragGesture_t2914643285 * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject DragRecognizer::ilo_get_StartSelection1(Gesture)
extern "C"  GameObject_t3674682005 * DragRecognizer_ilo_get_StartSelection1_m3331391890 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DragRecognizer::ilo_GetAverageDistanceFromStart2(FingerGestures/IFingerList)
extern "C"  float DragRecognizer_ilo_GetAverageDistanceFromStart2_m3951261602 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DragRecognizer::ilo_get_RequiredFingerCount3(GestureRecognizer)
extern "C"  int32_t DragRecognizer_ilo_get_RequiredFingerCount3_m2175551374 (Il2CppObject * __this /* static, unused */, GestureRecognizer_t3512875949 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 DragRecognizer::ilo_GetAveragePosition4(FingerGestures/IFingerList)
extern "C"  Vector2_t4282066565  DragRecognizer_ilo_GetAveragePosition4_m106701219 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 DragRecognizer::ilo_GetAverageStartPosition5(FingerGestures/IFingerList)
extern "C"  Vector2_t4282066565  DragRecognizer_ilo_GetAverageStartPosition5_m3516521836 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 DragRecognizer::ilo_get_Position6(Gesture)
extern "C"  Vector2_t4282066565  DragRecognizer_ilo_get_Position6_m3157169714 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 DragRecognizer::ilo_get_StartPosition7(Gesture)
extern "C"  Vector2_t4282066565  DragRecognizer_ilo_get_StartPosition7_m1039308485 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragRecognizer::ilo_set_DeltaMove8(DragGesture,UnityEngine.Vector2)
extern "C"  void DragRecognizer_ilo_set_DeltaMove8_m3603219441 (Il2CppObject * __this /* static, unused */, DragGesture_t2914643285 * ____this0, Vector2_t4282066565  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DragRecognizer::ilo_MovingInSameDirection9(FingerGestures/IFingerList,System.Single)
extern "C"  bool DragRecognizer_ilo_MovingInSameDirection9_m4068816396 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, float ___tolerance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
