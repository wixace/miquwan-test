﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIStretchGenerated
struct UIStretchGenerated_t2757725886;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UIStretchGenerated::.ctor()
extern "C"  void UIStretchGenerated__ctor_m3118999453 (UIStretchGenerated_t2757725886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIStretchGenerated::UIStretch_UIStretch1(JSVCall,System.Int32)
extern "C"  bool UIStretchGenerated_UIStretch_UIStretch1_m887834585 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIStretchGenerated::UIStretch_uiCamera(JSVCall)
extern "C"  void UIStretchGenerated_UIStretch_uiCamera_m3679743673 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIStretchGenerated::UIStretch_container(JSVCall)
extern "C"  void UIStretchGenerated_UIStretch_container_m1299563449 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIStretchGenerated::UIStretch_style(JSVCall)
extern "C"  void UIStretchGenerated_UIStretch_style_m4028904073 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIStretchGenerated::UIStretch_runOnlyOnce(JSVCall)
extern "C"  void UIStretchGenerated_UIStretch_runOnlyOnce_m2738458914 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIStretchGenerated::UIStretch_relativeSize(JSVCall)
extern "C"  void UIStretchGenerated_UIStretch_relativeSize_m3056739621 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIStretchGenerated::UIStretch_initialSize(JSVCall)
extern "C"  void UIStretchGenerated_UIStretch_initialSize_m3895054933 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIStretchGenerated::UIStretch_borderPadding(JSVCall)
extern "C"  void UIStretchGenerated_UIStretch_borderPadding_m3243986037 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIStretchGenerated::__Register()
extern "C"  void UIStretchGenerated___Register_m899845962 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIStretchGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UIStretchGenerated_ilo_getObject1_m1131716629 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIStretchGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UIStretchGenerated_ilo_attachFinalizerObject2_m3939173667 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIStretchGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIStretchGenerated_ilo_setObject3_m2980639759 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIStretchGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIStretchGenerated_ilo_getObject4_m2712384117 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIStretchGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void UIStretchGenerated_ilo_setBooleanS5_m3391920842 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UIStretchGenerated::ilo_getVector2S6(System.Int32)
extern "C"  Vector2_t4282066565  UIStretchGenerated_ilo_getVector2S6_m977871586 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIStretchGenerated::ilo_setVector2S7(System.Int32,UnityEngine.Vector2)
extern "C"  void UIStretchGenerated_ilo_setVector2S7_m3012737392 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
