﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIDragObject
struct UIDragObject_t512213831;
// UnityEngine.Transform
struct Transform_t1659122786;
// UIRect
struct UIRect_t2503437976;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UIPanel
struct UIPanel_t295209936;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_UIRect2503437976.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_UIDragObject512213831.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"

// System.Void UIDragObject::.ctor()
extern "C"  void UIDragObject__ctor_m2192174900 (UIDragObject_t512213831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIDragObject::get_dragMovement()
extern "C"  Vector3_t4282066566  UIDragObject_get_dragMovement_m4140805046 (UIDragObject_t512213831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObject::set_dragMovement(UnityEngine.Vector3)
extern "C"  void UIDragObject_set_dragMovement_m3916724777 (UIDragObject_t512213831 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObject::OnEnable()
extern "C"  void UIDragObject_OnEnable_m4176288722 (UIDragObject_t512213831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObject::OnDisable()
extern "C"  void UIDragObject_OnDisable_m1056868763 (UIDragObject_t512213831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObject::FindPanel()
extern "C"  void UIDragObject_FindPanel_m1317358813 (UIDragObject_t512213831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObject::UpdateBounds()
extern "C"  void UIDragObject_UpdateBounds_m893643246 (UIDragObject_t512213831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObject::OnPress(System.Boolean)
extern "C"  void UIDragObject_OnPress_m1853465517 (UIDragObject_t512213831 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObject::OnDrag(UnityEngine.Vector2)
extern "C"  void UIDragObject_OnDrag_m2658505751 (UIDragObject_t512213831 * __this, Vector2_t4282066565  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObject::Move(UnityEngine.Vector3)
extern "C"  void UIDragObject_Move_m2882952984 (UIDragObject_t512213831 * __this, Vector3_t4282066566  ___worldDelta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObject::LateUpdate()
extern "C"  void UIDragObject_LateUpdate_m1724169119 (UIDragObject_t512213831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObject::CancelMovement()
extern "C"  void UIDragObject_CancelMovement_m1006939097 (UIDragObject_t512213831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObject::CancelSpring()
extern "C"  void UIDragObject_CancelSpring_m2747479031 (UIDragObject_t512213831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObject::OnScroll(System.Single)
extern "C"  void UIDragObject_OnScroll_m2867358863 (UIDragObject_t512213831 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UIDragObject::ilo_get_cachedTransform1(UIRect)
extern "C"  Transform_t1659122786 * UIDragObject_ilo_get_cachedTransform1_m912121721 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDragObject::ilo_GetActive2(UnityEngine.GameObject)
extern "C"  bool UIDragObject_ilo_GetActive2_m3138358821 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObject::ilo_CancelMovement3(UIDragObject)
extern "C"  void UIDragObject_ilo_CancelMovement3_m508779490 (Il2CppObject * __this /* static, unused */, UIDragObject_t512213831 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObject::ilo_FindPanel4(UIDragObject)
extern "C"  void UIDragObject_ilo_FindPanel4_m172351269 (Il2CppObject * __this /* static, unused */, UIDragObject_t512213831 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObject::ilo_UpdateBounds5(UIDragObject)
extern "C"  void UIDragObject_ilo_UpdateBounds5_m2213143471 (Il2CppObject * __this /* static, unused */, UIDragObject_t512213831 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObject::ilo_CancelSpring6(UIDragObject)
extern "C"  void UIDragObject_ilo_CancelSpring6_m1119509127 (Il2CppObject * __this /* static, unused */, UIDragObject_t512213831 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragObject::ilo_Move7(UIDragObject,UnityEngine.Vector3)
extern "C"  void UIDragObject_ilo_Move7_m1605755249 (Il2CppObject * __this /* static, unused */, UIDragObject_t512213831 * ____this0, Vector3_t4282066566  ___worldDelta1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDragObject::ilo_ConstrainTargetToBounds8(UIPanel,UnityEngine.Transform,UnityEngine.Bounds&,System.Boolean)
extern "C"  bool UIDragObject_ilo_ConstrainTargetToBounds8_m1712964073 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, Transform_t1659122786 * ___target1, Bounds_t2711641849 * ___targetBounds2, bool ___immediate3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIDragObject::ilo_SpringDampen9(UnityEngine.Vector3&,System.Single,System.Single)
extern "C"  Vector3_t4282066566  UIDragObject_ilo_SpringDampen9_m3944993253 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___velocity0, float ___strength1, float ___deltaTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
