﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.AIMoveBvr
struct  AIMoveBvr_t2054365827  : public IBehavior_t770859129
{
public:
	// System.Single Entity.Behavior.AIMoveBvr::curTime
	float ___curTime_2;
	// System.Boolean Entity.Behavior.AIMoveBvr::isFollowCap
	bool ___isFollowCap_3;
	// UnityEngine.Vector3 Entity.Behavior.AIMoveBvr::lastForward
	Vector3_t4282066566  ___lastForward_4;
	// UnityEngine.Vector3 Entity.Behavior.AIMoveBvr::lastpos
	Vector3_t4282066566  ___lastpos_5;
	// UnityEngine.Vector3 Entity.Behavior.AIMoveBvr::marshalPos
	Vector3_t4282066566  ___marshalPos_6;

public:
	inline static int32_t get_offset_of_curTime_2() { return static_cast<int32_t>(offsetof(AIMoveBvr_t2054365827, ___curTime_2)); }
	inline float get_curTime_2() const { return ___curTime_2; }
	inline float* get_address_of_curTime_2() { return &___curTime_2; }
	inline void set_curTime_2(float value)
	{
		___curTime_2 = value;
	}

	inline static int32_t get_offset_of_isFollowCap_3() { return static_cast<int32_t>(offsetof(AIMoveBvr_t2054365827, ___isFollowCap_3)); }
	inline bool get_isFollowCap_3() const { return ___isFollowCap_3; }
	inline bool* get_address_of_isFollowCap_3() { return &___isFollowCap_3; }
	inline void set_isFollowCap_3(bool value)
	{
		___isFollowCap_3 = value;
	}

	inline static int32_t get_offset_of_lastForward_4() { return static_cast<int32_t>(offsetof(AIMoveBvr_t2054365827, ___lastForward_4)); }
	inline Vector3_t4282066566  get_lastForward_4() const { return ___lastForward_4; }
	inline Vector3_t4282066566 * get_address_of_lastForward_4() { return &___lastForward_4; }
	inline void set_lastForward_4(Vector3_t4282066566  value)
	{
		___lastForward_4 = value;
	}

	inline static int32_t get_offset_of_lastpos_5() { return static_cast<int32_t>(offsetof(AIMoveBvr_t2054365827, ___lastpos_5)); }
	inline Vector3_t4282066566  get_lastpos_5() const { return ___lastpos_5; }
	inline Vector3_t4282066566 * get_address_of_lastpos_5() { return &___lastpos_5; }
	inline void set_lastpos_5(Vector3_t4282066566  value)
	{
		___lastpos_5 = value;
	}

	inline static int32_t get_offset_of_marshalPos_6() { return static_cast<int32_t>(offsetof(AIMoveBvr_t2054365827, ___marshalPos_6)); }
	inline Vector3_t4282066566  get_marshalPos_6() const { return ___marshalPos_6; }
	inline Vector3_t4282066566 * get_address_of_marshalPos_6() { return &___marshalPos_6; }
	inline void set_marshalPos_6(Vector3_t4282066566  value)
	{
		___marshalPos_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
