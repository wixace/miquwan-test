﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_OcclusionPortalGenerated
struct UnityEngine_OcclusionPortalGenerated_t1197351582;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_OcclusionPortalGenerated::.ctor()
extern "C"  void UnityEngine_OcclusionPortalGenerated__ctor_m2381126077 (UnityEngine_OcclusionPortalGenerated_t1197351582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_OcclusionPortalGenerated::OcclusionPortal_OcclusionPortal1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_OcclusionPortalGenerated_OcclusionPortal_OcclusionPortal1_m589907049 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OcclusionPortalGenerated::OcclusionPortal_open(JSVCall)
extern "C"  void UnityEngine_OcclusionPortalGenerated_OcclusionPortal_open_m3156620144 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OcclusionPortalGenerated::__Register()
extern "C"  void UnityEngine_OcclusionPortalGenerated___Register_m1870655786 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_OcclusionPortalGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_OcclusionPortalGenerated_ilo_getObject1_m828119669 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_OcclusionPortalGenerated::ilo_getBooleanS2(System.Int32)
extern "C"  bool UnityEngine_OcclusionPortalGenerated_ilo_getBooleanS2_m3502768816 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
