﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>
struct Dictionary_2_t2245318082;
// System.Collections.Generic.IEqualityComparer`1<Pathfinding.Int2>
struct IEqualityComparer_1_t2765079997;
// System.Collections.Generic.IDictionary`2<Pathfinding.Int2,System.Int32>
struct IDictionary_2_t1823191427;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<Pathfinding.Int2>
struct ICollection_1_t2868635580;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t2048428487;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1566256653;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>>
struct IEnumerator_1_t4055963837;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Int32>
struct KeyCollection_t3872077533;
// System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,System.Int32>
struct ValueCollection_t945923795;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22144098788.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3562641474.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::.ctor()
extern "C"  void Dictionary_2__ctor_m3996279142_gshared (Dictionary_2_t2245318082 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3996279142(__this, method) ((  void (*) (Dictionary_2_t2245318082 *, const MethodInfo*))Dictionary_2__ctor_m3996279142_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m4168789172_gshared (Dictionary_2_t2245318082 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m4168789172(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t2245318082 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m4168789172_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m399171323_gshared (Dictionary_2_t2245318082 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m399171323(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t2245318082 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m399171323_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3373141134_gshared (Dictionary_2_t2245318082 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m3373141134(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t2245318082 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3373141134_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1696925666_gshared (Dictionary_2_t2245318082 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1696925666(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t2245318082 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1696925666_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m1370199678_gshared (Dictionary_2_t2245318082 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m1370199678(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2245318082 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m1370199678_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3585543473_gshared (Dictionary_2_t2245318082 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3585543473(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2245318082 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3585543473_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1552006065_gshared (Dictionary_2_t2245318082 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1552006065(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2245318082 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1552006065_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m484502083_gshared (Dictionary_2_t2245318082 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m484502083(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2245318082 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m484502083_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m4117588593_gshared (Dictionary_2_t2245318082 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m4117588593(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2245318082 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m4117588593_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2765928900_gshared (Dictionary_2_t2245318082 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2765928900(__this, method) ((  bool (*) (Dictionary_2_t2245318082 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2765928900_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3591273653_gshared (Dictionary_2_t2245318082 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3591273653(__this, method) ((  bool (*) (Dictionary_2_t2245318082 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3591273653_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3023372523_gshared (Dictionary_2_t2245318082 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3023372523(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2245318082 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m3023372523_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m187758480_gshared (Dictionary_2_t2245318082 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m187758480(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2245318082 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m187758480_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m664587457_gshared (Dictionary_2_t2245318082 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m664587457(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2245318082 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m664587457_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m2178365077_gshared (Dictionary_2_t2245318082 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m2178365077(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2245318082 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2178365077_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2305069518_gshared (Dictionary_2_t2245318082 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m2305069518(__this, ___key0, method) ((  void (*) (Dictionary_2_t2245318082 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2305069518_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m352258271_gshared (Dictionary_2_t2245318082 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m352258271(__this, method) ((  bool (*) (Dictionary_2_t2245318082 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m352258271_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1163248203_gshared (Dictionary_2_t2245318082 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1163248203(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2245318082 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1163248203_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m199646307_gshared (Dictionary_2_t2245318082 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m199646307(__this, method) ((  bool (*) (Dictionary_2_t2245318082 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m199646307_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3360325476_gshared (Dictionary_2_t2245318082 * __this, KeyValuePair_2_t2144098788  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3360325476(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t2245318082 *, KeyValuePair_2_t2144098788 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3360325476_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1896868574_gshared (Dictionary_2_t2245318082 * __this, KeyValuePair_2_t2144098788  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1896868574(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2245318082 *, KeyValuePair_2_t2144098788 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1896868574_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3019103816_gshared (Dictionary_2_t2245318082 * __this, KeyValuePair_2U5BU5D_t1566256653* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3019103816(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2245318082 *, KeyValuePair_2U5BU5D_t1566256653*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3019103816_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2327261571_gshared (Dictionary_2_t2245318082 * __this, KeyValuePair_2_t2144098788  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2327261571(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2245318082 *, KeyValuePair_2_t2144098788 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2327261571_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m4218086695_gshared (Dictionary_2_t2245318082 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m4218086695(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2245318082 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m4218086695_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3297169762_gshared (Dictionary_2_t2245318082 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3297169762(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2245318082 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3297169762_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1435663903_gshared (Dictionary_2_t2245318082 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1435663903(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2245318082 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1435663903_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m4051282554_gshared (Dictionary_2_t2245318082 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m4051282554(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2245318082 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m4051282554_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m4114342565_gshared (Dictionary_2_t2245318082 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m4114342565(__this, method) ((  int32_t (*) (Dictionary_2_t2245318082 *, const MethodInfo*))Dictionary_2_get_Count_m4114342565_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::get_Item(TKey)
extern "C"  int32_t Dictionary_2_get_Item_m3608872742_gshared (Dictionary_2_t2245318082 * __this, Int2_t1974045593  ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m3608872742(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t2245318082 *, Int2_t1974045593 , const MethodInfo*))Dictionary_2_get_Item_m3608872742_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1164159191_gshared (Dictionary_2_t2245318082 * __this, Int2_t1974045593  ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1164159191(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2245318082 *, Int2_t1974045593 , int32_t, const MethodInfo*))Dictionary_2_set_Item_m1164159191_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m1879308853_gshared (Dictionary_2_t2245318082 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m1879308853(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t2245318082 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1879308853_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m1102037538_gshared (Dictionary_2_t2245318082 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1102037538(__this, ___size0, method) ((  void (*) (Dictionary_2_t2245318082 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1102037538_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m614152862_gshared (Dictionary_2_t2245318082 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m614152862(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2245318082 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m614152862_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t2144098788  Dictionary_2_make_pair_m3478916842_gshared (Il2CppObject * __this /* static, unused */, Int2_t1974045593  ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m3478916842(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2144098788  (*) (Il2CppObject * /* static, unused */, Int2_t1974045593 , int32_t, const MethodInfo*))Dictionary_2_make_pair_m3478916842_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::pick_key(TKey,TValue)
extern "C"  Int2_t1974045593  Dictionary_2_pick_key_m2783336652_gshared (Il2CppObject * __this /* static, unused */, Int2_t1974045593  ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m2783336652(__this /* static, unused */, ___key0, ___value1, method) ((  Int2_t1974045593  (*) (Il2CppObject * /* static, unused */, Int2_t1974045593 , int32_t, const MethodInfo*))Dictionary_2_pick_key_m2783336652_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::pick_value(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_value_m1410960396_gshared (Il2CppObject * __this /* static, unused */, Int2_t1974045593  ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m1410960396(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Int2_t1974045593 , int32_t, const MethodInfo*))Dictionary_2_pick_value_m1410960396_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3119252337_gshared (Dictionary_2_t2245318082 * __this, KeyValuePair_2U5BU5D_t1566256653* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3119252337(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2245318082 *, KeyValuePair_2U5BU5D_t1566256653*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3119252337_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::Resize()
extern "C"  void Dictionary_2_Resize_m3575624987_gshared (Dictionary_2_t2245318082 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m3575624987(__this, method) ((  void (*) (Dictionary_2_t2245318082 *, const MethodInfo*))Dictionary_2_Resize_m3575624987_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m987301120_gshared (Dictionary_2_t2245318082 * __this, Int2_t1974045593  ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m987301120(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2245318082 *, Int2_t1974045593 , int32_t, const MethodInfo*))Dictionary_2_Add_m987301120_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::Clear()
extern "C"  void Dictionary_2_Clear_m1402412433_gshared (Dictionary_2_t2245318082 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1402412433(__this, method) ((  void (*) (Dictionary_2_t2245318082 *, const MethodInfo*))Dictionary_2_Clear_m1402412433_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m3580284353_gshared (Dictionary_2_t2245318082 * __this, Int2_t1974045593  ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m3580284353(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2245318082 *, Int2_t1974045593 , const MethodInfo*))Dictionary_2_ContainsKey_m3580284353_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m3216293710_gshared (Dictionary_2_t2245318082 * __this, int32_t ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m3216293710(__this, ___value0, method) ((  bool (*) (Dictionary_2_t2245318082 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m3216293710_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m161841115_gshared (Dictionary_2_t2245318082 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m161841115(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2245318082 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m161841115_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m719451497_gshared (Dictionary_2_t2245318082 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m719451497(__this, ___sender0, method) ((  void (*) (Dictionary_2_t2245318082 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m719451497_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m1429749218_gshared (Dictionary_2_t2245318082 * __this, Int2_t1974045593  ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m1429749218(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2245318082 *, Int2_t1974045593 , const MethodInfo*))Dictionary_2_Remove_m1429749218_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m3802742157_gshared (Dictionary_2_t2245318082 * __this, Int2_t1974045593  ___key0, int32_t* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m3802742157(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t2245318082 *, Int2_t1974045593 , int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m3802742157_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::get_Keys()
extern "C"  KeyCollection_t3872077533 * Dictionary_2_get_Keys_m2546576440_gshared (Dictionary_2_t2245318082 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m2546576440(__this, method) ((  KeyCollection_t3872077533 * (*) (Dictionary_2_t2245318082 *, const MethodInfo*))Dictionary_2_get_Keys_m2546576440_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::get_Values()
extern "C"  ValueCollection_t945923795 * Dictionary_2_get_Values_m3492096568_gshared (Dictionary_2_t2245318082 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3492096568(__this, method) ((  ValueCollection_t945923795 * (*) (Dictionary_2_t2245318082 *, const MethodInfo*))Dictionary_2_get_Values_m3492096568_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::ToTKey(System.Object)
extern "C"  Int2_t1974045593  Dictionary_2_ToTKey_m2233195559_gshared (Dictionary_2_t2245318082 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m2233195559(__this, ___key0, method) ((  Int2_t1974045593  (*) (Dictionary_2_t2245318082 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m2233195559_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::ToTValue(System.Object)
extern "C"  int32_t Dictionary_2_ToTValue_m4018241831_gshared (Dictionary_2_t2245318082 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m4018241831(__this, ___value0, method) ((  int32_t (*) (Dictionary_2_t2245318082 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m4018241831_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m435772357_gshared (Dictionary_2_t2245318082 * __this, KeyValuePair_2_t2144098788  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m435772357(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t2245318082 *, KeyValuePair_2_t2144098788 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m435772357_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t3562641474  Dictionary_2_GetEnumerator_m4209491586_gshared (Dictionary_2_t2245318082 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m4209491586(__this, method) ((  Enumerator_t3562641474  (*) (Dictionary_2_t2245318082 *, const MethodInfo*))Dictionary_2_GetEnumerator_m4209491586_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m321773905_gshared (Il2CppObject * __this /* static, unused */, Int2_t1974045593  ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m321773905(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, Int2_t1974045593 , int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m321773905_gshared)(__this /* static, unused */, ___key0, ___value1, method)
