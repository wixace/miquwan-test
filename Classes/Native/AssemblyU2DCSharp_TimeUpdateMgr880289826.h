﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<TimeUpdateVo>
struct List_1_t3197697599;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeUpdateMgr
struct  TimeUpdateMgr_t880289826  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<TimeUpdateVo> TimeUpdateMgr::timeVoList
	List_1_t3197697599 * ___timeVoList_0;

public:
	inline static int32_t get_offset_of_timeVoList_0() { return static_cast<int32_t>(offsetof(TimeUpdateMgr_t880289826, ___timeVoList_0)); }
	inline List_1_t3197697599 * get_timeVoList_0() const { return ___timeVoList_0; }
	inline List_1_t3197697599 ** get_address_of_timeVoList_0() { return &___timeVoList_0; }
	inline void set_timeVoList_0(List_1_t3197697599 * value)
	{
		___timeVoList_0 = value;
		Il2CppCodeGenWriteBarrier(&___timeVoList_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
