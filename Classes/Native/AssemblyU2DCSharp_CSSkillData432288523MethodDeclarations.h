﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSSkillData
struct CSSkillData_t432288523;
// skill_upgradeCfg
struct skill_upgradeCfg_t790726486;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"

// System.Void CSSkillData::.ctor(System.UInt32)
extern "C"  void CSSkillData__ctor_m424584250 (CSSkillData_t432288523 * __this, uint32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSSkillData::.ctor(System.UInt32,System.UInt32,System.UInt32,System.Int32)
extern "C"  void CSSkillData__ctor_m2059764085 (CSSkillData_t432288523 * __this, uint32_t ___id0, uint32_t ___level1, uint32_t ___state2, int32_t ___upLvID3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// skill_upgradeCfg CSSkillData::GetUpgradeCfg()
extern "C"  skill_upgradeCfg_t790726486 * CSSkillData_GetUpgradeCfg_m3495803043 (CSSkillData_t432288523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// skill_upgradeCfg CSSkillData::GetAwakenGradeCfg()
extern "C"  skill_upgradeCfg_t790726486 * CSSkillData_GetAwakenGradeCfg_m3553976113 (CSSkillData_t432288523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSSkillData::get_IsUpgrade()
extern "C"  bool CSSkillData_get_IsUpgrade_m3628836667 (CSSkillData_t432288523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSDatacfgManager CSSkillData::ilo_get_CfgDataMgr1()
extern "C"  CSDatacfgManager_t1565254243 * CSSkillData_ilo_get_CfgDataMgr1_m1941108227 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// skill_upgradeCfg CSSkillData::ilo_Getskill_upgradeCfg2(CSDatacfgManager,System.Int32)
extern "C"  skill_upgradeCfg_t790726486 * CSSkillData_ilo_Getskill_upgradeCfg2_m1026170948 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
