﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.GraphNode[]
struct GraphNodeU5BU5D_t927449255;
// System.UInt32[]
struct UInt32U5BU5D_t3230734560;

#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.QuadtreeNode
struct  QuadtreeNode_t1423301757  : public GraphNode_t23612370
{
public:
	// Pathfinding.GraphNode[] Pathfinding.QuadtreeNode::connections
	GraphNodeU5BU5D_t927449255* ___connections_15;
	// System.UInt32[] Pathfinding.QuadtreeNode::connectionCosts
	UInt32U5BU5D_t3230734560* ___connectionCosts_16;

public:
	inline static int32_t get_offset_of_connections_15() { return static_cast<int32_t>(offsetof(QuadtreeNode_t1423301757, ___connections_15)); }
	inline GraphNodeU5BU5D_t927449255* get_connections_15() const { return ___connections_15; }
	inline GraphNodeU5BU5D_t927449255** get_address_of_connections_15() { return &___connections_15; }
	inline void set_connections_15(GraphNodeU5BU5D_t927449255* value)
	{
		___connections_15 = value;
		Il2CppCodeGenWriteBarrier(&___connections_15, value);
	}

	inline static int32_t get_offset_of_connectionCosts_16() { return static_cast<int32_t>(offsetof(QuadtreeNode_t1423301757, ___connectionCosts_16)); }
	inline UInt32U5BU5D_t3230734560* get_connectionCosts_16() const { return ___connectionCosts_16; }
	inline UInt32U5BU5D_t3230734560** get_address_of_connectionCosts_16() { return &___connectionCosts_16; }
	inline void set_connectionCosts_16(UInt32U5BU5D_t3230734560* value)
	{
		___connectionCosts_16 = value;
		Il2CppCodeGenWriteBarrier(&___connectionCosts_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
