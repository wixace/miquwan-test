﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// skillCfgGenerated
struct skillCfgGenerated_t1892914812;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// skillCfg
struct skillCfg_t2142425171;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_skillCfg2142425171.h"

// System.Void skillCfgGenerated::.ctor()
extern "C"  void skillCfgGenerated__ctor_m3225133679 (skillCfgGenerated_t1892914812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean skillCfgGenerated::skillCfg_skillCfg1(JSVCall,System.Int32)
extern "C"  bool skillCfgGenerated_skillCfg_skillCfg1_m1693570179 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_id(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_id_m3117761971 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_show_super_name(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_show_super_name_m3687733101 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_name(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_name_m2210651203 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_type(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_type_m2452331636 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_fun_type(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_fun_type_m1388395956 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_add_anger(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_add_anger_m2156110037 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_last_type(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_last_type_m4081293755 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_lasttime(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_lasttime_m2785626987 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_damage_time(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_damage_time_m1700630177 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_hitrate(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_hitrate_m3967150603 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_Hurt_times(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_Hurt_times_m1312869880 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_effectCount(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_effectCount_m3594927968 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_coefficient(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_coefficient_m3219963401 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_hatryratio(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_hatryratio_m655682405 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_move_coefficient(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_move_coefficient_m3905326215 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_trigger(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_trigger_m2421702886 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_triggertype(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_triggertype_m1455260396 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_probability(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_probability_m4070813449 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_preskill(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_preskill_m3456438432 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_followskill(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_followskill_m3766455902 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_if_target(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_if_target_m3760966667 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_distance(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_distance_m3474525721 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_isshoot(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_isshoot_m4046131721 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_shootdis(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_shootdis_m383909695 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_shootspeed(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_shootspeed_m4185660902 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_target(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_target_m1999360509 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_target_num(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_target_num_m1040585622 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_region_type(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_region_type_m2681375641 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_region_radius(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_region_radius_m1633116673 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_region_angle(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_region_angle_m2180384678 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_region_length(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_region_length_m4216877613 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_region_width(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_region_width_m2942514195 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_action(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_action_m2470326008 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_endaction(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_endaction_m3609126381 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_actiontime(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_actiontime_m1076996555 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_cdtime(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_cdtime_m1752460672 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_cdstarttime(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_cdstarttime_m1286365296 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_is_rand_atk(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_is_rand_atk_m1125928459 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_relate_cd_skillID(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_relate_cd_skillID_m1340764714 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_isProgressBar(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_isProgressBar_m3565969922 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_progressTime(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_progressTime_m1706943764 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_breakskill(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_breakskill_m2219756540 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_icon(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_icon_m2868274709 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_effect_sing(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_effect_sing_m2156259265 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_sing_showtime(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_sing_showtime_m1295041508 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_sing_playspeed(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_sing_playspeed_m2063814635 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_sound(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_sound_m1276200303 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_soundTime(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_soundTime_m3283475618 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_effect_skill(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_effect_skill_m986366891 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_skill_showtime(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_skill_showtime_m200996342 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_skill_playspeed(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_skill_playspeed_m2508152857 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_effect_skill_event(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_effect_skill_event_m2587696304 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_damage_separation(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_damage_separation_m1540350024 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_effect_kick(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_effect_kick_m2009057770 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_kick_showtime(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_kick_showtime_m1538801627 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_activetime(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_activetime_m3294984955 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_activecount(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_activecount_m2266171381 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_active_spacing(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_active_spacing_m329248676 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_black_effect(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_black_effect_m997704637 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_zoomintime(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_zoomintime_m1685943049 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_zoominoffset(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_zoominoffset_m3154334723 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_waittime(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_waittime_m2596515564 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_zoomouttime(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_zoomouttime_m254319798 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_shock_event(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_shock_event_m465619329 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_shock_time(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_shock_time_m215806308 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_shock_type(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_shock_type_m1185243031 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_shock_fps(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_shock_fps_m3649513810 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_shock_lastingtime(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_shock_lastingtime_m4019834082 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_shock_strength(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_shock_strength_m3642546416 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_falseSkill(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_falseSkill_m1733380416 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_skillIdArr(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_skillIdArr_m2311341209 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_buffid(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_buffid_m2755125120 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_buffprob(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_buffprob_m212226374 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_buffevent(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_buffevent_m3913949015 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_bufftarget(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_bufftarget_m3621961034 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_buffbreak(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_buffbreak_m1668455058 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_move_event(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_move_event_m1258302786 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_move_start_time(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_move_start_time_m1145015526 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_movetype(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_movetype_m1476995139 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_movetime(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_movetime_m507558416 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_movespeed(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_movespeed_m2034473800 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_movepara(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_movepara_m1039740509 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_moveaction(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_moveaction_m1474822919 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_movehight(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_movehight_m1372535005 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_radiusrage(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_radiusrage_m1095403631 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_sputter_id(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_sputter_id_m304982361 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_sputter(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_sputter_m304200793 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_sputter_radius(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_sputter_radius_m172083394 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_sputter_num(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_sputter_num_m1023897586 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_sputter_hurt(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_sputter_hurt_m1663422821 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_leader_skill_att(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_leader_skill_att_m4147220241 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_leader_skill_per(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_leader_skill_per_m3365987349 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_leader_skill_value(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_leader_skill_value_m2261356353 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_summonid(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_summonid_m1743491122 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_summon_time(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_summon_time_m107271315 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_summon_attackper(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_summon_attackper_m1349189719 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_summonid_target(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_summonid_target_m1269811370 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_SkillDes(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_SkillDes_m666784621 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_skillbrief(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_skillbrief_m3561670085 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_openTermDes(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_openTermDes_m1922939074 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_evolveLevel(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_evolveLevel_m297027447 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_break_or_not(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_break_or_not_m706882199 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_upgrade_begin(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_upgrade_begin_m1926480792 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::skillCfg_upgrade_end(JSVCall)
extern "C"  void skillCfgGenerated_skillCfg_upgrade_end_m754333286 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean skillCfgGenerated::skillCfg_Init__DictionaryT2_String_String(JSVCall,System.Int32)
extern "C"  bool skillCfgGenerated_skillCfg_Init__DictionaryT2_String_String_m803762561 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::__Register()
extern "C"  void skillCfgGenerated___Register_m3234138040 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean skillCfgGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool skillCfgGenerated_ilo_attachFinalizerObject1_m2714815080 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::ilo_setInt322(System.Int32,System.Int32)
extern "C"  void skillCfgGenerated_ilo_setInt322_m981942950 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::ilo_setStringS3(System.Int32,System.String)
extern "C"  void skillCfgGenerated_ilo_setStringS3_m3908006586 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String skillCfgGenerated::ilo_getStringS4(System.Int32)
extern "C"  String_t* skillCfgGenerated_ilo_getStringS4_m1864430934 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 skillCfgGenerated::ilo_getInt325(System.Int32)
extern "C"  int32_t skillCfgGenerated_ilo_getInt325_m1546411798 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean skillCfgGenerated::ilo_getBooleanS6(System.Int32)
extern "C"  bool skillCfgGenerated_ilo_getBooleanS6_m3948430810 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::ilo_setSingle7(System.Int32,System.Single)
extern "C"  void skillCfgGenerated_ilo_setSingle7_m3749773995 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single skillCfgGenerated::ilo_getSingle8(System.Int32)
extern "C"  float skillCfgGenerated_ilo_getSingle8_m3425174471 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::ilo_setBooleanS9(System.Int32,System.Boolean)
extern "C"  void skillCfgGenerated_ilo_setBooleanS9_m3583121048 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object skillCfgGenerated::ilo_getObject10(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * skillCfgGenerated_ilo_getObject10_m1523753514 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfgGenerated::ilo_Init11(skillCfg,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void skillCfgGenerated_ilo_Init11_m4017792114 (Il2CppObject * __this /* static, unused */, skillCfg_t2142425171 * ____this0, Dictionary_2_t827649927 * ____info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
