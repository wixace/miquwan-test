﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSMgr/JS_CS_Rel
struct JS_CS_Rel_t3554103776;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void JSMgr/JS_CS_Rel::.ctor(System.Int32,System.Object)
extern "C"  void JS_CS_Rel__ctor_m4283385898 (JS_CS_Rel_t3554103776 * __this, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
