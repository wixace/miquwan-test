﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;
// Pathfinding.RichAI[]
struct RichAIU5BU5D_t3615124959;
// AIPath[]
struct AIPathU5BU5D_t616438592;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.TargetMover
struct  TargetMover_t379830674  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.LayerMask Pathfinding.TargetMover::mask
	LayerMask_t3236759763  ___mask_2;
	// UnityEngine.Transform Pathfinding.TargetMover::target
	Transform_t1659122786 * ___target_3;
	// Pathfinding.RichAI[] Pathfinding.TargetMover::ais
	RichAIU5BU5D_t3615124959* ___ais_4;
	// AIPath[] Pathfinding.TargetMover::ais2
	AIPathU5BU5D_t616438592* ___ais2_5;
	// System.Boolean Pathfinding.TargetMover::onlyOnDoubleClick
	bool ___onlyOnDoubleClick_6;
	// UnityEngine.Camera Pathfinding.TargetMover::cam
	Camera_t2727095145 * ___cam_7;

public:
	inline static int32_t get_offset_of_mask_2() { return static_cast<int32_t>(offsetof(TargetMover_t379830674, ___mask_2)); }
	inline LayerMask_t3236759763  get_mask_2() const { return ___mask_2; }
	inline LayerMask_t3236759763 * get_address_of_mask_2() { return &___mask_2; }
	inline void set_mask_2(LayerMask_t3236759763  value)
	{
		___mask_2 = value;
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(TargetMover_t379830674, ___target_3)); }
	inline Transform_t1659122786 * get_target_3() const { return ___target_3; }
	inline Transform_t1659122786 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(Transform_t1659122786 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier(&___target_3, value);
	}

	inline static int32_t get_offset_of_ais_4() { return static_cast<int32_t>(offsetof(TargetMover_t379830674, ___ais_4)); }
	inline RichAIU5BU5D_t3615124959* get_ais_4() const { return ___ais_4; }
	inline RichAIU5BU5D_t3615124959** get_address_of_ais_4() { return &___ais_4; }
	inline void set_ais_4(RichAIU5BU5D_t3615124959* value)
	{
		___ais_4 = value;
		Il2CppCodeGenWriteBarrier(&___ais_4, value);
	}

	inline static int32_t get_offset_of_ais2_5() { return static_cast<int32_t>(offsetof(TargetMover_t379830674, ___ais2_5)); }
	inline AIPathU5BU5D_t616438592* get_ais2_5() const { return ___ais2_5; }
	inline AIPathU5BU5D_t616438592** get_address_of_ais2_5() { return &___ais2_5; }
	inline void set_ais2_5(AIPathU5BU5D_t616438592* value)
	{
		___ais2_5 = value;
		Il2CppCodeGenWriteBarrier(&___ais2_5, value);
	}

	inline static int32_t get_offset_of_onlyOnDoubleClick_6() { return static_cast<int32_t>(offsetof(TargetMover_t379830674, ___onlyOnDoubleClick_6)); }
	inline bool get_onlyOnDoubleClick_6() const { return ___onlyOnDoubleClick_6; }
	inline bool* get_address_of_onlyOnDoubleClick_6() { return &___onlyOnDoubleClick_6; }
	inline void set_onlyOnDoubleClick_6(bool value)
	{
		___onlyOnDoubleClick_6 = value;
	}

	inline static int32_t get_offset_of_cam_7() { return static_cast<int32_t>(offsetof(TargetMover_t379830674, ___cam_7)); }
	inline Camera_t2727095145 * get_cam_7() const { return ___cam_7; }
	inline Camera_t2727095145 ** get_address_of_cam_7() { return &___cam_7; }
	inline void set_cam_7(Camera_t2727095145 * value)
	{
		___cam_7 = value;
		Il2CppCodeGenWriteBarrier(&___cam_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
