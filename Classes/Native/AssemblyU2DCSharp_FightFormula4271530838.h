﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int32,ElementVO>
struct Dictionary_2_t1742714908;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FightFormula
struct  FightFormula_t4271530838  : public Il2CppObject
{
public:

public:
};

struct FightFormula_t4271530838_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,ElementVO> FightFormula::elementDic
	Dictionary_2_t1742714908 * ___elementDic_0;

public:
	inline static int32_t get_offset_of_elementDic_0() { return static_cast<int32_t>(offsetof(FightFormula_t4271530838_StaticFields, ___elementDic_0)); }
	inline Dictionary_2_t1742714908 * get_elementDic_0() const { return ___elementDic_0; }
	inline Dictionary_2_t1742714908 ** get_address_of_elementDic_0() { return &___elementDic_0; }
	inline void set_elementDic_0(Dictionary_2_t1742714908 * value)
	{
		___elementDic_0 = value;
		Il2CppCodeGenWriteBarrier(&___elementDic_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
