﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Interpolate/<NewEase>c__Iterator23
struct U3CNewEaseU3Ec__Iterator23_t3715628756;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Interpolate/<NewEase>c__Iterator23::.ctor()
extern "C"  void U3CNewEaseU3Ec__Iterator23__ctor_m3046785095 (U3CNewEaseU3Ec__Iterator23_t3715628756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Interpolate/<NewEase>c__Iterator23::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CNewEaseU3Ec__Iterator23_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3600633525 (U3CNewEaseU3Ec__Iterator23_t3715628756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Interpolate/<NewEase>c__Iterator23::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CNewEaseU3Ec__Iterator23_System_Collections_IEnumerator_get_Current_m2372230217 (U3CNewEaseU3Ec__Iterator23_t3715628756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Interpolate/<NewEase>c__Iterator23::MoveNext()
extern "C"  bool U3CNewEaseU3Ec__Iterator23_MoveNext_m1035145013 (U3CNewEaseU3Ec__Iterator23_t3715628756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Interpolate/<NewEase>c__Iterator23::Dispose()
extern "C"  void U3CNewEaseU3Ec__Iterator23_Dispose_m2986030788 (U3CNewEaseU3Ec__Iterator23_t3715628756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Interpolate/<NewEase>c__Iterator23::Reset()
extern "C"  void U3CNewEaseU3Ec__Iterator23_Reset_m693218036 (U3CNewEaseU3Ec__Iterator23_t3715628756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
