﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>
struct Dictionary_2_t3630855731;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4245791785.h"
#include "AssemblyU2DCSharp_AIEnum_EAIEventtype3896289311.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AIEnum.EAIEventtype,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2572819713_gshared (Enumerator_t4245791785 * __this, Dictionary_2_t3630855731 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2572819713(__this, ___host0, method) ((  void (*) (Enumerator_t4245791785 *, Dictionary_2_t3630855731 *, const MethodInfo*))Enumerator__ctor_m2572819713_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AIEnum.EAIEventtype,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1870827584_gshared (Enumerator_t4245791785 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1870827584(__this, method) ((  Il2CppObject * (*) (Enumerator_t4245791785 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1870827584_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AIEnum.EAIEventtype,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1671836948_gshared (Enumerator_t4245791785 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1671836948(__this, method) ((  void (*) (Enumerator_t4245791785 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1671836948_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AIEnum.EAIEventtype,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2522420707_gshared (Enumerator_t4245791785 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2522420707(__this, method) ((  void (*) (Enumerator_t4245791785 *, const MethodInfo*))Enumerator_Dispose_m2522420707_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AIEnum.EAIEventtype,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3333084416_gshared (Enumerator_t4245791785 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3333084416(__this, method) ((  bool (*) (Enumerator_t4245791785 *, const MethodInfo*))Enumerator_MoveNext_m3333084416_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<AIEnum.EAIEventtype,System.Object>::get_Current()
extern "C"  uint8_t Enumerator_get_Current_m1641221076_gshared (Enumerator_t4245791785 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1641221076(__this, method) ((  uint8_t (*) (Enumerator_t4245791785 *, const MethodInfo*))Enumerator_get_Current_m1641221076_gshared)(__this, method)
