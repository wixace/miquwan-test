﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,System.Object>
struct ValueCollection_t749037579;
// System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>
struct Dictionary_2_t2048431866;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4275232570.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1989077408_gshared (ValueCollection_t749037579 * __this, Dictionary_2_t2048431866 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1989077408(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t749037579 *, Dictionary_2_t2048431866 *, const MethodInfo*))ValueCollection__ctor_m1989077408_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1041997458_gshared (ValueCollection_t749037579 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1041997458(__this, ___item0, method) ((  void (*) (ValueCollection_t749037579 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1041997458_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m708551771_gshared (ValueCollection_t749037579 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m708551771(__this, method) ((  void (*) (ValueCollection_t749037579 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m708551771_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4273385556_gshared (ValueCollection_t749037579 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4273385556(__this, ___item0, method) ((  bool (*) (ValueCollection_t749037579 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4273385556_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m845663737_gshared (ValueCollection_t749037579 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m845663737(__this, ___item0, method) ((  bool (*) (ValueCollection_t749037579 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m845663737_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2085042217_gshared (ValueCollection_t749037579 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2085042217(__this, method) ((  Il2CppObject* (*) (ValueCollection_t749037579 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2085042217_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2558578591_gshared (ValueCollection_t749037579 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m2558578591(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t749037579 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2558578591_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m998340826_gshared (ValueCollection_t749037579 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m998340826(__this, method) ((  Il2CppObject * (*) (ValueCollection_t749037579 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m998340826_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2548561415_gshared (ValueCollection_t749037579 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2548561415(__this, method) ((  bool (*) (ValueCollection_t749037579 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2548561415_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2925645415_gshared (ValueCollection_t749037579 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2925645415(__this, method) ((  bool (*) (ValueCollection_t749037579 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2925645415_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m45093459_gshared (ValueCollection_t749037579 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m45093459(__this, method) ((  Il2CppObject * (*) (ValueCollection_t749037579 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m45093459_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m1218987495_gshared (ValueCollection_t749037579 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m1218987495(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t749037579 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1218987495_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,System.Object>::GetEnumerator()
extern "C"  Enumerator_t4275232570  ValueCollection_GetEnumerator_m3758762570_gshared (ValueCollection_t749037579 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m3758762570(__this, method) ((  Enumerator_t4275232570  (*) (ValueCollection_t749037579 *, const MethodInfo*))ValueCollection_GetEnumerator_m3758762570_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1914373933_gshared (ValueCollection_t749037579 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1914373933(__this, method) ((  int32_t (*) (ValueCollection_t749037579 *, const MethodInfo*))ValueCollection_get_Count_m1914373933_gshared)(__this, method)
