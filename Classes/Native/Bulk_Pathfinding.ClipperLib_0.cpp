﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Pathfinding.ClipperLib.Clipper
struct Clipper_t636949239;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>
struct List_1_t1767529987;
// Pathfinding.ClipperLib.PolyTree
struct PolyTree_t3336435468;
// Pathfinding.ClipperLib.OutRec
struct OutRec_t3245805284;
// Pathfinding.ClipperLib.OutPt
struct OutPt_t3947633180;
// Pathfinding.ClipperLib.TEdge
struct TEdge_t3950806139;
// Pathfinding.ClipperLib.IntersectNode
struct IntersectNode_t4106323947;
// System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>
struct List_1_t399344435;
// Pathfinding.ClipperLib.Join
struct Join_t3970117804;
// Pathfinding.ClipperLib.ClipperBase
struct ClipperBase_t512275816;
// Pathfinding.ClipperLib.LocalMinima
struct LocalMinima_t2863342752;
// Pathfinding.ClipperLib.ClipperException
struct ClipperException_t2818206852;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// Pathfinding.ClipperLib.PolyNode
struct PolyNode_t3336253808;
// System.Collections.Generic.List`1<Pathfinding.ClipperLib.PolyNode>
struct List_1_t409472064;
// Pathfinding.ClipperLib.Scanbeam
struct Scanbeam_t1885114670;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "Pathfinding_ClipperLib_U3CModuleU3E86524790.h"
#include "Pathfinding_ClipperLib_U3CModuleU3E86524790MethodDeclarations.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Clipp636949239.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Clipp636949239MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Int321153838500.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Clipp512275816MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen319023540MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1043336060MethodDeclarations.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Scan1885114670.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_TEdg3950806139.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Inte4106323947.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Collections_Generic_List_1_gen319023540.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1043336060.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2392209947MethodDeclarations.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Clipp512275816.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2392209947.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Loca2863342752.h"
#include "mscorlib_System_Int641153838595.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Scan1885114670MethodDeclarations.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Clip3693001772.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Poly3967424203.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1767529987.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Clip2818206852MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1767529987MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Clip2818206852.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Poly3336435468.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_OutR3245805284.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_OutP3947633180.h"
#include "mscorlib_System_Double3868226565.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Join3970117804MethodDeclarations.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Join3970117804.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Poly3336442536.h"
#include "mscorlib_System_Math2862914300MethodDeclarations.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_EdgeSi11130422.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_OutR3245805284MethodDeclarations.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Poly3336253808.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_OutP3947633180MethodDeclarations.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179MethodDeclarations.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Direc297621929.h"
#include "mscorlib_System_Object4170816371.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Inte4106323947MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen399344435.h"
#include "mscorlib_System_Collections_Generic_List_1_gen399344435MethodDeclarations.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Poly3336435468MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen409472064MethodDeclarations.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Poly3336253808MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen409472064.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat338696310MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat338696310.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Int13067532394MethodDeclarations.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Int13067532394.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1024024395MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1024024395.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_TEdg3950806139MethodDeclarations.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Loca2863342752MethodDeclarations.h"
#include "mscorlib_System_Exception3991598821MethodDeclarations.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Clip3693001772MethodDeclarations.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Direc297621929MethodDeclarations.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_EdgeSi11130422MethodDeclarations.h"
#include "mscorlib_System_UInt6424668076.h"
#include "mscorlib_System_Int641153838595MethodDeclarations.h"
#include "mscorlib_System_UInt6424668076MethodDeclarations.h"
#include "mscorlib_System_ValueType1744280289MethodDeclarations.h"
#include "mscorlib_System_ValueType1744280289.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Poly3967424203MethodDeclarations.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Poly3336442536MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Pathfinding.ClipperLib.Clipper::.ctor(System.Int32)
extern Il2CppClass* List_1_t319023540_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1043336060_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1463096449_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3314438089_MethodInfo_var;
extern const uint32_t Clipper__ctor_m3661235519_MetadataUsageId;
extern "C"  void Clipper__ctor_m3661235519 (Clipper_t636949239 * __this, int32_t ___InitOptions0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper__ctor_m3661235519_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ClipperBase__ctor_m848649117(__this, /*hidden argument*/NULL);
		__this->set_m_Scanbeam_17((Scanbeam_t1885114670 *)NULL);
		__this->set_m_ActiveEdges_18((TEdge_t3950806139 *)NULL);
		__this->set_m_SortedEdges_19((TEdge_t3950806139 *)NULL);
		__this->set_m_IntersectNodes_20((IntersectNode_t4106323947 *)NULL);
		__this->set_m_ExecuteLocked_21((bool)0);
		__this->set_m_UsingPolyTree_26((bool)0);
		List_1_t319023540 * L_0 = (List_1_t319023540 *)il2cpp_codegen_object_new(List_1_t319023540_il2cpp_TypeInfo_var);
		List_1__ctor_m1463096449(L_0, /*hidden argument*/List_1__ctor_m1463096449_MethodInfo_var);
		__this->set_m_PolyOuts_15(L_0);
		List_1_t1043336060 * L_1 = (List_1_t1043336060 *)il2cpp_codegen_object_new(List_1_t1043336060_il2cpp_TypeInfo_var);
		List_1__ctor_m3314438089(L_1, /*hidden argument*/List_1__ctor_m3314438089_MethodInfo_var);
		__this->set_m_Joins_24(L_1);
		List_1_t1043336060 * L_2 = (List_1_t1043336060 *)il2cpp_codegen_object_new(List_1_t1043336060_il2cpp_TypeInfo_var);
		List_1__ctor_m3314438089(L_2, /*hidden argument*/List_1__ctor_m3314438089_MethodInfo_var);
		__this->set_m_GhostJoins_25(L_2);
		int32_t L_3 = ___InitOptions0;
		Clipper_set_ReverseSolution_m3191234529(__this, (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)1&(int32_t)L_3))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		int32_t L_4 = ___InitOptions0;
		Clipper_set_StrictlySimple_m4072491954(__this, (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)2&(int32_t)L_4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		int32_t L_5 = ___InitOptions0;
		ClipperBase_set_PreserveCollinear_m2366595086(__this, (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)4&(int32_t)L_5))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::Clear()
extern const MethodInfo* List_1_get_Count_m932073596_MethodInfo_var;
extern const uint32_t Clipper_Clear_m1082589465_MetadataUsageId;
extern "C"  void Clipper_Clear_m1082589465 (Clipper_t636949239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_Clear_m1082589465_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2392209947 * L_0 = ((ClipperBase_t512275816 *)__this)->get_m_edges_8();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m932073596(L_0, /*hidden argument*/List_1_get_Count_m932073596_MethodInfo_var);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Clipper_DisposeAllPolyPts_m4187081581(__this, /*hidden argument*/NULL);
		ClipperBase_Clear_m2549749704(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::Reset()
extern "C"  void Clipper_Reset_m1322889115 (Clipper_t636949239 * __this, const MethodInfo* method)
{
	LocalMinima_t2863342752 * V_0 = NULL;
	{
		ClipperBase_Reset_m2790049354(__this, /*hidden argument*/NULL);
		__this->set_m_Scanbeam_17((Scanbeam_t1885114670 *)NULL);
		__this->set_m_ActiveEdges_18((TEdge_t3950806139 *)NULL);
		__this->set_m_SortedEdges_19((TEdge_t3950806139 *)NULL);
		Clipper_DisposeAllPolyPts_m4187081581(__this, /*hidden argument*/NULL);
		LocalMinima_t2863342752 * L_0 = ((ClipperBase_t512275816 *)__this)->get_m_MinimaList_6();
		V_0 = L_0;
		goto IL_0040;
	}

IL_002d:
	{
		LocalMinima_t2863342752 * L_1 = V_0;
		NullCheck(L_1);
		int64_t L_2 = L_1->get_Y_0();
		Clipper_InsertScanbeam_m2447896813(__this, L_2, /*hidden argument*/NULL);
		LocalMinima_t2863342752 * L_3 = V_0;
		NullCheck(L_3);
		LocalMinima_t2863342752 * L_4 = L_3->get_Next_3();
		V_0 = L_4;
	}

IL_0040:
	{
		LocalMinima_t2863342752 * L_5 = V_0;
		if (L_5)
		{
			goto IL_002d;
		}
	}
	{
		return;
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::get_ReverseSolution()
extern "C"  bool Clipper_get_ReverseSolution_m2875557634 (Clipper_t636949239 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CReverseSolutionU3Ek__BackingField_27();
		return L_0;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::set_ReverseSolution(System.Boolean)
extern "C"  void Clipper_set_ReverseSolution_m3191234529 (Clipper_t636949239 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CReverseSolutionU3Ek__BackingField_27(L_0);
		return;
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::get_StrictlySimple()
extern "C"  bool Clipper_get_StrictlySimple_m1330170723 (Clipper_t636949239 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CStrictlySimpleU3Ek__BackingField_28();
		return L_0;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::set_StrictlySimple(System.Boolean)
extern "C"  void Clipper_set_StrictlySimple_m4072491954 (Clipper_t636949239 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CStrictlySimpleU3Ek__BackingField_28(L_0);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::InsertScanbeam(System.Int64)
extern Il2CppClass* Scanbeam_t1885114670_il2cpp_TypeInfo_var;
extern const uint32_t Clipper_InsertScanbeam_m2447896813_MetadataUsageId;
extern "C"  void Clipper_InsertScanbeam_m2447896813 (Clipper_t636949239 * __this, int64_t ___Y0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_InsertScanbeam_m2447896813_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Scanbeam_t1885114670 * V_0 = NULL;
	Scanbeam_t1885114670 * V_1 = NULL;
	Scanbeam_t1885114670 * V_2 = NULL;
	{
		Scanbeam_t1885114670 * L_0 = __this->get_m_Scanbeam_17();
		if (L_0)
		{
			goto IL_0033;
		}
	}
	{
		Scanbeam_t1885114670 * L_1 = (Scanbeam_t1885114670 *)il2cpp_codegen_object_new(Scanbeam_t1885114670_il2cpp_TypeInfo_var);
		Scanbeam__ctor_m2642114495(L_1, /*hidden argument*/NULL);
		__this->set_m_Scanbeam_17(L_1);
		Scanbeam_t1885114670 * L_2 = __this->get_m_Scanbeam_17();
		NullCheck(L_2);
		L_2->set_Next_1((Scanbeam_t1885114670 *)NULL);
		Scanbeam_t1885114670 * L_3 = __this->get_m_Scanbeam_17();
		int64_t L_4 = ___Y0;
		NullCheck(L_3);
		L_3->set_Y_0(L_4);
		goto IL_00c5;
	}

IL_0033:
	{
		int64_t L_5 = ___Y0;
		Scanbeam_t1885114670 * L_6 = __this->get_m_Scanbeam_17();
		NullCheck(L_6);
		int64_t L_7 = L_6->get_Y_0();
		if ((((int64_t)L_5) <= ((int64_t)L_7)))
		{
			goto IL_0069;
		}
	}
	{
		Scanbeam_t1885114670 * L_8 = (Scanbeam_t1885114670 *)il2cpp_codegen_object_new(Scanbeam_t1885114670_il2cpp_TypeInfo_var);
		Scanbeam__ctor_m2642114495(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
		Scanbeam_t1885114670 * L_9 = V_0;
		int64_t L_10 = ___Y0;
		NullCheck(L_9);
		L_9->set_Y_0(L_10);
		Scanbeam_t1885114670 * L_11 = V_0;
		Scanbeam_t1885114670 * L_12 = __this->get_m_Scanbeam_17();
		NullCheck(L_11);
		L_11->set_Next_1(L_12);
		Scanbeam_t1885114670 * L_13 = V_0;
		__this->set_m_Scanbeam_17(L_13);
		goto IL_00c5;
	}

IL_0069:
	{
		Scanbeam_t1885114670 * L_14 = __this->get_m_Scanbeam_17();
		V_1 = L_14;
		goto IL_007c;
	}

IL_0075:
	{
		Scanbeam_t1885114670 * L_15 = V_1;
		NullCheck(L_15);
		Scanbeam_t1885114670 * L_16 = L_15->get_Next_1();
		V_1 = L_16;
	}

IL_007c:
	{
		Scanbeam_t1885114670 * L_17 = V_1;
		NullCheck(L_17);
		Scanbeam_t1885114670 * L_18 = L_17->get_Next_1();
		if (!L_18)
		{
			goto IL_0098;
		}
	}
	{
		int64_t L_19 = ___Y0;
		Scanbeam_t1885114670 * L_20 = V_1;
		NullCheck(L_20);
		Scanbeam_t1885114670 * L_21 = L_20->get_Next_1();
		NullCheck(L_21);
		int64_t L_22 = L_21->get_Y_0();
		if ((((int64_t)L_19) <= ((int64_t)L_22)))
		{
			goto IL_0075;
		}
	}

IL_0098:
	{
		int64_t L_23 = ___Y0;
		Scanbeam_t1885114670 * L_24 = V_1;
		NullCheck(L_24);
		int64_t L_25 = L_24->get_Y_0();
		if ((!(((uint64_t)L_23) == ((uint64_t)L_25))))
		{
			goto IL_00a5;
		}
	}
	{
		return;
	}

IL_00a5:
	{
		Scanbeam_t1885114670 * L_26 = (Scanbeam_t1885114670 *)il2cpp_codegen_object_new(Scanbeam_t1885114670_il2cpp_TypeInfo_var);
		Scanbeam__ctor_m2642114495(L_26, /*hidden argument*/NULL);
		V_2 = L_26;
		Scanbeam_t1885114670 * L_27 = V_2;
		int64_t L_28 = ___Y0;
		NullCheck(L_27);
		L_27->set_Y_0(L_28);
		Scanbeam_t1885114670 * L_29 = V_2;
		Scanbeam_t1885114670 * L_30 = V_1;
		NullCheck(L_30);
		Scanbeam_t1885114670 * L_31 = L_30->get_Next_1();
		NullCheck(L_29);
		L_29->set_Next_1(L_31);
		Scanbeam_t1885114670 * L_32 = V_1;
		Scanbeam_t1885114670 * L_33 = V_2;
		NullCheck(L_32);
		L_32->set_Next_1(L_33);
	}

IL_00c5:
	{
		return;
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::Execute(Pathfinding.ClipperLib.ClipType,System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>,Pathfinding.ClipperLib.PolyFillType,Pathfinding.ClipperLib.PolyFillType)
extern Il2CppClass* ClipperException_t2818206852_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m476408529_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1597694969;
extern const uint32_t Clipper_Execute_m264922624_MetadataUsageId;
extern "C"  bool Clipper_Execute_m264922624 (Clipper_t636949239 * __this, int32_t ___clipType0, List_1_t1767529987 * ___solution1, int32_t ___subjFillType2, int32_t ___clipFillType3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_Execute_m264922624_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		bool L_0 = __this->get_m_ExecuteLocked_21();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		bool L_1 = ((ClipperBase_t512275816 *)__this)->get_m_HasOpenPaths_10();
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		ClipperException_t2818206852 * L_2 = (ClipperException_t2818206852 *)il2cpp_codegen_object_new(ClipperException_t2818206852_il2cpp_TypeInfo_var);
		ClipperException__ctor_m106504313(L_2, _stringLiteral1597694969, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0023:
	{
		__this->set_m_ExecuteLocked_21((bool)1);
		List_1_t1767529987 * L_3 = ___solution1;
		NullCheck(L_3);
		List_1_Clear_m476408529(L_3, /*hidden argument*/List_1_Clear_m476408529_MethodInfo_var);
		int32_t L_4 = ___subjFillType2;
		__this->set_m_SubjFillType_23(L_4);
		int32_t L_5 = ___clipFillType3;
		__this->set_m_ClipFillType_22(L_5);
		int32_t L_6 = ___clipType0;
		__this->set_m_ClipType_16(L_6);
		__this->set_m_UsingPolyTree_26((bool)0);
		bool L_7 = Clipper_ExecuteInternal_m87382082(__this, /*hidden argument*/NULL);
		V_0 = L_7;
		bool L_8 = V_0;
		if (!L_8)
		{
			goto IL_0061;
		}
	}
	{
		List_1_t1767529987 * L_9 = ___solution1;
		Clipper_BuildResult_m1163037098(__this, L_9, /*hidden argument*/NULL);
	}

IL_0061:
	{
		__this->set_m_ExecuteLocked_21((bool)0);
		bool L_10 = V_0;
		return L_10;
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::Execute(Pathfinding.ClipperLib.ClipType,Pathfinding.ClipperLib.PolyTree,Pathfinding.ClipperLib.PolyFillType,Pathfinding.ClipperLib.PolyFillType)
extern "C"  bool Clipper_Execute_m4064679803 (Clipper_t636949239 * __this, int32_t ___clipType0, PolyTree_t3336435468 * ___polytree1, int32_t ___subjFillType2, int32_t ___clipFillType3, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_m_ExecuteLocked_21();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		__this->set_m_ExecuteLocked_21((bool)1);
		int32_t L_1 = ___subjFillType2;
		__this->set_m_SubjFillType_23(L_1);
		int32_t L_2 = ___clipFillType3;
		__this->set_m_ClipFillType_22(L_2);
		int32_t L_3 = ___clipType0;
		__this->set_m_ClipType_16(L_3);
		__this->set_m_UsingPolyTree_26((bool)1);
		bool L_4 = Clipper_ExecuteInternal_m87382082(__this, /*hidden argument*/NULL);
		V_0 = L_4;
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_0045;
		}
	}
	{
		PolyTree_t3336435468 * L_6 = ___polytree1;
		Clipper_BuildResult2_m2430771551(__this, L_6, /*hidden argument*/NULL);
	}

IL_0045:
	{
		__this->set_m_ExecuteLocked_21((bool)0);
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::FixHoleLinkage(Pathfinding.ClipperLib.OutRec)
extern "C"  void Clipper_FixHoleLinkage_m891396734 (Clipper_t636949239 * __this, OutRec_t3245805284 * ___outRec0, const MethodInfo* method)
{
	OutRec_t3245805284 * V_0 = NULL;
	{
		OutRec_t3245805284 * L_0 = ___outRec0;
		NullCheck(L_0);
		OutRec_t3245805284 * L_1 = L_0->get_FirstLeft_3();
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		OutRec_t3245805284 * L_2 = ___outRec0;
		NullCheck(L_2);
		bool L_3 = L_2->get_IsHole_1();
		OutRec_t3245805284 * L_4 = ___outRec0;
		NullCheck(L_4);
		OutRec_t3245805284 * L_5 = L_4->get_FirstLeft_3();
		NullCheck(L_5);
		bool L_6 = L_5->get_IsHole_1();
		if ((((int32_t)L_3) == ((int32_t)L_6)))
		{
			goto IL_0032;
		}
	}
	{
		OutRec_t3245805284 * L_7 = ___outRec0;
		NullCheck(L_7);
		OutRec_t3245805284 * L_8 = L_7->get_FirstLeft_3();
		NullCheck(L_8);
		OutPt_t3947633180 * L_9 = L_8->get_Pts_4();
		if (!L_9)
		{
			goto IL_0032;
		}
	}

IL_0031:
	{
		return;
	}

IL_0032:
	{
		OutRec_t3245805284 * L_10 = ___outRec0;
		NullCheck(L_10);
		OutRec_t3245805284 * L_11 = L_10->get_FirstLeft_3();
		V_0 = L_11;
		goto IL_0045;
	}

IL_003e:
	{
		OutRec_t3245805284 * L_12 = V_0;
		NullCheck(L_12);
		OutRec_t3245805284 * L_13 = L_12->get_FirstLeft_3();
		V_0 = L_13;
	}

IL_0045:
	{
		OutRec_t3245805284 * L_14 = V_0;
		if (!L_14)
		{
			goto IL_0067;
		}
	}
	{
		OutRec_t3245805284 * L_15 = V_0;
		NullCheck(L_15);
		bool L_16 = L_15->get_IsHole_1();
		OutRec_t3245805284 * L_17 = ___outRec0;
		NullCheck(L_17);
		bool L_18 = L_17->get_IsHole_1();
		if ((((int32_t)L_16) == ((int32_t)L_18)))
		{
			goto IL_003e;
		}
	}
	{
		OutRec_t3245805284 * L_19 = V_0;
		NullCheck(L_19);
		OutPt_t3947633180 * L_20 = L_19->get_Pts_4();
		if (!L_20)
		{
			goto IL_003e;
		}
	}

IL_0067:
	{
		OutRec_t3245805284 * L_21 = ___outRec0;
		OutRec_t3245805284 * L_22 = V_0;
		NullCheck(L_21);
		L_21->set_FirstLeft_3(L_22);
		return;
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::ExecuteInternal()
extern const MethodInfo* List_1_Clear_m720571380_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2053160358_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m1367576335_MethodInfo_var;
extern const uint32_t Clipper_ExecuteInternal_m87382082_MetadataUsageId;
extern "C"  bool Clipper_ExecuteInternal_m87382082 (Clipper_t636949239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_ExecuteInternal_m87382082_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int64_t V_1 = 0;
	int64_t V_2 = 0;
	int32_t V_3 = 0;
	OutRec_t3245805284 * V_4 = NULL;
	int32_t V_5 = 0;
	OutRec_t3245805284 * V_6 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			VirtActionInvoker0::Invoke(5 /* System.Void Pathfinding.ClipperLib.ClipperBase::Reset() */, __this);
			LocalMinima_t2863342752 * L_0 = ((ClipperBase_t512275816 *)__this)->get_m_CurrentLM_7();
			if (L_0)
			{
				goto IL_0018;
			}
		}

IL_0011:
		{
			V_0 = (bool)0;
			IL2CPP_LEAVE(0x185, FINALLY_016e);
		}

IL_0018:
		{
			int64_t L_1 = Clipper_PopScanbeam_m2240798930(__this, /*hidden argument*/NULL);
			V_1 = L_1;
		}

IL_001f:
		{
			int64_t L_2 = V_1;
			Clipper_InsertLocalMinimaIntoAEL_m2894934835(__this, L_2, /*hidden argument*/NULL);
			List_1_t1043336060 * L_3 = __this->get_m_GhostJoins_25();
			NullCheck(L_3);
			List_1_Clear_m720571380(L_3, /*hidden argument*/List_1_Clear_m720571380_MethodInfo_var);
			Clipper_ProcessHorizontals_m563259309(__this, (bool)0, /*hidden argument*/NULL);
			Scanbeam_t1885114670 * L_4 = __this->get_m_Scanbeam_17();
			if (L_4)
			{
				goto IL_0048;
			}
		}

IL_0043:
		{
			goto IL_0082;
		}

IL_0048:
		{
			int64_t L_5 = Clipper_PopScanbeam_m2240798930(__this, /*hidden argument*/NULL);
			V_2 = L_5;
			int64_t L_6 = V_1;
			int64_t L_7 = V_2;
			bool L_8 = Clipper_ProcessIntersections_m661125977(__this, L_6, L_7, /*hidden argument*/NULL);
			if (L_8)
			{
				goto IL_0063;
			}
		}

IL_005c:
		{
			V_0 = (bool)0;
			IL2CPP_LEAVE(0x185, FINALLY_016e);
		}

IL_0063:
		{
			int64_t L_9 = V_2;
			Clipper_ProcessEdgesAtTopOfScanbeam_m897821276(__this, L_9, /*hidden argument*/NULL);
			int64_t L_10 = V_2;
			V_1 = L_10;
			Scanbeam_t1885114670 * L_11 = __this->get_m_Scanbeam_17();
			if (L_11)
			{
				goto IL_001f;
			}
		}

IL_0077:
		{
			LocalMinima_t2863342752 * L_12 = ((ClipperBase_t512275816 *)__this)->get_m_CurrentLM_7();
			if (L_12)
			{
				goto IL_001f;
			}
		}

IL_0082:
		{
			V_3 = 0;
			goto IL_00eb;
		}

IL_0089:
		{
			List_1_t319023540 * L_13 = __this->get_m_PolyOuts_15();
			int32_t L_14 = V_3;
			NullCheck(L_13);
			OutRec_t3245805284 * L_15 = List_1_get_Item_m2053160358(L_13, L_14, /*hidden argument*/List_1_get_Item_m2053160358_MethodInfo_var);
			V_4 = L_15;
			OutRec_t3245805284 * L_16 = V_4;
			NullCheck(L_16);
			OutPt_t3947633180 * L_17 = L_16->get_Pts_4();
			if (!L_17)
			{
				goto IL_00af;
			}
		}

IL_00a3:
		{
			OutRec_t3245805284 * L_18 = V_4;
			NullCheck(L_18);
			bool L_19 = L_18->get_IsOpen_2();
			if (!L_19)
			{
				goto IL_00b4;
			}
		}

IL_00af:
		{
			goto IL_00e7;
		}

IL_00b4:
		{
			OutRec_t3245805284 * L_20 = V_4;
			NullCheck(L_20);
			bool L_21 = L_20->get_IsHole_1();
			bool L_22 = Clipper_get_ReverseSolution_m2875557634(__this, /*hidden argument*/NULL);
			OutRec_t3245805284 * L_23 = V_4;
			double L_24 = Clipper_Area_m2380044382(__this, L_23, /*hidden argument*/NULL);
			if ((!(((uint32_t)((int32_t)((int32_t)L_21^(int32_t)L_22))) == ((uint32_t)((((double)L_24) > ((double)(0.0)))? 1 : 0)))))
			{
				goto IL_00e7;
			}
		}

IL_00da:
		{
			OutRec_t3245805284 * L_25 = V_4;
			NullCheck(L_25);
			OutPt_t3947633180 * L_26 = L_25->get_Pts_4();
			Clipper_ReversePolyPtLinks_m59152439(__this, L_26, /*hidden argument*/NULL);
		}

IL_00e7:
		{
			int32_t L_27 = V_3;
			V_3 = ((int32_t)((int32_t)L_27+(int32_t)1));
		}

IL_00eb:
		{
			int32_t L_28 = V_3;
			List_1_t319023540 * L_29 = __this->get_m_PolyOuts_15();
			NullCheck(L_29);
			int32_t L_30 = List_1_get_Count_m1367576335(L_29, /*hidden argument*/List_1_get_Count_m1367576335_MethodInfo_var);
			if ((((int32_t)L_28) < ((int32_t)L_30)))
			{
				goto IL_0089;
			}
		}

IL_00fc:
		{
			Clipper_JoinCommonEdges_m1930505133(__this, /*hidden argument*/NULL);
			V_5 = 0;
			goto IL_013f;
		}

IL_010a:
		{
			List_1_t319023540 * L_31 = __this->get_m_PolyOuts_15();
			int32_t L_32 = V_5;
			NullCheck(L_31);
			OutRec_t3245805284 * L_33 = List_1_get_Item_m2053160358(L_31, L_32, /*hidden argument*/List_1_get_Item_m2053160358_MethodInfo_var);
			V_6 = L_33;
			OutRec_t3245805284 * L_34 = V_6;
			NullCheck(L_34);
			OutPt_t3947633180 * L_35 = L_34->get_Pts_4();
			if (!L_35)
			{
				goto IL_0139;
			}
		}

IL_0125:
		{
			OutRec_t3245805284 * L_36 = V_6;
			NullCheck(L_36);
			bool L_37 = L_36->get_IsOpen_2();
			if (L_37)
			{
				goto IL_0139;
			}
		}

IL_0131:
		{
			OutRec_t3245805284 * L_38 = V_6;
			Clipper_FixupOutPolygon_m3468612540(__this, L_38, /*hidden argument*/NULL);
		}

IL_0139:
		{
			int32_t L_39 = V_5;
			V_5 = ((int32_t)((int32_t)L_39+(int32_t)1));
		}

IL_013f:
		{
			int32_t L_40 = V_5;
			List_1_t319023540 * L_41 = __this->get_m_PolyOuts_15();
			NullCheck(L_41);
			int32_t L_42 = List_1_get_Count_m1367576335(L_41, /*hidden argument*/List_1_get_Count_m1367576335_MethodInfo_var);
			if ((((int32_t)L_40) < ((int32_t)L_42)))
			{
				goto IL_010a;
			}
		}

IL_0151:
		{
			bool L_43 = Clipper_get_StrictlySimple_m1330170723(__this, /*hidden argument*/NULL);
			if (!L_43)
			{
				goto IL_0162;
			}
		}

IL_015c:
		{
			Clipper_DoSimplePolygons_m2818696524(__this, /*hidden argument*/NULL);
		}

IL_0162:
		{
			V_0 = (bool)1;
			IL2CPP_LEAVE(0x185, FINALLY_016e);
		}

IL_0169:
		{
			; // IL_0169: leave IL_0185
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_016e;
	}

FINALLY_016e:
	{ // begin finally (depth: 1)
		List_1_t1043336060 * L_44 = __this->get_m_Joins_24();
		NullCheck(L_44);
		List_1_Clear_m720571380(L_44, /*hidden argument*/List_1_Clear_m720571380_MethodInfo_var);
		List_1_t1043336060 * L_45 = __this->get_m_GhostJoins_25();
		NullCheck(L_45);
		List_1_Clear_m720571380(L_45, /*hidden argument*/List_1_Clear_m720571380_MethodInfo_var);
		IL2CPP_END_FINALLY(366)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(366)
	{
		IL2CPP_JUMP_TBL(0x185, IL_0185)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0185:
	{
		bool L_46 = V_0;
		return L_46;
	}
}
// System.Int64 Pathfinding.ClipperLib.Clipper::PopScanbeam()
extern "C"  int64_t Clipper_PopScanbeam_m2240798930 (Clipper_t636949239 * __this, const MethodInfo* method)
{
	int64_t V_0 = 0;
	Scanbeam_t1885114670 * V_1 = NULL;
	{
		Scanbeam_t1885114670 * L_0 = __this->get_m_Scanbeam_17();
		NullCheck(L_0);
		int64_t L_1 = L_0->get_Y_0();
		V_0 = L_1;
		Scanbeam_t1885114670 * L_2 = __this->get_m_Scanbeam_17();
		V_1 = L_2;
		Scanbeam_t1885114670 * L_3 = __this->get_m_Scanbeam_17();
		NullCheck(L_3);
		Scanbeam_t1885114670 * L_4 = L_3->get_Next_1();
		__this->set_m_Scanbeam_17(L_4);
		V_1 = (Scanbeam_t1885114670 *)NULL;
		int64_t L_5 = V_0;
		return L_5;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::DisposeAllPolyPts()
extern const MethodInfo* List_1_get_Count_m1367576335_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m3164197036_MethodInfo_var;
extern const uint32_t Clipper_DisposeAllPolyPts_m4187081581_MetadataUsageId;
extern "C"  void Clipper_DisposeAllPolyPts_m4187081581 (Clipper_t636949239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_DisposeAllPolyPts_m4187081581_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0012;
	}

IL_0007:
	{
		int32_t L_0 = V_0;
		Clipper_DisposeOutRec_m4039825918(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		V_0 = ((int32_t)((int32_t)L_1+(int32_t)1));
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		List_1_t319023540 * L_3 = __this->get_m_PolyOuts_15();
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Count_m1367576335(L_3, /*hidden argument*/List_1_get_Count_m1367576335_MethodInfo_var);
		if ((((int32_t)L_2) < ((int32_t)L_4)))
		{
			goto IL_0007;
		}
	}
	{
		List_1_t319023540 * L_5 = __this->get_m_PolyOuts_15();
		NullCheck(L_5);
		List_1_Clear_m3164197036(L_5, /*hidden argument*/List_1_Clear_m3164197036_MethodInfo_var);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::DisposeOutRec(System.Int32)
extern const MethodInfo* List_1_get_Item_m2053160358_MethodInfo_var;
extern const MethodInfo* List_1_set_Item_m3965723443_MethodInfo_var;
extern const uint32_t Clipper_DisposeOutRec_m4039825918_MetadataUsageId;
extern "C"  void Clipper_DisposeOutRec_m4039825918 (Clipper_t636949239 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_DisposeOutRec_m4039825918_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	OutRec_t3245805284 * V_0 = NULL;
	{
		List_1_t319023540 * L_0 = __this->get_m_PolyOuts_15();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		OutRec_t3245805284 * L_2 = List_1_get_Item_m2053160358(L_0, L_1, /*hidden argument*/List_1_get_Item_m2053160358_MethodInfo_var);
		V_0 = L_2;
		OutRec_t3245805284 * L_3 = V_0;
		NullCheck(L_3);
		OutPt_t3947633180 * L_4 = L_3->get_Pts_4();
		if (!L_4)
		{
			goto IL_0024;
		}
	}
	{
		OutRec_t3245805284 * L_5 = V_0;
		NullCheck(L_5);
		OutPt_t3947633180 * L_6 = L_5->get_Pts_4();
		Clipper_DisposeOutPts_m3153880070(__this, L_6, /*hidden argument*/NULL);
	}

IL_0024:
	{
		V_0 = (OutRec_t3245805284 *)NULL;
		List_1_t319023540 * L_7 = __this->get_m_PolyOuts_15();
		int32_t L_8 = ___index0;
		NullCheck(L_7);
		List_1_set_Item_m3965723443(L_7, L_8, (OutRec_t3245805284 *)NULL, /*hidden argument*/List_1_set_Item_m3965723443_MethodInfo_var);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::DisposeOutPts(Pathfinding.ClipperLib.OutPt)
extern "C"  void Clipper_DisposeOutPts_m3153880070 (Clipper_t636949239 * __this, OutPt_t3947633180 * ___pp0, const MethodInfo* method)
{
	OutPt_t3947633180 * V_0 = NULL;
	{
		OutPt_t3947633180 * L_0 = ___pp0;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		V_0 = (OutPt_t3947633180 *)NULL;
		OutPt_t3947633180 * L_1 = ___pp0;
		NullCheck(L_1);
		OutPt_t3947633180 * L_2 = L_1->get_Prev_3();
		NullCheck(L_2);
		L_2->set_Next_2((OutPt_t3947633180 *)NULL);
		goto IL_0026;
	}

IL_001a:
	{
		OutPt_t3947633180 * L_3 = ___pp0;
		V_0 = L_3;
		OutPt_t3947633180 * L_4 = ___pp0;
		NullCheck(L_4);
		OutPt_t3947633180 * L_5 = L_4->get_Next_2();
		___pp0 = L_5;
		V_0 = (OutPt_t3947633180 *)NULL;
	}

IL_0026:
	{
		OutPt_t3947633180 * L_6 = ___pp0;
		if (L_6)
		{
			goto IL_001a;
		}
	}
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::AddJoin(Pathfinding.ClipperLib.OutPt,Pathfinding.ClipperLib.OutPt,Pathfinding.ClipperLib.IntPoint)
extern Il2CppClass* Join_t3970117804_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m3008798905_MethodInfo_var;
extern const uint32_t Clipper_AddJoin_m2260685358_MetadataUsageId;
extern "C"  void Clipper_AddJoin_m2260685358 (Clipper_t636949239 * __this, OutPt_t3947633180 * ___Op10, OutPt_t3947633180 * ___Op21, IntPoint_t3326126179  ___OffPt2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_AddJoin_m2260685358_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Join_t3970117804 * V_0 = NULL;
	{
		Join_t3970117804 * L_0 = (Join_t3970117804 *)il2cpp_codegen_object_new(Join_t3970117804_il2cpp_TypeInfo_var);
		Join__ctor_m1417981953(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Join_t3970117804 * L_1 = V_0;
		OutPt_t3947633180 * L_2 = ___Op10;
		NullCheck(L_1);
		L_1->set_OutPt1_0(L_2);
		Join_t3970117804 * L_3 = V_0;
		OutPt_t3947633180 * L_4 = ___Op21;
		NullCheck(L_3);
		L_3->set_OutPt2_1(L_4);
		Join_t3970117804 * L_5 = V_0;
		IntPoint_t3326126179  L_6 = ___OffPt2;
		NullCheck(L_5);
		L_5->set_OffPt_2(L_6);
		List_1_t1043336060 * L_7 = __this->get_m_Joins_24();
		Join_t3970117804 * L_8 = V_0;
		NullCheck(L_7);
		List_1_Add_m3008798905(L_7, L_8, /*hidden argument*/List_1_Add_m3008798905_MethodInfo_var);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::AddGhostJoin(Pathfinding.ClipperLib.OutPt,Pathfinding.ClipperLib.IntPoint)
extern Il2CppClass* Join_t3970117804_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m3008798905_MethodInfo_var;
extern const uint32_t Clipper_AddGhostJoin_m1097100551_MetadataUsageId;
extern "C"  void Clipper_AddGhostJoin_m1097100551 (Clipper_t636949239 * __this, OutPt_t3947633180 * ___Op0, IntPoint_t3326126179  ___OffPt1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_AddGhostJoin_m1097100551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Join_t3970117804 * V_0 = NULL;
	{
		Join_t3970117804 * L_0 = (Join_t3970117804 *)il2cpp_codegen_object_new(Join_t3970117804_il2cpp_TypeInfo_var);
		Join__ctor_m1417981953(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Join_t3970117804 * L_1 = V_0;
		OutPt_t3947633180 * L_2 = ___Op0;
		NullCheck(L_1);
		L_1->set_OutPt1_0(L_2);
		Join_t3970117804 * L_3 = V_0;
		IntPoint_t3326126179  L_4 = ___OffPt1;
		NullCheck(L_3);
		L_3->set_OffPt_2(L_4);
		List_1_t1043336060 * L_5 = __this->get_m_GhostJoins_25();
		Join_t3970117804 * L_6 = V_0;
		NullCheck(L_5);
		List_1_Add_m3008798905(L_5, L_6, /*hidden argument*/List_1_Add_m3008798905_MethodInfo_var);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::InsertLocalMinimaIntoAEL(System.Int64)
extern const MethodInfo* List_1_get_Count_m896344279_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2818082526_MethodInfo_var;
extern const uint32_t Clipper_InsertLocalMinimaIntoAEL_m2894934835_MetadataUsageId;
extern "C"  void Clipper_InsertLocalMinimaIntoAEL_m2894934835 (Clipper_t636949239 * __this, int64_t ___botY0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_InsertLocalMinimaIntoAEL_m2894934835_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TEdge_t3950806139 * V_0 = NULL;
	TEdge_t3950806139 * V_1 = NULL;
	OutPt_t3947633180 * V_2 = NULL;
	int32_t V_3 = 0;
	Join_t3970117804 * V_4 = NULL;
	OutPt_t3947633180 * V_5 = NULL;
	OutPt_t3947633180 * V_6 = NULL;
	TEdge_t3950806139 * V_7 = NULL;
	{
		goto IL_02cd;
	}

IL_0005:
	{
		LocalMinima_t2863342752 * L_0 = ((ClipperBase_t512275816 *)__this)->get_m_CurrentLM_7();
		NullCheck(L_0);
		TEdge_t3950806139 * L_1 = L_0->get_LeftBound_1();
		V_0 = L_1;
		LocalMinima_t2863342752 * L_2 = ((ClipperBase_t512275816 *)__this)->get_m_CurrentLM_7();
		NullCheck(L_2);
		TEdge_t3950806139 * L_3 = L_2->get_RightBound_2();
		V_1 = L_3;
		ClipperBase_PopLocalMinima_m3271239084(__this, /*hidden argument*/NULL);
		V_2 = (OutPt_t3947633180 *)NULL;
		TEdge_t3950806139 * L_4 = V_0;
		if (L_4)
		{
			goto IL_0059;
		}
	}
	{
		TEdge_t3950806139 * L_5 = V_1;
		Clipper_InsertEdgeIntoAEL_m1577452744(__this, L_5, (TEdge_t3950806139 *)NULL, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_6 = V_1;
		Clipper_SetWindingCount_m167856286(__this, L_6, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_7 = V_1;
		bool L_8 = Clipper_IsContributing_m3580620193(__this, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0054;
		}
	}
	{
		TEdge_t3950806139 * L_9 = V_1;
		TEdge_t3950806139 * L_10 = V_1;
		NullCheck(L_10);
		IntPoint_t3326126179  L_11 = L_10->get_Bot_0();
		OutPt_t3947633180 * L_12 = Clipper_AddOutPt_m3632434112(__this, L_9, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
	}

IL_0054:
	{
		goto IL_00b4;
	}

IL_0059:
	{
		TEdge_t3950806139 * L_13 = V_0;
		Clipper_InsertEdgeIntoAEL_m1577452744(__this, L_13, (TEdge_t3950806139 *)NULL, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_14 = V_1;
		TEdge_t3950806139 * L_15 = V_0;
		Clipper_InsertEdgeIntoAEL_m1577452744(__this, L_14, L_15, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_16 = V_0;
		Clipper_SetWindingCount_m167856286(__this, L_16, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_17 = V_1;
		TEdge_t3950806139 * L_18 = V_0;
		NullCheck(L_18);
		int32_t L_19 = L_18->get_WindCnt_8();
		NullCheck(L_17);
		L_17->set_WindCnt_8(L_19);
		TEdge_t3950806139 * L_20 = V_1;
		TEdge_t3950806139 * L_21 = V_0;
		NullCheck(L_21);
		int32_t L_22 = L_21->get_WindCnt2_9();
		NullCheck(L_20);
		L_20->set_WindCnt2_9(L_22);
		TEdge_t3950806139 * L_23 = V_0;
		bool L_24 = Clipper_IsContributing_m3580620193(__this, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00a3;
		}
	}
	{
		TEdge_t3950806139 * L_25 = V_0;
		TEdge_t3950806139 * L_26 = V_1;
		TEdge_t3950806139 * L_27 = V_0;
		NullCheck(L_27);
		IntPoint_t3326126179  L_28 = L_27->get_Bot_0();
		OutPt_t3947633180 * L_29 = Clipper_AddLocalMinPoly_m2497180132(__this, L_25, L_26, L_28, /*hidden argument*/NULL);
		V_2 = L_29;
	}

IL_00a3:
	{
		TEdge_t3950806139 * L_30 = V_0;
		NullCheck(L_30);
		IntPoint_t3326126179 * L_31 = L_30->get_address_of_Top_2();
		int64_t L_32 = L_31->get_Y_1();
		Clipper_InsertScanbeam_m2447896813(__this, L_32, /*hidden argument*/NULL);
	}

IL_00b4:
	{
		TEdge_t3950806139 * L_33 = V_1;
		bool L_34 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_00cb;
		}
	}
	{
		TEdge_t3950806139 * L_35 = V_1;
		Clipper_AddEdgeToSEL_m213197330(__this, L_35, /*hidden argument*/NULL);
		goto IL_00dc;
	}

IL_00cb:
	{
		TEdge_t3950806139 * L_36 = V_1;
		NullCheck(L_36);
		IntPoint_t3326126179 * L_37 = L_36->get_address_of_Top_2();
		int64_t L_38 = L_37->get_Y_1();
		Clipper_InsertScanbeam_m2447896813(__this, L_38, /*hidden argument*/NULL);
	}

IL_00dc:
	{
		TEdge_t3950806139 * L_39 = V_0;
		if (L_39)
		{
			goto IL_00e7;
		}
	}
	{
		goto IL_02cd;
	}

IL_00e7:
	{
		OutPt_t3947633180 * L_40 = V_2;
		if (!L_40)
		{
			goto IL_017d;
		}
	}
	{
		TEdge_t3950806139 * L_41 = V_1;
		bool L_42 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_017d;
		}
	}
	{
		List_1_t1043336060 * L_43 = __this->get_m_GhostJoins_25();
		NullCheck(L_43);
		int32_t L_44 = List_1_get_Count_m896344279(L_43, /*hidden argument*/List_1_get_Count_m896344279_MethodInfo_var);
		if ((((int32_t)L_44) <= ((int32_t)0)))
		{
			goto IL_017d;
		}
	}
	{
		TEdge_t3950806139 * L_45 = V_1;
		NullCheck(L_45);
		int32_t L_46 = L_45->get_WindDelta_7();
		if (!L_46)
		{
			goto IL_017d;
		}
	}
	{
		V_3 = 0;
		goto IL_016c;
	}

IL_011b:
	{
		List_1_t1043336060 * L_47 = __this->get_m_GhostJoins_25();
		int32_t L_48 = V_3;
		NullCheck(L_47);
		Join_t3970117804 * L_49 = List_1_get_Item_m2818082526(L_47, L_48, /*hidden argument*/List_1_get_Item_m2818082526_MethodInfo_var);
		V_4 = L_49;
		Join_t3970117804 * L_50 = V_4;
		NullCheck(L_50);
		OutPt_t3947633180 * L_51 = L_50->get_OutPt1_0();
		NullCheck(L_51);
		IntPoint_t3326126179  L_52 = L_51->get_Pt_1();
		Join_t3970117804 * L_53 = V_4;
		NullCheck(L_53);
		IntPoint_t3326126179  L_54 = L_53->get_OffPt_2();
		TEdge_t3950806139 * L_55 = V_1;
		NullCheck(L_55);
		IntPoint_t3326126179  L_56 = L_55->get_Bot_0();
		TEdge_t3950806139 * L_57 = V_1;
		NullCheck(L_57);
		IntPoint_t3326126179  L_58 = L_57->get_Top_2();
		bool L_59 = Clipper_HorzSegmentsOverlap_m2752565594(__this, L_52, L_54, L_56, L_58, /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_0168;
		}
	}
	{
		Join_t3970117804 * L_60 = V_4;
		NullCheck(L_60);
		OutPt_t3947633180 * L_61 = L_60->get_OutPt1_0();
		OutPt_t3947633180 * L_62 = V_2;
		Join_t3970117804 * L_63 = V_4;
		NullCheck(L_63);
		IntPoint_t3326126179  L_64 = L_63->get_OffPt_2();
		Clipper_AddJoin_m2260685358(__this, L_61, L_62, L_64, /*hidden argument*/NULL);
	}

IL_0168:
	{
		int32_t L_65 = V_3;
		V_3 = ((int32_t)((int32_t)L_65+(int32_t)1));
	}

IL_016c:
	{
		int32_t L_66 = V_3;
		List_1_t1043336060 * L_67 = __this->get_m_GhostJoins_25();
		NullCheck(L_67);
		int32_t L_68 = List_1_get_Count_m896344279(L_67, /*hidden argument*/List_1_get_Count_m896344279_MethodInfo_var);
		if ((((int32_t)L_66) < ((int32_t)L_68)))
		{
			goto IL_011b;
		}
	}

IL_017d:
	{
		TEdge_t3950806139 * L_69 = V_0;
		NullCheck(L_69);
		int32_t L_70 = L_69->get_OutIdx_10();
		if ((((int32_t)L_70) < ((int32_t)0)))
		{
			goto IL_021a;
		}
	}
	{
		TEdge_t3950806139 * L_71 = V_0;
		NullCheck(L_71);
		TEdge_t3950806139 * L_72 = L_71->get_PrevInAEL_15();
		if (!L_72)
		{
			goto IL_021a;
		}
	}
	{
		TEdge_t3950806139 * L_73 = V_0;
		NullCheck(L_73);
		TEdge_t3950806139 * L_74 = L_73->get_PrevInAEL_15();
		NullCheck(L_74);
		IntPoint_t3326126179 * L_75 = L_74->get_address_of_Curr_1();
		int64_t L_76 = L_75->get_X_0();
		TEdge_t3950806139 * L_77 = V_0;
		NullCheck(L_77);
		IntPoint_t3326126179 * L_78 = L_77->get_address_of_Bot_0();
		int64_t L_79 = L_78->get_X_0();
		if ((!(((uint64_t)L_76) == ((uint64_t)L_79))))
		{
			goto IL_021a;
		}
	}
	{
		TEdge_t3950806139 * L_80 = V_0;
		NullCheck(L_80);
		TEdge_t3950806139 * L_81 = L_80->get_PrevInAEL_15();
		NullCheck(L_81);
		int32_t L_82 = L_81->get_OutIdx_10();
		if ((((int32_t)L_82) < ((int32_t)0)))
		{
			goto IL_021a;
		}
	}
	{
		TEdge_t3950806139 * L_83 = V_0;
		NullCheck(L_83);
		TEdge_t3950806139 * L_84 = L_83->get_PrevInAEL_15();
		TEdge_t3950806139 * L_85 = V_0;
		bool L_86 = ((ClipperBase_t512275816 *)__this)->get_m_UseFullRange_9();
		bool L_87 = ClipperBase_SlopesEqual_m3887656866(NULL /*static, unused*/, L_84, L_85, L_86, /*hidden argument*/NULL);
		if (!L_87)
		{
			goto IL_021a;
		}
	}
	{
		TEdge_t3950806139 * L_88 = V_0;
		NullCheck(L_88);
		int32_t L_89 = L_88->get_WindDelta_7();
		if (!L_89)
		{
			goto IL_021a;
		}
	}
	{
		TEdge_t3950806139 * L_90 = V_0;
		NullCheck(L_90);
		TEdge_t3950806139 * L_91 = L_90->get_PrevInAEL_15();
		NullCheck(L_91);
		int32_t L_92 = L_91->get_WindDelta_7();
		if (!L_92)
		{
			goto IL_021a;
		}
	}
	{
		TEdge_t3950806139 * L_93 = V_0;
		NullCheck(L_93);
		TEdge_t3950806139 * L_94 = L_93->get_PrevInAEL_15();
		TEdge_t3950806139 * L_95 = V_0;
		NullCheck(L_95);
		IntPoint_t3326126179  L_96 = L_95->get_Bot_0();
		OutPt_t3947633180 * L_97 = Clipper_AddOutPt_m3632434112(__this, L_94, L_96, /*hidden argument*/NULL);
		V_5 = L_97;
		OutPt_t3947633180 * L_98 = V_2;
		OutPt_t3947633180 * L_99 = V_5;
		TEdge_t3950806139 * L_100 = V_0;
		NullCheck(L_100);
		IntPoint_t3326126179  L_101 = L_100->get_Top_2();
		Clipper_AddJoin_m2260685358(__this, L_98, L_99, L_101, /*hidden argument*/NULL);
	}

IL_021a:
	{
		TEdge_t3950806139 * L_102 = V_0;
		NullCheck(L_102);
		TEdge_t3950806139 * L_103 = L_102->get_NextInAEL_14();
		TEdge_t3950806139 * L_104 = V_1;
		if ((((Il2CppObject*)(TEdge_t3950806139 *)L_103) == ((Il2CppObject*)(TEdge_t3950806139 *)L_104)))
		{
			goto IL_02cd;
		}
	}
	{
		TEdge_t3950806139 * L_105 = V_1;
		NullCheck(L_105);
		int32_t L_106 = L_105->get_OutIdx_10();
		if ((((int32_t)L_106) < ((int32_t)0)))
		{
			goto IL_0298;
		}
	}
	{
		TEdge_t3950806139 * L_107 = V_1;
		NullCheck(L_107);
		TEdge_t3950806139 * L_108 = L_107->get_PrevInAEL_15();
		NullCheck(L_108);
		int32_t L_109 = L_108->get_OutIdx_10();
		if ((((int32_t)L_109) < ((int32_t)0)))
		{
			goto IL_0298;
		}
	}
	{
		TEdge_t3950806139 * L_110 = V_1;
		NullCheck(L_110);
		TEdge_t3950806139 * L_111 = L_110->get_PrevInAEL_15();
		TEdge_t3950806139 * L_112 = V_1;
		bool L_113 = ((ClipperBase_t512275816 *)__this)->get_m_UseFullRange_9();
		bool L_114 = ClipperBase_SlopesEqual_m3887656866(NULL /*static, unused*/, L_111, L_112, L_113, /*hidden argument*/NULL);
		if (!L_114)
		{
			goto IL_0298;
		}
	}
	{
		TEdge_t3950806139 * L_115 = V_1;
		NullCheck(L_115);
		int32_t L_116 = L_115->get_WindDelta_7();
		if (!L_116)
		{
			goto IL_0298;
		}
	}
	{
		TEdge_t3950806139 * L_117 = V_1;
		NullCheck(L_117);
		TEdge_t3950806139 * L_118 = L_117->get_PrevInAEL_15();
		NullCheck(L_118);
		int32_t L_119 = L_118->get_WindDelta_7();
		if (!L_119)
		{
			goto IL_0298;
		}
	}
	{
		TEdge_t3950806139 * L_120 = V_1;
		NullCheck(L_120);
		TEdge_t3950806139 * L_121 = L_120->get_PrevInAEL_15();
		TEdge_t3950806139 * L_122 = V_1;
		NullCheck(L_122);
		IntPoint_t3326126179  L_123 = L_122->get_Bot_0();
		OutPt_t3947633180 * L_124 = Clipper_AddOutPt_m3632434112(__this, L_121, L_123, /*hidden argument*/NULL);
		V_6 = L_124;
		OutPt_t3947633180 * L_125 = V_2;
		OutPt_t3947633180 * L_126 = V_6;
		TEdge_t3950806139 * L_127 = V_1;
		NullCheck(L_127);
		IntPoint_t3326126179  L_128 = L_127->get_Top_2();
		Clipper_AddJoin_m2260685358(__this, L_125, L_126, L_128, /*hidden argument*/NULL);
	}

IL_0298:
	{
		TEdge_t3950806139 * L_129 = V_0;
		NullCheck(L_129);
		TEdge_t3950806139 * L_130 = L_129->get_NextInAEL_14();
		V_7 = L_130;
		TEdge_t3950806139 * L_131 = V_7;
		if (!L_131)
		{
			goto IL_02cd;
		}
	}
	{
		goto IL_02c5;
	}

IL_02ac:
	{
		TEdge_t3950806139 * L_132 = V_1;
		TEdge_t3950806139 * L_133 = V_7;
		TEdge_t3950806139 * L_134 = V_0;
		NullCheck(L_134);
		IntPoint_t3326126179  L_135 = L_134->get_Curr_1();
		Clipper_IntersectEdges_m1787610885(__this, L_132, L_133, L_135, (bool)0, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_136 = V_7;
		NullCheck(L_136);
		TEdge_t3950806139 * L_137 = L_136->get_NextInAEL_14();
		V_7 = L_137;
	}

IL_02c5:
	{
		TEdge_t3950806139 * L_138 = V_7;
		TEdge_t3950806139 * L_139 = V_1;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_138) == ((Il2CppObject*)(TEdge_t3950806139 *)L_139))))
		{
			goto IL_02ac;
		}
	}

IL_02cd:
	{
		LocalMinima_t2863342752 * L_140 = ((ClipperBase_t512275816 *)__this)->get_m_CurrentLM_7();
		if (!L_140)
		{
			goto IL_02e9;
		}
	}
	{
		LocalMinima_t2863342752 * L_141 = ((ClipperBase_t512275816 *)__this)->get_m_CurrentLM_7();
		NullCheck(L_141);
		int64_t L_142 = L_141->get_Y_0();
		int64_t L_143 = ___botY0;
		if ((((int64_t)L_142) == ((int64_t)L_143)))
		{
			goto IL_0005;
		}
	}

IL_02e9:
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::InsertEdgeIntoAEL(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge)
extern "C"  void Clipper_InsertEdgeIntoAEL_m1577452744 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___edge0, TEdge_t3950806139 * ___startEdge1, const MethodInfo* method)
{
	{
		TEdge_t3950806139 * L_0 = __this->get_m_ActiveEdges_18();
		if (L_0)
		{
			goto IL_0025;
		}
	}
	{
		TEdge_t3950806139 * L_1 = ___edge0;
		NullCheck(L_1);
		L_1->set_PrevInAEL_15((TEdge_t3950806139 *)NULL);
		TEdge_t3950806139 * L_2 = ___edge0;
		NullCheck(L_2);
		L_2->set_NextInAEL_14((TEdge_t3950806139 *)NULL);
		TEdge_t3950806139 * L_3 = ___edge0;
		__this->set_m_ActiveEdges_18(L_3);
		goto IL_00d1;
	}

IL_0025:
	{
		TEdge_t3950806139 * L_4 = ___startEdge1;
		if (L_4)
		{
			goto IL_0068;
		}
	}
	{
		TEdge_t3950806139 * L_5 = __this->get_m_ActiveEdges_18();
		TEdge_t3950806139 * L_6 = ___edge0;
		bool L_7 = Clipper_E2InsertsBeforeE1_m3305521566(__this, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0068;
		}
	}
	{
		TEdge_t3950806139 * L_8 = ___edge0;
		NullCheck(L_8);
		L_8->set_PrevInAEL_15((TEdge_t3950806139 *)NULL);
		TEdge_t3950806139 * L_9 = ___edge0;
		TEdge_t3950806139 * L_10 = __this->get_m_ActiveEdges_18();
		NullCheck(L_9);
		L_9->set_NextInAEL_14(L_10);
		TEdge_t3950806139 * L_11 = __this->get_m_ActiveEdges_18();
		TEdge_t3950806139 * L_12 = ___edge0;
		NullCheck(L_11);
		L_11->set_PrevInAEL_15(L_12);
		TEdge_t3950806139 * L_13 = ___edge0;
		__this->set_m_ActiveEdges_18(L_13);
		goto IL_00d1;
	}

IL_0068:
	{
		TEdge_t3950806139 * L_14 = ___startEdge1;
		if (L_14)
		{
			goto IL_0076;
		}
	}
	{
		TEdge_t3950806139 * L_15 = __this->get_m_ActiveEdges_18();
		___startEdge1 = L_15;
	}

IL_0076:
	{
		goto IL_0083;
	}

IL_007b:
	{
		TEdge_t3950806139 * L_16 = ___startEdge1;
		NullCheck(L_16);
		TEdge_t3950806139 * L_17 = L_16->get_NextInAEL_14();
		___startEdge1 = L_17;
	}

IL_0083:
	{
		TEdge_t3950806139 * L_18 = ___startEdge1;
		NullCheck(L_18);
		TEdge_t3950806139 * L_19 = L_18->get_NextInAEL_14();
		if (!L_19)
		{
			goto IL_00a0;
		}
	}
	{
		TEdge_t3950806139 * L_20 = ___startEdge1;
		NullCheck(L_20);
		TEdge_t3950806139 * L_21 = L_20->get_NextInAEL_14();
		TEdge_t3950806139 * L_22 = ___edge0;
		bool L_23 = Clipper_E2InsertsBeforeE1_m3305521566(__this, L_21, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_007b;
		}
	}

IL_00a0:
	{
		TEdge_t3950806139 * L_24 = ___edge0;
		TEdge_t3950806139 * L_25 = ___startEdge1;
		NullCheck(L_25);
		TEdge_t3950806139 * L_26 = L_25->get_NextInAEL_14();
		NullCheck(L_24);
		L_24->set_NextInAEL_14(L_26);
		TEdge_t3950806139 * L_27 = ___startEdge1;
		NullCheck(L_27);
		TEdge_t3950806139 * L_28 = L_27->get_NextInAEL_14();
		if (!L_28)
		{
			goto IL_00c3;
		}
	}
	{
		TEdge_t3950806139 * L_29 = ___startEdge1;
		NullCheck(L_29);
		TEdge_t3950806139 * L_30 = L_29->get_NextInAEL_14();
		TEdge_t3950806139 * L_31 = ___edge0;
		NullCheck(L_30);
		L_30->set_PrevInAEL_15(L_31);
	}

IL_00c3:
	{
		TEdge_t3950806139 * L_32 = ___edge0;
		TEdge_t3950806139 * L_33 = ___startEdge1;
		NullCheck(L_32);
		L_32->set_PrevInAEL_15(L_33);
		TEdge_t3950806139 * L_34 = ___startEdge1;
		TEdge_t3950806139 * L_35 = ___edge0;
		NullCheck(L_34);
		L_34->set_NextInAEL_14(L_35);
	}

IL_00d1:
	{
		return;
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::E2InsertsBeforeE1(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge)
extern "C"  bool Clipper_E2InsertsBeforeE1_m3305521566 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e10, TEdge_t3950806139 * ___e21, const MethodInfo* method)
{
	{
		TEdge_t3950806139 * L_0 = ___e21;
		NullCheck(L_0);
		IntPoint_t3326126179 * L_1 = L_0->get_address_of_Curr_1();
		int64_t L_2 = L_1->get_X_0();
		TEdge_t3950806139 * L_3 = ___e10;
		NullCheck(L_3);
		IntPoint_t3326126179 * L_4 = L_3->get_address_of_Curr_1();
		int64_t L_5 = L_4->get_X_0();
		if ((!(((uint64_t)L_2) == ((uint64_t)L_5))))
		{
			goto IL_0074;
		}
	}
	{
		TEdge_t3950806139 * L_6 = ___e21;
		NullCheck(L_6);
		IntPoint_t3326126179 * L_7 = L_6->get_address_of_Top_2();
		int64_t L_8 = L_7->get_Y_1();
		TEdge_t3950806139 * L_9 = ___e10;
		NullCheck(L_9);
		IntPoint_t3326126179 * L_10 = L_9->get_address_of_Top_2();
		int64_t L_11 = L_10->get_Y_1();
		if ((((int64_t)L_8) <= ((int64_t)L_11)))
		{
			goto IL_0055;
		}
	}
	{
		TEdge_t3950806139 * L_12 = ___e21;
		NullCheck(L_12);
		IntPoint_t3326126179 * L_13 = L_12->get_address_of_Top_2();
		int64_t L_14 = L_13->get_X_0();
		TEdge_t3950806139 * L_15 = ___e10;
		TEdge_t3950806139 * L_16 = ___e21;
		NullCheck(L_16);
		IntPoint_t3326126179 * L_17 = L_16->get_address_of_Top_2();
		int64_t L_18 = L_17->get_Y_1();
		int64_t L_19 = Clipper_TopX_m1760470797(NULL /*static, unused*/, L_15, L_18, /*hidden argument*/NULL);
		return (bool)((((int64_t)L_14) < ((int64_t)L_19))? 1 : 0);
	}

IL_0055:
	{
		TEdge_t3950806139 * L_20 = ___e10;
		NullCheck(L_20);
		IntPoint_t3326126179 * L_21 = L_20->get_address_of_Top_2();
		int64_t L_22 = L_21->get_X_0();
		TEdge_t3950806139 * L_23 = ___e21;
		TEdge_t3950806139 * L_24 = ___e10;
		NullCheck(L_24);
		IntPoint_t3326126179 * L_25 = L_24->get_address_of_Top_2();
		int64_t L_26 = L_25->get_Y_1();
		int64_t L_27 = Clipper_TopX_m1760470797(NULL /*static, unused*/, L_23, L_26, /*hidden argument*/NULL);
		return (bool)((((int64_t)L_22) > ((int64_t)L_27))? 1 : 0);
	}

IL_0074:
	{
		TEdge_t3950806139 * L_28 = ___e21;
		NullCheck(L_28);
		IntPoint_t3326126179 * L_29 = L_28->get_address_of_Curr_1();
		int64_t L_30 = L_29->get_X_0();
		TEdge_t3950806139 * L_31 = ___e10;
		NullCheck(L_31);
		IntPoint_t3326126179 * L_32 = L_31->get_address_of_Curr_1();
		int64_t L_33 = L_32->get_X_0();
		return (bool)((((int64_t)L_30) < ((int64_t)L_33))? 1 : 0);
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::IsEvenOddFillType(Pathfinding.ClipperLib.TEdge)
extern "C"  bool Clipper_IsEvenOddFillType_m201048083 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___edge0, const MethodInfo* method)
{
	{
		TEdge_t3950806139 * L_0 = ___edge0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_PolyTyp_5();
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_2 = __this->get_m_SubjFillType_23();
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_3 = __this->get_m_ClipFillType_22();
		return (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::IsEvenOddAltFillType(Pathfinding.ClipperLib.TEdge)
extern "C"  bool Clipper_IsEvenOddAltFillType_m1541475944 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___edge0, const MethodInfo* method)
{
	{
		TEdge_t3950806139 * L_0 = ___edge0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_PolyTyp_5();
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_2 = __this->get_m_ClipFillType_22();
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_3 = __this->get_m_SubjFillType_23();
		return (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::IsContributing(Pathfinding.ClipperLib.TEdge)
extern "C"  bool Clipper_IsContributing_m3580620193 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___edge0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		TEdge_t3950806139 * L_0 = ___edge0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_PolyTyp_5();
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_2 = __this->get_m_SubjFillType_23();
		V_0 = L_2;
		int32_t L_3 = __this->get_m_ClipFillType_22();
		V_1 = L_3;
		goto IL_002c;
	}

IL_001e:
	{
		int32_t L_4 = __this->get_m_ClipFillType_22();
		V_0 = L_4;
		int32_t L_5 = __this->get_m_SubjFillType_23();
		V_1 = L_5;
	}

IL_002c:
	{
		int32_t L_6 = V_0;
		V_2 = L_6;
		int32_t L_7 = V_2;
		if (L_7 == 0)
		{
			goto IL_0045;
		}
		if (L_7 == 1)
		{
			goto IL_0063;
		}
		if (L_7 == 2)
		{
			goto IL_007b;
		}
	}
	{
		goto IL_008e;
	}

IL_0045:
	{
		TEdge_t3950806139 * L_8 = ___edge0;
		NullCheck(L_8);
		int32_t L_9 = L_8->get_WindDelta_7();
		if (L_9)
		{
			goto IL_005e;
		}
	}
	{
		TEdge_t3950806139 * L_10 = ___edge0;
		NullCheck(L_10);
		int32_t L_11 = L_10->get_WindCnt_8();
		if ((((int32_t)L_11) == ((int32_t)1)))
		{
			goto IL_005e;
		}
	}
	{
		return (bool)0;
	}

IL_005e:
	{
		goto IL_00a1;
	}

IL_0063:
	{
		TEdge_t3950806139 * L_12 = ___edge0;
		NullCheck(L_12);
		int32_t L_13 = L_12->get_WindCnt_8();
		int32_t L_14 = abs(L_13);
		if ((((int32_t)L_14) == ((int32_t)1)))
		{
			goto IL_0076;
		}
	}
	{
		return (bool)0;
	}

IL_0076:
	{
		goto IL_00a1;
	}

IL_007b:
	{
		TEdge_t3950806139 * L_15 = ___edge0;
		NullCheck(L_15);
		int32_t L_16 = L_15->get_WindCnt_8();
		if ((((int32_t)L_16) == ((int32_t)1)))
		{
			goto IL_0089;
		}
	}
	{
		return (bool)0;
	}

IL_0089:
	{
		goto IL_00a1;
	}

IL_008e:
	{
		TEdge_t3950806139 * L_17 = ___edge0;
		NullCheck(L_17);
		int32_t L_18 = L_17->get_WindCnt_8();
		if ((((int32_t)L_18) == ((int32_t)(-1))))
		{
			goto IL_009c;
		}
	}
	{
		return (bool)0;
	}

IL_009c:
	{
		goto IL_00a1;
	}

IL_00a1:
	{
		int32_t L_19 = __this->get_m_ClipType_16();
		V_3 = L_19;
		int32_t L_20 = V_3;
		if (L_20 == 0)
		{
			goto IL_00c3;
		}
		if (L_20 == 1)
		{
			goto IL_00fd;
		}
		if (L_20 == 2)
		{
			goto IL_013a;
		}
		if (L_20 == 3)
		{
			goto IL_01bc;
		}
	}
	{
		goto IL_0206;
	}

IL_00c3:
	{
		int32_t L_21 = V_1;
		V_2 = L_21;
		int32_t L_22 = V_2;
		if (L_22 == 0)
		{
			goto IL_00dc;
		}
		if (L_22 == 1)
		{
			goto IL_00dc;
		}
		if (L_22 == 2)
		{
			goto IL_00e9;
		}
	}
	{
		goto IL_00f3;
	}

IL_00dc:
	{
		TEdge_t3950806139 * L_23 = ___edge0;
		NullCheck(L_23);
		int32_t L_24 = L_23->get_WindCnt2_9();
		return (bool)((((int32_t)((((int32_t)L_24) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_00e9:
	{
		TEdge_t3950806139 * L_25 = ___edge0;
		NullCheck(L_25);
		int32_t L_26 = L_25->get_WindCnt2_9();
		return (bool)((((int32_t)L_26) > ((int32_t)0))? 1 : 0);
	}

IL_00f3:
	{
		TEdge_t3950806139 * L_27 = ___edge0;
		NullCheck(L_27);
		int32_t L_28 = L_27->get_WindCnt2_9();
		return (bool)((((int32_t)L_28) < ((int32_t)0))? 1 : 0);
	}

IL_00fd:
	{
		int32_t L_29 = V_1;
		V_2 = L_29;
		int32_t L_30 = V_2;
		if (L_30 == 0)
		{
			goto IL_0116;
		}
		if (L_30 == 1)
		{
			goto IL_0116;
		}
		if (L_30 == 2)
		{
			goto IL_0120;
		}
	}
	{
		goto IL_012d;
	}

IL_0116:
	{
		TEdge_t3950806139 * L_31 = ___edge0;
		NullCheck(L_31);
		int32_t L_32 = L_31->get_WindCnt2_9();
		return (bool)((((int32_t)L_32) == ((int32_t)0))? 1 : 0);
	}

IL_0120:
	{
		TEdge_t3950806139 * L_33 = ___edge0;
		NullCheck(L_33);
		int32_t L_34 = L_33->get_WindCnt2_9();
		return (bool)((((int32_t)((((int32_t)L_34) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_012d:
	{
		TEdge_t3950806139 * L_35 = ___edge0;
		NullCheck(L_35);
		int32_t L_36 = L_35->get_WindCnt2_9();
		return (bool)((((int32_t)((((int32_t)L_36) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_013a:
	{
		TEdge_t3950806139 * L_37 = ___edge0;
		NullCheck(L_37);
		int32_t L_38 = L_37->get_PolyTyp_5();
		if (L_38)
		{
			goto IL_0182;
		}
	}
	{
		int32_t L_39 = V_1;
		V_2 = L_39;
		int32_t L_40 = V_2;
		if (L_40 == 0)
		{
			goto IL_015e;
		}
		if (L_40 == 1)
		{
			goto IL_015e;
		}
		if (L_40 == 2)
		{
			goto IL_0168;
		}
	}
	{
		goto IL_0175;
	}

IL_015e:
	{
		TEdge_t3950806139 * L_41 = ___edge0;
		NullCheck(L_41);
		int32_t L_42 = L_41->get_WindCnt2_9();
		return (bool)((((int32_t)L_42) == ((int32_t)0))? 1 : 0);
	}

IL_0168:
	{
		TEdge_t3950806139 * L_43 = ___edge0;
		NullCheck(L_43);
		int32_t L_44 = L_43->get_WindCnt2_9();
		return (bool)((((int32_t)((((int32_t)L_44) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_0175:
	{
		TEdge_t3950806139 * L_45 = ___edge0;
		NullCheck(L_45);
		int32_t L_46 = L_45->get_WindCnt2_9();
		return (bool)((((int32_t)((((int32_t)L_46) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_0182:
	{
		int32_t L_47 = V_1;
		V_2 = L_47;
		int32_t L_48 = V_2;
		if (L_48 == 0)
		{
			goto IL_019b;
		}
		if (L_48 == 1)
		{
			goto IL_019b;
		}
		if (L_48 == 2)
		{
			goto IL_01a8;
		}
	}
	{
		goto IL_01b2;
	}

IL_019b:
	{
		TEdge_t3950806139 * L_49 = ___edge0;
		NullCheck(L_49);
		int32_t L_50 = L_49->get_WindCnt2_9();
		return (bool)((((int32_t)((((int32_t)L_50) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_01a8:
	{
		TEdge_t3950806139 * L_51 = ___edge0;
		NullCheck(L_51);
		int32_t L_52 = L_51->get_WindCnt2_9();
		return (bool)((((int32_t)L_52) > ((int32_t)0))? 1 : 0);
	}

IL_01b2:
	{
		TEdge_t3950806139 * L_53 = ___edge0;
		NullCheck(L_53);
		int32_t L_54 = L_53->get_WindCnt2_9();
		return (bool)((((int32_t)L_54) < ((int32_t)0))? 1 : 0);
	}

IL_01bc:
	{
		TEdge_t3950806139 * L_55 = ___edge0;
		NullCheck(L_55);
		int32_t L_56 = L_55->get_WindDelta_7();
		if (L_56)
		{
			goto IL_0204;
		}
	}
	{
		int32_t L_57 = V_1;
		V_2 = L_57;
		int32_t L_58 = V_2;
		if (L_58 == 0)
		{
			goto IL_01e0;
		}
		if (L_58 == 1)
		{
			goto IL_01e0;
		}
		if (L_58 == 2)
		{
			goto IL_01ea;
		}
	}
	{
		goto IL_01f7;
	}

IL_01e0:
	{
		TEdge_t3950806139 * L_59 = ___edge0;
		NullCheck(L_59);
		int32_t L_60 = L_59->get_WindCnt2_9();
		return (bool)((((int32_t)L_60) == ((int32_t)0))? 1 : 0);
	}

IL_01ea:
	{
		TEdge_t3950806139 * L_61 = ___edge0;
		NullCheck(L_61);
		int32_t L_62 = L_61->get_WindCnt2_9();
		return (bool)((((int32_t)((((int32_t)L_62) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_01f7:
	{
		TEdge_t3950806139 * L_63 = ___edge0;
		NullCheck(L_63);
		int32_t L_64 = L_63->get_WindCnt2_9();
		return (bool)((((int32_t)((((int32_t)L_64) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_0204:
	{
		return (bool)1;
	}

IL_0206:
	{
		return (bool)1;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::SetWindingCount(Pathfinding.ClipperLib.TEdge)
extern "C"  void Clipper_SetWindingCount_m167856286 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___edge0, const MethodInfo* method)
{
	TEdge_t3950806139 * V_0 = NULL;
	bool V_1 = false;
	TEdge_t3950806139 * V_2 = NULL;
	TEdge_t3950806139 * G_B8_0 = NULL;
	TEdge_t3950806139 * G_B7_0 = NULL;
	int32_t G_B9_0 = 0;
	TEdge_t3950806139 * G_B9_1 = NULL;
	TEdge_t3950806139 * G_B23_0 = NULL;
	TEdge_t3950806139 * G_B22_0 = NULL;
	int32_t G_B24_0 = 0;
	TEdge_t3950806139 * G_B24_1 = NULL;
	TEdge_t3950806139 * G_B35_0 = NULL;
	TEdge_t3950806139 * G_B34_0 = NULL;
	int32_t G_B36_0 = 0;
	TEdge_t3950806139 * G_B36_1 = NULL;
	TEdge_t3950806139 * G_B41_0 = NULL;
	TEdge_t3950806139 * G_B40_0 = NULL;
	int32_t G_B42_0 = 0;
	TEdge_t3950806139 * G_B42_1 = NULL;
	TEdge_t3950806139 * G_B52_0 = NULL;
	TEdge_t3950806139 * G_B51_0 = NULL;
	int32_t G_B53_0 = 0;
	TEdge_t3950806139 * G_B53_1 = NULL;
	{
		TEdge_t3950806139 * L_0 = ___edge0;
		NullCheck(L_0);
		TEdge_t3950806139 * L_1 = L_0->get_PrevInAEL_15();
		V_0 = L_1;
		goto IL_0013;
	}

IL_000c:
	{
		TEdge_t3950806139 * L_2 = V_0;
		NullCheck(L_2);
		TEdge_t3950806139 * L_3 = L_2->get_PrevInAEL_15();
		V_0 = L_3;
	}

IL_0013:
	{
		TEdge_t3950806139 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_0035;
		}
	}
	{
		TEdge_t3950806139 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_PolyTyp_5();
		TEdge_t3950806139 * L_7 = ___edge0;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_PolyTyp_5();
		if ((!(((uint32_t)L_6) == ((uint32_t)L_8))))
		{
			goto IL_000c;
		}
	}
	{
		TEdge_t3950806139 * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = L_9->get_WindDelta_7();
		if (!L_10)
		{
			goto IL_000c;
		}
	}

IL_0035:
	{
		TEdge_t3950806139 * L_11 = V_0;
		if (L_11)
		{
			goto IL_006b;
		}
	}
	{
		TEdge_t3950806139 * L_12 = ___edge0;
		TEdge_t3950806139 * L_13 = ___edge0;
		NullCheck(L_13);
		int32_t L_14 = L_13->get_WindDelta_7();
		G_B7_0 = L_12;
		if (L_14)
		{
			G_B8_0 = L_12;
			goto IL_004d;
		}
	}
	{
		G_B9_0 = 1;
		G_B9_1 = G_B7_0;
		goto IL_0053;
	}

IL_004d:
	{
		TEdge_t3950806139 * L_15 = ___edge0;
		NullCheck(L_15);
		int32_t L_16 = L_15->get_WindDelta_7();
		G_B9_0 = L_16;
		G_B9_1 = G_B8_0;
	}

IL_0053:
	{
		NullCheck(G_B9_1);
		G_B9_1->set_WindCnt_8(G_B9_0);
		TEdge_t3950806139 * L_17 = ___edge0;
		NullCheck(L_17);
		L_17->set_WindCnt2_9(0);
		TEdge_t3950806139 * L_18 = __this->get_m_ActiveEdges_18();
		V_0 = L_18;
		goto IL_0233;
	}

IL_006b:
	{
		TEdge_t3950806139 * L_19 = ___edge0;
		NullCheck(L_19);
		int32_t L_20 = L_19->get_WindDelta_7();
		if (L_20)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_21 = __this->get_m_ClipType_16();
		if ((((int32_t)L_21) == ((int32_t)1)))
		{
			goto IL_00a1;
		}
	}
	{
		TEdge_t3950806139 * L_22 = ___edge0;
		NullCheck(L_22);
		L_22->set_WindCnt_8(1);
		TEdge_t3950806139 * L_23 = ___edge0;
		TEdge_t3950806139 * L_24 = V_0;
		NullCheck(L_24);
		int32_t L_25 = L_24->get_WindCnt2_9();
		NullCheck(L_23);
		L_23->set_WindCnt2_9(L_25);
		TEdge_t3950806139 * L_26 = V_0;
		NullCheck(L_26);
		TEdge_t3950806139 * L_27 = L_26->get_NextInAEL_14();
		V_0 = L_27;
		goto IL_0233;
	}

IL_00a1:
	{
		TEdge_t3950806139 * L_28 = ___edge0;
		bool L_29 = Clipper_IsEvenOddFillType_m201048083(__this, L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_0130;
		}
	}
	{
		TEdge_t3950806139 * L_30 = ___edge0;
		NullCheck(L_30);
		int32_t L_31 = L_30->get_WindDelta_7();
		if (L_31)
		{
			goto IL_010c;
		}
	}
	{
		V_1 = (bool)1;
		TEdge_t3950806139 * L_32 = V_0;
		NullCheck(L_32);
		TEdge_t3950806139 * L_33 = L_32->get_PrevInAEL_15();
		V_2 = L_33;
		goto IL_00ee;
	}

IL_00c6:
	{
		TEdge_t3950806139 * L_34 = V_2;
		NullCheck(L_34);
		int32_t L_35 = L_34->get_PolyTyp_5();
		TEdge_t3950806139 * L_36 = V_0;
		NullCheck(L_36);
		int32_t L_37 = L_36->get_PolyTyp_5();
		if ((!(((uint32_t)L_35) == ((uint32_t)L_37))))
		{
			goto IL_00e7;
		}
	}
	{
		TEdge_t3950806139 * L_38 = V_2;
		NullCheck(L_38);
		int32_t L_39 = L_38->get_WindDelta_7();
		if (!L_39)
		{
			goto IL_00e7;
		}
	}
	{
		bool L_40 = V_1;
		V_1 = (bool)((((int32_t)L_40) == ((int32_t)0))? 1 : 0);
	}

IL_00e7:
	{
		TEdge_t3950806139 * L_41 = V_2;
		NullCheck(L_41);
		TEdge_t3950806139 * L_42 = L_41->get_PrevInAEL_15();
		V_2 = L_42;
	}

IL_00ee:
	{
		TEdge_t3950806139 * L_43 = V_2;
		if (L_43)
		{
			goto IL_00c6;
		}
	}
	{
		TEdge_t3950806139 * L_44 = ___edge0;
		bool L_45 = V_1;
		G_B22_0 = L_44;
		if (!L_45)
		{
			G_B23_0 = L_44;
			goto IL_0101;
		}
	}
	{
		G_B24_0 = 0;
		G_B24_1 = G_B22_0;
		goto IL_0102;
	}

IL_0101:
	{
		G_B24_0 = 1;
		G_B24_1 = G_B23_0;
	}

IL_0102:
	{
		NullCheck(G_B24_1);
		G_B24_1->set_WindCnt_8(G_B24_0);
		goto IL_0118;
	}

IL_010c:
	{
		TEdge_t3950806139 * L_46 = ___edge0;
		TEdge_t3950806139 * L_47 = ___edge0;
		NullCheck(L_47);
		int32_t L_48 = L_47->get_WindDelta_7();
		NullCheck(L_46);
		L_46->set_WindCnt_8(L_48);
	}

IL_0118:
	{
		TEdge_t3950806139 * L_49 = ___edge0;
		TEdge_t3950806139 * L_50 = V_0;
		NullCheck(L_50);
		int32_t L_51 = L_50->get_WindCnt2_9();
		NullCheck(L_49);
		L_49->set_WindCnt2_9(L_51);
		TEdge_t3950806139 * L_52 = V_0;
		NullCheck(L_52);
		TEdge_t3950806139 * L_53 = L_52->get_NextInAEL_14();
		V_0 = L_53;
		goto IL_0233;
	}

IL_0130:
	{
		TEdge_t3950806139 * L_54 = V_0;
		NullCheck(L_54);
		int32_t L_55 = L_54->get_WindCnt_8();
		TEdge_t3950806139 * L_56 = V_0;
		NullCheck(L_56);
		int32_t L_57 = L_56->get_WindDelta_7();
		if ((((int32_t)((int32_t)((int32_t)L_55*(int32_t)L_57))) >= ((int32_t)0)))
		{
			goto IL_01b2;
		}
	}
	{
		TEdge_t3950806139 * L_58 = V_0;
		NullCheck(L_58);
		int32_t L_59 = L_58->get_WindCnt_8();
		int32_t L_60 = abs(L_59);
		if ((((int32_t)L_60) <= ((int32_t)1)))
		{
			goto IL_0190;
		}
	}
	{
		TEdge_t3950806139 * L_61 = V_0;
		NullCheck(L_61);
		int32_t L_62 = L_61->get_WindDelta_7();
		TEdge_t3950806139 * L_63 = ___edge0;
		NullCheck(L_63);
		int32_t L_64 = L_63->get_WindDelta_7();
		if ((((int32_t)((int32_t)((int32_t)L_62*(int32_t)L_64))) >= ((int32_t)0)))
		{
			goto IL_0178;
		}
	}
	{
		TEdge_t3950806139 * L_65 = ___edge0;
		TEdge_t3950806139 * L_66 = V_0;
		NullCheck(L_66);
		int32_t L_67 = L_66->get_WindCnt_8();
		NullCheck(L_65);
		L_65->set_WindCnt_8(L_67);
		goto IL_018b;
	}

IL_0178:
	{
		TEdge_t3950806139 * L_68 = ___edge0;
		TEdge_t3950806139 * L_69 = V_0;
		NullCheck(L_69);
		int32_t L_70 = L_69->get_WindCnt_8();
		TEdge_t3950806139 * L_71 = ___edge0;
		NullCheck(L_71);
		int32_t L_72 = L_71->get_WindDelta_7();
		NullCheck(L_68);
		L_68->set_WindCnt_8(((int32_t)((int32_t)L_70+(int32_t)L_72)));
	}

IL_018b:
	{
		goto IL_01ad;
	}

IL_0190:
	{
		TEdge_t3950806139 * L_73 = ___edge0;
		TEdge_t3950806139 * L_74 = ___edge0;
		NullCheck(L_74);
		int32_t L_75 = L_74->get_WindDelta_7();
		G_B34_0 = L_73;
		if (L_75)
		{
			G_B35_0 = L_73;
			goto IL_01a2;
		}
	}
	{
		G_B36_0 = 1;
		G_B36_1 = G_B34_0;
		goto IL_01a8;
	}

IL_01a2:
	{
		TEdge_t3950806139 * L_76 = ___edge0;
		NullCheck(L_76);
		int32_t L_77 = L_76->get_WindDelta_7();
		G_B36_0 = L_77;
		G_B36_1 = G_B35_0;
	}

IL_01a8:
	{
		NullCheck(G_B36_1);
		G_B36_1->set_WindCnt_8(G_B36_0);
	}

IL_01ad:
	{
		goto IL_0220;
	}

IL_01b2:
	{
		TEdge_t3950806139 * L_78 = ___edge0;
		NullCheck(L_78);
		int32_t L_79 = L_78->get_WindDelta_7();
		if (L_79)
		{
			goto IL_01e9;
		}
	}
	{
		TEdge_t3950806139 * L_80 = ___edge0;
		TEdge_t3950806139 * L_81 = V_0;
		NullCheck(L_81);
		int32_t L_82 = L_81->get_WindCnt_8();
		G_B40_0 = L_80;
		if ((((int32_t)L_82) >= ((int32_t)0)))
		{
			G_B41_0 = L_80;
			goto IL_01d7;
		}
	}
	{
		TEdge_t3950806139 * L_83 = V_0;
		NullCheck(L_83);
		int32_t L_84 = L_83->get_WindCnt_8();
		G_B42_0 = ((int32_t)((int32_t)L_84-(int32_t)1));
		G_B42_1 = G_B40_0;
		goto IL_01df;
	}

IL_01d7:
	{
		TEdge_t3950806139 * L_85 = V_0;
		NullCheck(L_85);
		int32_t L_86 = L_85->get_WindCnt_8();
		G_B42_0 = ((int32_t)((int32_t)L_86+(int32_t)1));
		G_B42_1 = G_B41_0;
	}

IL_01df:
	{
		NullCheck(G_B42_1);
		G_B42_1->set_WindCnt_8(G_B42_0);
		goto IL_0220;
	}

IL_01e9:
	{
		TEdge_t3950806139 * L_87 = V_0;
		NullCheck(L_87);
		int32_t L_88 = L_87->get_WindDelta_7();
		TEdge_t3950806139 * L_89 = ___edge0;
		NullCheck(L_89);
		int32_t L_90 = L_89->get_WindDelta_7();
		if ((((int32_t)((int32_t)((int32_t)L_88*(int32_t)L_90))) >= ((int32_t)0)))
		{
			goto IL_020d;
		}
	}
	{
		TEdge_t3950806139 * L_91 = ___edge0;
		TEdge_t3950806139 * L_92 = V_0;
		NullCheck(L_92);
		int32_t L_93 = L_92->get_WindCnt_8();
		NullCheck(L_91);
		L_91->set_WindCnt_8(L_93);
		goto IL_0220;
	}

IL_020d:
	{
		TEdge_t3950806139 * L_94 = ___edge0;
		TEdge_t3950806139 * L_95 = V_0;
		NullCheck(L_95);
		int32_t L_96 = L_95->get_WindCnt_8();
		TEdge_t3950806139 * L_97 = ___edge0;
		NullCheck(L_97);
		int32_t L_98 = L_97->get_WindDelta_7();
		NullCheck(L_94);
		L_94->set_WindCnt_8(((int32_t)((int32_t)L_96+(int32_t)L_98)));
	}

IL_0220:
	{
		TEdge_t3950806139 * L_99 = ___edge0;
		TEdge_t3950806139 * L_100 = V_0;
		NullCheck(L_100);
		int32_t L_101 = L_100->get_WindCnt2_9();
		NullCheck(L_99);
		L_99->set_WindCnt2_9(L_101);
		TEdge_t3950806139 * L_102 = V_0;
		NullCheck(L_102);
		TEdge_t3950806139 * L_103 = L_102->get_NextInAEL_14();
		V_0 = L_103;
	}

IL_0233:
	{
		TEdge_t3950806139 * L_104 = ___edge0;
		bool L_105 = Clipper_IsEvenOddAltFillType_m1541475944(__this, L_104, /*hidden argument*/NULL);
		if (!L_105)
		{
			goto IL_027a;
		}
	}
	{
		goto IL_026e;
	}

IL_0244:
	{
		TEdge_t3950806139 * L_106 = V_0;
		NullCheck(L_106);
		int32_t L_107 = L_106->get_WindDelta_7();
		if (!L_107)
		{
			goto IL_0267;
		}
	}
	{
		TEdge_t3950806139 * L_108 = ___edge0;
		TEdge_t3950806139 * L_109 = ___edge0;
		NullCheck(L_109);
		int32_t L_110 = L_109->get_WindCnt2_9();
		G_B51_0 = L_108;
		if (L_110)
		{
			G_B52_0 = L_108;
			goto IL_0261;
		}
	}
	{
		G_B53_0 = 1;
		G_B53_1 = G_B51_0;
		goto IL_0262;
	}

IL_0261:
	{
		G_B53_0 = 0;
		G_B53_1 = G_B52_0;
	}

IL_0262:
	{
		NullCheck(G_B53_1);
		G_B53_1->set_WindCnt2_9(G_B53_0);
	}

IL_0267:
	{
		TEdge_t3950806139 * L_111 = V_0;
		NullCheck(L_111);
		TEdge_t3950806139 * L_112 = L_111->get_NextInAEL_14();
		V_0 = L_112;
	}

IL_026e:
	{
		TEdge_t3950806139 * L_113 = V_0;
		TEdge_t3950806139 * L_114 = ___edge0;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_113) == ((Il2CppObject*)(TEdge_t3950806139 *)L_114))))
		{
			goto IL_0244;
		}
	}
	{
		goto IL_02a0;
	}

IL_027a:
	{
		goto IL_0299;
	}

IL_027f:
	{
		TEdge_t3950806139 * L_115 = ___edge0;
		TEdge_t3950806139 * L_116 = L_115;
		NullCheck(L_116);
		int32_t L_117 = L_116->get_WindCnt2_9();
		TEdge_t3950806139 * L_118 = V_0;
		NullCheck(L_118);
		int32_t L_119 = L_118->get_WindDelta_7();
		NullCheck(L_116);
		L_116->set_WindCnt2_9(((int32_t)((int32_t)L_117+(int32_t)L_119)));
		TEdge_t3950806139 * L_120 = V_0;
		NullCheck(L_120);
		TEdge_t3950806139 * L_121 = L_120->get_NextInAEL_14();
		V_0 = L_121;
	}

IL_0299:
	{
		TEdge_t3950806139 * L_122 = V_0;
		TEdge_t3950806139 * L_123 = ___edge0;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_122) == ((Il2CppObject*)(TEdge_t3950806139 *)L_123))))
		{
			goto IL_027f;
		}
	}

IL_02a0:
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::AddEdgeToSEL(Pathfinding.ClipperLib.TEdge)
extern "C"  void Clipper_AddEdgeToSEL_m213197330 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___edge0, const MethodInfo* method)
{
	{
		TEdge_t3950806139 * L_0 = __this->get_m_SortedEdges_19();
		if (L_0)
		{
			goto IL_0025;
		}
	}
	{
		TEdge_t3950806139 * L_1 = ___edge0;
		__this->set_m_SortedEdges_19(L_1);
		TEdge_t3950806139 * L_2 = ___edge0;
		NullCheck(L_2);
		L_2->set_PrevInSEL_17((TEdge_t3950806139 *)NULL);
		TEdge_t3950806139 * L_3 = ___edge0;
		NullCheck(L_3);
		L_3->set_NextInSEL_16((TEdge_t3950806139 *)NULL);
		goto IL_004b;
	}

IL_0025:
	{
		TEdge_t3950806139 * L_4 = ___edge0;
		TEdge_t3950806139 * L_5 = __this->get_m_SortedEdges_19();
		NullCheck(L_4);
		L_4->set_NextInSEL_16(L_5);
		TEdge_t3950806139 * L_6 = ___edge0;
		NullCheck(L_6);
		L_6->set_PrevInSEL_17((TEdge_t3950806139 *)NULL);
		TEdge_t3950806139 * L_7 = __this->get_m_SortedEdges_19();
		TEdge_t3950806139 * L_8 = ___edge0;
		NullCheck(L_7);
		L_7->set_PrevInSEL_17(L_8);
		TEdge_t3950806139 * L_9 = ___edge0;
		__this->set_m_SortedEdges_19(L_9);
	}

IL_004b:
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::CopyAELToSEL()
extern "C"  void Clipper_CopyAELToSEL_m3766598946 (Clipper_t636949239 * __this, const MethodInfo* method)
{
	TEdge_t3950806139 * V_0 = NULL;
	{
		TEdge_t3950806139 * L_0 = __this->get_m_ActiveEdges_18();
		V_0 = L_0;
		TEdge_t3950806139 * L_1 = V_0;
		__this->set_m_SortedEdges_19(L_1);
		goto IL_0032;
	}

IL_0013:
	{
		TEdge_t3950806139 * L_2 = V_0;
		TEdge_t3950806139 * L_3 = V_0;
		NullCheck(L_3);
		TEdge_t3950806139 * L_4 = L_3->get_PrevInAEL_15();
		NullCheck(L_2);
		L_2->set_PrevInSEL_17(L_4);
		TEdge_t3950806139 * L_5 = V_0;
		TEdge_t3950806139 * L_6 = V_0;
		NullCheck(L_6);
		TEdge_t3950806139 * L_7 = L_6->get_NextInAEL_14();
		NullCheck(L_5);
		L_5->set_NextInSEL_16(L_7);
		TEdge_t3950806139 * L_8 = V_0;
		NullCheck(L_8);
		TEdge_t3950806139 * L_9 = L_8->get_NextInAEL_14();
		V_0 = L_9;
	}

IL_0032:
	{
		TEdge_t3950806139 * L_10 = V_0;
		if (L_10)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::SwapPositionsInAEL(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge)
extern "C"  void Clipper_SwapPositionsInAEL_m423077188 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___edge10, TEdge_t3950806139 * ___edge21, const MethodInfo* method)
{
	TEdge_t3950806139 * V_0 = NULL;
	TEdge_t3950806139 * V_1 = NULL;
	TEdge_t3950806139 * V_2 = NULL;
	TEdge_t3950806139 * V_3 = NULL;
	TEdge_t3950806139 * V_4 = NULL;
	TEdge_t3950806139 * V_5 = NULL;
	{
		TEdge_t3950806139 * L_0 = ___edge10;
		NullCheck(L_0);
		TEdge_t3950806139 * L_1 = L_0->get_NextInAEL_14();
		TEdge_t3950806139 * L_2 = ___edge10;
		NullCheck(L_2);
		TEdge_t3950806139 * L_3 = L_2->get_PrevInAEL_15();
		if ((((Il2CppObject*)(TEdge_t3950806139 *)L_1) == ((Il2CppObject*)(TEdge_t3950806139 *)L_3)))
		{
			goto IL_0022;
		}
	}
	{
		TEdge_t3950806139 * L_4 = ___edge21;
		NullCheck(L_4);
		TEdge_t3950806139 * L_5 = L_4->get_NextInAEL_14();
		TEdge_t3950806139 * L_6 = ___edge21;
		NullCheck(L_6);
		TEdge_t3950806139 * L_7 = L_6->get_PrevInAEL_15();
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_5) == ((Il2CppObject*)(TEdge_t3950806139 *)L_7))))
		{
			goto IL_0023;
		}
	}

IL_0022:
	{
		return;
	}

IL_0023:
	{
		TEdge_t3950806139 * L_8 = ___edge10;
		NullCheck(L_8);
		TEdge_t3950806139 * L_9 = L_8->get_NextInAEL_14();
		TEdge_t3950806139 * L_10 = ___edge21;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_9) == ((Il2CppObject*)(TEdge_t3950806139 *)L_10))))
		{
			goto IL_0078;
		}
	}
	{
		TEdge_t3950806139 * L_11 = ___edge21;
		NullCheck(L_11);
		TEdge_t3950806139 * L_12 = L_11->get_NextInAEL_14();
		V_0 = L_12;
		TEdge_t3950806139 * L_13 = V_0;
		if (!L_13)
		{
			goto IL_0043;
		}
	}
	{
		TEdge_t3950806139 * L_14 = V_0;
		TEdge_t3950806139 * L_15 = ___edge10;
		NullCheck(L_14);
		L_14->set_PrevInAEL_15(L_15);
	}

IL_0043:
	{
		TEdge_t3950806139 * L_16 = ___edge10;
		NullCheck(L_16);
		TEdge_t3950806139 * L_17 = L_16->get_PrevInAEL_15();
		V_1 = L_17;
		TEdge_t3950806139 * L_18 = V_1;
		if (!L_18)
		{
			goto IL_0057;
		}
	}
	{
		TEdge_t3950806139 * L_19 = V_1;
		TEdge_t3950806139 * L_20 = ___edge21;
		NullCheck(L_19);
		L_19->set_NextInAEL_14(L_20);
	}

IL_0057:
	{
		TEdge_t3950806139 * L_21 = ___edge21;
		TEdge_t3950806139 * L_22 = V_1;
		NullCheck(L_21);
		L_21->set_PrevInAEL_15(L_22);
		TEdge_t3950806139 * L_23 = ___edge21;
		TEdge_t3950806139 * L_24 = ___edge10;
		NullCheck(L_23);
		L_23->set_NextInAEL_14(L_24);
		TEdge_t3950806139 * L_25 = ___edge10;
		TEdge_t3950806139 * L_26 = ___edge21;
		NullCheck(L_25);
		L_25->set_PrevInAEL_15(L_26);
		TEdge_t3950806139 * L_27 = ___edge10;
		TEdge_t3950806139 * L_28 = V_0;
		NullCheck(L_27);
		L_27->set_NextInAEL_14(L_28);
		goto IL_0161;
	}

IL_0078:
	{
		TEdge_t3950806139 * L_29 = ___edge21;
		NullCheck(L_29);
		TEdge_t3950806139 * L_30 = L_29->get_NextInAEL_14();
		TEdge_t3950806139 * L_31 = ___edge10;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_30) == ((Il2CppObject*)(TEdge_t3950806139 *)L_31))))
		{
			goto IL_00cd;
		}
	}
	{
		TEdge_t3950806139 * L_32 = ___edge10;
		NullCheck(L_32);
		TEdge_t3950806139 * L_33 = L_32->get_NextInAEL_14();
		V_2 = L_33;
		TEdge_t3950806139 * L_34 = V_2;
		if (!L_34)
		{
			goto IL_0098;
		}
	}
	{
		TEdge_t3950806139 * L_35 = V_2;
		TEdge_t3950806139 * L_36 = ___edge21;
		NullCheck(L_35);
		L_35->set_PrevInAEL_15(L_36);
	}

IL_0098:
	{
		TEdge_t3950806139 * L_37 = ___edge21;
		NullCheck(L_37);
		TEdge_t3950806139 * L_38 = L_37->get_PrevInAEL_15();
		V_3 = L_38;
		TEdge_t3950806139 * L_39 = V_3;
		if (!L_39)
		{
			goto IL_00ac;
		}
	}
	{
		TEdge_t3950806139 * L_40 = V_3;
		TEdge_t3950806139 * L_41 = ___edge10;
		NullCheck(L_40);
		L_40->set_NextInAEL_14(L_41);
	}

IL_00ac:
	{
		TEdge_t3950806139 * L_42 = ___edge10;
		TEdge_t3950806139 * L_43 = V_3;
		NullCheck(L_42);
		L_42->set_PrevInAEL_15(L_43);
		TEdge_t3950806139 * L_44 = ___edge10;
		TEdge_t3950806139 * L_45 = ___edge21;
		NullCheck(L_44);
		L_44->set_NextInAEL_14(L_45);
		TEdge_t3950806139 * L_46 = ___edge21;
		TEdge_t3950806139 * L_47 = ___edge10;
		NullCheck(L_46);
		L_46->set_PrevInAEL_15(L_47);
		TEdge_t3950806139 * L_48 = ___edge21;
		TEdge_t3950806139 * L_49 = V_2;
		NullCheck(L_48);
		L_48->set_NextInAEL_14(L_49);
		goto IL_0161;
	}

IL_00cd:
	{
		TEdge_t3950806139 * L_50 = ___edge10;
		NullCheck(L_50);
		TEdge_t3950806139 * L_51 = L_50->get_NextInAEL_14();
		V_4 = L_51;
		TEdge_t3950806139 * L_52 = ___edge10;
		NullCheck(L_52);
		TEdge_t3950806139 * L_53 = L_52->get_PrevInAEL_15();
		V_5 = L_53;
		TEdge_t3950806139 * L_54 = ___edge10;
		TEdge_t3950806139 * L_55 = ___edge21;
		NullCheck(L_55);
		TEdge_t3950806139 * L_56 = L_55->get_NextInAEL_14();
		NullCheck(L_54);
		L_54->set_NextInAEL_14(L_56);
		TEdge_t3950806139 * L_57 = ___edge10;
		NullCheck(L_57);
		TEdge_t3950806139 * L_58 = L_57->get_NextInAEL_14();
		if (!L_58)
		{
			goto IL_0100;
		}
	}
	{
		TEdge_t3950806139 * L_59 = ___edge10;
		NullCheck(L_59);
		TEdge_t3950806139 * L_60 = L_59->get_NextInAEL_14();
		TEdge_t3950806139 * L_61 = ___edge10;
		NullCheck(L_60);
		L_60->set_PrevInAEL_15(L_61);
	}

IL_0100:
	{
		TEdge_t3950806139 * L_62 = ___edge10;
		TEdge_t3950806139 * L_63 = ___edge21;
		NullCheck(L_63);
		TEdge_t3950806139 * L_64 = L_63->get_PrevInAEL_15();
		NullCheck(L_62);
		L_62->set_PrevInAEL_15(L_64);
		TEdge_t3950806139 * L_65 = ___edge10;
		NullCheck(L_65);
		TEdge_t3950806139 * L_66 = L_65->get_PrevInAEL_15();
		if (!L_66)
		{
			goto IL_0123;
		}
	}
	{
		TEdge_t3950806139 * L_67 = ___edge10;
		NullCheck(L_67);
		TEdge_t3950806139 * L_68 = L_67->get_PrevInAEL_15();
		TEdge_t3950806139 * L_69 = ___edge10;
		NullCheck(L_68);
		L_68->set_NextInAEL_14(L_69);
	}

IL_0123:
	{
		TEdge_t3950806139 * L_70 = ___edge21;
		TEdge_t3950806139 * L_71 = V_4;
		NullCheck(L_70);
		L_70->set_NextInAEL_14(L_71);
		TEdge_t3950806139 * L_72 = ___edge21;
		NullCheck(L_72);
		TEdge_t3950806139 * L_73 = L_72->get_NextInAEL_14();
		if (!L_73)
		{
			goto IL_0142;
		}
	}
	{
		TEdge_t3950806139 * L_74 = ___edge21;
		NullCheck(L_74);
		TEdge_t3950806139 * L_75 = L_74->get_NextInAEL_14();
		TEdge_t3950806139 * L_76 = ___edge21;
		NullCheck(L_75);
		L_75->set_PrevInAEL_15(L_76);
	}

IL_0142:
	{
		TEdge_t3950806139 * L_77 = ___edge21;
		TEdge_t3950806139 * L_78 = V_5;
		NullCheck(L_77);
		L_77->set_PrevInAEL_15(L_78);
		TEdge_t3950806139 * L_79 = ___edge21;
		NullCheck(L_79);
		TEdge_t3950806139 * L_80 = L_79->get_PrevInAEL_15();
		if (!L_80)
		{
			goto IL_0161;
		}
	}
	{
		TEdge_t3950806139 * L_81 = ___edge21;
		NullCheck(L_81);
		TEdge_t3950806139 * L_82 = L_81->get_PrevInAEL_15();
		TEdge_t3950806139 * L_83 = ___edge21;
		NullCheck(L_82);
		L_82->set_NextInAEL_14(L_83);
	}

IL_0161:
	{
		TEdge_t3950806139 * L_84 = ___edge10;
		NullCheck(L_84);
		TEdge_t3950806139 * L_85 = L_84->get_PrevInAEL_15();
		if (L_85)
		{
			goto IL_0178;
		}
	}
	{
		TEdge_t3950806139 * L_86 = ___edge10;
		__this->set_m_ActiveEdges_18(L_86);
		goto IL_018a;
	}

IL_0178:
	{
		TEdge_t3950806139 * L_87 = ___edge21;
		NullCheck(L_87);
		TEdge_t3950806139 * L_88 = L_87->get_PrevInAEL_15();
		if (L_88)
		{
			goto IL_018a;
		}
	}
	{
		TEdge_t3950806139 * L_89 = ___edge21;
		__this->set_m_ActiveEdges_18(L_89);
	}

IL_018a:
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::SwapPositionsInSEL(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge)
extern "C"  void Clipper_SwapPositionsInSEL_m2241560178 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___edge10, TEdge_t3950806139 * ___edge21, const MethodInfo* method)
{
	TEdge_t3950806139 * V_0 = NULL;
	TEdge_t3950806139 * V_1 = NULL;
	TEdge_t3950806139 * V_2 = NULL;
	TEdge_t3950806139 * V_3 = NULL;
	TEdge_t3950806139 * V_4 = NULL;
	TEdge_t3950806139 * V_5 = NULL;
	{
		TEdge_t3950806139 * L_0 = ___edge10;
		NullCheck(L_0);
		TEdge_t3950806139 * L_1 = L_0->get_NextInSEL_16();
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		TEdge_t3950806139 * L_2 = ___edge10;
		NullCheck(L_2);
		TEdge_t3950806139 * L_3 = L_2->get_PrevInSEL_17();
		if (L_3)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		TEdge_t3950806139 * L_4 = ___edge21;
		NullCheck(L_4);
		TEdge_t3950806139 * L_5 = L_4->get_NextInSEL_16();
		if (L_5)
		{
			goto IL_002e;
		}
	}
	{
		TEdge_t3950806139 * L_6 = ___edge21;
		NullCheck(L_6);
		TEdge_t3950806139 * L_7 = L_6->get_PrevInSEL_17();
		if (L_7)
		{
			goto IL_002e;
		}
	}
	{
		return;
	}

IL_002e:
	{
		TEdge_t3950806139 * L_8 = ___edge10;
		NullCheck(L_8);
		TEdge_t3950806139 * L_9 = L_8->get_NextInSEL_16();
		TEdge_t3950806139 * L_10 = ___edge21;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_9) == ((Il2CppObject*)(TEdge_t3950806139 *)L_10))))
		{
			goto IL_0083;
		}
	}
	{
		TEdge_t3950806139 * L_11 = ___edge21;
		NullCheck(L_11);
		TEdge_t3950806139 * L_12 = L_11->get_NextInSEL_16();
		V_0 = L_12;
		TEdge_t3950806139 * L_13 = V_0;
		if (!L_13)
		{
			goto IL_004e;
		}
	}
	{
		TEdge_t3950806139 * L_14 = V_0;
		TEdge_t3950806139 * L_15 = ___edge10;
		NullCheck(L_14);
		L_14->set_PrevInSEL_17(L_15);
	}

IL_004e:
	{
		TEdge_t3950806139 * L_16 = ___edge10;
		NullCheck(L_16);
		TEdge_t3950806139 * L_17 = L_16->get_PrevInSEL_17();
		V_1 = L_17;
		TEdge_t3950806139 * L_18 = V_1;
		if (!L_18)
		{
			goto IL_0062;
		}
	}
	{
		TEdge_t3950806139 * L_19 = V_1;
		TEdge_t3950806139 * L_20 = ___edge21;
		NullCheck(L_19);
		L_19->set_NextInSEL_16(L_20);
	}

IL_0062:
	{
		TEdge_t3950806139 * L_21 = ___edge21;
		TEdge_t3950806139 * L_22 = V_1;
		NullCheck(L_21);
		L_21->set_PrevInSEL_17(L_22);
		TEdge_t3950806139 * L_23 = ___edge21;
		TEdge_t3950806139 * L_24 = ___edge10;
		NullCheck(L_23);
		L_23->set_NextInSEL_16(L_24);
		TEdge_t3950806139 * L_25 = ___edge10;
		TEdge_t3950806139 * L_26 = ___edge21;
		NullCheck(L_25);
		L_25->set_PrevInSEL_17(L_26);
		TEdge_t3950806139 * L_27 = ___edge10;
		TEdge_t3950806139 * L_28 = V_0;
		NullCheck(L_27);
		L_27->set_NextInSEL_16(L_28);
		goto IL_016c;
	}

IL_0083:
	{
		TEdge_t3950806139 * L_29 = ___edge21;
		NullCheck(L_29);
		TEdge_t3950806139 * L_30 = L_29->get_NextInSEL_16();
		TEdge_t3950806139 * L_31 = ___edge10;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_30) == ((Il2CppObject*)(TEdge_t3950806139 *)L_31))))
		{
			goto IL_00d8;
		}
	}
	{
		TEdge_t3950806139 * L_32 = ___edge10;
		NullCheck(L_32);
		TEdge_t3950806139 * L_33 = L_32->get_NextInSEL_16();
		V_2 = L_33;
		TEdge_t3950806139 * L_34 = V_2;
		if (!L_34)
		{
			goto IL_00a3;
		}
	}
	{
		TEdge_t3950806139 * L_35 = V_2;
		TEdge_t3950806139 * L_36 = ___edge21;
		NullCheck(L_35);
		L_35->set_PrevInSEL_17(L_36);
	}

IL_00a3:
	{
		TEdge_t3950806139 * L_37 = ___edge21;
		NullCheck(L_37);
		TEdge_t3950806139 * L_38 = L_37->get_PrevInSEL_17();
		V_3 = L_38;
		TEdge_t3950806139 * L_39 = V_3;
		if (!L_39)
		{
			goto IL_00b7;
		}
	}
	{
		TEdge_t3950806139 * L_40 = V_3;
		TEdge_t3950806139 * L_41 = ___edge10;
		NullCheck(L_40);
		L_40->set_NextInSEL_16(L_41);
	}

IL_00b7:
	{
		TEdge_t3950806139 * L_42 = ___edge10;
		TEdge_t3950806139 * L_43 = V_3;
		NullCheck(L_42);
		L_42->set_PrevInSEL_17(L_43);
		TEdge_t3950806139 * L_44 = ___edge10;
		TEdge_t3950806139 * L_45 = ___edge21;
		NullCheck(L_44);
		L_44->set_NextInSEL_16(L_45);
		TEdge_t3950806139 * L_46 = ___edge21;
		TEdge_t3950806139 * L_47 = ___edge10;
		NullCheck(L_46);
		L_46->set_PrevInSEL_17(L_47);
		TEdge_t3950806139 * L_48 = ___edge21;
		TEdge_t3950806139 * L_49 = V_2;
		NullCheck(L_48);
		L_48->set_NextInSEL_16(L_49);
		goto IL_016c;
	}

IL_00d8:
	{
		TEdge_t3950806139 * L_50 = ___edge10;
		NullCheck(L_50);
		TEdge_t3950806139 * L_51 = L_50->get_NextInSEL_16();
		V_4 = L_51;
		TEdge_t3950806139 * L_52 = ___edge10;
		NullCheck(L_52);
		TEdge_t3950806139 * L_53 = L_52->get_PrevInSEL_17();
		V_5 = L_53;
		TEdge_t3950806139 * L_54 = ___edge10;
		TEdge_t3950806139 * L_55 = ___edge21;
		NullCheck(L_55);
		TEdge_t3950806139 * L_56 = L_55->get_NextInSEL_16();
		NullCheck(L_54);
		L_54->set_NextInSEL_16(L_56);
		TEdge_t3950806139 * L_57 = ___edge10;
		NullCheck(L_57);
		TEdge_t3950806139 * L_58 = L_57->get_NextInSEL_16();
		if (!L_58)
		{
			goto IL_010b;
		}
	}
	{
		TEdge_t3950806139 * L_59 = ___edge10;
		NullCheck(L_59);
		TEdge_t3950806139 * L_60 = L_59->get_NextInSEL_16();
		TEdge_t3950806139 * L_61 = ___edge10;
		NullCheck(L_60);
		L_60->set_PrevInSEL_17(L_61);
	}

IL_010b:
	{
		TEdge_t3950806139 * L_62 = ___edge10;
		TEdge_t3950806139 * L_63 = ___edge21;
		NullCheck(L_63);
		TEdge_t3950806139 * L_64 = L_63->get_PrevInSEL_17();
		NullCheck(L_62);
		L_62->set_PrevInSEL_17(L_64);
		TEdge_t3950806139 * L_65 = ___edge10;
		NullCheck(L_65);
		TEdge_t3950806139 * L_66 = L_65->get_PrevInSEL_17();
		if (!L_66)
		{
			goto IL_012e;
		}
	}
	{
		TEdge_t3950806139 * L_67 = ___edge10;
		NullCheck(L_67);
		TEdge_t3950806139 * L_68 = L_67->get_PrevInSEL_17();
		TEdge_t3950806139 * L_69 = ___edge10;
		NullCheck(L_68);
		L_68->set_NextInSEL_16(L_69);
	}

IL_012e:
	{
		TEdge_t3950806139 * L_70 = ___edge21;
		TEdge_t3950806139 * L_71 = V_4;
		NullCheck(L_70);
		L_70->set_NextInSEL_16(L_71);
		TEdge_t3950806139 * L_72 = ___edge21;
		NullCheck(L_72);
		TEdge_t3950806139 * L_73 = L_72->get_NextInSEL_16();
		if (!L_73)
		{
			goto IL_014d;
		}
	}
	{
		TEdge_t3950806139 * L_74 = ___edge21;
		NullCheck(L_74);
		TEdge_t3950806139 * L_75 = L_74->get_NextInSEL_16();
		TEdge_t3950806139 * L_76 = ___edge21;
		NullCheck(L_75);
		L_75->set_PrevInSEL_17(L_76);
	}

IL_014d:
	{
		TEdge_t3950806139 * L_77 = ___edge21;
		TEdge_t3950806139 * L_78 = V_5;
		NullCheck(L_77);
		L_77->set_PrevInSEL_17(L_78);
		TEdge_t3950806139 * L_79 = ___edge21;
		NullCheck(L_79);
		TEdge_t3950806139 * L_80 = L_79->get_PrevInSEL_17();
		if (!L_80)
		{
			goto IL_016c;
		}
	}
	{
		TEdge_t3950806139 * L_81 = ___edge21;
		NullCheck(L_81);
		TEdge_t3950806139 * L_82 = L_81->get_PrevInSEL_17();
		TEdge_t3950806139 * L_83 = ___edge21;
		NullCheck(L_82);
		L_82->set_NextInSEL_16(L_83);
	}

IL_016c:
	{
		TEdge_t3950806139 * L_84 = ___edge10;
		NullCheck(L_84);
		TEdge_t3950806139 * L_85 = L_84->get_PrevInSEL_17();
		if (L_85)
		{
			goto IL_0183;
		}
	}
	{
		TEdge_t3950806139 * L_86 = ___edge10;
		__this->set_m_SortedEdges_19(L_86);
		goto IL_0195;
	}

IL_0183:
	{
		TEdge_t3950806139 * L_87 = ___edge21;
		NullCheck(L_87);
		TEdge_t3950806139 * L_88 = L_87->get_PrevInSEL_17();
		if (L_88)
		{
			goto IL_0195;
		}
	}
	{
		TEdge_t3950806139 * L_89 = ___edge21;
		__this->set_m_SortedEdges_19(L_89);
	}

IL_0195:
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::AddLocalMaxPoly(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.IntPoint)
extern "C"  void Clipper_AddLocalMaxPoly_m913303731 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e10, TEdge_t3950806139 * ___e21, IntPoint_t3326126179  ___pt2, const MethodInfo* method)
{
	{
		TEdge_t3950806139 * L_0 = ___e10;
		IntPoint_t3326126179  L_1 = ___pt2;
		Clipper_AddOutPt_m3632434112(__this, L_0, L_1, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_2 = ___e10;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_OutIdx_10();
		TEdge_t3950806139 * L_4 = ___e21;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_OutIdx_10();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_5))))
		{
			goto IL_002d;
		}
	}
	{
		TEdge_t3950806139 * L_6 = ___e10;
		NullCheck(L_6);
		L_6->set_OutIdx_10((-1));
		TEdge_t3950806139 * L_7 = ___e21;
		NullCheck(L_7);
		L_7->set_OutIdx_10((-1));
		goto IL_0053;
	}

IL_002d:
	{
		TEdge_t3950806139 * L_8 = ___e10;
		NullCheck(L_8);
		int32_t L_9 = L_8->get_OutIdx_10();
		TEdge_t3950806139 * L_10 = ___e21;
		NullCheck(L_10);
		int32_t L_11 = L_10->get_OutIdx_10();
		if ((((int32_t)L_9) >= ((int32_t)L_11)))
		{
			goto IL_004b;
		}
	}
	{
		TEdge_t3950806139 * L_12 = ___e10;
		TEdge_t3950806139 * L_13 = ___e21;
		Clipper_AppendPolygon_m921090842(__this, L_12, L_13, /*hidden argument*/NULL);
		goto IL_0053;
	}

IL_004b:
	{
		TEdge_t3950806139 * L_14 = ___e21;
		TEdge_t3950806139 * L_15 = ___e10;
		Clipper_AppendPolygon_m921090842(__this, L_14, L_15, /*hidden argument*/NULL);
	}

IL_0053:
	{
		return;
	}
}
// Pathfinding.ClipperLib.OutPt Pathfinding.ClipperLib.Clipper::AddLocalMinPoly(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.IntPoint)
extern "C"  OutPt_t3947633180 * Clipper_AddLocalMinPoly_m2497180132 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e10, TEdge_t3950806139 * ___e21, IntPoint_t3326126179  ___pt2, const MethodInfo* method)
{
	OutPt_t3947633180 * V_0 = NULL;
	TEdge_t3950806139 * V_1 = NULL;
	TEdge_t3950806139 * V_2 = NULL;
	OutPt_t3947633180 * V_3 = NULL;
	{
		TEdge_t3950806139 * L_0 = ___e21;
		bool L_1 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		TEdge_t3950806139 * L_2 = ___e10;
		NullCheck(L_2);
		double L_3 = L_2->get_Dx_4();
		TEdge_t3950806139 * L_4 = ___e21;
		NullCheck(L_4);
		double L_5 = L_4->get_Dx_4();
		if ((!(((double)L_3) > ((double)L_5))))
		{
			goto IL_0065;
		}
	}

IL_001c:
	{
		TEdge_t3950806139 * L_6 = ___e10;
		IntPoint_t3326126179  L_7 = ___pt2;
		OutPt_t3947633180 * L_8 = Clipper_AddOutPt_m3632434112(__this, L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		TEdge_t3950806139 * L_9 = ___e21;
		TEdge_t3950806139 * L_10 = ___e10;
		NullCheck(L_10);
		int32_t L_11 = L_10->get_OutIdx_10();
		NullCheck(L_9);
		L_9->set_OutIdx_10(L_11);
		TEdge_t3950806139 * L_12 = ___e10;
		NullCheck(L_12);
		L_12->set_Side_6(0);
		TEdge_t3950806139 * L_13 = ___e21;
		NullCheck(L_13);
		L_13->set_Side_6(1);
		TEdge_t3950806139 * L_14 = ___e10;
		V_1 = L_14;
		TEdge_t3950806139 * L_15 = V_1;
		NullCheck(L_15);
		TEdge_t3950806139 * L_16 = L_15->get_PrevInAEL_15();
		TEdge_t3950806139 * L_17 = ___e21;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_16) == ((Il2CppObject*)(TEdge_t3950806139 *)L_17))))
		{
			goto IL_0059;
		}
	}
	{
		TEdge_t3950806139 * L_18 = ___e21;
		NullCheck(L_18);
		TEdge_t3950806139 * L_19 = L_18->get_PrevInAEL_15();
		V_2 = L_19;
		goto IL_0060;
	}

IL_0059:
	{
		TEdge_t3950806139 * L_20 = V_1;
		NullCheck(L_20);
		TEdge_t3950806139 * L_21 = L_20->get_PrevInAEL_15();
		V_2 = L_21;
	}

IL_0060:
	{
		goto IL_00a9;
	}

IL_0065:
	{
		TEdge_t3950806139 * L_22 = ___e21;
		IntPoint_t3326126179  L_23 = ___pt2;
		OutPt_t3947633180 * L_24 = Clipper_AddOutPt_m3632434112(__this, L_22, L_23, /*hidden argument*/NULL);
		V_0 = L_24;
		TEdge_t3950806139 * L_25 = ___e10;
		TEdge_t3950806139 * L_26 = ___e21;
		NullCheck(L_26);
		int32_t L_27 = L_26->get_OutIdx_10();
		NullCheck(L_25);
		L_25->set_OutIdx_10(L_27);
		TEdge_t3950806139 * L_28 = ___e10;
		NullCheck(L_28);
		L_28->set_Side_6(1);
		TEdge_t3950806139 * L_29 = ___e21;
		NullCheck(L_29);
		L_29->set_Side_6(0);
		TEdge_t3950806139 * L_30 = ___e21;
		V_1 = L_30;
		TEdge_t3950806139 * L_31 = V_1;
		NullCheck(L_31);
		TEdge_t3950806139 * L_32 = L_31->get_PrevInAEL_15();
		TEdge_t3950806139 * L_33 = ___e10;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_32) == ((Il2CppObject*)(TEdge_t3950806139 *)L_33))))
		{
			goto IL_00a2;
		}
	}
	{
		TEdge_t3950806139 * L_34 = ___e10;
		NullCheck(L_34);
		TEdge_t3950806139 * L_35 = L_34->get_PrevInAEL_15();
		V_2 = L_35;
		goto IL_00a9;
	}

IL_00a2:
	{
		TEdge_t3950806139 * L_36 = V_1;
		NullCheck(L_36);
		TEdge_t3950806139 * L_37 = L_36->get_PrevInAEL_15();
		V_2 = L_37;
	}

IL_00a9:
	{
		TEdge_t3950806139 * L_38 = V_2;
		if (!L_38)
		{
			goto IL_0119;
		}
	}
	{
		TEdge_t3950806139 * L_39 = V_2;
		NullCheck(L_39);
		int32_t L_40 = L_39->get_OutIdx_10();
		if ((((int32_t)L_40) < ((int32_t)0)))
		{
			goto IL_0119;
		}
	}
	{
		TEdge_t3950806139 * L_41 = V_2;
		int64_t L_42 = (&___pt2)->get_Y_1();
		int64_t L_43 = Clipper_TopX_m1760470797(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_44 = V_1;
		int64_t L_45 = (&___pt2)->get_Y_1();
		int64_t L_46 = Clipper_TopX_m1760470797(NULL /*static, unused*/, L_44, L_45, /*hidden argument*/NULL);
		if ((!(((uint64_t)L_43) == ((uint64_t)L_46))))
		{
			goto IL_0119;
		}
	}
	{
		TEdge_t3950806139 * L_47 = V_1;
		TEdge_t3950806139 * L_48 = V_2;
		bool L_49 = ((ClipperBase_t512275816 *)__this)->get_m_UseFullRange_9();
		bool L_50 = ClipperBase_SlopesEqual_m3887656866(NULL /*static, unused*/, L_47, L_48, L_49, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_0119;
		}
	}
	{
		TEdge_t3950806139 * L_51 = V_1;
		NullCheck(L_51);
		int32_t L_52 = L_51->get_WindDelta_7();
		if (!L_52)
		{
			goto IL_0119;
		}
	}
	{
		TEdge_t3950806139 * L_53 = V_2;
		NullCheck(L_53);
		int32_t L_54 = L_53->get_WindDelta_7();
		if (!L_54)
		{
			goto IL_0119;
		}
	}
	{
		TEdge_t3950806139 * L_55 = V_2;
		IntPoint_t3326126179  L_56 = ___pt2;
		OutPt_t3947633180 * L_57 = Clipper_AddOutPt_m3632434112(__this, L_55, L_56, /*hidden argument*/NULL);
		V_3 = L_57;
		OutPt_t3947633180 * L_58 = V_0;
		OutPt_t3947633180 * L_59 = V_3;
		TEdge_t3950806139 * L_60 = V_1;
		NullCheck(L_60);
		IntPoint_t3326126179  L_61 = L_60->get_Top_2();
		Clipper_AddJoin_m2260685358(__this, L_58, L_59, L_61, /*hidden argument*/NULL);
	}

IL_0119:
	{
		OutPt_t3947633180 * L_62 = V_0;
		return L_62;
	}
}
// Pathfinding.ClipperLib.OutRec Pathfinding.ClipperLib.Clipper::CreateOutRec()
extern Il2CppClass* OutRec_t3245805284_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m1157457265_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m1367576335_MethodInfo_var;
extern const uint32_t Clipper_CreateOutRec_m1755291545_MetadataUsageId;
extern "C"  OutRec_t3245805284 * Clipper_CreateOutRec_m1755291545 (Clipper_t636949239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_CreateOutRec_m1755291545_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	OutRec_t3245805284 * V_0 = NULL;
	{
		OutRec_t3245805284 * L_0 = (OutRec_t3245805284 *)il2cpp_codegen_object_new(OutRec_t3245805284_il2cpp_TypeInfo_var);
		OutRec__ctor_m2716404681(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		OutRec_t3245805284 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_Idx_0((-1));
		OutRec_t3245805284 * L_2 = V_0;
		NullCheck(L_2);
		L_2->set_IsHole_1((bool)0);
		OutRec_t3245805284 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_IsOpen_2((bool)0);
		OutRec_t3245805284 * L_4 = V_0;
		NullCheck(L_4);
		L_4->set_FirstLeft_3((OutRec_t3245805284 *)NULL);
		OutRec_t3245805284 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_Pts_4((OutPt_t3947633180 *)NULL);
		OutRec_t3245805284 * L_6 = V_0;
		NullCheck(L_6);
		L_6->set_BottomPt_5((OutPt_t3947633180 *)NULL);
		OutRec_t3245805284 * L_7 = V_0;
		NullCheck(L_7);
		L_7->set_PolyNode_6((PolyNode_t3336253808 *)NULL);
		List_1_t319023540 * L_8 = __this->get_m_PolyOuts_15();
		OutRec_t3245805284 * L_9 = V_0;
		NullCheck(L_8);
		List_1_Add_m1157457265(L_8, L_9, /*hidden argument*/List_1_Add_m1157457265_MethodInfo_var);
		OutRec_t3245805284 * L_10 = V_0;
		List_1_t319023540 * L_11 = __this->get_m_PolyOuts_15();
		NullCheck(L_11);
		int32_t L_12 = List_1_get_Count_m1367576335(L_11, /*hidden argument*/List_1_get_Count_m1367576335_MethodInfo_var);
		NullCheck(L_10);
		L_10->set_Idx_0(((int32_t)((int32_t)L_12-(int32_t)1)));
		OutRec_t3245805284 * L_13 = V_0;
		return L_13;
	}
}
// Pathfinding.ClipperLib.OutPt Pathfinding.ClipperLib.Clipper::AddOutPt(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.IntPoint)
extern Il2CppClass* OutPt_t3947633180_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m2053160358_MethodInfo_var;
extern const uint32_t Clipper_AddOutPt_m3632434112_MetadataUsageId;
extern "C"  OutPt_t3947633180 * Clipper_AddOutPt_m3632434112 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e0, IntPoint_t3326126179  ___pt1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_AddOutPt_m3632434112_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	OutRec_t3245805284 * V_1 = NULL;
	OutPt_t3947633180 * V_2 = NULL;
	OutRec_t3245805284 * V_3 = NULL;
	OutPt_t3947633180 * V_4 = NULL;
	OutPt_t3947633180 * V_5 = NULL;
	{
		TEdge_t3950806139 * L_0 = ___e0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_Side_6();
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		TEdge_t3950806139 * L_2 = ___e0;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_OutIdx_10();
		if ((((int32_t)L_3) >= ((int32_t)0)))
		{
			goto IL_007b;
		}
	}
	{
		OutRec_t3245805284 * L_4 = Clipper_CreateOutRec_m1755291545(__this, /*hidden argument*/NULL);
		V_1 = L_4;
		OutRec_t3245805284 * L_5 = V_1;
		TEdge_t3950806139 * L_6 = ___e0;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_WindDelta_7();
		NullCheck(L_5);
		L_5->set_IsOpen_2((bool)((((int32_t)L_7) == ((int32_t)0))? 1 : 0));
		OutPt_t3947633180 * L_8 = (OutPt_t3947633180 *)il2cpp_codegen_object_new(OutPt_t3947633180_il2cpp_TypeInfo_var);
		OutPt__ctor_m1159254633(L_8, /*hidden argument*/NULL);
		V_2 = L_8;
		OutRec_t3245805284 * L_9 = V_1;
		OutPt_t3947633180 * L_10 = V_2;
		NullCheck(L_9);
		L_9->set_Pts_4(L_10);
		OutPt_t3947633180 * L_11 = V_2;
		OutRec_t3245805284 * L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = L_12->get_Idx_0();
		NullCheck(L_11);
		L_11->set_Idx_0(L_13);
		OutPt_t3947633180 * L_14 = V_2;
		IntPoint_t3326126179  L_15 = ___pt1;
		NullCheck(L_14);
		L_14->set_Pt_1(L_15);
		OutPt_t3947633180 * L_16 = V_2;
		OutPt_t3947633180 * L_17 = V_2;
		NullCheck(L_16);
		L_16->set_Next_2(L_17);
		OutPt_t3947633180 * L_18 = V_2;
		OutPt_t3947633180 * L_19 = V_2;
		NullCheck(L_18);
		L_18->set_Prev_3(L_19);
		OutRec_t3245805284 * L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->get_IsOpen_2();
		if (L_21)
		{
			goto IL_006d;
		}
	}
	{
		TEdge_t3950806139 * L_22 = ___e0;
		OutRec_t3245805284 * L_23 = V_1;
		Clipper_SetHoleState_m1938498398(__this, L_22, L_23, /*hidden argument*/NULL);
	}

IL_006d:
	{
		TEdge_t3950806139 * L_24 = ___e0;
		OutRec_t3245805284 * L_25 = V_1;
		NullCheck(L_25);
		int32_t L_26 = L_25->get_Idx_0();
		NullCheck(L_24);
		L_24->set_OutIdx_10(L_26);
		OutPt_t3947633180 * L_27 = V_2;
		return L_27;
	}

IL_007b:
	{
		List_1_t319023540 * L_28 = __this->get_m_PolyOuts_15();
		TEdge_t3950806139 * L_29 = ___e0;
		NullCheck(L_29);
		int32_t L_30 = L_29->get_OutIdx_10();
		NullCheck(L_28);
		OutRec_t3245805284 * L_31 = List_1_get_Item_m2053160358(L_28, L_30, /*hidden argument*/List_1_get_Item_m2053160358_MethodInfo_var);
		V_3 = L_31;
		OutRec_t3245805284 * L_32 = V_3;
		NullCheck(L_32);
		OutPt_t3947633180 * L_33 = L_32->get_Pts_4();
		V_4 = L_33;
		bool L_34 = V_0;
		if (!L_34)
		{
			goto IL_00b0;
		}
	}
	{
		IntPoint_t3326126179  L_35 = ___pt1;
		OutPt_t3947633180 * L_36 = V_4;
		NullCheck(L_36);
		IntPoint_t3326126179  L_37 = L_36->get_Pt_1();
		bool L_38 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_35, L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_00b0;
		}
	}
	{
		OutPt_t3947633180 * L_39 = V_4;
		return L_39;
	}

IL_00b0:
	{
		bool L_40 = V_0;
		if (L_40)
		{
			goto IL_00d5;
		}
	}
	{
		IntPoint_t3326126179  L_41 = ___pt1;
		OutPt_t3947633180 * L_42 = V_4;
		NullCheck(L_42);
		OutPt_t3947633180 * L_43 = L_42->get_Prev_3();
		NullCheck(L_43);
		IntPoint_t3326126179  L_44 = L_43->get_Pt_1();
		bool L_45 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_41, L_44, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_00d5;
		}
	}
	{
		OutPt_t3947633180 * L_46 = V_4;
		NullCheck(L_46);
		OutPt_t3947633180 * L_47 = L_46->get_Prev_3();
		return L_47;
	}

IL_00d5:
	{
		OutPt_t3947633180 * L_48 = (OutPt_t3947633180 *)il2cpp_codegen_object_new(OutPt_t3947633180_il2cpp_TypeInfo_var);
		OutPt__ctor_m1159254633(L_48, /*hidden argument*/NULL);
		V_5 = L_48;
		OutPt_t3947633180 * L_49 = V_5;
		OutRec_t3245805284 * L_50 = V_3;
		NullCheck(L_50);
		int32_t L_51 = L_50->get_Idx_0();
		NullCheck(L_49);
		L_49->set_Idx_0(L_51);
		OutPt_t3947633180 * L_52 = V_5;
		IntPoint_t3326126179  L_53 = ___pt1;
		NullCheck(L_52);
		L_52->set_Pt_1(L_53);
		OutPt_t3947633180 * L_54 = V_5;
		OutPt_t3947633180 * L_55 = V_4;
		NullCheck(L_54);
		L_54->set_Next_2(L_55);
		OutPt_t3947633180 * L_56 = V_5;
		OutPt_t3947633180 * L_57 = V_4;
		NullCheck(L_57);
		OutPt_t3947633180 * L_58 = L_57->get_Prev_3();
		NullCheck(L_56);
		L_56->set_Prev_3(L_58);
		OutPt_t3947633180 * L_59 = V_5;
		NullCheck(L_59);
		OutPt_t3947633180 * L_60 = L_59->get_Prev_3();
		OutPt_t3947633180 * L_61 = V_5;
		NullCheck(L_60);
		L_60->set_Next_2(L_61);
		OutPt_t3947633180 * L_62 = V_4;
		OutPt_t3947633180 * L_63 = V_5;
		NullCheck(L_62);
		L_62->set_Prev_3(L_63);
		bool L_64 = V_0;
		if (!L_64)
		{
			goto IL_012d;
		}
	}
	{
		OutRec_t3245805284 * L_65 = V_3;
		OutPt_t3947633180 * L_66 = V_5;
		NullCheck(L_65);
		L_65->set_Pts_4(L_66);
	}

IL_012d:
	{
		OutPt_t3947633180 * L_67 = V_5;
		return L_67;
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::HorzSegmentsOverlap(Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint)
extern "C"  bool Clipper_HorzSegmentsOverlap_m2752565594 (Clipper_t636949239 * __this, IntPoint_t3326126179  ___Pt1a0, IntPoint_t3326126179  ___Pt1b1, IntPoint_t3326126179  ___Pt2a2, IntPoint_t3326126179  ___Pt2b3, const MethodInfo* method)
{
	{
		int64_t L_0 = (&___Pt1a0)->get_X_0();
		int64_t L_1 = (&___Pt2a2)->get_X_0();
		int64_t L_2 = (&___Pt1a0)->get_X_0();
		int64_t L_3 = (&___Pt2b3)->get_X_0();
		if ((!(((uint32_t)((((int64_t)L_0) > ((int64_t)L_1))? 1 : 0)) == ((uint32_t)((((int64_t)L_2) < ((int64_t)L_3))? 1 : 0)))))
		{
			goto IL_0027;
		}
	}
	{
		return (bool)1;
	}

IL_0027:
	{
		int64_t L_4 = (&___Pt1b1)->get_X_0();
		int64_t L_5 = (&___Pt2a2)->get_X_0();
		int64_t L_6 = (&___Pt1b1)->get_X_0();
		int64_t L_7 = (&___Pt2b3)->get_X_0();
		if ((!(((uint32_t)((((int64_t)L_4) > ((int64_t)L_5))? 1 : 0)) == ((uint32_t)((((int64_t)L_6) < ((int64_t)L_7))? 1 : 0)))))
		{
			goto IL_004e;
		}
	}
	{
		return (bool)1;
	}

IL_004e:
	{
		int64_t L_8 = (&___Pt2a2)->get_X_0();
		int64_t L_9 = (&___Pt1a0)->get_X_0();
		int64_t L_10 = (&___Pt2a2)->get_X_0();
		int64_t L_11 = (&___Pt1b1)->get_X_0();
		if ((!(((uint32_t)((((int64_t)L_8) > ((int64_t)L_9))? 1 : 0)) == ((uint32_t)((((int64_t)L_10) < ((int64_t)L_11))? 1 : 0)))))
		{
			goto IL_0075;
		}
	}
	{
		return (bool)1;
	}

IL_0075:
	{
		int64_t L_12 = (&___Pt2b3)->get_X_0();
		int64_t L_13 = (&___Pt1a0)->get_X_0();
		int64_t L_14 = (&___Pt2b3)->get_X_0();
		int64_t L_15 = (&___Pt1b1)->get_X_0();
		if ((!(((uint32_t)((((int64_t)L_12) > ((int64_t)L_13))? 1 : 0)) == ((uint32_t)((((int64_t)L_14) < ((int64_t)L_15))? 1 : 0)))))
		{
			goto IL_009c;
		}
	}
	{
		return (bool)1;
	}

IL_009c:
	{
		int64_t L_16 = (&___Pt1a0)->get_X_0();
		int64_t L_17 = (&___Pt2a2)->get_X_0();
		if ((!(((uint64_t)L_16) == ((uint64_t)L_17))))
		{
			goto IL_00c4;
		}
	}
	{
		int64_t L_18 = (&___Pt1b1)->get_X_0();
		int64_t L_19 = (&___Pt2b3)->get_X_0();
		if ((!(((uint64_t)L_18) == ((uint64_t)L_19))))
		{
			goto IL_00c4;
		}
	}
	{
		return (bool)1;
	}

IL_00c4:
	{
		int64_t L_20 = (&___Pt1a0)->get_X_0();
		int64_t L_21 = (&___Pt2b3)->get_X_0();
		if ((!(((uint64_t)L_20) == ((uint64_t)L_21))))
		{
			goto IL_00ec;
		}
	}
	{
		int64_t L_22 = (&___Pt1b1)->get_X_0();
		int64_t L_23 = (&___Pt2a2)->get_X_0();
		if ((!(((uint64_t)L_22) == ((uint64_t)L_23))))
		{
			goto IL_00ec;
		}
	}
	{
		return (bool)1;
	}

IL_00ec:
	{
		return (bool)0;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::SetHoleState(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.OutRec)
extern const MethodInfo* List_1_get_Item_m2053160358_MethodInfo_var;
extern const uint32_t Clipper_SetHoleState_m1938498398_MetadataUsageId;
extern "C"  void Clipper_SetHoleState_m1938498398 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e0, OutRec_t3245805284 * ___outRec1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_SetHoleState_m1938498398_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	TEdge_t3950806139 * V_1 = NULL;
	{
		V_0 = (bool)0;
		TEdge_t3950806139 * L_0 = ___e0;
		NullCheck(L_0);
		TEdge_t3950806139 * L_1 = L_0->get_PrevInAEL_15();
		V_1 = L_1;
		goto IL_0048;
	}

IL_000e:
	{
		TEdge_t3950806139 * L_2 = V_1;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_OutIdx_10();
		if ((((int32_t)L_3) < ((int32_t)0)))
		{
			goto IL_0041;
		}
	}
	{
		bool L_4 = V_0;
		V_0 = (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
		OutRec_t3245805284 * L_5 = ___outRec1;
		NullCheck(L_5);
		OutRec_t3245805284 * L_6 = L_5->get_FirstLeft_3();
		if (L_6)
		{
			goto IL_0041;
		}
	}
	{
		OutRec_t3245805284 * L_7 = ___outRec1;
		List_1_t319023540 * L_8 = __this->get_m_PolyOuts_15();
		TEdge_t3950806139 * L_9 = V_1;
		NullCheck(L_9);
		int32_t L_10 = L_9->get_OutIdx_10();
		NullCheck(L_8);
		OutRec_t3245805284 * L_11 = List_1_get_Item_m2053160358(L_8, L_10, /*hidden argument*/List_1_get_Item_m2053160358_MethodInfo_var);
		NullCheck(L_7);
		L_7->set_FirstLeft_3(L_11);
	}

IL_0041:
	{
		TEdge_t3950806139 * L_12 = V_1;
		NullCheck(L_12);
		TEdge_t3950806139 * L_13 = L_12->get_PrevInAEL_15();
		V_1 = L_13;
	}

IL_0048:
	{
		TEdge_t3950806139 * L_14 = V_1;
		if (L_14)
		{
			goto IL_000e;
		}
	}
	{
		bool L_15 = V_0;
		if (!L_15)
		{
			goto IL_005b;
		}
	}
	{
		OutRec_t3245805284 * L_16 = ___outRec1;
		NullCheck(L_16);
		L_16->set_IsHole_1((bool)1);
	}

IL_005b:
	{
		return;
	}
}
// System.Double Pathfinding.ClipperLib.Clipper::GetDx(Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint)
extern "C"  double Clipper_GetDx_m3219334801 (Clipper_t636949239 * __this, IntPoint_t3326126179  ___pt10, IntPoint_t3326126179  ___pt21, const MethodInfo* method)
{
	{
		int64_t L_0 = (&___pt10)->get_Y_1();
		int64_t L_1 = (&___pt21)->get_Y_1();
		if ((!(((uint64_t)L_0) == ((uint64_t)L_1))))
		{
			goto IL_001d;
		}
	}
	{
		return (-3.4E+38);
	}

IL_001d:
	{
		int64_t L_2 = (&___pt21)->get_X_0();
		int64_t L_3 = (&___pt10)->get_X_0();
		int64_t L_4 = (&___pt21)->get_Y_1();
		int64_t L_5 = (&___pt10)->get_Y_1();
		return ((double)((double)(((double)((double)((int64_t)((int64_t)L_2-(int64_t)L_3)))))/(double)(((double)((double)((int64_t)((int64_t)L_4-(int64_t)L_5)))))));
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::FirstIsBottomPt(Pathfinding.ClipperLib.OutPt,Pathfinding.ClipperLib.OutPt)
extern "C"  bool Clipper_FirstIsBottomPt_m2070132493 (Clipper_t636949239 * __this, OutPt_t3947633180 * ___btmPt10, OutPt_t3947633180 * ___btmPt21, const MethodInfo* method)
{
	OutPt_t3947633180 * V_0 = NULL;
	double V_1 = 0.0;
	double V_2 = 0.0;
	double V_3 = 0.0;
	double V_4 = 0.0;
	int32_t G_B21_0 = 0;
	int32_t G_B23_0 = 0;
	{
		OutPt_t3947633180 * L_0 = ___btmPt10;
		NullCheck(L_0);
		OutPt_t3947633180 * L_1 = L_0->get_Prev_3();
		V_0 = L_1;
		goto IL_0013;
	}

IL_000c:
	{
		OutPt_t3947633180 * L_2 = V_0;
		NullCheck(L_2);
		OutPt_t3947633180 * L_3 = L_2->get_Prev_3();
		V_0 = L_3;
	}

IL_0013:
	{
		OutPt_t3947633180 * L_4 = V_0;
		NullCheck(L_4);
		IntPoint_t3326126179  L_5 = L_4->get_Pt_1();
		OutPt_t3947633180 * L_6 = ___btmPt10;
		NullCheck(L_6);
		IntPoint_t3326126179  L_7 = L_6->get_Pt_1();
		bool L_8 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0030;
		}
	}
	{
		OutPt_t3947633180 * L_9 = V_0;
		OutPt_t3947633180 * L_10 = ___btmPt10;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_9) == ((Il2CppObject*)(OutPt_t3947633180 *)L_10))))
		{
			goto IL_000c;
		}
	}

IL_0030:
	{
		OutPt_t3947633180 * L_11 = ___btmPt10;
		NullCheck(L_11);
		IntPoint_t3326126179  L_12 = L_11->get_Pt_1();
		OutPt_t3947633180 * L_13 = V_0;
		NullCheck(L_13);
		IntPoint_t3326126179  L_14 = L_13->get_Pt_1();
		double L_15 = Clipper_GetDx_m3219334801(__this, L_12, L_14, /*hidden argument*/NULL);
		double L_16 = fabs(L_15);
		V_1 = L_16;
		OutPt_t3947633180 * L_17 = ___btmPt10;
		NullCheck(L_17);
		OutPt_t3947633180 * L_18 = L_17->get_Next_2();
		V_0 = L_18;
		goto IL_005b;
	}

IL_0054:
	{
		OutPt_t3947633180 * L_19 = V_0;
		NullCheck(L_19);
		OutPt_t3947633180 * L_20 = L_19->get_Next_2();
		V_0 = L_20;
	}

IL_005b:
	{
		OutPt_t3947633180 * L_21 = V_0;
		NullCheck(L_21);
		IntPoint_t3326126179  L_22 = L_21->get_Pt_1();
		OutPt_t3947633180 * L_23 = ___btmPt10;
		NullCheck(L_23);
		IntPoint_t3326126179  L_24 = L_23->get_Pt_1();
		bool L_25 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_22, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_0078;
		}
	}
	{
		OutPt_t3947633180 * L_26 = V_0;
		OutPt_t3947633180 * L_27 = ___btmPt10;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_26) == ((Il2CppObject*)(OutPt_t3947633180 *)L_27))))
		{
			goto IL_0054;
		}
	}

IL_0078:
	{
		OutPt_t3947633180 * L_28 = ___btmPt10;
		NullCheck(L_28);
		IntPoint_t3326126179  L_29 = L_28->get_Pt_1();
		OutPt_t3947633180 * L_30 = V_0;
		NullCheck(L_30);
		IntPoint_t3326126179  L_31 = L_30->get_Pt_1();
		double L_32 = Clipper_GetDx_m3219334801(__this, L_29, L_31, /*hidden argument*/NULL);
		double L_33 = fabs(L_32);
		V_2 = L_33;
		OutPt_t3947633180 * L_34 = ___btmPt21;
		NullCheck(L_34);
		OutPt_t3947633180 * L_35 = L_34->get_Prev_3();
		V_0 = L_35;
		goto IL_00a3;
	}

IL_009c:
	{
		OutPt_t3947633180 * L_36 = V_0;
		NullCheck(L_36);
		OutPt_t3947633180 * L_37 = L_36->get_Prev_3();
		V_0 = L_37;
	}

IL_00a3:
	{
		OutPt_t3947633180 * L_38 = V_0;
		NullCheck(L_38);
		IntPoint_t3326126179  L_39 = L_38->get_Pt_1();
		OutPt_t3947633180 * L_40 = ___btmPt21;
		NullCheck(L_40);
		IntPoint_t3326126179  L_41 = L_40->get_Pt_1();
		bool L_42 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_39, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_00c0;
		}
	}
	{
		OutPt_t3947633180 * L_43 = V_0;
		OutPt_t3947633180 * L_44 = ___btmPt21;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_43) == ((Il2CppObject*)(OutPt_t3947633180 *)L_44))))
		{
			goto IL_009c;
		}
	}

IL_00c0:
	{
		OutPt_t3947633180 * L_45 = ___btmPt21;
		NullCheck(L_45);
		IntPoint_t3326126179  L_46 = L_45->get_Pt_1();
		OutPt_t3947633180 * L_47 = V_0;
		NullCheck(L_47);
		IntPoint_t3326126179  L_48 = L_47->get_Pt_1();
		double L_49 = Clipper_GetDx_m3219334801(__this, L_46, L_48, /*hidden argument*/NULL);
		double L_50 = fabs(L_49);
		V_3 = L_50;
		OutPt_t3947633180 * L_51 = ___btmPt21;
		NullCheck(L_51);
		OutPt_t3947633180 * L_52 = L_51->get_Next_2();
		V_0 = L_52;
		goto IL_00eb;
	}

IL_00e4:
	{
		OutPt_t3947633180 * L_53 = V_0;
		NullCheck(L_53);
		OutPt_t3947633180 * L_54 = L_53->get_Next_2();
		V_0 = L_54;
	}

IL_00eb:
	{
		OutPt_t3947633180 * L_55 = V_0;
		NullCheck(L_55);
		IntPoint_t3326126179  L_56 = L_55->get_Pt_1();
		OutPt_t3947633180 * L_57 = ___btmPt21;
		NullCheck(L_57);
		IntPoint_t3326126179  L_58 = L_57->get_Pt_1();
		bool L_59 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_56, L_58, /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_0108;
		}
	}
	{
		OutPt_t3947633180 * L_60 = V_0;
		OutPt_t3947633180 * L_61 = ___btmPt21;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_60) == ((Il2CppObject*)(OutPt_t3947633180 *)L_61))))
		{
			goto IL_00e4;
		}
	}

IL_0108:
	{
		OutPt_t3947633180 * L_62 = ___btmPt21;
		NullCheck(L_62);
		IntPoint_t3326126179  L_63 = L_62->get_Pt_1();
		OutPt_t3947633180 * L_64 = V_0;
		NullCheck(L_64);
		IntPoint_t3326126179  L_65 = L_64->get_Pt_1();
		double L_66 = Clipper_GetDx_m3219334801(__this, L_63, L_65, /*hidden argument*/NULL);
		double L_67 = fabs(L_66);
		V_4 = L_67;
		double L_68 = V_1;
		double L_69 = V_3;
		if ((!(((double)L_68) >= ((double)L_69))))
		{
			goto IL_0130;
		}
	}
	{
		double L_70 = V_1;
		double L_71 = V_4;
		if ((((double)L_70) >= ((double)L_71)))
		{
			goto IL_0144;
		}
	}

IL_0130:
	{
		double L_72 = V_2;
		double L_73 = V_3;
		if ((!(((double)L_72) >= ((double)L_73))))
		{
			goto IL_0141;
		}
	}
	{
		double L_74 = V_2;
		double L_75 = V_4;
		G_B21_0 = ((((int32_t)((!(((double)L_74) >= ((double)L_75)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0142;
	}

IL_0141:
	{
		G_B21_0 = 0;
	}

IL_0142:
	{
		G_B23_0 = G_B21_0;
		goto IL_0145;
	}

IL_0144:
	{
		G_B23_0 = 1;
	}

IL_0145:
	{
		return (bool)G_B23_0;
	}
}
// Pathfinding.ClipperLib.OutPt Pathfinding.ClipperLib.Clipper::GetBottomPt(Pathfinding.ClipperLib.OutPt)
extern "C"  OutPt_t3947633180 * Clipper_GetBottomPt_m1952022412 (Clipper_t636949239 * __this, OutPt_t3947633180 * ___pp0, const MethodInfo* method)
{
	OutPt_t3947633180 * V_0 = NULL;
	OutPt_t3947633180 * V_1 = NULL;
	{
		V_0 = (OutPt_t3947633180 *)NULL;
		OutPt_t3947633180 * L_0 = ___pp0;
		NullCheck(L_0);
		OutPt_t3947633180 * L_1 = L_0->get_Next_2();
		V_1 = L_1;
		goto IL_00af;
	}

IL_000e:
	{
		OutPt_t3947633180 * L_2 = V_1;
		NullCheck(L_2);
		IntPoint_t3326126179 * L_3 = L_2->get_address_of_Pt_1();
		int64_t L_4 = L_3->get_Y_1();
		OutPt_t3947633180 * L_5 = ___pp0;
		NullCheck(L_5);
		IntPoint_t3326126179 * L_6 = L_5->get_address_of_Pt_1();
		int64_t L_7 = L_6->get_Y_1();
		if ((((int64_t)L_4) <= ((int64_t)L_7)))
		{
			goto IL_0033;
		}
	}
	{
		OutPt_t3947633180 * L_8 = V_1;
		___pp0 = L_8;
		V_0 = (OutPt_t3947633180 *)NULL;
		goto IL_00a8;
	}

IL_0033:
	{
		OutPt_t3947633180 * L_9 = V_1;
		NullCheck(L_9);
		IntPoint_t3326126179 * L_10 = L_9->get_address_of_Pt_1();
		int64_t L_11 = L_10->get_Y_1();
		OutPt_t3947633180 * L_12 = ___pp0;
		NullCheck(L_12);
		IntPoint_t3326126179 * L_13 = L_12->get_address_of_Pt_1();
		int64_t L_14 = L_13->get_Y_1();
		if ((!(((uint64_t)L_11) == ((uint64_t)L_14))))
		{
			goto IL_00a8;
		}
	}
	{
		OutPt_t3947633180 * L_15 = V_1;
		NullCheck(L_15);
		IntPoint_t3326126179 * L_16 = L_15->get_address_of_Pt_1();
		int64_t L_17 = L_16->get_X_0();
		OutPt_t3947633180 * L_18 = ___pp0;
		NullCheck(L_18);
		IntPoint_t3326126179 * L_19 = L_18->get_address_of_Pt_1();
		int64_t L_20 = L_19->get_X_0();
		if ((((int64_t)L_17) > ((int64_t)L_20)))
		{
			goto IL_00a8;
		}
	}
	{
		OutPt_t3947633180 * L_21 = V_1;
		NullCheck(L_21);
		IntPoint_t3326126179 * L_22 = L_21->get_address_of_Pt_1();
		int64_t L_23 = L_22->get_X_0();
		OutPt_t3947633180 * L_24 = ___pp0;
		NullCheck(L_24);
		IntPoint_t3326126179 * L_25 = L_24->get_address_of_Pt_1();
		int64_t L_26 = L_25->get_X_0();
		if ((((int64_t)L_23) >= ((int64_t)L_26)))
		{
			goto IL_008e;
		}
	}
	{
		V_0 = (OutPt_t3947633180 *)NULL;
		OutPt_t3947633180 * L_27 = V_1;
		___pp0 = L_27;
		goto IL_00a8;
	}

IL_008e:
	{
		OutPt_t3947633180 * L_28 = V_1;
		NullCheck(L_28);
		OutPt_t3947633180 * L_29 = L_28->get_Next_2();
		OutPt_t3947633180 * L_30 = ___pp0;
		if ((((Il2CppObject*)(OutPt_t3947633180 *)L_29) == ((Il2CppObject*)(OutPt_t3947633180 *)L_30)))
		{
			goto IL_00a8;
		}
	}
	{
		OutPt_t3947633180 * L_31 = V_1;
		NullCheck(L_31);
		OutPt_t3947633180 * L_32 = L_31->get_Prev_3();
		OutPt_t3947633180 * L_33 = ___pp0;
		if ((((Il2CppObject*)(OutPt_t3947633180 *)L_32) == ((Il2CppObject*)(OutPt_t3947633180 *)L_33)))
		{
			goto IL_00a8;
		}
	}
	{
		OutPt_t3947633180 * L_34 = V_1;
		V_0 = L_34;
	}

IL_00a8:
	{
		OutPt_t3947633180 * L_35 = V_1;
		NullCheck(L_35);
		OutPt_t3947633180 * L_36 = L_35->get_Next_2();
		V_1 = L_36;
	}

IL_00af:
	{
		OutPt_t3947633180 * L_37 = V_1;
		OutPt_t3947633180 * L_38 = ___pp0;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_37) == ((Il2CppObject*)(OutPt_t3947633180 *)L_38))))
		{
			goto IL_000e;
		}
	}
	{
		OutPt_t3947633180 * L_39 = V_0;
		if (!L_39)
		{
			goto IL_0101;
		}
	}
	{
		goto IL_00fa;
	}

IL_00c1:
	{
		OutPt_t3947633180 * L_40 = V_1;
		OutPt_t3947633180 * L_41 = V_0;
		bool L_42 = Clipper_FirstIsBottomPt_m2070132493(__this, L_40, L_41, /*hidden argument*/NULL);
		if (L_42)
		{
			goto IL_00d1;
		}
	}
	{
		OutPt_t3947633180 * L_43 = V_0;
		___pp0 = L_43;
	}

IL_00d1:
	{
		OutPt_t3947633180 * L_44 = V_0;
		NullCheck(L_44);
		OutPt_t3947633180 * L_45 = L_44->get_Next_2();
		V_0 = L_45;
		goto IL_00e4;
	}

IL_00dd:
	{
		OutPt_t3947633180 * L_46 = V_0;
		NullCheck(L_46);
		OutPt_t3947633180 * L_47 = L_46->get_Next_2();
		V_0 = L_47;
	}

IL_00e4:
	{
		OutPt_t3947633180 * L_48 = V_0;
		NullCheck(L_48);
		IntPoint_t3326126179  L_49 = L_48->get_Pt_1();
		OutPt_t3947633180 * L_50 = ___pp0;
		NullCheck(L_50);
		IntPoint_t3326126179  L_51 = L_50->get_Pt_1();
		bool L_52 = IntPoint_op_Inequality_m3996904515(NULL /*static, unused*/, L_49, L_51, /*hidden argument*/NULL);
		if (L_52)
		{
			goto IL_00dd;
		}
	}

IL_00fa:
	{
		OutPt_t3947633180 * L_53 = V_0;
		OutPt_t3947633180 * L_54 = V_1;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_53) == ((Il2CppObject*)(OutPt_t3947633180 *)L_54))))
		{
			goto IL_00c1;
		}
	}

IL_0101:
	{
		OutPt_t3947633180 * L_55 = ___pp0;
		return L_55;
	}
}
// Pathfinding.ClipperLib.OutRec Pathfinding.ClipperLib.Clipper::GetLowermostRec(Pathfinding.ClipperLib.OutRec,Pathfinding.ClipperLib.OutRec)
extern "C"  OutRec_t3245805284 * Clipper_GetLowermostRec_m3541598937 (Clipper_t636949239 * __this, OutRec_t3245805284 * ___outRec10, OutRec_t3245805284 * ___outRec21, const MethodInfo* method)
{
	OutPt_t3947633180 * V_0 = NULL;
	OutPt_t3947633180 * V_1 = NULL;
	{
		OutRec_t3245805284 * L_0 = ___outRec10;
		NullCheck(L_0);
		OutPt_t3947633180 * L_1 = L_0->get_BottomPt_5();
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		OutRec_t3245805284 * L_2 = ___outRec10;
		OutRec_t3245805284 * L_3 = ___outRec10;
		NullCheck(L_3);
		OutPt_t3947633180 * L_4 = L_3->get_Pts_4();
		OutPt_t3947633180 * L_5 = Clipper_GetBottomPt_m1952022412(__this, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_BottomPt_5(L_5);
	}

IL_001d:
	{
		OutRec_t3245805284 * L_6 = ___outRec21;
		NullCheck(L_6);
		OutPt_t3947633180 * L_7 = L_6->get_BottomPt_5();
		if (L_7)
		{
			goto IL_003a;
		}
	}
	{
		OutRec_t3245805284 * L_8 = ___outRec21;
		OutRec_t3245805284 * L_9 = ___outRec21;
		NullCheck(L_9);
		OutPt_t3947633180 * L_10 = L_9->get_Pts_4();
		OutPt_t3947633180 * L_11 = Clipper_GetBottomPt_m1952022412(__this, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		L_8->set_BottomPt_5(L_11);
	}

IL_003a:
	{
		OutRec_t3245805284 * L_12 = ___outRec10;
		NullCheck(L_12);
		OutPt_t3947633180 * L_13 = L_12->get_BottomPt_5();
		V_0 = L_13;
		OutRec_t3245805284 * L_14 = ___outRec21;
		NullCheck(L_14);
		OutPt_t3947633180 * L_15 = L_14->get_BottomPt_5();
		V_1 = L_15;
		OutPt_t3947633180 * L_16 = V_0;
		NullCheck(L_16);
		IntPoint_t3326126179 * L_17 = L_16->get_address_of_Pt_1();
		int64_t L_18 = L_17->get_Y_1();
		OutPt_t3947633180 * L_19 = V_1;
		NullCheck(L_19);
		IntPoint_t3326126179 * L_20 = L_19->get_address_of_Pt_1();
		int64_t L_21 = L_20->get_Y_1();
		if ((((int64_t)L_18) <= ((int64_t)L_21)))
		{
			goto IL_0065;
		}
	}
	{
		OutRec_t3245805284 * L_22 = ___outRec10;
		return L_22;
	}

IL_0065:
	{
		OutPt_t3947633180 * L_23 = V_0;
		NullCheck(L_23);
		IntPoint_t3326126179 * L_24 = L_23->get_address_of_Pt_1();
		int64_t L_25 = L_24->get_Y_1();
		OutPt_t3947633180 * L_26 = V_1;
		NullCheck(L_26);
		IntPoint_t3326126179 * L_27 = L_26->get_address_of_Pt_1();
		int64_t L_28 = L_27->get_Y_1();
		if ((((int64_t)L_25) >= ((int64_t)L_28)))
		{
			goto IL_0082;
		}
	}
	{
		OutRec_t3245805284 * L_29 = ___outRec21;
		return L_29;
	}

IL_0082:
	{
		OutPt_t3947633180 * L_30 = V_0;
		NullCheck(L_30);
		IntPoint_t3326126179 * L_31 = L_30->get_address_of_Pt_1();
		int64_t L_32 = L_31->get_X_0();
		OutPt_t3947633180 * L_33 = V_1;
		NullCheck(L_33);
		IntPoint_t3326126179 * L_34 = L_33->get_address_of_Pt_1();
		int64_t L_35 = L_34->get_X_0();
		if ((((int64_t)L_32) >= ((int64_t)L_35)))
		{
			goto IL_009f;
		}
	}
	{
		OutRec_t3245805284 * L_36 = ___outRec10;
		return L_36;
	}

IL_009f:
	{
		OutPt_t3947633180 * L_37 = V_0;
		NullCheck(L_37);
		IntPoint_t3326126179 * L_38 = L_37->get_address_of_Pt_1();
		int64_t L_39 = L_38->get_X_0();
		OutPt_t3947633180 * L_40 = V_1;
		NullCheck(L_40);
		IntPoint_t3326126179 * L_41 = L_40->get_address_of_Pt_1();
		int64_t L_42 = L_41->get_X_0();
		if ((((int64_t)L_39) <= ((int64_t)L_42)))
		{
			goto IL_00bc;
		}
	}
	{
		OutRec_t3245805284 * L_43 = ___outRec21;
		return L_43;
	}

IL_00bc:
	{
		OutPt_t3947633180 * L_44 = V_0;
		NullCheck(L_44);
		OutPt_t3947633180 * L_45 = L_44->get_Next_2();
		OutPt_t3947633180 * L_46 = V_0;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_45) == ((Il2CppObject*)(OutPt_t3947633180 *)L_46))))
		{
			goto IL_00ca;
		}
	}
	{
		OutRec_t3245805284 * L_47 = ___outRec21;
		return L_47;
	}

IL_00ca:
	{
		OutPt_t3947633180 * L_48 = V_1;
		NullCheck(L_48);
		OutPt_t3947633180 * L_49 = L_48->get_Next_2();
		OutPt_t3947633180 * L_50 = V_1;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_49) == ((Il2CppObject*)(OutPt_t3947633180 *)L_50))))
		{
			goto IL_00d8;
		}
	}
	{
		OutRec_t3245805284 * L_51 = ___outRec10;
		return L_51;
	}

IL_00d8:
	{
		OutPt_t3947633180 * L_52 = V_0;
		OutPt_t3947633180 * L_53 = V_1;
		bool L_54 = Clipper_FirstIsBottomPt_m2070132493(__this, L_52, L_53, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_00e7;
		}
	}
	{
		OutRec_t3245805284 * L_55 = ___outRec10;
		return L_55;
	}

IL_00e7:
	{
		OutRec_t3245805284 * L_56 = ___outRec21;
		return L_56;
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::Param1RightOfParam2(Pathfinding.ClipperLib.OutRec,Pathfinding.ClipperLib.OutRec)
extern "C"  bool Clipper_Param1RightOfParam2_m667347646 (Clipper_t636949239 * __this, OutRec_t3245805284 * ___outRec10, OutRec_t3245805284 * ___outRec21, const MethodInfo* method)
{

IL_0000:
	{
		OutRec_t3245805284 * L_0 = ___outRec10;
		NullCheck(L_0);
		OutRec_t3245805284 * L_1 = L_0->get_FirstLeft_3();
		___outRec10 = L_1;
		OutRec_t3245805284 * L_2 = ___outRec10;
		OutRec_t3245805284 * L_3 = ___outRec21;
		if ((!(((Il2CppObject*)(OutRec_t3245805284 *)L_2) == ((Il2CppObject*)(OutRec_t3245805284 *)L_3))))
		{
			goto IL_0011;
		}
	}
	{
		return (bool)1;
	}

IL_0011:
	{
		OutRec_t3245805284 * L_4 = ___outRec10;
		if (L_4)
		{
			goto IL_0000;
		}
	}
	{
		return (bool)0;
	}
}
// Pathfinding.ClipperLib.OutRec Pathfinding.ClipperLib.Clipper::GetOutRec(System.Int32)
extern const MethodInfo* List_1_get_Item_m2053160358_MethodInfo_var;
extern const uint32_t Clipper_GetOutRec_m918585808_MetadataUsageId;
extern "C"  OutRec_t3245805284 * Clipper_GetOutRec_m918585808 (Clipper_t636949239 * __this, int32_t ___idx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_GetOutRec_m918585808_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	OutRec_t3245805284 * V_0 = NULL;
	{
		List_1_t319023540 * L_0 = __this->get_m_PolyOuts_15();
		int32_t L_1 = ___idx0;
		NullCheck(L_0);
		OutRec_t3245805284 * L_2 = List_1_get_Item_m2053160358(L_0, L_1, /*hidden argument*/List_1_get_Item_m2053160358_MethodInfo_var);
		V_0 = L_2;
		goto IL_0024;
	}

IL_0012:
	{
		List_1_t319023540 * L_3 = __this->get_m_PolyOuts_15();
		OutRec_t3245805284 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_Idx_0();
		NullCheck(L_3);
		OutRec_t3245805284 * L_6 = List_1_get_Item_m2053160358(L_3, L_5, /*hidden argument*/List_1_get_Item_m2053160358_MethodInfo_var);
		V_0 = L_6;
	}

IL_0024:
	{
		OutRec_t3245805284 * L_7 = V_0;
		List_1_t319023540 * L_8 = __this->get_m_PolyOuts_15();
		OutRec_t3245805284 * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = L_9->get_Idx_0();
		NullCheck(L_8);
		OutRec_t3245805284 * L_11 = List_1_get_Item_m2053160358(L_8, L_10, /*hidden argument*/List_1_get_Item_m2053160358_MethodInfo_var);
		if ((!(((Il2CppObject*)(OutRec_t3245805284 *)L_7) == ((Il2CppObject*)(OutRec_t3245805284 *)L_11))))
		{
			goto IL_0012;
		}
	}
	{
		OutRec_t3245805284 * L_12 = V_0;
		return L_12;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::AppendPolygon(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge)
extern const MethodInfo* List_1_get_Item_m2053160358_MethodInfo_var;
extern const uint32_t Clipper_AppendPolygon_m921090842_MetadataUsageId;
extern "C"  void Clipper_AppendPolygon_m921090842 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e10, TEdge_t3950806139 * ___e21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_AppendPolygon_m921090842_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	OutRec_t3245805284 * V_0 = NULL;
	OutRec_t3245805284 * V_1 = NULL;
	OutRec_t3245805284 * V_2 = NULL;
	OutPt_t3947633180 * V_3 = NULL;
	OutPt_t3947633180 * V_4 = NULL;
	OutPt_t3947633180 * V_5 = NULL;
	OutPt_t3947633180 * V_6 = NULL;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	TEdge_t3950806139 * V_10 = NULL;
	{
		List_1_t319023540 * L_0 = __this->get_m_PolyOuts_15();
		TEdge_t3950806139 * L_1 = ___e10;
		NullCheck(L_1);
		int32_t L_2 = L_1->get_OutIdx_10();
		NullCheck(L_0);
		OutRec_t3245805284 * L_3 = List_1_get_Item_m2053160358(L_0, L_2, /*hidden argument*/List_1_get_Item_m2053160358_MethodInfo_var);
		V_0 = L_3;
		List_1_t319023540 * L_4 = __this->get_m_PolyOuts_15();
		TEdge_t3950806139 * L_5 = ___e21;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_OutIdx_10();
		NullCheck(L_4);
		OutRec_t3245805284 * L_7 = List_1_get_Item_m2053160358(L_4, L_6, /*hidden argument*/List_1_get_Item_m2053160358_MethodInfo_var);
		V_1 = L_7;
		OutRec_t3245805284 * L_8 = V_0;
		OutRec_t3245805284 * L_9 = V_1;
		bool L_10 = Clipper_Param1RightOfParam2_m667347646(__this, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0038;
		}
	}
	{
		OutRec_t3245805284 * L_11 = V_1;
		V_2 = L_11;
		goto IL_0055;
	}

IL_0038:
	{
		OutRec_t3245805284 * L_12 = V_1;
		OutRec_t3245805284 * L_13 = V_0;
		bool L_14 = Clipper_Param1RightOfParam2_m667347646(__this, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_004c;
		}
	}
	{
		OutRec_t3245805284 * L_15 = V_0;
		V_2 = L_15;
		goto IL_0055;
	}

IL_004c:
	{
		OutRec_t3245805284 * L_16 = V_0;
		OutRec_t3245805284 * L_17 = V_1;
		OutRec_t3245805284 * L_18 = Clipper_GetLowermostRec_m3541598937(__this, L_16, L_17, /*hidden argument*/NULL);
		V_2 = L_18;
	}

IL_0055:
	{
		OutRec_t3245805284 * L_19 = V_0;
		NullCheck(L_19);
		OutPt_t3947633180 * L_20 = L_19->get_Pts_4();
		V_3 = L_20;
		OutPt_t3947633180 * L_21 = V_3;
		NullCheck(L_21);
		OutPt_t3947633180 * L_22 = L_21->get_Prev_3();
		V_4 = L_22;
		OutRec_t3245805284 * L_23 = V_1;
		NullCheck(L_23);
		OutPt_t3947633180 * L_24 = L_23->get_Pts_4();
		V_5 = L_24;
		OutPt_t3947633180 * L_25 = V_5;
		NullCheck(L_25);
		OutPt_t3947633180 * L_26 = L_25->get_Prev_3();
		V_6 = L_26;
		TEdge_t3950806139 * L_27 = ___e10;
		NullCheck(L_27);
		int32_t L_28 = L_27->get_Side_6();
		if (L_28)
		{
			goto IL_00f4;
		}
	}
	{
		TEdge_t3950806139 * L_29 = ___e21;
		NullCheck(L_29);
		int32_t L_30 = L_29->get_Side_6();
		if (L_30)
		{
			goto IL_00c2;
		}
	}
	{
		OutPt_t3947633180 * L_31 = V_5;
		Clipper_ReversePolyPtLinks_m59152439(__this, L_31, /*hidden argument*/NULL);
		OutPt_t3947633180 * L_32 = V_5;
		OutPt_t3947633180 * L_33 = V_3;
		NullCheck(L_32);
		L_32->set_Next_2(L_33);
		OutPt_t3947633180 * L_34 = V_3;
		OutPt_t3947633180 * L_35 = V_5;
		NullCheck(L_34);
		L_34->set_Prev_3(L_35);
		OutPt_t3947633180 * L_36 = V_4;
		OutPt_t3947633180 * L_37 = V_6;
		NullCheck(L_36);
		L_36->set_Next_2(L_37);
		OutPt_t3947633180 * L_38 = V_6;
		OutPt_t3947633180 * L_39 = V_4;
		NullCheck(L_38);
		L_38->set_Prev_3(L_39);
		OutRec_t3245805284 * L_40 = V_0;
		OutPt_t3947633180 * L_41 = V_6;
		NullCheck(L_40);
		L_40->set_Pts_4(L_41);
		goto IL_00ec;
	}

IL_00c2:
	{
		OutPt_t3947633180 * L_42 = V_6;
		OutPt_t3947633180 * L_43 = V_3;
		NullCheck(L_42);
		L_42->set_Next_2(L_43);
		OutPt_t3947633180 * L_44 = V_3;
		OutPt_t3947633180 * L_45 = V_6;
		NullCheck(L_44);
		L_44->set_Prev_3(L_45);
		OutPt_t3947633180 * L_46 = V_5;
		OutPt_t3947633180 * L_47 = V_4;
		NullCheck(L_46);
		L_46->set_Prev_3(L_47);
		OutPt_t3947633180 * L_48 = V_4;
		OutPt_t3947633180 * L_49 = V_5;
		NullCheck(L_48);
		L_48->set_Next_2(L_49);
		OutRec_t3245805284 * L_50 = V_0;
		OutPt_t3947633180 * L_51 = V_5;
		NullCheck(L_50);
		L_50->set_Pts_4(L_51);
	}

IL_00ec:
	{
		V_7 = 0;
		goto IL_0154;
	}

IL_00f4:
	{
		TEdge_t3950806139 * L_52 = ___e21;
		NullCheck(L_52);
		int32_t L_53 = L_52->get_Side_6();
		if ((!(((uint32_t)L_53) == ((uint32_t)1))))
		{
			goto IL_012f;
		}
	}
	{
		OutPt_t3947633180 * L_54 = V_5;
		Clipper_ReversePolyPtLinks_m59152439(__this, L_54, /*hidden argument*/NULL);
		OutPt_t3947633180 * L_55 = V_4;
		OutPt_t3947633180 * L_56 = V_6;
		NullCheck(L_55);
		L_55->set_Next_2(L_56);
		OutPt_t3947633180 * L_57 = V_6;
		OutPt_t3947633180 * L_58 = V_4;
		NullCheck(L_57);
		L_57->set_Prev_3(L_58);
		OutPt_t3947633180 * L_59 = V_5;
		OutPt_t3947633180 * L_60 = V_3;
		NullCheck(L_59);
		L_59->set_Next_2(L_60);
		OutPt_t3947633180 * L_61 = V_3;
		OutPt_t3947633180 * L_62 = V_5;
		NullCheck(L_61);
		L_61->set_Prev_3(L_62);
		goto IL_0151;
	}

IL_012f:
	{
		OutPt_t3947633180 * L_63 = V_4;
		OutPt_t3947633180 * L_64 = V_5;
		NullCheck(L_63);
		L_63->set_Next_2(L_64);
		OutPt_t3947633180 * L_65 = V_5;
		OutPt_t3947633180 * L_66 = V_4;
		NullCheck(L_65);
		L_65->set_Prev_3(L_66);
		OutPt_t3947633180 * L_67 = V_3;
		OutPt_t3947633180 * L_68 = V_6;
		NullCheck(L_67);
		L_67->set_Prev_3(L_68);
		OutPt_t3947633180 * L_69 = V_6;
		OutPt_t3947633180 * L_70 = V_3;
		NullCheck(L_69);
		L_69->set_Next_2(L_70);
	}

IL_0151:
	{
		V_7 = 1;
	}

IL_0154:
	{
		OutRec_t3245805284 * L_71 = V_0;
		NullCheck(L_71);
		L_71->set_BottomPt_5((OutPt_t3947633180 *)NULL);
		OutRec_t3245805284 * L_72 = V_2;
		OutRec_t3245805284 * L_73 = V_1;
		if ((!(((Il2CppObject*)(OutRec_t3245805284 *)L_72) == ((Il2CppObject*)(OutRec_t3245805284 *)L_73))))
		{
			goto IL_0186;
		}
	}
	{
		OutRec_t3245805284 * L_74 = V_1;
		NullCheck(L_74);
		OutRec_t3245805284 * L_75 = L_74->get_FirstLeft_3();
		OutRec_t3245805284 * L_76 = V_0;
		if ((((Il2CppObject*)(OutRec_t3245805284 *)L_75) == ((Il2CppObject*)(OutRec_t3245805284 *)L_76)))
		{
			goto IL_017a;
		}
	}
	{
		OutRec_t3245805284 * L_77 = V_0;
		OutRec_t3245805284 * L_78 = V_1;
		NullCheck(L_78);
		OutRec_t3245805284 * L_79 = L_78->get_FirstLeft_3();
		NullCheck(L_77);
		L_77->set_FirstLeft_3(L_79);
	}

IL_017a:
	{
		OutRec_t3245805284 * L_80 = V_0;
		OutRec_t3245805284 * L_81 = V_1;
		NullCheck(L_81);
		bool L_82 = L_81->get_IsHole_1();
		NullCheck(L_80);
		L_80->set_IsHole_1(L_82);
	}

IL_0186:
	{
		OutRec_t3245805284 * L_83 = V_1;
		NullCheck(L_83);
		L_83->set_Pts_4((OutPt_t3947633180 *)NULL);
		OutRec_t3245805284 * L_84 = V_1;
		NullCheck(L_84);
		L_84->set_BottomPt_5((OutPt_t3947633180 *)NULL);
		OutRec_t3245805284 * L_85 = V_1;
		OutRec_t3245805284 * L_86 = V_0;
		NullCheck(L_85);
		L_85->set_FirstLeft_3(L_86);
		TEdge_t3950806139 * L_87 = ___e10;
		NullCheck(L_87);
		int32_t L_88 = L_87->get_OutIdx_10();
		V_8 = L_88;
		TEdge_t3950806139 * L_89 = ___e21;
		NullCheck(L_89);
		int32_t L_90 = L_89->get_OutIdx_10();
		V_9 = L_90;
		TEdge_t3950806139 * L_91 = ___e10;
		NullCheck(L_91);
		L_91->set_OutIdx_10((-1));
		TEdge_t3950806139 * L_92 = ___e21;
		NullCheck(L_92);
		L_92->set_OutIdx_10((-1));
		TEdge_t3950806139 * L_93 = __this->get_m_ActiveEdges_18();
		V_10 = L_93;
		goto IL_01f4;
	}

IL_01c6:
	{
		TEdge_t3950806139 * L_94 = V_10;
		NullCheck(L_94);
		int32_t L_95 = L_94->get_OutIdx_10();
		int32_t L_96 = V_9;
		if ((!(((uint32_t)L_95) == ((uint32_t)L_96))))
		{
			goto IL_01eb;
		}
	}
	{
		TEdge_t3950806139 * L_97 = V_10;
		int32_t L_98 = V_8;
		NullCheck(L_97);
		L_97->set_OutIdx_10(L_98);
		TEdge_t3950806139 * L_99 = V_10;
		int32_t L_100 = V_7;
		NullCheck(L_99);
		L_99->set_Side_6(L_100);
		goto IL_01fb;
	}

IL_01eb:
	{
		TEdge_t3950806139 * L_101 = V_10;
		NullCheck(L_101);
		TEdge_t3950806139 * L_102 = L_101->get_NextInAEL_14();
		V_10 = L_102;
	}

IL_01f4:
	{
		TEdge_t3950806139 * L_103 = V_10;
		if (L_103)
		{
			goto IL_01c6;
		}
	}

IL_01fb:
	{
		OutRec_t3245805284 * L_104 = V_1;
		OutRec_t3245805284 * L_105 = V_0;
		NullCheck(L_105);
		int32_t L_106 = L_105->get_Idx_0();
		NullCheck(L_104);
		L_104->set_Idx_0(L_106);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::ReversePolyPtLinks(Pathfinding.ClipperLib.OutPt)
extern "C"  void Clipper_ReversePolyPtLinks_m59152439 (Clipper_t636949239 * __this, OutPt_t3947633180 * ___pp0, const MethodInfo* method)
{
	OutPt_t3947633180 * V_0 = NULL;
	OutPt_t3947633180 * V_1 = NULL;
	{
		OutPt_t3947633180 * L_0 = ___pp0;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		OutPt_t3947633180 * L_1 = ___pp0;
		V_0 = L_1;
	}

IL_0009:
	{
		OutPt_t3947633180 * L_2 = V_0;
		NullCheck(L_2);
		OutPt_t3947633180 * L_3 = L_2->get_Next_2();
		V_1 = L_3;
		OutPt_t3947633180 * L_4 = V_0;
		OutPt_t3947633180 * L_5 = V_0;
		NullCheck(L_5);
		OutPt_t3947633180 * L_6 = L_5->get_Prev_3();
		NullCheck(L_4);
		L_4->set_Next_2(L_6);
		OutPt_t3947633180 * L_7 = V_0;
		OutPt_t3947633180 * L_8 = V_1;
		NullCheck(L_7);
		L_7->set_Prev_3(L_8);
		OutPt_t3947633180 * L_9 = V_1;
		V_0 = L_9;
		OutPt_t3947633180 * L_10 = V_0;
		OutPt_t3947633180 * L_11 = ___pp0;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_10) == ((Il2CppObject*)(OutPt_t3947633180 *)L_11))))
		{
			goto IL_0009;
		}
	}
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::SwapSides(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge)
extern "C"  void Clipper_SwapSides_m1446163601 (Il2CppObject * __this /* static, unused */, TEdge_t3950806139 * ___edge10, TEdge_t3950806139 * ___edge21, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TEdge_t3950806139 * L_0 = ___edge10;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_Side_6();
		V_0 = L_1;
		TEdge_t3950806139 * L_2 = ___edge10;
		TEdge_t3950806139 * L_3 = ___edge21;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_Side_6();
		NullCheck(L_2);
		L_2->set_Side_6(L_4);
		TEdge_t3950806139 * L_5 = ___edge21;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		L_5->set_Side_6(L_6);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::SwapPolyIndexes(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge)
extern "C"  void Clipper_SwapPolyIndexes_m109539833 (Il2CppObject * __this /* static, unused */, TEdge_t3950806139 * ___edge10, TEdge_t3950806139 * ___edge21, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TEdge_t3950806139 * L_0 = ___edge10;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_OutIdx_10();
		V_0 = L_1;
		TEdge_t3950806139 * L_2 = ___edge10;
		TEdge_t3950806139 * L_3 = ___edge21;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_OutIdx_10();
		NullCheck(L_2);
		L_2->set_OutIdx_10(L_4);
		TEdge_t3950806139 * L_5 = ___edge21;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		L_5->set_OutIdx_10(L_6);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::IntersectEdges(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.IntPoint,System.Boolean)
extern "C"  void Clipper_IntersectEdges_m1787610885 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e10, TEdge_t3950806139 * ___e21, IntPoint_t3326126179  ___pt2, bool ___protect3, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int64_t V_12 = 0;
	int64_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B10_0 = 0;
	TEdge_t3950806139 * G_B24_0 = NULL;
	TEdge_t3950806139 * G_B23_0 = NULL;
	int32_t G_B25_0 = 0;
	TEdge_t3950806139 * G_B25_1 = NULL;
	TEdge_t3950806139 * G_B30_0 = NULL;
	TEdge_t3950806139 * G_B29_0 = NULL;
	int32_t G_B31_0 = 0;
	TEdge_t3950806139 * G_B31_1 = NULL;
	{
		bool L_0 = ___protect3;
		if (L_0)
		{
			goto IL_003f;
		}
	}
	{
		TEdge_t3950806139 * L_1 = ___e10;
		NullCheck(L_1);
		TEdge_t3950806139 * L_2 = L_1->get_NextInLML_13();
		if (L_2)
		{
			goto IL_003f;
		}
	}
	{
		TEdge_t3950806139 * L_3 = ___e10;
		NullCheck(L_3);
		IntPoint_t3326126179 * L_4 = L_3->get_address_of_Top_2();
		int64_t L_5 = L_4->get_X_0();
		int64_t L_6 = (&___pt2)->get_X_0();
		if ((!(((uint64_t)L_5) == ((uint64_t)L_6))))
		{
			goto IL_003f;
		}
	}
	{
		TEdge_t3950806139 * L_7 = ___e10;
		NullCheck(L_7);
		IntPoint_t3326126179 * L_8 = L_7->get_address_of_Top_2();
		int64_t L_9 = L_8->get_Y_1();
		int64_t L_10 = (&___pt2)->get_Y_1();
		G_B5_0 = ((((int64_t)L_9) == ((int64_t)L_10))? 1 : 0);
		goto IL_0040;
	}

IL_003f:
	{
		G_B5_0 = 0;
	}

IL_0040:
	{
		V_0 = (bool)G_B5_0;
		bool L_11 = ___protect3;
		if (L_11)
		{
			goto IL_0080;
		}
	}
	{
		TEdge_t3950806139 * L_12 = ___e21;
		NullCheck(L_12);
		TEdge_t3950806139 * L_13 = L_12->get_NextInLML_13();
		if (L_13)
		{
			goto IL_0080;
		}
	}
	{
		TEdge_t3950806139 * L_14 = ___e21;
		NullCheck(L_14);
		IntPoint_t3326126179 * L_15 = L_14->get_address_of_Top_2();
		int64_t L_16 = L_15->get_X_0();
		int64_t L_17 = (&___pt2)->get_X_0();
		if ((!(((uint64_t)L_16) == ((uint64_t)L_17))))
		{
			goto IL_0080;
		}
	}
	{
		TEdge_t3950806139 * L_18 = ___e21;
		NullCheck(L_18);
		IntPoint_t3326126179 * L_19 = L_18->get_address_of_Top_2();
		int64_t L_20 = L_19->get_Y_1();
		int64_t L_21 = (&___pt2)->get_Y_1();
		G_B10_0 = ((((int64_t)L_20) == ((int64_t)L_21))? 1 : 0);
		goto IL_0081;
	}

IL_0080:
	{
		G_B10_0 = 0;
	}

IL_0081:
	{
		V_1 = (bool)G_B10_0;
		TEdge_t3950806139 * L_22 = ___e10;
		NullCheck(L_22);
		int32_t L_23 = L_22->get_OutIdx_10();
		V_2 = (bool)((((int32_t)((((int32_t)L_23) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		TEdge_t3950806139 * L_24 = ___e21;
		NullCheck(L_24);
		int32_t L_25 = L_24->get_OutIdx_10();
		V_3 = (bool)((((int32_t)((((int32_t)L_25) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		TEdge_t3950806139 * L_26 = ___e10;
		NullCheck(L_26);
		int32_t L_27 = L_26->get_PolyTyp_5();
		TEdge_t3950806139 * L_28 = ___e21;
		NullCheck(L_28);
		int32_t L_29 = L_28->get_PolyTyp_5();
		if ((!(((uint32_t)L_27) == ((uint32_t)L_29))))
		{
			goto IL_014d;
		}
	}
	{
		TEdge_t3950806139 * L_30 = ___e10;
		bool L_31 = Clipper_IsEvenOddFillType_m201048083(__this, L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_00da;
		}
	}
	{
		TEdge_t3950806139 * L_32 = ___e10;
		NullCheck(L_32);
		int32_t L_33 = L_32->get_WindCnt_8();
		V_4 = L_33;
		TEdge_t3950806139 * L_34 = ___e10;
		TEdge_t3950806139 * L_35 = ___e21;
		NullCheck(L_35);
		int32_t L_36 = L_35->get_WindCnt_8();
		NullCheck(L_34);
		L_34->set_WindCnt_8(L_36);
		TEdge_t3950806139 * L_37 = ___e21;
		int32_t L_38 = V_4;
		NullCheck(L_37);
		L_37->set_WindCnt_8(L_38);
		goto IL_0148;
	}

IL_00da:
	{
		TEdge_t3950806139 * L_39 = ___e10;
		NullCheck(L_39);
		int32_t L_40 = L_39->get_WindCnt_8();
		TEdge_t3950806139 * L_41 = ___e21;
		NullCheck(L_41);
		int32_t L_42 = L_41->get_WindDelta_7();
		if (((int32_t)((int32_t)L_40+(int32_t)L_42)))
		{
			goto IL_00fe;
		}
	}
	{
		TEdge_t3950806139 * L_43 = ___e10;
		TEdge_t3950806139 * L_44 = ___e10;
		NullCheck(L_44);
		int32_t L_45 = L_44->get_WindCnt_8();
		NullCheck(L_43);
		L_43->set_WindCnt_8(((-L_45)));
		goto IL_0111;
	}

IL_00fe:
	{
		TEdge_t3950806139 * L_46 = ___e10;
		TEdge_t3950806139 * L_47 = L_46;
		NullCheck(L_47);
		int32_t L_48 = L_47->get_WindCnt_8();
		TEdge_t3950806139 * L_49 = ___e21;
		NullCheck(L_49);
		int32_t L_50 = L_49->get_WindDelta_7();
		NullCheck(L_47);
		L_47->set_WindCnt_8(((int32_t)((int32_t)L_48+(int32_t)L_50)));
	}

IL_0111:
	{
		TEdge_t3950806139 * L_51 = ___e21;
		NullCheck(L_51);
		int32_t L_52 = L_51->get_WindCnt_8();
		TEdge_t3950806139 * L_53 = ___e10;
		NullCheck(L_53);
		int32_t L_54 = L_53->get_WindDelta_7();
		if (((int32_t)((int32_t)L_52-(int32_t)L_54)))
		{
			goto IL_0135;
		}
	}
	{
		TEdge_t3950806139 * L_55 = ___e21;
		TEdge_t3950806139 * L_56 = ___e21;
		NullCheck(L_56);
		int32_t L_57 = L_56->get_WindCnt_8();
		NullCheck(L_55);
		L_55->set_WindCnt_8(((-L_57)));
		goto IL_0148;
	}

IL_0135:
	{
		TEdge_t3950806139 * L_58 = ___e21;
		TEdge_t3950806139 * L_59 = L_58;
		NullCheck(L_59);
		int32_t L_60 = L_59->get_WindCnt_8();
		TEdge_t3950806139 * L_61 = ___e10;
		NullCheck(L_61);
		int32_t L_62 = L_61->get_WindDelta_7();
		NullCheck(L_59);
		L_59->set_WindCnt_8(((int32_t)((int32_t)L_60-(int32_t)L_62)));
	}

IL_0148:
	{
		goto IL_01c5;
	}

IL_014d:
	{
		TEdge_t3950806139 * L_63 = ___e21;
		bool L_64 = Clipper_IsEvenOddFillType_m201048083(__this, L_63, /*hidden argument*/NULL);
		if (L_64)
		{
			goto IL_0171;
		}
	}
	{
		TEdge_t3950806139 * L_65 = ___e10;
		TEdge_t3950806139 * L_66 = L_65;
		NullCheck(L_66);
		int32_t L_67 = L_66->get_WindCnt2_9();
		TEdge_t3950806139 * L_68 = ___e21;
		NullCheck(L_68);
		int32_t L_69 = L_68->get_WindDelta_7();
		NullCheck(L_66);
		L_66->set_WindCnt2_9(((int32_t)((int32_t)L_67+(int32_t)L_69)));
		goto IL_0189;
	}

IL_0171:
	{
		TEdge_t3950806139 * L_70 = ___e10;
		TEdge_t3950806139 * L_71 = ___e10;
		NullCheck(L_71);
		int32_t L_72 = L_71->get_WindCnt2_9();
		G_B23_0 = L_70;
		if (L_72)
		{
			G_B24_0 = L_70;
			goto IL_0183;
		}
	}
	{
		G_B25_0 = 1;
		G_B25_1 = G_B23_0;
		goto IL_0184;
	}

IL_0183:
	{
		G_B25_0 = 0;
		G_B25_1 = G_B24_0;
	}

IL_0184:
	{
		NullCheck(G_B25_1);
		G_B25_1->set_WindCnt2_9(G_B25_0);
	}

IL_0189:
	{
		TEdge_t3950806139 * L_73 = ___e10;
		bool L_74 = Clipper_IsEvenOddFillType_m201048083(__this, L_73, /*hidden argument*/NULL);
		if (L_74)
		{
			goto IL_01ad;
		}
	}
	{
		TEdge_t3950806139 * L_75 = ___e21;
		TEdge_t3950806139 * L_76 = L_75;
		NullCheck(L_76);
		int32_t L_77 = L_76->get_WindCnt2_9();
		TEdge_t3950806139 * L_78 = ___e10;
		NullCheck(L_78);
		int32_t L_79 = L_78->get_WindDelta_7();
		NullCheck(L_76);
		L_76->set_WindCnt2_9(((int32_t)((int32_t)L_77-(int32_t)L_79)));
		goto IL_01c5;
	}

IL_01ad:
	{
		TEdge_t3950806139 * L_80 = ___e21;
		TEdge_t3950806139 * L_81 = ___e21;
		NullCheck(L_81);
		int32_t L_82 = L_81->get_WindCnt2_9();
		G_B29_0 = L_80;
		if (L_82)
		{
			G_B30_0 = L_80;
			goto IL_01bf;
		}
	}
	{
		G_B31_0 = 1;
		G_B31_1 = G_B29_0;
		goto IL_01c0;
	}

IL_01bf:
	{
		G_B31_0 = 0;
		G_B31_1 = G_B30_0;
	}

IL_01c0:
	{
		NullCheck(G_B31_1);
		G_B31_1->set_WindCnt2_9(G_B31_0);
	}

IL_01c5:
	{
		TEdge_t3950806139 * L_83 = ___e10;
		NullCheck(L_83);
		int32_t L_84 = L_83->get_PolyTyp_5();
		if (L_84)
		{
			goto IL_01e5;
		}
	}
	{
		int32_t L_85 = __this->get_m_SubjFillType_23();
		V_5 = L_85;
		int32_t L_86 = __this->get_m_ClipFillType_22();
		V_7 = L_86;
		goto IL_01f5;
	}

IL_01e5:
	{
		int32_t L_87 = __this->get_m_ClipFillType_22();
		V_5 = L_87;
		int32_t L_88 = __this->get_m_SubjFillType_23();
		V_7 = L_88;
	}

IL_01f5:
	{
		TEdge_t3950806139 * L_89 = ___e21;
		NullCheck(L_89);
		int32_t L_90 = L_89->get_PolyTyp_5();
		if (L_90)
		{
			goto IL_0215;
		}
	}
	{
		int32_t L_91 = __this->get_m_SubjFillType_23();
		V_6 = L_91;
		int32_t L_92 = __this->get_m_ClipFillType_22();
		V_8 = L_92;
		goto IL_0225;
	}

IL_0215:
	{
		int32_t L_93 = __this->get_m_ClipFillType_22();
		V_6 = L_93;
		int32_t L_94 = __this->get_m_SubjFillType_23();
		V_8 = L_94;
	}

IL_0225:
	{
		int32_t L_95 = V_5;
		V_11 = L_95;
		int32_t L_96 = V_11;
		if ((((int32_t)L_96) == ((int32_t)2)))
		{
			goto IL_023e;
		}
	}
	{
		int32_t L_97 = V_11;
		if ((((int32_t)L_97) == ((int32_t)3)))
		{
			goto IL_024b;
		}
	}
	{
		goto IL_0259;
	}

IL_023e:
	{
		TEdge_t3950806139 * L_98 = ___e10;
		NullCheck(L_98);
		int32_t L_99 = L_98->get_WindCnt_8();
		V_9 = L_99;
		goto IL_026b;
	}

IL_024b:
	{
		TEdge_t3950806139 * L_100 = ___e10;
		NullCheck(L_100);
		int32_t L_101 = L_100->get_WindCnt_8();
		V_9 = ((-L_101));
		goto IL_026b;
	}

IL_0259:
	{
		TEdge_t3950806139 * L_102 = ___e10;
		NullCheck(L_102);
		int32_t L_103 = L_102->get_WindCnt_8();
		int32_t L_104 = abs(L_103);
		V_9 = L_104;
		goto IL_026b;
	}

IL_026b:
	{
		int32_t L_105 = V_6;
		V_11 = L_105;
		int32_t L_106 = V_11;
		if ((((int32_t)L_106) == ((int32_t)2)))
		{
			goto IL_0284;
		}
	}
	{
		int32_t L_107 = V_11;
		if ((((int32_t)L_107) == ((int32_t)3)))
		{
			goto IL_0291;
		}
	}
	{
		goto IL_029f;
	}

IL_0284:
	{
		TEdge_t3950806139 * L_108 = ___e21;
		NullCheck(L_108);
		int32_t L_109 = L_108->get_WindCnt_8();
		V_10 = L_109;
		goto IL_02b1;
	}

IL_0291:
	{
		TEdge_t3950806139 * L_110 = ___e21;
		NullCheck(L_110);
		int32_t L_111 = L_110->get_WindCnt_8();
		V_10 = ((-L_111));
		goto IL_02b1;
	}

IL_029f:
	{
		TEdge_t3950806139 * L_112 = ___e21;
		NullCheck(L_112);
		int32_t L_113 = L_112->get_WindCnt_8();
		int32_t L_114 = abs(L_113);
		V_10 = L_114;
		goto IL_02b1;
	}

IL_02b1:
	{
		bool L_115 = V_2;
		if (!L_115)
		{
			goto IL_0337;
		}
	}
	{
		bool L_116 = V_3;
		if (!L_116)
		{
			goto IL_0337;
		}
	}
	{
		bool L_117 = V_0;
		if (L_117)
		{
			goto IL_0304;
		}
	}
	{
		bool L_118 = V_1;
		if (L_118)
		{
			goto IL_0304;
		}
	}
	{
		int32_t L_119 = V_9;
		if (!L_119)
		{
			goto IL_02d8;
		}
	}
	{
		int32_t L_120 = V_9;
		if ((!(((uint32_t)L_120) == ((uint32_t)1))))
		{
			goto IL_0304;
		}
	}

IL_02d8:
	{
		int32_t L_121 = V_10;
		if (!L_121)
		{
			goto IL_02e7;
		}
	}
	{
		int32_t L_122 = V_10;
		if ((!(((uint32_t)L_122) == ((uint32_t)1))))
		{
			goto IL_0304;
		}
	}

IL_02e7:
	{
		TEdge_t3950806139 * L_123 = ___e10;
		NullCheck(L_123);
		int32_t L_124 = L_123->get_PolyTyp_5();
		TEdge_t3950806139 * L_125 = ___e21;
		NullCheck(L_125);
		int32_t L_126 = L_125->get_PolyTyp_5();
		if ((((int32_t)L_124) == ((int32_t)L_126)))
		{
			goto IL_0312;
		}
	}
	{
		int32_t L_127 = __this->get_m_ClipType_16();
		if ((((int32_t)L_127) == ((int32_t)3)))
		{
			goto IL_0312;
		}
	}

IL_0304:
	{
		TEdge_t3950806139 * L_128 = ___e10;
		TEdge_t3950806139 * L_129 = ___e21;
		IntPoint_t3326126179  L_130 = ___pt2;
		Clipper_AddLocalMaxPoly_m913303731(__this, L_128, L_129, L_130, /*hidden argument*/NULL);
		goto IL_0332;
	}

IL_0312:
	{
		TEdge_t3950806139 * L_131 = ___e10;
		IntPoint_t3326126179  L_132 = ___pt2;
		Clipper_AddOutPt_m3632434112(__this, L_131, L_132, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_133 = ___e21;
		IntPoint_t3326126179  L_134 = ___pt2;
		Clipper_AddOutPt_m3632434112(__this, L_133, L_134, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_135 = ___e10;
		TEdge_t3950806139 * L_136 = ___e21;
		Clipper_SwapSides_m1446163601(NULL /*static, unused*/, L_135, L_136, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_137 = ___e10;
		TEdge_t3950806139 * L_138 = ___e21;
		Clipper_SwapPolyIndexes_m109539833(NULL /*static, unused*/, L_137, L_138, /*hidden argument*/NULL);
	}

IL_0332:
	{
		goto IL_0550;
	}

IL_0337:
	{
		bool L_139 = V_2;
		if (!L_139)
		{
			goto IL_0368;
		}
	}
	{
		int32_t L_140 = V_10;
		if (!L_140)
		{
			goto IL_034c;
		}
	}
	{
		int32_t L_141 = V_10;
		if ((!(((uint32_t)L_141) == ((uint32_t)1))))
		{
			goto IL_0363;
		}
	}

IL_034c:
	{
		TEdge_t3950806139 * L_142 = ___e10;
		IntPoint_t3326126179  L_143 = ___pt2;
		Clipper_AddOutPt_m3632434112(__this, L_142, L_143, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_144 = ___e10;
		TEdge_t3950806139 * L_145 = ___e21;
		Clipper_SwapSides_m1446163601(NULL /*static, unused*/, L_144, L_145, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_146 = ___e10;
		TEdge_t3950806139 * L_147 = ___e21;
		Clipper_SwapPolyIndexes_m109539833(NULL /*static, unused*/, L_146, L_147, /*hidden argument*/NULL);
	}

IL_0363:
	{
		goto IL_0550;
	}

IL_0368:
	{
		bool L_148 = V_3;
		if (!L_148)
		{
			goto IL_0399;
		}
	}
	{
		int32_t L_149 = V_9;
		if (!L_149)
		{
			goto IL_037d;
		}
	}
	{
		int32_t L_150 = V_9;
		if ((!(((uint32_t)L_150) == ((uint32_t)1))))
		{
			goto IL_0394;
		}
	}

IL_037d:
	{
		TEdge_t3950806139 * L_151 = ___e21;
		IntPoint_t3326126179  L_152 = ___pt2;
		Clipper_AddOutPt_m3632434112(__this, L_151, L_152, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_153 = ___e10;
		TEdge_t3950806139 * L_154 = ___e21;
		Clipper_SwapSides_m1446163601(NULL /*static, unused*/, L_153, L_154, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_155 = ___e10;
		TEdge_t3950806139 * L_156 = ___e21;
		Clipper_SwapPolyIndexes_m109539833(NULL /*static, unused*/, L_155, L_156, /*hidden argument*/NULL);
	}

IL_0394:
	{
		goto IL_0550;
	}

IL_0399:
	{
		int32_t L_157 = V_9;
		if (!L_157)
		{
			goto IL_03a8;
		}
	}
	{
		int32_t L_158 = V_9;
		if ((!(((uint32_t)L_158) == ((uint32_t)1))))
		{
			goto IL_0550;
		}
	}

IL_03a8:
	{
		int32_t L_159 = V_10;
		if (!L_159)
		{
			goto IL_03b7;
		}
	}
	{
		int32_t L_160 = V_10;
		if ((!(((uint32_t)L_160) == ((uint32_t)1))))
		{
			goto IL_0550;
		}
	}

IL_03b7:
	{
		bool L_161 = V_0;
		if (L_161)
		{
			goto IL_0550;
		}
	}
	{
		bool L_162 = V_1;
		if (L_162)
		{
			goto IL_0550;
		}
	}
	{
		int32_t L_163 = V_7;
		V_11 = L_163;
		int32_t L_164 = V_11;
		if ((((int32_t)L_164) == ((int32_t)2)))
		{
			goto IL_03dc;
		}
	}
	{
		int32_t L_165 = V_11;
		if ((((int32_t)L_165) == ((int32_t)3)))
		{
			goto IL_03ea;
		}
	}
	{
		goto IL_03f9;
	}

IL_03dc:
	{
		TEdge_t3950806139 * L_166 = ___e10;
		NullCheck(L_166);
		int32_t L_167 = L_166->get_WindCnt2_9();
		V_12 = (((int64_t)((int64_t)L_167)));
		goto IL_040c;
	}

IL_03ea:
	{
		TEdge_t3950806139 * L_168 = ___e10;
		NullCheck(L_168);
		int32_t L_169 = L_168->get_WindCnt2_9();
		V_12 = (((int64_t)((int64_t)((-L_169)))));
		goto IL_040c;
	}

IL_03f9:
	{
		TEdge_t3950806139 * L_170 = ___e10;
		NullCheck(L_170);
		int32_t L_171 = L_170->get_WindCnt2_9();
		int32_t L_172 = abs(L_171);
		V_12 = (((int64_t)((int64_t)L_172)));
		goto IL_040c;
	}

IL_040c:
	{
		int32_t L_173 = V_8;
		V_11 = L_173;
		int32_t L_174 = V_11;
		if ((((int32_t)L_174) == ((int32_t)2)))
		{
			goto IL_0425;
		}
	}
	{
		int32_t L_175 = V_11;
		if ((((int32_t)L_175) == ((int32_t)3)))
		{
			goto IL_0433;
		}
	}
	{
		goto IL_0442;
	}

IL_0425:
	{
		TEdge_t3950806139 * L_176 = ___e21;
		NullCheck(L_176);
		int32_t L_177 = L_176->get_WindCnt2_9();
		V_13 = (((int64_t)((int64_t)L_177)));
		goto IL_0455;
	}

IL_0433:
	{
		TEdge_t3950806139 * L_178 = ___e21;
		NullCheck(L_178);
		int32_t L_179 = L_178->get_WindCnt2_9();
		V_13 = (((int64_t)((int64_t)((-L_179)))));
		goto IL_0455;
	}

IL_0442:
	{
		TEdge_t3950806139 * L_180 = ___e21;
		NullCheck(L_180);
		int32_t L_181 = L_180->get_WindCnt2_9();
		int32_t L_182 = abs(L_181);
		V_13 = (((int64_t)((int64_t)L_182)));
		goto IL_0455;
	}

IL_0455:
	{
		TEdge_t3950806139 * L_183 = ___e10;
		NullCheck(L_183);
		int32_t L_184 = L_183->get_PolyTyp_5();
		TEdge_t3950806139 * L_185 = ___e21;
		NullCheck(L_185);
		int32_t L_186 = L_185->get_PolyTyp_5();
		if ((((int32_t)L_184) == ((int32_t)L_186)))
		{
			goto IL_0475;
		}
	}
	{
		TEdge_t3950806139 * L_187 = ___e10;
		TEdge_t3950806139 * L_188 = ___e21;
		IntPoint_t3326126179  L_189 = ___pt2;
		Clipper_AddLocalMinPoly_m2497180132(__this, L_187, L_188, L_189, /*hidden argument*/NULL);
		goto IL_0550;
	}

IL_0475:
	{
		int32_t L_190 = V_9;
		if ((!(((uint32_t)L_190) == ((uint32_t)1))))
		{
			goto IL_0549;
		}
	}
	{
		int32_t L_191 = V_10;
		if ((!(((uint32_t)L_191) == ((uint32_t)1))))
		{
			goto IL_0549;
		}
	}
	{
		int32_t L_192 = __this->get_m_ClipType_16();
		V_14 = L_192;
		int32_t L_193 = V_14;
		if (L_193 == 0)
		{
			goto IL_04a9;
		}
		if (L_193 == 1)
		{
			goto IL_04ca;
		}
		if (L_193 == 2)
		{
			goto IL_04eb;
		}
		if (L_193 == 3)
		{
			goto IL_0535;
		}
	}
	{
		goto IL_0544;
	}

IL_04a9:
	{
		int64_t L_194 = V_12;
		if ((((int64_t)L_194) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_04c5;
		}
	}
	{
		int64_t L_195 = V_13;
		if ((((int64_t)L_195) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_04c5;
		}
	}
	{
		TEdge_t3950806139 * L_196 = ___e10;
		TEdge_t3950806139 * L_197 = ___e21;
		IntPoint_t3326126179  L_198 = ___pt2;
		Clipper_AddLocalMinPoly_m2497180132(__this, L_196, L_197, L_198, /*hidden argument*/NULL);
	}

IL_04c5:
	{
		goto IL_0544;
	}

IL_04ca:
	{
		int64_t L_199 = V_12;
		if ((((int64_t)L_199) > ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_04e6;
		}
	}
	{
		int64_t L_200 = V_13;
		if ((((int64_t)L_200) > ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_04e6;
		}
	}
	{
		TEdge_t3950806139 * L_201 = ___e10;
		TEdge_t3950806139 * L_202 = ___e21;
		IntPoint_t3326126179  L_203 = ___pt2;
		Clipper_AddLocalMinPoly_m2497180132(__this, L_201, L_202, L_203, /*hidden argument*/NULL);
	}

IL_04e6:
	{
		goto IL_0544;
	}

IL_04eb:
	{
		TEdge_t3950806139 * L_204 = ___e10;
		NullCheck(L_204);
		int32_t L_205 = L_204->get_PolyTyp_5();
		if ((!(((uint32_t)L_205) == ((uint32_t)1))))
		{
			goto IL_0509;
		}
	}
	{
		int64_t L_206 = V_12;
		if ((((int64_t)L_206) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0509;
		}
	}
	{
		int64_t L_207 = V_13;
		if ((((int64_t)L_207) > ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0526;
		}
	}

IL_0509:
	{
		TEdge_t3950806139 * L_208 = ___e10;
		NullCheck(L_208);
		int32_t L_209 = L_208->get_PolyTyp_5();
		if (L_209)
		{
			goto IL_0530;
		}
	}
	{
		int64_t L_210 = V_12;
		if ((((int64_t)L_210) > ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0530;
		}
	}
	{
		int64_t L_211 = V_13;
		if ((((int64_t)L_211) > ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0530;
		}
	}

IL_0526:
	{
		TEdge_t3950806139 * L_212 = ___e10;
		TEdge_t3950806139 * L_213 = ___e21;
		IntPoint_t3326126179  L_214 = ___pt2;
		Clipper_AddLocalMinPoly_m2497180132(__this, L_212, L_213, L_214, /*hidden argument*/NULL);
	}

IL_0530:
	{
		goto IL_0544;
	}

IL_0535:
	{
		TEdge_t3950806139 * L_215 = ___e10;
		TEdge_t3950806139 * L_216 = ___e21;
		IntPoint_t3326126179  L_217 = ___pt2;
		Clipper_AddLocalMinPoly_m2497180132(__this, L_215, L_216, L_217, /*hidden argument*/NULL);
		goto IL_0544;
	}

IL_0544:
	{
		goto IL_0550;
	}

IL_0549:
	{
		TEdge_t3950806139 * L_218 = ___e10;
		TEdge_t3950806139 * L_219 = ___e21;
		Clipper_SwapSides_m1446163601(NULL /*static, unused*/, L_218, L_219, /*hidden argument*/NULL);
	}

IL_0550:
	{
		bool L_220 = V_0;
		bool L_221 = V_1;
		if ((((int32_t)L_220) == ((int32_t)L_221)))
		{
			goto IL_0589;
		}
	}
	{
		bool L_222 = V_0;
		if (!L_222)
		{
			goto IL_0569;
		}
	}
	{
		TEdge_t3950806139 * L_223 = ___e10;
		NullCheck(L_223);
		int32_t L_224 = L_223->get_OutIdx_10();
		if ((((int32_t)L_224) >= ((int32_t)0)))
		{
			goto IL_057b;
		}
	}

IL_0569:
	{
		bool L_225 = V_1;
		if (!L_225)
		{
			goto IL_0589;
		}
	}
	{
		TEdge_t3950806139 * L_226 = ___e21;
		NullCheck(L_226);
		int32_t L_227 = L_226->get_OutIdx_10();
		if ((((int32_t)L_227) < ((int32_t)0)))
		{
			goto IL_0589;
		}
	}

IL_057b:
	{
		TEdge_t3950806139 * L_228 = ___e10;
		TEdge_t3950806139 * L_229 = ___e21;
		Clipper_SwapSides_m1446163601(NULL /*static, unused*/, L_228, L_229, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_230 = ___e10;
		TEdge_t3950806139 * L_231 = ___e21;
		Clipper_SwapPolyIndexes_m109539833(NULL /*static, unused*/, L_230, L_231, /*hidden argument*/NULL);
	}

IL_0589:
	{
		bool L_232 = V_0;
		if (!L_232)
		{
			goto IL_0596;
		}
	}
	{
		TEdge_t3950806139 * L_233 = ___e10;
		Clipper_DeleteFromAEL_m2256743194(__this, L_233, /*hidden argument*/NULL);
	}

IL_0596:
	{
		bool L_234 = V_1;
		if (!L_234)
		{
			goto IL_05a3;
		}
	}
	{
		TEdge_t3950806139 * L_235 = ___e21;
		Clipper_DeleteFromAEL_m2256743194(__this, L_235, /*hidden argument*/NULL);
	}

IL_05a3:
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::DeleteFromAEL(Pathfinding.ClipperLib.TEdge)
extern "C"  void Clipper_DeleteFromAEL_m2256743194 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e0, const MethodInfo* method)
{
	TEdge_t3950806139 * V_0 = NULL;
	TEdge_t3950806139 * V_1 = NULL;
	{
		TEdge_t3950806139 * L_0 = ___e0;
		NullCheck(L_0);
		TEdge_t3950806139 * L_1 = L_0->get_PrevInAEL_15();
		V_0 = L_1;
		TEdge_t3950806139 * L_2 = ___e0;
		NullCheck(L_2);
		TEdge_t3950806139 * L_3 = L_2->get_NextInAEL_14();
		V_1 = L_3;
		TEdge_t3950806139 * L_4 = V_0;
		if (L_4)
		{
			goto IL_0027;
		}
	}
	{
		TEdge_t3950806139 * L_5 = V_1;
		if (L_5)
		{
			goto IL_0027;
		}
	}
	{
		TEdge_t3950806139 * L_6 = ___e0;
		TEdge_t3950806139 * L_7 = __this->get_m_ActiveEdges_18();
		if ((((Il2CppObject*)(TEdge_t3950806139 *)L_6) == ((Il2CppObject*)(TEdge_t3950806139 *)L_7)))
		{
			goto IL_0027;
		}
	}
	{
		return;
	}

IL_0027:
	{
		TEdge_t3950806139 * L_8 = V_0;
		if (!L_8)
		{
			goto IL_0039;
		}
	}
	{
		TEdge_t3950806139 * L_9 = V_0;
		TEdge_t3950806139 * L_10 = V_1;
		NullCheck(L_9);
		L_9->set_NextInAEL_14(L_10);
		goto IL_0040;
	}

IL_0039:
	{
		TEdge_t3950806139 * L_11 = V_1;
		__this->set_m_ActiveEdges_18(L_11);
	}

IL_0040:
	{
		TEdge_t3950806139 * L_12 = V_1;
		if (!L_12)
		{
			goto IL_004d;
		}
	}
	{
		TEdge_t3950806139 * L_13 = V_1;
		TEdge_t3950806139 * L_14 = V_0;
		NullCheck(L_13);
		L_13->set_PrevInAEL_15(L_14);
	}

IL_004d:
	{
		TEdge_t3950806139 * L_15 = ___e0;
		NullCheck(L_15);
		L_15->set_NextInAEL_14((TEdge_t3950806139 *)NULL);
		TEdge_t3950806139 * L_16 = ___e0;
		NullCheck(L_16);
		L_16->set_PrevInAEL_15((TEdge_t3950806139 *)NULL);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::DeleteFromSEL(Pathfinding.ClipperLib.TEdge)
extern "C"  void Clipper_DeleteFromSEL_m1605264172 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e0, const MethodInfo* method)
{
	TEdge_t3950806139 * V_0 = NULL;
	TEdge_t3950806139 * V_1 = NULL;
	{
		TEdge_t3950806139 * L_0 = ___e0;
		NullCheck(L_0);
		TEdge_t3950806139 * L_1 = L_0->get_PrevInSEL_17();
		V_0 = L_1;
		TEdge_t3950806139 * L_2 = ___e0;
		NullCheck(L_2);
		TEdge_t3950806139 * L_3 = L_2->get_NextInSEL_16();
		V_1 = L_3;
		TEdge_t3950806139 * L_4 = V_0;
		if (L_4)
		{
			goto IL_0027;
		}
	}
	{
		TEdge_t3950806139 * L_5 = V_1;
		if (L_5)
		{
			goto IL_0027;
		}
	}
	{
		TEdge_t3950806139 * L_6 = ___e0;
		TEdge_t3950806139 * L_7 = __this->get_m_SortedEdges_19();
		if ((((Il2CppObject*)(TEdge_t3950806139 *)L_6) == ((Il2CppObject*)(TEdge_t3950806139 *)L_7)))
		{
			goto IL_0027;
		}
	}
	{
		return;
	}

IL_0027:
	{
		TEdge_t3950806139 * L_8 = V_0;
		if (!L_8)
		{
			goto IL_0039;
		}
	}
	{
		TEdge_t3950806139 * L_9 = V_0;
		TEdge_t3950806139 * L_10 = V_1;
		NullCheck(L_9);
		L_9->set_NextInSEL_16(L_10);
		goto IL_0040;
	}

IL_0039:
	{
		TEdge_t3950806139 * L_11 = V_1;
		__this->set_m_SortedEdges_19(L_11);
	}

IL_0040:
	{
		TEdge_t3950806139 * L_12 = V_1;
		if (!L_12)
		{
			goto IL_004d;
		}
	}
	{
		TEdge_t3950806139 * L_13 = V_1;
		TEdge_t3950806139 * L_14 = V_0;
		NullCheck(L_13);
		L_13->set_PrevInSEL_17(L_14);
	}

IL_004d:
	{
		TEdge_t3950806139 * L_15 = ___e0;
		NullCheck(L_15);
		L_15->set_NextInSEL_16((TEdge_t3950806139 *)NULL);
		TEdge_t3950806139 * L_16 = ___e0;
		NullCheck(L_16);
		L_16->set_PrevInSEL_17((TEdge_t3950806139 *)NULL);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::UpdateEdgeIntoAEL(Pathfinding.ClipperLib.TEdge&)
extern Il2CppClass* ClipperException_t2818206852_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3922764463;
extern const uint32_t Clipper_UpdateEdgeIntoAEL_m1906260707_MetadataUsageId;
extern "C"  void Clipper_UpdateEdgeIntoAEL_m1906260707 (Clipper_t636949239 * __this, TEdge_t3950806139 ** ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_UpdateEdgeIntoAEL_m1906260707_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TEdge_t3950806139 * V_0 = NULL;
	TEdge_t3950806139 * V_1 = NULL;
	{
		TEdge_t3950806139 ** L_0 = ___e0;
		NullCheck((*((TEdge_t3950806139 **)L_0)));
		TEdge_t3950806139 * L_1 = (*((TEdge_t3950806139 **)L_0))->get_NextInLML_13();
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		ClipperException_t2818206852 * L_2 = (ClipperException_t2818206852 *)il2cpp_codegen_object_new(ClipperException_t2818206852_il2cpp_TypeInfo_var);
		ClipperException__ctor_m106504313(L_2, _stringLiteral3922764463, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		TEdge_t3950806139 ** L_3 = ___e0;
		NullCheck((*((TEdge_t3950806139 **)L_3)));
		TEdge_t3950806139 * L_4 = (*((TEdge_t3950806139 **)L_3))->get_PrevInAEL_15();
		V_0 = L_4;
		TEdge_t3950806139 ** L_5 = ___e0;
		NullCheck((*((TEdge_t3950806139 **)L_5)));
		TEdge_t3950806139 * L_6 = (*((TEdge_t3950806139 **)L_5))->get_NextInAEL_14();
		V_1 = L_6;
		TEdge_t3950806139 ** L_7 = ___e0;
		NullCheck((*((TEdge_t3950806139 **)L_7)));
		TEdge_t3950806139 * L_8 = (*((TEdge_t3950806139 **)L_7))->get_NextInLML_13();
		TEdge_t3950806139 ** L_9 = ___e0;
		NullCheck((*((TEdge_t3950806139 **)L_9)));
		int32_t L_10 = (*((TEdge_t3950806139 **)L_9))->get_OutIdx_10();
		NullCheck(L_8);
		L_8->set_OutIdx_10(L_10);
		TEdge_t3950806139 * L_11 = V_0;
		if (!L_11)
		{
			goto IL_0052;
		}
	}
	{
		TEdge_t3950806139 * L_12 = V_0;
		TEdge_t3950806139 ** L_13 = ___e0;
		NullCheck((*((TEdge_t3950806139 **)L_13)));
		TEdge_t3950806139 * L_14 = (*((TEdge_t3950806139 **)L_13))->get_NextInLML_13();
		NullCheck(L_12);
		L_12->set_NextInAEL_14(L_14);
		goto IL_005f;
	}

IL_0052:
	{
		TEdge_t3950806139 ** L_15 = ___e0;
		NullCheck((*((TEdge_t3950806139 **)L_15)));
		TEdge_t3950806139 * L_16 = (*((TEdge_t3950806139 **)L_15))->get_NextInLML_13();
		__this->set_m_ActiveEdges_18(L_16);
	}

IL_005f:
	{
		TEdge_t3950806139 * L_17 = V_1;
		if (!L_17)
		{
			goto IL_0072;
		}
	}
	{
		TEdge_t3950806139 * L_18 = V_1;
		TEdge_t3950806139 ** L_19 = ___e0;
		NullCheck((*((TEdge_t3950806139 **)L_19)));
		TEdge_t3950806139 * L_20 = (*((TEdge_t3950806139 **)L_19))->get_NextInLML_13();
		NullCheck(L_18);
		L_18->set_PrevInAEL_15(L_20);
	}

IL_0072:
	{
		TEdge_t3950806139 ** L_21 = ___e0;
		NullCheck((*((TEdge_t3950806139 **)L_21)));
		TEdge_t3950806139 * L_22 = (*((TEdge_t3950806139 **)L_21))->get_NextInLML_13();
		TEdge_t3950806139 ** L_23 = ___e0;
		NullCheck((*((TEdge_t3950806139 **)L_23)));
		int32_t L_24 = (*((TEdge_t3950806139 **)L_23))->get_Side_6();
		NullCheck(L_22);
		L_22->set_Side_6(L_24);
		TEdge_t3950806139 ** L_25 = ___e0;
		NullCheck((*((TEdge_t3950806139 **)L_25)));
		TEdge_t3950806139 * L_26 = (*((TEdge_t3950806139 **)L_25))->get_NextInLML_13();
		TEdge_t3950806139 ** L_27 = ___e0;
		NullCheck((*((TEdge_t3950806139 **)L_27)));
		int32_t L_28 = (*((TEdge_t3950806139 **)L_27))->get_WindDelta_7();
		NullCheck(L_26);
		L_26->set_WindDelta_7(L_28);
		TEdge_t3950806139 ** L_29 = ___e0;
		NullCheck((*((TEdge_t3950806139 **)L_29)));
		TEdge_t3950806139 * L_30 = (*((TEdge_t3950806139 **)L_29))->get_NextInLML_13();
		TEdge_t3950806139 ** L_31 = ___e0;
		NullCheck((*((TEdge_t3950806139 **)L_31)));
		int32_t L_32 = (*((TEdge_t3950806139 **)L_31))->get_WindCnt_8();
		NullCheck(L_30);
		L_30->set_WindCnt_8(L_32);
		TEdge_t3950806139 ** L_33 = ___e0;
		NullCheck((*((TEdge_t3950806139 **)L_33)));
		TEdge_t3950806139 * L_34 = (*((TEdge_t3950806139 **)L_33))->get_NextInLML_13();
		TEdge_t3950806139 ** L_35 = ___e0;
		NullCheck((*((TEdge_t3950806139 **)L_35)));
		int32_t L_36 = (*((TEdge_t3950806139 **)L_35))->get_WindCnt2_9();
		NullCheck(L_34);
		L_34->set_WindCnt2_9(L_36);
		TEdge_t3950806139 ** L_37 = ___e0;
		TEdge_t3950806139 ** L_38 = ___e0;
		NullCheck((*((TEdge_t3950806139 **)L_38)));
		TEdge_t3950806139 * L_39 = (*((TEdge_t3950806139 **)L_38))->get_NextInLML_13();
		*((Il2CppObject **)(L_37)) = (Il2CppObject *)L_39;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_37), (Il2CppObject *)L_39);
		TEdge_t3950806139 ** L_40 = ___e0;
		TEdge_t3950806139 ** L_41 = ___e0;
		NullCheck((*((TEdge_t3950806139 **)L_41)));
		IntPoint_t3326126179  L_42 = (*((TEdge_t3950806139 **)L_41))->get_Bot_0();
		NullCheck((*((TEdge_t3950806139 **)L_40)));
		(*((TEdge_t3950806139 **)L_40))->set_Curr_1(L_42);
		TEdge_t3950806139 ** L_43 = ___e0;
		TEdge_t3950806139 * L_44 = V_0;
		NullCheck((*((TEdge_t3950806139 **)L_43)));
		(*((TEdge_t3950806139 **)L_43))->set_PrevInAEL_15(L_44);
		TEdge_t3950806139 ** L_45 = ___e0;
		TEdge_t3950806139 * L_46 = V_1;
		NullCheck((*((TEdge_t3950806139 **)L_45)));
		(*((TEdge_t3950806139 **)L_45))->set_NextInAEL_14(L_46);
		TEdge_t3950806139 ** L_47 = ___e0;
		bool L_48 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, (*((TEdge_t3950806139 **)L_47)), /*hidden argument*/NULL);
		if (L_48)
		{
			goto IL_0103;
		}
	}
	{
		TEdge_t3950806139 ** L_49 = ___e0;
		NullCheck((*((TEdge_t3950806139 **)L_49)));
		IntPoint_t3326126179 * L_50 = (*((TEdge_t3950806139 **)L_49))->get_address_of_Top_2();
		int64_t L_51 = L_50->get_Y_1();
		Clipper_InsertScanbeam_m2447896813(__this, L_51, /*hidden argument*/NULL);
	}

IL_0103:
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::ProcessHorizontals(System.Boolean)
extern "C"  void Clipper_ProcessHorizontals_m563259309 (Clipper_t636949239 * __this, bool ___isTopOfScanbeam0, const MethodInfo* method)
{
	TEdge_t3950806139 * V_0 = NULL;
	{
		TEdge_t3950806139 * L_0 = __this->get_m_SortedEdges_19();
		V_0 = L_0;
		goto IL_0022;
	}

IL_000c:
	{
		TEdge_t3950806139 * L_1 = V_0;
		Clipper_DeleteFromSEL_m1605264172(__this, L_1, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_2 = V_0;
		bool L_3 = ___isTopOfScanbeam0;
		Clipper_ProcessHorizontal_m2266688323(__this, L_2, L_3, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_4 = __this->get_m_SortedEdges_19();
		V_0 = L_4;
	}

IL_0022:
	{
		TEdge_t3950806139 * L_5 = V_0;
		if (L_5)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::GetHorzDirection(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.Direction&,System.Int64&,System.Int64&)
extern "C"  void Clipper_GetHorzDirection_m834255180 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___HorzEdge0, int32_t* ___Dir1, int64_t* ___Left2, int64_t* ___Right3, const MethodInfo* method)
{
	{
		TEdge_t3950806139 * L_0 = ___HorzEdge0;
		NullCheck(L_0);
		IntPoint_t3326126179 * L_1 = L_0->get_address_of_Bot_0();
		int64_t L_2 = L_1->get_X_0();
		TEdge_t3950806139 * L_3 = ___HorzEdge0;
		NullCheck(L_3);
		IntPoint_t3326126179 * L_4 = L_3->get_address_of_Top_2();
		int64_t L_5 = L_4->get_X_0();
		if ((((int64_t)L_2) >= ((int64_t)L_5)))
		{
			goto IL_003e;
		}
	}
	{
		int64_t* L_6 = ___Left2;
		TEdge_t3950806139 * L_7 = ___HorzEdge0;
		NullCheck(L_7);
		IntPoint_t3326126179 * L_8 = L_7->get_address_of_Bot_0();
		int64_t L_9 = L_8->get_X_0();
		*((int64_t*)(L_6)) = (int64_t)L_9;
		int64_t* L_10 = ___Right3;
		TEdge_t3950806139 * L_11 = ___HorzEdge0;
		NullCheck(L_11);
		IntPoint_t3326126179 * L_12 = L_11->get_address_of_Top_2();
		int64_t L_13 = L_12->get_X_0();
		*((int64_t*)(L_10)) = (int64_t)L_13;
		int32_t* L_14 = ___Dir1;
		*((int32_t*)(L_14)) = (int32_t)1;
		goto IL_005c;
	}

IL_003e:
	{
		int64_t* L_15 = ___Left2;
		TEdge_t3950806139 * L_16 = ___HorzEdge0;
		NullCheck(L_16);
		IntPoint_t3326126179 * L_17 = L_16->get_address_of_Top_2();
		int64_t L_18 = L_17->get_X_0();
		*((int64_t*)(L_15)) = (int64_t)L_18;
		int64_t* L_19 = ___Right3;
		TEdge_t3950806139 * L_20 = ___HorzEdge0;
		NullCheck(L_20);
		IntPoint_t3326126179 * L_21 = L_20->get_address_of_Bot_0();
		int64_t L_22 = L_21->get_X_0();
		*((int64_t*)(L_19)) = (int64_t)L_22;
		int32_t* L_23 = ___Dir1;
		*((int32_t*)(L_23)) = (int32_t)0;
	}

IL_005c:
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::PrepareHorzJoins(Pathfinding.ClipperLib.TEdge,System.Boolean)
extern const MethodInfo* List_1_get_Item_m2053160358_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2818082526_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m896344279_MethodInfo_var;
extern const uint32_t Clipper_PrepareHorzJoins_m1033324665_MetadataUsageId;
extern "C"  void Clipper_PrepareHorzJoins_m1033324665 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___horzEdge0, bool ___isTopOfScanbeam1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_PrepareHorzJoins_m1033324665_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	OutPt_t3947633180 * V_0 = NULL;
	int32_t V_1 = 0;
	Join_t3970117804 * V_2 = NULL;
	{
		List_1_t319023540 * L_0 = __this->get_m_PolyOuts_15();
		TEdge_t3950806139 * L_1 = ___horzEdge0;
		NullCheck(L_1);
		int32_t L_2 = L_1->get_OutIdx_10();
		NullCheck(L_0);
		OutRec_t3245805284 * L_3 = List_1_get_Item_m2053160358(L_0, L_2, /*hidden argument*/List_1_get_Item_m2053160358_MethodInfo_var);
		NullCheck(L_3);
		OutPt_t3947633180 * L_4 = L_3->get_Pts_4();
		V_0 = L_4;
		TEdge_t3950806139 * L_5 = ___horzEdge0;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_Side_6();
		if (!L_6)
		{
			goto IL_0029;
		}
	}
	{
		OutPt_t3947633180 * L_7 = V_0;
		NullCheck(L_7);
		OutPt_t3947633180 * L_8 = L_7->get_Prev_3();
		V_0 = L_8;
	}

IL_0029:
	{
		V_1 = 0;
		goto IL_007c;
	}

IL_0030:
	{
		List_1_t1043336060 * L_9 = __this->get_m_GhostJoins_25();
		int32_t L_10 = V_1;
		NullCheck(L_9);
		Join_t3970117804 * L_11 = List_1_get_Item_m2818082526(L_9, L_10, /*hidden argument*/List_1_get_Item_m2818082526_MethodInfo_var);
		V_2 = L_11;
		Join_t3970117804 * L_12 = V_2;
		NullCheck(L_12);
		OutPt_t3947633180 * L_13 = L_12->get_OutPt1_0();
		NullCheck(L_13);
		IntPoint_t3326126179  L_14 = L_13->get_Pt_1();
		Join_t3970117804 * L_15 = V_2;
		NullCheck(L_15);
		IntPoint_t3326126179  L_16 = L_15->get_OffPt_2();
		TEdge_t3950806139 * L_17 = ___horzEdge0;
		NullCheck(L_17);
		IntPoint_t3326126179  L_18 = L_17->get_Bot_0();
		TEdge_t3950806139 * L_19 = ___horzEdge0;
		NullCheck(L_19);
		IntPoint_t3326126179  L_20 = L_19->get_Top_2();
		bool L_21 = Clipper_HorzSegmentsOverlap_m2752565594(__this, L_14, L_16, L_18, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0078;
		}
	}
	{
		Join_t3970117804 * L_22 = V_2;
		NullCheck(L_22);
		OutPt_t3947633180 * L_23 = L_22->get_OutPt1_0();
		OutPt_t3947633180 * L_24 = V_0;
		Join_t3970117804 * L_25 = V_2;
		NullCheck(L_25);
		IntPoint_t3326126179  L_26 = L_25->get_OffPt_2();
		Clipper_AddJoin_m2260685358(__this, L_23, L_24, L_26, /*hidden argument*/NULL);
	}

IL_0078:
	{
		int32_t L_27 = V_1;
		V_1 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_007c:
	{
		int32_t L_28 = V_1;
		List_1_t1043336060 * L_29 = __this->get_m_GhostJoins_25();
		NullCheck(L_29);
		int32_t L_30 = List_1_get_Count_m896344279(L_29, /*hidden argument*/List_1_get_Count_m896344279_MethodInfo_var);
		if ((((int32_t)L_28) < ((int32_t)L_30)))
		{
			goto IL_0030;
		}
	}
	{
		bool L_31 = ___isTopOfScanbeam1;
		if (!L_31)
		{
			goto IL_00c8;
		}
	}
	{
		OutPt_t3947633180 * L_32 = V_0;
		NullCheck(L_32);
		IntPoint_t3326126179  L_33 = L_32->get_Pt_1();
		TEdge_t3950806139 * L_34 = ___horzEdge0;
		NullCheck(L_34);
		IntPoint_t3326126179  L_35 = L_34->get_Top_2();
		bool L_36 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_33, L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00bb;
		}
	}
	{
		OutPt_t3947633180 * L_37 = V_0;
		TEdge_t3950806139 * L_38 = ___horzEdge0;
		NullCheck(L_38);
		IntPoint_t3326126179  L_39 = L_38->get_Bot_0();
		Clipper_AddGhostJoin_m1097100551(__this, L_37, L_39, /*hidden argument*/NULL);
		goto IL_00c8;
	}

IL_00bb:
	{
		OutPt_t3947633180 * L_40 = V_0;
		TEdge_t3950806139 * L_41 = ___horzEdge0;
		NullCheck(L_41);
		IntPoint_t3326126179  L_42 = L_41->get_Top_2();
		Clipper_AddGhostJoin_m1097100551(__this, L_40, L_42, /*hidden argument*/NULL);
	}

IL_00c8:
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::ProcessHorizontal(Pathfinding.ClipperLib.TEdge,System.Boolean)
extern Il2CppClass* ClipperException_t2818206852_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3907404667;
extern const uint32_t Clipper_ProcessHorizontal_m2266688323_MetadataUsageId;
extern "C"  void Clipper_ProcessHorizontal_m2266688323 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___horzEdge0, bool ___isTopOfScanbeam1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_ProcessHorizontal_m2266688323_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int64_t V_1 = 0;
	int64_t V_2 = 0;
	TEdge_t3950806139 * V_3 = NULL;
	TEdge_t3950806139 * V_4 = NULL;
	bool V_5 = false;
	TEdge_t3950806139 * V_6 = NULL;
	TEdge_t3950806139 * V_7 = NULL;
	IntPoint_t3326126179  V_8;
	memset(&V_8, 0, sizeof(V_8));
	IntPoint_t3326126179  V_9;
	memset(&V_9, 0, sizeof(V_9));
	OutPt_t3947633180 * V_10 = NULL;
	TEdge_t3950806139 * V_11 = NULL;
	TEdge_t3950806139 * V_12 = NULL;
	OutPt_t3947633180 * V_13 = NULL;
	OutPt_t3947633180 * V_14 = NULL;
	{
		TEdge_t3950806139 * L_0 = ___horzEdge0;
		Clipper_GetHorzDirection_m834255180(__this, L_0, (&V_0), (&V_1), (&V_2), /*hidden argument*/NULL);
		TEdge_t3950806139 * L_1 = ___horzEdge0;
		V_3 = L_1;
		V_4 = (TEdge_t3950806139 *)NULL;
		goto IL_001e;
	}

IL_0017:
	{
		TEdge_t3950806139 * L_2 = V_3;
		NullCheck(L_2);
		TEdge_t3950806139 * L_3 = L_2->get_NextInLML_13();
		V_3 = L_3;
	}

IL_001e:
	{
		TEdge_t3950806139 * L_4 = V_3;
		NullCheck(L_4);
		TEdge_t3950806139 * L_5 = L_4->get_NextInLML_13();
		if (!L_5)
		{
			goto IL_0039;
		}
	}
	{
		TEdge_t3950806139 * L_6 = V_3;
		NullCheck(L_6);
		TEdge_t3950806139 * L_7 = L_6->get_NextInLML_13();
		bool L_8 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0017;
		}
	}

IL_0039:
	{
		TEdge_t3950806139 * L_9 = V_3;
		NullCheck(L_9);
		TEdge_t3950806139 * L_10 = L_9->get_NextInLML_13();
		if (L_10)
		{
			goto IL_004d;
		}
	}
	{
		TEdge_t3950806139 * L_11 = V_3;
		TEdge_t3950806139 * L_12 = Clipper_GetMaximaPair_m3274930736(__this, L_11, /*hidden argument*/NULL);
		V_4 = L_12;
	}

IL_004d:
	{
		goto IL_027e;
	}

IL_0052:
	{
		TEdge_t3950806139 * L_13 = ___horzEdge0;
		TEdge_t3950806139 * L_14 = V_3;
		V_5 = (bool)((((Il2CppObject*)(TEdge_t3950806139 *)L_13) == ((Il2CppObject*)(TEdge_t3950806139 *)L_14))? 1 : 0);
		TEdge_t3950806139 * L_15 = ___horzEdge0;
		int32_t L_16 = V_0;
		TEdge_t3950806139 * L_17 = Clipper_GetNextInAEL_m1569553480(__this, L_15, L_16, /*hidden argument*/NULL);
		V_6 = L_17;
		goto IL_0204;
	}

IL_0067:
	{
		TEdge_t3950806139 * L_18 = V_6;
		NullCheck(L_18);
		IntPoint_t3326126179 * L_19 = L_18->get_address_of_Curr_1();
		int64_t L_20 = L_19->get_X_0();
		TEdge_t3950806139 * L_21 = ___horzEdge0;
		NullCheck(L_21);
		IntPoint_t3326126179 * L_22 = L_21->get_address_of_Top_2();
		int64_t L_23 = L_22->get_X_0();
		if ((!(((uint64_t)L_20) == ((uint64_t)L_23))))
		{
			goto IL_00aa;
		}
	}
	{
		TEdge_t3950806139 * L_24 = ___horzEdge0;
		NullCheck(L_24);
		TEdge_t3950806139 * L_25 = L_24->get_NextInLML_13();
		if (!L_25)
		{
			goto IL_00aa;
		}
	}
	{
		TEdge_t3950806139 * L_26 = V_6;
		NullCheck(L_26);
		double L_27 = L_26->get_Dx_4();
		TEdge_t3950806139 * L_28 = ___horzEdge0;
		NullCheck(L_28);
		TEdge_t3950806139 * L_29 = L_28->get_NextInLML_13();
		NullCheck(L_29);
		double L_30 = L_29->get_Dx_4();
		if ((!(((double)L_27) < ((double)L_30))))
		{
			goto IL_00aa;
		}
	}
	{
		goto IL_020b;
	}

IL_00aa:
	{
		TEdge_t3950806139 * L_31 = V_6;
		int32_t L_32 = V_0;
		TEdge_t3950806139 * L_33 = Clipper_GetNextInAEL_m1569553480(__this, L_31, L_32, /*hidden argument*/NULL);
		V_7 = L_33;
		int32_t L_34 = V_0;
		if ((!(((uint32_t)L_34) == ((uint32_t)1))))
		{
			goto IL_00ce;
		}
	}
	{
		TEdge_t3950806139 * L_35 = V_6;
		NullCheck(L_35);
		IntPoint_t3326126179 * L_36 = L_35->get_address_of_Curr_1();
		int64_t L_37 = L_36->get_X_0();
		int64_t L_38 = V_2;
		if ((((int64_t)L_37) <= ((int64_t)L_38)))
		{
			goto IL_00e6;
		}
	}

IL_00ce:
	{
		int32_t L_39 = V_0;
		if (L_39)
		{
			goto IL_01ca;
		}
	}
	{
		TEdge_t3950806139 * L_40 = V_6;
		NullCheck(L_40);
		IntPoint_t3326126179 * L_41 = L_40->get_address_of_Curr_1();
		int64_t L_42 = L_41->get_X_0();
		int64_t L_43 = V_1;
		if ((((int64_t)L_42) < ((int64_t)L_43)))
		{
			goto IL_01ca;
		}
	}

IL_00e6:
	{
		TEdge_t3950806139 * L_44 = V_6;
		TEdge_t3950806139 * L_45 = V_4;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_44) == ((Il2CppObject*)(TEdge_t3950806139 *)L_45))))
		{
			goto IL_015c;
		}
	}
	{
		bool L_46 = V_5;
		if (!L_46)
		{
			goto IL_015c;
		}
	}
	{
		TEdge_t3950806139 * L_47 = ___horzEdge0;
		NullCheck(L_47);
		int32_t L_48 = L_47->get_OutIdx_10();
		if ((((int32_t)L_48) < ((int32_t)0)))
		{
			goto IL_0115;
		}
	}
	{
		TEdge_t3950806139 * L_49 = ___horzEdge0;
		NullCheck(L_49);
		int32_t L_50 = L_49->get_WindDelta_7();
		if (!L_50)
		{
			goto IL_0115;
		}
	}
	{
		TEdge_t3950806139 * L_51 = ___horzEdge0;
		bool L_52 = ___isTopOfScanbeam1;
		Clipper_PrepareHorzJoins_m1033324665(__this, L_51, L_52, /*hidden argument*/NULL);
	}

IL_0115:
	{
		int32_t L_53 = V_0;
		if ((!(((uint32_t)L_53) == ((uint32_t)1))))
		{
			goto IL_0132;
		}
	}
	{
		TEdge_t3950806139 * L_54 = ___horzEdge0;
		TEdge_t3950806139 * L_55 = V_6;
		TEdge_t3950806139 * L_56 = V_6;
		NullCheck(L_56);
		IntPoint_t3326126179  L_57 = L_56->get_Top_2();
		Clipper_IntersectEdges_m1787610885(__this, L_54, L_55, L_57, (bool)0, /*hidden argument*/NULL);
		goto IL_0143;
	}

IL_0132:
	{
		TEdge_t3950806139 * L_58 = V_6;
		TEdge_t3950806139 * L_59 = ___horzEdge0;
		TEdge_t3950806139 * L_60 = V_6;
		NullCheck(L_60);
		IntPoint_t3326126179  L_61 = L_60->get_Top_2();
		Clipper_IntersectEdges_m1787610885(__this, L_58, L_59, L_61, (bool)0, /*hidden argument*/NULL);
	}

IL_0143:
	{
		TEdge_t3950806139 * L_62 = V_4;
		NullCheck(L_62);
		int32_t L_63 = L_62->get_OutIdx_10();
		if ((((int32_t)L_63) < ((int32_t)0)))
		{
			goto IL_015b;
		}
	}
	{
		ClipperException_t2818206852 * L_64 = (ClipperException_t2818206852 *)il2cpp_codegen_object_new(ClipperException_t2818206852_il2cpp_TypeInfo_var);
		ClipperException__ctor_m106504313(L_64, _stringLiteral3907404667, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_64);
	}

IL_015b:
	{
		return;
	}

IL_015c:
	{
		int32_t L_65 = V_0;
		if ((!(((uint32_t)L_65) == ((uint32_t)1))))
		{
			goto IL_0192;
		}
	}
	{
		TEdge_t3950806139 * L_66 = V_6;
		NullCheck(L_66);
		IntPoint_t3326126179 * L_67 = L_66->get_address_of_Curr_1();
		int64_t L_68 = L_67->get_X_0();
		TEdge_t3950806139 * L_69 = ___horzEdge0;
		NullCheck(L_69);
		IntPoint_t3326126179 * L_70 = L_69->get_address_of_Curr_1();
		int64_t L_71 = L_70->get_Y_1();
		IntPoint__ctor_m1065894876((&V_8), L_68, L_71, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_72 = ___horzEdge0;
		TEdge_t3950806139 * L_73 = V_6;
		IntPoint_t3326126179  L_74 = V_8;
		Clipper_IntersectEdges_m1787610885(__this, L_72, L_73, L_74, (bool)1, /*hidden argument*/NULL);
		goto IL_01bc;
	}

IL_0192:
	{
		TEdge_t3950806139 * L_75 = V_6;
		NullCheck(L_75);
		IntPoint_t3326126179 * L_76 = L_75->get_address_of_Curr_1();
		int64_t L_77 = L_76->get_X_0();
		TEdge_t3950806139 * L_78 = ___horzEdge0;
		NullCheck(L_78);
		IntPoint_t3326126179 * L_79 = L_78->get_address_of_Curr_1();
		int64_t L_80 = L_79->get_Y_1();
		IntPoint__ctor_m1065894876((&V_9), L_77, L_80, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_81 = V_6;
		TEdge_t3950806139 * L_82 = ___horzEdge0;
		IntPoint_t3326126179  L_83 = V_9;
		Clipper_IntersectEdges_m1787610885(__this, L_81, L_82, L_83, (bool)1, /*hidden argument*/NULL);
	}

IL_01bc:
	{
		TEdge_t3950806139 * L_84 = ___horzEdge0;
		TEdge_t3950806139 * L_85 = V_6;
		Clipper_SwapPositionsInAEL_m423077188(__this, L_84, L_85, /*hidden argument*/NULL);
		goto IL_0200;
	}

IL_01ca:
	{
		int32_t L_86 = V_0;
		if ((!(((uint32_t)L_86) == ((uint32_t)1))))
		{
			goto IL_01e3;
		}
	}
	{
		TEdge_t3950806139 * L_87 = V_6;
		NullCheck(L_87);
		IntPoint_t3326126179 * L_88 = L_87->get_address_of_Curr_1();
		int64_t L_89 = L_88->get_X_0();
		int64_t L_90 = V_2;
		if ((((int64_t)L_89) >= ((int64_t)L_90)))
		{
			goto IL_01fb;
		}
	}

IL_01e3:
	{
		int32_t L_91 = V_0;
		if (L_91)
		{
			goto IL_0200;
		}
	}
	{
		TEdge_t3950806139 * L_92 = V_6;
		NullCheck(L_92);
		IntPoint_t3326126179 * L_93 = L_92->get_address_of_Curr_1();
		int64_t L_94 = L_93->get_X_0();
		int64_t L_95 = V_1;
		if ((((int64_t)L_94) > ((int64_t)L_95)))
		{
			goto IL_0200;
		}
	}

IL_01fb:
	{
		goto IL_020b;
	}

IL_0200:
	{
		TEdge_t3950806139 * L_96 = V_7;
		V_6 = L_96;
	}

IL_0204:
	{
		TEdge_t3950806139 * L_97 = V_6;
		if (L_97)
		{
			goto IL_0067;
		}
	}

IL_020b:
	{
		TEdge_t3950806139 * L_98 = ___horzEdge0;
		NullCheck(L_98);
		int32_t L_99 = L_98->get_OutIdx_10();
		if ((((int32_t)L_99) < ((int32_t)0)))
		{
			goto IL_022a;
		}
	}
	{
		TEdge_t3950806139 * L_100 = ___horzEdge0;
		NullCheck(L_100);
		int32_t L_101 = L_100->get_WindDelta_7();
		if (!L_101)
		{
			goto IL_022a;
		}
	}
	{
		TEdge_t3950806139 * L_102 = ___horzEdge0;
		bool L_103 = ___isTopOfScanbeam1;
		Clipper_PrepareHorzJoins_m1033324665(__this, L_102, L_103, /*hidden argument*/NULL);
	}

IL_022a:
	{
		TEdge_t3950806139 * L_104 = ___horzEdge0;
		NullCheck(L_104);
		TEdge_t3950806139 * L_105 = L_104->get_NextInLML_13();
		if (!L_105)
		{
			goto IL_0279;
		}
	}
	{
		TEdge_t3950806139 * L_106 = ___horzEdge0;
		NullCheck(L_106);
		TEdge_t3950806139 * L_107 = L_106->get_NextInLML_13();
		bool L_108 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_107, /*hidden argument*/NULL);
		if (!L_108)
		{
			goto IL_0279;
		}
	}
	{
		Clipper_UpdateEdgeIntoAEL_m1906260707(__this, (&___horzEdge0), /*hidden argument*/NULL);
		TEdge_t3950806139 * L_109 = ___horzEdge0;
		NullCheck(L_109);
		int32_t L_110 = L_109->get_OutIdx_10();
		if ((((int32_t)L_110) < ((int32_t)0)))
		{
			goto IL_0267;
		}
	}
	{
		TEdge_t3950806139 * L_111 = ___horzEdge0;
		TEdge_t3950806139 * L_112 = ___horzEdge0;
		NullCheck(L_112);
		IntPoint_t3326126179  L_113 = L_112->get_Bot_0();
		Clipper_AddOutPt_m3632434112(__this, L_111, L_113, /*hidden argument*/NULL);
	}

IL_0267:
	{
		TEdge_t3950806139 * L_114 = ___horzEdge0;
		Clipper_GetHorzDirection_m834255180(__this, L_114, (&V_0), (&V_1), (&V_2), /*hidden argument*/NULL);
		goto IL_027e;
	}

IL_0279:
	{
		goto IL_0283;
	}

IL_027e:
	{
		goto IL_0052;
	}

IL_0283:
	{
		TEdge_t3950806139 * L_115 = ___horzEdge0;
		NullCheck(L_115);
		TEdge_t3950806139 * L_116 = L_115->get_NextInLML_13();
		if (!L_116)
		{
			goto IL_0434;
		}
	}
	{
		TEdge_t3950806139 * L_117 = ___horzEdge0;
		NullCheck(L_117);
		int32_t L_118 = L_117->get_OutIdx_10();
		if ((((int32_t)L_118) < ((int32_t)0)))
		{
			goto IL_0427;
		}
	}
	{
		TEdge_t3950806139 * L_119 = ___horzEdge0;
		TEdge_t3950806139 * L_120 = ___horzEdge0;
		NullCheck(L_120);
		IntPoint_t3326126179  L_121 = L_120->get_Top_2();
		OutPt_t3947633180 * L_122 = Clipper_AddOutPt_m3632434112(__this, L_119, L_121, /*hidden argument*/NULL);
		V_10 = L_122;
		Clipper_UpdateEdgeIntoAEL_m1906260707(__this, (&___horzEdge0), /*hidden argument*/NULL);
		TEdge_t3950806139 * L_123 = ___horzEdge0;
		NullCheck(L_123);
		int32_t L_124 = L_123->get_WindDelta_7();
		if (L_124)
		{
			goto IL_02bd;
		}
	}
	{
		return;
	}

IL_02bd:
	{
		TEdge_t3950806139 * L_125 = ___horzEdge0;
		NullCheck(L_125);
		TEdge_t3950806139 * L_126 = L_125->get_PrevInAEL_15();
		V_11 = L_126;
		TEdge_t3950806139 * L_127 = ___horzEdge0;
		NullCheck(L_127);
		TEdge_t3950806139 * L_128 = L_127->get_NextInAEL_14();
		V_12 = L_128;
		TEdge_t3950806139 * L_129 = V_11;
		if (!L_129)
		{
			goto IL_037a;
		}
	}
	{
		TEdge_t3950806139 * L_130 = V_11;
		NullCheck(L_130);
		IntPoint_t3326126179 * L_131 = L_130->get_address_of_Curr_1();
		int64_t L_132 = L_131->get_X_0();
		TEdge_t3950806139 * L_133 = ___horzEdge0;
		NullCheck(L_133);
		IntPoint_t3326126179 * L_134 = L_133->get_address_of_Bot_0();
		int64_t L_135 = L_134->get_X_0();
		if ((!(((uint64_t)L_132) == ((uint64_t)L_135))))
		{
			goto IL_037a;
		}
	}
	{
		TEdge_t3950806139 * L_136 = V_11;
		NullCheck(L_136);
		IntPoint_t3326126179 * L_137 = L_136->get_address_of_Curr_1();
		int64_t L_138 = L_137->get_Y_1();
		TEdge_t3950806139 * L_139 = ___horzEdge0;
		NullCheck(L_139);
		IntPoint_t3326126179 * L_140 = L_139->get_address_of_Bot_0();
		int64_t L_141 = L_140->get_Y_1();
		if ((!(((uint64_t)L_138) == ((uint64_t)L_141))))
		{
			goto IL_037a;
		}
	}
	{
		TEdge_t3950806139 * L_142 = V_11;
		NullCheck(L_142);
		int32_t L_143 = L_142->get_WindDelta_7();
		if (!L_143)
		{
			goto IL_037a;
		}
	}
	{
		TEdge_t3950806139 * L_144 = V_11;
		NullCheck(L_144);
		int32_t L_145 = L_144->get_OutIdx_10();
		if ((((int32_t)L_145) < ((int32_t)0)))
		{
			goto IL_037a;
		}
	}
	{
		TEdge_t3950806139 * L_146 = V_11;
		NullCheck(L_146);
		IntPoint_t3326126179 * L_147 = L_146->get_address_of_Curr_1();
		int64_t L_148 = L_147->get_Y_1();
		TEdge_t3950806139 * L_149 = V_11;
		NullCheck(L_149);
		IntPoint_t3326126179 * L_150 = L_149->get_address_of_Top_2();
		int64_t L_151 = L_150->get_Y_1();
		if ((((int64_t)L_148) <= ((int64_t)L_151)))
		{
			goto IL_037a;
		}
	}
	{
		TEdge_t3950806139 * L_152 = ___horzEdge0;
		TEdge_t3950806139 * L_153 = V_11;
		bool L_154 = ((ClipperBase_t512275816 *)__this)->get_m_UseFullRange_9();
		bool L_155 = ClipperBase_SlopesEqual_m3887656866(NULL /*static, unused*/, L_152, L_153, L_154, /*hidden argument*/NULL);
		if (!L_155)
		{
			goto IL_037a;
		}
	}
	{
		TEdge_t3950806139 * L_156 = V_11;
		TEdge_t3950806139 * L_157 = ___horzEdge0;
		NullCheck(L_157);
		IntPoint_t3326126179  L_158 = L_157->get_Bot_0();
		OutPt_t3947633180 * L_159 = Clipper_AddOutPt_m3632434112(__this, L_156, L_158, /*hidden argument*/NULL);
		V_13 = L_159;
		OutPt_t3947633180 * L_160 = V_10;
		OutPt_t3947633180 * L_161 = V_13;
		TEdge_t3950806139 * L_162 = ___horzEdge0;
		NullCheck(L_162);
		IntPoint_t3326126179  L_163 = L_162->get_Top_2();
		Clipper_AddJoin_m2260685358(__this, L_160, L_161, L_163, /*hidden argument*/NULL);
		goto IL_0422;
	}

IL_037a:
	{
		TEdge_t3950806139 * L_164 = V_12;
		if (!L_164)
		{
			goto IL_0422;
		}
	}
	{
		TEdge_t3950806139 * L_165 = V_12;
		NullCheck(L_165);
		IntPoint_t3326126179 * L_166 = L_165->get_address_of_Curr_1();
		int64_t L_167 = L_166->get_X_0();
		TEdge_t3950806139 * L_168 = ___horzEdge0;
		NullCheck(L_168);
		IntPoint_t3326126179 * L_169 = L_168->get_address_of_Bot_0();
		int64_t L_170 = L_169->get_X_0();
		if ((!(((uint64_t)L_167) == ((uint64_t)L_170))))
		{
			goto IL_0422;
		}
	}
	{
		TEdge_t3950806139 * L_171 = V_12;
		NullCheck(L_171);
		IntPoint_t3326126179 * L_172 = L_171->get_address_of_Curr_1();
		int64_t L_173 = L_172->get_Y_1();
		TEdge_t3950806139 * L_174 = ___horzEdge0;
		NullCheck(L_174);
		IntPoint_t3326126179 * L_175 = L_174->get_address_of_Bot_0();
		int64_t L_176 = L_175->get_Y_1();
		if ((!(((uint64_t)L_173) == ((uint64_t)L_176))))
		{
			goto IL_0422;
		}
	}
	{
		TEdge_t3950806139 * L_177 = V_12;
		NullCheck(L_177);
		int32_t L_178 = L_177->get_WindDelta_7();
		if (!L_178)
		{
			goto IL_0422;
		}
	}
	{
		TEdge_t3950806139 * L_179 = V_12;
		NullCheck(L_179);
		int32_t L_180 = L_179->get_OutIdx_10();
		if ((((int32_t)L_180) < ((int32_t)0)))
		{
			goto IL_0422;
		}
	}
	{
		TEdge_t3950806139 * L_181 = V_12;
		NullCheck(L_181);
		IntPoint_t3326126179 * L_182 = L_181->get_address_of_Curr_1();
		int64_t L_183 = L_182->get_Y_1();
		TEdge_t3950806139 * L_184 = V_12;
		NullCheck(L_184);
		IntPoint_t3326126179 * L_185 = L_184->get_address_of_Top_2();
		int64_t L_186 = L_185->get_Y_1();
		if ((((int64_t)L_183) <= ((int64_t)L_186)))
		{
			goto IL_0422;
		}
	}
	{
		TEdge_t3950806139 * L_187 = ___horzEdge0;
		TEdge_t3950806139 * L_188 = V_12;
		bool L_189 = ((ClipperBase_t512275816 *)__this)->get_m_UseFullRange_9();
		bool L_190 = ClipperBase_SlopesEqual_m3887656866(NULL /*static, unused*/, L_187, L_188, L_189, /*hidden argument*/NULL);
		if (!L_190)
		{
			goto IL_0422;
		}
	}
	{
		TEdge_t3950806139 * L_191 = V_12;
		TEdge_t3950806139 * L_192 = ___horzEdge0;
		NullCheck(L_192);
		IntPoint_t3326126179  L_193 = L_192->get_Bot_0();
		OutPt_t3947633180 * L_194 = Clipper_AddOutPt_m3632434112(__this, L_191, L_193, /*hidden argument*/NULL);
		V_14 = L_194;
		OutPt_t3947633180 * L_195 = V_10;
		OutPt_t3947633180 * L_196 = V_14;
		TEdge_t3950806139 * L_197 = ___horzEdge0;
		NullCheck(L_197);
		IntPoint_t3326126179  L_198 = L_197->get_Top_2();
		Clipper_AddJoin_m2260685358(__this, L_195, L_196, L_198, /*hidden argument*/NULL);
	}

IL_0422:
	{
		goto IL_042f;
	}

IL_0427:
	{
		Clipper_UpdateEdgeIntoAEL_m1906260707(__this, (&___horzEdge0), /*hidden argument*/NULL);
	}

IL_042f:
	{
		goto IL_04c6;
	}

IL_0434:
	{
		TEdge_t3950806139 * L_199 = V_4;
		if (!L_199)
		{
			goto IL_04a5;
		}
	}
	{
		TEdge_t3950806139 * L_200 = V_4;
		NullCheck(L_200);
		int32_t L_201 = L_200->get_OutIdx_10();
		if ((((int32_t)L_201) < ((int32_t)0)))
		{
			goto IL_0491;
		}
	}
	{
		int32_t L_202 = V_0;
		if ((!(((uint32_t)L_202) == ((uint32_t)1))))
		{
			goto IL_0464;
		}
	}
	{
		TEdge_t3950806139 * L_203 = ___horzEdge0;
		TEdge_t3950806139 * L_204 = V_4;
		TEdge_t3950806139 * L_205 = ___horzEdge0;
		NullCheck(L_205);
		IntPoint_t3326126179  L_206 = L_205->get_Top_2();
		Clipper_IntersectEdges_m1787610885(__this, L_203, L_204, L_206, (bool)0, /*hidden argument*/NULL);
		goto IL_0474;
	}

IL_0464:
	{
		TEdge_t3950806139 * L_207 = V_4;
		TEdge_t3950806139 * L_208 = ___horzEdge0;
		TEdge_t3950806139 * L_209 = ___horzEdge0;
		NullCheck(L_209);
		IntPoint_t3326126179  L_210 = L_209->get_Top_2();
		Clipper_IntersectEdges_m1787610885(__this, L_207, L_208, L_210, (bool)0, /*hidden argument*/NULL);
	}

IL_0474:
	{
		TEdge_t3950806139 * L_211 = V_4;
		NullCheck(L_211);
		int32_t L_212 = L_211->get_OutIdx_10();
		if ((((int32_t)L_212) < ((int32_t)0)))
		{
			goto IL_048c;
		}
	}
	{
		ClipperException_t2818206852 * L_213 = (ClipperException_t2818206852 *)il2cpp_codegen_object_new(ClipperException_t2818206852_il2cpp_TypeInfo_var);
		ClipperException__ctor_m106504313(L_213, _stringLiteral3907404667, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_213);
	}

IL_048c:
	{
		goto IL_04a0;
	}

IL_0491:
	{
		TEdge_t3950806139 * L_214 = ___horzEdge0;
		Clipper_DeleteFromAEL_m2256743194(__this, L_214, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_215 = V_4;
		Clipper_DeleteFromAEL_m2256743194(__this, L_215, /*hidden argument*/NULL);
	}

IL_04a0:
	{
		goto IL_04c6;
	}

IL_04a5:
	{
		TEdge_t3950806139 * L_216 = ___horzEdge0;
		NullCheck(L_216);
		int32_t L_217 = L_216->get_OutIdx_10();
		if ((((int32_t)L_217) < ((int32_t)0)))
		{
			goto IL_04bf;
		}
	}
	{
		TEdge_t3950806139 * L_218 = ___horzEdge0;
		TEdge_t3950806139 * L_219 = ___horzEdge0;
		NullCheck(L_219);
		IntPoint_t3326126179  L_220 = L_219->get_Top_2();
		Clipper_AddOutPt_m3632434112(__this, L_218, L_220, /*hidden argument*/NULL);
	}

IL_04bf:
	{
		TEdge_t3950806139 * L_221 = ___horzEdge0;
		Clipper_DeleteFromAEL_m2256743194(__this, L_221, /*hidden argument*/NULL);
	}

IL_04c6:
	{
		return;
	}
}
// Pathfinding.ClipperLib.TEdge Pathfinding.ClipperLib.Clipper::GetNextInAEL(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.Direction)
extern "C"  TEdge_t3950806139 * Clipper_GetNextInAEL_m1569553480 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e0, int32_t ___Direction1, const MethodInfo* method)
{
	TEdge_t3950806139 * G_B3_0 = NULL;
	{
		int32_t L_0 = ___Direction1;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0012;
		}
	}
	{
		TEdge_t3950806139 * L_1 = ___e0;
		NullCheck(L_1);
		TEdge_t3950806139 * L_2 = L_1->get_NextInAEL_14();
		G_B3_0 = L_2;
		goto IL_0018;
	}

IL_0012:
	{
		TEdge_t3950806139 * L_3 = ___e0;
		NullCheck(L_3);
		TEdge_t3950806139 * L_4 = L_3->get_PrevInAEL_15();
		G_B3_0 = L_4;
	}

IL_0018:
	{
		return G_B3_0;
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::IsMaxima(Pathfinding.ClipperLib.TEdge,System.Double)
extern "C"  bool Clipper_IsMaxima_m2272585228 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e0, double ___Y1, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		TEdge_t3950806139 * L_0 = ___e0;
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		TEdge_t3950806139 * L_1 = ___e0;
		NullCheck(L_1);
		IntPoint_t3326126179 * L_2 = L_1->get_address_of_Top_2();
		int64_t L_3 = L_2->get_Y_1();
		double L_4 = ___Y1;
		if ((!(((double)(((double)((double)L_3)))) == ((double)L_4))))
		{
			goto IL_0023;
		}
	}
	{
		TEdge_t3950806139 * L_5 = ___e0;
		NullCheck(L_5);
		TEdge_t3950806139 * L_6 = L_5->get_NextInLML_13();
		G_B4_0 = ((((Il2CppObject*)(TEdge_t3950806139 *)L_6) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		goto IL_0024;
	}

IL_0023:
	{
		G_B4_0 = 0;
	}

IL_0024:
	{
		return (bool)G_B4_0;
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::IsIntermediate(Pathfinding.ClipperLib.TEdge,System.Double)
extern "C"  bool Clipper_IsIntermediate_m743671532 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e0, double ___Y1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		TEdge_t3950806139 * L_0 = ___e0;
		NullCheck(L_0);
		IntPoint_t3326126179 * L_1 = L_0->get_address_of_Top_2();
		int64_t L_2 = L_1->get_Y_1();
		double L_3 = ___Y1;
		if ((!(((double)(((double)((double)L_2)))) == ((double)L_3))))
		{
			goto IL_0020;
		}
	}
	{
		TEdge_t3950806139 * L_4 = ___e0;
		NullCheck(L_4);
		TEdge_t3950806139 * L_5 = L_4->get_NextInLML_13();
		G_B3_0 = ((((int32_t)((((Il2CppObject*)(TEdge_t3950806139 *)L_5) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0021;
	}

IL_0020:
	{
		G_B3_0 = 0;
	}

IL_0021:
	{
		return (bool)G_B3_0;
	}
}
// Pathfinding.ClipperLib.TEdge Pathfinding.ClipperLib.Clipper::GetMaximaPair(Pathfinding.ClipperLib.TEdge)
extern "C"  TEdge_t3950806139 * Clipper_GetMaximaPair_m3274930736 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e0, const MethodInfo* method)
{
	TEdge_t3950806139 * V_0 = NULL;
	{
		V_0 = (TEdge_t3950806139 *)NULL;
		TEdge_t3950806139 * L_0 = ___e0;
		NullCheck(L_0);
		TEdge_t3950806139 * L_1 = L_0->get_Next_11();
		NullCheck(L_1);
		IntPoint_t3326126179  L_2 = L_1->get_Top_2();
		TEdge_t3950806139 * L_3 = ___e0;
		NullCheck(L_3);
		IntPoint_t3326126179  L_4 = L_3->get_Top_2();
		bool L_5 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0039;
		}
	}
	{
		TEdge_t3950806139 * L_6 = ___e0;
		NullCheck(L_6);
		TEdge_t3950806139 * L_7 = L_6->get_Next_11();
		NullCheck(L_7);
		TEdge_t3950806139 * L_8 = L_7->get_NextInLML_13();
		if (L_8)
		{
			goto IL_0039;
		}
	}
	{
		TEdge_t3950806139 * L_9 = ___e0;
		NullCheck(L_9);
		TEdge_t3950806139 * L_10 = L_9->get_Next_11();
		V_0 = L_10;
		goto IL_006b;
	}

IL_0039:
	{
		TEdge_t3950806139 * L_11 = ___e0;
		NullCheck(L_11);
		TEdge_t3950806139 * L_12 = L_11->get_Prev_12();
		NullCheck(L_12);
		IntPoint_t3326126179  L_13 = L_12->get_Top_2();
		TEdge_t3950806139 * L_14 = ___e0;
		NullCheck(L_14);
		IntPoint_t3326126179  L_15 = L_14->get_Top_2();
		bool L_16 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_006b;
		}
	}
	{
		TEdge_t3950806139 * L_17 = ___e0;
		NullCheck(L_17);
		TEdge_t3950806139 * L_18 = L_17->get_Prev_12();
		NullCheck(L_18);
		TEdge_t3950806139 * L_19 = L_18->get_NextInLML_13();
		if (L_19)
		{
			goto IL_006b;
		}
	}
	{
		TEdge_t3950806139 * L_20 = ___e0;
		NullCheck(L_20);
		TEdge_t3950806139 * L_21 = L_20->get_Prev_12();
		V_0 = L_21;
	}

IL_006b:
	{
		TEdge_t3950806139 * L_22 = V_0;
		if (!L_22)
		{
			goto IL_009c;
		}
	}
	{
		TEdge_t3950806139 * L_23 = V_0;
		NullCheck(L_23);
		int32_t L_24 = L_23->get_OutIdx_10();
		if ((((int32_t)L_24) == ((int32_t)((int32_t)-2))))
		{
			goto IL_009a;
		}
	}
	{
		TEdge_t3950806139 * L_25 = V_0;
		NullCheck(L_25);
		TEdge_t3950806139 * L_26 = L_25->get_NextInAEL_14();
		TEdge_t3950806139 * L_27 = V_0;
		NullCheck(L_27);
		TEdge_t3950806139 * L_28 = L_27->get_PrevInAEL_15();
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_26) == ((Il2CppObject*)(TEdge_t3950806139 *)L_28))))
		{
			goto IL_009c;
		}
	}
	{
		TEdge_t3950806139 * L_29 = V_0;
		bool L_30 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		if (L_30)
		{
			goto IL_009c;
		}
	}

IL_009a:
	{
		return (TEdge_t3950806139 *)NULL;
	}

IL_009c:
	{
		TEdge_t3950806139 * L_31 = V_0;
		return L_31;
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::ProcessIntersections(System.Int64,System.Int64)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* ClipperException_t2818206852_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227045027;
extern const uint32_t Clipper_ProcessIntersections_m661125977_MetadataUsageId;
extern "C"  bool Clipper_ProcessIntersections_m661125977 (Clipper_t636949239 * __this, int64_t ___botY0, int64_t ___topY1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_ProcessIntersections_m661125977_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		TEdge_t3950806139 * L_0 = __this->get_m_ActiveEdges_18();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)1;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			int64_t L_1 = ___botY0;
			int64_t L_2 = ___topY1;
			Clipper_BuildIntersectList_m563933121(__this, L_1, L_2, /*hidden argument*/NULL);
			IntersectNode_t4106323947 * L_3 = __this->get_m_IntersectNodes_20();
			if (L_3)
			{
				goto IL_0027;
			}
		}

IL_0020:
		{
			V_0 = (bool)1;
			goto IL_0080;
		}

IL_0027:
		{
			IntersectNode_t4106323947 * L_4 = __this->get_m_IntersectNodes_20();
			NullCheck(L_4);
			IntersectNode_t4106323947 * L_5 = L_4->get_Next_3();
			if (!L_5)
			{
				goto IL_0042;
			}
		}

IL_0037:
		{
			bool L_6 = Clipper_FixupIntersectionOrder_m1801248039(__this, /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_004d;
			}
		}

IL_0042:
		{
			Clipper_ProcessIntersectList_m4219475268(__this, /*hidden argument*/NULL);
			goto IL_0054;
		}

IL_004d:
		{
			V_0 = (bool)0;
			goto IL_0080;
		}

IL_0054:
		{
			goto IL_0077;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0059;
		throw e;
	}

CATCH_0059:
	{ // begin catch(System.Object)
		{
			__this->set_m_SortedEdges_19((TEdge_t3950806139 *)NULL);
			Clipper_DisposeIntersectNodes_m1202039965(__this, /*hidden argument*/NULL);
			ClipperException_t2818206852 * L_7 = (ClipperException_t2818206852 *)il2cpp_codegen_object_new(ClipperException_t2818206852_il2cpp_TypeInfo_var);
			ClipperException__ctor_m106504313(L_7, _stringLiteral4227045027, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
		}

IL_0072:
		{
			goto IL_0077;
		}
	} // end catch (depth: 1)

IL_0077:
	{
		__this->set_m_SortedEdges_19((TEdge_t3950806139 *)NULL);
		return (bool)1;
	}

IL_0080:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::BuildIntersectList(System.Int64,System.Int64)
extern Il2CppClass* ClipperException_t2818206852_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral984613329;
extern const uint32_t Clipper_BuildIntersectList_m563933121_MetadataUsageId;
extern "C"  void Clipper_BuildIntersectList_m563933121 (Clipper_t636949239 * __this, int64_t ___botY0, int64_t ___topY1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_BuildIntersectList_m563933121_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TEdge_t3950806139 * V_0 = NULL;
	bool V_1 = false;
	TEdge_t3950806139 * V_2 = NULL;
	IntPoint_t3326126179  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		TEdge_t3950806139 * L_0 = __this->get_m_ActiveEdges_18();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		TEdge_t3950806139 * L_1 = __this->get_m_ActiveEdges_18();
		V_0 = L_1;
		TEdge_t3950806139 * L_2 = V_0;
		__this->set_m_SortedEdges_19(L_2);
		goto IL_0050;
	}

IL_001f:
	{
		TEdge_t3950806139 * L_3 = V_0;
		TEdge_t3950806139 * L_4 = V_0;
		NullCheck(L_4);
		TEdge_t3950806139 * L_5 = L_4->get_PrevInAEL_15();
		NullCheck(L_3);
		L_3->set_PrevInSEL_17(L_5);
		TEdge_t3950806139 * L_6 = V_0;
		TEdge_t3950806139 * L_7 = V_0;
		NullCheck(L_7);
		TEdge_t3950806139 * L_8 = L_7->get_NextInAEL_14();
		NullCheck(L_6);
		L_6->set_NextInSEL_16(L_8);
		TEdge_t3950806139 * L_9 = V_0;
		NullCheck(L_9);
		IntPoint_t3326126179 * L_10 = L_9->get_address_of_Curr_1();
		TEdge_t3950806139 * L_11 = V_0;
		int64_t L_12 = ___topY1;
		int64_t L_13 = Clipper_TopX_m1760470797(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		L_10->set_X_0(L_13);
		TEdge_t3950806139 * L_14 = V_0;
		NullCheck(L_14);
		TEdge_t3950806139 * L_15 = L_14->get_NextInAEL_14();
		V_0 = L_15;
	}

IL_0050:
	{
		TEdge_t3950806139 * L_16 = V_0;
		if (L_16)
		{
			goto IL_001f;
		}
	}
	{
		V_1 = (bool)1;
		goto IL_015c;
	}

IL_005d:
	{
		V_1 = (bool)0;
		TEdge_t3950806139 * L_17 = __this->get_m_SortedEdges_19();
		V_0 = L_17;
		goto IL_0130;
	}

IL_006b:
	{
		TEdge_t3950806139 * L_18 = V_0;
		NullCheck(L_18);
		TEdge_t3950806139 * L_19 = L_18->get_NextInSEL_16();
		V_2 = L_19;
		TEdge_t3950806139 * L_20 = V_0;
		NullCheck(L_20);
		IntPoint_t3326126179 * L_21 = L_20->get_address_of_Curr_1();
		int64_t L_22 = L_21->get_X_0();
		TEdge_t3950806139 * L_23 = V_2;
		NullCheck(L_23);
		IntPoint_t3326126179 * L_24 = L_23->get_address_of_Curr_1();
		int64_t L_25 = L_24->get_X_0();
		if ((((int64_t)L_22) <= ((int64_t)L_25)))
		{
			goto IL_012e;
		}
	}
	{
		TEdge_t3950806139 * L_26 = V_0;
		TEdge_t3950806139 * L_27 = V_2;
		bool L_28 = Clipper_IntersectPoint_m1806336746(__this, L_26, L_27, (&V_3), /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_00c5;
		}
	}
	{
		TEdge_t3950806139 * L_29 = V_0;
		NullCheck(L_29);
		IntPoint_t3326126179 * L_30 = L_29->get_address_of_Curr_1();
		int64_t L_31 = L_30->get_X_0();
		TEdge_t3950806139 * L_32 = V_2;
		NullCheck(L_32);
		IntPoint_t3326126179 * L_33 = L_32->get_address_of_Curr_1();
		int64_t L_34 = L_33->get_X_0();
		if ((((int64_t)L_31) <= ((int64_t)((int64_t)((int64_t)L_34+(int64_t)(((int64_t)((int64_t)1))))))))
		{
			goto IL_00c5;
		}
	}
	{
		ClipperException_t2818206852 * L_35 = (ClipperException_t2818206852 *)il2cpp_codegen_object_new(ClipperException_t2818206852_il2cpp_TypeInfo_var);
		ClipperException__ctor_m106504313(L_35, _stringLiteral984613329, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_35);
	}

IL_00c5:
	{
		int64_t L_36 = (&V_3)->get_Y_1();
		int64_t L_37 = ___botY0;
		if ((((int64_t)L_36) <= ((int64_t)L_37)))
		{
			goto IL_0116;
		}
	}
	{
		int64_t L_38 = ___botY0;
		(&V_3)->set_Y_1(L_38);
		TEdge_t3950806139 * L_39 = V_0;
		NullCheck(L_39);
		double L_40 = L_39->get_Dx_4();
		double L_41 = fabs(L_40);
		TEdge_t3950806139 * L_42 = V_2;
		NullCheck(L_42);
		double L_43 = L_42->get_Dx_4();
		double L_44 = fabs(L_43);
		if ((!(((double)L_41) > ((double)L_44))))
		{
			goto IL_0108;
		}
	}
	{
		TEdge_t3950806139 * L_45 = V_2;
		int64_t L_46 = ___botY0;
		int64_t L_47 = Clipper_TopX_m1760470797(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
		(&V_3)->set_X_0(L_47);
		goto IL_0116;
	}

IL_0108:
	{
		TEdge_t3950806139 * L_48 = V_0;
		int64_t L_49 = ___botY0;
		int64_t L_50 = Clipper_TopX_m1760470797(NULL /*static, unused*/, L_48, L_49, /*hidden argument*/NULL);
		(&V_3)->set_X_0(L_50);
	}

IL_0116:
	{
		TEdge_t3950806139 * L_51 = V_0;
		TEdge_t3950806139 * L_52 = V_2;
		IntPoint_t3326126179  L_53 = V_3;
		Clipper_InsertIntersectNode_m3406234641(__this, L_51, L_52, L_53, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_54 = V_0;
		TEdge_t3950806139 * L_55 = V_2;
		Clipper_SwapPositionsInSEL_m2241560178(__this, L_54, L_55, /*hidden argument*/NULL);
		V_1 = (bool)1;
		goto IL_0130;
	}

IL_012e:
	{
		TEdge_t3950806139 * L_56 = V_2;
		V_0 = L_56;
	}

IL_0130:
	{
		TEdge_t3950806139 * L_57 = V_0;
		NullCheck(L_57);
		TEdge_t3950806139 * L_58 = L_57->get_NextInSEL_16();
		if (L_58)
		{
			goto IL_006b;
		}
	}
	{
		TEdge_t3950806139 * L_59 = V_0;
		NullCheck(L_59);
		TEdge_t3950806139 * L_60 = L_59->get_PrevInSEL_17();
		if (!L_60)
		{
			goto IL_0157;
		}
	}
	{
		TEdge_t3950806139 * L_61 = V_0;
		NullCheck(L_61);
		TEdge_t3950806139 * L_62 = L_61->get_PrevInSEL_17();
		NullCheck(L_62);
		L_62->set_NextInSEL_16((TEdge_t3950806139 *)NULL);
		goto IL_015c;
	}

IL_0157:
	{
		goto IL_016d;
	}

IL_015c:
	{
		bool L_63 = V_1;
		if (!L_63)
		{
			goto IL_016d;
		}
	}
	{
		TEdge_t3950806139 * L_64 = __this->get_m_SortedEdges_19();
		if (L_64)
		{
			goto IL_005d;
		}
	}

IL_016d:
	{
		__this->set_m_SortedEdges_19((TEdge_t3950806139 *)NULL);
		return;
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::EdgesAdjacent(Pathfinding.ClipperLib.IntersectNode)
extern "C"  bool Clipper_EdgesAdjacent_m1627663155 (Clipper_t636949239 * __this, IntersectNode_t4106323947 * ___inode0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		IntersectNode_t4106323947 * L_0 = ___inode0;
		NullCheck(L_0);
		TEdge_t3950806139 * L_1 = L_0->get_Edge1_0();
		NullCheck(L_1);
		TEdge_t3950806139 * L_2 = L_1->get_NextInSEL_16();
		IntersectNode_t4106323947 * L_3 = ___inode0;
		NullCheck(L_3);
		TEdge_t3950806139 * L_4 = L_3->get_Edge2_1();
		if ((((Il2CppObject*)(TEdge_t3950806139 *)L_2) == ((Il2CppObject*)(TEdge_t3950806139 *)L_4)))
		{
			goto IL_002b;
		}
	}
	{
		IntersectNode_t4106323947 * L_5 = ___inode0;
		NullCheck(L_5);
		TEdge_t3950806139 * L_6 = L_5->get_Edge1_0();
		NullCheck(L_6);
		TEdge_t3950806139 * L_7 = L_6->get_PrevInSEL_17();
		IntersectNode_t4106323947 * L_8 = ___inode0;
		NullCheck(L_8);
		TEdge_t3950806139 * L_9 = L_8->get_Edge2_1();
		G_B3_0 = ((((Il2CppObject*)(TEdge_t3950806139 *)L_7) == ((Il2CppObject*)(TEdge_t3950806139 *)L_9))? 1 : 0);
		goto IL_002c;
	}

IL_002b:
	{
		G_B3_0 = 1;
	}

IL_002c:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::FixupIntersectionOrder()
extern "C"  bool Clipper_FixupIntersectionOrder_m1801248039 (Clipper_t636949239 * __this, const MethodInfo* method)
{
	IntersectNode_t4106323947 * V_0 = NULL;
	IntersectNode_t4106323947 * V_1 = NULL;
	{
		IntersectNode_t4106323947 * L_0 = __this->get_m_IntersectNodes_20();
		V_0 = L_0;
		Clipper_CopyAELToSEL_m3766598946(__this, /*hidden argument*/NULL);
		goto IL_006c;
	}

IL_0012:
	{
		IntersectNode_t4106323947 * L_1 = V_0;
		bool L_2 = Clipper_EdgesAdjacent_m1627663155(__this, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0053;
		}
	}
	{
		IntersectNode_t4106323947 * L_3 = V_0;
		NullCheck(L_3);
		IntersectNode_t4106323947 * L_4 = L_3->get_Next_3();
		V_1 = L_4;
		goto IL_0031;
	}

IL_002a:
	{
		IntersectNode_t4106323947 * L_5 = V_1;
		NullCheck(L_5);
		IntersectNode_t4106323947 * L_6 = L_5->get_Next_3();
		V_1 = L_6;
	}

IL_0031:
	{
		IntersectNode_t4106323947 * L_7 = V_1;
		if (!L_7)
		{
			goto IL_0043;
		}
	}
	{
		IntersectNode_t4106323947 * L_8 = V_1;
		bool L_9 = Clipper_EdgesAdjacent_m1627663155(__this, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_002a;
		}
	}

IL_0043:
	{
		IntersectNode_t4106323947 * L_10 = V_1;
		if (L_10)
		{
			goto IL_004b;
		}
	}
	{
		return (bool)0;
	}

IL_004b:
	{
		IntersectNode_t4106323947 * L_11 = V_0;
		IntersectNode_t4106323947 * L_12 = V_1;
		Clipper_SwapIntersectNodes_m33861867(__this, L_11, L_12, /*hidden argument*/NULL);
	}

IL_0053:
	{
		IntersectNode_t4106323947 * L_13 = V_0;
		NullCheck(L_13);
		TEdge_t3950806139 * L_14 = L_13->get_Edge1_0();
		IntersectNode_t4106323947 * L_15 = V_0;
		NullCheck(L_15);
		TEdge_t3950806139 * L_16 = L_15->get_Edge2_1();
		Clipper_SwapPositionsInSEL_m2241560178(__this, L_14, L_16, /*hidden argument*/NULL);
		IntersectNode_t4106323947 * L_17 = V_0;
		NullCheck(L_17);
		IntersectNode_t4106323947 * L_18 = L_17->get_Next_3();
		V_0 = L_18;
	}

IL_006c:
	{
		IntersectNode_t4106323947 * L_19 = V_0;
		if (L_19)
		{
			goto IL_0012;
		}
	}
	{
		return (bool)1;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::ProcessIntersectList()
extern "C"  void Clipper_ProcessIntersectList_m4219475268 (Clipper_t636949239 * __this, const MethodInfo* method)
{
	IntersectNode_t4106323947 * V_0 = NULL;
	{
		goto IL_0063;
	}

IL_0005:
	{
		IntersectNode_t4106323947 * L_0 = __this->get_m_IntersectNodes_20();
		NullCheck(L_0);
		IntersectNode_t4106323947 * L_1 = L_0->get_Next_3();
		V_0 = L_1;
		IntersectNode_t4106323947 * L_2 = __this->get_m_IntersectNodes_20();
		NullCheck(L_2);
		TEdge_t3950806139 * L_3 = L_2->get_Edge1_0();
		IntersectNode_t4106323947 * L_4 = __this->get_m_IntersectNodes_20();
		NullCheck(L_4);
		TEdge_t3950806139 * L_5 = L_4->get_Edge2_1();
		IntersectNode_t4106323947 * L_6 = __this->get_m_IntersectNodes_20();
		NullCheck(L_6);
		IntPoint_t3326126179  L_7 = L_6->get_Pt_2();
		Clipper_IntersectEdges_m1787610885(__this, L_3, L_5, L_7, (bool)1, /*hidden argument*/NULL);
		IntersectNode_t4106323947 * L_8 = __this->get_m_IntersectNodes_20();
		NullCheck(L_8);
		TEdge_t3950806139 * L_9 = L_8->get_Edge1_0();
		IntersectNode_t4106323947 * L_10 = __this->get_m_IntersectNodes_20();
		NullCheck(L_10);
		TEdge_t3950806139 * L_11 = L_10->get_Edge2_1();
		Clipper_SwapPositionsInAEL_m423077188(__this, L_9, L_11, /*hidden argument*/NULL);
		__this->set_m_IntersectNodes_20((IntersectNode_t4106323947 *)NULL);
		IntersectNode_t4106323947 * L_12 = V_0;
		__this->set_m_IntersectNodes_20(L_12);
	}

IL_0063:
	{
		IntersectNode_t4106323947 * L_13 = __this->get_m_IntersectNodes_20();
		if (L_13)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Int64 Pathfinding.ClipperLib.Clipper::Round(System.Double)
extern "C"  int64_t Clipper_Round_m1914183711 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method)
{
	int64_t G_B3_0 = 0;
	{
		double L_0 = ___value0;
		if ((!(((double)L_0) < ((double)(0.0)))))
		{
			goto IL_0020;
		}
	}
	{
		double L_1 = ___value0;
		G_B3_0 = (((int64_t)((int64_t)((double)((double)L_1-(double)(0.5))))));
		goto IL_002c;
	}

IL_0020:
	{
		double L_2 = ___value0;
		G_B3_0 = (((int64_t)((int64_t)((double)((double)L_2+(double)(0.5))))));
	}

IL_002c:
	{
		return G_B3_0;
	}
}
// System.Int64 Pathfinding.ClipperLib.Clipper::TopX(Pathfinding.ClipperLib.TEdge,System.Int64)
extern "C"  int64_t Clipper_TopX_m1760470797 (Il2CppObject * __this /* static, unused */, TEdge_t3950806139 * ___edge0, int64_t ___currentY1, const MethodInfo* method)
{
	{
		int64_t L_0 = ___currentY1;
		TEdge_t3950806139 * L_1 = ___edge0;
		NullCheck(L_1);
		IntPoint_t3326126179 * L_2 = L_1->get_address_of_Top_2();
		int64_t L_3 = L_2->get_Y_1();
		if ((!(((uint64_t)L_0) == ((uint64_t)L_3))))
		{
			goto IL_001d;
		}
	}
	{
		TEdge_t3950806139 * L_4 = ___edge0;
		NullCheck(L_4);
		IntPoint_t3326126179 * L_5 = L_4->get_address_of_Top_2();
		int64_t L_6 = L_5->get_X_0();
		return L_6;
	}

IL_001d:
	{
		TEdge_t3950806139 * L_7 = ___edge0;
		NullCheck(L_7);
		IntPoint_t3326126179 * L_8 = L_7->get_address_of_Bot_0();
		int64_t L_9 = L_8->get_X_0();
		TEdge_t3950806139 * L_10 = ___edge0;
		NullCheck(L_10);
		double L_11 = L_10->get_Dx_4();
		int64_t L_12 = ___currentY1;
		TEdge_t3950806139 * L_13 = ___edge0;
		NullCheck(L_13);
		IntPoint_t3326126179 * L_14 = L_13->get_address_of_Bot_0();
		int64_t L_15 = L_14->get_Y_1();
		int64_t L_16 = Clipper_Round_m1914183711(NULL /*static, unused*/, ((double)((double)L_11*(double)(((double)((double)((int64_t)((int64_t)L_12-(int64_t)L_15))))))), /*hidden argument*/NULL);
		return ((int64_t)((int64_t)L_9+(int64_t)L_16));
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::InsertIntersectNode(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.IntPoint)
extern Il2CppClass* IntersectNode_t4106323947_il2cpp_TypeInfo_var;
extern const uint32_t Clipper_InsertIntersectNode_m3406234641_MetadataUsageId;
extern "C"  void Clipper_InsertIntersectNode_m3406234641 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e10, TEdge_t3950806139 * ___e21, IntPoint_t3326126179  ___pt2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_InsertIntersectNode_m3406234641_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntersectNode_t4106323947 * V_0 = NULL;
	IntersectNode_t4106323947 * V_1 = NULL;
	{
		IntersectNode_t4106323947 * L_0 = (IntersectNode_t4106323947 *)il2cpp_codegen_object_new(IntersectNode_t4106323947_il2cpp_TypeInfo_var);
		IntersectNode__ctor_m641175930(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		IntersectNode_t4106323947 * L_1 = V_0;
		TEdge_t3950806139 * L_2 = ___e10;
		NullCheck(L_1);
		L_1->set_Edge1_0(L_2);
		IntersectNode_t4106323947 * L_3 = V_0;
		TEdge_t3950806139 * L_4 = ___e21;
		NullCheck(L_3);
		L_3->set_Edge2_1(L_4);
		IntersectNode_t4106323947 * L_5 = V_0;
		IntPoint_t3326126179  L_6 = ___pt2;
		NullCheck(L_5);
		L_5->set_Pt_2(L_6);
		IntersectNode_t4106323947 * L_7 = V_0;
		NullCheck(L_7);
		L_7->set_Next_3((IntersectNode_t4106323947 *)NULL);
		IntersectNode_t4106323947 * L_8 = __this->get_m_IntersectNodes_20();
		if (L_8)
		{
			goto IL_0039;
		}
	}
	{
		IntersectNode_t4106323947 * L_9 = V_0;
		__this->set_m_IntersectNodes_20(L_9);
		goto IL_00c2;
	}

IL_0039:
	{
		IntersectNode_t4106323947 * L_10 = V_0;
		NullCheck(L_10);
		IntPoint_t3326126179 * L_11 = L_10->get_address_of_Pt_2();
		int64_t L_12 = L_11->get_Y_1();
		IntersectNode_t4106323947 * L_13 = __this->get_m_IntersectNodes_20();
		NullCheck(L_13);
		IntPoint_t3326126179 * L_14 = L_13->get_address_of_Pt_2();
		int64_t L_15 = L_14->get_Y_1();
		if ((((int64_t)L_12) <= ((int64_t)L_15)))
		{
			goto IL_0071;
		}
	}
	{
		IntersectNode_t4106323947 * L_16 = V_0;
		IntersectNode_t4106323947 * L_17 = __this->get_m_IntersectNodes_20();
		NullCheck(L_16);
		L_16->set_Next_3(L_17);
		IntersectNode_t4106323947 * L_18 = V_0;
		__this->set_m_IntersectNodes_20(L_18);
		goto IL_00c2;
	}

IL_0071:
	{
		IntersectNode_t4106323947 * L_19 = __this->get_m_IntersectNodes_20();
		V_1 = L_19;
		goto IL_0084;
	}

IL_007d:
	{
		IntersectNode_t4106323947 * L_20 = V_1;
		NullCheck(L_20);
		IntersectNode_t4106323947 * L_21 = L_20->get_Next_3();
		V_1 = L_21;
	}

IL_0084:
	{
		IntersectNode_t4106323947 * L_22 = V_1;
		NullCheck(L_22);
		IntersectNode_t4106323947 * L_23 = L_22->get_Next_3();
		if (!L_23)
		{
			goto IL_00af;
		}
	}
	{
		IntersectNode_t4106323947 * L_24 = V_0;
		NullCheck(L_24);
		IntPoint_t3326126179 * L_25 = L_24->get_address_of_Pt_2();
		int64_t L_26 = L_25->get_Y_1();
		IntersectNode_t4106323947 * L_27 = V_1;
		NullCheck(L_27);
		IntersectNode_t4106323947 * L_28 = L_27->get_Next_3();
		NullCheck(L_28);
		IntPoint_t3326126179 * L_29 = L_28->get_address_of_Pt_2();
		int64_t L_30 = L_29->get_Y_1();
		if ((((int64_t)L_26) < ((int64_t)L_30)))
		{
			goto IL_007d;
		}
	}

IL_00af:
	{
		IntersectNode_t4106323947 * L_31 = V_0;
		IntersectNode_t4106323947 * L_32 = V_1;
		NullCheck(L_32);
		IntersectNode_t4106323947 * L_33 = L_32->get_Next_3();
		NullCheck(L_31);
		L_31->set_Next_3(L_33);
		IntersectNode_t4106323947 * L_34 = V_1;
		IntersectNode_t4106323947 * L_35 = V_0;
		NullCheck(L_34);
		L_34->set_Next_3(L_35);
	}

IL_00c2:
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::SwapIntersectNodes(Pathfinding.ClipperLib.IntersectNode,Pathfinding.ClipperLib.IntersectNode)
extern "C"  void Clipper_SwapIntersectNodes_m33861867 (Clipper_t636949239 * __this, IntersectNode_t4106323947 * ___int10, IntersectNode_t4106323947 * ___int21, const MethodInfo* method)
{
	TEdge_t3950806139 * V_0 = NULL;
	TEdge_t3950806139 * V_1 = NULL;
	IntPoint_t3326126179  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		IntersectNode_t4106323947 * L_0 = ___int10;
		NullCheck(L_0);
		TEdge_t3950806139 * L_1 = L_0->get_Edge1_0();
		V_0 = L_1;
		IntersectNode_t4106323947 * L_2 = ___int10;
		NullCheck(L_2);
		TEdge_t3950806139 * L_3 = L_2->get_Edge2_1();
		V_1 = L_3;
		IntersectNode_t4106323947 * L_4 = ___int10;
		NullCheck(L_4);
		IntPoint_t3326126179  L_5 = L_4->get_Pt_2();
		IntPoint__ctor_m964253275((&V_2), L_5, /*hidden argument*/NULL);
		IntersectNode_t4106323947 * L_6 = ___int10;
		IntersectNode_t4106323947 * L_7 = ___int21;
		NullCheck(L_7);
		TEdge_t3950806139 * L_8 = L_7->get_Edge1_0();
		NullCheck(L_6);
		L_6->set_Edge1_0(L_8);
		IntersectNode_t4106323947 * L_9 = ___int10;
		IntersectNode_t4106323947 * L_10 = ___int21;
		NullCheck(L_10);
		TEdge_t3950806139 * L_11 = L_10->get_Edge2_1();
		NullCheck(L_9);
		L_9->set_Edge2_1(L_11);
		IntersectNode_t4106323947 * L_12 = ___int10;
		IntersectNode_t4106323947 * L_13 = ___int21;
		NullCheck(L_13);
		IntPoint_t3326126179  L_14 = L_13->get_Pt_2();
		NullCheck(L_12);
		L_12->set_Pt_2(L_14);
		IntersectNode_t4106323947 * L_15 = ___int21;
		TEdge_t3950806139 * L_16 = V_0;
		NullCheck(L_15);
		L_15->set_Edge1_0(L_16);
		IntersectNode_t4106323947 * L_17 = ___int21;
		TEdge_t3950806139 * L_18 = V_1;
		NullCheck(L_17);
		L_17->set_Edge2_1(L_18);
		IntersectNode_t4106323947 * L_19 = ___int21;
		IntPoint_t3326126179  L_20 = V_2;
		NullCheck(L_19);
		L_19->set_Pt_2(L_20);
		return;
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::IntersectPoint(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.IntPoint&)
extern Il2CppClass* IntPoint_t3326126179_il2cpp_TypeInfo_var;
extern const uint32_t Clipper_IntersectPoint_m1806336746_MetadataUsageId;
extern "C"  bool Clipper_IntersectPoint_m1806336746 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___edge10, TEdge_t3950806139 * ___edge21, IntPoint_t3326126179 * ___ip2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_IntersectPoint_m1806336746_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	double V_1 = 0.0;
	double V_2 = 0.0;
	{
		IntPoint_t3326126179 * L_0 = ___ip2;
		Initobj (IntPoint_t3326126179_il2cpp_TypeInfo_var, L_0);
		TEdge_t3950806139 * L_1 = ___edge10;
		TEdge_t3950806139 * L_2 = ___edge21;
		bool L_3 = ((ClipperBase_t512275816 *)__this)->get_m_UseFullRange_9();
		bool L_4 = ClipperBase_SlopesEqual_m3887656866(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_002a;
		}
	}
	{
		TEdge_t3950806139 * L_5 = ___edge10;
		NullCheck(L_5);
		double L_6 = L_5->get_Dx_4();
		TEdge_t3950806139 * L_7 = ___edge21;
		NullCheck(L_7);
		double L_8 = L_7->get_Dx_4();
		if ((!(((double)L_6) == ((double)L_8))))
		{
			goto IL_006e;
		}
	}

IL_002a:
	{
		TEdge_t3950806139 * L_9 = ___edge21;
		NullCheck(L_9);
		IntPoint_t3326126179 * L_10 = L_9->get_address_of_Bot_0();
		int64_t L_11 = L_10->get_Y_1();
		TEdge_t3950806139 * L_12 = ___edge10;
		NullCheck(L_12);
		IntPoint_t3326126179 * L_13 = L_12->get_address_of_Bot_0();
		int64_t L_14 = L_13->get_Y_1();
		if ((((int64_t)L_11) <= ((int64_t)L_14)))
		{
			goto IL_005b;
		}
	}
	{
		IntPoint_t3326126179 * L_15 = ___ip2;
		TEdge_t3950806139 * L_16 = ___edge21;
		NullCheck(L_16);
		IntPoint_t3326126179 * L_17 = L_16->get_address_of_Bot_0();
		int64_t L_18 = L_17->get_Y_1();
		L_15->set_Y_1(L_18);
		goto IL_006c;
	}

IL_005b:
	{
		IntPoint_t3326126179 * L_19 = ___ip2;
		TEdge_t3950806139 * L_20 = ___edge10;
		NullCheck(L_20);
		IntPoint_t3326126179 * L_21 = L_20->get_address_of_Bot_0();
		int64_t L_22 = L_21->get_Y_1();
		L_19->set_Y_1(L_22);
	}

IL_006c:
	{
		return (bool)0;
	}

IL_006e:
	{
		TEdge_t3950806139 * L_23 = ___edge10;
		NullCheck(L_23);
		IntPoint_t3326126179 * L_24 = L_23->get_address_of_Delta_3();
		int64_t L_25 = L_24->get_X_0();
		if ((!(((uint64_t)L_25) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_00f3;
		}
	}
	{
		IntPoint_t3326126179 * L_26 = ___ip2;
		TEdge_t3950806139 * L_27 = ___edge10;
		NullCheck(L_27);
		IntPoint_t3326126179 * L_28 = L_27->get_address_of_Bot_0();
		int64_t L_29 = L_28->get_X_0();
		L_26->set_X_0(L_29);
		TEdge_t3950806139 * L_30 = ___edge21;
		bool L_31 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_00b2;
		}
	}
	{
		IntPoint_t3326126179 * L_32 = ___ip2;
		TEdge_t3950806139 * L_33 = ___edge21;
		NullCheck(L_33);
		IntPoint_t3326126179 * L_34 = L_33->get_address_of_Bot_0();
		int64_t L_35 = L_34->get_Y_1();
		L_32->set_Y_1(L_35);
		goto IL_00ee;
	}

IL_00b2:
	{
		TEdge_t3950806139 * L_36 = ___edge21;
		NullCheck(L_36);
		IntPoint_t3326126179 * L_37 = L_36->get_address_of_Bot_0();
		int64_t L_38 = L_37->get_Y_1();
		TEdge_t3950806139 * L_39 = ___edge21;
		NullCheck(L_39);
		IntPoint_t3326126179 * L_40 = L_39->get_address_of_Bot_0();
		int64_t L_41 = L_40->get_X_0();
		TEdge_t3950806139 * L_42 = ___edge21;
		NullCheck(L_42);
		double L_43 = L_42->get_Dx_4();
		V_1 = ((double)((double)(((double)((double)L_38)))-(double)((double)((double)(((double)((double)L_41)))/(double)L_43))));
		IntPoint_t3326126179 * L_44 = ___ip2;
		IntPoint_t3326126179 * L_45 = ___ip2;
		int64_t L_46 = L_45->get_X_0();
		TEdge_t3950806139 * L_47 = ___edge21;
		NullCheck(L_47);
		double L_48 = L_47->get_Dx_4();
		double L_49 = V_1;
		int64_t L_50 = Clipper_Round_m1914183711(NULL /*static, unused*/, ((double)((double)((double)((double)(((double)((double)L_46)))/(double)L_48))+(double)L_49)), /*hidden argument*/NULL);
		L_44->set_Y_1(L_50);
	}

IL_00ee:
	{
		goto IL_0222;
	}

IL_00f3:
	{
		TEdge_t3950806139 * L_51 = ___edge21;
		NullCheck(L_51);
		IntPoint_t3326126179 * L_52 = L_51->get_address_of_Delta_3();
		int64_t L_53 = L_52->get_X_0();
		if ((!(((uint64_t)L_53) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_0178;
		}
	}
	{
		IntPoint_t3326126179 * L_54 = ___ip2;
		TEdge_t3950806139 * L_55 = ___edge21;
		NullCheck(L_55);
		IntPoint_t3326126179 * L_56 = L_55->get_address_of_Bot_0();
		int64_t L_57 = L_56->get_X_0();
		L_54->set_X_0(L_57);
		TEdge_t3950806139 * L_58 = ___edge10;
		bool L_59 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_58, /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_0137;
		}
	}
	{
		IntPoint_t3326126179 * L_60 = ___ip2;
		TEdge_t3950806139 * L_61 = ___edge10;
		NullCheck(L_61);
		IntPoint_t3326126179 * L_62 = L_61->get_address_of_Bot_0();
		int64_t L_63 = L_62->get_Y_1();
		L_60->set_Y_1(L_63);
		goto IL_0173;
	}

IL_0137:
	{
		TEdge_t3950806139 * L_64 = ___edge10;
		NullCheck(L_64);
		IntPoint_t3326126179 * L_65 = L_64->get_address_of_Bot_0();
		int64_t L_66 = L_65->get_Y_1();
		TEdge_t3950806139 * L_67 = ___edge10;
		NullCheck(L_67);
		IntPoint_t3326126179 * L_68 = L_67->get_address_of_Bot_0();
		int64_t L_69 = L_68->get_X_0();
		TEdge_t3950806139 * L_70 = ___edge10;
		NullCheck(L_70);
		double L_71 = L_70->get_Dx_4();
		V_0 = ((double)((double)(((double)((double)L_66)))-(double)((double)((double)(((double)((double)L_69)))/(double)L_71))));
		IntPoint_t3326126179 * L_72 = ___ip2;
		IntPoint_t3326126179 * L_73 = ___ip2;
		int64_t L_74 = L_73->get_X_0();
		TEdge_t3950806139 * L_75 = ___edge10;
		NullCheck(L_75);
		double L_76 = L_75->get_Dx_4();
		double L_77 = V_0;
		int64_t L_78 = Clipper_Round_m1914183711(NULL /*static, unused*/, ((double)((double)((double)((double)(((double)((double)L_74)))/(double)L_76))+(double)L_77)), /*hidden argument*/NULL);
		L_72->set_Y_1(L_78);
	}

IL_0173:
	{
		goto IL_0222;
	}

IL_0178:
	{
		TEdge_t3950806139 * L_79 = ___edge10;
		NullCheck(L_79);
		IntPoint_t3326126179 * L_80 = L_79->get_address_of_Bot_0();
		int64_t L_81 = L_80->get_X_0();
		TEdge_t3950806139 * L_82 = ___edge10;
		NullCheck(L_82);
		IntPoint_t3326126179 * L_83 = L_82->get_address_of_Bot_0();
		int64_t L_84 = L_83->get_Y_1();
		TEdge_t3950806139 * L_85 = ___edge10;
		NullCheck(L_85);
		double L_86 = L_85->get_Dx_4();
		V_0 = ((double)((double)(((double)((double)L_81)))-(double)((double)((double)(((double)((double)L_84)))*(double)L_86))));
		TEdge_t3950806139 * L_87 = ___edge21;
		NullCheck(L_87);
		IntPoint_t3326126179 * L_88 = L_87->get_address_of_Bot_0();
		int64_t L_89 = L_88->get_X_0();
		TEdge_t3950806139 * L_90 = ___edge21;
		NullCheck(L_90);
		IntPoint_t3326126179 * L_91 = L_90->get_address_of_Bot_0();
		int64_t L_92 = L_91->get_Y_1();
		TEdge_t3950806139 * L_93 = ___edge21;
		NullCheck(L_93);
		double L_94 = L_93->get_Dx_4();
		V_1 = ((double)((double)(((double)((double)L_89)))-(double)((double)((double)(((double)((double)L_92)))*(double)L_94))));
		double L_95 = V_1;
		double L_96 = V_0;
		TEdge_t3950806139 * L_97 = ___edge10;
		NullCheck(L_97);
		double L_98 = L_97->get_Dx_4();
		TEdge_t3950806139 * L_99 = ___edge21;
		NullCheck(L_99);
		double L_100 = L_99->get_Dx_4();
		V_2 = ((double)((double)((double)((double)L_95-(double)L_96))/(double)((double)((double)L_98-(double)L_100))));
		IntPoint_t3326126179 * L_101 = ___ip2;
		double L_102 = V_2;
		int64_t L_103 = Clipper_Round_m1914183711(NULL /*static, unused*/, L_102, /*hidden argument*/NULL);
		L_101->set_Y_1(L_103);
		TEdge_t3950806139 * L_104 = ___edge10;
		NullCheck(L_104);
		double L_105 = L_104->get_Dx_4();
		double L_106 = fabs(L_105);
		TEdge_t3950806139 * L_107 = ___edge21;
		NullCheck(L_107);
		double L_108 = L_107->get_Dx_4();
		double L_109 = fabs(L_108);
		if ((!(((double)L_106) < ((double)L_109))))
		{
			goto IL_020d;
		}
	}
	{
		IntPoint_t3326126179 * L_110 = ___ip2;
		TEdge_t3950806139 * L_111 = ___edge10;
		NullCheck(L_111);
		double L_112 = L_111->get_Dx_4();
		double L_113 = V_2;
		double L_114 = V_0;
		int64_t L_115 = Clipper_Round_m1914183711(NULL /*static, unused*/, ((double)((double)((double)((double)L_112*(double)L_113))+(double)L_114)), /*hidden argument*/NULL);
		L_110->set_X_0(L_115);
		goto IL_0222;
	}

IL_020d:
	{
		IntPoint_t3326126179 * L_116 = ___ip2;
		TEdge_t3950806139 * L_117 = ___edge21;
		NullCheck(L_117);
		double L_118 = L_117->get_Dx_4();
		double L_119 = V_2;
		double L_120 = V_1;
		int64_t L_121 = Clipper_Round_m1914183711(NULL /*static, unused*/, ((double)((double)((double)((double)L_118*(double)L_119))+(double)L_120)), /*hidden argument*/NULL);
		L_116->set_X_0(L_121);
	}

IL_0222:
	{
		IntPoint_t3326126179 * L_122 = ___ip2;
		int64_t L_123 = L_122->get_Y_1();
		TEdge_t3950806139 * L_124 = ___edge10;
		NullCheck(L_124);
		IntPoint_t3326126179 * L_125 = L_124->get_address_of_Top_2();
		int64_t L_126 = L_125->get_Y_1();
		if ((((int64_t)L_123) < ((int64_t)L_126)))
		{
			goto IL_024e;
		}
	}
	{
		IntPoint_t3326126179 * L_127 = ___ip2;
		int64_t L_128 = L_127->get_Y_1();
		TEdge_t3950806139 * L_129 = ___edge21;
		NullCheck(L_129);
		IntPoint_t3326126179 * L_130 = L_129->get_address_of_Top_2();
		int64_t L_131 = L_130->get_Y_1();
		if ((((int64_t)L_128) >= ((int64_t)L_131)))
		{
			goto IL_02e1;
		}
	}

IL_024e:
	{
		TEdge_t3950806139 * L_132 = ___edge10;
		NullCheck(L_132);
		IntPoint_t3326126179 * L_133 = L_132->get_address_of_Top_2();
		int64_t L_134 = L_133->get_Y_1();
		TEdge_t3950806139 * L_135 = ___edge21;
		NullCheck(L_135);
		IntPoint_t3326126179 * L_136 = L_135->get_address_of_Top_2();
		int64_t L_137 = L_136->get_Y_1();
		if ((((int64_t)L_134) <= ((int64_t)L_137)))
		{
			goto IL_02a5;
		}
	}
	{
		IntPoint_t3326126179 * L_138 = ___ip2;
		TEdge_t3950806139 * L_139 = ___edge10;
		NullCheck(L_139);
		IntPoint_t3326126179 * L_140 = L_139->get_address_of_Top_2();
		int64_t L_141 = L_140->get_Y_1();
		L_138->set_Y_1(L_141);
		IntPoint_t3326126179 * L_142 = ___ip2;
		TEdge_t3950806139 * L_143 = ___edge21;
		TEdge_t3950806139 * L_144 = ___edge10;
		NullCheck(L_144);
		IntPoint_t3326126179 * L_145 = L_144->get_address_of_Top_2();
		int64_t L_146 = L_145->get_Y_1();
		int64_t L_147 = Clipper_TopX_m1760470797(NULL /*static, unused*/, L_143, L_146, /*hidden argument*/NULL);
		L_142->set_X_0(L_147);
		IntPoint_t3326126179 * L_148 = ___ip2;
		int64_t L_149 = L_148->get_X_0();
		TEdge_t3950806139 * L_150 = ___edge10;
		NullCheck(L_150);
		IntPoint_t3326126179 * L_151 = L_150->get_address_of_Top_2();
		int64_t L_152 = L_151->get_X_0();
		return (bool)((((int64_t)L_149) < ((int64_t)L_152))? 1 : 0);
	}

IL_02a5:
	{
		IntPoint_t3326126179 * L_153 = ___ip2;
		TEdge_t3950806139 * L_154 = ___edge21;
		NullCheck(L_154);
		IntPoint_t3326126179 * L_155 = L_154->get_address_of_Top_2();
		int64_t L_156 = L_155->get_Y_1();
		L_153->set_Y_1(L_156);
		IntPoint_t3326126179 * L_157 = ___ip2;
		TEdge_t3950806139 * L_158 = ___edge10;
		TEdge_t3950806139 * L_159 = ___edge21;
		NullCheck(L_159);
		IntPoint_t3326126179 * L_160 = L_159->get_address_of_Top_2();
		int64_t L_161 = L_160->get_Y_1();
		int64_t L_162 = Clipper_TopX_m1760470797(NULL /*static, unused*/, L_158, L_161, /*hidden argument*/NULL);
		L_157->set_X_0(L_162);
		IntPoint_t3326126179 * L_163 = ___ip2;
		int64_t L_164 = L_163->get_X_0();
		TEdge_t3950806139 * L_165 = ___edge21;
		NullCheck(L_165);
		IntPoint_t3326126179 * L_166 = L_165->get_address_of_Top_2();
		int64_t L_167 = L_166->get_X_0();
		return (bool)((((int64_t)L_164) > ((int64_t)L_167))? 1 : 0);
	}

IL_02e1:
	{
		return (bool)1;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::DisposeIntersectNodes()
extern "C"  void Clipper_DisposeIntersectNodes_m1202039965 (Clipper_t636949239 * __this, const MethodInfo* method)
{
	IntersectNode_t4106323947 * V_0 = NULL;
	{
		goto IL_001f;
	}

IL_0005:
	{
		IntersectNode_t4106323947 * L_0 = __this->get_m_IntersectNodes_20();
		NullCheck(L_0);
		IntersectNode_t4106323947 * L_1 = L_0->get_Next_3();
		V_0 = L_1;
		__this->set_m_IntersectNodes_20((IntersectNode_t4106323947 *)NULL);
		IntersectNode_t4106323947 * L_2 = V_0;
		__this->set_m_IntersectNodes_20(L_2);
	}

IL_001f:
	{
		IntersectNode_t4106323947 * L_3 = __this->get_m_IntersectNodes_20();
		if (L_3)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::ProcessEdgesAtTopOfScanbeam(System.Int64)
extern "C"  void Clipper_ProcessEdgesAtTopOfScanbeam_m897821276 (Clipper_t636949239 * __this, int64_t ___topY0, const MethodInfo* method)
{
	TEdge_t3950806139 * V_0 = NULL;
	bool V_1 = false;
	TEdge_t3950806139 * V_2 = NULL;
	TEdge_t3950806139 * V_3 = NULL;
	TEdge_t3950806139 * V_4 = NULL;
	OutPt_t3947633180 * V_5 = NULL;
	OutPt_t3947633180 * V_6 = NULL;
	OutPt_t3947633180 * V_7 = NULL;
	TEdge_t3950806139 * V_8 = NULL;
	TEdge_t3950806139 * V_9 = NULL;
	OutPt_t3947633180 * V_10 = NULL;
	OutPt_t3947633180 * V_11 = NULL;
	int32_t G_B5_0 = 0;
	{
		TEdge_t3950806139 * L_0 = __this->get_m_ActiveEdges_18();
		V_0 = L_0;
		goto IL_016f;
	}

IL_000c:
	{
		TEdge_t3950806139 * L_1 = V_0;
		int64_t L_2 = ___topY0;
		bool L_3 = Clipper_IsMaxima_m2272585228(__this, L_1, (((double)((double)L_2))), /*hidden argument*/NULL);
		V_1 = L_3;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0037;
		}
	}
	{
		TEdge_t3950806139 * L_5 = V_0;
		TEdge_t3950806139 * L_6 = Clipper_GetMaximaPair_m3274930736(__this, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		TEdge_t3950806139 * L_7 = V_2;
		if (!L_7)
		{
			goto IL_0035;
		}
	}
	{
		TEdge_t3950806139 * L_8 = V_2;
		bool L_9 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		G_B5_0 = ((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
		goto IL_0036;
	}

IL_0035:
	{
		G_B5_0 = 1;
	}

IL_0036:
	{
		V_1 = (bool)G_B5_0;
	}

IL_0037:
	{
		bool L_10 = V_1;
		if (!L_10)
		{
			goto IL_0069;
		}
	}
	{
		TEdge_t3950806139 * L_11 = V_0;
		NullCheck(L_11);
		TEdge_t3950806139 * L_12 = L_11->get_PrevInAEL_15();
		V_3 = L_12;
		TEdge_t3950806139 * L_13 = V_0;
		Clipper_DoMaxima_m606392597(__this, L_13, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_14 = V_3;
		if (L_14)
		{
			goto IL_005d;
		}
	}
	{
		TEdge_t3950806139 * L_15 = __this->get_m_ActiveEdges_18();
		V_0 = L_15;
		goto IL_0064;
	}

IL_005d:
	{
		TEdge_t3950806139 * L_16 = V_3;
		NullCheck(L_16);
		TEdge_t3950806139 * L_17 = L_16->get_NextInAEL_14();
		V_0 = L_17;
	}

IL_0064:
	{
		goto IL_016f;
	}

IL_0069:
	{
		TEdge_t3950806139 * L_18 = V_0;
		int64_t L_19 = ___topY0;
		bool L_20 = Clipper_IsIntermediate_m743671532(__this, L_18, (((double)((double)L_19))), /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00b5;
		}
	}
	{
		TEdge_t3950806139 * L_21 = V_0;
		NullCheck(L_21);
		TEdge_t3950806139 * L_22 = L_21->get_NextInLML_13();
		bool L_23 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00b5;
		}
	}
	{
		Clipper_UpdateEdgeIntoAEL_m1906260707(__this, (&V_0), /*hidden argument*/NULL);
		TEdge_t3950806139 * L_24 = V_0;
		NullCheck(L_24);
		int32_t L_25 = L_24->get_OutIdx_10();
		if ((((int32_t)L_25) < ((int32_t)0)))
		{
			goto IL_00a9;
		}
	}
	{
		TEdge_t3950806139 * L_26 = V_0;
		TEdge_t3950806139 * L_27 = V_0;
		NullCheck(L_27);
		IntPoint_t3326126179  L_28 = L_27->get_Bot_0();
		Clipper_AddOutPt_m3632434112(__this, L_26, L_28, /*hidden argument*/NULL);
	}

IL_00a9:
	{
		TEdge_t3950806139 * L_29 = V_0;
		Clipper_AddEdgeToSEL_m213197330(__this, L_29, /*hidden argument*/NULL);
		goto IL_00d3;
	}

IL_00b5:
	{
		TEdge_t3950806139 * L_30 = V_0;
		NullCheck(L_30);
		IntPoint_t3326126179 * L_31 = L_30->get_address_of_Curr_1();
		TEdge_t3950806139 * L_32 = V_0;
		int64_t L_33 = ___topY0;
		int64_t L_34 = Clipper_TopX_m1760470797(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		L_31->set_X_0(L_34);
		TEdge_t3950806139 * L_35 = V_0;
		NullCheck(L_35);
		IntPoint_t3326126179 * L_36 = L_35->get_address_of_Curr_1();
		int64_t L_37 = ___topY0;
		L_36->set_Y_1(L_37);
	}

IL_00d3:
	{
		bool L_38 = Clipper_get_StrictlySimple_m1330170723(__this, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0168;
		}
	}
	{
		TEdge_t3950806139 * L_39 = V_0;
		NullCheck(L_39);
		TEdge_t3950806139 * L_40 = L_39->get_PrevInAEL_15();
		V_4 = L_40;
		TEdge_t3950806139 * L_41 = V_0;
		NullCheck(L_41);
		int32_t L_42 = L_41->get_OutIdx_10();
		if ((((int32_t)L_42) < ((int32_t)0)))
		{
			goto IL_0168;
		}
	}
	{
		TEdge_t3950806139 * L_43 = V_0;
		NullCheck(L_43);
		int32_t L_44 = L_43->get_WindDelta_7();
		if (!L_44)
		{
			goto IL_0168;
		}
	}
	{
		TEdge_t3950806139 * L_45 = V_4;
		if (!L_45)
		{
			goto IL_0168;
		}
	}
	{
		TEdge_t3950806139 * L_46 = V_4;
		NullCheck(L_46);
		int32_t L_47 = L_46->get_OutIdx_10();
		if ((((int32_t)L_47) < ((int32_t)0)))
		{
			goto IL_0168;
		}
	}
	{
		TEdge_t3950806139 * L_48 = V_4;
		NullCheck(L_48);
		IntPoint_t3326126179 * L_49 = L_48->get_address_of_Curr_1();
		int64_t L_50 = L_49->get_X_0();
		TEdge_t3950806139 * L_51 = V_0;
		NullCheck(L_51);
		IntPoint_t3326126179 * L_52 = L_51->get_address_of_Curr_1();
		int64_t L_53 = L_52->get_X_0();
		if ((!(((uint64_t)L_50) == ((uint64_t)L_53))))
		{
			goto IL_0168;
		}
	}
	{
		TEdge_t3950806139 * L_54 = V_4;
		NullCheck(L_54);
		int32_t L_55 = L_54->get_WindDelta_7();
		if (!L_55)
		{
			goto IL_0168;
		}
	}
	{
		TEdge_t3950806139 * L_56 = V_4;
		TEdge_t3950806139 * L_57 = V_0;
		NullCheck(L_57);
		IntPoint_t3326126179  L_58 = L_57->get_Curr_1();
		OutPt_t3947633180 * L_59 = Clipper_AddOutPt_m3632434112(__this, L_56, L_58, /*hidden argument*/NULL);
		V_5 = L_59;
		TEdge_t3950806139 * L_60 = V_0;
		TEdge_t3950806139 * L_61 = V_0;
		NullCheck(L_61);
		IntPoint_t3326126179  L_62 = L_61->get_Curr_1();
		OutPt_t3947633180 * L_63 = Clipper_AddOutPt_m3632434112(__this, L_60, L_62, /*hidden argument*/NULL);
		V_6 = L_63;
		OutPt_t3947633180 * L_64 = V_5;
		OutPt_t3947633180 * L_65 = V_6;
		TEdge_t3950806139 * L_66 = V_0;
		NullCheck(L_66);
		IntPoint_t3326126179  L_67 = L_66->get_Curr_1();
		Clipper_AddJoin_m2260685358(__this, L_64, L_65, L_67, /*hidden argument*/NULL);
	}

IL_0168:
	{
		TEdge_t3950806139 * L_68 = V_0;
		NullCheck(L_68);
		TEdge_t3950806139 * L_69 = L_68->get_NextInAEL_14();
		V_0 = L_69;
	}

IL_016f:
	{
		TEdge_t3950806139 * L_70 = V_0;
		if (L_70)
		{
			goto IL_000c;
		}
	}
	{
		Clipper_ProcessHorizontals_m563259309(__this, (bool)1, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_71 = __this->get_m_ActiveEdges_18();
		V_0 = L_71;
		goto IL_034c;
	}

IL_0188:
	{
		TEdge_t3950806139 * L_72 = V_0;
		int64_t L_73 = ___topY0;
		bool L_74 = Clipper_IsIntermediate_m743671532(__this, L_72, (((double)((double)L_73))), /*hidden argument*/NULL);
		if (!L_74)
		{
			goto IL_0345;
		}
	}
	{
		V_7 = (OutPt_t3947633180 *)NULL;
		TEdge_t3950806139 * L_75 = V_0;
		NullCheck(L_75);
		int32_t L_76 = L_75->get_OutIdx_10();
		if ((((int32_t)L_76) < ((int32_t)0)))
		{
			goto IL_01b4;
		}
	}
	{
		TEdge_t3950806139 * L_77 = V_0;
		TEdge_t3950806139 * L_78 = V_0;
		NullCheck(L_78);
		IntPoint_t3326126179  L_79 = L_78->get_Top_2();
		OutPt_t3947633180 * L_80 = Clipper_AddOutPt_m3632434112(__this, L_77, L_79, /*hidden argument*/NULL);
		V_7 = L_80;
	}

IL_01b4:
	{
		Clipper_UpdateEdgeIntoAEL_m1906260707(__this, (&V_0), /*hidden argument*/NULL);
		TEdge_t3950806139 * L_81 = V_0;
		NullCheck(L_81);
		TEdge_t3950806139 * L_82 = L_81->get_PrevInAEL_15();
		V_8 = L_82;
		TEdge_t3950806139 * L_83 = V_0;
		NullCheck(L_83);
		TEdge_t3950806139 * L_84 = L_83->get_NextInAEL_14();
		V_9 = L_84;
		TEdge_t3950806139 * L_85 = V_8;
		if (!L_85)
		{
			goto IL_028b;
		}
	}
	{
		TEdge_t3950806139 * L_86 = V_8;
		NullCheck(L_86);
		IntPoint_t3326126179 * L_87 = L_86->get_address_of_Curr_1();
		int64_t L_88 = L_87->get_X_0();
		TEdge_t3950806139 * L_89 = V_0;
		NullCheck(L_89);
		IntPoint_t3326126179 * L_90 = L_89->get_address_of_Bot_0();
		int64_t L_91 = L_90->get_X_0();
		if ((!(((uint64_t)L_88) == ((uint64_t)L_91))))
		{
			goto IL_028b;
		}
	}
	{
		TEdge_t3950806139 * L_92 = V_8;
		NullCheck(L_92);
		IntPoint_t3326126179 * L_93 = L_92->get_address_of_Curr_1();
		int64_t L_94 = L_93->get_Y_1();
		TEdge_t3950806139 * L_95 = V_0;
		NullCheck(L_95);
		IntPoint_t3326126179 * L_96 = L_95->get_address_of_Bot_0();
		int64_t L_97 = L_96->get_Y_1();
		if ((!(((uint64_t)L_94) == ((uint64_t)L_97))))
		{
			goto IL_028b;
		}
	}
	{
		OutPt_t3947633180 * L_98 = V_7;
		if (!L_98)
		{
			goto IL_028b;
		}
	}
	{
		TEdge_t3950806139 * L_99 = V_8;
		NullCheck(L_99);
		int32_t L_100 = L_99->get_OutIdx_10();
		if ((((int32_t)L_100) < ((int32_t)0)))
		{
			goto IL_028b;
		}
	}
	{
		TEdge_t3950806139 * L_101 = V_8;
		NullCheck(L_101);
		IntPoint_t3326126179 * L_102 = L_101->get_address_of_Curr_1();
		int64_t L_103 = L_102->get_Y_1();
		TEdge_t3950806139 * L_104 = V_8;
		NullCheck(L_104);
		IntPoint_t3326126179 * L_105 = L_104->get_address_of_Top_2();
		int64_t L_106 = L_105->get_Y_1();
		if ((((int64_t)L_103) <= ((int64_t)L_106)))
		{
			goto IL_028b;
		}
	}
	{
		TEdge_t3950806139 * L_107 = V_0;
		TEdge_t3950806139 * L_108 = V_8;
		bool L_109 = ((ClipperBase_t512275816 *)__this)->get_m_UseFullRange_9();
		bool L_110 = ClipperBase_SlopesEqual_m3887656866(NULL /*static, unused*/, L_107, L_108, L_109, /*hidden argument*/NULL);
		if (!L_110)
		{
			goto IL_028b;
		}
	}
	{
		TEdge_t3950806139 * L_111 = V_0;
		NullCheck(L_111);
		int32_t L_112 = L_111->get_WindDelta_7();
		if (!L_112)
		{
			goto IL_028b;
		}
	}
	{
		TEdge_t3950806139 * L_113 = V_8;
		NullCheck(L_113);
		int32_t L_114 = L_113->get_WindDelta_7();
		if (!L_114)
		{
			goto IL_028b;
		}
	}
	{
		TEdge_t3950806139 * L_115 = V_8;
		TEdge_t3950806139 * L_116 = V_0;
		NullCheck(L_116);
		IntPoint_t3326126179  L_117 = L_116->get_Bot_0();
		OutPt_t3947633180 * L_118 = Clipper_AddOutPt_m3632434112(__this, L_115, L_117, /*hidden argument*/NULL);
		V_10 = L_118;
		OutPt_t3947633180 * L_119 = V_7;
		OutPt_t3947633180 * L_120 = V_10;
		TEdge_t3950806139 * L_121 = V_0;
		NullCheck(L_121);
		IntPoint_t3326126179  L_122 = L_121->get_Top_2();
		Clipper_AddJoin_m2260685358(__this, L_119, L_120, L_122, /*hidden argument*/NULL);
		goto IL_0345;
	}

IL_028b:
	{
		TEdge_t3950806139 * L_123 = V_9;
		if (!L_123)
		{
			goto IL_0345;
		}
	}
	{
		TEdge_t3950806139 * L_124 = V_9;
		NullCheck(L_124);
		IntPoint_t3326126179 * L_125 = L_124->get_address_of_Curr_1();
		int64_t L_126 = L_125->get_X_0();
		TEdge_t3950806139 * L_127 = V_0;
		NullCheck(L_127);
		IntPoint_t3326126179 * L_128 = L_127->get_address_of_Bot_0();
		int64_t L_129 = L_128->get_X_0();
		if ((!(((uint64_t)L_126) == ((uint64_t)L_129))))
		{
			goto IL_0345;
		}
	}
	{
		TEdge_t3950806139 * L_130 = V_9;
		NullCheck(L_130);
		IntPoint_t3326126179 * L_131 = L_130->get_address_of_Curr_1();
		int64_t L_132 = L_131->get_Y_1();
		TEdge_t3950806139 * L_133 = V_0;
		NullCheck(L_133);
		IntPoint_t3326126179 * L_134 = L_133->get_address_of_Bot_0();
		int64_t L_135 = L_134->get_Y_1();
		if ((!(((uint64_t)L_132) == ((uint64_t)L_135))))
		{
			goto IL_0345;
		}
	}
	{
		OutPt_t3947633180 * L_136 = V_7;
		if (!L_136)
		{
			goto IL_0345;
		}
	}
	{
		TEdge_t3950806139 * L_137 = V_9;
		NullCheck(L_137);
		int32_t L_138 = L_137->get_OutIdx_10();
		if ((((int32_t)L_138) < ((int32_t)0)))
		{
			goto IL_0345;
		}
	}
	{
		TEdge_t3950806139 * L_139 = V_9;
		NullCheck(L_139);
		IntPoint_t3326126179 * L_140 = L_139->get_address_of_Curr_1();
		int64_t L_141 = L_140->get_Y_1();
		TEdge_t3950806139 * L_142 = V_9;
		NullCheck(L_142);
		IntPoint_t3326126179 * L_143 = L_142->get_address_of_Top_2();
		int64_t L_144 = L_143->get_Y_1();
		if ((((int64_t)L_141) <= ((int64_t)L_144)))
		{
			goto IL_0345;
		}
	}
	{
		TEdge_t3950806139 * L_145 = V_0;
		TEdge_t3950806139 * L_146 = V_9;
		bool L_147 = ((ClipperBase_t512275816 *)__this)->get_m_UseFullRange_9();
		bool L_148 = ClipperBase_SlopesEqual_m3887656866(NULL /*static, unused*/, L_145, L_146, L_147, /*hidden argument*/NULL);
		if (!L_148)
		{
			goto IL_0345;
		}
	}
	{
		TEdge_t3950806139 * L_149 = V_0;
		NullCheck(L_149);
		int32_t L_150 = L_149->get_WindDelta_7();
		if (!L_150)
		{
			goto IL_0345;
		}
	}
	{
		TEdge_t3950806139 * L_151 = V_9;
		NullCheck(L_151);
		int32_t L_152 = L_151->get_WindDelta_7();
		if (!L_152)
		{
			goto IL_0345;
		}
	}
	{
		TEdge_t3950806139 * L_153 = V_9;
		TEdge_t3950806139 * L_154 = V_0;
		NullCheck(L_154);
		IntPoint_t3326126179  L_155 = L_154->get_Bot_0();
		OutPt_t3947633180 * L_156 = Clipper_AddOutPt_m3632434112(__this, L_153, L_155, /*hidden argument*/NULL);
		V_11 = L_156;
		OutPt_t3947633180 * L_157 = V_7;
		OutPt_t3947633180 * L_158 = V_11;
		TEdge_t3950806139 * L_159 = V_0;
		NullCheck(L_159);
		IntPoint_t3326126179  L_160 = L_159->get_Top_2();
		Clipper_AddJoin_m2260685358(__this, L_157, L_158, L_160, /*hidden argument*/NULL);
	}

IL_0345:
	{
		TEdge_t3950806139 * L_161 = V_0;
		NullCheck(L_161);
		TEdge_t3950806139 * L_162 = L_161->get_NextInAEL_14();
		V_0 = L_162;
	}

IL_034c:
	{
		TEdge_t3950806139 * L_163 = V_0;
		if (L_163)
		{
			goto IL_0188;
		}
	}
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::DoMaxima(Pathfinding.ClipperLib.TEdge)
extern Il2CppClass* ClipperException_t2818206852_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4190821196;
extern const uint32_t Clipper_DoMaxima_m606392597_MetadataUsageId;
extern "C"  void Clipper_DoMaxima_m606392597 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_DoMaxima_m606392597_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TEdge_t3950806139 * V_0 = NULL;
	TEdge_t3950806139 * V_1 = NULL;
	{
		TEdge_t3950806139 * L_0 = ___e0;
		TEdge_t3950806139 * L_1 = Clipper_GetMaximaPair_m3274930736(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		TEdge_t3950806139 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0030;
		}
	}
	{
		TEdge_t3950806139 * L_3 = ___e0;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_OutIdx_10();
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0028;
		}
	}
	{
		TEdge_t3950806139 * L_5 = ___e0;
		TEdge_t3950806139 * L_6 = ___e0;
		NullCheck(L_6);
		IntPoint_t3326126179  L_7 = L_6->get_Top_2();
		Clipper_AddOutPt_m3632434112(__this, L_5, L_7, /*hidden argument*/NULL);
	}

IL_0028:
	{
		TEdge_t3950806139 * L_8 = ___e0;
		Clipper_DeleteFromAEL_m2256743194(__this, L_8, /*hidden argument*/NULL);
		return;
	}

IL_0030:
	{
		TEdge_t3950806139 * L_9 = ___e0;
		NullCheck(L_9);
		TEdge_t3950806139 * L_10 = L_9->get_NextInAEL_14();
		V_1 = L_10;
		goto IL_005a;
	}

IL_003c:
	{
		TEdge_t3950806139 * L_11 = ___e0;
		TEdge_t3950806139 * L_12 = V_1;
		TEdge_t3950806139 * L_13 = ___e0;
		NullCheck(L_13);
		IntPoint_t3326126179  L_14 = L_13->get_Top_2();
		Clipper_IntersectEdges_m1787610885(__this, L_11, L_12, L_14, (bool)1, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_15 = ___e0;
		TEdge_t3950806139 * L_16 = V_1;
		Clipper_SwapPositionsInAEL_m423077188(__this, L_15, L_16, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_17 = ___e0;
		NullCheck(L_17);
		TEdge_t3950806139 * L_18 = L_17->get_NextInAEL_14();
		V_1 = L_18;
	}

IL_005a:
	{
		TEdge_t3950806139 * L_19 = V_1;
		if (!L_19)
		{
			goto IL_0067;
		}
	}
	{
		TEdge_t3950806139 * L_20 = V_1;
		TEdge_t3950806139 * L_21 = V_0;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_20) == ((Il2CppObject*)(TEdge_t3950806139 *)L_21))))
		{
			goto IL_003c;
		}
	}

IL_0067:
	{
		TEdge_t3950806139 * L_22 = ___e0;
		NullCheck(L_22);
		int32_t L_23 = L_22->get_OutIdx_10();
		if ((!(((uint32_t)L_23) == ((uint32_t)(-1)))))
		{
			goto IL_0092;
		}
	}
	{
		TEdge_t3950806139 * L_24 = V_0;
		NullCheck(L_24);
		int32_t L_25 = L_24->get_OutIdx_10();
		if ((!(((uint32_t)L_25) == ((uint32_t)(-1)))))
		{
			goto IL_0092;
		}
	}
	{
		TEdge_t3950806139 * L_26 = ___e0;
		Clipper_DeleteFromAEL_m2256743194(__this, L_26, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_27 = V_0;
		Clipper_DeleteFromAEL_m2256743194(__this, L_27, /*hidden argument*/NULL);
		goto IL_00c9;
	}

IL_0092:
	{
		TEdge_t3950806139 * L_28 = ___e0;
		NullCheck(L_28);
		int32_t L_29 = L_28->get_OutIdx_10();
		if ((((int32_t)L_29) < ((int32_t)0)))
		{
			goto IL_00be;
		}
	}
	{
		TEdge_t3950806139 * L_30 = V_0;
		NullCheck(L_30);
		int32_t L_31 = L_30->get_OutIdx_10();
		if ((((int32_t)L_31) < ((int32_t)0)))
		{
			goto IL_00be;
		}
	}
	{
		TEdge_t3950806139 * L_32 = ___e0;
		TEdge_t3950806139 * L_33 = V_0;
		TEdge_t3950806139 * L_34 = ___e0;
		NullCheck(L_34);
		IntPoint_t3326126179  L_35 = L_34->get_Top_2();
		Clipper_IntersectEdges_m1787610885(__this, L_32, L_33, L_35, (bool)0, /*hidden argument*/NULL);
		goto IL_00c9;
	}

IL_00be:
	{
		ClipperException_t2818206852 * L_36 = (ClipperException_t2818206852 *)il2cpp_codegen_object_new(ClipperException_t2818206852_il2cpp_TypeInfo_var);
		ClipperException__ctor_m106504313(L_36, _stringLiteral4190821196, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_36);
	}

IL_00c9:
	{
		return;
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::Orientation(System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>)
extern "C"  bool Clipper_Orientation_m1168645137 (Il2CppObject * __this /* static, unused */, List_1_t399344435 * ___poly0, const MethodInfo* method)
{
	{
		List_1_t399344435 * L_0 = ___poly0;
		double L_1 = Clipper_Area_m791992023(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return (bool)((((int32_t)((!(((double)L_1) >= ((double)(0.0))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 Pathfinding.ClipperLib.Clipper::PointCount(Pathfinding.ClipperLib.OutPt)
extern "C"  int32_t Clipper_PointCount_m3418340581 (Clipper_t636949239 * __this, OutPt_t3947633180 * ___pts0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	OutPt_t3947633180 * V_1 = NULL;
	{
		OutPt_t3947633180 * L_0 = ___pts0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		V_0 = 0;
		OutPt_t3947633180 * L_1 = ___pts0;
		V_1 = L_1;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		V_0 = ((int32_t)((int32_t)L_2+(int32_t)1));
		OutPt_t3947633180 * L_3 = V_1;
		NullCheck(L_3);
		OutPt_t3947633180 * L_4 = L_3->get_Next_2();
		V_1 = L_4;
		OutPt_t3947633180 * L_5 = V_1;
		OutPt_t3947633180 * L_6 = ___pts0;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_5) == ((Il2CppObject*)(OutPt_t3947633180 *)L_6))))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_7 = V_0;
		return L_7;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::BuildResult(System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>)
extern Il2CppClass* List_1_t399344435_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m476408529_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m1367576335_MethodInfo_var;
extern const MethodInfo* List_1_set_Capacity_m2760403174_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2053160358_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2937085201_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4114127024_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2764636054_MethodInfo_var;
extern const uint32_t Clipper_BuildResult_m1163037098_MetadataUsageId;
extern "C"  void Clipper_BuildResult_m1163037098 (Clipper_t636949239 * __this, List_1_t1767529987 * ___polyg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_BuildResult_m1163037098_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	OutRec_t3245805284 * V_1 = NULL;
	OutPt_t3947633180 * V_2 = NULL;
	int32_t V_3 = 0;
	List_1_t399344435 * V_4 = NULL;
	int32_t V_5 = 0;
	{
		List_1_t1767529987 * L_0 = ___polyg0;
		NullCheck(L_0);
		List_1_Clear_m476408529(L_0, /*hidden argument*/List_1_Clear_m476408529_MethodInfo_var);
		List_1_t1767529987 * L_1 = ___polyg0;
		List_1_t319023540 * L_2 = __this->get_m_PolyOuts_15();
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m1367576335(L_2, /*hidden argument*/List_1_get_Count_m1367576335_MethodInfo_var);
		NullCheck(L_1);
		List_1_set_Capacity_m2760403174(L_1, L_3, /*hidden argument*/List_1_set_Capacity_m2760403174_MethodInfo_var);
		V_0 = 0;
		goto IL_0094;
	}

IL_001e:
	{
		List_1_t319023540 * L_4 = __this->get_m_PolyOuts_15();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		OutRec_t3245805284 * L_6 = List_1_get_Item_m2053160358(L_4, L_5, /*hidden argument*/List_1_get_Item_m2053160358_MethodInfo_var);
		V_1 = L_6;
		OutRec_t3245805284 * L_7 = V_1;
		NullCheck(L_7);
		OutPt_t3947633180 * L_8 = L_7->get_Pts_4();
		if (L_8)
		{
			goto IL_003b;
		}
	}
	{
		goto IL_0090;
	}

IL_003b:
	{
		OutRec_t3245805284 * L_9 = V_1;
		NullCheck(L_9);
		OutPt_t3947633180 * L_10 = L_9->get_Pts_4();
		V_2 = L_10;
		OutPt_t3947633180 * L_11 = V_2;
		int32_t L_12 = Clipper_PointCount_m3418340581(__this, L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		int32_t L_13 = V_3;
		if ((((int32_t)L_13) >= ((int32_t)2)))
		{
			goto IL_0056;
		}
	}
	{
		goto IL_0090;
	}

IL_0056:
	{
		int32_t L_14 = V_3;
		List_1_t399344435 * L_15 = (List_1_t399344435 *)il2cpp_codegen_object_new(List_1_t399344435_il2cpp_TypeInfo_var);
		List_1__ctor_m2937085201(L_15, L_14, /*hidden argument*/List_1__ctor_m2937085201_MethodInfo_var);
		V_4 = L_15;
		V_5 = 0;
		goto IL_0080;
	}

IL_0066:
	{
		List_1_t399344435 * L_16 = V_4;
		OutPt_t3947633180 * L_17 = V_2;
		NullCheck(L_17);
		IntPoint_t3326126179  L_18 = L_17->get_Pt_1();
		NullCheck(L_16);
		List_1_Add_m4114127024(L_16, L_18, /*hidden argument*/List_1_Add_m4114127024_MethodInfo_var);
		OutPt_t3947633180 * L_19 = V_2;
		NullCheck(L_19);
		OutPt_t3947633180 * L_20 = L_19->get_Prev_3();
		V_2 = L_20;
		int32_t L_21 = V_5;
		V_5 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0080:
	{
		int32_t L_22 = V_5;
		int32_t L_23 = V_3;
		if ((((int32_t)L_22) < ((int32_t)L_23)))
		{
			goto IL_0066;
		}
	}
	{
		List_1_t1767529987 * L_24 = ___polyg0;
		List_1_t399344435 * L_25 = V_4;
		NullCheck(L_24);
		List_1_Add_m2764636054(L_24, L_25, /*hidden argument*/List_1_Add_m2764636054_MethodInfo_var);
	}

IL_0090:
	{
		int32_t L_26 = V_0;
		V_0 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_0094:
	{
		int32_t L_27 = V_0;
		List_1_t319023540 * L_28 = __this->get_m_PolyOuts_15();
		NullCheck(L_28);
		int32_t L_29 = List_1_get_Count_m1367576335(L_28, /*hidden argument*/List_1_get_Count_m1367576335_MethodInfo_var);
		if ((((int32_t)L_27) < ((int32_t)L_29)))
		{
			goto IL_001e;
		}
	}
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::BuildResult2(Pathfinding.ClipperLib.PolyTree)
extern Il2CppClass* PolyNode_t3336253808_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m1367576335_MethodInfo_var;
extern const MethodInfo* List_1_set_Capacity_m898286495_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2053160358_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1889072509_MethodInfo_var;
extern const MethodInfo* List_1_set_Capacity_m2269640076_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4114127024_MethodInfo_var;
extern const uint32_t Clipper_BuildResult2_m2430771551_MetadataUsageId;
extern "C"  void Clipper_BuildResult2_m2430771551 (Clipper_t636949239 * __this, PolyTree_t3336435468 * ___polytree0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_BuildResult2_m2430771551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	OutRec_t3245805284 * V_1 = NULL;
	int32_t V_2 = 0;
	PolyNode_t3336253808 * V_3 = NULL;
	OutPt_t3947633180 * V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	OutRec_t3245805284 * V_7 = NULL;
	{
		PolyTree_t3336435468 * L_0 = ___polytree0;
		NullCheck(L_0);
		PolyTree_Clear_m1053853644(L_0, /*hidden argument*/NULL);
		PolyTree_t3336435468 * L_1 = ___polytree0;
		NullCheck(L_1);
		List_1_t409472064 * L_2 = L_1->get_m_AllPolys_5();
		List_1_t319023540 * L_3 = __this->get_m_PolyOuts_15();
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Count_m1367576335(L_3, /*hidden argument*/List_1_get_Count_m1367576335_MethodInfo_var);
		NullCheck(L_2);
		List_1_set_Capacity_m898286495(L_2, L_4, /*hidden argument*/List_1_set_Capacity_m898286495_MethodInfo_var);
		V_0 = 0;
		goto IL_00d4;
	}

IL_0023:
	{
		List_1_t319023540 * L_5 = __this->get_m_PolyOuts_15();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		OutRec_t3245805284 * L_7 = List_1_get_Item_m2053160358(L_5, L_6, /*hidden argument*/List_1_get_Item_m2053160358_MethodInfo_var);
		V_1 = L_7;
		OutRec_t3245805284 * L_8 = V_1;
		NullCheck(L_8);
		OutPt_t3947633180 * L_9 = L_8->get_Pts_4();
		int32_t L_10 = Clipper_PointCount_m3418340581(__this, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		OutRec_t3245805284 * L_11 = V_1;
		NullCheck(L_11);
		bool L_12 = L_11->get_IsOpen_2();
		if (!L_12)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_13 = V_2;
		if ((((int32_t)L_13) < ((int32_t)2)))
		{
			goto IL_0061;
		}
	}

IL_004f:
	{
		OutRec_t3245805284 * L_14 = V_1;
		NullCheck(L_14);
		bool L_15 = L_14->get_IsOpen_2();
		if (L_15)
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_16 = V_2;
		if ((((int32_t)L_16) >= ((int32_t)3)))
		{
			goto IL_0066;
		}
	}

IL_0061:
	{
		goto IL_00d0;
	}

IL_0066:
	{
		OutRec_t3245805284 * L_17 = V_1;
		Clipper_FixHoleLinkage_m891396734(__this, L_17, /*hidden argument*/NULL);
		PolyNode_t3336253808 * L_18 = (PolyNode_t3336253808 *)il2cpp_codegen_object_new(PolyNode_t3336253808_il2cpp_TypeInfo_var);
		PolyNode__ctor_m2522874301(L_18, /*hidden argument*/NULL);
		V_3 = L_18;
		PolyTree_t3336435468 * L_19 = ___polytree0;
		NullCheck(L_19);
		List_1_t409472064 * L_20 = L_19->get_m_AllPolys_5();
		PolyNode_t3336253808 * L_21 = V_3;
		NullCheck(L_20);
		List_1_Add_m1889072509(L_20, L_21, /*hidden argument*/List_1_Add_m1889072509_MethodInfo_var);
		OutRec_t3245805284 * L_22 = V_1;
		PolyNode_t3336253808 * L_23 = V_3;
		NullCheck(L_22);
		L_22->set_PolyNode_6(L_23);
		PolyNode_t3336253808 * L_24 = V_3;
		NullCheck(L_24);
		List_1_t399344435 * L_25 = L_24->get_m_polygon_1();
		int32_t L_26 = V_2;
		NullCheck(L_25);
		List_1_set_Capacity_m2269640076(L_25, L_26, /*hidden argument*/List_1_set_Capacity_m2269640076_MethodInfo_var);
		OutRec_t3245805284 * L_27 = V_1;
		NullCheck(L_27);
		OutPt_t3947633180 * L_28 = L_27->get_Pts_4();
		NullCheck(L_28);
		OutPt_t3947633180 * L_29 = L_28->get_Prev_3();
		V_4 = L_29;
		V_5 = 0;
		goto IL_00c8;
	}

IL_00a7:
	{
		PolyNode_t3336253808 * L_30 = V_3;
		NullCheck(L_30);
		List_1_t399344435 * L_31 = L_30->get_m_polygon_1();
		OutPt_t3947633180 * L_32 = V_4;
		NullCheck(L_32);
		IntPoint_t3326126179  L_33 = L_32->get_Pt_1();
		NullCheck(L_31);
		List_1_Add_m4114127024(L_31, L_33, /*hidden argument*/List_1_Add_m4114127024_MethodInfo_var);
		OutPt_t3947633180 * L_34 = V_4;
		NullCheck(L_34);
		OutPt_t3947633180 * L_35 = L_34->get_Prev_3();
		V_4 = L_35;
		int32_t L_36 = V_5;
		V_5 = ((int32_t)((int32_t)L_36+(int32_t)1));
	}

IL_00c8:
	{
		int32_t L_37 = V_5;
		int32_t L_38 = V_2;
		if ((((int32_t)L_37) < ((int32_t)L_38)))
		{
			goto IL_00a7;
		}
	}

IL_00d0:
	{
		int32_t L_39 = V_0;
		V_0 = ((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_00d4:
	{
		int32_t L_40 = V_0;
		List_1_t319023540 * L_41 = __this->get_m_PolyOuts_15();
		NullCheck(L_41);
		int32_t L_42 = List_1_get_Count_m1367576335(L_41, /*hidden argument*/List_1_get_Count_m1367576335_MethodInfo_var);
		if ((((int32_t)L_40) < ((int32_t)L_42)))
		{
			goto IL_0023;
		}
	}
	{
		PolyTree_t3336435468 * L_43 = ___polytree0;
		NullCheck(L_43);
		List_1_t409472064 * L_44 = ((PolyNode_t3336253808 *)L_43)->get_m_Childs_3();
		List_1_t319023540 * L_45 = __this->get_m_PolyOuts_15();
		NullCheck(L_45);
		int32_t L_46 = List_1_get_Count_m1367576335(L_45, /*hidden argument*/List_1_get_Count_m1367576335_MethodInfo_var);
		NullCheck(L_44);
		List_1_set_Capacity_m898286495(L_44, L_46, /*hidden argument*/List_1_set_Capacity_m898286495_MethodInfo_var);
		V_6 = 0;
		goto IL_018a;
	}

IL_0103:
	{
		List_1_t319023540 * L_47 = __this->get_m_PolyOuts_15();
		int32_t L_48 = V_6;
		NullCheck(L_47);
		OutRec_t3245805284 * L_49 = List_1_get_Item_m2053160358(L_47, L_48, /*hidden argument*/List_1_get_Item_m2053160358_MethodInfo_var);
		V_7 = L_49;
		OutRec_t3245805284 * L_50 = V_7;
		NullCheck(L_50);
		PolyNode_t3336253808 * L_51 = L_50->get_PolyNode_6();
		if (L_51)
		{
			goto IL_0123;
		}
	}
	{
		goto IL_0184;
	}

IL_0123:
	{
		OutRec_t3245805284 * L_52 = V_7;
		NullCheck(L_52);
		bool L_53 = L_52->get_IsOpen_2();
		if (!L_53)
		{
			goto IL_014e;
		}
	}
	{
		OutRec_t3245805284 * L_54 = V_7;
		NullCheck(L_54);
		PolyNode_t3336253808 * L_55 = L_54->get_PolyNode_6();
		NullCheck(L_55);
		PolyNode_set_IsOpen_m4104302511(L_55, (bool)1, /*hidden argument*/NULL);
		PolyTree_t3336435468 * L_56 = ___polytree0;
		OutRec_t3245805284 * L_57 = V_7;
		NullCheck(L_57);
		PolyNode_t3336253808 * L_58 = L_57->get_PolyNode_6();
		NullCheck(L_56);
		PolyNode_AddChild_m4066781430(L_56, L_58, /*hidden argument*/NULL);
		goto IL_0184;
	}

IL_014e:
	{
		OutRec_t3245805284 * L_59 = V_7;
		NullCheck(L_59);
		OutRec_t3245805284 * L_60 = L_59->get_FirstLeft_3();
		if (!L_60)
		{
			goto IL_0177;
		}
	}
	{
		OutRec_t3245805284 * L_61 = V_7;
		NullCheck(L_61);
		OutRec_t3245805284 * L_62 = L_61->get_FirstLeft_3();
		NullCheck(L_62);
		PolyNode_t3336253808 * L_63 = L_62->get_PolyNode_6();
		OutRec_t3245805284 * L_64 = V_7;
		NullCheck(L_64);
		PolyNode_t3336253808 * L_65 = L_64->get_PolyNode_6();
		NullCheck(L_63);
		PolyNode_AddChild_m4066781430(L_63, L_65, /*hidden argument*/NULL);
		goto IL_0184;
	}

IL_0177:
	{
		PolyTree_t3336435468 * L_66 = ___polytree0;
		OutRec_t3245805284 * L_67 = V_7;
		NullCheck(L_67);
		PolyNode_t3336253808 * L_68 = L_67->get_PolyNode_6();
		NullCheck(L_66);
		PolyNode_AddChild_m4066781430(L_66, L_68, /*hidden argument*/NULL);
	}

IL_0184:
	{
		int32_t L_69 = V_6;
		V_6 = ((int32_t)((int32_t)L_69+(int32_t)1));
	}

IL_018a:
	{
		int32_t L_70 = V_6;
		List_1_t319023540 * L_71 = __this->get_m_PolyOuts_15();
		NullCheck(L_71);
		int32_t L_72 = List_1_get_Count_m1367576335(L_71, /*hidden argument*/List_1_get_Count_m1367576335_MethodInfo_var);
		if ((((int32_t)L_70) < ((int32_t)L_72)))
		{
			goto IL_0103;
		}
	}
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::FixupOutPolygon(Pathfinding.ClipperLib.OutRec)
extern "C"  void Clipper_FixupOutPolygon_m3468612540 (Clipper_t636949239 * __this, OutRec_t3245805284 * ___outRec0, const MethodInfo* method)
{
	OutPt_t3947633180 * V_0 = NULL;
	OutPt_t3947633180 * V_1 = NULL;
	OutPt_t3947633180 * V_2 = NULL;
	{
		V_0 = (OutPt_t3947633180 *)NULL;
		OutRec_t3245805284 * L_0 = ___outRec0;
		NullCheck(L_0);
		L_0->set_BottomPt_5((OutPt_t3947633180 *)NULL);
		OutRec_t3245805284 * L_1 = ___outRec0;
		NullCheck(L_1);
		OutPt_t3947633180 * L_2 = L_1->get_Pts_4();
		V_1 = L_2;
		goto IL_0124;
	}

IL_0015:
	{
		OutPt_t3947633180 * L_3 = V_1;
		NullCheck(L_3);
		OutPt_t3947633180 * L_4 = L_3->get_Prev_3();
		OutPt_t3947633180 * L_5 = V_1;
		if ((((Il2CppObject*)(OutPt_t3947633180 *)L_4) == ((Il2CppObject*)(OutPt_t3947633180 *)L_5)))
		{
			goto IL_0032;
		}
	}
	{
		OutPt_t3947633180 * L_6 = V_1;
		NullCheck(L_6);
		OutPt_t3947633180 * L_7 = L_6->get_Prev_3();
		OutPt_t3947633180 * L_8 = V_1;
		NullCheck(L_8);
		OutPt_t3947633180 * L_9 = L_8->get_Next_2();
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_7) == ((Il2CppObject*)(OutPt_t3947633180 *)L_9))))
		{
			goto IL_0041;
		}
	}

IL_0032:
	{
		OutPt_t3947633180 * L_10 = V_1;
		Clipper_DisposeOutPts_m3153880070(__this, L_10, /*hidden argument*/NULL);
		OutRec_t3245805284 * L_11 = ___outRec0;
		NullCheck(L_11);
		L_11->set_Pts_4((OutPt_t3947633180 *)NULL);
		return;
	}

IL_0041:
	{
		OutPt_t3947633180 * L_12 = V_1;
		NullCheck(L_12);
		IntPoint_t3326126179  L_13 = L_12->get_Pt_1();
		OutPt_t3947633180 * L_14 = V_1;
		NullCheck(L_14);
		OutPt_t3947633180 * L_15 = L_14->get_Next_2();
		NullCheck(L_15);
		IntPoint_t3326126179  L_16 = L_15->get_Pt_1();
		bool L_17 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_13, L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00d5;
		}
	}
	{
		OutPt_t3947633180 * L_18 = V_1;
		NullCheck(L_18);
		IntPoint_t3326126179  L_19 = L_18->get_Pt_1();
		OutPt_t3947633180 * L_20 = V_1;
		NullCheck(L_20);
		OutPt_t3947633180 * L_21 = L_20->get_Prev_3();
		NullCheck(L_21);
		IntPoint_t3326126179  L_22 = L_21->get_Pt_1();
		bool L_23 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_19, L_22, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_00d5;
		}
	}
	{
		OutPt_t3947633180 * L_24 = V_1;
		NullCheck(L_24);
		OutPt_t3947633180 * L_25 = L_24->get_Prev_3();
		NullCheck(L_25);
		IntPoint_t3326126179  L_26 = L_25->get_Pt_1();
		OutPt_t3947633180 * L_27 = V_1;
		NullCheck(L_27);
		IntPoint_t3326126179  L_28 = L_27->get_Pt_1();
		OutPt_t3947633180 * L_29 = V_1;
		NullCheck(L_29);
		OutPt_t3947633180 * L_30 = L_29->get_Next_2();
		NullCheck(L_30);
		IntPoint_t3326126179  L_31 = L_30->get_Pt_1();
		bool L_32 = ((ClipperBase_t512275816 *)__this)->get_m_UseFullRange_9();
		bool L_33 = ClipperBase_SlopesEqual_m1151992421(NULL /*static, unused*/, L_26, L_28, L_31, L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0109;
		}
	}
	{
		bool L_34 = ClipperBase_get_PreserveCollinear_m701993455(__this, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_00d5;
		}
	}
	{
		OutPt_t3947633180 * L_35 = V_1;
		NullCheck(L_35);
		OutPt_t3947633180 * L_36 = L_35->get_Prev_3();
		NullCheck(L_36);
		IntPoint_t3326126179  L_37 = L_36->get_Pt_1();
		OutPt_t3947633180 * L_38 = V_1;
		NullCheck(L_38);
		IntPoint_t3326126179  L_39 = L_38->get_Pt_1();
		OutPt_t3947633180 * L_40 = V_1;
		NullCheck(L_40);
		OutPt_t3947633180 * L_41 = L_40->get_Next_2();
		NullCheck(L_41);
		IntPoint_t3326126179  L_42 = L_41->get_Pt_1();
		bool L_43 = ClipperBase_Pt2IsBetweenPt1AndPt3_m1930781903(__this, L_37, L_39, L_42, /*hidden argument*/NULL);
		if (L_43)
		{
			goto IL_0109;
		}
	}

IL_00d5:
	{
		V_0 = (OutPt_t3947633180 *)NULL;
		OutPt_t3947633180 * L_44 = V_1;
		V_2 = L_44;
		OutPt_t3947633180 * L_45 = V_1;
		NullCheck(L_45);
		OutPt_t3947633180 * L_46 = L_45->get_Prev_3();
		OutPt_t3947633180 * L_47 = V_1;
		NullCheck(L_47);
		OutPt_t3947633180 * L_48 = L_47->get_Next_2();
		NullCheck(L_46);
		L_46->set_Next_2(L_48);
		OutPt_t3947633180 * L_49 = V_1;
		NullCheck(L_49);
		OutPt_t3947633180 * L_50 = L_49->get_Next_2();
		OutPt_t3947633180 * L_51 = V_1;
		NullCheck(L_51);
		OutPt_t3947633180 * L_52 = L_51->get_Prev_3();
		NullCheck(L_50);
		L_50->set_Prev_3(L_52);
		OutPt_t3947633180 * L_53 = V_1;
		NullCheck(L_53);
		OutPt_t3947633180 * L_54 = L_53->get_Prev_3();
		V_1 = L_54;
		V_2 = (OutPt_t3947633180 *)NULL;
		goto IL_0124;
	}

IL_0109:
	{
		OutPt_t3947633180 * L_55 = V_1;
		OutPt_t3947633180 * L_56 = V_0;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_55) == ((Il2CppObject*)(OutPt_t3947633180 *)L_56))))
		{
			goto IL_0115;
		}
	}
	{
		goto IL_0129;
	}

IL_0115:
	{
		OutPt_t3947633180 * L_57 = V_0;
		if (L_57)
		{
			goto IL_011d;
		}
	}
	{
		OutPt_t3947633180 * L_58 = V_1;
		V_0 = L_58;
	}

IL_011d:
	{
		OutPt_t3947633180 * L_59 = V_1;
		NullCheck(L_59);
		OutPt_t3947633180 * L_60 = L_59->get_Next_2();
		V_1 = L_60;
	}

IL_0124:
	{
		goto IL_0015;
	}

IL_0129:
	{
		OutRec_t3245805284 * L_61 = ___outRec0;
		OutPt_t3947633180 * L_62 = V_1;
		NullCheck(L_61);
		L_61->set_Pts_4(L_62);
		return;
	}
}
// Pathfinding.ClipperLib.OutPt Pathfinding.ClipperLib.Clipper::DupOutPt(Pathfinding.ClipperLib.OutPt,System.Boolean)
extern Il2CppClass* OutPt_t3947633180_il2cpp_TypeInfo_var;
extern const uint32_t Clipper_DupOutPt_m4193918363_MetadataUsageId;
extern "C"  OutPt_t3947633180 * Clipper_DupOutPt_m4193918363 (Clipper_t636949239 * __this, OutPt_t3947633180 * ___outPt0, bool ___InsertAfter1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_DupOutPt_m4193918363_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	OutPt_t3947633180 * V_0 = NULL;
	{
		OutPt_t3947633180 * L_0 = (OutPt_t3947633180 *)il2cpp_codegen_object_new(OutPt_t3947633180_il2cpp_TypeInfo_var);
		OutPt__ctor_m1159254633(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		OutPt_t3947633180 * L_1 = V_0;
		OutPt_t3947633180 * L_2 = ___outPt0;
		NullCheck(L_2);
		IntPoint_t3326126179  L_3 = L_2->get_Pt_1();
		NullCheck(L_1);
		L_1->set_Pt_1(L_3);
		OutPt_t3947633180 * L_4 = V_0;
		OutPt_t3947633180 * L_5 = ___outPt0;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_Idx_0();
		NullCheck(L_4);
		L_4->set_Idx_0(L_6);
		bool L_7 = ___InsertAfter1;
		if (!L_7)
		{
			goto IL_004f;
		}
	}
	{
		OutPt_t3947633180 * L_8 = V_0;
		OutPt_t3947633180 * L_9 = ___outPt0;
		NullCheck(L_9);
		OutPt_t3947633180 * L_10 = L_9->get_Next_2();
		NullCheck(L_8);
		L_8->set_Next_2(L_10);
		OutPt_t3947633180 * L_11 = V_0;
		OutPt_t3947633180 * L_12 = ___outPt0;
		NullCheck(L_11);
		L_11->set_Prev_3(L_12);
		OutPt_t3947633180 * L_13 = ___outPt0;
		NullCheck(L_13);
		OutPt_t3947633180 * L_14 = L_13->get_Next_2();
		OutPt_t3947633180 * L_15 = V_0;
		NullCheck(L_14);
		L_14->set_Prev_3(L_15);
		OutPt_t3947633180 * L_16 = ___outPt0;
		OutPt_t3947633180 * L_17 = V_0;
		NullCheck(L_16);
		L_16->set_Next_2(L_17);
		goto IL_0075;
	}

IL_004f:
	{
		OutPt_t3947633180 * L_18 = V_0;
		OutPt_t3947633180 * L_19 = ___outPt0;
		NullCheck(L_19);
		OutPt_t3947633180 * L_20 = L_19->get_Prev_3();
		NullCheck(L_18);
		L_18->set_Prev_3(L_20);
		OutPt_t3947633180 * L_21 = V_0;
		OutPt_t3947633180 * L_22 = ___outPt0;
		NullCheck(L_21);
		L_21->set_Next_2(L_22);
		OutPt_t3947633180 * L_23 = ___outPt0;
		NullCheck(L_23);
		OutPt_t3947633180 * L_24 = L_23->get_Prev_3();
		OutPt_t3947633180 * L_25 = V_0;
		NullCheck(L_24);
		L_24->set_Next_2(L_25);
		OutPt_t3947633180 * L_26 = ___outPt0;
		OutPt_t3947633180 * L_27 = V_0;
		NullCheck(L_26);
		L_26->set_Prev_3(L_27);
	}

IL_0075:
	{
		OutPt_t3947633180 * L_28 = V_0;
		return L_28;
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::GetOverlap(System.Int64,System.Int64,System.Int64,System.Int64,System.Int64&,System.Int64&)
extern "C"  bool Clipper_GetOverlap_m1667878347 (Clipper_t636949239 * __this, int64_t ___a10, int64_t ___a21, int64_t ___b12, int64_t ___b23, int64_t* ___Left4, int64_t* ___Right5, const MethodInfo* method)
{
	{
		int64_t L_0 = ___a10;
		int64_t L_1 = ___a21;
		if ((((int64_t)L_0) >= ((int64_t)L_1)))
		{
			goto IL_0043;
		}
	}
	{
		int64_t L_2 = ___b12;
		int64_t L_3 = ___b23;
		if ((((int64_t)L_2) >= ((int64_t)L_3)))
		{
			goto IL_0029;
		}
	}
	{
		int64_t* L_4 = ___Left4;
		int64_t L_5 = ___a10;
		int64_t L_6 = ___b12;
		int64_t L_7 = Math_Max_m136328954(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		*((int64_t*)(L_4)) = (int64_t)L_7;
		int64_t* L_8 = ___Right5;
		int64_t L_9 = ___a21;
		int64_t L_10 = ___b23;
		int64_t L_11 = Math_Min_m3933540684(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		*((int64_t*)(L_8)) = (int64_t)L_11;
		goto IL_003e;
	}

IL_0029:
	{
		int64_t* L_12 = ___Left4;
		int64_t L_13 = ___a10;
		int64_t L_14 = ___b23;
		int64_t L_15 = Math_Max_m136328954(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		*((int64_t*)(L_12)) = (int64_t)L_15;
		int64_t* L_16 = ___Right5;
		int64_t L_17 = ___a21;
		int64_t L_18 = ___b12;
		int64_t L_19 = Math_Min_m3933540684(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		*((int64_t*)(L_16)) = (int64_t)L_19;
	}

IL_003e:
	{
		goto IL_007a;
	}

IL_0043:
	{
		int64_t L_20 = ___b12;
		int64_t L_21 = ___b23;
		if ((((int64_t)L_20) >= ((int64_t)L_21)))
		{
			goto IL_0065;
		}
	}
	{
		int64_t* L_22 = ___Left4;
		int64_t L_23 = ___a21;
		int64_t L_24 = ___b12;
		int64_t L_25 = Math_Max_m136328954(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		*((int64_t*)(L_22)) = (int64_t)L_25;
		int64_t* L_26 = ___Right5;
		int64_t L_27 = ___a10;
		int64_t L_28 = ___b23;
		int64_t L_29 = Math_Min_m3933540684(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
		*((int64_t*)(L_26)) = (int64_t)L_29;
		goto IL_007a;
	}

IL_0065:
	{
		int64_t* L_30 = ___Left4;
		int64_t L_31 = ___a21;
		int64_t L_32 = ___b23;
		int64_t L_33 = Math_Max_m136328954(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		*((int64_t*)(L_30)) = (int64_t)L_33;
		int64_t* L_34 = ___Right5;
		int64_t L_35 = ___a10;
		int64_t L_36 = ___b12;
		int64_t L_37 = Math_Min_m3933540684(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		*((int64_t*)(L_34)) = (int64_t)L_37;
	}

IL_007a:
	{
		int64_t* L_38 = ___Left4;
		int64_t* L_39 = ___Right5;
		return (bool)((((int64_t)(*((int64_t*)L_38))) < ((int64_t)(*((int64_t*)L_39))))? 1 : 0);
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::JoinHorz(Pathfinding.ClipperLib.OutPt,Pathfinding.ClipperLib.OutPt,Pathfinding.ClipperLib.OutPt,Pathfinding.ClipperLib.OutPt,Pathfinding.ClipperLib.IntPoint,System.Boolean)
extern "C"  bool Clipper_JoinHorz_m1354690147 (Clipper_t636949239 * __this, OutPt_t3947633180 * ___op10, OutPt_t3947633180 * ___op1b1, OutPt_t3947633180 * ___op22, OutPt_t3947633180 * ___op2b3, IntPoint_t3326126179  ___Pt4, bool ___DiscardLeft5, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B6_0 = 0;
	{
		OutPt_t3947633180 * L_0 = ___op10;
		NullCheck(L_0);
		IntPoint_t3326126179 * L_1 = L_0->get_address_of_Pt_1();
		int64_t L_2 = L_1->get_X_0();
		OutPt_t3947633180 * L_3 = ___op1b1;
		NullCheck(L_3);
		IntPoint_t3326126179 * L_4 = L_3->get_address_of_Pt_1();
		int64_t L_5 = L_4->get_X_0();
		if ((((int64_t)L_2) <= ((int64_t)L_5)))
		{
			goto IL_0021;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 1;
	}

IL_0022:
	{
		V_0 = G_B3_0;
		OutPt_t3947633180 * L_6 = ___op22;
		NullCheck(L_6);
		IntPoint_t3326126179 * L_7 = L_6->get_address_of_Pt_1();
		int64_t L_8 = L_7->get_X_0();
		OutPt_t3947633180 * L_9 = ___op2b3;
		NullCheck(L_9);
		IntPoint_t3326126179 * L_10 = L_9->get_address_of_Pt_1();
		int64_t L_11 = L_10->get_X_0();
		if ((((int64_t)L_8) <= ((int64_t)L_11)))
		{
			goto IL_0045;
		}
	}
	{
		G_B6_0 = 0;
		goto IL_0046;
	}

IL_0045:
	{
		G_B6_0 = 1;
	}

IL_0046:
	{
		V_1 = G_B6_0;
		int32_t L_12 = V_0;
		int32_t L_13 = V_1;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0050;
		}
	}
	{
		return (bool)0;
	}

IL_0050:
	{
		int32_t L_14 = V_0;
		if ((!(((uint32_t)L_14) == ((uint32_t)1))))
		{
			goto IL_0120;
		}
	}
	{
		goto IL_0064;
	}

IL_005c:
	{
		OutPt_t3947633180 * L_15 = ___op10;
		NullCheck(L_15);
		OutPt_t3947633180 * L_16 = L_15->get_Next_2();
		___op10 = L_16;
	}

IL_0064:
	{
		OutPt_t3947633180 * L_17 = ___op10;
		NullCheck(L_17);
		OutPt_t3947633180 * L_18 = L_17->get_Next_2();
		NullCheck(L_18);
		IntPoint_t3326126179 * L_19 = L_18->get_address_of_Pt_1();
		int64_t L_20 = L_19->get_X_0();
		int64_t L_21 = (&___Pt4)->get_X_0();
		if ((((int64_t)L_20) > ((int64_t)L_21)))
		{
			goto IL_00bc;
		}
	}
	{
		OutPt_t3947633180 * L_22 = ___op10;
		NullCheck(L_22);
		OutPt_t3947633180 * L_23 = L_22->get_Next_2();
		NullCheck(L_23);
		IntPoint_t3326126179 * L_24 = L_23->get_address_of_Pt_1();
		int64_t L_25 = L_24->get_X_0();
		OutPt_t3947633180 * L_26 = ___op10;
		NullCheck(L_26);
		IntPoint_t3326126179 * L_27 = L_26->get_address_of_Pt_1();
		int64_t L_28 = L_27->get_X_0();
		if ((((int64_t)L_25) < ((int64_t)L_28)))
		{
			goto IL_00bc;
		}
	}
	{
		OutPt_t3947633180 * L_29 = ___op10;
		NullCheck(L_29);
		OutPt_t3947633180 * L_30 = L_29->get_Next_2();
		NullCheck(L_30);
		IntPoint_t3326126179 * L_31 = L_30->get_address_of_Pt_1();
		int64_t L_32 = L_31->get_Y_1();
		int64_t L_33 = (&___Pt4)->get_Y_1();
		if ((((int64_t)L_32) == ((int64_t)L_33)))
		{
			goto IL_005c;
		}
	}

IL_00bc:
	{
		bool L_34 = ___DiscardLeft5;
		if (!L_34)
		{
			goto IL_00e2;
		}
	}
	{
		OutPt_t3947633180 * L_35 = ___op10;
		NullCheck(L_35);
		IntPoint_t3326126179 * L_36 = L_35->get_address_of_Pt_1();
		int64_t L_37 = L_36->get_X_0();
		int64_t L_38 = (&___Pt4)->get_X_0();
		if ((((int64_t)L_37) == ((int64_t)L_38)))
		{
			goto IL_00e2;
		}
	}
	{
		OutPt_t3947633180 * L_39 = ___op10;
		NullCheck(L_39);
		OutPt_t3947633180 * L_40 = L_39->get_Next_2();
		___op10 = L_40;
	}

IL_00e2:
	{
		OutPt_t3947633180 * L_41 = ___op10;
		bool L_42 = ___DiscardLeft5;
		OutPt_t3947633180 * L_43 = Clipper_DupOutPt_m4193918363(__this, L_41, (bool)((((int32_t)L_42) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		___op1b1 = L_43;
		OutPt_t3947633180 * L_44 = ___op1b1;
		NullCheck(L_44);
		IntPoint_t3326126179  L_45 = L_44->get_Pt_1();
		IntPoint_t3326126179  L_46 = ___Pt4;
		bool L_47 = IntPoint_op_Inequality_m3996904515(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_011b;
		}
	}
	{
		OutPt_t3947633180 * L_48 = ___op1b1;
		___op10 = L_48;
		OutPt_t3947633180 * L_49 = ___op10;
		IntPoint_t3326126179  L_50 = ___Pt4;
		NullCheck(L_49);
		L_49->set_Pt_1(L_50);
		OutPt_t3947633180 * L_51 = ___op10;
		bool L_52 = ___DiscardLeft5;
		OutPt_t3947633180 * L_53 = Clipper_DupOutPt_m4193918363(__this, L_51, (bool)((((int32_t)L_52) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		___op1b1 = L_53;
	}

IL_011b:
	{
		goto IL_01de;
	}

IL_0120:
	{
		goto IL_012d;
	}

IL_0125:
	{
		OutPt_t3947633180 * L_54 = ___op10;
		NullCheck(L_54);
		OutPt_t3947633180 * L_55 = L_54->get_Next_2();
		___op10 = L_55;
	}

IL_012d:
	{
		OutPt_t3947633180 * L_56 = ___op10;
		NullCheck(L_56);
		OutPt_t3947633180 * L_57 = L_56->get_Next_2();
		NullCheck(L_57);
		IntPoint_t3326126179 * L_58 = L_57->get_address_of_Pt_1();
		int64_t L_59 = L_58->get_X_0();
		int64_t L_60 = (&___Pt4)->get_X_0();
		if ((((int64_t)L_59) < ((int64_t)L_60)))
		{
			goto IL_0185;
		}
	}
	{
		OutPt_t3947633180 * L_61 = ___op10;
		NullCheck(L_61);
		OutPt_t3947633180 * L_62 = L_61->get_Next_2();
		NullCheck(L_62);
		IntPoint_t3326126179 * L_63 = L_62->get_address_of_Pt_1();
		int64_t L_64 = L_63->get_X_0();
		OutPt_t3947633180 * L_65 = ___op10;
		NullCheck(L_65);
		IntPoint_t3326126179 * L_66 = L_65->get_address_of_Pt_1();
		int64_t L_67 = L_66->get_X_0();
		if ((((int64_t)L_64) > ((int64_t)L_67)))
		{
			goto IL_0185;
		}
	}
	{
		OutPt_t3947633180 * L_68 = ___op10;
		NullCheck(L_68);
		OutPt_t3947633180 * L_69 = L_68->get_Next_2();
		NullCheck(L_69);
		IntPoint_t3326126179 * L_70 = L_69->get_address_of_Pt_1();
		int64_t L_71 = L_70->get_Y_1();
		int64_t L_72 = (&___Pt4)->get_Y_1();
		if ((((int64_t)L_71) == ((int64_t)L_72)))
		{
			goto IL_0125;
		}
	}

IL_0185:
	{
		bool L_73 = ___DiscardLeft5;
		if (L_73)
		{
			goto IL_01ab;
		}
	}
	{
		OutPt_t3947633180 * L_74 = ___op10;
		NullCheck(L_74);
		IntPoint_t3326126179 * L_75 = L_74->get_address_of_Pt_1();
		int64_t L_76 = L_75->get_X_0();
		int64_t L_77 = (&___Pt4)->get_X_0();
		if ((((int64_t)L_76) == ((int64_t)L_77)))
		{
			goto IL_01ab;
		}
	}
	{
		OutPt_t3947633180 * L_78 = ___op10;
		NullCheck(L_78);
		OutPt_t3947633180 * L_79 = L_78->get_Next_2();
		___op10 = L_79;
	}

IL_01ab:
	{
		OutPt_t3947633180 * L_80 = ___op10;
		bool L_81 = ___DiscardLeft5;
		OutPt_t3947633180 * L_82 = Clipper_DupOutPt_m4193918363(__this, L_80, L_81, /*hidden argument*/NULL);
		___op1b1 = L_82;
		OutPt_t3947633180 * L_83 = ___op1b1;
		NullCheck(L_83);
		IntPoint_t3326126179  L_84 = L_83->get_Pt_1();
		IntPoint_t3326126179  L_85 = ___Pt4;
		bool L_86 = IntPoint_op_Inequality_m3996904515(NULL /*static, unused*/, L_84, L_85, /*hidden argument*/NULL);
		if (!L_86)
		{
			goto IL_01de;
		}
	}
	{
		OutPt_t3947633180 * L_87 = ___op1b1;
		___op10 = L_87;
		OutPt_t3947633180 * L_88 = ___op10;
		IntPoint_t3326126179  L_89 = ___Pt4;
		NullCheck(L_88);
		L_88->set_Pt_1(L_89);
		OutPt_t3947633180 * L_90 = ___op10;
		bool L_91 = ___DiscardLeft5;
		OutPt_t3947633180 * L_92 = Clipper_DupOutPt_m4193918363(__this, L_90, L_91, /*hidden argument*/NULL);
		___op1b1 = L_92;
	}

IL_01de:
	{
		int32_t L_93 = V_1;
		if ((!(((uint32_t)L_93) == ((uint32_t)1))))
		{
			goto IL_02b0;
		}
	}
	{
		goto IL_01f2;
	}

IL_01ea:
	{
		OutPt_t3947633180 * L_94 = ___op22;
		NullCheck(L_94);
		OutPt_t3947633180 * L_95 = L_94->get_Next_2();
		___op22 = L_95;
	}

IL_01f2:
	{
		OutPt_t3947633180 * L_96 = ___op22;
		NullCheck(L_96);
		OutPt_t3947633180 * L_97 = L_96->get_Next_2();
		NullCheck(L_97);
		IntPoint_t3326126179 * L_98 = L_97->get_address_of_Pt_1();
		int64_t L_99 = L_98->get_X_0();
		int64_t L_100 = (&___Pt4)->get_X_0();
		if ((((int64_t)L_99) > ((int64_t)L_100)))
		{
			goto IL_024a;
		}
	}
	{
		OutPt_t3947633180 * L_101 = ___op22;
		NullCheck(L_101);
		OutPt_t3947633180 * L_102 = L_101->get_Next_2();
		NullCheck(L_102);
		IntPoint_t3326126179 * L_103 = L_102->get_address_of_Pt_1();
		int64_t L_104 = L_103->get_X_0();
		OutPt_t3947633180 * L_105 = ___op22;
		NullCheck(L_105);
		IntPoint_t3326126179 * L_106 = L_105->get_address_of_Pt_1();
		int64_t L_107 = L_106->get_X_0();
		if ((((int64_t)L_104) < ((int64_t)L_107)))
		{
			goto IL_024a;
		}
	}
	{
		OutPt_t3947633180 * L_108 = ___op22;
		NullCheck(L_108);
		OutPt_t3947633180 * L_109 = L_108->get_Next_2();
		NullCheck(L_109);
		IntPoint_t3326126179 * L_110 = L_109->get_address_of_Pt_1();
		int64_t L_111 = L_110->get_Y_1();
		int64_t L_112 = (&___Pt4)->get_Y_1();
		if ((((int64_t)L_111) == ((int64_t)L_112)))
		{
			goto IL_01ea;
		}
	}

IL_024a:
	{
		bool L_113 = ___DiscardLeft5;
		if (!L_113)
		{
			goto IL_0270;
		}
	}
	{
		OutPt_t3947633180 * L_114 = ___op22;
		NullCheck(L_114);
		IntPoint_t3326126179 * L_115 = L_114->get_address_of_Pt_1();
		int64_t L_116 = L_115->get_X_0();
		int64_t L_117 = (&___Pt4)->get_X_0();
		if ((((int64_t)L_116) == ((int64_t)L_117)))
		{
			goto IL_0270;
		}
	}
	{
		OutPt_t3947633180 * L_118 = ___op22;
		NullCheck(L_118);
		OutPt_t3947633180 * L_119 = L_118->get_Next_2();
		___op22 = L_119;
	}

IL_0270:
	{
		OutPt_t3947633180 * L_120 = ___op22;
		bool L_121 = ___DiscardLeft5;
		OutPt_t3947633180 * L_122 = Clipper_DupOutPt_m4193918363(__this, L_120, (bool)((((int32_t)L_121) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		___op2b3 = L_122;
		OutPt_t3947633180 * L_123 = ___op2b3;
		NullCheck(L_123);
		IntPoint_t3326126179  L_124 = L_123->get_Pt_1();
		IntPoint_t3326126179  L_125 = ___Pt4;
		bool L_126 = IntPoint_op_Inequality_m3996904515(NULL /*static, unused*/, L_124, L_125, /*hidden argument*/NULL);
		if (!L_126)
		{
			goto IL_02ab;
		}
	}
	{
		OutPt_t3947633180 * L_127 = ___op2b3;
		___op22 = L_127;
		OutPt_t3947633180 * L_128 = ___op22;
		IntPoint_t3326126179  L_129 = ___Pt4;
		NullCheck(L_128);
		L_128->set_Pt_1(L_129);
		OutPt_t3947633180 * L_130 = ___op22;
		bool L_131 = ___DiscardLeft5;
		OutPt_t3947633180 * L_132 = Clipper_DupOutPt_m4193918363(__this, L_130, (bool)((((int32_t)L_131) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		___op2b3 = L_132;
	}

IL_02ab:
	{
		goto IL_0370;
	}

IL_02b0:
	{
		goto IL_02bd;
	}

IL_02b5:
	{
		OutPt_t3947633180 * L_133 = ___op22;
		NullCheck(L_133);
		OutPt_t3947633180 * L_134 = L_133->get_Next_2();
		___op22 = L_134;
	}

IL_02bd:
	{
		OutPt_t3947633180 * L_135 = ___op22;
		NullCheck(L_135);
		OutPt_t3947633180 * L_136 = L_135->get_Next_2();
		NullCheck(L_136);
		IntPoint_t3326126179 * L_137 = L_136->get_address_of_Pt_1();
		int64_t L_138 = L_137->get_X_0();
		int64_t L_139 = (&___Pt4)->get_X_0();
		if ((((int64_t)L_138) < ((int64_t)L_139)))
		{
			goto IL_0315;
		}
	}
	{
		OutPt_t3947633180 * L_140 = ___op22;
		NullCheck(L_140);
		OutPt_t3947633180 * L_141 = L_140->get_Next_2();
		NullCheck(L_141);
		IntPoint_t3326126179 * L_142 = L_141->get_address_of_Pt_1();
		int64_t L_143 = L_142->get_X_0();
		OutPt_t3947633180 * L_144 = ___op22;
		NullCheck(L_144);
		IntPoint_t3326126179 * L_145 = L_144->get_address_of_Pt_1();
		int64_t L_146 = L_145->get_X_0();
		if ((((int64_t)L_143) > ((int64_t)L_146)))
		{
			goto IL_0315;
		}
	}
	{
		OutPt_t3947633180 * L_147 = ___op22;
		NullCheck(L_147);
		OutPt_t3947633180 * L_148 = L_147->get_Next_2();
		NullCheck(L_148);
		IntPoint_t3326126179 * L_149 = L_148->get_address_of_Pt_1();
		int64_t L_150 = L_149->get_Y_1();
		int64_t L_151 = (&___Pt4)->get_Y_1();
		if ((((int64_t)L_150) == ((int64_t)L_151)))
		{
			goto IL_02b5;
		}
	}

IL_0315:
	{
		bool L_152 = ___DiscardLeft5;
		if (L_152)
		{
			goto IL_033b;
		}
	}
	{
		OutPt_t3947633180 * L_153 = ___op22;
		NullCheck(L_153);
		IntPoint_t3326126179 * L_154 = L_153->get_address_of_Pt_1();
		int64_t L_155 = L_154->get_X_0();
		int64_t L_156 = (&___Pt4)->get_X_0();
		if ((((int64_t)L_155) == ((int64_t)L_156)))
		{
			goto IL_033b;
		}
	}
	{
		OutPt_t3947633180 * L_157 = ___op22;
		NullCheck(L_157);
		OutPt_t3947633180 * L_158 = L_157->get_Next_2();
		___op22 = L_158;
	}

IL_033b:
	{
		OutPt_t3947633180 * L_159 = ___op22;
		bool L_160 = ___DiscardLeft5;
		OutPt_t3947633180 * L_161 = Clipper_DupOutPt_m4193918363(__this, L_159, L_160, /*hidden argument*/NULL);
		___op2b3 = L_161;
		OutPt_t3947633180 * L_162 = ___op2b3;
		NullCheck(L_162);
		IntPoint_t3326126179  L_163 = L_162->get_Pt_1();
		IntPoint_t3326126179  L_164 = ___Pt4;
		bool L_165 = IntPoint_op_Inequality_m3996904515(NULL /*static, unused*/, L_163, L_164, /*hidden argument*/NULL);
		if (!L_165)
		{
			goto IL_0370;
		}
	}
	{
		OutPt_t3947633180 * L_166 = ___op2b3;
		___op22 = L_166;
		OutPt_t3947633180 * L_167 = ___op22;
		IntPoint_t3326126179  L_168 = ___Pt4;
		NullCheck(L_167);
		L_167->set_Pt_1(L_168);
		OutPt_t3947633180 * L_169 = ___op22;
		bool L_170 = ___DiscardLeft5;
		OutPt_t3947633180 * L_171 = Clipper_DupOutPt_m4193918363(__this, L_169, L_170, /*hidden argument*/NULL);
		___op2b3 = L_171;
	}

IL_0370:
	{
		int32_t L_172 = V_0;
		bool L_173 = ___DiscardLeft5;
		if ((!(((uint32_t)((((int32_t)L_172) == ((int32_t)1))? 1 : 0)) == ((uint32_t)L_173))))
		{
			goto IL_039e;
		}
	}
	{
		OutPt_t3947633180 * L_174 = ___op10;
		OutPt_t3947633180 * L_175 = ___op22;
		NullCheck(L_174);
		L_174->set_Prev_3(L_175);
		OutPt_t3947633180 * L_176 = ___op22;
		OutPt_t3947633180 * L_177 = ___op10;
		NullCheck(L_176);
		L_176->set_Next_2(L_177);
		OutPt_t3947633180 * L_178 = ___op1b1;
		OutPt_t3947633180 * L_179 = ___op2b3;
		NullCheck(L_178);
		L_178->set_Next_2(L_179);
		OutPt_t3947633180 * L_180 = ___op2b3;
		OutPt_t3947633180 * L_181 = ___op1b1;
		NullCheck(L_180);
		L_180->set_Prev_3(L_181);
		goto IL_03bc;
	}

IL_039e:
	{
		OutPt_t3947633180 * L_182 = ___op10;
		OutPt_t3947633180 * L_183 = ___op22;
		NullCheck(L_182);
		L_182->set_Next_2(L_183);
		OutPt_t3947633180 * L_184 = ___op22;
		OutPt_t3947633180 * L_185 = ___op10;
		NullCheck(L_184);
		L_184->set_Prev_3(L_185);
		OutPt_t3947633180 * L_186 = ___op1b1;
		OutPt_t3947633180 * L_187 = ___op2b3;
		NullCheck(L_186);
		L_186->set_Prev_3(L_187);
		OutPt_t3947633180 * L_188 = ___op2b3;
		OutPt_t3947633180 * L_189 = ___op1b1;
		NullCheck(L_188);
		L_188->set_Next_2(L_189);
	}

IL_03bc:
	{
		return (bool)1;
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::JoinPoints(Pathfinding.ClipperLib.Join,Pathfinding.ClipperLib.OutPt&,Pathfinding.ClipperLib.OutPt&)
extern "C"  bool Clipper_JoinPoints_m49902773 (Clipper_t636949239 * __this, Join_t3970117804 * ___j0, OutPt_t3947633180 ** ___p11, OutPt_t3947633180 ** ___p22, const MethodInfo* method)
{
	OutRec_t3245805284 * V_0 = NULL;
	OutRec_t3245805284 * V_1 = NULL;
	OutPt_t3947633180 * V_2 = NULL;
	OutPt_t3947633180 * V_3 = NULL;
	OutPt_t3947633180 * V_4 = NULL;
	OutPt_t3947633180 * V_5 = NULL;
	bool V_6 = false;
	bool V_7 = false;
	bool V_8 = false;
	int64_t V_9 = 0;
	int64_t V_10 = 0;
	IntPoint_t3326126179  V_11;
	memset(&V_11, 0, sizeof(V_11));
	bool V_12 = false;
	bool V_13 = false;
	bool V_14 = false;
	int32_t G_B63_0 = 0;
	int32_t G_B78_0 = 0;
	{
		Join_t3970117804 * L_0 = ___j0;
		NullCheck(L_0);
		OutPt_t3947633180 * L_1 = L_0->get_OutPt1_0();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_Idx_0();
		OutRec_t3245805284 * L_3 = Clipper_GetOutRec_m918585808(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Join_t3970117804 * L_4 = ___j0;
		NullCheck(L_4);
		OutPt_t3947633180 * L_5 = L_4->get_OutPt2_1();
		NullCheck(L_5);
		int32_t L_6 = L_5->get_Idx_0();
		OutRec_t3245805284 * L_7 = Clipper_GetOutRec_m918585808(__this, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Join_t3970117804 * L_8 = ___j0;
		NullCheck(L_8);
		OutPt_t3947633180 * L_9 = L_8->get_OutPt1_0();
		V_2 = L_9;
		Join_t3970117804 * L_10 = ___j0;
		NullCheck(L_10);
		OutPt_t3947633180 * L_11 = L_10->get_OutPt2_1();
		V_4 = L_11;
		OutPt_t3947633180 ** L_12 = ___p11;
		*((Il2CppObject **)(L_12)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_12), (Il2CppObject *)NULL);
		OutPt_t3947633180 ** L_13 = ___p22;
		*((Il2CppObject **)(L_13)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_13), (Il2CppObject *)NULL);
		Join_t3970117804 * L_14 = ___j0;
		NullCheck(L_14);
		OutPt_t3947633180 * L_15 = L_14->get_OutPt1_0();
		NullCheck(L_15);
		IntPoint_t3326126179 * L_16 = L_15->get_address_of_Pt_1();
		int64_t L_17 = L_16->get_Y_1();
		Join_t3970117804 * L_18 = ___j0;
		NullCheck(L_18);
		IntPoint_t3326126179 * L_19 = L_18->get_address_of_OffPt_2();
		int64_t L_20 = L_19->get_Y_1();
		V_6 = (bool)((((int64_t)L_17) == ((int64_t)L_20))? 1 : 0);
		bool L_21 = V_6;
		if (!L_21)
		{
			goto IL_01c4;
		}
	}
	{
		Join_t3970117804 * L_22 = ___j0;
		NullCheck(L_22);
		IntPoint_t3326126179  L_23 = L_22->get_OffPt_2();
		Join_t3970117804 * L_24 = ___j0;
		NullCheck(L_24);
		OutPt_t3947633180 * L_25 = L_24->get_OutPt1_0();
		NullCheck(L_25);
		IntPoint_t3326126179  L_26 = L_25->get_Pt_1();
		bool L_27 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_23, L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_01c4;
		}
	}
	{
		Join_t3970117804 * L_28 = ___j0;
		NullCheck(L_28);
		IntPoint_t3326126179  L_29 = L_28->get_OffPt_2();
		Join_t3970117804 * L_30 = ___j0;
		NullCheck(L_30);
		OutPt_t3947633180 * L_31 = L_30->get_OutPt2_1();
		NullCheck(L_31);
		IntPoint_t3326126179  L_32 = L_31->get_Pt_1();
		bool L_33 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_29, L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_01c4;
		}
	}
	{
		Join_t3970117804 * L_34 = ___j0;
		NullCheck(L_34);
		OutPt_t3947633180 * L_35 = L_34->get_OutPt1_0();
		NullCheck(L_35);
		OutPt_t3947633180 * L_36 = L_35->get_Next_2();
		V_3 = L_36;
		goto IL_00ad;
	}

IL_00a6:
	{
		OutPt_t3947633180 * L_37 = V_3;
		NullCheck(L_37);
		OutPt_t3947633180 * L_38 = L_37->get_Next_2();
		V_3 = L_38;
	}

IL_00ad:
	{
		OutPt_t3947633180 * L_39 = V_3;
		OutPt_t3947633180 * L_40 = V_2;
		if ((((Il2CppObject*)(OutPt_t3947633180 *)L_39) == ((Il2CppObject*)(OutPt_t3947633180 *)L_40)))
		{
			goto IL_00ca;
		}
	}
	{
		OutPt_t3947633180 * L_41 = V_3;
		NullCheck(L_41);
		IntPoint_t3326126179  L_42 = L_41->get_Pt_1();
		Join_t3970117804 * L_43 = ___j0;
		NullCheck(L_43);
		IntPoint_t3326126179  L_44 = L_43->get_OffPt_2();
		bool L_45 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_42, L_44, /*hidden argument*/NULL);
		if (L_45)
		{
			goto IL_00a6;
		}
	}

IL_00ca:
	{
		OutPt_t3947633180 * L_46 = V_3;
		NullCheck(L_46);
		IntPoint_t3326126179 * L_47 = L_46->get_address_of_Pt_1();
		int64_t L_48 = L_47->get_Y_1();
		Join_t3970117804 * L_49 = ___j0;
		NullCheck(L_49);
		IntPoint_t3326126179 * L_50 = L_49->get_address_of_OffPt_2();
		int64_t L_51 = L_50->get_Y_1();
		V_7 = (bool)((((int64_t)L_48) > ((int64_t)L_51))? 1 : 0);
		Join_t3970117804 * L_52 = ___j0;
		NullCheck(L_52);
		OutPt_t3947633180 * L_53 = L_52->get_OutPt2_1();
		NullCheck(L_53);
		OutPt_t3947633180 * L_54 = L_53->get_Next_2();
		V_5 = L_54;
		goto IL_00ff;
	}

IL_00f6:
	{
		OutPt_t3947633180 * L_55 = V_5;
		NullCheck(L_55);
		OutPt_t3947633180 * L_56 = L_55->get_Next_2();
		V_5 = L_56;
	}

IL_00ff:
	{
		OutPt_t3947633180 * L_57 = V_5;
		OutPt_t3947633180 * L_58 = V_4;
		if ((((Il2CppObject*)(OutPt_t3947633180 *)L_57) == ((Il2CppObject*)(OutPt_t3947633180 *)L_58)))
		{
			goto IL_011f;
		}
	}
	{
		OutPt_t3947633180 * L_59 = V_5;
		NullCheck(L_59);
		IntPoint_t3326126179  L_60 = L_59->get_Pt_1();
		Join_t3970117804 * L_61 = ___j0;
		NullCheck(L_61);
		IntPoint_t3326126179  L_62 = L_61->get_OffPt_2();
		bool L_63 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_60, L_62, /*hidden argument*/NULL);
		if (L_63)
		{
			goto IL_00f6;
		}
	}

IL_011f:
	{
		OutPt_t3947633180 * L_64 = V_5;
		NullCheck(L_64);
		IntPoint_t3326126179 * L_65 = L_64->get_address_of_Pt_1();
		int64_t L_66 = L_65->get_Y_1();
		Join_t3970117804 * L_67 = ___j0;
		NullCheck(L_67);
		IntPoint_t3326126179 * L_68 = L_67->get_address_of_OffPt_2();
		int64_t L_69 = L_68->get_Y_1();
		V_8 = (bool)((((int64_t)L_66) > ((int64_t)L_69))? 1 : 0);
		bool L_70 = V_7;
		bool L_71 = V_8;
		if ((!(((uint32_t)L_70) == ((uint32_t)L_71))))
		{
			goto IL_0145;
		}
	}
	{
		return (bool)0;
	}

IL_0145:
	{
		bool L_72 = V_7;
		if (!L_72)
		{
			goto IL_0188;
		}
	}
	{
		OutPt_t3947633180 * L_73 = V_2;
		OutPt_t3947633180 * L_74 = Clipper_DupOutPt_m4193918363(__this, L_73, (bool)0, /*hidden argument*/NULL);
		V_3 = L_74;
		OutPt_t3947633180 * L_75 = V_4;
		OutPt_t3947633180 * L_76 = Clipper_DupOutPt_m4193918363(__this, L_75, (bool)1, /*hidden argument*/NULL);
		V_5 = L_76;
		OutPt_t3947633180 * L_77 = V_2;
		OutPt_t3947633180 * L_78 = V_4;
		NullCheck(L_77);
		L_77->set_Prev_3(L_78);
		OutPt_t3947633180 * L_79 = V_4;
		OutPt_t3947633180 * L_80 = V_2;
		NullCheck(L_79);
		L_79->set_Next_2(L_80);
		OutPt_t3947633180 * L_81 = V_3;
		OutPt_t3947633180 * L_82 = V_5;
		NullCheck(L_81);
		L_81->set_Next_2(L_82);
		OutPt_t3947633180 * L_83 = V_5;
		OutPt_t3947633180 * L_84 = V_3;
		NullCheck(L_83);
		L_83->set_Prev_3(L_84);
		OutPt_t3947633180 ** L_85 = ___p11;
		OutPt_t3947633180 * L_86 = V_2;
		*((Il2CppObject **)(L_85)) = (Il2CppObject *)L_86;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_85), (Il2CppObject *)L_86);
		OutPt_t3947633180 ** L_87 = ___p22;
		OutPt_t3947633180 * L_88 = V_3;
		*((Il2CppObject **)(L_87)) = (Il2CppObject *)L_88;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_87), (Il2CppObject *)L_88);
		return (bool)1;
	}

IL_0188:
	{
		OutPt_t3947633180 * L_89 = V_2;
		OutPt_t3947633180 * L_90 = Clipper_DupOutPt_m4193918363(__this, L_89, (bool)1, /*hidden argument*/NULL);
		V_3 = L_90;
		OutPt_t3947633180 * L_91 = V_4;
		OutPt_t3947633180 * L_92 = Clipper_DupOutPt_m4193918363(__this, L_91, (bool)0, /*hidden argument*/NULL);
		V_5 = L_92;
		OutPt_t3947633180 * L_93 = V_2;
		OutPt_t3947633180 * L_94 = V_4;
		NullCheck(L_93);
		L_93->set_Next_2(L_94);
		OutPt_t3947633180 * L_95 = V_4;
		OutPt_t3947633180 * L_96 = V_2;
		NullCheck(L_95);
		L_95->set_Prev_3(L_96);
		OutPt_t3947633180 * L_97 = V_3;
		OutPt_t3947633180 * L_98 = V_5;
		NullCheck(L_97);
		L_97->set_Prev_3(L_98);
		OutPt_t3947633180 * L_99 = V_5;
		OutPt_t3947633180 * L_100 = V_3;
		NullCheck(L_99);
		L_99->set_Next_2(L_100);
		OutPt_t3947633180 ** L_101 = ___p11;
		OutPt_t3947633180 * L_102 = V_2;
		*((Il2CppObject **)(L_101)) = (Il2CppObject *)L_102;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_101), (Il2CppObject *)L_102);
		OutPt_t3947633180 ** L_103 = ___p22;
		OutPt_t3947633180 * L_104 = V_3;
		*((Il2CppObject **)(L_103)) = (Il2CppObject *)L_104;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_103), (Il2CppObject *)L_104);
		return (bool)1;
	}

IL_01c4:
	{
		bool L_105 = V_6;
		if (!L_105)
		{
			goto IL_048b;
		}
	}
	{
		OutPt_t3947633180 * L_106 = V_2;
		V_3 = L_106;
		goto IL_01d9;
	}

IL_01d2:
	{
		OutPt_t3947633180 * L_107 = V_2;
		NullCheck(L_107);
		OutPt_t3947633180 * L_108 = L_107->get_Prev_3();
		V_2 = L_108;
	}

IL_01d9:
	{
		OutPt_t3947633180 * L_109 = V_2;
		NullCheck(L_109);
		OutPt_t3947633180 * L_110 = L_109->get_Prev_3();
		NullCheck(L_110);
		IntPoint_t3326126179 * L_111 = L_110->get_address_of_Pt_1();
		int64_t L_112 = L_111->get_Y_1();
		OutPt_t3947633180 * L_113 = V_2;
		NullCheck(L_113);
		IntPoint_t3326126179 * L_114 = L_113->get_address_of_Pt_1();
		int64_t L_115 = L_114->get_Y_1();
		if ((!(((uint64_t)L_112) == ((uint64_t)L_115))))
		{
			goto IL_0212;
		}
	}
	{
		OutPt_t3947633180 * L_116 = V_2;
		NullCheck(L_116);
		OutPt_t3947633180 * L_117 = L_116->get_Prev_3();
		OutPt_t3947633180 * L_118 = V_3;
		if ((((Il2CppObject*)(OutPt_t3947633180 *)L_117) == ((Il2CppObject*)(OutPt_t3947633180 *)L_118)))
		{
			goto IL_0212;
		}
	}
	{
		OutPt_t3947633180 * L_119 = V_2;
		NullCheck(L_119);
		OutPt_t3947633180 * L_120 = L_119->get_Prev_3();
		OutPt_t3947633180 * L_121 = V_4;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_120) == ((Il2CppObject*)(OutPt_t3947633180 *)L_121))))
		{
			goto IL_01d2;
		}
	}

IL_0212:
	{
		goto IL_021e;
	}

IL_0217:
	{
		OutPt_t3947633180 * L_122 = V_3;
		NullCheck(L_122);
		OutPt_t3947633180 * L_123 = L_122->get_Next_2();
		V_3 = L_123;
	}

IL_021e:
	{
		OutPt_t3947633180 * L_124 = V_3;
		NullCheck(L_124);
		OutPt_t3947633180 * L_125 = L_124->get_Next_2();
		NullCheck(L_125);
		IntPoint_t3326126179 * L_126 = L_125->get_address_of_Pt_1();
		int64_t L_127 = L_126->get_Y_1();
		OutPt_t3947633180 * L_128 = V_3;
		NullCheck(L_128);
		IntPoint_t3326126179 * L_129 = L_128->get_address_of_Pt_1();
		int64_t L_130 = L_129->get_Y_1();
		if ((!(((uint64_t)L_127) == ((uint64_t)L_130))))
		{
			goto IL_0257;
		}
	}
	{
		OutPt_t3947633180 * L_131 = V_3;
		NullCheck(L_131);
		OutPt_t3947633180 * L_132 = L_131->get_Next_2();
		OutPt_t3947633180 * L_133 = V_2;
		if ((((Il2CppObject*)(OutPt_t3947633180 *)L_132) == ((Il2CppObject*)(OutPt_t3947633180 *)L_133)))
		{
			goto IL_0257;
		}
	}
	{
		OutPt_t3947633180 * L_134 = V_3;
		NullCheck(L_134);
		OutPt_t3947633180 * L_135 = L_134->get_Next_2();
		OutPt_t3947633180 * L_136 = V_4;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_135) == ((Il2CppObject*)(OutPt_t3947633180 *)L_136))))
		{
			goto IL_0217;
		}
	}

IL_0257:
	{
		OutPt_t3947633180 * L_137 = V_3;
		NullCheck(L_137);
		OutPt_t3947633180 * L_138 = L_137->get_Next_2();
		OutPt_t3947633180 * L_139 = V_2;
		if ((((Il2CppObject*)(OutPt_t3947633180 *)L_138) == ((Il2CppObject*)(OutPt_t3947633180 *)L_139)))
		{
			goto IL_0270;
		}
	}
	{
		OutPt_t3947633180 * L_140 = V_3;
		NullCheck(L_140);
		OutPt_t3947633180 * L_141 = L_140->get_Next_2();
		OutPt_t3947633180 * L_142 = V_4;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_141) == ((Il2CppObject*)(OutPt_t3947633180 *)L_142))))
		{
			goto IL_0272;
		}
	}

IL_0270:
	{
		return (bool)0;
	}

IL_0272:
	{
		OutPt_t3947633180 * L_143 = V_4;
		V_5 = L_143;
		goto IL_0284;
	}

IL_027b:
	{
		OutPt_t3947633180 * L_144 = V_4;
		NullCheck(L_144);
		OutPt_t3947633180 * L_145 = L_144->get_Prev_3();
		V_4 = L_145;
	}

IL_0284:
	{
		OutPt_t3947633180 * L_146 = V_4;
		NullCheck(L_146);
		OutPt_t3947633180 * L_147 = L_146->get_Prev_3();
		NullCheck(L_147);
		IntPoint_t3326126179 * L_148 = L_147->get_address_of_Pt_1();
		int64_t L_149 = L_148->get_Y_1();
		OutPt_t3947633180 * L_150 = V_4;
		NullCheck(L_150);
		IntPoint_t3326126179 * L_151 = L_150->get_address_of_Pt_1();
		int64_t L_152 = L_151->get_Y_1();
		if ((!(((uint64_t)L_149) == ((uint64_t)L_152))))
		{
			goto IL_02c1;
		}
	}
	{
		OutPt_t3947633180 * L_153 = V_4;
		NullCheck(L_153);
		OutPt_t3947633180 * L_154 = L_153->get_Prev_3();
		OutPt_t3947633180 * L_155 = V_5;
		if ((((Il2CppObject*)(OutPt_t3947633180 *)L_154) == ((Il2CppObject*)(OutPt_t3947633180 *)L_155)))
		{
			goto IL_02c1;
		}
	}
	{
		OutPt_t3947633180 * L_156 = V_4;
		NullCheck(L_156);
		OutPt_t3947633180 * L_157 = L_156->get_Prev_3();
		OutPt_t3947633180 * L_158 = V_3;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_157) == ((Il2CppObject*)(OutPt_t3947633180 *)L_158))))
		{
			goto IL_027b;
		}
	}

IL_02c1:
	{
		goto IL_02cf;
	}

IL_02c6:
	{
		OutPt_t3947633180 * L_159 = V_5;
		NullCheck(L_159);
		OutPt_t3947633180 * L_160 = L_159->get_Next_2();
		V_5 = L_160;
	}

IL_02cf:
	{
		OutPt_t3947633180 * L_161 = V_5;
		NullCheck(L_161);
		OutPt_t3947633180 * L_162 = L_161->get_Next_2();
		NullCheck(L_162);
		IntPoint_t3326126179 * L_163 = L_162->get_address_of_Pt_1();
		int64_t L_164 = L_163->get_Y_1();
		OutPt_t3947633180 * L_165 = V_5;
		NullCheck(L_165);
		IntPoint_t3326126179 * L_166 = L_165->get_address_of_Pt_1();
		int64_t L_167 = L_166->get_Y_1();
		if ((!(((uint64_t)L_164) == ((uint64_t)L_167))))
		{
			goto IL_030c;
		}
	}
	{
		OutPt_t3947633180 * L_168 = V_5;
		NullCheck(L_168);
		OutPt_t3947633180 * L_169 = L_168->get_Next_2();
		OutPt_t3947633180 * L_170 = V_4;
		if ((((Il2CppObject*)(OutPt_t3947633180 *)L_169) == ((Il2CppObject*)(OutPt_t3947633180 *)L_170)))
		{
			goto IL_030c;
		}
	}
	{
		OutPt_t3947633180 * L_171 = V_5;
		NullCheck(L_171);
		OutPt_t3947633180 * L_172 = L_171->get_Next_2();
		OutPt_t3947633180 * L_173 = V_2;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_172) == ((Il2CppObject*)(OutPt_t3947633180 *)L_173))))
		{
			goto IL_02c6;
		}
	}

IL_030c:
	{
		OutPt_t3947633180 * L_174 = V_5;
		NullCheck(L_174);
		OutPt_t3947633180 * L_175 = L_174->get_Next_2();
		OutPt_t3947633180 * L_176 = V_4;
		if ((((Il2CppObject*)(OutPt_t3947633180 *)L_175) == ((Il2CppObject*)(OutPt_t3947633180 *)L_176)))
		{
			goto IL_0327;
		}
	}
	{
		OutPt_t3947633180 * L_177 = V_5;
		NullCheck(L_177);
		OutPt_t3947633180 * L_178 = L_177->get_Next_2();
		OutPt_t3947633180 * L_179 = V_2;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_178) == ((Il2CppObject*)(OutPt_t3947633180 *)L_179))))
		{
			goto IL_0329;
		}
	}

IL_0327:
	{
		return (bool)0;
	}

IL_0329:
	{
		OutPt_t3947633180 * L_180 = V_2;
		NullCheck(L_180);
		IntPoint_t3326126179 * L_181 = L_180->get_address_of_Pt_1();
		int64_t L_182 = L_181->get_X_0();
		OutPt_t3947633180 * L_183 = V_3;
		NullCheck(L_183);
		IntPoint_t3326126179 * L_184 = L_183->get_address_of_Pt_1();
		int64_t L_185 = L_184->get_X_0();
		OutPt_t3947633180 * L_186 = V_4;
		NullCheck(L_186);
		IntPoint_t3326126179 * L_187 = L_186->get_address_of_Pt_1();
		int64_t L_188 = L_187->get_X_0();
		OutPt_t3947633180 * L_189 = V_5;
		NullCheck(L_189);
		IntPoint_t3326126179 * L_190 = L_189->get_address_of_Pt_1();
		int64_t L_191 = L_190->get_X_0();
		bool L_192 = Clipper_GetOverlap_m1667878347(__this, L_182, L_185, L_188, L_191, (&V_9), (&V_10), /*hidden argument*/NULL);
		if (L_192)
		{
			goto IL_0368;
		}
	}
	{
		return (bool)0;
	}

IL_0368:
	{
		OutPt_t3947633180 * L_193 = V_2;
		NullCheck(L_193);
		IntPoint_t3326126179 * L_194 = L_193->get_address_of_Pt_1();
		int64_t L_195 = L_194->get_X_0();
		int64_t L_196 = V_9;
		if ((((int64_t)L_195) < ((int64_t)L_196)))
		{
			goto IL_03b3;
		}
	}
	{
		OutPt_t3947633180 * L_197 = V_2;
		NullCheck(L_197);
		IntPoint_t3326126179 * L_198 = L_197->get_address_of_Pt_1();
		int64_t L_199 = L_198->get_X_0();
		int64_t L_200 = V_10;
		if ((((int64_t)L_199) > ((int64_t)L_200)))
		{
			goto IL_03b3;
		}
	}
	{
		OutPt_t3947633180 * L_201 = V_2;
		NullCheck(L_201);
		IntPoint_t3326126179  L_202 = L_201->get_Pt_1();
		V_11 = L_202;
		OutPt_t3947633180 * L_203 = V_2;
		NullCheck(L_203);
		IntPoint_t3326126179 * L_204 = L_203->get_address_of_Pt_1();
		int64_t L_205 = L_204->get_X_0();
		OutPt_t3947633180 * L_206 = V_3;
		NullCheck(L_206);
		IntPoint_t3326126179 * L_207 = L_206->get_address_of_Pt_1();
		int64_t L_208 = L_207->get_X_0();
		V_12 = (bool)((((int64_t)L_205) > ((int64_t)L_208))? 1 : 0);
		goto IL_0473;
	}

IL_03b3:
	{
		OutPt_t3947633180 * L_209 = V_4;
		NullCheck(L_209);
		IntPoint_t3326126179 * L_210 = L_209->get_address_of_Pt_1();
		int64_t L_211 = L_210->get_X_0();
		int64_t L_212 = V_9;
		if ((((int64_t)L_211) < ((int64_t)L_212)))
		{
			goto IL_0403;
		}
	}
	{
		OutPt_t3947633180 * L_213 = V_4;
		NullCheck(L_213);
		IntPoint_t3326126179 * L_214 = L_213->get_address_of_Pt_1();
		int64_t L_215 = L_214->get_X_0();
		int64_t L_216 = V_10;
		if ((((int64_t)L_215) > ((int64_t)L_216)))
		{
			goto IL_0403;
		}
	}
	{
		OutPt_t3947633180 * L_217 = V_4;
		NullCheck(L_217);
		IntPoint_t3326126179  L_218 = L_217->get_Pt_1();
		V_11 = L_218;
		OutPt_t3947633180 * L_219 = V_4;
		NullCheck(L_219);
		IntPoint_t3326126179 * L_220 = L_219->get_address_of_Pt_1();
		int64_t L_221 = L_220->get_X_0();
		OutPt_t3947633180 * L_222 = V_5;
		NullCheck(L_222);
		IntPoint_t3326126179 * L_223 = L_222->get_address_of_Pt_1();
		int64_t L_224 = L_223->get_X_0();
		V_12 = (bool)((((int64_t)L_221) > ((int64_t)L_224))? 1 : 0);
		goto IL_0473;
	}

IL_0403:
	{
		OutPt_t3947633180 * L_225 = V_3;
		NullCheck(L_225);
		IntPoint_t3326126179 * L_226 = L_225->get_address_of_Pt_1();
		int64_t L_227 = L_226->get_X_0();
		int64_t L_228 = V_9;
		if ((((int64_t)L_227) < ((int64_t)L_228)))
		{
			goto IL_044e;
		}
	}
	{
		OutPt_t3947633180 * L_229 = V_3;
		NullCheck(L_229);
		IntPoint_t3326126179 * L_230 = L_229->get_address_of_Pt_1();
		int64_t L_231 = L_230->get_X_0();
		int64_t L_232 = V_10;
		if ((((int64_t)L_231) > ((int64_t)L_232)))
		{
			goto IL_044e;
		}
	}
	{
		OutPt_t3947633180 * L_233 = V_3;
		NullCheck(L_233);
		IntPoint_t3326126179  L_234 = L_233->get_Pt_1();
		V_11 = L_234;
		OutPt_t3947633180 * L_235 = V_3;
		NullCheck(L_235);
		IntPoint_t3326126179 * L_236 = L_235->get_address_of_Pt_1();
		int64_t L_237 = L_236->get_X_0();
		OutPt_t3947633180 * L_238 = V_2;
		NullCheck(L_238);
		IntPoint_t3326126179 * L_239 = L_238->get_address_of_Pt_1();
		int64_t L_240 = L_239->get_X_0();
		V_12 = (bool)((((int64_t)L_237) > ((int64_t)L_240))? 1 : 0);
		goto IL_0473;
	}

IL_044e:
	{
		OutPt_t3947633180 * L_241 = V_5;
		NullCheck(L_241);
		IntPoint_t3326126179  L_242 = L_241->get_Pt_1();
		V_11 = L_242;
		OutPt_t3947633180 * L_243 = V_5;
		NullCheck(L_243);
		IntPoint_t3326126179 * L_244 = L_243->get_address_of_Pt_1();
		int64_t L_245 = L_244->get_X_0();
		OutPt_t3947633180 * L_246 = V_4;
		NullCheck(L_246);
		IntPoint_t3326126179 * L_247 = L_246->get_address_of_Pt_1();
		int64_t L_248 = L_247->get_X_0();
		V_12 = (bool)((((int64_t)L_245) > ((int64_t)L_248))? 1 : 0);
	}

IL_0473:
	{
		OutPt_t3947633180 ** L_249 = ___p11;
		OutPt_t3947633180 * L_250 = V_2;
		*((Il2CppObject **)(L_249)) = (Il2CppObject *)L_250;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_249), (Il2CppObject *)L_250);
		OutPt_t3947633180 ** L_251 = ___p22;
		OutPt_t3947633180 * L_252 = V_4;
		*((Il2CppObject **)(L_251)) = (Il2CppObject *)L_252;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_251), (Il2CppObject *)L_252);
		OutPt_t3947633180 * L_253 = V_2;
		OutPt_t3947633180 * L_254 = V_3;
		OutPt_t3947633180 * L_255 = V_4;
		OutPt_t3947633180 * L_256 = V_5;
		IntPoint_t3326126179  L_257 = V_11;
		bool L_258 = V_12;
		bool L_259 = Clipper_JoinHorz_m1354690147(__this, L_253, L_254, L_255, L_256, L_257, L_258, /*hidden argument*/NULL);
		return L_259;
	}

IL_048b:
	{
		OutPt_t3947633180 * L_260 = V_2;
		NullCheck(L_260);
		OutPt_t3947633180 * L_261 = L_260->get_Next_2();
		V_3 = L_261;
		goto IL_049e;
	}

IL_0497:
	{
		OutPt_t3947633180 * L_262 = V_3;
		NullCheck(L_262);
		OutPt_t3947633180 * L_263 = L_262->get_Next_2();
		V_3 = L_263;
	}

IL_049e:
	{
		OutPt_t3947633180 * L_264 = V_3;
		NullCheck(L_264);
		IntPoint_t3326126179  L_265 = L_264->get_Pt_1();
		OutPt_t3947633180 * L_266 = V_2;
		NullCheck(L_266);
		IntPoint_t3326126179  L_267 = L_266->get_Pt_1();
		bool L_268 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_265, L_267, /*hidden argument*/NULL);
		if (!L_268)
		{
			goto IL_04bb;
		}
	}
	{
		OutPt_t3947633180 * L_269 = V_3;
		OutPt_t3947633180 * L_270 = V_2;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_269) == ((Il2CppObject*)(OutPt_t3947633180 *)L_270))))
		{
			goto IL_0497;
		}
	}

IL_04bb:
	{
		OutPt_t3947633180 * L_271 = V_3;
		NullCheck(L_271);
		IntPoint_t3326126179 * L_272 = L_271->get_address_of_Pt_1();
		int64_t L_273 = L_272->get_Y_1();
		OutPt_t3947633180 * L_274 = V_2;
		NullCheck(L_274);
		IntPoint_t3326126179 * L_275 = L_274->get_address_of_Pt_1();
		int64_t L_276 = L_275->get_Y_1();
		if ((((int64_t)L_273) > ((int64_t)L_276)))
		{
			goto IL_04f8;
		}
	}
	{
		OutPt_t3947633180 * L_277 = V_2;
		NullCheck(L_277);
		IntPoint_t3326126179  L_278 = L_277->get_Pt_1();
		OutPt_t3947633180 * L_279 = V_3;
		NullCheck(L_279);
		IntPoint_t3326126179  L_280 = L_279->get_Pt_1();
		Join_t3970117804 * L_281 = ___j0;
		NullCheck(L_281);
		IntPoint_t3326126179  L_282 = L_281->get_OffPt_2();
		bool L_283 = ((ClipperBase_t512275816 *)__this)->get_m_UseFullRange_9();
		bool L_284 = ClipperBase_SlopesEqual_m1151992421(NULL /*static, unused*/, L_278, L_280, L_282, L_283, /*hidden argument*/NULL);
		G_B63_0 = ((((int32_t)L_284) == ((int32_t)0))? 1 : 0);
		goto IL_04f9;
	}

IL_04f8:
	{
		G_B63_0 = 1;
	}

IL_04f9:
	{
		V_13 = (bool)G_B63_0;
		bool L_285 = V_13;
		if (!L_285)
		{
			goto IL_0571;
		}
	}
	{
		OutPt_t3947633180 * L_286 = V_2;
		NullCheck(L_286);
		OutPt_t3947633180 * L_287 = L_286->get_Prev_3();
		V_3 = L_287;
		goto IL_0515;
	}

IL_050e:
	{
		OutPt_t3947633180 * L_288 = V_3;
		NullCheck(L_288);
		OutPt_t3947633180 * L_289 = L_288->get_Prev_3();
		V_3 = L_289;
	}

IL_0515:
	{
		OutPt_t3947633180 * L_290 = V_3;
		NullCheck(L_290);
		IntPoint_t3326126179  L_291 = L_290->get_Pt_1();
		OutPt_t3947633180 * L_292 = V_2;
		NullCheck(L_292);
		IntPoint_t3326126179  L_293 = L_292->get_Pt_1();
		bool L_294 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_291, L_293, /*hidden argument*/NULL);
		if (!L_294)
		{
			goto IL_0532;
		}
	}
	{
		OutPt_t3947633180 * L_295 = V_3;
		OutPt_t3947633180 * L_296 = V_2;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_295) == ((Il2CppObject*)(OutPt_t3947633180 *)L_296))))
		{
			goto IL_050e;
		}
	}

IL_0532:
	{
		OutPt_t3947633180 * L_297 = V_3;
		NullCheck(L_297);
		IntPoint_t3326126179 * L_298 = L_297->get_address_of_Pt_1();
		int64_t L_299 = L_298->get_Y_1();
		OutPt_t3947633180 * L_300 = V_2;
		NullCheck(L_300);
		IntPoint_t3326126179 * L_301 = L_300->get_address_of_Pt_1();
		int64_t L_302 = L_301->get_Y_1();
		if ((((int64_t)L_299) > ((int64_t)L_302)))
		{
			goto IL_056f;
		}
	}
	{
		OutPt_t3947633180 * L_303 = V_2;
		NullCheck(L_303);
		IntPoint_t3326126179  L_304 = L_303->get_Pt_1();
		OutPt_t3947633180 * L_305 = V_3;
		NullCheck(L_305);
		IntPoint_t3326126179  L_306 = L_305->get_Pt_1();
		Join_t3970117804 * L_307 = ___j0;
		NullCheck(L_307);
		IntPoint_t3326126179  L_308 = L_307->get_OffPt_2();
		bool L_309 = ((ClipperBase_t512275816 *)__this)->get_m_UseFullRange_9();
		bool L_310 = ClipperBase_SlopesEqual_m1151992421(NULL /*static, unused*/, L_304, L_306, L_308, L_309, /*hidden argument*/NULL);
		if (L_310)
		{
			goto IL_0571;
		}
	}

IL_056f:
	{
		return (bool)0;
	}

IL_0571:
	{
		OutPt_t3947633180 * L_311 = V_4;
		NullCheck(L_311);
		OutPt_t3947633180 * L_312 = L_311->get_Next_2();
		V_5 = L_312;
		goto IL_0588;
	}

IL_057f:
	{
		OutPt_t3947633180 * L_313 = V_5;
		NullCheck(L_313);
		OutPt_t3947633180 * L_314 = L_313->get_Next_2();
		V_5 = L_314;
	}

IL_0588:
	{
		OutPt_t3947633180 * L_315 = V_5;
		NullCheck(L_315);
		IntPoint_t3326126179  L_316 = L_315->get_Pt_1();
		OutPt_t3947633180 * L_317 = V_4;
		NullCheck(L_317);
		IntPoint_t3326126179  L_318 = L_317->get_Pt_1();
		bool L_319 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_316, L_318, /*hidden argument*/NULL);
		if (!L_319)
		{
			goto IL_05a9;
		}
	}
	{
		OutPt_t3947633180 * L_320 = V_5;
		OutPt_t3947633180 * L_321 = V_4;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_320) == ((Il2CppObject*)(OutPt_t3947633180 *)L_321))))
		{
			goto IL_057f;
		}
	}

IL_05a9:
	{
		OutPt_t3947633180 * L_322 = V_5;
		NullCheck(L_322);
		IntPoint_t3326126179 * L_323 = L_322->get_address_of_Pt_1();
		int64_t L_324 = L_323->get_Y_1();
		OutPt_t3947633180 * L_325 = V_4;
		NullCheck(L_325);
		IntPoint_t3326126179 * L_326 = L_325->get_address_of_Pt_1();
		int64_t L_327 = L_326->get_Y_1();
		if ((((int64_t)L_324) > ((int64_t)L_327)))
		{
			goto IL_05ea;
		}
	}
	{
		OutPt_t3947633180 * L_328 = V_4;
		NullCheck(L_328);
		IntPoint_t3326126179  L_329 = L_328->get_Pt_1();
		OutPt_t3947633180 * L_330 = V_5;
		NullCheck(L_330);
		IntPoint_t3326126179  L_331 = L_330->get_Pt_1();
		Join_t3970117804 * L_332 = ___j0;
		NullCheck(L_332);
		IntPoint_t3326126179  L_333 = L_332->get_OffPt_2();
		bool L_334 = ((ClipperBase_t512275816 *)__this)->get_m_UseFullRange_9();
		bool L_335 = ClipperBase_SlopesEqual_m1151992421(NULL /*static, unused*/, L_329, L_331, L_333, L_334, /*hidden argument*/NULL);
		G_B78_0 = ((((int32_t)L_335) == ((int32_t)0))? 1 : 0);
		goto IL_05eb;
	}

IL_05ea:
	{
		G_B78_0 = 1;
	}

IL_05eb:
	{
		V_14 = (bool)G_B78_0;
		bool L_336 = V_14;
		if (!L_336)
		{
			goto IL_066f;
		}
	}
	{
		OutPt_t3947633180 * L_337 = V_4;
		NullCheck(L_337);
		OutPt_t3947633180 * L_338 = L_337->get_Prev_3();
		V_5 = L_338;
		goto IL_060b;
	}

IL_0602:
	{
		OutPt_t3947633180 * L_339 = V_5;
		NullCheck(L_339);
		OutPt_t3947633180 * L_340 = L_339->get_Prev_3();
		V_5 = L_340;
	}

IL_060b:
	{
		OutPt_t3947633180 * L_341 = V_5;
		NullCheck(L_341);
		IntPoint_t3326126179  L_342 = L_341->get_Pt_1();
		OutPt_t3947633180 * L_343 = V_4;
		NullCheck(L_343);
		IntPoint_t3326126179  L_344 = L_343->get_Pt_1();
		bool L_345 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_342, L_344, /*hidden argument*/NULL);
		if (!L_345)
		{
			goto IL_062c;
		}
	}
	{
		OutPt_t3947633180 * L_346 = V_5;
		OutPt_t3947633180 * L_347 = V_4;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_346) == ((Il2CppObject*)(OutPt_t3947633180 *)L_347))))
		{
			goto IL_0602;
		}
	}

IL_062c:
	{
		OutPt_t3947633180 * L_348 = V_5;
		NullCheck(L_348);
		IntPoint_t3326126179 * L_349 = L_348->get_address_of_Pt_1();
		int64_t L_350 = L_349->get_Y_1();
		OutPt_t3947633180 * L_351 = V_4;
		NullCheck(L_351);
		IntPoint_t3326126179 * L_352 = L_351->get_address_of_Pt_1();
		int64_t L_353 = L_352->get_Y_1();
		if ((((int64_t)L_350) > ((int64_t)L_353)))
		{
			goto IL_066d;
		}
	}
	{
		OutPt_t3947633180 * L_354 = V_4;
		NullCheck(L_354);
		IntPoint_t3326126179  L_355 = L_354->get_Pt_1();
		OutPt_t3947633180 * L_356 = V_5;
		NullCheck(L_356);
		IntPoint_t3326126179  L_357 = L_356->get_Pt_1();
		Join_t3970117804 * L_358 = ___j0;
		NullCheck(L_358);
		IntPoint_t3326126179  L_359 = L_358->get_OffPt_2();
		bool L_360 = ((ClipperBase_t512275816 *)__this)->get_m_UseFullRange_9();
		bool L_361 = ClipperBase_SlopesEqual_m1151992421(NULL /*static, unused*/, L_355, L_357, L_359, L_360, /*hidden argument*/NULL);
		if (L_361)
		{
			goto IL_066f;
		}
	}

IL_066d:
	{
		return (bool)0;
	}

IL_066f:
	{
		OutPt_t3947633180 * L_362 = V_3;
		OutPt_t3947633180 * L_363 = V_2;
		if ((((Il2CppObject*)(OutPt_t3947633180 *)L_362) == ((Il2CppObject*)(OutPt_t3947633180 *)L_363)))
		{
			goto IL_0697;
		}
	}
	{
		OutPt_t3947633180 * L_364 = V_5;
		OutPt_t3947633180 * L_365 = V_4;
		if ((((Il2CppObject*)(OutPt_t3947633180 *)L_364) == ((Il2CppObject*)(OutPt_t3947633180 *)L_365)))
		{
			goto IL_0697;
		}
	}
	{
		OutPt_t3947633180 * L_366 = V_3;
		OutPt_t3947633180 * L_367 = V_5;
		if ((((Il2CppObject*)(OutPt_t3947633180 *)L_366) == ((Il2CppObject*)(OutPt_t3947633180 *)L_367)))
		{
			goto IL_0697;
		}
	}
	{
		OutRec_t3245805284 * L_368 = V_0;
		OutRec_t3245805284 * L_369 = V_1;
		if ((!(((Il2CppObject*)(OutRec_t3245805284 *)L_368) == ((Il2CppObject*)(OutRec_t3245805284 *)L_369))))
		{
			goto IL_0699;
		}
	}
	{
		bool L_370 = V_13;
		bool L_371 = V_14;
		if ((!(((uint32_t)L_370) == ((uint32_t)L_371))))
		{
			goto IL_0699;
		}
	}

IL_0697:
	{
		return (bool)0;
	}

IL_0699:
	{
		bool L_372 = V_13;
		if (!L_372)
		{
			goto IL_06dc;
		}
	}
	{
		OutPt_t3947633180 * L_373 = V_2;
		OutPt_t3947633180 * L_374 = Clipper_DupOutPt_m4193918363(__this, L_373, (bool)0, /*hidden argument*/NULL);
		V_3 = L_374;
		OutPt_t3947633180 * L_375 = V_4;
		OutPt_t3947633180 * L_376 = Clipper_DupOutPt_m4193918363(__this, L_375, (bool)1, /*hidden argument*/NULL);
		V_5 = L_376;
		OutPt_t3947633180 * L_377 = V_2;
		OutPt_t3947633180 * L_378 = V_4;
		NullCheck(L_377);
		L_377->set_Prev_3(L_378);
		OutPt_t3947633180 * L_379 = V_4;
		OutPt_t3947633180 * L_380 = V_2;
		NullCheck(L_379);
		L_379->set_Next_2(L_380);
		OutPt_t3947633180 * L_381 = V_3;
		OutPt_t3947633180 * L_382 = V_5;
		NullCheck(L_381);
		L_381->set_Next_2(L_382);
		OutPt_t3947633180 * L_383 = V_5;
		OutPt_t3947633180 * L_384 = V_3;
		NullCheck(L_383);
		L_383->set_Prev_3(L_384);
		OutPt_t3947633180 ** L_385 = ___p11;
		OutPt_t3947633180 * L_386 = V_2;
		*((Il2CppObject **)(L_385)) = (Il2CppObject *)L_386;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_385), (Il2CppObject *)L_386);
		OutPt_t3947633180 ** L_387 = ___p22;
		OutPt_t3947633180 * L_388 = V_3;
		*((Il2CppObject **)(L_387)) = (Il2CppObject *)L_388;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_387), (Il2CppObject *)L_388);
		return (bool)1;
	}

IL_06dc:
	{
		OutPt_t3947633180 * L_389 = V_2;
		OutPt_t3947633180 * L_390 = Clipper_DupOutPt_m4193918363(__this, L_389, (bool)1, /*hidden argument*/NULL);
		V_3 = L_390;
		OutPt_t3947633180 * L_391 = V_4;
		OutPt_t3947633180 * L_392 = Clipper_DupOutPt_m4193918363(__this, L_391, (bool)0, /*hidden argument*/NULL);
		V_5 = L_392;
		OutPt_t3947633180 * L_393 = V_2;
		OutPt_t3947633180 * L_394 = V_4;
		NullCheck(L_393);
		L_393->set_Next_2(L_394);
		OutPt_t3947633180 * L_395 = V_4;
		OutPt_t3947633180 * L_396 = V_2;
		NullCheck(L_395);
		L_395->set_Prev_3(L_396);
		OutPt_t3947633180 * L_397 = V_3;
		OutPt_t3947633180 * L_398 = V_5;
		NullCheck(L_397);
		L_397->set_Prev_3(L_398);
		OutPt_t3947633180 * L_399 = V_5;
		OutPt_t3947633180 * L_400 = V_3;
		NullCheck(L_399);
		L_399->set_Next_2(L_400);
		OutPt_t3947633180 ** L_401 = ___p11;
		OutPt_t3947633180 * L_402 = V_2;
		*((Il2CppObject **)(L_401)) = (Il2CppObject *)L_402;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_401), (Il2CppObject *)L_402);
		OutPt_t3947633180 ** L_403 = ___p22;
		OutPt_t3947633180 * L_404 = V_3;
		*((Il2CppObject **)(L_403)) = (Il2CppObject *)L_404;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_403), (Il2CppObject *)L_404);
		return (bool)1;
	}
}
// System.Boolean Pathfinding.ClipperLib.Clipper::Poly2ContainsPoly1(Pathfinding.ClipperLib.OutPt,Pathfinding.ClipperLib.OutPt,System.Boolean)
extern "C"  bool Clipper_Poly2ContainsPoly1_m2873516521 (Clipper_t636949239 * __this, OutPt_t3947633180 * ___outPt10, OutPt_t3947633180 * ___outPt21, bool ___UseFullRange2, const MethodInfo* method)
{
	OutPt_t3947633180 * V_0 = NULL;
	{
		OutPt_t3947633180 * L_0 = ___outPt10;
		V_0 = L_0;
		OutPt_t3947633180 * L_1 = V_0;
		NullCheck(L_1);
		IntPoint_t3326126179  L_2 = L_1->get_Pt_1();
		OutPt_t3947633180 * L_3 = ___outPt21;
		bool L_4 = ___UseFullRange2;
		bool L_5 = ClipperBase_PointOnPolygon_m903142260(__this, L_2, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		OutPt_t3947633180 * L_6 = V_0;
		NullCheck(L_6);
		OutPt_t3947633180 * L_7 = L_6->get_Next_2();
		V_0 = L_7;
		goto IL_0028;
	}

IL_0021:
	{
		OutPt_t3947633180 * L_8 = V_0;
		NullCheck(L_8);
		OutPt_t3947633180 * L_9 = L_8->get_Next_2();
		V_0 = L_9;
	}

IL_0028:
	{
		OutPt_t3947633180 * L_10 = V_0;
		OutPt_t3947633180 * L_11 = ___outPt10;
		if ((((Il2CppObject*)(OutPt_t3947633180 *)L_10) == ((Il2CppObject*)(OutPt_t3947633180 *)L_11)))
		{
			goto IL_0042;
		}
	}
	{
		OutPt_t3947633180 * L_12 = V_0;
		NullCheck(L_12);
		IntPoint_t3326126179  L_13 = L_12->get_Pt_1();
		OutPt_t3947633180 * L_14 = ___outPt21;
		bool L_15 = ___UseFullRange2;
		bool L_16 = ClipperBase_PointOnPolygon_m903142260(__this, L_13, L_14, L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_0021;
		}
	}

IL_0042:
	{
		OutPt_t3947633180 * L_17 = V_0;
		OutPt_t3947633180 * L_18 = ___outPt10;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_17) == ((Il2CppObject*)(OutPt_t3947633180 *)L_18))))
		{
			goto IL_004b;
		}
	}
	{
		return (bool)1;
	}

IL_004b:
	{
		OutPt_t3947633180 * L_19 = V_0;
		NullCheck(L_19);
		IntPoint_t3326126179  L_20 = L_19->get_Pt_1();
		OutPt_t3947633180 * L_21 = ___outPt21;
		bool L_22 = ___UseFullRange2;
		bool L_23 = ClipperBase_PointInPolygon_m2483899322(__this, L_20, L_21, L_22, /*hidden argument*/NULL);
		return L_23;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::FixupFirstLefts1(Pathfinding.ClipperLib.OutRec,Pathfinding.ClipperLib.OutRec)
extern const MethodInfo* List_1_get_Item_m2053160358_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m1367576335_MethodInfo_var;
extern const uint32_t Clipper_FixupFirstLefts1_m1187386151_MetadataUsageId;
extern "C"  void Clipper_FixupFirstLefts1_m1187386151 (Clipper_t636949239 * __this, OutRec_t3245805284 * ___OldOutRec0, OutRec_t3245805284 * ___NewOutRec1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_FixupFirstLefts1_m1187386151_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	OutRec_t3245805284 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0053;
	}

IL_0007:
	{
		List_1_t319023540 * L_0 = __this->get_m_PolyOuts_15();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		OutRec_t3245805284 * L_2 = List_1_get_Item_m2053160358(L_0, L_1, /*hidden argument*/List_1_get_Item_m2053160358_MethodInfo_var);
		V_1 = L_2;
		OutRec_t3245805284 * L_3 = V_1;
		NullCheck(L_3);
		OutPt_t3947633180 * L_4 = L_3->get_Pts_4();
		if (!L_4)
		{
			goto IL_004f;
		}
	}
	{
		OutRec_t3245805284 * L_5 = V_1;
		NullCheck(L_5);
		OutRec_t3245805284 * L_6 = L_5->get_FirstLeft_3();
		OutRec_t3245805284 * L_7 = ___OldOutRec0;
		if ((!(((Il2CppObject*)(OutRec_t3245805284 *)L_6) == ((Il2CppObject*)(OutRec_t3245805284 *)L_7))))
		{
			goto IL_004f;
		}
	}
	{
		OutRec_t3245805284 * L_8 = V_1;
		NullCheck(L_8);
		OutPt_t3947633180 * L_9 = L_8->get_Pts_4();
		OutRec_t3245805284 * L_10 = ___NewOutRec1;
		NullCheck(L_10);
		OutPt_t3947633180 * L_11 = L_10->get_Pts_4();
		bool L_12 = ((ClipperBase_t512275816 *)__this)->get_m_UseFullRange_9();
		bool L_13 = Clipper_Poly2ContainsPoly1_m2873516521(__this, L_9, L_11, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_004f;
		}
	}
	{
		OutRec_t3245805284 * L_14 = V_1;
		OutRec_t3245805284 * L_15 = ___NewOutRec1;
		NullCheck(L_14);
		L_14->set_FirstLeft_3(L_15);
	}

IL_004f:
	{
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0053:
	{
		int32_t L_17 = V_0;
		List_1_t319023540 * L_18 = __this->get_m_PolyOuts_15();
		NullCheck(L_18);
		int32_t L_19 = List_1_get_Count_m1367576335(L_18, /*hidden argument*/List_1_get_Count_m1367576335_MethodInfo_var);
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::FixupFirstLefts2(Pathfinding.ClipperLib.OutRec,Pathfinding.ClipperLib.OutRec)
extern const MethodInfo* List_1_GetEnumerator_m3170781084_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m972253188_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3537270924_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m360403802_MethodInfo_var;
extern const uint32_t Clipper_FixupFirstLefts2_m3674505926_MetadataUsageId;
extern "C"  void Clipper_FixupFirstLefts2_m3674505926 (Clipper_t636949239 * __this, OutRec_t3245805284 * ___OldOutRec0, OutRec_t3245805284 * ___NewOutRec1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_FixupFirstLefts2_m3674505926_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Enumerator_t338696310  V_0;
	memset(&V_0, 0, sizeof(V_0));
	OutRec_t3245805284 * V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t319023540 * L_0 = __this->get_m_PolyOuts_15();
		NullCheck(L_0);
		Enumerator_t338696310  L_1 = List_1_GetEnumerator_m3170781084(L_0, /*hidden argument*/List_1_GetEnumerator_m3170781084_MethodInfo_var);
		V_0 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002c;
		}

IL_0011:
		{
			OutRec_t3245805284 * L_2 = Enumerator_get_Current_m972253188((&V_0), /*hidden argument*/Enumerator_get_Current_m972253188_MethodInfo_var);
			V_1 = L_2;
			OutRec_t3245805284 * L_3 = V_1;
			NullCheck(L_3);
			OutRec_t3245805284 * L_4 = L_3->get_FirstLeft_3();
			OutRec_t3245805284 * L_5 = ___OldOutRec0;
			if ((!(((Il2CppObject*)(OutRec_t3245805284 *)L_4) == ((Il2CppObject*)(OutRec_t3245805284 *)L_5))))
			{
				goto IL_002c;
			}
		}

IL_0025:
		{
			OutRec_t3245805284 * L_6 = V_1;
			OutRec_t3245805284 * L_7 = ___NewOutRec1;
			NullCheck(L_6);
			L_6->set_FirstLeft_3(L_7);
		}

IL_002c:
		{
			bool L_8 = Enumerator_MoveNext_m3537270924((&V_0), /*hidden argument*/Enumerator_MoveNext_m3537270924_MethodInfo_var);
			if (L_8)
			{
				goto IL_0011;
			}
		}

IL_0038:
		{
			IL2CPP_LEAVE(0x4B, FINALLY_003d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_003d;
	}

FINALLY_003d:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m360403802((&V_0), /*hidden argument*/Enumerator_Dispose_m360403802_MethodInfo_var);
		IL2CPP_END_FINALLY(61)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(61)
	{
		IL2CPP_JUMP_TBL(0x4B, IL_004b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_004b:
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::JoinCommonEdges()
extern const MethodInfo* List_1_get_Item_m2818082526_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m896344279_MethodInfo_var;
extern const uint32_t Clipper_JoinCommonEdges_m1930505133_MetadataUsageId;
extern "C"  void Clipper_JoinCommonEdges_m1930505133 (Clipper_t636949239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_JoinCommonEdges_m1930505133_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Join_t3970117804 * V_1 = NULL;
	OutRec_t3245805284 * V_2 = NULL;
	OutRec_t3245805284 * V_3 = NULL;
	OutRec_t3245805284 * V_4 = NULL;
	OutPt_t3947633180 * V_5 = NULL;
	OutPt_t3947633180 * V_6 = NULL;
	{
		V_0 = 0;
		goto IL_026e;
	}

IL_0007:
	{
		List_1_t1043336060 * L_0 = __this->get_m_Joins_24();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Join_t3970117804 * L_2 = List_1_get_Item_m2818082526(L_0, L_1, /*hidden argument*/List_1_get_Item_m2818082526_MethodInfo_var);
		V_1 = L_2;
		Join_t3970117804 * L_3 = V_1;
		NullCheck(L_3);
		OutPt_t3947633180 * L_4 = L_3->get_OutPt1_0();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_Idx_0();
		OutRec_t3245805284 * L_6 = Clipper_GetOutRec_m918585808(__this, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		Join_t3970117804 * L_7 = V_1;
		NullCheck(L_7);
		OutPt_t3947633180 * L_8 = L_7->get_OutPt2_1();
		NullCheck(L_8);
		int32_t L_9 = L_8->get_Idx_0();
		OutRec_t3245805284 * L_10 = Clipper_GetOutRec_m918585808(__this, L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		OutRec_t3245805284 * L_11 = V_2;
		NullCheck(L_11);
		OutPt_t3947633180 * L_12 = L_11->get_Pts_4();
		if (!L_12)
		{
			goto IL_004e;
		}
	}
	{
		OutRec_t3245805284 * L_13 = V_3;
		NullCheck(L_13);
		OutPt_t3947633180 * L_14 = L_13->get_Pts_4();
		if (L_14)
		{
			goto IL_0053;
		}
	}

IL_004e:
	{
		goto IL_026a;
	}

IL_0053:
	{
		OutRec_t3245805284 * L_15 = V_2;
		OutRec_t3245805284 * L_16 = V_3;
		if ((!(((Il2CppObject*)(OutRec_t3245805284 *)L_15) == ((Il2CppObject*)(OutRec_t3245805284 *)L_16))))
		{
			goto IL_0062;
		}
	}
	{
		OutRec_t3245805284 * L_17 = V_2;
		V_4 = L_17;
		goto IL_0096;
	}

IL_0062:
	{
		OutRec_t3245805284 * L_18 = V_2;
		OutRec_t3245805284 * L_19 = V_3;
		bool L_20 = Clipper_Param1RightOfParam2_m667347646(__this, L_18, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0077;
		}
	}
	{
		OutRec_t3245805284 * L_21 = V_3;
		V_4 = L_21;
		goto IL_0096;
	}

IL_0077:
	{
		OutRec_t3245805284 * L_22 = V_3;
		OutRec_t3245805284 * L_23 = V_2;
		bool L_24 = Clipper_Param1RightOfParam2_m667347646(__this, L_22, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_008c;
		}
	}
	{
		OutRec_t3245805284 * L_25 = V_2;
		V_4 = L_25;
		goto IL_0096;
	}

IL_008c:
	{
		OutRec_t3245805284 * L_26 = V_2;
		OutRec_t3245805284 * L_27 = V_3;
		OutRec_t3245805284 * L_28 = Clipper_GetLowermostRec_m3541598937(__this, L_26, L_27, /*hidden argument*/NULL);
		V_4 = L_28;
	}

IL_0096:
	{
		Join_t3970117804 * L_29 = V_1;
		bool L_30 = Clipper_JoinPoints_m49902773(__this, L_29, (&V_5), (&V_6), /*hidden argument*/NULL);
		if (L_30)
		{
			goto IL_00ab;
		}
	}
	{
		goto IL_026a;
	}

IL_00ab:
	{
		OutRec_t3245805284 * L_31 = V_2;
		OutRec_t3245805284 * L_32 = V_3;
		if ((!(((Il2CppObject*)(OutRec_t3245805284 *)L_31) == ((Il2CppObject*)(OutRec_t3245805284 *)L_32))))
		{
			goto IL_0215;
		}
	}
	{
		OutRec_t3245805284 * L_33 = V_2;
		OutPt_t3947633180 * L_34 = V_5;
		NullCheck(L_33);
		L_33->set_Pts_4(L_34);
		OutRec_t3245805284 * L_35 = V_2;
		NullCheck(L_35);
		L_35->set_BottomPt_5((OutPt_t3947633180 *)NULL);
		OutRec_t3245805284 * L_36 = Clipper_CreateOutRec_m1755291545(__this, /*hidden argument*/NULL);
		V_3 = L_36;
		OutRec_t3245805284 * L_37 = V_3;
		OutPt_t3947633180 * L_38 = V_6;
		NullCheck(L_37);
		L_37->set_Pts_4(L_38);
		OutRec_t3245805284 * L_39 = V_3;
		Clipper_UpdateOutPtIdxs_m3611366233(__this, L_39, /*hidden argument*/NULL);
		OutRec_t3245805284 * L_40 = V_3;
		NullCheck(L_40);
		OutPt_t3947633180 * L_41 = L_40->get_Pts_4();
		OutRec_t3245805284 * L_42 = V_2;
		NullCheck(L_42);
		OutPt_t3947633180 * L_43 = L_42->get_Pts_4();
		bool L_44 = ((ClipperBase_t512275816 *)__this)->get_m_UseFullRange_9();
		bool L_45 = Clipper_Poly2ContainsPoly1_m2873516521(__this, L_41, L_43, L_44, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_0152;
		}
	}
	{
		OutRec_t3245805284 * L_46 = V_3;
		OutRec_t3245805284 * L_47 = V_2;
		NullCheck(L_47);
		bool L_48 = L_47->get_IsHole_1();
		NullCheck(L_46);
		L_46->set_IsHole_1((bool)((((int32_t)L_48) == ((int32_t)0))? 1 : 0));
		OutRec_t3245805284 * L_49 = V_3;
		OutRec_t3245805284 * L_50 = V_2;
		NullCheck(L_49);
		L_49->set_FirstLeft_3(L_50);
		bool L_51 = __this->get_m_UsingPolyTree_26();
		if (!L_51)
		{
			goto IL_011d;
		}
	}
	{
		OutRec_t3245805284 * L_52 = V_3;
		OutRec_t3245805284 * L_53 = V_2;
		Clipper_FixupFirstLefts2_m3674505926(__this, L_52, L_53, /*hidden argument*/NULL);
	}

IL_011d:
	{
		OutRec_t3245805284 * L_54 = V_3;
		NullCheck(L_54);
		bool L_55 = L_54->get_IsHole_1();
		bool L_56 = Clipper_get_ReverseSolution_m2875557634(__this, /*hidden argument*/NULL);
		OutRec_t3245805284 * L_57 = V_3;
		double L_58 = Clipper_Area_m2380044382(__this, L_57, /*hidden argument*/NULL);
		if ((!(((uint32_t)((int32_t)((int32_t)L_55^(int32_t)L_56))) == ((uint32_t)((((double)L_58) > ((double)(0.0)))? 1 : 0)))))
		{
			goto IL_014d;
		}
	}
	{
		OutRec_t3245805284 * L_59 = V_3;
		NullCheck(L_59);
		OutPt_t3947633180 * L_60 = L_59->get_Pts_4();
		Clipper_ReversePolyPtLinks_m59152439(__this, L_60, /*hidden argument*/NULL);
	}

IL_014d:
	{
		goto IL_0210;
	}

IL_0152:
	{
		OutRec_t3245805284 * L_61 = V_2;
		NullCheck(L_61);
		OutPt_t3947633180 * L_62 = L_61->get_Pts_4();
		OutRec_t3245805284 * L_63 = V_3;
		NullCheck(L_63);
		OutPt_t3947633180 * L_64 = L_63->get_Pts_4();
		bool L_65 = ((ClipperBase_t512275816 *)__this)->get_m_UseFullRange_9();
		bool L_66 = Clipper_Poly2ContainsPoly1_m2873516521(__this, L_62, L_64, L_65, /*hidden argument*/NULL);
		if (!L_66)
		{
			goto IL_01e5;
		}
	}
	{
		OutRec_t3245805284 * L_67 = V_3;
		OutRec_t3245805284 * L_68 = V_2;
		NullCheck(L_68);
		bool L_69 = L_68->get_IsHole_1();
		NullCheck(L_67);
		L_67->set_IsHole_1(L_69);
		OutRec_t3245805284 * L_70 = V_2;
		OutRec_t3245805284 * L_71 = V_3;
		NullCheck(L_71);
		bool L_72 = L_71->get_IsHole_1();
		NullCheck(L_70);
		L_70->set_IsHole_1((bool)((((int32_t)L_72) == ((int32_t)0))? 1 : 0));
		OutRec_t3245805284 * L_73 = V_3;
		OutRec_t3245805284 * L_74 = V_2;
		NullCheck(L_74);
		OutRec_t3245805284 * L_75 = L_74->get_FirstLeft_3();
		NullCheck(L_73);
		L_73->set_FirstLeft_3(L_75);
		OutRec_t3245805284 * L_76 = V_2;
		OutRec_t3245805284 * L_77 = V_3;
		NullCheck(L_76);
		L_76->set_FirstLeft_3(L_77);
		bool L_78 = __this->get_m_UsingPolyTree_26();
		if (!L_78)
		{
			goto IL_01b0;
		}
	}
	{
		OutRec_t3245805284 * L_79 = V_2;
		OutRec_t3245805284 * L_80 = V_3;
		Clipper_FixupFirstLefts2_m3674505926(__this, L_79, L_80, /*hidden argument*/NULL);
	}

IL_01b0:
	{
		OutRec_t3245805284 * L_81 = V_2;
		NullCheck(L_81);
		bool L_82 = L_81->get_IsHole_1();
		bool L_83 = Clipper_get_ReverseSolution_m2875557634(__this, /*hidden argument*/NULL);
		OutRec_t3245805284 * L_84 = V_2;
		double L_85 = Clipper_Area_m2380044382(__this, L_84, /*hidden argument*/NULL);
		if ((!(((uint32_t)((int32_t)((int32_t)L_82^(int32_t)L_83))) == ((uint32_t)((((double)L_85) > ((double)(0.0)))? 1 : 0)))))
		{
			goto IL_01e0;
		}
	}
	{
		OutRec_t3245805284 * L_86 = V_2;
		NullCheck(L_86);
		OutPt_t3947633180 * L_87 = L_86->get_Pts_4();
		Clipper_ReversePolyPtLinks_m59152439(__this, L_87, /*hidden argument*/NULL);
	}

IL_01e0:
	{
		goto IL_0210;
	}

IL_01e5:
	{
		OutRec_t3245805284 * L_88 = V_3;
		OutRec_t3245805284 * L_89 = V_2;
		NullCheck(L_89);
		bool L_90 = L_89->get_IsHole_1();
		NullCheck(L_88);
		L_88->set_IsHole_1(L_90);
		OutRec_t3245805284 * L_91 = V_3;
		OutRec_t3245805284 * L_92 = V_2;
		NullCheck(L_92);
		OutRec_t3245805284 * L_93 = L_92->get_FirstLeft_3();
		NullCheck(L_91);
		L_91->set_FirstLeft_3(L_93);
		bool L_94 = __this->get_m_UsingPolyTree_26();
		if (!L_94)
		{
			goto IL_0210;
		}
	}
	{
		OutRec_t3245805284 * L_95 = V_2;
		OutRec_t3245805284 * L_96 = V_3;
		Clipper_FixupFirstLefts1_m1187386151(__this, L_95, L_96, /*hidden argument*/NULL);
	}

IL_0210:
	{
		goto IL_026a;
	}

IL_0215:
	{
		OutRec_t3245805284 * L_97 = V_3;
		NullCheck(L_97);
		L_97->set_Pts_4((OutPt_t3947633180 *)NULL);
		OutRec_t3245805284 * L_98 = V_3;
		NullCheck(L_98);
		L_98->set_BottomPt_5((OutPt_t3947633180 *)NULL);
		OutRec_t3245805284 * L_99 = V_3;
		OutRec_t3245805284 * L_100 = V_2;
		NullCheck(L_100);
		int32_t L_101 = L_100->get_Idx_0();
		NullCheck(L_99);
		L_99->set_Idx_0(L_101);
		OutRec_t3245805284 * L_102 = V_2;
		OutRec_t3245805284 * L_103 = V_4;
		NullCheck(L_103);
		bool L_104 = L_103->get_IsHole_1();
		NullCheck(L_102);
		L_102->set_IsHole_1(L_104);
		OutRec_t3245805284 * L_105 = V_4;
		OutRec_t3245805284 * L_106 = V_3;
		if ((!(((Il2CppObject*)(OutRec_t3245805284 *)L_105) == ((Il2CppObject*)(OutRec_t3245805284 *)L_106))))
		{
			goto IL_0250;
		}
	}
	{
		OutRec_t3245805284 * L_107 = V_2;
		OutRec_t3245805284 * L_108 = V_3;
		NullCheck(L_108);
		OutRec_t3245805284 * L_109 = L_108->get_FirstLeft_3();
		NullCheck(L_107);
		L_107->set_FirstLeft_3(L_109);
	}

IL_0250:
	{
		OutRec_t3245805284 * L_110 = V_3;
		OutRec_t3245805284 * L_111 = V_2;
		NullCheck(L_110);
		L_110->set_FirstLeft_3(L_111);
		bool L_112 = __this->get_m_UsingPolyTree_26();
		if (!L_112)
		{
			goto IL_026a;
		}
	}
	{
		OutRec_t3245805284 * L_113 = V_3;
		OutRec_t3245805284 * L_114 = V_2;
		Clipper_FixupFirstLefts2_m3674505926(__this, L_113, L_114, /*hidden argument*/NULL);
	}

IL_026a:
	{
		int32_t L_115 = V_0;
		V_0 = ((int32_t)((int32_t)L_115+(int32_t)1));
	}

IL_026e:
	{
		int32_t L_116 = V_0;
		List_1_t1043336060 * L_117 = __this->get_m_Joins_24();
		NullCheck(L_117);
		int32_t L_118 = List_1_get_Count_m896344279(L_117, /*hidden argument*/List_1_get_Count_m896344279_MethodInfo_var);
		if ((((int32_t)L_116) < ((int32_t)L_118)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::UpdateOutPtIdxs(Pathfinding.ClipperLib.OutRec)
extern "C"  void Clipper_UpdateOutPtIdxs_m3611366233 (Clipper_t636949239 * __this, OutRec_t3245805284 * ___outrec0, const MethodInfo* method)
{
	OutPt_t3947633180 * V_0 = NULL;
	{
		OutRec_t3245805284 * L_0 = ___outrec0;
		NullCheck(L_0);
		OutPt_t3947633180 * L_1 = L_0->get_Pts_4();
		V_0 = L_1;
	}

IL_0007:
	{
		OutPt_t3947633180 * L_2 = V_0;
		OutRec_t3245805284 * L_3 = ___outrec0;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_Idx_0();
		NullCheck(L_2);
		L_2->set_Idx_0(L_4);
		OutPt_t3947633180 * L_5 = V_0;
		NullCheck(L_5);
		OutPt_t3947633180 * L_6 = L_5->get_Prev_3();
		V_0 = L_6;
		OutPt_t3947633180 * L_7 = V_0;
		OutRec_t3245805284 * L_8 = ___outrec0;
		NullCheck(L_8);
		OutPt_t3947633180 * L_9 = L_8->get_Pts_4();
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_7) == ((Il2CppObject*)(OutPt_t3947633180 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Clipper::DoSimplePolygons()
extern const MethodInfo* List_1_get_Item_m2053160358_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m1367576335_MethodInfo_var;
extern const uint32_t Clipper_DoSimplePolygons_m2818696524_MetadataUsageId;
extern "C"  void Clipper_DoSimplePolygons_m2818696524 (Clipper_t636949239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_DoSimplePolygons_m2818696524_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	OutRec_t3245805284 * V_1 = NULL;
	OutPt_t3947633180 * V_2 = NULL;
	OutPt_t3947633180 * V_3 = NULL;
	OutPt_t3947633180 * V_4 = NULL;
	OutPt_t3947633180 * V_5 = NULL;
	OutRec_t3245805284 * V_6 = NULL;
	{
		V_0 = 0;
		goto IL_0185;
	}

IL_0007:
	{
		List_1_t319023540 * L_0 = __this->get_m_PolyOuts_15();
		int32_t L_1 = V_0;
		int32_t L_2 = L_1;
		V_0 = ((int32_t)((int32_t)L_2+(int32_t)1));
		NullCheck(L_0);
		OutRec_t3245805284 * L_3 = List_1_get_Item_m2053160358(L_0, L_2, /*hidden argument*/List_1_get_Item_m2053160358_MethodInfo_var);
		V_1 = L_3;
		OutRec_t3245805284 * L_4 = V_1;
		NullCheck(L_4);
		OutPt_t3947633180 * L_5 = L_4->get_Pts_4();
		V_2 = L_5;
		OutPt_t3947633180 * L_6 = V_2;
		if (L_6)
		{
			goto IL_002a;
		}
	}
	{
		goto IL_0185;
	}

IL_002a:
	{
		OutPt_t3947633180 * L_7 = V_2;
		NullCheck(L_7);
		OutPt_t3947633180 * L_8 = L_7->get_Next_2();
		V_3 = L_8;
		goto IL_0166;
	}

IL_0036:
	{
		OutPt_t3947633180 * L_9 = V_2;
		NullCheck(L_9);
		IntPoint_t3326126179  L_10 = L_9->get_Pt_1();
		OutPt_t3947633180 * L_11 = V_3;
		NullCheck(L_11);
		IntPoint_t3326126179  L_12 = L_11->get_Pt_1();
		bool L_13 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_015f;
		}
	}
	{
		OutPt_t3947633180 * L_14 = V_3;
		NullCheck(L_14);
		OutPt_t3947633180 * L_15 = L_14->get_Next_2();
		OutPt_t3947633180 * L_16 = V_2;
		if ((((Il2CppObject*)(OutPt_t3947633180 *)L_15) == ((Il2CppObject*)(OutPt_t3947633180 *)L_16)))
		{
			goto IL_015f;
		}
	}
	{
		OutPt_t3947633180 * L_17 = V_3;
		NullCheck(L_17);
		OutPt_t3947633180 * L_18 = L_17->get_Prev_3();
		OutPt_t3947633180 * L_19 = V_2;
		if ((((Il2CppObject*)(OutPt_t3947633180 *)L_18) == ((Il2CppObject*)(OutPt_t3947633180 *)L_19)))
		{
			goto IL_015f;
		}
	}
	{
		OutPt_t3947633180 * L_20 = V_2;
		NullCheck(L_20);
		OutPt_t3947633180 * L_21 = L_20->get_Prev_3();
		V_4 = L_21;
		OutPt_t3947633180 * L_22 = V_3;
		NullCheck(L_22);
		OutPt_t3947633180 * L_23 = L_22->get_Prev_3();
		V_5 = L_23;
		OutPt_t3947633180 * L_24 = V_2;
		OutPt_t3947633180 * L_25 = V_5;
		NullCheck(L_24);
		L_24->set_Prev_3(L_25);
		OutPt_t3947633180 * L_26 = V_5;
		OutPt_t3947633180 * L_27 = V_2;
		NullCheck(L_26);
		L_26->set_Next_2(L_27);
		OutPt_t3947633180 * L_28 = V_3;
		OutPt_t3947633180 * L_29 = V_4;
		NullCheck(L_28);
		L_28->set_Prev_3(L_29);
		OutPt_t3947633180 * L_30 = V_4;
		OutPt_t3947633180 * L_31 = V_3;
		NullCheck(L_30);
		L_30->set_Next_2(L_31);
		OutRec_t3245805284 * L_32 = V_1;
		OutPt_t3947633180 * L_33 = V_2;
		NullCheck(L_32);
		L_32->set_Pts_4(L_33);
		OutRec_t3245805284 * L_34 = Clipper_CreateOutRec_m1755291545(__this, /*hidden argument*/NULL);
		V_6 = L_34;
		OutRec_t3245805284 * L_35 = V_6;
		OutPt_t3947633180 * L_36 = V_3;
		NullCheck(L_35);
		L_35->set_Pts_4(L_36);
		OutRec_t3245805284 * L_37 = V_6;
		Clipper_UpdateOutPtIdxs_m3611366233(__this, L_37, /*hidden argument*/NULL);
		OutRec_t3245805284 * L_38 = V_6;
		NullCheck(L_38);
		OutPt_t3947633180 * L_39 = L_38->get_Pts_4();
		OutRec_t3245805284 * L_40 = V_1;
		NullCheck(L_40);
		OutPt_t3947633180 * L_41 = L_40->get_Pts_4();
		bool L_42 = ((ClipperBase_t512275816 *)__this)->get_m_UseFullRange_9();
		bool L_43 = Clipper_Poly2ContainsPoly1_m2873516521(__this, L_39, L_41, L_42, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_00ee;
		}
	}
	{
		OutRec_t3245805284 * L_44 = V_6;
		OutRec_t3245805284 * L_45 = V_1;
		NullCheck(L_45);
		bool L_46 = L_45->get_IsHole_1();
		NullCheck(L_44);
		L_44->set_IsHole_1((bool)((((int32_t)L_46) == ((int32_t)0))? 1 : 0));
		OutRec_t3245805284 * L_47 = V_6;
		OutRec_t3245805284 * L_48 = V_1;
		NullCheck(L_47);
		L_47->set_FirstLeft_3(L_48);
		goto IL_015d;
	}

IL_00ee:
	{
		OutRec_t3245805284 * L_49 = V_1;
		NullCheck(L_49);
		OutPt_t3947633180 * L_50 = L_49->get_Pts_4();
		OutRec_t3245805284 * L_51 = V_6;
		NullCheck(L_51);
		OutPt_t3947633180 * L_52 = L_51->get_Pts_4();
		bool L_53 = ((ClipperBase_t512275816 *)__this)->get_m_UseFullRange_9();
		bool L_54 = Clipper_Poly2ContainsPoly1_m2873516521(__this, L_50, L_52, L_53, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_0143;
		}
	}
	{
		OutRec_t3245805284 * L_55 = V_6;
		OutRec_t3245805284 * L_56 = V_1;
		NullCheck(L_56);
		bool L_57 = L_56->get_IsHole_1();
		NullCheck(L_55);
		L_55->set_IsHole_1(L_57);
		OutRec_t3245805284 * L_58 = V_1;
		OutRec_t3245805284 * L_59 = V_6;
		NullCheck(L_59);
		bool L_60 = L_59->get_IsHole_1();
		NullCheck(L_58);
		L_58->set_IsHole_1((bool)((((int32_t)L_60) == ((int32_t)0))? 1 : 0));
		OutRec_t3245805284 * L_61 = V_6;
		OutRec_t3245805284 * L_62 = V_1;
		NullCheck(L_62);
		OutRec_t3245805284 * L_63 = L_62->get_FirstLeft_3();
		NullCheck(L_61);
		L_61->set_FirstLeft_3(L_63);
		OutRec_t3245805284 * L_64 = V_1;
		OutRec_t3245805284 * L_65 = V_6;
		NullCheck(L_64);
		L_64->set_FirstLeft_3(L_65);
		goto IL_015d;
	}

IL_0143:
	{
		OutRec_t3245805284 * L_66 = V_6;
		OutRec_t3245805284 * L_67 = V_1;
		NullCheck(L_67);
		bool L_68 = L_67->get_IsHole_1();
		NullCheck(L_66);
		L_66->set_IsHole_1(L_68);
		OutRec_t3245805284 * L_69 = V_6;
		OutRec_t3245805284 * L_70 = V_1;
		NullCheck(L_70);
		OutRec_t3245805284 * L_71 = L_70->get_FirstLeft_3();
		NullCheck(L_69);
		L_69->set_FirstLeft_3(L_71);
	}

IL_015d:
	{
		OutPt_t3947633180 * L_72 = V_2;
		V_3 = L_72;
	}

IL_015f:
	{
		OutPt_t3947633180 * L_73 = V_3;
		NullCheck(L_73);
		OutPt_t3947633180 * L_74 = L_73->get_Next_2();
		V_3 = L_74;
	}

IL_0166:
	{
		OutPt_t3947633180 * L_75 = V_3;
		OutRec_t3245805284 * L_76 = V_1;
		NullCheck(L_76);
		OutPt_t3947633180 * L_77 = L_76->get_Pts_4();
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_75) == ((Il2CppObject*)(OutPt_t3947633180 *)L_77))))
		{
			goto IL_0036;
		}
	}
	{
		OutPt_t3947633180 * L_78 = V_2;
		NullCheck(L_78);
		OutPt_t3947633180 * L_79 = L_78->get_Next_2();
		V_2 = L_79;
		OutPt_t3947633180 * L_80 = V_2;
		OutRec_t3245805284 * L_81 = V_1;
		NullCheck(L_81);
		OutPt_t3947633180 * L_82 = L_81->get_Pts_4();
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_80) == ((Il2CppObject*)(OutPt_t3947633180 *)L_82))))
		{
			goto IL_002a;
		}
	}

IL_0185:
	{
		int32_t L_83 = V_0;
		List_1_t319023540 * L_84 = __this->get_m_PolyOuts_15();
		NullCheck(L_84);
		int32_t L_85 = List_1_get_Count_m1367576335(L_84, /*hidden argument*/List_1_get_Count_m1367576335_MethodInfo_var);
		if ((((int32_t)L_83) < ((int32_t)L_85)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Double Pathfinding.ClipperLib.Clipper::Area(System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>)
extern const MethodInfo* List_1_get_Count_m647655502_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m788539143_MethodInfo_var;
extern const uint32_t Clipper_Area_m791992023_MetadataUsageId;
extern "C"  double Clipper_Area_m791992023 (Il2CppObject * __this /* static, unused */, List_1_t399344435 * ___poly0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Clipper_Area_m791992023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	double V_1 = 0.0;
	IntPoint_t3326126179  V_2;
	memset(&V_2, 0, sizeof(V_2));
	IntPoint_t3326126179  V_3;
	memset(&V_3, 0, sizeof(V_3));
	IntPoint_t3326126179  V_4;
	memset(&V_4, 0, sizeof(V_4));
	IntPoint_t3326126179  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t V_6 = 0;
	IntPoint_t3326126179  V_7;
	memset(&V_7, 0, sizeof(V_7));
	IntPoint_t3326126179  V_8;
	memset(&V_8, 0, sizeof(V_8));
	IntPoint_t3326126179  V_9;
	memset(&V_9, 0, sizeof(V_9));
	IntPoint_t3326126179  V_10;
	memset(&V_10, 0, sizeof(V_10));
	{
		List_1_t399344435 * L_0 = ___poly0;
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m647655502(L_0, /*hidden argument*/List_1_get_Count_m647655502_MethodInfo_var);
		V_0 = ((int32_t)((int32_t)L_1-(int32_t)1));
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)2)))
		{
			goto IL_001a;
		}
	}
	{
		return (0.0);
	}

IL_001a:
	{
		List_1_t399344435 * L_3 = ___poly0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IntPoint_t3326126179  L_5 = List_1_get_Item_m788539143(L_3, L_4, /*hidden argument*/List_1_get_Item_m788539143_MethodInfo_var);
		V_2 = L_5;
		int64_t L_6 = (&V_2)->get_X_0();
		List_1_t399344435 * L_7 = ___poly0;
		NullCheck(L_7);
		IntPoint_t3326126179  L_8 = List_1_get_Item_m788539143(L_7, 0, /*hidden argument*/List_1_get_Item_m788539143_MethodInfo_var);
		V_3 = L_8;
		int64_t L_9 = (&V_3)->get_X_0();
		List_1_t399344435 * L_10 = ___poly0;
		NullCheck(L_10);
		IntPoint_t3326126179  L_11 = List_1_get_Item_m788539143(L_10, 0, /*hidden argument*/List_1_get_Item_m788539143_MethodInfo_var);
		V_4 = L_11;
		int64_t L_12 = (&V_4)->get_Y_1();
		List_1_t399344435 * L_13 = ___poly0;
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IntPoint_t3326126179  L_15 = List_1_get_Item_m788539143(L_13, L_14, /*hidden argument*/List_1_get_Item_m788539143_MethodInfo_var);
		V_5 = L_15;
		int64_t L_16 = (&V_5)->get_Y_1();
		V_1 = ((double)((double)((double)((double)(((double)((double)L_6)))+(double)(((double)((double)L_9)))))*(double)((double)((double)(((double)((double)L_12)))-(double)(((double)((double)L_16)))))));
		V_6 = 1;
		goto IL_00c0;
	}

IL_0068:
	{
		double L_17 = V_1;
		List_1_t399344435 * L_18 = ___poly0;
		int32_t L_19 = V_6;
		NullCheck(L_18);
		IntPoint_t3326126179  L_20 = List_1_get_Item_m788539143(L_18, ((int32_t)((int32_t)L_19-(int32_t)1)), /*hidden argument*/List_1_get_Item_m788539143_MethodInfo_var);
		V_7 = L_20;
		int64_t L_21 = (&V_7)->get_X_0();
		List_1_t399344435 * L_22 = ___poly0;
		int32_t L_23 = V_6;
		NullCheck(L_22);
		IntPoint_t3326126179  L_24 = List_1_get_Item_m788539143(L_22, L_23, /*hidden argument*/List_1_get_Item_m788539143_MethodInfo_var);
		V_8 = L_24;
		int64_t L_25 = (&V_8)->get_X_0();
		List_1_t399344435 * L_26 = ___poly0;
		int32_t L_27 = V_6;
		NullCheck(L_26);
		IntPoint_t3326126179  L_28 = List_1_get_Item_m788539143(L_26, L_27, /*hidden argument*/List_1_get_Item_m788539143_MethodInfo_var);
		V_9 = L_28;
		int64_t L_29 = (&V_9)->get_Y_1();
		List_1_t399344435 * L_30 = ___poly0;
		int32_t L_31 = V_6;
		NullCheck(L_30);
		IntPoint_t3326126179  L_32 = List_1_get_Item_m788539143(L_30, ((int32_t)((int32_t)L_31-(int32_t)1)), /*hidden argument*/List_1_get_Item_m788539143_MethodInfo_var);
		V_10 = L_32;
		int64_t L_33 = (&V_10)->get_Y_1();
		V_1 = ((double)((double)L_17+(double)((double)((double)((double)((double)(((double)((double)L_21)))+(double)(((double)((double)L_25)))))*(double)((double)((double)(((double)((double)L_29)))-(double)(((double)((double)L_33)))))))));
		int32_t L_34 = V_6;
		V_6 = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_00c0:
	{
		int32_t L_35 = V_6;
		int32_t L_36 = V_0;
		if ((((int32_t)L_35) <= ((int32_t)L_36)))
		{
			goto IL_0068;
		}
	}
	{
		double L_37 = V_1;
		return ((double)((double)L_37/(double)(2.0)));
	}
}
// System.Double Pathfinding.ClipperLib.Clipper::Area(Pathfinding.ClipperLib.OutRec)
extern "C"  double Clipper_Area_m2380044382 (Clipper_t636949239 * __this, OutRec_t3245805284 * ___outRec0, const MethodInfo* method)
{
	OutPt_t3947633180 * V_0 = NULL;
	double V_1 = 0.0;
	{
		OutRec_t3245805284 * L_0 = ___outRec0;
		NullCheck(L_0);
		OutPt_t3947633180 * L_1 = L_0->get_Pts_4();
		V_0 = L_1;
		OutPt_t3947633180 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		return (0.0);
	}

IL_0017:
	{
		V_1 = (0.0);
	}

IL_0021:
	{
		double L_3 = V_1;
		OutPt_t3947633180 * L_4 = V_0;
		NullCheck(L_4);
		IntPoint_t3326126179 * L_5 = L_4->get_address_of_Pt_1();
		int64_t L_6 = L_5->get_X_0();
		OutPt_t3947633180 * L_7 = V_0;
		NullCheck(L_7);
		OutPt_t3947633180 * L_8 = L_7->get_Prev_3();
		NullCheck(L_8);
		IntPoint_t3326126179 * L_9 = L_8->get_address_of_Pt_1();
		int64_t L_10 = L_9->get_X_0();
		OutPt_t3947633180 * L_11 = V_0;
		NullCheck(L_11);
		OutPt_t3947633180 * L_12 = L_11->get_Prev_3();
		NullCheck(L_12);
		IntPoint_t3326126179 * L_13 = L_12->get_address_of_Pt_1();
		int64_t L_14 = L_13->get_Y_1();
		OutPt_t3947633180 * L_15 = V_0;
		NullCheck(L_15);
		IntPoint_t3326126179 * L_16 = L_15->get_address_of_Pt_1();
		int64_t L_17 = L_16->get_Y_1();
		V_1 = ((double)((double)L_3+(double)((double)((double)(((double)((double)((int64_t)((int64_t)L_6+(int64_t)L_10)))))*(double)(((double)((double)((int64_t)((int64_t)L_14-(int64_t)L_17)))))))));
		OutPt_t3947633180 * L_18 = V_0;
		NullCheck(L_18);
		OutPt_t3947633180 * L_19 = L_18->get_Next_2();
		V_0 = L_19;
		OutPt_t3947633180 * L_20 = V_0;
		OutRec_t3245805284 * L_21 = ___outRec0;
		NullCheck(L_21);
		OutPt_t3947633180 * L_22 = L_21->get_Pts_4();
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_20) == ((Il2CppObject*)(OutPt_t3947633180 *)L_22))))
		{
			goto IL_0021;
		}
	}
	{
		double L_23 = V_1;
		return ((double)((double)L_23/(double)(2.0)));
	}
}
// System.Void Pathfinding.ClipperLib.ClipperBase::.ctor()
extern Il2CppClass* List_1_t2392209947_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3675264750_MethodInfo_var;
extern const uint32_t ClipperBase__ctor_m848649117_MetadataUsageId;
extern "C"  void ClipperBase__ctor_m848649117 (ClipperBase_t512275816 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClipperBase__ctor_m848649117_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2392209947 * L_0 = (List_1_t2392209947 *)il2cpp_codegen_object_new(List_1_t2392209947_il2cpp_TypeInfo_var);
		List_1__ctor_m3675264750(L_0, /*hidden argument*/List_1__ctor_m3675264750_MethodInfo_var);
		__this->set_m_edges_8(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		__this->set_m_MinimaList_6((LocalMinima_t2863342752 *)NULL);
		__this->set_m_CurrentLM_7((LocalMinima_t2863342752 *)NULL);
		__this->set_m_UseFullRange_9((bool)0);
		__this->set_m_HasOpenPaths_10((bool)0);
		return;
	}
}
// System.Boolean Pathfinding.ClipperLib.ClipperBase::get_PreserveCollinear()
extern "C"  bool ClipperBase_get_PreserveCollinear_m701993455 (ClipperBase_t512275816 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CPreserveCollinearU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void Pathfinding.ClipperLib.ClipperBase::set_PreserveCollinear(System.Boolean)
extern "C"  void ClipperBase_set_PreserveCollinear_m2366595086 (ClipperBase_t512275816 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CPreserveCollinearU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Boolean Pathfinding.ClipperLib.ClipperBase::IsHorizontal(Pathfinding.ClipperLib.TEdge)
extern "C"  bool ClipperBase_IsHorizontal_m1610406924 (Il2CppObject * __this /* static, unused */, TEdge_t3950806139 * ___e0, const MethodInfo* method)
{
	{
		TEdge_t3950806139 * L_0 = ___e0;
		NullCheck(L_0);
		IntPoint_t3326126179 * L_1 = L_0->get_address_of_Delta_3();
		int64_t L_2 = L_1->get_Y_1();
		return (bool)((((int64_t)L_2) == ((int64_t)(((int64_t)((int64_t)0)))))? 1 : 0);
	}
}
// System.Boolean Pathfinding.ClipperLib.ClipperBase::PointOnLineSegment(Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint,System.Boolean)
extern "C"  bool ClipperBase_PointOnLineSegment_m3036412045 (ClipperBase_t512275816 * __this, IntPoint_t3326126179  ___pt0, IntPoint_t3326126179  ___linePt11, IntPoint_t3326126179  ___linePt22, bool ___UseFullRange3, const MethodInfo* method)
{
	int32_t G_B9_0 = 0;
	int32_t G_B11_0 = 0;
	int32_t G_B20_0 = 0;
	int32_t G_B22_0 = 0;
	{
		bool L_0 = ___UseFullRange3;
		if (!L_0)
		{
			goto IL_00ef;
		}
	}
	{
		int64_t L_1 = (&___pt0)->get_X_0();
		int64_t L_2 = (&___linePt11)->get_X_0();
		if ((!(((uint64_t)L_1) == ((uint64_t)L_2))))
		{
			goto IL_002d;
		}
	}
	{
		int64_t L_3 = (&___pt0)->get_Y_1();
		int64_t L_4 = (&___linePt11)->get_Y_1();
		if ((((int64_t)L_3) == ((int64_t)L_4)))
		{
			goto IL_00ed;
		}
	}

IL_002d:
	{
		int64_t L_5 = (&___pt0)->get_X_0();
		int64_t L_6 = (&___linePt22)->get_X_0();
		if ((!(((uint64_t)L_5) == ((uint64_t)L_6))))
		{
			goto IL_0053;
		}
	}
	{
		int64_t L_7 = (&___pt0)->get_Y_1();
		int64_t L_8 = (&___linePt22)->get_Y_1();
		if ((((int64_t)L_7) == ((int64_t)L_8)))
		{
			goto IL_00ed;
		}
	}

IL_0053:
	{
		int64_t L_9 = (&___pt0)->get_X_0();
		int64_t L_10 = (&___linePt11)->get_X_0();
		int64_t L_11 = (&___pt0)->get_X_0();
		int64_t L_12 = (&___linePt22)->get_X_0();
		if ((!(((uint32_t)((((int64_t)L_9) > ((int64_t)L_10))? 1 : 0)) == ((uint32_t)((((int64_t)L_11) < ((int64_t)L_12))? 1 : 0)))))
		{
			goto IL_00ea;
		}
	}
	{
		int64_t L_13 = (&___pt0)->get_Y_1();
		int64_t L_14 = (&___linePt11)->get_Y_1();
		int64_t L_15 = (&___pt0)->get_Y_1();
		int64_t L_16 = (&___linePt22)->get_Y_1();
		if ((!(((uint32_t)((((int64_t)L_13) > ((int64_t)L_14))? 1 : 0)) == ((uint32_t)((((int64_t)L_15) < ((int64_t)L_16))? 1 : 0)))))
		{
			goto IL_00ea;
		}
	}
	{
		int64_t L_17 = (&___pt0)->get_X_0();
		int64_t L_18 = (&___linePt11)->get_X_0();
		int64_t L_19 = (&___linePt22)->get_Y_1();
		int64_t L_20 = (&___linePt11)->get_Y_1();
		Int128_t3067532394  L_21 = Int128_Int128Mul_m44987946(NULL /*static, unused*/, ((int64_t)((int64_t)L_17-(int64_t)L_18)), ((int64_t)((int64_t)L_19-(int64_t)L_20)), /*hidden argument*/NULL);
		int64_t L_22 = (&___linePt22)->get_X_0();
		int64_t L_23 = (&___linePt11)->get_X_0();
		int64_t L_24 = (&___pt0)->get_Y_1();
		int64_t L_25 = (&___linePt11)->get_Y_1();
		Int128_t3067532394  L_26 = Int128_Int128Mul_m44987946(NULL /*static, unused*/, ((int64_t)((int64_t)L_22-(int64_t)L_23)), ((int64_t)((int64_t)L_24-(int64_t)L_25)), /*hidden argument*/NULL);
		bool L_27 = Int128_op_Equality_m3976304897(NULL /*static, unused*/, L_21, L_26, /*hidden argument*/NULL);
		G_B9_0 = ((int32_t)(L_27));
		goto IL_00eb;
	}

IL_00ea:
	{
		G_B9_0 = 0;
	}

IL_00eb:
	{
		G_B11_0 = G_B9_0;
		goto IL_00ee;
	}

IL_00ed:
	{
		G_B11_0 = 1;
	}

IL_00ee:
	{
		return (bool)G_B11_0;
	}

IL_00ef:
	{
		int64_t L_28 = (&___pt0)->get_X_0();
		int64_t L_29 = (&___linePt11)->get_X_0();
		if ((!(((uint64_t)L_28) == ((uint64_t)L_29))))
		{
			goto IL_0115;
		}
	}
	{
		int64_t L_30 = (&___pt0)->get_Y_1();
		int64_t L_31 = (&___linePt11)->get_Y_1();
		if ((((int64_t)L_30) == ((int64_t)L_31)))
		{
			goto IL_01ca;
		}
	}

IL_0115:
	{
		int64_t L_32 = (&___pt0)->get_X_0();
		int64_t L_33 = (&___linePt22)->get_X_0();
		if ((!(((uint64_t)L_32) == ((uint64_t)L_33))))
		{
			goto IL_013b;
		}
	}
	{
		int64_t L_34 = (&___pt0)->get_Y_1();
		int64_t L_35 = (&___linePt22)->get_Y_1();
		if ((((int64_t)L_34) == ((int64_t)L_35)))
		{
			goto IL_01ca;
		}
	}

IL_013b:
	{
		int64_t L_36 = (&___pt0)->get_X_0();
		int64_t L_37 = (&___linePt11)->get_X_0();
		int64_t L_38 = (&___pt0)->get_X_0();
		int64_t L_39 = (&___linePt22)->get_X_0();
		if ((!(((uint32_t)((((int64_t)L_36) > ((int64_t)L_37))? 1 : 0)) == ((uint32_t)((((int64_t)L_38) < ((int64_t)L_39))? 1 : 0)))))
		{
			goto IL_01c7;
		}
	}
	{
		int64_t L_40 = (&___pt0)->get_Y_1();
		int64_t L_41 = (&___linePt11)->get_Y_1();
		int64_t L_42 = (&___pt0)->get_Y_1();
		int64_t L_43 = (&___linePt22)->get_Y_1();
		if ((!(((uint32_t)((((int64_t)L_40) > ((int64_t)L_41))? 1 : 0)) == ((uint32_t)((((int64_t)L_42) < ((int64_t)L_43))? 1 : 0)))))
		{
			goto IL_01c7;
		}
	}
	{
		int64_t L_44 = (&___pt0)->get_X_0();
		int64_t L_45 = (&___linePt11)->get_X_0();
		int64_t L_46 = (&___linePt22)->get_Y_1();
		int64_t L_47 = (&___linePt11)->get_Y_1();
		int64_t L_48 = (&___linePt22)->get_X_0();
		int64_t L_49 = (&___linePt11)->get_X_0();
		int64_t L_50 = (&___pt0)->get_Y_1();
		int64_t L_51 = (&___linePt11)->get_Y_1();
		G_B20_0 = ((((int64_t)((int64_t)((int64_t)((int64_t)((int64_t)L_44-(int64_t)L_45))*(int64_t)((int64_t)((int64_t)L_46-(int64_t)L_47))))) == ((int64_t)((int64_t)((int64_t)((int64_t)((int64_t)L_48-(int64_t)L_49))*(int64_t)((int64_t)((int64_t)L_50-(int64_t)L_51))))))? 1 : 0);
		goto IL_01c8;
	}

IL_01c7:
	{
		G_B20_0 = 0;
	}

IL_01c8:
	{
		G_B22_0 = G_B20_0;
		goto IL_01cb;
	}

IL_01ca:
	{
		G_B22_0 = 1;
	}

IL_01cb:
	{
		return (bool)G_B22_0;
	}
}
// System.Boolean Pathfinding.ClipperLib.ClipperBase::PointOnPolygon(Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.OutPt,System.Boolean)
extern "C"  bool ClipperBase_PointOnPolygon_m903142260 (ClipperBase_t512275816 * __this, IntPoint_t3326126179  ___pt0, OutPt_t3947633180 * ___pp1, bool ___UseFullRange2, const MethodInfo* method)
{
	OutPt_t3947633180 * V_0 = NULL;
	{
		OutPt_t3947633180 * L_0 = ___pp1;
		V_0 = L_0;
	}

IL_0002:
	{
		IntPoint_t3326126179  L_1 = ___pt0;
		OutPt_t3947633180 * L_2 = V_0;
		NullCheck(L_2);
		IntPoint_t3326126179  L_3 = L_2->get_Pt_1();
		OutPt_t3947633180 * L_4 = V_0;
		NullCheck(L_4);
		OutPt_t3947633180 * L_5 = L_4->get_Next_2();
		NullCheck(L_5);
		IntPoint_t3326126179  L_6 = L_5->get_Pt_1();
		bool L_7 = ___UseFullRange2;
		bool L_8 = ClipperBase_PointOnLineSegment_m3036412045(__this, L_1, L_3, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0022;
		}
	}
	{
		return (bool)1;
	}

IL_0022:
	{
		OutPt_t3947633180 * L_9 = V_0;
		NullCheck(L_9);
		OutPt_t3947633180 * L_10 = L_9->get_Next_2();
		V_0 = L_10;
		OutPt_t3947633180 * L_11 = V_0;
		OutPt_t3947633180 * L_12 = ___pp1;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_11) == ((Il2CppObject*)(OutPt_t3947633180 *)L_12))))
		{
			goto IL_0035;
		}
	}
	{
		goto IL_003a;
	}

IL_0035:
	{
		goto IL_0002;
	}

IL_003a:
	{
		return (bool)0;
	}
}
// System.Boolean Pathfinding.ClipperLib.ClipperBase::PointInPolygon(Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.OutPt,System.Boolean)
extern "C"  bool ClipperBase_PointInPolygon_m2483899322 (ClipperBase_t512275816 * __this, IntPoint_t3326126179  ___pt0, OutPt_t3947633180 * ___pp1, bool ___UseFullRange2, const MethodInfo* method)
{
	OutPt_t3947633180 * V_0 = NULL;
	bool V_1 = false;
	{
		OutPt_t3947633180 * L_0 = ___pp1;
		V_0 = L_0;
		V_1 = (bool)0;
		bool L_1 = ___UseFullRange2;
		if (!L_1)
		{
			goto IL_00d0;
		}
	}

IL_000a:
	{
		OutPt_t3947633180 * L_2 = V_0;
		NullCheck(L_2);
		IntPoint_t3326126179 * L_3 = L_2->get_address_of_Pt_1();
		int64_t L_4 = L_3->get_Y_1();
		int64_t L_5 = (&___pt0)->get_Y_1();
		OutPt_t3947633180 * L_6 = V_0;
		NullCheck(L_6);
		OutPt_t3947633180 * L_7 = L_6->get_Prev_3();
		NullCheck(L_7);
		IntPoint_t3326126179 * L_8 = L_7->get_address_of_Pt_1();
		int64_t L_9 = L_8->get_Y_1();
		int64_t L_10 = (&___pt0)->get_Y_1();
		if ((((int32_t)((((int64_t)L_4) > ((int64_t)L_5))? 1 : 0)) == ((int32_t)((((int64_t)L_9) > ((int64_t)L_10))? 1 : 0))))
		{
			goto IL_00bd;
		}
	}
	{
		int64_t L_11 = (&___pt0)->get_X_0();
		OutPt_t3947633180 * L_12 = V_0;
		NullCheck(L_12);
		IntPoint_t3326126179 * L_13 = L_12->get_address_of_Pt_1();
		int64_t L_14 = L_13->get_X_0();
		Int128_t3067532394  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Int128__ctor_m3350950229(&L_15, ((int64_t)((int64_t)L_11-(int64_t)L_14)), /*hidden argument*/NULL);
		OutPt_t3947633180 * L_16 = V_0;
		NullCheck(L_16);
		OutPt_t3947633180 * L_17 = L_16->get_Prev_3();
		NullCheck(L_17);
		IntPoint_t3326126179 * L_18 = L_17->get_address_of_Pt_1();
		int64_t L_19 = L_18->get_X_0();
		OutPt_t3947633180 * L_20 = V_0;
		NullCheck(L_20);
		IntPoint_t3326126179 * L_21 = L_20->get_address_of_Pt_1();
		int64_t L_22 = L_21->get_X_0();
		int64_t L_23 = (&___pt0)->get_Y_1();
		OutPt_t3947633180 * L_24 = V_0;
		NullCheck(L_24);
		IntPoint_t3326126179 * L_25 = L_24->get_address_of_Pt_1();
		int64_t L_26 = L_25->get_Y_1();
		Int128_t3067532394  L_27 = Int128_Int128Mul_m44987946(NULL /*static, unused*/, ((int64_t)((int64_t)L_19-(int64_t)L_22)), ((int64_t)((int64_t)L_23-(int64_t)L_26)), /*hidden argument*/NULL);
		OutPt_t3947633180 * L_28 = V_0;
		NullCheck(L_28);
		OutPt_t3947633180 * L_29 = L_28->get_Prev_3();
		NullCheck(L_29);
		IntPoint_t3326126179 * L_30 = L_29->get_address_of_Pt_1();
		int64_t L_31 = L_30->get_Y_1();
		OutPt_t3947633180 * L_32 = V_0;
		NullCheck(L_32);
		IntPoint_t3326126179 * L_33 = L_32->get_address_of_Pt_1();
		int64_t L_34 = L_33->get_Y_1();
		Int128_t3067532394  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Int128__ctor_m3350950229(&L_35, ((int64_t)((int64_t)L_31-(int64_t)L_34)), /*hidden argument*/NULL);
		Int128_t3067532394  L_36 = Int128_op_Division_m2463004203(NULL /*static, unused*/, L_27, L_35, /*hidden argument*/NULL);
		bool L_37 = Int128_op_LessThan_m3057609505(NULL /*static, unused*/, L_15, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_00bd;
		}
	}
	{
		bool L_38 = V_1;
		V_1 = (bool)((((int32_t)L_38) == ((int32_t)0))? 1 : 0);
	}

IL_00bd:
	{
		OutPt_t3947633180 * L_39 = V_0;
		NullCheck(L_39);
		OutPt_t3947633180 * L_40 = L_39->get_Next_2();
		V_0 = L_40;
		OutPt_t3947633180 * L_41 = V_0;
		OutPt_t3947633180 * L_42 = ___pp1;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_41) == ((Il2CppObject*)(OutPt_t3947633180 *)L_42))))
		{
			goto IL_000a;
		}
	}
	{
		goto IL_017a;
	}

IL_00d0:
	{
		OutPt_t3947633180 * L_43 = V_0;
		NullCheck(L_43);
		IntPoint_t3326126179 * L_44 = L_43->get_address_of_Pt_1();
		int64_t L_45 = L_44->get_Y_1();
		int64_t L_46 = (&___pt0)->get_Y_1();
		OutPt_t3947633180 * L_47 = V_0;
		NullCheck(L_47);
		OutPt_t3947633180 * L_48 = L_47->get_Prev_3();
		NullCheck(L_48);
		IntPoint_t3326126179 * L_49 = L_48->get_address_of_Pt_1();
		int64_t L_50 = L_49->get_Y_1();
		int64_t L_51 = (&___pt0)->get_Y_1();
		if ((((int32_t)((((int64_t)L_45) > ((int64_t)L_46))? 1 : 0)) == ((int32_t)((((int64_t)L_50) > ((int64_t)L_51))? 1 : 0))))
		{
			goto IL_016c;
		}
	}
	{
		int64_t L_52 = (&___pt0)->get_X_0();
		OutPt_t3947633180 * L_53 = V_0;
		NullCheck(L_53);
		IntPoint_t3326126179 * L_54 = L_53->get_address_of_Pt_1();
		int64_t L_55 = L_54->get_X_0();
		OutPt_t3947633180 * L_56 = V_0;
		NullCheck(L_56);
		OutPt_t3947633180 * L_57 = L_56->get_Prev_3();
		NullCheck(L_57);
		IntPoint_t3326126179 * L_58 = L_57->get_address_of_Pt_1();
		int64_t L_59 = L_58->get_X_0();
		OutPt_t3947633180 * L_60 = V_0;
		NullCheck(L_60);
		IntPoint_t3326126179 * L_61 = L_60->get_address_of_Pt_1();
		int64_t L_62 = L_61->get_X_0();
		int64_t L_63 = (&___pt0)->get_Y_1();
		OutPt_t3947633180 * L_64 = V_0;
		NullCheck(L_64);
		IntPoint_t3326126179 * L_65 = L_64->get_address_of_Pt_1();
		int64_t L_66 = L_65->get_Y_1();
		OutPt_t3947633180 * L_67 = V_0;
		NullCheck(L_67);
		OutPt_t3947633180 * L_68 = L_67->get_Prev_3();
		NullCheck(L_68);
		IntPoint_t3326126179 * L_69 = L_68->get_address_of_Pt_1();
		int64_t L_70 = L_69->get_Y_1();
		OutPt_t3947633180 * L_71 = V_0;
		NullCheck(L_71);
		IntPoint_t3326126179 * L_72 = L_71->get_address_of_Pt_1();
		int64_t L_73 = L_72->get_Y_1();
		if ((((int64_t)((int64_t)((int64_t)L_52-(int64_t)L_55))) >= ((int64_t)((int64_t)((int64_t)((int64_t)((int64_t)((int64_t)((int64_t)L_59-(int64_t)L_62))*(int64_t)((int64_t)((int64_t)L_63-(int64_t)L_66))))/(int64_t)((int64_t)((int64_t)L_70-(int64_t)L_73)))))))
		{
			goto IL_016c;
		}
	}
	{
		bool L_74 = V_1;
		V_1 = (bool)((((int32_t)L_74) == ((int32_t)0))? 1 : 0);
	}

IL_016c:
	{
		OutPt_t3947633180 * L_75 = V_0;
		NullCheck(L_75);
		OutPt_t3947633180 * L_76 = L_75->get_Next_2();
		V_0 = L_76;
		OutPt_t3947633180 * L_77 = V_0;
		OutPt_t3947633180 * L_78 = ___pp1;
		if ((!(((Il2CppObject*)(OutPt_t3947633180 *)L_77) == ((Il2CppObject*)(OutPt_t3947633180 *)L_78))))
		{
			goto IL_00d0;
		}
	}

IL_017a:
	{
		bool L_79 = V_1;
		return L_79;
	}
}
// System.Boolean Pathfinding.ClipperLib.ClipperBase::SlopesEqual(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge,System.Boolean)
extern "C"  bool ClipperBase_SlopesEqual_m3887656866 (Il2CppObject * __this /* static, unused */, TEdge_t3950806139 * ___e10, TEdge_t3950806139 * ___e21, bool ___UseFullRange2, const MethodInfo* method)
{
	{
		bool L_0 = ___UseFullRange2;
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		TEdge_t3950806139 * L_1 = ___e10;
		NullCheck(L_1);
		IntPoint_t3326126179 * L_2 = L_1->get_address_of_Delta_3();
		int64_t L_3 = L_2->get_Y_1();
		TEdge_t3950806139 * L_4 = ___e21;
		NullCheck(L_4);
		IntPoint_t3326126179 * L_5 = L_4->get_address_of_Delta_3();
		int64_t L_6 = L_5->get_X_0();
		Int128_t3067532394  L_7 = Int128_Int128Mul_m44987946(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_8 = ___e10;
		NullCheck(L_8);
		IntPoint_t3326126179 * L_9 = L_8->get_address_of_Delta_3();
		int64_t L_10 = L_9->get_X_0();
		TEdge_t3950806139 * L_11 = ___e21;
		NullCheck(L_11);
		IntPoint_t3326126179 * L_12 = L_11->get_address_of_Delta_3();
		int64_t L_13 = L_12->get_Y_1();
		Int128_t3067532394  L_14 = Int128_Int128Mul_m44987946(NULL /*static, unused*/, L_10, L_13, /*hidden argument*/NULL);
		bool L_15 = Int128_op_Equality_m3976304897(NULL /*static, unused*/, L_7, L_14, /*hidden argument*/NULL);
		return L_15;
	}

IL_0042:
	{
		TEdge_t3950806139 * L_16 = ___e10;
		NullCheck(L_16);
		IntPoint_t3326126179 * L_17 = L_16->get_address_of_Delta_3();
		int64_t L_18 = L_17->get_Y_1();
		TEdge_t3950806139 * L_19 = ___e21;
		NullCheck(L_19);
		IntPoint_t3326126179 * L_20 = L_19->get_address_of_Delta_3();
		int64_t L_21 = L_20->get_X_0();
		TEdge_t3950806139 * L_22 = ___e10;
		NullCheck(L_22);
		IntPoint_t3326126179 * L_23 = L_22->get_address_of_Delta_3();
		int64_t L_24 = L_23->get_X_0();
		TEdge_t3950806139 * L_25 = ___e21;
		NullCheck(L_25);
		IntPoint_t3326126179 * L_26 = L_25->get_address_of_Delta_3();
		int64_t L_27 = L_26->get_Y_1();
		return (bool)((((int64_t)((int64_t)((int64_t)L_18*(int64_t)L_21))) == ((int64_t)((int64_t)((int64_t)L_24*(int64_t)L_27))))? 1 : 0);
	}
}
// System.Boolean Pathfinding.ClipperLib.ClipperBase::SlopesEqual(Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint,System.Boolean)
extern "C"  bool ClipperBase_SlopesEqual_m1151992421 (Il2CppObject * __this /* static, unused */, IntPoint_t3326126179  ___pt10, IntPoint_t3326126179  ___pt21, IntPoint_t3326126179  ___pt32, bool ___UseFullRange3, const MethodInfo* method)
{
	{
		bool L_0 = ___UseFullRange3;
		if (!L_0)
		{
			goto IL_0052;
		}
	}
	{
		int64_t L_1 = (&___pt10)->get_Y_1();
		int64_t L_2 = (&___pt21)->get_Y_1();
		int64_t L_3 = (&___pt21)->get_X_0();
		int64_t L_4 = (&___pt32)->get_X_0();
		Int128_t3067532394  L_5 = Int128_Int128Mul_m44987946(NULL /*static, unused*/, ((int64_t)((int64_t)L_1-(int64_t)L_2)), ((int64_t)((int64_t)L_3-(int64_t)L_4)), /*hidden argument*/NULL);
		int64_t L_6 = (&___pt10)->get_X_0();
		int64_t L_7 = (&___pt21)->get_X_0();
		int64_t L_8 = (&___pt21)->get_Y_1();
		int64_t L_9 = (&___pt32)->get_Y_1();
		Int128_t3067532394  L_10 = Int128_Int128Mul_m44987946(NULL /*static, unused*/, ((int64_t)((int64_t)L_6-(int64_t)L_7)), ((int64_t)((int64_t)L_8-(int64_t)L_9)), /*hidden argument*/NULL);
		bool L_11 = Int128_op_Equality_m3976304897(NULL /*static, unused*/, L_5, L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_0052:
	{
		int64_t L_12 = (&___pt10)->get_Y_1();
		int64_t L_13 = (&___pt21)->get_Y_1();
		int64_t L_14 = (&___pt21)->get_X_0();
		int64_t L_15 = (&___pt32)->get_X_0();
		int64_t L_16 = (&___pt10)->get_X_0();
		int64_t L_17 = (&___pt21)->get_X_0();
		int64_t L_18 = (&___pt21)->get_Y_1();
		int64_t L_19 = (&___pt32)->get_Y_1();
		return (bool)((((int64_t)((int64_t)((int64_t)((int64_t)((int64_t)((int64_t)((int64_t)L_12-(int64_t)L_13))*(int64_t)((int64_t)((int64_t)L_14-(int64_t)L_15))))-(int64_t)((int64_t)((int64_t)((int64_t)((int64_t)L_16-(int64_t)L_17))*(int64_t)((int64_t)((int64_t)L_18-(int64_t)L_19))))))) == ((int64_t)(((int64_t)((int64_t)0)))))? 1 : 0);
	}
}
// System.Void Pathfinding.ClipperLib.ClipperBase::Clear()
extern const MethodInfo* List_1_get_Item_m581926425_MethodInfo_var;
extern const MethodInfo* List_1_set_Item_m3034623278_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2660703478_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m898200423_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m932073596_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m1081398041_MethodInfo_var;
extern const uint32_t ClipperBase_Clear_m2549749704_MetadataUsageId;
extern "C"  void ClipperBase_Clear_m2549749704 (ClipperBase_t512275816 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClipperBase_Clear_m2549749704_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		ClipperBase_DisposeLocalMinimaList_m1190589532(__this, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_0057;
	}

IL_000d:
	{
		V_1 = 0;
		goto IL_002b;
	}

IL_0014:
	{
		List_1_t2392209947 * L_0 = __this->get_m_edges_8();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		List_1_t1024024395 * L_2 = List_1_get_Item_m581926425(L_0, L_1, /*hidden argument*/List_1_get_Item_m581926425_MethodInfo_var);
		int32_t L_3 = V_1;
		NullCheck(L_2);
		List_1_set_Item_m3034623278(L_2, L_3, (TEdge_t3950806139 *)NULL, /*hidden argument*/List_1_set_Item_m3034623278_MethodInfo_var);
		int32_t L_4 = V_1;
		V_1 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_002b:
	{
		int32_t L_5 = V_1;
		List_1_t2392209947 * L_6 = __this->get_m_edges_8();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		List_1_t1024024395 * L_8 = List_1_get_Item_m581926425(L_6, L_7, /*hidden argument*/List_1_get_Item_m581926425_MethodInfo_var);
		NullCheck(L_8);
		int32_t L_9 = List_1_get_Count_m2660703478(L_8, /*hidden argument*/List_1_get_Count_m2660703478_MethodInfo_var);
		if ((((int32_t)L_5) < ((int32_t)L_9)))
		{
			goto IL_0014;
		}
	}
	{
		List_1_t2392209947 * L_10 = __this->get_m_edges_8();
		int32_t L_11 = V_0;
		NullCheck(L_10);
		List_1_t1024024395 * L_12 = List_1_get_Item_m581926425(L_10, L_11, /*hidden argument*/List_1_get_Item_m581926425_MethodInfo_var);
		NullCheck(L_12);
		List_1_Clear_m898200423(L_12, /*hidden argument*/List_1_Clear_m898200423_MethodInfo_var);
		int32_t L_13 = V_0;
		V_0 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0057:
	{
		int32_t L_14 = V_0;
		List_1_t2392209947 * L_15 = __this->get_m_edges_8();
		NullCheck(L_15);
		int32_t L_16 = List_1_get_Count_m932073596(L_15, /*hidden argument*/List_1_get_Count_m932073596_MethodInfo_var);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_000d;
		}
	}
	{
		List_1_t2392209947 * L_17 = __this->get_m_edges_8();
		NullCheck(L_17);
		List_1_Clear_m1081398041(L_17, /*hidden argument*/List_1_Clear_m1081398041_MethodInfo_var);
		__this->set_m_UseFullRange_9((bool)0);
		__this->set_m_HasOpenPaths_10((bool)0);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.ClipperBase::DisposeLocalMinimaList()
extern "C"  void ClipperBase_DisposeLocalMinimaList_m1190589532 (ClipperBase_t512275816 * __this, const MethodInfo* method)
{
	LocalMinima_t2863342752 * V_0 = NULL;
	{
		goto IL_001f;
	}

IL_0005:
	{
		LocalMinima_t2863342752 * L_0 = __this->get_m_MinimaList_6();
		NullCheck(L_0);
		LocalMinima_t2863342752 * L_1 = L_0->get_Next_3();
		V_0 = L_1;
		__this->set_m_MinimaList_6((LocalMinima_t2863342752 *)NULL);
		LocalMinima_t2863342752 * L_2 = V_0;
		__this->set_m_MinimaList_6(L_2);
	}

IL_001f:
	{
		LocalMinima_t2863342752 * L_3 = __this->get_m_MinimaList_6();
		if (L_3)
		{
			goto IL_0005;
		}
	}
	{
		__this->set_m_CurrentLM_7((LocalMinima_t2863342752 *)NULL);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.ClipperBase::RangeTest(Pathfinding.ClipperLib.IntPoint,System.Boolean&)
extern Il2CppClass* ClipperException_t2818206852_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1800450;
extern const uint32_t ClipperBase_RangeTest_m1801249034_MetadataUsageId;
extern "C"  void ClipperBase_RangeTest_m1801249034 (ClipperBase_t512275816 * __this, IntPoint_t3326126179  ___Pt0, bool* ___useFullRange1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClipperBase_RangeTest_m1801249034_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool* L_0 = ___useFullRange1;
		if (!(*((int8_t*)L_0)))
		{
			goto IL_006d;
		}
	}
	{
		int64_t L_1 = (&___Pt0)->get_X_0();
		if ((((int64_t)L_1) > ((int64_t)((int64_t)4611686018427387903LL))))
		{
			goto IL_005d;
		}
	}
	{
		int64_t L_2 = (&___Pt0)->get_Y_1();
		if ((((int64_t)L_2) > ((int64_t)((int64_t)4611686018427387903LL))))
		{
			goto IL_005d;
		}
	}
	{
		int64_t L_3 = (&___Pt0)->get_X_0();
		if ((((int64_t)((-L_3))) > ((int64_t)((int64_t)4611686018427387903LL))))
		{
			goto IL_005d;
		}
	}
	{
		int64_t L_4 = (&___Pt0)->get_Y_1();
		if ((((int64_t)((-L_4))) <= ((int64_t)((int64_t)4611686018427387903LL))))
		{
			goto IL_0068;
		}
	}

IL_005d:
	{
		ClipperException_t2818206852 * L_5 = (ClipperException_t2818206852 *)il2cpp_codegen_object_new(ClipperException_t2818206852_il2cpp_TypeInfo_var);
		ClipperException__ctor_m106504313(L_5, _stringLiteral1800450, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0068:
	{
		goto IL_00c2;
	}

IL_006d:
	{
		int64_t L_6 = (&___Pt0)->get_X_0();
		if ((((int64_t)L_6) > ((int64_t)(((int64_t)((int64_t)((int32_t)1073741823)))))))
		{
			goto IL_00b7;
		}
	}
	{
		int64_t L_7 = (&___Pt0)->get_Y_1();
		if ((((int64_t)L_7) > ((int64_t)(((int64_t)((int64_t)((int32_t)1073741823)))))))
		{
			goto IL_00b7;
		}
	}
	{
		int64_t L_8 = (&___Pt0)->get_X_0();
		if ((((int64_t)((-L_8))) > ((int64_t)(((int64_t)((int64_t)((int32_t)1073741823)))))))
		{
			goto IL_00b7;
		}
	}
	{
		int64_t L_9 = (&___Pt0)->get_Y_1();
		if ((((int64_t)((-L_9))) <= ((int64_t)(((int64_t)((int64_t)((int32_t)1073741823)))))))
		{
			goto IL_00c2;
		}
	}

IL_00b7:
	{
		bool* L_10 = ___useFullRange1;
		*((int8_t*)(L_10)) = (int8_t)1;
		IntPoint_t3326126179  L_11 = ___Pt0;
		bool* L_12 = ___useFullRange1;
		ClipperBase_RangeTest_m1801249034(__this, L_11, L_12, /*hidden argument*/NULL);
	}

IL_00c2:
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.ClipperBase::InitEdge(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.IntPoint)
extern "C"  void ClipperBase_InitEdge_m2546155214 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___e0, TEdge_t3950806139 * ___eNext1, TEdge_t3950806139 * ___ePrev2, IntPoint_t3326126179  ___pt3, const MethodInfo* method)
{
	{
		TEdge_t3950806139 * L_0 = ___e0;
		TEdge_t3950806139 * L_1 = ___eNext1;
		NullCheck(L_0);
		L_0->set_Next_11(L_1);
		TEdge_t3950806139 * L_2 = ___e0;
		TEdge_t3950806139 * L_3 = ___ePrev2;
		NullCheck(L_2);
		L_2->set_Prev_12(L_3);
		TEdge_t3950806139 * L_4 = ___e0;
		IntPoint_t3326126179  L_5 = ___pt3;
		NullCheck(L_4);
		L_4->set_Curr_1(L_5);
		TEdge_t3950806139 * L_6 = ___e0;
		NullCheck(L_6);
		L_6->set_OutIdx_10((-1));
		return;
	}
}
// System.Void Pathfinding.ClipperLib.ClipperBase::InitEdge2(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.PolyType)
extern "C"  void ClipperBase_InitEdge2_m1715435253 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___e0, int32_t ___polyType1, const MethodInfo* method)
{
	{
		TEdge_t3950806139 * L_0 = ___e0;
		NullCheck(L_0);
		IntPoint_t3326126179 * L_1 = L_0->get_address_of_Curr_1();
		int64_t L_2 = L_1->get_Y_1();
		TEdge_t3950806139 * L_3 = ___e0;
		NullCheck(L_3);
		TEdge_t3950806139 * L_4 = L_3->get_Next_11();
		NullCheck(L_4);
		IntPoint_t3326126179 * L_5 = L_4->get_address_of_Curr_1();
		int64_t L_6 = L_5->get_Y_1();
		if ((((int64_t)L_2) < ((int64_t)L_6)))
		{
			goto IL_0042;
		}
	}
	{
		TEdge_t3950806139 * L_7 = ___e0;
		TEdge_t3950806139 * L_8 = ___e0;
		NullCheck(L_8);
		IntPoint_t3326126179  L_9 = L_8->get_Curr_1();
		NullCheck(L_7);
		L_7->set_Bot_0(L_9);
		TEdge_t3950806139 * L_10 = ___e0;
		TEdge_t3950806139 * L_11 = ___e0;
		NullCheck(L_11);
		TEdge_t3950806139 * L_12 = L_11->get_Next_11();
		NullCheck(L_12);
		IntPoint_t3326126179  L_13 = L_12->get_Curr_1();
		NullCheck(L_10);
		L_10->set_Top_2(L_13);
		goto IL_005f;
	}

IL_0042:
	{
		TEdge_t3950806139 * L_14 = ___e0;
		TEdge_t3950806139 * L_15 = ___e0;
		NullCheck(L_15);
		IntPoint_t3326126179  L_16 = L_15->get_Curr_1();
		NullCheck(L_14);
		L_14->set_Top_2(L_16);
		TEdge_t3950806139 * L_17 = ___e0;
		TEdge_t3950806139 * L_18 = ___e0;
		NullCheck(L_18);
		TEdge_t3950806139 * L_19 = L_18->get_Next_11();
		NullCheck(L_19);
		IntPoint_t3326126179  L_20 = L_19->get_Curr_1();
		NullCheck(L_17);
		L_17->set_Bot_0(L_20);
	}

IL_005f:
	{
		TEdge_t3950806139 * L_21 = ___e0;
		ClipperBase_SetDx_m807634188(__this, L_21, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_22 = ___e0;
		int32_t L_23 = ___polyType1;
		NullCheck(L_22);
		L_22->set_PolyTyp_5(L_23);
		return;
	}
}
// System.Boolean Pathfinding.ClipperLib.ClipperBase::AddPath(System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>,Pathfinding.ClipperLib.PolyType,System.Boolean)
extern Il2CppClass* ClipperException_t2818206852_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1024024395_il2cpp_TypeInfo_var;
extern Il2CppClass* TEdge_t3950806139_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m647655502_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m788539143_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1628271245_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3186427948_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m706652959_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3369625566_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral767082618;
extern const uint32_t ClipperBase_AddPath_m2901119053_MetadataUsageId;
extern "C"  bool ClipperBase_AddPath_m2901119053 (ClipperBase_t512275816 * __this, List_1_t399344435 * ___pg0, int32_t ___polyType1, bool ___Closed2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClipperBase_AddPath_m2901119053_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	List_1_t1024024395 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	bool V_5 = false;
	TEdge_t3950806139 * V_6 = NULL;
	TEdge_t3950806139 * V_7 = NULL;
	TEdge_t3950806139 * V_8 = NULL;
	TEdge_t3950806139 * V_9 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B6_0 = 0;
	int32_t G_B8_0 = 0;
	{
		bool L_0 = ___Closed2;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ClipperException_t2818206852 * L_1 = (ClipperException_t2818206852 *)il2cpp_codegen_object_new(ClipperException_t2818206852_il2cpp_TypeInfo_var);
		ClipperException__ctor_m106504313(L_1, _stringLiteral767082618, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		List_1_t399344435 * L_2 = ___pg0;
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m647655502(L_2, /*hidden argument*/List_1_get_Count_m647655502_MethodInfo_var);
		V_0 = ((int32_t)((int32_t)L_3-(int32_t)1));
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_003f;
		}
	}
	{
		bool L_5 = ___Closed2;
		if (L_5)
		{
			goto IL_003c;
		}
	}
	{
		List_1_t399344435 * L_6 = ___pg0;
		NullCheck(L_6);
		IntPoint_t3326126179  L_7 = List_1_get_Item_m788539143(L_6, 0, /*hidden argument*/List_1_get_Item_m788539143_MethodInfo_var);
		List_1_t399344435 * L_8 = ___pg0;
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IntPoint_t3326126179  L_10 = List_1_get_Item_m788539143(L_8, L_9, /*hidden argument*/List_1_get_Item_m788539143_MethodInfo_var);
		bool L_11 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_7, L_10, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_11));
		goto IL_003d;
	}

IL_003c:
	{
		G_B6_0 = 1;
	}

IL_003d:
	{
		G_B8_0 = G_B6_0;
		goto IL_0040;
	}

IL_003f:
	{
		G_B8_0 = 0;
	}

IL_0040:
	{
		V_1 = (bool)G_B8_0;
		goto IL_004a;
	}

IL_0046:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12-(int32_t)1));
	}

IL_004a:
	{
		int32_t L_13 = V_0;
		if ((((int32_t)L_13) <= ((int32_t)0)))
		{
			goto IL_0069;
		}
	}
	{
		List_1_t399344435 * L_14 = ___pg0;
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IntPoint_t3326126179  L_16 = List_1_get_Item_m788539143(L_14, L_15, /*hidden argument*/List_1_get_Item_m788539143_MethodInfo_var);
		List_1_t399344435 * L_17 = ___pg0;
		NullCheck(L_17);
		IntPoint_t3326126179  L_18 = List_1_get_Item_m788539143(L_17, 0, /*hidden argument*/List_1_get_Item_m788539143_MethodInfo_var);
		bool L_19 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_16, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0046;
		}
	}

IL_0069:
	{
		goto IL_0072;
	}

IL_006e:
	{
		int32_t L_20 = V_0;
		V_0 = ((int32_t)((int32_t)L_20-(int32_t)1));
	}

IL_0072:
	{
		int32_t L_21 = V_0;
		if ((((int32_t)L_21) <= ((int32_t)0)))
		{
			goto IL_0093;
		}
	}
	{
		List_1_t399344435 * L_22 = ___pg0;
		int32_t L_23 = V_0;
		NullCheck(L_22);
		IntPoint_t3326126179  L_24 = List_1_get_Item_m788539143(L_22, L_23, /*hidden argument*/List_1_get_Item_m788539143_MethodInfo_var);
		List_1_t399344435 * L_25 = ___pg0;
		int32_t L_26 = V_0;
		NullCheck(L_25);
		IntPoint_t3326126179  L_27 = List_1_get_Item_m788539143(L_25, ((int32_t)((int32_t)L_26-(int32_t)1)), /*hidden argument*/List_1_get_Item_m788539143_MethodInfo_var);
		bool L_28 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_24, L_27, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_006e;
		}
	}

IL_0093:
	{
		bool L_29 = ___Closed2;
		if (!L_29)
		{
			goto IL_00a0;
		}
	}
	{
		int32_t L_30 = V_0;
		if ((((int32_t)L_30) < ((int32_t)2)))
		{
			goto IL_00ad;
		}
	}

IL_00a0:
	{
		bool L_31 = ___Closed2;
		if (L_31)
		{
			goto IL_00af;
		}
	}
	{
		int32_t L_32 = V_0;
		if ((((int32_t)L_32) >= ((int32_t)1)))
		{
			goto IL_00af;
		}
	}

IL_00ad:
	{
		return (bool)0;
	}

IL_00af:
	{
		int32_t L_33 = V_0;
		List_1_t1024024395 * L_34 = (List_1_t1024024395 *)il2cpp_codegen_object_new(List_1_t1024024395_il2cpp_TypeInfo_var);
		List_1__ctor_m1628271245(L_34, ((int32_t)((int32_t)L_33+(int32_t)1)), /*hidden argument*/List_1__ctor_m1628271245_MethodInfo_var);
		V_2 = L_34;
		V_3 = 0;
		goto IL_00ce;
	}

IL_00bf:
	{
		List_1_t1024024395 * L_35 = V_2;
		TEdge_t3950806139 * L_36 = (TEdge_t3950806139 *)il2cpp_codegen_object_new(TEdge_t3950806139_il2cpp_TypeInfo_var);
		TEdge__ctor_m2332074730(L_36, /*hidden argument*/NULL);
		NullCheck(L_35);
		List_1_Add_m3186427948(L_35, L_36, /*hidden argument*/List_1_Add_m3186427948_MethodInfo_var);
		int32_t L_37 = V_3;
		V_3 = ((int32_t)((int32_t)L_37+(int32_t)1));
	}

IL_00ce:
	{
		int32_t L_38 = V_3;
		int32_t L_39 = V_0;
		if ((((int32_t)L_38) <= ((int32_t)L_39)))
		{
			goto IL_00bf;
		}
	}

IL_00d5:
	try
	{ // begin try (depth: 1)
		{
			List_1_t1024024395 * L_40 = V_2;
			NullCheck(L_40);
			TEdge_t3950806139 * L_41 = List_1_get_Item_m706652959(L_40, 1, /*hidden argument*/List_1_get_Item_m706652959_MethodInfo_var);
			List_1_t399344435 * L_42 = ___pg0;
			NullCheck(L_42);
			IntPoint_t3326126179  L_43 = List_1_get_Item_m788539143(L_42, 1, /*hidden argument*/List_1_get_Item_m788539143_MethodInfo_var);
			NullCheck(L_41);
			L_41->set_Curr_1(L_43);
			List_1_t399344435 * L_44 = ___pg0;
			NullCheck(L_44);
			IntPoint_t3326126179  L_45 = List_1_get_Item_m788539143(L_44, 0, /*hidden argument*/List_1_get_Item_m788539143_MethodInfo_var);
			bool* L_46 = __this->get_address_of_m_UseFullRange_9();
			ClipperBase_RangeTest_m1801249034(__this, L_45, L_46, /*hidden argument*/NULL);
			List_1_t399344435 * L_47 = ___pg0;
			int32_t L_48 = V_0;
			NullCheck(L_47);
			IntPoint_t3326126179  L_49 = List_1_get_Item_m788539143(L_47, L_48, /*hidden argument*/List_1_get_Item_m788539143_MethodInfo_var);
			bool* L_50 = __this->get_address_of_m_UseFullRange_9();
			ClipperBase_RangeTest_m1801249034(__this, L_49, L_50, /*hidden argument*/NULL);
			List_1_t1024024395 * L_51 = V_2;
			NullCheck(L_51);
			TEdge_t3950806139 * L_52 = List_1_get_Item_m706652959(L_51, 0, /*hidden argument*/List_1_get_Item_m706652959_MethodInfo_var);
			List_1_t1024024395 * L_53 = V_2;
			NullCheck(L_53);
			TEdge_t3950806139 * L_54 = List_1_get_Item_m706652959(L_53, 1, /*hidden argument*/List_1_get_Item_m706652959_MethodInfo_var);
			List_1_t1024024395 * L_55 = V_2;
			int32_t L_56 = V_0;
			NullCheck(L_55);
			TEdge_t3950806139 * L_57 = List_1_get_Item_m706652959(L_55, L_56, /*hidden argument*/List_1_get_Item_m706652959_MethodInfo_var);
			List_1_t399344435 * L_58 = ___pg0;
			NullCheck(L_58);
			IntPoint_t3326126179  L_59 = List_1_get_Item_m788539143(L_58, 0, /*hidden argument*/List_1_get_Item_m788539143_MethodInfo_var);
			ClipperBase_InitEdge_m2546155214(__this, L_52, L_54, L_57, L_59, /*hidden argument*/NULL);
			List_1_t1024024395 * L_60 = V_2;
			int32_t L_61 = V_0;
			NullCheck(L_60);
			TEdge_t3950806139 * L_62 = List_1_get_Item_m706652959(L_60, L_61, /*hidden argument*/List_1_get_Item_m706652959_MethodInfo_var);
			List_1_t1024024395 * L_63 = V_2;
			NullCheck(L_63);
			TEdge_t3950806139 * L_64 = List_1_get_Item_m706652959(L_63, 0, /*hidden argument*/List_1_get_Item_m706652959_MethodInfo_var);
			List_1_t1024024395 * L_65 = V_2;
			int32_t L_66 = V_0;
			NullCheck(L_65);
			TEdge_t3950806139 * L_67 = List_1_get_Item_m706652959(L_65, ((int32_t)((int32_t)L_66-(int32_t)1)), /*hidden argument*/List_1_get_Item_m706652959_MethodInfo_var);
			List_1_t399344435 * L_68 = ___pg0;
			int32_t L_69 = V_0;
			NullCheck(L_68);
			IntPoint_t3326126179  L_70 = List_1_get_Item_m788539143(L_68, L_69, /*hidden argument*/List_1_get_Item_m788539143_MethodInfo_var);
			ClipperBase_InitEdge_m2546155214(__this, L_62, L_64, L_67, L_70, /*hidden argument*/NULL);
			int32_t L_71 = V_0;
			V_4 = ((int32_t)((int32_t)L_71-(int32_t)1));
			goto IL_01a2;
		}

IL_015e:
		{
			List_1_t399344435 * L_72 = ___pg0;
			int32_t L_73 = V_4;
			NullCheck(L_72);
			IntPoint_t3326126179  L_74 = List_1_get_Item_m788539143(L_72, L_73, /*hidden argument*/List_1_get_Item_m788539143_MethodInfo_var);
			bool* L_75 = __this->get_address_of_m_UseFullRange_9();
			ClipperBase_RangeTest_m1801249034(__this, L_74, L_75, /*hidden argument*/NULL);
			List_1_t1024024395 * L_76 = V_2;
			int32_t L_77 = V_4;
			NullCheck(L_76);
			TEdge_t3950806139 * L_78 = List_1_get_Item_m706652959(L_76, L_77, /*hidden argument*/List_1_get_Item_m706652959_MethodInfo_var);
			List_1_t1024024395 * L_79 = V_2;
			int32_t L_80 = V_4;
			NullCheck(L_79);
			TEdge_t3950806139 * L_81 = List_1_get_Item_m706652959(L_79, ((int32_t)((int32_t)L_80+(int32_t)1)), /*hidden argument*/List_1_get_Item_m706652959_MethodInfo_var);
			List_1_t1024024395 * L_82 = V_2;
			int32_t L_83 = V_4;
			NullCheck(L_82);
			TEdge_t3950806139 * L_84 = List_1_get_Item_m706652959(L_82, ((int32_t)((int32_t)L_83-(int32_t)1)), /*hidden argument*/List_1_get_Item_m706652959_MethodInfo_var);
			List_1_t399344435 * L_85 = ___pg0;
			int32_t L_86 = V_4;
			NullCheck(L_85);
			IntPoint_t3326126179  L_87 = List_1_get_Item_m788539143(L_85, L_86, /*hidden argument*/List_1_get_Item_m788539143_MethodInfo_var);
			ClipperBase_InitEdge_m2546155214(__this, L_78, L_81, L_84, L_87, /*hidden argument*/NULL);
			int32_t L_88 = V_4;
			V_4 = ((int32_t)((int32_t)L_88-(int32_t)1));
		}

IL_01a2:
		{
			int32_t L_89 = V_4;
			if ((((int32_t)L_89) >= ((int32_t)1)))
			{
				goto IL_015e;
			}
		}

IL_01aa:
		{
			goto IL_01bd;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_01af;
		throw e;
	}

CATCH_01af:
	{ // begin catch(System.Object)
		{
			V_5 = (bool)0;
			goto IL_058b;
		}

IL_01b8:
		{
			; // IL_01b8: leave IL_01bd
		}
	} // end catch (depth: 1)

IL_01bd:
	{
		List_1_t1024024395 * L_90 = V_2;
		NullCheck(L_90);
		TEdge_t3950806139 * L_91 = List_1_get_Item_m706652959(L_90, 0, /*hidden argument*/List_1_get_Item_m706652959_MethodInfo_var);
		V_6 = L_91;
		bool L_92 = V_1;
		if (L_92)
		{
			goto IL_01da;
		}
	}
	{
		TEdge_t3950806139 * L_93 = V_6;
		NullCheck(L_93);
		TEdge_t3950806139 * L_94 = L_93->get_Prev_12();
		NullCheck(L_94);
		L_94->set_OutIdx_10(((int32_t)-2));
	}

IL_01da:
	{
		TEdge_t3950806139 * L_95 = V_6;
		V_7 = L_95;
		TEdge_t3950806139 * L_96 = V_6;
		V_8 = L_96;
		goto IL_033d;
	}

IL_01e7:
	{
		TEdge_t3950806139 * L_97 = V_7;
		NullCheck(L_97);
		IntPoint_t3326126179  L_98 = L_97->get_Curr_1();
		TEdge_t3950806139 * L_99 = V_7;
		NullCheck(L_99);
		TEdge_t3950806139 * L_100 = L_99->get_Next_11();
		NullCheck(L_100);
		IntPoint_t3326126179  L_101 = L_100->get_Curr_1();
		bool L_102 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_98, L_101, /*hidden argument*/NULL);
		if (!L_102)
		{
			goto IL_023c;
		}
	}
	{
		TEdge_t3950806139 * L_103 = V_7;
		TEdge_t3950806139 * L_104 = V_7;
		NullCheck(L_104);
		TEdge_t3950806139 * L_105 = L_104->get_Next_11();
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_103) == ((Il2CppObject*)(TEdge_t3950806139 *)L_105))))
		{
			goto IL_0217;
		}
	}
	{
		goto IL_0342;
	}

IL_0217:
	{
		TEdge_t3950806139 * L_106 = V_7;
		TEdge_t3950806139 * L_107 = V_6;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_106) == ((Il2CppObject*)(TEdge_t3950806139 *)L_107))))
		{
			goto IL_0229;
		}
	}
	{
		TEdge_t3950806139 * L_108 = V_7;
		NullCheck(L_108);
		TEdge_t3950806139 * L_109 = L_108->get_Next_11();
		V_6 = L_109;
	}

IL_0229:
	{
		TEdge_t3950806139 * L_110 = V_7;
		TEdge_t3950806139 * L_111 = ClipperBase_RemoveEdge_m2040466051(__this, L_110, /*hidden argument*/NULL);
		V_7 = L_111;
		TEdge_t3950806139 * L_112 = V_7;
		V_8 = L_112;
		goto IL_033d;
	}

IL_023c:
	{
		TEdge_t3950806139 * L_113 = V_7;
		NullCheck(L_113);
		TEdge_t3950806139 * L_114 = L_113->get_Prev_12();
		TEdge_t3950806139 * L_115 = V_7;
		NullCheck(L_115);
		TEdge_t3950806139 * L_116 = L_115->get_Next_11();
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_114) == ((Il2CppObject*)(TEdge_t3950806139 *)L_116))))
		{
			goto IL_0254;
		}
	}
	{
		goto IL_0342;
	}

IL_0254:
	{
		bool L_117 = V_1;
		if (L_117)
		{
			goto IL_028e;
		}
	}
	{
		TEdge_t3950806139 * L_118 = V_7;
		NullCheck(L_118);
		TEdge_t3950806139 * L_119 = L_118->get_Prev_12();
		NullCheck(L_119);
		int32_t L_120 = L_119->get_OutIdx_10();
		if ((((int32_t)L_120) == ((int32_t)((int32_t)-2))))
		{
			goto IL_0326;
		}
	}
	{
		TEdge_t3950806139 * L_121 = V_7;
		NullCheck(L_121);
		int32_t L_122 = L_121->get_OutIdx_10();
		if ((((int32_t)L_122) == ((int32_t)((int32_t)-2))))
		{
			goto IL_0326;
		}
	}
	{
		TEdge_t3950806139 * L_123 = V_7;
		NullCheck(L_123);
		TEdge_t3950806139 * L_124 = L_123->get_Next_11();
		NullCheck(L_124);
		int32_t L_125 = L_124->get_OutIdx_10();
		if ((((int32_t)L_125) == ((int32_t)((int32_t)-2))))
		{
			goto IL_0326;
		}
	}

IL_028e:
	{
		TEdge_t3950806139 * L_126 = V_7;
		NullCheck(L_126);
		TEdge_t3950806139 * L_127 = L_126->get_Prev_12();
		NullCheck(L_127);
		IntPoint_t3326126179  L_128 = L_127->get_Curr_1();
		TEdge_t3950806139 * L_129 = V_7;
		NullCheck(L_129);
		IntPoint_t3326126179  L_130 = L_129->get_Curr_1();
		TEdge_t3950806139 * L_131 = V_7;
		NullCheck(L_131);
		TEdge_t3950806139 * L_132 = L_131->get_Next_11();
		NullCheck(L_132);
		IntPoint_t3326126179  L_133 = L_132->get_Curr_1();
		bool L_134 = __this->get_m_UseFullRange_9();
		bool L_135 = ClipperBase_SlopesEqual_m1151992421(NULL /*static, unused*/, L_128, L_130, L_133, L_134, /*hidden argument*/NULL);
		if (!L_135)
		{
			goto IL_0326;
		}
	}
	{
		bool L_136 = ___Closed2;
		if (!L_136)
		{
			goto IL_0326;
		}
	}
	{
		bool L_137 = ClipperBase_get_PreserveCollinear_m701993455(__this, /*hidden argument*/NULL);
		if (!L_137)
		{
			goto IL_02f8;
		}
	}
	{
		TEdge_t3950806139 * L_138 = V_7;
		NullCheck(L_138);
		TEdge_t3950806139 * L_139 = L_138->get_Prev_12();
		NullCheck(L_139);
		IntPoint_t3326126179  L_140 = L_139->get_Curr_1();
		TEdge_t3950806139 * L_141 = V_7;
		NullCheck(L_141);
		IntPoint_t3326126179  L_142 = L_141->get_Curr_1();
		TEdge_t3950806139 * L_143 = V_7;
		NullCheck(L_143);
		TEdge_t3950806139 * L_144 = L_143->get_Next_11();
		NullCheck(L_144);
		IntPoint_t3326126179  L_145 = L_144->get_Curr_1();
		bool L_146 = ClipperBase_Pt2IsBetweenPt1AndPt3_m1930781903(__this, L_140, L_142, L_145, /*hidden argument*/NULL);
		if (L_146)
		{
			goto IL_0326;
		}
	}

IL_02f8:
	{
		TEdge_t3950806139 * L_147 = V_7;
		TEdge_t3950806139 * L_148 = V_6;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_147) == ((Il2CppObject*)(TEdge_t3950806139 *)L_148))))
		{
			goto IL_030a;
		}
	}
	{
		TEdge_t3950806139 * L_149 = V_7;
		NullCheck(L_149);
		TEdge_t3950806139 * L_150 = L_149->get_Next_11();
		V_6 = L_150;
	}

IL_030a:
	{
		TEdge_t3950806139 * L_151 = V_7;
		TEdge_t3950806139 * L_152 = ClipperBase_RemoveEdge_m2040466051(__this, L_151, /*hidden argument*/NULL);
		V_7 = L_152;
		TEdge_t3950806139 * L_153 = V_7;
		NullCheck(L_153);
		TEdge_t3950806139 * L_154 = L_153->get_Prev_12();
		V_7 = L_154;
		TEdge_t3950806139 * L_155 = V_7;
		V_8 = L_155;
		goto IL_033d;
	}

IL_0326:
	{
		TEdge_t3950806139 * L_156 = V_7;
		NullCheck(L_156);
		TEdge_t3950806139 * L_157 = L_156->get_Next_11();
		V_7 = L_157;
		TEdge_t3950806139 * L_158 = V_7;
		TEdge_t3950806139 * L_159 = V_8;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_158) == ((Il2CppObject*)(TEdge_t3950806139 *)L_159))))
		{
			goto IL_033d;
		}
	}
	{
		goto IL_0342;
	}

IL_033d:
	{
		goto IL_01e7;
	}

IL_0342:
	{
		bool L_160 = ___Closed2;
		if (L_160)
		{
			goto IL_0356;
		}
	}
	{
		TEdge_t3950806139 * L_161 = V_7;
		TEdge_t3950806139 * L_162 = V_7;
		NullCheck(L_162);
		TEdge_t3950806139 * L_163 = L_162->get_Next_11();
		if ((((Il2CppObject*)(TEdge_t3950806139 *)L_161) == ((Il2CppObject*)(TEdge_t3950806139 *)L_163)))
		{
			goto IL_036f;
		}
	}

IL_0356:
	{
		bool L_164 = ___Closed2;
		if (!L_164)
		{
			goto IL_0371;
		}
	}
	{
		TEdge_t3950806139 * L_165 = V_7;
		NullCheck(L_165);
		TEdge_t3950806139 * L_166 = L_165->get_Prev_12();
		TEdge_t3950806139 * L_167 = V_7;
		NullCheck(L_167);
		TEdge_t3950806139 * L_168 = L_167->get_Next_11();
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_166) == ((Il2CppObject*)(TEdge_t3950806139 *)L_168))))
		{
			goto IL_0371;
		}
	}

IL_036f:
	{
		return (bool)0;
	}

IL_0371:
	{
		List_1_t2392209947 * L_169 = __this->get_m_edges_8();
		List_1_t1024024395 * L_170 = V_2;
		NullCheck(L_169);
		List_1_Add_m3369625566(L_169, L_170, /*hidden argument*/List_1_Add_m3369625566_MethodInfo_var);
		bool L_171 = ___Closed2;
		if (L_171)
		{
			goto IL_038a;
		}
	}
	{
		__this->set_m_HasOpenPaths_10((bool)1);
	}

IL_038a:
	{
		TEdge_t3950806139 * L_172 = V_6;
		V_9 = L_172;
		TEdge_t3950806139 * L_173 = V_6;
		V_7 = L_173;
	}

IL_0392:
	{
		TEdge_t3950806139 * L_174 = V_7;
		int32_t L_175 = ___polyType1;
		ClipperBase_InitEdge2_m1715435253(__this, L_174, L_175, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_176 = V_7;
		NullCheck(L_176);
		IntPoint_t3326126179 * L_177 = L_176->get_address_of_Top_2();
		int64_t L_178 = L_177->get_Y_1();
		TEdge_t3950806139 * L_179 = V_9;
		NullCheck(L_179);
		IntPoint_t3326126179 * L_180 = L_179->get_address_of_Top_2();
		int64_t L_181 = L_180->get_Y_1();
		if ((((int64_t)L_178) >= ((int64_t)L_181)))
		{
			goto IL_03bc;
		}
	}
	{
		TEdge_t3950806139 * L_182 = V_7;
		V_9 = L_182;
	}

IL_03bc:
	{
		TEdge_t3950806139 * L_183 = V_7;
		NullCheck(L_183);
		TEdge_t3950806139 * L_184 = L_183->get_Next_11();
		V_7 = L_184;
		TEdge_t3950806139 * L_185 = V_7;
		TEdge_t3950806139 * L_186 = V_6;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_185) == ((Il2CppObject*)(TEdge_t3950806139 *)L_186))))
		{
			goto IL_0392;
		}
	}
	{
		TEdge_t3950806139 * L_187 = V_7;
		bool L_188 = ClipperBase_AllHorizontal_m50403071(__this, L_187, /*hidden argument*/NULL);
		if (!L_188)
		{
			goto IL_03fb;
		}
	}
	{
		bool L_189 = V_1;
		if (!L_189)
		{
			goto IL_03ef;
		}
	}
	{
		TEdge_t3950806139 * L_190 = V_7;
		NullCheck(L_190);
		TEdge_t3950806139 * L_191 = L_190->get_Prev_12();
		NullCheck(L_191);
		L_191->set_OutIdx_10(((int32_t)-2));
	}

IL_03ef:
	{
		ClipperBase_AscendToMax_m1097068823(__this, (&V_7), (bool)0, (bool)0, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_03fb:
	{
		TEdge_t3950806139 * L_192 = V_6;
		NullCheck(L_192);
		TEdge_t3950806139 * L_193 = L_192->get_Prev_12();
		V_7 = L_193;
		TEdge_t3950806139 * L_194 = V_7;
		NullCheck(L_194);
		TEdge_t3950806139 * L_195 = L_194->get_Prev_12();
		TEdge_t3950806139 * L_196 = V_7;
		NullCheck(L_196);
		TEdge_t3950806139 * L_197 = L_196->get_Next_11();
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_195) == ((Il2CppObject*)(TEdge_t3950806139 *)L_197))))
		{
			goto IL_0425;
		}
	}
	{
		TEdge_t3950806139 * L_198 = V_7;
		NullCheck(L_198);
		TEdge_t3950806139 * L_199 = L_198->get_Next_11();
		V_9 = L_199;
		goto IL_0571;
	}

IL_0425:
	{
		bool L_200 = V_1;
		if (L_200)
		{
			goto IL_04e4;
		}
	}
	{
		TEdge_t3950806139 * L_201 = V_7;
		NullCheck(L_201);
		IntPoint_t3326126179 * L_202 = L_201->get_address_of_Top_2();
		int64_t L_203 = L_202->get_Y_1();
		TEdge_t3950806139 * L_204 = V_9;
		NullCheck(L_204);
		IntPoint_t3326126179 * L_205 = L_204->get_address_of_Top_2();
		int64_t L_206 = L_205->get_Y_1();
		if ((!(((uint64_t)L_203) == ((uint64_t)L_206))))
		{
			goto IL_04e4;
		}
	}
	{
		TEdge_t3950806139 * L_207 = V_7;
		bool L_208 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_207, /*hidden argument*/NULL);
		if (L_208)
		{
			goto IL_0465;
		}
	}
	{
		TEdge_t3950806139 * L_209 = V_7;
		NullCheck(L_209);
		TEdge_t3950806139 * L_210 = L_209->get_Next_11();
		bool L_211 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_210, /*hidden argument*/NULL);
		if (!L_211)
		{
			goto IL_0495;
		}
	}

IL_0465:
	{
		TEdge_t3950806139 * L_212 = V_7;
		NullCheck(L_212);
		TEdge_t3950806139 * L_213 = L_212->get_Next_11();
		NullCheck(L_213);
		IntPoint_t3326126179 * L_214 = L_213->get_address_of_Bot_0();
		int64_t L_215 = L_214->get_Y_1();
		TEdge_t3950806139 * L_216 = V_9;
		NullCheck(L_216);
		IntPoint_t3326126179 * L_217 = L_216->get_address_of_Top_2();
		int64_t L_218 = L_217->get_Y_1();
		if ((!(((uint64_t)L_215) == ((uint64_t)L_218))))
		{
			goto IL_0495;
		}
	}
	{
		TEdge_t3950806139 * L_219 = V_7;
		NullCheck(L_219);
		TEdge_t3950806139 * L_220 = L_219->get_Next_11();
		V_9 = L_220;
		goto IL_04df;
	}

IL_0495:
	{
		TEdge_t3950806139 * L_221 = V_7;
		bool L_222 = ClipperBase_SharedVertWithPrevAtTop_m4177200205(__this, L_221, /*hidden argument*/NULL);
		if (!L_222)
		{
			goto IL_04ab;
		}
	}
	{
		TEdge_t3950806139 * L_223 = V_7;
		V_9 = L_223;
		goto IL_04df;
	}

IL_04ab:
	{
		TEdge_t3950806139 * L_224 = V_7;
		NullCheck(L_224);
		IntPoint_t3326126179  L_225 = L_224->get_Top_2();
		TEdge_t3950806139 * L_226 = V_7;
		NullCheck(L_226);
		TEdge_t3950806139 * L_227 = L_226->get_Prev_12();
		NullCheck(L_227);
		IntPoint_t3326126179  L_228 = L_227->get_Top_2();
		bool L_229 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_225, L_228, /*hidden argument*/NULL);
		if (!L_229)
		{
			goto IL_04d6;
		}
	}
	{
		TEdge_t3950806139 * L_230 = V_7;
		NullCheck(L_230);
		TEdge_t3950806139 * L_231 = L_230->get_Prev_12();
		V_9 = L_231;
		goto IL_04df;
	}

IL_04d6:
	{
		TEdge_t3950806139 * L_232 = V_7;
		NullCheck(L_232);
		TEdge_t3950806139 * L_233 = L_232->get_Next_11();
		V_9 = L_233;
	}

IL_04df:
	{
		goto IL_0571;
	}

IL_04e4:
	{
		TEdge_t3950806139 * L_234 = V_9;
		V_7 = L_234;
		goto IL_052b;
	}

IL_04ed:
	{
		TEdge_t3950806139 * L_235 = V_9;
		NullCheck(L_235);
		TEdge_t3950806139 * L_236 = L_235->get_Next_11();
		V_9 = L_236;
		TEdge_t3950806139 * L_237 = V_9;
		TEdge_t3950806139 * L_238 = V_7;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_237) == ((Il2CppObject*)(TEdge_t3950806139 *)L_238))))
		{
			goto IL_052b;
		}
	}
	{
		goto IL_050d;
	}

IL_0504:
	{
		TEdge_t3950806139 * L_239 = V_9;
		NullCheck(L_239);
		TEdge_t3950806139 * L_240 = L_239->get_Next_11();
		V_9 = L_240;
	}

IL_050d:
	{
		TEdge_t3950806139 * L_241 = V_9;
		bool L_242 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_241, /*hidden argument*/NULL);
		if (L_242)
		{
			goto IL_0504;
		}
	}
	{
		TEdge_t3950806139 * L_243 = V_9;
		bool L_244 = ClipperBase_SharedVertWithPrevAtTop_m4177200205(__this, L_243, /*hidden argument*/NULL);
		if (!L_244)
		{
			goto IL_0504;
		}
	}
	{
		goto IL_0571;
	}

IL_052b:
	{
		TEdge_t3950806139 * L_245 = V_9;
		bool L_246 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_245, /*hidden argument*/NULL);
		if (L_246)
		{
			goto IL_04ed;
		}
	}
	{
		TEdge_t3950806139 * L_247 = V_9;
		NullCheck(L_247);
		IntPoint_t3326126179  L_248 = L_247->get_Top_2();
		TEdge_t3950806139 * L_249 = V_9;
		NullCheck(L_249);
		TEdge_t3950806139 * L_250 = L_249->get_Next_11();
		NullCheck(L_250);
		IntPoint_t3326126179  L_251 = L_250->get_Top_2();
		bool L_252 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_248, L_251, /*hidden argument*/NULL);
		if (L_252)
		{
			goto IL_04ed;
		}
	}
	{
		TEdge_t3950806139 * L_253 = V_9;
		NullCheck(L_253);
		IntPoint_t3326126179  L_254 = L_253->get_Top_2();
		TEdge_t3950806139 * L_255 = V_9;
		NullCheck(L_255);
		TEdge_t3950806139 * L_256 = L_255->get_Next_11();
		NullCheck(L_256);
		IntPoint_t3326126179  L_257 = L_256->get_Bot_0();
		bool L_258 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_254, L_257, /*hidden argument*/NULL);
		if (L_258)
		{
			goto IL_04ed;
		}
	}

IL_0571:
	{
		TEdge_t3950806139 * L_259 = V_9;
		V_7 = L_259;
	}

IL_0575:
	{
		TEdge_t3950806139 * L_260 = V_7;
		bool L_261 = ___Closed2;
		TEdge_t3950806139 * L_262 = ClipperBase_AddBoundsToLML_m1770302753(__this, L_260, L_261, /*hidden argument*/NULL);
		V_7 = L_262;
		TEdge_t3950806139 * L_263 = V_7;
		TEdge_t3950806139 * L_264 = V_9;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_263) == ((Il2CppObject*)(TEdge_t3950806139 *)L_264))))
		{
			goto IL_0575;
		}
	}
	{
		return (bool)1;
	}

IL_058b:
	{
		bool L_265 = V_5;
		return L_265;
	}
}
// System.Boolean Pathfinding.ClipperLib.ClipperBase::AddPolygon(System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>,Pathfinding.ClipperLib.PolyType)
extern "C"  bool ClipperBase_AddPolygon_m1504479783 (ClipperBase_t512275816 * __this, List_1_t399344435 * ___pg0, int32_t ___polyType1, const MethodInfo* method)
{
	{
		List_1_t399344435 * L_0 = ___pg0;
		int32_t L_1 = ___polyType1;
		bool L_2 = ClipperBase_AddPath_m2901119053(__this, L_0, L_1, (bool)1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean Pathfinding.ClipperLib.ClipperBase::Pt2IsBetweenPt1AndPt3(Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint)
extern "C"  bool ClipperBase_Pt2IsBetweenPt1AndPt3_m1930781903 (ClipperBase_t512275816 * __this, IntPoint_t3326126179  ___pt10, IntPoint_t3326126179  ___pt21, IntPoint_t3326126179  ___pt32, const MethodInfo* method)
{
	{
		IntPoint_t3326126179  L_0 = ___pt10;
		IntPoint_t3326126179  L_1 = ___pt32;
		bool L_2 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		IntPoint_t3326126179  L_3 = ___pt10;
		IntPoint_t3326126179  L_4 = ___pt21;
		bool L_5 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0024;
		}
	}
	{
		IntPoint_t3326126179  L_6 = ___pt32;
		IntPoint_t3326126179  L_7 = ___pt21;
		bool L_8 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0026;
		}
	}

IL_0024:
	{
		return (bool)0;
	}

IL_0026:
	{
		int64_t L_9 = (&___pt10)->get_X_0();
		int64_t L_10 = (&___pt32)->get_X_0();
		if ((((int64_t)L_9) == ((int64_t)L_10)))
		{
			goto IL_005c;
		}
	}
	{
		int64_t L_11 = (&___pt21)->get_X_0();
		int64_t L_12 = (&___pt10)->get_X_0();
		int64_t L_13 = (&___pt21)->get_X_0();
		int64_t L_14 = (&___pt32)->get_X_0();
		return (bool)((((int32_t)((((int64_t)L_11) > ((int64_t)L_12))? 1 : 0)) == ((int32_t)((((int64_t)L_13) < ((int64_t)L_14))? 1 : 0)))? 1 : 0);
	}

IL_005c:
	{
		int64_t L_15 = (&___pt21)->get_Y_1();
		int64_t L_16 = (&___pt10)->get_Y_1();
		int64_t L_17 = (&___pt21)->get_Y_1();
		int64_t L_18 = (&___pt32)->get_Y_1();
		return (bool)((((int32_t)((((int64_t)L_15) > ((int64_t)L_16))? 1 : 0)) == ((int32_t)((((int64_t)L_17) < ((int64_t)L_18))? 1 : 0)))? 1 : 0);
	}
}
// Pathfinding.ClipperLib.TEdge Pathfinding.ClipperLib.ClipperBase::RemoveEdge(Pathfinding.ClipperLib.TEdge)
extern "C"  TEdge_t3950806139 * ClipperBase_RemoveEdge_m2040466051 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___e0, const MethodInfo* method)
{
	TEdge_t3950806139 * V_0 = NULL;
	{
		TEdge_t3950806139 * L_0 = ___e0;
		NullCheck(L_0);
		TEdge_t3950806139 * L_1 = L_0->get_Prev_12();
		TEdge_t3950806139 * L_2 = ___e0;
		NullCheck(L_2);
		TEdge_t3950806139 * L_3 = L_2->get_Next_11();
		NullCheck(L_1);
		L_1->set_Next_11(L_3);
		TEdge_t3950806139 * L_4 = ___e0;
		NullCheck(L_4);
		TEdge_t3950806139 * L_5 = L_4->get_Next_11();
		TEdge_t3950806139 * L_6 = ___e0;
		NullCheck(L_6);
		TEdge_t3950806139 * L_7 = L_6->get_Prev_12();
		NullCheck(L_5);
		L_5->set_Prev_12(L_7);
		TEdge_t3950806139 * L_8 = ___e0;
		NullCheck(L_8);
		TEdge_t3950806139 * L_9 = L_8->get_Next_11();
		V_0 = L_9;
		TEdge_t3950806139 * L_10 = ___e0;
		NullCheck(L_10);
		L_10->set_Prev_12((TEdge_t3950806139 *)NULL);
		TEdge_t3950806139 * L_11 = V_0;
		return L_11;
	}
}
// Pathfinding.ClipperLib.TEdge Pathfinding.ClipperLib.ClipperBase::GetLastHorz(Pathfinding.ClipperLib.TEdge)
extern "C"  TEdge_t3950806139 * ClipperBase_GetLastHorz_m3636788433 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___Edge0, const MethodInfo* method)
{
	TEdge_t3950806139 * V_0 = NULL;
	{
		TEdge_t3950806139 * L_0 = ___Edge0;
		V_0 = L_0;
		goto IL_000e;
	}

IL_0007:
	{
		TEdge_t3950806139 * L_1 = V_0;
		NullCheck(L_1);
		TEdge_t3950806139 * L_2 = L_1->get_Next_11();
		V_0 = L_2;
	}

IL_000e:
	{
		TEdge_t3950806139 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_OutIdx_10();
		if ((((int32_t)L_4) == ((int32_t)((int32_t)-2))))
		{
			goto IL_0037;
		}
	}
	{
		TEdge_t3950806139 * L_5 = V_0;
		NullCheck(L_5);
		TEdge_t3950806139 * L_6 = L_5->get_Next_11();
		TEdge_t3950806139 * L_7 = ___Edge0;
		if ((((Il2CppObject*)(TEdge_t3950806139 *)L_6) == ((Il2CppObject*)(TEdge_t3950806139 *)L_7)))
		{
			goto IL_0037;
		}
	}
	{
		TEdge_t3950806139 * L_8 = V_0;
		NullCheck(L_8);
		TEdge_t3950806139 * L_9 = L_8->get_Next_11();
		bool L_10 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0007;
		}
	}

IL_0037:
	{
		TEdge_t3950806139 * L_11 = V_0;
		return L_11;
	}
}
// System.Boolean Pathfinding.ClipperLib.ClipperBase::SharedVertWithPrevAtTop(Pathfinding.ClipperLib.TEdge)
extern "C"  bool ClipperBase_SharedVertWithPrevAtTop_m4177200205 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___Edge0, const MethodInfo* method)
{
	TEdge_t3950806139 * V_0 = NULL;
	bool V_1 = false;
	{
		TEdge_t3950806139 * L_0 = ___Edge0;
		V_0 = L_0;
		V_1 = (bool)1;
		goto IL_0059;
	}

IL_0009:
	{
		TEdge_t3950806139 * L_1 = V_0;
		NullCheck(L_1);
		IntPoint_t3326126179  L_2 = L_1->get_Top_2();
		TEdge_t3950806139 * L_3 = V_0;
		NullCheck(L_3);
		TEdge_t3950806139 * L_4 = L_3->get_Prev_12();
		NullCheck(L_4);
		IntPoint_t3326126179  L_5 = L_4->get_Top_2();
		bool L_6 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0052;
		}
	}
	{
		TEdge_t3950806139 * L_7 = V_0;
		NullCheck(L_7);
		IntPoint_t3326126179  L_8 = L_7->get_Bot_0();
		TEdge_t3950806139 * L_9 = V_0;
		NullCheck(L_9);
		TEdge_t3950806139 * L_10 = L_9->get_Prev_12();
		NullCheck(L_10);
		IntPoint_t3326126179  L_11 = L_10->get_Bot_0();
		bool L_12 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_8, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_004b;
		}
	}
	{
		TEdge_t3950806139 * L_13 = V_0;
		NullCheck(L_13);
		TEdge_t3950806139 * L_14 = L_13->get_Prev_12();
		V_0 = L_14;
		goto IL_0059;
	}

IL_004b:
	{
		V_1 = (bool)1;
		goto IL_0054;
	}

IL_0052:
	{
		V_1 = (bool)0;
	}

IL_0054:
	{
		goto IL_0065;
	}

IL_0059:
	{
		TEdge_t3950806139 * L_15 = V_0;
		NullCheck(L_15);
		TEdge_t3950806139 * L_16 = L_15->get_Prev_12();
		TEdge_t3950806139 * L_17 = ___Edge0;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_16) == ((Il2CppObject*)(TEdge_t3950806139 *)L_17))))
		{
			goto IL_0009;
		}
	}

IL_0065:
	{
		goto IL_0076;
	}

IL_006a:
	{
		bool L_18 = V_1;
		V_1 = (bool)((((int32_t)L_18) == ((int32_t)0))? 1 : 0);
		TEdge_t3950806139 * L_19 = V_0;
		NullCheck(L_19);
		TEdge_t3950806139 * L_20 = L_19->get_Next_11();
		V_0 = L_20;
	}

IL_0076:
	{
		TEdge_t3950806139 * L_21 = V_0;
		TEdge_t3950806139 * L_22 = ___Edge0;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_21) == ((Il2CppObject*)(TEdge_t3950806139 *)L_22))))
		{
			goto IL_006a;
		}
	}
	{
		bool L_23 = V_1;
		return L_23;
	}
}
// System.Boolean Pathfinding.ClipperLib.ClipperBase::SharedVertWithNextIsBot(Pathfinding.ClipperLib.TEdge)
extern "C"  bool ClipperBase_SharedVertWithNextIsBot_m1994098280 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___Edge0, const MethodInfo* method)
{
	bool V_0 = false;
	TEdge_t3950806139 * V_1 = NULL;
	bool V_2 = false;
	bool V_3 = false;
	{
		V_0 = (bool)1;
		TEdge_t3950806139 * L_0 = ___Edge0;
		V_1 = L_0;
		goto IL_0088;
	}

IL_0009:
	{
		TEdge_t3950806139 * L_1 = V_1;
		NullCheck(L_1);
		TEdge_t3950806139 * L_2 = L_1->get_Next_11();
		NullCheck(L_2);
		IntPoint_t3326126179  L_3 = L_2->get_Bot_0();
		TEdge_t3950806139 * L_4 = V_1;
		NullCheck(L_4);
		IntPoint_t3326126179  L_5 = L_4->get_Bot_0();
		bool L_6 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		TEdge_t3950806139 * L_7 = V_1;
		NullCheck(L_7);
		TEdge_t3950806139 * L_8 = L_7->get_Prev_12();
		NullCheck(L_8);
		IntPoint_t3326126179  L_9 = L_8->get_Bot_0();
		TEdge_t3950806139 * L_10 = V_1;
		NullCheck(L_10);
		IntPoint_t3326126179  L_11 = L_10->get_Bot_0();
		bool L_12 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		bool L_13 = V_2;
		bool L_14 = V_3;
		if ((((int32_t)L_13) == ((int32_t)L_14)))
		{
			goto IL_0045;
		}
	}
	{
		bool L_15 = V_2;
		V_0 = L_15;
		goto IL_0094;
	}

IL_0045:
	{
		TEdge_t3950806139 * L_16 = V_1;
		NullCheck(L_16);
		TEdge_t3950806139 * L_17 = L_16->get_Next_11();
		NullCheck(L_17);
		IntPoint_t3326126179  L_18 = L_17->get_Top_2();
		TEdge_t3950806139 * L_19 = V_1;
		NullCheck(L_19);
		IntPoint_t3326126179  L_20 = L_19->get_Top_2();
		bool L_21 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_18, L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		TEdge_t3950806139 * L_22 = V_1;
		NullCheck(L_22);
		TEdge_t3950806139 * L_23 = L_22->get_Prev_12();
		NullCheck(L_23);
		IntPoint_t3326126179  L_24 = L_23->get_Top_2();
		TEdge_t3950806139 * L_25 = V_1;
		NullCheck(L_25);
		IntPoint_t3326126179  L_26 = L_25->get_Top_2();
		bool L_27 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_24, L_26, /*hidden argument*/NULL);
		V_3 = L_27;
		bool L_28 = V_2;
		bool L_29 = V_3;
		if ((((int32_t)L_28) == ((int32_t)L_29)))
		{
			goto IL_0081;
		}
	}
	{
		bool L_30 = V_3;
		V_0 = L_30;
		goto IL_0094;
	}

IL_0081:
	{
		TEdge_t3950806139 * L_31 = V_1;
		NullCheck(L_31);
		TEdge_t3950806139 * L_32 = L_31->get_Prev_12();
		V_1 = L_32;
	}

IL_0088:
	{
		TEdge_t3950806139 * L_33 = V_1;
		NullCheck(L_33);
		TEdge_t3950806139 * L_34 = L_33->get_Prev_12();
		TEdge_t3950806139 * L_35 = ___Edge0;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_34) == ((Il2CppObject*)(TEdge_t3950806139 *)L_35))))
		{
			goto IL_0009;
		}
	}

IL_0094:
	{
		goto IL_00a5;
	}

IL_0099:
	{
		bool L_36 = V_0;
		V_0 = (bool)((((int32_t)L_36) == ((int32_t)0))? 1 : 0);
		TEdge_t3950806139 * L_37 = V_1;
		NullCheck(L_37);
		TEdge_t3950806139 * L_38 = L_37->get_Next_11();
		V_1 = L_38;
	}

IL_00a5:
	{
		TEdge_t3950806139 * L_39 = V_1;
		TEdge_t3950806139 * L_40 = ___Edge0;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_39) == ((Il2CppObject*)(TEdge_t3950806139 *)L_40))))
		{
			goto IL_0099;
		}
	}
	{
		bool L_41 = V_0;
		return L_41;
	}
}
// System.Boolean Pathfinding.ClipperLib.ClipperBase::MoreBelow(Pathfinding.ClipperLib.TEdge)
extern "C"  bool ClipperBase_MoreBelow_m3190791414 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___Edge0, const MethodInfo* method)
{
	TEdge_t3950806139 * V_0 = NULL;
	{
		TEdge_t3950806139 * L_0 = ___Edge0;
		V_0 = L_0;
		TEdge_t3950806139 * L_1 = V_0;
		bool L_2 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0047;
		}
	}
	{
		goto IL_0019;
	}

IL_0012:
	{
		TEdge_t3950806139 * L_3 = V_0;
		NullCheck(L_3);
		TEdge_t3950806139 * L_4 = L_3->get_Next_11();
		V_0 = L_4;
	}

IL_0019:
	{
		TEdge_t3950806139 * L_5 = V_0;
		NullCheck(L_5);
		TEdge_t3950806139 * L_6 = L_5->get_Next_11();
		bool L_7 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0012;
		}
	}
	{
		TEdge_t3950806139 * L_8 = V_0;
		NullCheck(L_8);
		TEdge_t3950806139 * L_9 = L_8->get_Next_11();
		NullCheck(L_9);
		IntPoint_t3326126179 * L_10 = L_9->get_address_of_Bot_0();
		int64_t L_11 = L_10->get_Y_1();
		TEdge_t3950806139 * L_12 = V_0;
		NullCheck(L_12);
		IntPoint_t3326126179 * L_13 = L_12->get_address_of_Bot_0();
		int64_t L_14 = L_13->get_Y_1();
		return (bool)((((int64_t)L_11) > ((int64_t)L_14))? 1 : 0);
	}

IL_0047:
	{
		TEdge_t3950806139 * L_15 = V_0;
		NullCheck(L_15);
		TEdge_t3950806139 * L_16 = L_15->get_Next_11();
		bool L_17 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0091;
		}
	}
	{
		goto IL_0063;
	}

IL_005c:
	{
		TEdge_t3950806139 * L_18 = V_0;
		NullCheck(L_18);
		TEdge_t3950806139 * L_19 = L_18->get_Next_11();
		V_0 = L_19;
	}

IL_0063:
	{
		TEdge_t3950806139 * L_20 = V_0;
		NullCheck(L_20);
		TEdge_t3950806139 * L_21 = L_20->get_Next_11();
		bool L_22 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_005c;
		}
	}
	{
		TEdge_t3950806139 * L_23 = V_0;
		NullCheck(L_23);
		TEdge_t3950806139 * L_24 = L_23->get_Next_11();
		NullCheck(L_24);
		IntPoint_t3326126179 * L_25 = L_24->get_address_of_Bot_0();
		int64_t L_26 = L_25->get_Y_1();
		TEdge_t3950806139 * L_27 = V_0;
		NullCheck(L_27);
		IntPoint_t3326126179 * L_28 = L_27->get_address_of_Bot_0();
		int64_t L_29 = L_28->get_Y_1();
		return (bool)((((int64_t)L_26) > ((int64_t)L_29))? 1 : 0);
	}

IL_0091:
	{
		TEdge_t3950806139 * L_30 = V_0;
		NullCheck(L_30);
		IntPoint_t3326126179  L_31 = L_30->get_Bot_0();
		TEdge_t3950806139 * L_32 = V_0;
		NullCheck(L_32);
		TEdge_t3950806139 * L_33 = L_32->get_Next_11();
		NullCheck(L_33);
		IntPoint_t3326126179  L_34 = L_33->get_Top_2();
		bool L_35 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_31, L_34, /*hidden argument*/NULL);
		return L_35;
	}
}
// System.Boolean Pathfinding.ClipperLib.ClipperBase::JustBeforeLocMin(Pathfinding.ClipperLib.TEdge)
extern "C"  bool ClipperBase_JustBeforeLocMin_m120078395 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___Edge0, const MethodInfo* method)
{
	TEdge_t3950806139 * V_0 = NULL;
	{
		TEdge_t3950806139 * L_0 = ___Edge0;
		V_0 = L_0;
		TEdge_t3950806139 * L_1 = V_0;
		bool L_2 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0047;
		}
	}
	{
		goto IL_0019;
	}

IL_0012:
	{
		TEdge_t3950806139 * L_3 = V_0;
		NullCheck(L_3);
		TEdge_t3950806139 * L_4 = L_3->get_Next_11();
		V_0 = L_4;
	}

IL_0019:
	{
		TEdge_t3950806139 * L_5 = V_0;
		NullCheck(L_5);
		TEdge_t3950806139 * L_6 = L_5->get_Next_11();
		bool L_7 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0012;
		}
	}
	{
		TEdge_t3950806139 * L_8 = V_0;
		NullCheck(L_8);
		TEdge_t3950806139 * L_9 = L_8->get_Next_11();
		NullCheck(L_9);
		IntPoint_t3326126179 * L_10 = L_9->get_address_of_Top_2();
		int64_t L_11 = L_10->get_Y_1();
		TEdge_t3950806139 * L_12 = V_0;
		NullCheck(L_12);
		IntPoint_t3326126179 * L_13 = L_12->get_address_of_Bot_0();
		int64_t L_14 = L_13->get_Y_1();
		return (bool)((((int64_t)L_11) < ((int64_t)L_14))? 1 : 0);
	}

IL_0047:
	{
		TEdge_t3950806139 * L_15 = V_0;
		bool L_16 = ClipperBase_SharedVertWithNextIsBot_m1994098280(__this, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Boolean Pathfinding.ClipperLib.ClipperBase::MoreAbove(Pathfinding.ClipperLib.TEdge)
extern "C"  bool ClipperBase_MoreAbove_m1517449058 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___Edge0, const MethodInfo* method)
{
	{
		TEdge_t3950806139 * L_0 = ___Edge0;
		bool L_1 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		TEdge_t3950806139 * L_2 = ___Edge0;
		TEdge_t3950806139 * L_3 = ClipperBase_GetLastHorz_m3636788433(__this, L_2, /*hidden argument*/NULL);
		___Edge0 = L_3;
		TEdge_t3950806139 * L_4 = ___Edge0;
		NullCheck(L_4);
		TEdge_t3950806139 * L_5 = L_4->get_Next_11();
		NullCheck(L_5);
		IntPoint_t3326126179 * L_6 = L_5->get_address_of_Top_2();
		int64_t L_7 = L_6->get_Y_1();
		TEdge_t3950806139 * L_8 = ___Edge0;
		NullCheck(L_8);
		IntPoint_t3326126179 * L_9 = L_8->get_address_of_Top_2();
		int64_t L_10 = L_9->get_Y_1();
		return (bool)((((int64_t)L_7) < ((int64_t)L_10))? 1 : 0);
	}

IL_0032:
	{
		TEdge_t3950806139 * L_11 = ___Edge0;
		NullCheck(L_11);
		TEdge_t3950806139 * L_12 = L_11->get_Next_11();
		bool L_13 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_006e;
		}
	}
	{
		TEdge_t3950806139 * L_14 = ___Edge0;
		NullCheck(L_14);
		TEdge_t3950806139 * L_15 = L_14->get_Next_11();
		TEdge_t3950806139 * L_16 = ClipperBase_GetLastHorz_m3636788433(__this, L_15, /*hidden argument*/NULL);
		___Edge0 = L_16;
		TEdge_t3950806139 * L_17 = ___Edge0;
		NullCheck(L_17);
		TEdge_t3950806139 * L_18 = L_17->get_Next_11();
		NullCheck(L_18);
		IntPoint_t3326126179 * L_19 = L_18->get_address_of_Top_2();
		int64_t L_20 = L_19->get_Y_1();
		TEdge_t3950806139 * L_21 = ___Edge0;
		NullCheck(L_21);
		IntPoint_t3326126179 * L_22 = L_21->get_address_of_Top_2();
		int64_t L_23 = L_22->get_Y_1();
		return (bool)((((int64_t)L_20) < ((int64_t)L_23))? 1 : 0);
	}

IL_006e:
	{
		TEdge_t3950806139 * L_24 = ___Edge0;
		NullCheck(L_24);
		TEdge_t3950806139 * L_25 = L_24->get_Next_11();
		NullCheck(L_25);
		IntPoint_t3326126179 * L_26 = L_25->get_address_of_Top_2();
		int64_t L_27 = L_26->get_Y_1();
		TEdge_t3950806139 * L_28 = ___Edge0;
		NullCheck(L_28);
		IntPoint_t3326126179 * L_29 = L_28->get_address_of_Top_2();
		int64_t L_30 = L_29->get_Y_1();
		return (bool)((((int64_t)L_27) < ((int64_t)L_30))? 1 : 0);
	}
}
// System.Boolean Pathfinding.ClipperLib.ClipperBase::AllHorizontal(Pathfinding.ClipperLib.TEdge)
extern "C"  bool ClipperBase_AllHorizontal_m50403071 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___Edge0, const MethodInfo* method)
{
	TEdge_t3950806139 * V_0 = NULL;
	{
		TEdge_t3950806139 * L_0 = ___Edge0;
		bool L_1 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		TEdge_t3950806139 * L_2 = ___Edge0;
		NullCheck(L_2);
		TEdge_t3950806139 * L_3 = L_2->get_Next_11();
		V_0 = L_3;
		goto IL_002d;
	}

IL_0019:
	{
		TEdge_t3950806139 * L_4 = V_0;
		bool L_5 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0026;
		}
	}
	{
		return (bool)0;
	}

IL_0026:
	{
		TEdge_t3950806139 * L_6 = V_0;
		NullCheck(L_6);
		TEdge_t3950806139 * L_7 = L_6->get_Next_11();
		V_0 = L_7;
	}

IL_002d:
	{
		TEdge_t3950806139 * L_8 = V_0;
		TEdge_t3950806139 * L_9 = ___Edge0;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_8) == ((Il2CppObject*)(TEdge_t3950806139 *)L_9))))
		{
			goto IL_0019;
		}
	}
	{
		return (bool)1;
	}
}
// System.Void Pathfinding.ClipperLib.ClipperBase::SetDx(Pathfinding.ClipperLib.TEdge)
extern "C"  void ClipperBase_SetDx_m807634188 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___e0, const MethodInfo* method)
{
	{
		TEdge_t3950806139 * L_0 = ___e0;
		NullCheck(L_0);
		IntPoint_t3326126179 * L_1 = L_0->get_address_of_Delta_3();
		TEdge_t3950806139 * L_2 = ___e0;
		NullCheck(L_2);
		IntPoint_t3326126179 * L_3 = L_2->get_address_of_Top_2();
		int64_t L_4 = L_3->get_X_0();
		TEdge_t3950806139 * L_5 = ___e0;
		NullCheck(L_5);
		IntPoint_t3326126179 * L_6 = L_5->get_address_of_Bot_0();
		int64_t L_7 = L_6->get_X_0();
		L_1->set_X_0(((int64_t)((int64_t)L_4-(int64_t)L_7)));
		TEdge_t3950806139 * L_8 = ___e0;
		NullCheck(L_8);
		IntPoint_t3326126179 * L_9 = L_8->get_address_of_Delta_3();
		TEdge_t3950806139 * L_10 = ___e0;
		NullCheck(L_10);
		IntPoint_t3326126179 * L_11 = L_10->get_address_of_Top_2();
		int64_t L_12 = L_11->get_Y_1();
		TEdge_t3950806139 * L_13 = ___e0;
		NullCheck(L_13);
		IntPoint_t3326126179 * L_14 = L_13->get_address_of_Bot_0();
		int64_t L_15 = L_14->get_Y_1();
		L_9->set_Y_1(((int64_t)((int64_t)L_12-(int64_t)L_15)));
		TEdge_t3950806139 * L_16 = ___e0;
		NullCheck(L_16);
		IntPoint_t3326126179 * L_17 = L_16->get_address_of_Delta_3();
		int64_t L_18 = L_17->get_Y_1();
		if ((!(((uint64_t)L_18) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_006a;
		}
	}
	{
		TEdge_t3950806139 * L_19 = ___e0;
		NullCheck(L_19);
		L_19->set_Dx_4((-3.4E+38));
		goto IL_0089;
	}

IL_006a:
	{
		TEdge_t3950806139 * L_20 = ___e0;
		TEdge_t3950806139 * L_21 = ___e0;
		NullCheck(L_21);
		IntPoint_t3326126179 * L_22 = L_21->get_address_of_Delta_3();
		int64_t L_23 = L_22->get_X_0();
		TEdge_t3950806139 * L_24 = ___e0;
		NullCheck(L_24);
		IntPoint_t3326126179 * L_25 = L_24->get_address_of_Delta_3();
		int64_t L_26 = L_25->get_Y_1();
		NullCheck(L_20);
		L_20->set_Dx_4(((double)((double)(((double)((double)L_23)))/(double)(((double)((double)L_26))))));
	}

IL_0089:
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.ClipperBase::DoMinimaLML(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge,System.Boolean)
extern Il2CppClass* LocalMinima_t2863342752_il2cpp_TypeInfo_var;
extern const uint32_t ClipperBase_DoMinimaLML_m1736312807_MetadataUsageId;
extern "C"  void ClipperBase_DoMinimaLML_m1736312807 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___E10, TEdge_t3950806139 * ___E21, bool ___IsClosed2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClipperBase_DoMinimaLML_m1736312807_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LocalMinima_t2863342752 * V_0 = NULL;
	LocalMinima_t2863342752 * V_1 = NULL;
	{
		TEdge_t3950806139 * L_0 = ___E10;
		if (L_0)
		{
			goto IL_004c;
		}
	}
	{
		TEdge_t3950806139 * L_1 = ___E21;
		if (L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		LocalMinima_t2863342752 * L_2 = (LocalMinima_t2863342752 *)il2cpp_codegen_object_new(LocalMinima_t2863342752_il2cpp_TypeInfo_var);
		LocalMinima__ctor_m1959824229(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		LocalMinima_t2863342752 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_Next_3((LocalMinima_t2863342752 *)NULL);
		LocalMinima_t2863342752 * L_4 = V_0;
		TEdge_t3950806139 * L_5 = ___E21;
		NullCheck(L_5);
		IntPoint_t3326126179 * L_6 = L_5->get_address_of_Bot_0();
		int64_t L_7 = L_6->get_Y_1();
		NullCheck(L_4);
		L_4->set_Y_0(L_7);
		LocalMinima_t2863342752 * L_8 = V_0;
		NullCheck(L_8);
		L_8->set_LeftBound_1((TEdge_t3950806139 *)NULL);
		TEdge_t3950806139 * L_9 = ___E21;
		NullCheck(L_9);
		L_9->set_WindDelta_7(0);
		LocalMinima_t2863342752 * L_10 = V_0;
		TEdge_t3950806139 * L_11 = ___E21;
		NullCheck(L_10);
		L_10->set_RightBound_2(L_11);
		LocalMinima_t2863342752 * L_12 = V_0;
		ClipperBase_InsertLocalMinima_m2132432750(__this, L_12, /*hidden argument*/NULL);
		goto IL_015c;
	}

IL_004c:
	{
		LocalMinima_t2863342752 * L_13 = (LocalMinima_t2863342752 *)il2cpp_codegen_object_new(LocalMinima_t2863342752_il2cpp_TypeInfo_var);
		LocalMinima__ctor_m1959824229(L_13, /*hidden argument*/NULL);
		V_1 = L_13;
		LocalMinima_t2863342752 * L_14 = V_1;
		TEdge_t3950806139 * L_15 = ___E10;
		NullCheck(L_15);
		IntPoint_t3326126179 * L_16 = L_15->get_address_of_Bot_0();
		int64_t L_17 = L_16->get_Y_1();
		NullCheck(L_14);
		L_14->set_Y_0(L_17);
		LocalMinima_t2863342752 * L_18 = V_1;
		NullCheck(L_18);
		L_18->set_Next_3((LocalMinima_t2863342752 *)NULL);
		TEdge_t3950806139 * L_19 = ___E21;
		bool L_20 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00aa;
		}
	}
	{
		TEdge_t3950806139 * L_21 = ___E21;
		NullCheck(L_21);
		IntPoint_t3326126179 * L_22 = L_21->get_address_of_Bot_0();
		int64_t L_23 = L_22->get_X_0();
		TEdge_t3950806139 * L_24 = ___E10;
		NullCheck(L_24);
		IntPoint_t3326126179 * L_25 = L_24->get_address_of_Bot_0();
		int64_t L_26 = L_25->get_X_0();
		if ((((int64_t)L_23) == ((int64_t)L_26)))
		{
			goto IL_0097;
		}
	}
	{
		TEdge_t3950806139 * L_27 = ___E21;
		ClipperBase_ReverseHorizontal_m3767571996(__this, L_27, /*hidden argument*/NULL);
	}

IL_0097:
	{
		LocalMinima_t2863342752 * L_28 = V_1;
		TEdge_t3950806139 * L_29 = ___E10;
		NullCheck(L_28);
		L_28->set_LeftBound_1(L_29);
		LocalMinima_t2863342752 * L_30 = V_1;
		TEdge_t3950806139 * L_31 = ___E21;
		NullCheck(L_30);
		L_30->set_RightBound_2(L_31);
		goto IL_00dc;
	}

IL_00aa:
	{
		TEdge_t3950806139 * L_32 = ___E21;
		NullCheck(L_32);
		double L_33 = L_32->get_Dx_4();
		TEdge_t3950806139 * L_34 = ___E10;
		NullCheck(L_34);
		double L_35 = L_34->get_Dx_4();
		if ((!(((double)L_33) < ((double)L_35))))
		{
			goto IL_00ce;
		}
	}
	{
		LocalMinima_t2863342752 * L_36 = V_1;
		TEdge_t3950806139 * L_37 = ___E10;
		NullCheck(L_36);
		L_36->set_LeftBound_1(L_37);
		LocalMinima_t2863342752 * L_38 = V_1;
		TEdge_t3950806139 * L_39 = ___E21;
		NullCheck(L_38);
		L_38->set_RightBound_2(L_39);
		goto IL_00dc;
	}

IL_00ce:
	{
		LocalMinima_t2863342752 * L_40 = V_1;
		TEdge_t3950806139 * L_41 = ___E21;
		NullCheck(L_40);
		L_40->set_LeftBound_1(L_41);
		LocalMinima_t2863342752 * L_42 = V_1;
		TEdge_t3950806139 * L_43 = ___E10;
		NullCheck(L_42);
		L_42->set_RightBound_2(L_43);
	}

IL_00dc:
	{
		LocalMinima_t2863342752 * L_44 = V_1;
		NullCheck(L_44);
		TEdge_t3950806139 * L_45 = L_44->get_LeftBound_1();
		NullCheck(L_45);
		L_45->set_Side_6(0);
		LocalMinima_t2863342752 * L_46 = V_1;
		NullCheck(L_46);
		TEdge_t3950806139 * L_47 = L_46->get_RightBound_2();
		NullCheck(L_47);
		L_47->set_Side_6(1);
		bool L_48 = ___IsClosed2;
		if (L_48)
		{
			goto IL_010b;
		}
	}
	{
		LocalMinima_t2863342752 * L_49 = V_1;
		NullCheck(L_49);
		TEdge_t3950806139 * L_50 = L_49->get_LeftBound_1();
		NullCheck(L_50);
		L_50->set_WindDelta_7(0);
		goto IL_013e;
	}

IL_010b:
	{
		LocalMinima_t2863342752 * L_51 = V_1;
		NullCheck(L_51);
		TEdge_t3950806139 * L_52 = L_51->get_LeftBound_1();
		NullCheck(L_52);
		TEdge_t3950806139 * L_53 = L_52->get_Next_11();
		LocalMinima_t2863342752 * L_54 = V_1;
		NullCheck(L_54);
		TEdge_t3950806139 * L_55 = L_54->get_RightBound_2();
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)L_53) == ((Il2CppObject*)(TEdge_t3950806139 *)L_55))))
		{
			goto IL_0132;
		}
	}
	{
		LocalMinima_t2863342752 * L_56 = V_1;
		NullCheck(L_56);
		TEdge_t3950806139 * L_57 = L_56->get_LeftBound_1();
		NullCheck(L_57);
		L_57->set_WindDelta_7((-1));
		goto IL_013e;
	}

IL_0132:
	{
		LocalMinima_t2863342752 * L_58 = V_1;
		NullCheck(L_58);
		TEdge_t3950806139 * L_59 = L_58->get_LeftBound_1();
		NullCheck(L_59);
		L_59->set_WindDelta_7(1);
	}

IL_013e:
	{
		LocalMinima_t2863342752 * L_60 = V_1;
		NullCheck(L_60);
		TEdge_t3950806139 * L_61 = L_60->get_RightBound_2();
		LocalMinima_t2863342752 * L_62 = V_1;
		NullCheck(L_62);
		TEdge_t3950806139 * L_63 = L_62->get_LeftBound_1();
		NullCheck(L_63);
		int32_t L_64 = L_63->get_WindDelta_7();
		NullCheck(L_61);
		L_61->set_WindDelta_7(((-L_64)));
		LocalMinima_t2863342752 * L_65 = V_1;
		ClipperBase_InsertLocalMinima_m2132432750(__this, L_65, /*hidden argument*/NULL);
	}

IL_015c:
	{
		return;
	}
}
// Pathfinding.ClipperLib.TEdge Pathfinding.ClipperLib.ClipperBase::DescendToMin(Pathfinding.ClipperLib.TEdge&)
extern "C"  TEdge_t3950806139 * ClipperBase_DescendToMin_m3585442493 (ClipperBase_t512275816 * __this, TEdge_t3950806139 ** ___E0, const MethodInfo* method)
{
	TEdge_t3950806139 * V_0 = NULL;
	{
		TEdge_t3950806139 ** L_0 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_0)));
		(*((TEdge_t3950806139 **)L_0))->set_NextInLML_13((TEdge_t3950806139 *)NULL);
		TEdge_t3950806139 ** L_1 = ___E0;
		bool L_2 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, (*((TEdge_t3950806139 **)L_1)), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0056;
		}
	}
	{
		TEdge_t3950806139 ** L_3 = ___E0;
		V_0 = (*((TEdge_t3950806139 **)L_3));
		goto IL_0023;
	}

IL_001c:
	{
		TEdge_t3950806139 * L_4 = V_0;
		NullCheck(L_4);
		TEdge_t3950806139 * L_5 = L_4->get_Next_11();
		V_0 = L_5;
	}

IL_0023:
	{
		TEdge_t3950806139 * L_6 = V_0;
		NullCheck(L_6);
		TEdge_t3950806139 * L_7 = L_6->get_Next_11();
		bool L_8 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_001c;
		}
	}
	{
		TEdge_t3950806139 * L_9 = V_0;
		NullCheck(L_9);
		IntPoint_t3326126179  L_10 = L_9->get_Bot_0();
		TEdge_t3950806139 * L_11 = V_0;
		NullCheck(L_11);
		TEdge_t3950806139 * L_12 = L_11->get_Next_11();
		NullCheck(L_12);
		IntPoint_t3326126179  L_13 = L_12->get_Top_2();
		bool L_14 = IntPoint_op_Inequality_m3996904515(NULL /*static, unused*/, L_10, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0056;
		}
	}
	{
		TEdge_t3950806139 ** L_15 = ___E0;
		ClipperBase_ReverseHorizontal_m3767571996(__this, (*((TEdge_t3950806139 **)L_15)), /*hidden argument*/NULL);
	}

IL_0056:
	{
		goto IL_01ab;
	}

IL_005b:
	{
		TEdge_t3950806139 ** L_16 = ___E0;
		TEdge_t3950806139 ** L_17 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_17)));
		TEdge_t3950806139 * L_18 = (*((TEdge_t3950806139 **)L_17))->get_Next_11();
		*((Il2CppObject **)(L_16)) = (Il2CppObject *)L_18;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_16), (Il2CppObject *)L_18);
		TEdge_t3950806139 ** L_19 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_19)));
		int32_t L_20 = (*((TEdge_t3950806139 **)L_19))->get_OutIdx_10();
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0077;
		}
	}
	{
		goto IL_01b0;
	}

IL_0077:
	{
		TEdge_t3950806139 ** L_21 = ___E0;
		bool L_22 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, (*((TEdge_t3950806139 **)L_21)), /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0176;
		}
	}
	{
		TEdge_t3950806139 ** L_23 = ___E0;
		TEdge_t3950806139 * L_24 = ClipperBase_GetLastHorz_m3636788433(__this, (*((TEdge_t3950806139 **)L_23)), /*hidden argument*/NULL);
		V_0 = L_24;
		TEdge_t3950806139 * L_25 = V_0;
		TEdge_t3950806139 ** L_26 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_26)));
		TEdge_t3950806139 * L_27 = (*((TEdge_t3950806139 **)L_26))->get_Prev_12();
		if ((((Il2CppObject*)(TEdge_t3950806139 *)L_25) == ((Il2CppObject*)(TEdge_t3950806139 *)L_27)))
		{
			goto IL_00e0;
		}
	}
	{
		TEdge_t3950806139 * L_28 = V_0;
		NullCheck(L_28);
		TEdge_t3950806139 * L_29 = L_28->get_Next_11();
		NullCheck(L_29);
		IntPoint_t3326126179 * L_30 = L_29->get_address_of_Top_2();
		int64_t L_31 = L_30->get_Y_1();
		TEdge_t3950806139 ** L_32 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_32)));
		IntPoint_t3326126179 * L_33 = (*((TEdge_t3950806139 **)L_32))->get_address_of_Top_2();
		int64_t L_34 = L_33->get_Y_1();
		if ((((int64_t)L_31) >= ((int64_t)L_34)))
		{
			goto IL_00e5;
		}
	}
	{
		TEdge_t3950806139 * L_35 = V_0;
		NullCheck(L_35);
		TEdge_t3950806139 * L_36 = L_35->get_Next_11();
		NullCheck(L_36);
		IntPoint_t3326126179 * L_37 = L_36->get_address_of_Bot_0();
		int64_t L_38 = L_37->get_X_0();
		TEdge_t3950806139 ** L_39 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_39)));
		TEdge_t3950806139 * L_40 = (*((TEdge_t3950806139 **)L_39))->get_Prev_12();
		NullCheck(L_40);
		IntPoint_t3326126179 * L_41 = L_40->get_address_of_Bot_0();
		int64_t L_42 = L_41->get_X_0();
		if ((((int64_t)L_38) <= ((int64_t)L_42)))
		{
			goto IL_00e5;
		}
	}

IL_00e0:
	{
		goto IL_01b0;
	}

IL_00e5:
	{
		TEdge_t3950806139 ** L_43 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_43)));
		IntPoint_t3326126179 * L_44 = (*((TEdge_t3950806139 **)L_43))->get_address_of_Top_2();
		int64_t L_45 = L_44->get_X_0();
		TEdge_t3950806139 ** L_46 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_46)));
		TEdge_t3950806139 * L_47 = (*((TEdge_t3950806139 **)L_46))->get_Prev_12();
		NullCheck(L_47);
		IntPoint_t3326126179 * L_48 = L_47->get_address_of_Bot_0();
		int64_t L_49 = L_48->get_X_0();
		if ((((int64_t)L_45) == ((int64_t)L_49)))
		{
			goto IL_010f;
		}
	}
	{
		TEdge_t3950806139 ** L_50 = ___E0;
		ClipperBase_ReverseHorizontal_m3767571996(__this, (*((TEdge_t3950806139 **)L_50)), /*hidden argument*/NULL);
	}

IL_010f:
	{
		TEdge_t3950806139 * L_51 = V_0;
		NullCheck(L_51);
		int32_t L_52 = L_51->get_OutIdx_10();
		if ((!(((uint32_t)L_52) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0123;
		}
	}
	{
		TEdge_t3950806139 * L_53 = V_0;
		NullCheck(L_53);
		TEdge_t3950806139 * L_54 = L_53->get_Prev_12();
		V_0 = L_54;
	}

IL_0123:
	{
		goto IL_0169;
	}

IL_0128:
	{
		TEdge_t3950806139 ** L_55 = ___E0;
		TEdge_t3950806139 ** L_56 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_56)));
		TEdge_t3950806139 * L_57 = (*((TEdge_t3950806139 **)L_56))->get_Prev_12();
		NullCheck((*((TEdge_t3950806139 **)L_55)));
		(*((TEdge_t3950806139 **)L_55))->set_NextInLML_13(L_57);
		TEdge_t3950806139 ** L_58 = ___E0;
		TEdge_t3950806139 ** L_59 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_59)));
		TEdge_t3950806139 * L_60 = (*((TEdge_t3950806139 **)L_59))->get_Next_11();
		*((Il2CppObject **)(L_58)) = (Il2CppObject *)L_60;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_58), (Il2CppObject *)L_60);
		TEdge_t3950806139 ** L_61 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_61)));
		IntPoint_t3326126179 * L_62 = (*((TEdge_t3950806139 **)L_61))->get_address_of_Top_2();
		int64_t L_63 = L_62->get_X_0();
		TEdge_t3950806139 ** L_64 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_64)));
		TEdge_t3950806139 * L_65 = (*((TEdge_t3950806139 **)L_64))->get_Prev_12();
		NullCheck(L_65);
		IntPoint_t3326126179 * L_66 = L_65->get_address_of_Bot_0();
		int64_t L_67 = L_66->get_X_0();
		if ((((int64_t)L_63) == ((int64_t)L_67)))
		{
			goto IL_0169;
		}
	}
	{
		TEdge_t3950806139 ** L_68 = ___E0;
		ClipperBase_ReverseHorizontal_m3767571996(__this, (*((TEdge_t3950806139 **)L_68)), /*hidden argument*/NULL);
	}

IL_0169:
	{
		TEdge_t3950806139 ** L_69 = ___E0;
		TEdge_t3950806139 * L_70 = V_0;
		if ((!(((Il2CppObject*)(TEdge_t3950806139 *)(*((TEdge_t3950806139 **)L_69))) == ((Il2CppObject*)(TEdge_t3950806139 *)L_70))))
		{
			goto IL_0128;
		}
	}
	{
		goto IL_019d;
	}

IL_0176:
	{
		TEdge_t3950806139 ** L_71 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_71)));
		IntPoint_t3326126179 * L_72 = (*((TEdge_t3950806139 **)L_71))->get_address_of_Bot_0();
		int64_t L_73 = L_72->get_Y_1();
		TEdge_t3950806139 ** L_74 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_74)));
		TEdge_t3950806139 * L_75 = (*((TEdge_t3950806139 **)L_74))->get_Prev_12();
		NullCheck(L_75);
		IntPoint_t3326126179 * L_76 = L_75->get_address_of_Bot_0();
		int64_t L_77 = L_76->get_Y_1();
		if ((!(((uint64_t)L_73) == ((uint64_t)L_77))))
		{
			goto IL_019d;
		}
	}
	{
		goto IL_01b0;
	}

IL_019d:
	{
		TEdge_t3950806139 ** L_78 = ___E0;
		TEdge_t3950806139 ** L_79 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_79)));
		TEdge_t3950806139 * L_80 = (*((TEdge_t3950806139 **)L_79))->get_Prev_12();
		NullCheck((*((TEdge_t3950806139 **)L_78)));
		(*((TEdge_t3950806139 **)L_78))->set_NextInLML_13(L_80);
	}

IL_01ab:
	{
		goto IL_005b;
	}

IL_01b0:
	{
		TEdge_t3950806139 ** L_81 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_81)));
		TEdge_t3950806139 * L_82 = (*((TEdge_t3950806139 **)L_81))->get_Prev_12();
		return L_82;
	}
}
// System.Void Pathfinding.ClipperLib.ClipperBase::AscendToMax(Pathfinding.ClipperLib.TEdge&,System.Boolean,System.Boolean)
extern "C"  void ClipperBase_AscendToMax_m1097068823 (ClipperBase_t512275816 * __this, TEdge_t3950806139 ** ___E0, bool ___Appending1, bool ___IsClosed2, const MethodInfo* method)
{
	TEdge_t3950806139 * V_0 = NULL;
	{
		TEdge_t3950806139 ** L_0 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_0)));
		int32_t L_1 = (*((TEdge_t3950806139 **)L_0))->get_OutIdx_10();
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_002a;
		}
	}
	{
		TEdge_t3950806139 ** L_2 = ___E0;
		TEdge_t3950806139 ** L_3 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_3)));
		TEdge_t3950806139 * L_4 = (*((TEdge_t3950806139 **)L_3))->get_Next_11();
		*((Il2CppObject **)(L_2)) = (Il2CppObject *)L_4;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_2), (Il2CppObject *)L_4);
		TEdge_t3950806139 ** L_5 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_5)));
		TEdge_t3950806139 * L_6 = (*((TEdge_t3950806139 **)L_5))->get_Prev_12();
		bool L_7 = ClipperBase_MoreAbove_m1517449058(__this, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_002a;
		}
	}
	{
		return;
	}

IL_002a:
	{
		TEdge_t3950806139 ** L_8 = ___E0;
		bool L_9 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, (*((TEdge_t3950806139 **)L_8)), /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0061;
		}
	}
	{
		bool L_10 = ___Appending1;
		if (!L_10)
		{
			goto IL_0061;
		}
	}
	{
		TEdge_t3950806139 ** L_11 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_11)));
		IntPoint_t3326126179  L_12 = (*((TEdge_t3950806139 **)L_11))->get_Bot_0();
		TEdge_t3950806139 ** L_13 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_13)));
		TEdge_t3950806139 * L_14 = (*((TEdge_t3950806139 **)L_13))->get_Prev_12();
		NullCheck(L_14);
		IntPoint_t3326126179  L_15 = L_14->get_Bot_0();
		bool L_16 = IntPoint_op_Inequality_m3996904515(NULL /*static, unused*/, L_12, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0061;
		}
	}
	{
		TEdge_t3950806139 ** L_17 = ___E0;
		ClipperBase_ReverseHorizontal_m3767571996(__this, (*((TEdge_t3950806139 **)L_17)), /*hidden argument*/NULL);
	}

IL_0061:
	{
		TEdge_t3950806139 ** L_18 = ___E0;
		V_0 = (*((TEdge_t3950806139 **)L_18));
		goto IL_0101;
	}

IL_0069:
	{
		TEdge_t3950806139 ** L_19 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_19)));
		TEdge_t3950806139 * L_20 = (*((TEdge_t3950806139 **)L_19))->get_Next_11();
		NullCheck(L_20);
		int32_t L_21 = L_20->get_OutIdx_10();
		if ((((int32_t)L_21) == ((int32_t)((int32_t)-2))))
		{
			goto IL_00af;
		}
	}
	{
		TEdge_t3950806139 ** L_22 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_22)));
		TEdge_t3950806139 * L_23 = (*((TEdge_t3950806139 **)L_22))->get_Next_11();
		NullCheck(L_23);
		IntPoint_t3326126179 * L_24 = L_23->get_address_of_Top_2();
		int64_t L_25 = L_24->get_Y_1();
		TEdge_t3950806139 ** L_26 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_26)));
		IntPoint_t3326126179 * L_27 = (*((TEdge_t3950806139 **)L_26))->get_address_of_Top_2();
		int64_t L_28 = L_27->get_Y_1();
		if ((!(((uint64_t)L_25) == ((uint64_t)L_28))))
		{
			goto IL_00b4;
		}
	}
	{
		TEdge_t3950806139 ** L_29 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_29)));
		TEdge_t3950806139 * L_30 = (*((TEdge_t3950806139 **)L_29))->get_Next_11();
		bool L_31 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_00b4;
		}
	}

IL_00af:
	{
		goto IL_0106;
	}

IL_00b4:
	{
		TEdge_t3950806139 ** L_32 = ___E0;
		TEdge_t3950806139 ** L_33 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_33)));
		TEdge_t3950806139 * L_34 = (*((TEdge_t3950806139 **)L_33))->get_Next_11();
		NullCheck((*((TEdge_t3950806139 **)L_32)));
		(*((TEdge_t3950806139 **)L_32))->set_NextInLML_13(L_34);
		TEdge_t3950806139 ** L_35 = ___E0;
		TEdge_t3950806139 ** L_36 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_36)));
		TEdge_t3950806139 * L_37 = (*((TEdge_t3950806139 **)L_36))->get_Next_11();
		*((Il2CppObject **)(L_35)) = (Il2CppObject *)L_37;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_35), (Il2CppObject *)L_37);
		TEdge_t3950806139 ** L_38 = ___E0;
		bool L_39 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, (*((TEdge_t3950806139 **)L_38)), /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0101;
		}
	}
	{
		TEdge_t3950806139 ** L_40 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_40)));
		IntPoint_t3326126179 * L_41 = (*((TEdge_t3950806139 **)L_40))->get_address_of_Bot_0();
		int64_t L_42 = L_41->get_X_0();
		TEdge_t3950806139 ** L_43 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_43)));
		TEdge_t3950806139 * L_44 = (*((TEdge_t3950806139 **)L_43))->get_Prev_12();
		NullCheck(L_44);
		IntPoint_t3326126179 * L_45 = L_44->get_address_of_Top_2();
		int64_t L_46 = L_45->get_X_0();
		if ((((int64_t)L_42) == ((int64_t)L_46)))
		{
			goto IL_0101;
		}
	}
	{
		TEdge_t3950806139 ** L_47 = ___E0;
		ClipperBase_ReverseHorizontal_m3767571996(__this, (*((TEdge_t3950806139 **)L_47)), /*hidden argument*/NULL);
	}

IL_0101:
	{
		goto IL_0069;
	}

IL_0106:
	{
		bool L_48 = ___Appending1;
		if (L_48)
		{
			goto IL_0136;
		}
	}
	{
		TEdge_t3950806139 * L_49 = V_0;
		NullCheck(L_49);
		int32_t L_50 = L_49->get_OutIdx_10();
		if ((!(((uint32_t)L_50) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0120;
		}
	}
	{
		TEdge_t3950806139 * L_51 = V_0;
		NullCheck(L_51);
		TEdge_t3950806139 * L_52 = L_51->get_Next_11();
		V_0 = L_52;
	}

IL_0120:
	{
		TEdge_t3950806139 * L_53 = V_0;
		TEdge_t3950806139 ** L_54 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_54)));
		TEdge_t3950806139 * L_55 = (*((TEdge_t3950806139 **)L_54))->get_Next_11();
		if ((((Il2CppObject*)(TEdge_t3950806139 *)L_53) == ((Il2CppObject*)(TEdge_t3950806139 *)L_55)))
		{
			goto IL_0136;
		}
	}
	{
		TEdge_t3950806139 * L_56 = V_0;
		bool L_57 = ___IsClosed2;
		ClipperBase_DoMinimaLML_m1736312807(__this, (TEdge_t3950806139 *)NULL, L_56, L_57, /*hidden argument*/NULL);
	}

IL_0136:
	{
		TEdge_t3950806139 ** L_58 = ___E0;
		TEdge_t3950806139 ** L_59 = ___E0;
		NullCheck((*((TEdge_t3950806139 **)L_59)));
		TEdge_t3950806139 * L_60 = (*((TEdge_t3950806139 **)L_59))->get_Next_11();
		*((Il2CppObject **)(L_58)) = (Il2CppObject *)L_60;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_58), (Il2CppObject *)L_60);
		return;
	}
}
// Pathfinding.ClipperLib.TEdge Pathfinding.ClipperLib.ClipperBase::AddBoundsToLML(Pathfinding.ClipperLib.TEdge,System.Boolean)
extern "C"  TEdge_t3950806139 * ClipperBase_AddBoundsToLML_m1770302753 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___E0, bool ___Closed1, const MethodInfo* method)
{
	TEdge_t3950806139 * V_0 = NULL;
	bool V_1 = false;
	{
		TEdge_t3950806139 * L_0 = ___E0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_OutIdx_10();
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0036;
		}
	}
	{
		TEdge_t3950806139 * L_2 = ___E0;
		bool L_3 = ClipperBase_MoreBelow_m3190791414(__this, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		TEdge_t3950806139 * L_4 = ___E0;
		NullCheck(L_4);
		TEdge_t3950806139 * L_5 = L_4->get_Next_11();
		___E0 = L_5;
		TEdge_t3950806139 * L_6 = ClipperBase_DescendToMin_m3585442493(__this, (&___E0), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0031;
	}

IL_002f:
	{
		V_0 = (TEdge_t3950806139 *)NULL;
	}

IL_0031:
	{
		goto IL_003f;
	}

IL_0036:
	{
		TEdge_t3950806139 * L_7 = ClipperBase_DescendToMin_m3585442493(__this, (&___E0), /*hidden argument*/NULL);
		V_0 = L_7;
	}

IL_003f:
	{
		TEdge_t3950806139 * L_8 = ___E0;
		NullCheck(L_8);
		int32_t L_9 = L_8->get_OutIdx_10();
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_00b8;
		}
	}
	{
		TEdge_t3950806139 * L_10 = V_0;
		bool L_11 = ___Closed1;
		ClipperBase_DoMinimaLML_m1736312807(__this, (TEdge_t3950806139 *)NULL, L_10, L_11, /*hidden argument*/NULL);
		V_1 = (bool)0;
		TEdge_t3950806139 * L_12 = ___E0;
		NullCheck(L_12);
		IntPoint_t3326126179  L_13 = L_12->get_Bot_0();
		TEdge_t3950806139 * L_14 = ___E0;
		NullCheck(L_14);
		TEdge_t3950806139 * L_15 = L_14->get_Prev_12();
		NullCheck(L_15);
		IntPoint_t3326126179  L_16 = L_15->get_Bot_0();
		bool L_17 = IntPoint_op_Inequality_m3996904515(NULL /*static, unused*/, L_13, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_009f;
		}
	}
	{
		TEdge_t3950806139 * L_18 = ___E0;
		bool L_19 = ClipperBase_MoreBelow_m3190791414(__this, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_009f;
		}
	}
	{
		TEdge_t3950806139 * L_20 = ___E0;
		NullCheck(L_20);
		TEdge_t3950806139 * L_21 = L_20->get_Next_11();
		___E0 = L_21;
		TEdge_t3950806139 * L_22 = ClipperBase_DescendToMin_m3585442493(__this, (&___E0), /*hidden argument*/NULL);
		V_0 = L_22;
		TEdge_t3950806139 * L_23 = V_0;
		TEdge_t3950806139 * L_24 = ___E0;
		bool L_25 = ___Closed1;
		ClipperBase_DoMinimaLML_m1736312807(__this, L_23, L_24, L_25, /*hidden argument*/NULL);
		V_1 = (bool)1;
		goto IL_00b3;
	}

IL_009f:
	{
		TEdge_t3950806139 * L_26 = ___E0;
		bool L_27 = ClipperBase_JustBeforeLocMin_m120078395(__this, L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00b3;
		}
	}
	{
		TEdge_t3950806139 * L_28 = ___E0;
		NullCheck(L_28);
		TEdge_t3950806139 * L_29 = L_28->get_Next_11();
		___E0 = L_29;
	}

IL_00b3:
	{
		goto IL_00c3;
	}

IL_00b8:
	{
		TEdge_t3950806139 * L_30 = V_0;
		TEdge_t3950806139 * L_31 = ___E0;
		bool L_32 = ___Closed1;
		ClipperBase_DoMinimaLML_m1736312807(__this, L_30, L_31, L_32, /*hidden argument*/NULL);
		V_1 = (bool)1;
	}

IL_00c3:
	{
		bool L_33 = V_1;
		bool L_34 = ___Closed1;
		ClipperBase_AscendToMax_m1097068823(__this, (&___E0), L_33, L_34, /*hidden argument*/NULL);
		TEdge_t3950806139 * L_35 = ___E0;
		NullCheck(L_35);
		int32_t L_36 = L_35->get_OutIdx_10();
		if ((!(((uint32_t)L_36) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0166;
		}
	}
	{
		TEdge_t3950806139 * L_37 = ___E0;
		NullCheck(L_37);
		IntPoint_t3326126179  L_38 = L_37->get_Top_2();
		TEdge_t3950806139 * L_39 = ___E0;
		NullCheck(L_39);
		TEdge_t3950806139 * L_40 = L_39->get_Prev_12();
		NullCheck(L_40);
		IntPoint_t3326126179  L_41 = L_40->get_Top_2();
		bool L_42 = IntPoint_op_Inequality_m3996904515(NULL /*static, unused*/, L_38, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0166;
		}
	}
	{
		TEdge_t3950806139 * L_43 = ___E0;
		bool L_44 = ClipperBase_MoreAbove_m1517449058(__this, L_43, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_0118;
		}
	}
	{
		TEdge_t3950806139 * L_45 = ___E0;
		NullCheck(L_45);
		TEdge_t3950806139 * L_46 = L_45->get_Next_11();
		___E0 = L_46;
		bool L_47 = ___Closed1;
		ClipperBase_AscendToMax_m1097068823(__this, (&___E0), (bool)0, L_47, /*hidden argument*/NULL);
		goto IL_0166;
	}

IL_0118:
	{
		TEdge_t3950806139 * L_48 = ___E0;
		NullCheck(L_48);
		IntPoint_t3326126179  L_49 = L_48->get_Top_2();
		TEdge_t3950806139 * L_50 = ___E0;
		NullCheck(L_50);
		TEdge_t3950806139 * L_51 = L_50->get_Next_11();
		NullCheck(L_51);
		IntPoint_t3326126179  L_52 = L_51->get_Top_2();
		bool L_53 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_49, L_52, /*hidden argument*/NULL);
		if (L_53)
		{
			goto IL_015e;
		}
	}
	{
		TEdge_t3950806139 * L_54 = ___E0;
		NullCheck(L_54);
		TEdge_t3950806139 * L_55 = L_54->get_Next_11();
		bool L_56 = ClipperBase_IsHorizontal_m1610406924(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_0166;
		}
	}
	{
		TEdge_t3950806139 * L_57 = ___E0;
		NullCheck(L_57);
		IntPoint_t3326126179  L_58 = L_57->get_Top_2();
		TEdge_t3950806139 * L_59 = ___E0;
		NullCheck(L_59);
		TEdge_t3950806139 * L_60 = L_59->get_Next_11();
		NullCheck(L_60);
		IntPoint_t3326126179  L_61 = L_60->get_Bot_0();
		bool L_62 = IntPoint_op_Equality_m1729684360(NULL /*static, unused*/, L_58, L_61, /*hidden argument*/NULL);
		if (!L_62)
		{
			goto IL_0166;
		}
	}

IL_015e:
	{
		TEdge_t3950806139 * L_63 = ___E0;
		NullCheck(L_63);
		TEdge_t3950806139 * L_64 = L_63->get_Next_11();
		___E0 = L_64;
	}

IL_0166:
	{
		TEdge_t3950806139 * L_65 = ___E0;
		return L_65;
	}
}
// System.Void Pathfinding.ClipperLib.ClipperBase::InsertLocalMinima(Pathfinding.ClipperLib.LocalMinima)
extern "C"  void ClipperBase_InsertLocalMinima_m2132432750 (ClipperBase_t512275816 * __this, LocalMinima_t2863342752 * ___newLm0, const MethodInfo* method)
{
	LocalMinima_t2863342752 * V_0 = NULL;
	{
		LocalMinima_t2863342752 * L_0 = __this->get_m_MinimaList_6();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		LocalMinima_t2863342752 * L_1 = ___newLm0;
		__this->set_m_MinimaList_6(L_1);
		goto IL_008c;
	}

IL_0017:
	{
		LocalMinima_t2863342752 * L_2 = ___newLm0;
		NullCheck(L_2);
		int64_t L_3 = L_2->get_Y_0();
		LocalMinima_t2863342752 * L_4 = __this->get_m_MinimaList_6();
		NullCheck(L_4);
		int64_t L_5 = L_4->get_Y_0();
		if ((((int64_t)L_3) < ((int64_t)L_5)))
		{
			goto IL_0045;
		}
	}
	{
		LocalMinima_t2863342752 * L_6 = ___newLm0;
		LocalMinima_t2863342752 * L_7 = __this->get_m_MinimaList_6();
		NullCheck(L_6);
		L_6->set_Next_3(L_7);
		LocalMinima_t2863342752 * L_8 = ___newLm0;
		__this->set_m_MinimaList_6(L_8);
		goto IL_008c;
	}

IL_0045:
	{
		LocalMinima_t2863342752 * L_9 = __this->get_m_MinimaList_6();
		V_0 = L_9;
		goto IL_0058;
	}

IL_0051:
	{
		LocalMinima_t2863342752 * L_10 = V_0;
		NullCheck(L_10);
		LocalMinima_t2863342752 * L_11 = L_10->get_Next_3();
		V_0 = L_11;
	}

IL_0058:
	{
		LocalMinima_t2863342752 * L_12 = V_0;
		NullCheck(L_12);
		LocalMinima_t2863342752 * L_13 = L_12->get_Next_3();
		if (!L_13)
		{
			goto IL_0079;
		}
	}
	{
		LocalMinima_t2863342752 * L_14 = ___newLm0;
		NullCheck(L_14);
		int64_t L_15 = L_14->get_Y_0();
		LocalMinima_t2863342752 * L_16 = V_0;
		NullCheck(L_16);
		LocalMinima_t2863342752 * L_17 = L_16->get_Next_3();
		NullCheck(L_17);
		int64_t L_18 = L_17->get_Y_0();
		if ((((int64_t)L_15) < ((int64_t)L_18)))
		{
			goto IL_0051;
		}
	}

IL_0079:
	{
		LocalMinima_t2863342752 * L_19 = ___newLm0;
		LocalMinima_t2863342752 * L_20 = V_0;
		NullCheck(L_20);
		LocalMinima_t2863342752 * L_21 = L_20->get_Next_3();
		NullCheck(L_19);
		L_19->set_Next_3(L_21);
		LocalMinima_t2863342752 * L_22 = V_0;
		LocalMinima_t2863342752 * L_23 = ___newLm0;
		NullCheck(L_22);
		L_22->set_Next_3(L_23);
	}

IL_008c:
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.ClipperBase::PopLocalMinima()
extern "C"  void ClipperBase_PopLocalMinima_m3271239084 (ClipperBase_t512275816 * __this, const MethodInfo* method)
{
	{
		LocalMinima_t2863342752 * L_0 = __this->get_m_CurrentLM_7();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		LocalMinima_t2863342752 * L_1 = __this->get_m_CurrentLM_7();
		NullCheck(L_1);
		LocalMinima_t2863342752 * L_2 = L_1->get_Next_3();
		__this->set_m_CurrentLM_7(L_2);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.ClipperBase::ReverseHorizontal(Pathfinding.ClipperLib.TEdge)
extern "C"  void ClipperBase_ReverseHorizontal_m3767571996 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___e0, const MethodInfo* method)
{
	int64_t V_0 = 0;
	{
		TEdge_t3950806139 * L_0 = ___e0;
		NullCheck(L_0);
		IntPoint_t3326126179 * L_1 = L_0->get_address_of_Top_2();
		int64_t L_2 = L_1->get_X_0();
		V_0 = L_2;
		TEdge_t3950806139 * L_3 = ___e0;
		NullCheck(L_3);
		IntPoint_t3326126179 * L_4 = L_3->get_address_of_Top_2();
		TEdge_t3950806139 * L_5 = ___e0;
		NullCheck(L_5);
		IntPoint_t3326126179 * L_6 = L_5->get_address_of_Bot_0();
		int64_t L_7 = L_6->get_X_0();
		L_4->set_X_0(L_7);
		TEdge_t3950806139 * L_8 = ___e0;
		NullCheck(L_8);
		IntPoint_t3326126179 * L_9 = L_8->get_address_of_Bot_0();
		int64_t L_10 = V_0;
		L_9->set_X_0(L_10);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.ClipperBase::Reset()
extern "C"  void ClipperBase_Reset_m2790049354 (ClipperBase_t512275816 * __this, const MethodInfo* method)
{
	LocalMinima_t2863342752 * V_0 = NULL;
	TEdge_t3950806139 * V_1 = NULL;
	{
		LocalMinima_t2863342752 * L_0 = __this->get_m_MinimaList_6();
		__this->set_m_CurrentLM_7(L_0);
		LocalMinima_t2863342752 * L_1 = __this->get_m_CurrentLM_7();
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		return;
	}

IL_0018:
	{
		LocalMinima_t2863342752 * L_2 = __this->get_m_MinimaList_6();
		V_0 = L_2;
		goto IL_008d;
	}

IL_0024:
	{
		LocalMinima_t2863342752 * L_3 = V_0;
		NullCheck(L_3);
		TEdge_t3950806139 * L_4 = L_3->get_LeftBound_1();
		V_1 = L_4;
		TEdge_t3950806139 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0058;
		}
	}
	{
		TEdge_t3950806139 * L_6 = V_1;
		TEdge_t3950806139 * L_7 = V_1;
		NullCheck(L_7);
		IntPoint_t3326126179  L_8 = L_7->get_Bot_0();
		NullCheck(L_6);
		L_6->set_Curr_1(L_8);
		TEdge_t3950806139 * L_9 = V_1;
		NullCheck(L_9);
		L_9->set_Side_6(0);
		TEdge_t3950806139 * L_10 = V_1;
		NullCheck(L_10);
		int32_t L_11 = L_10->get_OutIdx_10();
		if ((((int32_t)L_11) == ((int32_t)((int32_t)-2))))
		{
			goto IL_0058;
		}
	}
	{
		TEdge_t3950806139 * L_12 = V_1;
		NullCheck(L_12);
		L_12->set_OutIdx_10((-1));
	}

IL_0058:
	{
		LocalMinima_t2863342752 * L_13 = V_0;
		NullCheck(L_13);
		TEdge_t3950806139 * L_14 = L_13->get_RightBound_2();
		V_1 = L_14;
		TEdge_t3950806139 * L_15 = V_1;
		TEdge_t3950806139 * L_16 = V_1;
		NullCheck(L_16);
		IntPoint_t3326126179  L_17 = L_16->get_Bot_0();
		NullCheck(L_15);
		L_15->set_Curr_1(L_17);
		TEdge_t3950806139 * L_18 = V_1;
		NullCheck(L_18);
		L_18->set_Side_6(1);
		TEdge_t3950806139 * L_19 = V_1;
		NullCheck(L_19);
		int32_t L_20 = L_19->get_OutIdx_10();
		if ((((int32_t)L_20) == ((int32_t)((int32_t)-2))))
		{
			goto IL_0086;
		}
	}
	{
		TEdge_t3950806139 * L_21 = V_1;
		NullCheck(L_21);
		L_21->set_OutIdx_10((-1));
	}

IL_0086:
	{
		LocalMinima_t2863342752 * L_22 = V_0;
		NullCheck(L_22);
		LocalMinima_t2863342752 * L_23 = L_22->get_Next_3();
		V_0 = L_23;
	}

IL_008d:
	{
		LocalMinima_t2863342752 * L_24 = V_0;
		if (L_24)
		{
			goto IL_0024;
		}
	}
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.ClipperException::.ctor(System.String)
extern "C"  void ClipperException__ctor_m106504313 (ClipperException_t2818206852 * __this, String_t* ___description0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___description0;
		Exception__ctor_m3870771296(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Int128::.ctor(System.Int64)
extern "C"  void Int128__ctor_m3350950229 (Int128_t3067532394 * __this, int64_t ____lo0, const MethodInfo* method)
{
	{
		int64_t L_0 = ____lo0;
		__this->set_lo_1(L_0);
		int64_t L_1 = ____lo0;
		if ((((int64_t)L_1) >= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_001c;
		}
	}
	{
		__this->set_hi_0((((int64_t)((int64_t)(-1)))));
		goto IL_0024;
	}

IL_001c:
	{
		__this->set_hi_0((((int64_t)((int64_t)0))));
	}

IL_0024:
	{
		return;
	}
}
extern "C"  void Int128__ctor_m3350950229_AdjustorThunk (Il2CppObject * __this, int64_t ____lo0, const MethodInfo* method)
{
	Int128_t3067532394 * _thisAdjusted = reinterpret_cast<Int128_t3067532394 *>(__this + 1);
	Int128__ctor_m3350950229(_thisAdjusted, ____lo0, method);
}
// System.Void Pathfinding.ClipperLib.Int128::.ctor(System.Int64,System.UInt64)
extern "C"  void Int128__ctor_m82640458 (Int128_t3067532394 * __this, int64_t ____hi0, uint64_t ____lo1, const MethodInfo* method)
{
	{
		uint64_t L_0 = ____lo1;
		__this->set_lo_1(L_0);
		int64_t L_1 = ____hi0;
		__this->set_hi_0(L_1);
		return;
	}
}
extern "C"  void Int128__ctor_m82640458_AdjustorThunk (Il2CppObject * __this, int64_t ____hi0, uint64_t ____lo1, const MethodInfo* method)
{
	Int128_t3067532394 * _thisAdjusted = reinterpret_cast<Int128_t3067532394 *>(__this + 1);
	Int128__ctor_m82640458(_thisAdjusted, ____hi0, ____lo1, method);
}
// System.Boolean Pathfinding.ClipperLib.Int128::Equals(System.Object)
extern Il2CppClass* Int128_t3067532394_il2cpp_TypeInfo_var;
extern const uint32_t Int128_Equals_m177562640_MetadataUsageId;
extern "C"  bool Int128_Equals_m177562640 (Int128_t3067532394 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Int128_Equals_m177562640_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int128_t3067532394  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		Il2CppObject * L_1 = ___obj0;
		if (((Il2CppObject *)IsInstSealed(L_1, Int128_t3067532394_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return (bool)0;
	}

IL_0013:
	{
		Il2CppObject * L_2 = ___obj0;
		V_0 = ((*(Int128_t3067532394 *)((Int128_t3067532394 *)UnBox (L_2, Int128_t3067532394_il2cpp_TypeInfo_var))));
		int64_t L_3 = (&V_0)->get_hi_0();
		int64_t L_4 = __this->get_hi_0();
		if ((!(((uint64_t)L_3) == ((uint64_t)L_4))))
		{
			goto IL_003d;
		}
	}
	{
		uint64_t L_5 = (&V_0)->get_lo_1();
		uint64_t L_6 = __this->get_lo_1();
		G_B6_0 = ((((int64_t)L_5) == ((int64_t)L_6))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B6_0 = 0;
	}

IL_003e:
	{
		return (bool)G_B6_0;
	}
}
extern "C"  bool Int128_Equals_m177562640_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	Int128_t3067532394 * _thisAdjusted = reinterpret_cast<Int128_t3067532394 *>(__this + 1);
	return Int128_Equals_m177562640(_thisAdjusted, ___obj0, method);
}
// System.Int32 Pathfinding.ClipperLib.Int128::GetHashCode()
extern "C"  int32_t Int128_GetHashCode_m551020328 (Int128_t3067532394 * __this, const MethodInfo* method)
{
	{
		int64_t* L_0 = __this->get_address_of_hi_0();
		int32_t L_1 = Int64_GetHashCode_m2140836887(L_0, /*hidden argument*/NULL);
		uint64_t* L_2 = __this->get_address_of_lo_1();
		int32_t L_3 = UInt64_GetHashCode_m4085537544(L_2, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_1^(int32_t)L_3));
	}
}
extern "C"  int32_t Int128_GetHashCode_m551020328_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Int128_t3067532394 * _thisAdjusted = reinterpret_cast<Int128_t3067532394 *>(__this + 1);
	return Int128_GetHashCode_m551020328(_thisAdjusted, method);
}
// Pathfinding.ClipperLib.Int128 Pathfinding.ClipperLib.Int128::Int128Mul(System.Int64,System.Int64)
extern "C"  Int128_t3067532394  Int128_Int128Mul_m44987946 (Il2CppObject * __this /* static, unused */, int64_t ___lhs0, int64_t ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	uint64_t V_1 = 0;
	uint64_t V_2 = 0;
	uint64_t V_3 = 0;
	uint64_t V_4 = 0;
	uint64_t V_5 = 0;
	uint64_t V_6 = 0;
	uint64_t V_7 = 0;
	uint64_t V_8 = 0;
	int64_t V_9 = 0;
	Int128_t3067532394  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Int128_t3067532394  G_B9_0;
	memset(&G_B9_0, 0, sizeof(G_B9_0));
	{
		int64_t L_0 = ___lhs0;
		int64_t L_1 = ___rhs1;
		V_0 = (bool)((((int32_t)((((int32_t)((((int64_t)L_0) < ((int64_t)(((int64_t)((int64_t)0)))))? 1 : 0)) == ((int32_t)((((int64_t)L_1) < ((int64_t)(((int64_t)((int64_t)0)))))? 1 : 0)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		int64_t L_2 = ___lhs0;
		if ((((int64_t)L_2) >= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_001c;
		}
	}
	{
		int64_t L_3 = ___lhs0;
		___lhs0 = ((-L_3));
	}

IL_001c:
	{
		int64_t L_4 = ___rhs1;
		if ((((int64_t)L_4) >= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0028;
		}
	}
	{
		int64_t L_5 = ___rhs1;
		___rhs1 = ((-L_5));
	}

IL_0028:
	{
		int64_t L_6 = ___lhs0;
		V_1 = ((int64_t)((uint64_t)L_6>>((int32_t)32)));
		int64_t L_7 = ___lhs0;
		V_2 = ((int64_t)((int64_t)L_7&(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1)))))))));
		int64_t L_8 = ___rhs1;
		V_3 = ((int64_t)((uint64_t)L_8>>((int32_t)32)));
		int64_t L_9 = ___rhs1;
		V_4 = ((int64_t)((int64_t)L_9&(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1)))))))));
		uint64_t L_10 = V_1;
		uint64_t L_11 = V_3;
		V_5 = ((int64_t)((int64_t)L_10*(int64_t)L_11));
		uint64_t L_12 = V_2;
		uint64_t L_13 = V_4;
		V_6 = ((int64_t)((int64_t)L_12*(int64_t)L_13));
		uint64_t L_14 = V_1;
		uint64_t L_15 = V_4;
		uint64_t L_16 = V_2;
		uint64_t L_17 = V_3;
		V_7 = ((int64_t)((int64_t)((int64_t)((int64_t)L_14*(int64_t)L_15))+(int64_t)((int64_t)((int64_t)L_16*(int64_t)L_17))));
		uint64_t L_18 = V_5;
		uint64_t L_19 = V_7;
		V_9 = ((int64_t)((int64_t)L_18+(int64_t)((int64_t)((uint64_t)L_19>>((int32_t)32)))));
		uint64_t L_20 = V_7;
		uint64_t L_21 = V_6;
		V_8 = ((int64_t)((int64_t)((int64_t)((int64_t)L_20<<(int32_t)((int32_t)32)))+(int64_t)L_21));
		uint64_t L_22 = V_8;
		uint64_t L_23 = V_6;
		if ((!(((uint64_t)L_22) < ((uint64_t)L_23))))
		{
			goto IL_0076;
		}
	}
	{
		int64_t L_24 = V_9;
		V_9 = ((int64_t)((int64_t)L_24+(int64_t)(((int64_t)((int64_t)1)))));
	}

IL_0076:
	{
		int64_t L_25 = V_9;
		uint64_t L_26 = V_8;
		Int128__ctor_m82640458((&V_10), L_25, L_26, /*hidden argument*/NULL);
		bool L_27 = V_0;
		if (!L_27)
		{
			goto IL_0093;
		}
	}
	{
		Int128_t3067532394  L_28 = V_10;
		Int128_t3067532394  L_29 = Int128_op_UnaryNegation_m2803597574(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		G_B9_0 = L_29;
		goto IL_0095;
	}

IL_0093:
	{
		Int128_t3067532394  L_30 = V_10;
		G_B9_0 = L_30;
	}

IL_0095:
	{
		return G_B9_0;
	}
}
// System.Boolean Pathfinding.ClipperLib.Int128::op_Equality(Pathfinding.ClipperLib.Int128,Pathfinding.ClipperLib.Int128)
extern Il2CppClass* Int128_t3067532394_il2cpp_TypeInfo_var;
extern const uint32_t Int128_op_Equality_m3976304897_MetadataUsageId;
extern "C"  bool Int128_op_Equality_m3976304897 (Il2CppObject * __this /* static, unused */, Int128_t3067532394  ___val10, Int128_t3067532394  ___val21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Int128_op_Equality_m3976304897_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B8_0 = 0;
	{
		Int128_t3067532394  L_0 = ___val10;
		Int128_t3067532394  L_1 = L_0;
		Il2CppObject * L_2 = Box(Int128_t3067532394_il2cpp_TypeInfo_var, &L_1);
		Int128_t3067532394  L_3 = ___val21;
		Int128_t3067532394  L_4 = L_3;
		Il2CppObject * L_5 = Box(Int128_t3067532394_il2cpp_TypeInfo_var, &L_4);
		if ((!(((Il2CppObject*)(Il2CppObject *)L_2) == ((Il2CppObject*)(Il2CppObject *)L_5))))
		{
			goto IL_0013;
		}
	}
	{
		return (bool)1;
	}

IL_0013:
	{
	}
	{
		goto IL_002b;
	}

IL_0029:
	{
		return (bool)0;
	}

IL_002b:
	{
		int64_t L_8 = (&___val10)->get_hi_0();
		int64_t L_9 = (&___val21)->get_hi_0();
		if ((!(((uint64_t)L_8) == ((uint64_t)L_9))))
		{
			goto IL_0050;
		}
	}
	{
		uint64_t L_10 = (&___val10)->get_lo_1();
		uint64_t L_11 = (&___val21)->get_lo_1();
		G_B8_0 = ((((int64_t)L_10) == ((int64_t)L_11))? 1 : 0);
		goto IL_0051;
	}

IL_0050:
	{
		G_B8_0 = 0;
	}

IL_0051:
	{
		return (bool)G_B8_0;
	}
}
// System.Boolean Pathfinding.ClipperLib.Int128::op_GreaterThan(Pathfinding.ClipperLib.Int128,Pathfinding.ClipperLib.Int128)
extern "C"  bool Int128_op_GreaterThan_m2422758740 (Il2CppObject * __this /* static, unused */, Int128_t3067532394  ___val10, Int128_t3067532394  ___val21, const MethodInfo* method)
{
	{
		int64_t L_0 = (&___val10)->get_hi_0();
		int64_t L_1 = (&___val21)->get_hi_0();
		if ((((int64_t)L_0) == ((int64_t)L_1)))
		{
			goto IL_0024;
		}
	}
	{
		int64_t L_2 = (&___val10)->get_hi_0();
		int64_t L_3 = (&___val21)->get_hi_0();
		return (bool)((((int64_t)L_2) > ((int64_t)L_3))? 1 : 0);
	}

IL_0024:
	{
		uint64_t L_4 = (&___val10)->get_lo_1();
		uint64_t L_5 = (&___val21)->get_lo_1();
		return (bool)((!(((uint64_t)L_4) <= ((uint64_t)L_5)))? 1 : 0);
	}
}
// System.Boolean Pathfinding.ClipperLib.Int128::op_LessThan(Pathfinding.ClipperLib.Int128,Pathfinding.ClipperLib.Int128)
extern "C"  bool Int128_op_LessThan_m3057609505 (Il2CppObject * __this /* static, unused */, Int128_t3067532394  ___val10, Int128_t3067532394  ___val21, const MethodInfo* method)
{
	{
		int64_t L_0 = (&___val10)->get_hi_0();
		int64_t L_1 = (&___val21)->get_hi_0();
		if ((((int64_t)L_0) == ((int64_t)L_1)))
		{
			goto IL_0024;
		}
	}
	{
		int64_t L_2 = (&___val10)->get_hi_0();
		int64_t L_3 = (&___val21)->get_hi_0();
		return (bool)((((int64_t)L_2) < ((int64_t)L_3))? 1 : 0);
	}

IL_0024:
	{
		uint64_t L_4 = (&___val10)->get_lo_1();
		uint64_t L_5 = (&___val21)->get_lo_1();
		return (bool)((!(((uint64_t)L_4) >= ((uint64_t)L_5)))? 1 : 0);
	}
}
// Pathfinding.ClipperLib.Int128 Pathfinding.ClipperLib.Int128::op_Addition(Pathfinding.ClipperLib.Int128,Pathfinding.ClipperLib.Int128)
extern "C"  Int128_t3067532394  Int128_op_Addition_m2329452092 (Il2CppObject * __this /* static, unused */, Int128_t3067532394  ___lhs0, Int128_t3067532394  ___rhs1, const MethodInfo* method)
{
	{
		Int128_t3067532394 * L_0 = (&___lhs0);
		int64_t L_1 = L_0->get_hi_0();
		int64_t L_2 = (&___rhs1)->get_hi_0();
		L_0->set_hi_0(((int64_t)((int64_t)L_1+(int64_t)L_2)));
		Int128_t3067532394 * L_3 = (&___lhs0);
		uint64_t L_4 = L_3->get_lo_1();
		uint64_t L_5 = (&___rhs1)->get_lo_1();
		L_3->set_lo_1(((int64_t)((int64_t)L_4+(int64_t)L_5)));
		uint64_t L_6 = (&___lhs0)->get_lo_1();
		uint64_t L_7 = (&___rhs1)->get_lo_1();
		if ((!(((uint64_t)L_6) < ((uint64_t)L_7))))
		{
			goto IL_004d;
		}
	}
	{
		Int128_t3067532394 * L_8 = (&___lhs0);
		int64_t L_9 = L_8->get_hi_0();
		L_8->set_hi_0(((int64_t)((int64_t)L_9+(int64_t)(((int64_t)((int64_t)1))))));
	}

IL_004d:
	{
		Int128_t3067532394  L_10 = ___lhs0;
		return L_10;
	}
}
// Pathfinding.ClipperLib.Int128 Pathfinding.ClipperLib.Int128::op_Subtraction(Pathfinding.ClipperLib.Int128,Pathfinding.ClipperLib.Int128)
extern "C"  Int128_t3067532394  Int128_op_Subtraction_m2448771870 (Il2CppObject * __this /* static, unused */, Int128_t3067532394  ___lhs0, Int128_t3067532394  ___rhs1, const MethodInfo* method)
{
	{
		Int128_t3067532394  L_0 = ___lhs0;
		Int128_t3067532394  L_1 = ___rhs1;
		Int128_t3067532394  L_2 = Int128_op_UnaryNegation_m2803597574(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Int128_t3067532394  L_3 = Int128_op_Addition_m2329452092(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// Pathfinding.ClipperLib.Int128 Pathfinding.ClipperLib.Int128::op_UnaryNegation(Pathfinding.ClipperLib.Int128)
extern "C"  Int128_t3067532394  Int128_op_UnaryNegation_m2803597574 (Il2CppObject * __this /* static, unused */, Int128_t3067532394  ___val0, const MethodInfo* method)
{
	{
		uint64_t L_0 = (&___val0)->get_lo_1();
		if ((!(((uint64_t)L_0) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_001e;
		}
	}
	{
		int64_t L_1 = (&___val0)->get_hi_0();
		Int128_t3067532394  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Int128__ctor_m82640458(&L_2, ((-L_1)), (((int64_t)((int64_t)0))), /*hidden argument*/NULL);
		return L_2;
	}

IL_001e:
	{
		int64_t L_3 = (&___val0)->get_hi_0();
		uint64_t L_4 = (&___val0)->get_lo_1();
		Int128_t3067532394  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Int128__ctor_m82640458(&L_5, ((~L_3)), ((int64_t)((int64_t)((~L_4))+(int64_t)(((int64_t)((int64_t)1))))), /*hidden argument*/NULL);
		return L_5;
	}
}
// Pathfinding.ClipperLib.Int128 Pathfinding.ClipperLib.Int128::op_Division(Pathfinding.ClipperLib.Int128,Pathfinding.ClipperLib.Int128)
extern Il2CppClass* ClipperException_t2818206852_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral493642968;
extern const uint32_t Int128_op_Division_m2463004203_MetadataUsageId;
extern "C"  Int128_t3067532394  Int128_op_Division_m2463004203 (Il2CppObject * __this /* static, unused */, Int128_t3067532394  ___lhs0, Int128_t3067532394  ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Int128_op_Division_m2463004203_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Int128_t3067532394  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Int128_t3067532394  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Int128_t3067532394  G_B33_0;
	memset(&G_B33_0, 0, sizeof(G_B33_0));
	{
		uint64_t L_0 = (&___rhs1)->get_lo_1();
		if ((!(((uint64_t)L_0) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_0027;
		}
	}
	{
		int64_t L_1 = (&___rhs1)->get_hi_0();
		if ((!(((uint64_t)L_1) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_0027;
		}
	}
	{
		ClipperException_t2818206852 * L_2 = (ClipperException_t2818206852 *)il2cpp_codegen_object_new(ClipperException_t2818206852_il2cpp_TypeInfo_var);
		ClipperException__ctor_m106504313(L_2, _stringLiteral493642968, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0027:
	{
		int64_t L_3 = (&___rhs1)->get_hi_0();
		int64_t L_4 = (&___lhs0)->get_hi_0();
		V_0 = (bool)((((int32_t)((((int32_t)((((int64_t)L_3) < ((int64_t)(((int64_t)((int64_t)0)))))? 1 : 0)) == ((int32_t)((((int64_t)L_4) < ((int64_t)(((int64_t)((int64_t)0)))))? 1 : 0)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		int64_t L_5 = (&___lhs0)->get_hi_0();
		if ((((int64_t)L_5) >= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0059;
		}
	}
	{
		Int128_t3067532394  L_6 = ___lhs0;
		Int128_t3067532394  L_7 = Int128_op_UnaryNegation_m2803597574(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		___lhs0 = L_7;
	}

IL_0059:
	{
		int64_t L_8 = (&___rhs1)->get_hi_0();
		if ((((int64_t)L_8) >= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_006f;
		}
	}
	{
		Int128_t3067532394  L_9 = ___rhs1;
		Int128_t3067532394  L_10 = Int128_op_UnaryNegation_m2803597574(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		___rhs1 = L_10;
	}

IL_006f:
	{
		Int128_t3067532394  L_11 = ___rhs1;
		Int128_t3067532394  L_12 = ___lhs0;
		bool L_13 = Int128_op_LessThan_m3057609505(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_02b0;
		}
	}
	{
		Int128__ctor_m3350950229((&V_1), (((int64_t)((int64_t)0))), /*hidden argument*/NULL);
		Int128__ctor_m3350950229((&V_2), (((int64_t)((int64_t)1))), /*hidden argument*/NULL);
		goto IL_010a;
	}

IL_0092:
	{
		Int128_t3067532394 * L_14 = (&___rhs1);
		int64_t L_15 = L_14->get_hi_0();
		L_14->set_hi_0(((int64_t)((int64_t)L_15<<(int32_t)1)));
		uint64_t L_16 = (&___rhs1)->get_lo_1();
		if ((((int64_t)L_16) >= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_00bf;
		}
	}
	{
		Int128_t3067532394 * L_17 = (&___rhs1);
		int64_t L_18 = L_17->get_hi_0();
		L_17->set_hi_0(((int64_t)((int64_t)L_18+(int64_t)(((int64_t)((int64_t)1))))));
	}

IL_00bf:
	{
		Int128_t3067532394 * L_19 = (&___rhs1);
		uint64_t L_20 = L_19->get_lo_1();
		L_19->set_lo_1(((int64_t)((int64_t)L_20<<(int32_t)1)));
		Int128_t3067532394 * L_21 = (&V_2);
		int64_t L_22 = L_21->get_hi_0();
		L_21->set_hi_0(((int64_t)((int64_t)L_22<<(int32_t)1)));
		uint64_t L_23 = (&V_2)->get_lo_1();
		if ((((int64_t)L_23) >= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_00fb;
		}
	}
	{
		Int128_t3067532394 * L_24 = (&V_2);
		int64_t L_25 = L_24->get_hi_0();
		L_24->set_hi_0(((int64_t)((int64_t)L_25+(int64_t)(((int64_t)((int64_t)1))))));
	}

IL_00fb:
	{
		Int128_t3067532394 * L_26 = (&V_2);
		uint64_t L_27 = L_26->get_lo_1();
		L_26->set_lo_1(((int64_t)((int64_t)L_27<<(int32_t)1)));
	}

IL_010a:
	{
		int64_t L_28 = (&___rhs1)->get_hi_0();
		if ((((int64_t)L_28) < ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0124;
		}
	}
	{
		Int128_t3067532394  L_29 = ___rhs1;
		Int128_t3067532394  L_30 = ___lhs0;
		bool L_31 = Int128_op_GreaterThan_m2422758740(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0092;
		}
	}

IL_0124:
	{
		Int128_t3067532394 * L_32 = (&___rhs1);
		uint64_t L_33 = L_32->get_lo_1();
		L_32->set_lo_1(((int64_t)((uint64_t)L_33>>1)));
		int64_t L_34 = (&___rhs1)->get_hi_0();
		if ((!(((uint64_t)((int64_t)((int64_t)L_34&(int64_t)(((int64_t)((int64_t)1)))))) == ((uint64_t)(((int64_t)((int64_t)1)))))))
		{
			goto IL_015b;
		}
	}
	{
		Int128_t3067532394 * L_35 = (&___rhs1);
		uint64_t L_36 = L_35->get_lo_1();
		L_35->set_lo_1(((int64_t)((int64_t)L_36|(int64_t)((int64_t)std::numeric_limits<int64_t>::min()))));
	}

IL_015b:
	{
		int64_t L_37 = (&___rhs1)->get_hi_0();
		(&___rhs1)->set_hi_0(((int64_t)((uint64_t)L_37>>1)));
		Int128_t3067532394 * L_38 = (&V_2);
		uint64_t L_39 = L_38->get_lo_1();
		L_38->set_lo_1(((int64_t)((uint64_t)L_39>>1)));
		int64_t L_40 = (&V_2)->get_hi_0();
		if ((!(((uint64_t)((int64_t)((int64_t)L_40&(int64_t)(((int64_t)((int64_t)1)))))) == ((uint64_t)(((int64_t)((int64_t)1)))))))
		{
			goto IL_01a2;
		}
	}
	{
		Int128_t3067532394 * L_41 = (&V_2);
		uint64_t L_42 = L_41->get_lo_1();
		L_41->set_lo_1(((int64_t)((int64_t)L_42|(int64_t)((int64_t)std::numeric_limits<int64_t>::min()))));
	}

IL_01a2:
	{
		Int128_t3067532394 * L_43 = (&V_2);
		int64_t L_44 = L_43->get_hi_0();
		L_43->set_hi_0(((int64_t)((int64_t)L_44>>(int32_t)1)));
		goto IL_0281;
	}

IL_01b6:
	{
		Int128_t3067532394  L_45 = ___lhs0;
		Int128_t3067532394  L_46 = ___rhs1;
		bool L_47 = Int128_op_LessThan_m3057609505(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
		if (L_47)
		{
			goto IL_01f5;
		}
	}
	{
		Int128_t3067532394  L_48 = ___lhs0;
		Int128_t3067532394  L_49 = ___rhs1;
		Int128_t3067532394  L_50 = Int128_op_Subtraction_m2448771870(NULL /*static, unused*/, L_48, L_49, /*hidden argument*/NULL);
		___lhs0 = L_50;
		Int128_t3067532394 * L_51 = (&V_1);
		int64_t L_52 = L_51->get_hi_0();
		int64_t L_53 = (&V_2)->get_hi_0();
		L_51->set_hi_0(((int64_t)((int64_t)L_52|(int64_t)L_53)));
		Int128_t3067532394 * L_54 = (&V_1);
		uint64_t L_55 = L_54->get_lo_1();
		uint64_t L_56 = (&V_2)->get_lo_1();
		L_54->set_lo_1(((int64_t)((int64_t)L_55|(int64_t)L_56)));
	}

IL_01f5:
	{
		Int128_t3067532394 * L_57 = (&___rhs1);
		uint64_t L_58 = L_57->get_lo_1();
		L_57->set_lo_1(((int64_t)((uint64_t)L_58>>1)));
		int64_t L_59 = (&___rhs1)->get_hi_0();
		if ((!(((uint64_t)((int64_t)((int64_t)L_59&(int64_t)(((int64_t)((int64_t)1)))))) == ((uint64_t)(((int64_t)((int64_t)1)))))))
		{
			goto IL_022c;
		}
	}
	{
		Int128_t3067532394 * L_60 = (&___rhs1);
		uint64_t L_61 = L_60->get_lo_1();
		L_60->set_lo_1(((int64_t)((int64_t)L_61|(int64_t)((int64_t)std::numeric_limits<int64_t>::min()))));
	}

IL_022c:
	{
		Int128_t3067532394 * L_62 = (&___rhs1);
		int64_t L_63 = L_62->get_hi_0();
		L_62->set_hi_0(((int64_t)((int64_t)L_63>>(int32_t)1)));
		Int128_t3067532394 * L_64 = (&V_2);
		uint64_t L_65 = L_64->get_lo_1();
		L_64->set_lo_1(((int64_t)((uint64_t)L_65>>1)));
		int64_t L_66 = (&V_2)->get_hi_0();
		if ((!(((uint64_t)((int64_t)((int64_t)L_66&(int64_t)(((int64_t)((int64_t)1)))))) == ((uint64_t)(((int64_t)((int64_t)1)))))))
		{
			goto IL_0272;
		}
	}
	{
		Int128_t3067532394 * L_67 = (&V_2);
		uint64_t L_68 = L_67->get_lo_1();
		L_67->set_lo_1(((int64_t)((int64_t)L_68|(int64_t)((int64_t)std::numeric_limits<int64_t>::min()))));
	}

IL_0272:
	{
		Int128_t3067532394 * L_69 = (&V_2);
		int64_t L_70 = L_69->get_hi_0();
		L_69->set_hi_0(((int64_t)((int64_t)L_70>>(int32_t)1)));
	}

IL_0281:
	{
		int64_t L_71 = (&V_2)->get_hi_0();
		if ((!(((uint64_t)L_71) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_01b6;
		}
	}
	{
		uint64_t L_72 = (&V_2)->get_lo_1();
		if ((!(((uint64_t)L_72) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_01b6;
		}
	}
	{
		bool L_73 = V_0;
		if (!L_73)
		{
			goto IL_02ae;
		}
	}
	{
		Int128_t3067532394  L_74 = V_1;
		Int128_t3067532394  L_75 = Int128_op_UnaryNegation_m2803597574(NULL /*static, unused*/, L_74, /*hidden argument*/NULL);
		G_B33_0 = L_75;
		goto IL_02af;
	}

IL_02ae:
	{
		Int128_t3067532394  L_76 = V_1;
		G_B33_0 = L_76;
	}

IL_02af:
	{
		return G_B33_0;
	}

IL_02b0:
	{
		Int128_t3067532394  L_77 = ___rhs1;
		Int128_t3067532394  L_78 = ___lhs0;
		bool L_79 = Int128_op_Equality_m3976304897(NULL /*static, unused*/, L_77, L_78, /*hidden argument*/NULL);
		if (!L_79)
		{
			goto IL_02c4;
		}
	}
	{
		Int128_t3067532394  L_80;
		memset(&L_80, 0, sizeof(L_80));
		Int128__ctor_m3350950229(&L_80, (((int64_t)((int64_t)1))), /*hidden argument*/NULL);
		return L_80;
	}

IL_02c4:
	{
		Int128_t3067532394  L_81;
		memset(&L_81, 0, sizeof(L_81));
		Int128__ctor_m3350950229(&L_81, (((int64_t)((int64_t)0))), /*hidden argument*/NULL);
		return L_81;
	}
}
// Conversion methods for marshalling of: Pathfinding.ClipperLib.Int128
extern "C" void Int128_t3067532394_marshal_pinvoke(const Int128_t3067532394& unmarshaled, Int128_t3067532394_marshaled_pinvoke& marshaled)
{
	marshaled.___hi_0 = unmarshaled.get_hi_0();
	marshaled.___lo_1 = unmarshaled.get_lo_1();
}
extern "C" void Int128_t3067532394_marshal_pinvoke_back(const Int128_t3067532394_marshaled_pinvoke& marshaled, Int128_t3067532394& unmarshaled)
{
	int64_t unmarshaled_hi_temp_0 = 0;
	unmarshaled_hi_temp_0 = marshaled.___hi_0;
	unmarshaled.set_hi_0(unmarshaled_hi_temp_0);
	uint64_t unmarshaled_lo_temp_1 = 0;
	unmarshaled_lo_temp_1 = marshaled.___lo_1;
	unmarshaled.set_lo_1(unmarshaled_lo_temp_1);
}
// Conversion method for clean up from marshalling of: Pathfinding.ClipperLib.Int128
extern "C" void Int128_t3067532394_marshal_pinvoke_cleanup(Int128_t3067532394_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: Pathfinding.ClipperLib.Int128
extern "C" void Int128_t3067532394_marshal_com(const Int128_t3067532394& unmarshaled, Int128_t3067532394_marshaled_com& marshaled)
{
	marshaled.___hi_0 = unmarshaled.get_hi_0();
	marshaled.___lo_1 = unmarshaled.get_lo_1();
}
extern "C" void Int128_t3067532394_marshal_com_back(const Int128_t3067532394_marshaled_com& marshaled, Int128_t3067532394& unmarshaled)
{
	int64_t unmarshaled_hi_temp_0 = 0;
	unmarshaled_hi_temp_0 = marshaled.___hi_0;
	unmarshaled.set_hi_0(unmarshaled_hi_temp_0);
	uint64_t unmarshaled_lo_temp_1 = 0;
	unmarshaled_lo_temp_1 = marshaled.___lo_1;
	unmarshaled.set_lo_1(unmarshaled_lo_temp_1);
}
// Conversion method for clean up from marshalling of: Pathfinding.ClipperLib.Int128
extern "C" void Int128_t3067532394_marshal_com_cleanup(Int128_t3067532394_marshaled_com& marshaled)
{
}
// System.Void Pathfinding.ClipperLib.IntersectNode::.ctor()
extern "C"  void IntersectNode__ctor_m641175930 (IntersectNode_t4106323947 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.IntPoint::.ctor(System.Int64,System.Int64)
extern "C"  void IntPoint__ctor_m1065894876 (IntPoint_t3326126179 * __this, int64_t ___X0, int64_t ___Y1, const MethodInfo* method)
{
	{
		int64_t L_0 = ___X0;
		__this->set_X_0(L_0);
		int64_t L_1 = ___Y1;
		__this->set_Y_1(L_1);
		return;
	}
}
extern "C"  void IntPoint__ctor_m1065894876_AdjustorThunk (Il2CppObject * __this, int64_t ___X0, int64_t ___Y1, const MethodInfo* method)
{
	IntPoint_t3326126179 * _thisAdjusted = reinterpret_cast<IntPoint_t3326126179 *>(__this + 1);
	IntPoint__ctor_m1065894876(_thisAdjusted, ___X0, ___Y1, method);
}
// System.Void Pathfinding.ClipperLib.IntPoint::.ctor(Pathfinding.ClipperLib.IntPoint)
extern "C"  void IntPoint__ctor_m964253275 (IntPoint_t3326126179 * __this, IntPoint_t3326126179  ___pt0, const MethodInfo* method)
{
	{
		int64_t L_0 = (&___pt0)->get_X_0();
		__this->set_X_0(L_0);
		int64_t L_1 = (&___pt0)->get_Y_1();
		__this->set_Y_1(L_1);
		return;
	}
}
extern "C"  void IntPoint__ctor_m964253275_AdjustorThunk (Il2CppObject * __this, IntPoint_t3326126179  ___pt0, const MethodInfo* method)
{
	IntPoint_t3326126179 * _thisAdjusted = reinterpret_cast<IntPoint_t3326126179 *>(__this + 1);
	IntPoint__ctor_m964253275(_thisAdjusted, ___pt0, method);
}
// System.Boolean Pathfinding.ClipperLib.IntPoint::Equals(System.Object)
extern Il2CppClass* IntPoint_t3326126179_il2cpp_TypeInfo_var;
extern const uint32_t IntPoint_Equals_m2369764343_MetadataUsageId;
extern "C"  bool IntPoint_Equals_m2369764343 (IntPoint_t3326126179 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IntPoint_Equals_m2369764343_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPoint_t3326126179  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInstSealed(L_1, IntPoint_t3326126179_il2cpp_TypeInfo_var)))
		{
			goto IL_003f;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		V_0 = ((*(IntPoint_t3326126179 *)((IntPoint_t3326126179 *)UnBox (L_2, IntPoint_t3326126179_il2cpp_TypeInfo_var))));
		int64_t L_3 = __this->get_X_0();
		int64_t L_4 = (&V_0)->get_X_0();
		if ((!(((uint64_t)L_3) == ((uint64_t)L_4))))
		{
			goto IL_003d;
		}
	}
	{
		int64_t L_5 = __this->get_Y_1();
		int64_t L_6 = (&V_0)->get_Y_1();
		G_B6_0 = ((((int64_t)L_5) == ((int64_t)L_6))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B6_0 = 0;
	}

IL_003e:
	{
		return (bool)G_B6_0;
	}

IL_003f:
	{
		return (bool)0;
	}
}
extern "C"  bool IntPoint_Equals_m2369764343_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	IntPoint_t3326126179 * _thisAdjusted = reinterpret_cast<IntPoint_t3326126179 *>(__this + 1);
	return IntPoint_Equals_m2369764343(_thisAdjusted, ___obj0, method);
}
// System.Int32 Pathfinding.ClipperLib.IntPoint::GetHashCode()
extern Il2CppClass* IntPoint_t3326126179_il2cpp_TypeInfo_var;
extern const uint32_t IntPoint_GetHashCode_m2934122383_MetadataUsageId;
extern "C"  int32_t IntPoint_GetHashCode_m2934122383 (IntPoint_t3326126179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IntPoint_GetHashCode_m2934122383_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPoint_t3326126179  L_0 = (*(IntPoint_t3326126179 *)__this);
		Il2CppObject * L_1 = Box(IntPoint_t3326126179_il2cpp_TypeInfo_var, &L_0);
		NullCheck((ValueType_t1744280289 *)L_1);
		int32_t L_2 = ValueType_GetHashCode_m3949669881((ValueType_t1744280289 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t IntPoint_GetHashCode_m2934122383_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	IntPoint_t3326126179 * _thisAdjusted = reinterpret_cast<IntPoint_t3326126179 *>(__this + 1);
	return IntPoint_GetHashCode_m2934122383(_thisAdjusted, method);
}
// System.Boolean Pathfinding.ClipperLib.IntPoint::op_Equality(Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint)
extern "C"  bool IntPoint_op_Equality_m1729684360 (Il2CppObject * __this /* static, unused */, IntPoint_t3326126179  ___a0, IntPoint_t3326126179  ___b1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int64_t L_0 = (&___a0)->get_X_0();
		int64_t L_1 = (&___b1)->get_X_0();
		if ((!(((uint64_t)L_0) == ((uint64_t)L_1))))
		{
			goto IL_0025;
		}
	}
	{
		int64_t L_2 = (&___a0)->get_Y_1();
		int64_t L_3 = (&___b1)->get_Y_1();
		G_B3_0 = ((((int64_t)L_2) == ((int64_t)L_3))? 1 : 0);
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean Pathfinding.ClipperLib.IntPoint::op_Inequality(Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint)
extern "C"  bool IntPoint_op_Inequality_m3996904515 (Il2CppObject * __this /* static, unused */, IntPoint_t3326126179  ___a0, IntPoint_t3326126179  ___b1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int64_t L_0 = (&___a0)->get_X_0();
		int64_t L_1 = (&___b1)->get_X_0();
		if ((!(((uint64_t)L_0) == ((uint64_t)L_1))))
		{
			goto IL_0028;
		}
	}
	{
		int64_t L_2 = (&___a0)->get_Y_1();
		int64_t L_3 = (&___b1)->get_Y_1();
		G_B3_0 = ((((int32_t)((((int64_t)L_2) == ((int64_t)L_3))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0029;
	}

IL_0028:
	{
		G_B3_0 = 1;
	}

IL_0029:
	{
		return (bool)G_B3_0;
	}
}
// Conversion methods for marshalling of: Pathfinding.ClipperLib.IntPoint
extern "C" void IntPoint_t3326126179_marshal_pinvoke(const IntPoint_t3326126179& unmarshaled, IntPoint_t3326126179_marshaled_pinvoke& marshaled)
{
	marshaled.___X_0 = unmarshaled.get_X_0();
	marshaled.___Y_1 = unmarshaled.get_Y_1();
}
extern "C" void IntPoint_t3326126179_marshal_pinvoke_back(const IntPoint_t3326126179_marshaled_pinvoke& marshaled, IntPoint_t3326126179& unmarshaled)
{
	int64_t unmarshaled_X_temp_0 = 0;
	unmarshaled_X_temp_0 = marshaled.___X_0;
	unmarshaled.set_X_0(unmarshaled_X_temp_0);
	int64_t unmarshaled_Y_temp_1 = 0;
	unmarshaled_Y_temp_1 = marshaled.___Y_1;
	unmarshaled.set_Y_1(unmarshaled_Y_temp_1);
}
// Conversion method for clean up from marshalling of: Pathfinding.ClipperLib.IntPoint
extern "C" void IntPoint_t3326126179_marshal_pinvoke_cleanup(IntPoint_t3326126179_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: Pathfinding.ClipperLib.IntPoint
extern "C" void IntPoint_t3326126179_marshal_com(const IntPoint_t3326126179& unmarshaled, IntPoint_t3326126179_marshaled_com& marshaled)
{
	marshaled.___X_0 = unmarshaled.get_X_0();
	marshaled.___Y_1 = unmarshaled.get_Y_1();
}
extern "C" void IntPoint_t3326126179_marshal_com_back(const IntPoint_t3326126179_marshaled_com& marshaled, IntPoint_t3326126179& unmarshaled)
{
	int64_t unmarshaled_X_temp_0 = 0;
	unmarshaled_X_temp_0 = marshaled.___X_0;
	unmarshaled.set_X_0(unmarshaled_X_temp_0);
	int64_t unmarshaled_Y_temp_1 = 0;
	unmarshaled_Y_temp_1 = marshaled.___Y_1;
	unmarshaled.set_Y_1(unmarshaled_Y_temp_1);
}
// Conversion method for clean up from marshalling of: Pathfinding.ClipperLib.IntPoint
extern "C" void IntPoint_t3326126179_marshal_com_cleanup(IntPoint_t3326126179_marshaled_com& marshaled)
{
}
// System.Void Pathfinding.ClipperLib.Join::.ctor()
extern "C"  void Join__ctor_m1417981953 (Join_t3970117804 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.LocalMinima::.ctor()
extern "C"  void LocalMinima__ctor_m1959824229 (LocalMinima_t2863342752 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.OutPt::.ctor()
extern "C"  void OutPt__ctor_m1159254633 (OutPt_t3947633180 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.OutRec::.ctor()
extern "C"  void OutRec__ctor_m2716404681 (OutRec_t3245805284 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.PolyNode::.ctor()
extern Il2CppClass* List_1_t399344435_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t409472064_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m124798912_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2194711693_MethodInfo_var;
extern const uint32_t PolyNode__ctor_m2522874301_MetadataUsageId;
extern "C"  void PolyNode__ctor_m2522874301 (PolyNode_t3336253808 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PolyNode__ctor_m2522874301_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t399344435 * L_0 = (List_1_t399344435 *)il2cpp_codegen_object_new(List_1_t399344435_il2cpp_TypeInfo_var);
		List_1__ctor_m124798912(L_0, /*hidden argument*/List_1__ctor_m124798912_MethodInfo_var);
		__this->set_m_polygon_1(L_0);
		List_1_t409472064 * L_1 = (List_1_t409472064 *)il2cpp_codegen_object_new(List_1_t409472064_il2cpp_TypeInfo_var);
		List_1__ctor_m2194711693(L_1, /*hidden argument*/List_1__ctor_m2194711693_MethodInfo_var);
		__this->set_m_Childs_3(L_1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Pathfinding.ClipperLib.PolyNode::get_ChildCount()
extern const MethodInfo* List_1_get_Count_m230006427_MethodInfo_var;
extern const uint32_t PolyNode_get_ChildCount_m368656717_MetadataUsageId;
extern "C"  int32_t PolyNode_get_ChildCount_m368656717 (PolyNode_t3336253808 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PolyNode_get_ChildCount_m368656717_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t409472064 * L_0 = __this->get_m_Childs_3();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m230006427(L_0, /*hidden argument*/List_1_get_Count_m230006427_MethodInfo_var);
		return L_1;
	}
}
// System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint> Pathfinding.ClipperLib.PolyNode::get_Contour()
extern "C"  List_1_t399344435 * PolyNode_get_Contour_m2041513154 (PolyNode_t3336253808 * __this, const MethodInfo* method)
{
	{
		List_1_t399344435 * L_0 = __this->get_m_polygon_1();
		return L_0;
	}
}
// System.Void Pathfinding.ClipperLib.PolyNode::AddChild(Pathfinding.ClipperLib.PolyNode)
extern const MethodInfo* List_1_get_Count_m230006427_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1889072509_MethodInfo_var;
extern const uint32_t PolyNode_AddChild_m4066781430_MetadataUsageId;
extern "C"  void PolyNode_AddChild_m4066781430 (PolyNode_t3336253808 * __this, PolyNode_t3336253808 * ___Child0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PolyNode_AddChild_m4066781430_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_t409472064 * L_0 = __this->get_m_Childs_3();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m230006427(L_0, /*hidden argument*/List_1_get_Count_m230006427_MethodInfo_var);
		V_0 = L_1;
		List_1_t409472064 * L_2 = __this->get_m_Childs_3();
		PolyNode_t3336253808 * L_3 = ___Child0;
		NullCheck(L_2);
		List_1_Add_m1889072509(L_2, L_3, /*hidden argument*/List_1_Add_m1889072509_MethodInfo_var);
		PolyNode_t3336253808 * L_4 = ___Child0;
		NullCheck(L_4);
		L_4->set_m_Parent_0(__this);
		PolyNode_t3336253808 * L_5 = ___Child0;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		L_5->set_m_Index_2(L_6);
		return;
	}
}
// System.Collections.Generic.List`1<Pathfinding.ClipperLib.PolyNode> Pathfinding.ClipperLib.PolyNode::get_Childs()
extern "C"  List_1_t409472064 * PolyNode_get_Childs_m2605215012 (PolyNode_t3336253808 * __this, const MethodInfo* method)
{
	{
		List_1_t409472064 * L_0 = __this->get_m_Childs_3();
		return L_0;
	}
}
// System.Void Pathfinding.ClipperLib.PolyNode::set_IsOpen(System.Boolean)
extern "C"  void PolyNode_set_IsOpen_m4104302511 (PolyNode_t3336253808 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CIsOpenU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.PolyTree::.ctor()
extern Il2CppClass* List_1_t409472064_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2194711693_MethodInfo_var;
extern const uint32_t PolyTree__ctor_m3647720353_MetadataUsageId;
extern "C"  void PolyTree__ctor_m3647720353 (PolyTree_t3336435468 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PolyTree__ctor_m3647720353_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t409472064 * L_0 = (List_1_t409472064 *)il2cpp_codegen_object_new(List_1_t409472064_il2cpp_TypeInfo_var);
		List_1__ctor_m2194711693(L_0, /*hidden argument*/List_1__ctor_m2194711693_MethodInfo_var);
		__this->set_m_AllPolys_5(L_0);
		PolyNode__ctor_m2522874301(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.PolyTree::Finalize()
extern "C"  void PolyTree_Finalize_m2371986721 (PolyTree_t3336435468 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		PolyTree_Clear_m1053853644(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void Pathfinding.ClipperLib.PolyTree::Clear()
extern const MethodInfo* List_1_set_Item_m36980799_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m230006427_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m3895812280_MethodInfo_var;
extern const uint32_t PolyTree_Clear_m1053853644_MetadataUsageId;
extern "C"  void PolyTree_Clear_m1053853644 (PolyTree_t3336435468 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PolyTree_Clear_m1053853644_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0018;
	}

IL_0007:
	{
		List_1_t409472064 * L_0 = __this->get_m_AllPolys_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		List_1_set_Item_m36980799(L_0, L_1, (PolyNode_t3336253808 *)NULL, /*hidden argument*/List_1_set_Item_m36980799_MethodInfo_var);
		int32_t L_2 = V_0;
		V_0 = ((int32_t)((int32_t)L_2+(int32_t)1));
	}

IL_0018:
	{
		int32_t L_3 = V_0;
		List_1_t409472064 * L_4 = __this->get_m_AllPolys_5();
		NullCheck(L_4);
		int32_t L_5 = List_1_get_Count_m230006427(L_4, /*hidden argument*/List_1_get_Count_m230006427_MethodInfo_var);
		if ((((int32_t)L_3) < ((int32_t)L_5)))
		{
			goto IL_0007;
		}
	}
	{
		List_1_t409472064 * L_6 = __this->get_m_AllPolys_5();
		NullCheck(L_6);
		List_1_Clear_m3895812280(L_6, /*hidden argument*/List_1_Clear_m3895812280_MethodInfo_var);
		List_1_t409472064 * L_7 = ((PolyNode_t3336253808 *)__this)->get_m_Childs_3();
		NullCheck(L_7);
		List_1_Clear_m3895812280(L_7, /*hidden argument*/List_1_Clear_m3895812280_MethodInfo_var);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.Scanbeam::.ctor()
extern "C"  void Scanbeam__ctor_m2642114495 (Scanbeam_t1885114670 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.ClipperLib.TEdge::.ctor()
extern "C"  void TEdge__ctor_m2332074730 (TEdge_t3950806139 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
