﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_IEffComponent3802264961.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoopEffect
struct  LoopEffect_t2053225845  : public IEffComponent_t3802264961
{
public:
	// System.Int32 LoopEffect::id
	int32_t ___id_1;
	// System.Int32 LoopEffect::instanceID
	int32_t ___instanceID_2;

public:
	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(LoopEffect_t2053225845, ___id_1)); }
	inline int32_t get_id_1() const { return ___id_1; }
	inline int32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int32_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_instanceID_2() { return static_cast<int32_t>(offsetof(LoopEffect_t2053225845, ___instanceID_2)); }
	inline int32_t get_instanceID_2() const { return ___instanceID_2; }
	inline int32_t* get_address_of_instanceID_2() { return &___instanceID_2; }
	inline void set_instanceID_2(int32_t value)
	{
		___instanceID_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
