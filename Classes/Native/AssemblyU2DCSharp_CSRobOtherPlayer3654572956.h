﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// CSHeroUnit[]
struct CSHeroUnitU5BU5D_t1342235227;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSRobOtherPlayer
struct  CSRobOtherPlayer_t3654572956  : public Il2CppObject
{
public:
	// System.UInt32 CSRobOtherPlayer::roleId
	uint32_t ___roleId_0;
	// System.String CSRobOtherPlayer::name
	String_t* ___name_1;
	// System.String CSRobOtherPlayer::icon
	String_t* ___icon_2;
	// System.UInt32 CSRobOtherPlayer::rank
	uint32_t ___rank_3;
	// System.UInt32 CSRobOtherPlayer::lv
	uint32_t ___lv_4;
	// System.UInt32 CSRobOtherPlayer::exp
	uint32_t ___exp_5;
	// System.UInt32 CSRobOtherPlayer::power
	uint32_t ___power_6;
	// System.UInt32 CSRobOtherPlayer::uPower
	uint32_t ___uPower_7;
	// CSHeroUnit[] CSRobOtherPlayer::heroInfoList
	CSHeroUnitU5BU5D_t1342235227* ___heroInfoList_8;

public:
	inline static int32_t get_offset_of_roleId_0() { return static_cast<int32_t>(offsetof(CSRobOtherPlayer_t3654572956, ___roleId_0)); }
	inline uint32_t get_roleId_0() const { return ___roleId_0; }
	inline uint32_t* get_address_of_roleId_0() { return &___roleId_0; }
	inline void set_roleId_0(uint32_t value)
	{
		___roleId_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(CSRobOtherPlayer_t3654572956, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_icon_2() { return static_cast<int32_t>(offsetof(CSRobOtherPlayer_t3654572956, ___icon_2)); }
	inline String_t* get_icon_2() const { return ___icon_2; }
	inline String_t** get_address_of_icon_2() { return &___icon_2; }
	inline void set_icon_2(String_t* value)
	{
		___icon_2 = value;
		Il2CppCodeGenWriteBarrier(&___icon_2, value);
	}

	inline static int32_t get_offset_of_rank_3() { return static_cast<int32_t>(offsetof(CSRobOtherPlayer_t3654572956, ___rank_3)); }
	inline uint32_t get_rank_3() const { return ___rank_3; }
	inline uint32_t* get_address_of_rank_3() { return &___rank_3; }
	inline void set_rank_3(uint32_t value)
	{
		___rank_3 = value;
	}

	inline static int32_t get_offset_of_lv_4() { return static_cast<int32_t>(offsetof(CSRobOtherPlayer_t3654572956, ___lv_4)); }
	inline uint32_t get_lv_4() const { return ___lv_4; }
	inline uint32_t* get_address_of_lv_4() { return &___lv_4; }
	inline void set_lv_4(uint32_t value)
	{
		___lv_4 = value;
	}

	inline static int32_t get_offset_of_exp_5() { return static_cast<int32_t>(offsetof(CSRobOtherPlayer_t3654572956, ___exp_5)); }
	inline uint32_t get_exp_5() const { return ___exp_5; }
	inline uint32_t* get_address_of_exp_5() { return &___exp_5; }
	inline void set_exp_5(uint32_t value)
	{
		___exp_5 = value;
	}

	inline static int32_t get_offset_of_power_6() { return static_cast<int32_t>(offsetof(CSRobOtherPlayer_t3654572956, ___power_6)); }
	inline uint32_t get_power_6() const { return ___power_6; }
	inline uint32_t* get_address_of_power_6() { return &___power_6; }
	inline void set_power_6(uint32_t value)
	{
		___power_6 = value;
	}

	inline static int32_t get_offset_of_uPower_7() { return static_cast<int32_t>(offsetof(CSRobOtherPlayer_t3654572956, ___uPower_7)); }
	inline uint32_t get_uPower_7() const { return ___uPower_7; }
	inline uint32_t* get_address_of_uPower_7() { return &___uPower_7; }
	inline void set_uPower_7(uint32_t value)
	{
		___uPower_7 = value;
	}

	inline static int32_t get_offset_of_heroInfoList_8() { return static_cast<int32_t>(offsetof(CSRobOtherPlayer_t3654572956, ___heroInfoList_8)); }
	inline CSHeroUnitU5BU5D_t1342235227* get_heroInfoList_8() const { return ___heroInfoList_8; }
	inline CSHeroUnitU5BU5D_t1342235227** get_address_of_heroInfoList_8() { return &___heroInfoList_8; }
	inline void set_heroInfoList_8(CSHeroUnitU5BU5D_t1342235227* value)
	{
		___heroInfoList_8 = value;
		Il2CppCodeGenWriteBarrier(&___heroInfoList_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
