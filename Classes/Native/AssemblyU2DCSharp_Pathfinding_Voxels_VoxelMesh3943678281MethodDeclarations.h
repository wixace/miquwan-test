﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Voxels.VoxelMesh
struct VoxelMesh_t3943678281;
struct VoxelMesh_t3943678281_marshaled_pinvoke;
struct VoxelMesh_t3943678281_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct VoxelMesh_t3943678281;
struct VoxelMesh_t3943678281_marshaled_pinvoke;

extern "C" void VoxelMesh_t3943678281_marshal_pinvoke(const VoxelMesh_t3943678281& unmarshaled, VoxelMesh_t3943678281_marshaled_pinvoke& marshaled);
extern "C" void VoxelMesh_t3943678281_marshal_pinvoke_back(const VoxelMesh_t3943678281_marshaled_pinvoke& marshaled, VoxelMesh_t3943678281& unmarshaled);
extern "C" void VoxelMesh_t3943678281_marshal_pinvoke_cleanup(VoxelMesh_t3943678281_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct VoxelMesh_t3943678281;
struct VoxelMesh_t3943678281_marshaled_com;

extern "C" void VoxelMesh_t3943678281_marshal_com(const VoxelMesh_t3943678281& unmarshaled, VoxelMesh_t3943678281_marshaled_com& marshaled);
extern "C" void VoxelMesh_t3943678281_marshal_com_back(const VoxelMesh_t3943678281_marshaled_com& marshaled, VoxelMesh_t3943678281& unmarshaled);
extern "C" void VoxelMesh_t3943678281_marshal_com_cleanup(VoxelMesh_t3943678281_marshaled_com& marshaled);
