﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_EventTriggerGenerated
struct UnityEngine_EventSystems_EventTriggerGenerated_t276083300;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_EventSystems_EventTriggerGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_EventTriggerGenerated__ctor_m3282250423 (UnityEngine_EventSystems_EventTriggerGenerated_t276083300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_EventTriggerGenerated::EventTrigger_triggers(JSVCall)
extern "C"  void UnityEngine_EventSystems_EventTriggerGenerated_EventTrigger_triggers_m3728835734 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventTriggerGenerated::EventTrigger_OnBeginDrag__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventTriggerGenerated_EventTrigger_OnBeginDrag__PointerEventData_m1257803797 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventTriggerGenerated::EventTrigger_OnCancel__BaseEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventTriggerGenerated_EventTrigger_OnCancel__BaseEventData_m2130265674 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventTriggerGenerated::EventTrigger_OnDeselect__BaseEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventTriggerGenerated_EventTrigger_OnDeselect__BaseEventData_m1757875687 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventTriggerGenerated::EventTrigger_OnDrag__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventTriggerGenerated_EventTrigger_OnDrag__PointerEventData_m3747415180 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventTriggerGenerated::EventTrigger_OnDrop__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventTriggerGenerated_EventTrigger_OnDrop__PointerEventData_m948399943 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventTriggerGenerated::EventTrigger_OnEndDrag__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventTriggerGenerated_EventTrigger_OnEndDrag__PointerEventData_m3131942215 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventTriggerGenerated::EventTrigger_OnInitializePotentialDrag__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventTriggerGenerated_EventTrigger_OnInitializePotentialDrag__PointerEventData_m2154963546 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventTriggerGenerated::EventTrigger_OnMove__AxisEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventTriggerGenerated_EventTrigger_OnMove__AxisEventData_m478126211 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventTriggerGenerated::EventTrigger_OnPointerClick__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventTriggerGenerated_EventTrigger_OnPointerClick__PointerEventData_m3540240803 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventTriggerGenerated::EventTrigger_OnPointerDown__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventTriggerGenerated_EventTrigger_OnPointerDown__PointerEventData_m3986141463 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventTriggerGenerated::EventTrigger_OnPointerEnter__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventTriggerGenerated_EventTrigger_OnPointerEnter__PointerEventData_m1327069939 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventTriggerGenerated::EventTrigger_OnPointerExit__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventTriggerGenerated_EventTrigger_OnPointerExit__PointerEventData_m2752059795 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventTriggerGenerated::EventTrigger_OnPointerUp__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventTriggerGenerated_EventTrigger_OnPointerUp__PointerEventData_m1775536464 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventTriggerGenerated::EventTrigger_OnScroll__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventTriggerGenerated_EventTrigger_OnScroll__PointerEventData_m2040363301 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventTriggerGenerated::EventTrigger_OnSelect__BaseEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventTriggerGenerated_EventTrigger_OnSelect__BaseEventData_m4264790280 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventTriggerGenerated::EventTrigger_OnSubmit__BaseEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventTriggerGenerated_EventTrigger_OnSubmit__BaseEventData_m1698332524 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventTriggerGenerated::EventTrigger_OnUpdateSelected__BaseEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventTriggerGenerated_EventTrigger_OnUpdateSelected__BaseEventData_m3706895488 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_EventTriggerGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_EventTriggerGenerated___Register_m1404005488 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_EventSystems_EventTriggerGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_EventSystems_EventTriggerGenerated_ilo_setObject1_m218226611 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_EventSystems_EventTriggerGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_EventSystems_EventTriggerGenerated_ilo_getObject2_m645610649 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
