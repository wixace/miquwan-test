﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._16096cdb37d9e00f2c8218ce436c4ad6
struct _16096cdb37d9e00f2c8218ce436c4ad6_t1364278635;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._16096cdb37d9e00f2c8218ce436c4ad6::.ctor()
extern "C"  void _16096cdb37d9e00f2c8218ce436c4ad6__ctor_m4030255394 (_16096cdb37d9e00f2c8218ce436c4ad6_t1364278635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._16096cdb37d9e00f2c8218ce436c4ad6::_16096cdb37d9e00f2c8218ce436c4ad6m2(System.Int32)
extern "C"  int32_t _16096cdb37d9e00f2c8218ce436c4ad6__16096cdb37d9e00f2c8218ce436c4ad6m2_m370557369 (_16096cdb37d9e00f2c8218ce436c4ad6_t1364278635 * __this, int32_t ____16096cdb37d9e00f2c8218ce436c4ad6a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._16096cdb37d9e00f2c8218ce436c4ad6::_16096cdb37d9e00f2c8218ce436c4ad6m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _16096cdb37d9e00f2c8218ce436c4ad6__16096cdb37d9e00f2c8218ce436c4ad6m_m2530134301 (_16096cdb37d9e00f2c8218ce436c4ad6_t1364278635 * __this, int32_t ____16096cdb37d9e00f2c8218ce436c4ad6a0, int32_t ____16096cdb37d9e00f2c8218ce436c4ad6991, int32_t ____16096cdb37d9e00f2c8218ce436c4ad6c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
