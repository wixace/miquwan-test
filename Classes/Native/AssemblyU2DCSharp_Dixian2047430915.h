﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.MeshCollider
struct MeshCollider_t2667653125;
// UnityEngine.Animation
struct Animation_t1724966010;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_Dixian_DIXIAN_TRIGGER_TYPE2965472337.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Dixian
struct  Dixian_t2047430915  : public MonoBehaviour_t667441552
{
public:
	// Dixian/DIXIAN_TRIGGER_TYPE Dixian::Trigger_type
	int32_t ___Trigger_type_3;
	// UnityEngine.MeshCollider Dixian::Up_meshCollider
	MeshCollider_t2667653125 * ___Up_meshCollider_4;
	// UnityEngine.MeshCollider Dixian::Down_meshCollider
	MeshCollider_t2667653125 * ___Down_meshCollider_5;
	// System.Single Dixian::AnimationTime
	float ___AnimationTime_6;
	// System.Single Dixian::DixianDelay
	float ___DixianDelay_7;
	// System.Int32 Dixian::Round
	int32_t ___Round_8;
	// UnityEngine.Vector3 Dixian::ShockV3
	Vector3_t4282066566  ___ShockV3_9;
	// System.Single Dixian::ShockTime
	float ___ShockTime_10;
	// System.Single Dixian::ShockDelay
	float ___ShockDelay_11;
	// System.Single Dixian::DieTime
	float ___DieTime_12;
	// UnityEngine.Animation Dixian::_anim
	Animation_t1724966010 * ____anim_13;
	// System.Boolean Dixian::_isTrigger
	bool ____isTrigger_14;
	// System.Boolean Dixian::_isStartDie
	bool ____isStartDie_15;
	// System.Single Dixian::_aphValue
	float ____aphValue_16;

public:
	inline static int32_t get_offset_of_Trigger_type_3() { return static_cast<int32_t>(offsetof(Dixian_t2047430915, ___Trigger_type_3)); }
	inline int32_t get_Trigger_type_3() const { return ___Trigger_type_3; }
	inline int32_t* get_address_of_Trigger_type_3() { return &___Trigger_type_3; }
	inline void set_Trigger_type_3(int32_t value)
	{
		___Trigger_type_3 = value;
	}

	inline static int32_t get_offset_of_Up_meshCollider_4() { return static_cast<int32_t>(offsetof(Dixian_t2047430915, ___Up_meshCollider_4)); }
	inline MeshCollider_t2667653125 * get_Up_meshCollider_4() const { return ___Up_meshCollider_4; }
	inline MeshCollider_t2667653125 ** get_address_of_Up_meshCollider_4() { return &___Up_meshCollider_4; }
	inline void set_Up_meshCollider_4(MeshCollider_t2667653125 * value)
	{
		___Up_meshCollider_4 = value;
		Il2CppCodeGenWriteBarrier(&___Up_meshCollider_4, value);
	}

	inline static int32_t get_offset_of_Down_meshCollider_5() { return static_cast<int32_t>(offsetof(Dixian_t2047430915, ___Down_meshCollider_5)); }
	inline MeshCollider_t2667653125 * get_Down_meshCollider_5() const { return ___Down_meshCollider_5; }
	inline MeshCollider_t2667653125 ** get_address_of_Down_meshCollider_5() { return &___Down_meshCollider_5; }
	inline void set_Down_meshCollider_5(MeshCollider_t2667653125 * value)
	{
		___Down_meshCollider_5 = value;
		Il2CppCodeGenWriteBarrier(&___Down_meshCollider_5, value);
	}

	inline static int32_t get_offset_of_AnimationTime_6() { return static_cast<int32_t>(offsetof(Dixian_t2047430915, ___AnimationTime_6)); }
	inline float get_AnimationTime_6() const { return ___AnimationTime_6; }
	inline float* get_address_of_AnimationTime_6() { return &___AnimationTime_6; }
	inline void set_AnimationTime_6(float value)
	{
		___AnimationTime_6 = value;
	}

	inline static int32_t get_offset_of_DixianDelay_7() { return static_cast<int32_t>(offsetof(Dixian_t2047430915, ___DixianDelay_7)); }
	inline float get_DixianDelay_7() const { return ___DixianDelay_7; }
	inline float* get_address_of_DixianDelay_7() { return &___DixianDelay_7; }
	inline void set_DixianDelay_7(float value)
	{
		___DixianDelay_7 = value;
	}

	inline static int32_t get_offset_of_Round_8() { return static_cast<int32_t>(offsetof(Dixian_t2047430915, ___Round_8)); }
	inline int32_t get_Round_8() const { return ___Round_8; }
	inline int32_t* get_address_of_Round_8() { return &___Round_8; }
	inline void set_Round_8(int32_t value)
	{
		___Round_8 = value;
	}

	inline static int32_t get_offset_of_ShockV3_9() { return static_cast<int32_t>(offsetof(Dixian_t2047430915, ___ShockV3_9)); }
	inline Vector3_t4282066566  get_ShockV3_9() const { return ___ShockV3_9; }
	inline Vector3_t4282066566 * get_address_of_ShockV3_9() { return &___ShockV3_9; }
	inline void set_ShockV3_9(Vector3_t4282066566  value)
	{
		___ShockV3_9 = value;
	}

	inline static int32_t get_offset_of_ShockTime_10() { return static_cast<int32_t>(offsetof(Dixian_t2047430915, ___ShockTime_10)); }
	inline float get_ShockTime_10() const { return ___ShockTime_10; }
	inline float* get_address_of_ShockTime_10() { return &___ShockTime_10; }
	inline void set_ShockTime_10(float value)
	{
		___ShockTime_10 = value;
	}

	inline static int32_t get_offset_of_ShockDelay_11() { return static_cast<int32_t>(offsetof(Dixian_t2047430915, ___ShockDelay_11)); }
	inline float get_ShockDelay_11() const { return ___ShockDelay_11; }
	inline float* get_address_of_ShockDelay_11() { return &___ShockDelay_11; }
	inline void set_ShockDelay_11(float value)
	{
		___ShockDelay_11 = value;
	}

	inline static int32_t get_offset_of_DieTime_12() { return static_cast<int32_t>(offsetof(Dixian_t2047430915, ___DieTime_12)); }
	inline float get_DieTime_12() const { return ___DieTime_12; }
	inline float* get_address_of_DieTime_12() { return &___DieTime_12; }
	inline void set_DieTime_12(float value)
	{
		___DieTime_12 = value;
	}

	inline static int32_t get_offset_of__anim_13() { return static_cast<int32_t>(offsetof(Dixian_t2047430915, ____anim_13)); }
	inline Animation_t1724966010 * get__anim_13() const { return ____anim_13; }
	inline Animation_t1724966010 ** get_address_of__anim_13() { return &____anim_13; }
	inline void set__anim_13(Animation_t1724966010 * value)
	{
		____anim_13 = value;
		Il2CppCodeGenWriteBarrier(&____anim_13, value);
	}

	inline static int32_t get_offset_of__isTrigger_14() { return static_cast<int32_t>(offsetof(Dixian_t2047430915, ____isTrigger_14)); }
	inline bool get__isTrigger_14() const { return ____isTrigger_14; }
	inline bool* get_address_of__isTrigger_14() { return &____isTrigger_14; }
	inline void set__isTrigger_14(bool value)
	{
		____isTrigger_14 = value;
	}

	inline static int32_t get_offset_of__isStartDie_15() { return static_cast<int32_t>(offsetof(Dixian_t2047430915, ____isStartDie_15)); }
	inline bool get__isStartDie_15() const { return ____isStartDie_15; }
	inline bool* get_address_of__isStartDie_15() { return &____isStartDie_15; }
	inline void set__isStartDie_15(bool value)
	{
		____isStartDie_15 = value;
	}

	inline static int32_t get_offset_of__aphValue_16() { return static_cast<int32_t>(offsetof(Dixian_t2047430915, ____aphValue_16)); }
	inline float get__aphValue_16() const { return ____aphValue_16; }
	inline float* get_address_of__aphValue_16() { return &____aphValue_16; }
	inline void set__aphValue_16(float value)
	{
		____aphValue_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
