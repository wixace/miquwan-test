﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginQiZi
struct  PluginQiZi_t1606345050  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginQiZi::userId
	String_t* ___userId_2;
	// System.String PluginQiZi::sign
	String_t* ___sign_3;
	// System.Boolean PluginQiZi::isOut
	bool ___isOut_4;
	// System.String PluginQiZi::configId
	String_t* ___configId_5;
	// System.String PluginQiZi::withDic
	String_t* ___withDic_6;
	// Mihua.SDK.PayInfo PluginQiZi::payInfo
	PayInfo_t1775308120 * ___payInfo_7;

public:
	inline static int32_t get_offset_of_userId_2() { return static_cast<int32_t>(offsetof(PluginQiZi_t1606345050, ___userId_2)); }
	inline String_t* get_userId_2() const { return ___userId_2; }
	inline String_t** get_address_of_userId_2() { return &___userId_2; }
	inline void set_userId_2(String_t* value)
	{
		___userId_2 = value;
		Il2CppCodeGenWriteBarrier(&___userId_2, value);
	}

	inline static int32_t get_offset_of_sign_3() { return static_cast<int32_t>(offsetof(PluginQiZi_t1606345050, ___sign_3)); }
	inline String_t* get_sign_3() const { return ___sign_3; }
	inline String_t** get_address_of_sign_3() { return &___sign_3; }
	inline void set_sign_3(String_t* value)
	{
		___sign_3 = value;
		Il2CppCodeGenWriteBarrier(&___sign_3, value);
	}

	inline static int32_t get_offset_of_isOut_4() { return static_cast<int32_t>(offsetof(PluginQiZi_t1606345050, ___isOut_4)); }
	inline bool get_isOut_4() const { return ___isOut_4; }
	inline bool* get_address_of_isOut_4() { return &___isOut_4; }
	inline void set_isOut_4(bool value)
	{
		___isOut_4 = value;
	}

	inline static int32_t get_offset_of_configId_5() { return static_cast<int32_t>(offsetof(PluginQiZi_t1606345050, ___configId_5)); }
	inline String_t* get_configId_5() const { return ___configId_5; }
	inline String_t** get_address_of_configId_5() { return &___configId_5; }
	inline void set_configId_5(String_t* value)
	{
		___configId_5 = value;
		Il2CppCodeGenWriteBarrier(&___configId_5, value);
	}

	inline static int32_t get_offset_of_withDic_6() { return static_cast<int32_t>(offsetof(PluginQiZi_t1606345050, ___withDic_6)); }
	inline String_t* get_withDic_6() const { return ___withDic_6; }
	inline String_t** get_address_of_withDic_6() { return &___withDic_6; }
	inline void set_withDic_6(String_t* value)
	{
		___withDic_6 = value;
		Il2CppCodeGenWriteBarrier(&___withDic_6, value);
	}

	inline static int32_t get_offset_of_payInfo_7() { return static_cast<int32_t>(offsetof(PluginQiZi_t1606345050, ___payInfo_7)); }
	inline PayInfo_t1775308120 * get_payInfo_7() const { return ___payInfo_7; }
	inline PayInfo_t1775308120 ** get_address_of_payInfo_7() { return &___payInfo_7; }
	inline void set_payInfo_7(PayInfo_t1775308120 * value)
	{
		___payInfo_7 = value;
		Il2CppCodeGenWriteBarrier(&___payInfo_7, value);
	}
};

struct PluginQiZi_t1606345050_StaticFields
{
public:
	// System.Action PluginQiZi::<>f__am$cache6
	Action_t3771233898 * ___U3CU3Ef__amU24cache6_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_8() { return static_cast<int32_t>(offsetof(PluginQiZi_t1606345050_StaticFields, ___U3CU3Ef__amU24cache6_8)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache6_8() const { return ___U3CU3Ef__amU24cache6_8; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache6_8() { return &___U3CU3Ef__amU24cache6_8; }
	inline void set_U3CU3Ef__amU24cache6_8(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache6_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
