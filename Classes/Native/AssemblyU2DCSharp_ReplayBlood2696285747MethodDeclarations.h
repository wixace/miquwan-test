﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayBlood
struct ReplayBlood_t2696285747;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// CombatEntity
struct CombatEntity_t684137495;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;
// ReplayBase
struct ReplayBase_t779703160;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ReplayBase779703160.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_FightEnum_EDamageType2535357340.h"

// System.Void ReplayBlood::.ctor()
extern "C"  void ReplayBlood__ctor_m3456546456 (ReplayBlood_t2696285747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayBlood::.ctor(System.Object[])
extern "C"  void ReplayBlood__ctor_m3196566042 (ReplayBlood_t2696285747 * __this, ObjectU5BU5D_t1108656482* ___datas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity ReplayBlood::get_SrcEntity()
extern "C"  CombatEntity_t684137495 * ReplayBlood_get_SrcEntity_m3439672442 (ReplayBlood_t2696285747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] ReplayBlood::ParserJsonStr(System.String)
extern "C"  StringU5BU5D_t4054002952* ReplayBlood_ParserJsonStr_m1974419617 (ReplayBlood_t2696285747 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayBlood::GetJsonStr()
extern "C"  String_t* ReplayBlood_GetJsonStr_m3833730524 (ReplayBlood_t2696285747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayBlood::Execute()
extern "C"  void ReplayBlood_Execute_m287846571 (ReplayBlood_t2696285747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity ReplayBlood::ilo_GetEntity1(ReplayBase,System.Int32,System.Int32)
extern "C"  CombatEntity_t684137495 * ReplayBlood_ilo_GetEntity1_m935618681 (Il2CppObject * __this /* static, unused */, ReplayBase_t779703160 * ____this0, int32_t ___heroId1, int32_t ___IsEnemy2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayBlood::ilo_GetJsonStr2(ReplayBase)
extern "C"  String_t* ReplayBlood_ilo_GetJsonStr2_m2836495469 (Il2CppObject * __this /* static, unused */, ReplayBase_t779703160 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity ReplayBlood::ilo_get_Entity3(ReplayBase)
extern "C"  CombatEntity_t684137495 * ReplayBlood_ilo_get_Entity3_m563332728 (Il2CppObject * __this /* static, unused */, ReplayBase_t779703160 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayBlood::ilo_ReduceHP4(CombatEntity,System.Single,CombatEntity,FightEnum.EDamageType,System.Boolean,System.Boolean,System.Int32,System.Int32)
extern "C"  void ReplayBlood_ilo_ReduceHP4_m2189342611 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___delta1, CombatEntity_t684137495 * ___src2, int32_t ___damageType3, bool ___isRepaly4, bool ___isActionSkill5, int32_t ___neardeathprotect6, int32_t ___buffId7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
