﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Buffer.OutBuffer
struct OutBuffer_t840750200;
// System.IO.Stream
struct Stream_t1561764144;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"

// System.Void SevenZip.Buffer.OutBuffer::.ctor(System.UInt32)
extern "C"  void OutBuffer__ctor_m4206821801 (OutBuffer_t840750200 * __this, uint32_t ___bufferSize0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Buffer.OutBuffer::SetStream(System.IO.Stream)
extern "C"  void OutBuffer_SetStream_m2085044136 (OutBuffer_t840750200 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Buffer.OutBuffer::FlushStream()
extern "C"  void OutBuffer_FlushStream_m2307533875 (OutBuffer_t840750200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Buffer.OutBuffer::CloseStream()
extern "C"  void OutBuffer_CloseStream_m2211945255 (OutBuffer_t840750200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Buffer.OutBuffer::ReleaseStream()
extern "C"  void OutBuffer_ReleaseStream_m4083579574 (OutBuffer_t840750200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Buffer.OutBuffer::Init()
extern "C"  void OutBuffer_Init_m2839014723 (OutBuffer_t840750200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Buffer.OutBuffer::WriteByte(System.Byte)
extern "C"  void OutBuffer_WriteByte_m1417746645 (OutBuffer_t840750200 * __this, uint8_t ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Buffer.OutBuffer::FlushData()
extern "C"  void OutBuffer_FlushData_m2974461533 (OutBuffer_t840750200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 SevenZip.Buffer.OutBuffer::GetProcessedSize()
extern "C"  uint64_t OutBuffer_GetProcessedSize_m1791762832 (OutBuffer_t840750200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
