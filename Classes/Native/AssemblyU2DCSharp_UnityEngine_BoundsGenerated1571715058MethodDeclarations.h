﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_BoundsGenerated
struct UnityEngine_BoundsGenerated_t1571715058;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_BoundsGenerated::.ctor()
extern "C"  void UnityEngine_BoundsGenerated__ctor_m3087860665 (UnityEngine_BoundsGenerated_t1571715058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoundsGenerated::.cctor()
extern "C"  void UnityEngine_BoundsGenerated__cctor_m752303892 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoundsGenerated::Bounds_Bounds1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoundsGenerated_Bounds_Bounds1_m3933922233 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoundsGenerated::Bounds_Bounds2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoundsGenerated_Bounds_Bounds2_m883719418 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoundsGenerated::Bounds_center(JSVCall)
extern "C"  void UnityEngine_BoundsGenerated_Bounds_center_m966486193 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoundsGenerated::Bounds_size(JSVCall)
extern "C"  void UnityEngine_BoundsGenerated_Bounds_size_m3155308165 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoundsGenerated::Bounds_extents(JSVCall)
extern "C"  void UnityEngine_BoundsGenerated_Bounds_extents_m3797701949 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoundsGenerated::Bounds_min(JSVCall)
extern "C"  void UnityEngine_BoundsGenerated_Bounds_min_m2050463636 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoundsGenerated::Bounds_max(JSVCall)
extern "C"  void UnityEngine_BoundsGenerated_Bounds_max_m1576037570 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoundsGenerated::Bounds_ClosestPoint__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoundsGenerated_Bounds_ClosestPoint__Vector3_m926103894 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoundsGenerated::Bounds_Contains__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoundsGenerated_Bounds_Contains__Vector3_m164242158 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoundsGenerated::Bounds_Encapsulate__Bounds(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoundsGenerated_Bounds_Encapsulate__Bounds_m1968998307 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoundsGenerated::Bounds_Encapsulate__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoundsGenerated_Bounds_Encapsulate__Vector3_m1441536612 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoundsGenerated::Bounds_Equals__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoundsGenerated_Bounds_Equals__Object_m460582275 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoundsGenerated::Bounds_Expand__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoundsGenerated_Bounds_Expand__Vector3_m3489209875 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoundsGenerated::Bounds_Expand__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoundsGenerated_Bounds_Expand__Single_m2011602791 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoundsGenerated::Bounds_GetHashCode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoundsGenerated_Bounds_GetHashCode_m2907279726 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoundsGenerated::Bounds_IntersectRay__Ray__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoundsGenerated_Bounds_IntersectRay__Ray__Single_m3124146212 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoundsGenerated::Bounds_IntersectRay__Ray(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoundsGenerated_Bounds_IntersectRay__Ray_m431783132 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoundsGenerated::Bounds_Intersects__Bounds(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoundsGenerated_Bounds_Intersects__Bounds_m1499402030 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoundsGenerated::Bounds_SetMinMax__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoundsGenerated_Bounds_SetMinMax__Vector3__Vector3_m3597491537 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoundsGenerated::Bounds_SqrDistance__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoundsGenerated_Bounds_SqrDistance__Vector3_m4017248588 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoundsGenerated::Bounds_ToString__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoundsGenerated_Bounds_ToString__String_m3564116418 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoundsGenerated::Bounds_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoundsGenerated_Bounds_ToString_m1942510705 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoundsGenerated::Bounds_op_Equality__Bounds__Bounds(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoundsGenerated_Bounds_op_Equality__Bounds__Bounds_m669990751 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoundsGenerated::Bounds_op_Inequality__Bounds__Bounds(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoundsGenerated_Bounds_op_Inequality__Bounds__Bounds_m3277308228 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoundsGenerated::__Register()
extern "C"  void UnityEngine_BoundsGenerated___Register_m133096622 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoundsGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_BoundsGenerated_ilo_attachFinalizerObject1_m599305054 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoundsGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_BoundsGenerated_ilo_addJSCSRel2_m405722742 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_BoundsGenerated::ilo_getObject3(System.Int32)
extern "C"  int32_t UnityEngine_BoundsGenerated_ilo_getObject3_m1139889567 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_BoundsGenerated::ilo_getVector3S4(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_BoundsGenerated_ilo_getVector3S4_m1529151088 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoundsGenerated::ilo_setVector3S5(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_BoundsGenerated_ilo_setVector3S5_m2303604722 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoundsGenerated::ilo_setBooleanS6(System.Int32,System.Boolean)
extern "C"  void UnityEngine_BoundsGenerated_ilo_setBooleanS6_m3734252933 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoundsGenerated::ilo_changeJSObj7(System.Int32,System.Object)
extern "C"  void UnityEngine_BoundsGenerated_ilo_changeJSObj7_m277253206 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_BoundsGenerated::ilo_getObject8(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_BoundsGenerated_ilo_getObject8_m1526922131 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_BoundsGenerated::ilo_getStringS9(System.Int32)
extern "C"  String_t* UnityEngine_BoundsGenerated_ilo_getStringS9_m2995611301 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
