﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TBDragToMove
struct TBDragToMove_t231186510;
// DragGesture
struct DragGesture_t2914643285;
// FingerGestures/FingerList
struct FingerList_t1886137443;
// Gesture
struct Gesture_t1589572905;
// ContinuousGesture
struct ContinuousGesture_t3323503386;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_DragGesture2914643285.h"
#include "AssemblyU2DCSharp_Gesture1589572905.h"
#include "AssemblyU2DCSharp_ContinuousGesturePhase2680903297.h"
#include "AssemblyU2DCSharp_ContinuousGesture3323503386.h"
#include "AssemblyU2DCSharp_TBDragToMove231186510.h"

// System.Void TBDragToMove::.ctor()
extern "C"  void TBDragToMove__ctor_m1593100813 (TBDragToMove_t231186510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TBDragToMove::get_Dragging()
extern "C"  bool TBDragToMove_get_Dragging_m2089114211 (TBDragToMove_t231186510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBDragToMove::set_Dragging(System.Boolean)
extern "C"  void TBDragToMove_set_Dragging_m2042802522 (TBDragToMove_t231186510 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBDragToMove::Start()
extern "C"  void TBDragToMove_Start_m540238605 (TBDragToMove_t231186510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TBDragToMove::ProjectScreenPointOnDragPlane(UnityEngine.Vector3,UnityEngine.Vector2,UnityEngine.Vector3&)
extern "C"  bool TBDragToMove_ProjectScreenPointOnDragPlane_m1552644001 (TBDragToMove_t231186510 * __this, Vector3_t4282066566  ___refPos0, Vector2_t4282066565  ___screenPos1, Vector3_t4282066566 * ___worldPos2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBDragToMove::HandleDrag(DragGesture)
extern "C"  void TBDragToMove_HandleDrag_m2301961674 (TBDragToMove_t231186510 * __this, DragGesture_t2914643285 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBDragToMove::FixedUpdate()
extern "C"  void TBDragToMove_FixedUpdate_m2289052104 (TBDragToMove_t231186510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBDragToMove::OnDrag(DragGesture)
extern "C"  void TBDragToMove_OnDrag_m1138208755 (TBDragToMove_t231186510 * __this, DragGesture_t2914643285 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBDragToMove::OnDisable()
extern "C"  void TBDragToMove_OnDisable_m4064169972 (TBDragToMove_t231186510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/FingerList TBDragToMove::ilo_get_Fingers1(Gesture)
extern "C"  FingerList_t1886137443 * TBDragToMove_ilo_get_Fingers1_m3797206335 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ContinuousGesturePhase TBDragToMove::ilo_get_Phase2(ContinuousGesture)
extern "C"  int32_t TBDragToMove_ilo_get_Phase2_m2657088146 (Il2CppObject * __this /* static, unused */, ContinuousGesture_t3323503386 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TBDragToMove::ilo_get_Dragging3(TBDragToMove)
extern "C"  bool TBDragToMove_ilo_get_Dragging3_m3562663473 (Il2CppObject * __this /* static, unused */, TBDragToMove_t231186510 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
