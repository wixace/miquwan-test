﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.PathNNConstraint
struct PathNNConstraint_t2457425624;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.PathNNConstraint::.ctor()
extern "C"  void PathNNConstraint__ctor_m3365934735 (PathNNConstraint_t2457425624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathNNConstraint Pathfinding.PathNNConstraint::get_Default()
extern "C"  PathNNConstraint_t2457425624 * PathNNConstraint_get_Default_m1575994932 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PathNNConstraint::SetStart(Pathfinding.GraphNode)
extern "C"  void PathNNConstraint_SetStart_m2764508695 (PathNNConstraint_t2457425624 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
