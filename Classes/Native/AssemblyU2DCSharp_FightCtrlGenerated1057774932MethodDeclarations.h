﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FightCtrlGenerated
struct FightCtrlGenerated_t1057774932;
// JSVCall
struct JSVCall_t3708497963;
// System.Action
struct Action_t3771233898;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// FightCtrl
struct FightCtrl_t648967803;
// Skill
struct Skill_t79944241;
// skillCfg
struct skillCfg_t2142425171;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_FightCtrl648967803.h"
#include "AssemblyU2DCSharp_Skill79944241.h"
#include "AssemblyU2DCSharp_FightEnum_EFightRst3627658870.h"
#include "AssemblyU2DCSharp_FightEnum_ESkillState1611731219.h"
#include "AssemblyU2DCSharp_skillCfg2142425171.h"
#include "AssemblyU2DCSharp_FightEnum_EMoveEvent3662091020.h"

// System.Void FightCtrlGenerated::.ctor()
extern "C"  void FightCtrlGenerated__ctor_m2630574535 (FightCtrlGenerated_t1057774932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_FightCtrl1(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_FightCtrl1_m3506014747 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::FightCtrl_skillEffectState(JSVCall)
extern "C"  void FightCtrlGenerated_FightCtrl_skillEffectState_m1182741303 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::FightCtrl_atkTarget(JSVCall)
extern "C"  void FightCtrlGenerated_FightCtrl_atkTarget_m1032632861 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::FightCtrl_hitList(JSVCall)
extern "C"  void FightCtrlGenerated_FightCtrl_hitList_m3037516437 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::FightCtrl_sputterList(JSVCall)
extern "C"  void FightCtrlGenerated_FightCtrl_sputterList_m1276026499 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::FightCtrl_isUsingCD(JSVCall)
extern "C"  void FightCtrlGenerated_FightCtrl_isUsingCD_m1362622795 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::FightCtrl_isProBar(JSVCall)
extern "C"  void FightCtrlGenerated_FightCtrl_isProBar_m3344583382 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::FightCtrl_activeSkill(JSVCall)
extern "C"  void FightCtrlGenerated_FightCtrl_activeSkill_m3955349435 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::FightCtrl_normalAttackSkill(JSVCall)
extern "C"  void FightCtrlGenerated_FightCtrl_normalAttackSkill_m4050932484 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::FightCtrl_deathSkill(JSVCall)
extern "C"  void FightCtrlGenerated_FightCtrl_deathSkill_m3161880873 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::FightCtrl_wakeSkill(JSVCall)
extern "C"  void FightCtrlGenerated_FightCtrl_wakeSkill_m3165597145 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::FightCtrl_isUsingSkill(JSVCall)
extern "C"  void FightCtrlGenerated_FightCtrl_isUsingSkill_m2965937583 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::FightCtrl_isUsingActiveSkill(JSVCall)
extern "C"  void FightCtrlGenerated_FightCtrl_isUsingActiveSkill_m2792012501 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::FightCtrl_isUsingNormalSkill(JSVCall)
extern "C"  void FightCtrlGenerated_FightCtrl_isUsingNormalSkill_m3140197110 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_ActiveSkillFillConsume__Skill(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_ActiveSkillFillConsume__Skill_m888943936 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_ActiveSkillForbid__Skill(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_ActiveSkillForbid__Skill_m4123357239 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_AddSkill__Int32(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_AddSkill__Int32_m459293915 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_AddSkill__Int32_Array(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_AddSkill__Int32_Array_m3692536597 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_AwakAddSkillBuff(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_AwakAddSkillBuff_m2895220744 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_BreakSkill__Boolean(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_BreakSkill__Boolean_m2767635027 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_CanUseSkill__Int32(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_CanUseSkill__Int32_m3617839865 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_Clear(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_Clear_m3980241546 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_ClearAllSkillUseState(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_ClearAllSkillUseState_m4032238020 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_ClearmLastUseTime(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_ClearmLastUseTime_m923233563 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_ClearSkillUseState__ESkillState(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_ClearSkillUseState__ESkillState_m3501126676 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_DeathUsingSkill(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_DeathUsingSkill_m1367854078 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_DelSkill__Int32_Array(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_DelSkill__Int32_Array_m4165106815 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_DelSkill__Int32(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_DelSkill__Int32_m22869445 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_Fight(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_Fight_m343119885 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_FindSkillByIndex__Int32(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_FindSkillByIndex__Int32_m1212347496 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_ForceAddSkill__Int32(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_ForceAddSkill__Int32_m1077333080 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_ForceUsingSkill__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_ForceUsingSkill__Int32__Int32__Int32_m479060987 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_GetCurSkillID(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_GetCurSkillID_m541596479 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_GetNextSkillIndex(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_GetNextSkillIndex_m1074036679 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_GetSkill__Int32(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_GetSkill__Int32_m1931112016 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_HasSkillUseState__ESkillState(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_HasSkillUseState__ESkillState_m2306485601 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_Init__String_Array(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_Init__String_Array_m1744833792 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_PlayAction(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_PlayAction_m1215931567 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_PlaySound(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_PlaySound_m2905212408 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_ReduceCDBySkillID__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_ReduceCDBySkillID__Int32__Int32_m4090086539 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_SetDelayPauseState__Boolean(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_SetDelayPauseState__Boolean_m3144389705 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_SetNextSkillID__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_SetNextSkillID__Int32__Boolean_m2664372022 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_SetNextSkillIDByAI__Int32(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_SetNextSkillIDByAI__Int32_m2296006517 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_SetSkillUseState__ESkillState(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_SetSkillUseState__ESkillState_m916760361 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_SkillShift__skillCfg__EMoveEvent__ListT1_CombatEntity(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_SkillShift__skillCfg__EMoveEvent__ListT1_CombatEntity_m963753744 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_TeamAddSkill__Int32_Array(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_TeamAddSkill__Int32_Array_m1287207544 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_Update(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_Update_m2591408046 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_UseingSkill(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_UseingSkill_m4039005331 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_UseSkill__Int32(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_UseSkill__Int32_m3674855489 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action FightCtrlGenerated::FightCtrl_UseSkillAuto_GetDelegate_member35_arg1(CSRepresentedObject)
extern "C"  Action_t3771233898 * FightCtrlGenerated_FightCtrl_UseSkillAuto_GetDelegate_member35_arg1_m2734293833 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_UseSkillAuto__Skill__Action(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_UseSkillAuto__Skill__Action_m306630923 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_UseSkillByID__Int32(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_UseSkillByID__Int32_m3515142319 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action FightCtrlGenerated::FightCtrl_UseSkillUI_GetDelegate_member37_arg1(CSRepresentedObject)
extern "C"  Action_t3771233898 * FightCtrlGenerated_FightCtrl_UseSkillUI_GetDelegate_member37_arg1_m1310153574 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::FightCtrl_UseSkillUI__Int32__Action(JSVCall,System.Int32)
extern "C"  bool FightCtrlGenerated_FightCtrl_UseSkillUI__Int32__Action_m2886044515 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::__Register()
extern "C"  void FightCtrlGenerated___Register_m3915325280 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] FightCtrlGenerated::<FightCtrl_AddSkill__Int32_Array>m__4E()
extern "C"  Int32U5BU5D_t3230847821* FightCtrlGenerated_U3CFightCtrl_AddSkill__Int32_ArrayU3Em__4E_m447548481 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] FightCtrlGenerated::<FightCtrl_DelSkill__Int32_Array>m__4F()
extern "C"  Int32U5BU5D_t3230847821* FightCtrlGenerated_U3CFightCtrl_DelSkill__Int32_ArrayU3Em__4F_m1375095532 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] FightCtrlGenerated::<FightCtrl_Init__String_Array>m__50()
extern "C"  StringU5BU5D_t4054002952* FightCtrlGenerated_U3CFightCtrl_Init__String_ArrayU3Em__50_m279945353 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] FightCtrlGenerated::<FightCtrl_TeamAddSkill__Int32_Array>m__51()
extern "C"  Int32U5BU5D_t3230847821* FightCtrlGenerated_U3CFightCtrl_TeamAddSkill__Int32_ArrayU3Em__51_m445716527 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action FightCtrlGenerated::<FightCtrl_UseSkillAuto__Skill__Action>m__53()
extern "C"  Action_t3771233898 * FightCtrlGenerated_U3CFightCtrl_UseSkillAuto__Skill__ActionU3Em__53_m3948585782 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action FightCtrlGenerated::<FightCtrl_UseSkillUI__Int32__Action>m__55()
extern "C"  Action_t3771233898 * FightCtrlGenerated_U3CFightCtrl_UseSkillUI__Int32__ActionU3Em__55_m2667563664 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightCtrlGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t FightCtrlGenerated_ilo_getObject1_m2057978923 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void FightCtrlGenerated_ilo_addJSCSRel2_m2831402372 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightCtrlGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t FightCtrlGenerated_ilo_setObject3_m3654860837 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::ilo_setBooleanS4(System.Int32,System.Boolean)
extern "C"  void FightCtrlGenerated_ilo_setBooleanS4_m2622503381 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::ilo_get_isUsingNormalSkill5(FightCtrl)
extern "C"  bool FightCtrlGenerated_ilo_get_isUsingNormalSkill5_m2192331357 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::ilo_ActiveSkillForbid6(FightCtrl,Skill)
extern "C"  bool FightCtrlGenerated_ilo_ActiveSkillForbid6_m2699369303 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, Skill_t79944241 * ___skill1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::ilo_AddSkill7(FightCtrl,System.Int32[])
extern "C"  void FightCtrlGenerated_ilo_AddSkill7_m3072363607 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, Int32U5BU5D_t3230847821* ___skillIDS1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::ilo_BreakSkill8(FightCtrl,System.Boolean)
extern "C"  void FightCtrlGenerated_ilo_BreakSkill8_m1163746942 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, bool ___isForce1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::ilo_getBooleanS9(System.Int32)
extern "C"  bool FightCtrlGenerated_ilo_getBooleanS9_m308263981 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightCtrlGenerated::ilo_getInt3210(System.Int32)
extern "C"  int32_t FightCtrlGenerated_ilo_getInt3210_m3678944582 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightEnum.EFightRst FightCtrlGenerated::ilo_CanUseSkill11(FightCtrl,System.Int32)
extern "C"  int32_t FightCtrlGenerated_ilo_CanUseSkill11_m3854699652 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, int32_t ___skillIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::ilo_Clear12(FightCtrl)
extern "C"  void FightCtrlGenerated_ilo_Clear12_m1925030487 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightCtrlGenerated::ilo_getEnum13(System.Int32)
extern "C"  int32_t FightCtrlGenerated_ilo_getEnum13_m3320112014 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::ilo_DelSkill14(FightCtrl,System.Int32[])
extern "C"  void FightCtrlGenerated_ilo_DelSkill14_m2355370999 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, Int32U5BU5D_t3230847821* ___skillIDS1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::ilo_DelSkill15(FightCtrl,System.Int32)
extern "C"  void FightCtrlGenerated_ilo_DelSkill15_m3940877338 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, int32_t ___skillID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::ilo_ForceUsingSkill16(FightCtrl,System.Int32,System.Int32,System.Int32)
extern "C"  void FightCtrlGenerated_ilo_ForceUsingSkill16_m3768277679 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, int32_t ___skillId1, int32_t ___hostId2, int32_t ___effectId3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightCtrlGenerated::ilo_GetCurSkillID17(FightCtrl)
extern "C"  int32_t FightCtrlGenerated_ilo_GetCurSkillID17_m695170155 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::ilo_setInt3218(System.Int32,System.Int32)
extern "C"  void FightCtrlGenerated_ilo_setInt3218_m1641483683 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightCtrlGenerated::ilo_HasSkillUseState19(FightCtrl,FightEnum.ESkillState)
extern "C"  bool FightCtrlGenerated_ilo_HasSkillUseState19_m1684704942 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, int32_t ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::ilo_SetDelayPauseState20(FightCtrl,System.Boolean)
extern "C"  void FightCtrlGenerated_ilo_SetDelayPauseState20_m358048304 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, bool ___isPause1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::ilo_SkillShift21(FightCtrl,skillCfg,FightEnum.EMoveEvent,System.Collections.Generic.List`1<CombatEntity>)
extern "C"  void FightCtrlGenerated_ilo_SkillShift21_m3276276044 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, skillCfg_t2142425171 * ___skillInfo1, int32_t ___moveEvent2, List_1_t2052323047 * ___hitEntitys3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightCtrlGenerated::ilo_UseingSkill22(FightCtrl)
extern "C"  void FightCtrlGenerated_ilo_UseingSkill22_m2976946287 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightCtrlGenerated::ilo_getArrayLength23(System.Int32)
extern "C"  int32_t FightCtrlGenerated_ilo_getArrayLength23_m1025542841 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightCtrlGenerated::ilo_getElement24(System.Int32,System.Int32)
extern "C"  int32_t FightCtrlGenerated_ilo_getElement24_m2045456224 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action FightCtrlGenerated::ilo_FightCtrl_UseSkillAuto_GetDelegate_member35_arg125(CSRepresentedObject)
extern "C"  Action_t3771233898 * FightCtrlGenerated_ilo_FightCtrl_UseSkillAuto_GetDelegate_member35_arg125_m2838204627 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FightCtrlGenerated::ilo_getObject26(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * FightCtrlGenerated_ilo_getObject26_m2840530269 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject FightCtrlGenerated::ilo_getFunctionS27(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * FightCtrlGenerated_ilo_getFunctionS27_m1514472648 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action FightCtrlGenerated::ilo_FightCtrl_UseSkillUI_GetDelegate_member37_arg128(CSRepresentedObject)
extern "C"  Action_t3771233898 * FightCtrlGenerated_ilo_FightCtrl_UseSkillUI_GetDelegate_member37_arg128_m1601273037 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
