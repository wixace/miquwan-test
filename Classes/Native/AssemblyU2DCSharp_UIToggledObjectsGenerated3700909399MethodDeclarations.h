﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIToggledObjectsGenerated
struct UIToggledObjectsGenerated_t3700909399;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UIToggledObjectsGenerated::.ctor()
extern "C"  void UIToggledObjectsGenerated__ctor_m2018767348 (UIToggledObjectsGenerated_t3700909399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggledObjectsGenerated::UIToggledObjects_UIToggledObjects1(JSVCall,System.Int32)
extern "C"  bool UIToggledObjectsGenerated_UIToggledObjects_UIToggledObjects1_m3097943294 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggledObjectsGenerated::UIToggledObjects_activate(JSVCall)
extern "C"  void UIToggledObjectsGenerated_UIToggledObjects_activate_m1059834811 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggledObjectsGenerated::UIToggledObjects_deactivate(JSVCall)
extern "C"  void UIToggledObjectsGenerated_UIToggledObjects_deactivate_m1407901530 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggledObjectsGenerated::UIToggledObjects_Toggle(JSVCall,System.Int32)
extern "C"  bool UIToggledObjectsGenerated_UIToggledObjects_Toggle_m4241870193 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggledObjectsGenerated::__Register()
extern "C"  void UIToggledObjectsGenerated___Register_m159032147 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIToggledObjectsGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UIToggledObjectsGenerated_ilo_getObject1_m4114029122 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIToggledObjectsGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIToggledObjectsGenerated_ilo_getObject2_m618297266 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIToggledObjectsGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIToggledObjectsGenerated_ilo_setObject3_m3387914812 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
