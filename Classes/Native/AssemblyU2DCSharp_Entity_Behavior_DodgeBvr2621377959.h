﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.DodgeBvr
struct  DodgeBvr_t2621377959  : public IBehavior_t770859129
{
public:
	// System.Single Entity.Behavior.DodgeBvr::distance
	float ___distance_2;
	// System.Boolean Entity.Behavior.DodgeBvr::isRandDir
	bool ___isRandDir_3;
	// System.Boolean Entity.Behavior.DodgeBvr::isLock
	bool ___isLock_4;
	// UnityEngine.Vector3 Entity.Behavior.DodgeBvr::targetPos
	Vector3_t4282066566  ___targetPos_5;

public:
	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(DodgeBvr_t2621377959, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_isRandDir_3() { return static_cast<int32_t>(offsetof(DodgeBvr_t2621377959, ___isRandDir_3)); }
	inline bool get_isRandDir_3() const { return ___isRandDir_3; }
	inline bool* get_address_of_isRandDir_3() { return &___isRandDir_3; }
	inline void set_isRandDir_3(bool value)
	{
		___isRandDir_3 = value;
	}

	inline static int32_t get_offset_of_isLock_4() { return static_cast<int32_t>(offsetof(DodgeBvr_t2621377959, ___isLock_4)); }
	inline bool get_isLock_4() const { return ___isLock_4; }
	inline bool* get_address_of_isLock_4() { return &___isLock_4; }
	inline void set_isLock_4(bool value)
	{
		___isLock_4 = value;
	}

	inline static int32_t get_offset_of_targetPos_5() { return static_cast<int32_t>(offsetof(DodgeBvr_t2621377959, ___targetPos_5)); }
	inline Vector3_t4282066566  get_targetPos_5() const { return ___targetPos_5; }
	inline Vector3_t4282066566 * get_address_of_targetPos_5() { return &___targetPos_5; }
	inline void set_targetPos_5(Vector3_t4282066566  value)
	{
		___targetPos_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
