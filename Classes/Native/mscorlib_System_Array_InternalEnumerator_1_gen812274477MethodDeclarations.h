﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen812274477.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_VOLin2029931801.h"

// System.Void System.Array/InternalEnumerator`1<Pathfinding.LocalAvoidance/VOLine>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3440748254_gshared (InternalEnumerator_1_t812274477 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3440748254(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t812274477 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3440748254_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2758981122_gshared (InternalEnumerator_1_t812274477 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2758981122(__this, method) ((  void (*) (InternalEnumerator_1_t812274477 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2758981122_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4154256312_gshared (InternalEnumerator_1_t812274477 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4154256312(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t812274477 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4154256312_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.LocalAvoidance/VOLine>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1903070773_gshared (InternalEnumerator_1_t812274477 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1903070773(__this, method) ((  void (*) (InternalEnumerator_1_t812274477 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1903070773_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.LocalAvoidance/VOLine>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3745996082_gshared (InternalEnumerator_1_t812274477 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3745996082(__this, method) ((  bool (*) (InternalEnumerator_1_t812274477 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3745996082_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Pathfinding.LocalAvoidance/VOLine>::get_Current()
extern "C"  VOLine_t2029931801  InternalEnumerator_1_get_Current_m1622204871_gshared (InternalEnumerator_1_t812274477 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1622204871(__this, method) ((  VOLine_t2029931801  (*) (InternalEnumerator_1_t812274477 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1622204871_gshared)(__this, method)
