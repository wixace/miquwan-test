﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_DistanceJoint2DGenerated
struct UnityEngine_DistanceJoint2DGenerated_t3236733968;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_DistanceJoint2DGenerated::.ctor()
extern "C"  void UnityEngine_DistanceJoint2DGenerated__ctor_m3201570955 (UnityEngine_DistanceJoint2DGenerated_t3236733968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_DistanceJoint2DGenerated::DistanceJoint2D_DistanceJoint2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_DistanceJoint2DGenerated_DistanceJoint2D_DistanceJoint2D1_m2518337471 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DistanceJoint2DGenerated::DistanceJoint2D_autoConfigureDistance(JSVCall)
extern "C"  void UnityEngine_DistanceJoint2DGenerated_DistanceJoint2D_autoConfigureDistance_m1424169706 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DistanceJoint2DGenerated::DistanceJoint2D_distance(JSVCall)
extern "C"  void UnityEngine_DistanceJoint2DGenerated_DistanceJoint2D_distance_m619928641 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DistanceJoint2DGenerated::DistanceJoint2D_maxDistanceOnly(JSVCall)
extern "C"  void UnityEngine_DistanceJoint2DGenerated_DistanceJoint2D_maxDistanceOnly_m3265917265 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DistanceJoint2DGenerated::__Register()
extern "C"  void UnityEngine_DistanceJoint2DGenerated___Register_m2899182364 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DistanceJoint2DGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_DistanceJoint2DGenerated_ilo_addJSCSRel1_m2589427143 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_DistanceJoint2DGenerated::ilo_setBooleanS2(System.Int32,System.Boolean)
extern "C"  void UnityEngine_DistanceJoint2DGenerated_ilo_setBooleanS2_m2340709979 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_DistanceJoint2DGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_DistanceJoint2DGenerated_ilo_getSingle3_m2135227390 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
