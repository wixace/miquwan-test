﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>
struct ReadOnlyCollection_1_t588236419;
// System.Collections.Generic.IList`1<Pathfinding.ClipperLib.IntPoint>
struct IList_1_t1725806086;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.ClipperLib.IntPoint[]
struct IntPointU5BU5D_t3364663986;
// System.Collections.Generic.IEnumerator`1<Pathfinding.ClipperLib.IntPoint>
struct IEnumerator_1_t943023932;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m1607445891_gshared (ReadOnlyCollection_1_t588236419 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1607445891(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t588236419 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1607445891_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1665885677_gshared (ReadOnlyCollection_1_t588236419 * __this, IntPoint_t3326126179  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1665885677(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t588236419 *, IntPoint_t3326126179 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1665885677_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2109539453_gshared (ReadOnlyCollection_1_t588236419 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2109539453(__this, method) ((  void (*) (ReadOnlyCollection_1_t588236419 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2109539453_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3041338516_gshared (ReadOnlyCollection_1_t588236419 * __this, int32_t ___index0, IntPoint_t3326126179  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3041338516(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t588236419 *, int32_t, IntPoint_t3326126179 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3041338516_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3788076390_gshared (ReadOnlyCollection_1_t588236419 * __this, IntPoint_t3326126179  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3788076390(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t588236419 *, IntPoint_t3326126179 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3788076390_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m915191386_gshared (ReadOnlyCollection_1_t588236419 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m915191386(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t588236419 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m915191386_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  IntPoint_t3326126179  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1096767710_gshared (ReadOnlyCollection_1_t588236419 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1096767710(__this, ___index0, method) ((  IntPoint_t3326126179  (*) (ReadOnlyCollection_1_t588236419 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1096767710_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3958149227_gshared (ReadOnlyCollection_1_t588236419 * __this, int32_t ___index0, IntPoint_t3326126179  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3958149227(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t588236419 *, int32_t, IntPoint_t3326126179 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3958149227_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4098394473_gshared (ReadOnlyCollection_1_t588236419 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4098394473(__this, method) ((  bool (*) (ReadOnlyCollection_1_t588236419 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4098394473_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1321115826_gshared (ReadOnlyCollection_1_t588236419 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1321115826(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t588236419 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1321115826_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m41028397_gshared (ReadOnlyCollection_1_t588236419 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m41028397(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t588236419 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m41028397_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m4082229220_gshared (ReadOnlyCollection_1_t588236419 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m4082229220(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t588236419 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m4082229220_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m952825792_gshared (ReadOnlyCollection_1_t588236419 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m952825792(__this, method) ((  void (*) (ReadOnlyCollection_1_t588236419 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m952825792_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2388769640_gshared (ReadOnlyCollection_1_t588236419 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2388769640(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t588236419 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2388769640_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3843523644_gshared (ReadOnlyCollection_1_t588236419 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3843523644(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t588236419 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3843523644_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1602174055_gshared (ReadOnlyCollection_1_t588236419 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1602174055(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t588236419 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1602174055_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1961316705_gshared (ReadOnlyCollection_1_t588236419 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1961316705(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t588236419 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1961316705_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2256631415_gshared (ReadOnlyCollection_1_t588236419 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2256631415(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t588236419 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2256631415_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m518193908_gshared (ReadOnlyCollection_1_t588236419 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m518193908(__this, method) ((  bool (*) (ReadOnlyCollection_1_t588236419 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m518193908_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1090973024_gshared (ReadOnlyCollection_1_t588236419 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1090973024(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t588236419 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1090973024_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m4187232471_gshared (ReadOnlyCollection_1_t588236419 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m4187232471(__this, method) ((  bool (*) (ReadOnlyCollection_1_t588236419 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m4187232471_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2251648834_gshared (ReadOnlyCollection_1_t588236419 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2251648834(__this, method) ((  bool (*) (ReadOnlyCollection_1_t588236419 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2251648834_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1518667495_gshared (ReadOnlyCollection_1_t588236419 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1518667495(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t588236419 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1518667495_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2704927358_gshared (ReadOnlyCollection_1_t588236419 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2704927358(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t588236419 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2704927358_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m2143740075_gshared (ReadOnlyCollection_1_t588236419 * __this, IntPoint_t3326126179  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m2143740075(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t588236419 *, IntPoint_t3326126179 , const MethodInfo*))ReadOnlyCollection_1_Contains_m2143740075_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3542052893_gshared (ReadOnlyCollection_1_t588236419 * __this, IntPointU5BU5D_t3364663986* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m3542052893(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t588236419 *, IntPointU5BU5D_t3364663986*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m3542052893_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3827127950_gshared (ReadOnlyCollection_1_t588236419 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3827127950(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t588236419 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3827127950_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m3624461921_gshared (ReadOnlyCollection_1_t588236419 * __this, IntPoint_t3326126179  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m3624461921(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t588236419 *, IntPoint_t3326126179 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m3624461921_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m506477370_gshared (ReadOnlyCollection_1_t588236419 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m506477370(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t588236419 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m506477370_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>::get_Item(System.Int32)
extern "C"  IntPoint_t3326126179  ReadOnlyCollection_1_get_Item_m1804341662_gshared (ReadOnlyCollection_1_t588236419 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1804341662(__this, ___index0, method) ((  IntPoint_t3326126179  (*) (ReadOnlyCollection_1_t588236419 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1804341662_gshared)(__this, ___index0, method)
