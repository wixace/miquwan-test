﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.DetailPrototype
struct DetailPrototype_t532078087;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
struct DetailPrototype_t532078087_marshaled_pinvoke;
struct DetailPrototype_t532078087_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_DetailRenderMode3449214734.h"

// System.Void UnityEngine.DetailPrototype::.ctor()
extern "C"  void DetailPrototype__ctor_m923135912 (DetailPrototype_t532078087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.DetailPrototype::get_prototype()
extern "C"  GameObject_t3674682005 * DetailPrototype_get_prototype_m2550379654 (DetailPrototype_t532078087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DetailPrototype::set_prototype(UnityEngine.GameObject)
extern "C"  void DetailPrototype_set_prototype_m3035115875 (DetailPrototype_t532078087 * __this, GameObject_t3674682005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.DetailPrototype::get_prototypeTexture()
extern "C"  Texture2D_t3884108195 * DetailPrototype_get_prototypeTexture_m1513311169 (DetailPrototype_t532078087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DetailPrototype::set_prototypeTexture(UnityEngine.Texture2D)
extern "C"  void DetailPrototype_set_prototypeTexture_m811949194 (DetailPrototype_t532078087 * __this, Texture2D_t3884108195 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.DetailPrototype::get_minWidth()
extern "C"  float DetailPrototype_get_minWidth_m1886195981 (DetailPrototype_t532078087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DetailPrototype::set_minWidth(System.Single)
extern "C"  void DetailPrototype_set_minWidth_m1796030398 (DetailPrototype_t532078087 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.DetailPrototype::get_maxWidth()
extern "C"  float DetailPrototype_get_maxWidth_m3709163963 (DetailPrototype_t532078087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DetailPrototype::set_maxWidth(System.Single)
extern "C"  void DetailPrototype_set_maxWidth_m3034120656 (DetailPrototype_t532078087 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.DetailPrototype::get_minHeight()
extern "C"  float DetailPrototype_get_minHeight_m3141314914 (DetailPrototype_t532078087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DetailPrototype::set_minHeight(System.Single)
extern "C"  void DetailPrototype_set_minHeight_m2288383369 (DetailPrototype_t532078087 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.DetailPrototype::get_maxHeight()
extern "C"  float DetailPrototype_get_maxHeight_m3818747508 (DetailPrototype_t532078087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DetailPrototype::set_maxHeight(System.Single)
extern "C"  void DetailPrototype_set_maxHeight_m2014475703 (DetailPrototype_t532078087 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.DetailPrototype::get_noiseSpread()
extern "C"  float DetailPrototype_get_noiseSpread_m442064246 (DetailPrototype_t532078087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DetailPrototype::set_noiseSpread(System.Single)
extern "C"  void DetailPrototype_set_noiseSpread_m3047555573 (DetailPrototype_t532078087 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.DetailPrototype::get_bendFactor()
extern "C"  float DetailPrototype_get_bendFactor_m4194172769 (DetailPrototype_t532078087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DetailPrototype::set_bendFactor(System.Single)
extern "C"  void DetailPrototype_set_bendFactor_m1734392298 (DetailPrototype_t532078087 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.DetailPrototype::get_healthyColor()
extern "C"  Color_t4194546905  DetailPrototype_get_healthyColor_m191660132 (DetailPrototype_t532078087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DetailPrototype::set_healthyColor(UnityEngine.Color)
extern "C"  void DetailPrototype_set_healthyColor_m2774666311 (DetailPrototype_t532078087 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.DetailPrototype::get_dryColor()
extern "C"  Color_t4194546905  DetailPrototype_get_dryColor_m1513901846 (DetailPrototype_t532078087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DetailPrototype::set_dryColor(UnityEngine.Color)
extern "C"  void DetailPrototype_set_dryColor_m3288400469 (DetailPrototype_t532078087 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.DetailRenderMode UnityEngine.DetailPrototype::get_renderMode()
extern "C"  int32_t DetailPrototype_get_renderMode_m2753305552 (DetailPrototype_t532078087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DetailPrototype::set_renderMode(UnityEngine.DetailRenderMode)
extern "C"  void DetailPrototype_set_renderMode_m2691128177 (DetailPrototype_t532078087 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.DetailPrototype::get_usePrototypeMesh()
extern "C"  bool DetailPrototype_get_usePrototypeMesh_m4104228679 (DetailPrototype_t532078087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DetailPrototype::set_usePrototypeMesh(System.Boolean)
extern "C"  void DetailPrototype_set_usePrototypeMesh_m327519000 (DetailPrototype_t532078087 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct DetailPrototype_t532078087;
struct DetailPrototype_t532078087_marshaled_pinvoke;

extern "C" void DetailPrototype_t532078087_marshal_pinvoke(const DetailPrototype_t532078087& unmarshaled, DetailPrototype_t532078087_marshaled_pinvoke& marshaled);
extern "C" void DetailPrototype_t532078087_marshal_pinvoke_back(const DetailPrototype_t532078087_marshaled_pinvoke& marshaled, DetailPrototype_t532078087& unmarshaled);
extern "C" void DetailPrototype_t532078087_marshal_pinvoke_cleanup(DetailPrototype_t532078087_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct DetailPrototype_t532078087;
struct DetailPrototype_t532078087_marshaled_com;

extern "C" void DetailPrototype_t532078087_marshal_com(const DetailPrototype_t532078087& unmarshaled, DetailPrototype_t532078087_marshaled_com& marshaled);
extern "C" void DetailPrototype_t532078087_marshal_com_back(const DetailPrototype_t532078087_marshaled_com& marshaled, DetailPrototype_t532078087& unmarshaled);
extern "C" void DetailPrototype_t532078087_marshal_com_cleanup(DetailPrototype_t532078087_marshaled_com& marshaled);
