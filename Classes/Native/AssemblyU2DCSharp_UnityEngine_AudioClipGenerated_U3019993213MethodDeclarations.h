﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AudioClipGenerated/<AudioClip_Create_GetDelegate_member4_arg6>c__AnonStoreyDC
struct U3CAudioClip_Create_GetDelegate_member4_arg6U3Ec__AnonStoreyDC_t3019993213;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_AudioClipGenerated/<AudioClip_Create_GetDelegate_member4_arg6>c__AnonStoreyDC::.ctor()
extern "C"  void U3CAudioClip_Create_GetDelegate_member4_arg6U3Ec__AnonStoreyDC__ctor_m210882958 (U3CAudioClip_Create_GetDelegate_member4_arg6U3Ec__AnonStoreyDC_t3019993213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioClipGenerated/<AudioClip_Create_GetDelegate_member4_arg6>c__AnonStoreyDC::<>m__187(System.Int32)
extern "C"  void U3CAudioClip_Create_GetDelegate_member4_arg6U3Ec__AnonStoreyDC_U3CU3Em__187_m109247404 (U3CAudioClip_Create_GetDelegate_member4_arg6U3Ec__AnonStoreyDC_t3019993213 * __this, int32_t ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
