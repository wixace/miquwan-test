﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GeometryUtility
struct GeometryUtility_t1481310928;
// UnityEngine.Plane[]
struct PlaneU5BU5D_t1447812263;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"

// System.Void UnityEngine.GeometryUtility::.ctor()
extern "C"  void GeometryUtility__ctor_m2256805119 (GeometryUtility_t1481310928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Plane[] UnityEngine.GeometryUtility::CalculateFrustumPlanes(UnityEngine.Camera)
extern "C"  PlaneU5BU5D_t1447812263* GeometryUtility_CalculateFrustumPlanes_m4034520784 (Il2CppObject * __this /* static, unused */, Camera_t2727095145 * ___camera0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Plane[] UnityEngine.GeometryUtility::CalculateFrustumPlanes(UnityEngine.Matrix4x4)
extern "C"  PlaneU5BU5D_t1447812263* GeometryUtility_CalculateFrustumPlanes_m2105136846 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333  ___worldToProjectionMatrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GeometryUtility::Internal_ExtractPlanes(UnityEngine.Plane[],UnityEngine.Matrix4x4)
extern "C"  void GeometryUtility_Internal_ExtractPlanes_m1419166916 (Il2CppObject * __this /* static, unused */, PlaneU5BU5D_t1447812263* ___planes0, Matrix4x4_t1651859333  ___worldToProjectionMatrix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GeometryUtility::INTERNAL_CALL_Internal_ExtractPlanes(UnityEngine.Plane[],UnityEngine.Matrix4x4&)
extern "C"  void GeometryUtility_INTERNAL_CALL_Internal_ExtractPlanes_m750424711 (Il2CppObject * __this /* static, unused */, PlaneU5BU5D_t1447812263* ___planes0, Matrix4x4_t1651859333 * ___worldToProjectionMatrix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GeometryUtility::TestPlanesAABB(UnityEngine.Plane[],UnityEngine.Bounds)
extern "C"  bool GeometryUtility_TestPlanesAABB_m1286581431 (Il2CppObject * __this /* static, unused */, PlaneU5BU5D_t1447812263* ___planes0, Bounds_t2711641849  ___bounds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GeometryUtility::INTERNAL_CALL_TestPlanesAABB(UnityEngine.Plane[],UnityEngine.Bounds&)
extern "C"  bool GeometryUtility_INTERNAL_CALL_TestPlanesAABB_m3075197398 (Il2CppObject * __this /* static, unused */, PlaneU5BU5D_t1447812263* ___planes0, Bounds_t2711641849 * ___bounds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
