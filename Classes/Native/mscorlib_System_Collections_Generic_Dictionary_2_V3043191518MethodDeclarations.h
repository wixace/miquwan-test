﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va749037579MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,ISound>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1918768260(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3043191518 *, Dictionary_2_t47618509 *, const MethodInfo*))ValueCollection__ctor_m1989077408_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,ISound>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2148108078(__this, ___item0, method) ((  void (*) (ValueCollection_t3043191518 *, ISound_t2170003014 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1041997458_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,ISound>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3134077687(__this, method) ((  void (*) (ValueCollection_t3043191518 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m708551771_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,ISound>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4289496956(__this, ___item0, method) ((  bool (*) (ValueCollection_t3043191518 *, ISound_t2170003014 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4273385556_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,ISound>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3250147105(__this, ___item0, method) ((  bool (*) (ValueCollection_t3043191518 *, ISound_t2170003014 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m845663737_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,ISound>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2999708599(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3043191518 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2085042217_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,ISound>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3601650491(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3043191518 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2558578591_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,ISound>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3859132938(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3043191518 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m998340826_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,ISound>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2564672815(__this, method) ((  bool (*) (ValueCollection_t3043191518 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2548561415_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,ISound>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m960700815(__this, method) ((  bool (*) (ValueCollection_t3043191518 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2925645415_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,ISound>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3734988353(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3043191518 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m45093459_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,ISound>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m2277038027(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3043191518 *, ISoundU5BU5D_t1350087139*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1218987495_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,ISound>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3797953140(__this, method) ((  Enumerator_t2274419213  (*) (ValueCollection_t3043191518 *, const MethodInfo*))ValueCollection_GetEnumerator_m3758762570_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,ISound>::get_Count()
#define ValueCollection_get_Count_m1879667145(__this, method) ((  int32_t (*) (ValueCollection_t3043191518 *, const MethodInfo*))ValueCollection_get_Count_m1914373933_gshared)(__this, method)
