﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.ClipperLib.TEdge
struct TEdge_t3950806139;
// Pathfinding.ClipperLib.LocalMinima
struct LocalMinima_t2863342752;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.ClipperLib.LocalMinima
struct  LocalMinima_t2863342752  : public Il2CppObject
{
public:
	// System.Int64 Pathfinding.ClipperLib.LocalMinima::Y
	int64_t ___Y_0;
	// Pathfinding.ClipperLib.TEdge Pathfinding.ClipperLib.LocalMinima::LeftBound
	TEdge_t3950806139 * ___LeftBound_1;
	// Pathfinding.ClipperLib.TEdge Pathfinding.ClipperLib.LocalMinima::RightBound
	TEdge_t3950806139 * ___RightBound_2;
	// Pathfinding.ClipperLib.LocalMinima Pathfinding.ClipperLib.LocalMinima::Next
	LocalMinima_t2863342752 * ___Next_3;

public:
	inline static int32_t get_offset_of_Y_0() { return static_cast<int32_t>(offsetof(LocalMinima_t2863342752, ___Y_0)); }
	inline int64_t get_Y_0() const { return ___Y_0; }
	inline int64_t* get_address_of_Y_0() { return &___Y_0; }
	inline void set_Y_0(int64_t value)
	{
		___Y_0 = value;
	}

	inline static int32_t get_offset_of_LeftBound_1() { return static_cast<int32_t>(offsetof(LocalMinima_t2863342752, ___LeftBound_1)); }
	inline TEdge_t3950806139 * get_LeftBound_1() const { return ___LeftBound_1; }
	inline TEdge_t3950806139 ** get_address_of_LeftBound_1() { return &___LeftBound_1; }
	inline void set_LeftBound_1(TEdge_t3950806139 * value)
	{
		___LeftBound_1 = value;
		Il2CppCodeGenWriteBarrier(&___LeftBound_1, value);
	}

	inline static int32_t get_offset_of_RightBound_2() { return static_cast<int32_t>(offsetof(LocalMinima_t2863342752, ___RightBound_2)); }
	inline TEdge_t3950806139 * get_RightBound_2() const { return ___RightBound_2; }
	inline TEdge_t3950806139 ** get_address_of_RightBound_2() { return &___RightBound_2; }
	inline void set_RightBound_2(TEdge_t3950806139 * value)
	{
		___RightBound_2 = value;
		Il2CppCodeGenWriteBarrier(&___RightBound_2, value);
	}

	inline static int32_t get_offset_of_Next_3() { return static_cast<int32_t>(offsetof(LocalMinima_t2863342752, ___Next_3)); }
	inline LocalMinima_t2863342752 * get_Next_3() const { return ___Next_3; }
	inline LocalMinima_t2863342752 ** get_address_of_Next_3() { return &___Next_3; }
	inline void set_Next_3(LocalMinima_t2863342752 * value)
	{
		___Next_3 = value;
		Il2CppCodeGenWriteBarrier(&___Next_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
