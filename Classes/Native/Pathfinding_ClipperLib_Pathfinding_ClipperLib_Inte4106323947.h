﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.ClipperLib.TEdge
struct TEdge_t3950806139;
// Pathfinding.ClipperLib.IntersectNode
struct IntersectNode_t4106323947;

#include "mscorlib_System_Object4170816371.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.ClipperLib.IntersectNode
struct  IntersectNode_t4106323947  : public Il2CppObject
{
public:
	// Pathfinding.ClipperLib.TEdge Pathfinding.ClipperLib.IntersectNode::Edge1
	TEdge_t3950806139 * ___Edge1_0;
	// Pathfinding.ClipperLib.TEdge Pathfinding.ClipperLib.IntersectNode::Edge2
	TEdge_t3950806139 * ___Edge2_1;
	// Pathfinding.ClipperLib.IntPoint Pathfinding.ClipperLib.IntersectNode::Pt
	IntPoint_t3326126179  ___Pt_2;
	// Pathfinding.ClipperLib.IntersectNode Pathfinding.ClipperLib.IntersectNode::Next
	IntersectNode_t4106323947 * ___Next_3;

public:
	inline static int32_t get_offset_of_Edge1_0() { return static_cast<int32_t>(offsetof(IntersectNode_t4106323947, ___Edge1_0)); }
	inline TEdge_t3950806139 * get_Edge1_0() const { return ___Edge1_0; }
	inline TEdge_t3950806139 ** get_address_of_Edge1_0() { return &___Edge1_0; }
	inline void set_Edge1_0(TEdge_t3950806139 * value)
	{
		___Edge1_0 = value;
		Il2CppCodeGenWriteBarrier(&___Edge1_0, value);
	}

	inline static int32_t get_offset_of_Edge2_1() { return static_cast<int32_t>(offsetof(IntersectNode_t4106323947, ___Edge2_1)); }
	inline TEdge_t3950806139 * get_Edge2_1() const { return ___Edge2_1; }
	inline TEdge_t3950806139 ** get_address_of_Edge2_1() { return &___Edge2_1; }
	inline void set_Edge2_1(TEdge_t3950806139 * value)
	{
		___Edge2_1 = value;
		Il2CppCodeGenWriteBarrier(&___Edge2_1, value);
	}

	inline static int32_t get_offset_of_Pt_2() { return static_cast<int32_t>(offsetof(IntersectNode_t4106323947, ___Pt_2)); }
	inline IntPoint_t3326126179  get_Pt_2() const { return ___Pt_2; }
	inline IntPoint_t3326126179 * get_address_of_Pt_2() { return &___Pt_2; }
	inline void set_Pt_2(IntPoint_t3326126179  value)
	{
		___Pt_2 = value;
	}

	inline static int32_t get_offset_of_Next_3() { return static_cast<int32_t>(offsetof(IntersectNode_t4106323947, ___Next_3)); }
	inline IntersectNode_t4106323947 * get_Next_3() const { return ___Next_3; }
	inline IntersectNode_t4106323947 ** get_address_of_Next_3() { return &___Next_3; }
	inline void set_Next_3(IntersectNode_t4106323947 * value)
	{
		___Next_3 = value;
		Il2CppCodeGenWriteBarrier(&___Next_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
