﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.SdkwwwCallBack
struct SdkwwwCallBack_t3004559384;
// VersionMgr
struct VersionMgr_t1322950208;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VersionMgr/<UpdateDeviceInfo>c__AnonStorey15F
struct  U3CUpdateDeviceInfoU3Ec__AnonStorey15F_t1071543071  : public Il2CppObject
{
public:
	// System.String VersionMgr/<UpdateDeviceInfo>c__AnonStorey15F::arg
	String_t* ___arg_0;
	// System.Int32 VersionMgr/<UpdateDeviceInfo>c__AnonStorey15F::try_count
	int32_t ___try_count_1;
	// UnityEngine.WWWForm VersionMgr/<UpdateDeviceInfo>c__AnonStorey15F::wwwForm
	WWWForm_t461342257 * ___wwwForm_2;
	// Mihua.SDK.SdkwwwCallBack VersionMgr/<UpdateDeviceInfo>c__AnonStorey15F::callBack
	SdkwwwCallBack_t3004559384 * ___callBack_3;
	// VersionMgr VersionMgr/<UpdateDeviceInfo>c__AnonStorey15F::<>f__this
	VersionMgr_t1322950208 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_arg_0() { return static_cast<int32_t>(offsetof(U3CUpdateDeviceInfoU3Ec__AnonStorey15F_t1071543071, ___arg_0)); }
	inline String_t* get_arg_0() const { return ___arg_0; }
	inline String_t** get_address_of_arg_0() { return &___arg_0; }
	inline void set_arg_0(String_t* value)
	{
		___arg_0 = value;
		Il2CppCodeGenWriteBarrier(&___arg_0, value);
	}

	inline static int32_t get_offset_of_try_count_1() { return static_cast<int32_t>(offsetof(U3CUpdateDeviceInfoU3Ec__AnonStorey15F_t1071543071, ___try_count_1)); }
	inline int32_t get_try_count_1() const { return ___try_count_1; }
	inline int32_t* get_address_of_try_count_1() { return &___try_count_1; }
	inline void set_try_count_1(int32_t value)
	{
		___try_count_1 = value;
	}

	inline static int32_t get_offset_of_wwwForm_2() { return static_cast<int32_t>(offsetof(U3CUpdateDeviceInfoU3Ec__AnonStorey15F_t1071543071, ___wwwForm_2)); }
	inline WWWForm_t461342257 * get_wwwForm_2() const { return ___wwwForm_2; }
	inline WWWForm_t461342257 ** get_address_of_wwwForm_2() { return &___wwwForm_2; }
	inline void set_wwwForm_2(WWWForm_t461342257 * value)
	{
		___wwwForm_2 = value;
		Il2CppCodeGenWriteBarrier(&___wwwForm_2, value);
	}

	inline static int32_t get_offset_of_callBack_3() { return static_cast<int32_t>(offsetof(U3CUpdateDeviceInfoU3Ec__AnonStorey15F_t1071543071, ___callBack_3)); }
	inline SdkwwwCallBack_t3004559384 * get_callBack_3() const { return ___callBack_3; }
	inline SdkwwwCallBack_t3004559384 ** get_address_of_callBack_3() { return &___callBack_3; }
	inline void set_callBack_3(SdkwwwCallBack_t3004559384 * value)
	{
		___callBack_3 = value;
		Il2CppCodeGenWriteBarrier(&___callBack_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CUpdateDeviceInfoU3Ec__AnonStorey15F_t1071543071, ___U3CU3Ef__this_4)); }
	inline VersionMgr_t1322950208 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline VersionMgr_t1322950208 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(VersionMgr_t1322950208 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
