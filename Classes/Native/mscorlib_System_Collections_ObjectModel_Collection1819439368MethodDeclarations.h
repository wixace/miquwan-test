﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct Collection_1_t1819439368;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Mihua.Assets.SubAssetMgr/ErrorInfo[]
struct ErrorInfoU5BU5D_t2864912703;
// System.Collections.Generic.IEnumerator`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct IEnumerator_1_t250878963;
// System.Collections.Generic.IList`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct IList_1_t1033661117;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr_ErrorIn2633981210.h"

// System.Void System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor()
extern "C"  void Collection_1__ctor_m1058872353_gshared (Collection_1_t1819439368 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1058872353(__this, method) ((  void (*) (Collection_1_t1819439368 *, const MethodInfo*))Collection_1__ctor_m1058872353_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3780728438_gshared (Collection_1_t1819439368 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3780728438(__this, method) ((  bool (*) (Collection_1_t1819439368 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3780728438_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m602373187_gshared (Collection_1_t1819439368 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m602373187(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1819439368 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m602373187_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1302669330_gshared (Collection_1_t1819439368 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1302669330(__this, method) ((  Il2CppObject * (*) (Collection_1_t1819439368 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1302669330_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1145597547_gshared (Collection_1_t1819439368 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1145597547(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1819439368 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1145597547_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m623903925_gshared (Collection_1_t1819439368 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m623903925(__this, ___value0, method) ((  bool (*) (Collection_1_t1819439368 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m623903925_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m2743433027_gshared (Collection_1_t1819439368 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m2743433027(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1819439368 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2743433027_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m3834489398_gshared (Collection_1_t1819439368 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m3834489398(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1819439368 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3834489398_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3163529010_gshared (Collection_1_t1819439368 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3163529010(__this, ___value0, method) ((  void (*) (Collection_1_t1819439368 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3163529010_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m2393315975_gshared (Collection_1_t1819439368 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2393315975(__this, method) ((  bool (*) (Collection_1_t1819439368 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m2393315975_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m55666617_gshared (Collection_1_t1819439368 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m55666617(__this, method) ((  Il2CppObject * (*) (Collection_1_t1819439368 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m55666617_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m3052576740_gshared (Collection_1_t1819439368 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m3052576740(__this, method) ((  bool (*) (Collection_1_t1819439368 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m3052576740_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m3046331029_gshared (Collection_1_t1819439368 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3046331029(__this, method) ((  bool (*) (Collection_1_t1819439368 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m3046331029_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3021623040_gshared (Collection_1_t1819439368 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3021623040(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1819439368 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3021623040_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m476323981_gshared (Collection_1_t1819439368 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m476323981(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1819439368 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m476323981_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::Add(T)
extern "C"  void Collection_1_Add_m1271225406_gshared (Collection_1_t1819439368 * __this, ErrorInfo_t2633981210  ___item0, const MethodInfo* method);
#define Collection_1_Add_m1271225406(__this, ___item0, method) ((  void (*) (Collection_1_t1819439368 *, ErrorInfo_t2633981210 , const MethodInfo*))Collection_1_Add_m1271225406_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::Clear()
extern "C"  void Collection_1_Clear_m2759972940_gshared (Collection_1_t1819439368 * __this, const MethodInfo* method);
#define Collection_1_Clear_m2759972940(__this, method) ((  void (*) (Collection_1_t1819439368 *, const MethodInfo*))Collection_1_Clear_m2759972940_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1946228790_gshared (Collection_1_t1819439368 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1946228790(__this, method) ((  void (*) (Collection_1_t1819439368 *, const MethodInfo*))Collection_1_ClearItems_m1946228790_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::Contains(T)
extern "C"  bool Collection_1_Contains_m4218439486_gshared (Collection_1_t1819439368 * __this, ErrorInfo_t2633981210  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m4218439486(__this, ___item0, method) ((  bool (*) (Collection_1_t1819439368 *, ErrorInfo_t2633981210 , const MethodInfo*))Collection_1_Contains_m4218439486_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2618290350_gshared (Collection_1_t1819439368 * __this, ErrorInfoU5BU5D_t2864912703* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2618290350(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1819439368 *, ErrorInfoU5BU5D_t2864912703*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2618290350_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m1288776597_gshared (Collection_1_t1819439368 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1288776597(__this, method) ((  Il2CppObject* (*) (Collection_1_t1819439368 *, const MethodInfo*))Collection_1_GetEnumerator_m1288776597_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m788762362_gshared (Collection_1_t1819439368 * __this, ErrorInfo_t2633981210  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m788762362(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1819439368 *, ErrorInfo_t2633981210 , const MethodInfo*))Collection_1_IndexOf_m788762362_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m4105067173_gshared (Collection_1_t1819439368 * __this, int32_t ___index0, ErrorInfo_t2633981210  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m4105067173(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1819439368 *, int32_t, ErrorInfo_t2633981210 , const MethodInfo*))Collection_1_Insert_m4105067173_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1287499352_gshared (Collection_1_t1819439368 * __this, int32_t ___index0, ErrorInfo_t2633981210  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m1287499352(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1819439368 *, int32_t, ErrorInfo_t2633981210 , const MethodInfo*))Collection_1_InsertItem_m1287499352_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m3879460876_gshared (Collection_1_t1819439368 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m3879460876(__this, method) ((  Il2CppObject* (*) (Collection_1_t1819439368 *, const MethodInfo*))Collection_1_get_Items_m3879460876_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::Remove(T)
extern "C"  bool Collection_1_Remove_m2917777081_gshared (Collection_1_t1819439368 * __this, ErrorInfo_t2633981210  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2917777081(__this, ___item0, method) ((  bool (*) (Collection_1_t1819439368 *, ErrorInfo_t2633981210 , const MethodInfo*))Collection_1_Remove_m2917777081_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1978920043_gshared (Collection_1_t1819439368 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m1978920043(__this, ___index0, method) ((  void (*) (Collection_1_t1819439368 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1978920043_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1971274187_gshared (Collection_1_t1819439368 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m1971274187(__this, ___index0, method) ((  void (*) (Collection_1_t1819439368 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1971274187_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m2794104257_gshared (Collection_1_t1819439368 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m2794104257(__this, method) ((  int32_t (*) (Collection_1_t1819439368 *, const MethodInfo*))Collection_1_get_Count_m2794104257_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Item(System.Int32)
extern "C"  ErrorInfo_t2633981210  Collection_1_get_Item_m3655149201_gshared (Collection_1_t1819439368 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3655149201(__this, ___index0, method) ((  ErrorInfo_t2633981210  (*) (Collection_1_t1819439368 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3655149201_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3999172156_gshared (Collection_1_t1819439368 * __this, int32_t ___index0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3999172156(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1819439368 *, int32_t, ErrorInfo_t2633981210 , const MethodInfo*))Collection_1_set_Item_m3999172156_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m4019620541_gshared (Collection_1_t1819439368 * __this, int32_t ___index0, ErrorInfo_t2633981210  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m4019620541(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1819439368 *, int32_t, ErrorInfo_t2633981210 , const MethodInfo*))Collection_1_SetItem_m4019620541_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2142045998_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2142045998(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2142045998_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::ConvertItem(System.Object)
extern "C"  ErrorInfo_t2633981210  Collection_1_ConvertItem_m370091248_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m370091248(__this /* static, unused */, ___item0, method) ((  ErrorInfo_t2633981210  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m370091248_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m2511423854_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m2511423854(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m2511423854_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m2686234326_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m2686234326(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2686234326_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m2682124169_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m2682124169(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m2682124169_gshared)(__this /* static, unused */, ___list0, method)
