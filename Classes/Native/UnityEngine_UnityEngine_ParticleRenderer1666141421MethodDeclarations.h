﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleRenderer
struct ParticleRenderer_t1666141421;
// UnityEngine.Rect[]
struct RectU5BU5D_t1023580025;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ParticleRenderMode1751956803.h"

// System.Void UnityEngine.ParticleRenderer::.ctor()
extern "C"  void ParticleRenderer__ctor_m2330310692 (ParticleRenderer_t1666141421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleRenderMode UnityEngine.ParticleRenderer::get_particleRenderMode()
extern "C"  int32_t ParticleRenderer_get_particleRenderMode_m1100762625 (ParticleRenderer_t1666141421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleRenderer::set_particleRenderMode(UnityEngine.ParticleRenderMode)
extern "C"  void ParticleRenderer_set_particleRenderMode_m4013052102 (ParticleRenderer_t1666141421 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleRenderer::get_lengthScale()
extern "C"  float ParticleRenderer_get_lengthScale_m138390385 (ParticleRenderer_t1666141421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleRenderer::set_lengthScale(System.Single)
extern "C"  void ParticleRenderer_set_lengthScale_m3926741058 (ParticleRenderer_t1666141421 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleRenderer::get_velocityScale()
extern "C"  float ParticleRenderer_get_velocityScale_m22424218 (ParticleRenderer_t1666141421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleRenderer::set_velocityScale(System.Single)
extern "C"  void ParticleRenderer_set_velocityScale_m4079321529 (ParticleRenderer_t1666141421 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleRenderer::get_cameraVelocityScale()
extern "C"  float ParticleRenderer_get_cameraVelocityScale_m2987151285 (ParticleRenderer_t1666141421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleRenderer::set_cameraVelocityScale(System.Single)
extern "C"  void ParticleRenderer_set_cameraVelocityScale_m2028458622 (ParticleRenderer_t1666141421 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleRenderer::get_maxParticleSize()
extern "C"  float ParticleRenderer_get_maxParticleSize_m1019526360 (ParticleRenderer_t1666141421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleRenderer::set_maxParticleSize(System.Single)
extern "C"  void ParticleRenderer_set_maxParticleSize_m426019387 (ParticleRenderer_t1666141421 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticleRenderer::get_uvAnimationXTile()
extern "C"  int32_t ParticleRenderer_get_uvAnimationXTile_m3656951020 (ParticleRenderer_t1666141421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleRenderer::set_uvAnimationXTile(System.Int32)
extern "C"  void ParticleRenderer_set_uvAnimationXTile_m2785859889 (ParticleRenderer_t1666141421 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticleRenderer::get_uvAnimationYTile()
extern "C"  int32_t ParticleRenderer_get_uvAnimationYTile_m249487405 (ParticleRenderer_t1666141421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleRenderer::set_uvAnimationYTile(System.Int32)
extern "C"  void ParticleRenderer_set_uvAnimationYTile_m1941388018 (ParticleRenderer_t1666141421 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleRenderer::get_uvAnimationCycles()
extern "C"  float ParticleRenderer_get_uvAnimationCycles_m118302077 (ParticleRenderer_t1666141421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleRenderer::set_uvAnimationCycles(System.Single)
extern "C"  void ParticleRenderer_set_uvAnimationCycles_m4159183798 (ParticleRenderer_t1666141421 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleRenderer::get_maxPartileSize()
extern "C"  float ParticleRenderer_get_maxPartileSize_m3884881025 (ParticleRenderer_t1666141421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleRenderer::set_maxPartileSize(System.Single)
extern "C"  void ParticleRenderer_set_maxPartileSize_m3828511586 (ParticleRenderer_t1666141421 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect[] UnityEngine.ParticleRenderer::get_uvTiles()
extern "C"  RectU5BU5D_t1023580025* ParticleRenderer_get_uvTiles_m1375240843 (ParticleRenderer_t1666141421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleRenderer::set_uvTiles(UnityEngine.Rect[])
extern "C"  void ParticleRenderer_set_uvTiles_m679873740 (ParticleRenderer_t1666141421 * __this, RectU5BU5D_t1023580025* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
