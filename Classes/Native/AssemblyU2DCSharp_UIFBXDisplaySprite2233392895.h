﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture
struct Texture_t2526458961;

#include "AssemblyU2DCSharp_UITexture3903132647.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIFBXDisplaySprite
struct  UIFBXDisplaySprite_t2233392895  : public UITexture_t3903132647
{
public:
	// UnityEngine.Texture UIFBXDisplaySprite::mainTexture_
	Texture_t2526458961 * ___mainTexture__76;
	// UIModelDisplayType UIFBXDisplaySprite::type
	int32_t ___type_77;

public:
	inline static int32_t get_offset_of_mainTexture__76() { return static_cast<int32_t>(offsetof(UIFBXDisplaySprite_t2233392895, ___mainTexture__76)); }
	inline Texture_t2526458961 * get_mainTexture__76() const { return ___mainTexture__76; }
	inline Texture_t2526458961 ** get_address_of_mainTexture__76() { return &___mainTexture__76; }
	inline void set_mainTexture__76(Texture_t2526458961 * value)
	{
		___mainTexture__76 = value;
		Il2CppCodeGenWriteBarrier(&___mainTexture__76, value);
	}

	inline static int32_t get_offset_of_type_77() { return static_cast<int32_t>(offsetof(UIFBXDisplaySprite_t2233392895, ___type_77)); }
	inline int32_t get_type_77() const { return ___type_77; }
	inline int32_t* get_address_of_type_77() { return &___type_77; }
	inline void set_type_77(int32_t value)
	{
		___type_77 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
