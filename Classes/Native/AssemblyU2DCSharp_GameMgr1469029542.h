﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// GameMgr
struct GameMgr_t1469029542;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// SkillHurtShow
struct SkillHurtShow_t1728264445;
// BossAnimationMgr
struct BossAnimationMgr_t150948897;
// CameraAnimationMgr
struct CameraAnimationMgr_t3311695833;
// CSCheckFinishData
struct CSCheckFinishData_t2410628981;
// CameraMouseMove_PVP
struct CameraMouseMove_PVP_t2551559708;
// CSCheckPointUnit
struct CSCheckPointUnit_t3129216924;
// taixuTipCfg
struct taixuTipCfg_t1268722370;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_FunctionType4248117074.h"
#include "AssemblyU2DCSharp_PassType1280596747.h"
#include "AssemblyU2DCSharp_PanelType521505598.h"
#include "AssemblyU2DCSharp_FinishPanelType432548843.h"
#include "AssemblyU2DCSharp_ENavigateAIType3799782360.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameMgr
struct  GameMgr_t1469029542  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean GameMgr::是否一刀切
	bool ___U662FU5426U4E00U5200U5207_34;
	// System.Boolean GameMgr::是否战斗不掉血
	bool ___U662FU5426U6218U6597U4E0DU6389U8840_35;
	// UnityEngine.GameObject GameMgr::combatIndicator
	GameObject_t3674682005 * ___combatIndicator_36;
	// FunctionType GameMgr::functionType
	int32_t ___functionType_37;
	// SkillHurtShow GameMgr::UIskillHurtShow
	SkillHurtShow_t1728264445 * ___UIskillHurtShow_38;
	// System.Boolean GameMgr::isSaveHpOrMp
	bool ___isSaveHpOrMp_39;
	// PassType GameMgr::passType
	int32_t ___passType_40;
	// PanelType GameMgr::panelType
	int32_t ___panelType_41;
	// FinishPanelType GameMgr::finishPanelType
	int32_t ___finishPanelType_42;
	// System.Boolean GameMgr::IsPlayPuGongEffect
	bool ___IsPlayPuGongEffect_43;
	// System.Boolean GameMgr::missionTwo
	bool ___missionTwo_44;
	// System.Boolean GameMgr::missionThree
	bool ___missionThree_45;
	// System.Boolean GameMgr::isFinishEnd
	bool ___isFinishEnd_46;
	// BossAnimationMgr GameMgr::bossAniMgr
	BossAnimationMgr_t150948897 * ___bossAniMgr_47;
	// CameraAnimationMgr GameMgr::cameraAnimationMgr
	CameraAnimationMgr_t3311695833 * ___cameraAnimationMgr_48;
	// System.Single GameMgr::publicCDTime
	float ___publicCDTime_49;
	// System.Single GameMgr::totalPublicCDTime
	float ___totalPublicCDTime_50;
	// System.Single GameMgr::_temTime
	float ____temTime_51;
	// System.Boolean GameMgr::isQuited
	bool ___isQuited_52;
	// CSCheckFinishData GameMgr::finishData
	CSCheckFinishData_t2410628981 * ___finishData_53;
	// CameraMouseMove_PVP GameMgr::cameraMouseMove_PVP
	CameraMouseMove_PVP_t2551559708 * ___cameraMouseMove_PVP_54;
	// CSCheckPointUnit GameMgr::<checkPoint>k__BackingField
	CSCheckPointUnit_t3129216924 * ___U3CcheckPointU3Ek__BackingField_55;
	// taixuTipCfg GameMgr::<taixuTip>k__BackingField
	taixuTipCfg_t1268722370 * ___U3CtaixuTipU3Ek__BackingField_56;
	// System.Int32 GameMgr::<time>k__BackingField
	int32_t ___U3CtimeU3Ek__BackingField_57;
	// System.Int32 GameMgr::<maxTime>k__BackingField
	int32_t ___U3CmaxTimeU3Ek__BackingField_58;
	// System.Boolean GameMgr::<IsTimeLock>k__BackingField
	bool ___U3CIsTimeLockU3Ek__BackingField_59;
	// System.Boolean GameMgr::<isAuto>k__BackingField
	bool ___U3CisAutoU3Ek__BackingField_60;
	// System.Boolean GameMgr::<isAutoSkill>k__BackingField
	bool ___U3CisAutoSkillU3Ek__BackingField_61;
	// System.Boolean GameMgr::<isPublicCD>k__BackingField
	bool ___U3CisPublicCDU3Ek__BackingField_62;
	// System.Boolean GameMgr::<isFrozen>k__BackingField
	bool ___U3CisFrozenU3Ek__BackingField_63;
	// System.Boolean GameMgr::<isFrozenIdle>k__BackingField
	bool ___U3CisFrozenIdleU3Ek__BackingField_64;
	// System.Boolean GameMgr::<isBattleStart>k__BackingField
	bool ___U3CisBattleStartU3Ek__BackingField_65;
	// System.Boolean GameMgr::<isBattleEnd>k__BackingField
	bool ___U3CisBattleEndU3Ek__BackingField_66;
	// System.Boolean GameMgr::<isBattleEndBeforWin>k__BackingField
	bool ___U3CisBattleEndBeforWinU3Ek__BackingField_67;
	// System.Boolean GameMgr::<isCameraScale>k__BackingField
	bool ___U3CisCameraScaleU3Ek__BackingField_68;
	// System.Boolean GameMgr::<isStart>k__BackingField
	bool ___U3CisStartU3Ek__BackingField_70;
	// System.Int32 GameMgr::<level>k__BackingField
	int32_t ___U3ClevelU3Ek__BackingField_71;
	// ENavigateAIType GameMgr::<navAIType>k__BackingField
	int32_t ___U3CnavAITypeU3Ek__BackingField_72;
	// System.Boolean GameMgr::<IsOnTap>k__BackingField
	bool ___U3CIsOnTapU3Ek__BackingField_73;
	// System.Boolean GameMgr::<IsPlayPloting>k__BackingField
	bool ___U3CIsPlayPlotingU3Ek__BackingField_74;

public:
	inline static int32_t get_offset_of_U662FU5426U4E00U5200U5207_34() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___U662FU5426U4E00U5200U5207_34)); }
	inline bool get_U662FU5426U4E00U5200U5207_34() const { return ___U662FU5426U4E00U5200U5207_34; }
	inline bool* get_address_of_U662FU5426U4E00U5200U5207_34() { return &___U662FU5426U4E00U5200U5207_34; }
	inline void set_U662FU5426U4E00U5200U5207_34(bool value)
	{
		___U662FU5426U4E00U5200U5207_34 = value;
	}

	inline static int32_t get_offset_of_U662FU5426U6218U6597U4E0DU6389U8840_35() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___U662FU5426U6218U6597U4E0DU6389U8840_35)); }
	inline bool get_U662FU5426U6218U6597U4E0DU6389U8840_35() const { return ___U662FU5426U6218U6597U4E0DU6389U8840_35; }
	inline bool* get_address_of_U662FU5426U6218U6597U4E0DU6389U8840_35() { return &___U662FU5426U6218U6597U4E0DU6389U8840_35; }
	inline void set_U662FU5426U6218U6597U4E0DU6389U8840_35(bool value)
	{
		___U662FU5426U6218U6597U4E0DU6389U8840_35 = value;
	}

	inline static int32_t get_offset_of_combatIndicator_36() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___combatIndicator_36)); }
	inline GameObject_t3674682005 * get_combatIndicator_36() const { return ___combatIndicator_36; }
	inline GameObject_t3674682005 ** get_address_of_combatIndicator_36() { return &___combatIndicator_36; }
	inline void set_combatIndicator_36(GameObject_t3674682005 * value)
	{
		___combatIndicator_36 = value;
		Il2CppCodeGenWriteBarrier(&___combatIndicator_36, value);
	}

	inline static int32_t get_offset_of_functionType_37() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___functionType_37)); }
	inline int32_t get_functionType_37() const { return ___functionType_37; }
	inline int32_t* get_address_of_functionType_37() { return &___functionType_37; }
	inline void set_functionType_37(int32_t value)
	{
		___functionType_37 = value;
	}

	inline static int32_t get_offset_of_UIskillHurtShow_38() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___UIskillHurtShow_38)); }
	inline SkillHurtShow_t1728264445 * get_UIskillHurtShow_38() const { return ___UIskillHurtShow_38; }
	inline SkillHurtShow_t1728264445 ** get_address_of_UIskillHurtShow_38() { return &___UIskillHurtShow_38; }
	inline void set_UIskillHurtShow_38(SkillHurtShow_t1728264445 * value)
	{
		___UIskillHurtShow_38 = value;
		Il2CppCodeGenWriteBarrier(&___UIskillHurtShow_38, value);
	}

	inline static int32_t get_offset_of_isSaveHpOrMp_39() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___isSaveHpOrMp_39)); }
	inline bool get_isSaveHpOrMp_39() const { return ___isSaveHpOrMp_39; }
	inline bool* get_address_of_isSaveHpOrMp_39() { return &___isSaveHpOrMp_39; }
	inline void set_isSaveHpOrMp_39(bool value)
	{
		___isSaveHpOrMp_39 = value;
	}

	inline static int32_t get_offset_of_passType_40() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___passType_40)); }
	inline int32_t get_passType_40() const { return ___passType_40; }
	inline int32_t* get_address_of_passType_40() { return &___passType_40; }
	inline void set_passType_40(int32_t value)
	{
		___passType_40 = value;
	}

	inline static int32_t get_offset_of_panelType_41() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___panelType_41)); }
	inline int32_t get_panelType_41() const { return ___panelType_41; }
	inline int32_t* get_address_of_panelType_41() { return &___panelType_41; }
	inline void set_panelType_41(int32_t value)
	{
		___panelType_41 = value;
	}

	inline static int32_t get_offset_of_finishPanelType_42() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___finishPanelType_42)); }
	inline int32_t get_finishPanelType_42() const { return ___finishPanelType_42; }
	inline int32_t* get_address_of_finishPanelType_42() { return &___finishPanelType_42; }
	inline void set_finishPanelType_42(int32_t value)
	{
		___finishPanelType_42 = value;
	}

	inline static int32_t get_offset_of_IsPlayPuGongEffect_43() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___IsPlayPuGongEffect_43)); }
	inline bool get_IsPlayPuGongEffect_43() const { return ___IsPlayPuGongEffect_43; }
	inline bool* get_address_of_IsPlayPuGongEffect_43() { return &___IsPlayPuGongEffect_43; }
	inline void set_IsPlayPuGongEffect_43(bool value)
	{
		___IsPlayPuGongEffect_43 = value;
	}

	inline static int32_t get_offset_of_missionTwo_44() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___missionTwo_44)); }
	inline bool get_missionTwo_44() const { return ___missionTwo_44; }
	inline bool* get_address_of_missionTwo_44() { return &___missionTwo_44; }
	inline void set_missionTwo_44(bool value)
	{
		___missionTwo_44 = value;
	}

	inline static int32_t get_offset_of_missionThree_45() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___missionThree_45)); }
	inline bool get_missionThree_45() const { return ___missionThree_45; }
	inline bool* get_address_of_missionThree_45() { return &___missionThree_45; }
	inline void set_missionThree_45(bool value)
	{
		___missionThree_45 = value;
	}

	inline static int32_t get_offset_of_isFinishEnd_46() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___isFinishEnd_46)); }
	inline bool get_isFinishEnd_46() const { return ___isFinishEnd_46; }
	inline bool* get_address_of_isFinishEnd_46() { return &___isFinishEnd_46; }
	inline void set_isFinishEnd_46(bool value)
	{
		___isFinishEnd_46 = value;
	}

	inline static int32_t get_offset_of_bossAniMgr_47() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___bossAniMgr_47)); }
	inline BossAnimationMgr_t150948897 * get_bossAniMgr_47() const { return ___bossAniMgr_47; }
	inline BossAnimationMgr_t150948897 ** get_address_of_bossAniMgr_47() { return &___bossAniMgr_47; }
	inline void set_bossAniMgr_47(BossAnimationMgr_t150948897 * value)
	{
		___bossAniMgr_47 = value;
		Il2CppCodeGenWriteBarrier(&___bossAniMgr_47, value);
	}

	inline static int32_t get_offset_of_cameraAnimationMgr_48() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___cameraAnimationMgr_48)); }
	inline CameraAnimationMgr_t3311695833 * get_cameraAnimationMgr_48() const { return ___cameraAnimationMgr_48; }
	inline CameraAnimationMgr_t3311695833 ** get_address_of_cameraAnimationMgr_48() { return &___cameraAnimationMgr_48; }
	inline void set_cameraAnimationMgr_48(CameraAnimationMgr_t3311695833 * value)
	{
		___cameraAnimationMgr_48 = value;
		Il2CppCodeGenWriteBarrier(&___cameraAnimationMgr_48, value);
	}

	inline static int32_t get_offset_of_publicCDTime_49() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___publicCDTime_49)); }
	inline float get_publicCDTime_49() const { return ___publicCDTime_49; }
	inline float* get_address_of_publicCDTime_49() { return &___publicCDTime_49; }
	inline void set_publicCDTime_49(float value)
	{
		___publicCDTime_49 = value;
	}

	inline static int32_t get_offset_of_totalPublicCDTime_50() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___totalPublicCDTime_50)); }
	inline float get_totalPublicCDTime_50() const { return ___totalPublicCDTime_50; }
	inline float* get_address_of_totalPublicCDTime_50() { return &___totalPublicCDTime_50; }
	inline void set_totalPublicCDTime_50(float value)
	{
		___totalPublicCDTime_50 = value;
	}

	inline static int32_t get_offset_of__temTime_51() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ____temTime_51)); }
	inline float get__temTime_51() const { return ____temTime_51; }
	inline float* get_address_of__temTime_51() { return &____temTime_51; }
	inline void set__temTime_51(float value)
	{
		____temTime_51 = value;
	}

	inline static int32_t get_offset_of_isQuited_52() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___isQuited_52)); }
	inline bool get_isQuited_52() const { return ___isQuited_52; }
	inline bool* get_address_of_isQuited_52() { return &___isQuited_52; }
	inline void set_isQuited_52(bool value)
	{
		___isQuited_52 = value;
	}

	inline static int32_t get_offset_of_finishData_53() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___finishData_53)); }
	inline CSCheckFinishData_t2410628981 * get_finishData_53() const { return ___finishData_53; }
	inline CSCheckFinishData_t2410628981 ** get_address_of_finishData_53() { return &___finishData_53; }
	inline void set_finishData_53(CSCheckFinishData_t2410628981 * value)
	{
		___finishData_53 = value;
		Il2CppCodeGenWriteBarrier(&___finishData_53, value);
	}

	inline static int32_t get_offset_of_cameraMouseMove_PVP_54() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___cameraMouseMove_PVP_54)); }
	inline CameraMouseMove_PVP_t2551559708 * get_cameraMouseMove_PVP_54() const { return ___cameraMouseMove_PVP_54; }
	inline CameraMouseMove_PVP_t2551559708 ** get_address_of_cameraMouseMove_PVP_54() { return &___cameraMouseMove_PVP_54; }
	inline void set_cameraMouseMove_PVP_54(CameraMouseMove_PVP_t2551559708 * value)
	{
		___cameraMouseMove_PVP_54 = value;
		Il2CppCodeGenWriteBarrier(&___cameraMouseMove_PVP_54, value);
	}

	inline static int32_t get_offset_of_U3CcheckPointU3Ek__BackingField_55() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___U3CcheckPointU3Ek__BackingField_55)); }
	inline CSCheckPointUnit_t3129216924 * get_U3CcheckPointU3Ek__BackingField_55() const { return ___U3CcheckPointU3Ek__BackingField_55; }
	inline CSCheckPointUnit_t3129216924 ** get_address_of_U3CcheckPointU3Ek__BackingField_55() { return &___U3CcheckPointU3Ek__BackingField_55; }
	inline void set_U3CcheckPointU3Ek__BackingField_55(CSCheckPointUnit_t3129216924 * value)
	{
		___U3CcheckPointU3Ek__BackingField_55 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcheckPointU3Ek__BackingField_55, value);
	}

	inline static int32_t get_offset_of_U3CtaixuTipU3Ek__BackingField_56() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___U3CtaixuTipU3Ek__BackingField_56)); }
	inline taixuTipCfg_t1268722370 * get_U3CtaixuTipU3Ek__BackingField_56() const { return ___U3CtaixuTipU3Ek__BackingField_56; }
	inline taixuTipCfg_t1268722370 ** get_address_of_U3CtaixuTipU3Ek__BackingField_56() { return &___U3CtaixuTipU3Ek__BackingField_56; }
	inline void set_U3CtaixuTipU3Ek__BackingField_56(taixuTipCfg_t1268722370 * value)
	{
		___U3CtaixuTipU3Ek__BackingField_56 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtaixuTipU3Ek__BackingField_56, value);
	}

	inline static int32_t get_offset_of_U3CtimeU3Ek__BackingField_57() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___U3CtimeU3Ek__BackingField_57)); }
	inline int32_t get_U3CtimeU3Ek__BackingField_57() const { return ___U3CtimeU3Ek__BackingField_57; }
	inline int32_t* get_address_of_U3CtimeU3Ek__BackingField_57() { return &___U3CtimeU3Ek__BackingField_57; }
	inline void set_U3CtimeU3Ek__BackingField_57(int32_t value)
	{
		___U3CtimeU3Ek__BackingField_57 = value;
	}

	inline static int32_t get_offset_of_U3CmaxTimeU3Ek__BackingField_58() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___U3CmaxTimeU3Ek__BackingField_58)); }
	inline int32_t get_U3CmaxTimeU3Ek__BackingField_58() const { return ___U3CmaxTimeU3Ek__BackingField_58; }
	inline int32_t* get_address_of_U3CmaxTimeU3Ek__BackingField_58() { return &___U3CmaxTimeU3Ek__BackingField_58; }
	inline void set_U3CmaxTimeU3Ek__BackingField_58(int32_t value)
	{
		___U3CmaxTimeU3Ek__BackingField_58 = value;
	}

	inline static int32_t get_offset_of_U3CIsTimeLockU3Ek__BackingField_59() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___U3CIsTimeLockU3Ek__BackingField_59)); }
	inline bool get_U3CIsTimeLockU3Ek__BackingField_59() const { return ___U3CIsTimeLockU3Ek__BackingField_59; }
	inline bool* get_address_of_U3CIsTimeLockU3Ek__BackingField_59() { return &___U3CIsTimeLockU3Ek__BackingField_59; }
	inline void set_U3CIsTimeLockU3Ek__BackingField_59(bool value)
	{
		___U3CIsTimeLockU3Ek__BackingField_59 = value;
	}

	inline static int32_t get_offset_of_U3CisAutoU3Ek__BackingField_60() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___U3CisAutoU3Ek__BackingField_60)); }
	inline bool get_U3CisAutoU3Ek__BackingField_60() const { return ___U3CisAutoU3Ek__BackingField_60; }
	inline bool* get_address_of_U3CisAutoU3Ek__BackingField_60() { return &___U3CisAutoU3Ek__BackingField_60; }
	inline void set_U3CisAutoU3Ek__BackingField_60(bool value)
	{
		___U3CisAutoU3Ek__BackingField_60 = value;
	}

	inline static int32_t get_offset_of_U3CisAutoSkillU3Ek__BackingField_61() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___U3CisAutoSkillU3Ek__BackingField_61)); }
	inline bool get_U3CisAutoSkillU3Ek__BackingField_61() const { return ___U3CisAutoSkillU3Ek__BackingField_61; }
	inline bool* get_address_of_U3CisAutoSkillU3Ek__BackingField_61() { return &___U3CisAutoSkillU3Ek__BackingField_61; }
	inline void set_U3CisAutoSkillU3Ek__BackingField_61(bool value)
	{
		___U3CisAutoSkillU3Ek__BackingField_61 = value;
	}

	inline static int32_t get_offset_of_U3CisPublicCDU3Ek__BackingField_62() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___U3CisPublicCDU3Ek__BackingField_62)); }
	inline bool get_U3CisPublicCDU3Ek__BackingField_62() const { return ___U3CisPublicCDU3Ek__BackingField_62; }
	inline bool* get_address_of_U3CisPublicCDU3Ek__BackingField_62() { return &___U3CisPublicCDU3Ek__BackingField_62; }
	inline void set_U3CisPublicCDU3Ek__BackingField_62(bool value)
	{
		___U3CisPublicCDU3Ek__BackingField_62 = value;
	}

	inline static int32_t get_offset_of_U3CisFrozenU3Ek__BackingField_63() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___U3CisFrozenU3Ek__BackingField_63)); }
	inline bool get_U3CisFrozenU3Ek__BackingField_63() const { return ___U3CisFrozenU3Ek__BackingField_63; }
	inline bool* get_address_of_U3CisFrozenU3Ek__BackingField_63() { return &___U3CisFrozenU3Ek__BackingField_63; }
	inline void set_U3CisFrozenU3Ek__BackingField_63(bool value)
	{
		___U3CisFrozenU3Ek__BackingField_63 = value;
	}

	inline static int32_t get_offset_of_U3CisFrozenIdleU3Ek__BackingField_64() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___U3CisFrozenIdleU3Ek__BackingField_64)); }
	inline bool get_U3CisFrozenIdleU3Ek__BackingField_64() const { return ___U3CisFrozenIdleU3Ek__BackingField_64; }
	inline bool* get_address_of_U3CisFrozenIdleU3Ek__BackingField_64() { return &___U3CisFrozenIdleU3Ek__BackingField_64; }
	inline void set_U3CisFrozenIdleU3Ek__BackingField_64(bool value)
	{
		___U3CisFrozenIdleU3Ek__BackingField_64 = value;
	}

	inline static int32_t get_offset_of_U3CisBattleStartU3Ek__BackingField_65() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___U3CisBattleStartU3Ek__BackingField_65)); }
	inline bool get_U3CisBattleStartU3Ek__BackingField_65() const { return ___U3CisBattleStartU3Ek__BackingField_65; }
	inline bool* get_address_of_U3CisBattleStartU3Ek__BackingField_65() { return &___U3CisBattleStartU3Ek__BackingField_65; }
	inline void set_U3CisBattleStartU3Ek__BackingField_65(bool value)
	{
		___U3CisBattleStartU3Ek__BackingField_65 = value;
	}

	inline static int32_t get_offset_of_U3CisBattleEndU3Ek__BackingField_66() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___U3CisBattleEndU3Ek__BackingField_66)); }
	inline bool get_U3CisBattleEndU3Ek__BackingField_66() const { return ___U3CisBattleEndU3Ek__BackingField_66; }
	inline bool* get_address_of_U3CisBattleEndU3Ek__BackingField_66() { return &___U3CisBattleEndU3Ek__BackingField_66; }
	inline void set_U3CisBattleEndU3Ek__BackingField_66(bool value)
	{
		___U3CisBattleEndU3Ek__BackingField_66 = value;
	}

	inline static int32_t get_offset_of_U3CisBattleEndBeforWinU3Ek__BackingField_67() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___U3CisBattleEndBeforWinU3Ek__BackingField_67)); }
	inline bool get_U3CisBattleEndBeforWinU3Ek__BackingField_67() const { return ___U3CisBattleEndBeforWinU3Ek__BackingField_67; }
	inline bool* get_address_of_U3CisBattleEndBeforWinU3Ek__BackingField_67() { return &___U3CisBattleEndBeforWinU3Ek__BackingField_67; }
	inline void set_U3CisBattleEndBeforWinU3Ek__BackingField_67(bool value)
	{
		___U3CisBattleEndBeforWinU3Ek__BackingField_67 = value;
	}

	inline static int32_t get_offset_of_U3CisCameraScaleU3Ek__BackingField_68() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___U3CisCameraScaleU3Ek__BackingField_68)); }
	inline bool get_U3CisCameraScaleU3Ek__BackingField_68() const { return ___U3CisCameraScaleU3Ek__BackingField_68; }
	inline bool* get_address_of_U3CisCameraScaleU3Ek__BackingField_68() { return &___U3CisCameraScaleU3Ek__BackingField_68; }
	inline void set_U3CisCameraScaleU3Ek__BackingField_68(bool value)
	{
		___U3CisCameraScaleU3Ek__BackingField_68 = value;
	}

	inline static int32_t get_offset_of_U3CisStartU3Ek__BackingField_70() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___U3CisStartU3Ek__BackingField_70)); }
	inline bool get_U3CisStartU3Ek__BackingField_70() const { return ___U3CisStartU3Ek__BackingField_70; }
	inline bool* get_address_of_U3CisStartU3Ek__BackingField_70() { return &___U3CisStartU3Ek__BackingField_70; }
	inline void set_U3CisStartU3Ek__BackingField_70(bool value)
	{
		___U3CisStartU3Ek__BackingField_70 = value;
	}

	inline static int32_t get_offset_of_U3ClevelU3Ek__BackingField_71() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___U3ClevelU3Ek__BackingField_71)); }
	inline int32_t get_U3ClevelU3Ek__BackingField_71() const { return ___U3ClevelU3Ek__BackingField_71; }
	inline int32_t* get_address_of_U3ClevelU3Ek__BackingField_71() { return &___U3ClevelU3Ek__BackingField_71; }
	inline void set_U3ClevelU3Ek__BackingField_71(int32_t value)
	{
		___U3ClevelU3Ek__BackingField_71 = value;
	}

	inline static int32_t get_offset_of_U3CnavAITypeU3Ek__BackingField_72() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___U3CnavAITypeU3Ek__BackingField_72)); }
	inline int32_t get_U3CnavAITypeU3Ek__BackingField_72() const { return ___U3CnavAITypeU3Ek__BackingField_72; }
	inline int32_t* get_address_of_U3CnavAITypeU3Ek__BackingField_72() { return &___U3CnavAITypeU3Ek__BackingField_72; }
	inline void set_U3CnavAITypeU3Ek__BackingField_72(int32_t value)
	{
		___U3CnavAITypeU3Ek__BackingField_72 = value;
	}

	inline static int32_t get_offset_of_U3CIsOnTapU3Ek__BackingField_73() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___U3CIsOnTapU3Ek__BackingField_73)); }
	inline bool get_U3CIsOnTapU3Ek__BackingField_73() const { return ___U3CIsOnTapU3Ek__BackingField_73; }
	inline bool* get_address_of_U3CIsOnTapU3Ek__BackingField_73() { return &___U3CIsOnTapU3Ek__BackingField_73; }
	inline void set_U3CIsOnTapU3Ek__BackingField_73(bool value)
	{
		___U3CIsOnTapU3Ek__BackingField_73 = value;
	}

	inline static int32_t get_offset_of_U3CIsPlayPlotingU3Ek__BackingField_74() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542, ___U3CIsPlayPlotingU3Ek__BackingField_74)); }
	inline bool get_U3CIsPlayPlotingU3Ek__BackingField_74() const { return ___U3CIsPlayPlotingU3Ek__BackingField_74; }
	inline bool* get_address_of_U3CIsPlayPlotingU3Ek__BackingField_74() { return &___U3CIsPlayPlotingU3Ek__BackingField_74; }
	inline void set_U3CIsPlayPlotingU3Ek__BackingField_74(bool value)
	{
		___U3CIsPlayPlotingU3Ek__BackingField_74 = value;
	}
};

struct GameMgr_t1469029542_StaticFields
{
public:
	// GameMgr GameMgr::instance
	GameMgr_t1469029542 * ___instance_33;
	// System.Boolean GameMgr::<isWin>k__BackingField
	bool ___U3CisWinU3Ek__BackingField_69;
	// System.Action GameMgr::<>f__am$cache2A
	Action_t3771233898 * ___U3CU3Ef__amU24cache2A_75;

public:
	inline static int32_t get_offset_of_instance_33() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542_StaticFields, ___instance_33)); }
	inline GameMgr_t1469029542 * get_instance_33() const { return ___instance_33; }
	inline GameMgr_t1469029542 ** get_address_of_instance_33() { return &___instance_33; }
	inline void set_instance_33(GameMgr_t1469029542 * value)
	{
		___instance_33 = value;
		Il2CppCodeGenWriteBarrier(&___instance_33, value);
	}

	inline static int32_t get_offset_of_U3CisWinU3Ek__BackingField_69() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542_StaticFields, ___U3CisWinU3Ek__BackingField_69)); }
	inline bool get_U3CisWinU3Ek__BackingField_69() const { return ___U3CisWinU3Ek__BackingField_69; }
	inline bool* get_address_of_U3CisWinU3Ek__BackingField_69() { return &___U3CisWinU3Ek__BackingField_69; }
	inline void set_U3CisWinU3Ek__BackingField_69(bool value)
	{
		___U3CisWinU3Ek__BackingField_69 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2A_75() { return static_cast<int32_t>(offsetof(GameMgr_t1469029542_StaticFields, ___U3CU3Ef__amU24cache2A_75)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache2A_75() const { return ___U3CU3Ef__amU24cache2A_75; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache2A_75() { return &___U3CU3Ef__amU24cache2A_75; }
	inline void set_U3CU3Ef__amU24cache2A_75(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache2A_75 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2A_75, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
