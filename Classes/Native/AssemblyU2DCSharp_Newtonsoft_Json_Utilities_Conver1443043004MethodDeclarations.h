﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ConvertUtils/<TryConvertOrCast>c__AnonStorey13E
struct U3CTryConvertOrCastU3Ec__AnonStorey13E_t1443043004;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Utilities.ConvertUtils/<TryConvertOrCast>c__AnonStorey13E::.ctor()
extern "C"  void U3CTryConvertOrCastU3Ec__AnonStorey13E__ctor_m2604962351 (U3CTryConvertOrCastU3Ec__AnonStorey13E_t1443043004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ConvertUtils/<TryConvertOrCast>c__AnonStorey13E::<>m__39A()
extern "C"  Il2CppObject * U3CTryConvertOrCastU3Ec__AnonStorey13E_U3CU3Em__39A_m4193103344 (U3CTryConvertOrCastU3Ec__AnonStorey13E_t1443043004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
