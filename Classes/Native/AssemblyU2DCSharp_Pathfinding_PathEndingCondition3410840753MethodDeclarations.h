﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.PathEndingCondition
struct PathEndingCondition_t3410840753;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.PathNode
struct PathNode_t417131581;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"

// System.Void Pathfinding.PathEndingCondition::.ctor()
extern "C"  void PathEndingCondition__ctor_m183294374 (PathEndingCondition_t3410840753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PathEndingCondition::.ctor(Pathfinding.Path)
extern "C"  void PathEndingCondition__ctor_m3749440823 (PathEndingCondition_t3410840753 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.PathEndingCondition::TargetFound(Pathfinding.PathNode)
extern "C"  bool PathEndingCondition_TargetFound_m4284556184 (PathEndingCondition_t3410840753 * __this, PathNode_t417131581 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
