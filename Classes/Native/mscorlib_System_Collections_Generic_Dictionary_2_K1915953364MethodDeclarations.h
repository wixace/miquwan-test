﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>
struct Dictionary_2_t1301017310;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1915953364.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,HatredCtrl/stValue>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4155014541_gshared (Enumerator_t1915953364 * __this, Dictionary_2_t1301017310 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m4155014541(__this, ___host0, method) ((  void (*) (Enumerator_t1915953364 *, Dictionary_2_t1301017310 *, const MethodInfo*))Enumerator__ctor_m4155014541_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,HatredCtrl/stValue>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2627249022_gshared (Enumerator_t1915953364 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2627249022(__this, method) ((  Il2CppObject * (*) (Enumerator_t1915953364 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2627249022_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,HatredCtrl/stValue>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2103383304_gshared (Enumerator_t1915953364 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2103383304(__this, method) ((  void (*) (Enumerator_t1915953364 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2103383304_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,HatredCtrl/stValue>::Dispose()
extern "C"  void Enumerator_Dispose_m530575727_gshared (Enumerator_t1915953364 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m530575727(__this, method) ((  void (*) (Enumerator_t1915953364 *, const MethodInfo*))Enumerator_Dispose_m530575727_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,HatredCtrl/stValue>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1777250744_gshared (Enumerator_t1915953364 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1777250744(__this, method) ((  bool (*) (Enumerator_t1915953364 *, const MethodInfo*))Enumerator_MoveNext_m1777250744_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,HatredCtrl/stValue>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m4197202144_gshared (Enumerator_t1915953364 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4197202144(__this, method) ((  Il2CppObject * (*) (Enumerator_t1915953364 *, const MethodInfo*))Enumerator_get_Current_m4197202144_gshared)(__this, method)
