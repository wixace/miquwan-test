﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.ParticleSystem
struct ParticleSystem_t381473177;
// GesturesMgr
struct GesturesMgr_t1320398158;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GesturesMgr
struct  GesturesMgr_t1320398158  : public MonoBehaviour_t667441552
{
public:
	// System.Single GesturesMgr::zoomSpeed
	float ___zoomSpeed_2;
	// System.Single GesturesMgr::minZoomAmount
	float ___minZoomAmount_3;
	// System.Single GesturesMgr::maxZoomAmount
	float ___maxZoomAmount_4;
	// System.Single GesturesMgr::SmoothSpeed
	float ___SmoothSpeed_5;
	// System.Boolean GesturesMgr::isAllowPinch
	bool ___isAllowPinch_6;
	// System.Single GesturesMgr::defaultFov
	float ___defaultFov_7;
	// System.Single GesturesMgr::idealZoomAmount
	float ___idealZoomAmount_8;
	// System.Single GesturesMgr::zoomAmount
	float ___zoomAmount_9;
	// System.Boolean GesturesMgr::isDoubleClick
	bool ___isDoubleClick_10;
	// System.Boolean GesturesMgr::isPinch
	bool ___isPinch_11;
	// UnityEngine.ParticleSystem GesturesMgr::clickEff
	ParticleSystem_t381473177 * ___clickEff_12;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> GesturesMgr::captainPositions
	List_1_t1355284822 * ___captainPositions_14;

public:
	inline static int32_t get_offset_of_zoomSpeed_2() { return static_cast<int32_t>(offsetof(GesturesMgr_t1320398158, ___zoomSpeed_2)); }
	inline float get_zoomSpeed_2() const { return ___zoomSpeed_2; }
	inline float* get_address_of_zoomSpeed_2() { return &___zoomSpeed_2; }
	inline void set_zoomSpeed_2(float value)
	{
		___zoomSpeed_2 = value;
	}

	inline static int32_t get_offset_of_minZoomAmount_3() { return static_cast<int32_t>(offsetof(GesturesMgr_t1320398158, ___minZoomAmount_3)); }
	inline float get_minZoomAmount_3() const { return ___minZoomAmount_3; }
	inline float* get_address_of_minZoomAmount_3() { return &___minZoomAmount_3; }
	inline void set_minZoomAmount_3(float value)
	{
		___minZoomAmount_3 = value;
	}

	inline static int32_t get_offset_of_maxZoomAmount_4() { return static_cast<int32_t>(offsetof(GesturesMgr_t1320398158, ___maxZoomAmount_4)); }
	inline float get_maxZoomAmount_4() const { return ___maxZoomAmount_4; }
	inline float* get_address_of_maxZoomAmount_4() { return &___maxZoomAmount_4; }
	inline void set_maxZoomAmount_4(float value)
	{
		___maxZoomAmount_4 = value;
	}

	inline static int32_t get_offset_of_SmoothSpeed_5() { return static_cast<int32_t>(offsetof(GesturesMgr_t1320398158, ___SmoothSpeed_5)); }
	inline float get_SmoothSpeed_5() const { return ___SmoothSpeed_5; }
	inline float* get_address_of_SmoothSpeed_5() { return &___SmoothSpeed_5; }
	inline void set_SmoothSpeed_5(float value)
	{
		___SmoothSpeed_5 = value;
	}

	inline static int32_t get_offset_of_isAllowPinch_6() { return static_cast<int32_t>(offsetof(GesturesMgr_t1320398158, ___isAllowPinch_6)); }
	inline bool get_isAllowPinch_6() const { return ___isAllowPinch_6; }
	inline bool* get_address_of_isAllowPinch_6() { return &___isAllowPinch_6; }
	inline void set_isAllowPinch_6(bool value)
	{
		___isAllowPinch_6 = value;
	}

	inline static int32_t get_offset_of_defaultFov_7() { return static_cast<int32_t>(offsetof(GesturesMgr_t1320398158, ___defaultFov_7)); }
	inline float get_defaultFov_7() const { return ___defaultFov_7; }
	inline float* get_address_of_defaultFov_7() { return &___defaultFov_7; }
	inline void set_defaultFov_7(float value)
	{
		___defaultFov_7 = value;
	}

	inline static int32_t get_offset_of_idealZoomAmount_8() { return static_cast<int32_t>(offsetof(GesturesMgr_t1320398158, ___idealZoomAmount_8)); }
	inline float get_idealZoomAmount_8() const { return ___idealZoomAmount_8; }
	inline float* get_address_of_idealZoomAmount_8() { return &___idealZoomAmount_8; }
	inline void set_idealZoomAmount_8(float value)
	{
		___idealZoomAmount_8 = value;
	}

	inline static int32_t get_offset_of_zoomAmount_9() { return static_cast<int32_t>(offsetof(GesturesMgr_t1320398158, ___zoomAmount_9)); }
	inline float get_zoomAmount_9() const { return ___zoomAmount_9; }
	inline float* get_address_of_zoomAmount_9() { return &___zoomAmount_9; }
	inline void set_zoomAmount_9(float value)
	{
		___zoomAmount_9 = value;
	}

	inline static int32_t get_offset_of_isDoubleClick_10() { return static_cast<int32_t>(offsetof(GesturesMgr_t1320398158, ___isDoubleClick_10)); }
	inline bool get_isDoubleClick_10() const { return ___isDoubleClick_10; }
	inline bool* get_address_of_isDoubleClick_10() { return &___isDoubleClick_10; }
	inline void set_isDoubleClick_10(bool value)
	{
		___isDoubleClick_10 = value;
	}

	inline static int32_t get_offset_of_isPinch_11() { return static_cast<int32_t>(offsetof(GesturesMgr_t1320398158, ___isPinch_11)); }
	inline bool get_isPinch_11() const { return ___isPinch_11; }
	inline bool* get_address_of_isPinch_11() { return &___isPinch_11; }
	inline void set_isPinch_11(bool value)
	{
		___isPinch_11 = value;
	}

	inline static int32_t get_offset_of_clickEff_12() { return static_cast<int32_t>(offsetof(GesturesMgr_t1320398158, ___clickEff_12)); }
	inline ParticleSystem_t381473177 * get_clickEff_12() const { return ___clickEff_12; }
	inline ParticleSystem_t381473177 ** get_address_of_clickEff_12() { return &___clickEff_12; }
	inline void set_clickEff_12(ParticleSystem_t381473177 * value)
	{
		___clickEff_12 = value;
		Il2CppCodeGenWriteBarrier(&___clickEff_12, value);
	}

	inline static int32_t get_offset_of_captainPositions_14() { return static_cast<int32_t>(offsetof(GesturesMgr_t1320398158, ___captainPositions_14)); }
	inline List_1_t1355284822 * get_captainPositions_14() const { return ___captainPositions_14; }
	inline List_1_t1355284822 ** get_address_of_captainPositions_14() { return &___captainPositions_14; }
	inline void set_captainPositions_14(List_1_t1355284822 * value)
	{
		___captainPositions_14 = value;
		Il2CppCodeGenWriteBarrier(&___captainPositions_14, value);
	}
};

struct GesturesMgr_t1320398158_StaticFields
{
public:
	// GesturesMgr GesturesMgr::instance
	GesturesMgr_t1320398158 * ___instance_13;

public:
	inline static int32_t get_offset_of_instance_13() { return static_cast<int32_t>(offsetof(GesturesMgr_t1320398158_StaticFields, ___instance_13)); }
	inline GesturesMgr_t1320398158 * get_instance_13() const { return ___instance_13; }
	inline GesturesMgr_t1320398158 ** get_address_of_instance_13() { return &___instance_13; }
	inline void set_instance_13(GesturesMgr_t1320398158 * value)
	{
		___instance_13 = value;
		Il2CppCodeGenWriteBarrier(&___instance_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
