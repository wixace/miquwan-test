﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebUtil/<GetJson>c__Iterator2`1<System.Object>
struct U3CGetJsonU3Ec__Iterator2_1_t2839321413;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void WebUtil/<GetJson>c__Iterator2`1<System.Object>::.ctor()
extern "C"  void U3CGetJsonU3Ec__Iterator2_1__ctor_m2914445012_gshared (U3CGetJsonU3Ec__Iterator2_1_t2839321413 * __this, const MethodInfo* method);
#define U3CGetJsonU3Ec__Iterator2_1__ctor_m2914445012(__this, method) ((  void (*) (U3CGetJsonU3Ec__Iterator2_1_t2839321413 *, const MethodInfo*))U3CGetJsonU3Ec__Iterator2_1__ctor_m2914445012_gshared)(__this, method)
// System.Object WebUtil/<GetJson>c__Iterator2`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetJsonU3Ec__Iterator2_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1425325768_gshared (U3CGetJsonU3Ec__Iterator2_1_t2839321413 * __this, const MethodInfo* method);
#define U3CGetJsonU3Ec__Iterator2_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1425325768(__this, method) ((  Il2CppObject * (*) (U3CGetJsonU3Ec__Iterator2_1_t2839321413 *, const MethodInfo*))U3CGetJsonU3Ec__Iterator2_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1425325768_gshared)(__this, method)
// System.Object WebUtil/<GetJson>c__Iterator2`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetJsonU3Ec__Iterator2_1_System_Collections_IEnumerator_get_Current_m2177716828_gshared (U3CGetJsonU3Ec__Iterator2_1_t2839321413 * __this, const MethodInfo* method);
#define U3CGetJsonU3Ec__Iterator2_1_System_Collections_IEnumerator_get_Current_m2177716828(__this, method) ((  Il2CppObject * (*) (U3CGetJsonU3Ec__Iterator2_1_t2839321413 *, const MethodInfo*))U3CGetJsonU3Ec__Iterator2_1_System_Collections_IEnumerator_get_Current_m2177716828_gshared)(__this, method)
// System.Boolean WebUtil/<GetJson>c__Iterator2`1<System.Object>::MoveNext()
extern "C"  bool U3CGetJsonU3Ec__Iterator2_1_MoveNext_m2231948168_gshared (U3CGetJsonU3Ec__Iterator2_1_t2839321413 * __this, const MethodInfo* method);
#define U3CGetJsonU3Ec__Iterator2_1_MoveNext_m2231948168(__this, method) ((  bool (*) (U3CGetJsonU3Ec__Iterator2_1_t2839321413 *, const MethodInfo*))U3CGetJsonU3Ec__Iterator2_1_MoveNext_m2231948168_gshared)(__this, method)
// System.Void WebUtil/<GetJson>c__Iterator2`1<System.Object>::Dispose()
extern "C"  void U3CGetJsonU3Ec__Iterator2_1_Dispose_m361262609_gshared (U3CGetJsonU3Ec__Iterator2_1_t2839321413 * __this, const MethodInfo* method);
#define U3CGetJsonU3Ec__Iterator2_1_Dispose_m361262609(__this, method) ((  void (*) (U3CGetJsonU3Ec__Iterator2_1_t2839321413 *, const MethodInfo*))U3CGetJsonU3Ec__Iterator2_1_Dispose_m361262609_gshared)(__this, method)
// System.Void WebUtil/<GetJson>c__Iterator2`1<System.Object>::Reset()
extern "C"  void U3CGetJsonU3Ec__Iterator2_1_Reset_m560877953_gshared (U3CGetJsonU3Ec__Iterator2_1_t2839321413 * __this, const MethodInfo* method);
#define U3CGetJsonU3Ec__Iterator2_1_Reset_m560877953(__this, method) ((  void (*) (U3CGetJsonU3Ec__Iterator2_1_t2839321413 *, const MethodInfo*))U3CGetJsonU3Ec__Iterator2_1_Reset_m560877953_gshared)(__this, method)
// System.Void WebUtil/<GetJson>c__Iterator2`1<System.Object>::<>m__32D(System.String)
extern "C"  void U3CGetJsonU3Ec__Iterator2_1_U3CU3Em__32D_m3010707928_gshared (U3CGetJsonU3Ec__Iterator2_1_t2839321413 * __this, String_t* ___data0, const MethodInfo* method);
#define U3CGetJsonU3Ec__Iterator2_1_U3CU3Em__32D_m3010707928(__this, ___data0, method) ((  void (*) (U3CGetJsonU3Ec__Iterator2_1_t2839321413 *, String_t*, const MethodInfo*))U3CGetJsonU3Ec__Iterator2_1_U3CU3Em__32D_m3010707928_gshared)(__this, ___data0, method)
