﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSApi
struct  JSApi_t70879249  : public Il2CppObject
{
public:

public:
};

struct JSApi_t70879249_StaticFields
{
public:
	// System.Int32 JSApi::protectedFunCount
	int32_t ___protectedFunCount_1;

public:
	inline static int32_t get_offset_of_protectedFunCount_1() { return static_cast<int32_t>(offsetof(JSApi_t70879249_StaticFields, ___protectedFunCount_1)); }
	inline int32_t get_protectedFunCount_1() const { return ___protectedFunCount_1; }
	inline int32_t* get_address_of_protectedFunCount_1() { return &___protectedFunCount_1; }
	inline void set_protectedFunCount_1(int32_t value)
	{
		___protectedFunCount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
