﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChangeParent
struct ChangeParent_t4263565466;

#include "codegen/il2cpp-codegen.h"

// System.Void ChangeParent::.ctor()
extern "C"  void ChangeParent__ctor_m1497900865 (ChangeParent_t4263565466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeParent::Init(System.Single)
extern "C"  void ChangeParent_Init_m3207342488 (ChangeParent_t4263565466 * __this, float ___delayTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeParent::OnChange()
extern "C"  void ChangeParent_OnChange_m1116267410 (ChangeParent_t4263565466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeParent::OnDestroy()
extern "C"  void ChangeParent_OnDestroy_m776106938 (ChangeParent_t4263565466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeParent::ilo_StopCoroutine1(System.UInt32)
extern "C"  void ChangeParent_ilo_StopCoroutine1_m4155002569 (Il2CppObject * __this /* static, unused */, uint32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
