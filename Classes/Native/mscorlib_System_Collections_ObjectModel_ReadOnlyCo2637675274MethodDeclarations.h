﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct ReadOnlyCollection_1_t2637675274;
// System.Collections.Generic.IList`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct IList_1_t3775244941;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// UnityEngine.Rendering.ReflectionProbeBlendInfo[]
struct ReflectionProbeBlendInfoU5BU5D_t558780463;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct IEnumerator_1_t2992462787;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeB1080597738.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3525458685_gshared (ReadOnlyCollection_1_t2637675274 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3525458685(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2637675274 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3525458685_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m642164327_gshared (ReadOnlyCollection_1_t2637675274 * __this, ReflectionProbeBlendInfo_t1080597738  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m642164327(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2637675274 *, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m642164327_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m438948675_gshared (ReadOnlyCollection_1_t2637675274 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m438948675(__this, method) ((  void (*) (ReadOnlyCollection_1_t2637675274 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m438948675_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1546622094_gshared (ReadOnlyCollection_1_t2637675274 * __this, int32_t ___index0, ReflectionProbeBlendInfo_t1080597738  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1546622094(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2637675274 *, int32_t, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1546622094_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m558628976_gshared (ReadOnlyCollection_1_t2637675274 * __this, ReflectionProbeBlendInfo_t1080597738  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m558628976(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2637675274 *, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m558628976_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3715442260_gshared (ReadOnlyCollection_1_t2637675274 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3715442260(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2637675274 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3715442260_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  ReflectionProbeBlendInfo_t1080597738  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3270625658_gshared (ReadOnlyCollection_1_t2637675274 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3270625658(__this, ___index0, method) ((  ReflectionProbeBlendInfo_t1080597738  (*) (ReadOnlyCollection_1_t2637675274 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3270625658_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2054744549_gshared (ReadOnlyCollection_1_t2637675274 * __this, int32_t ___index0, ReflectionProbeBlendInfo_t1080597738  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2054744549(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2637675274 *, int32_t, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2054744549_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4038024991_gshared (ReadOnlyCollection_1_t2637675274 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4038024991(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2637675274 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4038024991_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2754456876_gshared (ReadOnlyCollection_1_t2637675274 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2754456876(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2637675274 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2754456876_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m870512699_gshared (ReadOnlyCollection_1_t2637675274 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m870512699(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2637675274 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m870512699_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m358168546_gshared (ReadOnlyCollection_1_t2637675274 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m358168546(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2637675274 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m358168546_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m1346006074_gshared (ReadOnlyCollection_1_t2637675274 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1346006074(__this, method) ((  void (*) (ReadOnlyCollection_1_t2637675274 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1346006074_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2354060446_gshared (ReadOnlyCollection_1_t2637675274 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2354060446(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2637675274 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2354060446_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2207690042_gshared (ReadOnlyCollection_1_t2637675274 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2207690042(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2637675274 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2207690042_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2510605229_gshared (ReadOnlyCollection_1_t2637675274 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2510605229(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2637675274 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2510605229_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2630641755_gshared (ReadOnlyCollection_1_t2637675274 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2630641755(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2637675274 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2630641755_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1530871485_gshared (ReadOnlyCollection_1_t2637675274 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1530871485(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2637675274 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1530871485_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1583713790_gshared (ReadOnlyCollection_1_t2637675274 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1583713790(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2637675274 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1583713790_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1826515184_gshared (ReadOnlyCollection_1_t2637675274 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1826515184(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2637675274 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1826515184_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m492378765_gshared (ReadOnlyCollection_1_t2637675274 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m492378765(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2637675274 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m492378765_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1301176012_gshared (ReadOnlyCollection_1_t2637675274 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1301176012(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2637675274 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1301176012_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m4000046711_gshared (ReadOnlyCollection_1_t2637675274 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m4000046711(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2637675274 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m4000046711_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3828924484_gshared (ReadOnlyCollection_1_t2637675274 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3828924484(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2637675274 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3828924484_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1894301109_gshared (ReadOnlyCollection_1_t2637675274 * __this, ReflectionProbeBlendInfo_t1080597738  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1894301109(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2637675274 *, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1894301109_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2355101335_gshared (ReadOnlyCollection_1_t2637675274 * __this, ReflectionProbeBlendInfoU5BU5D_t558780463* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m2355101335(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2637675274 *, ReflectionProbeBlendInfoU5BU5D_t558780463*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m2355101335_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m415792524_gshared (ReadOnlyCollection_1_t2637675274 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m415792524(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2637675274 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m415792524_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1293491875_gshared (ReadOnlyCollection_1_t2637675274 * __this, ReflectionProbeBlendInfo_t1080597738  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1293491875(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2637675274 *, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1293491875_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1260849976_gshared (ReadOnlyCollection_1_t2637675274 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1260849976(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2637675274 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1260849976_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::get_Item(System.Int32)
extern "C"  ReflectionProbeBlendInfo_t1080597738  ReadOnlyCollection_1_get_Item_m696819770_gshared (ReadOnlyCollection_1_t2637675274 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m696819770(__this, ___index0, method) ((  ReflectionProbeBlendInfo_t1080597738  (*) (ReadOnlyCollection_1_t2637675274 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m696819770_gshared)(__this, ___index0, method)
