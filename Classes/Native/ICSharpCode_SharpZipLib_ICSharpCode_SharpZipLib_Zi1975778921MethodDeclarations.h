﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.Compression.Inflater
struct Inflater_t1975778921;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::.ctor(System.Boolean)
extern "C"  void Inflater__ctor_m422624816 (Inflater_t1975778921 * __this, bool ___noHeader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::Reset()
extern "C"  void Inflater_Reset_m1534606118 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeHeader()
extern "C"  bool Inflater_DecodeHeader_m579569528 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeDict()
extern "C"  bool Inflater_DecodeDict_m3876441313 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeHuffman()
extern "C"  bool Inflater_DecodeHuffman_m3108706180 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeChksum()
extern "C"  bool Inflater_DecodeChksum_m3418579664 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::Decode()
extern "C"  bool Inflater_Decode_m2502049675 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::SetInput(System.Byte[],System.Int32,System.Int32)
extern "C"  void Inflater_SetInput_m2977611094 (Inflater_t1975778921 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___index1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::Inflate(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t Inflater_Inflate_m2860834391 (Inflater_t1975778921 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_IsNeedingInput()
extern "C"  bool Inflater_get_IsNeedingInput_m2862282958 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_IsNeedingDictionary()
extern "C"  bool Inflater_get_IsNeedingDictionary_m479189876 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_IsFinished()
extern "C"  bool Inflater_get_IsFinished_m3219719202 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_TotalOut()
extern "C"  int64_t Inflater_get_TotalOut_m3201385003 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_RemainingInput()
extern "C"  int32_t Inflater_get_RemainingInput_m39630580 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::.cctor()
extern "C"  void Inflater__cctor_m4087155284 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
