﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3261773968MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.AnimationClip,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3952215701(__this, ___host0, method) ((  void (*) (Enumerator_t2044277981 *, Dictionary_2_t1429341927 *, const MethodInfo*))Enumerator__ctor_m3084319988_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.AnimationClip,System.Boolean>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3187795574(__this, method) ((  Il2CppObject * (*) (Enumerator_t2044277981 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2216994167_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.AnimationClip,System.Boolean>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1537799936(__this, method) ((  void (*) (Enumerator_t2044277981 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m530834241_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.AnimationClip,System.Boolean>::Dispose()
#define Enumerator_Dispose_m2900184183(__this, method) ((  void (*) (Enumerator_t2044277981 *, const MethodInfo*))Enumerator_Dispose_m22587542_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.AnimationClip,System.Boolean>::MoveNext()
#define Enumerator_MoveNext_m630330800(__this, method) ((  bool (*) (Enumerator_t2044277981 *, const MethodInfo*))Enumerator_MoveNext_m3418026097_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.AnimationClip,System.Boolean>::get_Current()
#define Enumerator_get_Current_m2921402344(__this, method) ((  AnimationClip_t2007702890 * (*) (Enumerator_t2044277981 *, const MethodInfo*))Enumerator_get_Current_m898163847_gshared)(__this, method)
