﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CombatAttPlus
struct CombatAttPlus_t649400871;
// System.String
struct String_t;
// CombatAttPlus/FightPlusAtt
struct FightPlusAtt_t4246110079;
// System.Object
struct Il2CppObject;
// CSPlusAtt
struct CSPlusAtt_t3268315159;
// CSHeroData
struct CSHeroData_t3763839828;
// System.Collections.Generic.List`1<CSPlusAtt>
struct List_1_t341533415;
// CSPlayerData
struct CSPlayerData_t1029357243;
// CSGameDataMgr
struct CSGameDataMgr_t2623305516;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_HERO_ATT_KEY3680959548.h"
#include "AssemblyU2DCSharp_CombatAttPlus_FightPlusAtt4246110079.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_CombatAttPlus649400871.h"
#include "AssemblyU2DCSharp_CSHeroData3763839828.h"
#include "AssemblyU2DCSharp_CSPlayerData1029357243.h"

// System.Void CombatAttPlus::.ctor()
extern "C"  void CombatAttPlus__ctor_m3704212260 (CombatAttPlus_t649400871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus::AddPlusAttribute(System.String,System.UInt32,System.UInt32)
extern "C"  void CombatAttPlus_AddPlusAttribute_m3884259497 (CombatAttPlus_t649400871 * __this, String_t* ___key0, uint32_t ___value1, uint32_t ___valuePer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus::GetAttribute(HERO_ATT_KEY,System.Single)
extern "C"  uint32_t CombatAttPlus_GetAttribute_m3644052404 (CombatAttPlus_t649400871 * __this, int32_t ___key0, float ___basicsAtt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus::GetHeroAttr(HERO_ATT_KEY,System.UInt32)
extern "C"  uint32_t CombatAttPlus_GetHeroAttr_m4274523606 (CombatAttPlus_t649400871 * __this, int32_t ___key0, uint32_t ___basicsAtt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus::GetEnemyAttr(HERO_ATT_KEY,System.UInt32)
extern "C"  uint32_t CombatAttPlus_GetEnemyAttr_m3164963968 (CombatAttPlus_t649400871 * __this, int32_t ___key0, uint32_t ___basicsAtt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CombatAttPlus::EnemyToString(HERO_ATT_KEY)
extern "C"  String_t* CombatAttPlus_EnemyToString_m2647570045 (CombatAttPlus_t649400871 * __this, int32_t ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus::Clear()
extern "C"  void CombatAttPlus_Clear_m1110345551 (CombatAttPlus_t649400871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus::ilo_get_attack_per1(CombatAttPlus/FightPlusAtt)
extern "C"  uint32_t CombatAttPlus_ilo_get_attack_per1_m2137855085 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus::ilo_set_attack_per2(CombatAttPlus/FightPlusAtt,System.UInt32)
extern "C"  void CombatAttPlus_ilo_set_attack_per2_m4207297299 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus::ilo_get_hp_per3(CombatAttPlus/FightPlusAtt)
extern "C"  uint32_t CombatAttPlus_ilo_get_hp_per3_m1654470319 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus::ilo_get_phy_def_per4(CombatAttPlus/FightPlusAtt)
extern "C"  uint32_t CombatAttPlus_ilo_get_phy_def_per4_m1387451729 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus::ilo_set_mag_def_per5(CombatAttPlus/FightPlusAtt,System.UInt32)
extern "C"  void CombatAttPlus_ilo_set_mag_def_per5_m2400481571 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus::ilo_set_hitrate_per6(CombatAttPlus/FightPlusAtt,System.UInt32)
extern "C"  void CombatAttPlus_ilo_set_hitrate_per6_m2638049482 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus::ilo_set_hitrate7(CombatAttPlus/FightPlusAtt,System.UInt32)
extern "C"  void CombatAttPlus_ilo_set_hitrate7_m4033871017 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus::ilo_set_dodge_per8(CombatAttPlus/FightPlusAtt,System.UInt32)
extern "C"  void CombatAttPlus_ilo_set_dodge_per8_m2903081128 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus::ilo_get_destroy9(CombatAttPlus/FightPlusAtt)
extern "C"  uint32_t CombatAttPlus_ilo_get_destroy9_m1086503201 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus::ilo_set_destroy10(CombatAttPlus/FightPlusAtt,System.UInt32)
extern "C"  void CombatAttPlus_ilo_set_destroy10_m2895425632 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus::ilo_get_block_per11(CombatAttPlus/FightPlusAtt)
extern "C"  uint32_t CombatAttPlus_ilo_get_block_per11_m2190776301 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus::ilo_set_block_per12(CombatAttPlus/FightPlusAtt,System.UInt32)
extern "C"  void CombatAttPlus_ilo_set_block_per12_m583116435 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus::ilo_get_block13(CombatAttPlus/FightPlusAtt)
extern "C"  uint32_t CombatAttPlus_ilo_get_block13_m1236223889 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus::ilo_set_crit_per14(CombatAttPlus/FightPlusAtt,System.UInt32)
extern "C"  void CombatAttPlus_ilo_set_crit_per14_m1031167456 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus::ilo_get_anticrit15(CombatAttPlus/FightPlusAtt)
extern "C"  uint32_t CombatAttPlus_ilo_get_anticrit15_m2701320578 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus::ilo_set_damage_bonus16(CombatAttPlus/FightPlusAtt,System.UInt32)
extern "C"  void CombatAttPlus_ilo_set_damage_bonus16_m1037854585 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus::ilo_get_damage_reduce17(CombatAttPlus/FightPlusAtt)
extern "C"  uint32_t CombatAttPlus_ilo_get_damage_reduce17_m3711714174 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus::ilo_set_crit_hurt18(CombatAttPlus/FightPlusAtt,System.UInt32)
extern "C"  void CombatAttPlus_ilo_set_crit_hurt18_m2466042498 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus::ilo_get_phy_def19(CombatAttPlus/FightPlusAtt)
extern "C"  uint32_t CombatAttPlus_ilo_get_phy_def19_m2995423441 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus::ilo_get_hitrate_per20(CombatAttPlus/FightPlusAtt)
extern "C"  uint32_t CombatAttPlus_ilo_get_hitrate_per20_m3990425585 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus::ilo_get_hitrate21(CombatAttPlus/FightPlusAtt)
extern "C"  uint32_t CombatAttPlus_ilo_get_hitrate21_m859586068 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus::ilo_get_destroy_per22(CombatAttPlus/FightPlusAtt)
extern "C"  uint32_t CombatAttPlus_ilo_get_destroy_per22_m3170670650 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus::ilo_get_crit_per23(CombatAttPlus/FightPlusAtt)
extern "C"  uint32_t CombatAttPlus_ilo_get_crit_per23_m2452358171 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus::ilo_get_crit24(CombatAttPlus/FightPlusAtt)
extern "C"  uint32_t CombatAttPlus_ilo_get_crit24_m3960643006 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus::ilo_get_crit_hurt25(CombatAttPlus/FightPlusAtt)
extern "C"  uint32_t CombatAttPlus_ilo_get_crit_hurt25_m1495639225 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus::ilo_get_damage_bonus26(CombatAttPlus/FightPlusAtt)
extern "C"  uint32_t CombatAttPlus_ilo_get_damage_bonus26_m3150000501 (Il2CppObject * __this /* static, unused */, FightPlusAtt_t4246110079 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatAttPlus::ilo_Log27(System.Object,System.Boolean)
extern "C"  void CombatAttPlus_ilo_Log27_m1756243937 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CombatAttPlus::ilo_EnemyToString28(CombatAttPlus,HERO_ATT_KEY)
extern "C"  String_t* CombatAttPlus_ilo_EnemyToString28_m964261099 (Il2CppObject * __this /* static, unused */, CombatAttPlus_t649400871 * ____this0, int32_t ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSPlusAtt CombatAttPlus::ilo_GetLearerAttUnit29(CSHeroData,System.String)
extern "C"  CSPlusAtt_t3268315159 * CombatAttPlus_ilo_GetLearerAttUnit29_m2104023198 (Il2CppObject * __this /* static, unused */, CSHeroData_t3763839828 * ____this0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CombatAttPlus::ilo_GetAttribute30(CombatAttPlus,HERO_ATT_KEY,System.Single)
extern "C"  uint32_t CombatAttPlus_ilo_GetAttribute30_m2538108159 (Il2CppObject * __this /* static, unused */, CombatAttPlus_t649400871 * ____this0, int32_t ___key1, float ___basicsAtt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPlusAtt> CombatAttPlus::ilo_GetReinPlusAtts31(CSPlayerData)
extern "C"  List_1_t341533415 * CombatAttPlus_ilo_GetReinPlusAtts31_m408249252 (Il2CppObject * __this /* static, unused */, CSPlayerData_t1029357243 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSPlusAtt CombatAttPlus::ilo_GetLearerAttUnit32(CSPlayerData,System.String)
extern "C"  CSPlusAtt_t3268315159 * CombatAttPlus_ilo_GetLearerAttUnit32_m686346031 (Il2CppObject * __this /* static, unused */, CSPlayerData_t1029357243 * ____this0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSGameDataMgr CombatAttPlus::ilo_get_CSGameDataMgr33()
extern "C"  CSGameDataMgr_t2623305516 * CombatAttPlus_ilo_get_CSGameDataMgr33_m332643545 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
