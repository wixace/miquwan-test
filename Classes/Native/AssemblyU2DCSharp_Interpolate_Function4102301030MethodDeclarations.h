﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Interpolate/Function
struct Function_t4102301030;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void Interpolate/Function::.ctor(System.Object,System.IntPtr)
extern "C"  void Function__ctor_m2656297741 (Function_t4102301030 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate/Function::Invoke(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Function_Invoke_m3274273671 (Function_t4102301030 * __this, float ___a0, float ___b1, float ___c2, float ___d3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Interpolate/Function::BeginInvoke(System.Single,System.Single,System.Single,System.Single,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Function_BeginInvoke_m3393752552 (Function_t4102301030 * __this, float ___a0, float ___b1, float ___c2, float ___d3, AsyncCallback_t1369114871 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate/Function::EndInvoke(System.IAsyncResult)
extern "C"  float Function_EndInvoke_m780890729 (Function_t4102301030 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
