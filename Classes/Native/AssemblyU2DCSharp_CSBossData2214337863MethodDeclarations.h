﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSBossData
struct CSBossData_t2214337863;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void CSBossData::.ctor()
extern "C"  void CSBossData__ctor_m3850271540 (CSBossData_t2214337863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSBossData::LoadData(System.String)
extern "C"  void CSBossData_LoadData_m1032208706 (CSBossData_t2214337863 * __this, String_t* ___jsonStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSBossData::UnLoadData()
extern "C"  void CSBossData_UnLoadData_m807300473 (CSBossData_t2214337863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
