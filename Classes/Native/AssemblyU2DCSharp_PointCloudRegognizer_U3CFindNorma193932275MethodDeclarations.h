﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PointCloudRegognizer/<FindNormalizedTemplate>c__AnonStorey129
struct U3CFindNormalizedTemplateU3Ec__AnonStorey129_t193932275;
// PointCloudRegognizer/NormalizedTemplate
struct NormalizedTemplate_t2023934299;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PointCloudRegognizer_NormalizedT2023934299.h"

// System.Void PointCloudRegognizer/<FindNormalizedTemplate>c__AnonStorey129::.ctor()
extern "C"  void U3CFindNormalizedTemplateU3Ec__AnonStorey129__ctor_m3630532312 (U3CFindNormalizedTemplateU3Ec__AnonStorey129_t193932275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PointCloudRegognizer/<FindNormalizedTemplate>c__AnonStorey129::<>m__363(PointCloudRegognizer/NormalizedTemplate)
extern "C"  bool U3CFindNormalizedTemplateU3Ec__AnonStorey129_U3CU3Em__363_m1623365498 (U3CFindNormalizedTemplateU3Ec__AnonStorey129_t193932275 * __this, NormalizedTemplate_t2023934299 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
