﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.HumanTrait
struct HumanTrait_t3308579237;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.HumanTrait::.ctor()
extern "C"  void HumanTrait__ctor_m793079596 (HumanTrait_t3308579237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.HumanTrait::get_MuscleCount()
extern "C"  int32_t HumanTrait_get_MuscleCount_m1747418623 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UnityEngine.HumanTrait::get_MuscleName()
extern "C"  StringU5BU5D_t4054002952* HumanTrait_get_MuscleName_m4027480702 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.HumanTrait::get_BoneCount()
extern "C"  int32_t HumanTrait_get_BoneCount_m3672017228 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UnityEngine.HumanTrait::get_BoneName()
extern "C"  StringU5BU5D_t4054002952* HumanTrait_get_BoneName_m360721873 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.HumanTrait::MuscleFromBone(System.Int32,System.Int32)
extern "C"  int32_t HumanTrait_MuscleFromBone_m3036118095 (Il2CppObject * __this /* static, unused */, int32_t ___i0, int32_t ___dofIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.HumanTrait::BoneFromMuscle(System.Int32)
extern "C"  int32_t HumanTrait_BoneFromMuscle_m3586950920 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.HumanTrait::RequiredBone(System.Int32)
extern "C"  bool HumanTrait_RequiredBone_m12572178 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.HumanTrait::get_RequiredBoneCount()
extern "C"  int32_t HumanTrait_get_RequiredBoneCount_m4194008109 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.HumanTrait::GetMuscleDefaultMin(System.Int32)
extern "C"  float HumanTrait_GetMuscleDefaultMin_m933324647 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.HumanTrait::GetMuscleDefaultMax(System.Int32)
extern "C"  float HumanTrait_GetMuscleDefaultMax_m2913246969 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.HumanTrait::GetParentBone(System.Int32)
extern "C"  int32_t HumanTrait_GetParentBone_m2123850911 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
