﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Poly2Tri.TriangulationConstraint
struct TriangulationConstraint_t137695394;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Poly2Tri.TriangulationConstraint::.ctor()
extern "C"  void TriangulationConstraint__ctor_m2519444399 (TriangulationConstraint_t137695394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
