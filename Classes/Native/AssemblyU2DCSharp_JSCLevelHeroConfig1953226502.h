﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<JSCLevelHeroPointConfig>
struct List_1_t1929876094;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSCLevelHeroConfig
struct  JSCLevelHeroConfig_t1953226502  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<JSCLevelHeroPointConfig> JSCLevelHeroConfig::_pointList
	List_1_t1929876094 * ____pointList_0;
	// System.Collections.Generic.List`1<System.Int32> JSCLevelHeroConfig::_idList
	List_1_t2522024052 * ____idList_1;
	// System.Single JSCLevelHeroConfig::_spacing
	float ____spacing_2;
	// ProtoBuf.IExtension JSCLevelHeroConfig::extensionObject
	Il2CppObject * ___extensionObject_3;

public:
	inline static int32_t get_offset_of__pointList_0() { return static_cast<int32_t>(offsetof(JSCLevelHeroConfig_t1953226502, ____pointList_0)); }
	inline List_1_t1929876094 * get__pointList_0() const { return ____pointList_0; }
	inline List_1_t1929876094 ** get_address_of__pointList_0() { return &____pointList_0; }
	inline void set__pointList_0(List_1_t1929876094 * value)
	{
		____pointList_0 = value;
		Il2CppCodeGenWriteBarrier(&____pointList_0, value);
	}

	inline static int32_t get_offset_of__idList_1() { return static_cast<int32_t>(offsetof(JSCLevelHeroConfig_t1953226502, ____idList_1)); }
	inline List_1_t2522024052 * get__idList_1() const { return ____idList_1; }
	inline List_1_t2522024052 ** get_address_of__idList_1() { return &____idList_1; }
	inline void set__idList_1(List_1_t2522024052 * value)
	{
		____idList_1 = value;
		Il2CppCodeGenWriteBarrier(&____idList_1, value);
	}

	inline static int32_t get_offset_of__spacing_2() { return static_cast<int32_t>(offsetof(JSCLevelHeroConfig_t1953226502, ____spacing_2)); }
	inline float get__spacing_2() const { return ____spacing_2; }
	inline float* get_address_of__spacing_2() { return &____spacing_2; }
	inline void set__spacing_2(float value)
	{
		____spacing_2 = value;
	}

	inline static int32_t get_offset_of_extensionObject_3() { return static_cast<int32_t>(offsetof(JSCLevelHeroConfig_t1953226502, ___extensionObject_3)); }
	inline Il2CppObject * get_extensionObject_3() const { return ___extensionObject_3; }
	inline Il2CppObject ** get_address_of_extensionObject_3() { return &___extensionObject_3; }
	inline void set_extensionObject_3(Il2CppObject * value)
	{
		___extensionObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObject_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
