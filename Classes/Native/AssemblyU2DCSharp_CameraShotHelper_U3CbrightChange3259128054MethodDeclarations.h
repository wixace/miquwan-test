﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraShotHelper/<brightChange>c__AnonStorey14F
struct U3CbrightChangeU3Ec__AnonStorey14F_t3259128054;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"

// System.Void CameraShotHelper/<brightChange>c__AnonStorey14F::.ctor()
extern "C"  void U3CbrightChangeU3Ec__AnonStorey14F__ctor_m2393211701 (U3CbrightChangeU3Ec__AnonStorey14F_t3259128054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotHelper/<brightChange>c__AnonStorey14F::<>m__3C0(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  void U3CbrightChangeU3Ec__AnonStorey14F_U3CU3Em__3C0_m2533821707 (U3CbrightChangeU3Ec__AnonStorey14F_t3259128054 * __this, AssetOperation_t778728221 * ___ao0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
