﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2664323283.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmoni3881980607.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rendering.SphericalHarmonicsL2>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m465633785_gshared (InternalEnumerator_1_t2664323283 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m465633785(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2664323283 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m465633785_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rendering.SphericalHarmonicsL2>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3173848839_gshared (InternalEnumerator_1_t2664323283 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3173848839(__this, method) ((  void (*) (InternalEnumerator_1_t2664323283 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3173848839_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Rendering.SphericalHarmonicsL2>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3607159347_gshared (InternalEnumerator_1_t2664323283 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3607159347(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2664323283 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3607159347_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rendering.SphericalHarmonicsL2>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3502309136_gshared (InternalEnumerator_1_t2664323283 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3502309136(__this, method) ((  void (*) (InternalEnumerator_1_t2664323283 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3502309136_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Rendering.SphericalHarmonicsL2>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2010133491_gshared (InternalEnumerator_1_t2664323283 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2010133491(__this, method) ((  bool (*) (InternalEnumerator_1_t2664323283 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2010133491_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Rendering.SphericalHarmonicsL2>::get_Current()
extern "C"  SphericalHarmonicsL2_t3881980607  InternalEnumerator_1_get_Current_m316206208_gshared (InternalEnumerator_1_t2664323283 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m316206208(__this, method) ((  SphericalHarmonicsL2_t3881980607  (*) (InternalEnumerator_1_t2664323283 *, const MethodInfo*))InternalEnumerator_1_get_Current_m316206208_gshared)(__this, method)
