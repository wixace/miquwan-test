﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>
struct Collection_1_t1897100007;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// UnityEngine.Bounds[]
struct BoundsU5BU5D_t3144995076;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Bounds>
struct IEnumerator_1_t328539602;
// System.Collections.Generic.IList`1<UnityEngine.Bounds>
struct IList_1_t1111321756;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::.ctor()
extern "C"  void Collection_1__ctor_m989703987_gshared (Collection_1_t1897100007 * __this, const MethodInfo* method);
#define Collection_1__ctor_m989703987(__this, method) ((  void (*) (Collection_1_t1897100007 *, const MethodInfo*))Collection_1__ctor_m989703987_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3388209700_gshared (Collection_1_t1897100007 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3388209700(__this, method) ((  bool (*) (Collection_1_t1897100007 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3388209700_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m3506039153_gshared (Collection_1_t1897100007 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m3506039153(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1897100007 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m3506039153_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1873983680_gshared (Collection_1_t1897100007 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1873983680(__this, method) ((  Il2CppObject * (*) (Collection_1_t1897100007 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1873983680_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m4229437565_gshared (Collection_1_t1897100007 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m4229437565(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1897100007 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m4229437565_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1881459171_gshared (Collection_1_t1897100007 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1881459171(__this, ___value0, method) ((  bool (*) (Collection_1_t1897100007 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1881459171_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m946718805_gshared (Collection_1_t1897100007 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m946718805(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1897100007 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m946718805_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m1383807048_gshared (Collection_1_t1897100007 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1383807048(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1897100007 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1383807048_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1981455328_gshared (Collection_1_t1897100007 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1981455328(__this, ___value0, method) ((  void (*) (Collection_1_t1897100007 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1981455328_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1221926553_gshared (Collection_1_t1897100007 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1221926553(__this, method) ((  bool (*) (Collection_1_t1897100007 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1221926553_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m623191883_gshared (Collection_1_t1897100007 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m623191883(__this, method) ((  Il2CppObject * (*) (Collection_1_t1897100007 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m623191883_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1953914770_gshared (Collection_1_t1897100007 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1953914770(__this, method) ((  bool (*) (Collection_1_t1897100007 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1953914770_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1625416999_gshared (Collection_1_t1897100007 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1625416999(__this, method) ((  bool (*) (Collection_1_t1897100007 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1625416999_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2952557074_gshared (Collection_1_t1897100007 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m2952557074(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1897100007 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2952557074_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m3307631135_gshared (Collection_1_t1897100007 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m3307631135(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1897100007 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m3307631135_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::Add(T)
extern "C"  void Collection_1_Add_m853352172_gshared (Collection_1_t1897100007 * __this, Bounds_t2711641849  ___item0, const MethodInfo* method);
#define Collection_1_Add_m853352172(__this, ___item0, method) ((  void (*) (Collection_1_t1897100007 *, Bounds_t2711641849 , const MethodInfo*))Collection_1_Add_m853352172_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::Clear()
extern "C"  void Collection_1_Clear_m2690804574_gshared (Collection_1_t1897100007 * __this, const MethodInfo* method);
#define Collection_1_Clear_m2690804574(__this, method) ((  void (*) (Collection_1_t1897100007 *, const MethodInfo*))Collection_1_Clear_m2690804574_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3678117988_gshared (Collection_1_t1897100007 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3678117988(__this, method) ((  void (*) (Collection_1_t1897100007 *, const MethodInfo*))Collection_1_ClearItems_m3678117988_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::Contains(T)
extern "C"  bool Collection_1_Contains_m665929040_gshared (Collection_1_t1897100007 * __this, Bounds_t2711641849  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m665929040(__this, ___item0, method) ((  bool (*) (Collection_1_t1897100007 *, Bounds_t2711641849 , const MethodInfo*))Collection_1_Contains_m665929040_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1934205916_gshared (Collection_1_t1897100007 * __this, BoundsU5BU5D_t3144995076* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m1934205916(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1897100007 *, BoundsU5BU5D_t3144995076*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1934205916_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3614983335_gshared (Collection_1_t1897100007 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m3614983335(__this, method) ((  Il2CppObject* (*) (Collection_1_t1897100007 *, const MethodInfo*))Collection_1_GetEnumerator_m3614983335_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m1687655080_gshared (Collection_1_t1897100007 * __this, Bounds_t2711641849  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m1687655080(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1897100007 *, Bounds_t2711641849 , const MethodInfo*))Collection_1_IndexOf_m1687655080_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1534525779_gshared (Collection_1_t1897100007 * __this, int32_t ___index0, Bounds_t2711641849  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m1534525779(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1897100007 *, int32_t, Bounds_t2711641849 , const MethodInfo*))Collection_1_Insert_m1534525779_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1012354566_gshared (Collection_1_t1897100007 * __this, int32_t ___index0, Bounds_t2711641849  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m1012354566(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1897100007 *, int32_t, Bounds_t2711641849 , const MethodInfo*))Collection_1_InsertItem_m1012354566_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m2791261726_gshared (Collection_1_t1897100007 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m2791261726(__this, method) ((  Il2CppObject* (*) (Collection_1_t1897100007 *, const MethodInfo*))Collection_1_get_Items_m2791261726_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::Remove(T)
extern "C"  bool Collection_1_Remove_m2954303819_gshared (Collection_1_t1897100007 * __this, Bounds_t2711641849  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2954303819(__this, ___item0, method) ((  bool (*) (Collection_1_t1897100007 *, Bounds_t2711641849 , const MethodInfo*))Collection_1_Remove_m2954303819_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3703345945_gshared (Collection_1_t1897100007 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3703345945(__this, ___index0, method) ((  void (*) (Collection_1_t1897100007 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3703345945_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1287189753_gshared (Collection_1_t1897100007 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m1287189753(__this, ___index0, method) ((  void (*) (Collection_1_t1897100007 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1287189753_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m595007443_gshared (Collection_1_t1897100007 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m595007443(__this, method) ((  int32_t (*) (Collection_1_t1897100007 *, const MethodInfo*))Collection_1_get_Count_m595007443_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::get_Item(System.Int32)
extern "C"  Bounds_t2711641849  Collection_1_get_Item_m1090826047_gshared (Collection_1_t1897100007 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m1090826047(__this, ___index0, method) ((  Bounds_t2711641849  (*) (Collection_1_t1897100007 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1090826047_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3315087722_gshared (Collection_1_t1897100007 * __this, int32_t ___index0, Bounds_t2711641849  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3315087722(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1897100007 *, int32_t, Bounds_t2711641849 , const MethodInfo*))Collection_1_set_Item_m3315087722_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1642248655_gshared (Collection_1_t1897100007 * __this, int32_t ___index0, Bounds_t2711641849  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1642248655(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1897100007 *, int32_t, Bounds_t2711641849 , const MethodInfo*))Collection_1_SetItem_m1642248655_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m3788628700_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m3788628700(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3788628700_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::ConvertItem(System.Object)
extern "C"  Bounds_t2711641849  Collection_1_ConvertItem_m398894750_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m398894750(__this /* static, unused */, ___item0, method) ((  Bounds_t2711641849  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m398894750_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m378243996_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m378243996(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m378243996_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m2262431336_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m2262431336(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2262431336_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Bounds>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m11938103_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m11938103(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m11938103_gshared)(__this /* static, unused */, ___list0, method)
