﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSMgr/CSCallbackProperty
struct CSCallbackProperty_t2220951498;
// System.Object
struct Il2CppObject;
// JSVCall
struct JSVCall_t3708497963;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void JSMgr/CSCallbackProperty::.ctor(System.Object,System.IntPtr)
extern "C"  void CSCallbackProperty__ctor_m3392974705 (CSCallbackProperty_t2220951498 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr/CSCallbackProperty::Invoke(JSVCall)
extern "C"  void CSCallbackProperty_Invoke_m2354342972 (CSCallbackProperty_t2220951498 * __this, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult JSMgr/CSCallbackProperty::BeginInvoke(JSVCall,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CSCallbackProperty_BeginInvoke_m958818199 (CSCallbackProperty_t2220951498 * __this, JSVCall_t3708497963 * ___vc0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSMgr/CSCallbackProperty::EndInvoke(System.IAsyncResult)
extern "C"  void CSCallbackProperty_EndInvoke_m1575715329 (CSCallbackProperty_t2220951498 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
