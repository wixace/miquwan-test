﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginYinHu
struct PluginYinHu_t2559462776;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.String
struct String_t;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Object
struct Il2CppObject;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PluginYinHu2559462776.h"

// System.Void PluginYinHu::.ctor()
extern "C"  void PluginYinHu__ctor_m332453107 (PluginYinHu_t2559462776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYinHu::Init()
extern "C"  void PluginYinHu_Init_m2456112897 (PluginYinHu_t2559462776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginYinHu::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginYinHu_ReqSDKHttpLogin_m2953084580 (PluginYinHu_t2559462776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYinHu::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginYinHu_IsLoginSuccess_m672366872 (PluginYinHu_t2559462776 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYinHu::OpenUserLogin()
extern "C"  void PluginYinHu_OpenUserLogin_m3865793605 (PluginYinHu_t2559462776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYinHu::OnLoginSuccess(System.String)
extern "C"  void PluginYinHu_OnLoginSuccess_m2221687736 (PluginYinHu_t2559462776 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYinHu::OnLogoutSuccess(System.String)
extern "C"  void PluginYinHu_OnLogoutSuccess_m156385751 (PluginYinHu_t2559462776 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYinHu::OnSDKLogout(System.String)
extern "C"  void PluginYinHu_OnSDKLogout_m3489701548 (PluginYinHu_t2559462776 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYinHu::OnGameSwitchSuccess(System.String)
extern "C"  void PluginYinHu_OnGameSwitchSuccess_m3533109651 (PluginYinHu_t2559462776 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYinHu::UserPay(CEvent.ZEvent)
extern "C"  void PluginYinHu_UserPay_m3540178829 (PluginYinHu_t2559462776 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYinHu::OnPaySuccess(System.String)
extern "C"  void PluginYinHu_OnPaySuccess_m1786786807 (PluginYinHu_t2559462776 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYinHu::OnPayFail(System.String)
extern "C"  void PluginYinHu_OnPayFail_m952604842 (PluginYinHu_t2559462776 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYinHu::OnEnterGame(CEvent.ZEvent)
extern "C"  void PluginYinHu_OnEnterGame_m321110751 (PluginYinHu_t2559462776 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYinHu::OnCreatRole(CEvent.ZEvent)
extern "C"  void PluginYinHu_OnCreatRole_m1150526602 (PluginYinHu_t2559462776 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYinHu::OnLevelUp(CEvent.ZEvent)
extern "C"  void PluginYinHu_OnLevelUp_m3238962090 (PluginYinHu_t2559462776 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYinHu::ClollectData(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYinHu_ClollectData_m2316784855 (PluginYinHu_t2559462776 * __this, String_t* ___type0, String_t* ___serverid1, String_t* ___servername2, String_t* ___roleid3, String_t* ___rolename4, String_t* ___rolelevel5, String_t* ___roleGold6, String_t* ___VIPLv7, String_t* ___GameName8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYinHu::init(System.String,System.String)
extern "C"  void PluginYinHu_init_m3881382973 (PluginYinHu_t2559462776 * __this, String_t* ___AppID0, String_t* ___channel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYinHu::login()
extern "C"  void PluginYinHu_login_m4149435226 (PluginYinHu_t2559462776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYinHu::goZF(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYinHu_goZF_m3240882585 (PluginYinHu_t2559462776 * __this, String_t* ___amount0, String_t* ___orderId1, String_t* ___extra2, String_t* ___tempId3, String_t* ___serverId4, String_t* ___serverName5, String_t* ___roleId6, String_t* ___roleName7, String_t* ___setProductNumber8, String_t* ___setProductName9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYinHu::coloectData(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYinHu_coloectData_m2129476844 (PluginYinHu_t2559462776 * __this, String_t* ___serverId0, String_t* ___servername1, String_t* ___roleid2, String_t* ___rolename3, String_t* ___rolelevel4, String_t* ___roleGold5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYinHu::<OnLogoutSuccess>m__470()
extern "C"  void PluginYinHu_U3COnLogoutSuccessU3Em__470_m3516878961 (PluginYinHu_t2559462776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginYinHu::ilo_get_Instance1()
extern "C"  VersionMgr_t1322950208 * PluginYinHu_ilo_get_Instance1_m1423239310 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginYinHu::ilo_get_DeviceID2(PluginsSdkMgr)
extern "C"  String_t* PluginYinHu_ilo_get_DeviceID2_m858383515 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYinHu::ilo_get_isSdkLogin3(VersionMgr)
extern "C"  bool PluginYinHu_ilo_get_isSdkLogin3_m2907936867 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYinHu::ilo_Log4(System.Object,System.Boolean)
extern "C"  void PluginYinHu_ilo_Log4_m4183952573 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYinHu::ilo_goZF5(PluginYinHu,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYinHu_ilo_goZF5_m2848706547 (Il2CppObject * __this /* static, unused */, PluginYinHu_t2559462776 * ____this0, String_t* ___amount1, String_t* ___orderId2, String_t* ___extra3, String_t* ___tempId4, String_t* ___serverId5, String_t* ___serverName6, String_t* ___roleId7, String_t* ___roleName8, String_t* ___setProductNumber9, String_t* ___setProductName10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginYinHu::ilo_get_ProductsCfgMgr6()
extern "C"  ProductsCfgMgr_t2493714872 * PluginYinHu_ilo_get_ProductsCfgMgr6_m1015484856 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYinHu::ilo_ClollectData7(PluginYinHu,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYinHu_ilo_ClollectData7_m1216959803 (Il2CppObject * __this /* static, unused */, PluginYinHu_t2559462776 * ____this0, String_t* ___type1, String_t* ___serverid2, String_t* ___servername3, String_t* ___roleid4, String_t* ___rolename5, String_t* ___rolelevel6, String_t* ___roleGold7, String_t* ___VIPLv8, String_t* ___GameName9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
