﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Helpers
struct Helpers_t2486338795;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3339007067;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t4136801618;
// System.Reflection.ConstructorInfo[]
struct ConstructorInfoU5BU5D_t2079826215;
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t674955999;
// System.Reflection.MemberInfo
struct MemberInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoTypeCode2227754741.h"
#include "mscorlib_System_Reflection_PropertyInfo924268725.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"

// System.Void ProtoBuf.Helpers::.ctor()
extern "C"  void Helpers__ctor_m691019225 (Helpers_t2486338795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Helpers::.cctor()
extern "C"  void Helpers__cctor_m3759630580 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder ProtoBuf.Helpers::AppendLine(System.Text.StringBuilder)
extern "C"  StringBuilder_t243639308 * Helpers_AppendLine_m642602534 (Il2CppObject * __this /* static, unused */, StringBuilder_t243639308 * ___builder0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Helpers::IsNullOrEmpty(System.String)
extern "C"  bool Helpers_IsNullOrEmpty_m4041311206 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Helpers::DebugWriteLine(System.String,System.Object)
extern "C"  void Helpers_DebugWriteLine_m1374252101 (Il2CppObject * __this /* static, unused */, String_t* ___message0, Il2CppObject * ___obj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Helpers::DebugWriteLine(System.String)
extern "C"  void Helpers_DebugWriteLine_m2147282359 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Helpers::TraceWriteLine(System.String)
extern "C"  void Helpers_TraceWriteLine_m3969847977 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Helpers::DebugAssert(System.Boolean,System.String)
extern "C"  void Helpers_DebugAssert_m2176613667 (Il2CppObject * __this /* static, unused */, bool ___condition0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Helpers::DebugAssert(System.Boolean,System.String,System.Object[])
extern "C"  void Helpers_DebugAssert_m195784079 (Il2CppObject * __this /* static, unused */, bool ___condition0, String_t* ___message1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Helpers::DebugAssert(System.Boolean)
extern "C"  void Helpers_DebugAssert_m3293242663 (Il2CppObject * __this /* static, unused */, bool ___condition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Helpers::Sort(System.Int32[],System.Object[])
extern "C"  void Helpers_Sort_m4232994116 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___keys0, ObjectU5BU5D_t1108656482* ___values1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Helpers::BlockCopy(System.Byte[],System.Int32,System.Byte[],System.Int32,System.Int32)
extern "C"  void Helpers_BlockCopy_m4157848874 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___from0, int32_t ___fromIndex1, ByteU5BU5D_t4260760469* ___to2, int32_t ___toIndex3, int32_t ___count4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Helpers::IsInfinity(System.Single)
extern "C"  bool Helpers_IsInfinity_m1219434458 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Helpers::GetInstanceMethod(System.Type,System.String)
extern "C"  MethodInfo_t * Helpers_GetInstanceMethod_m268601404 (Il2CppObject * __this /* static, unused */, Type_t * ___declaringType0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Helpers::GetStaticMethod(System.Type,System.String)
extern "C"  MethodInfo_t * Helpers_GetStaticMethod_m634651907 (Il2CppObject * __this /* static, unused */, Type_t * ___declaringType0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Helpers::GetInstanceMethod(System.Type,System.String,System.Type[])
extern "C"  MethodInfo_t * Helpers_GetInstanceMethod_m3526018253 (Il2CppObject * __this /* static, unused */, Type_t * ___declaringType0, String_t* ___name1, TypeU5BU5D_t3339007067* ___types2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Helpers::IsSubclassOf(System.Type,System.Type)
extern "C"  bool Helpers_IsSubclassOf_m2928286580 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, Type_t * ___baseClass1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Helpers::IsInfinity(System.Double)
extern "C"  bool Helpers_IsInfinity_m969871729 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.ProtoTypeCode ProtoBuf.Helpers::GetTypeCode(System.Type)
extern "C"  int32_t Helpers_GetTypeCode_m603794742 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Helpers::GetUnderlyingType(System.Type)
extern "C"  Type_t * Helpers_GetUnderlyingType_m4160206895 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Helpers::IsValueType(System.Type)
extern "C"  bool Helpers_IsValueType_m2465035701 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Helpers::IsEnum(System.Type)
extern "C"  bool Helpers_IsEnum_m2846879535 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Helpers::GetGetMethod(System.Reflection.PropertyInfo,System.Boolean,System.Boolean)
extern "C"  MethodInfo_t * Helpers_GetGetMethod_m3568137519 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___property0, bool ___nonPublic1, bool ___allowInternal2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Helpers::GetSetMethod(System.Reflection.PropertyInfo,System.Boolean,System.Boolean)
extern "C"  MethodInfo_t * Helpers_GetSetMethod_m2059000891 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___property0, bool ___nonPublic1, bool ___allowInternal2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo ProtoBuf.Helpers::GetConstructor(System.Type,System.Type[],System.Boolean)
extern "C"  ConstructorInfo_t4136801618 * Helpers_GetConstructor_m1030297489 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, TypeU5BU5D_t3339007067* ___parameterTypes1, bool ___nonPublic2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo[] ProtoBuf.Helpers::GetConstructors(System.Type,System.Boolean)
extern "C"  ConstructorInfoU5BU5D_t2079826215* Helpers_GetConstructors_m1254036093 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, bool ___nonPublic1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo ProtoBuf.Helpers::GetProperty(System.Type,System.String,System.Boolean)
extern "C"  PropertyInfo_t * Helpers_GetProperty_m1111878068 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, String_t* ___name1, bool ___nonPublic2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Helpers::ParseEnum(System.Type,System.String)
extern "C"  Il2CppObject * Helpers_ParseEnum_m2958120959 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo[] ProtoBuf.Helpers::GetInstanceFieldsAndProperties(System.Type,System.Boolean)
extern "C"  MemberInfoU5BU5D_t674955999* Helpers_GetInstanceFieldsAndProperties_m2786703940 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, bool ___publicOnly1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Helpers::GetMemberType(System.Reflection.MemberInfo)
extern "C"  Type_t * Helpers_GetMemberType_m175806043 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Helpers::IsAssignableFrom(System.Type,System.Type)
extern "C"  bool Helpers_IsAssignableFrom_m486781200 (Il2CppObject * __this /* static, unused */, Type_t * ___target0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
