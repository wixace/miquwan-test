﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Core.RpsResult>
struct DefaultComparer_t3282807897;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Core_RpsResult479594824.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Core.RpsResult>::.ctor()
extern "C"  void DefaultComparer__ctor_m1456593449_gshared (DefaultComparer_t3282807897 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1456593449(__this, method) ((  void (*) (DefaultComparer_t3282807897 *, const MethodInfo*))DefaultComparer__ctor_m1456593449_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Core.RpsResult>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3864333550_gshared (DefaultComparer_t3282807897 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m3864333550(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t3282807897 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m3864333550_gshared)(__this, ___x0, ___y1, method)
