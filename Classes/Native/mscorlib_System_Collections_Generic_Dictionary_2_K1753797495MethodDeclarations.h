﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<FLOAT_TEXT_ID,System.Object>
struct KeyCollection_t1753797495;
// System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>
struct Dictionary_2_t127038044;
// System.Collections.Generic.IEnumerator`1<FLOAT_TEXT_ID>
struct IEnumerator_1_t4111210819;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// FLOAT_TEXT_ID[]
struct FLOAT_TEXT_IDU5BU5D_t3484773551;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke741974098.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<FLOAT_TEXT_ID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m1918685332_gshared (KeyCollection_t1753797495 * __this, Dictionary_2_t127038044 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m1918685332(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1753797495 *, Dictionary_2_t127038044 *, const MethodInfo*))KeyCollection__ctor_m1918685332_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<FLOAT_TEXT_ID,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2584050754_gshared (KeyCollection_t1753797495 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2584050754(__this, ___item0, method) ((  void (*) (KeyCollection_t1753797495 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2584050754_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<FLOAT_TEXT_ID,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3131165241_gshared (KeyCollection_t1753797495 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3131165241(__this, method) ((  void (*) (KeyCollection_t1753797495 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3131165241_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<FLOAT_TEXT_ID,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m888753160_gshared (KeyCollection_t1753797495 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m888753160(__this, ___item0, method) ((  bool (*) (KeyCollection_t1753797495 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m888753160_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<FLOAT_TEXT_ID,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1605967725_gshared (KeyCollection_t1753797495 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1605967725(__this, ___item0, method) ((  bool (*) (KeyCollection_t1753797495 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1605967725_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<FLOAT_TEXT_ID,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3479802613_gshared (KeyCollection_t1753797495 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3479802613(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1753797495 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3479802613_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<FLOAT_TEXT_ID,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m265363755_gshared (KeyCollection_t1753797495 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m265363755(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1753797495 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m265363755_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<FLOAT_TEXT_ID,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3286455654_gshared (KeyCollection_t1753797495 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3286455654(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1753797495 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3286455654_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<FLOAT_TEXT_ID,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2274353513_gshared (KeyCollection_t1753797495 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2274353513(__this, method) ((  bool (*) (KeyCollection_t1753797495 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2274353513_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<FLOAT_TEXT_ID,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2477779291_gshared (KeyCollection_t1753797495 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2477779291(__this, method) ((  bool (*) (KeyCollection_t1753797495 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2477779291_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<FLOAT_TEXT_ID,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m395998279_gshared (KeyCollection_t1753797495 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m395998279(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1753797495 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m395998279_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<FLOAT_TEXT_ID,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2953938761_gshared (KeyCollection_t1753797495 * __this, FLOAT_TEXT_IDU5BU5D_t3484773551* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2953938761(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1753797495 *, FLOAT_TEXT_IDU5BU5D_t3484773551*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2953938761_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<FLOAT_TEXT_ID,System.Object>::GetEnumerator()
extern "C"  Enumerator_t741974098  KeyCollection_GetEnumerator_m3548158636_gshared (KeyCollection_t1753797495 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m3548158636(__this, method) ((  Enumerator_t741974098  (*) (KeyCollection_t1753797495 *, const MethodInfo*))KeyCollection_GetEnumerator_m3548158636_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<FLOAT_TEXT_ID,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2159420961_gshared (KeyCollection_t1753797495 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m2159420961(__this, method) ((  int32_t (*) (KeyCollection_t1753797495 *, const MethodInfo*))KeyCollection_get_Count_m2159420961_gshared)(__this, method)
