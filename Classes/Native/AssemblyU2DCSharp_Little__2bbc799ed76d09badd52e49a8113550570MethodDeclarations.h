﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._2bbc799ed76d09badd52e49a809df426
struct _2bbc799ed76d09badd52e49a809df426_t113550570;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__2bbc799ed76d09badd52e49a8113550570.h"

// System.Void Little._2bbc799ed76d09badd52e49a809df426::.ctor()
extern "C"  void _2bbc799ed76d09badd52e49a809df426__ctor_m1763207299 (_2bbc799ed76d09badd52e49a809df426_t113550570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._2bbc799ed76d09badd52e49a809df426::_2bbc799ed76d09badd52e49a809df426m2(System.Int32)
extern "C"  int32_t _2bbc799ed76d09badd52e49a809df426__2bbc799ed76d09badd52e49a809df426m2_m1247713625 (_2bbc799ed76d09badd52e49a809df426_t113550570 * __this, int32_t ____2bbc799ed76d09badd52e49a809df426a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._2bbc799ed76d09badd52e49a809df426::_2bbc799ed76d09badd52e49a809df426m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _2bbc799ed76d09badd52e49a809df426__2bbc799ed76d09badd52e49a809df426m_m1184515453 (_2bbc799ed76d09badd52e49a809df426_t113550570 * __this, int32_t ____2bbc799ed76d09badd52e49a809df426a0, int32_t ____2bbc799ed76d09badd52e49a809df426721, int32_t ____2bbc799ed76d09badd52e49a809df426c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._2bbc799ed76d09badd52e49a809df426::ilo__2bbc799ed76d09badd52e49a809df426m21(Little._2bbc799ed76d09badd52e49a809df426,System.Int32)
extern "C"  int32_t _2bbc799ed76d09badd52e49a809df426_ilo__2bbc799ed76d09badd52e49a809df426m21_m2342888945 (Il2CppObject * __this /* static, unused */, _2bbc799ed76d09badd52e49a809df426_t113550570 * ____this0, int32_t ____2bbc799ed76d09badd52e49a809df426a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
