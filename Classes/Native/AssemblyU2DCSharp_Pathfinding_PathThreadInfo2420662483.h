﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.PathHandler
struct PathHandler_t918952263;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.PathThreadInfo
struct  PathThreadInfo_t2420662483 
{
public:
	// System.Int32 Pathfinding.PathThreadInfo::threadIndex
	int32_t ___threadIndex_0;
	// AstarPath Pathfinding.PathThreadInfo::astar
	AstarPath_t4090270936 * ___astar_1;
	// Pathfinding.PathHandler Pathfinding.PathThreadInfo::runData
	PathHandler_t918952263 * ___runData_2;
	// System.Object Pathfinding.PathThreadInfo::_lock
	Il2CppObject * ____lock_3;

public:
	inline static int32_t get_offset_of_threadIndex_0() { return static_cast<int32_t>(offsetof(PathThreadInfo_t2420662483, ___threadIndex_0)); }
	inline int32_t get_threadIndex_0() const { return ___threadIndex_0; }
	inline int32_t* get_address_of_threadIndex_0() { return &___threadIndex_0; }
	inline void set_threadIndex_0(int32_t value)
	{
		___threadIndex_0 = value;
	}

	inline static int32_t get_offset_of_astar_1() { return static_cast<int32_t>(offsetof(PathThreadInfo_t2420662483, ___astar_1)); }
	inline AstarPath_t4090270936 * get_astar_1() const { return ___astar_1; }
	inline AstarPath_t4090270936 ** get_address_of_astar_1() { return &___astar_1; }
	inline void set_astar_1(AstarPath_t4090270936 * value)
	{
		___astar_1 = value;
		Il2CppCodeGenWriteBarrier(&___astar_1, value);
	}

	inline static int32_t get_offset_of_runData_2() { return static_cast<int32_t>(offsetof(PathThreadInfo_t2420662483, ___runData_2)); }
	inline PathHandler_t918952263 * get_runData_2() const { return ___runData_2; }
	inline PathHandler_t918952263 ** get_address_of_runData_2() { return &___runData_2; }
	inline void set_runData_2(PathHandler_t918952263 * value)
	{
		___runData_2 = value;
		Il2CppCodeGenWriteBarrier(&___runData_2, value);
	}

	inline static int32_t get_offset_of__lock_3() { return static_cast<int32_t>(offsetof(PathThreadInfo_t2420662483, ____lock_3)); }
	inline Il2CppObject * get__lock_3() const { return ____lock_3; }
	inline Il2CppObject ** get_address_of__lock_3() { return &____lock_3; }
	inline void set__lock_3(Il2CppObject * value)
	{
		____lock_3 = value;
		Il2CppCodeGenWriteBarrier(&____lock_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Pathfinding.PathThreadInfo
struct PathThreadInfo_t2420662483_marshaled_pinvoke
{
	int32_t ___threadIndex_0;
	AstarPath_t4090270936 * ___astar_1;
	PathHandler_t918952263 * ___runData_2;
	Il2CppIUnknown* ____lock_3;
};
// Native definition for marshalling of: Pathfinding.PathThreadInfo
struct PathThreadInfo_t2420662483_marshaled_com
{
	int32_t ___threadIndex_0;
	AstarPath_t4090270936 * ___astar_1;
	PathHandler_t918952263 * ___runData_2;
	Il2CppIUnknown* ____lock_3;
};
