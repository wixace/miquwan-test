﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>
struct List_1_t3398117353;
// System.Collections.Generic.IEnumerable`1<Pathfinding.LocalAvoidance/VOLine>
struct IEnumerable_1_t1035877462;
// System.Collections.Generic.IEnumerator`1<Pathfinding.LocalAvoidance/VOLine>
struct IEnumerator_1_t3941796850;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Pathfinding.LocalAvoidance/VOLine>
struct ICollection_1_t2924521788;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>
struct ReadOnlyCollection_1_t3587009337;
// Pathfinding.LocalAvoidance/VOLine[]
struct VOLineU5BU5D_t1958725220;
// System.Predicate`1<Pathfinding.LocalAvoidance/VOLine>
struct Predicate_1_t1640988684;
// System.Collections.Generic.IComparer`1<Pathfinding.LocalAvoidance/VOLine>
struct IComparer_1_t309978547;
// System.Comparison`1<Pathfinding.LocalAvoidance/VOLine>
struct Comparison_1_t746292988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_VOLin2029931801.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3417790123.h"

// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::.ctor()
extern "C"  void List_1__ctor_m2755161336_gshared (List_1_t3398117353 * __this, const MethodInfo* method);
#define List_1__ctor_m2755161336(__this, method) ((  void (*) (List_1_t3398117353 *, const MethodInfo*))List_1__ctor_m2755161336_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m4227733776_gshared (List_1_t3398117353 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m4227733776(__this, ___collection0, method) ((  void (*) (List_1_t3398117353 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m4227733776_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m236787424_gshared (List_1_t3398117353 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m236787424(__this, ___capacity0, method) ((  void (*) (List_1_t3398117353 *, int32_t, const MethodInfo*))List_1__ctor_m236787424_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::.cctor()
extern "C"  void List_1__cctor_m3198274942_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3198274942(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3198274942_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4072455585_gshared (List_1_t3398117353 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4072455585(__this, method) ((  Il2CppObject* (*) (List_1_t3398117353 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4072455585_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m915372053_gshared (List_1_t3398117353 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m915372053(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3398117353 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m915372053_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1144779088_gshared (List_1_t3398117353 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1144779088(__this, method) ((  Il2CppObject * (*) (List_1_t3398117353 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1144779088_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1307908257_gshared (List_1_t3398117353 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1307908257(__this, ___item0, method) ((  int32_t (*) (List_1_t3398117353 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1307908257_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1114307787_gshared (List_1_t3398117353 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1114307787(__this, ___item0, method) ((  bool (*) (List_1_t3398117353 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1114307787_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1439045241_gshared (List_1_t3398117353 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1439045241(__this, ___item0, method) ((  int32_t (*) (List_1_t3398117353 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1439045241_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2756186916_gshared (List_1_t3398117353 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2756186916(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3398117353 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2756186916_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m2427931524_gshared (List_1_t3398117353 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m2427931524(__this, ___item0, method) ((  void (*) (List_1_t3398117353 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m2427931524_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2531633932_gshared (List_1_t3398117353 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2531633932(__this, method) ((  bool (*) (List_1_t3398117353 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2531633932_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m536027825_gshared (List_1_t3398117353 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m536027825(__this, method) ((  bool (*) (List_1_t3398117353 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m536027825_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3919304925_gshared (List_1_t3398117353 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m3919304925(__this, method) ((  Il2CppObject * (*) (List_1_t3398117353 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3919304925_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m947314810_gshared (List_1_t3398117353 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m947314810(__this, method) ((  bool (*) (List_1_t3398117353 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m947314810_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m4225345343_gshared (List_1_t3398117353 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m4225345343(__this, method) ((  bool (*) (List_1_t3398117353 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m4225345343_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m831325988_gshared (List_1_t3398117353 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m831325988(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3398117353 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m831325988_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3609724411_gshared (List_1_t3398117353 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3609724411(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3398117353 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3609724411_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::Add(T)
extern "C"  void List_1_Add_m2449522152_gshared (List_1_t3398117353 * __this, VOLine_t2029931801  ___item0, const MethodInfo* method);
#define List_1_Add_m2449522152(__this, ___item0, method) ((  void (*) (List_1_t3398117353 *, VOLine_t2029931801 , const MethodInfo*))List_1_Add_m2449522152_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3246753995_gshared (List_1_t3398117353 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3246753995(__this, ___newCount0, method) ((  void (*) (List_1_t3398117353 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3246753995_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m3548233276_gshared (List_1_t3398117353 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m3548233276(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3398117353 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m3548233276_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m6812617_gshared (List_1_t3398117353 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m6812617(__this, ___collection0, method) ((  void (*) (List_1_t3398117353 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m6812617_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3562752137_gshared (List_1_t3398117353 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3562752137(__this, ___enumerable0, method) ((  void (*) (List_1_t3398117353 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3562752137_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2551886958_gshared (List_1_t3398117353 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2551886958(__this, ___collection0, method) ((  void (*) (List_1_t3398117353 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2551886958_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3587009337 * List_1_AsReadOnly_m4049989883_gshared (List_1_t3398117353 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m4049989883(__this, method) ((  ReadOnlyCollection_1_t3587009337 * (*) (List_1_t3398117353 *, const MethodInfo*))List_1_AsReadOnly_m4049989883_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m2049450084_gshared (List_1_t3398117353 * __this, VOLine_t2029931801  ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m2049450084(__this, ___item0, method) ((  int32_t (*) (List_1_t3398117353 *, VOLine_t2029931801 , const MethodInfo*))List_1_BinarySearch_m2049450084_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::Clear()
extern "C"  void List_1_Clear_m2789653562_gshared (List_1_t3398117353 * __this, const MethodInfo* method);
#define List_1_Clear_m2789653562(__this, method) ((  void (*) (List_1_t3398117353 *, const MethodInfo*))List_1_Clear_m2789653562_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::Contains(T)
extern "C"  bool List_1_Contains_m242696552_gshared (List_1_t3398117353 * __this, VOLine_t2029931801  ___item0, const MethodInfo* method);
#define List_1_Contains_m242696552(__this, ___item0, method) ((  bool (*) (List_1_t3398117353 *, VOLine_t2029931801 , const MethodInfo*))List_1_Contains_m242696552_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m4125992576_gshared (List_1_t3398117353 * __this, VOLineU5BU5D_t1958725220* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m4125992576(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3398117353 *, VOLineU5BU5D_t1958725220*, int32_t, const MethodInfo*))List_1_CopyTo_m4125992576_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::Find(System.Predicate`1<T>)
extern "C"  VOLine_t2029931801  List_1_Find_m2368136936_gshared (List_1_t3398117353 * __this, Predicate_1_t1640988684 * ___match0, const MethodInfo* method);
#define List_1_Find_m2368136936(__this, ___match0, method) ((  VOLine_t2029931801  (*) (List_1_t3398117353 *, Predicate_1_t1640988684 *, const MethodInfo*))List_1_Find_m2368136936_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m2533869667_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t1640988684 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m2533869667(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1640988684 *, const MethodInfo*))List_1_CheckMatch_m2533869667_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1007584392_gshared (List_1_t3398117353 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t1640988684 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1007584392(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3398117353 *, int32_t, int32_t, Predicate_1_t1640988684 *, const MethodInfo*))List_1_GetIndex_m1007584392_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::GetEnumerator()
extern "C"  Enumerator_t3417790123  List_1_GetEnumerator_m3244147365_gshared (List_1_t3398117353 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3244147365(__this, method) ((  Enumerator_t3417790123  (*) (List_1_t3398117353 *, const MethodInfo*))List_1_GetEnumerator_m3244147365_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m560754180_gshared (List_1_t3398117353 * __this, VOLine_t2029931801  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m560754180(__this, ___item0, method) ((  int32_t (*) (List_1_t3398117353 *, VOLine_t2029931801 , const MethodInfo*))List_1_IndexOf_m560754180_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2809593047_gshared (List_1_t3398117353 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2809593047(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3398117353 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2809593047_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3872359760_gshared (List_1_t3398117353 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3872359760(__this, ___index0, method) ((  void (*) (List_1_t3398117353 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3872359760_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3217251575_gshared (List_1_t3398117353 * __this, int32_t ___index0, VOLine_t2029931801  ___item1, const MethodInfo* method);
#define List_1_Insert_m3217251575(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3398117353 *, int32_t, VOLine_t2029931801 , const MethodInfo*))List_1_Insert_m3217251575_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2778626092_gshared (List_1_t3398117353 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2778626092(__this, ___collection0, method) ((  void (*) (List_1_t3398117353 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2778626092_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::Remove(T)
extern "C"  bool List_1_Remove_m1876769635_gshared (List_1_t3398117353 * __this, VOLine_t2029931801  ___item0, const MethodInfo* method);
#define List_1_Remove_m1876769635(__this, ___item0, method) ((  bool (*) (List_1_t3398117353 *, VOLine_t2029931801 , const MethodInfo*))List_1_Remove_m1876769635_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m3190233799_gshared (List_1_t3398117353 * __this, Predicate_1_t1640988684 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m3190233799(__this, ___match0, method) ((  int32_t (*) (List_1_t3398117353 *, Predicate_1_t1640988684 *, const MethodInfo*))List_1_RemoveAll_m3190233799_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m1091104445_gshared (List_1_t3398117353 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m1091104445(__this, ___index0, method) ((  void (*) (List_1_t3398117353 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1091104445_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m2356261088_gshared (List_1_t3398117353 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m2356261088(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3398117353 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m2356261088_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::Reverse()
extern "C"  void List_1_Reverse_m2460167311_gshared (List_1_t3398117353 * __this, const MethodInfo* method);
#define List_1_Reverse_m2460167311(__this, method) ((  void (*) (List_1_t3398117353 *, const MethodInfo*))List_1_Reverse_m2460167311_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::Sort()
extern "C"  void List_1_Sort_m274134387_gshared (List_1_t3398117353 * __this, const MethodInfo* method);
#define List_1_Sort_m274134387(__this, method) ((  void (*) (List_1_t3398117353 *, const MethodInfo*))List_1_Sort_m274134387_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m2254228945_gshared (List_1_t3398117353 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m2254228945(__this, ___comparer0, method) ((  void (*) (List_1_t3398117353 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m2254228945_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m21961094_gshared (List_1_t3398117353 * __this, Comparison_1_t746292988 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m21961094(__this, ___comparison0, method) ((  void (*) (List_1_t3398117353 *, Comparison_1_t746292988 *, const MethodInfo*))List_1_Sort_m21961094_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::ToArray()
extern "C"  VOLineU5BU5D_t1958725220* List_1_ToArray_m1076268558_gshared (List_1_t3398117353 * __this, const MethodInfo* method);
#define List_1_ToArray_m1076268558(__this, method) ((  VOLineU5BU5D_t1958725220* (*) (List_1_t3398117353 *, const MethodInfo*))List_1_ToArray_m1076268558_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::TrimExcess()
extern "C"  void List_1_TrimExcess_m4196856524_gshared (List_1_t3398117353 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m4196856524(__this, method) ((  void (*) (List_1_t3398117353 *, const MethodInfo*))List_1_TrimExcess_m4196856524_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2044576820_gshared (List_1_t3398117353 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2044576820(__this, method) ((  int32_t (*) (List_1_t3398117353 *, const MethodInfo*))List_1_get_Capacity_m2044576820_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m456942813_gshared (List_1_t3398117353 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m456942813(__this, ___value0, method) ((  void (*) (List_1_t3398117353 *, int32_t, const MethodInfo*))List_1_set_Capacity_m456942813_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::get_Count()
extern "C"  int32_t List_1_get_Count_m480502958_gshared (List_1_t3398117353 * __this, const MethodInfo* method);
#define List_1_get_Count_m480502958(__this, method) ((  int32_t (*) (List_1_t3398117353 *, const MethodInfo*))List_1_get_Count_m480502958_gshared)(__this, method)
// T System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::get_Item(System.Int32)
extern "C"  VOLine_t2029931801  List_1_get_Item_m2196678055_gshared (List_1_t3398117353 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m2196678055(__this, ___index0, method) ((  VOLine_t2029931801  (*) (List_1_t3398117353 *, int32_t, const MethodInfo*))List_1_get_Item_m2196678055_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1211907086_gshared (List_1_t3398117353 * __this, int32_t ___index0, VOLine_t2029931801  ___value1, const MethodInfo* method);
#define List_1_set_Item_m1211907086(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3398117353 *, int32_t, VOLine_t2029931801 , const MethodInfo*))List_1_set_Item_m1211907086_gshared)(__this, ___index0, ___value1, method)
