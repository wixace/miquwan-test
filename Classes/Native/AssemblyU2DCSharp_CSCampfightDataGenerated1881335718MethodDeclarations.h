﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSCampfightDataGenerated
struct CSCampfightDataGenerated_t1881335718;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// CSCampfightData
struct CSCampfightData_t179152873;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_CSCampfightData179152873.h"
#include "mscorlib_System_String7231557.h"

// System.Void CSCampfightDataGenerated::.ctor()
extern "C"  void CSCampfightDataGenerated__ctor_m3289506229 (CSCampfightDataGenerated_t1881335718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSCampfightDataGenerated::CSCampfightData_CSCampfightData1(JSVCall,System.Int32)
extern "C"  bool CSCampfightDataGenerated_CSCampfightData_CSCampfightData1_m3004401681 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCampfightDataGenerated::CSCampfightData_isRealMan(JSVCall)
extern "C"  void CSCampfightDataGenerated_CSCampfightData_isRealMan_m650896984 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCampfightDataGenerated::CSCampfightData_country(JSVCall)
extern "C"  void CSCampfightDataGenerated_CSCampfightData_country_m1832415892 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCampfightDataGenerated::CSCampfightData_enemyCountry(JSVCall)
extern "C"  void CSCampfightDataGenerated_CSCampfightData_enemyCountry_m1940741364 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSCampfightDataGenerated::CSCampfightData_GetAttPlus(JSVCall,System.Int32)
extern "C"  bool CSCampfightDataGenerated_CSCampfightData_GetAttPlus_m4046343342 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSCampfightDataGenerated::CSCampfightData_GetAttPlus_fightfor(JSVCall,System.Int32)
extern "C"  bool CSCampfightDataGenerated_CSCampfightData_GetAttPlus_fightfor_m1416388652 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSCampfightDataGenerated::CSCampfightData_GetEnemyAttPlus(JSVCall,System.Int32)
extern "C"  bool CSCampfightDataGenerated_CSCampfightData_GetEnemyAttPlus_m2811019074 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSCampfightDataGenerated::CSCampfightData_GetEnemyAttPlus_fightfor(JSVCall,System.Int32)
extern "C"  bool CSCampfightDataGenerated_CSCampfightData_GetEnemyAttPlus_fightfor_m3601044504 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSCampfightDataGenerated::CSCampfightData_GetMonsterIds(JSVCall,System.Int32)
extern "C"  bool CSCampfightDataGenerated_CSCampfightData_GetMonsterIds_m1943415245 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSCampfightDataGenerated::CSCampfightData_GetMonsterLevel__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool CSCampfightDataGenerated_CSCampfightData_GetMonsterLevel__Int32__Boolean_m1930754675 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSCampfightDataGenerated::CSCampfightData_LoadData__String(JSVCall,System.Int32)
extern "C"  bool CSCampfightDataGenerated_CSCampfightData_LoadData__String_m3655232458 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSCampfightDataGenerated::CSCampfightData_UnLoadData(JSVCall,System.Int32)
extern "C"  bool CSCampfightDataGenerated_CSCampfightData_UnLoadData_m84603250 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCampfightDataGenerated::__Register()
extern "C"  void CSCampfightDataGenerated___Register_m2876335154 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSCampfightDataGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t CSCampfightDataGenerated_ilo_getObject1_m2327929469 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCampfightDataGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void CSCampfightDataGenerated_ilo_addJSCSRel2_m2902287986 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCampfightDataGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void CSCampfightDataGenerated_ilo_setBooleanS3_m2012785572 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSCampfightDataGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t CSCampfightDataGenerated_ilo_setObject4_m3666623672 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] CSCampfightDataGenerated::ilo_GetMonsterIds5(CSCampfightData)
extern "C"  Int32U5BU5D_t3230847821* CSCampfightDataGenerated_ilo_GetMonsterIds5_m2934421946 (Il2CppObject * __this /* static, unused */, CSCampfightData_t179152873 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCampfightDataGenerated::ilo_setInt326(System.Int32,System.Int32)
extern "C"  void CSCampfightDataGenerated_ilo_setInt326_m3567821212 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCampfightDataGenerated::ilo_setArrayS7(System.Int32,System.Int32,System.Boolean)
extern "C"  void CSCampfightDataGenerated_ilo_setArrayS7_m392764532 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSCampfightDataGenerated::ilo_GetMonsterLevel8(CSCampfightData,System.Int32,System.Boolean)
extern "C"  int32_t CSCampfightDataGenerated_ilo_GetMonsterLevel8_m3118713735 (Il2CppObject * __this /* static, unused */, CSCampfightData_t179152873 * ____this0, int32_t ___id1, bool ___isView2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSCampfightDataGenerated::ilo_getStringS9(System.Int32)
extern "C"  String_t* CSCampfightDataGenerated_ilo_getStringS9_m2511045051 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCampfightDataGenerated::ilo_LoadData10(CSCampfightData,System.String)
extern "C"  void CSCampfightDataGenerated_ilo_LoadData10_m1952661908 (Il2CppObject * __this /* static, unused */, CSCampfightData_t179152873 * ____this0, String_t* ___jsonStr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCampfightDataGenerated::ilo_UnLoadData11(CSCampfightData)
extern "C"  void CSCampfightDataGenerated_ilo_UnLoadData11_m1574486878 (Il2CppObject * __this /* static, unused */, CSCampfightData_t179152873 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
