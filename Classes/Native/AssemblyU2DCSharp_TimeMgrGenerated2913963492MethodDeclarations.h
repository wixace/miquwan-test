﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimeMgrGenerated
struct TimeMgrGenerated_t2913963492;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// TimeMgr
struct TimeMgr_t350708715;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_TimeMgr350708715.h"

// System.Void TimeMgrGenerated::.ctor()
extern "C"  void TimeMgrGenerated__ctor_m876411191 (TimeMgrGenerated_t2913963492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimeMgrGenerated::TimeMgr_TimeMgr1(JSVCall,System.Int32)
extern "C"  bool TimeMgrGenerated_TimeMgr_TimeMgr1_m991024075 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeMgrGenerated::TimeMgr_CAMERAHELPER(JSVCall)
extern "C"  void TimeMgrGenerated_TimeMgr_CAMERAHELPER_m728657139 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeMgrGenerated::TimeMgr_TARGETMOTIONBLUR(JSVCall)
extern "C"  void TimeMgrGenerated_TimeMgr_TARGETMOTIONBLUR_m4196133112 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeMgrGenerated::TimeMgr_BOSSDATA(JSVCall)
extern "C"  void TimeMgrGenerated_TimeMgr_BOSSDATA_m4017938415 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeMgrGenerated::TimeMgr_SHILIFUBENDATA(JSVCall)
extern "C"  void TimeMgrGenerated_TimeMgr_SHILIFUBENDATA_m1460897105 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeMgrGenerated::TimeMgr_GAMESPEED(JSVCall)
extern "C"  void TimeMgrGenerated_TimeMgr_GAMESPEED_m808346769 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeMgrGenerated::TimeMgr_serverTime(JSVCall)
extern "C"  void TimeMgrGenerated_TimeMgr_serverTime_m1050354678 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeMgrGenerated::TimeMgr_serverStartTime(JSVCall)
extern "C"  void TimeMgrGenerated_TimeMgr_serverStartTime_m3913446298 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeMgrGenerated::TimeMgr_mergeServerTime(JSVCall)
extern "C"  void TimeMgrGenerated_TimeMgr_mergeServerTime_m3131459166 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimeMgrGenerated::TimeMgr_AddTimeScale__Single__String(JSVCall,System.Int32)
extern "C"  bool TimeMgrGenerated_TimeMgr_AddTimeScale__Single__String_m3004796474 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimeMgrGenerated::TimeMgr_DelTimeScale__Single__String(JSVCall,System.Int32)
extern "C"  bool TimeMgrGenerated_TimeMgr_DelTimeScale__Single__String_m1442130320 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimeMgrGenerated::TimeMgr_SetServerStarTime__Double(JSVCall,System.Int32)
extern "C"  bool TimeMgrGenerated_TimeMgr_SetServerStarTime__Double_m2174535058 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimeMgrGenerated::TimeMgr_SetServerTime__Double(JSVCall,System.Int32)
extern "C"  bool TimeMgrGenerated_TimeMgr_SetServerTime__Double_m2567616544 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeMgrGenerated::__Register()
extern "C"  void TimeMgrGenerated___Register_m3144855024 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeMgrGenerated::ilo_setStringS1(System.Int32,System.String)
extern "C"  void TimeMgrGenerated_ilo_setStringS1_m2583379072 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double TimeMgrGenerated::ilo_get_serverTime2(TimeMgr)
extern "C"  double TimeMgrGenerated_ilo_get_serverTime2_m420883759 (Il2CppObject * __this /* static, unused */, TimeMgr_t350708715 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeMgrGenerated::ilo_setDouble3(System.Int32,System.Double)
extern "C"  void TimeMgrGenerated_ilo_setDouble3_m2920587149 (Il2CppObject * __this /* static, unused */, int32_t ___e0, double ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeMgrGenerated::ilo_AddTimeScale4(TimeMgr,System.Single,System.String)
extern "C"  void TimeMgrGenerated_ilo_AddTimeScale4_m775717102 (Il2CppObject * __this /* static, unused */, TimeMgr_t350708715 * ____this0, float ___timeScale1, String_t* ___timeKey2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TimeMgrGenerated::ilo_getSingle5(System.Int32)
extern "C"  float TimeMgrGenerated_ilo_getSingle5_m4144378196 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TimeMgrGenerated::ilo_getStringS6(System.Int32)
extern "C"  String_t* TimeMgrGenerated_ilo_getStringS6_m279296506 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double TimeMgrGenerated::ilo_getDouble7(System.Int32)
extern "C"  double TimeMgrGenerated_ilo_getDouble7_m293713028 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeMgrGenerated::ilo_SetServerStarTime8(TimeMgr,System.Double)
extern "C"  void TimeMgrGenerated_ilo_SetServerStarTime8_m4172268463 (Il2CppObject * __this /* static, unused */, TimeMgr_t350708715 * ____this0, double ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
