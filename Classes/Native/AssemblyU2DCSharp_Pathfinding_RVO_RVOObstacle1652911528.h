﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.RVO.Simulator
struct Simulator_t2705969170;
// System.Collections.Generic.List`1<Pathfinding.RVO.ObstacleVertex>
struct List_1_t1243525355;
// System.Collections.Generic.List`1<UnityEngine.Vector3[]>
struct List_1_t1583586163;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_RVOObstacle_Obst2151944869.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_RVOLayer1386980398.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RVO.RVOObstacle
struct  RVOObstacle_t1652911528  : public MonoBehaviour_t667441552
{
public:
	// Pathfinding.RVO.RVOObstacle/ObstacleVertexWinding Pathfinding.RVO.RVOObstacle::obstacleMode
	int32_t ___obstacleMode_2;
	// Pathfinding.RVO.RVOLayer Pathfinding.RVO.RVOObstacle::layer
	int32_t ___layer_3;
	// Pathfinding.RVO.Simulator Pathfinding.RVO.RVOObstacle::sim
	Simulator_t2705969170 * ___sim_4;
	// System.Collections.Generic.List`1<Pathfinding.RVO.ObstacleVertex> Pathfinding.RVO.RVOObstacle::addedObstacles
	List_1_t1243525355 * ___addedObstacles_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3[]> Pathfinding.RVO.RVOObstacle::sourceObstacles
	List_1_t1583586163 * ___sourceObstacles_6;
	// System.Boolean Pathfinding.RVO.RVOObstacle::gizmoDrawing
	bool ___gizmoDrawing_7;
	// System.Collections.Generic.List`1<UnityEngine.Vector3[]> Pathfinding.RVO.RVOObstacle::gizmoVerts
	List_1_t1583586163 * ___gizmoVerts_8;
	// Pathfinding.RVO.RVOObstacle/ObstacleVertexWinding Pathfinding.RVO.RVOObstacle::_obstacleMode
	int32_t ____obstacleMode_9;
	// UnityEngine.Matrix4x4 Pathfinding.RVO.RVOObstacle::prevUpdateMatrix
	Matrix4x4_t1651859333  ___prevUpdateMatrix_10;

public:
	inline static int32_t get_offset_of_obstacleMode_2() { return static_cast<int32_t>(offsetof(RVOObstacle_t1652911528, ___obstacleMode_2)); }
	inline int32_t get_obstacleMode_2() const { return ___obstacleMode_2; }
	inline int32_t* get_address_of_obstacleMode_2() { return &___obstacleMode_2; }
	inline void set_obstacleMode_2(int32_t value)
	{
		___obstacleMode_2 = value;
	}

	inline static int32_t get_offset_of_layer_3() { return static_cast<int32_t>(offsetof(RVOObstacle_t1652911528, ___layer_3)); }
	inline int32_t get_layer_3() const { return ___layer_3; }
	inline int32_t* get_address_of_layer_3() { return &___layer_3; }
	inline void set_layer_3(int32_t value)
	{
		___layer_3 = value;
	}

	inline static int32_t get_offset_of_sim_4() { return static_cast<int32_t>(offsetof(RVOObstacle_t1652911528, ___sim_4)); }
	inline Simulator_t2705969170 * get_sim_4() const { return ___sim_4; }
	inline Simulator_t2705969170 ** get_address_of_sim_4() { return &___sim_4; }
	inline void set_sim_4(Simulator_t2705969170 * value)
	{
		___sim_4 = value;
		Il2CppCodeGenWriteBarrier(&___sim_4, value);
	}

	inline static int32_t get_offset_of_addedObstacles_5() { return static_cast<int32_t>(offsetof(RVOObstacle_t1652911528, ___addedObstacles_5)); }
	inline List_1_t1243525355 * get_addedObstacles_5() const { return ___addedObstacles_5; }
	inline List_1_t1243525355 ** get_address_of_addedObstacles_5() { return &___addedObstacles_5; }
	inline void set_addedObstacles_5(List_1_t1243525355 * value)
	{
		___addedObstacles_5 = value;
		Il2CppCodeGenWriteBarrier(&___addedObstacles_5, value);
	}

	inline static int32_t get_offset_of_sourceObstacles_6() { return static_cast<int32_t>(offsetof(RVOObstacle_t1652911528, ___sourceObstacles_6)); }
	inline List_1_t1583586163 * get_sourceObstacles_6() const { return ___sourceObstacles_6; }
	inline List_1_t1583586163 ** get_address_of_sourceObstacles_6() { return &___sourceObstacles_6; }
	inline void set_sourceObstacles_6(List_1_t1583586163 * value)
	{
		___sourceObstacles_6 = value;
		Il2CppCodeGenWriteBarrier(&___sourceObstacles_6, value);
	}

	inline static int32_t get_offset_of_gizmoDrawing_7() { return static_cast<int32_t>(offsetof(RVOObstacle_t1652911528, ___gizmoDrawing_7)); }
	inline bool get_gizmoDrawing_7() const { return ___gizmoDrawing_7; }
	inline bool* get_address_of_gizmoDrawing_7() { return &___gizmoDrawing_7; }
	inline void set_gizmoDrawing_7(bool value)
	{
		___gizmoDrawing_7 = value;
	}

	inline static int32_t get_offset_of_gizmoVerts_8() { return static_cast<int32_t>(offsetof(RVOObstacle_t1652911528, ___gizmoVerts_8)); }
	inline List_1_t1583586163 * get_gizmoVerts_8() const { return ___gizmoVerts_8; }
	inline List_1_t1583586163 ** get_address_of_gizmoVerts_8() { return &___gizmoVerts_8; }
	inline void set_gizmoVerts_8(List_1_t1583586163 * value)
	{
		___gizmoVerts_8 = value;
		Il2CppCodeGenWriteBarrier(&___gizmoVerts_8, value);
	}

	inline static int32_t get_offset_of__obstacleMode_9() { return static_cast<int32_t>(offsetof(RVOObstacle_t1652911528, ____obstacleMode_9)); }
	inline int32_t get__obstacleMode_9() const { return ____obstacleMode_9; }
	inline int32_t* get_address_of__obstacleMode_9() { return &____obstacleMode_9; }
	inline void set__obstacleMode_9(int32_t value)
	{
		____obstacleMode_9 = value;
	}

	inline static int32_t get_offset_of_prevUpdateMatrix_10() { return static_cast<int32_t>(offsetof(RVOObstacle_t1652911528, ___prevUpdateMatrix_10)); }
	inline Matrix4x4_t1651859333  get_prevUpdateMatrix_10() const { return ___prevUpdateMatrix_10; }
	inline Matrix4x4_t1651859333 * get_address_of_prevUpdateMatrix_10() { return &___prevUpdateMatrix_10; }
	inline void set_prevUpdateMatrix_10(Matrix4x4_t1651859333  value)
	{
		___prevUpdateMatrix_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
