﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_serecherRirsisdou147
struct M_serecherRirsisdou147_t1306728851;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_serecherRirsisdou141306728851.h"

// System.Void GarbageiOS.M_serecherRirsisdou147::.ctor()
extern "C"  void M_serecherRirsisdou147__ctor_m1278920192 (M_serecherRirsisdou147_t1306728851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serecherRirsisdou147::M_draijo0(System.String[],System.Int32)
extern "C"  void M_serecherRirsisdou147_M_draijo0_m2949430360 (M_serecherRirsisdou147_t1306728851 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serecherRirsisdou147::M_lowdel1(System.String[],System.Int32)
extern "C"  void M_serecherRirsisdou147_M_lowdel1_m2624631261 (M_serecherRirsisdou147_t1306728851 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serecherRirsisdou147::M_qalmo2(System.String[],System.Int32)
extern "C"  void M_serecherRirsisdou147_M_qalmo2_m2849481089 (M_serecherRirsisdou147_t1306728851 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serecherRirsisdou147::M_notoHowsiswho3(System.String[],System.Int32)
extern "C"  void M_serecherRirsisdou147_M_notoHowsiswho3_m1046349675 (M_serecherRirsisdou147_t1306728851 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serecherRirsisdou147::M_rawtrouhoo4(System.String[],System.Int32)
extern "C"  void M_serecherRirsisdou147_M_rawtrouhoo4_m3000769787 (M_serecherRirsisdou147_t1306728851 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serecherRirsisdou147::M_karpis5(System.String[],System.Int32)
extern "C"  void M_serecherRirsisdou147_M_karpis5_m216599546 (M_serecherRirsisdou147_t1306728851 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serecherRirsisdou147::ilo_M_draijo01(GarbageiOS.M_serecherRirsisdou147,System.String[],System.Int32)
extern "C"  void M_serecherRirsisdou147_ilo_M_draijo01_m3402197531 (Il2CppObject * __this /* static, unused */, M_serecherRirsisdou147_t1306728851 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_serecherRirsisdou147::ilo_M_qalmo22(GarbageiOS.M_serecherRirsisdou147,System.String[],System.Int32)
extern "C"  void M_serecherRirsisdou147_ilo_M_qalmo22_m3834169357 (Il2CppObject * __this /* static, unused */, M_serecherRirsisdou147_t1306728851 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
