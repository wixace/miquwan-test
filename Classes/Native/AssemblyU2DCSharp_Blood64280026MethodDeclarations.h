﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Blood
struct Blood_t64280026;
// CombatEntity
struct CombatEntity_t684137495;
// UnityEngine.Transform
struct Transform_t1659122786;
// UISprite
struct UISprite_t661437049;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitType643359572.h"
#include "AssemblyU2DCSharp_UISprite661437049.h"
#include "mscorlib_System_String7231557.h"

// System.Void Blood::.ctor()
extern "C"  void Blood__ctor_m1414195409 (Blood_t64280026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blood::.cctor()
extern "C"  void Blood__cctor_m408288508 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blood::Awake()
extern "C"  void Blood_Awake_m1651800628 (Blood_t64280026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blood::setDutyType(CombatEntity)
extern "C"  void Blood_setDutyType_m342840202 (Blood_t64280026 * __this, CombatEntity_t684137495 * ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blood::Update()
extern "C"  void Blood_Update_m2617246812 (Blood_t64280026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blood::set_visible(System.Boolean)
extern "C"  void Blood_set_visible_m9177691 (Blood_t64280026 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blood::SetHpShow(System.Boolean)
extern "C"  void Blood_SetHpShow_m1183928141 (Blood_t64280026 * __this, bool ___isShow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blood::SetHp(System.Single)
extern "C"  void Blood_SetHp_m2765546962 (Blood_t64280026 * __this, float ___ratio0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blood::SetFocus(UnityEngine.Transform)
extern "C"  void Blood_SetFocus_m2225997172 (Blood_t64280026 * __this, Transform_t1659122786 * ___tr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blood::ShowFocus()
extern "C"  void Blood_ShowFocus_m4182146058 (Blood_t64280026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blood::HideFocus()
extern "C"  void Blood_HideFocus_m3673575973 (Blood_t64280026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blood::Clear()
extern "C"  void Blood_Clear_m3115295996 (Blood_t64280026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitType Blood::ilo_get_unitType1(CombatEntity)
extern "C"  int32_t Blood_ilo_get_unitType1_m1213810170 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blood::ilo_set_spriteName2(UISprite,System.String)
extern "C"  void Blood_ilo_set_spriteName2_m3237696580 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
