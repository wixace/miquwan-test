﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>
struct Dictionary_2_t1301017310;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2618340702.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21199798016.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,HatredCtrl/stValue>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1640369951_gshared (Enumerator_t2618340702 * __this, Dictionary_2_t1301017310 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1640369951(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2618340702 *, Dictionary_2_t1301017310 *, const MethodInfo*))Enumerator__ctor_m1640369951_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,HatredCtrl/stValue>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3140493548_gshared (Enumerator_t2618340702 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3140493548(__this, method) ((  Il2CppObject * (*) (Enumerator_t2618340702 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3140493548_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,HatredCtrl/stValue>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2071292982_gshared (Enumerator_t2618340702 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2071292982(__this, method) ((  void (*) (Enumerator_t2618340702 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2071292982_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,HatredCtrl/stValue>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3903294253_gshared (Enumerator_t2618340702 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3903294253(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2618340702 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3903294253_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,HatredCtrl/stValue>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4033215688_gshared (Enumerator_t2618340702 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4033215688(__this, method) ((  Il2CppObject * (*) (Enumerator_t2618340702 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4033215688_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,HatredCtrl/stValue>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2908211546_gshared (Enumerator_t2618340702 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2908211546(__this, method) ((  Il2CppObject * (*) (Enumerator_t2618340702 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2908211546_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,HatredCtrl/stValue>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3843515750_gshared (Enumerator_t2618340702 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3843515750(__this, method) ((  bool (*) (Enumerator_t2618340702 *, const MethodInfo*))Enumerator_MoveNext_m3843515750_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,HatredCtrl/stValue>::get_Current()
extern "C"  KeyValuePair_2_t1199798016  Enumerator_get_Current_m2556378390_gshared (Enumerator_t2618340702 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2556378390(__this, method) ((  KeyValuePair_2_t1199798016  (*) (Enumerator_t2618340702 *, const MethodInfo*))Enumerator_get_Current_m2556378390_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,HatredCtrl/stValue>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m726024431_gshared (Enumerator_t2618340702 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m726024431(__this, method) ((  Il2CppObject * (*) (Enumerator_t2618340702 *, const MethodInfo*))Enumerator_get_CurrentKey_m726024431_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,HatredCtrl/stValue>::get_CurrentValue()
extern "C"  stValue_t3425945410  Enumerator_get_CurrentValue_m453661935_gshared (Enumerator_t2618340702 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m453661935(__this, method) ((  stValue_t3425945410  (*) (Enumerator_t2618340702 *, const MethodInfo*))Enumerator_get_CurrentValue_m453661935_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,HatredCtrl/stValue>::Reset()
extern "C"  void Enumerator_Reset_m439249649_gshared (Enumerator_t2618340702 * __this, const MethodInfo* method);
#define Enumerator_Reset_m439249649(__this, method) ((  void (*) (Enumerator_t2618340702 *, const MethodInfo*))Enumerator_Reset_m439249649_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,HatredCtrl/stValue>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2466775610_gshared (Enumerator_t2618340702 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2466775610(__this, method) ((  void (*) (Enumerator_t2618340702 *, const MethodInfo*))Enumerator_VerifyState_m2466775610_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,HatredCtrl/stValue>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m2494285602_gshared (Enumerator_t2618340702 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m2494285602(__this, method) ((  void (*) (Enumerator_t2618340702 *, const MethodInfo*))Enumerator_VerifyCurrent_m2494285602_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,HatredCtrl/stValue>::Dispose()
extern "C"  void Enumerator_Dispose_m3735546753_gshared (Enumerator_t2618340702 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3735546753(__this, method) ((  void (*) (Enumerator_t2618340702 *, const MethodInfo*))Enumerator_Dispose_m3735546753_gshared)(__this, method)
