﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.LzmaBench/CrcOutStream
struct CrcOutStream_t4130964789;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_SeekOrigin4120335598.h"

// System.Void SevenZip.LzmaBench/CrcOutStream::.ctor()
extern "C"  void CrcOutStream__ctor_m2864545238 (CrcOutStream_t4130964789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench/CrcOutStream::Init()
extern "C"  void CrcOutStream_Init_m43941310 (CrcOutStream_t4130964789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.LzmaBench/CrcOutStream::GetDigest()
extern "C"  uint32_t CrcOutStream_GetDigest_m4032904009 (CrcOutStream_t4130964789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.LzmaBench/CrcOutStream::get_CanRead()
extern "C"  bool CrcOutStream_get_CanRead_m369694405 (CrcOutStream_t4130964789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.LzmaBench/CrcOutStream::get_CanSeek()
extern "C"  bool CrcOutStream_get_CanSeek_m398449447 (CrcOutStream_t4130964789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.LzmaBench/CrcOutStream::get_CanWrite()
extern "C"  bool CrcOutStream_get_CanWrite_m3393245490 (CrcOutStream_t4130964789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 SevenZip.LzmaBench/CrcOutStream::get_Length()
extern "C"  int64_t CrcOutStream_get_Length_m1811915246 (CrcOutStream_t4130964789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 SevenZip.LzmaBench/CrcOutStream::get_Position()
extern "C"  int64_t CrcOutStream_get_Position_m2010297777 (CrcOutStream_t4130964789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench/CrcOutStream::set_Position(System.Int64)
extern "C"  void CrcOutStream_set_Position_m4240772454 (CrcOutStream_t4130964789 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench/CrcOutStream::Flush()
extern "C"  void CrcOutStream_Flush_m2948492536 (CrcOutStream_t4130964789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 SevenZip.LzmaBench/CrcOutStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t CrcOutStream_Seek_m1467999542 (CrcOutStream_t4130964789 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench/CrcOutStream::SetLength(System.Int64)
extern "C"  void CrcOutStream_SetLength_m772210734 (CrcOutStream_t4130964789 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SevenZip.LzmaBench/CrcOutStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t CrcOutStream_Read_m3615309875 (CrcOutStream_t4130964789 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench/CrcOutStream::WriteByte(System.Byte)
extern "C"  void CrcOutStream_WriteByte_m1885688656 (CrcOutStream_t4130964789 * __this, uint8_t ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench/CrcOutStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void CrcOutStream_Write_m886340982 (CrcOutStream_t4130964789 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
