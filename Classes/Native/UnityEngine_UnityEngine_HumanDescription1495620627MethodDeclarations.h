﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.HumanDescription
struct HumanDescription_t1495620627;
struct HumanDescription_t1495620627_marshaled_pinvoke;
struct HumanDescription_t1495620627_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_HumanDescription1495620627.h"

// System.Single UnityEngine.HumanDescription::get_upperArmTwist()
extern "C"  float HumanDescription_get_upperArmTwist_m2703372724 (HumanDescription_t1495620627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HumanDescription::set_upperArmTwist(System.Single)
extern "C"  void HumanDescription_set_upperArmTwist_m1608733919 (HumanDescription_t1495620627 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.HumanDescription::get_lowerArmTwist()
extern "C"  float HumanDescription_get_lowerArmTwist_m472045907 (HumanDescription_t1495620627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HumanDescription::set_lowerArmTwist(System.Single)
extern "C"  void HumanDescription_set_lowerArmTwist_m3156887456 (HumanDescription_t1495620627 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.HumanDescription::get_upperLegTwist()
extern "C"  float HumanDescription_get_upperLegTwist_m696868290 (HumanDescription_t1495620627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HumanDescription::set_upperLegTwist(System.Single)
extern "C"  void HumanDescription_set_upperLegTwist_m1873063825 (HumanDescription_t1495620627 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.HumanDescription::get_lowerLegTwist()
extern "C"  float HumanDescription_get_lowerLegTwist_m2760508769 (HumanDescription_t1495620627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HumanDescription::set_lowerLegTwist(System.Single)
extern "C"  void HumanDescription_set_lowerLegTwist_m3421217362 (HumanDescription_t1495620627 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.HumanDescription::get_armStretch()
extern "C"  float HumanDescription_get_armStretch_m2824038020 (HumanDescription_t1495620627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HumanDescription::set_armStretch(System.Single)
extern "C"  void HumanDescription_set_armStretch_m1003113023 (HumanDescription_t1495620627 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.HumanDescription::get_legStretch()
extern "C"  float HumanDescription_get_legStretch_m3013592850 (HumanDescription_t1495620627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HumanDescription::set_legStretch(System.Single)
extern "C"  void HumanDescription_set_legStretch_m1621082225 (HumanDescription_t1495620627 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.HumanDescription::get_feetSpacing()
extern "C"  float HumanDescription_get_feetSpacing_m2686279420 (HumanDescription_t1495620627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HumanDescription::set_feetSpacing(System.Single)
extern "C"  void HumanDescription_set_feetSpacing_m2025256599 (HumanDescription_t1495620627 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.HumanDescription::get_hasTranslationDoF()
extern "C"  bool HumanDescription_get_hasTranslationDoF_m3711180369 (HumanDescription_t1495620627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HumanDescription::set_hasTranslationDoF(System.Boolean)
extern "C"  void HumanDescription_set_hasTranslationDoF_m2776192186 (HumanDescription_t1495620627 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct HumanDescription_t1495620627;
struct HumanDescription_t1495620627_marshaled_pinvoke;

extern "C" void HumanDescription_t1495620627_marshal_pinvoke(const HumanDescription_t1495620627& unmarshaled, HumanDescription_t1495620627_marshaled_pinvoke& marshaled);
extern "C" void HumanDescription_t1495620627_marshal_pinvoke_back(const HumanDescription_t1495620627_marshaled_pinvoke& marshaled, HumanDescription_t1495620627& unmarshaled);
extern "C" void HumanDescription_t1495620627_marshal_pinvoke_cleanup(HumanDescription_t1495620627_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct HumanDescription_t1495620627;
struct HumanDescription_t1495620627_marshaled_com;

extern "C" void HumanDescription_t1495620627_marshal_com(const HumanDescription_t1495620627& unmarshaled, HumanDescription_t1495620627_marshaled_com& marshaled);
extern "C" void HumanDescription_t1495620627_marshal_com_back(const HumanDescription_t1495620627_marshaled_com& marshaled, HumanDescription_t1495620627& unmarshaled);
extern "C" void HumanDescription_t1495620627_marshal_com_cleanup(HumanDescription_t1495620627_marshaled_com& marshaled);
