﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginR2
struct  PluginR2_t2499992755  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginR2::uid
	String_t* ___uid_2;
	// System.String PluginR2::sign
	String_t* ___sign_3;
	// System.String PluginR2::time
	String_t* ___time_4;
	// System.Boolean PluginR2::isOut
	bool ___isOut_5;

public:
	inline static int32_t get_offset_of_uid_2() { return static_cast<int32_t>(offsetof(PluginR2_t2499992755, ___uid_2)); }
	inline String_t* get_uid_2() const { return ___uid_2; }
	inline String_t** get_address_of_uid_2() { return &___uid_2; }
	inline void set_uid_2(String_t* value)
	{
		___uid_2 = value;
		Il2CppCodeGenWriteBarrier(&___uid_2, value);
	}

	inline static int32_t get_offset_of_sign_3() { return static_cast<int32_t>(offsetof(PluginR2_t2499992755, ___sign_3)); }
	inline String_t* get_sign_3() const { return ___sign_3; }
	inline String_t** get_address_of_sign_3() { return &___sign_3; }
	inline void set_sign_3(String_t* value)
	{
		___sign_3 = value;
		Il2CppCodeGenWriteBarrier(&___sign_3, value);
	}

	inline static int32_t get_offset_of_time_4() { return static_cast<int32_t>(offsetof(PluginR2_t2499992755, ___time_4)); }
	inline String_t* get_time_4() const { return ___time_4; }
	inline String_t** get_address_of_time_4() { return &___time_4; }
	inline void set_time_4(String_t* value)
	{
		___time_4 = value;
		Il2CppCodeGenWriteBarrier(&___time_4, value);
	}

	inline static int32_t get_offset_of_isOut_5() { return static_cast<int32_t>(offsetof(PluginR2_t2499992755, ___isOut_5)); }
	inline bool get_isOut_5() const { return ___isOut_5; }
	inline bool* get_address_of_isOut_5() { return &___isOut_5; }
	inline void set_isOut_5(bool value)
	{
		___isOut_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
