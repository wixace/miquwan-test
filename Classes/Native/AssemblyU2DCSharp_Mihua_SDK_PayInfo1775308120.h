﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mihua.SDK.PayInfo
struct  PayInfo_t1775308120  : public Il2CppObject
{
public:
	// System.String Mihua.SDK.PayInfo::amount
	String_t* ___amount_0;
	// System.String Mihua.SDK.PayInfo::orderId
	String_t* ___orderId_1;
	// System.String Mihua.SDK.PayInfo::productName
	String_t* ___productName_2;
	// System.String Mihua.SDK.PayInfo::productType
	String_t* ___productType_3;
	// System.String Mihua.SDK.PayInfo::roleId
	String_t* ___roleId_4;
	// System.String Mihua.SDK.PayInfo::roleName
	String_t* ___roleName_5;
	// System.String Mihua.SDK.PayInfo::roleLv
	String_t* ___roleLv_6;
	// System.String Mihua.SDK.PayInfo::serverId
	String_t* ___serverId_7;
	// System.String Mihua.SDK.PayInfo::serverName
	String_t* ___serverName_8;
	// System.String Mihua.SDK.PayInfo::tempId
	String_t* ___tempId_9;
	// System.String Mihua.SDK.PayInfo::unionName
	String_t* ___unionName_10;
	// System.String Mihua.SDK.PayInfo::vipLv
	String_t* ___vipLv_11;
	// System.String Mihua.SDK.PayInfo::curGlod
	String_t* ___curGlod_12;
	// System.String Mihua.SDK.PayInfo::openId
	String_t* ___openId_13;
	// System.String Mihua.SDK.PayInfo::gold
	String_t* ___gold_14;
	// System.String Mihua.SDK.PayInfo::swcardName
	String_t* ___swcardName_15;
	// System.String Mihua.SDK.PayInfo::swrewardNum
	String_t* ___swrewardNum_16;
	// System.String Mihua.SDK.PayInfo::creditGoldNum
	String_t* ___creditGoldNum_17;
	// System.String Mihua.SDK.PayInfo::rechargeCount
	String_t* ___rechargeCount_18;

public:
	inline static int32_t get_offset_of_amount_0() { return static_cast<int32_t>(offsetof(PayInfo_t1775308120, ___amount_0)); }
	inline String_t* get_amount_0() const { return ___amount_0; }
	inline String_t** get_address_of_amount_0() { return &___amount_0; }
	inline void set_amount_0(String_t* value)
	{
		___amount_0 = value;
		Il2CppCodeGenWriteBarrier(&___amount_0, value);
	}

	inline static int32_t get_offset_of_orderId_1() { return static_cast<int32_t>(offsetof(PayInfo_t1775308120, ___orderId_1)); }
	inline String_t* get_orderId_1() const { return ___orderId_1; }
	inline String_t** get_address_of_orderId_1() { return &___orderId_1; }
	inline void set_orderId_1(String_t* value)
	{
		___orderId_1 = value;
		Il2CppCodeGenWriteBarrier(&___orderId_1, value);
	}

	inline static int32_t get_offset_of_productName_2() { return static_cast<int32_t>(offsetof(PayInfo_t1775308120, ___productName_2)); }
	inline String_t* get_productName_2() const { return ___productName_2; }
	inline String_t** get_address_of_productName_2() { return &___productName_2; }
	inline void set_productName_2(String_t* value)
	{
		___productName_2 = value;
		Il2CppCodeGenWriteBarrier(&___productName_2, value);
	}

	inline static int32_t get_offset_of_productType_3() { return static_cast<int32_t>(offsetof(PayInfo_t1775308120, ___productType_3)); }
	inline String_t* get_productType_3() const { return ___productType_3; }
	inline String_t** get_address_of_productType_3() { return &___productType_3; }
	inline void set_productType_3(String_t* value)
	{
		___productType_3 = value;
		Il2CppCodeGenWriteBarrier(&___productType_3, value);
	}

	inline static int32_t get_offset_of_roleId_4() { return static_cast<int32_t>(offsetof(PayInfo_t1775308120, ___roleId_4)); }
	inline String_t* get_roleId_4() const { return ___roleId_4; }
	inline String_t** get_address_of_roleId_4() { return &___roleId_4; }
	inline void set_roleId_4(String_t* value)
	{
		___roleId_4 = value;
		Il2CppCodeGenWriteBarrier(&___roleId_4, value);
	}

	inline static int32_t get_offset_of_roleName_5() { return static_cast<int32_t>(offsetof(PayInfo_t1775308120, ___roleName_5)); }
	inline String_t* get_roleName_5() const { return ___roleName_5; }
	inline String_t** get_address_of_roleName_5() { return &___roleName_5; }
	inline void set_roleName_5(String_t* value)
	{
		___roleName_5 = value;
		Il2CppCodeGenWriteBarrier(&___roleName_5, value);
	}

	inline static int32_t get_offset_of_roleLv_6() { return static_cast<int32_t>(offsetof(PayInfo_t1775308120, ___roleLv_6)); }
	inline String_t* get_roleLv_6() const { return ___roleLv_6; }
	inline String_t** get_address_of_roleLv_6() { return &___roleLv_6; }
	inline void set_roleLv_6(String_t* value)
	{
		___roleLv_6 = value;
		Il2CppCodeGenWriteBarrier(&___roleLv_6, value);
	}

	inline static int32_t get_offset_of_serverId_7() { return static_cast<int32_t>(offsetof(PayInfo_t1775308120, ___serverId_7)); }
	inline String_t* get_serverId_7() const { return ___serverId_7; }
	inline String_t** get_address_of_serverId_7() { return &___serverId_7; }
	inline void set_serverId_7(String_t* value)
	{
		___serverId_7 = value;
		Il2CppCodeGenWriteBarrier(&___serverId_7, value);
	}

	inline static int32_t get_offset_of_serverName_8() { return static_cast<int32_t>(offsetof(PayInfo_t1775308120, ___serverName_8)); }
	inline String_t* get_serverName_8() const { return ___serverName_8; }
	inline String_t** get_address_of_serverName_8() { return &___serverName_8; }
	inline void set_serverName_8(String_t* value)
	{
		___serverName_8 = value;
		Il2CppCodeGenWriteBarrier(&___serverName_8, value);
	}

	inline static int32_t get_offset_of_tempId_9() { return static_cast<int32_t>(offsetof(PayInfo_t1775308120, ___tempId_9)); }
	inline String_t* get_tempId_9() const { return ___tempId_9; }
	inline String_t** get_address_of_tempId_9() { return &___tempId_9; }
	inline void set_tempId_9(String_t* value)
	{
		___tempId_9 = value;
		Il2CppCodeGenWriteBarrier(&___tempId_9, value);
	}

	inline static int32_t get_offset_of_unionName_10() { return static_cast<int32_t>(offsetof(PayInfo_t1775308120, ___unionName_10)); }
	inline String_t* get_unionName_10() const { return ___unionName_10; }
	inline String_t** get_address_of_unionName_10() { return &___unionName_10; }
	inline void set_unionName_10(String_t* value)
	{
		___unionName_10 = value;
		Il2CppCodeGenWriteBarrier(&___unionName_10, value);
	}

	inline static int32_t get_offset_of_vipLv_11() { return static_cast<int32_t>(offsetof(PayInfo_t1775308120, ___vipLv_11)); }
	inline String_t* get_vipLv_11() const { return ___vipLv_11; }
	inline String_t** get_address_of_vipLv_11() { return &___vipLv_11; }
	inline void set_vipLv_11(String_t* value)
	{
		___vipLv_11 = value;
		Il2CppCodeGenWriteBarrier(&___vipLv_11, value);
	}

	inline static int32_t get_offset_of_curGlod_12() { return static_cast<int32_t>(offsetof(PayInfo_t1775308120, ___curGlod_12)); }
	inline String_t* get_curGlod_12() const { return ___curGlod_12; }
	inline String_t** get_address_of_curGlod_12() { return &___curGlod_12; }
	inline void set_curGlod_12(String_t* value)
	{
		___curGlod_12 = value;
		Il2CppCodeGenWriteBarrier(&___curGlod_12, value);
	}

	inline static int32_t get_offset_of_openId_13() { return static_cast<int32_t>(offsetof(PayInfo_t1775308120, ___openId_13)); }
	inline String_t* get_openId_13() const { return ___openId_13; }
	inline String_t** get_address_of_openId_13() { return &___openId_13; }
	inline void set_openId_13(String_t* value)
	{
		___openId_13 = value;
		Il2CppCodeGenWriteBarrier(&___openId_13, value);
	}

	inline static int32_t get_offset_of_gold_14() { return static_cast<int32_t>(offsetof(PayInfo_t1775308120, ___gold_14)); }
	inline String_t* get_gold_14() const { return ___gold_14; }
	inline String_t** get_address_of_gold_14() { return &___gold_14; }
	inline void set_gold_14(String_t* value)
	{
		___gold_14 = value;
		Il2CppCodeGenWriteBarrier(&___gold_14, value);
	}

	inline static int32_t get_offset_of_swcardName_15() { return static_cast<int32_t>(offsetof(PayInfo_t1775308120, ___swcardName_15)); }
	inline String_t* get_swcardName_15() const { return ___swcardName_15; }
	inline String_t** get_address_of_swcardName_15() { return &___swcardName_15; }
	inline void set_swcardName_15(String_t* value)
	{
		___swcardName_15 = value;
		Il2CppCodeGenWriteBarrier(&___swcardName_15, value);
	}

	inline static int32_t get_offset_of_swrewardNum_16() { return static_cast<int32_t>(offsetof(PayInfo_t1775308120, ___swrewardNum_16)); }
	inline String_t* get_swrewardNum_16() const { return ___swrewardNum_16; }
	inline String_t** get_address_of_swrewardNum_16() { return &___swrewardNum_16; }
	inline void set_swrewardNum_16(String_t* value)
	{
		___swrewardNum_16 = value;
		Il2CppCodeGenWriteBarrier(&___swrewardNum_16, value);
	}

	inline static int32_t get_offset_of_creditGoldNum_17() { return static_cast<int32_t>(offsetof(PayInfo_t1775308120, ___creditGoldNum_17)); }
	inline String_t* get_creditGoldNum_17() const { return ___creditGoldNum_17; }
	inline String_t** get_address_of_creditGoldNum_17() { return &___creditGoldNum_17; }
	inline void set_creditGoldNum_17(String_t* value)
	{
		___creditGoldNum_17 = value;
		Il2CppCodeGenWriteBarrier(&___creditGoldNum_17, value);
	}

	inline static int32_t get_offset_of_rechargeCount_18() { return static_cast<int32_t>(offsetof(PayInfo_t1775308120, ___rechargeCount_18)); }
	inline String_t* get_rechargeCount_18() const { return ___rechargeCount_18; }
	inline String_t** get_address_of_rechargeCount_18() { return &___rechargeCount_18; }
	inline void set_rechargeCount_18(String_t* value)
	{
		___rechargeCount_18 = value;
		Il2CppCodeGenWriteBarrier(&___rechargeCount_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
