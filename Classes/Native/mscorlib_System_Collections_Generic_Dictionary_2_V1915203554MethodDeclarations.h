﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2868572062MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,Mihua.Utils.WaitForSeconds>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m2957134984(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1915203554 *, Dictionary_2_t3214597841 *, const MethodInfo*))ValueCollection__ctor_m587328820_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,Mihua.Utils.WaitForSeconds>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1350319786(__this, ___item0, method) ((  void (*) (ValueCollection_t1915203554 *, WaitForSeconds_t3217447863 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1412421758_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,Mihua.Utils.WaitForSeconds>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2069893235(__this, method) ((  void (*) (ValueCollection_t1915203554 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2867967047_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,Mihua.Utils.WaitForSeconds>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2561626112(__this, ___item0, method) ((  bool (*) (ValueCollection_t1915203554 *, WaitForSeconds_t3217447863 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1938379368_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,Mihua.Utils.WaitForSeconds>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m656173221(__this, ___item0, method) ((  bool (*) (ValueCollection_t1915203554 *, WaitForSeconds_t3217447863 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2514740493_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,Mihua.Utils.WaitForSeconds>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3856818291(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1915203554 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1129618005_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,Mihua.Utils.WaitForSeconds>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1031732663(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1915203554 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3607423115_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,Mihua.Utils.WaitForSeconds>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4017211398(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1915203554 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2307639110_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,Mihua.Utils.WaitForSeconds>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m836801971(__this, method) ((  bool (*) (ValueCollection_t1915203554 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m213555227_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,Mihua.Utils.WaitForSeconds>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1173065747(__this, method) ((  bool (*) (ValueCollection_t1915203554 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m422047355_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,Mihua.Utils.WaitForSeconds>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2838327173(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1915203554 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1579731495_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,Mihua.Utils.WaitForSeconds>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m556543695(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1915203554 *, WaitForSecondsU5BU5D_t695223182*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2183660667_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,Mihua.Utils.WaitForSeconds>::GetEnumerator()
#define ValueCollection_GetEnumerator_m274108472(__this, method) ((  Enumerator_t1146431249  (*) (ValueCollection_t1915203554 *, const MethodInfo*))ValueCollection_GetEnumerator_m2490352798_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,Mihua.Utils.WaitForSeconds>::get_Count()
#define ValueCollection_get_Count_m1216742605(__this, method) ((  int32_t (*) (ValueCollection_t1915203554 *, const MethodInfo*))ValueCollection_get_Count_m1502902721_gshared)(__this, method)
