﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator29<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator29_t1622812832;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator29<System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29__ctor_m798431143_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1622812832 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29__ctor_m798431143(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t1622812832 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29__ctor_m798431143_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator29<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3093908112_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1622812832 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3093908112(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator29_t1622812832 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3093908112_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator29<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m4215503657_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1622812832 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m4215503657(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator29_t1622812832 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m4215503657_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator29<System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m4037934293_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1622812832 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m4037934293(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator29_t1622812832 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m4037934293_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator29<System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29_Dispose_m2686432804_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1622812832 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_Dispose_m2686432804(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t1622812832 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_Dispose_m2686432804_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator29<System.Object>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29_Reset_m2739831380_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1622812832 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_Reset_m2739831380(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t1622812832 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_Reset_m2739831380_gshared)(__this, method)
