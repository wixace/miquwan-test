﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_CameraGenerated/<Camera_onPreRender_GetDelegate_member1_arg0>c__AnonStoreyDF
struct U3CCamera_onPreRender_GetDelegate_member1_arg0U3Ec__AnonStoreyDF_t2606848577;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"

// System.Void UnityEngine_CameraGenerated/<Camera_onPreRender_GetDelegate_member1_arg0>c__AnonStoreyDF::.ctor()
extern "C"  void U3CCamera_onPreRender_GetDelegate_member1_arg0U3Ec__AnonStoreyDF__ctor_m2318298490 (U3CCamera_onPreRender_GetDelegate_member1_arg0U3Ec__AnonStoreyDF_t2606848577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated/<Camera_onPreRender_GetDelegate_member1_arg0>c__AnonStoreyDF::<>m__192(UnityEngine.Camera)
extern "C"  void U3CCamera_onPreRender_GetDelegate_member1_arg0U3Ec__AnonStoreyDF_U3CU3Em__192_m2243660141 (U3CCamera_onPreRender_GetDelegate_member1_arg0U3Ec__AnonStoreyDF_t2606848577 * __this, Camera_t2727095145 * ___cam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
