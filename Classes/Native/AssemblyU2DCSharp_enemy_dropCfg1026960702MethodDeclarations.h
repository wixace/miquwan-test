﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// enemy_dropCfg
struct enemy_dropCfg_t1026960702;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void enemy_dropCfg::.ctor()
extern "C"  void enemy_dropCfg__ctor_m955267565 (enemy_dropCfg_t1026960702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemy_dropCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void enemy_dropCfg_Init_m1611707000 (enemy_dropCfg_t1026960702 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
