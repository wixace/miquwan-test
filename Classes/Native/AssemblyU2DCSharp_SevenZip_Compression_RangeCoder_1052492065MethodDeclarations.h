﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Compression.RangeCoder.Encoder
struct Encoder_t2248006694;
// SevenZip.Compression.RangeCoder.BitEncoder
struct BitEncoder_t1052492065;
struct BitEncoder_t1052492065_marshaled_pinvoke;
struct BitEncoder_t1052492065_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_1052492065.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_2248006694.h"

// System.Void SevenZip.Compression.RangeCoder.BitEncoder::.cctor()
extern "C"  void BitEncoder__cctor_m2608872613 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.BitEncoder::Init()
extern "C"  void BitEncoder_Init_m794975564 (BitEncoder_t1052492065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.BitEncoder::UpdateModel(System.UInt32)
extern "C"  void BitEncoder_UpdateModel_m1491189108 (BitEncoder_t1052492065 * __this, uint32_t ___symbol0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.RangeCoder.BitEncoder::Encode(SevenZip.Compression.RangeCoder.Encoder,System.UInt32)
extern "C"  void BitEncoder_Encode_m2733039708 (BitEncoder_t1052492065 * __this, Encoder_t2248006694 * ___encoder0, uint32_t ___symbol1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.RangeCoder.BitEncoder::GetPrice(System.UInt32)
extern "C"  uint32_t BitEncoder_GetPrice_m500833744 (BitEncoder_t1052492065 * __this, uint32_t ___symbol0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.RangeCoder.BitEncoder::GetPrice0()
extern "C"  uint32_t BitEncoder_GetPrice0_m2347555816 (BitEncoder_t1052492065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.RangeCoder.BitEncoder::GetPrice1()
extern "C"  uint32_t BitEncoder_GetPrice1_m2347556777 (BitEncoder_t1052492065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct BitEncoder_t1052492065;
struct BitEncoder_t1052492065_marshaled_pinvoke;

extern "C" void BitEncoder_t1052492065_marshal_pinvoke(const BitEncoder_t1052492065& unmarshaled, BitEncoder_t1052492065_marshaled_pinvoke& marshaled);
extern "C" void BitEncoder_t1052492065_marshal_pinvoke_back(const BitEncoder_t1052492065_marshaled_pinvoke& marshaled, BitEncoder_t1052492065& unmarshaled);
extern "C" void BitEncoder_t1052492065_marshal_pinvoke_cleanup(BitEncoder_t1052492065_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct BitEncoder_t1052492065;
struct BitEncoder_t1052492065_marshaled_com;

extern "C" void BitEncoder_t1052492065_marshal_com(const BitEncoder_t1052492065& unmarshaled, BitEncoder_t1052492065_marshaled_com& marshaled);
extern "C" void BitEncoder_t1052492065_marshal_com_back(const BitEncoder_t1052492065_marshaled_com& marshaled, BitEncoder_t1052492065& unmarshaled);
extern "C" void BitEncoder_t1052492065_marshal_com_cleanup(BitEncoder_t1052492065_marshaled_com& marshaled);
