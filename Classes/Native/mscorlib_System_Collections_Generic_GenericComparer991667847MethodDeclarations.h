﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<Pathfinding.LocalAvoidance/IntersectionPair>
struct GenericComparer_1_t991667847;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_Inter2821319919.h"

// System.Void System.Collections.Generic.GenericComparer`1<Pathfinding.LocalAvoidance/IntersectionPair>::.ctor()
extern "C"  void GenericComparer_1__ctor_m4209416431_gshared (GenericComparer_1_t991667847 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m4209416431(__this, method) ((  void (*) (GenericComparer_1_t991667847 *, const MethodInfo*))GenericComparer_1__ctor_m4209416431_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<Pathfinding.LocalAvoidance/IntersectionPair>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m1190100968_gshared (GenericComparer_1_t991667847 * __this, IntersectionPair_t2821319919  ___x0, IntersectionPair_t2821319919  ___y1, const MethodInfo* method);
#define GenericComparer_1_Compare_m1190100968(__this, ___x0, ___y1, method) ((  int32_t (*) (GenericComparer_1_t991667847 *, IntersectionPair_t2821319919 , IntersectionPair_t2821319919 , const MethodInfo*))GenericComparer_1_Compare_m1190100968_gshared)(__this, ___x0, ___y1, method)
