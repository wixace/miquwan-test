﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIDragCameraGenerated
struct UIDragCameraGenerated_t3468957922;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UIDragCameraGenerated::.ctor()
extern "C"  void UIDragCameraGenerated__ctor_m1553186505 (UIDragCameraGenerated_t3468957922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDragCameraGenerated::UIDragCamera_UIDragCamera1(JSVCall,System.Int32)
extern "C"  bool UIDragCameraGenerated_UIDragCamera_UIDragCamera1_m593976105 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragCameraGenerated::UIDragCamera_draggableCamera(JSVCall)
extern "C"  void UIDragCameraGenerated_UIDragCamera_draggableCamera_m4190312460 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragCameraGenerated::__Register()
extern "C"  void UIDragCameraGenerated___Register_m2092077982 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIDragCameraGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UIDragCameraGenerated_ilo_getObject1_m3509714317 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
