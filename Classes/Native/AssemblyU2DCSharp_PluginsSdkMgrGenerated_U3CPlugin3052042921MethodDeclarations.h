﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginsSdkMgrGenerated/<PluginsSdkMgr_ShowDialog_GetDelegate_member16_arg6>c__AnonStorey76
struct U3CPluginsSdkMgr_ShowDialog_GetDelegate_member16_arg6U3Ec__AnonStorey76_t3052042921;

#include "codegen/il2cpp-codegen.h"

// System.Void PluginsSdkMgrGenerated/<PluginsSdkMgr_ShowDialog_GetDelegate_member16_arg6>c__AnonStorey76::.ctor()
extern "C"  void U3CPluginsSdkMgr_ShowDialog_GetDelegate_member16_arg6U3Ec__AnonStorey76__ctor_m1749425170 (U3CPluginsSdkMgr_ShowDialog_GetDelegate_member16_arg6U3Ec__AnonStorey76_t3052042921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsSdkMgrGenerated/<PluginsSdkMgr_ShowDialog_GetDelegate_member16_arg6>c__AnonStorey76::<>m__8F()
extern "C"  void U3CPluginsSdkMgr_ShowDialog_GetDelegate_member16_arg6U3Ec__AnonStorey76_U3CU3Em__8F_m4173645289 (U3CPluginsSdkMgr_ShowDialog_GetDelegate_member16_arg6U3Ec__AnonStorey76_t3052042921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
