﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform[]
struct TransformU5BU5D_t3792884695;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WaypointCam
struct  WaypointCam_t1504517102  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Color WaypointCam::WaypointsColor
	Color_t4194546905  ___WaypointsColor_2;
	// System.Boolean WaypointCam::draw
	bool ___draw_3;

public:
	inline static int32_t get_offset_of_WaypointsColor_2() { return static_cast<int32_t>(offsetof(WaypointCam_t1504517102, ___WaypointsColor_2)); }
	inline Color_t4194546905  get_WaypointsColor_2() const { return ___WaypointsColor_2; }
	inline Color_t4194546905 * get_address_of_WaypointsColor_2() { return &___WaypointsColor_2; }
	inline void set_WaypointsColor_2(Color_t4194546905  value)
	{
		___WaypointsColor_2 = value;
	}

	inline static int32_t get_offset_of_draw_3() { return static_cast<int32_t>(offsetof(WaypointCam_t1504517102, ___draw_3)); }
	inline bool get_draw_3() const { return ___draw_3; }
	inline bool* get_address_of_draw_3() { return &___draw_3; }
	inline void set_draw_3(bool value)
	{
		___draw_3 = value;
	}
};

struct WaypointCam_t1504517102_StaticFields
{
public:
	// UnityEngine.Transform[] WaypointCam::waypoints
	TransformU5BU5D_t3792884695* ___waypoints_4;

public:
	inline static int32_t get_offset_of_waypoints_4() { return static_cast<int32_t>(offsetof(WaypointCam_t1504517102_StaticFields, ___waypoints_4)); }
	inline TransformU5BU5D_t3792884695* get_waypoints_4() const { return ___waypoints_4; }
	inline TransformU5BU5D_t3792884695** get_address_of_waypoints_4() { return &___waypoints_4; }
	inline void set_waypoints_4(TransformU5BU5D_t3792884695* value)
	{
		___waypoints_4 = value;
		Il2CppCodeGenWriteBarrier(&___waypoints_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
