﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<Mihua.Asset.ABLoadOperation.AssetOperationPreload1>
struct List_1_t3758029909;

#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_ABOp1894014760.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mihua.Asset.ABLoadOperation.AssetOperationPreload
struct  AssetOperationPreload_t1774208244  : public ABOperation_t1894014760
{
public:
	// System.Collections.Generic.List`1<System.String> Mihua.Asset.ABLoadOperation.AssetOperationPreload::preloadList
	List_1_t1375417109 * ___preloadList_2;
	// System.Int32 Mihua.Asset.ABLoadOperation.AssetOperationPreload::index
	int32_t ___index_3;
	// System.Collections.Generic.List`1<System.String> Mihua.Asset.ABLoadOperation.AssetOperationPreload::objPreLoadList
	List_1_t1375417109 * ___objPreLoadList_4;
	// System.Int32 Mihua.Asset.ABLoadOperation.AssetOperationPreload::objIndex
	int32_t ___objIndex_5;
	// System.Collections.Generic.List`1<Mihua.Asset.ABLoadOperation.AssetOperationPreload1> Mihua.Asset.ABLoadOperation.AssetOperationPreload::aoLoadList
	List_1_t3758029909 * ___aoLoadList_6;

public:
	inline static int32_t get_offset_of_preloadList_2() { return static_cast<int32_t>(offsetof(AssetOperationPreload_t1774208244, ___preloadList_2)); }
	inline List_1_t1375417109 * get_preloadList_2() const { return ___preloadList_2; }
	inline List_1_t1375417109 ** get_address_of_preloadList_2() { return &___preloadList_2; }
	inline void set_preloadList_2(List_1_t1375417109 * value)
	{
		___preloadList_2 = value;
		Il2CppCodeGenWriteBarrier(&___preloadList_2, value);
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(AssetOperationPreload_t1774208244, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_objPreLoadList_4() { return static_cast<int32_t>(offsetof(AssetOperationPreload_t1774208244, ___objPreLoadList_4)); }
	inline List_1_t1375417109 * get_objPreLoadList_4() const { return ___objPreLoadList_4; }
	inline List_1_t1375417109 ** get_address_of_objPreLoadList_4() { return &___objPreLoadList_4; }
	inline void set_objPreLoadList_4(List_1_t1375417109 * value)
	{
		___objPreLoadList_4 = value;
		Il2CppCodeGenWriteBarrier(&___objPreLoadList_4, value);
	}

	inline static int32_t get_offset_of_objIndex_5() { return static_cast<int32_t>(offsetof(AssetOperationPreload_t1774208244, ___objIndex_5)); }
	inline int32_t get_objIndex_5() const { return ___objIndex_5; }
	inline int32_t* get_address_of_objIndex_5() { return &___objIndex_5; }
	inline void set_objIndex_5(int32_t value)
	{
		___objIndex_5 = value;
	}

	inline static int32_t get_offset_of_aoLoadList_6() { return static_cast<int32_t>(offsetof(AssetOperationPreload_t1774208244, ___aoLoadList_6)); }
	inline List_1_t3758029909 * get_aoLoadList_6() const { return ___aoLoadList_6; }
	inline List_1_t3758029909 ** get_address_of_aoLoadList_6() { return &___aoLoadList_6; }
	inline void set_aoLoadList_6(List_1_t3758029909 * value)
	{
		___aoLoadList_6 = value;
		Il2CppCodeGenWriteBarrier(&___aoLoadList_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
