﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>
struct DefaultComparer_t1419621458;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_De2971844791.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::.ctor()
extern "C"  void DefaultComparer__ctor_m2745096803_gshared (DefaultComparer_t1419621458 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2745096803(__this, method) ((  void (*) (DefaultComparer_t1419621458 *, const MethodInfo*))DefaultComparer__ctor_m2745096803_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1761873200_gshared (DefaultComparer_t1419621458 * __this, TypeNameKey_t2971844791  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1761873200(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1419621458 *, TypeNameKey_t2971844791 , const MethodInfo*))DefaultComparer_GetHashCode_m1761873200_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m921698296_gshared (DefaultComparer_t1419621458 * __this, TypeNameKey_t2971844791  ___x0, TypeNameKey_t2971844791  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m921698296(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1419621458 *, TypeNameKey_t2971844791 , TypeNameKey_t2971844791 , const MethodInfo*))DefaultComparer_Equals_m921698296_gshared)(__this, ___x0, ___y1, method)
