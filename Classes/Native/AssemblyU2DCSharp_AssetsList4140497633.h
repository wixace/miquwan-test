﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t4054002952;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetsList
struct  AssetsList_t4140497633  : public MonoBehaviour_t667441552
{
public:
	// System.String[] AssetsList::keys
	StringU5BU5D_t4054002952* ___keys_2;
	// System.String[] AssetsList::assets
	StringU5BU5D_t4054002952* ___assets_3;

public:
	inline static int32_t get_offset_of_keys_2() { return static_cast<int32_t>(offsetof(AssetsList_t4140497633, ___keys_2)); }
	inline StringU5BU5D_t4054002952* get_keys_2() const { return ___keys_2; }
	inline StringU5BU5D_t4054002952** get_address_of_keys_2() { return &___keys_2; }
	inline void set_keys_2(StringU5BU5D_t4054002952* value)
	{
		___keys_2 = value;
		Il2CppCodeGenWriteBarrier(&___keys_2, value);
	}

	inline static int32_t get_offset_of_assets_3() { return static_cast<int32_t>(offsetof(AssetsList_t4140497633, ___assets_3)); }
	inline StringU5BU5D_t4054002952* get_assets_3() const { return ___assets_3; }
	inline StringU5BU5D_t4054002952** get_address_of_assets_3() { return &___assets_3; }
	inline void set_assets_3(StringU5BU5D_t4054002952* value)
	{
		___assets_3 = value;
		Il2CppCodeGenWriteBarrier(&___assets_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
