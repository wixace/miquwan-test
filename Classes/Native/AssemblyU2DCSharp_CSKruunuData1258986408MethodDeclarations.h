﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSKruunuData
struct CSKruunuData_t1258986408;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;
// System.Collections.Generic.List`1<CSPlusAtt>
struct List_1_t341533415;
// towerbuffCfg
struct towerbuffCfg_t1755856872;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"

// System.Void CSKruunuData::.ctor()
extern "C"  void CSKruunuData__ctor_m3484283891 (CSKruunuData_t1258986408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSKruunuData::LoadData(System.String)
extern "C"  void CSKruunuData_LoadData_m3095550977 (CSKruunuData_t1258986408 * __this, String_t* ___jsonStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> CSKruunuData::GetBuffAttrDic()
extern "C"  Dictionary_2_t1974256870 * CSKruunuData_GetBuffAttrDic_m1834398970 (CSKruunuData_t1258986408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPlusAtt> CSKruunuData::GetPlusAttList()
extern "C"  List_1_t341533415 * CSKruunuData_GetPlusAttList_m3634898680 (CSKruunuData_t1258986408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// towerbuffCfg CSKruunuData::ilo_GettowerbuffCfg1(CSDatacfgManager,System.Int32)
extern "C"  towerbuffCfg_t1755856872 * CSKruunuData_ilo_GettowerbuffCfg1_m3390303054 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
