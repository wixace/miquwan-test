﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginJingang
struct  PluginJingang_t2495688911  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginJingang::userId
	String_t* ___userId_2;
	// System.String PluginJingang::sessiond
	String_t* ___sessiond_3;
	// System.String[] PluginJingang::sdkInfos
	StringU5BU5D_t4054002952* ___sdkInfos_4;

public:
	inline static int32_t get_offset_of_userId_2() { return static_cast<int32_t>(offsetof(PluginJingang_t2495688911, ___userId_2)); }
	inline String_t* get_userId_2() const { return ___userId_2; }
	inline String_t** get_address_of_userId_2() { return &___userId_2; }
	inline void set_userId_2(String_t* value)
	{
		___userId_2 = value;
		Il2CppCodeGenWriteBarrier(&___userId_2, value);
	}

	inline static int32_t get_offset_of_sessiond_3() { return static_cast<int32_t>(offsetof(PluginJingang_t2495688911, ___sessiond_3)); }
	inline String_t* get_sessiond_3() const { return ___sessiond_3; }
	inline String_t** get_address_of_sessiond_3() { return &___sessiond_3; }
	inline void set_sessiond_3(String_t* value)
	{
		___sessiond_3 = value;
		Il2CppCodeGenWriteBarrier(&___sessiond_3, value);
	}

	inline static int32_t get_offset_of_sdkInfos_4() { return static_cast<int32_t>(offsetof(PluginJingang_t2495688911, ___sdkInfos_4)); }
	inline StringU5BU5D_t4054002952* get_sdkInfos_4() const { return ___sdkInfos_4; }
	inline StringU5BU5D_t4054002952** get_address_of_sdkInfos_4() { return &___sdkInfos_4; }
	inline void set_sdkInfos_4(StringU5BU5D_t4054002952* value)
	{
		___sdkInfos_4 = value;
		Il2CppCodeGenWriteBarrier(&___sdkInfos_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
