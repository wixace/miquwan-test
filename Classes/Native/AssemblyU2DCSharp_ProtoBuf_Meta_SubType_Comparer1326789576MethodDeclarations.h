﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.SubType/Comparer
struct Comparer_t1326789576;
// System.Object
struct Il2CppObject;
// ProtoBuf.Meta.SubType
struct SubType_t3836516844;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_SubType3836516844.h"

// System.Void ProtoBuf.Meta.SubType/Comparer::.ctor()
extern "C"  void Comparer__ctor_m2993629651 (Comparer_t1326789576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.SubType/Comparer::.cctor()
extern "C"  void Comparer__cctor_m2126109754 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.SubType/Comparer::Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_Compare_m1382374426 (Comparer_t1326789576 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.SubType/Comparer::Compare(ProtoBuf.Meta.SubType,ProtoBuf.Meta.SubType)
extern "C"  int32_t Comparer_Compare_m210361710 (Comparer_t1326789576 * __this, SubType_t3836516844 * ___x0, SubType_t3836516844 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
