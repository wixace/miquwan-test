﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// P.Urge.GameController
struct GameController_t2483546214;
// Little._c42809240831d585e809b108e4937c60
struct _c42809240831d585e809b108e4937c60_t2258242993;
// Little._26902b1caef146e088d6e9b526922abe
struct _26902b1caef146e088d6e9b526922abe_t4158580794;
// Little._2b3eb8c74147c8dab99bdc70f4605c6e
struct _2b3eb8c74147c8dab99bdc70f4605c6e_t2138106516;
// Little._87c21dda4e85d3634a40420a7749a883
struct _87c21dda4e85d3634a40420a7749a883_t2060898368;
// Little._ff8ba0cebdfe44bdb5336cb8b08ba789
struct _ff8ba0cebdfe44bdb5336cb8b08ba789_t2917261756;
// Little._467029077e5aaa1e8020d207976ba8d6
struct _467029077e5aaa1e8020d207976ba8d6_t1255820846;
// Little._5c362c6c94dee1a02ffb7bc0c96fa998
struct _5c362c6c94dee1a02ffb7bc0c96fa998_t1139819844;
// Little._e8eb5619f1d235579def6e4059a29389
struct _e8eb5619f1d235579def6e4059a29389_t2754089430;
// Little._b87dcdfdb0b0d74fbc6209bd6bb9c531
struct _b87dcdfdb0b0d74fbc6209bd6bb9c531_t4288317975;
// Little._a0abc00ecd622dca924328774ffa94c9
struct _a0abc00ecd622dca924328774ffa94c9_t3509190516;
// Little._84f295d9e929c55c5983bc4cd9659d44
struct _84f295d9e929c55c5983bc4cd9659d44_t3798592955;
// Little._4be65f8fab0a75ac9a67b61b1cb6edf0
struct _4be65f8fab0a75ac9a67b61b1cb6edf0_t1328082694;
// Little._e89a2ef985bd96a1929091ce28dacb89
struct _e89a2ef985bd96a1929091ce28dacb89_t761373491;
// Little._5a831d133e768c3c948101db67b391b5
struct _5a831d133e768c3c948101db67b391b5_t3525352721;
// Little._8aba5004c924d9f2b8615579bcaf5ac2
struct _8aba5004c924d9f2b8615579bcaf5ac2_t2793217133;
// Little._e350ca12784702d8132d833fbb3fbb8c
struct _e350ca12784702d8132d833fbb3fbb8c_t67521019;
// Little._67e6222770055ea1f4fb5a250b38cd0d
struct _67e6222770055ea1f4fb5a250b38cd0d_t4178649075;
// Little._c5ace683f9eb6b64212742b1a35758f5
struct _c5ace683f9eb6b64212742b1a35758f5_t3018450634;
// Little._e9073d9ccfe55f9d7a7c79199f47cb3b
struct _e9073d9ccfe55f9d7a7c79199f47cb3b_t2273523772;
// Little._b545c3d8fff462e6ab058c3d4ad4d7c8
struct _b545c3d8fff462e6ab058c3d4ad4d7c8_t1229431059;
// Little._fab624ae181b49406953a181a69a3293
struct _fab624ae181b49406953a181a69a3293_t2547807111;
// Little._d6ae9611fd25390a52430f92ed2d41af
struct _d6ae9611fd25390a52430f92ed2d41af_t3788711490;
// Little._986e7d6bf24cf1d1e32d088e197b6fc6
struct _986e7d6bf24cf1d1e32d088e197b6fc6_t2438134174;
// Little._902ed3760130c07565991dd70d32741b
struct _902ed3760130c07565991dd70d32741b_t296926208;
// Little._be828380dd71b902461de44aa6967044
struct _be828380dd71b902461de44aa6967044_t3513928536;
// Little._1936a668fe148e9c23fd62f8dec3fbd0
struct _1936a668fe148e9c23fd62f8dec3fbd0_t2755553210;
// Little._40e018f3508a8f7c4130c7f43f3a2e6e
struct _40e018f3508a8f7c4130c7f43f3a2e6e_t3704539217;
// Little._402f8d6f3c0995c0a79e19bb66784e6d
struct _402f8d6f3c0995c0a79e19bb66784e6d_t4265043569;
// Little._a3f0b96b25ff1f517249ebc5d78cf977
struct _a3f0b96b25ff1f517249ebc5d78cf977_t3576449452;
// Little._301b7f65ab5137f09469f2274d37df38
struct _301b7f65ab5137f09469f2274d37df38_t1148155434;
// Little._63298887f62ffd4ba3e9a1a910ad45b6
struct _63298887f62ffd4ba3e9a1a910ad45b6_t154404785;
// Little._e7a59e7de407be428e469c9b519472a7
struct _e7a59e7de407be428e469c9b519472a7_t2685196953;
// Little._6f321bc077d2b863cf05f25a221a6c71
struct _6f321bc077d2b863cf05f25a221a6c71_t2883371262;
// Little._a74631a3718d786f597a7e924f5b555f
struct _a74631a3718d786f597a7e924f5b555f_t3594651005;
// Little._c1d471834af38ddc3c4fe73c461aabd2
struct _c1d471834af38ddc3c4fe73c461aabd2_t1537084310;
// Little._237aee5a5ee389ca1d9e24ba27e115e7
struct _237aee5a5ee389ca1d9e24ba27e115e7_t559586452;
// Little._0d5c71543f6c7dc9008979d319600e44
struct _0d5c71543f6c7dc9008979d319600e44_t1147517344;
// Little._0dda97714a6d81c868fa83d60ff9ecab
struct _0dda97714a6d81c868fa83d60ff9ecab_t1721758941;
// Little._b58a0be3a356d4d391be03951271308d
struct _b58a0be3a356d4d391be03951271308d_t172209893;
// Little._217181e4e3c8a70d8949eb63f65948eb
struct _217181e4e3c8a70d8949eb63f65948eb_t2112101980;
// Little._24b18d696f860f5cbf1c6268957365e9
struct _24b18d696f860f5cbf1c6268957365e9_t2903922058;
// Little._e4d88929f95c57f0d5ddf5c2e379a42e
struct _e4d88929f95c57f0d5ddf5c2e379a42e_t176483638;
// Little._e61d8ce101f35e44df5f1672613d9d0d
struct _e61d8ce101f35e44df5f1672613d9d0d_t2746409392;
// Little._fae995a476d99337380fd567b994a551
struct _fae995a476d99337380fd567b994a551_t4088872568;
// Little._2b0841d2beaa343275e0f45893f75a17
struct _2b0841d2beaa343275e0f45893f75a17_t213419414;
// Little._c3efc2b5ec1f644352d33fbc5d9b29b9
struct _c3efc2b5ec1f644352d33fbc5d9b29b9_t3041996178;
// Little._a68d852d3d0d498e2cc6c927f5f6f402
struct _a68d852d3d0d498e2cc6c927f5f6f402_t4230635182;
// Little._cf94381410fe54b0cde1308e3b813427
struct _cf94381410fe54b0cde1308e3b813427_t3623958767;
// Little._babdc95f3d0136d9cd86c347ec215bf2
struct _babdc95f3d0136d9cd86c347ec215bf2_t1694397363;
// Little._7a849745227a240bd28845fc59065605
struct _7a849745227a240bd28845fc59065605_t1773294104;
// Little._65e3d7afd786a185d9f4e2a8e4ce5b8a
struct _65e3d7afd786a185d9f4e2a8e4ce5b8a_t2754037130;
// Little._1fb56dd813fe334b6404430513f138bc
struct _1fb56dd813fe334b6404430513f138bc_t4163233611;
// Little._f9f7383e08fc8c6f67a9942e03b92888
struct _f9f7383e08fc8c6f67a9942e03b92888_t1003330555;
// Little._57f8cb4c0c3f73fe08e46335ac989d7d
struct _57f8cb4c0c3f73fe08e46335ac989d7d_t2260031403;
// Little._73c6935296a13f42e8b7499443bbd435
struct _73c6935296a13f42e8b7499443bbd435_t1747641622;
// Little._73db4f97c2fd7e8b51e95f30f3bf841d
struct _73db4f97c2fd7e8b51e95f30f3bf841d_t1627998150;
// Little._6cbb6c301f3237ba4eb5838f24ef1aee
struct _6cbb6c301f3237ba4eb5838f24ef1aee_t3704541607;
// Little._1ea71e0a65a6ebaf7fb7939afd4d412d
struct _1ea71e0a65a6ebaf7fb7939afd4d412d_t3308296833;
// Little._66ce15e203d0f4cc0d078f9eb3293853
struct _66ce15e203d0f4cc0d078f9eb3293853_t3930857753;
// Little._137fdf3de99bcc7b8f09d8650229aef0
struct _137fdf3de99bcc7b8f09d8650229aef0_t875788038;
// Little._e66f75f7f745328d65689c818f77fd2b
struct _e66f75f7f745328d65689c818f77fd2b_t3599083462;
// Little._61ce11f8be181b0cbd58a8e6669fa6be
struct _61ce11f8be181b0cbd58a8e6669fa6be_t1407465448;
// Little._798919d13467fd36a9d43d7c7e0d3f07
struct _798919d13467fd36a9d43d7c7e0d3f07_t1965012085;
// Little._4977984fa092d3ffa2fd97f1e85aa8c2
struct _4977984fa092d3ffa2fd97f1e85aa8c2_t3215607331;
// Little._acd985249d98f31768c2074d66f94909
struct _acd985249d98f31768c2074d66f94909_t3362093557;
// Little._f28cf315aa393e21752336c1d24f0e4f
struct _f28cf315aa393e21752336c1d24f0e4f_t2589423855;
// Little._1519195eb7ca7dc7ac84baf0853b209f
struct _1519195eb7ca7dc7ac84baf0853b209f_t1437772111;
// Little._335d969307e829d08f086d8855ca9f79
struct _335d969307e829d08f086d8855ca9f79_t1777578141;
// Little._d6620188ed1a0893940069833ea5d12e
struct _d6620188ed1a0893940069833ea5d12e_t3986220760;
// Little._b3d8ae55e6cec1f8630ccd39449765bd
struct _b3d8ae55e6cec1f8630ccd39449765bd_t1137912107;
// Little._167c8098c7d6a54a509bf88d4308b1db
struct _167c8098c7d6a54a509bf88d4308b1db_t3666518910;
// Little._358b2ceeb02da25bfb7d5a5003b54f3d
struct _358b2ceeb02da25bfb7d5a5003b54f3d_t2503056765;
// Little._7398858b399286fd156d20c25bce046f
struct _7398858b399286fd156d20c25bce046f_t79527896;
// Little._4a50809cc3a58127f091fba147ca7253
struct _4a50809cc3a58127f091fba147ca7253_t3335674093;
// Little._ee6a626890871938327aea2b1ccc8ec5
struct _ee6a626890871938327aea2b1ccc8ec5_t1940094027;
// Little._105105725aab512a55c3eca9cd73fc9b
struct _105105725aab512a55c3eca9cd73fc9b_t654465045;
// Little._6eea3fb2dfdc9ac8a56559e34cfe9565
struct _6eea3fb2dfdc9ac8a56559e34cfe9565_t3406393299;
// Little._eb9ab911a4c0ae469c89b90a95bc96c6
struct _eb9ab911a4c0ae469c89b90a95bc96c6_t936079547;
// Little._fcafb63bd94da05ea4966ac4d0a99955
struct _fcafb63bd94da05ea4966ac4d0a99955_t1851570322;
// Little._93de4ffbbdc61d548f157798159ab3d5
struct _93de4ffbbdc61d548f157798159ab3d5_t1927963368;
// Little._b3f01fd20587c7a1e7f3949dfc72de43
struct _b3f01fd20587c7a1e7f3949dfc72de43_t2020475196;
// Little._0b1c0065d68848cd685e688804c19acf
struct _0b1c0065d68848cd685e688804c19acf_t660943364;
// Little._31b8973e4e93620bc70503232dab3578
struct _31b8973e4e93620bc70503232dab3578_t3300672265;
// Little._0320aa762d7354d96830f3480b871fcd
struct _0320aa762d7354d96830f3480b871fcd_t3090868466;
// Little._c61155f4a5d0408136604c7f8af73de0
struct _c61155f4a5d0408136604c7f8af73de0_t2604529504;
// Little._b6fa4ce8834e6ca93691732bac8ab05e
struct _b6fa4ce8834e6ca93691732bac8ab05e_t97978827;
// Little._eadb7caa6752c5f0e0f248add71b2f4a
struct _eadb7caa6752c5f0e0f248add71b2f4a_t3055544156;
// Little._9fa0d3aba4ec4a380a50209ee71e7919
struct _9fa0d3aba4ec4a380a50209ee71e7919_t2230771598;
// Little._20b3e13d35f7c11f2d37a34ae55dcdd3
struct _20b3e13d35f7c11f2d37a34ae55dcdd3_t2623339839;
// Little._c7ddaba247e5c2461d57b7dbc6269161
struct _c7ddaba247e5c2461d57b7dbc6269161_t3415036309;
// Little._e3d68d5a9ce77f230dcc97aaf33931a6
struct _e3d68d5a9ce77f230dcc97aaf33931a6_t3540753077;
// Little._5ac663cbef4d858a7eaa7e7e41da552e
struct _5ac663cbef4d858a7eaa7e7e41da552e_t3375610690;
// Little._cc1da90d49f035389ffd86bf263ae690
struct _cc1da90d49f035389ffd86bf263ae690_t3634150005;
// Little._f6198862a0c5d0657419e00bf21db60b
struct _f6198862a0c5d0657419e00bf21db60b_t1098003072;
// Little._9444e5129c9e9532b37d1aa35f7acb12
struct _9444e5129c9e9532b37d1aa35f7acb12_t3452018129;
// Little._1ccc2e8f9a26a2ceca0997b5916a6b94
struct _1ccc2e8f9a26a2ceca0997b5916a6b94_t1222487213;
// Little._32f144836475fa0651f79c1649a2141f
struct _32f144836475fa0651f79c1649a2141f_t2022861989;
// Little._0a62ee45ea1c0830dcf23cc9c34830a8
struct _0a62ee45ea1c0830dcf23cc9c34830a8_t4117012819;
// Little._6a66de2b393decca12c0cc62ed48595c
struct _6a66de2b393decca12c0cc62ed48595c_t3858868583;
// Little._e628ed36c34984f19707b26f0f605a44
struct _e628ed36c34984f19707b26f0f605a44_t715680303;
// Little._f7d06a788686998f13bf3273d9b4bac6
struct _f7d06a788686998f13bf3273d9b4bac6_t621400070;
// Little._0a0c786a833f5bdadc6113e3ed2fc5f8
struct _0a0c786a833f5bdadc6113e3ed2fc5f8_t1051355010;
// Little._a2a7a4df4084131ec373845ea2e38aa9
struct _a2a7a4df4084131ec373845ea2e38aa9_t1922737915;
// Little._8c0a8d06add0702174c1414cf2bba00e
struct _8c0a8d06add0702174c1414cf2bba00e_t2489032853;
// Little._637622168db9c2e40c39460a4b5ac3af
struct _637622168db9c2e40c39460a4b5ac3af_t402205590;
// Little._e6a128008d0f9776c415ac7cba6879cd
struct _e6a128008d0f9776c415ac7cba6879cd_t3939383142;
// Little._2e032e1a76a66392a072885063cada0a
struct _2e032e1a76a66392a072885063cada0a_t920276266;
// Little._23efa7168bc295f944f9775382d3a0b0
struct _23efa7168bc295f944f9775382d3a0b0_t1506263605;
// Little._91a04852e9b0fb377e119d646dbf2486
struct _91a04852e9b0fb377e119d646dbf2486_t3538707980;
// Little._dd0d51d27df06c2a551ec5d6d2b2a7ba
struct _dd0d51d27df06c2a551ec5d6d2b2a7ba_t432501365;
// Little._62a1c1379d2b981c3250a6ea5de6565b
struct _62a1c1379d2b981c3250a6ea5de6565b_t322878076;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__c42809240831d585e809b1082258242993.h"
#include "AssemblyU2DCSharp_Little__26902b1caef146e088d6e9b54158580794.h"
#include "AssemblyU2DCSharp_Little__2b3eb8c74147c8dab99bdc702138106516.h"
#include "AssemblyU2DCSharp_Little__87c21dda4e85d3634a40420a2060898368.h"
#include "AssemblyU2DCSharp_Little__ff8ba0cebdfe44bdb5336cb82917261756.h"
#include "AssemblyU2DCSharp_Little__467029077e5aaa1e8020d2071255820846.h"
#include "AssemblyU2DCSharp_Little__5c362c6c94dee1a02ffb7bc01139819844.h"
#include "AssemblyU2DCSharp_Little__e8eb5619f1d235579def6e402754089430.h"
#include "AssemblyU2DCSharp_Little__b87dcdfdb0b0d74fbc6209bd4288317975.h"
#include "AssemblyU2DCSharp_Little__a0abc00ecd622dca924328773509190516.h"
#include "AssemblyU2DCSharp_Little__84f295d9e929c55c5983bc4c3798592955.h"
#include "AssemblyU2DCSharp_Little__4be65f8fab0a75ac9a67b61b1328082694.h"
#include "AssemblyU2DCSharp_Little__e89a2ef985bd96a1929091ce2761373491.h"
#include "AssemblyU2DCSharp_Little__5a831d133e768c3c948101db3525352721.h"
#include "AssemblyU2DCSharp_Little__8aba5004c924d9f2b86155792793217133.h"
#include "AssemblyU2DCSharp_Little__e350ca12784702d8132d833fbb67521019.h"
#include "AssemblyU2DCSharp_Little__67e6222770055ea1f4fb5a254178649075.h"
#include "AssemblyU2DCSharp_Little__c5ace683f9eb6b64212742b13018450634.h"
#include "AssemblyU2DCSharp_Little__e9073d9ccfe55f9d7a7c79192273523772.h"
#include "AssemblyU2DCSharp_Little__b545c3d8fff462e6ab058c3d1229431059.h"
#include "AssemblyU2DCSharp_Little__fab624ae181b49406953a1812547807111.h"
#include "AssemblyU2DCSharp_Little__d6ae9611fd25390a52430f923788711490.h"
#include "AssemblyU2DCSharp_Little__986e7d6bf24cf1d1e32d088e2438134174.h"
#include "AssemblyU2DCSharp_Little__902ed3760130c07565991dd70296926208.h"
#include "AssemblyU2DCSharp_Little__be828380dd71b902461de44a3513928536.h"
#include "AssemblyU2DCSharp_Little__1936a668fe148e9c23fd62f82755553210.h"
#include "AssemblyU2DCSharp_Little__40e018f3508a8f7c4130c7f43704539217.h"
#include "AssemblyU2DCSharp_Little__402f8d6f3c0995c0a79e19bb4265043569.h"
#include "AssemblyU2DCSharp_Little__a3f0b96b25ff1f517249ebc53576449452.h"
#include "AssemblyU2DCSharp_Little__301b7f65ab5137f09469f2271148155434.h"
#include "AssemblyU2DCSharp_Little__63298887f62ffd4ba3e9a1a91154404785.h"
#include "AssemblyU2DCSharp_Little__e7a59e7de407be428e469c9b2685196953.h"
#include "AssemblyU2DCSharp_Little__6f321bc077d2b863cf05f25a2883371262.h"
#include "AssemblyU2DCSharp_Little__a74631a3718d786f597a7e923594651005.h"
#include "AssemblyU2DCSharp_Little__c1d471834af38ddc3c4fe73c1537084310.h"
#include "AssemblyU2DCSharp_Little__237aee5a5ee389ca1d9e24ba2559586452.h"
#include "AssemblyU2DCSharp_Little__0d5c71543f6c7dc9008979d31147517344.h"
#include "AssemblyU2DCSharp_Little__0dda97714a6d81c868fa83d61721758941.h"
#include "AssemblyU2DCSharp_Little__b58a0be3a356d4d391be03951172209893.h"
#include "AssemblyU2DCSharp_Little__217181e4e3c8a70d8949eb632112101980.h"
#include "AssemblyU2DCSharp_Little__24b18d696f860f5cbf1c62682903922058.h"
#include "AssemblyU2DCSharp_Little__e4d88929f95c57f0d5ddf5c2e176483638.h"
#include "AssemblyU2DCSharp_Little__e61d8ce101f35e44df5f16722746409392.h"
#include "AssemblyU2DCSharp_Little__fae995a476d99337380fd5674088872568.h"
#include "AssemblyU2DCSharp_Little__2b0841d2beaa343275e0f4589213419414.h"
#include "AssemblyU2DCSharp_Little__c3efc2b5ec1f644352d33fbc3041996178.h"
#include "AssemblyU2DCSharp_Little__a68d852d3d0d498e2cc6c9274230635182.h"
#include "AssemblyU2DCSharp_Little__cf94381410fe54b0cde1308e3623958767.h"
#include "AssemblyU2DCSharp_Little__babdc95f3d0136d9cd86c3471694397363.h"
#include "AssemblyU2DCSharp_Little__7a849745227a240bd28845fc1773294104.h"
#include "AssemblyU2DCSharp_Little__65e3d7afd786a185d9f4e2a82754037130.h"
#include "AssemblyU2DCSharp_Little__1fb56dd813fe334b640443054163233611.h"
#include "AssemblyU2DCSharp_Little__f9f7383e08fc8c6f67a9942e1003330555.h"
#include "AssemblyU2DCSharp_Little__57f8cb4c0c3f73fe08e463352260031403.h"
#include "AssemblyU2DCSharp_Little__73c6935296a13f42e8b749941747641622.h"
#include "AssemblyU2DCSharp_Little__73db4f97c2fd7e8b51e95f301627998150.h"
#include "AssemblyU2DCSharp_Little__6cbb6c301f3237ba4eb5838f3704541607.h"
#include "AssemblyU2DCSharp_Little__1ea71e0a65a6ebaf7fb7939a3308296833.h"
#include "AssemblyU2DCSharp_Little__66ce15e203d0f4cc0d078f9e3930857753.h"
#include "AssemblyU2DCSharp_Little__137fdf3de99bcc7b8f09d8650875788038.h"
#include "AssemblyU2DCSharp_Little__e66f75f7f745328d65689c813599083462.h"
#include "AssemblyU2DCSharp_Little__61ce11f8be181b0cbd58a8e61407465448.h"
#include "AssemblyU2DCSharp_Little__798919d13467fd36a9d43d7c1965012085.h"
#include "AssemblyU2DCSharp_Little__4977984fa092d3ffa2fd97f13215607331.h"
#include "AssemblyU2DCSharp_Little__acd985249d98f31768c2074d3362093557.h"
#include "AssemblyU2DCSharp_Little__f28cf315aa393e21752336c12589423855.h"
#include "AssemblyU2DCSharp_Little__1519195eb7ca7dc7ac84baf01437772111.h"
#include "AssemblyU2DCSharp_Little__335d969307e829d08f086d881777578141.h"
#include "AssemblyU2DCSharp_Little__d6620188ed1a0893940069833986220760.h"
#include "AssemblyU2DCSharp_Little__b3d8ae55e6cec1f8630ccd391137912107.h"
#include "AssemblyU2DCSharp_Little__167c8098c7d6a54a509bf88d3666518910.h"
#include "AssemblyU2DCSharp_Little__358b2ceeb02da25bfb7d5a502503056765.h"
#include "AssemblyU2DCSharp_Little__7398858b399286fd156d20c25b79527896.h"
#include "AssemblyU2DCSharp_Little__4a50809cc3a58127f091fba13335674093.h"
#include "AssemblyU2DCSharp_Little__ee6a626890871938327aea2b1940094027.h"
#include "AssemblyU2DCSharp_Little__105105725aab512a55c3eca9c654465045.h"
#include "AssemblyU2DCSharp_Little__6eea3fb2dfdc9ac8a56559e33406393299.h"
#include "AssemblyU2DCSharp_Little__eb9ab911a4c0ae469c89b90a9936079547.h"
#include "AssemblyU2DCSharp_Little__fcafb63bd94da05ea4966ac41851570322.h"
#include "AssemblyU2DCSharp_Little__93de4ffbbdc61d548f1577981927963368.h"
#include "AssemblyU2DCSharp_Little__b3f01fd20587c7a1e7f3949d2020475196.h"
#include "AssemblyU2DCSharp_Little__0b1c0065d68848cd685e68880660943364.h"
#include "AssemblyU2DCSharp_Little__31b8973e4e93620bc70503233300672265.h"
#include "AssemblyU2DCSharp_Little__0320aa762d7354d96830f3483090868466.h"
#include "AssemblyU2DCSharp_Little__c61155f4a5d0408136604c7f2604529504.h"
#include "AssemblyU2DCSharp_Little__b6fa4ce8834e6ca93691732bac97978827.h"
#include "AssemblyU2DCSharp_Little__eadb7caa6752c5f0e0f248ad3055544156.h"
#include "AssemblyU2DCSharp_Little__9fa0d3aba4ec4a380a50209e2230771598.h"
#include "AssemblyU2DCSharp_Little__20b3e13d35f7c11f2d37a34a2623339839.h"
#include "AssemblyU2DCSharp_Little__c7ddaba247e5c2461d57b7db3415036309.h"
#include "AssemblyU2DCSharp_Little__e3d68d5a9ce77f230dcc97aa3540753077.h"
#include "AssemblyU2DCSharp_Little__5ac663cbef4d858a7eaa7e7e3375610690.h"
#include "AssemblyU2DCSharp_Little__cc1da90d49f035389ffd86bf3634150005.h"
#include "AssemblyU2DCSharp_Little__f6198862a0c5d0657419e00b1098003072.h"
#include "AssemblyU2DCSharp_Little__9444e5129c9e9532b37d1aa33452018129.h"
#include "AssemblyU2DCSharp_Little__1ccc2e8f9a26a2ceca0997b51222487213.h"
#include "AssemblyU2DCSharp_Little__32f144836475fa0651f79c162022861989.h"
#include "AssemblyU2DCSharp_Little__0a62ee45ea1c0830dcf23cc94117012819.h"
#include "AssemblyU2DCSharp_Little__6a66de2b393decca12c0cc623858868583.h"
#include "AssemblyU2DCSharp_Little__e628ed36c34984f19707b26f0715680303.h"
#include "AssemblyU2DCSharp_Little__f7d06a788686998f13bf3273d621400070.h"
#include "AssemblyU2DCSharp_Little__0a0c786a833f5bdadc6113e31051355010.h"
#include "AssemblyU2DCSharp_Little__a2a7a4df4084131ec373845e1922737915.h"
#include "AssemblyU2DCSharp_Little__8c0a8d06add0702174c1414c2489032853.h"
#include "AssemblyU2DCSharp_Little__637622168db9c2e40c39460a4402205590.h"
#include "AssemblyU2DCSharp_Little__e6a128008d0f9776c415ac7c3939383142.h"
#include "AssemblyU2DCSharp_Little__2e032e1a76a66392a07288506920276266.h"
#include "AssemblyU2DCSharp_Little__23efa7168bc295f944f977531506263605.h"
#include "AssemblyU2DCSharp_Little__91a04852e9b0fb377e119d643538707980.h"
#include "AssemblyU2DCSharp_Little__dd0d51d27df06c2a551ec5d6d432501365.h"
#include "AssemblyU2DCSharp_Little__62a1c1379d2b981c3250a6ea5322878076.h"

// System.Void P.Urge.GameController::.ctor()
extern "C"  void GameController__ctor_m559683624 (GameController_t2483546214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void P.Urge.GameController::Init()
extern "C"  void GameController_Init_m662327596 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__c42809240831d585e809b108e4937c60m1(Little._c42809240831d585e809b108e4937c60,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__c42809240831d585e809b108e4937c60m1_m3034297946 (Il2CppObject * __this /* static, unused */, _c42809240831d585e809b108e4937c60_t2258242993 * ____this0, int32_t ____c42809240831d585e809b108e4937c60a1, int32_t ____c42809240831d585e809b108e4937c60332, int32_t ____c42809240831d585e809b108e4937c60c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__26902b1caef146e088d6e9b526922abem2(Little._26902b1caef146e088d6e9b526922abe,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__26902b1caef146e088d6e9b526922abem2_m1701405913 (Il2CppObject * __this /* static, unused */, _26902b1caef146e088d6e9b526922abe_t4158580794 * ____this0, int32_t ____26902b1caef146e088d6e9b526922abea1, int32_t ____26902b1caef146e088d6e9b526922abe602, int32_t ____26902b1caef146e088d6e9b526922abec3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__2b3eb8c74147c8dab99bdc70f4605c6em3(Little._2b3eb8c74147c8dab99bdc70f4605c6e,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__2b3eb8c74147c8dab99bdc70f4605c6em3_m2349062840 (Il2CppObject * __this /* static, unused */, _2b3eb8c74147c8dab99bdc70f4605c6e_t2138106516 * ____this0, int32_t ____2b3eb8c74147c8dab99bdc70f4605c6ea1, int32_t ____2b3eb8c74147c8dab99bdc70f4605c6e282, int32_t ____2b3eb8c74147c8dab99bdc70f4605c6ec3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__87c21dda4e85d3634a40420a7749a883m4(Little._87c21dda4e85d3634a40420a7749a883,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__87c21dda4e85d3634a40420a7749a883m4_m2661839191 (Il2CppObject * __this /* static, unused */, _87c21dda4e85d3634a40420a7749a883_t2060898368 * ____this0, int32_t ____87c21dda4e85d3634a40420a7749a883a1, int32_t ____87c21dda4e85d3634a40420a7749a883272, int32_t ____87c21dda4e85d3634a40420a7749a883c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__ff8ba0cebdfe44bdb5336cb8b08ba789m5(Little._ff8ba0cebdfe44bdb5336cb8b08ba789,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__ff8ba0cebdfe44bdb5336cb8b08ba789m5_m1776272886 (Il2CppObject * __this /* static, unused */, _ff8ba0cebdfe44bdb5336cb8b08ba789_t2917261756 * ____this0, int32_t ____ff8ba0cebdfe44bdb5336cb8b08ba789a1, int32_t ____ff8ba0cebdfe44bdb5336cb8b08ba789412, int32_t ____ff8ba0cebdfe44bdb5336cb8b08ba789c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__467029077e5aaa1e8020d207976ba8d6m6(Little._467029077e5aaa1e8020d207976ba8d6,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__467029077e5aaa1e8020d207976ba8d6m6_m714136789 (Il2CppObject * __this /* static, unused */, _467029077e5aaa1e8020d207976ba8d6_t1255820846 * ____this0, int32_t ____467029077e5aaa1e8020d207976ba8d6a1, int32_t ____467029077e5aaa1e8020d207976ba8d6202, int32_t ____467029077e5aaa1e8020d207976ba8d6c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__5c362c6c94dee1a02ffb7bc0c96fa998m7(Little._5c362c6c94dee1a02ffb7bc0c96fa998,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__5c362c6c94dee1a02ffb7bc0c96fa998m7_m828618036 (Il2CppObject * __this /* static, unused */, _5c362c6c94dee1a02ffb7bc0c96fa998_t1139819844 * ____this0, int32_t ____5c362c6c94dee1a02ffb7bc0c96fa998a1, int32_t ____5c362c6c94dee1a02ffb7bc0c96fa998692, int32_t ____5c362c6c94dee1a02ffb7bc0c96fa998c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__e8eb5619f1d235579def6e4059a29389m8(Little._e8eb5619f1d235579def6e4059a29389,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__e8eb5619f1d235579def6e4059a29389m8_m1210710035 (Il2CppObject * __this /* static, unused */, _e8eb5619f1d235579def6e4059a29389_t2754089430 * ____this0, int32_t ____e8eb5619f1d235579def6e4059a29389a1, int32_t ____e8eb5619f1d235579def6e4059a29389972, int32_t ____e8eb5619f1d235579def6e4059a29389c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__b87dcdfdb0b0d74fbc6209bd6bb9c531m9(Little._b87dcdfdb0b0d74fbc6209bd6bb9c531,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__b87dcdfdb0b0d74fbc6209bd6bb9c531m9_m2290238354 (Il2CppObject * __this /* static, unused */, _b87dcdfdb0b0d74fbc6209bd6bb9c531_t4288317975 * ____this0, int32_t ____b87dcdfdb0b0d74fbc6209bd6bb9c531a1, int32_t ____b87dcdfdb0b0d74fbc6209bd6bb9c531222, int32_t ____b87dcdfdb0b0d74fbc6209bd6bb9c531c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__a0abc00ecd622dca924328774ffa94c9m10(Little._a0abc00ecd622dca924328774ffa94c9,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__a0abc00ecd622dca924328774ffa94c9m10_m2182606214 (Il2CppObject * __this /* static, unused */, _a0abc00ecd622dca924328774ffa94c9_t3509190516 * ____this0, int32_t ____a0abc00ecd622dca924328774ffa94c9a1, int32_t ____a0abc00ecd622dca924328774ffa94c9422, int32_t ____a0abc00ecd622dca924328774ffa94c9c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__84f295d9e929c55c5983bc4cd9659d44m11(Little._84f295d9e929c55c5983bc4cd9659d44,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__84f295d9e929c55c5983bc4cd9659d44m11_m2475589555 (Il2CppObject * __this /* static, unused */, _84f295d9e929c55c5983bc4cd9659d44_t3798592955 * ____this0, int32_t ____84f295d9e929c55c5983bc4cd9659d44a1, int32_t ____84f295d9e929c55c5983bc4cd9659d4412, int32_t ____84f295d9e929c55c5983bc4cd9659d44c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__4be65f8fab0a75ac9a67b61b1cb6edf0m12(Little._4be65f8fab0a75ac9a67b61b1cb6edf0,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__4be65f8fab0a75ac9a67b61b1cb6edf0m12_m84351464 (Il2CppObject * __this /* static, unused */, _4be65f8fab0a75ac9a67b61b1cb6edf0_t1328082694 * ____this0, int32_t ____4be65f8fab0a75ac9a67b61b1cb6edf0a1, int32_t ____4be65f8fab0a75ac9a67b61b1cb6edf022, int32_t ____4be65f8fab0a75ac9a67b61b1cb6edf0c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__e89a2ef985bd96a1929091ce28dacb89m13(Little._e89a2ef985bd96a1929091ce28dacb89,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__e89a2ef985bd96a1929091ce28dacb89m13_m874167009 (Il2CppObject * __this /* static, unused */, _e89a2ef985bd96a1929091ce28dacb89_t761373491 * ____this0, int32_t ____e89a2ef985bd96a1929091ce28dacb89a1, int32_t ____e89a2ef985bd96a1929091ce28dacb89172, int32_t ____e89a2ef985bd96a1929091ce28dacb89c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__5a831d133e768c3c948101db67b391b5m14(Little._5a831d133e768c3c948101db67b391b5,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__5a831d133e768c3c948101db67b391b5m14_m227203004 (Il2CppObject * __this /* static, unused */, _5a831d133e768c3c948101db67b391b5_t3525352721 * ____this0, int32_t ____5a831d133e768c3c948101db67b391b5a1, int32_t ____5a831d133e768c3c948101db67b391b5172, int32_t ____5a831d133e768c3c948101db67b391b5c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__8aba5004c924d9f2b8615579bcaf5ac2m15(Little._8aba5004c924d9f2b8615579bcaf5ac2,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__8aba5004c924d9f2b8615579bcaf5ac2m15_m354651795 (Il2CppObject * __this /* static, unused */, _8aba5004c924d9f2b8615579bcaf5ac2_t2793217133 * ____this0, int32_t ____8aba5004c924d9f2b8615579bcaf5ac2a1, int32_t ____8aba5004c924d9f2b8615579bcaf5ac242, int32_t ____8aba5004c924d9f2b8615579bcaf5ac2c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__e350ca12784702d8132d833fbb3fbb8cm16(Little._e350ca12784702d8132d833fbb3fbb8c,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__e350ca12784702d8132d833fbb3fbb8cm16_m3627182798 (Il2CppObject * __this /* static, unused */, _e350ca12784702d8132d833fbb3fbb8c_t67521019 * ____this0, int32_t ____e350ca12784702d8132d833fbb3fbb8ca1, int32_t ____e350ca12784702d8132d833fbb3fbb8c632, int32_t ____e350ca12784702d8132d833fbb3fbb8cc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__67e6222770055ea1f4fb5a250b38cd0dm17(Little._67e6222770055ea1f4fb5a250b38cd0d,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__67e6222770055ea1f4fb5a250b38cd0dm17_m3373251805 (Il2CppObject * __this /* static, unused */, _67e6222770055ea1f4fb5a250b38cd0d_t4178649075 * ____this0, int32_t ____67e6222770055ea1f4fb5a250b38cd0da1, int32_t ____67e6222770055ea1f4fb5a250b38cd0d742, int32_t ____67e6222770055ea1f4fb5a250b38cd0dc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__c5ace683f9eb6b64212742b1a35758f5m18(Little._c5ace683f9eb6b64212742b1a35758f5,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__c5ace683f9eb6b64212742b1a35758f5m18_m1769588778 (Il2CppObject * __this /* static, unused */, _c5ace683f9eb6b64212742b1a35758f5_t3018450634 * ____this0, int32_t ____c5ace683f9eb6b64212742b1a35758f5a1, int32_t ____c5ace683f9eb6b64212742b1a35758f5442, int32_t ____c5ace683f9eb6b64212742b1a35758f5c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__e9073d9ccfe55f9d7a7c79199f47cb3bm19(Little._e9073d9ccfe55f9d7a7c79199f47cb3b,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__e9073d9ccfe55f9d7a7c79199f47cb3bm19_m3870965293 (Il2CppObject * __this /* static, unused */, _e9073d9ccfe55f9d7a7c79199f47cb3b_t2273523772 * ____this0, int32_t ____e9073d9ccfe55f9d7a7c79199f47cb3ba1, int32_t ____e9073d9ccfe55f9d7a7c79199f47cb3b622, int32_t ____e9073d9ccfe55f9d7a7c79199f47cb3bc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__b545c3d8fff462e6ab058c3d4ad4d7c8m20(Little._b545c3d8fff462e6ab058c3d4ad4d7c8,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__b545c3d8fff462e6ab058c3d4ad4d7c8m20_m1602896389 (Il2CppObject * __this /* static, unused */, _b545c3d8fff462e6ab058c3d4ad4d7c8_t1229431059 * ____this0, int32_t ____b545c3d8fff462e6ab058c3d4ad4d7c8a1, int32_t ____b545c3d8fff462e6ab058c3d4ad4d7c8892, int32_t ____b545c3d8fff462e6ab058c3d4ad4d7c8c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__fab624ae181b49406953a181a69a3293m21(Little._fab624ae181b49406953a181a69a3293,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__fab624ae181b49406953a181a69a3293m21_m3123931916 (Il2CppObject * __this /* static, unused */, _fab624ae181b49406953a181a69a3293_t2547807111 * ____this0, int32_t ____fab624ae181b49406953a181a69a3293a1, int32_t ____fab624ae181b49406953a181a69a3293692, int32_t ____fab624ae181b49406953a181a69a3293c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__d6ae9611fd25390a52430f92ed2d41afm22(Little._d6ae9611fd25390a52430f92ed2d41af,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__d6ae9611fd25390a52430f92ed2d41afm22_m1552850977 (Il2CppObject * __this /* static, unused */, _d6ae9611fd25390a52430f92ed2d41af_t3788711490 * ____this0, int32_t ____d6ae9611fd25390a52430f92ed2d41afa1, int32_t ____d6ae9611fd25390a52430f92ed2d41af572, int32_t ____d6ae9611fd25390a52430f92ed2d41afc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__986e7d6bf24cf1d1e32d088e197b6fc6m23(Little._986e7d6bf24cf1d1e32d088e197b6fc6,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__986e7d6bf24cf1d1e32d088e197b6fc6m23_m2516057848 (Il2CppObject * __this /* static, unused */, _986e7d6bf24cf1d1e32d088e197b6fc6_t2438134174 * ____this0, int32_t ____986e7d6bf24cf1d1e32d088e197b6fc6a1, int32_t ____986e7d6bf24cf1d1e32d088e197b6fc6682, int32_t ____986e7d6bf24cf1d1e32d088e197b6fc6c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__902ed3760130c07565991dd70d32741bm24(Little._902ed3760130c07565991dd70d32741b,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__902ed3760130c07565991dd70d32741bm24_m642535131 (Il2CppObject * __this /* static, unused */, _902ed3760130c07565991dd70d32741b_t296926208 * ____this0, int32_t ____902ed3760130c07565991dd70d32741ba1, int32_t ____902ed3760130c07565991dd70d32741b42, int32_t ____902ed3760130c07565991dd70d32741bc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__be828380dd71b902461de44aa6967044m25(Little._be828380dd71b902461de44aa6967044,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__be828380dd71b902461de44aa6967044m25_m897631146 (Il2CppObject * __this /* static, unused */, _be828380dd71b902461de44aa6967044_t3513928536 * ____this0, int32_t ____be828380dd71b902461de44aa6967044a1, int32_t ____be828380dd71b902461de44aa6967044402, int32_t ____be828380dd71b902461de44aa6967044c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__1936a668fe148e9c23fd62f8dec3fbd0m26(Little._1936a668fe148e9c23fd62f8dec3fbd0,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__1936a668fe148e9c23fd62f8dec3fbd0m26_m776652685 (Il2CppObject * __this /* static, unused */, _1936a668fe148e9c23fd62f8dec3fbd0_t2755553210 * ____this0, int32_t ____1936a668fe148e9c23fd62f8dec3fbd0a1, int32_t ____1936a668fe148e9c23fd62f8dec3fbd092, int32_t ____1936a668fe148e9c23fd62f8dec3fbd0c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__40e018f3508a8f7c4130c7f43f3a2e6em27(Little._40e018f3508a8f7c4130c7f43f3a2e6e,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__40e018f3508a8f7c4130c7f43f3a2e6em27_m3191531098 (Il2CppObject * __this /* static, unused */, _40e018f3508a8f7c4130c7f43f3a2e6e_t3704539217 * ____this0, int32_t ____40e018f3508a8f7c4130c7f43f3a2e6ea1, int32_t ____40e018f3508a8f7c4130c7f43f3a2e6e42, int32_t ____40e018f3508a8f7c4130c7f43f3a2e6ec3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__402f8d6f3c0995c0a79e19bb66784e6dm28(Little._402f8d6f3c0995c0a79e19bb66784e6d,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__402f8d6f3c0995c0a79e19bb66784e6dm28_m4218395833 (Il2CppObject * __this /* static, unused */, _402f8d6f3c0995c0a79e19bb66784e6d_t4265043569 * ____this0, int32_t ____402f8d6f3c0995c0a79e19bb66784e6da1, int32_t ____402f8d6f3c0995c0a79e19bb66784e6d842, int32_t ____402f8d6f3c0995c0a79e19bb66784e6dc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__a3f0b96b25ff1f517249ebc5d78cf977m29(Little._a3f0b96b25ff1f517249ebc5d78cf977,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__a3f0b96b25ff1f517249ebc5d78cf977m29_m2846585550 (Il2CppObject * __this /* static, unused */, _a3f0b96b25ff1f517249ebc5d78cf977_t3576449452 * ____this0, int32_t ____a3f0b96b25ff1f517249ebc5d78cf977a1, int32_t ____a3f0b96b25ff1f517249ebc5d78cf97732, int32_t ____a3f0b96b25ff1f517249ebc5d78cf977c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__301b7f65ab5137f09469f2274d37df38m30(Little._301b7f65ab5137f09469f2274d37df38,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__301b7f65ab5137f09469f2274d37df38m30_m3696884084 (Il2CppObject * __this /* static, unused */, _301b7f65ab5137f09469f2274d37df38_t1148155434 * ____this0, int32_t ____301b7f65ab5137f09469f2274d37df38a1, int32_t ____301b7f65ab5137f09469f2274d37df38822, int32_t ____301b7f65ab5137f09469f2274d37df38c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__63298887f62ffd4ba3e9a1a910ad45b6m31(Little._63298887f62ffd4ba3e9a1a910ad45b6,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__63298887f62ffd4ba3e9a1a910ad45b6m31_m521761313 (Il2CppObject * __this /* static, unused */, _63298887f62ffd4ba3e9a1a910ad45b6_t154404785 * ____this0, int32_t ____63298887f62ffd4ba3e9a1a910ad45b6a1, int32_t ____63298887f62ffd4ba3e9a1a910ad45b6862, int32_t ____63298887f62ffd4ba3e9a1a910ad45b6c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__e7a59e7de407be428e469c9b519472a7m32(Little._e7a59e7de407be428e469c9b519472a7,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__e7a59e7de407be428e469c9b519472a7m32_m2617226768 (Il2CppObject * __this /* static, unused */, _e7a59e7de407be428e469c9b519472a7_t2685196953 * ____this0, int32_t ____e7a59e7de407be428e469c9b519472a7a1, int32_t ____e7a59e7de407be428e469c9b519472a7942, int32_t ____e7a59e7de407be428e469c9b519472a7c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__6f321bc077d2b863cf05f25a221a6c71m33(Little._6f321bc077d2b863cf05f25a221a6c71,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__6f321bc077d2b863cf05f25a221a6c71m33_m239867769 (Il2CppObject * __this /* static, unused */, _6f321bc077d2b863cf05f25a221a6c71_t2883371262 * ____this0, int32_t ____6f321bc077d2b863cf05f25a221a6c71a1, int32_t ____6f321bc077d2b863cf05f25a221a6c71462, int32_t ____6f321bc077d2b863cf05f25a221a6c71c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__a74631a3718d786f597a7e924f5b555fm34(Little._a74631a3718d786f597a7e924f5b555f,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__a74631a3718d786f597a7e924f5b555fm34_m378122774 (Il2CppObject * __this /* static, unused */, _a74631a3718d786f597a7e924f5b555f_t3594651005 * ____this0, int32_t ____a74631a3718d786f597a7e924f5b555fa1, int32_t ____a74631a3718d786f597a7e924f5b555f422, int32_t ____a74631a3718d786f597a7e924f5b555fc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__c1d471834af38ddc3c4fe73c461aabd2m35(Little._c1d471834af38ddc3c4fe73c461aabd2,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__c1d471834af38ddc3c4fe73c461aabd2m35_m2604731623 (Il2CppObject * __this /* static, unused */, _c1d471834af38ddc3c4fe73c461aabd2_t1537084310 * ____this0, int32_t ____c1d471834af38ddc3c4fe73c461aabd2a1, int32_t ____c1d471834af38ddc3c4fe73c461aabd2362, int32_t ____c1d471834af38ddc3c4fe73c461aabd2c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__237aee5a5ee389ca1d9e24ba27e115e7m36(Little._237aee5a5ee389ca1d9e24ba27e115e7,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__237aee5a5ee389ca1d9e24ba27e115e7m36_m3971147778 (Il2CppObject * __this /* static, unused */, _237aee5a5ee389ca1d9e24ba27e115e7_t559586452 * ____this0, int32_t ____237aee5a5ee389ca1d9e24ba27e115e7a1, int32_t ____237aee5a5ee389ca1d9e24ba27e115e7342, int32_t ____237aee5a5ee389ca1d9e24ba27e115e7c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__0d5c71543f6c7dc9008979d319600e44m37(Little._0d5c71543f6c7dc9008979d319600e44,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__0d5c71543f6c7dc9008979d319600e44m37_m2463642169 (Il2CppObject * __this /* static, unused */, _0d5c71543f6c7dc9008979d319600e44_t1147517344 * ____this0, int32_t ____0d5c71543f6c7dc9008979d319600e44a1, int32_t ____0d5c71543f6c7dc9008979d319600e44862, int32_t ____0d5c71543f6c7dc9008979d319600e44c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__0dda97714a6d81c868fa83d60ff9ecabm38(Little._0dda97714a6d81c868fa83d60ff9ecab,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__0dda97714a6d81c868fa83d60ff9ecabm38_m1336142162 (Il2CppObject * __this /* static, unused */, _0dda97714a6d81c868fa83d60ff9ecab_t1721758941 * ____this0, int32_t ____0dda97714a6d81c868fa83d60ff9ecaba1, int32_t ____0dda97714a6d81c868fa83d60ff9ecab612, int32_t ____0dda97714a6d81c868fa83d60ff9ecabc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__b58a0be3a356d4d391be03951271308dm39(Little._b58a0be3a356d4d391be03951271308d,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__b58a0be3a356d4d391be03951271308dm39_m252877697 (Il2CppObject * __this /* static, unused */, _b58a0be3a356d4d391be03951271308d_t172209893 * ____this0, int32_t ____b58a0be3a356d4d391be03951271308da1, int32_t ____b58a0be3a356d4d391be03951271308d42, int32_t ____b58a0be3a356d4d391be03951271308dc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__217181e4e3c8a70d8949eb63f65948ebm40(Little._217181e4e3c8a70d8949eb63f65948eb,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__217181e4e3c8a70d8949eb63f65948ebm40_m944303257 (Il2CppObject * __this /* static, unused */, _217181e4e3c8a70d8949eb63f65948eb_t2112101980 * ____this0, int32_t ____217181e4e3c8a70d8949eb63f65948eba1, int32_t ____217181e4e3c8a70d8949eb63f65948eb72, int32_t ____217181e4e3c8a70d8949eb63f65948ebc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__24b18d696f860f5cbf1c6268957365e9m41(Little._24b18d696f860f5cbf1c6268957365e9,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__24b18d696f860f5cbf1c6268957365e9m41_m2489691156 (Il2CppObject * __this /* static, unused */, _24b18d696f860f5cbf1c6268957365e9_t2903922058 * ____this0, int32_t ____24b18d696f860f5cbf1c6268957365e9a1, int32_t ____24b18d696f860f5cbf1c6268957365e9322, int32_t ____24b18d696f860f5cbf1c6268957365e9c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__e4d88929f95c57f0d5ddf5c2e379a42em42(Little._e4d88929f95c57f0d5ddf5c2e379a42e,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__e4d88929f95c57f0d5ddf5c2e379a42em42_m2170436491 (Il2CppObject * __this /* static, unused */, _e4d88929f95c57f0d5ddf5c2e379a42e_t176483638 * ____this0, int32_t ____e4d88929f95c57f0d5ddf5c2e379a42ea1, int32_t ____e4d88929f95c57f0d5ddf5c2e379a42e982, int32_t ____e4d88929f95c57f0d5ddf5c2e379a42ec3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__e61d8ce101f35e44df5f1672613d9d0dm43(Little._e61d8ce101f35e44df5f1672613d9d0d,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__e61d8ce101f35e44df5f1672613d9d0dm43_m3712367518 (Il2CppObject * __this /* static, unused */, _e61d8ce101f35e44df5f1672613d9d0d_t2746409392 * ____this0, int32_t ____e61d8ce101f35e44df5f1672613d9d0da1, int32_t ____e61d8ce101f35e44df5f1672613d9d0d832, int32_t ____e61d8ce101f35e44df5f1672613d9d0dc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__fae995a476d99337380fd567b994a551m44(Little._fae995a476d99337380fd567b994a551,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__fae995a476d99337380fd567b994a551m44_m581272397 (Il2CppObject * __this /* static, unused */, _fae995a476d99337380fd567b994a551_t4088872568 * ____this0, int32_t ____fae995a476d99337380fd567b994a551a1, int32_t ____fae995a476d99337380fd567b994a551472, int32_t ____fae995a476d99337380fd567b994a551c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__2b0841d2beaa343275e0f45893f75a17m45(Little._2b0841d2beaa343275e0f45893f75a17,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__2b0841d2beaa343275e0f45893f75a17m45_m2728319656 (Il2CppObject * __this /* static, unused */, _2b0841d2beaa343275e0f45893f75a17_t213419414 * ____this0, int32_t ____2b0841d2beaa343275e0f45893f75a17a1, int32_t ____2b0841d2beaa343275e0f45893f75a17622, int32_t ____2b0841d2beaa343275e0f45893f75a17c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__c3efc2b5ec1f644352d33fbc5d9b29b9m46(Little._c3efc2b5ec1f644352d33fbc5d9b29b9,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__c3efc2b5ec1f644352d33fbc5d9b29b9m46_m3764597439 (Il2CppObject * __this /* static, unused */, _c3efc2b5ec1f644352d33fbc5d9b29b9_t3041996178 * ____this0, int32_t ____c3efc2b5ec1f644352d33fbc5d9b29b9a1, int32_t ____c3efc2b5ec1f644352d33fbc5d9b29b9542, int32_t ____c3efc2b5ec1f644352d33fbc5d9b29b9c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__a68d852d3d0d498e2cc6c927f5f6f402m47(Little._a68d852d3d0d498e2cc6c927f5f6f402,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__a68d852d3d0d498e2cc6c927f5f6f402m47_m1238904086 (Il2CppObject * __this /* static, unused */, _a68d852d3d0d498e2cc6c927f5f6f402_t4230635182 * ____this0, int32_t ____a68d852d3d0d498e2cc6c927f5f6f402a1, int32_t ____a68d852d3d0d498e2cc6c927f5f6f402352, int32_t ____a68d852d3d0d498e2cc6c927f5f6f402c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__cf94381410fe54b0cde1308e3b813427m48(Little._cf94381410fe54b0cde1308e3b813427,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__cf94381410fe54b0cde1308e3b813427m48_m1288111159 (Il2CppObject * __this /* static, unused */, _cf94381410fe54b0cde1308e3b813427_t3623958767 * ____this0, int32_t ____cf94381410fe54b0cde1308e3b813427a1, int32_t ____cf94381410fe54b0cde1308e3b813427812, int32_t ____cf94381410fe54b0cde1308e3b813427c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__babdc95f3d0136d9cd86c347ec215bf2m49(Little._babdc95f3d0136d9cd86c347ec215bf2,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__babdc95f3d0136d9cd86c347ec215bf2m49_m2720310750 (Il2CppObject * __this /* static, unused */, _babdc95f3d0136d9cd86c347ec215bf2_t1694397363 * ____this0, int32_t ____babdc95f3d0136d9cd86c347ec215bf2a1, int32_t ____babdc95f3d0136d9cd86c347ec215bf2162, int32_t ____babdc95f3d0136d9cd86c347ec215bf2c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__7a849745227a240bd28845fc59065605m50(Little._7a849745227a240bd28845fc59065605,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__7a849745227a240bd28845fc59065605m50_m1208134610 (Il2CppObject * __this /* static, unused */, _7a849745227a240bd28845fc59065605_t1773294104 * ____this0, int32_t ____7a849745227a240bd28845fc59065605a1, int32_t ____7a849745227a240bd28845fc5906560562, int32_t ____7a849745227a240bd28845fc59065605c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__65e3d7afd786a185d9f4e2a8e4ce5b8am51(Little._65e3d7afd786a185d9f4e2a8e4ce5b8a,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__65e3d7afd786a185d9f4e2a8e4ce5b8am51_m16475605 (Il2CppObject * __this /* static, unused */, _65e3d7afd786a185d9f4e2a8e4ce5b8a_t2754037130 * ____this0, int32_t ____65e3d7afd786a185d9f4e2a8e4ce5b8aa1, int32_t ____65e3d7afd786a185d9f4e2a8e4ce5b8a412, int32_t ____65e3d7afd786a185d9f4e2a8e4ce5b8ac3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__1fb56dd813fe334b6404430513f138bcm52(Little._1fb56dd813fe334b6404430513f138bc,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__1fb56dd813fe334b6404430513f138bcm52_m3230764022 (Il2CppObject * __this /* static, unused */, _1fb56dd813fe334b6404430513f138bc_t4163233611 * ____this0, int32_t ____1fb56dd813fe334b6404430513f138bca1, int32_t ____1fb56dd813fe334b6404430513f138bc902, int32_t ____1fb56dd813fe334b6404430513f138bcc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__f9f7383e08fc8c6f67a9942e03b92888m53(Little._f9f7383e08fc8c6f67a9942e03b92888,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__f9f7383e08fc8c6f67a9942e03b92888m53_m1844787061 (Il2CppObject * __this /* static, unused */, _f9f7383e08fc8c6f67a9942e03b92888_t1003330555 * ____this0, int32_t ____f9f7383e08fc8c6f67a9942e03b92888a1, int32_t ____f9f7383e08fc8c6f67a9942e03b92888512, int32_t ____f9f7383e08fc8c6f67a9942e03b92888c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__57f8cb4c0c3f73fe08e46335ac989d7dm54(Little._57f8cb4c0c3f73fe08e46335ac989d7d,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__57f8cb4c0c3f73fe08e46335ac989d7dm54_m2718200052 (Il2CppObject * __this /* static, unused */, _57f8cb4c0c3f73fe08e46335ac989d7d_t2260031403 * ____this0, int32_t ____57f8cb4c0c3f73fe08e46335ac989d7da1, int32_t ____57f8cb4c0c3f73fe08e46335ac989d7d492, int32_t ____57f8cb4c0c3f73fe08e46335ac989d7dc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__73c6935296a13f42e8b7499443bbd435m55(Little._73c6935296a13f42e8b7499443bbd435,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__73c6935296a13f42e8b7499443bbd435m55_m3705877353 (Il2CppObject * __this /* static, unused */, _73c6935296a13f42e8b7499443bbd435_t1747641622 * ____this0, int32_t ____73c6935296a13f42e8b7499443bbd435a1, int32_t ____73c6935296a13f42e8b7499443bbd435222, int32_t ____73c6935296a13f42e8b7499443bbd435c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__73db4f97c2fd7e8b51e95f30f3bf841dm56(Little._73db4f97c2fd7e8b51e95f30f3bf841d,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__73db4f97c2fd7e8b51e95f30f3bf841dm56_m794508008 (Il2CppObject * __this /* static, unused */, _73db4f97c2fd7e8b51e95f30f3bf841d_t1627998150 * ____this0, int32_t ____73db4f97c2fd7e8b51e95f30f3bf841da1, int32_t ____73db4f97c2fd7e8b51e95f30f3bf841d442, int32_t ____73db4f97c2fd7e8b51e95f30f3bf841dc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__6cbb6c301f3237ba4eb5838f24ef1aeem57(Little._6cbb6c301f3237ba4eb5838f24ef1aee,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__6cbb6c301f3237ba4eb5838f24ef1aeem57_m2907062601 (Il2CppObject * __this /* static, unused */, _6cbb6c301f3237ba4eb5838f24ef1aee_t3704541607 * ____this0, int32_t ____6cbb6c301f3237ba4eb5838f24ef1aeea1, int32_t ____6cbb6c301f3237ba4eb5838f24ef1aee62, int32_t ____6cbb6c301f3237ba4eb5838f24ef1aeec3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__1ea71e0a65a6ebaf7fb7939afd4d412dm58(Little._1ea71e0a65a6ebaf7fb7939afd4d412d,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__1ea71e0a65a6ebaf7fb7939afd4d412dm58_m2887326236 (Il2CppObject * __this /* static, unused */, _1ea71e0a65a6ebaf7fb7939afd4d412d_t3308296833 * ____this0, int32_t ____1ea71e0a65a6ebaf7fb7939afd4d412da1, int32_t ____1ea71e0a65a6ebaf7fb7939afd4d412d452, int32_t ____1ea71e0a65a6ebaf7fb7939afd4d412dc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__66ce15e203d0f4cc0d078f9eb3293853m59(Little._66ce15e203d0f4cc0d078f9eb3293853,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__66ce15e203d0f4cc0d078f9eb3293853m59_m97459563 (Il2CppObject * __this /* static, unused */, _66ce15e203d0f4cc0d078f9eb3293853_t3930857753 * ____this0, int32_t ____66ce15e203d0f4cc0d078f9eb3293853a1, int32_t ____66ce15e203d0f4cc0d078f9eb3293853122, int32_t ____66ce15e203d0f4cc0d078f9eb3293853c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__137fdf3de99bcc7b8f09d8650229aef0m60(Little._137fdf3de99bcc7b8f09d8650229aef0,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__137fdf3de99bcc7b8f09d8650229aef0m60_m3710424687 (Il2CppObject * __this /* static, unused */, _137fdf3de99bcc7b8f09d8650229aef0_t875788038 * ____this0, int32_t ____137fdf3de99bcc7b8f09d8650229aef0a1, int32_t ____137fdf3de99bcc7b8f09d8650229aef0812, int32_t ____137fdf3de99bcc7b8f09d8650229aef0c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__e66f75f7f745328d65689c818f77fd2bm61(Little._e66f75f7f745328d65689c818f77fd2b,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__e66f75f7f745328d65689c818f77fd2bm61_m4018717198 (Il2CppObject * __this /* static, unused */, _e66f75f7f745328d65689c818f77fd2b_t3599083462 * ____this0, int32_t ____e66f75f7f745328d65689c818f77fd2ba1, int32_t ____e66f75f7f745328d65689c818f77fd2b572, int32_t ____e66f75f7f745328d65689c818f77fd2bc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__61ce11f8be181b0cbd58a8e6669fa6bem62(Little._61ce11f8be181b0cbd58a8e6669fa6be,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__61ce11f8be181b0cbd58a8e6669fa6bem62_m3362491761 (Il2CppObject * __this /* static, unused */, _61ce11f8be181b0cbd58a8e6669fa6be_t1407465448 * ____this0, int32_t ____61ce11f8be181b0cbd58a8e6669fa6bea1, int32_t ____61ce11f8be181b0cbd58a8e6669fa6be142, int32_t ____61ce11f8be181b0cbd58a8e6669fa6bec3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__798919d13467fd36a9d43d7c7e0d3f07m63(Little._798919d13467fd36a9d43d7c7e0d3f07,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__798919d13467fd36a9d43d7c7e0d3f07m63_m3126980394 (Il2CppObject * __this /* static, unused */, _798919d13467fd36a9d43d7c7e0d3f07_t1965012085 * ____this0, int32_t ____798919d13467fd36a9d43d7c7e0d3f07a1, int32_t ____798919d13467fd36a9d43d7c7e0d3f0772, int32_t ____798919d13467fd36a9d43d7c7e0d3f07c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__4977984fa092d3ffa2fd97f1e85aa8c2m64(Little._4977984fa092d3ffa2fd97f1e85aa8c2,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__4977984fa092d3ffa2fd97f1e85aa8c2m64_m3977814949 (Il2CppObject * __this /* static, unused */, _4977984fa092d3ffa2fd97f1e85aa8c2_t3215607331 * ____this0, int32_t ____4977984fa092d3ffa2fd97f1e85aa8c2a1, int32_t ____4977984fa092d3ffa2fd97f1e85aa8c262, int32_t ____4977984fa092d3ffa2fd97f1e85aa8c2c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__acd985249d98f31768c2074d66f94909m65(Little._acd985249d98f31768c2074d66f94909,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__acd985249d98f31768c2074d66f94909m65_m3497463400 (Il2CppObject * __this /* static, unused */, _acd985249d98f31768c2074d66f94909_t3362093557 * ____this0, int32_t ____acd985249d98f31768c2074d66f94909a1, int32_t ____acd985249d98f31768c2074d66f94909172, int32_t ____acd985249d98f31768c2074d66f94909c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__f28cf315aa393e21752336c1d24f0e4fm66(Little._f28cf315aa393e21752336c1d24f0e4f,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__f28cf315aa393e21752336c1d24f0e4fm66_m606626683 (Il2CppObject * __this /* static, unused */, _f28cf315aa393e21752336c1d24f0e4f_t2589423855 * ____this0, int32_t ____f28cf315aa393e21752336c1d24f0e4fa1, int32_t ____f28cf315aa393e21752336c1d24f0e4f552, int32_t ____f28cf315aa393e21752336c1d24f0e4fc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__1519195eb7ca7dc7ac84baf0853b209fm67(Little._1519195eb7ca7dc7ac84baf0853b209f,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__1519195eb7ca7dc7ac84baf0853b209fm67_m2422300250 (Il2CppObject * __this /* static, unused */, _1519195eb7ca7dc7ac84baf0853b209f_t1437772111 * ____this0, int32_t ____1519195eb7ca7dc7ac84baf0853b209fa1, int32_t ____1519195eb7ca7dc7ac84baf0853b209f172, int32_t ____1519195eb7ca7dc7ac84baf0853b209fc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__335d969307e829d08f086d8855ca9f79m68(Little._335d969307e829d08f086d8855ca9f79,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__335d969307e829d08f086d8855ca9f79m68_m1528221205 (Il2CppObject * __this /* static, unused */, _335d969307e829d08f086d8855ca9f79_t1777578141 * ____this0, int32_t ____335d969307e829d08f086d8855ca9f79a1, int32_t ____335d969307e829d08f086d8855ca9f79212, int32_t ____335d969307e829d08f086d8855ca9f79c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__d6620188ed1a0893940069833ea5d12em69(Little._d6620188ed1a0893940069833ea5d12e,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__d6620188ed1a0893940069833ea5d12em69_m3492071978 (Il2CppObject * __this /* static, unused */, _d6620188ed1a0893940069833ea5d12e_t3986220760 * ____this0, int32_t ____d6620188ed1a0893940069833ea5d12ea1, int32_t ____d6620188ed1a0893940069833ea5d12e672, int32_t ____d6620188ed1a0893940069833ea5d12ec3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__b3d8ae55e6cec1f8630ccd39449765bdm70(Little._b3d8ae55e6cec1f8630ccd39449765bd,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__b3d8ae55e6cec1f8630ccd39449765bdm70_m2243878650 (Il2CppObject * __this /* static, unused */, _b3d8ae55e6cec1f8630ccd39449765bd_t1137912107 * ____this0, int32_t ____b3d8ae55e6cec1f8630ccd39449765bda1, int32_t ____b3d8ae55e6cec1f8630ccd39449765bd862, int32_t ____b3d8ae55e6cec1f8630ccd39449765bdc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__167c8098c7d6a54a509bf88d4308b1dbm71(Little._167c8098c7d6a54a509bf88d4308b1db,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__167c8098c7d6a54a509bf88d4308b1dbm71_m530689855 (Il2CppObject * __this /* static, unused */, _167c8098c7d6a54a509bf88d4308b1db_t3666518910 * ____this0, int32_t ____167c8098c7d6a54a509bf88d4308b1dba1, int32_t ____167c8098c7d6a54a509bf88d4308b1db862, int32_t ____167c8098c7d6a54a509bf88d4308b1dbc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__358b2ceeb02da25bfb7d5a5003b54f3dm72(Little._358b2ceeb02da25bfb7d5a5003b54f3d,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__358b2ceeb02da25bfb7d5a5003b54f3dm72_m2936108252 (Il2CppObject * __this /* static, unused */, _358b2ceeb02da25bfb7d5a5003b54f3d_t2503056765 * ____this0, int32_t ____358b2ceeb02da25bfb7d5a5003b54f3da1, int32_t ____358b2ceeb02da25bfb7d5a5003b54f3d742, int32_t ____358b2ceeb02da25bfb7d5a5003b54f3dc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__7398858b399286fd156d20c25bce046fm73(Little._7398858b399286fd156d20c25bce046f,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__7398858b399286fd156d20c25bce046fm73_m2263210289 (Il2CppObject * __this /* static, unused */, _7398858b399286fd156d20c25bce046f_t79527896 * ____this0, int32_t ____7398858b399286fd156d20c25bce046fa1, int32_t ____7398858b399286fd156d20c25bce046f122, int32_t ____7398858b399286fd156d20c25bce046fc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__4a50809cc3a58127f091fba147ca7253m74(Little._4a50809cc3a58127f091fba147ca7253,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__4a50809cc3a58127f091fba147ca7253m74_m4035835898 (Il2CppObject * __this /* static, unused */, _4a50809cc3a58127f091fba147ca7253_t3335674093 * ____this0, int32_t ____4a50809cc3a58127f091fba147ca7253a1, int32_t ____4a50809cc3a58127f091fba147ca725362, int32_t ____4a50809cc3a58127f091fba147ca7253c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__ee6a626890871938327aea2b1ccc8ec5m75(Little._ee6a626890871938327aea2b1ccc8ec5,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__ee6a626890871938327aea2b1ccc8ec5m75_m1574643669 (Il2CppObject * __this /* static, unused */, _ee6a626890871938327aea2b1ccc8ec5_t1940094027 * ____this0, int32_t ____ee6a626890871938327aea2b1ccc8ec5a1, int32_t ____ee6a626890871938327aea2b1ccc8ec5172, int32_t ____ee6a626890871938327aea2b1ccc8ec5c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__105105725aab512a55c3eca9cd73fc9bm76(Little._105105725aab512a55c3eca9cd73fc9b,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__105105725aab512a55c3eca9cd73fc9bm76_m3484425864 (Il2CppObject * __this /* static, unused */, _105105725aab512a55c3eca9cd73fc9b_t654465045 * ____this0, int32_t ____105105725aab512a55c3eca9cd73fc9ba1, int32_t ____105105725aab512a55c3eca9cd73fc9b592, int32_t ____105105725aab512a55c3eca9cd73fc9bc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__6eea3fb2dfdc9ac8a56559e34cfe9565m77(Little._6eea3fb2dfdc9ac8a56559e34cfe9565,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__6eea3fb2dfdc9ac8a56559e34cfe9565m77_m4093404963 (Il2CppObject * __this /* static, unused */, _6eea3fb2dfdc9ac8a56559e34cfe9565_t3406393299 * ____this0, int32_t ____6eea3fb2dfdc9ac8a56559e34cfe9565a1, int32_t ____6eea3fb2dfdc9ac8a56559e34cfe9565802, int32_t ____6eea3fb2dfdc9ac8a56559e34cfe9565c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__eb9ab911a4c0ae469c89b90a95bc96c6m78(Little._eb9ab911a4c0ae469c89b90a95bc96c6,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__eb9ab911a4c0ae469c89b90a95bc96c6m78_m2895906578 (Il2CppObject * __this /* static, unused */, _eb9ab911a4c0ae469c89b90a95bc96c6_t936079547 * ____this0, int32_t ____eb9ab911a4c0ae469c89b90a95bc96c6a1, int32_t ____eb9ab911a4c0ae469c89b90a95bc96c6782, int32_t ____eb9ab911a4c0ae469c89b90a95bc96c6c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__fcafb63bd94da05ea4966ac4d0a99955m79(Little._fcafb63bd94da05ea4966ac4d0a99955,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__fcafb63bd94da05ea4966ac4d0a99955m79_m961516639 (Il2CppObject * __this /* static, unused */, _fcafb63bd94da05ea4966ac4d0a99955_t1851570322 * ____this0, int32_t ____fcafb63bd94da05ea4966ac4d0a99955a1, int32_t ____fcafb63bd94da05ea4966ac4d0a99955762, int32_t ____fcafb63bd94da05ea4966ac4d0a99955c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__93de4ffbbdc61d548f157798159ab3d5m80(Little._93de4ffbbdc61d548f157798159ab3d5,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__93de4ffbbdc61d548f157798159ab3d5m80_m2829730485 (Il2CppObject * __this /* static, unused */, _93de4ffbbdc61d548f157798159ab3d5_t1927963368 * ____this0, int32_t ____93de4ffbbdc61d548f157798159ab3d5a1, int32_t ____93de4ffbbdc61d548f157798159ab3d5722, int32_t ____93de4ffbbdc61d548f157798159ab3d5c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__b3f01fd20587c7a1e7f3949dfc72de43m81(Little._b3f01fd20587c7a1e7f3949dfc72de43,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__b3f01fd20587c7a1e7f3949dfc72de43m81_m2955146108 (Il2CppObject * __this /* static, unused */, _b3f01fd20587c7a1e7f3949dfc72de43_t2020475196 * ____this0, int32_t ____b3f01fd20587c7a1e7f3949dfc72de43a1, int32_t ____b3f01fd20587c7a1e7f3949dfc72de43212, int32_t ____b3f01fd20587c7a1e7f3949dfc72de43c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__0b1c0065d68848cd685e688804c19acfm82(Little._0b1c0065d68848cd685e688804c19acf,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__0b1c0065d68848cd685e688804c19acfm82_m1253237547 (Il2CppObject * __this /* static, unused */, _0b1c0065d68848cd685e688804c19acf_t660943364 * ____this0, int32_t ____0b1c0065d68848cd685e688804c19acfa1, int32_t ____0b1c0065d68848cd685e688804c19acf802, int32_t ____0b1c0065d68848cd685e688804c19acfc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__31b8973e4e93620bc70503232dab3578m83(Little._31b8973e4e93620bc70503232dab3578,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__31b8973e4e93620bc70503232dab3578m83_m3027364308 (Il2CppObject * __this /* static, unused */, _31b8973e4e93620bc70503232dab3578_t3300672265 * ____this0, int32_t ____31b8973e4e93620bc70503232dab3578a1, int32_t ____31b8973e4e93620bc70503232dab3578302, int32_t ____31b8973e4e93620bc70503232dab3578c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__0320aa762d7354d96830f3480b871fcdm84(Little._0320aa762d7354d96830f3480b871fcd,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__0320aa762d7354d96830f3480b871fcdm84_m1533144133 (Il2CppObject * __this /* static, unused */, _0320aa762d7354d96830f3480b871fcd_t3090868466 * ____this0, int32_t ____0320aa762d7354d96830f3480b871fcda1, int32_t ____0320aa762d7354d96830f3480b871fcd792, int32_t ____0320aa762d7354d96830f3480b871fcdc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__c61155f4a5d0408136604c7f8af73de0m85(Little._c61155f4a5d0408136604c7f8af73de0,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__c61155f4a5d0408136604c7f8af73de0m85_m3193567808 (Il2CppObject * __this /* static, unused */, _c61155f4a5d0408136604c7f8af73de0_t2604529504 * ____this0, int32_t ____c61155f4a5d0408136604c7f8af73de0a1, int32_t ____c61155f4a5d0408136604c7f8af73de0732, int32_t ____c61155f4a5d0408136604c7f8af73de0c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__b6fa4ce8834e6ca93691732bac8ab05em86(Little._b6fa4ce8834e6ca93691732bac8ab05e,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__b6fa4ce8834e6ca93691732bac8ab05em86_m1882867381 (Il2CppObject * __this /* static, unused */, _b6fa4ce8834e6ca93691732bac8ab05e_t97978827 * ____this0, int32_t ____b6fa4ce8834e6ca93691732bac8ab05ea1, int32_t ____b6fa4ce8834e6ca93691732bac8ab05e82, int32_t ____b6fa4ce8834e6ca93691732bac8ab05ec3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__eadb7caa6752c5f0e0f248add71b2f4am87(Little._eadb7caa6752c5f0e0f248add71b2f4a,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__eadb7caa6752c5f0e0f248add71b2f4am87_m382094454 (Il2CppObject * __this /* static, unused */, _eadb7caa6752c5f0e0f248add71b2f4a_t3055544156 * ____this0, int32_t ____eadb7caa6752c5f0e0f248add71b2f4aa1, int32_t ____eadb7caa6752c5f0e0f248add71b2f4a372, int32_t ____eadb7caa6752c5f0e0f248add71b2f4ac3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__9fa0d3aba4ec4a380a50209ee71e7919m88(Little._9fa0d3aba4ec4a380a50209ee71e7919,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__9fa0d3aba4ec4a380a50209ee71e7919m88_m4116148217 (Il2CppObject * __this /* static, unused */, _9fa0d3aba4ec4a380a50209ee71e7919_t2230771598 * ____this0, int32_t ____9fa0d3aba4ec4a380a50209ee71e7919a1, int32_t ____9fa0d3aba4ec4a380a50209ee71e791942, int32_t ____9fa0d3aba4ec4a380a50209ee71e7919c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__20b3e13d35f7c11f2d37a34ae55dcdd3m89(Little._20b3e13d35f7c11f2d37a34ae55dcdd3,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__20b3e13d35f7c11f2d37a34ae55dcdd3m89_m3897624058 (Il2CppObject * __this /* static, unused */, _20b3e13d35f7c11f2d37a34ae55dcdd3_t2623339839 * ____this0, int32_t ____20b3e13d35f7c11f2d37a34ae55dcdd3a1, int32_t ____20b3e13d35f7c11f2d37a34ae55dcdd3702, int32_t ____20b3e13d35f7c11f2d37a34ae55dcdd3c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__c7ddaba247e5c2461d57b7dbc6269161m90(Little._c7ddaba247e5c2461d57b7dbc6269161,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__c7ddaba247e5c2461d57b7dbc6269161m90_m2335574096 (Il2CppObject * __this /* static, unused */, _c7ddaba247e5c2461d57b7dbc6269161_t3415036309 * ____this0, int32_t ____c7ddaba247e5c2461d57b7dbc6269161a1, int32_t ____c7ddaba247e5c2461d57b7dbc6269161552, int32_t ____c7ddaba247e5c2461d57b7dbc6269161c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__e3d68d5a9ce77f230dcc97aaf33931a6m91(Little._e3d68d5a9ce77f230dcc97aaf33931a6,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__e3d68d5a9ce77f230dcc97aaf33931a6m91_m15963311 (Il2CppObject * __this /* static, unused */, _e3d68d5a9ce77f230dcc97aaf33931a6_t3540753077 * ____this0, int32_t ____e3d68d5a9ce77f230dcc97aaf33931a6a1, int32_t ____e3d68d5a9ce77f230dcc97aaf33931a6182, int32_t ____e3d68d5a9ce77f230dcc97aaf33931a6c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__5ac663cbef4d858a7eaa7e7e41da552em92(Little._5ac663cbef4d858a7eaa7e7e41da552e,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__5ac663cbef4d858a7eaa7e7e41da552em92_m1148534376 (Il2CppObject * __this /* static, unused */, _5ac663cbef4d858a7eaa7e7e41da552e_t3375610690 * ____this0, int32_t ____5ac663cbef4d858a7eaa7e7e41da552ea1, int32_t ____5ac663cbef4d858a7eaa7e7e41da552e522, int32_t ____5ac663cbef4d858a7eaa7e7e41da552ec3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__cc1da90d49f035389ffd86bf263ae690m93(Little._cc1da90d49f035389ffd86bf263ae690,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__cc1da90d49f035389ffd86bf263ae690m93_m3118631021 (Il2CppObject * __this /* static, unused */, _cc1da90d49f035389ffd86bf263ae690_t3634150005 * ____this0, int32_t ____cc1da90d49f035389ffd86bf263ae690a1, int32_t ____cc1da90d49f035389ffd86bf263ae690772, int32_t ____cc1da90d49f035389ffd86bf263ae690c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__f6198862a0c5d0657419e00bf21db60bm94(Little._f6198862a0c5d0657419e00bf21db60b,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__f6198862a0c5d0657419e00bf21db60bm94_m1984262178 (Il2CppObject * __this /* static, unused */, _f6198862a0c5d0657419e00bf21db60b_t1098003072 * ____this0, int32_t ____f6198862a0c5d0657419e00bf21db60ba1, int32_t ____f6198862a0c5d0657419e00bf21db60b782, int32_t ____f6198862a0c5d0657419e00bf21db60bc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__9444e5129c9e9532b37d1aa35f7acb12m95(Little._9444e5129c9e9532b37d1aa35f7acb12,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__9444e5129c9e9532b37d1aa35f7acb12m95_m1095725411 (Il2CppObject * __this /* static, unused */, _9444e5129c9e9532b37d1aa35f7acb12_t3452018129 * ____this0, int32_t ____9444e5129c9e9532b37d1aa35f7acb12a1, int32_t ____9444e5129c9e9532b37d1aa35f7acb12612, int32_t ____9444e5129c9e9532b37d1aa35f7acb12c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__1ccc2e8f9a26a2ceca0997b5916a6b94m96(Little._1ccc2e8f9a26a2ceca0997b5916a6b94,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__1ccc2e8f9a26a2ceca0997b5916a6b94m96_m761464634 (Il2CppObject * __this /* static, unused */, _1ccc2e8f9a26a2ceca0997b5916a6b94_t1222487213 * ____this0, int32_t ____1ccc2e8f9a26a2ceca0997b5916a6b94a1, int32_t ____1ccc2e8f9a26a2ceca0997b5916a6b94352, int32_t ____1ccc2e8f9a26a2ceca0997b5916a6b94c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__32f144836475fa0651f79c1649a2141fm97(Little._32f144836475fa0651f79c1649a2141f,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__32f144836475fa0651f79c1649a2141fm97_m3279498569 (Il2CppObject * __this /* static, unused */, _32f144836475fa0651f79c1649a2141f_t2022861989 * ____this0, int32_t ____32f144836475fa0651f79c1649a2141fa1, int32_t ____32f144836475fa0651f79c1649a2141f272, int32_t ____32f144836475fa0651f79c1649a2141fc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__0a62ee45ea1c0830dcf23cc9c34830a8m98(Little._0a62ee45ea1c0830dcf23cc9c34830a8,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__0a62ee45ea1c0830dcf23cc9c34830a8m98_m727230404 (Il2CppObject * __this /* static, unused */, _0a62ee45ea1c0830dcf23cc9c34830a8_t4117012819 * ____this0, int32_t ____0a62ee45ea1c0830dcf23cc9c34830a8a1, int32_t ____0a62ee45ea1c0830dcf23cc9c34830a8652, int32_t ____0a62ee45ea1c0830dcf23cc9c34830a8c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__6a66de2b393decca12c0cc62ed48595cm99(Little._6a66de2b393decca12c0cc62ed48595c,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__6a66de2b393decca12c0cc62ed48595cm99_m2172914699 (Il2CppObject * __this /* static, unused */, _6a66de2b393decca12c0cc62ed48595c_t3858868583 * ____this0, int32_t ____6a66de2b393decca12c0cc62ed48595ca1, int32_t ____6a66de2b393decca12c0cc62ed48595c352, int32_t ____6a66de2b393decca12c0cc62ed48595cc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__e628ed36c34984f19707b26f0f605a44m100(Little._e628ed36c34984f19707b26f0f605a44,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__e628ed36c34984f19707b26f0f605a44m100_m4054478810 (Il2CppObject * __this /* static, unused */, _e628ed36c34984f19707b26f0f605a44_t715680303 * ____this0, int32_t ____e628ed36c34984f19707b26f0f605a44a1, int32_t ____e628ed36c34984f19707b26f0f605a44352, int32_t ____e628ed36c34984f19707b26f0f605a44c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__f7d06a788686998f13bf3273d9b4bac6m101(Little._f7d06a788686998f13bf3273d9b4bac6,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__f7d06a788686998f13bf3273d9b4bac6m101_m1328483161 (Il2CppObject * __this /* static, unused */, _f7d06a788686998f13bf3273d9b4bac6_t621400070 * ____this0, int32_t ____f7d06a788686998f13bf3273d9b4bac6a1, int32_t ____f7d06a788686998f13bf3273d9b4bac6392, int32_t ____f7d06a788686998f13bf3273d9b4bac6c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__0a0c786a833f5bdadc6113e3ed2fc5f8m102(Little._0a0c786a833f5bdadc6113e3ed2fc5f8,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__0a0c786a833f5bdadc6113e3ed2fc5f8m102_m289074936 (Il2CppObject * __this /* static, unused */, _0a0c786a833f5bdadc6113e3ed2fc5f8_t1051355010 * ____this0, int32_t ____0a0c786a833f5bdadc6113e3ed2fc5f8a1, int32_t ____0a0c786a833f5bdadc6113e3ed2fc5f8862, int32_t ____0a0c786a833f5bdadc6113e3ed2fc5f8c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__a2a7a4df4084131ec373845ea2e38aa9m103(Little._a2a7a4df4084131ec373845ea2e38aa9,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__a2a7a4df4084131ec373845ea2e38aa9m103_m1624476087 (Il2CppObject * __this /* static, unused */, _a2a7a4df4084131ec373845ea2e38aa9_t1922737915 * ____this0, int32_t ____a2a7a4df4084131ec373845ea2e38aa9a1, int32_t ____a2a7a4df4084131ec373845ea2e38aa9912, int32_t ____a2a7a4df4084131ec373845ea2e38aa9c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__8c0a8d06add0702174c1414cf2bba00em104(Little._8c0a8d06add0702174c1414cf2bba00e,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__8c0a8d06add0702174c1414cf2bba00em104_m2256635414 (Il2CppObject * __this /* static, unused */, _8c0a8d06add0702174c1414cf2bba00e_t2489032853 * ____this0, int32_t ____8c0a8d06add0702174c1414cf2bba00ea1, int32_t ____8c0a8d06add0702174c1414cf2bba00e832, int32_t ____8c0a8d06add0702174c1414cf2bba00ec3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__637622168db9c2e40c39460a4b5ac3afm105(Little._637622168db9c2e40c39460a4b5ac3af,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__637622168db9c2e40c39460a4b5ac3afm105_m689585621 (Il2CppObject * __this /* static, unused */, _637622168db9c2e40c39460a4b5ac3af_t402205590 * ____this0, int32_t ____637622168db9c2e40c39460a4b5ac3afa1, int32_t ____637622168db9c2e40c39460a4b5ac3af942, int32_t ____637622168db9c2e40c39460a4b5ac3afc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__e6a128008d0f9776c415ac7cba6879cdm106(Little._e6a128008d0f9776c415ac7cba6879cd,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__e6a128008d0f9776c415ac7cba6879cdm106_m1925880308 (Il2CppObject * __this /* static, unused */, _e6a128008d0f9776c415ac7cba6879cd_t3939383142 * ____this0, int32_t ____e6a128008d0f9776c415ac7cba6879cda1, int32_t ____e6a128008d0f9776c415ac7cba6879cd12, int32_t ____e6a128008d0f9776c415ac7cba6879cdc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__2e032e1a76a66392a072885063cada0am107(Little._2e032e1a76a66392a072885063cada0a,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__2e032e1a76a66392a072885063cada0am107_m2635207315 (Il2CppObject * __this /* static, unused */, _2e032e1a76a66392a072885063cada0a_t920276266 * ____this0, int32_t ____2e032e1a76a66392a072885063cada0aa1, int32_t ____2e032e1a76a66392a072885063cada0a592, int32_t ____2e032e1a76a66392a072885063cada0ac3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__23efa7168bc295f944f9775382d3a0b0m108(Little._23efa7168bc295f944f9775382d3a0b0,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__23efa7168bc295f944f9775382d3a0b0m108_m2026721938 (Il2CppObject * __this /* static, unused */, _23efa7168bc295f944f9775382d3a0b0_t1506263605 * ____this0, int32_t ____23efa7168bc295f944f9775382d3a0b0a1, int32_t ____23efa7168bc295f944f9775382d3a0b0482, int32_t ____23efa7168bc295f944f9775382d3a0b0c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__91a04852e9b0fb377e119d646dbf2486m109(Little._91a04852e9b0fb377e119d646dbf2486,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__91a04852e9b0fb377e119d646dbf2486m109_m4022251025 (Il2CppObject * __this /* static, unused */, _91a04852e9b0fb377e119d646dbf2486_t3538707980 * ____this0, int32_t ____91a04852e9b0fb377e119d646dbf2486a1, int32_t ____91a04852e9b0fb377e119d646dbf2486462, int32_t ____91a04852e9b0fb377e119d646dbf2486c3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__dd0d51d27df06c2a551ec5d6d2b2a7bam110(Little._dd0d51d27df06c2a551ec5d6d2b2a7ba,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__dd0d51d27df06c2a551ec5d6d2b2a7bam110_m3369550683 (Il2CppObject * __this /* static, unused */, _dd0d51d27df06c2a551ec5d6d2b2a7ba_t432501365 * ____this0, int32_t ____dd0d51d27df06c2a551ec5d6d2b2a7baa1, int32_t ____dd0d51d27df06c2a551ec5d6d2b2a7ba432, int32_t ____dd0d51d27df06c2a551ec5d6d2b2a7bac3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 P.Urge.GameController::ilo__62a1c1379d2b981c3250a6ea5de6565bm111(Little._62a1c1379d2b981c3250a6ea5de6565b,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t GameController_ilo__62a1c1379d2b981c3250a6ea5de6565bm111_m3012013274 (Il2CppObject * __this /* static, unused */, _62a1c1379d2b981c3250a6ea5de6565b_t322878076 * ____this0, int32_t ____62a1c1379d2b981c3250a6ea5de6565ba1, int32_t ____62a1c1379d2b981c3250a6ea5de6565b312, int32_t ____62a1c1379d2b981c3250a6ea5de6565bc3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
