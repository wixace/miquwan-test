﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginYuLe
struct  PluginYuLe_t1606594472  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginYuLe::userId
	String_t* ___userId_2;
	// System.String PluginYuLe::loginVerifyParm
	String_t* ___loginVerifyParm_3;
	// System.String PluginYuLe::sign
	String_t* ___sign_4;
	// System.Boolean PluginYuLe::isOut
	bool ___isOut_5;
	// System.String PluginYuLe::configId
	String_t* ___configId_6;
	// Mihua.SDK.PayInfo PluginYuLe::payInfo
	PayInfo_t1775308120 * ___payInfo_7;

public:
	inline static int32_t get_offset_of_userId_2() { return static_cast<int32_t>(offsetof(PluginYuLe_t1606594472, ___userId_2)); }
	inline String_t* get_userId_2() const { return ___userId_2; }
	inline String_t** get_address_of_userId_2() { return &___userId_2; }
	inline void set_userId_2(String_t* value)
	{
		___userId_2 = value;
		Il2CppCodeGenWriteBarrier(&___userId_2, value);
	}

	inline static int32_t get_offset_of_loginVerifyParm_3() { return static_cast<int32_t>(offsetof(PluginYuLe_t1606594472, ___loginVerifyParm_3)); }
	inline String_t* get_loginVerifyParm_3() const { return ___loginVerifyParm_3; }
	inline String_t** get_address_of_loginVerifyParm_3() { return &___loginVerifyParm_3; }
	inline void set_loginVerifyParm_3(String_t* value)
	{
		___loginVerifyParm_3 = value;
		Il2CppCodeGenWriteBarrier(&___loginVerifyParm_3, value);
	}

	inline static int32_t get_offset_of_sign_4() { return static_cast<int32_t>(offsetof(PluginYuLe_t1606594472, ___sign_4)); }
	inline String_t* get_sign_4() const { return ___sign_4; }
	inline String_t** get_address_of_sign_4() { return &___sign_4; }
	inline void set_sign_4(String_t* value)
	{
		___sign_4 = value;
		Il2CppCodeGenWriteBarrier(&___sign_4, value);
	}

	inline static int32_t get_offset_of_isOut_5() { return static_cast<int32_t>(offsetof(PluginYuLe_t1606594472, ___isOut_5)); }
	inline bool get_isOut_5() const { return ___isOut_5; }
	inline bool* get_address_of_isOut_5() { return &___isOut_5; }
	inline void set_isOut_5(bool value)
	{
		___isOut_5 = value;
	}

	inline static int32_t get_offset_of_configId_6() { return static_cast<int32_t>(offsetof(PluginYuLe_t1606594472, ___configId_6)); }
	inline String_t* get_configId_6() const { return ___configId_6; }
	inline String_t** get_address_of_configId_6() { return &___configId_6; }
	inline void set_configId_6(String_t* value)
	{
		___configId_6 = value;
		Il2CppCodeGenWriteBarrier(&___configId_6, value);
	}

	inline static int32_t get_offset_of_payInfo_7() { return static_cast<int32_t>(offsetof(PluginYuLe_t1606594472, ___payInfo_7)); }
	inline PayInfo_t1775308120 * get_payInfo_7() const { return ___payInfo_7; }
	inline PayInfo_t1775308120 ** get_address_of_payInfo_7() { return &___payInfo_7; }
	inline void set_payInfo_7(PayInfo_t1775308120 * value)
	{
		___payInfo_7 = value;
		Il2CppCodeGenWriteBarrier(&___payInfo_7, value);
	}
};

struct PluginYuLe_t1606594472_StaticFields
{
public:
	// System.Action PluginYuLe::<>f__am$cache6
	Action_t3771233898 * ___U3CU3Ef__amU24cache6_8;
	// System.Action PluginYuLe::<>f__am$cache7
	Action_t3771233898 * ___U3CU3Ef__amU24cache7_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_8() { return static_cast<int32_t>(offsetof(PluginYuLe_t1606594472_StaticFields, ___U3CU3Ef__amU24cache6_8)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache6_8() const { return ___U3CU3Ef__amU24cache6_8; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache6_8() { return &___U3CU3Ef__amU24cache6_8; }
	inline void set_U3CU3Ef__amU24cache6_8(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache6_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_9() { return static_cast<int32_t>(offsetof(PluginYuLe_t1606594472_StaticFields, ___U3CU3Ef__amU24cache7_9)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache7_9() const { return ___U3CU3Ef__amU24cache7_9; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache7_9() { return &___U3CU3Ef__amU24cache7_9; }
	inline void set_U3CU3Ef__amU24cache7_9(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache7_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
