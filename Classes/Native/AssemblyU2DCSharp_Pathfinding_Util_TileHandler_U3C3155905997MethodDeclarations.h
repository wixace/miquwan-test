﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Util.TileHandler/<LoadTile>c__AnonStorey122
struct U3CLoadTileU3Ec__AnonStorey122_t3155905997;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Util.TileHandler/<LoadTile>c__AnonStorey122::.ctor()
extern "C"  void U3CLoadTileU3Ec__AnonStorey122__ctor_m886286398 (U3CLoadTileU3Ec__AnonStorey122_t3155905997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Util.TileHandler/<LoadTile>c__AnonStorey122::<>m__358(System.Boolean)
extern "C"  bool U3CLoadTileU3Ec__AnonStorey122_U3CU3Em__358_m3144915796 (U3CLoadTileU3Ec__AnonStorey122_t3155905997 * __this, bool ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
