﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// checkpointCfg
struct  checkpointCfg_t2816107964  : public CsCfgBase_t69924517
{
public:
	// System.Int32 checkpointCfg::id
	int32_t ___id_0;
	// System.Int32 checkpointCfg::mapID
	int32_t ___mapID_1;
	// System.String checkpointCfg::name
	String_t* ___name_2;
	// System.String checkpointCfg::sceneName
	String_t* ___sceneName_3;
	// System.Int32 checkpointCfg::iconType
	int32_t ___iconType_4;
	// System.String checkpointCfg::LevelIcon
	String_t* ___LevelIcon_5;
	// System.String checkpointCfg::localPos
	String_t* ___localPos_6;
	// System.Int32 checkpointCfg::Hatred
	int32_t ___Hatred_7;
	// System.Int32 checkpointCfg::Super_Skill_Limit
	int32_t ___Super_Skill_Limit_8;
	// System.Int32 checkpointCfg::attack_effect
	int32_t ___attack_effect_9;
	// System.Int32 checkpointCfg::workpoint
	int32_t ___workpoint_10;
	// System.String checkpointCfg::preloadUIID
	String_t* ___preloadUIID_11;
	// System.Int32 checkpointCfg::type
	int32_t ___type_12;
	// System.Int32 checkpointCfg::panelType
	int32_t ___panelType_13;
	// System.Int32 checkpointCfg::finishType
	int32_t ___finishType_14;
	// System.Int32 checkpointCfg::effect_nf
	int32_t ___effect_nf_15;
	// System.Int32 checkpointCfg::ifAppear
	int32_t ___ifAppear_16;
	// System.Int32 checkpointCfg::time
	int32_t ___time_17;
	// System.Int32 checkpointCfg::action
	int32_t ___action_18;
	// System.Int32 checkpointCfg::exp
	int32_t ___exp_19;
	// System.Int32 checkpointCfg::heroExp
	int32_t ___heroExp_20;
	// System.Int32 checkpointCfg::copper
	int32_t ___copper_21;
	// System.Int32 checkpointCfg::brushEnemy
	int32_t ___brushEnemy_22;
	// System.Int32 checkpointCfg::passType
	int32_t ___passType_23;
	// System.Int32 checkpointCfg::passValue
	int32_t ___passValue_24;
	// System.String checkpointCfg::loseValue
	String_t* ___loseValue_25;
	// System.Int32 checkpointCfg::passAnimation
	int32_t ___passAnimation_26;
	// System.Single checkpointCfg::delayFinishTime
	float ___delayFinishTime_27;
	// System.String checkpointCfg::passCameraAnimation
	String_t* ___passCameraAnimation_28;
	// System.String checkpointCfg::npcArr
	String_t* ___npcArr_29;
	// System.String checkpointCfg::rewardArr
	String_t* ___rewardArr_30;
	// System.Int32 checkpointCfg::dropId
	int32_t ___dropId_31;
	// System.Int32 checkpointCfg::firsttimedrop
	int32_t ___firsttimedrop_32;
	// System.Int32 checkpointCfg::firsttimedropnum
	int32_t ___firsttimedropnum_33;
	// System.Int32 checkpointCfg::sweep_lv
	int32_t ___sweep_lv_34;
	// System.String checkpointCfg::swipedrop
	String_t* ___swipedrop_35;
	// System.String checkpointCfg::swipedropcount
	String_t* ___swipedropcount_36;
	// System.Int32 checkpointCfg::hidestartext
	int32_t ___hidestartext_37;
	// System.Int32 checkpointCfg::gradeType1
	int32_t ___gradeType1_38;
	// System.Int32 checkpointCfg::gradeValue1
	int32_t ___gradeValue1_39;
	// System.String checkpointCfg::gradetext1
	String_t* ___gradetext1_40;
	// System.String checkpointCfg::gradetext1_lose
	String_t* ___gradetext1_lose_41;
	// System.Int32 checkpointCfg::gradeType2
	int32_t ___gradeType2_42;
	// System.Int32 checkpointCfg::gradeValue2
	int32_t ___gradeValue2_43;
	// System.String checkpointCfg::gradetext2
	String_t* ___gradetext2_44;
	// System.String checkpointCfg::gradetext2_lose
	String_t* ___gradetext2_lose_45;
	// System.Int32 checkpointCfg::gradeType3
	int32_t ___gradeType3_46;
	// System.Int32 checkpointCfg::gradeValue3
	int32_t ___gradeValue3_47;
	// System.String checkpointCfg::gradetext3
	String_t* ___gradetext3_48;
	// System.String checkpointCfg::gradetext3_lose
	String_t* ___gradetext3_lose_49;
	// System.Int32 checkpointCfg::Plot_off
	int32_t ___Plot_off_50;
	// System.Int32 checkpointCfg::enterPlotId
	int32_t ___enterPlotId_51;
	// System.Int32 checkpointCfg::passPlotId
	int32_t ___passPlotId_52;
	// System.String checkpointCfg::rewards
	String_t* ___rewards_53;
	// System.String checkpointCfg::des
	String_t* ___des_54;
	// System.Int32 checkpointCfg::AI
	int32_t ___AI_55;
	// System.Int32 checkpointCfg::ifAuto
	int32_t ___ifAuto_56;
	// System.Int32 checkpointCfg::ifMotion
	int32_t ___ifMotion_57;
	// System.Boolean checkpointCfg::ifStartAuto
	bool ___ifStartAuto_58;
	// System.Int32 checkpointCfg::Formation
	int32_t ___Formation_59;
	// System.Int32 checkpointCfg::MusicID
	int32_t ___MusicID_60;
	// System.Int32 checkpointCfg::bossAnimationId
	int32_t ___bossAnimationId_61;
	// System.Single checkpointCfg::isCameraSlow
	float ___isCameraSlow_62;
	// System.Single checkpointCfg::Camera_distance
	float ___Camera_distance_63;
	// System.Int32 checkpointCfg::camera_if_move
	int32_t ___camera_if_move_64;
	// System.Boolean checkpointCfg::isCameraAnimation
	bool ___isCameraAnimation_65;
	// System.Single checkpointCfg::Camera_distance_outattack
	float ___Camera_distance_outattack_66;
	// System.Single checkpointCfg::Camera_alternate_time
	float ___Camera_alternate_time_67;
	// System.Int32 checkpointCfg::Camera_time
	int32_t ___Camera_time_68;
	// System.String checkpointCfg::Camera_end
	String_t* ___Camera_end_69;
	// System.Single checkpointCfg::Camera_rx
	float ___Camera_rx_70;
	// System.Int32 checkpointCfg::Camera_rx__outattack
	int32_t ___Camera_rx__outattack_71;
	// System.Single checkpointCfg::Camera_ry
	float ___Camera_ry_72;
	// System.Single checkpointCfg::Camera_posDamping
	float ___Camera_posDamping_73;
	// System.Single checkpointCfg::Camera_MoveMax
	float ___Camera_MoveMax_74;
	// System.Single checkpointCfg::Camera_waittime
	float ___Camera_waittime_75;
	// System.Boolean checkpointCfg::Camera_disMove
	bool ___Camera_disMove_76;
	// System.Boolean checkpointCfg::IsExtryHero
	bool ___IsExtryHero_77;
	// System.String checkpointCfg::hero_Pos
	String_t* ___hero_Pos_78;
	// System.Single checkpointCfg::hero_Px
	float ___hero_Px_79;
	// System.String checkpointCfg::moster_Pos
	String_t* ___moster_Pos_80;
	// System.Single checkpointCfg::moster_Px
	float ___moster_Px_81;
	// System.Int32 checkpointCfg::friendNpc
	int32_t ___friendNpc_82;
	// System.Int32 checkpointCfg::power
	int32_t ___power_83;
	// System.Int32 checkpointCfg::maxSweepCount
	int32_t ___maxSweepCount_84;
	// System.Int32 checkpointCfg::maxCopper
	int32_t ___maxCopper_85;
	// System.Int32 checkpointCfg::need_checkpoint_id
	int32_t ___need_checkpoint_id_86;
	// System.Int32 checkpointCfg::need_normal_checkpoint_id
	int32_t ___need_normal_checkpoint_id_87;
	// System.Int32 checkpointCfg::need_role_level
	int32_t ___need_role_level_88;
	// System.Int32 checkpointCfg::bigType
	int32_t ___bigType_89;
	// System.Boolean checkpointCfg::isAutoCreatNpc
	bool ___isAutoCreatNpc_90;
	// System.Int32 checkpointCfg::bossId
	int32_t ___bossId_91;
	// System.Int32 checkpointCfg::isSpeedup
	int32_t ___isSpeedup_92;
	// System.String checkpointCfg::largeTip
	String_t* ___largeTip_93;
	// System.Int32 checkpointCfg::isTop
	int32_t ___isTop_94;
	// System.Int32 checkpointCfg::monsterwavecount
	int32_t ___monsterwavecount_95;
	// System.Int32 checkpointCfg::islvrepress
	int32_t ___islvrepress_96;
	// System.Boolean checkpointCfg::isShowNew
	bool ___isShowNew_97;
	// System.Int32 checkpointCfg::loc1_anger
	int32_t ___loc1_anger_98;
	// System.Int32 checkpointCfg::loc2_anger
	int32_t ___loc2_anger_99;
	// System.Int32 checkpointCfg::loc3_anger
	int32_t ___loc3_anger_100;
	// System.Int32 checkpointCfg::loc4_anger
	int32_t ___loc4_anger_101;
	// System.Int32 checkpointCfg::boxID
	int32_t ___boxID_102;
	// System.String checkpointCfg::treasure_pos
	String_t* ___treasure_pos_103;
	// System.Boolean checkpointCfg::is_superskill_prompt
	bool ___is_superskill_prompt_104;
	// System.Boolean checkpointCfg::ifUseHint
	bool ___ifUseHint_105;
	// System.String checkpointCfg::hintText
	String_t* ___hintText_106;
	// System.Int32 checkpointCfg::guide_or_not
	int32_t ___guide_or_not_107;
	// System.Int32 checkpointCfg::chapter_id
	int32_t ___chapter_id_108;
	// System.Int32 checkpointCfg::energy_rate
	int32_t ___energy_rate_109;
	// System.Single checkpointCfg::wait_time
	float ___wait_time_110;
	// System.Int32 checkpointCfg::item_id
	int32_t ___item_id_111;
	// System.Int32 checkpointCfg::is_time_not
	int32_t ___is_time_not_112;
	// System.String checkpointCfg::camera_limit
	String_t* ___camera_limit_113;
	// System.Int32 checkpointCfg::layer
	int32_t ___layer_114;
	// System.Boolean checkpointCfg::Is_Lead_skill
	bool ___Is_Lead_skill_115;
	// System.Boolean checkpointCfg::Is_relief
	bool ___Is_relief_116;
	// System.Boolean checkpointCfg::Is_close
	bool ___Is_close_117;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_mapID_1() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___mapID_1)); }
	inline int32_t get_mapID_1() const { return ___mapID_1; }
	inline int32_t* get_address_of_mapID_1() { return &___mapID_1; }
	inline void set_mapID_1(int32_t value)
	{
		___mapID_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_sceneName_3() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___sceneName_3)); }
	inline String_t* get_sceneName_3() const { return ___sceneName_3; }
	inline String_t** get_address_of_sceneName_3() { return &___sceneName_3; }
	inline void set_sceneName_3(String_t* value)
	{
		___sceneName_3 = value;
		Il2CppCodeGenWriteBarrier(&___sceneName_3, value);
	}

	inline static int32_t get_offset_of_iconType_4() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___iconType_4)); }
	inline int32_t get_iconType_4() const { return ___iconType_4; }
	inline int32_t* get_address_of_iconType_4() { return &___iconType_4; }
	inline void set_iconType_4(int32_t value)
	{
		___iconType_4 = value;
	}

	inline static int32_t get_offset_of_LevelIcon_5() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___LevelIcon_5)); }
	inline String_t* get_LevelIcon_5() const { return ___LevelIcon_5; }
	inline String_t** get_address_of_LevelIcon_5() { return &___LevelIcon_5; }
	inline void set_LevelIcon_5(String_t* value)
	{
		___LevelIcon_5 = value;
		Il2CppCodeGenWriteBarrier(&___LevelIcon_5, value);
	}

	inline static int32_t get_offset_of_localPos_6() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___localPos_6)); }
	inline String_t* get_localPos_6() const { return ___localPos_6; }
	inline String_t** get_address_of_localPos_6() { return &___localPos_6; }
	inline void set_localPos_6(String_t* value)
	{
		___localPos_6 = value;
		Il2CppCodeGenWriteBarrier(&___localPos_6, value);
	}

	inline static int32_t get_offset_of_Hatred_7() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___Hatred_7)); }
	inline int32_t get_Hatred_7() const { return ___Hatred_7; }
	inline int32_t* get_address_of_Hatred_7() { return &___Hatred_7; }
	inline void set_Hatred_7(int32_t value)
	{
		___Hatred_7 = value;
	}

	inline static int32_t get_offset_of_Super_Skill_Limit_8() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___Super_Skill_Limit_8)); }
	inline int32_t get_Super_Skill_Limit_8() const { return ___Super_Skill_Limit_8; }
	inline int32_t* get_address_of_Super_Skill_Limit_8() { return &___Super_Skill_Limit_8; }
	inline void set_Super_Skill_Limit_8(int32_t value)
	{
		___Super_Skill_Limit_8 = value;
	}

	inline static int32_t get_offset_of_attack_effect_9() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___attack_effect_9)); }
	inline int32_t get_attack_effect_9() const { return ___attack_effect_9; }
	inline int32_t* get_address_of_attack_effect_9() { return &___attack_effect_9; }
	inline void set_attack_effect_9(int32_t value)
	{
		___attack_effect_9 = value;
	}

	inline static int32_t get_offset_of_workpoint_10() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___workpoint_10)); }
	inline int32_t get_workpoint_10() const { return ___workpoint_10; }
	inline int32_t* get_address_of_workpoint_10() { return &___workpoint_10; }
	inline void set_workpoint_10(int32_t value)
	{
		___workpoint_10 = value;
	}

	inline static int32_t get_offset_of_preloadUIID_11() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___preloadUIID_11)); }
	inline String_t* get_preloadUIID_11() const { return ___preloadUIID_11; }
	inline String_t** get_address_of_preloadUIID_11() { return &___preloadUIID_11; }
	inline void set_preloadUIID_11(String_t* value)
	{
		___preloadUIID_11 = value;
		Il2CppCodeGenWriteBarrier(&___preloadUIID_11, value);
	}

	inline static int32_t get_offset_of_type_12() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___type_12)); }
	inline int32_t get_type_12() const { return ___type_12; }
	inline int32_t* get_address_of_type_12() { return &___type_12; }
	inline void set_type_12(int32_t value)
	{
		___type_12 = value;
	}

	inline static int32_t get_offset_of_panelType_13() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___panelType_13)); }
	inline int32_t get_panelType_13() const { return ___panelType_13; }
	inline int32_t* get_address_of_panelType_13() { return &___panelType_13; }
	inline void set_panelType_13(int32_t value)
	{
		___panelType_13 = value;
	}

	inline static int32_t get_offset_of_finishType_14() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___finishType_14)); }
	inline int32_t get_finishType_14() const { return ___finishType_14; }
	inline int32_t* get_address_of_finishType_14() { return &___finishType_14; }
	inline void set_finishType_14(int32_t value)
	{
		___finishType_14 = value;
	}

	inline static int32_t get_offset_of_effect_nf_15() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___effect_nf_15)); }
	inline int32_t get_effect_nf_15() const { return ___effect_nf_15; }
	inline int32_t* get_address_of_effect_nf_15() { return &___effect_nf_15; }
	inline void set_effect_nf_15(int32_t value)
	{
		___effect_nf_15 = value;
	}

	inline static int32_t get_offset_of_ifAppear_16() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___ifAppear_16)); }
	inline int32_t get_ifAppear_16() const { return ___ifAppear_16; }
	inline int32_t* get_address_of_ifAppear_16() { return &___ifAppear_16; }
	inline void set_ifAppear_16(int32_t value)
	{
		___ifAppear_16 = value;
	}

	inline static int32_t get_offset_of_time_17() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___time_17)); }
	inline int32_t get_time_17() const { return ___time_17; }
	inline int32_t* get_address_of_time_17() { return &___time_17; }
	inline void set_time_17(int32_t value)
	{
		___time_17 = value;
	}

	inline static int32_t get_offset_of_action_18() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___action_18)); }
	inline int32_t get_action_18() const { return ___action_18; }
	inline int32_t* get_address_of_action_18() { return &___action_18; }
	inline void set_action_18(int32_t value)
	{
		___action_18 = value;
	}

	inline static int32_t get_offset_of_exp_19() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___exp_19)); }
	inline int32_t get_exp_19() const { return ___exp_19; }
	inline int32_t* get_address_of_exp_19() { return &___exp_19; }
	inline void set_exp_19(int32_t value)
	{
		___exp_19 = value;
	}

	inline static int32_t get_offset_of_heroExp_20() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___heroExp_20)); }
	inline int32_t get_heroExp_20() const { return ___heroExp_20; }
	inline int32_t* get_address_of_heroExp_20() { return &___heroExp_20; }
	inline void set_heroExp_20(int32_t value)
	{
		___heroExp_20 = value;
	}

	inline static int32_t get_offset_of_copper_21() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___copper_21)); }
	inline int32_t get_copper_21() const { return ___copper_21; }
	inline int32_t* get_address_of_copper_21() { return &___copper_21; }
	inline void set_copper_21(int32_t value)
	{
		___copper_21 = value;
	}

	inline static int32_t get_offset_of_brushEnemy_22() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___brushEnemy_22)); }
	inline int32_t get_brushEnemy_22() const { return ___brushEnemy_22; }
	inline int32_t* get_address_of_brushEnemy_22() { return &___brushEnemy_22; }
	inline void set_brushEnemy_22(int32_t value)
	{
		___brushEnemy_22 = value;
	}

	inline static int32_t get_offset_of_passType_23() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___passType_23)); }
	inline int32_t get_passType_23() const { return ___passType_23; }
	inline int32_t* get_address_of_passType_23() { return &___passType_23; }
	inline void set_passType_23(int32_t value)
	{
		___passType_23 = value;
	}

	inline static int32_t get_offset_of_passValue_24() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___passValue_24)); }
	inline int32_t get_passValue_24() const { return ___passValue_24; }
	inline int32_t* get_address_of_passValue_24() { return &___passValue_24; }
	inline void set_passValue_24(int32_t value)
	{
		___passValue_24 = value;
	}

	inline static int32_t get_offset_of_loseValue_25() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___loseValue_25)); }
	inline String_t* get_loseValue_25() const { return ___loseValue_25; }
	inline String_t** get_address_of_loseValue_25() { return &___loseValue_25; }
	inline void set_loseValue_25(String_t* value)
	{
		___loseValue_25 = value;
		Il2CppCodeGenWriteBarrier(&___loseValue_25, value);
	}

	inline static int32_t get_offset_of_passAnimation_26() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___passAnimation_26)); }
	inline int32_t get_passAnimation_26() const { return ___passAnimation_26; }
	inline int32_t* get_address_of_passAnimation_26() { return &___passAnimation_26; }
	inline void set_passAnimation_26(int32_t value)
	{
		___passAnimation_26 = value;
	}

	inline static int32_t get_offset_of_delayFinishTime_27() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___delayFinishTime_27)); }
	inline float get_delayFinishTime_27() const { return ___delayFinishTime_27; }
	inline float* get_address_of_delayFinishTime_27() { return &___delayFinishTime_27; }
	inline void set_delayFinishTime_27(float value)
	{
		___delayFinishTime_27 = value;
	}

	inline static int32_t get_offset_of_passCameraAnimation_28() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___passCameraAnimation_28)); }
	inline String_t* get_passCameraAnimation_28() const { return ___passCameraAnimation_28; }
	inline String_t** get_address_of_passCameraAnimation_28() { return &___passCameraAnimation_28; }
	inline void set_passCameraAnimation_28(String_t* value)
	{
		___passCameraAnimation_28 = value;
		Il2CppCodeGenWriteBarrier(&___passCameraAnimation_28, value);
	}

	inline static int32_t get_offset_of_npcArr_29() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___npcArr_29)); }
	inline String_t* get_npcArr_29() const { return ___npcArr_29; }
	inline String_t** get_address_of_npcArr_29() { return &___npcArr_29; }
	inline void set_npcArr_29(String_t* value)
	{
		___npcArr_29 = value;
		Il2CppCodeGenWriteBarrier(&___npcArr_29, value);
	}

	inline static int32_t get_offset_of_rewardArr_30() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___rewardArr_30)); }
	inline String_t* get_rewardArr_30() const { return ___rewardArr_30; }
	inline String_t** get_address_of_rewardArr_30() { return &___rewardArr_30; }
	inline void set_rewardArr_30(String_t* value)
	{
		___rewardArr_30 = value;
		Il2CppCodeGenWriteBarrier(&___rewardArr_30, value);
	}

	inline static int32_t get_offset_of_dropId_31() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___dropId_31)); }
	inline int32_t get_dropId_31() const { return ___dropId_31; }
	inline int32_t* get_address_of_dropId_31() { return &___dropId_31; }
	inline void set_dropId_31(int32_t value)
	{
		___dropId_31 = value;
	}

	inline static int32_t get_offset_of_firsttimedrop_32() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___firsttimedrop_32)); }
	inline int32_t get_firsttimedrop_32() const { return ___firsttimedrop_32; }
	inline int32_t* get_address_of_firsttimedrop_32() { return &___firsttimedrop_32; }
	inline void set_firsttimedrop_32(int32_t value)
	{
		___firsttimedrop_32 = value;
	}

	inline static int32_t get_offset_of_firsttimedropnum_33() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___firsttimedropnum_33)); }
	inline int32_t get_firsttimedropnum_33() const { return ___firsttimedropnum_33; }
	inline int32_t* get_address_of_firsttimedropnum_33() { return &___firsttimedropnum_33; }
	inline void set_firsttimedropnum_33(int32_t value)
	{
		___firsttimedropnum_33 = value;
	}

	inline static int32_t get_offset_of_sweep_lv_34() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___sweep_lv_34)); }
	inline int32_t get_sweep_lv_34() const { return ___sweep_lv_34; }
	inline int32_t* get_address_of_sweep_lv_34() { return &___sweep_lv_34; }
	inline void set_sweep_lv_34(int32_t value)
	{
		___sweep_lv_34 = value;
	}

	inline static int32_t get_offset_of_swipedrop_35() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___swipedrop_35)); }
	inline String_t* get_swipedrop_35() const { return ___swipedrop_35; }
	inline String_t** get_address_of_swipedrop_35() { return &___swipedrop_35; }
	inline void set_swipedrop_35(String_t* value)
	{
		___swipedrop_35 = value;
		Il2CppCodeGenWriteBarrier(&___swipedrop_35, value);
	}

	inline static int32_t get_offset_of_swipedropcount_36() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___swipedropcount_36)); }
	inline String_t* get_swipedropcount_36() const { return ___swipedropcount_36; }
	inline String_t** get_address_of_swipedropcount_36() { return &___swipedropcount_36; }
	inline void set_swipedropcount_36(String_t* value)
	{
		___swipedropcount_36 = value;
		Il2CppCodeGenWriteBarrier(&___swipedropcount_36, value);
	}

	inline static int32_t get_offset_of_hidestartext_37() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___hidestartext_37)); }
	inline int32_t get_hidestartext_37() const { return ___hidestartext_37; }
	inline int32_t* get_address_of_hidestartext_37() { return &___hidestartext_37; }
	inline void set_hidestartext_37(int32_t value)
	{
		___hidestartext_37 = value;
	}

	inline static int32_t get_offset_of_gradeType1_38() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___gradeType1_38)); }
	inline int32_t get_gradeType1_38() const { return ___gradeType1_38; }
	inline int32_t* get_address_of_gradeType1_38() { return &___gradeType1_38; }
	inline void set_gradeType1_38(int32_t value)
	{
		___gradeType1_38 = value;
	}

	inline static int32_t get_offset_of_gradeValue1_39() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___gradeValue1_39)); }
	inline int32_t get_gradeValue1_39() const { return ___gradeValue1_39; }
	inline int32_t* get_address_of_gradeValue1_39() { return &___gradeValue1_39; }
	inline void set_gradeValue1_39(int32_t value)
	{
		___gradeValue1_39 = value;
	}

	inline static int32_t get_offset_of_gradetext1_40() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___gradetext1_40)); }
	inline String_t* get_gradetext1_40() const { return ___gradetext1_40; }
	inline String_t** get_address_of_gradetext1_40() { return &___gradetext1_40; }
	inline void set_gradetext1_40(String_t* value)
	{
		___gradetext1_40 = value;
		Il2CppCodeGenWriteBarrier(&___gradetext1_40, value);
	}

	inline static int32_t get_offset_of_gradetext1_lose_41() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___gradetext1_lose_41)); }
	inline String_t* get_gradetext1_lose_41() const { return ___gradetext1_lose_41; }
	inline String_t** get_address_of_gradetext1_lose_41() { return &___gradetext1_lose_41; }
	inline void set_gradetext1_lose_41(String_t* value)
	{
		___gradetext1_lose_41 = value;
		Il2CppCodeGenWriteBarrier(&___gradetext1_lose_41, value);
	}

	inline static int32_t get_offset_of_gradeType2_42() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___gradeType2_42)); }
	inline int32_t get_gradeType2_42() const { return ___gradeType2_42; }
	inline int32_t* get_address_of_gradeType2_42() { return &___gradeType2_42; }
	inline void set_gradeType2_42(int32_t value)
	{
		___gradeType2_42 = value;
	}

	inline static int32_t get_offset_of_gradeValue2_43() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___gradeValue2_43)); }
	inline int32_t get_gradeValue2_43() const { return ___gradeValue2_43; }
	inline int32_t* get_address_of_gradeValue2_43() { return &___gradeValue2_43; }
	inline void set_gradeValue2_43(int32_t value)
	{
		___gradeValue2_43 = value;
	}

	inline static int32_t get_offset_of_gradetext2_44() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___gradetext2_44)); }
	inline String_t* get_gradetext2_44() const { return ___gradetext2_44; }
	inline String_t** get_address_of_gradetext2_44() { return &___gradetext2_44; }
	inline void set_gradetext2_44(String_t* value)
	{
		___gradetext2_44 = value;
		Il2CppCodeGenWriteBarrier(&___gradetext2_44, value);
	}

	inline static int32_t get_offset_of_gradetext2_lose_45() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___gradetext2_lose_45)); }
	inline String_t* get_gradetext2_lose_45() const { return ___gradetext2_lose_45; }
	inline String_t** get_address_of_gradetext2_lose_45() { return &___gradetext2_lose_45; }
	inline void set_gradetext2_lose_45(String_t* value)
	{
		___gradetext2_lose_45 = value;
		Il2CppCodeGenWriteBarrier(&___gradetext2_lose_45, value);
	}

	inline static int32_t get_offset_of_gradeType3_46() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___gradeType3_46)); }
	inline int32_t get_gradeType3_46() const { return ___gradeType3_46; }
	inline int32_t* get_address_of_gradeType3_46() { return &___gradeType3_46; }
	inline void set_gradeType3_46(int32_t value)
	{
		___gradeType3_46 = value;
	}

	inline static int32_t get_offset_of_gradeValue3_47() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___gradeValue3_47)); }
	inline int32_t get_gradeValue3_47() const { return ___gradeValue3_47; }
	inline int32_t* get_address_of_gradeValue3_47() { return &___gradeValue3_47; }
	inline void set_gradeValue3_47(int32_t value)
	{
		___gradeValue3_47 = value;
	}

	inline static int32_t get_offset_of_gradetext3_48() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___gradetext3_48)); }
	inline String_t* get_gradetext3_48() const { return ___gradetext3_48; }
	inline String_t** get_address_of_gradetext3_48() { return &___gradetext3_48; }
	inline void set_gradetext3_48(String_t* value)
	{
		___gradetext3_48 = value;
		Il2CppCodeGenWriteBarrier(&___gradetext3_48, value);
	}

	inline static int32_t get_offset_of_gradetext3_lose_49() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___gradetext3_lose_49)); }
	inline String_t* get_gradetext3_lose_49() const { return ___gradetext3_lose_49; }
	inline String_t** get_address_of_gradetext3_lose_49() { return &___gradetext3_lose_49; }
	inline void set_gradetext3_lose_49(String_t* value)
	{
		___gradetext3_lose_49 = value;
		Il2CppCodeGenWriteBarrier(&___gradetext3_lose_49, value);
	}

	inline static int32_t get_offset_of_Plot_off_50() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___Plot_off_50)); }
	inline int32_t get_Plot_off_50() const { return ___Plot_off_50; }
	inline int32_t* get_address_of_Plot_off_50() { return &___Plot_off_50; }
	inline void set_Plot_off_50(int32_t value)
	{
		___Plot_off_50 = value;
	}

	inline static int32_t get_offset_of_enterPlotId_51() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___enterPlotId_51)); }
	inline int32_t get_enterPlotId_51() const { return ___enterPlotId_51; }
	inline int32_t* get_address_of_enterPlotId_51() { return &___enterPlotId_51; }
	inline void set_enterPlotId_51(int32_t value)
	{
		___enterPlotId_51 = value;
	}

	inline static int32_t get_offset_of_passPlotId_52() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___passPlotId_52)); }
	inline int32_t get_passPlotId_52() const { return ___passPlotId_52; }
	inline int32_t* get_address_of_passPlotId_52() { return &___passPlotId_52; }
	inline void set_passPlotId_52(int32_t value)
	{
		___passPlotId_52 = value;
	}

	inline static int32_t get_offset_of_rewards_53() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___rewards_53)); }
	inline String_t* get_rewards_53() const { return ___rewards_53; }
	inline String_t** get_address_of_rewards_53() { return &___rewards_53; }
	inline void set_rewards_53(String_t* value)
	{
		___rewards_53 = value;
		Il2CppCodeGenWriteBarrier(&___rewards_53, value);
	}

	inline static int32_t get_offset_of_des_54() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___des_54)); }
	inline String_t* get_des_54() const { return ___des_54; }
	inline String_t** get_address_of_des_54() { return &___des_54; }
	inline void set_des_54(String_t* value)
	{
		___des_54 = value;
		Il2CppCodeGenWriteBarrier(&___des_54, value);
	}

	inline static int32_t get_offset_of_AI_55() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___AI_55)); }
	inline int32_t get_AI_55() const { return ___AI_55; }
	inline int32_t* get_address_of_AI_55() { return &___AI_55; }
	inline void set_AI_55(int32_t value)
	{
		___AI_55 = value;
	}

	inline static int32_t get_offset_of_ifAuto_56() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___ifAuto_56)); }
	inline int32_t get_ifAuto_56() const { return ___ifAuto_56; }
	inline int32_t* get_address_of_ifAuto_56() { return &___ifAuto_56; }
	inline void set_ifAuto_56(int32_t value)
	{
		___ifAuto_56 = value;
	}

	inline static int32_t get_offset_of_ifMotion_57() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___ifMotion_57)); }
	inline int32_t get_ifMotion_57() const { return ___ifMotion_57; }
	inline int32_t* get_address_of_ifMotion_57() { return &___ifMotion_57; }
	inline void set_ifMotion_57(int32_t value)
	{
		___ifMotion_57 = value;
	}

	inline static int32_t get_offset_of_ifStartAuto_58() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___ifStartAuto_58)); }
	inline bool get_ifStartAuto_58() const { return ___ifStartAuto_58; }
	inline bool* get_address_of_ifStartAuto_58() { return &___ifStartAuto_58; }
	inline void set_ifStartAuto_58(bool value)
	{
		___ifStartAuto_58 = value;
	}

	inline static int32_t get_offset_of_Formation_59() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___Formation_59)); }
	inline int32_t get_Formation_59() const { return ___Formation_59; }
	inline int32_t* get_address_of_Formation_59() { return &___Formation_59; }
	inline void set_Formation_59(int32_t value)
	{
		___Formation_59 = value;
	}

	inline static int32_t get_offset_of_MusicID_60() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___MusicID_60)); }
	inline int32_t get_MusicID_60() const { return ___MusicID_60; }
	inline int32_t* get_address_of_MusicID_60() { return &___MusicID_60; }
	inline void set_MusicID_60(int32_t value)
	{
		___MusicID_60 = value;
	}

	inline static int32_t get_offset_of_bossAnimationId_61() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___bossAnimationId_61)); }
	inline int32_t get_bossAnimationId_61() const { return ___bossAnimationId_61; }
	inline int32_t* get_address_of_bossAnimationId_61() { return &___bossAnimationId_61; }
	inline void set_bossAnimationId_61(int32_t value)
	{
		___bossAnimationId_61 = value;
	}

	inline static int32_t get_offset_of_isCameraSlow_62() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___isCameraSlow_62)); }
	inline float get_isCameraSlow_62() const { return ___isCameraSlow_62; }
	inline float* get_address_of_isCameraSlow_62() { return &___isCameraSlow_62; }
	inline void set_isCameraSlow_62(float value)
	{
		___isCameraSlow_62 = value;
	}

	inline static int32_t get_offset_of_Camera_distance_63() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___Camera_distance_63)); }
	inline float get_Camera_distance_63() const { return ___Camera_distance_63; }
	inline float* get_address_of_Camera_distance_63() { return &___Camera_distance_63; }
	inline void set_Camera_distance_63(float value)
	{
		___Camera_distance_63 = value;
	}

	inline static int32_t get_offset_of_camera_if_move_64() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___camera_if_move_64)); }
	inline int32_t get_camera_if_move_64() const { return ___camera_if_move_64; }
	inline int32_t* get_address_of_camera_if_move_64() { return &___camera_if_move_64; }
	inline void set_camera_if_move_64(int32_t value)
	{
		___camera_if_move_64 = value;
	}

	inline static int32_t get_offset_of_isCameraAnimation_65() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___isCameraAnimation_65)); }
	inline bool get_isCameraAnimation_65() const { return ___isCameraAnimation_65; }
	inline bool* get_address_of_isCameraAnimation_65() { return &___isCameraAnimation_65; }
	inline void set_isCameraAnimation_65(bool value)
	{
		___isCameraAnimation_65 = value;
	}

	inline static int32_t get_offset_of_Camera_distance_outattack_66() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___Camera_distance_outattack_66)); }
	inline float get_Camera_distance_outattack_66() const { return ___Camera_distance_outattack_66; }
	inline float* get_address_of_Camera_distance_outattack_66() { return &___Camera_distance_outattack_66; }
	inline void set_Camera_distance_outattack_66(float value)
	{
		___Camera_distance_outattack_66 = value;
	}

	inline static int32_t get_offset_of_Camera_alternate_time_67() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___Camera_alternate_time_67)); }
	inline float get_Camera_alternate_time_67() const { return ___Camera_alternate_time_67; }
	inline float* get_address_of_Camera_alternate_time_67() { return &___Camera_alternate_time_67; }
	inline void set_Camera_alternate_time_67(float value)
	{
		___Camera_alternate_time_67 = value;
	}

	inline static int32_t get_offset_of_Camera_time_68() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___Camera_time_68)); }
	inline int32_t get_Camera_time_68() const { return ___Camera_time_68; }
	inline int32_t* get_address_of_Camera_time_68() { return &___Camera_time_68; }
	inline void set_Camera_time_68(int32_t value)
	{
		___Camera_time_68 = value;
	}

	inline static int32_t get_offset_of_Camera_end_69() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___Camera_end_69)); }
	inline String_t* get_Camera_end_69() const { return ___Camera_end_69; }
	inline String_t** get_address_of_Camera_end_69() { return &___Camera_end_69; }
	inline void set_Camera_end_69(String_t* value)
	{
		___Camera_end_69 = value;
		Il2CppCodeGenWriteBarrier(&___Camera_end_69, value);
	}

	inline static int32_t get_offset_of_Camera_rx_70() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___Camera_rx_70)); }
	inline float get_Camera_rx_70() const { return ___Camera_rx_70; }
	inline float* get_address_of_Camera_rx_70() { return &___Camera_rx_70; }
	inline void set_Camera_rx_70(float value)
	{
		___Camera_rx_70 = value;
	}

	inline static int32_t get_offset_of_Camera_rx__outattack_71() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___Camera_rx__outattack_71)); }
	inline int32_t get_Camera_rx__outattack_71() const { return ___Camera_rx__outattack_71; }
	inline int32_t* get_address_of_Camera_rx__outattack_71() { return &___Camera_rx__outattack_71; }
	inline void set_Camera_rx__outattack_71(int32_t value)
	{
		___Camera_rx__outattack_71 = value;
	}

	inline static int32_t get_offset_of_Camera_ry_72() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___Camera_ry_72)); }
	inline float get_Camera_ry_72() const { return ___Camera_ry_72; }
	inline float* get_address_of_Camera_ry_72() { return &___Camera_ry_72; }
	inline void set_Camera_ry_72(float value)
	{
		___Camera_ry_72 = value;
	}

	inline static int32_t get_offset_of_Camera_posDamping_73() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___Camera_posDamping_73)); }
	inline float get_Camera_posDamping_73() const { return ___Camera_posDamping_73; }
	inline float* get_address_of_Camera_posDamping_73() { return &___Camera_posDamping_73; }
	inline void set_Camera_posDamping_73(float value)
	{
		___Camera_posDamping_73 = value;
	}

	inline static int32_t get_offset_of_Camera_MoveMax_74() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___Camera_MoveMax_74)); }
	inline float get_Camera_MoveMax_74() const { return ___Camera_MoveMax_74; }
	inline float* get_address_of_Camera_MoveMax_74() { return &___Camera_MoveMax_74; }
	inline void set_Camera_MoveMax_74(float value)
	{
		___Camera_MoveMax_74 = value;
	}

	inline static int32_t get_offset_of_Camera_waittime_75() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___Camera_waittime_75)); }
	inline float get_Camera_waittime_75() const { return ___Camera_waittime_75; }
	inline float* get_address_of_Camera_waittime_75() { return &___Camera_waittime_75; }
	inline void set_Camera_waittime_75(float value)
	{
		___Camera_waittime_75 = value;
	}

	inline static int32_t get_offset_of_Camera_disMove_76() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___Camera_disMove_76)); }
	inline bool get_Camera_disMove_76() const { return ___Camera_disMove_76; }
	inline bool* get_address_of_Camera_disMove_76() { return &___Camera_disMove_76; }
	inline void set_Camera_disMove_76(bool value)
	{
		___Camera_disMove_76 = value;
	}

	inline static int32_t get_offset_of_IsExtryHero_77() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___IsExtryHero_77)); }
	inline bool get_IsExtryHero_77() const { return ___IsExtryHero_77; }
	inline bool* get_address_of_IsExtryHero_77() { return &___IsExtryHero_77; }
	inline void set_IsExtryHero_77(bool value)
	{
		___IsExtryHero_77 = value;
	}

	inline static int32_t get_offset_of_hero_Pos_78() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___hero_Pos_78)); }
	inline String_t* get_hero_Pos_78() const { return ___hero_Pos_78; }
	inline String_t** get_address_of_hero_Pos_78() { return &___hero_Pos_78; }
	inline void set_hero_Pos_78(String_t* value)
	{
		___hero_Pos_78 = value;
		Il2CppCodeGenWriteBarrier(&___hero_Pos_78, value);
	}

	inline static int32_t get_offset_of_hero_Px_79() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___hero_Px_79)); }
	inline float get_hero_Px_79() const { return ___hero_Px_79; }
	inline float* get_address_of_hero_Px_79() { return &___hero_Px_79; }
	inline void set_hero_Px_79(float value)
	{
		___hero_Px_79 = value;
	}

	inline static int32_t get_offset_of_moster_Pos_80() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___moster_Pos_80)); }
	inline String_t* get_moster_Pos_80() const { return ___moster_Pos_80; }
	inline String_t** get_address_of_moster_Pos_80() { return &___moster_Pos_80; }
	inline void set_moster_Pos_80(String_t* value)
	{
		___moster_Pos_80 = value;
		Il2CppCodeGenWriteBarrier(&___moster_Pos_80, value);
	}

	inline static int32_t get_offset_of_moster_Px_81() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___moster_Px_81)); }
	inline float get_moster_Px_81() const { return ___moster_Px_81; }
	inline float* get_address_of_moster_Px_81() { return &___moster_Px_81; }
	inline void set_moster_Px_81(float value)
	{
		___moster_Px_81 = value;
	}

	inline static int32_t get_offset_of_friendNpc_82() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___friendNpc_82)); }
	inline int32_t get_friendNpc_82() const { return ___friendNpc_82; }
	inline int32_t* get_address_of_friendNpc_82() { return &___friendNpc_82; }
	inline void set_friendNpc_82(int32_t value)
	{
		___friendNpc_82 = value;
	}

	inline static int32_t get_offset_of_power_83() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___power_83)); }
	inline int32_t get_power_83() const { return ___power_83; }
	inline int32_t* get_address_of_power_83() { return &___power_83; }
	inline void set_power_83(int32_t value)
	{
		___power_83 = value;
	}

	inline static int32_t get_offset_of_maxSweepCount_84() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___maxSweepCount_84)); }
	inline int32_t get_maxSweepCount_84() const { return ___maxSweepCount_84; }
	inline int32_t* get_address_of_maxSweepCount_84() { return &___maxSweepCount_84; }
	inline void set_maxSweepCount_84(int32_t value)
	{
		___maxSweepCount_84 = value;
	}

	inline static int32_t get_offset_of_maxCopper_85() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___maxCopper_85)); }
	inline int32_t get_maxCopper_85() const { return ___maxCopper_85; }
	inline int32_t* get_address_of_maxCopper_85() { return &___maxCopper_85; }
	inline void set_maxCopper_85(int32_t value)
	{
		___maxCopper_85 = value;
	}

	inline static int32_t get_offset_of_need_checkpoint_id_86() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___need_checkpoint_id_86)); }
	inline int32_t get_need_checkpoint_id_86() const { return ___need_checkpoint_id_86; }
	inline int32_t* get_address_of_need_checkpoint_id_86() { return &___need_checkpoint_id_86; }
	inline void set_need_checkpoint_id_86(int32_t value)
	{
		___need_checkpoint_id_86 = value;
	}

	inline static int32_t get_offset_of_need_normal_checkpoint_id_87() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___need_normal_checkpoint_id_87)); }
	inline int32_t get_need_normal_checkpoint_id_87() const { return ___need_normal_checkpoint_id_87; }
	inline int32_t* get_address_of_need_normal_checkpoint_id_87() { return &___need_normal_checkpoint_id_87; }
	inline void set_need_normal_checkpoint_id_87(int32_t value)
	{
		___need_normal_checkpoint_id_87 = value;
	}

	inline static int32_t get_offset_of_need_role_level_88() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___need_role_level_88)); }
	inline int32_t get_need_role_level_88() const { return ___need_role_level_88; }
	inline int32_t* get_address_of_need_role_level_88() { return &___need_role_level_88; }
	inline void set_need_role_level_88(int32_t value)
	{
		___need_role_level_88 = value;
	}

	inline static int32_t get_offset_of_bigType_89() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___bigType_89)); }
	inline int32_t get_bigType_89() const { return ___bigType_89; }
	inline int32_t* get_address_of_bigType_89() { return &___bigType_89; }
	inline void set_bigType_89(int32_t value)
	{
		___bigType_89 = value;
	}

	inline static int32_t get_offset_of_isAutoCreatNpc_90() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___isAutoCreatNpc_90)); }
	inline bool get_isAutoCreatNpc_90() const { return ___isAutoCreatNpc_90; }
	inline bool* get_address_of_isAutoCreatNpc_90() { return &___isAutoCreatNpc_90; }
	inline void set_isAutoCreatNpc_90(bool value)
	{
		___isAutoCreatNpc_90 = value;
	}

	inline static int32_t get_offset_of_bossId_91() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___bossId_91)); }
	inline int32_t get_bossId_91() const { return ___bossId_91; }
	inline int32_t* get_address_of_bossId_91() { return &___bossId_91; }
	inline void set_bossId_91(int32_t value)
	{
		___bossId_91 = value;
	}

	inline static int32_t get_offset_of_isSpeedup_92() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___isSpeedup_92)); }
	inline int32_t get_isSpeedup_92() const { return ___isSpeedup_92; }
	inline int32_t* get_address_of_isSpeedup_92() { return &___isSpeedup_92; }
	inline void set_isSpeedup_92(int32_t value)
	{
		___isSpeedup_92 = value;
	}

	inline static int32_t get_offset_of_largeTip_93() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___largeTip_93)); }
	inline String_t* get_largeTip_93() const { return ___largeTip_93; }
	inline String_t** get_address_of_largeTip_93() { return &___largeTip_93; }
	inline void set_largeTip_93(String_t* value)
	{
		___largeTip_93 = value;
		Il2CppCodeGenWriteBarrier(&___largeTip_93, value);
	}

	inline static int32_t get_offset_of_isTop_94() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___isTop_94)); }
	inline int32_t get_isTop_94() const { return ___isTop_94; }
	inline int32_t* get_address_of_isTop_94() { return &___isTop_94; }
	inline void set_isTop_94(int32_t value)
	{
		___isTop_94 = value;
	}

	inline static int32_t get_offset_of_monsterwavecount_95() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___monsterwavecount_95)); }
	inline int32_t get_monsterwavecount_95() const { return ___monsterwavecount_95; }
	inline int32_t* get_address_of_monsterwavecount_95() { return &___monsterwavecount_95; }
	inline void set_monsterwavecount_95(int32_t value)
	{
		___monsterwavecount_95 = value;
	}

	inline static int32_t get_offset_of_islvrepress_96() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___islvrepress_96)); }
	inline int32_t get_islvrepress_96() const { return ___islvrepress_96; }
	inline int32_t* get_address_of_islvrepress_96() { return &___islvrepress_96; }
	inline void set_islvrepress_96(int32_t value)
	{
		___islvrepress_96 = value;
	}

	inline static int32_t get_offset_of_isShowNew_97() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___isShowNew_97)); }
	inline bool get_isShowNew_97() const { return ___isShowNew_97; }
	inline bool* get_address_of_isShowNew_97() { return &___isShowNew_97; }
	inline void set_isShowNew_97(bool value)
	{
		___isShowNew_97 = value;
	}

	inline static int32_t get_offset_of_loc1_anger_98() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___loc1_anger_98)); }
	inline int32_t get_loc1_anger_98() const { return ___loc1_anger_98; }
	inline int32_t* get_address_of_loc1_anger_98() { return &___loc1_anger_98; }
	inline void set_loc1_anger_98(int32_t value)
	{
		___loc1_anger_98 = value;
	}

	inline static int32_t get_offset_of_loc2_anger_99() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___loc2_anger_99)); }
	inline int32_t get_loc2_anger_99() const { return ___loc2_anger_99; }
	inline int32_t* get_address_of_loc2_anger_99() { return &___loc2_anger_99; }
	inline void set_loc2_anger_99(int32_t value)
	{
		___loc2_anger_99 = value;
	}

	inline static int32_t get_offset_of_loc3_anger_100() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___loc3_anger_100)); }
	inline int32_t get_loc3_anger_100() const { return ___loc3_anger_100; }
	inline int32_t* get_address_of_loc3_anger_100() { return &___loc3_anger_100; }
	inline void set_loc3_anger_100(int32_t value)
	{
		___loc3_anger_100 = value;
	}

	inline static int32_t get_offset_of_loc4_anger_101() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___loc4_anger_101)); }
	inline int32_t get_loc4_anger_101() const { return ___loc4_anger_101; }
	inline int32_t* get_address_of_loc4_anger_101() { return &___loc4_anger_101; }
	inline void set_loc4_anger_101(int32_t value)
	{
		___loc4_anger_101 = value;
	}

	inline static int32_t get_offset_of_boxID_102() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___boxID_102)); }
	inline int32_t get_boxID_102() const { return ___boxID_102; }
	inline int32_t* get_address_of_boxID_102() { return &___boxID_102; }
	inline void set_boxID_102(int32_t value)
	{
		___boxID_102 = value;
	}

	inline static int32_t get_offset_of_treasure_pos_103() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___treasure_pos_103)); }
	inline String_t* get_treasure_pos_103() const { return ___treasure_pos_103; }
	inline String_t** get_address_of_treasure_pos_103() { return &___treasure_pos_103; }
	inline void set_treasure_pos_103(String_t* value)
	{
		___treasure_pos_103 = value;
		Il2CppCodeGenWriteBarrier(&___treasure_pos_103, value);
	}

	inline static int32_t get_offset_of_is_superskill_prompt_104() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___is_superskill_prompt_104)); }
	inline bool get_is_superskill_prompt_104() const { return ___is_superskill_prompt_104; }
	inline bool* get_address_of_is_superskill_prompt_104() { return &___is_superskill_prompt_104; }
	inline void set_is_superskill_prompt_104(bool value)
	{
		___is_superskill_prompt_104 = value;
	}

	inline static int32_t get_offset_of_ifUseHint_105() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___ifUseHint_105)); }
	inline bool get_ifUseHint_105() const { return ___ifUseHint_105; }
	inline bool* get_address_of_ifUseHint_105() { return &___ifUseHint_105; }
	inline void set_ifUseHint_105(bool value)
	{
		___ifUseHint_105 = value;
	}

	inline static int32_t get_offset_of_hintText_106() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___hintText_106)); }
	inline String_t* get_hintText_106() const { return ___hintText_106; }
	inline String_t** get_address_of_hintText_106() { return &___hintText_106; }
	inline void set_hintText_106(String_t* value)
	{
		___hintText_106 = value;
		Il2CppCodeGenWriteBarrier(&___hintText_106, value);
	}

	inline static int32_t get_offset_of_guide_or_not_107() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___guide_or_not_107)); }
	inline int32_t get_guide_or_not_107() const { return ___guide_or_not_107; }
	inline int32_t* get_address_of_guide_or_not_107() { return &___guide_or_not_107; }
	inline void set_guide_or_not_107(int32_t value)
	{
		___guide_or_not_107 = value;
	}

	inline static int32_t get_offset_of_chapter_id_108() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___chapter_id_108)); }
	inline int32_t get_chapter_id_108() const { return ___chapter_id_108; }
	inline int32_t* get_address_of_chapter_id_108() { return &___chapter_id_108; }
	inline void set_chapter_id_108(int32_t value)
	{
		___chapter_id_108 = value;
	}

	inline static int32_t get_offset_of_energy_rate_109() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___energy_rate_109)); }
	inline int32_t get_energy_rate_109() const { return ___energy_rate_109; }
	inline int32_t* get_address_of_energy_rate_109() { return &___energy_rate_109; }
	inline void set_energy_rate_109(int32_t value)
	{
		___energy_rate_109 = value;
	}

	inline static int32_t get_offset_of_wait_time_110() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___wait_time_110)); }
	inline float get_wait_time_110() const { return ___wait_time_110; }
	inline float* get_address_of_wait_time_110() { return &___wait_time_110; }
	inline void set_wait_time_110(float value)
	{
		___wait_time_110 = value;
	}

	inline static int32_t get_offset_of_item_id_111() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___item_id_111)); }
	inline int32_t get_item_id_111() const { return ___item_id_111; }
	inline int32_t* get_address_of_item_id_111() { return &___item_id_111; }
	inline void set_item_id_111(int32_t value)
	{
		___item_id_111 = value;
	}

	inline static int32_t get_offset_of_is_time_not_112() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___is_time_not_112)); }
	inline int32_t get_is_time_not_112() const { return ___is_time_not_112; }
	inline int32_t* get_address_of_is_time_not_112() { return &___is_time_not_112; }
	inline void set_is_time_not_112(int32_t value)
	{
		___is_time_not_112 = value;
	}

	inline static int32_t get_offset_of_camera_limit_113() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___camera_limit_113)); }
	inline String_t* get_camera_limit_113() const { return ___camera_limit_113; }
	inline String_t** get_address_of_camera_limit_113() { return &___camera_limit_113; }
	inline void set_camera_limit_113(String_t* value)
	{
		___camera_limit_113 = value;
		Il2CppCodeGenWriteBarrier(&___camera_limit_113, value);
	}

	inline static int32_t get_offset_of_layer_114() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___layer_114)); }
	inline int32_t get_layer_114() const { return ___layer_114; }
	inline int32_t* get_address_of_layer_114() { return &___layer_114; }
	inline void set_layer_114(int32_t value)
	{
		___layer_114 = value;
	}

	inline static int32_t get_offset_of_Is_Lead_skill_115() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___Is_Lead_skill_115)); }
	inline bool get_Is_Lead_skill_115() const { return ___Is_Lead_skill_115; }
	inline bool* get_address_of_Is_Lead_skill_115() { return &___Is_Lead_skill_115; }
	inline void set_Is_Lead_skill_115(bool value)
	{
		___Is_Lead_skill_115 = value;
	}

	inline static int32_t get_offset_of_Is_relief_116() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___Is_relief_116)); }
	inline bool get_Is_relief_116() const { return ___Is_relief_116; }
	inline bool* get_address_of_Is_relief_116() { return &___Is_relief_116; }
	inline void set_Is_relief_116(bool value)
	{
		___Is_relief_116 = value;
	}

	inline static int32_t get_offset_of_Is_close_117() { return static_cast<int32_t>(offsetof(checkpointCfg_t2816107964, ___Is_close_117)); }
	inline bool get_Is_close_117() const { return ___Is_close_117; }
	inline bool* get_address_of_Is_close_117() { return &___Is_close_117; }
	inline void set_Is_close_117(bool value)
	{
		___Is_close_117 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
