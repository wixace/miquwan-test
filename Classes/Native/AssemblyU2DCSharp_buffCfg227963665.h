﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// buffCfg
struct  buffCfg_t227963665  : public CsCfgBase_t69924517
{
public:
	// System.Int32 buffCfg::id
	int32_t ___id_0;
	// System.Single buffCfg::lasttime
	float ___lasttime_1;
	// System.Int32 buffCfg::type
	int32_t ___type_2;
	// System.Boolean buffCfg::break_skill
	bool ___break_skill_3;
	// System.Int32 buffCfg::anti_type
	int32_t ___anti_type_4;
	// System.Boolean buffCfg::anti_repeat
	bool ___anti_repeat_5;
	// System.Int32 buffCfg::main_type
	int32_t ___main_type_6;
	// System.Boolean buffCfg::self_repeat
	bool ___self_repeat_7;
	// System.Boolean buffCfg::self_replace
	bool ___self_replace_8;
	// System.Int32 buffCfg::fun1
	int32_t ___fun1_9;
	// System.Int32 buffCfg::para1
	int32_t ___para1_10;
	// System.Int32 buffCfg::fun2
	int32_t ___fun2_11;
	// System.Int32 buffCfg::para2
	int32_t ___para2_12;
	// System.Int32 buffCfg::fun3
	int32_t ___fun3_13;
	// System.Int32 buffCfg::para3
	int32_t ___para3_14;
	// System.Int32 buffCfg::fun4
	int32_t ___fun4_15;
	// System.Int32 buffCfg::para4
	int32_t ___para4_16;
	// System.Int32 buffCfg::effid
	int32_t ___effid_17;
	// System.Boolean buffCfg::permanent
	bool ___permanent_18;
	// System.String buffCfg::name
	String_t* ___name_19;
	// System.Single buffCfg::range
	float ___range_20;
	// System.Int32 buffCfg::forcountry
	int32_t ___forcountry_21;
	// System.Int32 buffCfg::forelement
	int32_t ___forelement_22;
	// System.Int32 buffCfg::forsex
	int32_t ___forsex_23;
	// System.Int32 buffCfg::atktype
	int32_t ___atktype_24;
	// System.Int32 buffCfg::forBoss
	int32_t ___forBoss_25;
	// System.Int32 buffCfg::forenv
	int32_t ___forenv_26;
	// System.Int32 buffCfg::showwordfortype
	int32_t ___showwordfortype_27;
	// System.String buffCfg::effect_text
	String_t* ___effect_text_28;
	// System.String buffCfg::path_text
	String_t* ___path_text_29;
	// System.String buffCfg::heaven_awaken
	String_t* ___heaven_awaken_30;
	// System.Int32 buffCfg::icon_effect
	int32_t ___icon_effect_31;
	// System.Int32 buffCfg::buff_layer
	int32_t ___buff_layer_32;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_lasttime_1() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___lasttime_1)); }
	inline float get_lasttime_1() const { return ___lasttime_1; }
	inline float* get_address_of_lasttime_1() { return &___lasttime_1; }
	inline void set_lasttime_1(float value)
	{
		___lasttime_1 = value;
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_break_skill_3() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___break_skill_3)); }
	inline bool get_break_skill_3() const { return ___break_skill_3; }
	inline bool* get_address_of_break_skill_3() { return &___break_skill_3; }
	inline void set_break_skill_3(bool value)
	{
		___break_skill_3 = value;
	}

	inline static int32_t get_offset_of_anti_type_4() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___anti_type_4)); }
	inline int32_t get_anti_type_4() const { return ___anti_type_4; }
	inline int32_t* get_address_of_anti_type_4() { return &___anti_type_4; }
	inline void set_anti_type_4(int32_t value)
	{
		___anti_type_4 = value;
	}

	inline static int32_t get_offset_of_anti_repeat_5() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___anti_repeat_5)); }
	inline bool get_anti_repeat_5() const { return ___anti_repeat_5; }
	inline bool* get_address_of_anti_repeat_5() { return &___anti_repeat_5; }
	inline void set_anti_repeat_5(bool value)
	{
		___anti_repeat_5 = value;
	}

	inline static int32_t get_offset_of_main_type_6() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___main_type_6)); }
	inline int32_t get_main_type_6() const { return ___main_type_6; }
	inline int32_t* get_address_of_main_type_6() { return &___main_type_6; }
	inline void set_main_type_6(int32_t value)
	{
		___main_type_6 = value;
	}

	inline static int32_t get_offset_of_self_repeat_7() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___self_repeat_7)); }
	inline bool get_self_repeat_7() const { return ___self_repeat_7; }
	inline bool* get_address_of_self_repeat_7() { return &___self_repeat_7; }
	inline void set_self_repeat_7(bool value)
	{
		___self_repeat_7 = value;
	}

	inline static int32_t get_offset_of_self_replace_8() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___self_replace_8)); }
	inline bool get_self_replace_8() const { return ___self_replace_8; }
	inline bool* get_address_of_self_replace_8() { return &___self_replace_8; }
	inline void set_self_replace_8(bool value)
	{
		___self_replace_8 = value;
	}

	inline static int32_t get_offset_of_fun1_9() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___fun1_9)); }
	inline int32_t get_fun1_9() const { return ___fun1_9; }
	inline int32_t* get_address_of_fun1_9() { return &___fun1_9; }
	inline void set_fun1_9(int32_t value)
	{
		___fun1_9 = value;
	}

	inline static int32_t get_offset_of_para1_10() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___para1_10)); }
	inline int32_t get_para1_10() const { return ___para1_10; }
	inline int32_t* get_address_of_para1_10() { return &___para1_10; }
	inline void set_para1_10(int32_t value)
	{
		___para1_10 = value;
	}

	inline static int32_t get_offset_of_fun2_11() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___fun2_11)); }
	inline int32_t get_fun2_11() const { return ___fun2_11; }
	inline int32_t* get_address_of_fun2_11() { return &___fun2_11; }
	inline void set_fun2_11(int32_t value)
	{
		___fun2_11 = value;
	}

	inline static int32_t get_offset_of_para2_12() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___para2_12)); }
	inline int32_t get_para2_12() const { return ___para2_12; }
	inline int32_t* get_address_of_para2_12() { return &___para2_12; }
	inline void set_para2_12(int32_t value)
	{
		___para2_12 = value;
	}

	inline static int32_t get_offset_of_fun3_13() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___fun3_13)); }
	inline int32_t get_fun3_13() const { return ___fun3_13; }
	inline int32_t* get_address_of_fun3_13() { return &___fun3_13; }
	inline void set_fun3_13(int32_t value)
	{
		___fun3_13 = value;
	}

	inline static int32_t get_offset_of_para3_14() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___para3_14)); }
	inline int32_t get_para3_14() const { return ___para3_14; }
	inline int32_t* get_address_of_para3_14() { return &___para3_14; }
	inline void set_para3_14(int32_t value)
	{
		___para3_14 = value;
	}

	inline static int32_t get_offset_of_fun4_15() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___fun4_15)); }
	inline int32_t get_fun4_15() const { return ___fun4_15; }
	inline int32_t* get_address_of_fun4_15() { return &___fun4_15; }
	inline void set_fun4_15(int32_t value)
	{
		___fun4_15 = value;
	}

	inline static int32_t get_offset_of_para4_16() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___para4_16)); }
	inline int32_t get_para4_16() const { return ___para4_16; }
	inline int32_t* get_address_of_para4_16() { return &___para4_16; }
	inline void set_para4_16(int32_t value)
	{
		___para4_16 = value;
	}

	inline static int32_t get_offset_of_effid_17() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___effid_17)); }
	inline int32_t get_effid_17() const { return ___effid_17; }
	inline int32_t* get_address_of_effid_17() { return &___effid_17; }
	inline void set_effid_17(int32_t value)
	{
		___effid_17 = value;
	}

	inline static int32_t get_offset_of_permanent_18() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___permanent_18)); }
	inline bool get_permanent_18() const { return ___permanent_18; }
	inline bool* get_address_of_permanent_18() { return &___permanent_18; }
	inline void set_permanent_18(bool value)
	{
		___permanent_18 = value;
	}

	inline static int32_t get_offset_of_name_19() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___name_19)); }
	inline String_t* get_name_19() const { return ___name_19; }
	inline String_t** get_address_of_name_19() { return &___name_19; }
	inline void set_name_19(String_t* value)
	{
		___name_19 = value;
		Il2CppCodeGenWriteBarrier(&___name_19, value);
	}

	inline static int32_t get_offset_of_range_20() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___range_20)); }
	inline float get_range_20() const { return ___range_20; }
	inline float* get_address_of_range_20() { return &___range_20; }
	inline void set_range_20(float value)
	{
		___range_20 = value;
	}

	inline static int32_t get_offset_of_forcountry_21() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___forcountry_21)); }
	inline int32_t get_forcountry_21() const { return ___forcountry_21; }
	inline int32_t* get_address_of_forcountry_21() { return &___forcountry_21; }
	inline void set_forcountry_21(int32_t value)
	{
		___forcountry_21 = value;
	}

	inline static int32_t get_offset_of_forelement_22() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___forelement_22)); }
	inline int32_t get_forelement_22() const { return ___forelement_22; }
	inline int32_t* get_address_of_forelement_22() { return &___forelement_22; }
	inline void set_forelement_22(int32_t value)
	{
		___forelement_22 = value;
	}

	inline static int32_t get_offset_of_forsex_23() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___forsex_23)); }
	inline int32_t get_forsex_23() const { return ___forsex_23; }
	inline int32_t* get_address_of_forsex_23() { return &___forsex_23; }
	inline void set_forsex_23(int32_t value)
	{
		___forsex_23 = value;
	}

	inline static int32_t get_offset_of_atktype_24() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___atktype_24)); }
	inline int32_t get_atktype_24() const { return ___atktype_24; }
	inline int32_t* get_address_of_atktype_24() { return &___atktype_24; }
	inline void set_atktype_24(int32_t value)
	{
		___atktype_24 = value;
	}

	inline static int32_t get_offset_of_forBoss_25() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___forBoss_25)); }
	inline int32_t get_forBoss_25() const { return ___forBoss_25; }
	inline int32_t* get_address_of_forBoss_25() { return &___forBoss_25; }
	inline void set_forBoss_25(int32_t value)
	{
		___forBoss_25 = value;
	}

	inline static int32_t get_offset_of_forenv_26() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___forenv_26)); }
	inline int32_t get_forenv_26() const { return ___forenv_26; }
	inline int32_t* get_address_of_forenv_26() { return &___forenv_26; }
	inline void set_forenv_26(int32_t value)
	{
		___forenv_26 = value;
	}

	inline static int32_t get_offset_of_showwordfortype_27() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___showwordfortype_27)); }
	inline int32_t get_showwordfortype_27() const { return ___showwordfortype_27; }
	inline int32_t* get_address_of_showwordfortype_27() { return &___showwordfortype_27; }
	inline void set_showwordfortype_27(int32_t value)
	{
		___showwordfortype_27 = value;
	}

	inline static int32_t get_offset_of_effect_text_28() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___effect_text_28)); }
	inline String_t* get_effect_text_28() const { return ___effect_text_28; }
	inline String_t** get_address_of_effect_text_28() { return &___effect_text_28; }
	inline void set_effect_text_28(String_t* value)
	{
		___effect_text_28 = value;
		Il2CppCodeGenWriteBarrier(&___effect_text_28, value);
	}

	inline static int32_t get_offset_of_path_text_29() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___path_text_29)); }
	inline String_t* get_path_text_29() const { return ___path_text_29; }
	inline String_t** get_address_of_path_text_29() { return &___path_text_29; }
	inline void set_path_text_29(String_t* value)
	{
		___path_text_29 = value;
		Il2CppCodeGenWriteBarrier(&___path_text_29, value);
	}

	inline static int32_t get_offset_of_heaven_awaken_30() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___heaven_awaken_30)); }
	inline String_t* get_heaven_awaken_30() const { return ___heaven_awaken_30; }
	inline String_t** get_address_of_heaven_awaken_30() { return &___heaven_awaken_30; }
	inline void set_heaven_awaken_30(String_t* value)
	{
		___heaven_awaken_30 = value;
		Il2CppCodeGenWriteBarrier(&___heaven_awaken_30, value);
	}

	inline static int32_t get_offset_of_icon_effect_31() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___icon_effect_31)); }
	inline int32_t get_icon_effect_31() const { return ___icon_effect_31; }
	inline int32_t* get_address_of_icon_effect_31() { return &___icon_effect_31; }
	inline void set_icon_effect_31(int32_t value)
	{
		___icon_effect_31 = value;
	}

	inline static int32_t get_offset_of_buff_layer_32() { return static_cast<int32_t>(offsetof(buffCfg_t227963665, ___buff_layer_32)); }
	inline int32_t get_buff_layer_32() const { return ___buff_layer_32; }
	inline int32_t* get_address_of_buff_layer_32() { return &___buff_layer_32; }
	inline void set_buff_layer_32(int32_t value)
	{
		___buff_layer_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
