﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._7a14430683901f31beb44924cf7c7846
struct _7a14430683901f31beb44924cf7c7846_t949903102;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__7a14430683901f31beb44924c949903102.h"

// System.Void Little._7a14430683901f31beb44924cf7c7846::.ctor()
extern "C"  void _7a14430683901f31beb44924cf7c7846__ctor_m2907489519 (_7a14430683901f31beb44924cf7c7846_t949903102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7a14430683901f31beb44924cf7c7846::_7a14430683901f31beb44924cf7c7846m2(System.Int32)
extern "C"  int32_t _7a14430683901f31beb44924cf7c7846__7a14430683901f31beb44924cf7c7846m2_m1634058457 (_7a14430683901f31beb44924cf7c7846_t949903102 * __this, int32_t ____7a14430683901f31beb44924cf7c7846a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7a14430683901f31beb44924cf7c7846::_7a14430683901f31beb44924cf7c7846m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _7a14430683901f31beb44924cf7c7846__7a14430683901f31beb44924cf7c7846m_m1573293565 (_7a14430683901f31beb44924cf7c7846_t949903102 * __this, int32_t ____7a14430683901f31beb44924cf7c7846a0, int32_t ____7a14430683901f31beb44924cf7c7846601, int32_t ____7a14430683901f31beb44924cf7c7846c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7a14430683901f31beb44924cf7c7846::ilo__7a14430683901f31beb44924cf7c7846m21(Little._7a14430683901f31beb44924cf7c7846,System.Int32)
extern "C"  int32_t _7a14430683901f31beb44924cf7c7846_ilo__7a14430683901f31beb44924cf7c7846m21_m3320766085 (Il2CppObject * __this /* static, unused */, _7a14430683901f31beb44924cf7c7846_t949903102 * ____this0, int32_t ____7a14430683901f31beb44924cf7c7846a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
