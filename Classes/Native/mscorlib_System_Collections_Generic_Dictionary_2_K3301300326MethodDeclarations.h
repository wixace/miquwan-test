﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Object>
struct KeyCollection_t3301300326;
// System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>
struct Dictionary_2_t1674540875;
// System.Collections.Generic.IEnumerator`1<UIModelDisplayType>
struct IEnumerator_1_t3803617728;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// UIModelDisplayType[]
struct UIModelDisplayTypeU5BU5D_t737309086;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2289476929.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m1848708705_gshared (KeyCollection_t3301300326 * __this, Dictionary_2_t1674540875 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m1848708705(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3301300326 *, Dictionary_2_t1674540875 *, const MethodInfo*))KeyCollection__ctor_m1848708705_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2494239317_gshared (KeyCollection_t3301300326 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2494239317(__this, ___item0, method) ((  void (*) (KeyCollection_t3301300326 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2494239317_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1472973068_gshared (KeyCollection_t3301300326 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1472973068(__this, method) ((  void (*) (KeyCollection_t3301300326 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1472973068_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3964704537_gshared (KeyCollection_t3301300326 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3964704537(__this, ___item0, method) ((  bool (*) (KeyCollection_t3301300326 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3964704537_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2614753982_gshared (KeyCollection_t3301300326 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2614753982(__this, ___item0, method) ((  bool (*) (KeyCollection_t3301300326 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2614753982_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m37742110_gshared (KeyCollection_t3301300326 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m37742110(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3301300326 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m37742110_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3454827390_gshared (KeyCollection_t3301300326 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m3454827390(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3301300326 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3454827390_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1451607565_gshared (KeyCollection_t3301300326 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1451607565(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3301300326 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1451607565_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3326127162_gshared (KeyCollection_t3301300326 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3326127162(__this, method) ((  bool (*) (KeyCollection_t3301300326 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3326127162_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3274167660_gshared (KeyCollection_t3301300326 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3274167660(__this, method) ((  bool (*) (KeyCollection_t3301300326 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3274167660_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3354365790_gshared (KeyCollection_t3301300326 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3354365790(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3301300326 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3354365790_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2502441238_gshared (KeyCollection_t3301300326 * __this, UIModelDisplayTypeU5BU5D_t737309086* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2502441238(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3301300326 *, UIModelDisplayTypeU5BU5D_t737309086*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2502441238_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2289476929  KeyCollection_GetEnumerator_m2827792547_gshared (KeyCollection_t3301300326 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m2827792547(__this, method) ((  Enumerator_t2289476929  (*) (KeyCollection_t3301300326 *, const MethodInfo*))KeyCollection_GetEnumerator_m2827792547_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<UIModelDisplayType,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3442814886_gshared (KeyCollection_t3301300326 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m3442814886(__this, method) ((  int32_t (*) (KeyCollection_t3301300326 *, const MethodInfo*))KeyCollection_get_Count_m3442814886_gshared)(__this, method)
