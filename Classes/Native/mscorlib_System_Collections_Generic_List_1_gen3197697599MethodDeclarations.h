﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::.ctor()
#define List_1__ctor_m656120154(__this, method) ((  void (*) (List_1_t3197697599 *, const MethodInfo*))List_1__ctor_m1239231859_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m532666044(__this, ___collection0, method) ((  void (*) (List_1_t3197697599 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::.ctor(System.Int32)
#define List_1__ctor_m2126995124(__this, ___capacity0, method) ((  void (*) (List_1_t3197697599 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::.cctor()
#define List_1__cctor_m857088554(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<TimeUpdateVo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m681882925(__this, method) ((  Il2CppObject* (*) (List_1_t3197697599 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m2439895233(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3197697599 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<TimeUpdateVo>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m766865424(__this, method) ((  Il2CppObject * (*) (List_1_t3197697599 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<TimeUpdateVo>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m410185517(__this, ___item0, method) ((  int32_t (*) (List_1_t3197697599 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<TimeUpdateVo>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1689228595(__this, ___item0, method) ((  bool (*) (List_1_t3197697599 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<TimeUpdateVo>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1763559173(__this, ___item0, method) ((  int32_t (*) (List_1_t3197697599 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m2206398200(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3197697599 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m1811070768(__this, ___item0, method) ((  void (*) (List_1_t3197697599 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<TimeUpdateVo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4214546804(__this, method) ((  bool (*) (List_1_t3197697599 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<TimeUpdateVo>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3967209801(__this, method) ((  bool (*) (List_1_t3197697599 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<TimeUpdateVo>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m3763299323(__this, method) ((  Il2CppObject * (*) (List_1_t3197697599 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<TimeUpdateVo>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m781182690(__this, method) ((  bool (*) (List_1_t3197697599 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<TimeUpdateVo>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m617755607(__this, method) ((  bool (*) (List_1_t3197697599 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<TimeUpdateVo>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1233797826(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3197697599 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m3543745743(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3197697599 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::Add(T)
#define List_1_Add_m350480970(__this, ___item0, method) ((  void (*) (List_1_t3197697599 *, TimeUpdateVo_t1829512047 *, const MethodInfo*))List_1_Add_m933592675_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m2542176887(__this, ___newCount0, method) ((  void (*) (List_1_t3197697599 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m3218946320(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3197697599 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m590371457_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m4145497461(__this, ___collection0, method) ((  void (*) (List_1_t3197697599 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m3406469685(__this, ___enumerable0, method) ((  void (*) (List_1_t3197697599 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m2800879426(__this, ___collection0, method) ((  void (*) (List_1_t3197697599 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<TimeUpdateVo>::AsReadOnly()
#define List_1_AsReadOnly_m2550227587(__this, method) ((  ReadOnlyCollection_1_t3386589583 * (*) (List_1_t3197697599 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<TimeUpdateVo>::BinarySearch(T)
#define List_1_BinarySearch_m1728381168(__this, ___item0, method) ((  int32_t (*) (List_1_t3197697599 *, TimeUpdateVo_t1829512047 *, const MethodInfo*))List_1_BinarySearch_m2761349225_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::Clear()
#define List_1_Clear_m2357220741(__this, method) ((  void (*) (List_1_t3197697599 *, const MethodInfo*))List_1_Clear_m2940332446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<TimeUpdateVo>::Contains(T)
#define List_1_Contains_m1031759872(__this, ___item0, method) ((  bool (*) (List_1_t3197697599 *, TimeUpdateVo_t1829512047 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m4049281836(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3197697599 *, TimeUpdateVoU5BU5D_t3056897526*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<TimeUpdateVo>::Find(System.Predicate`1<T>)
#define List_1_Find_m1991644634(__this, ___match0, method) ((  TimeUpdateVo_t1829512047 * (*) (List_1_t3197697599 *, Predicate_1_t1440568930 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m3663580471(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1440568930 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<TimeUpdateVo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m4137416212(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3197697599 *, int32_t, int32_t, Predicate_1_t1440568930 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<TimeUpdateVo>::GetEnumerator()
#define List_1_GetEnumerator_m2653694269(__this, method) ((  Enumerator_t3217370369  (*) (List_1_t3197697599 *, const MethodInfo*))List_1_GetEnumerator_m1919240000_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<TimeUpdateVo>::IndexOf(T)
#define List_1_IndexOf_m3620759032(__this, ___item0, method) ((  int32_t (*) (List_1_t3197697599 *, TimeUpdateVo_t1829512047 *, const MethodInfo*))List_1_IndexOf_m2776798407_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m2846035331(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3197697599 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m3795649020(__this, ___index0, method) ((  void (*) (List_1_t3197697599 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::Insert(System.Int32,T)
#define List_1_Insert_m3078624419(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3197697599 *, int32_t, TimeUpdateVo_t1829512047 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m2915045080(__this, ___collection0, method) ((  void (*) (List_1_t3197697599 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<TimeUpdateVo>::Remove(T)
#define List_1_Remove_m2919929643(__this, ___item0, method) ((  bool (*) (List_1_t3197697599 *, TimeUpdateVo_t1829512047 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<TimeUpdateVo>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m3396199355(__this, ___match0, method) ((  int32_t (*) (List_1_t3197697599 *, Predicate_1_t1440568930 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m952477289(__this, ___index0, method) ((  void (*) (List_1_t3197697599 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2208877785_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m738300044(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3197697599 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m856857915_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::Reverse()
#define List_1_Reverse_m2897833315(__this, method) ((  void (*) (List_1_t3197697599 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::Sort()
#define List_1_Sort_m1156613407(__this, method) ((  void (*) (List_1_t3197697599 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m311414693(__this, ___comparer0, method) ((  void (*) (List_1_t3197697599 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m1528193074(__this, ___comparison0, method) ((  void (*) (List_1_t3197697599 *, Comparison_1_t545873234 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<TimeUpdateVo>::ToArray()
#define List_1_ToArray_m1855481212(__this, method) ((  TimeUpdateVoU5BU5D_t3056897526* (*) (List_1_t3197697599 *, const MethodInfo*))List_1_ToArray_m1449333879_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::TrimExcess()
#define List_1_TrimExcess_m3184071032(__this, method) ((  void (*) (List_1_t3197697599 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<TimeUpdateVo>::get_Capacity()
#define List_1_get_Capacity_m3973882408(__this, method) ((  int32_t (*) (List_1_t3197697599 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m4047333001(__this, ___value0, method) ((  void (*) (List_1_t3197697599 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<TimeUpdateVo>::get_Count()
#define List_1_get_Count_m4270853484(__this, method) ((  int32_t (*) (List_1_t3197697599 *, const MethodInfo*))List_1_get_Count_m3549598589_gshared)(__this, method)
// T System.Collections.Generic.List`1<TimeUpdateVo>::get_Item(System.Int32)
#define List_1_get_Item_m1013194217(__this, ___index0, method) ((  TimeUpdateVo_t1829512047 * (*) (List_1_t3197697599 *, int32_t, const MethodInfo*))List_1_get_Item_m404118264_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<TimeUpdateVo>::set_Item(System.Int32,T)
#define List_1_set_Item_m1135196346(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3197697599 *, int32_t, TimeUpdateVo_t1829512047 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
