﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Serialization.AstarSerializer
struct AstarSerializer_t1384811327;
// Pathfinding.AstarData
struct AstarData_t3283402719;
// Pathfinding.Serialization.SerializeSettings
struct SerializeSettings_t2480699453;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// Pathfinding.NavGraph[]
struct NavGraphU5BU5D_t850130684;
// Pathfinding.UserConnection[]
struct UserConnectionU5BU5D_t328197926;
// Pathfinding.NavGraph
struct NavGraph_t1254319713;
// Pathfinding.GraphEditorBase[]
struct GraphEditorBaseU5BU5D_t3067496667;
// System.IO.BinaryReader
struct BinaryReader_t3990958868;
// System.String
struct String_t;
// Pathfinding.Ionic.Zip.ZipEntry
struct ZipEntry_t2786874973;
// Pathfinding.Serialization.GraphMeta
struct GraphMeta_t513097101;
// Pathfinding.Serialization.GraphSerializationContext
struct GraphSerializationContext_t3256954663;
// Pathfinding.GraphNodeDelegateCancelable
struct GraphNodeDelegateCancelable_t3591372971;
// System.Version
struct Version_t763695022;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_AstarData3283402719.h"
#include "AssemblyU2DCSharp_Pathfinding_Serialization_Serial2480699453.h"
#include "AssemblyU2DCSharp_Pathfinding_NavGraph1254319713.h"
#include "mscorlib_System_IO_BinaryReader3990958868.h"
#include "Pathfinding_Ionic_Zip_Reduced_Pathfinding_Ionic_Zi2786874973.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Pathfinding_Serialization_AstarS1384811327.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_Guid3584625871.h"
#include "AssemblyU2DCSharp_Pathfinding_Serialization_GraphS3256954663.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNodeDelegateCan3591372971.h"
#include "AssemblyU2DCSharp_Pathfinding_Serialization_GraphMe513097101.h"
#include "mscorlib_System_Type2863145774.h"

// System.Void Pathfinding.Serialization.AstarSerializer::.ctor(Pathfinding.AstarData)
extern "C"  void AstarSerializer__ctor_m3037339729 (AstarSerializer_t1384811327 * __this, AstarData_t3283402719 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::.ctor(Pathfinding.AstarData,Pathfinding.Serialization.SerializeSettings)
extern "C"  void AstarSerializer__ctor_m259497922 (AstarSerializer_t1384811327 * __this, AstarData_t3283402719 * ___data0, SerializeSettings_t2480699453 * ___settings1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::.cctor()
extern "C"  void AstarSerializer__cctor_m323038335 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder Pathfinding.Serialization.AstarSerializer::GetStringBuilder()
extern "C"  StringBuilder_t243639308 * AstarSerializer_GetStringBuilder_m1967744127 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::AddChecksum(System.Byte[])
extern "C"  void AstarSerializer_AddChecksum_m3301231865 (AstarSerializer_t1384811327 * __this, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.Serialization.AstarSerializer::GetChecksum()
extern "C"  uint32_t AstarSerializer_GetChecksum_m1025834080 (AstarSerializer_t1384811327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::OpenSerialize()
extern "C"  void AstarSerializer_OpenSerialize_m3612353858 (AstarSerializer_t1384811327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pathfinding.Serialization.AstarSerializer::CloseSerialize()
extern "C"  ByteU5BU5D_t4260760469* AstarSerializer_CloseSerialize_m3360652532 (AstarSerializer_t1384811327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::SerializeGraphs(Pathfinding.NavGraph[])
extern "C"  void AstarSerializer_SerializeGraphs_m483888282 (AstarSerializer_t1384811327 * __this, NavGraphU5BU5D_t850130684* ____graphs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::SerializeUserConnections(Pathfinding.UserConnection[])
extern "C"  void AstarSerializer_SerializeUserConnections_m4186668011 (AstarSerializer_t1384811327 * __this, UserConnectionU5BU5D_t328197926* ___conns0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pathfinding.Serialization.AstarSerializer::SerializeMeta()
extern "C"  ByteU5BU5D_t4260760469* AstarSerializer_SerializeMeta_m529759547 (AstarSerializer_t1384811327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pathfinding.Serialization.AstarSerializer::Serialize(Pathfinding.NavGraph)
extern "C"  ByteU5BU5D_t4260760469* AstarSerializer_Serialize_m3452120033 (AstarSerializer_t1384811327 * __this, NavGraph_t1254319713 * ___graph0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::SerializeNodes()
extern "C"  void AstarSerializer_SerializeNodes_m3250766791 (AstarSerializer_t1384811327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pathfinding.Serialization.AstarSerializer::SerializeNodes(System.Int32)
extern "C"  ByteU5BU5D_t4260760469* AstarSerializer_SerializeNodes_m316145774 (AstarSerializer_t1384811327 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::SerializeExtraInfo()
extern "C"  void AstarSerializer_SerializeExtraInfo_m1090161716 (AstarSerializer_t1384811327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pathfinding.Serialization.AstarSerializer::SerializeNodeConnections(System.Int32)
extern "C"  ByteU5BU5D_t4260760469* AstarSerializer_SerializeNodeConnections_m2740247152 (AstarSerializer_t1384811327 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::SerializeEditorSettings(Pathfinding.GraphEditorBase[])
extern "C"  void AstarSerializer_SerializeEditorSettings_m183979634 (AstarSerializer_t1384811327 * __this, GraphEditorBaseU5BU5D_t3067496667* ___editors0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Serialization.AstarSerializer::OpenDeserialize(System.Byte[])
extern "C"  bool AstarSerializer_OpenDeserialize_m3950229332 (AstarSerializer_t1384811327 * __this, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::CloseDeserialize()
extern "C"  void AstarSerializer_CloseDeserialize_m246161341 (AstarSerializer_t1384811327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NavGraph[] Pathfinding.Serialization.AstarSerializer::DeserializeGraphs()
extern "C"  NavGraphU5BU5D_t850130684* AstarSerializer_DeserializeGraphs_m1887384310 (AstarSerializer_t1384811327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.UserConnection[] Pathfinding.Serialization.AstarSerializer::DeserializeUserConnections()
extern "C"  UserConnectionU5BU5D_t328197926* AstarSerializer_DeserializeUserConnections_m3700994201 (AstarSerializer_t1384811327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::DeserializeNodes()
extern "C"  void AstarSerializer_DeserializeNodes_m3867749640 (AstarSerializer_t1384811327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::DeserializeExtraInfo()
extern "C"  void AstarSerializer_DeserializeExtraInfo_m1576561909 (AstarSerializer_t1384811327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::PostDeserialization()
extern "C"  void AstarSerializer_PostDeserialization_m3478216539 (AstarSerializer_t1384811327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::DeserializeNodes(System.Int32,System.IO.BinaryReader)
extern "C"  void AstarSerializer_DeserializeNodes_m363823488 (AstarSerializer_t1384811327 * __this, int32_t ___index0, BinaryReader_t3990958868 * ___reader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::DeserializeNodeConnections(System.Int32,System.IO.BinaryReader)
extern "C"  void AstarSerializer_DeserializeNodeConnections_m2530846462 (AstarSerializer_t1384811327 * __this, int32_t ___index0, BinaryReader_t3990958868 * ___reader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::DeserializeEditorSettings(Pathfinding.GraphEditorBase[])
extern "C"  void AstarSerializer_DeserializeEditorSettings_m3655712499 (AstarSerializer_t1384811327 * __this, GraphEditorBaseU5BU5D_t3067496667* ___graphEditors0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.Serialization.AstarSerializer::GetString(Pathfinding.Ionic.Zip.ZipEntry)
extern "C"  String_t* AstarSerializer_GetString_m2588191042 (AstarSerializer_t1384811327 * __this, ZipEntry_t2786874973 * ___entry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Serialization.GraphMeta Pathfinding.Serialization.AstarSerializer::DeserializeMeta(Pathfinding.Ionic.Zip.ZipEntry)
extern "C"  GraphMeta_t513097101 * AstarSerializer_DeserializeMeta_m2910370820 (AstarSerializer_t1384811327 * __this, ZipEntry_t2786874973 * ___entry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::SaveToFile(System.String,System.Byte[])
extern "C"  void AstarSerializer_SaveToFile_m274085371 (Il2CppObject * __this /* static, unused */, String_t* ___path0, ByteU5BU5D_t4260760469* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pathfinding.Serialization.AstarSerializer::LoadFromFile(System.String)
extern "C"  ByteU5BU5D_t4260760469* AstarSerializer_LoadFromFile_m278208458 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::ilo_AddChecksum1(Pathfinding.Serialization.AstarSerializer,System.Byte[])
extern "C"  void AstarSerializer_ilo_AddChecksum1_m4214535874 (Il2CppObject * __this /* static, unused */, AstarSerializer_t1384811327 * ____this0, ByteU5BU5D_t4260760469* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder Pathfinding.Serialization.AstarSerializer::ilo_GetStringBuilder2()
extern "C"  StringBuilder_t243639308 * AstarSerializer_ilo_GetStringBuilder2_m970865890 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.Serialization.AstarSerializer::ilo_ToString3(Pathfinding.Util.Guid&)
extern "C"  String_t* AstarSerializer_ilo_ToString3_m3590258280 (Il2CppObject * __this /* static, unused */, Guid_t3584625871 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pathfinding.Serialization.AstarSerializer::ilo_SerializeNodeConnections4(Pathfinding.Serialization.AstarSerializer,System.Int32)
extern "C"  ByteU5BU5D_t4260760469* AstarSerializer_ilo_SerializeNodeConnections4_m2812560390 (Il2CppObject * __this /* static, unused */, AstarSerializer_t1384811327 * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::ilo_SerializeExtraInfo5(Pathfinding.NavGraph,Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void AstarSerializer_ilo_SerializeExtraInfo5_m1383833122 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ____this0, GraphSerializationContext_t3256954663 * ___ctx1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::ilo_GetNodes6(Pathfinding.NavGraph,Pathfinding.GraphNodeDelegateCancelable)
extern "C"  void AstarSerializer_ilo_GetNodes6_m3270167436 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ____this0, GraphNodeDelegateCancelable_t3591372971 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Serialization.GraphMeta Pathfinding.Serialization.AstarSerializer::ilo_DeserializeMeta7(Pathfinding.Serialization.AstarSerializer,Pathfinding.Ionic.Zip.ZipEntry)
extern "C"  GraphMeta_t513097101 * AstarSerializer_ilo_DeserializeMeta7_m1469804817 (Il2CppObject * __this /* static, unused */, AstarSerializer_t1384811327 * ____this0, ZipEntry_t2786874973 * ___entry1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Version Pathfinding.Serialization.AstarSerializer::ilo_get_Version8()
extern "C"  Version_t763695022 * AstarSerializer_ilo_get_Version8_m812175726 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Pathfinding.Serialization.AstarSerializer::ilo_GetGraphType9(Pathfinding.Serialization.GraphMeta,System.Int32)
extern "C"  Type_t * AstarSerializer_ilo_GetGraphType9_m745157546 (Il2CppObject * __this /* static, unused */, GraphMeta_t513097101 * ____this0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NavGraph Pathfinding.Serialization.AstarSerializer::ilo_CreateGraph10(Pathfinding.AstarData,System.Type)
extern "C"  NavGraph_t1254319713 * AstarSerializer_ilo_CreateGraph10_m3375911072 (Il2CppObject * __this /* static, unused */, AstarData_t3283402719 * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.Serialization.AstarSerializer::ilo_GetString11(Pathfinding.Serialization.AstarSerializer,Pathfinding.Ionic.Zip.ZipEntry)
extern "C"  String_t* AstarSerializer_ilo_GetString11_m1582202368 (Il2CppObject * __this /* static, unused */, AstarSerializer_t1384811327 * ____this0, ZipEntry_t2786874973 * ___entry1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Util.Guid Pathfinding.Serialization.AstarSerializer::ilo_get_guid12(Pathfinding.NavGraph)
extern "C"  Guid_t3584625871  AstarSerializer_ilo_get_guid12_m1355580831 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::ilo_DeserializeNodes13(Pathfinding.Serialization.AstarSerializer,System.Int32,System.IO.BinaryReader)
extern "C"  void AstarSerializer_ilo_DeserializeNodes13_m1901870458 (Il2CppObject * __this /* static, unused */, AstarSerializer_t1384811327 * ____this0, int32_t ___index1, BinaryReader_t3990958868 * ___reader2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.AstarSerializer::ilo_PostDeserialization14(Pathfinding.NavGraph)
extern "C"  void AstarSerializer_ilo_PostDeserialization14_m4282374582 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
