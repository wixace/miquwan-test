﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Compression.LZMA.Decoder/LiteralDecoder
struct LiteralDecoder_t386054442;
// SevenZip.Compression.RangeCoder.Decoder
struct Decoder_t1102840654;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_1102840654.h"

// System.Void SevenZip.Compression.LZMA.Decoder/LiteralDecoder::.ctor()
extern "C"  void LiteralDecoder__ctor_m2225126577 (LiteralDecoder_t386054442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder/LiteralDecoder::Create(System.Int32,System.Int32)
extern "C"  void LiteralDecoder_Create_m3821500951 (LiteralDecoder_t386054442 * __this, int32_t ___numPosBits0, int32_t ___numPrevBits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder/LiteralDecoder::Init()
extern "C"  void LiteralDecoder_Init_m1547335555 (LiteralDecoder_t386054442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Decoder/LiteralDecoder::GetState(System.UInt32,System.Byte)
extern "C"  uint32_t LiteralDecoder_GetState_m960973334 (LiteralDecoder_t386054442 * __this, uint32_t ___pos0, uint8_t ___prevByte1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte SevenZip.Compression.LZMA.Decoder/LiteralDecoder::DecodeNormal(SevenZip.Compression.RangeCoder.Decoder,System.UInt32,System.Byte)
extern "C"  uint8_t LiteralDecoder_DecodeNormal_m2237452535 (LiteralDecoder_t386054442 * __this, Decoder_t1102840654 * ___rangeDecoder0, uint32_t ___pos1, uint8_t ___prevByte2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte SevenZip.Compression.LZMA.Decoder/LiteralDecoder::DecodeWithMatchByte(SevenZip.Compression.RangeCoder.Decoder,System.UInt32,System.Byte,System.Byte)
extern "C"  uint8_t LiteralDecoder_DecodeWithMatchByte_m3853645780 (LiteralDecoder_t386054442 * __this, Decoder_t1102840654 * ___rangeDecoder0, uint32_t ___pos1, uint8_t ___prevByte2, uint8_t ___matchByte3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
