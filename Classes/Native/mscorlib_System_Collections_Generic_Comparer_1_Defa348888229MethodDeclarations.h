﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<PushType>
struct DefaultComparer_t348888229;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PushType1840642452.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<PushType>::.ctor()
extern "C"  void DefaultComparer__ctor_m1619159130_gshared (DefaultComparer_t348888229 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1619159130(__this, method) ((  void (*) (DefaultComparer_t348888229 *, const MethodInfo*))DefaultComparer__ctor_m1619159130_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<PushType>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m2111590493_gshared (DefaultComparer_t348888229 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m2111590493(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t348888229 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m2111590493_gshared)(__this, ___x0, ___y1, method)
