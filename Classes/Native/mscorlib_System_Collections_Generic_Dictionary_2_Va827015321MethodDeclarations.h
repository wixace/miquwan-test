﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2176636147MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ProductsCfgMgr/ProductInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m2017638008(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t827015321 *, Dictionary_2_t2126409608 *, const MethodInfo*))ValueCollection__ctor_m1696084746_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3536770746(__this, ___item0, method) ((  void (*) (ValueCollection_t827015321 *, ProductInfo_t1305991238 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3387850728_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2724278403(__this, method) ((  void (*) (ValueCollection_t827015321 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3962609841_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m898187760(__this, ___item0, method) ((  bool (*) (ValueCollection_t827015321 *, ProductInfo_t1305991238 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2336892290_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4037678741(__this, ___item0, method) ((  bool (*) (ValueCollection_t827015321 *, ProductInfo_t1305991238 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4262639271_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1309307011(__this, method) ((  Il2CppObject* (*) (ValueCollection_t827015321 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3988116145_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1004751303(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t827015321 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2654918773_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2270951446(__this, method) ((  Il2CppObject * (*) (ValueCollection_t827015321 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4013782596_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3468330915(__this, method) ((  bool (*) (ValueCollection_t827015321 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m612068149_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m338487811(__this, method) ((  bool (*) (ValueCollection_t827015321 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3255376021_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m842333557(__this, method) ((  Il2CppObject * (*) (ValueCollection_t827015321 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3392405895_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ProductsCfgMgr/ProductInfo>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1500355775(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t827015321 *, ProductInfoU5BU5D_t182341603*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2638939345_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ProductsCfgMgr/ProductInfo>::GetEnumerator()
#define ValueCollection_GetEnumerator_m2247500328(__this, method) ((  Enumerator_t58243016  (*) (ValueCollection_t827015321 *, const MethodInfo*))ValueCollection_GetEnumerator_m4114035130_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ProductsCfgMgr/ProductInfo>::get_Count()
#define ValueCollection_get_Count_m1830555325(__this, method) ((  int32_t (*) (ValueCollection_t827015321 *, const MethodInfo*))ValueCollection_get_Count_m845657935_gshared)(__this, method)
