﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Poly2Tri.Polygon
struct Polygon_t1047479568;
// Pathfinding.Poly2Tri.TriangulationContext
struct TriangulationContext_t3528662164;
// Pathfinding.Poly2Tri.Triangulatable
struct Triangulatable_t923279207;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Polygon1047479568.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul2946406228.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3528662164.h"

// System.Void Pathfinding.Poly2Tri.P2T::.cctor()
extern "C"  void P2T__cctor_m3244147300 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.P2T::Triangulate(Pathfinding.Poly2Tri.Polygon)
extern "C"  void P2T_Triangulate_m3525691096 (Il2CppObject * __this /* static, unused */, Polygon_t1047479568 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.TriangulationContext Pathfinding.Poly2Tri.P2T::CreateContext(Pathfinding.Poly2Tri.TriangulationAlgorithm)
extern "C"  TriangulationContext_t3528662164 * P2T_CreateContext_m2227060077 (Il2CppObject * __this /* static, unused */, int32_t ___algorithm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.P2T::Triangulate(Pathfinding.Poly2Tri.TriangulationAlgorithm,Pathfinding.Poly2Tri.Triangulatable)
extern "C"  void P2T_Triangulate_m3381876250 (Il2CppObject * __this /* static, unused */, int32_t ___algorithm0, Il2CppObject * ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.P2T::Triangulate(Pathfinding.Poly2Tri.TriangulationContext)
extern "C"  void P2T_Triangulate_m3976741632 (Il2CppObject * __this /* static, unused */, TriangulationContext_t3528662164 * ___tcx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
