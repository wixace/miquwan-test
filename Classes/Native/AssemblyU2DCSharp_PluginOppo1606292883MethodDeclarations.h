﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginOppo
struct PluginOppo_t1606292883;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// System.Object
struct Il2CppObject;
// System.Action
struct Action_t3771233898;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PluginOppo1606292883.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginOppo::.ctor()
extern "C"  void PluginOppo__ctor_m1581324392 (PluginOppo_t1606292883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::Init()
extern "C"  void PluginOppo_Init_m3050588396 (PluginOppo_t1606292883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::OnDestory()
extern "C"  void PluginOppo_OnDestory_m1029148667 (PluginOppo_t1606292883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::Clear()
extern "C"  void PluginOppo_Clear_m3282424979 (PluginOppo_t1606292883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::RoleEnterGame(CEvent.ZEvent)
extern "C"  void PluginOppo_RoleEnterGame_m1260784673 (PluginOppo_t1606292883 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::UserPay(CEvent.ZEvent)
extern "C"  void PluginOppo_UserPay_m2672728952 (PluginOppo_t1606292883 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::onOppoLoginSuccess(System.String)
extern "C"  void PluginOppo_onOppoLoginSuccess_m4190087885 (PluginOppo_t1606292883 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::OnPayOppoSuccess(System.String)
extern "C"  void PluginOppo_OnPayOppoSuccess_m2064908204 (PluginOppo_t1606292883 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::OnPayOppoError(System.String)
extern "C"  void PluginOppo_OnPayOppoError_m40453767 (PluginOppo_t1606292883 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::onOppoEnterGameSuccess(System.String)
extern "C"  void PluginOppo_onOppoEnterGameSuccess_m3577117838 (PluginOppo_t1606292883 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::onOppoEnterGameFailure(System.String)
extern "C"  void PluginOppo_onOppoEnterGameFailure_m1337682983 (PluginOppo_t1606292883 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::OnExitGameSuccess(System.String)
extern "C"  void PluginOppo_OnExitGameSuccess_m2035318888 (PluginOppo_t1606292883 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginOppo::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginOppo_ReqSDKHttpLogin_m3596704573 (PluginOppo_t1606292883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginOppo::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginOppo_IsLoginSuccess_m3528003307 (PluginOppo_t1606292883 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::LoginOppoMgr()
extern "C"  void PluginOppo_LoginOppoMgr_m3596255179 (PluginOppo_t1606292883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::PlayOppoMgr(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.Int32,System.String,System.String,System.String)
extern "C"  void PluginOppo_PlayOppoMgr_m15784367 (PluginOppo_t1606292883 * __this, String_t* ___price0, String_t* ___roleId1, String_t* ___roleName2, String_t* ___roleLevel3, String_t* ___serverId4, String_t* ___serverName5, String_t* ___productId6, String_t* ___productName7, int32_t ___buyNum8, String_t* ___exts9, String_t* ___productDes10, String_t* ___notifyUrl11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::EnterGameOppoMgr(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginOppo_EnterGameOppoMgr_m3427769928 (PluginOppo_t1606292883 * __this, String_t* ___roleId0, String_t* ___roleName1, String_t* ___roleLevel2, String_t* ___serverId3, String_t* ___serverName4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::<OnExitGameSuccess>m__448()
extern "C"  void PluginOppo_U3COnExitGameSuccessU3Em__448_m3611507255 (PluginOppo_t1606292883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginOppo_ilo_AddEventListener1_m146369420 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::ilo_RemoveEventListener2(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginOppo_ilo_RemoveEventListener2_m1019434508 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::ilo_EnterGameOppoMgr3(PluginOppo,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginOppo_ilo_EnterGameOppoMgr3_m661670033 (Il2CppObject * __this /* static, unused */, PluginOppo_t1606292883 * ____this0, String_t* ___roleId1, String_t* ___roleName2, String_t* ___roleLevel3, String_t* ___serverId4, String_t* ___serverName5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginOppo::ilo_get_Instance4()
extern "C"  VersionMgr_t1322950208 * PluginOppo_ilo_get_Instance4_m3805735340 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::ilo_PlayOppoMgr5(PluginOppo,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.Int32,System.String,System.String,System.String)
extern "C"  void PluginOppo_ilo_PlayOppoMgr5_m888624894 (Il2CppObject * __this /* static, unused */, PluginOppo_t1606292883 * ____this0, String_t* ___price1, String_t* ___roleId2, String_t* ___roleName3, String_t* ___roleLevel4, String_t* ___serverId5, String_t* ___serverName6, String_t* ___productId7, String_t* ___productName8, int32_t ___buyNum9, String_t* ___exts10, String_t* ___productDes11, String_t* ___notifyUrl12, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::ilo_Log6(System.Object,System.Boolean)
extern "C"  void PluginOppo_ilo_Log6_m1770531626 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::ilo_Clear7(PluginOppo)
extern "C"  void PluginOppo_ilo_Clear7_m2020326566 (Il2CppObject * __this /* static, unused */, PluginOppo_t1606292883 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginOppo::ilo_Logout8(System.Action)
extern "C"  void PluginOppo_ilo_Logout8_m3655979452 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginOppo::ilo_get_PluginsSdkMgr9()
extern "C"  PluginsSdkMgr_t3884624670 * PluginOppo_ilo_get_PluginsSdkMgr9_m3419174024 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
