﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_halljasBistra153
struct M_halljasBistra153_t4051408741;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_halljasBistra1534051408741.h"

// System.Void GarbageiOS.M_halljasBistra153::.ctor()
extern "C"  void M_halljasBistra153__ctor_m2803656814 (M_halljasBistra153_t4051408741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_halljasBistra153::M_lewair0(System.String[],System.Int32)
extern "C"  void M_halljasBistra153_M_lewair0_m2629649205 (M_halljasBistra153_t4051408741 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_halljasBistra153::M_sasemrarLuserwhur1(System.String[],System.Int32)
extern "C"  void M_halljasBistra153_M_sasemrarLuserwhur1_m3946987681 (M_halljasBistra153_t4051408741 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_halljasBistra153::M_narardem2(System.String[],System.Int32)
extern "C"  void M_halljasBistra153_M_narardem2_m4010029415 (M_halljasBistra153_t4051408741 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_halljasBistra153::M_soojalldri3(System.String[],System.Int32)
extern "C"  void M_halljasBistra153_M_soojalldri3_m3366947059 (M_halljasBistra153_t4051408741 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_halljasBistra153::M_tago4(System.String[],System.Int32)
extern "C"  void M_halljasBistra153_M_tago4_m3057954736 (M_halljasBistra153_t4051408741 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_halljasBistra153::M_mirowFotri5(System.String[],System.Int32)
extern "C"  void M_halljasBistra153_M_mirowFotri5_m1396488002 (M_halljasBistra153_t4051408741 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_halljasBistra153::M_pihu6(System.String[],System.Int32)
extern "C"  void M_halljasBistra153_M_pihu6_m3702168929 (M_halljasBistra153_t4051408741 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_halljasBistra153::M_maspi7(System.String[],System.Int32)
extern "C"  void M_halljasBistra153_M_maspi7_m3419425470 (M_halljasBistra153_t4051408741 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_halljasBistra153::ilo_M_soojalldri31(GarbageiOS.M_halljasBistra153,System.String[],System.Int32)
extern "C"  void M_halljasBistra153_ilo_M_soojalldri31_m1891250162 (Il2CppObject * __this /* static, unused */, M_halljasBistra153_t4051408741 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_halljasBistra153::ilo_M_tago42(GarbageiOS.M_halljasBistra153,System.String[],System.Int32)
extern "C"  void M_halljasBistra153_ilo_M_tago42_m1593623638 (Il2CppObject * __this /* static, unused */, M_halljasBistra153_t4051408741 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_halljasBistra153::ilo_M_mirowFotri53(GarbageiOS.M_halljasBistra153,System.String[],System.Int32)
extern "C"  void M_halljasBistra153_ilo_M_mirowFotri53_m4225120453 (Il2CppObject * __this /* static, unused */, M_halljasBistra153_t4051408741 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
