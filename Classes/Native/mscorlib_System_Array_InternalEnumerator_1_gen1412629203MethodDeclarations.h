﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1412629203.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22630286527.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m979912433_gshared (InternalEnumerator_1_t1412629203 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m979912433(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1412629203 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m979912433_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m417267471_gshared (InternalEnumerator_1_t1412629203 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m417267471(__this, method) ((  void (*) (InternalEnumerator_1_t1412629203 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m417267471_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1351702267_gshared (InternalEnumerator_1_t1412629203 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1351702267(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1412629203 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1351702267_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2362677768_gshared (InternalEnumerator_1_t1412629203 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2362677768(__this, method) ((  void (*) (InternalEnumerator_1_t1412629203 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2362677768_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m620181371_gshared (InternalEnumerator_1_t1412629203 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m620181371(__this, method) ((  bool (*) (InternalEnumerator_1_t1412629203 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m620181371_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Int32>>::get_Current()
extern "C"  KeyValuePair_2_t2630286527  InternalEnumerator_1_get_Current_m2445546296_gshared (InternalEnumerator_1_t1412629203 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2445546296(__this, method) ((  KeyValuePair_2_t2630286527  (*) (InternalEnumerator_1_t1412629203 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2445546296_gshared)(__this, method)
