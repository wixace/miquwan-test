﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_noumougairWereco179
struct M_noumougairWereco179_t2633418148;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_noumougairWereco1792633418148.h"

// System.Void GarbageiOS.M_noumougairWereco179::.ctor()
extern "C"  void M_noumougairWereco179__ctor_m1806841407 (M_noumougairWereco179_t2633418148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_noumougairWereco179::M_jootuner0(System.String[],System.Int32)
extern "C"  void M_noumougairWereco179_M_jootuner0_m2180860546 (M_noumougairWereco179_t2633418148 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_noumougairWereco179::M_worcastraRubay1(System.String[],System.Int32)
extern "C"  void M_noumougairWereco179_M_worcastraRubay1_m1648609348 (M_noumougairWereco179_t2633418148 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_noumougairWereco179::M_revu2(System.String[],System.Int32)
extern "C"  void M_noumougairWereco179_M_revu2_m272809346 (M_noumougairWereco179_t2633418148 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_noumougairWereco179::M_chalwoo3(System.String[],System.Int32)
extern "C"  void M_noumougairWereco179_M_chalwoo3_m3644651194 (M_noumougairWereco179_t2633418148 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_noumougairWereco179::M_nase4(System.String[],System.Int32)
extern "C"  void M_noumougairWereco179_M_nase4_m2913406097 (M_noumougairWereco179_t2633418148 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_noumougairWereco179::M_nirereDuhayfi5(System.String[],System.Int32)
extern "C"  void M_noumougairWereco179_M_nirereDuhayfi5_m3501189746 (M_noumougairWereco179_t2633418148 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_noumougairWereco179::M_mailuPeretem6(System.String[],System.Int32)
extern "C"  void M_noumougairWereco179_M_mailuPeretem6_m1100022658 (M_noumougairWereco179_t2633418148 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_noumougairWereco179::M_calkearlaiWalldaga7(System.String[],System.Int32)
extern "C"  void M_noumougairWereco179_M_calkearlaiWalldaga7_m1974990109 (M_noumougairWereco179_t2633418148 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_noumougairWereco179::ilo_M_revu21(GarbageiOS.M_noumougairWereco179,System.String[],System.Int32)
extern "C"  void M_noumougairWereco179_ilo_M_revu21_m1216814238 (Il2CppObject * __this /* static, unused */, M_noumougairWereco179_t2633418148 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_noumougairWereco179::ilo_M_chalwoo32(GarbageiOS.M_noumougairWereco179,System.String[],System.Int32)
extern "C"  void M_noumougairWereco179_ilo_M_chalwoo32_m1941798843 (Il2CppObject * __this /* static, unused */, M_noumougairWereco179_t2633418148 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_noumougairWereco179::ilo_M_nase43(GarbageiOS.M_noumougairWereco179,System.String[],System.Int32)
extern "C"  void M_noumougairWereco179_ilo_M_nase43_m1938616235 (Il2CppObject * __this /* static, unused */, M_noumougairWereco179_t2633418148 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
