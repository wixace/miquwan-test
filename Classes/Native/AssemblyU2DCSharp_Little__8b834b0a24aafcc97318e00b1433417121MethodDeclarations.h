﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._8b834b0a24aafcc97318e00b8d9e150c
struct _8b834b0a24aafcc97318e00b8d9e150c_t1433417121;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__8b834b0a24aafcc97318e00b1433417121.h"

// System.Void Little._8b834b0a24aafcc97318e00b8d9e150c::.ctor()
extern "C"  void _8b834b0a24aafcc97318e00b8d9e150c__ctor_m1525593516 (_8b834b0a24aafcc97318e00b8d9e150c_t1433417121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8b834b0a24aafcc97318e00b8d9e150c::_8b834b0a24aafcc97318e00b8d9e150cm2(System.Int32)
extern "C"  int32_t _8b834b0a24aafcc97318e00b8d9e150c__8b834b0a24aafcc97318e00b8d9e150cm2_m3313018361 (_8b834b0a24aafcc97318e00b8d9e150c_t1433417121 * __this, int32_t ____8b834b0a24aafcc97318e00b8d9e150ca0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8b834b0a24aafcc97318e00b8d9e150c::_8b834b0a24aafcc97318e00b8d9e150cm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _8b834b0a24aafcc97318e00b8d9e150c__8b834b0a24aafcc97318e00b8d9e150cm_m4101614813 (_8b834b0a24aafcc97318e00b8d9e150c_t1433417121 * __this, int32_t ____8b834b0a24aafcc97318e00b8d9e150ca0, int32_t ____8b834b0a24aafcc97318e00b8d9e150c821, int32_t ____8b834b0a24aafcc97318e00b8d9e150cc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8b834b0a24aafcc97318e00b8d9e150c::ilo__8b834b0a24aafcc97318e00b8d9e150cm21(Little._8b834b0a24aafcc97318e00b8d9e150c,System.Int32)
extern "C"  int32_t _8b834b0a24aafcc97318e00b8d9e150c_ilo__8b834b0a24aafcc97318e00b8d9e150cm21_m185660040 (Il2CppObject * __this /* static, unused */, _8b834b0a24aafcc97318e00b8d9e150c_t1433417121 * ____this0, int32_t ____8b834b0a24aafcc97318e00b8d9e150ca1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
