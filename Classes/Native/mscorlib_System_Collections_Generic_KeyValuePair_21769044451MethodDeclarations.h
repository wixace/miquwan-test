﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21637661969MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt16,System.String>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m259463693(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1769044451 *, uint16_t, String_t*, const MethodInfo*))KeyValuePair_2__ctor_m2617982879_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.UInt16,System.String>::get_Key()
#define KeyValuePair_2_get_Key_m914454203(__this, method) ((  uint16_t (*) (KeyValuePair_2_t1769044451 *, const MethodInfo*))KeyValuePair_2_get_Key_m1818977513_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt16,System.String>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1862888444(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1769044451 *, uint16_t, const MethodInfo*))KeyValuePair_2_set_Key_m2765394730_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.UInt16,System.String>::get_Value()
#define KeyValuePair_2_get_Value_m3779017887(__this, method) ((  String_t* (*) (KeyValuePair_2_t1769044451 *, const MethodInfo*))KeyValuePair_2_get_Value_m1147557709_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt16,System.String>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2828427772(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1769044451 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Value_m687751722_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.UInt16,System.String>::ToString()
#define KeyValuePair_2_ToString_m1725628556(__this, method) ((  String_t* (*) (KeyValuePair_2_t1769044451 *, const MethodInfo*))KeyValuePair_2_ToString_m3996047390_gshared)(__this, method)
