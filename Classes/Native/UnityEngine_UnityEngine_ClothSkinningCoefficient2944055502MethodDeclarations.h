﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ClothSkinningCoefficient
struct ClothSkinningCoefficient_t2944055502;
struct ClothSkinningCoefficient_t2944055502_marshaled_pinvoke;
struct ClothSkinningCoefficient_t2944055502_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct ClothSkinningCoefficient_t2944055502;
struct ClothSkinningCoefficient_t2944055502_marshaled_pinvoke;

extern "C" void ClothSkinningCoefficient_t2944055502_marshal_pinvoke(const ClothSkinningCoefficient_t2944055502& unmarshaled, ClothSkinningCoefficient_t2944055502_marshaled_pinvoke& marshaled);
extern "C" void ClothSkinningCoefficient_t2944055502_marshal_pinvoke_back(const ClothSkinningCoefficient_t2944055502_marshaled_pinvoke& marshaled, ClothSkinningCoefficient_t2944055502& unmarshaled);
extern "C" void ClothSkinningCoefficient_t2944055502_marshal_pinvoke_cleanup(ClothSkinningCoefficient_t2944055502_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ClothSkinningCoefficient_t2944055502;
struct ClothSkinningCoefficient_t2944055502_marshaled_com;

extern "C" void ClothSkinningCoefficient_t2944055502_marshal_com(const ClothSkinningCoefficient_t2944055502& unmarshaled, ClothSkinningCoefficient_t2944055502_marshaled_com& marshaled);
extern "C" void ClothSkinningCoefficient_t2944055502_marshal_com_back(const ClothSkinningCoefficient_t2944055502_marshaled_com& marshaled, ClothSkinningCoefficient_t2944055502& unmarshaled);
extern "C" void ClothSkinningCoefficient_t2944055502_marshal_com_cleanup(ClothSkinningCoefficient_t2944055502_marshaled_com& marshaled);
