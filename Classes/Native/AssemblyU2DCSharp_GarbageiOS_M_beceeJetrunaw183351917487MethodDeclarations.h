﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_beceeJetrunaw18
struct M_beceeJetrunaw18_t3351917487;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_beceeJetrunaw18::.ctor()
extern "C"  void M_beceeJetrunaw18__ctor_m2757570708 (M_beceeJetrunaw18_t3351917487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_beceeJetrunaw18::M_mahar0(System.String[],System.Int32)
extern "C"  void M_beceeJetrunaw18_M_mahar0_m1190351236 (M_beceeJetrunaw18_t3351917487 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_beceeJetrunaw18::M_trawe1(System.String[],System.Int32)
extern "C"  void M_beceeJetrunaw18_M_trawe1_m2553539865 (M_beceeJetrunaw18_t3351917487 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
