﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<NumEffect>::.ctor()
#define List_1__ctor_m3503602934(__this, method) ((  void (*) (List_1_t4192407399 *, const MethodInfo*))List_1__ctor_m1239231859_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1339736402(__this, ___collection0, method) ((  void (*) (List_1_t4192407399 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::.ctor(System.Int32)
#define List_1__ctor_m115666654(__this, ___capacity0, method) ((  void (*) (List_1_t4192407399 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::.cctor()
#define List_1__cctor_m3734046784(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<NumEffect>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3566048031(__this, method) ((  Il2CppObject* (*) (List_1_t4192407399 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m3250029783(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4192407399 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<NumEffect>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1361505426(__this, method) ((  Il2CppObject * (*) (List_1_t4192407399 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<NumEffect>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m731333279(__this, ___item0, method) ((  int32_t (*) (List_1_t4192407399 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<NumEffect>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1511961485(__this, ___item0, method) ((  bool (*) (List_1_t4192407399 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<NumEffect>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m3794211191(__this, ___item0, method) ((  int32_t (*) (List_1_t4192407399 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m353867042(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4192407399 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m3492177094(__this, ___item0, method) ((  void (*) (List_1_t4192407399 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<NumEffect>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1463637838(__this, method) ((  bool (*) (List_1_t4192407399 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<NumEffect>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1517542575(__this, method) ((  bool (*) (List_1_t4192407399 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<NumEffect>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m2255183963(__this, method) ((  Il2CppObject * (*) (List_1_t4192407399 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<NumEffect>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m870535356(__this, method) ((  bool (*) (List_1_t4192407399 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<NumEffect>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m2698847933(__this, method) ((  bool (*) (List_1_t4192407399 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<NumEffect>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m3633882914(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t4192407399 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1377763449(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4192407399 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::Add(T)
#define List_1_Add_m3197963750(__this, ___item0, method) ((  void (*) (List_1_t4192407399 *, NumEffect_t2824221847 *, const MethodInfo*))List_1_Add_m933592675_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m3640153869(__this, ___newCount0, method) ((  void (*) (List_1_t4192407399 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m4020949690(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t4192407399 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m590371457_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m3670096395(__this, ___collection0, method) ((  void (*) (List_1_t4192407399 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m2931068619(__this, ___enumerable0, method) ((  void (*) (List_1_t4192407399 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m3006991596(__this, ___collection0, method) ((  void (*) (List_1_t4192407399 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<NumEffect>::AsReadOnly()
#define List_1_AsReadOnly_m3951243453(__this, method) ((  ReadOnlyCollection_1_t86332087 * (*) (List_1_t4192407399 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<NumEffect>::BinarySearch(T)
#define List_1_BinarySearch_m2294584162(__this, ___item0, method) ((  int32_t (*) (List_1_t4192407399 *, NumEffect_t2824221847 *, const MethodInfo*))List_1_BinarySearch_m2761349225_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::Clear()
#define List_1_Clear_m909736225(__this, method) ((  void (*) (List_1_t4192407399 *, const MethodInfo*))List_1_Clear_m2940332446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<NumEffect>::Contains(T)
#define List_1_Contains_m2223526246(__this, ___item0, method) ((  bool (*) (List_1_t4192407399 *, NumEffect_t2824221847 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m4108524866(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4192407399 *, NumEffectU5BU5D_t1853655854*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<NumEffect>::Find(System.Predicate`1<T>)
#define List_1_Find_m41415654(__this, ___match0, method) ((  NumEffect_t2824221847 * (*) (List_1_t4192407399 *, Predicate_1_t2435278730 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m2508114913(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2435278730 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<NumEffect>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m3790100998(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t4192407399 *, int32_t, int32_t, Predicate_1_t2435278730 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<NumEffect>::GetEnumerator()
#define List_1_GetEnumerator_m920167331(__this, method) ((  Enumerator_t4212080169  (*) (List_1_t4192407399 *, const MethodInfo*))List_1_GetEnumerator_m1919240000_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<NumEffect>::IndexOf(T)
#define List_1_IndexOf_m1249350726(__this, ___item0, method) ((  int32_t (*) (List_1_t4192407399 *, NumEffect_t2824221847 *, const MethodInfo*))List_1_IndexOf_m2776798407_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m3362951577(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t4192407399 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m3854892050(__this, ___index0, method) ((  void (*) (List_1_t4192407399 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::Insert(System.Int32,T)
#define List_1_Insert_m2680921145(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4192407399 *, int32_t, NumEffect_t2824221847 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m1321154030(__this, ___collection0, method) ((  void (*) (List_1_t4192407399 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<NumEffect>::Remove(T)
#define List_1_Remove_m1805348855(__this, ___item0, method) ((  bool (*) (List_1_t4192407399 *, NumEffect_t2824221847 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<NumEffect>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m981156745(__this, ___match0, method) ((  int32_t (*) (List_1_t4192407399 *, Predicate_1_t2435278730 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m946202358(__this, ___index0, method) ((  void (*) (List_1_t4192407399 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2208877785_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m4125568034(__this, ___index0, ___count1, method) ((  void (*) (List_1_t4192407399 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m856857915_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::Reverse()
#define List_1_Reverse_m1889225229(__this, method) ((  void (*) (List_1_t4192407399 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::Sort()
#define List_1_Sort_m3438934197(__this, method) ((  void (*) (List_1_t4192407399 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m886103247(__this, ___comparer0, method) ((  void (*) (List_1_t4192407399 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m122117960(__this, ___comparison0, method) ((  void (*) (List_1_t4192407399 *, Comparison_1_t1540583034 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<NumEffect>::ToArray()
#define List_1_ToArray_m153945228(__this, method) ((  NumEffectU5BU5D_t1853655854* (*) (List_1_t4192407399 *, const MethodInfo*))List_1_ToArray_m1449333879_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::TrimExcess()
#define List_1_TrimExcess_m3331783822(__this, method) ((  void (*) (List_1_t4192407399 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<NumEffect>::get_Capacity()
#define List_1_get_Capacity_m1082653046(__this, method) ((  int32_t (*) (List_1_t4192407399 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m850342687(__this, ___value0, method) ((  void (*) (List_1_t4192407399 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<NumEffect>::get_Count()
#define List_1_get_Count_m498695084(__this, method) ((  int32_t (*) (List_1_t4192407399 *, const MethodInfo*))List_1_get_Count_m3549598589_gshared)(__this, method)
// T System.Collections.Generic.List`1<NumEffect>::get_Item(System.Int32)
#define List_1_get_Item_m3177432041(__this, ___index0, method) ((  NumEffect_t2824221847 * (*) (List_1_t4192407399 *, int32_t, const MethodInfo*))List_1_get_Item_m404118264_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<NumEffect>::set_Item(System.Int32,T)
#define List_1_set_Item_m1194439376(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4192407399 *, int32_t, NumEffect_t2824221847 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
