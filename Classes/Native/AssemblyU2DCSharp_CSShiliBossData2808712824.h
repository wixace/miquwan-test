﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CSShiliBossUnit
struct CSShiliBossUnit_t2809231442;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSShiliBossData
struct  CSShiliBossData_t2808712824  : public Il2CppObject
{
public:
	// CSShiliBossUnit CSShiliBossData::CurShiliBossUnit
	CSShiliBossUnit_t2809231442 * ___CurShiliBossUnit_0;

public:
	inline static int32_t get_offset_of_CurShiliBossUnit_0() { return static_cast<int32_t>(offsetof(CSShiliBossData_t2808712824, ___CurShiliBossUnit_0)); }
	inline CSShiliBossUnit_t2809231442 * get_CurShiliBossUnit_0() const { return ___CurShiliBossUnit_0; }
	inline CSShiliBossUnit_t2809231442 ** get_address_of_CurShiliBossUnit_0() { return &___CurShiliBossUnit_0; }
	inline void set_CurShiliBossUnit_0(CSShiliBossUnit_t2809231442 * value)
	{
		___CurShiliBossUnit_0 = value;
		Il2CppCodeGenWriteBarrier(&___CurShiliBossUnit_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
