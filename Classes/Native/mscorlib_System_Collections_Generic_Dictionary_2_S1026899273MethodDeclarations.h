﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<Entity.Behavior.EBehaviorID,System.Object>
struct ShimEnumerator_t1026899273;
// System.Collections.Generic.Dictionary`2<Entity.Behavior.EBehaviorID,System.Object>
struct Dictionary_2_t1311121246;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Entity.Behavior.EBehaviorID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2538520253_gshared (ShimEnumerator_t1026899273 * __this, Dictionary_2_t1311121246 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m2538520253(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1026899273 *, Dictionary_2_t1311121246 *, const MethodInfo*))ShimEnumerator__ctor_m2538520253_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Entity.Behavior.EBehaviorID,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3188800324_gshared (ShimEnumerator_t1026899273 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3188800324(__this, method) ((  bool (*) (ShimEnumerator_t1026899273 *, const MethodInfo*))ShimEnumerator_MoveNext_m3188800324_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Entity.Behavior.EBehaviorID,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m1886583888_gshared (ShimEnumerator_t1026899273 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1886583888(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1026899273 *, const MethodInfo*))ShimEnumerator_get_Entry_m1886583888_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Entity.Behavior.EBehaviorID,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m374516651_gshared (ShimEnumerator_t1026899273 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m374516651(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1026899273 *, const MethodInfo*))ShimEnumerator_get_Key_m374516651_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Entity.Behavior.EBehaviorID,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m181685117_gshared (ShimEnumerator_t1026899273 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m181685117(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1026899273 *, const MethodInfo*))ShimEnumerator_get_Value_m181685117_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Entity.Behavior.EBehaviorID,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m4225396421_gshared (ShimEnumerator_t1026899273 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m4225396421(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1026899273 *, const MethodInfo*))ShimEnumerator_get_Current_m4225396421_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Entity.Behavior.EBehaviorID,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m2146078095_gshared (ShimEnumerator_t1026899273 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m2146078095(__this, method) ((  void (*) (ShimEnumerator_t1026899273 *, const MethodInfo*))ShimEnumerator_Reset_m2146078095_gshared)(__this, method)
