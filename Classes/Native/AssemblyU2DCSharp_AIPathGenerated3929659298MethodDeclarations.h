﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AIPathGenerated
struct AIPathGenerated_t3929659298;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// AIPath
struct AIPath_t1930792045;
// Pathfinding.Path
struct Path_t1974241691;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_AIPath1930792045.h"

// System.Void AIPathGenerated::.ctor()
extern "C"  void AIPathGenerated__ctor_m2626250249 (AIPathGenerated_t3929659298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AIPathGenerated::AIPath_AIPath1(JSVCall,System.Int32)
extern "C"  bool AIPathGenerated_AIPath_AIPath1_m3349385321 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::AIPath_AIId(JSVCall)
extern "C"  void AIPathGenerated_AIPath_AIId_m3385226411 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::AIPath_repathRate(JSVCall)
extern "C"  void AIPathGenerated_AIPath_repathRate_m4213234646 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::AIPath_target(JSVCall)
extern "C"  void AIPathGenerated_AIPath_target_m3513062461 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::AIPath_canSearch(JSVCall)
extern "C"  void AIPathGenerated_AIPath_canSearch_m690105542 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::AIPath_canMoveNextWaypoint(JSVCall)
extern "C"  void AIPathGenerated_AIPath_canMoveNextWaypoint_m2127382153 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::AIPath_speed(JSVCall)
extern "C"  void AIPathGenerated_AIPath_speed_m3085247223 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::AIPath_turningSpeed(JSVCall)
extern "C"  void AIPathGenerated_AIPath_turningSpeed_m3130461676 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::AIPath_slowdownDistance(JSVCall)
extern "C"  void AIPathGenerated_AIPath_slowdownDistance_m1319754870 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::AIPath_pickNextWaypointDist(JSVCall)
extern "C"  void AIPathGenerated_AIPath_pickNextWaypointDist_m2762263795 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::AIPath_forwardLook(JSVCall)
extern "C"  void AIPathGenerated_AIPath_forwardLook_m3843834586 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::AIPath_endReachedDistance(JSVCall)
extern "C"  void AIPathGenerated_AIPath_endReachedDistance_m1342206754 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::AIPath_closestOnPathCheck(JSVCall)
extern "C"  void AIPathGenerated_AIPath_closestOnPathCheck_m1190933091 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::AIPath_tr(JSVCall)
extern "C"  void AIPathGenerated_AIPath_tr_m1734444528 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::AIPath_IsSearchPathing(JSVCall)
extern "C"  void AIPathGenerated_AIPath_IsSearchPathing_m241003539 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::AIPath_targetPoint(JSVCall)
extern "C"  void AIPathGenerated_AIPath_targetPoint_m1243716031 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::AIPath_canMove(JSVCall)
extern "C"  void AIPathGenerated_AIPath_canMove_m1947078333 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::AIPath_TargetReached(JSVCall)
extern "C"  void AIPathGenerated_AIPath_TargetReached_m3394834973 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AIPathGenerated::AIPath_GetFeetPosition(JSVCall,System.Int32)
extern "C"  bool AIPathGenerated_AIPath_GetFeetPosition_m1224390034 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AIPathGenerated::AIPath_OnDisable(JSVCall,System.Int32)
extern "C"  bool AIPathGenerated_AIPath_OnDisable_m3895624750 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AIPathGenerated::AIPath_OnPathComplete__Path(JSVCall,System.Int32)
extern "C"  bool AIPathGenerated_AIPath_OnPathComplete__Path_m3220522399 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AIPathGenerated::AIPath_OnTargetReached(JSVCall,System.Int32)
extern "C"  bool AIPathGenerated_AIPath_OnTargetReached_m3775611719 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AIPathGenerated::AIPath_SearchPath(JSVCall,System.Int32)
extern "C"  bool AIPathGenerated_AIPath_SearchPath_m2003040746 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AIPathGenerated::AIPath_Start(JSVCall,System.Int32)
extern "C"  bool AIPathGenerated_AIPath_Start_m2102556103 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AIPathGenerated::AIPath_TrySearchPath(JSVCall,System.Int32)
extern "C"  bool AIPathGenerated_AIPath_TrySearchPath_m1955354349 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AIPathGenerated::AIPath_Update(JSVCall,System.Int32)
extern "C"  bool AIPathGenerated_AIPath_Update_m2106487238 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::__Register()
extern "C"  void AIPathGenerated___Register_m1415803998 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AIPathGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t AIPathGenerated_ilo_getObject1_m55126989 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::ilo_setUInt322(System.Int32,System.UInt32)
extern "C"  void AIPathGenerated_ilo_setUInt322_m1017427050 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void AIPathGenerated_ilo_setSingle3_m3905730253 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AIPathGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t AIPathGenerated_ilo_setObject4_m756729352 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AIPathGenerated::ilo_getObject5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * AIPathGenerated_ilo_getObject5_m2191201216 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AIPathGenerated::ilo_getSingle6(System.Int32)
extern "C"  float AIPathGenerated_ilo_getSingle6_m3451022571 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AIPathGenerated::ilo_getBooleanS7(System.Int32)
extern "C"  bool AIPathGenerated_ilo_getBooleanS7_m1544028737 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::ilo_setVector3S8(System.Int32,UnityEngine.Vector3)
extern "C"  void AIPathGenerated_ilo_setVector3S8_m3457401573 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 AIPathGenerated::ilo_getVector3S9(System.Int32)
extern "C"  Vector3_t4282066566  AIPathGenerated_ilo_getVector3S9_m3995782757 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AIPathGenerated::ilo_get_TargetReached10(AIPath)
extern "C"  bool AIPathGenerated_ilo_get_TargetReached10_m3948604274 (Il2CppObject * __this /* static, unused */, AIPath_t1930792045 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::ilo_OnTargetReached11(AIPath)
extern "C"  void AIPathGenerated_ilo_OnTargetReached11_m2819082633 (Il2CppObject * __this /* static, unused */, AIPath_t1930792045 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Path AIPathGenerated::ilo_SearchPath12(AIPath)
extern "C"  Path_t1974241691 * AIPathGenerated_ilo_SearchPath12_m3968953025 (Il2CppObject * __this /* static, unused */, AIPath_t1930792045 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPathGenerated::ilo_Update13(AIPath)
extern "C"  void AIPathGenerated_ilo_Update13_m2889017772 (Il2CppObject * __this /* static, unused */, AIPath_t1930792045 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
