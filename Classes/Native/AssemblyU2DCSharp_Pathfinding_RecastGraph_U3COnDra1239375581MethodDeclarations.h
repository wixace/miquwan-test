﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RecastGraph/<OnDrawGizmos>c__AnonStorey11A
struct U3COnDrawGizmosU3Ec__AnonStorey11A_t1239375581;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.RecastGraph/<OnDrawGizmos>c__AnonStorey11A::.ctor()
extern "C"  void U3COnDrawGizmosU3Ec__AnonStorey11A__ctor_m360111710 (U3COnDrawGizmosU3Ec__AnonStorey11A_t1239375581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastGraph/<OnDrawGizmos>c__AnonStorey11A::<>m__34F(Pathfinding.GraphNode)
extern "C"  bool U3COnDrawGizmosU3Ec__AnonStorey11A_U3CU3Em__34F_m1404883928 (U3COnDrawGizmosU3Ec__AnonStorey11A_t1239375581 * __this, GraphNode_t23612370 * ____node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
