﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>
struct Dictionary_2_t967328657;
// System.Collections.Generic.IEqualityComparer`1<Pathfinding.Int2>
struct IEqualityComparer_1_t2765079997;
// System.Collections.Generic.IDictionary`2<Pathfinding.Int2,System.Object>
struct IDictionary_2_t545202002;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<Pathfinding.Int2>
struct ICollection_1_t2868635580;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>[]
struct KeyValuePair_2U5BU5D_t3739032610;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>>
struct IEnumerator_1_t2777974412;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Object>
struct KeyCollection_t2594088108;
// System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,System.Object>
struct ValueCollection_t3962901666;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_866109363.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2284652049.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m675884058_gshared (Dictionary_2_t967328657 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m675884058(__this, method) ((  void (*) (Dictionary_2_t967328657 *, const MethodInfo*))Dictionary_2__ctor_m675884058_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m158615953_gshared (Dictionary_2_t967328657 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m158615953(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t967328657 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m158615953_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m637853118_gshared (Dictionary_2_t967328657 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m637853118(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t967328657 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m637853118_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3619916395_gshared (Dictionary_2_t967328657 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m3619916395(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t967328657 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3619916395_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1931427071_gshared (Dictionary_2_t967328657 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1931427071(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t967328657 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1931427071_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m907956827_gshared (Dictionary_2_t967328657 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m907956827(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t967328657 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m907956827_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3012907134_gshared (Dictionary_2_t967328657 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3012907134(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t967328657 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3012907134_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2362193498_gshared (Dictionary_2_t967328657 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2362193498(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t967328657 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2362193498_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m597639128_gshared (Dictionary_2_t967328657 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m597639128(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t967328657 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m597639128_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m1173139142_gshared (Dictionary_2_t967328657 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m1173139142(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t967328657 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m1173139142_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2520507203_gshared (Dictionary_2_t967328657 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2520507203(__this, method) ((  bool (*) (Dictionary_2_t967328657 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2520507203_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1366599510_gshared (Dictionary_2_t967328657 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1366599510(__this, method) ((  bool (*) (Dictionary_2_t967328657 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1366599510_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m1880438052_gshared (Dictionary_2_t967328657 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1880438052(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t967328657 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1880438052_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m2796389203_gshared (Dictionary_2_t967328657 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m2796389203(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t967328657 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m2796389203_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3063067422_gshared (Dictionary_2_t967328657 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3063067422(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t967328657 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3063067422_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m3450537172_gshared (Dictionary_2_t967328657 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m3450537172(__this, ___key0, method) ((  bool (*) (Dictionary_2_t967328657 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m3450537172_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m746521489_gshared (Dictionary_2_t967328657 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m746521489(__this, ___key0, method) ((  void (*) (Dictionary_2_t967328657 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m746521489_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3323788032_gshared (Dictionary_2_t967328657 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3323788032(__this, method) ((  bool (*) (Dictionary_2_t967328657 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3323788032_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m850672114_gshared (Dictionary_2_t967328657 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m850672114(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t967328657 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m850672114_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2982228804_gshared (Dictionary_2_t967328657 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2982228804(__this, method) ((  bool (*) (Dictionary_2_t967328657 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2982228804_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1716712359_gshared (Dictionary_2_t967328657 * __this, KeyValuePair_2_t866109363  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1716712359(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t967328657 *, KeyValuePair_2_t866109363 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1716712359_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3650558911_gshared (Dictionary_2_t967328657 * __this, KeyValuePair_2_t866109363  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3650558911(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t967328657 *, KeyValuePair_2_t866109363 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3650558911_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3890145227_gshared (Dictionary_2_t967328657 * __this, KeyValuePair_2U5BU5D_t3739032610* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3890145227(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t967328657 *, KeyValuePair_2U5BU5D_t3739032610*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3890145227_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1600595620_gshared (Dictionary_2_t967328657 * __this, KeyValuePair_2_t866109363  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1600595620(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t967328657 *, KeyValuePair_2_t866109363 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1600595620_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m1261554282_gshared (Dictionary_2_t967328657 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1261554282(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t967328657 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1261554282_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m4118924281_gshared (Dictionary_2_t967328657 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m4118924281(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t967328657 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m4118924281_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m419783536_gshared (Dictionary_2_t967328657 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m419783536(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t967328657 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m419783536_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m488046013_gshared (Dictionary_2_t967328657 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m488046013(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t967328657 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m488046013_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m474344762_gshared (Dictionary_2_t967328657 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m474344762(__this, method) ((  int32_t (*) (Dictionary_2_t967328657 *, const MethodInfo*))Dictionary_2_get_Count_m474344762_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m496756493_gshared (Dictionary_2_t967328657 * __this, Int2_t1974045593  ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m496756493(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t967328657 *, Int2_t1974045593 , const MethodInfo*))Dictionary_2_get_Item_m496756493_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m722021466_gshared (Dictionary_2_t967328657 * __this, Int2_t1974045593  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m722021466(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t967328657 *, Int2_t1974045593 , Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m722021466_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m2458923922_gshared (Dictionary_2_t967328657 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m2458923922(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t967328657 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2458923922_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m3808388709_gshared (Dictionary_2_t967328657 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m3808388709(__this, ___size0, method) ((  void (*) (Dictionary_2_t967328657 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3808388709_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3827871137_gshared (Dictionary_2_t967328657 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3827871137(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t967328657 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3827871137_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t866109363  Dictionary_2_make_pair_m1704586805_gshared (Il2CppObject * __this /* static, unused */, Int2_t1974045593  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m1704586805(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t866109363  (*) (Il2CppObject * /* static, unused */, Int2_t1974045593 , Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m1704586805_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::pick_key(TKey,TValue)
extern "C"  Int2_t1974045593  Dictionary_2_pick_key_m912923625_gshared (Il2CppObject * __this /* static, unused */, Int2_t1974045593  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m912923625(__this /* static, unused */, ___key0, ___value1, method) ((  Int2_t1974045593  (*) (Il2CppObject * /* static, unused */, Int2_t1974045593 , Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m912923625_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m3311968261_gshared (Il2CppObject * __this /* static, unused */, Int2_t1974045593  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m3311968261(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Int2_t1974045593 , Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m3311968261_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m1045234446_gshared (Dictionary_2_t967328657 * __this, KeyValuePair_2U5BU5D_t3739032610* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1045234446(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t967328657 *, KeyValuePair_2U5BU5D_t3739032610*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1045234446_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m3830333278_gshared (Dictionary_2_t967328657 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m3830333278(__this, method) ((  void (*) (Dictionary_2_t967328657 *, const MethodInfo*))Dictionary_2_Resize_m3830333278_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m4106552795_gshared (Dictionary_2_t967328657 * __this, Int2_t1974045593  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m4106552795(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t967328657 *, Int2_t1974045593 , Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m4106552795_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m2376984645_gshared (Dictionary_2_t967328657 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2376984645(__this, method) ((  void (*) (Dictionary_2_t967328657 *, const MethodInfo*))Dictionary_2_Clear_m2376984645_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m1240403119_gshared (Dictionary_2_t967328657 * __this, Int2_t1974045593  ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1240403119(__this, ___key0, method) ((  bool (*) (Dictionary_2_t967328657 *, Int2_t1974045593 , const MethodInfo*))Dictionary_2_ContainsKey_m1240403119_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m1636200751_gshared (Dictionary_2_t967328657 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1636200751(__this, ___value0, method) ((  bool (*) (Dictionary_2_t967328657 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m1636200751_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m1480658616_gshared (Dictionary_2_t967328657 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1480658616(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t967328657 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m1480658616_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2989926060_gshared (Dictionary_2_t967328657 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2989926060(__this, ___sender0, method) ((  void (*) (Dictionary_2_t967328657 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2989926060_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m880392929_gshared (Dictionary_2_t967328657 * __this, Int2_t1974045593  ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m880392929(__this, ___key0, method) ((  bool (*) (Dictionary_2_t967328657 *, Int2_t1974045593 , const MethodInfo*))Dictionary_2_Remove_m880392929_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2323072520_gshared (Dictionary_2_t967328657 * __this, Int2_t1974045593  ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m2323072520(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t967328657 *, Int2_t1974045593 , Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m2323072520_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::get_Keys()
extern "C"  KeyCollection_t2594088108 * Dictionary_2_get_Keys_m4238372755_gshared (Dictionary_2_t967328657 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m4238372755(__this, method) ((  KeyCollection_t2594088108 * (*) (Dictionary_2_t967328657 *, const MethodInfo*))Dictionary_2_get_Keys_m4238372755_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::get_Values()
extern "C"  ValueCollection_t3962901666 * Dictionary_2_get_Values_m1264457455_gshared (Dictionary_2_t967328657 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1264457455(__this, method) ((  ValueCollection_t3962901666 * (*) (Dictionary_2_t967328657 *, const MethodInfo*))Dictionary_2_get_Values_m1264457455_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::ToTKey(System.Object)
extern "C"  Int2_t1974045593  Dictionary_2_ToTKey_m362782532_gshared (Dictionary_2_t967328657 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m362782532(__this, ___key0, method) ((  Int2_t1974045593  (*) (Dictionary_2_t967328657 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m362782532_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m1624282400_gshared (Dictionary_2_t967328657 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1624282400(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t967328657 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1624282400_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m2702401732_gshared (Dictionary_2_t967328657 * __this, KeyValuePair_2_t866109363  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m2702401732(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t967328657 *, KeyValuePair_2_t866109363 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m2702401732_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2284652049  Dictionary_2_GetEnumerator_m1946100133_gshared (Dictionary_2_t967328657 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1946100133(__this, method) ((  Enumerator_t2284652049  (*) (Dictionary_2_t967328657 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1946100133_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m3625508572_gshared (Il2CppObject * __this /* static, unused */, Int2_t1974045593  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m3625508572(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, Int2_t1974045593 , Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m3625508572_gshared)(__this /* static, unused */, ___key0, ___value1, method)
