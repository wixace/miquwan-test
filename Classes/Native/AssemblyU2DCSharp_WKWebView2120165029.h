﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// WebViewObject
struct WebViewObject_t388577432;
// System.String
struct String_t;
// System.Action`1<System.String>
struct Action_1_t403047693;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WKWebView
struct  WKWebView_t2120165029  : public MonoBehaviour_t667441552
{
public:
	// WebViewObject WKWebView::_webViewObject
	WebViewObject_t388577432 * ____webViewObject_2;

public:
	inline static int32_t get_offset_of__webViewObject_2() { return static_cast<int32_t>(offsetof(WKWebView_t2120165029, ____webViewObject_2)); }
	inline WebViewObject_t388577432 * get__webViewObject_2() const { return ____webViewObject_2; }
	inline WebViewObject_t388577432 ** get_address_of__webViewObject_2() { return &____webViewObject_2; }
	inline void set__webViewObject_2(WebViewObject_t388577432 * value)
	{
		____webViewObject_2 = value;
		Il2CppCodeGenWriteBarrier(&____webViewObject_2, value);
	}
};

struct WKWebView_t2120165029_StaticFields
{
public:
	// System.String WKWebView::<Url>k__BackingField
	String_t* ___U3CUrlU3Ek__BackingField_3;
	// System.Action`1<System.String> WKWebView::<>f__am$cache2
	Action_1_t403047693 * ___U3CU3Ef__amU24cache2_4;
	// System.Action`1<System.String> WKWebView::<>f__am$cache3
	Action_1_t403047693 * ___U3CU3Ef__amU24cache3_5;
	// System.Action`1<System.String> WKWebView::<>f__am$cache4
	Action_1_t403047693 * ___U3CU3Ef__amU24cache4_6;

public:
	inline static int32_t get_offset_of_U3CUrlU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WKWebView_t2120165029_StaticFields, ___U3CUrlU3Ek__BackingField_3)); }
	inline String_t* get_U3CUrlU3Ek__BackingField_3() const { return ___U3CUrlU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CUrlU3Ek__BackingField_3() { return &___U3CUrlU3Ek__BackingField_3; }
	inline void set_U3CUrlU3Ek__BackingField_3(String_t* value)
	{
		___U3CUrlU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUrlU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_4() { return static_cast<int32_t>(offsetof(WKWebView_t2120165029_StaticFields, ___U3CU3Ef__amU24cache2_4)); }
	inline Action_1_t403047693 * get_U3CU3Ef__amU24cache2_4() const { return ___U3CU3Ef__amU24cache2_4; }
	inline Action_1_t403047693 ** get_address_of_U3CU3Ef__amU24cache2_4() { return &___U3CU3Ef__amU24cache2_4; }
	inline void set_U3CU3Ef__amU24cache2_4(Action_1_t403047693 * value)
	{
		___U3CU3Ef__amU24cache2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_5() { return static_cast<int32_t>(offsetof(WKWebView_t2120165029_StaticFields, ___U3CU3Ef__amU24cache3_5)); }
	inline Action_1_t403047693 * get_U3CU3Ef__amU24cache3_5() const { return ___U3CU3Ef__amU24cache3_5; }
	inline Action_1_t403047693 ** get_address_of_U3CU3Ef__amU24cache3_5() { return &___U3CU3Ef__amU24cache3_5; }
	inline void set_U3CU3Ef__amU24cache3_5(Action_1_t403047693 * value)
	{
		___U3CU3Ef__amU24cache3_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_6() { return static_cast<int32_t>(offsetof(WKWebView_t2120165029_StaticFields, ___U3CU3Ef__amU24cache4_6)); }
	inline Action_1_t403047693 * get_U3CU3Ef__amU24cache4_6() const { return ___U3CU3Ef__amU24cache4_6; }
	inline Action_1_t403047693 ** get_address_of_U3CU3Ef__amU24cache4_6() { return &___U3CU3Ef__amU24cache4_6; }
	inline void set_U3CU3Ef__amU24cache4_6(Action_1_t403047693 * value)
	{
		___U3CU3Ef__amU24cache4_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
