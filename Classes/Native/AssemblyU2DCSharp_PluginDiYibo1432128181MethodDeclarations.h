﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginDiYibo
struct PluginDiYibo_t1432128181;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Object
struct Il2CppObject;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginDiYibo::.ctor()
extern "C"  void PluginDiYibo__ctor_m3369313158 (PluginDiYibo_t1432128181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::Init()
extern "C"  void PluginDiYibo_Init_m4216644110 (PluginDiYibo_t1432128181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginDiYibo::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginDiYibo_ReqSDKHttpLogin_m1048194139 (PluginDiYibo_t1432128181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginDiYibo::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginDiYibo_IsLoginSuccess_m1390167309 (PluginDiYibo_t1432128181 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::OpenUserLogin()
extern "C"  void PluginDiYibo_OpenUserLogin_m2920013784 (PluginDiYibo_t1432128181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::RolelvUpinfo(CEvent.ZEvent)
extern "C"  void PluginDiYibo_RolelvUpinfo_m1863027636 (PluginDiYibo_t1432128181 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::UserPay(CEvent.ZEvent)
extern "C"  void PluginDiYibo_UserPay_m554479770 (PluginDiYibo_t1432128181 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::OnInitSuccess(System.String)
extern "C"  void PluginDiYibo_OnInitSuccess_m2368566250 (PluginDiYibo_t1432128181 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::OnInitFail(System.String)
extern "C"  void PluginDiYibo_OnInitFail_m3314849175 (PluginDiYibo_t1432128181 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::OnLoginSuccess(System.String)
extern "C"  void PluginDiYibo_OnLoginSuccess_m800821771 (PluginDiYibo_t1432128181 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::OnLoginFail(System.String)
extern "C"  void PluginDiYibo_OnLoginFail_m2605912854 (PluginDiYibo_t1432128181 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::OnPaySuccess(System.String)
extern "C"  void PluginDiYibo_OnPaySuccess_m3689216778 (PluginDiYibo_t1432128181 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::OnPayFail(System.String)
extern "C"  void PluginDiYibo_OnPayFail_m733962871 (PluginDiYibo_t1432128181 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::OnExitGameSuccess(System.String)
extern "C"  void PluginDiYibo_OnExitGameSuccess_m4002943754 (PluginDiYibo_t1432128181 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::OnLogoutSuccess(System.String)
extern "C"  void PluginDiYibo_OnLogoutSuccess_m3354181092 (PluginDiYibo_t1432128181 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::OnLogoutFail(System.String)
extern "C"  void PluginDiYibo_OnLogoutFail_m3286769117 (PluginDiYibo_t1432128181 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::Initsdk()
extern "C"  void PluginDiYibo_Initsdk_m3207300750 (PluginDiYibo_t1432128181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::Login()
extern "C"  void PluginDiYibo_Login_m261013965 (PluginDiYibo_t1432128181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::Role(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginDiYibo_Role_m9727034 (PluginDiYibo_t1432128181 * __this, String_t* ___roleId0, String_t* ___roleName1, String_t* ___roleLevel2, String_t* ___zoneId3, String_t* ___zoneName4, String_t* ___ext5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::Pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginDiYibo_Pay_m3623256594 (PluginDiYibo_t1432128181 * __this, String_t* ___orderId0, String_t* ___orderRmb1, String_t* ___productId2, String_t* ___productName3, String_t* ___productDescription4, String_t* ___createTime5, String_t* ___notifyUrl6, String_t* ___callbackInfo7, String_t* ___serverId8, String_t* ___appSchemes9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::<OnLogoutSuccess>m__417()
extern "C"  void PluginDiYibo_U3COnLogoutSuccessU3Em__417_m1033773137 (PluginDiYibo_t1432128181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginDiYibo_ilo_AddEventListener1_m274724014 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginDiYibo::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginDiYibo_ilo_get_Instance2_m985953160 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginDiYibo::ilo_get_ProductsCfgMgr3()
extern "C"  ProductsCfgMgr_t2493714872 * PluginDiYibo_ilo_get_ProductsCfgMgr3_m2718779870 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginDiYibo::ilo_GetProductID4(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginDiYibo_ilo_GetProductID4_m2927768882 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::ilo_ReqSDKHttpLogin5(PluginsSdkMgr)
extern "C"  void PluginDiYibo_ilo_ReqSDKHttpLogin5_m1417185555 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::ilo_Log6(System.Object,System.Boolean)
extern "C"  void PluginDiYibo_ilo_Log6_m1426832460 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDiYibo::ilo_Logout7(System.Action)
extern "C"  void PluginDiYibo_ilo_Logout7_m2497882111 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
