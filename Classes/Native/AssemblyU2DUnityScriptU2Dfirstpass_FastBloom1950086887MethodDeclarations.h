﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FastBloom
struct FastBloom_t1950086887;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void FastBloom::.ctor()
extern "C"  void FastBloom__ctor_m2303717915 (FastBloom_t1950086887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FastBloom::CheckResources()
extern "C"  bool FastBloom_CheckResources_m3169202272 (FastBloom_t1950086887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FastBloom::OnDisable()
extern "C"  void FastBloom_OnDisable_m2877997314 (FastBloom_t1950086887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FastBloom::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void FastBloom_OnRenderImage_m1022115171 (FastBloom_t1950086887 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FastBloom::Main()
extern "C"  void FastBloom_Main_m3314943810 (FastBloom_t1950086887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
