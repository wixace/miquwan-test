﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Schema.JsonSchemaBuilder
struct JsonSchemaBuilder_t269040652;
// Newtonsoft.Json.Schema.JsonSchemaResolver
struct JsonSchemaResolver_t2728657753;
// Newtonsoft.Json.Schema.JsonSchema
struct JsonSchema_t460567603;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.String
struct String_t;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchema>
struct IList_1_t3155214806;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken>
struct IList_1_t1811925858;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_t2701878760;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchema>
struct IDictionary_2_t858859318;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem2728657753.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema460567603.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Nullable_1_gen2199542344.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem2115415821.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken4173078175.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema269040652.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"
#include "mscorlib_System_Nullable_1_gen3952353088.h"
#include "mscorlib_System_Nullable_1_gen1237965023.h"

// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::.ctor(Newtonsoft.Json.Schema.JsonSchemaResolver)
extern "C"  void JsonSchemaBuilder__ctor_m3640194314 (JsonSchemaBuilder_t269040652 * __this, JsonSchemaResolver_t2728657753 * ___resolver0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::Push(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void JsonSchemaBuilder_Push_m1711915188 (JsonSchemaBuilder_t269040652 * __this, JsonSchema_t460567603 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaBuilder::Pop()
extern "C"  JsonSchema_t460567603 * JsonSchemaBuilder_Pop_m2021407486 (JsonSchemaBuilder_t269040652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaBuilder::get_CurrentSchema()
extern "C"  JsonSchema_t460567603 * JsonSchemaBuilder_get_CurrentSchema_m268885758 (JsonSchemaBuilder_t269040652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaBuilder::Parse(Newtonsoft.Json.JsonReader)
extern "C"  JsonSchema_t460567603 * JsonSchemaBuilder_Parse_m2498143744 (JsonSchemaBuilder_t269040652 * __this, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaBuilder::BuildSchema()
extern "C"  JsonSchema_t460567603 * JsonSchemaBuilder_BuildSchema_m3553843932 (JsonSchemaBuilder_t269040652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ProcessSchemaProperty(System.String)
extern "C"  void JsonSchemaBuilder_ProcessSchemaProperty_m1330449662 (JsonSchemaBuilder_t269040652 * __this, String_t* ___propertyName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ProcessExtends()
extern "C"  void JsonSchemaBuilder_ProcessExtends_m2987724173 (JsonSchemaBuilder_t269040652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ProcessEnum()
extern "C"  void JsonSchemaBuilder_ProcessEnum_m337484911 (JsonSchemaBuilder_t269040652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ProcessOptions()
extern "C"  void JsonSchemaBuilder_ProcessOptions_m1151205970 (JsonSchemaBuilder_t269040652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ProcessDefault()
extern "C"  void JsonSchemaBuilder_ProcessDefault_m2098591029 (JsonSchemaBuilder_t269040652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ProcessIdentity()
extern "C"  void JsonSchemaBuilder_ProcessIdentity_m2630180460 (JsonSchemaBuilder_t269040652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ProcessAdditionalProperties()
extern "C"  void JsonSchemaBuilder_ProcessAdditionalProperties_m2157682664 (JsonSchemaBuilder_t269040652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ProcessPatternProperties()
extern "C"  void JsonSchemaBuilder_ProcessPatternProperties_m830815127 (JsonSchemaBuilder_t269040652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ProcessItems()
extern "C"  void JsonSchemaBuilder_ProcessItems_m1284215732 (JsonSchemaBuilder_t269040652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ProcessProperties()
extern "C"  void JsonSchemaBuilder_ProcessProperties_m1405999713 (JsonSchemaBuilder_t269040652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType> Newtonsoft.Json.Schema.JsonSchemaBuilder::ProcessType()
extern "C"  Nullable_1_t2199542344  JsonSchemaBuilder_ProcessType_m3913635737 (JsonSchemaBuilder_t269040652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaType Newtonsoft.Json.Schema.JsonSchemaBuilder::MapType(System.String)
extern "C"  int32_t JsonSchemaBuilder_MapType_m869536421 (Il2CppObject * __this /* static, unused */, String_t* ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaBuilder::MapType(Newtonsoft.Json.Schema.JsonSchemaType)
extern "C"  String_t* JsonSchemaBuilder_MapType_m2251695781 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchema> Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_get_LoadedSchemas1(Newtonsoft.Json.Schema.JsonSchemaResolver)
extern "C"  Il2CppObject* JsonSchemaBuilder_ilo_get_LoadedSchemas1_m2660469061 (Il2CppObject * __this /* static, unused */, JsonSchemaResolver_t2728657753 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonToken Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_get_TokenType2(Newtonsoft.Json.JsonReader)
extern "C"  int32_t JsonSchemaBuilder_ilo_get_TokenType2_m4162229205 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_FormatWith3(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* JsonSchemaBuilder_ilo_FormatWith3_m3935933341 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_Push4(Newtonsoft.Json.Schema.JsonSchemaBuilder,Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void JsonSchemaBuilder_ilo_Push4_m1478498967 (Il2CppObject * __this /* static, unused */, JsonSchemaBuilder_t269040652 * ____this0, JsonSchema_t460567603 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_Read5(Newtonsoft.Json.JsonReader)
extern "C"  bool JsonSchemaBuilder_ilo_Read5_m413896983 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_get_Value6(Newtonsoft.Json.JsonReader)
extern "C"  Il2CppObject * JsonSchemaBuilder_ilo_get_Value6_m3117965913 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_GetSchema7(Newtonsoft.Json.Schema.JsonSchemaResolver,System.String)
extern "C"  JsonSchema_t460567603 * JsonSchemaBuilder_ilo_GetSchema7_m287715295 (Il2CppObject * __this /* static, unused */, JsonSchemaResolver_t2728657753 * ____this0, String_t* ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_ProcessSchemaProperty8(Newtonsoft.Json.Schema.JsonSchemaBuilder,System.String)
extern "C"  void JsonSchemaBuilder_ilo_ProcessSchemaProperty8_m3416771435 (Il2CppObject * __this /* static, unused */, JsonSchemaBuilder_t269040652 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_Pop9(Newtonsoft.Json.Schema.JsonSchemaBuilder)
extern "C"  JsonSchema_t460567603 * JsonSchemaBuilder_ilo_Pop9_m2556669110 (Il2CppObject * __this /* static, unused */, JsonSchemaBuilder_t269040652 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_get_CurrentSchema10(Newtonsoft.Json.Schema.JsonSchemaBuilder)
extern "C"  JsonSchema_t460567603 * JsonSchemaBuilder_ilo_get_CurrentSchema10_m2958830576 (Il2CppObject * __this /* static, unused */, JsonSchemaBuilder_t269040652 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_set_Type11(Newtonsoft.Json.Schema.JsonSchema,System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>)
extern "C"  void JsonSchemaBuilder_ilo_set_Type11_m712628916 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, Nullable_1_t2199542344  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_set_Description12(Newtonsoft.Json.Schema.JsonSchema,System.String)
extern "C"  void JsonSchemaBuilder_ilo_set_Description12_m2055424577 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_ProcessProperties13(Newtonsoft.Json.Schema.JsonSchemaBuilder)
extern "C"  void JsonSchemaBuilder_ilo_ProcessProperties13_m423788246 (Il2CppObject * __this /* static, unused */, JsonSchemaBuilder_t269040652 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_set_Required14(Newtonsoft.Json.Schema.JsonSchema,System.Nullable`1<System.Boolean>)
extern "C"  void JsonSchemaBuilder_ilo_set_Required14_m2302003902 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, Nullable_1_t560925241  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_set_Requires15(Newtonsoft.Json.Schema.JsonSchema,System.String)
extern "C"  void JsonSchemaBuilder_ilo_set_Requires15_m448001608 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_set_Minimum16(Newtonsoft.Json.Schema.JsonSchema,System.Nullable`1<System.Double>)
extern "C"  void JsonSchemaBuilder_ilo_set_Minimum16_m1164768318 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, Nullable_1_t3952353088  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_set_ExclusiveMaximum17(Newtonsoft.Json.Schema.JsonSchema,System.Nullable`1<System.Boolean>)
extern "C"  void JsonSchemaBuilder_ilo_set_ExclusiveMaximum17_m1132341048 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, Nullable_1_t560925241  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_set_MaximumLength18(Newtonsoft.Json.Schema.JsonSchema,System.Nullable`1<System.Int32>)
extern "C"  void JsonSchemaBuilder_ilo_set_MaximumLength18_m4262592157 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, Nullable_1_t1237965023  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_set_MinimumItems19(Newtonsoft.Json.Schema.JsonSchema,System.Nullable`1<System.Int32>)
extern "C"  void JsonSchemaBuilder_ilo_set_MinimumItems19_m2589260908 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, Nullable_1_t1237965023  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_ProcessDefault20(Newtonsoft.Json.Schema.JsonSchemaBuilder)
extern "C"  void JsonSchemaBuilder_ilo_ProcessDefault20_m1441756556 (Il2CppObject * __this /* static, unused */, JsonSchemaBuilder_t269040652 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_ProcessOptions21(Newtonsoft.Json.Schema.JsonSchemaBuilder)
extern "C"  void JsonSchemaBuilder_ilo_ProcessOptions21_m1673674026 (Il2CppObject * __this /* static, unused */, JsonSchemaBuilder_t269040652 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_ProcessEnum22(Newtonsoft.Json.Schema.JsonSchemaBuilder)
extern "C"  void JsonSchemaBuilder_ilo_ProcessEnum22_m1169379266 (Il2CppObject * __this /* static, unused */, JsonSchemaBuilder_t269040652 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_ProcessExtends23(Newtonsoft.Json.Schema.JsonSchemaBuilder)
extern "C"  void JsonSchemaBuilder_ilo_ProcessExtends23_m2094197799 (Il2CppObject * __this /* static, unused */, JsonSchemaBuilder_t269040652 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_Skip24(Newtonsoft.Json.JsonReader)
extern "C"  void JsonSchemaBuilder_ilo_Skip24_m3356113975 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_ReadFrom25(Newtonsoft.Json.JsonReader)
extern "C"  JToken_t3412245951 * JsonSchemaBuilder_ilo_ReadFrom25_m1344330148 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_get_Enum26(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  Il2CppObject* JsonSchemaBuilder_ilo_get_Enum26_m2567115395 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_set_Identity27(Newtonsoft.Json.Schema.JsonSchema,System.Collections.Generic.IList`1<System.String>)
extern "C"  void JsonSchemaBuilder_ilo_set_Identity27_m2803828518 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, Il2CppObject* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<System.String> Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_get_Identity28(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  Il2CppObject* JsonSchemaBuilder_ilo_get_Identity28_m2077631676 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_set_Properties29(Newtonsoft.Json.Schema.JsonSchema,System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchema>)
extern "C"  void JsonSchemaBuilder_ilo_set_Properties29_m1215208221 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, Il2CppObject* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaType Newtonsoft.Json.Schema.JsonSchemaBuilder::ilo_MapType30(System.String)
extern "C"  int32_t JsonSchemaBuilder_ilo_MapType30_m849571291 (Il2CppObject * __this /* static, unused */, String_t* ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
