﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke807822589MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,ProductsCfgMgr/ProductInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m3569668490(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3753169059 *, Dictionary_2_t2126409608 *, const MethodInfo*))KeyCollection__ctor_m3248115228_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3914480012(__this, ___item0, method) ((  void (*) (KeyCollection_t3753169059 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m857844154_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m263279107(__this, method) ((  void (*) (KeyCollection_t3753169059 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m492500401_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3206756418(__this, ___item0, method) ((  bool (*) (KeyCollection_t3753169059 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2885203156_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1308938791(__this, ___item0, method) ((  bool (*) (KeyCollection_t3753169059 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2966702905_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3774553365(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3753169059 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2396336835_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m2452703733(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3753169059 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m4102871203_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3626945092(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3753169059 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1074808946_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m787457827(__this, method) ((  bool (*) (KeyCollection_t3753169059 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1012418357_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1182707605(__this, method) ((  bool (*) (KeyCollection_t3753169059 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4099595815_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,ProductsCfgMgr/ProductInfo>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2411764039(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3753169059 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m666869081_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,ProductsCfgMgr/ProductInfo>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m545972287(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3753169059 *, StringU5BU5D_t4054002952*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1785144529_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,ProductsCfgMgr/ProductInfo>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1256945740(__this, method) ((  Enumerator_t2741345662  (*) (KeyCollection_t3753169059 *, const MethodInfo*))KeyCollection_GetEnumerator_m3123480542_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,ProductsCfgMgr/ProductInfo>::get_Count()
#define KeyCollection_get_Count_m3430116559(__this, method) ((  int32_t (*) (KeyCollection_t3753169059 *, const MethodInfo*))KeyCollection_get_Count_m2445219169_gshared)(__this, method)
