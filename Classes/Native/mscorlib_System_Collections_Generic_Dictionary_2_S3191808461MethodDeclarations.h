﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ProductsCfgMgr/ProductInfo>
struct ShimEnumerator_t3191808461;
// System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>
struct Dictionary_2_t3476030434;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ProductsCfgMgr/ProductInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1529363306_gshared (ShimEnumerator_t3191808461 * __this, Dictionary_2_t3476030434 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m1529363306(__this, ___host0, method) ((  void (*) (ShimEnumerator_t3191808461 *, Dictionary_2_t3476030434 *, const MethodInfo*))ShimEnumerator__ctor_m1529363306_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ProductsCfgMgr/ProductInfo>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m4139019963_gshared (ShimEnumerator_t3191808461 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m4139019963(__this, method) ((  bool (*) (ShimEnumerator_t3191808461 *, const MethodInfo*))ShimEnumerator_MoveNext_m4139019963_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ProductsCfgMgr/ProductInfo>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m3478520463_gshared (ShimEnumerator_t3191808461 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m3478520463(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t3191808461 *, const MethodInfo*))ShimEnumerator_get_Entry_m3478520463_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ProductsCfgMgr/ProductInfo>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m483816718_gshared (ShimEnumerator_t3191808461 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m483816718(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3191808461 *, const MethodInfo*))ShimEnumerator_get_Key_m483816718_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ProductsCfgMgr/ProductInfo>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2139834400_gshared (ShimEnumerator_t3191808461 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m2139834400(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3191808461 *, const MethodInfo*))ShimEnumerator_get_Value_m2139834400_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ProductsCfgMgr/ProductInfo>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m516214440_gshared (ShimEnumerator_t3191808461 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m516214440(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3191808461 *, const MethodInfo*))ShimEnumerator_get_Current_m516214440_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,ProductsCfgMgr/ProductInfo>::Reset()
extern "C"  void ShimEnumerator_Reset_m1008031420_gshared (ShimEnumerator_t3191808461 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1008031420(__this, method) ((  void (*) (ShimEnumerator_t3191808461 *, const MethodInfo*))ShimEnumerator_Reset_m1008031420_gshared)(__this, method)
