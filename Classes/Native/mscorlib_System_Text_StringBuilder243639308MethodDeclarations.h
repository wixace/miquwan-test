﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.StringBuilder
struct StringBuilder_t243639308;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Char[]
struct CharU5BU5D_t3324145743;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.IFormatProvider
struct IFormatProvider_t192740775;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Text.StringBuilder::.ctor(System.String,System.Int32,System.Int32,System.Int32)
extern "C"  void StringBuilder__ctor_m4031300257 (StringBuilder_t243639308 * __this, String_t* ___value0, int32_t ___startIndex1, int32_t ___length2, int32_t ___capacity3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor(System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void StringBuilder__ctor_m3542181846 (StringBuilder_t243639308 * __this, String_t* ___value0, int32_t ___startIndex1, int32_t ___length2, int32_t ___capacity3, int32_t ___maxCapacity4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor()
extern "C"  void StringBuilder__ctor_m135953004 (StringBuilder_t243639308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor(System.Int32)
extern "C"  void StringBuilder__ctor_m3624398269 (StringBuilder_t243639308 * __this, int32_t ___capacity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor(System.Int32,System.Int32)
extern "C"  void StringBuilder__ctor_m2934057530 (StringBuilder_t243639308 * __this, int32_t ___capacity0, int32_t ___maxCapacity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor(System.String)
extern "C"  void StringBuilder__ctor_m1143895062 (StringBuilder_t243639308 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor(System.String,System.Int32)
extern "C"  void StringBuilder__ctor_m1310751873 (StringBuilder_t243639308 * __this, String_t* ___value0, int32_t ___capacity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void StringBuilder__ctor_m3783410093 (StringBuilder_t243639308 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void StringBuilder_System_Runtime_Serialization_ISerializable_GetObjectData_m2909896863 (StringBuilder_t243639308 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.StringBuilder::get_MaxCapacity()
extern "C"  int32_t StringBuilder_get_MaxCapacity_m2822541307 (StringBuilder_t243639308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.StringBuilder::get_Capacity()
extern "C"  int32_t StringBuilder_get_Capacity_m884438143 (StringBuilder_t243639308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::set_Capacity(System.Int32)
extern "C"  void StringBuilder_set_Capacity_m519605088 (StringBuilder_t243639308 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.StringBuilder::get_Length()
extern "C"  int32_t StringBuilder_get_Length_m2443133099 (StringBuilder_t243639308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::set_Length(System.Int32)
extern "C"  void StringBuilder_set_Length_m1952332172 (StringBuilder_t243639308 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.Text.StringBuilder::get_Chars(System.Int32)
extern "C"  Il2CppChar StringBuilder_get_Chars_m1670994701 (StringBuilder_t243639308 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::set_Chars(System.Int32,System.Char)
extern "C"  void StringBuilder_set_Chars_m1845996850 (StringBuilder_t243639308 * __this, int32_t ___index0, Il2CppChar ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.StringBuilder::ToString()
extern "C"  String_t* StringBuilder_ToString_m350379841 (StringBuilder_t243639308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.StringBuilder::ToString(System.Int32,System.Int32)
extern "C"  String_t* StringBuilder_ToString_m3621056261 (StringBuilder_t243639308 * __this, int32_t ___startIndex0, int32_t ___length1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.StringBuilder::EnsureCapacity(System.Int32)
extern "C"  int32_t StringBuilder_EnsureCapacity_m806299493 (StringBuilder_t243639308 * __this, int32_t ___capacity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.StringBuilder::Equals(System.Text.StringBuilder)
extern "C"  bool StringBuilder_Equals_m567821365 (StringBuilder_t243639308 * __this, StringBuilder_t243639308 * ___sb0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Remove(System.Int32,System.Int32)
extern "C"  StringBuilder_t243639308 * StringBuilder_Remove_m970775893 (StringBuilder_t243639308 * __this, int32_t ___startIndex0, int32_t ___length1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Replace(System.Char,System.Char)
extern "C"  StringBuilder_t243639308 * StringBuilder_Replace_m3116112395 (StringBuilder_t243639308 * __this, Il2CppChar ___oldChar0, Il2CppChar ___newChar1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Replace(System.Char,System.Char,System.Int32,System.Int32)
extern "C"  StringBuilder_t243639308 * StringBuilder_Replace_m1562191787 (StringBuilder_t243639308 * __this, Il2CppChar ___oldChar0, Il2CppChar ___newChar1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Replace(System.String,System.String)
extern "C"  StringBuilder_t243639308 * StringBuilder_Replace_m118777941 (StringBuilder_t243639308 * __this, String_t* ___oldValue0, String_t* ___newValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Replace(System.String,System.String,System.Int32,System.Int32)
extern "C"  StringBuilder_t243639308 * StringBuilder_Replace_m1895746933 (StringBuilder_t243639308 * __this, String_t* ___oldValue0, String_t* ___newValue1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char[])
extern "C"  StringBuilder_t243639308 * StringBuilder_Append_m2223932948 (StringBuilder_t243639308 * __this, CharU5BU5D_t3324145743* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
extern "C"  StringBuilder_t243639308 * StringBuilder_Append_m3898090075 (StringBuilder_t243639308 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Boolean)
extern "C"  StringBuilder_t243639308 * StringBuilder_Append_m792763710 (StringBuilder_t243639308 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Byte)
extern "C"  StringBuilder_t243639308 * StringBuilder_Append_m2142694660 (StringBuilder_t243639308 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Decimal)
extern "C"  StringBuilder_t243639308 * StringBuilder_Append_m3647200053 (StringBuilder_t243639308 * __this, Decimal_t1954350631  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Double)
extern "C"  StringBuilder_t243639308 * StringBuilder_Append_m3329851035 (StringBuilder_t243639308 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Int16)
extern "C"  StringBuilder_t243639308 * StringBuilder_Append_m2189220818 (StringBuilder_t243639308 * __this, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Int32)
extern "C"  StringBuilder_t243639308 * StringBuilder_Append_m2189222616 (StringBuilder_t243639308 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Int64)
extern "C"  StringBuilder_t243639308 * StringBuilder_Append_m2189225561 (StringBuilder_t243639308 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Object)
extern "C"  StringBuilder_t243639308 * StringBuilder_Append_m4120200429 (StringBuilder_t243639308 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.SByte)
extern "C"  StringBuilder_t243639308 * StringBuilder_Append_m2435092203 (StringBuilder_t243639308 * __this, int8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Single)
extern "C"  StringBuilder_t243639308 * StringBuilder_Append_m3579413764 (StringBuilder_t243639308 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.UInt16)
extern "C"  StringBuilder_t243639308 * StringBuilder_Append_m143650125 (StringBuilder_t243639308 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.UInt32)
extern "C"  StringBuilder_t243639308 * StringBuilder_Append_m143651923 (StringBuilder_t243639308 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.UInt64)
extern "C"  StringBuilder_t243639308 * StringBuilder_Append_m143654868 (StringBuilder_t243639308 * __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char)
extern "C"  StringBuilder_t243639308 * StringBuilder_Append_m2143093878 (StringBuilder_t243639308 * __this, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char,System.Int32)
extern "C"  StringBuilder_t243639308 * StringBuilder_Append_m1038583841 (StringBuilder_t243639308 * __this, Il2CppChar ___value0, int32_t ___repeatCount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char[],System.Int32,System.Int32)
extern "C"  StringBuilder_t243639308 * StringBuilder_Append_m2623154804 (StringBuilder_t243639308 * __this, CharU5BU5D_t3324145743* ___value0, int32_t ___startIndex1, int32_t ___charCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String,System.Int32,System.Int32)
extern "C"  StringBuilder_t243639308 * StringBuilder_Append_m2996071419 (StringBuilder_t243639308 * __this, String_t* ___value0, int32_t ___startIndex1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::AppendLine()
extern "C"  StringBuilder_t243639308 * StringBuilder_AppendLine_m568622107 (StringBuilder_t243639308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::AppendLine(System.String)
extern "C"  StringBuilder_t243639308 * StringBuilder_AppendLine_m655025863 (StringBuilder_t243639308 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::AppendFormat(System.String,System.Object[])
extern "C"  StringBuilder_t243639308 * StringBuilder_AppendFormat_m279545936 (StringBuilder_t243639308 * __this, String_t* ___format0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::AppendFormat(System.IFormatProvider,System.String,System.Object[])
extern "C"  StringBuilder_t243639308 * StringBuilder_AppendFormat_m259793396 (StringBuilder_t243639308 * __this, Il2CppObject * ___provider0, String_t* ___format1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::AppendFormat(System.String,System.Object)
extern "C"  StringBuilder_t243639308 * StringBuilder_AppendFormat_m3723191730 (StringBuilder_t243639308 * __this, String_t* ___format0, Il2CppObject * ___arg01, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::AppendFormat(System.String,System.Object,System.Object)
extern "C"  StringBuilder_t243639308 * StringBuilder_AppendFormat_m3487355136 (StringBuilder_t243639308 * __this, String_t* ___format0, Il2CppObject * ___arg01, Il2CppObject * ___arg12, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::AppendFormat(System.String,System.Object,System.Object,System.Object)
extern "C"  StringBuilder_t243639308 * StringBuilder_AppendFormat_m508648398 (StringBuilder_t243639308 * __this, String_t* ___format0, Il2CppObject * ___arg01, Il2CppObject * ___arg12, Il2CppObject * ___arg23, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Char[])
extern "C"  StringBuilder_t243639308 * StringBuilder_Insert_m3366646764 (StringBuilder_t243639308 * __this, int32_t ___index0, CharU5BU5D_t3324145743* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.String)
extern "C"  StringBuilder_t243639308 * StringBuilder_Insert_m745836595 (StringBuilder_t243639308 * __this, int32_t ___index0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Boolean)
extern "C"  StringBuilder_t243639308 * StringBuilder_Insert_m1857153638 (StringBuilder_t243639308 * __this, int32_t ___index0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Byte)
extern "C"  StringBuilder_t243639308 * StringBuilder_Insert_m1866789084 (StringBuilder_t243639308 * __this, int32_t ___index0, uint8_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Char)
extern "C"  StringBuilder_t243639308 * StringBuilder_Insert_m1867188302 (StringBuilder_t243639308 * __this, int32_t ___index0, Il2CppChar ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Decimal)
extern "C"  StringBuilder_t243639308 * StringBuilder_Insert_m416622685 (StringBuilder_t243639308 * __this, int32_t ___index0, Decimal_t1954350631  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Double)
extern "C"  StringBuilder_t243639308 * StringBuilder_Insert_m177597555 (StringBuilder_t243639308 * __this, int32_t ___index0, double ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Int16)
extern "C"  StringBuilder_t243639308 * StringBuilder_Insert_m2226082554 (StringBuilder_t243639308 * __this, int32_t ___index0, int16_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Int32)
extern "C"  StringBuilder_t243639308 * StringBuilder_Insert_m2226084352 (StringBuilder_t243639308 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Int64)
extern "C"  StringBuilder_t243639308 * StringBuilder_Insert_m2226087297 (StringBuilder_t243639308 * __this, int32_t ___index0, int64_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Object)
extern "C"  StringBuilder_t243639308 * StringBuilder_Insert_m967946949 (StringBuilder_t243639308 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.SByte)
extern "C"  StringBuilder_t243639308 * StringBuilder_Insert_m2471953939 (StringBuilder_t243639308 * __this, int32_t ___index0, int8_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Single)
extern "C"  StringBuilder_t243639308 * StringBuilder_Insert_m427160284 (StringBuilder_t243639308 * __this, int32_t ___index0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.UInt16)
extern "C"  StringBuilder_t243639308 * StringBuilder_Insert_m1286363941 (StringBuilder_t243639308 * __this, int32_t ___index0, uint16_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.UInt32)
extern "C"  StringBuilder_t243639308 * StringBuilder_Insert_m1286365739 (StringBuilder_t243639308 * __this, int32_t ___index0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.UInt64)
extern "C"  StringBuilder_t243639308 * StringBuilder_Insert_m1286368684 (StringBuilder_t243639308 * __this, int32_t ___index0, uint64_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.String,System.Int32)
extern "C"  StringBuilder_t243639308 * StringBuilder_Insert_m1606924164 (StringBuilder_t243639308 * __this, int32_t ___index0, String_t* ___value1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Char[],System.Int32,System.Int32)
extern "C"  StringBuilder_t243639308 * StringBuilder_Insert_m2516482636 (StringBuilder_t243639308 * __this, int32_t ___index0, CharU5BU5D_t3324145743* ___value1, int32_t ___startIndex2, int32_t ___charCount3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::InternalEnsureCapacity(System.Int32)
extern "C"  void StringBuilder_InternalEnsureCapacity_m3925915998 (StringBuilder_t243639308 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::CopyTo(System.Int32,System.Char[],System.Int32,System.Int32)
extern "C"  void StringBuilder_CopyTo_m3099741646 (StringBuilder_t243639308 * __this, int32_t ___sourceIndex0, CharU5BU5D_t3324145743* ___destination1, int32_t ___destinationIndex2, int32_t ___count3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
