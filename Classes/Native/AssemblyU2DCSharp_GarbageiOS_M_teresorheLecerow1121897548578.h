﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_teresorheLecerow112
struct  M_teresorheLecerow112_t1897548578  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_teresorheLecerow112::_lalpa
	int32_t ____lalpa_0;
	// System.String GarbageiOS.M_teresorheLecerow112::_sehustalForbouzou
	String_t* ____sehustalForbouzou_1;
	// System.Int32 GarbageiOS.M_teresorheLecerow112::_barwerasTrichibea
	int32_t ____barwerasTrichibea_2;
	// System.Boolean GarbageiOS.M_teresorheLecerow112::_sertrorseGawlallfi
	bool ____sertrorseGawlallfi_3;
	// System.Boolean GarbageiOS.M_teresorheLecerow112::_treebawReldido
	bool ____treebawReldido_4;
	// System.Int32 GarbageiOS.M_teresorheLecerow112::_largerCeratay
	int32_t ____largerCeratay_5;
	// System.UInt32 GarbageiOS.M_teresorheLecerow112::_morfocooMuhehaw
	uint32_t ____morfocooMuhehaw_6;
	// System.Int32 GarbageiOS.M_teresorheLecerow112::_rorfur
	int32_t ____rorfur_7;
	// System.Boolean GarbageiOS.M_teresorheLecerow112::_muba
	bool ____muba_8;
	// System.Boolean GarbageiOS.M_teresorheLecerow112::_sineBirroubu
	bool ____sineBirroubu_9;
	// System.Boolean GarbageiOS.M_teresorheLecerow112::_raijem
	bool ____raijem_10;
	// System.Single GarbageiOS.M_teresorheLecerow112::_jairkeeca
	float ____jairkeeca_11;
	// System.Single GarbageiOS.M_teresorheLecerow112::_pailou
	float ____pailou_12;
	// System.UInt32 GarbageiOS.M_teresorheLecerow112::_gaberedowSirjel
	uint32_t ____gaberedowSirjel_13;
	// System.Boolean GarbageiOS.M_teresorheLecerow112::_holeTawwhurme
	bool ____holeTawwhurme_14;
	// System.Single GarbageiOS.M_teresorheLecerow112::_heacellis
	float ____heacellis_15;
	// System.Boolean GarbageiOS.M_teresorheLecerow112::_marpere
	bool ____marpere_16;

public:
	inline static int32_t get_offset_of__lalpa_0() { return static_cast<int32_t>(offsetof(M_teresorheLecerow112_t1897548578, ____lalpa_0)); }
	inline int32_t get__lalpa_0() const { return ____lalpa_0; }
	inline int32_t* get_address_of__lalpa_0() { return &____lalpa_0; }
	inline void set__lalpa_0(int32_t value)
	{
		____lalpa_0 = value;
	}

	inline static int32_t get_offset_of__sehustalForbouzou_1() { return static_cast<int32_t>(offsetof(M_teresorheLecerow112_t1897548578, ____sehustalForbouzou_1)); }
	inline String_t* get__sehustalForbouzou_1() const { return ____sehustalForbouzou_1; }
	inline String_t** get_address_of__sehustalForbouzou_1() { return &____sehustalForbouzou_1; }
	inline void set__sehustalForbouzou_1(String_t* value)
	{
		____sehustalForbouzou_1 = value;
		Il2CppCodeGenWriteBarrier(&____sehustalForbouzou_1, value);
	}

	inline static int32_t get_offset_of__barwerasTrichibea_2() { return static_cast<int32_t>(offsetof(M_teresorheLecerow112_t1897548578, ____barwerasTrichibea_2)); }
	inline int32_t get__barwerasTrichibea_2() const { return ____barwerasTrichibea_2; }
	inline int32_t* get_address_of__barwerasTrichibea_2() { return &____barwerasTrichibea_2; }
	inline void set__barwerasTrichibea_2(int32_t value)
	{
		____barwerasTrichibea_2 = value;
	}

	inline static int32_t get_offset_of__sertrorseGawlallfi_3() { return static_cast<int32_t>(offsetof(M_teresorheLecerow112_t1897548578, ____sertrorseGawlallfi_3)); }
	inline bool get__sertrorseGawlallfi_3() const { return ____sertrorseGawlallfi_3; }
	inline bool* get_address_of__sertrorseGawlallfi_3() { return &____sertrorseGawlallfi_3; }
	inline void set__sertrorseGawlallfi_3(bool value)
	{
		____sertrorseGawlallfi_3 = value;
	}

	inline static int32_t get_offset_of__treebawReldido_4() { return static_cast<int32_t>(offsetof(M_teresorheLecerow112_t1897548578, ____treebawReldido_4)); }
	inline bool get__treebawReldido_4() const { return ____treebawReldido_4; }
	inline bool* get_address_of__treebawReldido_4() { return &____treebawReldido_4; }
	inline void set__treebawReldido_4(bool value)
	{
		____treebawReldido_4 = value;
	}

	inline static int32_t get_offset_of__largerCeratay_5() { return static_cast<int32_t>(offsetof(M_teresorheLecerow112_t1897548578, ____largerCeratay_5)); }
	inline int32_t get__largerCeratay_5() const { return ____largerCeratay_5; }
	inline int32_t* get_address_of__largerCeratay_5() { return &____largerCeratay_5; }
	inline void set__largerCeratay_5(int32_t value)
	{
		____largerCeratay_5 = value;
	}

	inline static int32_t get_offset_of__morfocooMuhehaw_6() { return static_cast<int32_t>(offsetof(M_teresorheLecerow112_t1897548578, ____morfocooMuhehaw_6)); }
	inline uint32_t get__morfocooMuhehaw_6() const { return ____morfocooMuhehaw_6; }
	inline uint32_t* get_address_of__morfocooMuhehaw_6() { return &____morfocooMuhehaw_6; }
	inline void set__morfocooMuhehaw_6(uint32_t value)
	{
		____morfocooMuhehaw_6 = value;
	}

	inline static int32_t get_offset_of__rorfur_7() { return static_cast<int32_t>(offsetof(M_teresorheLecerow112_t1897548578, ____rorfur_7)); }
	inline int32_t get__rorfur_7() const { return ____rorfur_7; }
	inline int32_t* get_address_of__rorfur_7() { return &____rorfur_7; }
	inline void set__rorfur_7(int32_t value)
	{
		____rorfur_7 = value;
	}

	inline static int32_t get_offset_of__muba_8() { return static_cast<int32_t>(offsetof(M_teresorheLecerow112_t1897548578, ____muba_8)); }
	inline bool get__muba_8() const { return ____muba_8; }
	inline bool* get_address_of__muba_8() { return &____muba_8; }
	inline void set__muba_8(bool value)
	{
		____muba_8 = value;
	}

	inline static int32_t get_offset_of__sineBirroubu_9() { return static_cast<int32_t>(offsetof(M_teresorheLecerow112_t1897548578, ____sineBirroubu_9)); }
	inline bool get__sineBirroubu_9() const { return ____sineBirroubu_9; }
	inline bool* get_address_of__sineBirroubu_9() { return &____sineBirroubu_9; }
	inline void set__sineBirroubu_9(bool value)
	{
		____sineBirroubu_9 = value;
	}

	inline static int32_t get_offset_of__raijem_10() { return static_cast<int32_t>(offsetof(M_teresorheLecerow112_t1897548578, ____raijem_10)); }
	inline bool get__raijem_10() const { return ____raijem_10; }
	inline bool* get_address_of__raijem_10() { return &____raijem_10; }
	inline void set__raijem_10(bool value)
	{
		____raijem_10 = value;
	}

	inline static int32_t get_offset_of__jairkeeca_11() { return static_cast<int32_t>(offsetof(M_teresorheLecerow112_t1897548578, ____jairkeeca_11)); }
	inline float get__jairkeeca_11() const { return ____jairkeeca_11; }
	inline float* get_address_of__jairkeeca_11() { return &____jairkeeca_11; }
	inline void set__jairkeeca_11(float value)
	{
		____jairkeeca_11 = value;
	}

	inline static int32_t get_offset_of__pailou_12() { return static_cast<int32_t>(offsetof(M_teresorheLecerow112_t1897548578, ____pailou_12)); }
	inline float get__pailou_12() const { return ____pailou_12; }
	inline float* get_address_of__pailou_12() { return &____pailou_12; }
	inline void set__pailou_12(float value)
	{
		____pailou_12 = value;
	}

	inline static int32_t get_offset_of__gaberedowSirjel_13() { return static_cast<int32_t>(offsetof(M_teresorheLecerow112_t1897548578, ____gaberedowSirjel_13)); }
	inline uint32_t get__gaberedowSirjel_13() const { return ____gaberedowSirjel_13; }
	inline uint32_t* get_address_of__gaberedowSirjel_13() { return &____gaberedowSirjel_13; }
	inline void set__gaberedowSirjel_13(uint32_t value)
	{
		____gaberedowSirjel_13 = value;
	}

	inline static int32_t get_offset_of__holeTawwhurme_14() { return static_cast<int32_t>(offsetof(M_teresorheLecerow112_t1897548578, ____holeTawwhurme_14)); }
	inline bool get__holeTawwhurme_14() const { return ____holeTawwhurme_14; }
	inline bool* get_address_of__holeTawwhurme_14() { return &____holeTawwhurme_14; }
	inline void set__holeTawwhurme_14(bool value)
	{
		____holeTawwhurme_14 = value;
	}

	inline static int32_t get_offset_of__heacellis_15() { return static_cast<int32_t>(offsetof(M_teresorheLecerow112_t1897548578, ____heacellis_15)); }
	inline float get__heacellis_15() const { return ____heacellis_15; }
	inline float* get_address_of__heacellis_15() { return &____heacellis_15; }
	inline void set__heacellis_15(float value)
	{
		____heacellis_15 = value;
	}

	inline static int32_t get_offset_of__marpere_16() { return static_cast<int32_t>(offsetof(M_teresorheLecerow112_t1897548578, ____marpere_16)); }
	inline bool get__marpere_16() const { return ____marpere_16; }
	inline bool* get_address_of__marpere_16() { return &____marpere_16; }
	inline void set__marpere_16(bool value)
	{
		____marpere_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
