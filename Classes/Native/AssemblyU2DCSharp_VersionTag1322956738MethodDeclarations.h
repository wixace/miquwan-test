﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VersionTag
struct VersionTag_t1322956738;

#include "codegen/il2cpp-codegen.h"

// System.Void VersionTag::.ctor()
extern "C"  void VersionTag__ctor_m28736281 (VersionTag_t1322956738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
