﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_lesayqeFasairba158
struct  M_lesayqeFasairba158_t225446925  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_lesayqeFasairba158::_zarnurrea
	uint32_t ____zarnurrea_0;
	// System.String GarbageiOS.M_lesayqeFasairba158::_merrurVeecar
	String_t* ____merrurVeecar_1;
	// System.String GarbageiOS.M_lesayqeFasairba158::_faiburweKorfea
	String_t* ____faiburweKorfea_2;
	// System.Int32 GarbageiOS.M_lesayqeFasairba158::_tijer
	int32_t ____tijer_3;
	// System.Int32 GarbageiOS.M_lesayqeFasairba158::_xarwhir
	int32_t ____xarwhir_4;
	// System.UInt32 GarbageiOS.M_lesayqeFasairba158::_lusisdi
	uint32_t ____lusisdi_5;
	// System.String GarbageiOS.M_lesayqeFasairba158::_whejusu
	String_t* ____whejusu_6;
	// System.Single GarbageiOS.M_lesayqeFasairba158::_raigarBuborwo
	float ____raigarBuborwo_7;
	// System.Int32 GarbageiOS.M_lesayqeFasairba158::_tuje
	int32_t ____tuje_8;
	// System.Int32 GarbageiOS.M_lesayqeFasairba158::_jalawBerejaqu
	int32_t ____jalawBerejaqu_9;
	// System.Int32 GarbageiOS.M_lesayqeFasairba158::_zukou
	int32_t ____zukou_10;
	// System.Single GarbageiOS.M_lesayqeFasairba158::_qernairawCaywofi
	float ____qernairawCaywofi_11;
	// System.String GarbageiOS.M_lesayqeFasairba158::_bikefe
	String_t* ____bikefe_12;
	// System.Boolean GarbageiOS.M_lesayqeFasairba158::_lertrea
	bool ____lertrea_13;
	// System.String GarbageiOS.M_lesayqeFasairba158::_mearpoupaWairlere
	String_t* ____mearpoupaWairlere_14;
	// System.Single GarbageiOS.M_lesayqeFasairba158::_migalStiwai
	float ____migalStiwai_15;

public:
	inline static int32_t get_offset_of__zarnurrea_0() { return static_cast<int32_t>(offsetof(M_lesayqeFasairba158_t225446925, ____zarnurrea_0)); }
	inline uint32_t get__zarnurrea_0() const { return ____zarnurrea_0; }
	inline uint32_t* get_address_of__zarnurrea_0() { return &____zarnurrea_0; }
	inline void set__zarnurrea_0(uint32_t value)
	{
		____zarnurrea_0 = value;
	}

	inline static int32_t get_offset_of__merrurVeecar_1() { return static_cast<int32_t>(offsetof(M_lesayqeFasairba158_t225446925, ____merrurVeecar_1)); }
	inline String_t* get__merrurVeecar_1() const { return ____merrurVeecar_1; }
	inline String_t** get_address_of__merrurVeecar_1() { return &____merrurVeecar_1; }
	inline void set__merrurVeecar_1(String_t* value)
	{
		____merrurVeecar_1 = value;
		Il2CppCodeGenWriteBarrier(&____merrurVeecar_1, value);
	}

	inline static int32_t get_offset_of__faiburweKorfea_2() { return static_cast<int32_t>(offsetof(M_lesayqeFasairba158_t225446925, ____faiburweKorfea_2)); }
	inline String_t* get__faiburweKorfea_2() const { return ____faiburweKorfea_2; }
	inline String_t** get_address_of__faiburweKorfea_2() { return &____faiburweKorfea_2; }
	inline void set__faiburweKorfea_2(String_t* value)
	{
		____faiburweKorfea_2 = value;
		Il2CppCodeGenWriteBarrier(&____faiburweKorfea_2, value);
	}

	inline static int32_t get_offset_of__tijer_3() { return static_cast<int32_t>(offsetof(M_lesayqeFasairba158_t225446925, ____tijer_3)); }
	inline int32_t get__tijer_3() const { return ____tijer_3; }
	inline int32_t* get_address_of__tijer_3() { return &____tijer_3; }
	inline void set__tijer_3(int32_t value)
	{
		____tijer_3 = value;
	}

	inline static int32_t get_offset_of__xarwhir_4() { return static_cast<int32_t>(offsetof(M_lesayqeFasairba158_t225446925, ____xarwhir_4)); }
	inline int32_t get__xarwhir_4() const { return ____xarwhir_4; }
	inline int32_t* get_address_of__xarwhir_4() { return &____xarwhir_4; }
	inline void set__xarwhir_4(int32_t value)
	{
		____xarwhir_4 = value;
	}

	inline static int32_t get_offset_of__lusisdi_5() { return static_cast<int32_t>(offsetof(M_lesayqeFasairba158_t225446925, ____lusisdi_5)); }
	inline uint32_t get__lusisdi_5() const { return ____lusisdi_5; }
	inline uint32_t* get_address_of__lusisdi_5() { return &____lusisdi_5; }
	inline void set__lusisdi_5(uint32_t value)
	{
		____lusisdi_5 = value;
	}

	inline static int32_t get_offset_of__whejusu_6() { return static_cast<int32_t>(offsetof(M_lesayqeFasairba158_t225446925, ____whejusu_6)); }
	inline String_t* get__whejusu_6() const { return ____whejusu_6; }
	inline String_t** get_address_of__whejusu_6() { return &____whejusu_6; }
	inline void set__whejusu_6(String_t* value)
	{
		____whejusu_6 = value;
		Il2CppCodeGenWriteBarrier(&____whejusu_6, value);
	}

	inline static int32_t get_offset_of__raigarBuborwo_7() { return static_cast<int32_t>(offsetof(M_lesayqeFasairba158_t225446925, ____raigarBuborwo_7)); }
	inline float get__raigarBuborwo_7() const { return ____raigarBuborwo_7; }
	inline float* get_address_of__raigarBuborwo_7() { return &____raigarBuborwo_7; }
	inline void set__raigarBuborwo_7(float value)
	{
		____raigarBuborwo_7 = value;
	}

	inline static int32_t get_offset_of__tuje_8() { return static_cast<int32_t>(offsetof(M_lesayqeFasairba158_t225446925, ____tuje_8)); }
	inline int32_t get__tuje_8() const { return ____tuje_8; }
	inline int32_t* get_address_of__tuje_8() { return &____tuje_8; }
	inline void set__tuje_8(int32_t value)
	{
		____tuje_8 = value;
	}

	inline static int32_t get_offset_of__jalawBerejaqu_9() { return static_cast<int32_t>(offsetof(M_lesayqeFasairba158_t225446925, ____jalawBerejaqu_9)); }
	inline int32_t get__jalawBerejaqu_9() const { return ____jalawBerejaqu_9; }
	inline int32_t* get_address_of__jalawBerejaqu_9() { return &____jalawBerejaqu_9; }
	inline void set__jalawBerejaqu_9(int32_t value)
	{
		____jalawBerejaqu_9 = value;
	}

	inline static int32_t get_offset_of__zukou_10() { return static_cast<int32_t>(offsetof(M_lesayqeFasairba158_t225446925, ____zukou_10)); }
	inline int32_t get__zukou_10() const { return ____zukou_10; }
	inline int32_t* get_address_of__zukou_10() { return &____zukou_10; }
	inline void set__zukou_10(int32_t value)
	{
		____zukou_10 = value;
	}

	inline static int32_t get_offset_of__qernairawCaywofi_11() { return static_cast<int32_t>(offsetof(M_lesayqeFasairba158_t225446925, ____qernairawCaywofi_11)); }
	inline float get__qernairawCaywofi_11() const { return ____qernairawCaywofi_11; }
	inline float* get_address_of__qernairawCaywofi_11() { return &____qernairawCaywofi_11; }
	inline void set__qernairawCaywofi_11(float value)
	{
		____qernairawCaywofi_11 = value;
	}

	inline static int32_t get_offset_of__bikefe_12() { return static_cast<int32_t>(offsetof(M_lesayqeFasairba158_t225446925, ____bikefe_12)); }
	inline String_t* get__bikefe_12() const { return ____bikefe_12; }
	inline String_t** get_address_of__bikefe_12() { return &____bikefe_12; }
	inline void set__bikefe_12(String_t* value)
	{
		____bikefe_12 = value;
		Il2CppCodeGenWriteBarrier(&____bikefe_12, value);
	}

	inline static int32_t get_offset_of__lertrea_13() { return static_cast<int32_t>(offsetof(M_lesayqeFasairba158_t225446925, ____lertrea_13)); }
	inline bool get__lertrea_13() const { return ____lertrea_13; }
	inline bool* get_address_of__lertrea_13() { return &____lertrea_13; }
	inline void set__lertrea_13(bool value)
	{
		____lertrea_13 = value;
	}

	inline static int32_t get_offset_of__mearpoupaWairlere_14() { return static_cast<int32_t>(offsetof(M_lesayqeFasairba158_t225446925, ____mearpoupaWairlere_14)); }
	inline String_t* get__mearpoupaWairlere_14() const { return ____mearpoupaWairlere_14; }
	inline String_t** get_address_of__mearpoupaWairlere_14() { return &____mearpoupaWairlere_14; }
	inline void set__mearpoupaWairlere_14(String_t* value)
	{
		____mearpoupaWairlere_14 = value;
		Il2CppCodeGenWriteBarrier(&____mearpoupaWairlere_14, value);
	}

	inline static int32_t get_offset_of__migalStiwai_15() { return static_cast<int32_t>(offsetof(M_lesayqeFasairba158_t225446925, ____migalStiwai_15)); }
	inline float get__migalStiwai_15() const { return ____migalStiwai_15; }
	inline float* get_address_of__migalStiwai_15() { return &____migalStiwai_15; }
	inline void set__migalStiwai_15(float value)
	{
		____migalStiwai_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
