﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// JSMgr/CSCallbackField
struct CSCallbackField_t1235469093;
// JSMgr/CSCallbackProperty
struct CSCallbackProperty_t2220951498;
// JSMgr/MethodCallBackInfo
struct MethodCallBackInfo_t2808125524;
// JSMgr/CallbackInfo
struct CallbackInfo_t3768479923;
// EventDelegate
struct EventDelegate_t4004424223;
// ACData
struct ACData_t1924893420;
// BMGlyph
struct BMGlyph_t719052705;
// BuffState
struct BuffState_t2048909278;
// CSHeroUnit
struct CSHeroUnit_t3764358446;
// CSPlusAtt
struct CSPlusAtt_t3268315159;
// CSGuideVO
struct CSGuideVO_t4116883493;
// CSSkillData
struct CSSkillData_t432288523;
// CSAttrBuffData
struct CSAttrBuffData_t1041646654;
// CSPlayerData/FightingPos
struct FightingPos_t1575607726;
// CSProvingGroundsHeroData
struct CSProvingGroundsHeroData_t1054716455;
// CSPVPRobotNPC
struct CSPVPRobotNPC_t3152738001;
// cameraShotCfg
struct cameraShotCfg_t781157413;
// CsCfgBase
struct CsCfgBase_t69924517;
// EnemyDropData
struct EnemyDropData_t1468587713;
// AIObject
struct AIObject_t37280135;
// EventDelegate/Parameter
struct Parameter_t2566940569;
// CombatEntity
struct CombatEntity_t684137495;
// CEvent.EventFunc`1<System.Object>
struct EventFunc_1_t1647712181;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// Hero
struct Hero_t2245658;
// FileInfoRes
struct FileInfoRes_t1217059798;
// UIWidget
struct UIWidget_t769069560;
// UIRect
struct UIRect_t2503437976;
// JSCLevelMonsterConfig
struct JSCLevelMonsterConfig_t1924079698;
// TeamSkill
struct TeamSkill_t1816898004;
// UISpriteData
struct UISpriteData_t3578345923;
// UICamera
struct UICamera_t189364953;
// UIDrawCall
struct UIDrawCall_t913273974;
// BMSymbol
struct BMSymbol_t1170982339;
// UIKeyNavigation
struct UIKeyNavigation_t1837256607;
// UIPanel
struct UIPanel_t295209936;
// UIRoot
struct UIRoot_t2503447958;
// UIScrollView
struct UIScrollView_t2113479878;
// UIToggle
struct UIToggle_t688812808;
// VersionInfo
struct VersionInfo_t2356638086;
// JSComponent
struct JSComponent_t1642894772;
// ICsObj
struct ICsObj_t2155308958;
// JSSerializer
struct JSSerializer_t3534558139;
// GenericTypeCache/TypeMembers
struct TypeMembers_t2369396673;
// JSCache/TypeInfo
struct TypeInfo_t3198813374;
// JSDebugMessages/Message
struct Message_t903789774;
// JSMgr/JS_CS_Rel
struct JS_CS_Rel_t3554103776;
// JSSerializer/SerializeStruct
struct SerializeStruct_t2767317089;
// Scripts.JSBindingClasses.JsRepresentClass
struct JsRepresentClass_t2913492551;
// SevenZip.CommandLineParser.SwitchResult
struct SwitchResult_t80294439;
// SevenZip.CommandLineParser.SwitchForm
struct SwitchForm_t300389966;
// SevenZip.CommandLineParser.CommandForm
struct CommandForm_t3963007505;
// SevenZip.CommandLineParser.CommandSubCharsSet
struct CommandSubCharsSet_t4216237968;
// SevenZip.Compression.LZMA.Encoder/Optimal
struct Optimal_t1633575781;
// Pathfinding.RichPathPart
struct RichPathPart_t2520985130;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// Pathfinding.RichFunnel
struct RichFunnel_t2453525928;
// Pathfinding.RichSpecial
struct RichSpecial_t2303562271;
// Pathfinding.TriangleMeshNode
struct TriangleMeshNode_t1626248749;
// Pathfinding.IPathModifier
struct IPathModifier_t1088723335;
// OnPathDelegate
struct OnPathDelegate_t598607977;
// Pathfinding.NavGraph
struct NavGraph_t1254319713;
// Pathfinding.UserConnection
struct UserConnection_t885308927;
// Pathfinding.GraphUpdateObject
struct GraphUpdateObject_t430843704;
// Pathfinding.PathNode[]
struct PathNodeU5BU5D_t1913565744;
// Pathfinding.PathNode
struct PathNode_t417131581;
// Pathfinding.AnimationLink/LinkClip
struct LinkClip_t2650609029;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.ABPath
struct ABPath_t1187561148;
// Pathfinding.GraphModifier
struct GraphModifier_t2555428519;
// Pathfinding.NodeLink2
struct NodeLink2_t1645404664;
// Pathfinding.NodeLink3
struct NodeLink3_t1645404665;
// Pathfinding.PointNode
struct PointNode_t2761813780;
// Pathfinding.RVO.ObstacleVertex
struct ObstacleVertex_t4170307099;
// Pathfinding.RVO.Sampled.Agent
struct Agent_t1290054243;
// Pathfinding.RVO.Simulator/Worker
struct Worker_t3927454006;
// Pathfinding.UnityReferenceHelper
struct UnityReferenceHelper_t2955464730;
// Pathfinding.GraphEditorBase
struct GraphEditorBase_t3362408622;
// RVOExampleAgent
struct RVOExampleAgent_t1174908390;
// Pathfinding.RVO.IAgent
struct IAgent_t2802767396;
// Pathfinding.GridNode
struct GridNode_t3795753694;
// ProceduralWorld/ProceduralPrefab
struct ProceduralPrefab_t2162393179;
// ProceduralWorld/ProceduralTile
struct ProceduralTile_t3586714437;
// Pathfinding.RichAI
struct RichAI_t1710845178;
// AIPath
struct AIPath_t1930792045;
// Pathfinding.LevelGridNode
struct LevelGridNode_t489265774;
// Pathfinding.LinkedLevelCell
struct LinkedLevelCell_t935711695;
// Pathfinding.LayerGridGraph
struct LayerGridGraph_t3415576653;
// Pathfinding.IUpdatableGraph
struct IUpdatableGraph_t4229287971;
// Pathfinding.IRaycastableGraph
struct IRaycastableGraph_t2032416694;
// Pathfinding.GridGraph
struct GridGraph_t2455707914;
// Pathfinding.MeshNode
struct MeshNode_t3005053445;
// Pathfinding.INavmeshHolder
struct INavmeshHolder_t1019551337;
// Pathfinding.QuadtreeNode
struct QuadtreeNode_t1423301757;
// Pathfinding.RecastGraph/NavmeshTile
struct NavmeshTile_t4022309569;
// Pathfinding.INavmesh
struct INavmesh_t889550173;
// Pathfinding.RecastGraph/CapsuleCache
struct CapsuleCache_t2032660306;
// Pathfinding.RecastMeshObj
struct RecastMeshObj_t2069195738;
// Pathfinding.RecastBBTreeBox
struct RecastBBTreeBox_t3100392477;
// Pathfinding.Util.TileHandler/TileType
struct TileType_t2364590013;
// Pathfinding.NavmeshCut
struct NavmeshCut_t300992648;
// Pathfinding.NavmeshAdd
struct NavmeshAdd_t300990183;
// Pathfinding.LocalAvoidance
struct LocalAvoidance_t2375621135;
// Pathfinding.LocalAvoidance/VO
struct VO_t2172220293;
// Pathfinding.RelevantGraphSurface
struct RelevantGraphSurface_t4201206834;
// Pathfinding.ConstantPath
struct ConstantPath_t275096927;
// Pathfinding.FleePath
struct FleePath_t909342113;
// Pathfinding.FloodPath
struct FloodPath_t3766979749;
// Pathfinding.FloodPathTracer
struct FloodPathTracer_t1668942290;
// Pathfinding.MultiTargetPath
struct MultiTargetPath_t4131434065;
// Pathfinding.RandomPath
struct RandomPath_t3634790782;
// Pathfinding.AstarProfiler/ProfilePoint
struct ProfilePoint_t940090916;
// FS_ShadowManager
struct FS_ShadowManager_t363579995;
// FS_ShadowSimple
struct FS_ShadowSimple_t4208748868;
// GestureRecognizer
struct GestureRecognizer_t3512875949;
// FingerClusterManager/Cluster
struct Cluster_t3870231303;
// FingerDownEvent
struct FingerDownEvent_t356126095;
// FingerHoverEvent
struct FingerHoverEvent_t1767068455;
// FingerMotionEvent
struct FingerMotionEvent_t3307437371;
// FingerUpEvent
struct FingerUpEvent_t3093014774;
// FingerGestures/Finger
struct Finger_t182428197;
// DragGesture
struct DragGesture_t2914643285;
// LongPressGesture
struct LongPressGesture_t2876118082;
// PinchGesture
struct PinchGesture_t1502590799;
// PointCloudGesture
struct PointCloudGesture_t1959506660;
// PointCloudGestureTemplate
struct PointCloudGestureTemplate_t3506552702;
// PointCloudRegognizer/NormalizedTemplate
struct NormalizedTemplate_t2023934299;
// SwipeGesture
struct SwipeGesture_t529355983;
// TapGesture
struct TapGesture_t659145798;
// TwistGesture
struct TwistGesture_t4198361154;
// Newtonsoft.Json.Bson.BsonReader/ContainerContext
struct ContainerContext_t1096623169;
// Newtonsoft.Json.Bson.BsonProperty
struct BsonProperty_t4293821333;
// Newtonsoft.Json.Bson.BsonToken
struct BsonToken_t455725415;
// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>
struct BidirectionalDictionary_2_t157076046;
// Newtonsoft.Json.Converters.IXmlNode
struct IXmlNode_t2349752174;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t2159686854;
// Newtonsoft.Json.Schema.JsonSchemaModel
struct JsonSchemaModel_t3035618138;
// Newtonsoft.Json.JsonValidatingReader/SchemaScope
struct SchemaScope_t3540946455;
// Newtonsoft.Json.JsonWriter/State[]
struct StateU5BU5D_t871800199;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Schema.JsonSchema
struct JsonSchema_t460567603;
// Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema
struct TypeSchema_t486414904;
// Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>
struct EnumValue_1_t685341039;
// Newtonsoft.Json.Serialization.JsonProperty
struct JsonProperty_t902655177;
// Newtonsoft.Json.Schema.JsonSchemaNode
struct JsonSchemaNode_t2115227093;
// Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>
struct EnumValue_1_t3851137816;
// Newtonsoft.Json.Serialization.JsonContract
struct JsonContract_t1328848902;
// Newtonsoft.Json.JsonContainerAttribute
struct JsonContainerAttribute_t1917602971;
// System.Runtime.Serialization.DataContractAttribute
struct DataContractAttribute_t2462274566;
// System.Runtime.Serialization.DataMemberAttribute
struct DataMemberAttribute_t2601848894;
// MeleeWeaponTrail/Point
struct Point_t3928961399;
// UITweener
struct UITweener_t105489188;
// UILabel
struct UILabel_t291504320;
// UIPlaySound
struct UIPlaySound_t532605447;
// UIAtlas/Sprite
struct Sprite_t2668923293;
// UISprite
struct UISprite_t661437049;
// UIBasicSprite
struct UIBasicSprite_t2501337439;
// UIFont
struct UIFont_t2503090435;
// UICamera/MouseOrTouch
struct MouseOrTouch_t2057376333;
// UITextList/Paragraph
struct Paragraph_t3894573406;
// ProtoBuf.Meta.AttributeMap
struct AttributeMap_t924393598;
// ProtoBuf.Serializers.IProtoSerializer
struct IProtoSerializer_t3033312651;
// ProtoBuf.ProtoMemberAttribute
struct ProtoMemberAttribute_t2007578278;
// ProtoBuf.Meta.ValueMember
struct ValueMember_t110398141;
// ProtoBuf.Meta.SubType
struct SubType_t3836516844;
// ProtoBuf.Meta.MetaType
struct MetaType_t448283965;
// ProtoBuf.Serializers.ISerializerProxy
struct ISerializerProxy_t1336599533;
// BossAnimationInfo
struct BossAnimationInfo_t384335813;
// AnimationPointJSC
struct AnimationPointJSC_t487595022;
// BAMCameras
struct BAMCameras_t1661014560;
// Mihua.Utils.WaitForSeconds
struct WaitForSeconds_t3217447863;
// TweenPosition
struct TweenPosition_t3684358292;
// ChannelConfigCfg
struct ChannelConfigCfg_t4272979999;
// HeroEmbattleCfg
struct HeroEmbattleCfg_t4134124650;
// JJCCoefficientCfg
struct JJCCoefficientCfg_t2755051730;
// PetsCfg
struct PetsCfg_t988016752;
// SoundCfg
struct SoundCfg_t1807275253;
// StoryCfg
struct StoryCfg_t1782371151;
// TextStringCfg
struct TextStringCfg_t884535238;
// UIEffectCfg
struct UIEffectCfg_t952653023;
// all_configCfg
struct all_configCfg_t3865068708;
// asset_sharedCfg
struct asset_sharedCfg_t1901313840;
// barrierGateCfg
struct barrierGateCfg_t3705088994;
// buffCfg
struct buffCfg_t227963665;
// checkpointCfg
struct checkpointCfg_t2816107964;
// demoStoryCfg
struct demoStoryCfg_t1993162290;
// effectCfg
struct effectCfg_t2826279187;
// elementCfg
struct elementCfg_t575911880;
// enemy_dropCfg
struct enemy_dropCfg_t1026960702;
// friendNpcCfg
struct friendNpcCfg_t3890310657;
// guideCfg
struct guideCfg_t2981043144;
// herosCfg
struct herosCfg_t3676934635;
// monstersCfg
struct monstersCfg_t1542396363;
// npcAICfg
struct npcAICfg_t1948330203;
// npcAIBehaviorCfg
struct npcAIBehaviorCfg_t3456736809;
// npcAIConditionCfg
struct npcAIConditionCfg_t930197170;
// portalCfg
struct portalCfg_t1117034584;
// skillCfg
struct skillCfg_t2142425171;
// skill_upgradeCfg
struct skill_upgradeCfg_t790726486;
// superskillCfg
struct superskillCfg_t3024131278;
// taixuTipCfg
struct taixuTipCfg_t1268722370;
// towerbuffCfg
struct towerbuffCfg_t1755856872;
// CameraAniMap
struct CameraAniMap_t3004656005;
// CameraAniGroup
struct CameraAniGroup_t1251365992;
// CameraShotsCfg
struct CameraShotsCfg_t1740446544;
// CurCameraShotCfg
struct CurCameraShotCfg_t224861669;
// CameraShotNodeCfg
struct CameraShotNodeCfg_t3577740835;
// CameraShotHeroOrNpcCfg
struct CameraShotHeroOrNpcCfg_t2707952927;
// CameraShotStoryCfg
struct CameraShotStoryCfg_t4130953262;
// CameraShotGameSpeedCfg
struct CameraShotGameSpeedCfg_t308624942;
// CameraShotActionCfg
struct CameraShotActionCfg_t1993837871;
// CameraShotSkillCfg
struct CameraShotSkillCfg_t260684114;
// CameraShotMoveCfg
struct CameraShotMoveCfg_t2706860532;
// CameraShotCircleCfg
struct CameraShotCircleCfg_t2591002709;
// CameraShotEffectCfg
struct CameraShotEffectCfg_t326881268;
// JSCLevelConfig
struct JSCLevelConfig_t1411099500;
// JSCMapPathConfig
struct JSCMapPathConfig_t3426118729;
// JSCWaveNpcConfig
struct JSCWaveNpcConfig_t4221504656;
// JSCMapPathPointInfoConfig
struct JSCMapPathPointInfoConfig_t2703681049;
// JSCLevelHeroPointConfig
struct JSCLevelHeroPointConfig_t561690542;
// TabData
struct TabData_t110553279;
// ByteReadArray
struct ByteReadArray_t751193627;
// UnitStateBase
struct UnitStateBase_t1040218558;
// shaderBuff
struct shaderBuff_t2982920664;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;
// AIEventInfo
struct AIEventInfo_t4031722272;
// PlotFightTalkMgr/plotData
struct plotData_t645270205;
// ReplayBase
struct ReplayBase_t779703160;
// ReplayHeroUnit
struct ReplayHeroUnit_t605237701;
// HitTarget
struct HitTarget_t3687896804;
// Buff
struct Buff_t2081907;
// Skill
struct Skill_t79944241;
// DamageSeparationSkill
struct DamageSeparationSkill_t2001134364;
// ElementVO
struct ElementVO_t1745451669;
// FlowControl.IFlowBase
struct IFlowBase_t1464761278;
// Blood
struct Blood_t64280026;
// EnemyDropItem
struct EnemyDropItem_t1468754474;
// NumEffect
struct NumEffect_t2824221847;
// HeroMgr/LineupPos
struct LineupPos_t2885729524;
// TweenScale
struct TweenScale_t2936666559;
// TargetMgr/FightPoint
struct FightPoint_t4216057832;
// TargetMgr/AngleEntity
struct AngleEntity_t3194128142;
// Mihua.Asset.ABLoadOperation.AssetOperationPreload1
struct AssetOperationPreload1_t2389844357;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;
// FileInfoCrc
struct FileInfoCrc_t1217045770;
// Mihua.Net.UrlLoader
struct UrlLoader_t2490729496;
// CSCampFightMonsterData
struct CSCampFightMonsterData_t1577764261;
// CSCampFightAddAttributeData
struct CSCampFightAddAttributeData_t790660836;
// IEffComponent
struct IEffComponent_t3802264961;
// EffectCtrl
struct EffectCtrl_t3708787644;
// FloatTextUnit
struct FloatTextUnit_t2362298029;
// UITexture
struct UITexture_t3903132647;
// TweenAlpha
struct TweenAlpha_t2920325587;
// StaticParticle
struct StaticParticle_t2206539572;
// MHIAPMgr.PayInfo
struct PayInfo_t444430236;
// SoundObj
struct SoundObj_t1807286664;
// ISound
struct ISound_t2170003014;
// CameraShotMgr/CameraShotVO
struct CameraShotVO_t1900810094;
// StoryMgr/StoryVO
struct StoryVO_t3225531714;
// TimeUpdateVo
struct TimeUpdateVo_t1829512047;
// AssetDownMgr/SetBytes
struct SetBytes_t2856136722;
// UIModelDisplay
struct UIModelDisplay_t1730520589;
// Pomelo.DotNetClient.MsgEvent
struct MsgEvent_t82323445;
// InputItem
struct InputItem_t3710611933;
// PluginPush/NotificationData
struct NotificationData_t3289515031;
// UpdateForJs
struct UpdateForJs_t1071193225;
// IZUpdate
struct IZUpdate_t3482043738;
// MScrollView/OnMoveCallBackFun
struct OnMoveCallBackFun_t3535987578;
// T4MLodObjSC
struct T4MLodObjSC_t2288258003;
// T4MBillBObjSC
struct T4MBillBObjSC_t2448121;

#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_JSMgr_CSCallbackField1235469093.h"
#include "AssemblyU2DCSharp_JSMgr_CSCallbackProperty2220951498.h"
#include "AssemblyU2DCSharp_JSMgr_MethodCallBackInfo2808125524.h"
#include "AssemblyU2DCSharp_JSMgr_CallbackInfo3768479923.h"
#include "AssemblyU2DCSharp_EventDelegate4004424223.h"
#include "AssemblyU2DCSharp_ACData1924893420.h"
#include "AssemblyU2DCSharp_BMGlyph719052705.h"
#include "AssemblyU2DCSharp_TypeFlag3682875878.h"
#include "AssemblyU2DCSharp_BuffState2048909278.h"
#include "AssemblyU2DCSharp_CSHeroUnit3764358446.h"
#include "AssemblyU2DCSharp_CSPlusAtt3268315159.h"
#include "AssemblyU2DCSharp_CSGuideVO4116883493.h"
#include "AssemblyU2DCSharp_CSSkillData432288523.h"
#include "AssemblyU2DCSharp_CSAttrBuffData1041646654.h"
#include "AssemblyU2DCSharp_CSPlayerData_FightingPos1575607726.h"
#include "AssemblyU2DCSharp_CSProvingGroundsHeroData1054716455.h"
#include "AssemblyU2DCSharp_CSPVPRobotNPC3152738001.h"
#include "AssemblyU2DCSharp_cameraShotCfg781157413.h"
#include "AssemblyU2DCSharp_CsCfgBase69924517.h"
#include "AssemblyU2DCSharp_EnemyDropData1468587713.h"
#include "AssemblyU2DCSharp_AIObject37280135.h"
#include "AssemblyU2DCSharp_EventDelegate_Parameter2566940569.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_CEvent_EventFunc_1_gen1647712181.h"
#include "AssemblyU2DCSharp_CEvent_EventFunc_1_gen1114914310.h"
#include "AssemblyU2DCSharp_Hero2245658.h"
#include "AssemblyU2DCSharp_FileInfoRes1217059798.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UIRect2503437976.h"
#include "AssemblyU2DCSharp_JSCLevelMonsterConfig1924079698.h"
#include "AssemblyU2DCSharp_FightEnum_ESkillBuffEvent465856265.h"
#include "AssemblyU2DCSharp_FightEnum_ESkillBuffTarget2441814200.h"
#include "AssemblyU2DCSharp_TeamSkill1816898004.h"
#include "AssemblyU2DCSharp_UISpriteData3578345923.h"
#include "AssemblyU2DCSharp_UICamera189364953.h"
#include "AssemblyU2DCSharp_UIDrawCall913273974.h"
#include "AssemblyU2DCSharp_BMSymbol1170982339.h"
#include "AssemblyU2DCSharp_UIKeyNavigation1837256607.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"
#include "AssemblyU2DCSharp_UIRoot2503447958.h"
#include "AssemblyU2DCSharp_UIScrollView2113479878.h"
#include "AssemblyU2DCSharp_UIToggle688812808.h"
#include "AssemblyU2DCSharp_VersionInfo2356638086.h"
#include "AssemblyU2DCSharp_JSComponent1642894772.h"
#include "AssemblyU2DCSharp_JSSerializer3534558139.h"
#include "AssemblyU2DCSharp_GenericTypeCache_TypeMembers2369396673.h"
#include "AssemblyU2DCSharp_JSCache_TypeInfo3198813374.h"
#include "AssemblyU2DCSharp_JSDebugMessages_Message903789774.h"
#include "AssemblyU2DCSharp_JSMgr_JS_CS_Rel3554103776.h"
#include "AssemblyU2DCSharp_JSSerializer_SerializeStruct2767317089.h"
#include "AssemblyU2DCSharp_Scripts_JSBindingClasses_JsRepre2913492551.h"
#include "AssemblyU2DCSharp_SevenZip_CommandLineParser_SwitchR80294439.h"
#include "AssemblyU2DCSharp_SevenZip_CommandLineParser_Switch300389966.h"
#include "AssemblyU2DCSharp_SevenZip_CommandLineParser_Comma3963007505.h"
#include "AssemblyU2DCSharp_SevenZip_CommandLineParser_Comma4216237968.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_4202293321.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_2730629963.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Decoder813340627.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Encode1633575781.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_1052492065.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_3875796003.h"
#include "AssemblyU2DCSharp_SevenZip_CoderPropID2354064101.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Encode2578924795.h"
#include "AssemblyU2DCSharp_Pathfinding_RichPathPart2520985130.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_RichFunnel2453525928.h"
#include "AssemblyU2DCSharp_Pathfinding_RichSpecial2303562271.h"
#include "AssemblyU2DCSharp_Pathfinding_TriangleMeshNode1626248749.h"
#include "AssemblyU2DCSharp_OnPathDelegate598607977.h"
#include "AssemblyU2DCSharp_Pathfinding_NavGraph1254319713.h"
#include "AssemblyU2DCSharp_Pathfinding_UserConnection885308927.h"
#include "AssemblyU2DCSharp_Pathfinding_PathThreadInfo2420662483.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphUpdateObject430843704.h"
#include "AssemblyU2DCSharp_AstarPath_GUOSingle3657339986.h"
#include "AssemblyU2DCSharp_AstarPath_AstarWorkItem2566693888.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"
#include "AssemblyU2DCSharp_Pathfinding_AnimationLink_LinkCl2650609029.h"
#include "AssemblyU2DCSharp_AstarDebugger_GraphPoint991085213.h"
#include "AssemblyU2DCSharp_AstarDebugger_PathTypeDebug4028073209.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Pathfinding_ABPath1187561148.h"
#include "AssemblyU2DCSharp_Pathfinding_BinaryHeapM_Tuple2515696975.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphModifier2555428519.h"
#include "AssemblyU2DCSharp_Pathfinding_NodeLink21645404664.h"
#include "AssemblyU2DCSharp_Pathfinding_NodeLink31645404665.h"
#include "AssemblyU2DCSharp_Pathfinding_PointNode2761813780.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_ObstacleVertex4170307099.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_Sampled_Agent1290054243.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_Sampled_Agent_VO2757914308.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_Simulator_Worker3927454006.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_RVOQuadtree_Node2108618958.h"
#include "AssemblyU2DCSharp_Pathfinding_UnityReferenceHelper2955464730.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphEditorBase3362408622.h"
#include "AssemblyU2DCSharp_RVOExampleAgent1174908390.h"
#include "AssemblyU2DCSharp_Pathfinding_GridNode3795753694.h"
#include "AssemblyU2DCSharp_ProceduralWorld_ProceduralPrefab2162393179.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"
#include "AssemblyU2DCSharp_ProceduralWorld_ProceduralTile3586714437.h"
#include "AssemblyU2DCSharp_Pathfinding_RichAI1710845178.h"
#include "AssemblyU2DCSharp_AIPath1930792045.h"
#include "AssemblyU2DCSharp_Pathfinding_GridGraph_TextureDat3235179551.h"
#include "AssemblyU2DCSharp_Pathfinding_LevelGridNode489265774.h"
#include "AssemblyU2DCSharp_Pathfinding_LinkedLevelCell935711695.h"
#include "AssemblyU2DCSharp_Pathfinding_LayerGridGraph3415576653.h"
#include "AssemblyU2DCSharp_Pathfinding_GridGraph2455707914.h"
#include "AssemblyU2DCSharp_Pathfinding_MeshNode3005053445.h"
#include "AssemblyU2DCSharp_Pathfinding_QuadtreeNode1423301757.h"
#include "AssemblyU2DCSharp_Pathfinding_RecastGraph_NavmeshT4022309569.h"
#include "AssemblyU2DCSharp_Pathfinding_RecastGraph_CapsuleC2032660306.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_ExtraMesh4218029715.h"
#include "AssemblyU2DCSharp_Pathfinding_RecastMeshObj2069195738.h"
#include "AssemblyU2DCSharp_Pathfinding_BBTree_BBTreeBox1958396198.h"
#include "AssemblyU2DCSharp_Pathfinding_RecastBBTreeBox3100392477.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_TileHandler_Til2364590013.h"
#include "AssemblyU2DCSharp_Pathfinding_NavmeshCut300992648.h"
#include "AssemblyU2DCSharp_Pathfinding_IntRect3015058261.h"
#include "AssemblyU2DCSharp_Pathfinding_NavmeshAdd300990183.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_CompactVoxelS3431582481.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_CompactVoxelC3431095593.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_LinkedVoxelSp3201576397.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_VoxelContour3597201016.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance2375621135.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_VO2172220293.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_VOLin2029931801.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_Inter2821319919.h"
#include "AssemblyU2DCSharp_Pathfinding_AdvancedSmooth_Turn465195378.h"
#include "AssemblyU2DCSharp_Pathfinding_RelevantGraphSurface4201206834.h"
#include "AssemblyU2DCSharp_Pathfinding_ConstantPath275096927.h"
#include "AssemblyU2DCSharp_Pathfinding_FleePath909342113.h"
#include "AssemblyU2DCSharp_Pathfinding_FloodPath3766979749.h"
#include "AssemblyU2DCSharp_Pathfinding_FloodPathTracer1668942290.h"
#include "AssemblyU2DCSharp_Pathfinding_MultiTargetPath4131434065.h"
#include "AssemblyU2DCSharp_Pathfinding_RandomPath3634790782.h"
#include "AssemblyU2DCSharp_Pathfinding_AstarProfiler_Profile940090916.h"
#include "AssemblyU2DCSharp_Core_RpsResult479594824.h"
#include "AssemblyU2DCSharp_Core_RpsChoice52797132.h"
#include "AssemblyU2DCSharp_FS_ShadowManager363579995.h"
#include "AssemblyU2DCSharp_FS_ShadowSimple4208748868.h"
#include "AssemblyU2DCSharp_GestureRecognizer3512875949.h"
#include "AssemblyU2DCSharp_FingerClusterManager_Cluster3870231303.h"
#include "AssemblyU2DCSharp_FingerDownEvent356126095.h"
#include "AssemblyU2DCSharp_FingerHoverEvent1767068455.h"
#include "AssemblyU2DCSharp_FingerMotionEvent3307437371.h"
#include "AssemblyU2DCSharp_FingerUpEvent3093014774.h"
#include "AssemblyU2DCSharp_FingerGestures_Finger182428197.h"
#include "AssemblyU2DCSharp_FingerGestures_SwipeDirection1218055201.h"
#include "AssemblyU2DCSharp_DragGesture2914643285.h"
#include "AssemblyU2DCSharp_LongPressGesture2876118082.h"
#include "AssemblyU2DCSharp_PinchGesture1502590799.h"
#include "AssemblyU2DCSharp_PointCloudRegognizer_Point1838831750.h"
#include "AssemblyU2DCSharp_PointCloudGesture1959506660.h"
#include "AssemblyU2DCSharp_PointCloudGestureTemplate3506552702.h"
#include "AssemblyU2DCSharp_PointCloudRegognizer_NormalizedT2023934299.h"
#include "AssemblyU2DCSharp_SwipeGesture529355983.h"
#include "AssemblyU2DCSharp_TapGesture659145798.h"
#include "AssemblyU2DCSharp_TwistGesture4198361154.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonReader_1096623169.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonPropert4293821333.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonToken455725415.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Bidirec157076046.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonConverter2159686854.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType3916897561.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3035618138.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonValidatingRe3540946455.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter_State671991922.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema460567603.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem2115415821.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema486414904.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_EnumVal685341039.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso902655177.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem2115227093.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_EnumVa3851137816.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js1328848902.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_De2971844791.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2892329490.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonContainerAtt1917602971.h"
#include "AssemblyU2DCSharp_System_Runtime_Serialization_Dat2462274566.h"
#include "AssemblyU2DCSharp_System_Runtime_Serialization_Dat2601848894.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Convert866134174.h"
#include "AssemblyU2DCSharp_MeleeWeaponTrail_Point3928961399.h"
#include "AssemblyU2DCSharp_TypewriterEffect_FadeEntry2858472101.h"
#include "AssemblyU2DCSharp_UITweener105489188.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "AssemblyU2DCSharp_UIPlaySound532605447.h"
#include "AssemblyU2DCSharp_UIAtlas_Sprite2668923293.h"
#include "AssemblyU2DCSharp_UISprite661437049.h"
#include "AssemblyU2DCSharp_UIBasicSprite2501337439.h"
#include "AssemblyU2DCSharp_UIFont2503090435.h"
#include "AssemblyU2DCSharp_UICamera_MouseOrTouch2057376333.h"
#include "AssemblyU2DCSharp_UICamera_DepthEntry1145614469.h"
#include "AssemblyU2DCSharp_UITextList_Paragraph3894573406.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_AttributeMap924393598.h"
#include "AssemblyU2DCSharp_ProtoBuf_Serializers_EnumSerializ276170183.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoMemberAttribute2007578278.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_ValueMember110398141.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_SubType3836516844.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_MetaType448283965.h"
#include "AssemblyU2DCSharp_BossAnimationInfo384335813.h"
#include "AssemblyU2DCSharp_AnimationPointJSC487595022.h"
#include "AssemblyU2DCSharp_BAMCameras1661014560.h"
#include "AssemblyU2DCSharp_Mihua_Utils_WaitForSeconds3217447863.h"
#include "AssemblyU2DCSharp_TweenPosition3684358292.h"
#include "AssemblyU2DCSharp_ChannelConfigCfg4272979999.h"
#include "AssemblyU2DCSharp_HeroEmbattleCfg4134124650.h"
#include "AssemblyU2DCSharp_JJCCoefficientCfg2755051730.h"
#include "AssemblyU2DCSharp_PetsCfg988016752.h"
#include "AssemblyU2DCSharp_SoundCfg1807275253.h"
#include "AssemblyU2DCSharp_StoryCfg1782371151.h"
#include "AssemblyU2DCSharp_TextStringCfg884535238.h"
#include "AssemblyU2DCSharp_UIEffectCfg952653023.h"
#include "AssemblyU2DCSharp_all_configCfg3865068708.h"
#include "AssemblyU2DCSharp_asset_sharedCfg1901313840.h"
#include "AssemblyU2DCSharp_barrierGateCfg3705088994.h"
#include "AssemblyU2DCSharp_buffCfg227963665.h"
#include "AssemblyU2DCSharp_checkpointCfg2816107964.h"
#include "AssemblyU2DCSharp_demoStoryCfg1993162290.h"
#include "AssemblyU2DCSharp_effectCfg2826279187.h"
#include "AssemblyU2DCSharp_elementCfg575911880.h"
#include "AssemblyU2DCSharp_enemy_dropCfg1026960702.h"
#include "AssemblyU2DCSharp_friendNpcCfg3890310657.h"
#include "AssemblyU2DCSharp_guideCfg2981043144.h"
#include "AssemblyU2DCSharp_herosCfg3676934635.h"
#include "AssemblyU2DCSharp_monstersCfg1542396363.h"
#include "AssemblyU2DCSharp_npcAICfg1948330203.h"
#include "AssemblyU2DCSharp_npcAIBehaviorCfg3456736809.h"
#include "AssemblyU2DCSharp_npcAIConditionCfg930197170.h"
#include "AssemblyU2DCSharp_portalCfg1117034584.h"
#include "AssemblyU2DCSharp_skillCfg2142425171.h"
#include "AssemblyU2DCSharp_skill_upgradeCfg790726486.h"
#include "AssemblyU2DCSharp_superskillCfg3024131278.h"
#include "AssemblyU2DCSharp_taixuTipCfg1268722370.h"
#include "AssemblyU2DCSharp_towerbuffCfg1755856872.h"
#include "AssemblyU2DCSharp_CameraAniMap3004656005.h"
#include "AssemblyU2DCSharp_CameraAniGroup1251365992.h"
#include "AssemblyU2DCSharp_CameraShotsCfg1740446544.h"
#include "AssemblyU2DCSharp_CurCameraShotCfg224861669.h"
#include "AssemblyU2DCSharp_CameraShotNodeCfg3577740835.h"
#include "AssemblyU2DCSharp_CameraShotHeroOrNpcCfg2707952927.h"
#include "AssemblyU2DCSharp_CameraShotStoryCfg4130953262.h"
#include "AssemblyU2DCSharp_CameraShotGameSpeedCfg308624942.h"
#include "AssemblyU2DCSharp_CameraShotActionCfg1993837871.h"
#include "AssemblyU2DCSharp_CameraShotSkillCfg260684114.h"
#include "AssemblyU2DCSharp_CameraShotMoveCfg2706860532.h"
#include "AssemblyU2DCSharp_CameraShotCircleCfg2591002709.h"
#include "AssemblyU2DCSharp_CameraShotEffectCfg326881268.h"
#include "AssemblyU2DCSharp_JSCLevelConfig1411099500.h"
#include "AssemblyU2DCSharp_JSCMapPathConfig3426118729.h"
#include "AssemblyU2DCSharp_JSCWaveNpcConfig4221504656.h"
#include "AssemblyU2DCSharp_JSCMapPathPointInfoConfig2703681049.h"
#include "AssemblyU2DCSharp_JSCLevelHeroPointConfig561690542.h"
#include "AssemblyU2DCSharp_TabData110553279.h"
#include "AssemblyU2DCSharp_ByteReadArray751193627.h"
#include "AssemblyU2DCSharp_UnitStateBase1040218558.h"
#include "AssemblyU2DCSharp_shaderBuff2982920664.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "AssemblyU2DCSharp_AIEventInfo4031722272.h"
#include "AssemblyU2DCSharp_AIEnum_EAIEventtype3896289311.h"
#include "AssemblyU2DCSharp_PlotFightTalkMgr_plotData645270205.h"
#include "AssemblyU2DCSharp_ReplayBase779703160.h"
#include "AssemblyU2DCSharp_ReplayHeroUnit605237701.h"
#include "AssemblyU2DCSharp_HitTarget3687896804.h"
#include "AssemblyU2DCSharp_Buff2081907.h"
#include "AssemblyU2DCSharp_Skill79944241.h"
#include "AssemblyU2DCSharp_DamageSeparationSkill2001134364.h"
#include "AssemblyU2DCSharp_ElementVO1745451669.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"
#include "AssemblyU2DCSharp_Blood64280026.h"
#include "AssemblyU2DCSharp_EnemyDropItem1468754474.h"
#include "AssemblyU2DCSharp_NumEffect2824221847.h"
#include "AssemblyU2DCSharp_HeroMgr_LineupPos2885729524.h"
#include "AssemblyU2DCSharp_TweenScale2936666559.h"
#include "AssemblyU2DCSharp_TargetMgr_FightPoint4216057832.h"
#include "AssemblyU2DCSharp_TargetMgr_AngleEntity3194128142.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asse2389844357.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"
#include "AssemblyU2DCSharp_FileInfoCrc1217045770.h"
#include "AssemblyU2DCSharp_Mihua_Net_UrlLoader2490729496.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr_ErrorIn2633981210.h"
#include "AssemblyU2DCSharp_CSCampFightMonsterData1577764261.h"
#include "AssemblyU2DCSharp_CSCampFightAddAttributeData790660836.h"
#include "AssemblyU2DCSharp_IEffComponent3802264961.h"
#include "AssemblyU2DCSharp_EffectCtrl3708787644.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "AssemblyU2DCSharp_UITexture3903132647.h"
#include "AssemblyU2DCSharp_TweenAlpha2920325587.h"
#include "AssemblyU2DCSharp_StaticParticle2206539572.h"
#include "AssemblyU2DCSharp_MHIAPMgr_PayInfo444430236.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr_ProductInfo1305991238.h"
#include "AssemblyU2DCSharp_SoundObj1807286664.h"
#include "AssemblyU2DCSharp_SoundTypeID3626616740.h"
#include "AssemblyU2DCSharp_ISound2170003014.h"
#include "AssemblyU2DCSharp_SoundStatus3592938945.h"
#include "AssemblyU2DCSharp_CameraShotMgr_CameraShotVO1900810094.h"
#include "AssemblyU2DCSharp_StoryMgr_StoryVO3225531714.h"
#include "AssemblyU2DCSharp_TimeUpdateVo1829512047.h"
#include "AssemblyU2DCSharp_AssetDownMgr_SetBytes2856136722.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"
#include "AssemblyU2DCSharp_UIModelDisplay1730520589.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_MsgEvent82323445.h"
#include "AssemblyU2DCSharp_InputItem3710611933.h"
#include "AssemblyU2DCSharp_PushType1840642452.h"
#include "AssemblyU2DCSharp_PluginPush_NotificationData3289515031.h"
#include "AssemblyU2DCSharp_PluginReYun_strutDict4055560227.h"
#include "AssemblyU2DCSharp_UpdateForJs1071193225.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"
#include "AssemblyU2DCSharp_MScrollView_MoveWay3474941550.h"
#include "AssemblyU2DCSharp_MScrollView_OnMoveCallBackFun3535987578.h"
#include "AssemblyU2DCSharp_T4MLodObjSC2288258003.h"
#include "AssemblyU2DCSharp_T4MBillBObjSC2448121.h"

#pragma once
// JSMgr/CSCallbackField[]
struct CSCallbackFieldU5BU5D_t1052203688  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CSCallbackField_t1235469093 * m_Items[1];

public:
	inline CSCallbackField_t1235469093 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CSCallbackField_t1235469093 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CSCallbackField_t1235469093 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// JSMgr/CSCallbackProperty[]
struct CSCallbackPropertyU5BU5D_t1496546767  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CSCallbackProperty_t2220951498 * m_Items[1];

public:
	inline CSCallbackProperty_t2220951498 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CSCallbackProperty_t2220951498 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CSCallbackProperty_t2220951498 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// JSMgr/MethodCallBackInfo[]
struct MethodCallBackInfoU5BU5D_t1108309981  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MethodCallBackInfo_t2808125524 * m_Items[1];

public:
	inline MethodCallBackInfo_t2808125524 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MethodCallBackInfo_t2808125524 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MethodCallBackInfo_t2808125524 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// JSMgr/CallbackInfo[]
struct CallbackInfoU5BU5D_t229224226  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CallbackInfo_t3768479923 * m_Items[1];

public:
	inline CallbackInfo_t3768479923 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CallbackInfo_t3768479923 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CallbackInfo_t3768479923 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// EventDelegate[]
struct EventDelegateU5BU5D_t1029252742  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EventDelegate_t4004424223 * m_Items[1];

public:
	inline EventDelegate_t4004424223 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EventDelegate_t4004424223 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EventDelegate_t4004424223 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ACData[]
struct ACDataU5BU5D_t1322332133  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ACData_t1924893420 * m_Items[1];

public:
	inline ACData_t1924893420 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ACData_t1924893420 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ACData_t1924893420 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// BMGlyph[]
struct BMGlyphU5BU5D_t3436589244  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BMGlyph_t719052705 * m_Items[1];

public:
	inline BMGlyph_t719052705 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BMGlyph_t719052705 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BMGlyph_t719052705 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TypeFlag[]
struct TypeFlagU5BU5D_t693965763  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// BuffState[]
struct BuffStateU5BU5D_t2344287979  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BuffState_t2048909278 * m_Items[1];

public:
	inline BuffState_t2048909278 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BuffState_t2048909278 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BuffState_t2048909278 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CSHeroUnit[]
struct CSHeroUnitU5BU5D_t1342235227  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CSHeroUnit_t3764358446 * m_Items[1];

public:
	inline CSHeroUnit_t3764358446 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CSHeroUnit_t3764358446 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CSHeroUnit_t3764358446 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CSPlusAtt[]
struct CSPlusAttU5BU5D_t3275946414  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CSPlusAtt_t3268315159 * m_Items[1];

public:
	inline CSPlusAtt_t3268315159 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CSPlusAtt_t3268315159 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CSPlusAtt_t3268315159 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CSGuideVO[]
struct CSGuideVOU5BU5D_t2582593960  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CSGuideVO_t4116883493 * m_Items[1];

public:
	inline CSGuideVO_t4116883493 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CSGuideVO_t4116883493 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CSGuideVO_t4116883493 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CSSkillData[]
struct CSSkillDataU5BU5D_t1071816810  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CSSkillData_t432288523 * m_Items[1];

public:
	inline CSSkillData_t432288523 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CSSkillData_t432288523 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CSSkillData_t432288523 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CSAttrBuffData[]
struct CSAttrBuffDataU5BU5D_t2697436427  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CSAttrBuffData_t1041646654 * m_Items[1];

public:
	inline CSAttrBuffData_t1041646654 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CSAttrBuffData_t1041646654 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CSAttrBuffData_t1041646654 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CSPlayerData/FightingPos[]
struct FightingPosU5BU5D_t3645161435  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FightingPos_t1575607726 * m_Items[1];

public:
	inline FightingPos_t1575607726 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FightingPos_t1575607726 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FightingPos_t1575607726 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CSProvingGroundsHeroData[]
struct CSProvingGroundsHeroDataU5BU5D_t1350178142  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CSProvingGroundsHeroData_t1054716455 * m_Items[1];

public:
	inline CSProvingGroundsHeroData_t1054716455 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CSProvingGroundsHeroData_t1054716455 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CSProvingGroundsHeroData_t1054716455 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CSPVPRobotNPC[]
struct CSPVPRobotNPCU5BU5D_t1447782092  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CSPVPRobotNPC_t3152738001 * m_Items[1];

public:
	inline CSPVPRobotNPC_t3152738001 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CSPVPRobotNPC_t3152738001 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CSPVPRobotNPC_t3152738001 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// cameraShotCfg[]
struct cameraShotCfgU5BU5D_t3196548520  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) cameraShotCfg_t781157413 * m_Items[1];

public:
	inline cameraShotCfg_t781157413 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline cameraShotCfg_t781157413 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, cameraShotCfg_t781157413 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CsCfgBase[]
struct CsCfgBaseU5BU5D_t2851563816  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CsCfgBase_t69924517 * m_Items[1];

public:
	inline CsCfgBase_t69924517 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CsCfgBase_t69924517 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CsCfgBase_t69924517 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// EnemyDropData[]
struct EnemyDropDataU5BU5D_t596029724  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EnemyDropData_t1468587713 * m_Items[1];

public:
	inline EnemyDropData_t1468587713 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EnemyDropData_t1468587713 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EnemyDropData_t1468587713 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// AIObject[]
struct AIObjectU5BU5D_t304055422  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AIObject_t37280135 * m_Items[1];

public:
	inline AIObject_t37280135 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AIObject_t37280135 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AIObject_t37280135 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// EventDelegate/Parameter[]
struct ParameterU5BU5D_t206664164  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Parameter_t2566940569 * m_Items[1];

public:
	inline Parameter_t2566940569 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Parameter_t2566940569 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Parameter_t2566940569 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CombatEntity[]
struct CombatEntityU5BU5D_t3566310830  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CombatEntity_t684137495 * m_Items[1];

public:
	inline CombatEntity_t684137495 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CombatEntity_t684137495 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CombatEntity_t684137495 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CEvent.EventFunc`1<System.Object>[]
struct EventFunc_1U5BU5D_t839438808  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EventFunc_1_t1647712181 * m_Items[1];

public:
	inline EventFunc_1_t1647712181 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EventFunc_1_t1647712181 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EventFunc_1_t1647712181 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CEvent.EventFunc`1<CEvent.ZEvent>[]
struct EventFunc_1U5BU5D_t2727140131  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EventFunc_1_t1114914310 * m_Items[1];

public:
	inline EventFunc_1_t1114914310 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EventFunc_1_t1114914310 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EventFunc_1_t1114914310 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Hero[]
struct HeroU5BU5D_t634172991  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Hero_t2245658 * m_Items[1];

public:
	inline Hero_t2245658 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Hero_t2245658 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Hero_t2245658 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FileInfoRes[]
struct FileInfoResU5BU5D_t82017299  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FileInfoRes_t1217059798 * m_Items[1];

public:
	inline FileInfoRes_t1217059798 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FileInfoRes_t1217059798 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FileInfoRes_t1217059798 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIWidget[]
struct UIWidgetU5BU5D_t4236988201  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIWidget_t769069560 * m_Items[1];

public:
	inline UIWidget_t769069560 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIWidget_t769069560 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIWidget_t769069560 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIRect[]
struct UIRectU5BU5D_t1524091913  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIRect_t2503437976 * m_Items[1];

public:
	inline UIRect_t2503437976 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIRect_t2503437976 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIRect_t2503437976 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// JSCLevelMonsterConfig[]
struct JSCLevelMonsterConfigU5BU5D_t1985514023  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JSCLevelMonsterConfig_t1924079698 * m_Items[1];

public:
	inline JSCLevelMonsterConfig_t1924079698 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JSCLevelMonsterConfig_t1924079698 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JSCLevelMonsterConfig_t1924079698 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FightEnum.ESkillBuffEvent[]
struct ESkillBuffEventU5BU5D_t2768361140  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// FightEnum.ESkillBuffTarget[]
struct ESkillBuffTargetU5BU5D_t2691263849  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// TeamSkill[]
struct TeamSkillU5BU5D_t2402971741  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TeamSkill_t1816898004 * m_Items[1];

public:
	inline TeamSkill_t1816898004 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TeamSkill_t1816898004 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TeamSkill_t1816898004 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UISpriteData[]
struct UISpriteDataU5BU5D_t2292174802  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UISpriteData_t3578345923 * m_Items[1];

public:
	inline UISpriteData_t3578345923 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UISpriteData_t3578345923 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UISpriteData_t3578345923 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UICamera[]
struct UICameraU5BU5D_t3682822564  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UICamera_t189364953 * m_Items[1];

public:
	inline UICamera_t189364953 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UICamera_t189364953 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UICamera_t189364953 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIDrawCall[]
struct UIDrawCallU5BU5D_t3158789363  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIDrawCall_t913273974 * m_Items[1];

public:
	inline UIDrawCall_t913273974 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIDrawCall_t913273974 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIDrawCall_t913273974 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// BMSymbol[]
struct BMSymbolU5BU5D_t484807634  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BMSymbol_t1170982339 * m_Items[1];

public:
	inline BMSymbol_t1170982339 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BMSymbol_t1170982339 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BMSymbol_t1170982339 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIKeyNavigation[]
struct UIKeyNavigationU5BU5D_t1243790086  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIKeyNavigation_t1837256607 * m_Items[1];

public:
	inline UIKeyNavigation_t1837256607 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIKeyNavigation_t1837256607 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIKeyNavigation_t1837256607 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIPanel[]
struct UIPanelU5BU5D_t3742972657  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIPanel_t295209936 * m_Items[1];

public:
	inline UIPanel_t295209936 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIPanel_t295209936 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIPanel_t295209936 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIRoot[]
struct UIRootU5BU5D_t1337058131  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIRoot_t2503447958 * m_Items[1];

public:
	inline UIRoot_t2503447958 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIRoot_t2503447958 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIRoot_t2503447958 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIScrollView[]
struct UIScrollViewU5BU5D_t4274241891  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIScrollView_t2113479878 * m_Items[1];

public:
	inline UIScrollView_t2113479878 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIScrollView_t2113479878 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIScrollView_t2113479878 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIToggle[]
struct UIToggleU5BU5D_t2046261209  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIToggle_t688812808 * m_Items[1];

public:
	inline UIToggle_t688812808 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIToggle_t688812808 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIToggle_t688812808 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// VersionInfo[]
struct VersionInfoU5BU5D_t1155590563  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) VersionInfo_t2356638086 * m_Items[1];

public:
	inline VersionInfo_t2356638086 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline VersionInfo_t2356638086 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, VersionInfo_t2356638086 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// JSComponent[]
struct JSComponentU5BU5D_t1575483645  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JSComponent_t1642894772 * m_Items[1];

public:
	inline JSComponent_t1642894772 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JSComponent_t1642894772 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JSComponent_t1642894772 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ICsObj[]
struct ICsObjU5BU5D_t3993215019  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// JSSerializer[]
struct JSSerializerU5BU5D_t2032809722  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JSSerializer_t3534558139 * m_Items[1];

public:
	inline JSSerializer_t3534558139 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JSSerializer_t3534558139 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JSSerializer_t3534558139 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GenericTypeCache/TypeMembers[]
struct TypeMembersU5BU5D_t3075165212  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TypeMembers_t2369396673 * m_Items[1];

public:
	inline TypeMembers_t2369396673 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TypeMembers_t2369396673 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TypeMembers_t2369396673 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// JSCache/TypeInfo[]
struct TypeInfoU5BU5D_t469650059  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TypeInfo_t3198813374 * m_Items[1];

public:
	inline TypeInfo_t3198813374 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TypeInfo_t3198813374 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TypeInfo_t3198813374 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// JSDebugMessages/Message[]
struct MessageU5BU5D_t4063354939  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Message_t903789774 * m_Items[1];

public:
	inline Message_t903789774 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Message_t903789774 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Message_t903789774 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// JSMgr/JS_CS_Rel[]
struct JS_CS_RelU5BU5D_t4022520481  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JS_CS_Rel_t3554103776 * m_Items[1];

public:
	inline JS_CS_Rel_t3554103776 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JS_CS_Rel_t3554103776 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JS_CS_Rel_t3554103776 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// JSSerializer/SerializeStruct[]
struct SerializeStructU5BU5D_t3132697852  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SerializeStruct_t2767317089 * m_Items[1];

public:
	inline SerializeStruct_t2767317089 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SerializeStruct_t2767317089 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SerializeStruct_t2767317089 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Scripts.JSBindingClasses.JsRepresentClass[]
struct JsRepresentClassU5BU5D_t4027028670  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JsRepresentClass_t2913492551 * m_Items[1];

public:
	inline JsRepresentClass_t2913492551 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsRepresentClass_t2913492551 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsRepresentClass_t2913492551 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SevenZip.CommandLineParser.SwitchResult[]
struct SwitchResultU5BU5D_t2817561950  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SwitchResult_t80294439 * m_Items[1];

public:
	inline SwitchResult_t80294439 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SwitchResult_t80294439 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SwitchResult_t80294439 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SevenZip.CommandLineParser.SwitchForm[]
struct SwitchFormU5BU5D_t778498235  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SwitchForm_t300389966 * m_Items[1];

public:
	inline SwitchForm_t300389966 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SwitchForm_t300389966 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SwitchForm_t300389966 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SevenZip.CommandLineParser.CommandForm[]
struct CommandFormU5BU5D_t1880299148  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CommandForm_t3963007505 * m_Items[1];

public:
	inline CommandForm_t3963007505 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CommandForm_t3963007505 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CommandForm_t3963007505 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SevenZip.CommandLineParser.CommandSubCharsSet[]
struct CommandSubCharsSetU5BU5D_t4194914353  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CommandSubCharsSet_t4216237968 * m_Items[1];

public:
	inline CommandSubCharsSet_t4216237968 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CommandSubCharsSet_t4216237968 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CommandSubCharsSet_t4216237968 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SevenZip.Compression.RangeCoder.BitDecoder[]
struct BitDecoderU5BU5D_t1049749620  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BitDecoder_t4202293321  m_Items[1];

public:
	inline BitDecoder_t4202293321  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BitDecoder_t4202293321 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BitDecoder_t4202293321  value)
	{
		m_Items[index] = value;
	}
};
// SevenZip.Compression.RangeCoder.BitTreeDecoder[]
struct BitTreeDecoderU5BU5D_t3968913194  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BitTreeDecoder_t2730629963  m_Items[1];

public:
	inline BitTreeDecoder_t2730629963  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BitTreeDecoder_t2730629963 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BitTreeDecoder_t2730629963  value)
	{
		m_Items[index] = value;
	}
};
// SevenZip.Compression.LZMA.Decoder/LiteralDecoder/Decoder2[]
struct Decoder2U5BU5D_t1694590850  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Decoder2_t813340627  m_Items[1];

public:
	inline Decoder2_t813340627  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Decoder2_t813340627 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Decoder2_t813340627  value)
	{
		m_Items[index] = value;
	}
};
// SevenZip.Compression.LZMA.Encoder/Optimal[]
struct OptimalU5BU5D_t591851880  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Optimal_t1633575781 * m_Items[1];

public:
	inline Optimal_t1633575781 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Optimal_t1633575781 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Optimal_t1633575781 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SevenZip.Compression.RangeCoder.BitEncoder[]
struct BitEncoderU5BU5D_t2970556732  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BitEncoder_t1052492065  m_Items[1];

public:
	inline BitEncoder_t1052492065  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BitEncoder_t1052492065 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BitEncoder_t1052492065  value)
	{
		m_Items[index] = value;
	}
};
// SevenZip.Compression.RangeCoder.BitTreeEncoder[]
struct BitTreeEncoderU5BU5D_t1594753010  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BitTreeEncoder_t3875796003  m_Items[1];

public:
	inline BitTreeEncoder_t3875796003  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BitTreeEncoder_t3875796003 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BitTreeEncoder_t3875796003  value)
	{
		m_Items[index] = value;
	}
};
// SevenZip.CoderPropID[]
struct CoderPropIDU5BU5D_t3009125352  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// SevenZip.Compression.LZMA.Encoder/LiteralEncoder/Encoder2[]
struct Encoder2U5BU5D_t1250744506  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Encoder2_t2578924795  m_Items[1];

public:
	inline Encoder2_t2578924795  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Encoder2_t2578924795 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Encoder2_t2578924795  value)
	{
		m_Items[index] = value;
	}
};
// Pathfinding.RichPathPart[]
struct RichPathPartU5BU5D_t3149074927  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RichPathPart_t2520985130 * m_Items[1];

public:
	inline RichPathPart_t2520985130 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline RichPathPart_t2520985130 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, RichPathPart_t2520985130 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.GraphNode[]
struct GraphNodeU5BU5D_t927449255  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GraphNode_t23612370 * m_Items[1];

public:
	inline GraphNode_t23612370 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GraphNode_t23612370 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GraphNode_t23612370 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.RichFunnel[]
struct RichFunnelU5BU5D_t1110054585  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RichFunnel_t2453525928 * m_Items[1];

public:
	inline RichFunnel_t2453525928 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline RichFunnel_t2453525928 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, RichFunnel_t2453525928 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.RichSpecial[]
struct RichSpecialU5BU5D_t2719587974  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RichSpecial_t2303562271 * m_Items[1];

public:
	inline RichSpecial_t2303562271 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline RichSpecial_t2303562271 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, RichSpecial_t2303562271 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.TriangleMeshNode[]
struct TriangleMeshNodeU5BU5D_t2064970368  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TriangleMeshNode_t1626248749 * m_Items[1];

public:
	inline TriangleMeshNode_t1626248749 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TriangleMeshNode_t1626248749 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TriangleMeshNode_t1626248749 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.IPathModifier[]
struct IPathModifierU5BU5D_t3733767294  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// OnPathDelegate[]
struct OnPathDelegateU5BU5D_t1505600468  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) OnPathDelegate_t598607977 * m_Items[1];

public:
	inline OnPathDelegate_t598607977 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline OnPathDelegate_t598607977 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, OnPathDelegate_t598607977 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.NavGraph[]
struct NavGraphU5BU5D_t850130684  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NavGraph_t1254319713 * m_Items[1];

public:
	inline NavGraph_t1254319713 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline NavGraph_t1254319713 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, NavGraph_t1254319713 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.UserConnection[]
struct UserConnectionU5BU5D_t328197926  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UserConnection_t885308927 * m_Items[1];

public:
	inline UserConnection_t885308927 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UserConnection_t885308927 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UserConnection_t885308927 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.PathThreadInfo[]
struct PathThreadInfoU5BU5D_t3711851138  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PathThreadInfo_t2420662483  m_Items[1];

public:
	inline PathThreadInfo_t2420662483  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PathThreadInfo_t2420662483 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PathThreadInfo_t2420662483  value)
	{
		m_Items[index] = value;
	}
};
// Pathfinding.GraphUpdateObject[]
struct GraphUpdateObjectU5BU5D_t2575105257  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GraphUpdateObject_t430843704 * m_Items[1];

public:
	inline GraphUpdateObject_t430843704 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GraphUpdateObject_t430843704 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GraphUpdateObject_t430843704 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// AstarPath/GUOSingle[]
struct GUOSingleU5BU5D_t1723385383  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GUOSingle_t3657339986  m_Items[1];

public:
	inline GUOSingle_t3657339986  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GUOSingle_t3657339986 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GUOSingle_t3657339986  value)
	{
		m_Items[index] = value;
	}
};
// AstarPath/AstarWorkItem[]
struct AstarWorkItemU5BU5D_t3890760705  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AstarWorkItem_t2566693888  m_Items[1];

public:
	inline AstarWorkItem_t2566693888  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AstarWorkItem_t2566693888 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AstarWorkItem_t2566693888  value)
	{
		m_Items[index] = value;
	}
};
// Pathfinding.Int3[]
struct Int3U5BU5D_t516284607  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Int3_t1974045594  m_Items[1];

public:
	inline Int3_t1974045594  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Int3_t1974045594 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Int3_t1974045594  value)
	{
		m_Items[index] = value;
	}
};
// Pathfinding.PathNode[][]
struct PathNodeU5BU5DU5BU5D_t573886225  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PathNodeU5BU5D_t1913565744* m_Items[1];

public:
	inline PathNodeU5BU5D_t1913565744* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PathNodeU5BU5D_t1913565744** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PathNodeU5BU5D_t1913565744* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.PathNode[]
struct PathNodeU5BU5D_t1913565744  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PathNode_t417131581 * m_Items[1];

public:
	inline PathNode_t417131581 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PathNode_t417131581 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PathNode_t417131581 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.AnimationLink/LinkClip[]
struct LinkClipU5BU5D_t3741422280  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LinkClip_t2650609029 * m_Items[1];

public:
	inline LinkClip_t2650609029 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LinkClip_t2650609029 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LinkClip_t2650609029 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// AstarDebugger/GraphPoint[]
struct GraphPointU5BU5D_t2709902928  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GraphPoint_t991085213  m_Items[1];

public:
	inline GraphPoint_t991085213  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GraphPoint_t991085213 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GraphPoint_t991085213  value)
	{
		m_Items[index] = value;
	}
};
// AstarDebugger/PathTypeDebug[]
struct PathTypeDebugU5BU5D_t1567511300  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PathTypeDebug_t4028073209  m_Items[1];

public:
	inline PathTypeDebug_t4028073209  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PathTypeDebug_t4028073209 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PathTypeDebug_t4028073209  value)
	{
		m_Items[index] = value;
	}
};
// Pathfinding.Path[]
struct PathU5BU5D_t789302682  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Path_t1974241691 * m_Items[1];

public:
	inline Path_t1974241691 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Path_t1974241691 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Path_t1974241691 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.ABPath[]
struct ABPathU5BU5D_t1778787029  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ABPath_t1187561148 * m_Items[1];

public:
	inline ABPath_t1187561148 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ABPath_t1187561148 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ABPath_t1187561148 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.BinaryHeapM/Tuple[]
struct TupleU5BU5D_t3169005718  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Tuple_t2515696975  m_Items[1];

public:
	inline Tuple_t2515696975  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Tuple_t2515696975 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Tuple_t2515696975  value)
	{
		m_Items[index] = value;
	}
};
// Pathfinding.GraphModifier[]
struct GraphModifierU5BU5D_t3637429982  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GraphModifier_t2555428519 * m_Items[1];

public:
	inline GraphModifier_t2555428519 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GraphModifier_t2555428519 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GraphModifier_t2555428519 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.NodeLink2[]
struct NodeLink2U5BU5D_t2773892905  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NodeLink2_t1645404664 * m_Items[1];

public:
	inline NodeLink2_t1645404664 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline NodeLink2_t1645404664 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, NodeLink2_t1645404664 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.NodeLink3[]
struct NodeLink3U5BU5D_t3260080644  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NodeLink3_t1645404665 * m_Items[1];

public:
	inline NodeLink3_t1645404665 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline NodeLink3_t1645404665 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, NodeLink3_t1645404665 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.PointNode[]
struct PointNodeU5BU5D_t1844325917  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PointNode_t2761813780 * m_Items[1];

public:
	inline PointNode_t2761813780 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PointNode_t2761813780 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PointNode_t2761813780 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.RVO.ObstacleVertex[]
struct ObstacleVertexU5BU5D_t4171054874  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ObstacleVertex_t4170307099 * m_Items[1];

public:
	inline ObstacleVertex_t4170307099 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ObstacleVertex_t4170307099 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ObstacleVertex_t4170307099 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.RVO.Sampled.Agent[]
struct AgentU5BU5D_t3021891762  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Agent_t1290054243 * m_Items[1];

public:
	inline Agent_t1290054243 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Agent_t1290054243 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Agent_t1290054243 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.RVO.Sampled.Agent/VO[]
struct VOU5BU5D_t2243217837  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) VO_t2757914308  m_Items[1];

public:
	inline VO_t2757914308  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline VO_t2757914308 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, VO_t2757914308  value)
	{
		m_Items[index] = value;
	}
};
// Pathfinding.RVO.Simulator/Worker[]
struct WorkerU5BU5D_t2291287347  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Worker_t3927454006 * m_Items[1];

public:
	inline Worker_t3927454006 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Worker_t3927454006 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Worker_t3927454006 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.RVO.RVOQuadtree/Node[]
struct NodeU5BU5D_t3209507899  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Node_t2108618958  m_Items[1];

public:
	inline Node_t2108618958  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Node_t2108618958 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Node_t2108618958  value)
	{
		m_Items[index] = value;
	}
};
// Pathfinding.UnityReferenceHelper[]
struct UnityReferenceHelperU5BU5D_t2550037055  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UnityReferenceHelper_t2955464730 * m_Items[1];

public:
	inline UnityReferenceHelper_t2955464730 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UnityReferenceHelper_t2955464730 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UnityReferenceHelper_t2955464730 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.GraphEditorBase[]
struct GraphEditorBaseU5BU5D_t3067496667  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GraphEditorBase_t3362408622 * m_Items[1];

public:
	inline GraphEditorBase_t3362408622 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GraphEditorBase_t3362408622 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GraphEditorBase_t3362408622 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// RVOExampleAgent[]
struct RVOExampleAgentU5BU5D_t3208204227  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RVOExampleAgent_t1174908390 * m_Items[1];

public:
	inline RVOExampleAgent_t1174908390 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline RVOExampleAgent_t1174908390 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, RVOExampleAgent_t1174908390 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.RVO.IAgent[]
struct IAgentU5BU5D_t1549531341  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.GridNode[]
struct GridNodeU5BU5D_t2925197291  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GridNode_t3795753694 * m_Items[1];

public:
	inline GridNode_t3795753694 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GridNode_t3795753694 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GridNode_t3795753694 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ProceduralWorld/ProceduralPrefab[]
struct ProceduralPrefabU5BU5D_t2260100058  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ProceduralPrefab_t2162393179 * m_Items[1];

public:
	inline ProceduralPrefab_t2162393179 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ProceduralPrefab_t2162393179 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ProceduralPrefab_t2162393179 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.Int2[]
struct Int2U5BU5D_t30096868  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Int2_t1974045593  m_Items[1];

public:
	inline Int2_t1974045593  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Int2_t1974045593 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Int2_t1974045593  value)
	{
		m_Items[index] = value;
	}
};
// ProceduralWorld/ProceduralTile[]
struct ProceduralTileU5BU5D_t1428483592  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ProceduralTile_t3586714437 * m_Items[1];

public:
	inline ProceduralTile_t3586714437 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ProceduralTile_t3586714437 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ProceduralTile_t3586714437 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.RichAI[]
struct RichAIU5BU5D_t3615124959  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RichAI_t1710845178 * m_Items[1];

public:
	inline RichAI_t1710845178 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline RichAI_t1710845178 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, RichAI_t1710845178 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// AIPath[]
struct AIPathU5BU5D_t616438592  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AIPath_t1930792045 * m_Items[1];

public:
	inline AIPath_t1930792045 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AIPath_t1930792045 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AIPath_t1930792045 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.GridGraph/TextureData/ChannelUse[]
struct ChannelUseU5BU5D_t2916588678  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// Pathfinding.LevelGridNode[]
struct LevelGridNodeU5BU5D_t498907163  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LevelGridNode_t489265774 * m_Items[1];

public:
	inline LevelGridNode_t489265774 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LevelGridNode_t489265774 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LevelGridNode_t489265774 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.LinkedLevelCell[]
struct LinkedLevelCellU5BU5D_t1651191830  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LinkedLevelCell_t935711695 * m_Items[1];

public:
	inline LinkedLevelCell_t935711695 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LinkedLevelCell_t935711695 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LinkedLevelCell_t935711695 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.LayerGridGraph[]
struct LayerGridGraphU5BU5D_t628435936  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LayerGridGraph_t3415576653 * m_Items[1];

public:
	inline LayerGridGraph_t3415576653 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LayerGridGraph_t3415576653 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LayerGridGraph_t3415576653 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.IUpdatableGraph[]
struct IUpdatableGraphU5BU5D_t2324509682  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.IRaycastableGraph[]
struct IRaycastableGraphU5BU5D_t3707294899  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.GridGraph[]
struct GridGraphU5BU5D_t3322779535  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GridGraph_t2455707914 * m_Items[1];

public:
	inline GridGraph_t2455707914 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GridGraph_t2455707914 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GridGraph_t2455707914 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.MeshNode[]
struct MeshNodeU5BU5D_t2521861192  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MeshNode_t3005053445 * m_Items[1];

public:
	inline MeshNode_t3005053445 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MeshNode_t3005053445 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MeshNode_t3005053445 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.INavmeshHolder[]
struct INavmeshHolderU5BU5D_t3899225556  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.QuadtreeNode[]
struct QuadtreeNodeU5BU5D_t3867982064  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) QuadtreeNode_t1423301757 * m_Items[1];

public:
	inline QuadtreeNode_t1423301757 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline QuadtreeNode_t1423301757 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, QuadtreeNode_t1423301757 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.RecastGraph/NavmeshTile[]
struct NavmeshTileU5BU5D_t3225070876  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NavmeshTile_t4022309569 * m_Items[1];

public:
	inline NavmeshTile_t4022309569 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline NavmeshTile_t4022309569 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, NavmeshTile_t4022309569 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.INavmesh[]
struct INavmeshU5BU5D_t2562867344  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.RecastGraph/CapsuleCache[]
struct CapsuleCacheU5BU5D_t2561646375  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CapsuleCache_t2032660306 * m_Items[1];

public:
	inline CapsuleCache_t2032660306 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CapsuleCache_t2032660306 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CapsuleCache_t2032660306 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.Voxels.ExtraMesh[]
struct ExtraMeshU5BU5D_t2875893186  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ExtraMesh_t4218029715  m_Items[1];

public:
	inline ExtraMesh_t4218029715  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ExtraMesh_t4218029715 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ExtraMesh_t4218029715  value)
	{
		m_Items[index] = value;
	}
};
// Pathfinding.RecastMeshObj[]
struct RecastMeshObjU5BU5D_t3140958079  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RecastMeshObj_t2069195738 * m_Items[1];

public:
	inline RecastMeshObj_t2069195738 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline RecastMeshObj_t2069195738 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, RecastMeshObj_t2069195738 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.BBTree/BBTreeBox[]
struct BBTreeBoxU5BU5D_t3508094851  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BBTreeBox_t1958396198  m_Items[1];

public:
	inline BBTreeBox_t1958396198  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BBTreeBox_t1958396198 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BBTreeBox_t1958396198  value)
	{
		m_Items[index] = value;
	}
};
// Pathfinding.RecastBBTreeBox[]
struct RecastBBTreeBoxU5BU5D_t890488528  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RecastBBTreeBox_t3100392477 * m_Items[1];

public:
	inline RecastBBTreeBox_t3100392477 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline RecastBBTreeBox_t3100392477 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, RecastBBTreeBox_t3100392477 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.Util.TileHandler/TileType[]
struct TileTypeU5BU5D_t2868017328  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TileType_t2364590013 * m_Items[1];

public:
	inline TileType_t2364590013 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TileType_t2364590013 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TileType_t2364590013 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.NavmeshCut[]
struct NavmeshCutU5BU5D_t1823579225  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NavmeshCut_t300992648 * m_Items[1];

public:
	inline NavmeshCut_t300992648 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline NavmeshCut_t300992648 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, NavmeshCut_t300992648 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.IntRect[]
struct IntRectU5BU5D_t3425567672  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) IntRect_t3015058261  m_Items[1];

public:
	inline IntRect_t3015058261  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline IntRect_t3015058261 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, IntRect_t3015058261  value)
	{
		m_Items[index] = value;
	}
};
// Pathfinding.NavmeshAdd[]
struct NavmeshAddU5BU5D_t1666678174  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NavmeshAdd_t300990183 * m_Items[1];

public:
	inline NavmeshAdd_t300990183 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline NavmeshAdd_t300990183 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, NavmeshAdd_t300990183 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.Voxels.CompactVoxelSpan[]
struct CompactVoxelSpanU5BU5D_t1459321228  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CompactVoxelSpan_t3431582481  m_Items[1];

public:
	inline CompactVoxelSpan_t3431582481  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CompactVoxelSpan_t3431582481 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CompactVoxelSpan_t3431582481  value)
	{
		m_Items[index] = value;
	}
};
// Pathfinding.Voxels.CompactVoxelCell[]
struct CompactVoxelCellU5BU5D_t3900941332  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CompactVoxelCell_t3431095593  m_Items[1];

public:
	inline CompactVoxelCell_t3431095593  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CompactVoxelCell_t3431095593 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CompactVoxelCell_t3431095593  value)
	{
		m_Items[index] = value;
	}
};
// Pathfinding.Voxels.LinkedVoxelSpan[]
struct LinkedVoxelSpanU5BU5D_t2863720544  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LinkedVoxelSpan_t3201576397  m_Items[1];

public:
	inline LinkedVoxelSpan_t3201576397  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LinkedVoxelSpan_t3201576397 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LinkedVoxelSpan_t3201576397  value)
	{
		m_Items[index] = value;
	}
};
// Pathfinding.Voxels.VoxelContour[]
struct VoxelContourU5BU5D_t3554406569  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) VoxelContour_t3597201016  m_Items[1];

public:
	inline VoxelContour_t3597201016  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline VoxelContour_t3597201016 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, VoxelContour_t3597201016  value)
	{
		m_Items[index] = value;
	}
};
// Pathfinding.LocalAvoidance[]
struct LocalAvoidanceU5BU5D_t454871254  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LocalAvoidance_t2375621135 * m_Items[1];

public:
	inline LocalAvoidance_t2375621135 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LocalAvoidance_t2375621135 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LocalAvoidance_t2375621135 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.LocalAvoidance/VO[]
struct VOU5BU5D_t4192337096  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) VO_t2172220293 * m_Items[1];

public:
	inline VO_t2172220293 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline VO_t2172220293 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, VO_t2172220293 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.LocalAvoidance/VOLine[]
struct VOLineU5BU5D_t1958725220  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) VOLine_t2029931801  m_Items[1];

public:
	inline VOLine_t2029931801  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline VOLine_t2029931801 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, VOLine_t2029931801  value)
	{
		m_Items[index] = value;
	}
};
// Pathfinding.LocalAvoidance/IntersectionPair[]
struct IntersectionPairU5BU5D_t3912429174  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) IntersectionPair_t2821319919  m_Items[1];

public:
	inline IntersectionPair_t2821319919  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline IntersectionPair_t2821319919 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, IntersectionPair_t2821319919  value)
	{
		m_Items[index] = value;
	}
};
// Pathfinding.AdvancedSmooth/Turn[]
struct TurnU5BU5D_t2705444999  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Turn_t465195378  m_Items[1];

public:
	inline Turn_t465195378  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Turn_t465195378 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Turn_t465195378  value)
	{
		m_Items[index] = value;
	}
};
// Pathfinding.RelevantGraphSurface[]
struct RelevantGraphSurfaceU5BU5D_t2419501767  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RelevantGraphSurface_t4201206834 * m_Items[1];

public:
	inline RelevantGraphSurface_t4201206834 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline RelevantGraphSurface_t4201206834 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, RelevantGraphSurface_t4201206834 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.ConstantPath[]
struct ConstantPathU5BU5D_t1012962886  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ConstantPath_t275096927 * m_Items[1];

public:
	inline ConstantPath_t275096927 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ConstantPath_t275096927 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ConstantPath_t275096927 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.FleePath[]
struct FleePathU5BU5D_t1773109948  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FleePath_t909342113 * m_Items[1];

public:
	inline FleePath_t909342113 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FleePath_t909342113 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FleePath_t909342113 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.FloodPath[]
struct FloodPathU5BU5D_t1075297064  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FloodPath_t3766979749 * m_Items[1];

public:
	inline FloodPath_t3766979749 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FloodPath_t3766979749 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FloodPath_t3766979749 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.FloodPathTracer[]
struct FloodPathTracerU5BU5D_t3803269799  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FloodPathTracer_t1668942290 * m_Items[1];

public:
	inline FloodPathTracer_t1668942290 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FloodPathTracer_t1668942290 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FloodPathTracer_t1668942290 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.MultiTargetPath[]
struct MultiTargetPathU5BU5D_t2931712332  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MultiTargetPath_t4131434065 * m_Items[1];

public:
	inline MultiTargetPath_t4131434065 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MultiTargetPath_t4131434065 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MultiTargetPath_t4131434065 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.RandomPath[]
struct RandomPathU5BU5D_t4050551499  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RandomPath_t3634790782 * m_Items[1];

public:
	inline RandomPath_t3634790782 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline RandomPath_t3634790782 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, RandomPath_t3634790782 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.AstarProfiler/ProfilePoint[]
struct ProfilePointU5BU5D_t2544953549  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ProfilePoint_t940090916 * m_Items[1];

public:
	inline ProfilePoint_t940090916 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ProfilePoint_t940090916 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ProfilePoint_t940090916 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Core.RpsResult[]
struct RpsResultU5BU5D_t1451851929  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// Core.RpsChoice[]
struct RpsChoiceU5BU5D_t3806589061  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// FS_ShadowManager[]
struct FS_ShadowManagerU5BU5D_t3504452058  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FS_ShadowManager_t363579995 * m_Items[1];

public:
	inline FS_ShadowManager_t363579995 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FS_ShadowManager_t363579995 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FS_ShadowManager_t363579995 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FS_ShadowSimple[]
struct FS_ShadowSimpleU5BU5D_t1368593709  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FS_ShadowSimple_t4208748868 * m_Items[1];

public:
	inline FS_ShadowSimple_t4208748868 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FS_ShadowSimple_t4208748868 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FS_ShadowSimple_t4208748868 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GestureRecognizer[]
struct GestureRecognizerU5BU5D_t2076083968  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GestureRecognizer_t3512875949 * m_Items[1];

public:
	inline GestureRecognizer_t3512875949 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GestureRecognizer_t3512875949 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GestureRecognizer_t3512875949 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FingerClusterManager/Cluster[]
struct ClusterU5BU5D_t2777603326  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Cluster_t3870231303 * m_Items[1];

public:
	inline Cluster_t3870231303 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Cluster_t3870231303 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Cluster_t3870231303 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FingerDownEvent[]
struct FingerDownEventU5BU5D_t3336836950  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FingerDownEvent_t356126095 * m_Items[1];

public:
	inline FingerDownEvent_t356126095 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FingerDownEvent_t356126095 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FingerDownEvent_t356126095 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FingerHoverEvent[]
struct FingerHoverEventU5BU5D_t1291636830  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FingerHoverEvent_t1767068455 * m_Items[1];

public:
	inline FingerHoverEvent_t1767068455 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FingerHoverEvent_t1767068455 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FingerHoverEvent_t1767068455 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FingerMotionEvent[]
struct FingerMotionEventU5BU5D_t3661199226  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FingerMotionEvent_t3307437371 * m_Items[1];

public:
	inline FingerMotionEvent_t3307437371 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FingerMotionEvent_t3307437371 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FingerMotionEvent_t3307437371 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FingerUpEvent[]
struct FingerUpEventU5BU5D_t2677094003  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FingerUpEvent_t3093014774 * m_Items[1];

public:
	inline FingerUpEvent_t3093014774 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FingerUpEvent_t3093014774 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FingerUpEvent_t3093014774 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FingerGestures/Finger[]
struct FingerU5BU5D_t906829736  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Finger_t182428197 * m_Items[1];

public:
	inline Finger_t182428197 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Finger_t182428197 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Finger_t182428197 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FingerGestures/SwipeDirection[]
struct SwipeDirectionU5BU5D_t196035132  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// DragGesture[]
struct DragGestureU5BU5D_t1463800248  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DragGesture_t2914643285 * m_Items[1];

public:
	inline DragGesture_t2914643285 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DragGesture_t2914643285 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DragGesture_t2914643285 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// LongPressGesture[]
struct LongPressGestureU5BU5D_t1349589111  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LongPressGesture_t2876118082 * m_Items[1];

public:
	inline LongPressGesture_t2876118082 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LongPressGesture_t2876118082 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LongPressGesture_t2876118082 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PinchGesture[]
struct PinchGestureU5BU5D_t924307094  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PinchGesture_t1502590799 * m_Items[1];

public:
	inline PinchGesture_t1502590799 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PinchGesture_t1502590799 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PinchGesture_t1502590799 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PointCloudRegognizer/Point[]
struct PointU5BU5D_t2313848483  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Point_t1838831750  m_Items[1];

public:
	inline Point_t1838831750  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Point_t1838831750 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Point_t1838831750  value)
	{
		m_Items[index] = value;
	}
};
// PointCloudGesture[]
struct PointCloudGestureU5BU5D_t1948143885  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PointCloudGesture_t1959506660 * m_Items[1];

public:
	inline PointCloudGesture_t1959506660 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PointCloudGesture_t1959506660 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PointCloudGesture_t1959506660 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PointCloudGestureTemplate[]
struct PointCloudGestureTemplateU5BU5D_t2964884683  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PointCloudGestureTemplate_t3506552702 * m_Items[1];

public:
	inline PointCloudGestureTemplate_t3506552702 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PointCloudGestureTemplate_t3506552702 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PointCloudGestureTemplate_t3506552702 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PointCloudRegognizer/NormalizedTemplate[]
struct NormalizedTemplateU5BU5D_t3448493786  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NormalizedTemplate_t2023934299 * m_Items[1];

public:
	inline NormalizedTemplate_t2023934299 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline NormalizedTemplate_t2023934299 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, NormalizedTemplate_t2023934299 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SwipeGesture[]
struct SwipeGestureU5BU5D_t3820522262  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SwipeGesture_t529355983 * m_Items[1];

public:
	inline SwipeGesture_t529355983 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SwipeGesture_t529355983 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SwipeGesture_t529355983 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TapGesture[]
struct TapGestureU5BU5D_t2662020067  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TapGesture_t659145798 * m_Items[1];

public:
	inline TapGesture_t659145798 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TapGesture_t659145798 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TapGesture_t659145798 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TwistGesture[]
struct TwistGestureU5BU5D_t1214477431  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TwistGesture_t4198361154 * m_Items[1];

public:
	inline TwistGesture_t4198361154 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TwistGesture_t4198361154 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TwistGesture_t4198361154 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Bson.BsonReader/ContainerContext[]
struct ContainerContextU5BU5D_t1595483548  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ContainerContext_t1096623169 * m_Items[1];

public:
	inline ContainerContext_t1096623169 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ContainerContext_t1096623169 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ContainerContext_t1096623169 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Bson.BsonProperty[]
struct BsonPropertyU5BU5D_t2882591352  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BsonProperty_t4293821333 * m_Items[1];

public:
	inline BsonProperty_t4293821333 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BsonProperty_t4293821333 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BsonProperty_t4293821333 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Bson.BsonToken[]
struct BsonTokenU5BU5D_t1977569566  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BsonToken_t455725415 * m_Items[1];

public:
	inline BsonToken_t455725415 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BsonToken_t455725415 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BsonToken_t455725415 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>[]
struct BidirectionalDictionary_2U5BU5D_t3532811451  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BidirectionalDictionary_2_t157076046 * m_Items[1];

public:
	inline BidirectionalDictionary_2_t157076046 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BidirectionalDictionary_2_t157076046 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BidirectionalDictionary_2_t157076046 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Converters.IXmlNode[]
struct IXmlNodeU5BU5D_t572210971  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.JsonConverter[]
struct JsonConverterU5BU5D_t638349667  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JsonConverter_t2159686854 * m_Items[1];

public:
	inline JsonConverter_t2159686854 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsonConverter_t2159686854 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsonConverter_t2159686854 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Linq.JTokenType[]
struct JTokenTypeU5BU5D_t1069135460  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// Newtonsoft.Json.Schema.JsonSchemaModel[]
struct JsonSchemaModelU5BU5D_t2682493439  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JsonSchemaModel_t3035618138 * m_Items[1];

public:
	inline JsonSchemaModel_t3035618138 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsonSchemaModel_t3035618138 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsonSchemaModel_t3035618138 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.JsonValidatingReader/SchemaScope[]
struct SchemaScopeU5BU5D_t164895662  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SchemaScope_t3540946455 * m_Items[1];

public:
	inline SchemaScope_t3540946455 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SchemaScope_t3540946455 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SchemaScope_t3540946455 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.JsonWriter/State[][]
struct StateU5BU5DU5BU5D_t494809214  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) StateU5BU5D_t871800199* m_Items[1];

public:
	inline StateU5BU5D_t871800199* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StateU5BU5D_t871800199** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StateU5BU5D_t871800199* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.JsonWriter/State[]
struct StateU5BU5D_t871800199  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// Newtonsoft.Json.Linq.JToken[]
struct JTokenU5BU5D_t2853253222  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JToken_t3412245951 * m_Items[1];

public:
	inline JToken_t3412245951 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JToken_t3412245951 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JToken_t3412245951 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Schema.JsonSchema[]
struct JsonSchemaU5BU5D_t1104244130  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JsonSchema_t460567603 * m_Items[1];

public:
	inline JsonSchema_t460567603 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsonSchema_t460567603 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsonSchema_t460567603 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Schema.JsonSchemaType[]
struct JsonSchemaTypeU5BU5D_t2531260960  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema[]
struct TypeSchemaU5BU5D_t1420287465  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TypeSchema_t486414904 * m_Items[1];

public:
	inline TypeSchema_t486414904 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TypeSchema_t486414904 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TypeSchema_t486414904 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Utilities.EnumValue`1<System.Int64>[]
struct EnumValue_1U5BU5D_t1263083510  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EnumValue_1_t685341039 * m_Items[1];

public:
	inline EnumValue_1_t685341039 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EnumValue_1_t685341039 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EnumValue_1_t685341039 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Serialization.JsonProperty[]
struct JsonPropertyU5BU5D_t1037910516  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JsonProperty_t902655177 * m_Items[1];

public:
	inline JsonProperty_t902655177 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsonProperty_t902655177 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsonProperty_t902655177 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Schema.JsonSchemaNode[]
struct JsonSchemaNodeU5BU5D_t2972966712  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JsonSchemaNode_t2115227093 * m_Items[1];

public:
	inline JsonSchemaNode_t2115227093 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsonSchemaNode_t2115227093 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsonSchemaNode_t2115227093 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Utilities.EnumValue`1<System.UInt64>[]
struct EnumValue_1U5BU5D_t1262970249  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EnumValue_1_t3851137816 * m_Items[1];

public:
	inline EnumValue_1_t3851137816 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EnumValue_1_t3851137816 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EnumValue_1_t3851137816 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Serialization.JsonContract[]
struct JsonContractU5BU5D_t4247040291  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JsonContract_t1328848902 * m_Items[1];

public:
	inline JsonContract_t1328848902 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsonContract_t1328848902 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsonContract_t1328848902 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey[]
struct TypeNameKeyU5BU5D_t3177491086  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TypeNameKey_t2971844791  m_Items[1];

public:
	inline TypeNameKey_t2971844791  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TypeNameKey_t2971844791 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TypeNameKey_t2971844791  value)
	{
		m_Items[index] = value;
	}
};
// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence[]
struct PropertyPresenceU5BU5D_t176945511  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// Newtonsoft.Json.JsonContainerAttribute[]
struct JsonContainerAttributeU5BU5D_t3541933722  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JsonContainerAttribute_t1917602971 * m_Items[1];

public:
	inline JsonContainerAttribute_t1917602971 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsonContainerAttribute_t1917602971 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsonContainerAttribute_t1917602971 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.Serialization.DataContractAttribute[]
struct DataContractAttributeU5BU5D_t3726683427  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DataContractAttribute_t2462274566 * m_Items[1];

public:
	inline DataContractAttribute_t2462274566 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataContractAttribute_t2462274566 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataContractAttribute_t2462274566 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.Serialization.DataMemberAttribute[]
struct DataMemberAttributeU5BU5D_t2748850443  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DataMemberAttribute_t2601848894 * m_Items[1];

public:
	inline DataMemberAttribute_t2601848894 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataMemberAttribute_t2601848894 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataMemberAttribute_t2601848894 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey[]
struct TypeConvertKeyU5BU5D_t1979890475  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TypeConvertKey_t866134174  m_Items[1];

public:
	inline TypeConvertKey_t866134174  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TypeConvertKey_t866134174 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TypeConvertKey_t866134174  value)
	{
		m_Items[index] = value;
	}
};
// MeleeWeaponTrail/Point[]
struct PointU5BU5D_t2246221518  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Point_t3928961399 * m_Items[1];

public:
	inline Point_t3928961399 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Point_t3928961399 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Point_t3928961399 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TypewriterEffect/FadeEntry[]
struct FadeEntryU5BU5D_t3343031592  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FadeEntry_t2858472101  m_Items[1];

public:
	inline FadeEntry_t2858472101  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FadeEntry_t2858472101 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FadeEntry_t2858472101  value)
	{
		m_Items[index] = value;
	}
};
// UITweener[]
struct UITweenerU5BU5D_t996366285  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UITweener_t105489188 * m_Items[1];

public:
	inline UITweener_t105489188 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UITweener_t105489188 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UITweener_t105489188 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UILabel[]
struct UILabelU5BU5D_t1494885441  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UILabel_t291504320 * m_Items[1];

public:
	inline UILabel_t291504320 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UILabel_t291504320 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UILabel_t291504320 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIPlaySound[]
struct UIPlaySoundU5BU5D_t1195418110  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIPlaySound_t532605447 * m_Items[1];

public:
	inline UIPlaySound_t532605447 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIPlaySound_t532605447 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIPlaySound_t532605447 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIAtlas/Sprite[]
struct SpriteU5BU5D_t2767786832  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Sprite_t2668923293 * m_Items[1];

public:
	inline Sprite_t2668923293 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Sprite_t2668923293 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Sprite_t2668923293 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UISprite[]
struct UISpriteU5BU5D_t3727562628  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UISprite_t661437049 * m_Items[1];

public:
	inline UISprite_t661437049 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UISprite_t661437049 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UISprite_t661437049 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIBasicSprite[]
struct UIBasicSpriteU5BU5D_t3513018950  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIBasicSprite_t2501337439 * m_Items[1];

public:
	inline UIBasicSprite_t2501337439 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIBasicSprite_t2501337439 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIBasicSprite_t2501337439 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIFont[]
struct UIFontU5BU5D_t3954451346  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIFont_t2503090435 * m_Items[1];

public:
	inline UIFont_t2503090435 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIFont_t2503090435 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIFont_t2503090435 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UICamera/MouseOrTouch[]
struct MouseOrTouchU5BU5D_t546398688  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MouseOrTouch_t2057376333 * m_Items[1];

public:
	inline MouseOrTouch_t2057376333 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MouseOrTouch_t2057376333 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MouseOrTouch_t2057376333 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UICamera/DepthEntry[]
struct DepthEntryU5BU5D_t1472539592  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DepthEntry_t1145614469  m_Items[1];

public:
	inline DepthEntry_t1145614469  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DepthEntry_t1145614469 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DepthEntry_t1145614469  value)
	{
		m_Items[index] = value;
	}
};
// UITextList/Paragraph[]
struct ParagraphU5BU5D_t873942891  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Paragraph_t3894573406 * m_Items[1];

public:
	inline Paragraph_t3894573406 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Paragraph_t3894573406 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Paragraph_t3894573406 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ProtoBuf.Meta.AttributeMap[]
struct AttributeMapU5BU5D_t2350548939  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AttributeMap_t924393598 * m_Items[1];

public:
	inline AttributeMap_t924393598 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AttributeMap_t924393598 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AttributeMap_t924393598 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ProtoBuf.Serializers.IProtoSerializer[]
struct IProtoSerializerU5BU5D_t3735579626  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ProtoBuf.Serializers.EnumSerializer/EnumPair[]
struct EnumPairU5BU5D_t754245438  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EnumPair_t276170183  m_Items[1];

public:
	inline EnumPair_t276170183  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EnumPair_t276170183 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EnumPair_t276170183  value)
	{
		m_Items[index] = value;
	}
};
// ProtoBuf.ProtoMemberAttribute[]
struct ProtoMemberAttributeU5BU5D_t4168272899  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ProtoMemberAttribute_t2007578278 * m_Items[1];

public:
	inline ProtoMemberAttribute_t2007578278 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ProtoMemberAttribute_t2007578278 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ProtoMemberAttribute_t2007578278 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ProtoBuf.Meta.ValueMember[]
struct ValueMemberU5BU5D_t3379579312  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ValueMember_t110398141 * m_Items[1];

public:
	inline ValueMember_t110398141 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ValueMember_t110398141 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ValueMember_t110398141 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ProtoBuf.Meta.SubType[]
struct SubTypeU5BU5D_t4186375397  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SubType_t3836516844 * m_Items[1];

public:
	inline SubType_t3836516844 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SubType_t3836516844 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SubType_t3836516844 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ProtoBuf.Meta.MetaType[]
struct MetaTypeU5BU5D_t3238384944  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MetaType_t448283965 * m_Items[1];

public:
	inline MetaType_t448283965 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MetaType_t448283965 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MetaType_t448283965 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ProtoBuf.Serializers.ISerializerProxy[]
struct ISerializerProxyU5BU5D_t3437163968  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// BossAnimationInfo[]
struct BossAnimationInfoU5BU5D_t203552648  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BossAnimationInfo_t384335813 * m_Items[1];

public:
	inline BossAnimationInfo_t384335813 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BossAnimationInfo_t384335813 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BossAnimationInfo_t384335813 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// AnimationPointJSC[]
struct AnimationPointJSCU5BU5D_t4231322619  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AnimationPointJSC_t487595022 * m_Items[1];

public:
	inline AnimationPointJSC_t487595022 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AnimationPointJSC_t487595022 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AnimationPointJSC_t487595022 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// BAMCameras[]
struct BAMCamerasU5BU5D_t2460139873  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BAMCameras_t1661014560 * m_Items[1];

public:
	inline BAMCameras_t1661014560 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BAMCameras_t1661014560 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BAMCameras_t1661014560 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Mihua.Utils.WaitForSeconds[]
struct WaitForSecondsU5BU5D_t695223182  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) WaitForSeconds_t3217447863 * m_Items[1];

public:
	inline WaitForSeconds_t3217447863 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WaitForSeconds_t3217447863 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WaitForSeconds_t3217447863 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TweenPosition[]
struct TweenPositionU5BU5D_t2332880541  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TweenPosition_t3684358292 * m_Items[1];

public:
	inline TweenPosition_t3684358292 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TweenPosition_t3684358292 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TweenPosition_t3684358292 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ChannelConfigCfg[]
struct ChannelConfigCfgU5BU5D_t341260422  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ChannelConfigCfg_t4272979999 * m_Items[1];

public:
	inline ChannelConfigCfg_t4272979999 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ChannelConfigCfg_t4272979999 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ChannelConfigCfg_t4272979999 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HeroEmbattleCfg[]
struct HeroEmbattleCfgU5BU5D_t1295205039  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) HeroEmbattleCfg_t4134124650 * m_Items[1];

public:
	inline HeroEmbattleCfg_t4134124650 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline HeroEmbattleCfg_t4134124650 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, HeroEmbattleCfg_t4134124650 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// JJCCoefficientCfg[]
struct JJCCoefficientCfgU5BU5D_t1710217127  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JJCCoefficientCfg_t2755051730 * m_Items[1];

public:
	inline JJCCoefficientCfg_t2755051730 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JJCCoefficientCfg_t2755051730 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JJCCoefficientCfg_t2755051730 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PetsCfg[]
struct PetsCfgU5BU5D_t2894434257  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PetsCfg_t988016752 * m_Items[1];

public:
	inline PetsCfg_t988016752 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PetsCfg_t988016752 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PetsCfg_t988016752 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SoundCfg[]
struct SoundCfgU5BU5D_t3777181592  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SoundCfg_t1807275253 * m_Items[1];

public:
	inline SoundCfg_t1807275253 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SoundCfg_t1807275253 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SoundCfg_t1807275253 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// StoryCfg[]
struct StoryCfgU5BU5D_t1592181398  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) StoryCfg_t1782371151 * m_Items[1];

public:
	inline StoryCfg_t1782371151 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StoryCfg_t1782371151 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StoryCfg_t1782371151 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TextStringCfg[]
struct TextStringCfgU5BU5D_t4048317027  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TextStringCfg_t884535238 * m_Items[1];

public:
	inline TextStringCfg_t884535238 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TextStringCfg_t884535238 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TextStringCfg_t884535238 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIEffectCfg[]
struct UIEffectCfgU5BU5D_t2665199814  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIEffectCfg_t952653023 * m_Items[1];

public:
	inline UIEffectCfg_t952653023 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIEffectCfg_t952653023 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIEffectCfg_t952653023 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// all_configCfg[]
struct all_configCfgU5BU5D_t4159864909  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) all_configCfg_t3865068708 * m_Items[1];

public:
	inline all_configCfg_t3865068708 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline all_configCfg_t3865068708 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, all_configCfg_t3865068708 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// asset_sharedCfg[]
struct asset_sharedCfgU5BU5D_t3867209233  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) asset_sharedCfg_t1901313840 * m_Items[1];

public:
	inline asset_sharedCfg_t1901313840 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline asset_sharedCfg_t1901313840 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, asset_sharedCfg_t1901313840 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// barrierGateCfg[]
struct barrierGateCfgU5BU5D_t2827718231  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) barrierGateCfg_t3705088994 * m_Items[1];

public:
	inline barrierGateCfg_t3705088994 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline barrierGateCfg_t3705088994 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, barrierGateCfg_t3705088994 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// buffCfg[]
struct buffCfgU5BU5D_t253702540  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) buffCfg_t227963665 * m_Items[1];

public:
	inline buffCfg_t227963665 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline buffCfg_t227963665 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, buffCfg_t227963665 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// checkpointCfg[]
struct checkpointCfgU5BU5D_t3050176469  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) checkpointCfg_t2816107964 * m_Items[1];

public:
	inline checkpointCfg_t2816107964 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline checkpointCfg_t2816107964 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, checkpointCfg_t2816107964 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// demoStoryCfg[]
struct demoStoryCfgU5BU5D_t3083065543  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) demoStoryCfg_t1993162290 * m_Items[1];

public:
	inline demoStoryCfg_t1993162290 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline demoStoryCfg_t1993162290 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, demoStoryCfg_t1993162290 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// effectCfg[]
struct effectCfgU5BU5D_t403044674  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) effectCfg_t2826279187 * m_Items[1];

public:
	inline effectCfg_t2826279187 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline effectCfg_t2826279187 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, effectCfg_t2826279187 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// elementCfg[]
struct elementCfgU5BU5D_t2973419545  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) elementCfg_t575911880 * m_Items[1];

public:
	inline elementCfg_t575911880 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline elementCfg_t575911880 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, elementCfg_t575911880 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// enemy_dropCfg[]
struct enemy_dropCfgU5BU5D_t2626023435  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) enemy_dropCfg_t1026960702 * m_Items[1];

public:
	inline enemy_dropCfg_t1026960702 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline enemy_dropCfg_t1026960702 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, enemy_dropCfg_t1026960702 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// friendNpcCfg[]
struct friendNpcCfgU5BU5D_t2389687516  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) friendNpcCfg_t3890310657 * m_Items[1];

public:
	inline friendNpcCfg_t3890310657 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline friendNpcCfg_t3890310657 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, friendNpcCfg_t3890310657 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// guideCfg[]
struct guideCfgU5BU5D_t3518059545  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) guideCfg_t2981043144 * m_Items[1];

public:
	inline guideCfg_t2981043144 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline guideCfg_t2981043144 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, guideCfg_t2981043144 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// herosCfg[]
struct herosCfgU5BU5D_t4268001802  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) herosCfg_t3676934635 * m_Items[1];

public:
	inline herosCfg_t3676934635 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline herosCfg_t3676934635 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, herosCfg_t3676934635 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// monstersCfg[]
struct monstersCfgU5BU5D_t1478198954  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) monstersCfg_t1542396363 * m_Items[1];

public:
	inline monstersCfg_t1542396363 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline monstersCfg_t1542396363 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, monstersCfg_t1542396363 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// npcAICfg[]
struct npcAICfgU5BU5D_t773228890  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) npcAICfg_t1948330203 * m_Items[1];

public:
	inline npcAICfg_t1948330203 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline npcAICfg_t1948330203 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, npcAICfg_t1948330203 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// npcAIBehaviorCfg[]
struct npcAIBehaviorCfgU5BU5D_t1213873940  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) npcAIBehaviorCfg_t3456736809 * m_Items[1];

public:
	inline npcAIBehaviorCfg_t3456736809 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline npcAIBehaviorCfg_t3456736809 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, npcAIBehaviorCfg_t3456736809 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// npcAIConditionCfg[]
struct npcAIConditionCfgU5BU5D_t1889605191  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) npcAIConditionCfg_t930197170 * m_Items[1];

public:
	inline npcAIConditionCfg_t930197170 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline npcAIConditionCfg_t930197170 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, npcAIConditionCfg_t930197170 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// portalCfg[]
struct portalCfgU5BU5D_t1668678473  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) portalCfg_t1117034584 * m_Items[1];

public:
	inline portalCfg_t1117034584 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline portalCfg_t1117034584 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, portalCfg_t1117034584 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// skillCfg[]
struct skillCfgU5BU5D_t3940203778  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) skillCfg_t2142425171 * m_Items[1];

public:
	inline skillCfg_t2142425171 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline skillCfg_t2142425171 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, skillCfg_t2142425171 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// skill_upgradeCfg[]
struct skill_upgradeCfgU5BU5D_t458175123  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) skill_upgradeCfg_t790726486 * m_Items[1];

public:
	inline skill_upgradeCfg_t790726486 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline skill_upgradeCfg_t790726486 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, skill_upgradeCfg_t790726486 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// superskillCfg[]
struct superskillCfgU5BU5D_t3935676475  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) superskillCfg_t3024131278 * m_Items[1];

public:
	inline superskillCfg_t3024131278 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline superskillCfg_t3024131278 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, superskillCfg_t3024131278 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// taixuTipCfg[]
struct taixuTipCfgU5BU5D_t1672239095  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) taixuTipCfg_t1268722370 * m_Items[1];

public:
	inline taixuTipCfg_t1268722370 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline taixuTipCfg_t1268722370 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, taixuTipCfg_t1268722370 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// towerbuffCfg[]
struct towerbuffCfgU5BU5D_t3371137913  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) towerbuffCfg_t1755856872 * m_Items[1];

public:
	inline towerbuffCfg_t1755856872 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline towerbuffCfg_t1755856872 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, towerbuffCfg_t1755856872 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CameraAniMap[]
struct CameraAniMapU5BU5D_t2645520072  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CameraAniMap_t3004656005 * m_Items[1];

public:
	inline CameraAniMap_t3004656005 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CameraAniMap_t3004656005 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CameraAniMap_t3004656005 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CameraAniGroup[]
struct CameraAniGroupU5BU5D_t3180594425  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CameraAniGroup_t1251365992 * m_Items[1];

public:
	inline CameraAniGroup_t1251365992 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CameraAniGroup_t1251365992 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CameraAniGroup_t1251365992 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CameraShotsCfg[]
struct CameraShotsCfgU5BU5D_t3593403761  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CameraShotsCfg_t1740446544 * m_Items[1];

public:
	inline CameraShotsCfg_t1740446544 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CameraShotsCfg_t1740446544 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CameraShotsCfg_t1740446544 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CurCameraShotCfg[]
struct CurCameraShotCfgU5BU5D_t4123229416  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CurCameraShotCfg_t224861669 * m_Items[1];

public:
	inline CurCameraShotCfg_t224861669 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CurCameraShotCfg_t224861669 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CurCameraShotCfg_t224861669 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CameraShotNodeCfg[]
struct CameraShotNodeCfgU5BU5D_t4005465586  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CameraShotNodeCfg_t3577740835 * m_Items[1];

public:
	inline CameraShotNodeCfg_t3577740835 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CameraShotNodeCfg_t3577740835 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CameraShotNodeCfg_t3577740835 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CameraShotHeroOrNpcCfg[]
struct CameraShotHeroOrNpcCfgU5BU5D_t4121800582  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CameraShotHeroOrNpcCfg_t2707952927 * m_Items[1];

public:
	inline CameraShotHeroOrNpcCfg_t2707952927 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CameraShotHeroOrNpcCfg_t2707952927 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CameraShotHeroOrNpcCfg_t2707952927 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CameraShotStoryCfg[]
struct CameraShotStoryCfgU5BU5D_t298290011  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CameraShotStoryCfg_t4130953262 * m_Items[1];

public:
	inline CameraShotStoryCfg_t4130953262 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CameraShotStoryCfg_t4130953262 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CameraShotStoryCfg_t4130953262 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CameraShotGameSpeedCfg[]
struct CameraShotGameSpeedCfgU5BU5D_t2397130075  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CameraShotGameSpeedCfg_t308624942 * m_Items[1];

public:
	inline CameraShotGameSpeedCfg_t308624942 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CameraShotGameSpeedCfg_t308624942 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CameraShotGameSpeedCfg_t308624942 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CameraShotActionCfg[]
struct CameraShotActionCfgU5BU5D_t363038006  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CameraShotActionCfg_t1993837871 * m_Items[1];

public:
	inline CameraShotActionCfg_t1993837871 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CameraShotActionCfg_t1993837871 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CameraShotActionCfg_t1993837871 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CameraShotSkillCfg[]
struct CameraShotSkillCfgU5BU5D_t1680567591  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CameraShotSkillCfg_t260684114 * m_Items[1];

public:
	inline CameraShotSkillCfg_t260684114 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CameraShotSkillCfg_t260684114 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CameraShotSkillCfg_t260684114 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CameraShotMoveCfg[]
struct CameraShotMoveCfgU5BU5D_t2132544445  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CameraShotMoveCfg_t2706860532 * m_Items[1];

public:
	inline CameraShotMoveCfg_t2706860532 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CameraShotMoveCfg_t2706860532 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CameraShotMoveCfg_t2706860532 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CameraShotCircleCfg[]
struct CameraShotCircleCfgU5BU5D_t4123542200  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CameraShotCircleCfg_t2591002709 * m_Items[1];

public:
	inline CameraShotCircleCfg_t2591002709 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CameraShotCircleCfg_t2591002709 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CameraShotCircleCfg_t2591002709 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CameraShotEffectCfg[]
struct CameraShotEffectCfgU5BU5D_t3368766909  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CameraShotEffectCfg_t326881268 * m_Items[1];

public:
	inline CameraShotEffectCfg_t326881268 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CameraShotEffectCfg_t326881268 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CameraShotEffectCfg_t326881268 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// JSCLevelConfig[]
struct JSCLevelConfigU5BU5D_t2912812389  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JSCLevelConfig_t1411099500 * m_Items[1];

public:
	inline JSCLevelConfig_t1411099500 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JSCLevelConfig_t1411099500 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JSCLevelConfig_t1411099500 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// JSCMapPathConfig[]
struct JSCMapPathConfigU5BU5D_t3730756724  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JSCMapPathConfig_t3426118729 * m_Items[1];

public:
	inline JSCMapPathConfig_t3426118729 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JSCMapPathConfig_t3426118729 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JSCMapPathConfig_t3426118729 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// JSCWaveNpcConfig[]
struct JSCWaveNpcConfigU5BU5D_t3953319729  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JSCWaveNpcConfig_t4221504656 * m_Items[1];

public:
	inline JSCWaveNpcConfig_t4221504656 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JSCWaveNpcConfig_t4221504656 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JSCWaveNpcConfig_t4221504656 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// JSCMapPathPointInfoConfig[]
struct JSCMapPathPointInfoConfigU5BU5D_t3930892644  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JSCMapPathPointInfoConfig_t2703681049 * m_Items[1];

public:
	inline JSCMapPathPointInfoConfig_t2703681049 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JSCMapPathPointInfoConfig_t2703681049 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JSCMapPathPointInfoConfig_t2703681049 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// JSCLevelHeroPointConfig[]
struct JSCLevelHeroPointConfigU5BU5D_t3022208475  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JSCLevelHeroPointConfig_t561690542 * m_Items[1];

public:
	inline JSCLevelHeroPointConfig_t561690542 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JSCLevelHeroPointConfig_t561690542 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JSCLevelHeroPointConfig_t561690542 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TabData[]
struct TabDataU5BU5D_t1357379942  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TabData_t110553279 * m_Items[1];

public:
	inline TabData_t110553279 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TabData_t110553279 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TabData_t110553279 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ByteReadArray[]
struct ByteReadArrayU5BU5D_t91219738  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ByteReadArray_t751193627 * m_Items[1];

public:
	inline ByteReadArray_t751193627 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ByteReadArray_t751193627 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ByteReadArray_t751193627 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnitStateBase[]
struct UnitStateBaseU5BU5D_t50225547  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UnitStateBase_t1040218558 * m_Items[1];

public:
	inline UnitStateBase_t1040218558 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UnitStateBase_t1040218558 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UnitStateBase_t1040218558 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// shaderBuff[]
struct shaderBuffU5BU5D_t4142498761  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) shaderBuff_t2982920664 * m_Items[1];

public:
	inline shaderBuff_t2982920664 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline shaderBuff_t2982920664 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, shaderBuff_t2982920664 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Entity.Behavior.EBehaviorID[]
struct EBehaviorIDU5BU5D_t373889457  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// Entity.Behavior.IBehavior[]
struct IBehaviorU5BU5D_t563026308  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) IBehavior_t770859129 * m_Items[1];

public:
	inline IBehavior_t770859129 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline IBehavior_t770859129 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, IBehavior_t770859129 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// AIEventInfo[]
struct AIEventInfoU5BU5D_t526056545  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AIEventInfo_t4031722272 * m_Items[1];

public:
	inline AIEventInfo_t4031722272 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AIEventInfo_t4031722272 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AIEventInfo_t4031722272 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// AIEnum.EAIEventtype[]
struct EAIEventtypeU5BU5D_t2693623942  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// PlotFightTalkMgr/plotData[]
struct plotDataU5BU5D_t3260598704  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) plotData_t645270205 * m_Items[1];

public:
	inline plotData_t645270205 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline plotData_t645270205 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, plotData_t645270205 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ReplayBase[]
struct ReplayBaseU5BU5D_t734812073  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ReplayBase_t779703160 * m_Items[1];

public:
	inline ReplayBase_t779703160 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ReplayBase_t779703160 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ReplayBase_t779703160 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ReplayHeroUnit[]
struct ReplayHeroUnitU5BU5D_t676085128  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ReplayHeroUnit_t605237701 * m_Items[1];

public:
	inline ReplayHeroUnit_t605237701 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ReplayHeroUnit_t605237701 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ReplayHeroUnit_t605237701 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HitTarget[]
struct HitTargetU5BU5D_t4266401037  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) HitTarget_t3687896804 * m_Items[1];

public:
	inline HitTarget_t3687896804 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline HitTarget_t3687896804 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, HitTarget_t3687896804 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Buff[]
struct BuffU5BU5D_t2714489954  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Buff_t2081907 * m_Items[1];

public:
	inline Buff_t2081907 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Buff_t2081907 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Buff_t2081907 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Skill[]
struct SkillU5BU5D_t1937287660  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Skill_t79944241 * m_Items[1];

public:
	inline Skill_t79944241 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Skill_t79944241 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Skill_t79944241 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// DamageSeparationSkill[]
struct DamageSeparationSkillU5BU5D_t3199467765  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DamageSeparationSkill_t2001134364 * m_Items[1];

public:
	inline DamageSeparationSkill_t2001134364 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DamageSeparationSkill_t2001134364 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DamageSeparationSkill_t2001134364 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ElementVO[]
struct ElementVOU5BU5D_t19933048  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ElementVO_t1745451669 * m_Items[1];

public:
	inline ElementVO_t1745451669 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ElementVO_t1745451669 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ElementVO_t1745451669 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HatredCtrl/stValue[]
struct stValueU5BU5D_t1121401207  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) stValue_t3425945410  m_Items[1];

public:
	inline stValue_t3425945410  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline stValue_t3425945410 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, stValue_t3425945410  value)
	{
		m_Items[index] = value;
	}
};
// FlowControl.IFlowBase[]
struct IFlowBaseU5BU5D_t4194178955  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Blood[]
struct BloodU5BU5D_t2773149055  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Blood_t64280026 * m_Items[1];

public:
	inline Blood_t64280026 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Blood_t64280026 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Blood_t64280026 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// EnemyDropItem[]
struct EnemyDropItemU5BU5D_t1651926511  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EnemyDropItem_t1468754474 * m_Items[1];

public:
	inline EnemyDropItem_t1468754474 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EnemyDropItem_t1468754474 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EnemyDropItem_t1468754474 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// NumEffect[]
struct NumEffectU5BU5D_t1853655854  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NumEffect_t2824221847 * m_Items[1];

public:
	inline NumEffect_t2824221847 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline NumEffect_t2824221847 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, NumEffect_t2824221847 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HeroMgr/LineupPos[]
struct LineupPosU5BU5D_t3531345085  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LineupPos_t2885729524 * m_Items[1];

public:
	inline LineupPos_t2885729524 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LineupPos_t2885729524 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LineupPos_t2885729524 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TweenScale[]
struct TweenScaleU5BU5D_t3591996518  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TweenScale_t2936666559 * m_Items[1];

public:
	inline TweenScale_t2936666559 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TweenScale_t2936666559 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TweenScale_t2936666559 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TargetMgr/FightPoint[]
struct FightPointU5BU5D_t2958494585  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FightPoint_t4216057832 * m_Items[1];

public:
	inline FightPoint_t4216057832 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FightPoint_t4216057832 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FightPoint_t4216057832 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TargetMgr/AngleEntity[]
struct AngleEntityU5BU5D_t2741789947  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AngleEntity_t3194128142 * m_Items[1];

public:
	inline AngleEntity_t3194128142 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AngleEntity_t3194128142 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AngleEntity_t3194128142 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Mihua.Asset.ABLoadOperation.AssetOperationPreload1[]
struct AssetOperationPreload1U5BU5D_t1555748552  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AssetOperationPreload1_t2389844357 * m_Items[1];

public:
	inline AssetOperationPreload1_t2389844357 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AssetOperationPreload1_t2389844357 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AssetOperationPreload1_t2389844357 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Mihua.Asset.ABLoadOperation.AssetOperation[]
struct AssetOperationU5BU5D_t2822427600  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AssetOperation_t778728221 * m_Items[1];

public:
	inline AssetOperation_t778728221 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AssetOperation_t778728221 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AssetOperation_t778728221 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FileInfoCrc[]
struct FileInfoCrcU5BU5D_t248480655  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FileInfoCrc_t1217045770 * m_Items[1];

public:
	inline FileInfoCrc_t1217045770 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FileInfoCrc_t1217045770 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FileInfoCrc_t1217045770 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Mihua.Net.UrlLoader[]
struct UrlLoaderU5BU5D_t4254019721  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UrlLoader_t2490729496 * m_Items[1];

public:
	inline UrlLoader_t2490729496 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UrlLoader_t2490729496 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UrlLoader_t2490729496 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Mihua.Assets.SubAssetMgr/ErrorInfo[]
struct ErrorInfoU5BU5D_t2864912703  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ErrorInfo_t2633981210  m_Items[1];

public:
	inline ErrorInfo_t2633981210  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ErrorInfo_t2633981210 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ErrorInfo_t2633981210  value)
	{
		m_Items[index] = value;
	}
};
// CSCampFightMonsterData[]
struct CSCampFightMonsterDataU5BU5D_t1399553576  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CSCampFightMonsterData_t1577764261 * m_Items[1];

public:
	inline CSCampFightMonsterData_t1577764261 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CSCampFightMonsterData_t1577764261 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CSCampFightMonsterData_t1577764261 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CSCampFightAddAttributeData[]
struct CSCampFightAddAttributeDataU5BU5D_t2430053645  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CSCampFightAddAttributeData_t790660836 * m_Items[1];

public:
	inline CSCampFightAddAttributeData_t790660836 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CSCampFightAddAttributeData_t790660836 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CSCampFightAddAttributeData_t790660836 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IEffComponent[]
struct IEffComponentU5BU5D_t971155292  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) IEffComponent_t3802264961 * m_Items[1];

public:
	inline IEffComponent_t3802264961 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline IEffComponent_t3802264961 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, IEffComponent_t3802264961 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// EffectCtrl[]
struct EffectCtrlU5BU5D_t2726244821  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EffectCtrl_t3708787644 * m_Items[1];

public:
	inline EffectCtrl_t3708787644 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EffectCtrl_t3708787644 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EffectCtrl_t3708787644 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FloatTextUnit[]
struct FloatTextUnitU5BU5D_t1909147648  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FloatTextUnit_t2362298029 * m_Items[1];

public:
	inline FloatTextUnit_t2362298029 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FloatTextUnit_t2362298029 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FloatTextUnit_t2362298029 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FLOAT_TEXT_ID[]
struct FLOAT_TEXT_IDU5BU5D_t3484773551  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UITexture[]
struct UITextureU5BU5D_t795227294  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UITexture_t3903132647 * m_Items[1];

public:
	inline UITexture_t3903132647 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UITexture_t3903132647 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UITexture_t3903132647 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TweenAlpha[]
struct TweenAlphaU5BU5D_t2326787458  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TweenAlpha_t2920325587 * m_Items[1];

public:
	inline TweenAlpha_t2920325587 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TweenAlpha_t2920325587 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TweenAlpha_t2920325587 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// StaticParticle[]
struct StaticParticleU5BU5D_t3915946365  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) StaticParticle_t2206539572 * m_Items[1];

public:
	inline StaticParticle_t2206539572 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StaticParticle_t2206539572 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StaticParticle_t2206539572 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// MHIAPMgr.PayInfo[]
struct PayInfoU5BU5D_t3982363253  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PayInfo_t444430236 * m_Items[1];

public:
	inline PayInfo_t444430236 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PayInfo_t444430236 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PayInfo_t444430236 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ProductsCfgMgr/ProductInfo[]
struct ProductInfoU5BU5D_t182341603  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ProductInfo_t1305991238  m_Items[1];

public:
	inline ProductInfo_t1305991238  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ProductInfo_t1305991238 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ProductInfo_t1305991238  value)
	{
		m_Items[index] = value;
	}
};
// SoundObj[]
struct SoundObjU5BU5D_t2567724889  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SoundObj_t1807286664 * m_Items[1];

public:
	inline SoundObj_t1807286664 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SoundObj_t1807286664 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SoundObj_t1807286664 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SoundTypeID[]
struct SoundTypeIDU5BU5D_t1111200077  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// ISound[]
struct ISoundU5BU5D_t1350087139  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ISound_t2170003014 * m_Items[1];

public:
	inline ISound_t2170003014 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ISound_t2170003014 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ISound_t2170003014 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SoundStatus[]
struct SoundStatusU5BU5D_t3992956444  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// CameraShotMgr/CameraShotVO[]
struct CameraShotVOU5BU5D_t658558747  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CameraShotVO_t1900810094 * m_Items[1];

public:
	inline CameraShotVO_t1900810094 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CameraShotVO_t1900810094 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CameraShotVO_t1900810094 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// StoryMgr/StoryVO[]
struct StoryVOU5BU5D_t1197331319  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) StoryVO_t3225531714 * m_Items[1];

public:
	inline StoryVO_t3225531714 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StoryVO_t3225531714 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StoryVO_t3225531714 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TimeUpdateVo[]
struct TimeUpdateVoU5BU5D_t3056897526  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TimeUpdateVo_t1829512047 * m_Items[1];

public:
	inline TimeUpdateVo_t1829512047 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TimeUpdateVo_t1829512047 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TimeUpdateVo_t1829512047 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// AssetDownMgr/SetBytes[]
struct SetBytesU5BU5D_t1146585959  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SetBytes_t2856136722 * m_Items[1];

public:
	inline SetBytes_t2856136722 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SetBytes_t2856136722 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SetBytes_t2856136722 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIModelDisplayType[]
struct UIModelDisplayTypeU5BU5D_t737309086  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UIModelDisplay[]
struct UIModelDisplayU5BU5D_t2767931168  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIModelDisplay_t1730520589 * m_Items[1];

public:
	inline UIModelDisplay_t1730520589 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIModelDisplay_t1730520589 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIModelDisplay_t1730520589 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pomelo.DotNetClient.MsgEvent[]
struct MsgEventU5BU5D_t3978639512  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MsgEvent_t82323445 * m_Items[1];

public:
	inline MsgEvent_t82323445 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MsgEvent_t82323445 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MsgEvent_t82323445 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// InputItem[]
struct InputItemU5BU5D_t269107728  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) InputItem_t3710611933 * m_Items[1];

public:
	inline InputItem_t3710611933 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline InputItem_t3710611933 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, InputItem_t3710611933 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// InputItem[,]
struct InputItemU5BU2CU5D_t269107729  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) InputItem_t3710611933 * m_Items[1];

public:
	inline InputItem_t3710611933 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline InputItem_t3710611933 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, InputItem_t3710611933 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline InputItem_t3710611933 * GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		return m_Items[index];
	}
	inline InputItem_t3710611933 ** GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, InputItem_t3710611933 * value)
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PushType[]
struct PushTypeU5BU5D_t3978570141  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// PluginPush/NotificationData[]
struct NotificationDataU5BU5D_t2759200174  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NotificationData_t3289515031 * m_Items[1];

public:
	inline NotificationData_t3289515031 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline NotificationData_t3289515031 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, NotificationData_t3289515031 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PluginReYun/strutDict[]
struct strutDictU5BU5D_t1829737458  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) strutDict_t4055560227  m_Items[1];

public:
	inline strutDict_t4055560227  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline strutDict_t4055560227 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, strutDict_t4055560227  value)
	{
		m_Items[index] = value;
	}
};
// UpdateForJs[]
struct UpdateForJsU5BU5D_t3700693812  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UpdateForJs_t1071193225 * m_Items[1];

public:
	inline UpdateForJs_t1071193225 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UpdateForJs_t1071193225 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UpdateForJs_t1071193225 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IZUpdate[]
struct IZUpdateU5BU5D_t2438514687  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// AnimationRunner/AniType[]
struct AniTypeU5BU5D_t3368333050  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// MScrollView/MoveWay[]
struct MoveWayU5BU5D_t505035803  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// MScrollView/OnMoveCallBackFun[]
struct OnMoveCallBackFunU5BU5D_t500157791  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) OnMoveCallBackFun_t3535987578 * m_Items[1];

public:
	inline OnMoveCallBackFun_t3535987578 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline OnMoveCallBackFun_t3535987578 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, OnMoveCallBackFun_t3535987578 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// T4MLodObjSC[]
struct T4MLodObjSCU5BU5D_t2636096386  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) T4MLodObjSC_t2288258003 * m_Items[1];

public:
	inline T4MLodObjSC_t2288258003 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline T4MLodObjSC_t2288258003 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, T4MLodObjSC_t2288258003 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// T4MBillBObjSC[]
struct T4MBillBObjSCU5BU5D_t3601884420  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) T4MBillBObjSC_t2448121 * m_Items[1];

public:
	inline T4MBillBObjSC_t2448121 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline T4MBillBObjSC_t2448121 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, T4MBillBObjSC_t2448121 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
