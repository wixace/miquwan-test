﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._adf6e546f2684172bf480b322bb5c3e0
struct _adf6e546f2684172bf480b322bb5c3e0_t3245021083;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._adf6e546f2684172bf480b322bb5c3e0::.ctor()
extern "C"  void _adf6e546f2684172bf480b322bb5c3e0__ctor_m3831065842 (_adf6e546f2684172bf480b322bb5c3e0_t3245021083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._adf6e546f2684172bf480b322bb5c3e0::_adf6e546f2684172bf480b322bb5c3e0m2(System.Int32)
extern "C"  int32_t _adf6e546f2684172bf480b322bb5c3e0__adf6e546f2684172bf480b322bb5c3e0m2_m2859373497 (_adf6e546f2684172bf480b322bb5c3e0_t3245021083 * __this, int32_t ____adf6e546f2684172bf480b322bb5c3e0a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._adf6e546f2684172bf480b322bb5c3e0::_adf6e546f2684172bf480b322bb5c3e0m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _adf6e546f2684172bf480b322bb5c3e0__adf6e546f2684172bf480b322bb5c3e0m_m2987833117 (_adf6e546f2684172bf480b322bb5c3e0_t3245021083 * __this, int32_t ____adf6e546f2684172bf480b322bb5c3e0a0, int32_t ____adf6e546f2684172bf480b322bb5c3e0851, int32_t ____adf6e546f2684172bf480b322bb5c3e0c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
