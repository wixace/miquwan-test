﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen756388270.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

// System.Void System.Array/InternalEnumerator`1<Pathfinding.Int3>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2008400095_gshared (InternalEnumerator_1_t756388270 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2008400095(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t756388270 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2008400095_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.Int3>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m221917793_gshared (InternalEnumerator_1_t756388270 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m221917793(__this, method) ((  void (*) (InternalEnumerator_1_t756388270 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m221917793_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Pathfinding.Int3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2710553037_gshared (InternalEnumerator_1_t756388270 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2710553037(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t756388270 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2710553037_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.Int3>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m396756342_gshared (InternalEnumerator_1_t756388270 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m396756342(__this, method) ((  void (*) (InternalEnumerator_1_t756388270 *, const MethodInfo*))InternalEnumerator_1_Dispose_m396756342_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.Int3>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1955302349_gshared (InternalEnumerator_1_t756388270 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1955302349(__this, method) ((  bool (*) (InternalEnumerator_1_t756388270 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1955302349_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Pathfinding.Int3>::get_Current()
extern "C"  Int3_t1974045594  InternalEnumerator_1_get_Current_m3819708582_gshared (InternalEnumerator_1_t756388270 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3819708582(__this, method) ((  Int3_t1974045594  (*) (InternalEnumerator_1_t756388270 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3819708582_gshared)(__this, method)
