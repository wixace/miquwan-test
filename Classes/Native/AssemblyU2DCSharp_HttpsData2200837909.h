﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Text.Encoding
struct Encoding_t2012439129;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HttpsData
struct  HttpsData_t2200837909  : public Il2CppObject
{
public:
	// System.String HttpsData::text
	String_t* ___text_0;
	// System.Byte[] HttpsData::bytes
	ByteU5BU5D_t4260760469* ___bytes_1;
	// System.Text.Encoding HttpsData::encoding
	Encoding_t2012439129 * ___encoding_2;
	// System.String HttpsData::url
	String_t* ___url_3;
	// System.Int64 HttpsData::contentLength
	int64_t ___contentLength_4;
	// System.String HttpsData::error
	String_t* ___error_5;
	// System.Single HttpsData::progress
	float ___progress_6;
	// System.Boolean HttpsData::isDone
	bool ___isDone_7;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(HttpsData_t2200837909, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier(&___text_0, value);
	}

	inline static int32_t get_offset_of_bytes_1() { return static_cast<int32_t>(offsetof(HttpsData_t2200837909, ___bytes_1)); }
	inline ByteU5BU5D_t4260760469* get_bytes_1() const { return ___bytes_1; }
	inline ByteU5BU5D_t4260760469** get_address_of_bytes_1() { return &___bytes_1; }
	inline void set_bytes_1(ByteU5BU5D_t4260760469* value)
	{
		___bytes_1 = value;
		Il2CppCodeGenWriteBarrier(&___bytes_1, value);
	}

	inline static int32_t get_offset_of_encoding_2() { return static_cast<int32_t>(offsetof(HttpsData_t2200837909, ___encoding_2)); }
	inline Encoding_t2012439129 * get_encoding_2() const { return ___encoding_2; }
	inline Encoding_t2012439129 ** get_address_of_encoding_2() { return &___encoding_2; }
	inline void set_encoding_2(Encoding_t2012439129 * value)
	{
		___encoding_2 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_2, value);
	}

	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(HttpsData_t2200837909, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier(&___url_3, value);
	}

	inline static int32_t get_offset_of_contentLength_4() { return static_cast<int32_t>(offsetof(HttpsData_t2200837909, ___contentLength_4)); }
	inline int64_t get_contentLength_4() const { return ___contentLength_4; }
	inline int64_t* get_address_of_contentLength_4() { return &___contentLength_4; }
	inline void set_contentLength_4(int64_t value)
	{
		___contentLength_4 = value;
	}

	inline static int32_t get_offset_of_error_5() { return static_cast<int32_t>(offsetof(HttpsData_t2200837909, ___error_5)); }
	inline String_t* get_error_5() const { return ___error_5; }
	inline String_t** get_address_of_error_5() { return &___error_5; }
	inline void set_error_5(String_t* value)
	{
		___error_5 = value;
		Il2CppCodeGenWriteBarrier(&___error_5, value);
	}

	inline static int32_t get_offset_of_progress_6() { return static_cast<int32_t>(offsetof(HttpsData_t2200837909, ___progress_6)); }
	inline float get_progress_6() const { return ___progress_6; }
	inline float* get_address_of_progress_6() { return &___progress_6; }
	inline void set_progress_6(float value)
	{
		___progress_6 = value;
	}

	inline static int32_t get_offset_of_isDone_7() { return static_cast<int32_t>(offsetof(HttpsData_t2200837909, ___isDone_7)); }
	inline bool get_isDone_7() const { return ___isDone_7; }
	inline bool* get_address_of_isDone_7() { return &___isDone_7; }
	inline void set_isDone_7(bool value)
	{
		___isDone_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
