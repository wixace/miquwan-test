﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1190435706MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,enemy_dropCfg>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4146133808(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2341547333 *, Dictionary_2_t1024223941 *, const MethodInfo*))Enumerator__ctor_m2377115088_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,enemy_dropCfg>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3167486139(__this, method) ((  Il2CppObject * (*) (Enumerator_t2341547333 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1037642267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,enemy_dropCfg>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2960390021(__this, method) ((  void (*) (Enumerator_t2341547333 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2809374949_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,enemy_dropCfg>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3717816700(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2341547333 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2434214620_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,enemy_dropCfg>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1703445847(__this, method) ((  Il2CppObject * (*) (Enumerator_t2341547333 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3735627447_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,enemy_dropCfg>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1677355561(__this, method) ((  Il2CppObject * (*) (Enumerator_t2341547333 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m393753481_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,enemy_dropCfg>::MoveNext()
#define Enumerator_MoveNext_m938907317(__this, method) ((  bool (*) (Enumerator_t2341547333 *, const MethodInfo*))Enumerator_MoveNext_m1213995029_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,enemy_dropCfg>::get_Current()
#define Enumerator_get_Current_m1059432935(__this, method) ((  KeyValuePair_2_t923004647  (*) (Enumerator_t2341547333 *, const MethodInfo*))Enumerator_get_Current_m1399860359_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,enemy_dropCfg>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m511795582(__this, method) ((  int32_t (*) (Enumerator_t2341547333 *, const MethodInfo*))Enumerator_get_CurrentKey_m1767398110_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,enemy_dropCfg>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3636627518(__this, method) ((  enemy_dropCfg_t1026960702 * (*) (Enumerator_t2341547333 *, const MethodInfo*))Enumerator_get_CurrentValue_m3384846750_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,enemy_dropCfg>::Reset()
#define Enumerator_Reset_m2346175874(__this, method) ((  void (*) (Enumerator_t2341547333 *, const MethodInfo*))Enumerator_Reset_m1080084514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,enemy_dropCfg>::VerifyState()
#define Enumerator_VerifyState_m2064086027(__this, method) ((  void (*) (Enumerator_t2341547333 *, const MethodInfo*))Enumerator_VerifyState_m2404513451_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,enemy_dropCfg>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2056652979(__this, method) ((  void (*) (Enumerator_t2341547333 *, const MethodInfo*))Enumerator_VerifyCurrent_m2789892947_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,enemy_dropCfg>::Dispose()
#define Enumerator_Dispose_m2340613586(__this, method) ((  void (*) (Enumerator_t2341547333 *, const MethodInfo*))Enumerator_Dispose_m1102561394_gshared)(__this, method)
