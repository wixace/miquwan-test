﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HitTarget
struct  HitTarget_t3687896804  : public Il2CppObject
{
public:
	// System.Int32 HitTarget::id
	int32_t ___id_0;
	// System.Int32 HitTarget::IsEnemy
	int32_t ___IsEnemy_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(HitTarget_t3687896804, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_IsEnemy_1() { return static_cast<int32_t>(offsetof(HitTarget_t3687896804, ___IsEnemy_1)); }
	inline int32_t get_IsEnemy_1() const { return ___IsEnemy_1; }
	inline int32_t* get_address_of_IsEnemy_1() { return &___IsEnemy_1; }
	inline void set_IsEnemy_1(int32_t value)
	{
		___IsEnemy_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
