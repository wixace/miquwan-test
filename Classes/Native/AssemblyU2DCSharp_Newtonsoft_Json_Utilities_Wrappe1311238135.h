﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1918497079;
// System.Func`2<System.Reflection.ParameterInfo,System.Type>
struct Func_2_t4078497774;
// System.Func`2<System.Type,System.String>
struct Func_2_t2546297092;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.WrapperMethodBuilder
struct  WrapperMethodBuilder_t1311238135  : public Il2CppObject
{
public:
	// System.Type Newtonsoft.Json.Utilities.WrapperMethodBuilder::_realObjectType
	Type_t * ____realObjectType_0;
	// System.Reflection.Emit.TypeBuilder Newtonsoft.Json.Utilities.WrapperMethodBuilder::_wrapperBuilder
	TypeBuilder_t1918497079 * ____wrapperBuilder_1;

public:
	inline static int32_t get_offset_of__realObjectType_0() { return static_cast<int32_t>(offsetof(WrapperMethodBuilder_t1311238135, ____realObjectType_0)); }
	inline Type_t * get__realObjectType_0() const { return ____realObjectType_0; }
	inline Type_t ** get_address_of__realObjectType_0() { return &____realObjectType_0; }
	inline void set__realObjectType_0(Type_t * value)
	{
		____realObjectType_0 = value;
		Il2CppCodeGenWriteBarrier(&____realObjectType_0, value);
	}

	inline static int32_t get_offset_of__wrapperBuilder_1() { return static_cast<int32_t>(offsetof(WrapperMethodBuilder_t1311238135, ____wrapperBuilder_1)); }
	inline TypeBuilder_t1918497079 * get__wrapperBuilder_1() const { return ____wrapperBuilder_1; }
	inline TypeBuilder_t1918497079 ** get_address_of__wrapperBuilder_1() { return &____wrapperBuilder_1; }
	inline void set__wrapperBuilder_1(TypeBuilder_t1918497079 * value)
	{
		____wrapperBuilder_1 = value;
		Il2CppCodeGenWriteBarrier(&____wrapperBuilder_1, value);
	}
};

struct WrapperMethodBuilder_t1311238135_StaticFields
{
public:
	// System.Func`2<System.Reflection.ParameterInfo,System.Type> Newtonsoft.Json.Utilities.WrapperMethodBuilder::<>f__am$cache2
	Func_2_t4078497774 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<System.Type,System.String> Newtonsoft.Json.Utilities.WrapperMethodBuilder::<>f__am$cache3
	Func_2_t2546297092 * ___U3CU3Ef__amU24cache3_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(WrapperMethodBuilder_t1311238135_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_t4078497774 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_t4078497774 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_t4078497774 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(WrapperMethodBuilder_t1311238135_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_t2546297092 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_t2546297092 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_t2546297092 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
