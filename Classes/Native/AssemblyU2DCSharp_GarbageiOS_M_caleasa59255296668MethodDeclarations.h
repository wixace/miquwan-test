﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_caleasa59
struct M_caleasa59_t255296668;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_caleasa59255296668.h"

// System.Void GarbageiOS.M_caleasa59::.ctor()
extern "C"  void M_caleasa59__ctor_m239275079 (M_caleasa59_t255296668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_caleasa59::M_stawjar0(System.String[],System.Int32)
extern "C"  void M_caleasa59_M_stawjar0_m157424690 (M_caleasa59_t255296668 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_caleasa59::M_paykereva1(System.String[],System.Int32)
extern "C"  void M_caleasa59_M_paykereva1_m1907120823 (M_caleasa59_t255296668 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_caleasa59::M_pubownallHevu2(System.String[],System.Int32)
extern "C"  void M_caleasa59_M_pubownallHevu2_m2477861476 (M_caleasa59_t255296668 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_caleasa59::M_wohuStise3(System.String[],System.Int32)
extern "C"  void M_caleasa59_M_wohuStise3_m2254915620 (M_caleasa59_t255296668 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_caleasa59::M_jasirouJudogoo4(System.String[],System.Int32)
extern "C"  void M_caleasa59_M_jasirouJudogoo4_m3880880888 (M_caleasa59_t255296668 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_caleasa59::ilo_M_paykereva11(GarbageiOS.M_caleasa59,System.String[],System.Int32)
extern "C"  void M_caleasa59_ilo_M_paykereva11_m2235254801 (Il2CppObject * __this /* static, unused */, M_caleasa59_t255296668 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_caleasa59::ilo_M_pubownallHevu22(GarbageiOS.M_caleasa59,System.String[],System.Int32)
extern "C"  void M_caleasa59_ilo_M_pubownallHevu22_m736865757 (Il2CppObject * __this /* static, unused */, M_caleasa59_t255296668 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_caleasa59::ilo_M_wohuStise33(GarbageiOS.M_caleasa59,System.String[],System.Int32)
extern "C"  void M_caleasa59_ilo_M_wohuStise33_m3221683132 (Il2CppObject * __this /* static, unused */, M_caleasa59_t255296668 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
