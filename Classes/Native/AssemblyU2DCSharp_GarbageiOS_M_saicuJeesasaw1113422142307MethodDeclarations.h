﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_saicuJeesasaw111
struct M_saicuJeesasaw111_t3422142307;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_saicuJeesasaw111::.ctor()
extern "C"  void M_saicuJeesasaw111__ctor_m1266179632 (M_saicuJeesasaw111_t3422142307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_saicuJeesasaw111::M_nojouta0(System.String[],System.Int32)
extern "C"  void M_saicuJeesasaw111_M_nojouta0_m947870641 (M_saicuJeesasaw111_t3422142307 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
