﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RecastGraph/<ScanAllTiles>c__AnonStorey119
struct  U3CScanAllTilesU3Ec__AnonStorey119_t2626901840  : public Il2CppObject
{
public:
	// System.UInt32 Pathfinding.RecastGraph/<ScanAllTiles>c__AnonStorey119::graphIndex
	uint32_t ___graphIndex_0;

public:
	inline static int32_t get_offset_of_graphIndex_0() { return static_cast<int32_t>(offsetof(U3CScanAllTilesU3Ec__AnonStorey119_t2626901840, ___graphIndex_0)); }
	inline uint32_t get_graphIndex_0() const { return ___graphIndex_0; }
	inline uint32_t* get_address_of_graphIndex_0() { return &___graphIndex_0; }
	inline void set_graphIndex_0(uint32_t value)
	{
		___graphIndex_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
