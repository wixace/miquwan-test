﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICameraGenerated/<UICamera_GetKeyDown_GetDelegate_member1_arg0>c__AnonStorey9D
struct U3CUICamera_GetKeyDown_GetDelegate_member1_arg0U3Ec__AnonStorey9D_t2653586763;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"

// System.Void UICameraGenerated/<UICamera_GetKeyDown_GetDelegate_member1_arg0>c__AnonStorey9D::.ctor()
extern "C"  void U3CUICamera_GetKeyDown_GetDelegate_member1_arg0U3Ec__AnonStorey9D__ctor_m2837169792 (U3CUICamera_GetKeyDown_GetDelegate_member1_arg0U3Ec__AnonStorey9D_t2653586763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated/<UICamera_GetKeyDown_GetDelegate_member1_arg0>c__AnonStorey9D::<>m__FE(UnityEngine.KeyCode)
extern "C"  bool U3CUICamera_GetKeyDown_GetDelegate_member1_arg0U3Ec__AnonStorey9D_U3CU3Em__FE_m955571265 (U3CUICamera_GetKeyDown_GetDelegate_member1_arg0U3Ec__AnonStorey9D_t2653586763 * __this, int32_t ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
