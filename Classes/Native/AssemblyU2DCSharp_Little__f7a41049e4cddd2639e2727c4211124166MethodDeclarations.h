﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._f7a41049e4cddd2639e2727ce481029f
struct _f7a41049e4cddd2639e2727ce481029f_t4211124166;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._f7a41049e4cddd2639e2727ce481029f::.ctor()
extern "C"  void _f7a41049e4cddd2639e2727ce481029f__ctor_m4200324391 (_f7a41049e4cddd2639e2727ce481029f_t4211124166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f7a41049e4cddd2639e2727ce481029f::_f7a41049e4cddd2639e2727ce481029fm2(System.Int32)
extern "C"  int32_t _f7a41049e4cddd2639e2727ce481029f__f7a41049e4cddd2639e2727ce481029fm2_m3186543577 (_f7a41049e4cddd2639e2727ce481029f_t4211124166 * __this, int32_t ____f7a41049e4cddd2639e2727ce481029fa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f7a41049e4cddd2639e2727ce481029f::_f7a41049e4cddd2639e2727ce481029fm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _f7a41049e4cddd2639e2727ce481029f__f7a41049e4cddd2639e2727ce481029fm_m1408230141 (_f7a41049e4cddd2639e2727ce481029f_t4211124166 * __this, int32_t ____f7a41049e4cddd2639e2727ce481029fa0, int32_t ____f7a41049e4cddd2639e2727ce481029f511, int32_t ____f7a41049e4cddd2639e2727ce481029fc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
