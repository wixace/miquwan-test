﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<Entity.Behavior.EBehaviorID,System.Object,System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>>
struct Transform_1_t2597404387;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21209901952.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Entity.Behavior.EBehaviorID,System.Object,System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m972786914_gshared (Transform_1_t2597404387 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m972786914(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t2597404387 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m972786914_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Entity.Behavior.EBehaviorID,System.Object,System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1209901952  Transform_1_Invoke_m3986244054_gshared (Transform_1_t2597404387 * __this, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m3986244054(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t1209901952  (*) (Transform_1_t2597404387 *, uint8_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m3986244054_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Entity.Behavior.EBehaviorID,System.Object,System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m900290689_gshared (Transform_1_t2597404387 * __this, uint8_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m900290689(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t2597404387 *, uint8_t, Il2CppObject *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m900290689_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Entity.Behavior.EBehaviorID,System.Object,System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1209901952  Transform_1_EndInvoke_m1559166196_gshared (Transform_1_t2597404387 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m1559166196(__this, ___result0, method) ((  KeyValuePair_2_t1209901952  (*) (Transform_1_t2597404387 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m1559166196_gshared)(__this, ___result0, method)
