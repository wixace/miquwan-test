﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]>
struct DGetV_1_t2855056638;
// JSDataExchangeMgr/DGetV`1<System.String[]>
struct DGetV_1_t3931654293;
// JSDataExchangeMgr/DGetV`1<UnityEngine.Texture[]>
struct DGetV_1_t483827417;
// JSDataExchangeMgr/DGetV`1<UnityEngine.GUIContent[]>
struct DGetV_1_t3466377156;
// JSDataExchangeMgr/DGetV`1<UnityEngine.GUI/WindowFunction>
struct DGetV_1_t2626940000;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine_GUILayoutGenerated
struct  UnityEngine_GUILayoutGenerated_t3776240178  : public Il2CppObject
{
public:

public:
};

struct UnityEngine_GUILayoutGenerated_t3776240178_StaticFields
{
public:
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache0
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache0_0;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache1
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache1_1;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache2
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache2_2;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache3
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache3_3;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache4
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache4_4;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache5
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache5_5;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache6
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache6_6;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache7
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache7_7;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache8
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache8_8;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache9
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache9_9;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cacheA
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cacheA_10;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cacheB
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cacheB_11;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cacheC
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cacheC_12;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cacheD
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cacheD_13;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cacheE
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cacheE_14;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cacheF
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cacheF_15;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache10
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache10_16;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache11
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache11_17;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache12
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache12_18;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache13
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache13_19;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache14
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache14_20;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache15
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache15_21;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache16
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache16_22;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache17
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache17_23;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache18
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache18_24;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache19
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache19_25;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache1A
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache1A_26;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache1B
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache1B_27;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache1C
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache1C_28;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache1D
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache1D_29;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache1E
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache1E_30;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache1F
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache1F_31;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache20
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache20_32;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache21
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache21_33;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache22
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache22_34;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache23
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache23_35;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache24
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache24_36;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache25
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache25_37;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache26
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache26_38;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache27
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache27_39;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache28
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache28_40;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache29
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache29_41;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache2A
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache2A_42;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache2B
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache2B_43;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache2C
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache2C_44;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache2D
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache2D_45;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache2E
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache2E_46;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache2F
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache2F_47;
	// JSDataExchangeMgr/DGetV`1<System.String[]> UnityEngine_GUILayoutGenerated::<>f__am$cache30
	DGetV_1_t3931654293 * ___U3CU3Ef__amU24cache30_48;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache31
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache31_49;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Texture[]> UnityEngine_GUILayoutGenerated::<>f__am$cache32
	DGetV_1_t483827417 * ___U3CU3Ef__amU24cache32_50;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache33
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache33_51;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUIContent[]> UnityEngine_GUILayoutGenerated::<>f__am$cache34
	DGetV_1_t3466377156 * ___U3CU3Ef__amU24cache34_52;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache35
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache35_53;
	// JSDataExchangeMgr/DGetV`1<System.String[]> UnityEngine_GUILayoutGenerated::<>f__am$cache36
	DGetV_1_t3931654293 * ___U3CU3Ef__amU24cache36_54;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache37
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache37_55;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Texture[]> UnityEngine_GUILayoutGenerated::<>f__am$cache38
	DGetV_1_t483827417 * ___U3CU3Ef__amU24cache38_56;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache39
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache39_57;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUIContent[]> UnityEngine_GUILayoutGenerated::<>f__am$cache3A
	DGetV_1_t3466377156 * ___U3CU3Ef__amU24cache3A_58;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache3B
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache3B_59;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache3C
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache3C_60;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache3D
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache3D_61;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache3E
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache3E_62;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache3F
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache3F_63;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache40
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache40_64;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache41
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache41_65;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache42
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache42_66;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache43
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache43_67;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache44
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache44_68;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache45
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache45_69;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache46
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache46_70;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache47
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache47_71;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache48
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache48_72;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache49
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache49_73;
	// JSDataExchangeMgr/DGetV`1<System.String[]> UnityEngine_GUILayoutGenerated::<>f__am$cache4A
	DGetV_1_t3931654293 * ___U3CU3Ef__amU24cache4A_74;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache4B
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache4B_75;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Texture[]> UnityEngine_GUILayoutGenerated::<>f__am$cache4C
	DGetV_1_t483827417 * ___U3CU3Ef__amU24cache4C_76;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache4D
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache4D_77;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUIContent[]> UnityEngine_GUILayoutGenerated::<>f__am$cache4E
	DGetV_1_t3466377156 * ___U3CU3Ef__amU24cache4E_78;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache4F
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache4F_79;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.Texture[]> UnityEngine_GUILayoutGenerated::<>f__am$cache50
	DGetV_1_t483827417 * ___U3CU3Ef__amU24cache50_80;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache51
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache51_81;
	// JSDataExchangeMgr/DGetV`1<System.String[]> UnityEngine_GUILayoutGenerated::<>f__am$cache52
	DGetV_1_t3931654293 * ___U3CU3Ef__amU24cache52_82;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache53
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache53_83;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUIContent[]> UnityEngine_GUILayoutGenerated::<>f__am$cache54
	DGetV_1_t3466377156 * ___U3CU3Ef__amU24cache54_84;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache55
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache55_85;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache56
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache56_86;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache57
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache57_87;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache58
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache58_88;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache59
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache59_89;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUI/WindowFunction> UnityEngine_GUILayoutGenerated::<>f__am$cache5A
	DGetV_1_t2626940000 * ___U3CU3Ef__amU24cache5A_90;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache5B
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache5B_91;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUI/WindowFunction> UnityEngine_GUILayoutGenerated::<>f__am$cache5C
	DGetV_1_t2626940000 * ___U3CU3Ef__amU24cache5C_92;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache5D
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache5D_93;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUI/WindowFunction> UnityEngine_GUILayoutGenerated::<>f__am$cache5E
	DGetV_1_t2626940000 * ___U3CU3Ef__amU24cache5E_94;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache5F
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache5F_95;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUI/WindowFunction> UnityEngine_GUILayoutGenerated::<>f__am$cache60
	DGetV_1_t2626940000 * ___U3CU3Ef__amU24cache60_96;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache61
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache61_97;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUI/WindowFunction> UnityEngine_GUILayoutGenerated::<>f__am$cache62
	DGetV_1_t2626940000 * ___U3CU3Ef__amU24cache62_98;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache63
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache63_99;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUI/WindowFunction> UnityEngine_GUILayoutGenerated::<>f__am$cache64
	DGetV_1_t2626940000 * ___U3CU3Ef__amU24cache64_100;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.GUILayoutOption[]> UnityEngine_GUILayoutGenerated::<>f__am$cache65
	DGetV_1_t2855056638 * ___U3CU3Ef__amU24cache65_101;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_8() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache8_8)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache8_8() const { return ___U3CU3Ef__amU24cache8_8; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache8_8() { return &___U3CU3Ef__amU24cache8_8; }
	inline void set_U3CU3Ef__amU24cache8_8(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_9() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache9_9)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache9_9() const { return ___U3CU3Ef__amU24cache9_9; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache9_9() { return &___U3CU3Ef__amU24cache9_9; }
	inline void set_U3CU3Ef__amU24cache9_9(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_10() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cacheA_10)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cacheA_10() const { return ___U3CU3Ef__amU24cacheA_10; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cacheA_10() { return &___U3CU3Ef__amU24cacheA_10; }
	inline void set_U3CU3Ef__amU24cacheA_10(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cacheA_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_11() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cacheB_11)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cacheB_11() const { return ___U3CU3Ef__amU24cacheB_11; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cacheB_11() { return &___U3CU3Ef__amU24cacheB_11; }
	inline void set_U3CU3Ef__amU24cacheB_11(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cacheB_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_12() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cacheC_12)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cacheC_12() const { return ___U3CU3Ef__amU24cacheC_12; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cacheC_12() { return &___U3CU3Ef__amU24cacheC_12; }
	inline void set_U3CU3Ef__amU24cacheC_12(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cacheC_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_13() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cacheD_13)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cacheD_13() const { return ___U3CU3Ef__amU24cacheD_13; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cacheD_13() { return &___U3CU3Ef__amU24cacheD_13; }
	inline void set_U3CU3Ef__amU24cacheD_13(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cacheD_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_14() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cacheE_14)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cacheE_14() const { return ___U3CU3Ef__amU24cacheE_14; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cacheE_14() { return &___U3CU3Ef__amU24cacheE_14; }
	inline void set_U3CU3Ef__amU24cacheE_14(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cacheE_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheF_15() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cacheF_15)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cacheF_15() const { return ___U3CU3Ef__amU24cacheF_15; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cacheF_15() { return &___U3CU3Ef__amU24cacheF_15; }
	inline void set_U3CU3Ef__amU24cacheF_15(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cacheF_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheF_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache10_16() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache10_16)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache10_16() const { return ___U3CU3Ef__amU24cache10_16; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache10_16() { return &___U3CU3Ef__amU24cache10_16; }
	inline void set_U3CU3Ef__amU24cache10_16(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache10_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache10_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache11_17() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache11_17)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache11_17() const { return ___U3CU3Ef__amU24cache11_17; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache11_17() { return &___U3CU3Ef__amU24cache11_17; }
	inline void set_U3CU3Ef__amU24cache11_17(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache11_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache11_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache12_18() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache12_18)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache12_18() const { return ___U3CU3Ef__amU24cache12_18; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache12_18() { return &___U3CU3Ef__amU24cache12_18; }
	inline void set_U3CU3Ef__amU24cache12_18(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache12_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache12_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache13_19() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache13_19)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache13_19() const { return ___U3CU3Ef__amU24cache13_19; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache13_19() { return &___U3CU3Ef__amU24cache13_19; }
	inline void set_U3CU3Ef__amU24cache13_19(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache13_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache13_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache14_20() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache14_20)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache14_20() const { return ___U3CU3Ef__amU24cache14_20; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache14_20() { return &___U3CU3Ef__amU24cache14_20; }
	inline void set_U3CU3Ef__amU24cache14_20(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache14_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache14_20, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache15_21() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache15_21)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache15_21() const { return ___U3CU3Ef__amU24cache15_21; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache15_21() { return &___U3CU3Ef__amU24cache15_21; }
	inline void set_U3CU3Ef__amU24cache15_21(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache15_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache15_21, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache16_22() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache16_22)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache16_22() const { return ___U3CU3Ef__amU24cache16_22; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache16_22() { return &___U3CU3Ef__amU24cache16_22; }
	inline void set_U3CU3Ef__amU24cache16_22(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache16_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache16_22, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache17_23() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache17_23)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache17_23() const { return ___U3CU3Ef__amU24cache17_23; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache17_23() { return &___U3CU3Ef__amU24cache17_23; }
	inline void set_U3CU3Ef__amU24cache17_23(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache17_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache17_23, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache18_24() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache18_24)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache18_24() const { return ___U3CU3Ef__amU24cache18_24; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache18_24() { return &___U3CU3Ef__amU24cache18_24; }
	inline void set_U3CU3Ef__amU24cache18_24(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache18_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache18_24, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache19_25() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache19_25)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache19_25() const { return ___U3CU3Ef__amU24cache19_25; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache19_25() { return &___U3CU3Ef__amU24cache19_25; }
	inline void set_U3CU3Ef__amU24cache19_25(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache19_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache19_25, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1A_26() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache1A_26)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache1A_26() const { return ___U3CU3Ef__amU24cache1A_26; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache1A_26() { return &___U3CU3Ef__amU24cache1A_26; }
	inline void set_U3CU3Ef__amU24cache1A_26(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache1A_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1A_26, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1B_27() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache1B_27)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache1B_27() const { return ___U3CU3Ef__amU24cache1B_27; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache1B_27() { return &___U3CU3Ef__amU24cache1B_27; }
	inline void set_U3CU3Ef__amU24cache1B_27(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache1B_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1B_27, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1C_28() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache1C_28)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache1C_28() const { return ___U3CU3Ef__amU24cache1C_28; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache1C_28() { return &___U3CU3Ef__amU24cache1C_28; }
	inline void set_U3CU3Ef__amU24cache1C_28(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache1C_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1C_28, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1D_29() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache1D_29)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache1D_29() const { return ___U3CU3Ef__amU24cache1D_29; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache1D_29() { return &___U3CU3Ef__amU24cache1D_29; }
	inline void set_U3CU3Ef__amU24cache1D_29(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache1D_29 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1D_29, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1E_30() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache1E_30)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache1E_30() const { return ___U3CU3Ef__amU24cache1E_30; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache1E_30() { return &___U3CU3Ef__amU24cache1E_30; }
	inline void set_U3CU3Ef__amU24cache1E_30(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache1E_30 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1E_30, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1F_31() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache1F_31)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache1F_31() const { return ___U3CU3Ef__amU24cache1F_31; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache1F_31() { return &___U3CU3Ef__amU24cache1F_31; }
	inline void set_U3CU3Ef__amU24cache1F_31(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache1F_31 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1F_31, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache20_32() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache20_32)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache20_32() const { return ___U3CU3Ef__amU24cache20_32; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache20_32() { return &___U3CU3Ef__amU24cache20_32; }
	inline void set_U3CU3Ef__amU24cache20_32(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache20_32 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache20_32, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache21_33() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache21_33)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache21_33() const { return ___U3CU3Ef__amU24cache21_33; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache21_33() { return &___U3CU3Ef__amU24cache21_33; }
	inline void set_U3CU3Ef__amU24cache21_33(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache21_33 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache21_33, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache22_34() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache22_34)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache22_34() const { return ___U3CU3Ef__amU24cache22_34; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache22_34() { return &___U3CU3Ef__amU24cache22_34; }
	inline void set_U3CU3Ef__amU24cache22_34(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache22_34 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache22_34, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache23_35() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache23_35)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache23_35() const { return ___U3CU3Ef__amU24cache23_35; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache23_35() { return &___U3CU3Ef__amU24cache23_35; }
	inline void set_U3CU3Ef__amU24cache23_35(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache23_35 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache23_35, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache24_36() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache24_36)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache24_36() const { return ___U3CU3Ef__amU24cache24_36; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache24_36() { return &___U3CU3Ef__amU24cache24_36; }
	inline void set_U3CU3Ef__amU24cache24_36(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache24_36 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache24_36, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache25_37() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache25_37)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache25_37() const { return ___U3CU3Ef__amU24cache25_37; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache25_37() { return &___U3CU3Ef__amU24cache25_37; }
	inline void set_U3CU3Ef__amU24cache25_37(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache25_37 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache25_37, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache26_38() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache26_38)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache26_38() const { return ___U3CU3Ef__amU24cache26_38; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache26_38() { return &___U3CU3Ef__amU24cache26_38; }
	inline void set_U3CU3Ef__amU24cache26_38(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache26_38 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache26_38, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache27_39() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache27_39)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache27_39() const { return ___U3CU3Ef__amU24cache27_39; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache27_39() { return &___U3CU3Ef__amU24cache27_39; }
	inline void set_U3CU3Ef__amU24cache27_39(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache27_39 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache27_39, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache28_40() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache28_40)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache28_40() const { return ___U3CU3Ef__amU24cache28_40; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache28_40() { return &___U3CU3Ef__amU24cache28_40; }
	inline void set_U3CU3Ef__amU24cache28_40(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache28_40 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache28_40, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache29_41() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache29_41)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache29_41() const { return ___U3CU3Ef__amU24cache29_41; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache29_41() { return &___U3CU3Ef__amU24cache29_41; }
	inline void set_U3CU3Ef__amU24cache29_41(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache29_41 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache29_41, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2A_42() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache2A_42)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache2A_42() const { return ___U3CU3Ef__amU24cache2A_42; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache2A_42() { return &___U3CU3Ef__amU24cache2A_42; }
	inline void set_U3CU3Ef__amU24cache2A_42(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache2A_42 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2A_42, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2B_43() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache2B_43)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache2B_43() const { return ___U3CU3Ef__amU24cache2B_43; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache2B_43() { return &___U3CU3Ef__amU24cache2B_43; }
	inline void set_U3CU3Ef__amU24cache2B_43(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache2B_43 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2B_43, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2C_44() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache2C_44)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache2C_44() const { return ___U3CU3Ef__amU24cache2C_44; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache2C_44() { return &___U3CU3Ef__amU24cache2C_44; }
	inline void set_U3CU3Ef__amU24cache2C_44(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache2C_44 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2C_44, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2D_45() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache2D_45)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache2D_45() const { return ___U3CU3Ef__amU24cache2D_45; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache2D_45() { return &___U3CU3Ef__amU24cache2D_45; }
	inline void set_U3CU3Ef__amU24cache2D_45(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache2D_45 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2D_45, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2E_46() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache2E_46)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache2E_46() const { return ___U3CU3Ef__amU24cache2E_46; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache2E_46() { return &___U3CU3Ef__amU24cache2E_46; }
	inline void set_U3CU3Ef__amU24cache2E_46(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache2E_46 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2E_46, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2F_47() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache2F_47)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache2F_47() const { return ___U3CU3Ef__amU24cache2F_47; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache2F_47() { return &___U3CU3Ef__amU24cache2F_47; }
	inline void set_U3CU3Ef__amU24cache2F_47(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache2F_47 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2F_47, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache30_48() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache30_48)); }
	inline DGetV_1_t3931654293 * get_U3CU3Ef__amU24cache30_48() const { return ___U3CU3Ef__amU24cache30_48; }
	inline DGetV_1_t3931654293 ** get_address_of_U3CU3Ef__amU24cache30_48() { return &___U3CU3Ef__amU24cache30_48; }
	inline void set_U3CU3Ef__amU24cache30_48(DGetV_1_t3931654293 * value)
	{
		___U3CU3Ef__amU24cache30_48 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache30_48, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache31_49() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache31_49)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache31_49() const { return ___U3CU3Ef__amU24cache31_49; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache31_49() { return &___U3CU3Ef__amU24cache31_49; }
	inline void set_U3CU3Ef__amU24cache31_49(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache31_49 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache31_49, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache32_50() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache32_50)); }
	inline DGetV_1_t483827417 * get_U3CU3Ef__amU24cache32_50() const { return ___U3CU3Ef__amU24cache32_50; }
	inline DGetV_1_t483827417 ** get_address_of_U3CU3Ef__amU24cache32_50() { return &___U3CU3Ef__amU24cache32_50; }
	inline void set_U3CU3Ef__amU24cache32_50(DGetV_1_t483827417 * value)
	{
		___U3CU3Ef__amU24cache32_50 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache32_50, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache33_51() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache33_51)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache33_51() const { return ___U3CU3Ef__amU24cache33_51; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache33_51() { return &___U3CU3Ef__amU24cache33_51; }
	inline void set_U3CU3Ef__amU24cache33_51(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache33_51 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache33_51, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache34_52() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache34_52)); }
	inline DGetV_1_t3466377156 * get_U3CU3Ef__amU24cache34_52() const { return ___U3CU3Ef__amU24cache34_52; }
	inline DGetV_1_t3466377156 ** get_address_of_U3CU3Ef__amU24cache34_52() { return &___U3CU3Ef__amU24cache34_52; }
	inline void set_U3CU3Ef__amU24cache34_52(DGetV_1_t3466377156 * value)
	{
		___U3CU3Ef__amU24cache34_52 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache34_52, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache35_53() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache35_53)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache35_53() const { return ___U3CU3Ef__amU24cache35_53; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache35_53() { return &___U3CU3Ef__amU24cache35_53; }
	inline void set_U3CU3Ef__amU24cache35_53(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache35_53 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache35_53, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache36_54() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache36_54)); }
	inline DGetV_1_t3931654293 * get_U3CU3Ef__amU24cache36_54() const { return ___U3CU3Ef__amU24cache36_54; }
	inline DGetV_1_t3931654293 ** get_address_of_U3CU3Ef__amU24cache36_54() { return &___U3CU3Ef__amU24cache36_54; }
	inline void set_U3CU3Ef__amU24cache36_54(DGetV_1_t3931654293 * value)
	{
		___U3CU3Ef__amU24cache36_54 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache36_54, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache37_55() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache37_55)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache37_55() const { return ___U3CU3Ef__amU24cache37_55; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache37_55() { return &___U3CU3Ef__amU24cache37_55; }
	inline void set_U3CU3Ef__amU24cache37_55(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache37_55 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache37_55, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache38_56() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache38_56)); }
	inline DGetV_1_t483827417 * get_U3CU3Ef__amU24cache38_56() const { return ___U3CU3Ef__amU24cache38_56; }
	inline DGetV_1_t483827417 ** get_address_of_U3CU3Ef__amU24cache38_56() { return &___U3CU3Ef__amU24cache38_56; }
	inline void set_U3CU3Ef__amU24cache38_56(DGetV_1_t483827417 * value)
	{
		___U3CU3Ef__amU24cache38_56 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache38_56, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache39_57() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache39_57)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache39_57() const { return ___U3CU3Ef__amU24cache39_57; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache39_57() { return &___U3CU3Ef__amU24cache39_57; }
	inline void set_U3CU3Ef__amU24cache39_57(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache39_57 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache39_57, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3A_58() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache3A_58)); }
	inline DGetV_1_t3466377156 * get_U3CU3Ef__amU24cache3A_58() const { return ___U3CU3Ef__amU24cache3A_58; }
	inline DGetV_1_t3466377156 ** get_address_of_U3CU3Ef__amU24cache3A_58() { return &___U3CU3Ef__amU24cache3A_58; }
	inline void set_U3CU3Ef__amU24cache3A_58(DGetV_1_t3466377156 * value)
	{
		___U3CU3Ef__amU24cache3A_58 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3A_58, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3B_59() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache3B_59)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache3B_59() const { return ___U3CU3Ef__amU24cache3B_59; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache3B_59() { return &___U3CU3Ef__amU24cache3B_59; }
	inline void set_U3CU3Ef__amU24cache3B_59(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache3B_59 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3B_59, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3C_60() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache3C_60)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache3C_60() const { return ___U3CU3Ef__amU24cache3C_60; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache3C_60() { return &___U3CU3Ef__amU24cache3C_60; }
	inline void set_U3CU3Ef__amU24cache3C_60(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache3C_60 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3C_60, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3D_61() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache3D_61)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache3D_61() const { return ___U3CU3Ef__amU24cache3D_61; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache3D_61() { return &___U3CU3Ef__amU24cache3D_61; }
	inline void set_U3CU3Ef__amU24cache3D_61(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache3D_61 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3D_61, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3E_62() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache3E_62)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache3E_62() const { return ___U3CU3Ef__amU24cache3E_62; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache3E_62() { return &___U3CU3Ef__amU24cache3E_62; }
	inline void set_U3CU3Ef__amU24cache3E_62(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache3E_62 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3E_62, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3F_63() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache3F_63)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache3F_63() const { return ___U3CU3Ef__amU24cache3F_63; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache3F_63() { return &___U3CU3Ef__amU24cache3F_63; }
	inline void set_U3CU3Ef__amU24cache3F_63(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache3F_63 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3F_63, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache40_64() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache40_64)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache40_64() const { return ___U3CU3Ef__amU24cache40_64; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache40_64() { return &___U3CU3Ef__amU24cache40_64; }
	inline void set_U3CU3Ef__amU24cache40_64(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache40_64 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache40_64, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache41_65() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache41_65)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache41_65() const { return ___U3CU3Ef__amU24cache41_65; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache41_65() { return &___U3CU3Ef__amU24cache41_65; }
	inline void set_U3CU3Ef__amU24cache41_65(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache41_65 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache41_65, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache42_66() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache42_66)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache42_66() const { return ___U3CU3Ef__amU24cache42_66; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache42_66() { return &___U3CU3Ef__amU24cache42_66; }
	inline void set_U3CU3Ef__amU24cache42_66(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache42_66 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache42_66, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache43_67() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache43_67)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache43_67() const { return ___U3CU3Ef__amU24cache43_67; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache43_67() { return &___U3CU3Ef__amU24cache43_67; }
	inline void set_U3CU3Ef__amU24cache43_67(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache43_67 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache43_67, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache44_68() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache44_68)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache44_68() const { return ___U3CU3Ef__amU24cache44_68; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache44_68() { return &___U3CU3Ef__amU24cache44_68; }
	inline void set_U3CU3Ef__amU24cache44_68(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache44_68 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache44_68, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache45_69() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache45_69)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache45_69() const { return ___U3CU3Ef__amU24cache45_69; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache45_69() { return &___U3CU3Ef__amU24cache45_69; }
	inline void set_U3CU3Ef__amU24cache45_69(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache45_69 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache45_69, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache46_70() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache46_70)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache46_70() const { return ___U3CU3Ef__amU24cache46_70; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache46_70() { return &___U3CU3Ef__amU24cache46_70; }
	inline void set_U3CU3Ef__amU24cache46_70(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache46_70 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache46_70, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache47_71() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache47_71)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache47_71() const { return ___U3CU3Ef__amU24cache47_71; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache47_71() { return &___U3CU3Ef__amU24cache47_71; }
	inline void set_U3CU3Ef__amU24cache47_71(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache47_71 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache47_71, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache48_72() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache48_72)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache48_72() const { return ___U3CU3Ef__amU24cache48_72; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache48_72() { return &___U3CU3Ef__amU24cache48_72; }
	inline void set_U3CU3Ef__amU24cache48_72(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache48_72 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache48_72, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache49_73() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache49_73)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache49_73() const { return ___U3CU3Ef__amU24cache49_73; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache49_73() { return &___U3CU3Ef__amU24cache49_73; }
	inline void set_U3CU3Ef__amU24cache49_73(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache49_73 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache49_73, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4A_74() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache4A_74)); }
	inline DGetV_1_t3931654293 * get_U3CU3Ef__amU24cache4A_74() const { return ___U3CU3Ef__amU24cache4A_74; }
	inline DGetV_1_t3931654293 ** get_address_of_U3CU3Ef__amU24cache4A_74() { return &___U3CU3Ef__amU24cache4A_74; }
	inline void set_U3CU3Ef__amU24cache4A_74(DGetV_1_t3931654293 * value)
	{
		___U3CU3Ef__amU24cache4A_74 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4A_74, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4B_75() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache4B_75)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache4B_75() const { return ___U3CU3Ef__amU24cache4B_75; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache4B_75() { return &___U3CU3Ef__amU24cache4B_75; }
	inline void set_U3CU3Ef__amU24cache4B_75(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache4B_75 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4B_75, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4C_76() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache4C_76)); }
	inline DGetV_1_t483827417 * get_U3CU3Ef__amU24cache4C_76() const { return ___U3CU3Ef__amU24cache4C_76; }
	inline DGetV_1_t483827417 ** get_address_of_U3CU3Ef__amU24cache4C_76() { return &___U3CU3Ef__amU24cache4C_76; }
	inline void set_U3CU3Ef__amU24cache4C_76(DGetV_1_t483827417 * value)
	{
		___U3CU3Ef__amU24cache4C_76 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4C_76, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4D_77() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache4D_77)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache4D_77() const { return ___U3CU3Ef__amU24cache4D_77; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache4D_77() { return &___U3CU3Ef__amU24cache4D_77; }
	inline void set_U3CU3Ef__amU24cache4D_77(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache4D_77 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4D_77, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4E_78() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache4E_78)); }
	inline DGetV_1_t3466377156 * get_U3CU3Ef__amU24cache4E_78() const { return ___U3CU3Ef__amU24cache4E_78; }
	inline DGetV_1_t3466377156 ** get_address_of_U3CU3Ef__amU24cache4E_78() { return &___U3CU3Ef__amU24cache4E_78; }
	inline void set_U3CU3Ef__amU24cache4E_78(DGetV_1_t3466377156 * value)
	{
		___U3CU3Ef__amU24cache4E_78 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4E_78, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4F_79() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache4F_79)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache4F_79() const { return ___U3CU3Ef__amU24cache4F_79; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache4F_79() { return &___U3CU3Ef__amU24cache4F_79; }
	inline void set_U3CU3Ef__amU24cache4F_79(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache4F_79 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4F_79, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache50_80() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache50_80)); }
	inline DGetV_1_t483827417 * get_U3CU3Ef__amU24cache50_80() const { return ___U3CU3Ef__amU24cache50_80; }
	inline DGetV_1_t483827417 ** get_address_of_U3CU3Ef__amU24cache50_80() { return &___U3CU3Ef__amU24cache50_80; }
	inline void set_U3CU3Ef__amU24cache50_80(DGetV_1_t483827417 * value)
	{
		___U3CU3Ef__amU24cache50_80 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache50_80, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache51_81() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache51_81)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache51_81() const { return ___U3CU3Ef__amU24cache51_81; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache51_81() { return &___U3CU3Ef__amU24cache51_81; }
	inline void set_U3CU3Ef__amU24cache51_81(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache51_81 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache51_81, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache52_82() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache52_82)); }
	inline DGetV_1_t3931654293 * get_U3CU3Ef__amU24cache52_82() const { return ___U3CU3Ef__amU24cache52_82; }
	inline DGetV_1_t3931654293 ** get_address_of_U3CU3Ef__amU24cache52_82() { return &___U3CU3Ef__amU24cache52_82; }
	inline void set_U3CU3Ef__amU24cache52_82(DGetV_1_t3931654293 * value)
	{
		___U3CU3Ef__amU24cache52_82 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache52_82, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache53_83() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache53_83)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache53_83() const { return ___U3CU3Ef__amU24cache53_83; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache53_83() { return &___U3CU3Ef__amU24cache53_83; }
	inline void set_U3CU3Ef__amU24cache53_83(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache53_83 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache53_83, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache54_84() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache54_84)); }
	inline DGetV_1_t3466377156 * get_U3CU3Ef__amU24cache54_84() const { return ___U3CU3Ef__amU24cache54_84; }
	inline DGetV_1_t3466377156 ** get_address_of_U3CU3Ef__amU24cache54_84() { return &___U3CU3Ef__amU24cache54_84; }
	inline void set_U3CU3Ef__amU24cache54_84(DGetV_1_t3466377156 * value)
	{
		___U3CU3Ef__amU24cache54_84 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache54_84, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache55_85() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache55_85)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache55_85() const { return ___U3CU3Ef__amU24cache55_85; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache55_85() { return &___U3CU3Ef__amU24cache55_85; }
	inline void set_U3CU3Ef__amU24cache55_85(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache55_85 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache55_85, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache56_86() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache56_86)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache56_86() const { return ___U3CU3Ef__amU24cache56_86; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache56_86() { return &___U3CU3Ef__amU24cache56_86; }
	inline void set_U3CU3Ef__amU24cache56_86(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache56_86 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache56_86, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache57_87() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache57_87)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache57_87() const { return ___U3CU3Ef__amU24cache57_87; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache57_87() { return &___U3CU3Ef__amU24cache57_87; }
	inline void set_U3CU3Ef__amU24cache57_87(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache57_87 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache57_87, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache58_88() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache58_88)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache58_88() const { return ___U3CU3Ef__amU24cache58_88; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache58_88() { return &___U3CU3Ef__amU24cache58_88; }
	inline void set_U3CU3Ef__amU24cache58_88(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache58_88 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache58_88, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache59_89() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache59_89)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache59_89() const { return ___U3CU3Ef__amU24cache59_89; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache59_89() { return &___U3CU3Ef__amU24cache59_89; }
	inline void set_U3CU3Ef__amU24cache59_89(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache59_89 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache59_89, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5A_90() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache5A_90)); }
	inline DGetV_1_t2626940000 * get_U3CU3Ef__amU24cache5A_90() const { return ___U3CU3Ef__amU24cache5A_90; }
	inline DGetV_1_t2626940000 ** get_address_of_U3CU3Ef__amU24cache5A_90() { return &___U3CU3Ef__amU24cache5A_90; }
	inline void set_U3CU3Ef__amU24cache5A_90(DGetV_1_t2626940000 * value)
	{
		___U3CU3Ef__amU24cache5A_90 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5A_90, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5B_91() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache5B_91)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache5B_91() const { return ___U3CU3Ef__amU24cache5B_91; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache5B_91() { return &___U3CU3Ef__amU24cache5B_91; }
	inline void set_U3CU3Ef__amU24cache5B_91(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache5B_91 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5B_91, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5C_92() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache5C_92)); }
	inline DGetV_1_t2626940000 * get_U3CU3Ef__amU24cache5C_92() const { return ___U3CU3Ef__amU24cache5C_92; }
	inline DGetV_1_t2626940000 ** get_address_of_U3CU3Ef__amU24cache5C_92() { return &___U3CU3Ef__amU24cache5C_92; }
	inline void set_U3CU3Ef__amU24cache5C_92(DGetV_1_t2626940000 * value)
	{
		___U3CU3Ef__amU24cache5C_92 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5C_92, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5D_93() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache5D_93)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache5D_93() const { return ___U3CU3Ef__amU24cache5D_93; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache5D_93() { return &___U3CU3Ef__amU24cache5D_93; }
	inline void set_U3CU3Ef__amU24cache5D_93(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache5D_93 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5D_93, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5E_94() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache5E_94)); }
	inline DGetV_1_t2626940000 * get_U3CU3Ef__amU24cache5E_94() const { return ___U3CU3Ef__amU24cache5E_94; }
	inline DGetV_1_t2626940000 ** get_address_of_U3CU3Ef__amU24cache5E_94() { return &___U3CU3Ef__amU24cache5E_94; }
	inline void set_U3CU3Ef__amU24cache5E_94(DGetV_1_t2626940000 * value)
	{
		___U3CU3Ef__amU24cache5E_94 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5E_94, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5F_95() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache5F_95)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache5F_95() const { return ___U3CU3Ef__amU24cache5F_95; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache5F_95() { return &___U3CU3Ef__amU24cache5F_95; }
	inline void set_U3CU3Ef__amU24cache5F_95(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache5F_95 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5F_95, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache60_96() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache60_96)); }
	inline DGetV_1_t2626940000 * get_U3CU3Ef__amU24cache60_96() const { return ___U3CU3Ef__amU24cache60_96; }
	inline DGetV_1_t2626940000 ** get_address_of_U3CU3Ef__amU24cache60_96() { return &___U3CU3Ef__amU24cache60_96; }
	inline void set_U3CU3Ef__amU24cache60_96(DGetV_1_t2626940000 * value)
	{
		___U3CU3Ef__amU24cache60_96 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache60_96, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache61_97() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache61_97)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache61_97() const { return ___U3CU3Ef__amU24cache61_97; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache61_97() { return &___U3CU3Ef__amU24cache61_97; }
	inline void set_U3CU3Ef__amU24cache61_97(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache61_97 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache61_97, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache62_98() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache62_98)); }
	inline DGetV_1_t2626940000 * get_U3CU3Ef__amU24cache62_98() const { return ___U3CU3Ef__amU24cache62_98; }
	inline DGetV_1_t2626940000 ** get_address_of_U3CU3Ef__amU24cache62_98() { return &___U3CU3Ef__amU24cache62_98; }
	inline void set_U3CU3Ef__amU24cache62_98(DGetV_1_t2626940000 * value)
	{
		___U3CU3Ef__amU24cache62_98 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache62_98, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache63_99() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache63_99)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache63_99() const { return ___U3CU3Ef__amU24cache63_99; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache63_99() { return &___U3CU3Ef__amU24cache63_99; }
	inline void set_U3CU3Ef__amU24cache63_99(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache63_99 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache63_99, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache64_100() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache64_100)); }
	inline DGetV_1_t2626940000 * get_U3CU3Ef__amU24cache64_100() const { return ___U3CU3Ef__amU24cache64_100; }
	inline DGetV_1_t2626940000 ** get_address_of_U3CU3Ef__amU24cache64_100() { return &___U3CU3Ef__amU24cache64_100; }
	inline void set_U3CU3Ef__amU24cache64_100(DGetV_1_t2626940000 * value)
	{
		___U3CU3Ef__amU24cache64_100 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache64_100, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache65_101() { return static_cast<int32_t>(offsetof(UnityEngine_GUILayoutGenerated_t3776240178_StaticFields, ___U3CU3Ef__amU24cache65_101)); }
	inline DGetV_1_t2855056638 * get_U3CU3Ef__amU24cache65_101() const { return ___U3CU3Ef__amU24cache65_101; }
	inline DGetV_1_t2855056638 ** get_address_of_U3CU3Ef__amU24cache65_101() { return &___U3CU3Ef__amU24cache65_101; }
	inline void set_U3CU3Ef__amU24cache65_101(DGetV_1_t2855056638 * value)
	{
		___U3CU3Ef__amU24cache65_101 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache65_101, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
