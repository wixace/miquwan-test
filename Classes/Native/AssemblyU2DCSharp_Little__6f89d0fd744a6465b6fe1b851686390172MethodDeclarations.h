﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._6f89d0fd744a6465b6fe1b8590cbc70f
struct _6f89d0fd744a6465b6fe1b8590cbc70f_t1686390172;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__6f89d0fd744a6465b6fe1b851686390172.h"

// System.Void Little._6f89d0fd744a6465b6fe1b8590cbc70f::.ctor()
extern "C"  void _6f89d0fd744a6465b6fe1b8590cbc70f__ctor_m3556601873 (_6f89d0fd744a6465b6fe1b8590cbc70f_t1686390172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6f89d0fd744a6465b6fe1b8590cbc70f::_6f89d0fd744a6465b6fe1b8590cbc70fm2(System.Int32)
extern "C"  int32_t _6f89d0fd744a6465b6fe1b8590cbc70f__6f89d0fd744a6465b6fe1b8590cbc70fm2_m2079476761 (_6f89d0fd744a6465b6fe1b8590cbc70f_t1686390172 * __this, int32_t ____6f89d0fd744a6465b6fe1b8590cbc70fa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6f89d0fd744a6465b6fe1b8590cbc70f::_6f89d0fd744a6465b6fe1b8590cbc70fm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _6f89d0fd744a6465b6fe1b8590cbc70f__6f89d0fd744a6465b6fe1b8590cbc70fm_m1865493181 (_6f89d0fd744a6465b6fe1b8590cbc70f_t1686390172 * __this, int32_t ____6f89d0fd744a6465b6fe1b8590cbc70fa0, int32_t ____6f89d0fd744a6465b6fe1b8590cbc70f261, int32_t ____6f89d0fd744a6465b6fe1b8590cbc70fc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6f89d0fd744a6465b6fe1b8590cbc70f::ilo__6f89d0fd744a6465b6fe1b8590cbc70fm21(Little._6f89d0fd744a6465b6fe1b8590cbc70f,System.Int32)
extern "C"  int32_t _6f89d0fd744a6465b6fe1b8590cbc70f_ilo__6f89d0fd744a6465b6fe1b8590cbc70fm21_m1209299683 (Il2CppObject * __this /* static, unused */, _6f89d0fd744a6465b6fe1b8590cbc70f_t1686390172 * ____this0, int32_t ____6f89d0fd744a6465b6fe1b8590cbc70fa1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
