﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AnchoredJoint2D
struct AnchoredJoint2D_t1716788702;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine.AnchoredJoint2D::.ctor()
extern "C"  void AnchoredJoint2D__ctor_m1301123505 (AnchoredJoint2D_t1716788702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.AnchoredJoint2D::get_anchor()
extern "C"  Vector2_t4282066565  AnchoredJoint2D_get_anchor_m2611081238 (AnchoredJoint2D_t1716788702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnchoredJoint2D::set_anchor(UnityEngine.Vector2)
extern "C"  void AnchoredJoint2D_set_anchor_m174863125 (AnchoredJoint2D_t1716788702 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnchoredJoint2D::INTERNAL_get_anchor(UnityEngine.Vector2&)
extern "C"  void AnchoredJoint2D_INTERNAL_get_anchor_m374217409 (AnchoredJoint2D_t1716788702 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnchoredJoint2D::INTERNAL_set_anchor(UnityEngine.Vector2&)
extern "C"  void AnchoredJoint2D_INTERNAL_set_anchor_m2022775093 (AnchoredJoint2D_t1716788702 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.AnchoredJoint2D::get_connectedAnchor()
extern "C"  Vector2_t4282066565  AnchoredJoint2D_get_connectedAnchor_m3517513471 (AnchoredJoint2D_t1716788702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnchoredJoint2D::set_connectedAnchor(UnityEngine.Vector2)
extern "C"  void AnchoredJoint2D_set_connectedAnchor_m4183292490 (AnchoredJoint2D_t1716788702 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnchoredJoint2D::INTERNAL_get_connectedAnchor(UnityEngine.Vector2&)
extern "C"  void AnchoredJoint2D_INTERNAL_get_connectedAnchor_m3642006168 (AnchoredJoint2D_t1716788702 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnchoredJoint2D::INTERNAL_set_connectedAnchor(UnityEngine.Vector2&)
extern "C"  void AnchoredJoint2D_INTERNAL_set_connectedAnchor_m1888919204 (AnchoredJoint2D_t1716788702 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnchoredJoint2D::get_autoConfigureConnectedAnchor()
extern "C"  bool AnchoredJoint2D_get_autoConfigureConnectedAnchor_m1851824925 (AnchoredJoint2D_t1716788702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnchoredJoint2D::set_autoConfigureConnectedAnchor(System.Boolean)
extern "C"  void AnchoredJoint2D_set_autoConfigureConnectedAnchor_m2108494190 (AnchoredJoint2D_t1716788702 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
