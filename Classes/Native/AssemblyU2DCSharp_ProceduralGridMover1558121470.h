﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;
// Pathfinding.GridGraph
struct GridGraph_t2455707914;
// Pathfinding.GridNode[]
struct GridNodeU5BU5D_t2925197291;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProceduralGridMover
struct  ProceduralGridMover_t1558121470  : public MonoBehaviour_t667441552
{
public:
	// System.Single ProceduralGridMover::updateDistance
	float ___updateDistance_2;
	// UnityEngine.Transform ProceduralGridMover::target
	Transform_t1659122786 * ___target_3;
	// System.Boolean ProceduralGridMover::floodFill
	bool ___floodFill_4;
	// Pathfinding.GridGraph ProceduralGridMover::graph
	GridGraph_t2455707914 * ___graph_5;
	// Pathfinding.GridNode[] ProceduralGridMover::tmp
	GridNodeU5BU5D_t2925197291* ___tmp_6;

public:
	inline static int32_t get_offset_of_updateDistance_2() { return static_cast<int32_t>(offsetof(ProceduralGridMover_t1558121470, ___updateDistance_2)); }
	inline float get_updateDistance_2() const { return ___updateDistance_2; }
	inline float* get_address_of_updateDistance_2() { return &___updateDistance_2; }
	inline void set_updateDistance_2(float value)
	{
		___updateDistance_2 = value;
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(ProceduralGridMover_t1558121470, ___target_3)); }
	inline Transform_t1659122786 * get_target_3() const { return ___target_3; }
	inline Transform_t1659122786 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(Transform_t1659122786 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier(&___target_3, value);
	}

	inline static int32_t get_offset_of_floodFill_4() { return static_cast<int32_t>(offsetof(ProceduralGridMover_t1558121470, ___floodFill_4)); }
	inline bool get_floodFill_4() const { return ___floodFill_4; }
	inline bool* get_address_of_floodFill_4() { return &___floodFill_4; }
	inline void set_floodFill_4(bool value)
	{
		___floodFill_4 = value;
	}

	inline static int32_t get_offset_of_graph_5() { return static_cast<int32_t>(offsetof(ProceduralGridMover_t1558121470, ___graph_5)); }
	inline GridGraph_t2455707914 * get_graph_5() const { return ___graph_5; }
	inline GridGraph_t2455707914 ** get_address_of_graph_5() { return &___graph_5; }
	inline void set_graph_5(GridGraph_t2455707914 * value)
	{
		___graph_5 = value;
		Il2CppCodeGenWriteBarrier(&___graph_5, value);
	}

	inline static int32_t get_offset_of_tmp_6() { return static_cast<int32_t>(offsetof(ProceduralGridMover_t1558121470, ___tmp_6)); }
	inline GridNodeU5BU5D_t2925197291* get_tmp_6() const { return ___tmp_6; }
	inline GridNodeU5BU5D_t2925197291** get_address_of_tmp_6() { return &___tmp_6; }
	inline void set_tmp_6(GridNodeU5BU5D_t2925197291* value)
	{
		___tmp_6 = value;
		Il2CppCodeGenWriteBarrier(&___tmp_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
