﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_jearlairCeedrow40
struct M_jearlairCeedrow40_t1290834507;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_jearlairCeedrow401290834507.h"

// System.Void GarbageiOS.M_jearlairCeedrow40::.ctor()
extern "C"  void M_jearlairCeedrow40__ctor_m2845531000 (M_jearlairCeedrow40_t1290834507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jearlairCeedrow40::M_xaimirtraw0(System.String[],System.Int32)
extern "C"  void M_jearlairCeedrow40_M_xaimirtraw0_m499204993 (M_jearlairCeedrow40_t1290834507 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jearlairCeedrow40::M_roubesairCaidrurche1(System.String[],System.Int32)
extern "C"  void M_jearlairCeedrow40_M_roubesairCaidrurche1_m4208077354 (M_jearlairCeedrow40_t1290834507 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jearlairCeedrow40::M_booballmel2(System.String[],System.Int32)
extern "C"  void M_jearlairCeedrow40_M_booballmel2_m3675836250 (M_jearlairCeedrow40_t1290834507 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jearlairCeedrow40::M_drezise3(System.String[],System.Int32)
extern "C"  void M_jearlairCeedrow40_M_drezise3_m3562476016 (M_jearlairCeedrow40_t1290834507 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jearlairCeedrow40::ilo_M_xaimirtraw01(GarbageiOS.M_jearlairCeedrow40,System.String[],System.Int32)
extern "C"  void M_jearlairCeedrow40_ilo_M_xaimirtraw01_m370370116 (Il2CppObject * __this /* static, unused */, M_jearlairCeedrow40_t1290834507 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jearlairCeedrow40::ilo_M_roubesairCaidrurche12(GarbageiOS.M_jearlairCeedrow40,System.String[],System.Int32)
extern "C"  void M_jearlairCeedrow40_ilo_M_roubesairCaidrurche12_m1130664018 (Il2CppObject * __this /* static, unused */, M_jearlairCeedrow40_t1290834507 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
