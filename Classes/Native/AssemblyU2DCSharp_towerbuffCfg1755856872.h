﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// towerbuffCfg
struct  towerbuffCfg_t1755856872  : public CsCfgBase_t69924517
{
public:
	// System.Int32 towerbuffCfg::id
	int32_t ___id_0;
	// System.String towerbuffCfg::buff_name
	String_t* ___buff_name_1;
	// System.String towerbuffCfg::icon
	String_t* ___icon_2;
	// System.String towerbuffCfg::buff1
	String_t* ___buff1_3;
	// System.Int32 towerbuffCfg::value1
	int32_t ___value1_4;
	// System.Int32 towerbuffCfg::fixed_value1
	int32_t ___fixed_value1_5;
	// System.String towerbuffCfg::buff2
	String_t* ___buff2_6;
	// System.Int32 towerbuffCfg::value2
	int32_t ___value2_7;
	// System.Int32 towerbuffCfg::fixed_value2
	int32_t ___fixed_value2_8;
	// System.Int32 towerbuffCfg::cost
	int32_t ___cost_9;
	// System.Int32 towerbuffCfg::level
	int32_t ___level_10;
	// System.Int32 towerbuffCfg::sequence
	int32_t ___sequence_11;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(towerbuffCfg_t1755856872, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_buff_name_1() { return static_cast<int32_t>(offsetof(towerbuffCfg_t1755856872, ___buff_name_1)); }
	inline String_t* get_buff_name_1() const { return ___buff_name_1; }
	inline String_t** get_address_of_buff_name_1() { return &___buff_name_1; }
	inline void set_buff_name_1(String_t* value)
	{
		___buff_name_1 = value;
		Il2CppCodeGenWriteBarrier(&___buff_name_1, value);
	}

	inline static int32_t get_offset_of_icon_2() { return static_cast<int32_t>(offsetof(towerbuffCfg_t1755856872, ___icon_2)); }
	inline String_t* get_icon_2() const { return ___icon_2; }
	inline String_t** get_address_of_icon_2() { return &___icon_2; }
	inline void set_icon_2(String_t* value)
	{
		___icon_2 = value;
		Il2CppCodeGenWriteBarrier(&___icon_2, value);
	}

	inline static int32_t get_offset_of_buff1_3() { return static_cast<int32_t>(offsetof(towerbuffCfg_t1755856872, ___buff1_3)); }
	inline String_t* get_buff1_3() const { return ___buff1_3; }
	inline String_t** get_address_of_buff1_3() { return &___buff1_3; }
	inline void set_buff1_3(String_t* value)
	{
		___buff1_3 = value;
		Il2CppCodeGenWriteBarrier(&___buff1_3, value);
	}

	inline static int32_t get_offset_of_value1_4() { return static_cast<int32_t>(offsetof(towerbuffCfg_t1755856872, ___value1_4)); }
	inline int32_t get_value1_4() const { return ___value1_4; }
	inline int32_t* get_address_of_value1_4() { return &___value1_4; }
	inline void set_value1_4(int32_t value)
	{
		___value1_4 = value;
	}

	inline static int32_t get_offset_of_fixed_value1_5() { return static_cast<int32_t>(offsetof(towerbuffCfg_t1755856872, ___fixed_value1_5)); }
	inline int32_t get_fixed_value1_5() const { return ___fixed_value1_5; }
	inline int32_t* get_address_of_fixed_value1_5() { return &___fixed_value1_5; }
	inline void set_fixed_value1_5(int32_t value)
	{
		___fixed_value1_5 = value;
	}

	inline static int32_t get_offset_of_buff2_6() { return static_cast<int32_t>(offsetof(towerbuffCfg_t1755856872, ___buff2_6)); }
	inline String_t* get_buff2_6() const { return ___buff2_6; }
	inline String_t** get_address_of_buff2_6() { return &___buff2_6; }
	inline void set_buff2_6(String_t* value)
	{
		___buff2_6 = value;
		Il2CppCodeGenWriteBarrier(&___buff2_6, value);
	}

	inline static int32_t get_offset_of_value2_7() { return static_cast<int32_t>(offsetof(towerbuffCfg_t1755856872, ___value2_7)); }
	inline int32_t get_value2_7() const { return ___value2_7; }
	inline int32_t* get_address_of_value2_7() { return &___value2_7; }
	inline void set_value2_7(int32_t value)
	{
		___value2_7 = value;
	}

	inline static int32_t get_offset_of_fixed_value2_8() { return static_cast<int32_t>(offsetof(towerbuffCfg_t1755856872, ___fixed_value2_8)); }
	inline int32_t get_fixed_value2_8() const { return ___fixed_value2_8; }
	inline int32_t* get_address_of_fixed_value2_8() { return &___fixed_value2_8; }
	inline void set_fixed_value2_8(int32_t value)
	{
		___fixed_value2_8 = value;
	}

	inline static int32_t get_offset_of_cost_9() { return static_cast<int32_t>(offsetof(towerbuffCfg_t1755856872, ___cost_9)); }
	inline int32_t get_cost_9() const { return ___cost_9; }
	inline int32_t* get_address_of_cost_9() { return &___cost_9; }
	inline void set_cost_9(int32_t value)
	{
		___cost_9 = value;
	}

	inline static int32_t get_offset_of_level_10() { return static_cast<int32_t>(offsetof(towerbuffCfg_t1755856872, ___level_10)); }
	inline int32_t get_level_10() const { return ___level_10; }
	inline int32_t* get_address_of_level_10() { return &___level_10; }
	inline void set_level_10(int32_t value)
	{
		___level_10 = value;
	}

	inline static int32_t get_offset_of_sequence_11() { return static_cast<int32_t>(offsetof(towerbuffCfg_t1755856872, ___sequence_11)); }
	inline int32_t get_sequence_11() const { return ___sequence_11; }
	inline int32_t* get_address_of_sequence_11() { return &___sequence_11; }
	inline void set_sequence_11(int32_t value)
	{
		___sequence_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
