﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TypewriterEffect
struct TypewriterEffect_t2948707390;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t1077642479;
// UIScrollView
struct UIScrollView_t2113479878;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TypewriterEffect2948707390.h"
#include "AssemblyU2DCSharp_UIScrollView2113479878.h"
#include "mscorlib_System_String7231557.h"

// System.Void TypewriterEffect::.ctor()
extern "C"  void TypewriterEffect__ctor_m789010461 (TypewriterEffect_t2948707390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TypewriterEffect::get_isActive()
extern "C"  bool TypewriterEffect_get_isActive_m2614901620 (TypewriterEffect_t2948707390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TypewriterEffect::ResetToBeginning()
extern "C"  void TypewriterEffect_ResetToBeginning_m680152986 (TypewriterEffect_t2948707390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TypewriterEffect::Finish()
extern "C"  void TypewriterEffect_Finish_m2715092954 (TypewriterEffect_t2948707390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TypewriterEffect::OnEnable()
extern "C"  void TypewriterEffect_OnEnable_m1126211145 (TypewriterEffect_t2948707390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TypewriterEffect::Update()
extern "C"  void TypewriterEffect_Update_m416382608 (TypewriterEffect_t2948707390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TypewriterEffect::OnDestory()
extern "C"  void TypewriterEffect_OnDestory_m3168595248 (TypewriterEffect_t2948707390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TypewriterEffect::ilo_Update1(TypewriterEffect)
extern "C"  void TypewriterEffect_ilo_Update1_m3267578098 (Il2CppObject * __this /* static, unused */, TypewriterEffect_t2948707390 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TypewriterEffect::ilo_Execute2(System.Collections.Generic.List`1<EventDelegate>)
extern "C"  void TypewriterEffect_ilo_Execute2_m2349750778 (Il2CppObject * __this /* static, unused */, List_1_t1077642479 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TypewriterEffect::ilo_UpdatePosition3(UIScrollView)
extern "C"  void TypewriterEffect_ilo_UpdatePosition3_m923517699 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TypewriterEffect::ilo_ParseSymbol4(System.String,System.Int32&)
extern "C"  bool TypewriterEffect_ilo_ParseSymbol4_m3161369504 (Il2CppObject * __this /* static, unused */, String_t* ___text0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TypewriterEffect::ilo_get_time5()
extern "C"  float TypewriterEffect_ilo_get_time5_m292000027 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
