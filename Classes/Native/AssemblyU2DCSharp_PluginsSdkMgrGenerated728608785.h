﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// JSDataExchangeMgr/DGetV`1<System.Action>
struct DGetV_1_t3648885239;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginsSdkMgrGenerated
struct  PluginsSdkMgrGenerated_t728608785  : public Il2CppObject
{
public:

public:
};

struct PluginsSdkMgrGenerated_t728608785_StaticFields
{
public:
	// JSDataExchangeMgr/DGetV`1<System.Action> PluginsSdkMgrGenerated::<>f__am$cache0
	DGetV_1_t3648885239 * ___U3CU3Ef__amU24cache0_0;
	// JSDataExchangeMgr/DGetV`1<System.Action> PluginsSdkMgrGenerated::<>f__am$cache1
	DGetV_1_t3648885239 * ___U3CU3Ef__amU24cache1_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(PluginsSdkMgrGenerated_t728608785_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline DGetV_1_t3648885239 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline DGetV_1_t3648885239 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(DGetV_1_t3648885239 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(PluginsSdkMgrGenerated_t728608785_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline DGetV_1_t3648885239 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline DGetV_1_t3648885239 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(DGetV_1_t3648885239 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
