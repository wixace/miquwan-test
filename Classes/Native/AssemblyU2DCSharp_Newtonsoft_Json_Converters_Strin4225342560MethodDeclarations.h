﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.StringEnumConverter
struct StringEnumConverter_t4225342560;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.Type
struct Type_t;
// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>
struct BidirectionalDictionary_2_t157076046;
// System.String
struct String_t;
// System.Runtime.Serialization.EnumMemberAttribute
struct EnumMemberAttribute_t1202205191;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer251850770.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_System_Runtime_Serialization_Enu1202205191.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_Strin4225342560.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken4173078175.h"

// System.Void Newtonsoft.Json.Converters.StringEnumConverter::.ctor()
extern "C"  void StringEnumConverter__ctor_m2012221811 (StringEnumConverter_t4225342560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.StringEnumConverter::get_CamelCaseText()
extern "C"  bool StringEnumConverter_get_CamelCaseText_m2928640775 (StringEnumConverter_t4225342560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.StringEnumConverter::set_CamelCaseText(System.Boolean)
extern "C"  void StringEnumConverter_set_CamelCaseText_m861618302 (StringEnumConverter_t4225342560 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.StringEnumConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  void StringEnumConverter_WriteJson_m1726638081 (StringEnumConverter_t4225342560 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonSerializer_t251850770 * ___serializer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Converters.StringEnumConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  Il2CppObject * StringEnumConverter_ReadJson_m2865608882 (StringEnumConverter_t4225342560 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, Il2CppObject * ___existingValue2, JsonSerializer_t251850770 * ___serializer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String> Newtonsoft.Json.Converters.StringEnumConverter::GetEnumNameMap(System.Type)
extern "C"  BidirectionalDictionary_2_t157076046 * StringEnumConverter_GetEnumNameMap_m2731751909 (StringEnumConverter_t4225342560 * __this, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.StringEnumConverter::CanConvert(System.Type)
extern "C"  bool StringEnumConverter_CanConvert_m378992977 (StringEnumConverter_t4225342560 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.StringEnumConverter::<GetEnumNameMap>m__367(System.Runtime.Serialization.EnumMemberAttribute)
extern "C"  String_t* StringEnumConverter_U3CGetEnumNameMapU3Em__367_m1701273525 (Il2CppObject * __this /* static, unused */, EnumMemberAttribute_t1202205191 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String> Newtonsoft.Json.Converters.StringEnumConverter::ilo_GetEnumNameMap1(Newtonsoft.Json.Converters.StringEnumConverter,System.Type)
extern "C"  BidirectionalDictionary_2_t157076046 * StringEnumConverter_ilo_GetEnumNameMap1_m1644498615 (Il2CppObject * __this /* static, unused */, StringEnumConverter_t4225342560 * ____this0, Type_t * ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.StringEnumConverter::ilo_WriteValue2(Newtonsoft.Json.JsonWriter,System.String)
extern "C"  void StringEnumConverter_ilo_WriteValue2_m1431937930 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonToken Newtonsoft.Json.Converters.StringEnumConverter::ilo_get_TokenType3(Newtonsoft.Json.JsonReader)
extern "C"  int32_t StringEnumConverter_ilo_get_TokenType3_m869025572 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.StringEnumConverter::ilo_FormatWith4(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* StringEnumConverter_ilo_FormatWith4_m1772968784 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
