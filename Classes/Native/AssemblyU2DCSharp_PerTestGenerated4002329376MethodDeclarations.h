﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PerTestGenerated
struct PerTestGenerated_t4002329376;
// JSVCall
struct JSVCall_t3708497963;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void PerTestGenerated::.ctor()
extern "C"  void PerTestGenerated__ctor_m1557002107 (PerTestGenerated_t4002329376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PerTestGenerated::PerTest_PerTest1(JSVCall,System.Int32)
extern "C"  bool PerTestGenerated_PerTest_PerTest1_m4018055551 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PerTestGenerated::PerTest_test(JSVCall)
extern "C"  void PerTestGenerated_PerTest_test_m940640732 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PerTestGenerated::__Register()
extern "C"  void PerTestGenerated___Register_m1339685420 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] PerTestGenerated::<PerTest_test>m__8D()
extern "C"  Int32U5BU5D_t3230847821* PerTestGenerated_U3CPerTest_testU3Em__8D_m1370694726 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PerTestGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool PerTestGenerated_ilo_attachFinalizerObject1_m4132330436 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PerTestGenerated::ilo_setInt322(System.Int32,System.Int32)
extern "C"  void PerTestGenerated_ilo_setInt322_m1641070362 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PerTestGenerated::ilo_getObject3(System.Int32)
extern "C"  int32_t PerTestGenerated_ilo_getObject3_m1506608889 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
