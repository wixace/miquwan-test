﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayMgr
struct ReplayMgr_t1549183121;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// CombatEntity
struct CombatEntity_t684137495;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.List`1<CSHeroUnit>
struct List_1_t837576702;
// CSOtherPlayer
struct CSOtherPlayer_t3817567105;
// ReplayOtherPlayer
struct ReplayOtherPlayer_t1774842762;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// CSGameDataMgr
struct CSGameDataMgr_t2623305516;
// ReplayData
struct ReplayData_t779762769;
// System.Action
struct Action_t3771233898;
// IZUpdate
struct IZUpdate_t3482043738;
// EffectMgr
struct EffectMgr_t535289511;
// ReplayBase
struct ReplayBase_t779703160;
// CSHeroData
struct CSHeroData_t3763839828;
// CSHeroUnit
struct CSHeroUnit_t3764358446;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ReplayExecuteType3106517448.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_ReplayMgr1549183121.h"
#include "AssemblyU2DCSharp_ReplayData779762769.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_EffectMgr535289511.h"
#include "AssemblyU2DCSharp_ReplayBase779703160.h"
#include "AssemblyU2DCSharp_CSHeroData3763839828.h"
#include "AssemblyU2DCSharp_CSHeroUnit3764358446.h"

// System.Void ReplayMgr::.ctor()
extern "C"  void ReplayMgr__ctor_m828014586 (ReplayMgr_t1549183121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReplayMgr ReplayMgr::get_Instance()
extern "C"  ReplayMgr_t1549183121 * ReplayMgr_get_Instance_m3592156998 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayMgr::get_IsPlayREC()
extern "C"  bool ReplayMgr_get_IsPlayREC_m1265244181 (ReplayMgr_t1549183121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::Init()
extern "C"  void ReplayMgr_Init_m1225172762 (ReplayMgr_t1549183121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::GetReplayData(CEvent.ZEvent)
extern "C"  void ReplayMgr_GetReplayData_m166007388 (ReplayMgr_t1549183121 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::StartRecording()
extern "C"  void ReplayMgr_StartRecording_m2866136761 (ReplayMgr_t1549183121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::StartREC()
extern "C"  void ReplayMgr_StartREC_m1753162456 (ReplayMgr_t1549183121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::ResetREC()
extern "C"  void ReplayMgr_ResetREC_m1649890827 (ReplayMgr_t1549183121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::ExitREC()
extern "C"  void ReplayMgr_ExitREC_m3712443754 (ReplayMgr_t1549183121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::EndREC(System.Boolean)
extern "C"  void ReplayMgr_EndREC_m2824121206 (ReplayMgr_t1549183121 * __this, bool ___isWin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::zipCompress()
extern "C"  void ReplayMgr_zipCompress_m2315165915 (ReplayMgr_t1549183121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::ClearEnd()
extern "C"  void ReplayMgr_ClearEnd_m2582747480 (ReplayMgr_t1549183121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::PlayREC(System.String)
extern "C"  void ReplayMgr_PlayREC_m45387374 (ReplayMgr_t1549183121 * __this, String_t* ___jsonDatas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::PlayEndERC()
extern "C"  void ReplayMgr_PlayEndERC_m2161342041 (ReplayMgr_t1549183121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::AddData(ReplayExecuteType,System.Object[])
extern "C"  void ReplayMgr_AddData_m671653555 (ReplayMgr_t1549183121 * __this, int32_t ___type0, ObjectU5BU5D_t1108656482* ___datas1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::AddData(ReplayExecuteType,CombatEntity,System.Object[])
extern "C"  void ReplayMgr_AddData_m456179540 (ReplayMgr_t1549183121 * __this, int32_t ___type0, CombatEntity_t684137495 * ___entity1, ObjectU5BU5D_t1108656482* ___datas2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::ZUpdate()
extern "C"  void ReplayMgr_ZUpdate_m721596539 (ReplayMgr_t1549183121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayMgr::GetMineName()
extern "C"  String_t* ReplayMgr_GetMineName_m2645129967 (ReplayMgr_t1549183121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ReplayMgr::GetMinePower()
extern "C"  float ReplayMgr_GetMinePower_m170717306 (ReplayMgr_t1549183121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayMgr::GetEnemyName()
extern "C"  String_t* ReplayMgr_GetEnemyName_m807044 (ReplayMgr_t1549183121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ReplayMgr::GetEnemyPower()
extern "C"  float ReplayMgr_GetEnemyPower_m543883639 (ReplayMgr_t1549183121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> ReplayMgr::GetMineHeroIDList()
extern "C"  List_1_t2522024052 * ReplayMgr_GetMineHeroIDList_m3709881398 (ReplayMgr_t1549183121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> ReplayMgr::GetEnemyHeroIDList()
extern "C"  List_1_t2522024052 * ReplayMgr_GetEnemyHeroIDList_m3576866669 (ReplayMgr_t1549183121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSHeroUnit> ReplayMgr::GetMineCSHeroUnits()
extern "C"  List_1_t837576702 * ReplayMgr_GetMineCSHeroUnits_m1059947653 (ReplayMgr_t1549183121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSHeroUnit> ReplayMgr::GetEnemyCSHeroUnits()
extern "C"  List_1_t837576702 * ReplayMgr_GetEnemyCSHeroUnits_m3904236364 (ReplayMgr_t1549183121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSOtherPlayer ReplayMgr::GetMineCSPlayerInfo()
extern "C"  CSOtherPlayer_t3817567105 * ReplayMgr_GetMineCSPlayerInfo_m2605657938 (ReplayMgr_t1549183121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReplayOtherPlayer ReplayMgr::GetMinePlayerInfo()
extern "C"  ReplayOtherPlayer_t1774842762 * ReplayMgr_GetMinePlayerInfo_m1016452281 (ReplayMgr_t1549183121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::CombatHurt(CEvent.ZEvent)
extern "C"  void ReplayMgr_CombatHurt_m3904172526 (ReplayMgr_t1549183121 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::<zipCompress>m__3C9()
extern "C"  void ReplayMgr_U3CzipCompressU3Em__3C9_m2359899083 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::ilo_Init1(ReplayMgr)
extern "C"  void ReplayMgr_ilo_Init1_m2382639515 (Il2CppObject * __this /* static, unused */, ReplayMgr_t1549183121 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr ReplayMgr::ilo_get_FloatTextMgr2()
extern "C"  FloatTextMgr_t630384591 * ReplayMgr_ilo_get_FloatTextMgr2_m3703340429 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayMgr::ilo_ZipDecompress3(System.Byte[])
extern "C"  String_t* ReplayMgr_ilo_ZipDecompress3_m1687349856 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::ilo_PlayREC4(ReplayMgr,System.String)
extern "C"  void ReplayMgr_ilo_PlayREC4_m2910723624 (Il2CppObject * __this /* static, unused */, ReplayMgr_t1549183121 * ____this0, String_t* ___jsonDatas1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSGameDataMgr ReplayMgr::ilo_get_CSGameDataMgr5()
extern "C"  CSGameDataMgr_t2623305516 * ReplayMgr_ilo_get_CSGameDataMgr5_m871705064 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayMgr::ilo_ToJson6(ReplayData)
extern "C"  String_t* ReplayMgr_ilo_ToJson6_m3078435146 (Il2CppObject * __this /* static, unused */, ReplayData_t779762769 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::ilo_ClearEnd7(ReplayMgr)
extern "C"  void ReplayMgr_ilo_ClearEnd7_m3280649491 (Il2CppObject * __this /* static, unused */, ReplayMgr_t1549183121 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::ilo_Wait8(System.Action)
extern "C"  void ReplayMgr_ilo_Wait8_m2815001045 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___waitCallBack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::ilo_RemoveUpdate9(IZUpdate)
extern "C"  void ReplayMgr_ilo_RemoveUpdate9_m3256489559 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::ilo_Clear10(EffectMgr)
extern "C"  void ReplayMgr_ilo_Clear10_m1961773658 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::ilo_AddData11(ReplayMgr,ReplayExecuteType,CombatEntity,System.Object[])
extern "C"  void ReplayMgr_ilo_AddData11_m640837318 (Il2CppObject * __this /* static, unused */, ReplayMgr_t1549183121 * ____this0, int32_t ___type1, CombatEntity_t684137495 * ___entity2, ObjectU5BU5D_t1108656482* ___datas3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::ilo_AddReplayBase12(ReplayData,System.String,ReplayBase)
extern "C"  void ReplayMgr_ilo_AddReplayBase12_m571966532 (Il2CppObject * __this /* static, unused */, ReplayData_t779762769 * ____this0, String_t* ___key1, ReplayBase_t779703160 * ___item2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgr::ilo_Execute13(ReplayBase)
extern "C"  void ReplayMgr_ilo_Execute13_m2767078500 (Il2CppObject * __this /* static, unused */, ReplayBase_t779703160 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReplayOtherPlayer ReplayMgr::ilo_GetMine14(ReplayData)
extern "C"  ReplayOtherPlayer_t1774842762 * ReplayMgr_ilo_GetMine14_m764618761 (Il2CppObject * __this /* static, unused */, ReplayData_t779762769 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReplayOtherPlayer ReplayMgr::ilo_GetEnemy15(ReplayData)
extern "C"  ReplayOtherPlayer_t1774842762 * ReplayMgr_ilo_GetEnemy15_m1030022233 (Il2CppObject * __this /* static, unused */, ReplayData_t779762769 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSHeroUnit> ReplayMgr::ilo_GetFightingHeroUntis16(CSHeroData,System.Int32)
extern "C"  List_1_t837576702 * ReplayMgr_ilo_GetFightingHeroUntis16_m2423834823 (Il2CppObject * __this /* static, unused */, CSHeroData_t3763839828 * ____this0, int32_t ___fuctionType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSHeroUnit> ReplayMgr::ilo_GetFightingHeroUntis217(CSHeroData)
extern "C"  List_1_t837576702 * ReplayMgr_ilo_GetFightingHeroUntis217_m3533335175 (Il2CppObject * __this /* static, unused */, CSHeroData_t3763839828 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ReplayMgr::ilo_get_lv18(CSHeroUnit)
extern "C"  uint32_t ReplayMgr_ilo_get_lv18_m424913518 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
