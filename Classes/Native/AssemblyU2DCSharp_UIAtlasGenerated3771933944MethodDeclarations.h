﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIAtlasGenerated
struct UIAtlasGenerated_t3771933944;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// UIAtlas
struct UIAtlas_t281921111;
// UnityEngine.Material
struct Material_t3870600107;
// System.Collections.Generic.List`1<UISpriteData>
struct List_1_t651564179;
// BetterList`1<System.String>
struct BetterList_1_t1504199569;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_UIAtlas281921111.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"

// System.Void UIAtlasGenerated::.ctor()
extern "C"  void UIAtlasGenerated__ctor_m1072699555 (UIAtlasGenerated_t3771933944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIAtlasGenerated::UIAtlas_UIAtlas1(JSVCall,System.Int32)
extern "C"  bool UIAtlasGenerated_UIAtlas_UIAtlas1_m2750528263 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAtlasGenerated::UIAtlas_spriteMaterial(JSVCall)
extern "C"  void UIAtlasGenerated_UIAtlas_spriteMaterial_m1715559154 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAtlasGenerated::UIAtlas_premultipliedAlpha(JSVCall)
extern "C"  void UIAtlasGenerated_UIAtlas_premultipliedAlpha_m3917262614 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAtlasGenerated::UIAtlas_spriteList(JSVCall)
extern "C"  void UIAtlasGenerated_UIAtlas_spriteList_m1467963099 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAtlasGenerated::UIAtlas_texture(JSVCall)
extern "C"  void UIAtlasGenerated_UIAtlas_texture_m2669156051 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAtlasGenerated::UIAtlas_pixelSize(JSVCall)
extern "C"  void UIAtlasGenerated_UIAtlas_pixelSize_m1515926471 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAtlasGenerated::UIAtlas_replacement(JSVCall)
extern "C"  void UIAtlasGenerated_UIAtlas_replacement_m1598193596 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIAtlasGenerated::UIAtlas_GetListOfSprites__String(JSVCall,System.Int32)
extern "C"  bool UIAtlasGenerated_UIAtlas_GetListOfSprites__String_m2002486721 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIAtlasGenerated::UIAtlas_GetListOfSprites(JSVCall,System.Int32)
extern "C"  bool UIAtlasGenerated_UIAtlas_GetListOfSprites_m4194514800 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIAtlasGenerated::UIAtlas_GetRandomSprite__String(JSVCall,System.Int32)
extern "C"  bool UIAtlasGenerated_UIAtlas_GetRandomSprite__String_m1134625124 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIAtlasGenerated::UIAtlas_GetSprite__String(JSVCall,System.Int32)
extern "C"  bool UIAtlasGenerated_UIAtlas_GetSprite__String_m2599700865 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIAtlasGenerated::UIAtlas_MarkAsChanged(JSVCall,System.Int32)
extern "C"  bool UIAtlasGenerated_UIAtlas_MarkAsChanged_m697095626 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIAtlasGenerated::UIAtlas_MarkSpriteListAsChanged(JSVCall,System.Int32)
extern "C"  bool UIAtlasGenerated_UIAtlas_MarkSpriteListAsChanged_m3410280327 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIAtlasGenerated::UIAtlas_SortAlphabetically(JSVCall,System.Int32)
extern "C"  bool UIAtlasGenerated_UIAtlas_SortAlphabetically_m4114954032 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIAtlasGenerated::UIAtlas_CheckIfRelated__UIAtlas__UIAtlas(JSVCall,System.Int32)
extern "C"  bool UIAtlasGenerated_UIAtlas_CheckIfRelated__UIAtlas__UIAtlas_m2416322035 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAtlasGenerated::__Register()
extern "C"  void UIAtlasGenerated___Register_m2787529220 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIAtlasGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UIAtlasGenerated_ilo_attachFinalizerObject1_m615186332 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIAtlasGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIAtlasGenerated_ilo_getObject2_m2321885165 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAtlasGenerated::ilo_set_spriteMaterial3(UIAtlas,UnityEngine.Material)
extern "C"  void UIAtlasGenerated_ilo_set_spriteMaterial3_m2680320549 (Il2CppObject * __this /* static, unused */, UIAtlas_t281921111 * ____this0, Material_t3870600107 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAtlasGenerated::ilo_set_spriteList4(UIAtlas,System.Collections.Generic.List`1<UISpriteData>)
extern "C"  void UIAtlasGenerated_ilo_set_spriteList4_m2155222760 (Il2CppObject * __this /* static, unused */, UIAtlas_t281921111 * ____this0, List_1_t651564179 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIAtlasGenerated::ilo_getSingle5(System.Int32)
extern "C"  float UIAtlasGenerated_ilo_getSingle5_m993471848 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BetterList`1<System.String> UIAtlasGenerated::ilo_GetListOfSprites6(UIAtlas)
extern "C"  BetterList_1_t1504199569 * UIAtlasGenerated_ilo_GetListOfSprites6_m3511216706 (Il2CppObject * __this /* static, unused */, UIAtlas_t281921111 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIAtlasGenerated::ilo_getStringS7(System.Int32)
extern "C"  String_t* UIAtlasGenerated_ilo_getStringS7_m2892999335 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAtlasGenerated::ilo_MarkSpriteListAsChanged8(UIAtlas)
extern "C"  void UIAtlasGenerated_ilo_MarkSpriteListAsChanged8_m1647451745 (Il2CppObject * __this /* static, unused */, UIAtlas_t281921111 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAtlasGenerated::ilo_setBooleanS9(System.Int32,System.Boolean)
extern "C"  void UIAtlasGenerated_ilo_setBooleanS9_m879773132 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
