﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MUIWrapContentGenerated
struct MUIWrapContentGenerated_t59900609;
// JSVCall
struct JSVCall_t3708497963;
// MUIWrapContent/OnInitializeItem
struct OnInitializeItem_t4000839715;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// MUIWrapContent
struct MUIWrapContent_t4054684270;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_MUIWrapContent4054684270.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void MUIWrapContentGenerated::.ctor()
extern "C"  void MUIWrapContentGenerated__ctor_m1425860042 (MUIWrapContentGenerated_t59900609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MUIWrapContentGenerated::MUIWrapContent_MUIWrapContent1(JSVCall,System.Int32)
extern "C"  bool MUIWrapContentGenerated_MUIWrapContent_MUIWrapContent1_m856168040 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContentGenerated::MUIWrapContent_cellWidth(JSVCall)
extern "C"  void MUIWrapContentGenerated_MUIWrapContent_cellWidth_m3904476474 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContentGenerated::MUIWrapContent_cellHeight(JSVCall)
extern "C"  void MUIWrapContentGenerated_MUIWrapContent_cellHeight_m1435130053 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContentGenerated::MUIWrapContent_cullContent(JSVCall)
extern "C"  void MUIWrapContentGenerated_MUIWrapContent_cullContent_m4035231095 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContentGenerated::MUIWrapContent_minIndex(JSVCall)
extern "C"  void MUIWrapContentGenerated_MUIWrapContent_minIndex_m1674715182 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContentGenerated::MUIWrapContent_LieShu(JSVCall)
extern "C"  void MUIWrapContentGenerated_MUIWrapContent_LieShu_m1451405718 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContentGenerated::MUIWrapContent_maxIndex(JSVCall)
extern "C"  void MUIWrapContentGenerated_MUIWrapContent_maxIndex_m3654637504 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MUIWrapContent/OnInitializeItem MUIWrapContentGenerated::MUIWrapContent_onInitializeItem_GetDelegate_member6_arg0(CSRepresentedObject)
extern "C"  OnInitializeItem_t4000839715 * MUIWrapContentGenerated_MUIWrapContent_onInitializeItem_GetDelegate_member6_arg0_m5323109 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContentGenerated::MUIWrapContent_onInitializeItem(JSVCall)
extern "C"  void MUIWrapContentGenerated_MUIWrapContent_onInitializeItem_m4275261836 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MUIWrapContentGenerated::MUIWrapContent_OnStart(JSVCall,System.Int32)
extern "C"  bool MUIWrapContentGenerated_MUIWrapContent_OnStart_m412387464 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MUIWrapContentGenerated::MUIWrapContent_RefreshChildData(JSVCall,System.Int32)
extern "C"  bool MUIWrapContentGenerated_MUIWrapContent_RefreshChildData_m21468584 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MUIWrapContentGenerated::MUIWrapContent_RefreshItem__Transform__Int32(JSVCall,System.Int32)
extern "C"  bool MUIWrapContentGenerated_MUIWrapContent_RefreshItem__Transform__Int32_m3143399093 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MUIWrapContentGenerated::MUIWrapContent_ResetAllItem__Boolean(JSVCall,System.Int32)
extern "C"  bool MUIWrapContentGenerated_MUIWrapContent_ResetAllItem__Boolean_m3695550856 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MUIWrapContentGenerated::MUIWrapContent_ResetAllItemAndResetPositions(JSVCall,System.Int32)
extern "C"  bool MUIWrapContentGenerated_MUIWrapContent_ResetAllItemAndResetPositions_m2489166930 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MUIWrapContentGenerated::MUIWrapContent_ResetAllItemDown(JSVCall,System.Int32)
extern "C"  bool MUIWrapContentGenerated_MUIWrapContent_ResetAllItemDown_m1024763044 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MUIWrapContentGenerated::MUIWrapContent_SortAlphabetically(JSVCall,System.Int32)
extern "C"  bool MUIWrapContentGenerated_MUIWrapContent_SortAlphabetically_m577171168 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MUIWrapContentGenerated::MUIWrapContent_SortBasedOnScrollMovement(JSVCall,System.Int32)
extern "C"  bool MUIWrapContentGenerated_MUIWrapContent_SortBasedOnScrollMovement_m2157816629 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MUIWrapContentGenerated::MUIWrapContent_TopByIndex__Int32__Int32__Int32__UIPanel(JSVCall,System.Int32)
extern "C"  bool MUIWrapContentGenerated_MUIWrapContent_TopByIndex__Int32__Int32__Int32__UIPanel_m1642754597 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MUIWrapContentGenerated::MUIWrapContent_WrapChildPosWithRealIndex__Int32__UIScrollView(JSVCall,System.Int32)
extern "C"  bool MUIWrapContentGenerated_MUIWrapContent_WrapChildPosWithRealIndex__Int32__UIScrollView_m1926665221 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MUIWrapContentGenerated::MUIWrapContent_WrapContent(JSVCall,System.Int32)
extern "C"  bool MUIWrapContentGenerated_MUIWrapContent_WrapContent_m26196116 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MUIWrapContentGenerated::MUIWrapContent_zhiDing__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool MUIWrapContentGenerated_MUIWrapContent_zhiDing__Int32__Int32_m1912049278 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContentGenerated::__Register()
extern "C"  void MUIWrapContentGenerated___Register_m3969819965 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MUIWrapContent/OnInitializeItem MUIWrapContentGenerated::<MUIWrapContent_onInitializeItem>m__77()
extern "C"  OnInitializeItem_t4000839715 * MUIWrapContentGenerated_U3CMUIWrapContent_onInitializeItemU3Em__77_m3464370800 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MUIWrapContentGenerated::ilo_getBooleanS1(System.Int32)
extern "C"  bool MUIWrapContentGenerated_ilo_getBooleanS1_m828657626 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContentGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void MUIWrapContentGenerated_ilo_setSingle2_m1523217131 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MUIWrapContentGenerated::ilo_getInt323(System.Int32)
extern "C"  int32_t MUIWrapContentGenerated_ilo_getInt323_m1444720623 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContentGenerated::ilo_OnStart4(MUIWrapContent)
extern "C"  void MUIWrapContentGenerated_ilo_OnStart4_m3421653200 (Il2CppObject * __this /* static, unused */, MUIWrapContent_t4054684270 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContentGenerated::ilo_RefreshChildData5(MUIWrapContent)
extern "C"  void MUIWrapContentGenerated_ilo_RefreshChildData5_m560920945 (Il2CppObject * __this /* static, unused */, MUIWrapContent_t4054684270 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContentGenerated::ilo_ResetAllItem6(MUIWrapContent,System.Boolean)
extern "C"  void MUIWrapContentGenerated_ilo_ResetAllItem6_m3666288005 (Il2CppObject * __this /* static, unused */, MUIWrapContent_t4054684270 * ____this0, bool ___isData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MUIWrapContentGenerated::ilo_getObject7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * MUIWrapContentGenerated_ilo_getObject7_m2363419105 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContentGenerated::ilo_WrapContent8(MUIWrapContent)
extern "C"  void MUIWrapContentGenerated_ilo_WrapContent8_m1764403528 (Il2CppObject * __this /* static, unused */, MUIWrapContent_t4054684270 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject MUIWrapContentGenerated::ilo_getFunctionS9(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * MUIWrapContentGenerated_ilo_getFunctionS9_m1593303 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
