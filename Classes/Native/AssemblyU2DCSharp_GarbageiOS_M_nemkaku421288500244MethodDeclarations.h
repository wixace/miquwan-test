﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_nemkaku42
struct M_nemkaku42_t1288500244;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_nemkaku42::.ctor()
extern "C"  void M_nemkaku42__ctor_m2730984399 (M_nemkaku42_t1288500244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nemkaku42::M_meaki0(System.String[],System.Int32)
extern "C"  void M_nemkaku42_M_meaki0_m324866631 (M_nemkaku42_t1288500244 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nemkaku42::M_separhiKalxee1(System.String[],System.Int32)
extern "C"  void M_nemkaku42_M_separhiKalxee1_m3073875997 (M_nemkaku42_t1288500244 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nemkaku42::M_gairziForkair2(System.String[],System.Int32)
extern "C"  void M_nemkaku42_M_gairziForkair2_m3816064218 (M_nemkaku42_t1288500244 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nemkaku42::M_pebowsear3(System.String[],System.Int32)
extern "C"  void M_nemkaku42_M_pebowsear3_m3175589785 (M_nemkaku42_t1288500244 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nemkaku42::M_deamereLelnawko4(System.String[],System.Int32)
extern "C"  void M_nemkaku42_M_deamereLelnawko4_m3204006738 (M_nemkaku42_t1288500244 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
