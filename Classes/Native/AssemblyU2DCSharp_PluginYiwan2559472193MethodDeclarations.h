﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginYiwan
struct PluginYiwan_t2559472193;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// System.Object
struct Il2CppObject;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// FloatTextMgr
struct FloatTextMgr_t630384591;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PluginYiwan2559472193.h"

// System.Void PluginYiwan::.ctor()
extern "C"  void PluginYiwan__ctor_m895681098 (PluginYiwan_t2559472193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwan::Init()
extern "C"  void PluginYiwan_Init_m2612828874 (PluginYiwan_t2559472193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginYiwan::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginYiwan_ReqSDKHttpLogin_m1346054971 (PluginYiwan_t2559472193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYiwan::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginYiwan_IsLoginSuccess_m1290158561 (PluginYiwan_t2559472193 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwan::OpenUserLogin()
extern "C"  void PluginYiwan_OpenUserLogin_m3233954460 (PluginYiwan_t2559472193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwan::UserPay(CEvent.ZEvent)
extern "C"  void PluginYiwan_UserPay_m1740861782 (PluginYiwan_t2559472193 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwan::RecRoleUpLv(CEvent.ZEvent)
extern "C"  void PluginYiwan_RecRoleUpLv_m3600643112 (PluginYiwan_t2559472193 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwan::RecGotoGame(CEvent.ZEvent)
extern "C"  void PluginYiwan_RecGotoGame_m3203281934 (PluginYiwan_t2559472193 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwan::RecCreateRole(CEvent.ZEvent)
extern "C"  void PluginYiwan_RecCreateRole_m1934739601 (PluginYiwan_t2559472193 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwan::SumitChannelData(System.Object[])
extern "C"  void PluginYiwan_SumitChannelData_m1845982305 (PluginYiwan_t2559472193 * __this, ObjectU5BU5D_t1108656482* ___datas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwan::OnLoginSuccess(System.String)
extern "C"  void PluginYiwan_OnLoginSuccess_m994690511 (PluginYiwan_t2559472193 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwan::OnLoginFail(System.String)
extern "C"  void PluginYiwan_OnLoginFail_m855840210 (PluginYiwan_t2559472193 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwan::OnPaySuccess(System.String)
extern "C"  void PluginYiwan_OnPaySuccess_m976572366 (PluginYiwan_t2559472193 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwan::OnPayFail(System.String)
extern "C"  void PluginYiwan_OnPayFail_m2680742963 (PluginYiwan_t2559472193 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwan::OnExitGameSuccess(System.String)
extern "C"  void PluginYiwan_OnExitGameSuccess_m2815563974 (PluginYiwan_t2559472193 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwan::OnLogoutSuccess(System.String)
extern "C"  void PluginYiwan_OnLogoutSuccess_m774177440 (PluginYiwan_t2559472193 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwan::<OnLogoutSuccess>m__471()
extern "C"  void PluginYiwan_U3COnLogoutSuccessU3Em__471_m1254586825 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwan::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginYiwan_ilo_AddEventListener1_m2946747754 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginYiwan::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginYiwan_ilo_get_Instance2_m3256012582 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwan::ilo_Log3(System.Object,System.Boolean)
extern "C"  void PluginYiwan_ilo_Log3_m3561434181 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwan::ilo_SumitChannelData4(PluginYiwan,System.Object[])
extern "C"  void PluginYiwan_ilo_SumitChannelData4_m877551499 (Il2CppObject * __this /* static, unused */, PluginYiwan_t2559472193 * ____this0, ObjectU5BU5D_t1108656482* ___datas1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginYiwan::ilo_get_PluginsSdkMgr5()
extern "C"  PluginsSdkMgr_t3884624670 * PluginYiwan_ilo_get_PluginsSdkMgr5_m364917912 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr PluginYiwan::ilo_get_FloatTextMgr6()
extern "C"  FloatTextMgr_t630384591 * PluginYiwan_ilo_get_FloatTextMgr6_m803304289 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
