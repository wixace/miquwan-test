﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t4054002952;
// TypeFlag[]
struct TypeFlagU5BU5D_t693965763;

#include "AssemblyU2DCSharp_MemberID3710171477.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConstructorID
struct  ConstructorID_t3348888181  : public MemberID_t3710171477
{
public:
	// System.String[] ConstructorID::paramTypeNames
	StringU5BU5D_t4054002952* ___paramTypeNames_1;
	// TypeFlag[] ConstructorID::paramFlags
	TypeFlagU5BU5D_t693965763* ___paramFlags_2;

public:
	inline static int32_t get_offset_of_paramTypeNames_1() { return static_cast<int32_t>(offsetof(ConstructorID_t3348888181, ___paramTypeNames_1)); }
	inline StringU5BU5D_t4054002952* get_paramTypeNames_1() const { return ___paramTypeNames_1; }
	inline StringU5BU5D_t4054002952** get_address_of_paramTypeNames_1() { return &___paramTypeNames_1; }
	inline void set_paramTypeNames_1(StringU5BU5D_t4054002952* value)
	{
		___paramTypeNames_1 = value;
		Il2CppCodeGenWriteBarrier(&___paramTypeNames_1, value);
	}

	inline static int32_t get_offset_of_paramFlags_2() { return static_cast<int32_t>(offsetof(ConstructorID_t3348888181, ___paramFlags_2)); }
	inline TypeFlagU5BU5D_t693965763* get_paramFlags_2() const { return ___paramFlags_2; }
	inline TypeFlagU5BU5D_t693965763** get_address_of_paramFlags_2() { return &___paramFlags_2; }
	inline void set_paramFlags_2(TypeFlagU5BU5D_t693965763* value)
	{
		___paramFlags_2 = value;
		Il2CppCodeGenWriteBarrier(&___paramFlags_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
