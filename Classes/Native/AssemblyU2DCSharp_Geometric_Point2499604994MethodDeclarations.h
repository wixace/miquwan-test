﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Geometric/Point
struct Point_t2499604994;
struct Point_t2499604994_marshaled_pinvoke;
struct Point_t2499604994_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Geometric_Point2499604994.h"

// System.Void Geometric/Point::.ctor(System.Single,System.Single)
extern "C"  void Point__ctor_m1046825991 (Point_t2499604994 * __this, float ____x0, float ____y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Point_t2499604994;
struct Point_t2499604994_marshaled_pinvoke;

extern "C" void Point_t2499604994_marshal_pinvoke(const Point_t2499604994& unmarshaled, Point_t2499604994_marshaled_pinvoke& marshaled);
extern "C" void Point_t2499604994_marshal_pinvoke_back(const Point_t2499604994_marshaled_pinvoke& marshaled, Point_t2499604994& unmarshaled);
extern "C" void Point_t2499604994_marshal_pinvoke_cleanup(Point_t2499604994_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Point_t2499604994;
struct Point_t2499604994_marshaled_com;

extern "C" void Point_t2499604994_marshal_com(const Point_t2499604994& unmarshaled, Point_t2499604994_marshaled_com& marshaled);
extern "C" void Point_t2499604994_marshal_com_back(const Point_t2499604994_marshaled_com& marshaled, Point_t2499604994& unmarshaled);
extern "C" void Point_t2499604994_marshal_com_cleanup(Point_t2499604994_marshaled_com& marshaled);
