﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginDolphin
struct  PluginDolphin_t1635836683  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginDolphin::token
	String_t* ___token_2;
	// System.String PluginDolphin::pid
	String_t* ___pid_3;
	// System.String PluginDolphin::gid
	String_t* ___gid_4;
	// System.String PluginDolphin::dolphin
	String_t* ___dolphin_5;
	// System.String PluginDolphin::gameName
	String_t* ___gameName_6;
	// System.String PluginDolphin::timeStamp
	String_t* ___timeStamp_7;
	// System.String PluginDolphin::appid
	String_t* ___appid_8;
	// System.String PluginDolphin::codeversion
	String_t* ___codeversion_9;
	// System.String PluginDolphin::os
	String_t* ___os_10;
	// System.Boolean PluginDolphin::isOut
	bool ___isOut_11;
	// System.String PluginDolphin::configId
	String_t* ___configId_12;
	// Mihua.SDK.PayInfo PluginDolphin::payInfo
	PayInfo_t1775308120 * ___payInfo_13;

public:
	inline static int32_t get_offset_of_token_2() { return static_cast<int32_t>(offsetof(PluginDolphin_t1635836683, ___token_2)); }
	inline String_t* get_token_2() const { return ___token_2; }
	inline String_t** get_address_of_token_2() { return &___token_2; }
	inline void set_token_2(String_t* value)
	{
		___token_2 = value;
		Il2CppCodeGenWriteBarrier(&___token_2, value);
	}

	inline static int32_t get_offset_of_pid_3() { return static_cast<int32_t>(offsetof(PluginDolphin_t1635836683, ___pid_3)); }
	inline String_t* get_pid_3() const { return ___pid_3; }
	inline String_t** get_address_of_pid_3() { return &___pid_3; }
	inline void set_pid_3(String_t* value)
	{
		___pid_3 = value;
		Il2CppCodeGenWriteBarrier(&___pid_3, value);
	}

	inline static int32_t get_offset_of_gid_4() { return static_cast<int32_t>(offsetof(PluginDolphin_t1635836683, ___gid_4)); }
	inline String_t* get_gid_4() const { return ___gid_4; }
	inline String_t** get_address_of_gid_4() { return &___gid_4; }
	inline void set_gid_4(String_t* value)
	{
		___gid_4 = value;
		Il2CppCodeGenWriteBarrier(&___gid_4, value);
	}

	inline static int32_t get_offset_of_dolphin_5() { return static_cast<int32_t>(offsetof(PluginDolphin_t1635836683, ___dolphin_5)); }
	inline String_t* get_dolphin_5() const { return ___dolphin_5; }
	inline String_t** get_address_of_dolphin_5() { return &___dolphin_5; }
	inline void set_dolphin_5(String_t* value)
	{
		___dolphin_5 = value;
		Il2CppCodeGenWriteBarrier(&___dolphin_5, value);
	}

	inline static int32_t get_offset_of_gameName_6() { return static_cast<int32_t>(offsetof(PluginDolphin_t1635836683, ___gameName_6)); }
	inline String_t* get_gameName_6() const { return ___gameName_6; }
	inline String_t** get_address_of_gameName_6() { return &___gameName_6; }
	inline void set_gameName_6(String_t* value)
	{
		___gameName_6 = value;
		Il2CppCodeGenWriteBarrier(&___gameName_6, value);
	}

	inline static int32_t get_offset_of_timeStamp_7() { return static_cast<int32_t>(offsetof(PluginDolphin_t1635836683, ___timeStamp_7)); }
	inline String_t* get_timeStamp_7() const { return ___timeStamp_7; }
	inline String_t** get_address_of_timeStamp_7() { return &___timeStamp_7; }
	inline void set_timeStamp_7(String_t* value)
	{
		___timeStamp_7 = value;
		Il2CppCodeGenWriteBarrier(&___timeStamp_7, value);
	}

	inline static int32_t get_offset_of_appid_8() { return static_cast<int32_t>(offsetof(PluginDolphin_t1635836683, ___appid_8)); }
	inline String_t* get_appid_8() const { return ___appid_8; }
	inline String_t** get_address_of_appid_8() { return &___appid_8; }
	inline void set_appid_8(String_t* value)
	{
		___appid_8 = value;
		Il2CppCodeGenWriteBarrier(&___appid_8, value);
	}

	inline static int32_t get_offset_of_codeversion_9() { return static_cast<int32_t>(offsetof(PluginDolphin_t1635836683, ___codeversion_9)); }
	inline String_t* get_codeversion_9() const { return ___codeversion_9; }
	inline String_t** get_address_of_codeversion_9() { return &___codeversion_9; }
	inline void set_codeversion_9(String_t* value)
	{
		___codeversion_9 = value;
		Il2CppCodeGenWriteBarrier(&___codeversion_9, value);
	}

	inline static int32_t get_offset_of_os_10() { return static_cast<int32_t>(offsetof(PluginDolphin_t1635836683, ___os_10)); }
	inline String_t* get_os_10() const { return ___os_10; }
	inline String_t** get_address_of_os_10() { return &___os_10; }
	inline void set_os_10(String_t* value)
	{
		___os_10 = value;
		Il2CppCodeGenWriteBarrier(&___os_10, value);
	}

	inline static int32_t get_offset_of_isOut_11() { return static_cast<int32_t>(offsetof(PluginDolphin_t1635836683, ___isOut_11)); }
	inline bool get_isOut_11() const { return ___isOut_11; }
	inline bool* get_address_of_isOut_11() { return &___isOut_11; }
	inline void set_isOut_11(bool value)
	{
		___isOut_11 = value;
	}

	inline static int32_t get_offset_of_configId_12() { return static_cast<int32_t>(offsetof(PluginDolphin_t1635836683, ___configId_12)); }
	inline String_t* get_configId_12() const { return ___configId_12; }
	inline String_t** get_address_of_configId_12() { return &___configId_12; }
	inline void set_configId_12(String_t* value)
	{
		___configId_12 = value;
		Il2CppCodeGenWriteBarrier(&___configId_12, value);
	}

	inline static int32_t get_offset_of_payInfo_13() { return static_cast<int32_t>(offsetof(PluginDolphin_t1635836683, ___payInfo_13)); }
	inline PayInfo_t1775308120 * get_payInfo_13() const { return ___payInfo_13; }
	inline PayInfo_t1775308120 ** get_address_of_payInfo_13() { return &___payInfo_13; }
	inline void set_payInfo_13(PayInfo_t1775308120 * value)
	{
		___payInfo_13 = value;
		Il2CppCodeGenWriteBarrier(&___payInfo_13, value);
	}
};

struct PluginDolphin_t1635836683_StaticFields
{
public:
	// System.Action PluginDolphin::<>f__am$cacheC
	Action_t3771233898 * ___U3CU3Ef__amU24cacheC_14;
	// System.Action PluginDolphin::<>f__am$cacheD
	Action_t3771233898 * ___U3CU3Ef__amU24cacheD_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_14() { return static_cast<int32_t>(offsetof(PluginDolphin_t1635836683_StaticFields, ___U3CU3Ef__amU24cacheC_14)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cacheC_14() const { return ___U3CU3Ef__amU24cacheC_14; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cacheC_14() { return &___U3CU3Ef__amU24cacheC_14; }
	inline void set_U3CU3Ef__amU24cacheC_14(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cacheC_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_15() { return static_cast<int32_t>(offsetof(PluginDolphin_t1635836683_StaticFields, ___U3CU3Ef__amU24cacheD_15)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cacheD_15() const { return ___U3CU3Ef__amU24cacheD_15; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cacheD_15() { return &___U3CU3Ef__amU24cacheD_15; }
	inline void set_U3CU3Ef__amU24cacheD_15(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cacheD_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
