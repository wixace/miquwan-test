﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_CombineInstanceGenerated
struct UnityEngine_CombineInstanceGenerated_t4071904867;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_CombineInstanceGenerated::.ctor()
extern "C"  void UnityEngine_CombineInstanceGenerated__ctor_m3781448600 (UnityEngine_CombineInstanceGenerated_t4071904867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CombineInstanceGenerated::.cctor()
extern "C"  void UnityEngine_CombineInstanceGenerated__cctor_m778693397 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CombineInstanceGenerated::CombineInstance_CombineInstance1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CombineInstanceGenerated_CombineInstance_CombineInstance1_m1575758136 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CombineInstanceGenerated::CombineInstance_mesh(JSVCall)
extern "C"  void UnityEngine_CombineInstanceGenerated_CombineInstance_mesh_m449322531 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CombineInstanceGenerated::CombineInstance_subMeshIndex(JSVCall)
extern "C"  void UnityEngine_CombineInstanceGenerated_CombineInstance_subMeshIndex_m1997921419 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CombineInstanceGenerated::CombineInstance_transform(JSVCall)
extern "C"  void UnityEngine_CombineInstanceGenerated_CombineInstance_transform_m1420366320 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CombineInstanceGenerated::__Register()
extern "C"  void UnityEngine_CombineInstanceGenerated___Register_m1750706223 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CombineInstanceGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_CombineInstanceGenerated_ilo_attachFinalizerObject1_m3623146823 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CombineInstanceGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_CombineInstanceGenerated_ilo_addJSCSRel2_m2340152021 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CombineInstanceGenerated::ilo_getInt323(System.Int32)
extern "C"  int32_t UnityEngine_CombineInstanceGenerated_ilo_getInt323_m1247798113 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CombineInstanceGenerated::ilo_changeJSObj4(System.Int32,System.Object)
extern "C"  void UnityEngine_CombineInstanceGenerated_ilo_changeJSObj4_m2330680916 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CombineInstanceGenerated::ilo_setObject5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_CombineInstanceGenerated_ilo_setObject5_m212005878 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
