﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIPlaySoundGenerated
struct UIPlaySoundGenerated_t234301256;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// UIPlaySound
struct UIPlaySound_t532605447;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_UIPlaySound532605447.h"

// System.Void UIPlaySoundGenerated::.ctor()
extern "C"  void UIPlaySoundGenerated__ctor_m3019170899 (UIPlaySoundGenerated_t234301256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPlaySoundGenerated::UIPlaySound_UIPlaySound1(JSVCall,System.Int32)
extern "C"  bool UIPlaySoundGenerated_UIPlaySound_UIPlaySound1_m782719991 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlaySoundGenerated::UIPlaySound_audioClip(JSVCall)
extern "C"  void UIPlaySoundGenerated_UIPlaySound_audioClip_m1179610792 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlaySoundGenerated::UIPlaySound_trigger(JSVCall)
extern "C"  void UIPlaySoundGenerated_UIPlaySound_trigger_m1872198518 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlaySoundGenerated::UIPlaySound_volume(JSVCall)
extern "C"  void UIPlaySoundGenerated_UIPlaySound_volume_m2250750468 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlaySoundGenerated::UIPlaySound_pitch(JSVCall)
extern "C"  void UIPlaySoundGenerated_UIPlaySound_pitch_m2599604462 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPlaySoundGenerated::UIPlaySound_Play(JSVCall,System.Int32)
extern "C"  bool UIPlaySoundGenerated_UIPlaySound_Play_m817523905 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlaySoundGenerated::__Register()
extern "C"  void UIPlaySoundGenerated___Register_m2830980180 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIPlaySoundGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UIPlaySoundGenerated_ilo_getObject1_m2547828127 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPlaySoundGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UIPlaySoundGenerated_ilo_attachFinalizerObject2_m445372973 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlaySoundGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UIPlaySoundGenerated_ilo_addJSCSRel3_m1105974929 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIPlaySoundGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIPlaySoundGenerated_ilo_setObject4_m3758266330 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIPlaySoundGenerated::ilo_getEnum5(System.Int32)
extern "C"  int32_t UIPlaySoundGenerated_ilo_getEnum5_m4289405793 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIPlaySoundGenerated::ilo_getSingle6(System.Int32)
extern "C"  float UIPlaySoundGenerated_ilo_getSingle6_m4287771641 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPlaySoundGenerated::ilo_Play7(UIPlaySound)
extern "C"  void UIPlaySoundGenerated_ilo_Play7_m1215250826 (Il2CppObject * __this /* static, unused */, UIPlaySound_t532605447 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
