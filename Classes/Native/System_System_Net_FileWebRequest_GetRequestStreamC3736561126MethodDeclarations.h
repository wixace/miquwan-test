﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.FileWebRequest/GetRequestStreamCallback
struct GetRequestStreamCallback_t3736561126;
// System.Object
struct Il2CppObject;
// System.IO.Stream
struct Stream_t1561764144;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Net.FileWebRequest/GetRequestStreamCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetRequestStreamCallback__ctor_m609089324 (GetRequestStreamCallback_t3736561126 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.FileWebRequest/GetRequestStreamCallback::Invoke()
extern "C"  Stream_t1561764144 * GetRequestStreamCallback_Invoke_m2602366844 (GetRequestStreamCallback_t3736561126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.FileWebRequest/GetRequestStreamCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetRequestStreamCallback_BeginInvoke_m1340948413 (GetRequestStreamCallback_t3736561126 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.FileWebRequest/GetRequestStreamCallback::EndInvoke(System.IAsyncResult)
extern "C"  Stream_t1561764144 * GetRequestStreamCallback_EndInvoke_m697554866 (GetRequestStreamCallback_t3736561126 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
