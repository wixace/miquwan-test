﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZiShiYingMgr
struct ZiShiYingMgr_t4004457962;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// GlobalGOMgr
struct GlobalGOMgr_t803081773;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"

// System.Void ZiShiYingMgr::.ctor()
extern "C"  void ZiShiYingMgr__ctor_m3791440881 (ZiShiYingMgr_t4004457962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZiShiYingMgr::Init()
extern "C"  void ZiShiYingMgr_Init_m1736409155 (ZiShiYingMgr_t4004457962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZiShiYingMgr::upDataFengBianLv(CEvent.ZEvent)
extern "C"  void ZiShiYingMgr_upDataFengBianLv_m3431466733 (ZiShiYingMgr_t4004457962 * __this, ZEvent_t3638018500 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GlobalGOMgr ZiShiYingMgr::ilo_get_GlobalGOMgr1()
extern "C"  GlobalGOMgr_t803081773 * ZiShiYingMgr_ilo_get_GlobalGOMgr1_m3214030137 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
