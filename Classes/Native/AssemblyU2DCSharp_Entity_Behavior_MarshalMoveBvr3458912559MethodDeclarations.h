﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.MarshalMoveBvr
struct MarshalMoveBvr_t3458912559;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// CombatEntity
struct CombatEntity_t684137495;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"

// System.Void Entity.Behavior.MarshalMoveBvr::.ctor()
extern "C"  void MarshalMoveBvr__ctor_m1013980891 (MarshalMoveBvr_t3458912559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.MarshalMoveBvr::get_id()
extern "C"  uint8_t MarshalMoveBvr_get_id_m172415391 (MarshalMoveBvr_t3458912559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.MarshalMoveBvr::Reason()
extern "C"  void MarshalMoveBvr_Reason_m920024653 (MarshalMoveBvr_t3458912559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.MarshalMoveBvr::Action()
extern "C"  void MarshalMoveBvr_Action_m116731327 (MarshalMoveBvr_t3458912559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.MarshalMoveBvr::SetParams(System.Object[])
extern "C"  void MarshalMoveBvr_SetParams_m4002867985 (MarshalMoveBvr_t3458912559 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.MarshalMoveBvr::DoEntering()
extern "C"  void MarshalMoveBvr_DoEntering_m3557801854 (MarshalMoveBvr_t3458912559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.MarshalMoveBvr::DoLeaving()
extern "C"  void MarshalMoveBvr_DoLeaving_m1113867362 (MarshalMoveBvr_t3458912559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.MarshalMoveBvr::OnForceShiftOver(CEvent.ZEvent)
extern "C"  void MarshalMoveBvr_OnForceShiftOver_m1509730632 (MarshalMoveBvr_t3458912559 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.MarshalMoveBvr::Pause()
extern "C"  void MarshalMoveBvr_Pause_m1068106863 (MarshalMoveBvr_t3458912559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.MarshalMoveBvr::Play()
extern "C"  void MarshalMoveBvr_Play_m3507705149 (MarshalMoveBvr_t3458912559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.MarshalMoveBvr::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void MarshalMoveBvr_ilo_AddEventListener1_m743858873 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.MarshalMoveBvr::ilo_get_entity2(Entity.Behavior.IBehavior)
extern "C"  CombatEntity_t684137495 * MarshalMoveBvr_ilo_get_entity2_m2187854178 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.MarshalMoveBvr::ilo_RemoveEventListener3(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void MarshalMoveBvr_ilo_RemoveEventListener3_m2242714078 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
