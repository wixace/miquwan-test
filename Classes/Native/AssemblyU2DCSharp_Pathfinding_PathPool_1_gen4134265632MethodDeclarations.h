﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// Pathfinding.Path
struct Path_t1974241691;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"

// System.Void Pathfinding.PathPool`1<System.Object>::.cctor()
extern "C"  void PathPool_1__cctor_m4111071586_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define PathPool_1__cctor_m4111071586(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PathPool_1__cctor_m4111071586_gshared)(__this /* static, unused */, method)
// System.Void Pathfinding.PathPool`1<System.Object>::Recycle(T)
extern "C"  void PathPool_1_Recycle_m1360667714_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___path0, const MethodInfo* method);
#define PathPool_1_Recycle_m1360667714(__this /* static, unused */, ___path0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))PathPool_1_Recycle_m1360667714_gshared)(__this /* static, unused */, ___path0, method)
// System.Void Pathfinding.PathPool`1<System.Object>::Warmup(System.Int32,System.Int32)
extern "C"  void PathPool_1_Warmup_m4243013421_gshared (Il2CppObject * __this /* static, unused */, int32_t ___count0, int32_t ___length1, const MethodInfo* method);
#define PathPool_1_Warmup_m4243013421(__this /* static, unused */, ___count0, ___length1, method) ((  void (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))PathPool_1_Warmup_m4243013421_gshared)(__this /* static, unused */, ___count0, ___length1, method)
// System.Int32 Pathfinding.PathPool`1<System.Object>::GetTotalCreated()
extern "C"  int32_t PathPool_1_GetTotalCreated_m3938489809_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define PathPool_1_GetTotalCreated_m3938489809(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PathPool_1_GetTotalCreated_m3938489809_gshared)(__this /* static, unused */, method)
// System.Int32 Pathfinding.PathPool`1<System.Object>::GetSize()
extern "C"  int32_t PathPool_1_GetSize_m2941120398_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define PathPool_1_GetSize_m2941120398(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PathPool_1_GetSize_m2941120398_gshared)(__this /* static, unused */, method)
// T Pathfinding.PathPool`1<System.Object>::GetPath()
extern "C"  Il2CppObject * PathPool_1_GetPath_m3122724387_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define PathPool_1_GetPath_m3122724387(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PathPool_1_GetPath_m3122724387_gshared)(__this /* static, unused */, method)
// System.Void Pathfinding.PathPool`1<System.Object>::ilo_Claim1(Pathfinding.Path,System.Object)
extern "C"  void PathPool_1_ilo_Claim1_m992502912_gshared (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, Il2CppObject * ___o1, const MethodInfo* method);
#define PathPool_1_ilo_Claim1_m992502912(__this /* static, unused */, ____this0, ___o1, method) ((  void (*) (Il2CppObject * /* static, unused */, Path_t1974241691 *, Il2CppObject *, const MethodInfo*))PathPool_1_ilo_Claim1_m992502912_gshared)(__this /* static, unused */, ____this0, ___o1, method)
// System.Void Pathfinding.PathPool`1<System.Object>::ilo_Release2(Pathfinding.Path,System.Object)
extern "C"  void PathPool_1_ilo_Release2_m2573264726_gshared (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, Il2CppObject * ___o1, const MethodInfo* method);
#define PathPool_1_ilo_Release2_m2573264726(__this /* static, unused */, ____this0, ___o1, method) ((  void (*) (Il2CppObject * /* static, unused */, Path_t1974241691 *, Il2CppObject *, const MethodInfo*))PathPool_1_ilo_Release2_m2573264726_gshared)(__this /* static, unused */, ____this0, ___o1, method)
