﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>
struct Dictionary_2_t3590202184;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4205138238.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,SoundStatus>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1476065633_gshared (Enumerator_t4205138238 * __this, Dictionary_2_t3590202184 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1476065633(__this, ___host0, method) ((  void (*) (Enumerator_t4205138238 *, Dictionary_2_t3590202184 *, const MethodInfo*))Enumerator__ctor_m1476065633_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,SoundStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1043513130_gshared (Enumerator_t4205138238 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1043513130(__this, method) ((  Il2CppObject * (*) (Enumerator_t4205138238 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1043513130_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,SoundStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1363696820_gshared (Enumerator_t4205138238 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1363696820(__this, method) ((  void (*) (Enumerator_t4205138238 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1363696820_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,SoundStatus>::Dispose()
extern "C"  void Enumerator_Dispose_m107203139_gshared (Enumerator_t4205138238 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m107203139(__this, method) ((  void (*) (Enumerator_t4205138238 *, const MethodInfo*))Enumerator_Dispose_m107203139_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,SoundStatus>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3090322788_gshared (Enumerator_t4205138238 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3090322788(__this, method) ((  bool (*) (Enumerator_t4205138238 *, const MethodInfo*))Enumerator_MoveNext_m3090322788_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,SoundStatus>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1547913140_gshared (Enumerator_t4205138238 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1547913140(__this, method) ((  int32_t (*) (Enumerator_t4205138238 *, const MethodInfo*))Enumerator_get_Current_m1547913140_gshared)(__this, method)
