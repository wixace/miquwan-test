﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.LocalAvoidance/VOLine>
struct DefaultComparer_t477708468;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_VOLin2029931801.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.LocalAvoidance/VOLine>::.ctor()
extern "C"  void DefaultComparer__ctor_m2228674775_gshared (DefaultComparer_t477708468 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2228674775(__this, method) ((  void (*) (DefaultComparer_t477708468 *, const MethodInfo*))DefaultComparer__ctor_m2228674775_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.LocalAvoidance/VOLine>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1669667316_gshared (DefaultComparer_t477708468 * __this, VOLine_t2029931801  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1669667316(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t477708468 *, VOLine_t2029931801 , const MethodInfo*))DefaultComparer_GetHashCode_m1669667316_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.LocalAvoidance/VOLine>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1841814952_gshared (DefaultComparer_t477708468 * __this, VOLine_t2029931801  ___x0, VOLine_t2029931801  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1841814952(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t477708468 *, VOLine_t2029931801 , VOLine_t2029931801 , const MethodInfo*))DefaultComparer_Equals_m1841814952_gshared)(__this, ___x0, ___y1, method)
