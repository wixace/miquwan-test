﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua_Asset_ABLoadOperation_AssetOperationSimulationGenerated
struct Mihua_Asset_ABLoadOperation_AssetOperationSimulationGenerated_t1845509148;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void Mihua_Asset_ABLoadOperation_AssetOperationSimulationGenerated::.ctor()
extern "C"  void Mihua_Asset_ABLoadOperation_AssetOperationSimulationGenerated__ctor_m1542034639 (Mihua_Asset_ABLoadOperation_AssetOperationSimulationGenerated_t1845509148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_AssetOperationSimulationGenerated::AssetOperationSimulation_AssetOperationSimulation1(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_AssetOperationSimulationGenerated_AssetOperationSimulation_AssetOperationSimulation1_m1480955331 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_AssetOperationSimulationGenerated::AssetOperationSimulation_progress(JSVCall)
extern "C"  void Mihua_Asset_ABLoadOperation_AssetOperationSimulationGenerated_AssetOperationSimulation_progress_m184770892 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_AssetOperationSimulationGenerated::AssetOperationSimulation_Clear(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_AssetOperationSimulationGenerated_AssetOperationSimulation_Clear_m2425902045 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_AssetOperationSimulationGenerated::AssetOperationSimulation_GetAsset(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_AssetOperationSimulationGenerated_AssetOperationSimulation_GetAsset_m3889486380 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_AssetOperationSimulationGenerated::AssetOperationSimulation_IsDone(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_AssetOperationSimulationGenerated_AssetOperationSimulation_IsDone_m3691484030 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_AssetOperationSimulationGenerated::__Register()
extern "C"  void Mihua_Asset_ABLoadOperation_AssetOperationSimulationGenerated___Register_m30380376 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mihua_Asset_ABLoadOperation_AssetOperationSimulationGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t Mihua_Asset_ABLoadOperation_AssetOperationSimulationGenerated_ilo_getObject1_m3320687943 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_AssetOperationSimulationGenerated::ilo_setBooleanS2(System.Int32,System.Boolean)
extern "C"  void Mihua_Asset_ABLoadOperation_AssetOperationSimulationGenerated_ilo_setBooleanS2_m2363395999 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
