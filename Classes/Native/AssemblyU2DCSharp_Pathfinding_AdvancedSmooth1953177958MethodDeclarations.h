﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.AdvancedSmooth
struct AdvancedSmooth_t1953177958;
// Pathfinding.Path
struct Path_t1974241691;
// System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>
struct List_1_t1833380930;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// Pathfinding.AdvancedSmooth/ConstantTurn
struct ConstantTurn_t1658810934;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// Pathfinding.AdvancedSmooth/TurnConstructor
struct TurnConstructor_t3543216936;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_ModifierData4046421655.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Pathfinding_AdvancedSmooth_Const1658810934.h"
#include "AssemblyU2DCSharp_Pathfinding_AdvancedSmooth_TurnC3543216936.h"

// System.Void Pathfinding.AdvancedSmooth::.ctor()
extern "C"  void AdvancedSmooth__ctor_m3452301761 (AdvancedSmooth_t1953177958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ModifierData Pathfinding.AdvancedSmooth::get_input()
extern "C"  int32_t AdvancedSmooth_get_input_m3851655758 (AdvancedSmooth_t1953177958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ModifierData Pathfinding.AdvancedSmooth::get_output()
extern "C"  int32_t AdvancedSmooth_get_output_m3036731935 (AdvancedSmooth_t1953177958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth::Apply(Pathfinding.Path,Pathfinding.ModifierData)
extern "C"  void AdvancedSmooth_Apply_m4181965565 (AdvancedSmooth_t1953177958 * __this, Path_t1974241691 * ___p0, int32_t ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth::EvaluatePaths(System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void AdvancedSmooth_EvaluatePaths_m1400789721 (AdvancedSmooth_t1953177958 * __this, List_1_t1833380930 * ___turnList0, List_1_t1355284822 * ___output1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth::ilo_Prepare1(Pathfinding.AdvancedSmooth/ConstantTurn,System.Int32,UnityEngine.Vector3[])
extern "C"  void AdvancedSmooth_ilo_Prepare1_m3416763500 (Il2CppObject * __this /* static, unused */, ConstantTurn_t1658810934 * ____this0, int32_t ___i1, Vector3U5BU5D_t215400611* ___vectorPath2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth::ilo_PointToTangent2(Pathfinding.AdvancedSmooth/TurnConstructor,System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>)
extern "C"  void AdvancedSmooth_ilo_PointToTangent2_m3747448640 (Il2CppObject * __this /* static, unused */, TurnConstructor_t3543216936 * ____this0, List_1_t1833380930 * ___turnList1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth::ilo_TangentToTangent3(Pathfinding.AdvancedSmooth/ConstantTurn,System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>)
extern "C"  void AdvancedSmooth_ilo_TangentToTangent3_m1758891226 (Il2CppObject * __this /* static, unused */, ConstantTurn_t1658810934 * ____this0, List_1_t1833380930 * ___turnList1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
