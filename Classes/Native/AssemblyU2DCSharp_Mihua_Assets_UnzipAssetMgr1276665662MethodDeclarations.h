﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.Assets.UnzipAssetMgr
struct UnzipAssetMgr_t1276665662;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;
// System.Action`2<System.Byte[],System.String>
struct Action_2_t1442129143;
// System.Object
struct Il2CppObject;
// SevenZip.Compression.LZMA.Encoder
struct Encoder_t1693961342;
// System.IO.Stream
struct Stream_t1561764144;
// SevenZip.ICodeProgress
struct ICodeProgress_t453587813;
// SevenZip.Compression.LZMA.Decoder
struct Decoder_t548795302;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Mihua_Assets_UnzipAssetMgr1276665662.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Encode1693961342.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Decoder548795302.h"

// System.Void Mihua.Assets.UnzipAssetMgr::.ctor()
extern "C"  void UnzipAssetMgr__ctor_m382337184 (UnzipAssetMgr_t1276665662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mihua.Assets.UnzipAssetMgr::get_UnZipFileCount()
extern "C"  int32_t UnzipAssetMgr_get_UnZipFileCount_m4242631814 (UnzipAssetMgr_t1276665662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.UnzipAssetMgr::Start()
extern "C"  void UnzipAssetMgr_Start_m3624442272 (UnzipAssetMgr_t1276665662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.UnzipAssetMgr::Unzip()
extern "C"  void UnzipAssetMgr_Unzip_m955523494 (UnzipAssetMgr_t1276665662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.UnzipAssetMgr::AddUnzip(System.Byte[],System.String,System.Action`2<System.Byte[],System.String>,System.Boolean)
extern "C"  void UnzipAssetMgr_AddUnzip_m836019639 (UnzipAssetMgr_t1276665662 * __this, ByteU5BU5D_t4260760469* ___bytes0, String_t* ___key1, Action_2_t1442129143 * ___callBack2, bool ___isCompress3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mihua.Assets.UnzipAssetMgr::Compress(System.String)
extern "C"  ByteU5BU5D_t4260760469* UnzipAssetMgr_Compress_m3087937426 (UnzipAssetMgr_t1276665662 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua.Assets.UnzipAssetMgr::Decompress(System.Byte[])
extern "C"  String_t* UnzipAssetMgr_Decompress_m1002134623 (UnzipAssetMgr_t1276665662 * __this, ByteU5BU5D_t4260760469* ___datas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mihua.Assets.UnzipAssetMgr::LZMACompress(System.Byte[])
extern "C"  ByteU5BU5D_t4260760469* UnzipAssetMgr_LZMACompress_m2079727863 (UnzipAssetMgr_t1276665662 * __this, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mihua.Assets.UnzipAssetMgr::LZMADecompress(System.Byte[])
extern "C"  ByteU5BU5D_t4260760469* UnzipAssetMgr_LZMADecompress_m3509340950 (UnzipAssetMgr_t1276665662 * __this, ByteU5BU5D_t4260760469* ___datas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.UnzipAssetMgr::BZip2Compression(System.String,System.Byte[])
extern "C"  void UnzipAssetMgr_BZip2Compression_m2056628238 (Il2CppObject * __this /* static, unused */, String_t* ___path0, ByteU5BU5D_t4260760469* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mihua.Assets.UnzipAssetMgr::BZip2DeCompression(System.Byte[])
extern "C"  ByteU5BU5D_t4260760469* UnzipAssetMgr_BZip2DeCompression_m3249222089 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.UnzipAssetMgr::ilo_LogError1(System.Object,System.Boolean)
extern "C"  void UnzipAssetMgr_ilo_LogError1_m1984429153 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mihua.Assets.UnzipAssetMgr::ilo_LZMADecompress2(Mihua.Assets.UnzipAssetMgr,System.Byte[])
extern "C"  ByteU5BU5D_t4260760469* UnzipAssetMgr_ilo_LZMADecompress2_m4238359958 (Il2CppObject * __this /* static, unused */, UnzipAssetMgr_t1276665662 * ____this0, ByteU5BU5D_t4260760469* ___datas1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.UnzipAssetMgr::ilo_Code3(SevenZip.Compression.LZMA.Encoder,System.IO.Stream,System.IO.Stream,System.Int64,System.Int64,SevenZip.ICodeProgress)
extern "C"  void UnzipAssetMgr_ilo_Code3_m1341167310 (Il2CppObject * __this /* static, unused */, Encoder_t1693961342 * ____this0, Stream_t1561764144 * ___inStream1, Stream_t1561764144 * ___outStream2, int64_t ___inSize3, int64_t ___outSize4, Il2CppObject * ___progress5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Assets.UnzipAssetMgr::ilo_Code4(SevenZip.Compression.LZMA.Decoder,System.IO.Stream,System.IO.Stream,System.Int64,System.Int64,SevenZip.ICodeProgress)
extern "C"  void UnzipAssetMgr_ilo_Code4_m1813522231 (Il2CppObject * __this /* static, unused */, Decoder_t548795302 * ____this0, Stream_t1561764144 * ___inStream1, Stream_t1561764144 * ___outStream2, int64_t ___inSize3, int64_t ___outSize4, Il2CppObject * ___progress5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
