﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._b58a0be3a356d4d391be03951271308d
struct _b58a0be3a356d4d391be03951271308d_t172209893;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__b58a0be3a356d4d391be03951172209893.h"

// System.Void Little._b58a0be3a356d4d391be03951271308d::.ctor()
extern "C"  void _b58a0be3a356d4d391be03951271308d__ctor_m1224630504 (_b58a0be3a356d4d391be03951271308d_t172209893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b58a0be3a356d4d391be03951271308d::_b58a0be3a356d4d391be03951271308dm2(System.Int32)
extern "C"  int32_t _b58a0be3a356d4d391be03951271308d__b58a0be3a356d4d391be03951271308dm2_m3588816761 (_b58a0be3a356d4d391be03951271308d_t172209893 * __this, int32_t ____b58a0be3a356d4d391be03951271308da0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b58a0be3a356d4d391be03951271308d::_b58a0be3a356d4d391be03951271308dm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _b58a0be3a356d4d391be03951271308d__b58a0be3a356d4d391be03951271308dm_m3164144477 (_b58a0be3a356d4d391be03951271308d_t172209893 * __this, int32_t ____b58a0be3a356d4d391be03951271308da0, int32_t ____b58a0be3a356d4d391be03951271308d41, int32_t ____b58a0be3a356d4d391be03951271308dc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b58a0be3a356d4d391be03951271308d::ilo__b58a0be3a356d4d391be03951271308dm21(Little._b58a0be3a356d4d391be03951271308d,System.Int32)
extern "C"  int32_t _b58a0be3a356d4d391be03951271308d_ilo__b58a0be3a356d4d391be03951271308dm21_m2356832332 (Il2CppObject * __this /* static, unused */, _b58a0be3a356d4d391be03951271308d_t172209893 * ____this0, int32_t ____b58a0be3a356d4d391be03951271308da1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
