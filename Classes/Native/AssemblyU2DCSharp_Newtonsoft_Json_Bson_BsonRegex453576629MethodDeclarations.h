﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Bson.BsonRegex
struct BsonRegex_t453576629;
// System.String
struct String_t;
// Newtonsoft.Json.Bson.BsonString
struct BsonString_t2875117585;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonString2875117585.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonType2455132538.h"

// System.Void Newtonsoft.Json.Bson.BsonRegex::.ctor(System.String,System.String)
extern "C"  void BsonRegex__ctor_m1696775601 (BsonRegex_t453576629 * __this, String_t* ___pattern0, String_t* ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonRegex::get_Pattern()
extern "C"  BsonString_t2875117585 * BsonRegex_get_Pattern_m3768710537 (BsonRegex_t453576629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonRegex::set_Pattern(Newtonsoft.Json.Bson.BsonString)
extern "C"  void BsonRegex_set_Pattern_m2132106410 (BsonRegex_t453576629 * __this, BsonString_t2875117585 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonRegex::get_Options()
extern "C"  BsonString_t2875117585 * BsonRegex_get_Options_m1347749143 (BsonRegex_t453576629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonRegex::set_Options(Newtonsoft.Json.Bson.BsonString)
extern "C"  void BsonRegex_set_Options_m931388252 (BsonRegex_t453576629 * __this, BsonString_t2875117585 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonRegex::get_Type()
extern "C"  int8_t BsonRegex_get_Type_m3629121914 (BsonRegex_t453576629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
