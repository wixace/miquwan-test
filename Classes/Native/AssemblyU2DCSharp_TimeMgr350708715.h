﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Single>
struct Dictionary_2_t817370046;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeMgr
struct  TimeMgr_t350708715  : public Il2CppObject
{
public:
	// System.Single TimeMgr::timeScale
	float ___timeScale_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> TimeMgr::timeEvents
	Dictionary_2_t817370046 * ___timeEvents_6;
	// System.Double TimeMgr::_serverTime
	double ____serverTime_7;
	// System.Single TimeMgr::_startTime
	float ____startTime_8;
	// System.Double TimeMgr::_serverStartTime
	double ____serverStartTime_9;
	// System.Double TimeMgr::_mergeServerTime
	double ____mergeServerTime_10;

public:
	inline static int32_t get_offset_of_timeScale_5() { return static_cast<int32_t>(offsetof(TimeMgr_t350708715, ___timeScale_5)); }
	inline float get_timeScale_5() const { return ___timeScale_5; }
	inline float* get_address_of_timeScale_5() { return &___timeScale_5; }
	inline void set_timeScale_5(float value)
	{
		___timeScale_5 = value;
	}

	inline static int32_t get_offset_of_timeEvents_6() { return static_cast<int32_t>(offsetof(TimeMgr_t350708715, ___timeEvents_6)); }
	inline Dictionary_2_t817370046 * get_timeEvents_6() const { return ___timeEvents_6; }
	inline Dictionary_2_t817370046 ** get_address_of_timeEvents_6() { return &___timeEvents_6; }
	inline void set_timeEvents_6(Dictionary_2_t817370046 * value)
	{
		___timeEvents_6 = value;
		Il2CppCodeGenWriteBarrier(&___timeEvents_6, value);
	}

	inline static int32_t get_offset_of__serverTime_7() { return static_cast<int32_t>(offsetof(TimeMgr_t350708715, ____serverTime_7)); }
	inline double get__serverTime_7() const { return ____serverTime_7; }
	inline double* get_address_of__serverTime_7() { return &____serverTime_7; }
	inline void set__serverTime_7(double value)
	{
		____serverTime_7 = value;
	}

	inline static int32_t get_offset_of__startTime_8() { return static_cast<int32_t>(offsetof(TimeMgr_t350708715, ____startTime_8)); }
	inline float get__startTime_8() const { return ____startTime_8; }
	inline float* get_address_of__startTime_8() { return &____startTime_8; }
	inline void set__startTime_8(float value)
	{
		____startTime_8 = value;
	}

	inline static int32_t get_offset_of__serverStartTime_9() { return static_cast<int32_t>(offsetof(TimeMgr_t350708715, ____serverStartTime_9)); }
	inline double get__serverStartTime_9() const { return ____serverStartTime_9; }
	inline double* get_address_of__serverStartTime_9() { return &____serverStartTime_9; }
	inline void set__serverStartTime_9(double value)
	{
		____serverStartTime_9 = value;
	}

	inline static int32_t get_offset_of__mergeServerTime_10() { return static_cast<int32_t>(offsetof(TimeMgr_t350708715, ____mergeServerTime_10)); }
	inline double get__mergeServerTime_10() const { return ____mergeServerTime_10; }
	inline double* get_address_of__mergeServerTime_10() { return &____mergeServerTime_10; }
	inline void set__mergeServerTime_10(double value)
	{
		____mergeServerTime_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
