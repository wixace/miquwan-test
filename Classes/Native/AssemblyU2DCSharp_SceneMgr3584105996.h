﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// LoadEvent
struct LoadEvent_t3369598260;
// System.Diagnostics.Stopwatch
struct Stopwatch_t3420517611;
// Mihua.Asset.ABLoadOperation.ABOperation
struct ABOperation_t1894014760;
// checkpointCfg
struct checkpointCfg_t2816107964;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneMgr
struct  SceneMgr_t3584105996  : public Il2CppObject
{
public:
	// System.Int32 SceneMgr::requestID
	int32_t ___requestID_7;
	// System.Collections.Generic.List`1<System.String> SceneMgr::assetList
	List_1_t1375417109 * ___assetList_8;
	// System.String SceneMgr::curLoadName
	String_t* ___curLoadName_9;
	// System.Single SceneMgr::minTime
	float ___minTime_10;
	// System.Single SceneMgr::curTime
	float ___curTime_11;
	// System.Single SceneMgr::toProgress
	float ___toProgress_12;
	// System.Single SceneMgr::displayProgress
	float ___displayProgress_13;
	// System.Boolean SceneMgr::isinit
	bool ___isinit_14;
	// System.Single SceneMgr::lastTime
	float ___lastTime_15;
	// LoadEvent SceneMgr::loadEvent
	LoadEvent_t3369598260 * ___loadEvent_16;
	// System.Boolean SceneMgr::isLoaedRes
	bool ___isLoaedRes_17;
	// System.Boolean SceneMgr::isLoaedScene
	bool ___isLoaedScene_18;
	// System.Diagnostics.Stopwatch SceneMgr::loadStopwatch
	Stopwatch_t3420517611 * ___loadStopwatch_19;
	// Mihua.Asset.ABLoadOperation.ABOperation SceneMgr::op
	ABOperation_t1894014760 * ___op_20;
	// System.String SceneMgr::<curSceneName>k__BackingField
	String_t* ___U3CcurSceneNameU3Ek__BackingField_21;
	// checkpointCfg SceneMgr::<checkPoint>k__BackingField
	checkpointCfg_t2816107964 * ___U3CcheckPointU3Ek__BackingField_22;
	// System.Boolean SceneMgr::<isloading>k__BackingField
	bool ___U3CisloadingU3Ek__BackingField_23;

public:
	inline static int32_t get_offset_of_requestID_7() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996, ___requestID_7)); }
	inline int32_t get_requestID_7() const { return ___requestID_7; }
	inline int32_t* get_address_of_requestID_7() { return &___requestID_7; }
	inline void set_requestID_7(int32_t value)
	{
		___requestID_7 = value;
	}

	inline static int32_t get_offset_of_assetList_8() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996, ___assetList_8)); }
	inline List_1_t1375417109 * get_assetList_8() const { return ___assetList_8; }
	inline List_1_t1375417109 ** get_address_of_assetList_8() { return &___assetList_8; }
	inline void set_assetList_8(List_1_t1375417109 * value)
	{
		___assetList_8 = value;
		Il2CppCodeGenWriteBarrier(&___assetList_8, value);
	}

	inline static int32_t get_offset_of_curLoadName_9() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996, ___curLoadName_9)); }
	inline String_t* get_curLoadName_9() const { return ___curLoadName_9; }
	inline String_t** get_address_of_curLoadName_9() { return &___curLoadName_9; }
	inline void set_curLoadName_9(String_t* value)
	{
		___curLoadName_9 = value;
		Il2CppCodeGenWriteBarrier(&___curLoadName_9, value);
	}

	inline static int32_t get_offset_of_minTime_10() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996, ___minTime_10)); }
	inline float get_minTime_10() const { return ___minTime_10; }
	inline float* get_address_of_minTime_10() { return &___minTime_10; }
	inline void set_minTime_10(float value)
	{
		___minTime_10 = value;
	}

	inline static int32_t get_offset_of_curTime_11() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996, ___curTime_11)); }
	inline float get_curTime_11() const { return ___curTime_11; }
	inline float* get_address_of_curTime_11() { return &___curTime_11; }
	inline void set_curTime_11(float value)
	{
		___curTime_11 = value;
	}

	inline static int32_t get_offset_of_toProgress_12() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996, ___toProgress_12)); }
	inline float get_toProgress_12() const { return ___toProgress_12; }
	inline float* get_address_of_toProgress_12() { return &___toProgress_12; }
	inline void set_toProgress_12(float value)
	{
		___toProgress_12 = value;
	}

	inline static int32_t get_offset_of_displayProgress_13() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996, ___displayProgress_13)); }
	inline float get_displayProgress_13() const { return ___displayProgress_13; }
	inline float* get_address_of_displayProgress_13() { return &___displayProgress_13; }
	inline void set_displayProgress_13(float value)
	{
		___displayProgress_13 = value;
	}

	inline static int32_t get_offset_of_isinit_14() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996, ___isinit_14)); }
	inline bool get_isinit_14() const { return ___isinit_14; }
	inline bool* get_address_of_isinit_14() { return &___isinit_14; }
	inline void set_isinit_14(bool value)
	{
		___isinit_14 = value;
	}

	inline static int32_t get_offset_of_lastTime_15() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996, ___lastTime_15)); }
	inline float get_lastTime_15() const { return ___lastTime_15; }
	inline float* get_address_of_lastTime_15() { return &___lastTime_15; }
	inline void set_lastTime_15(float value)
	{
		___lastTime_15 = value;
	}

	inline static int32_t get_offset_of_loadEvent_16() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996, ___loadEvent_16)); }
	inline LoadEvent_t3369598260 * get_loadEvent_16() const { return ___loadEvent_16; }
	inline LoadEvent_t3369598260 ** get_address_of_loadEvent_16() { return &___loadEvent_16; }
	inline void set_loadEvent_16(LoadEvent_t3369598260 * value)
	{
		___loadEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___loadEvent_16, value);
	}

	inline static int32_t get_offset_of_isLoaedRes_17() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996, ___isLoaedRes_17)); }
	inline bool get_isLoaedRes_17() const { return ___isLoaedRes_17; }
	inline bool* get_address_of_isLoaedRes_17() { return &___isLoaedRes_17; }
	inline void set_isLoaedRes_17(bool value)
	{
		___isLoaedRes_17 = value;
	}

	inline static int32_t get_offset_of_isLoaedScene_18() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996, ___isLoaedScene_18)); }
	inline bool get_isLoaedScene_18() const { return ___isLoaedScene_18; }
	inline bool* get_address_of_isLoaedScene_18() { return &___isLoaedScene_18; }
	inline void set_isLoaedScene_18(bool value)
	{
		___isLoaedScene_18 = value;
	}

	inline static int32_t get_offset_of_loadStopwatch_19() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996, ___loadStopwatch_19)); }
	inline Stopwatch_t3420517611 * get_loadStopwatch_19() const { return ___loadStopwatch_19; }
	inline Stopwatch_t3420517611 ** get_address_of_loadStopwatch_19() { return &___loadStopwatch_19; }
	inline void set_loadStopwatch_19(Stopwatch_t3420517611 * value)
	{
		___loadStopwatch_19 = value;
		Il2CppCodeGenWriteBarrier(&___loadStopwatch_19, value);
	}

	inline static int32_t get_offset_of_op_20() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996, ___op_20)); }
	inline ABOperation_t1894014760 * get_op_20() const { return ___op_20; }
	inline ABOperation_t1894014760 ** get_address_of_op_20() { return &___op_20; }
	inline void set_op_20(ABOperation_t1894014760 * value)
	{
		___op_20 = value;
		Il2CppCodeGenWriteBarrier(&___op_20, value);
	}

	inline static int32_t get_offset_of_U3CcurSceneNameU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996, ___U3CcurSceneNameU3Ek__BackingField_21)); }
	inline String_t* get_U3CcurSceneNameU3Ek__BackingField_21() const { return ___U3CcurSceneNameU3Ek__BackingField_21; }
	inline String_t** get_address_of_U3CcurSceneNameU3Ek__BackingField_21() { return &___U3CcurSceneNameU3Ek__BackingField_21; }
	inline void set_U3CcurSceneNameU3Ek__BackingField_21(String_t* value)
	{
		___U3CcurSceneNameU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcurSceneNameU3Ek__BackingField_21, value);
	}

	inline static int32_t get_offset_of_U3CcheckPointU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996, ___U3CcheckPointU3Ek__BackingField_22)); }
	inline checkpointCfg_t2816107964 * get_U3CcheckPointU3Ek__BackingField_22() const { return ___U3CcheckPointU3Ek__BackingField_22; }
	inline checkpointCfg_t2816107964 ** get_address_of_U3CcheckPointU3Ek__BackingField_22() { return &___U3CcheckPointU3Ek__BackingField_22; }
	inline void set_U3CcheckPointU3Ek__BackingField_22(checkpointCfg_t2816107964 * value)
	{
		___U3CcheckPointU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcheckPointU3Ek__BackingField_22, value);
	}

	inline static int32_t get_offset_of_U3CisloadingU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996, ___U3CisloadingU3Ek__BackingField_23)); }
	inline bool get_U3CisloadingU3Ek__BackingField_23() const { return ___U3CisloadingU3Ek__BackingField_23; }
	inline bool* get_address_of_U3CisloadingU3Ek__BackingField_23() { return &___U3CisloadingU3Ek__BackingField_23; }
	inline void set_U3CisloadingU3Ek__BackingField_23(bool value)
	{
		___U3CisloadingU3Ek__BackingField_23 = value;
	}
};

struct SceneMgr_t3584105996_StaticFields
{
public:
	// System.String SceneMgr::SCENE_LOAD_OK
	String_t* ___SCENE_LOAD_OK_0;
	// System.String SceneMgr::JUMP_STORY
	String_t* ___JUMP_STORY_1;
	// System.String SceneMgr::SCENE_START_LOAD
	String_t* ___SCENE_START_LOAD_2;
	// System.String SceneMgr::PROGRESS_UPDATE
	String_t* ___PROGRESS_UPDATE_3;
	// System.String SceneMgr::LOAD_OK
	String_t* ___LOAD_OK_4;
	// System.Int32 SceneMgr::SceneResPer
	int32_t ___SceneResPer_5;
	// System.Int32 SceneMgr::ScenePer
	int32_t ___ScenePer_6;

public:
	inline static int32_t get_offset_of_SCENE_LOAD_OK_0() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996_StaticFields, ___SCENE_LOAD_OK_0)); }
	inline String_t* get_SCENE_LOAD_OK_0() const { return ___SCENE_LOAD_OK_0; }
	inline String_t** get_address_of_SCENE_LOAD_OK_0() { return &___SCENE_LOAD_OK_0; }
	inline void set_SCENE_LOAD_OK_0(String_t* value)
	{
		___SCENE_LOAD_OK_0 = value;
		Il2CppCodeGenWriteBarrier(&___SCENE_LOAD_OK_0, value);
	}

	inline static int32_t get_offset_of_JUMP_STORY_1() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996_StaticFields, ___JUMP_STORY_1)); }
	inline String_t* get_JUMP_STORY_1() const { return ___JUMP_STORY_1; }
	inline String_t** get_address_of_JUMP_STORY_1() { return &___JUMP_STORY_1; }
	inline void set_JUMP_STORY_1(String_t* value)
	{
		___JUMP_STORY_1 = value;
		Il2CppCodeGenWriteBarrier(&___JUMP_STORY_1, value);
	}

	inline static int32_t get_offset_of_SCENE_START_LOAD_2() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996_StaticFields, ___SCENE_START_LOAD_2)); }
	inline String_t* get_SCENE_START_LOAD_2() const { return ___SCENE_START_LOAD_2; }
	inline String_t** get_address_of_SCENE_START_LOAD_2() { return &___SCENE_START_LOAD_2; }
	inline void set_SCENE_START_LOAD_2(String_t* value)
	{
		___SCENE_START_LOAD_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCENE_START_LOAD_2, value);
	}

	inline static int32_t get_offset_of_PROGRESS_UPDATE_3() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996_StaticFields, ___PROGRESS_UPDATE_3)); }
	inline String_t* get_PROGRESS_UPDATE_3() const { return ___PROGRESS_UPDATE_3; }
	inline String_t** get_address_of_PROGRESS_UPDATE_3() { return &___PROGRESS_UPDATE_3; }
	inline void set_PROGRESS_UPDATE_3(String_t* value)
	{
		___PROGRESS_UPDATE_3 = value;
		Il2CppCodeGenWriteBarrier(&___PROGRESS_UPDATE_3, value);
	}

	inline static int32_t get_offset_of_LOAD_OK_4() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996_StaticFields, ___LOAD_OK_4)); }
	inline String_t* get_LOAD_OK_4() const { return ___LOAD_OK_4; }
	inline String_t** get_address_of_LOAD_OK_4() { return &___LOAD_OK_4; }
	inline void set_LOAD_OK_4(String_t* value)
	{
		___LOAD_OK_4 = value;
		Il2CppCodeGenWriteBarrier(&___LOAD_OK_4, value);
	}

	inline static int32_t get_offset_of_SceneResPer_5() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996_StaticFields, ___SceneResPer_5)); }
	inline int32_t get_SceneResPer_5() const { return ___SceneResPer_5; }
	inline int32_t* get_address_of_SceneResPer_5() { return &___SceneResPer_5; }
	inline void set_SceneResPer_5(int32_t value)
	{
		___SceneResPer_5 = value;
	}

	inline static int32_t get_offset_of_ScenePer_6() { return static_cast<int32_t>(offsetof(SceneMgr_t3584105996_StaticFields, ___ScenePer_6)); }
	inline int32_t get_ScenePer_6() const { return ___ScenePer_6; }
	inline int32_t* get_address_of_ScenePer_6() { return &___ScenePer_6; }
	inline void set_ScenePer_6(int32_t value)
	{
		___ScenePer_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
