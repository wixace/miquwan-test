﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIBasicSprite
struct UIBasicSprite_t2501337439;
// BetterList`1<UnityEngine.Vector3>
struct BetterList_1_t1484067282;
// BetterList`1<UnityEngine.Vector2>
struct BetterList_1_t1484067281;
// BetterList`1<UnityEngine.Color32>
struct BetterList_1_t2095821700;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;
// UIWidget
struct UIWidget_t769069560;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIBasicSprite_Type741864970.h"
#include "AssemblyU2DCSharp_UIBasicSprite_Flip741435197.h"
#include "AssemblyU2DCSharp_UIBasicSprite_FillDirection3514300524.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UIBasicSprite2501337439.h"

// System.Void UIBasicSprite::.ctor()
extern "C"  void UIBasicSprite__ctor_m2527140588 (UIBasicSprite_t2501337439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::.cctor()
extern "C"  void UIBasicSprite__cctor_m549850689 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIBasicSprite/Type UIBasicSprite::get_type()
extern "C"  int32_t UIBasicSprite_get_type_m1197103272 (UIBasicSprite_t2501337439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::set_type(UIBasicSprite/Type)
extern "C"  void UIBasicSprite_set_type_m2103706149 (UIBasicSprite_t2501337439 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIBasicSprite/Flip UIBasicSprite::get_flip()
extern "C"  int32_t UIBasicSprite_get_flip_m3976253582 (UIBasicSprite_t2501337439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::set_flip(UIBasicSprite/Flip)
extern "C"  void UIBasicSprite_set_flip_m3437793541 (UIBasicSprite_t2501337439 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIBasicSprite/FillDirection UIBasicSprite::get_fillDirection()
extern "C"  int32_t UIBasicSprite_get_fillDirection_m2984301284 (UIBasicSprite_t2501337439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::set_fillDirection(UIBasicSprite/FillDirection)
extern "C"  void UIBasicSprite_set_fillDirection_m1195897437 (UIBasicSprite_t2501337439 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIBasicSprite::get_fillAmount()
extern "C"  float UIBasicSprite_get_fillAmount_m679518448 (UIBasicSprite_t2501337439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::set_fillAmount(System.Single)
extern "C"  void UIBasicSprite_set_fillAmount_m1926889979 (UIBasicSprite_t2501337439 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIBasicSprite::get_minWidth()
extern "C"  int32_t UIBasicSprite_get_minWidth_m2338180231 (UIBasicSprite_t2501337439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIBasicSprite::get_minHeight()
extern "C"  int32_t UIBasicSprite_get_minHeight_m4267924776 (UIBasicSprite_t2501337439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIBasicSprite::get_invert()
extern "C"  bool UIBasicSprite_get_invert_m2709338467 (UIBasicSprite_t2501337439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::set_invert(System.Boolean)
extern "C"  void UIBasicSprite_set_invert_m2614820674 (UIBasicSprite_t2501337439 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIBasicSprite::get_hasBorder()
extern "C"  bool UIBasicSprite_get_hasBorder_m529620315 (UIBasicSprite_t2501337439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIBasicSprite::get_premultipliedAlpha()
extern "C"  bool UIBasicSprite_get_premultipliedAlpha_m3752343925 (UIBasicSprite_t2501337439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIBasicSprite::get_pixelSize()
extern "C"  float UIBasicSprite_get_pixelSize_m3171688820 (UIBasicSprite_t2501337439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UIBasicSprite::get_drawingUVs()
extern "C"  Vector4_t4282066567  UIBasicSprite_get_drawingUVs_m1714057628 (UIBasicSprite_t2501337439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32 UIBasicSprite::get_drawingColor()
extern "C"  Color32_t598853688  UIBasicSprite_get_drawingColor_m3942065630 (UIBasicSprite_t2501337439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::Fill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>,UnityEngine.Rect,UnityEngine.Rect)
extern "C"  void UIBasicSprite_Fill_m366886202 (UIBasicSprite_t2501337439 * __this, BetterList_1_t1484067282 * ___verts0, BetterList_1_t1484067281 * ___uvs1, BetterList_1_t2095821700 * ___cols2, Rect_t4241904616  ___outer3, Rect_t4241904616  ___inner4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::SimpleFill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void UIBasicSprite_SimpleFill_m3286611080 (UIBasicSprite_t2501337439 * __this, BetterList_1_t1484067282 * ___verts0, BetterList_1_t1484067281 * ___uvs1, BetterList_1_t2095821700 * ___cols2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::SlicedFill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void UIBasicSprite_SlicedFill_m2141459752 (UIBasicSprite_t2501337439 * __this, BetterList_1_t1484067282 * ___verts0, BetterList_1_t1484067281 * ___uvs1, BetterList_1_t2095821700 * ___cols2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::TiledFill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void UIBasicSprite_TiledFill_m2413675410 (UIBasicSprite_t2501337439 * __this, BetterList_1_t1484067282 * ___verts0, BetterList_1_t1484067281 * ___uvs1, BetterList_1_t2095821700 * ___cols2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::FilledFill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void UIBasicSprite_FilledFill_m3915664088 (UIBasicSprite_t2501337439 * __this, BetterList_1_t1484067282 * ___verts0, BetterList_1_t1484067281 * ___uvs1, BetterList_1_t2095821700 * ___cols2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::AdvancedFill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void UIBasicSprite_AdvancedFill_m1585678808 (UIBasicSprite_t2501337439 * __this, BetterList_1_t1484067282 * ___verts0, BetterList_1_t1484067281 * ___uvs1, BetterList_1_t2095821700 * ___cols2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIBasicSprite::RadialCut(UnityEngine.Vector2[],UnityEngine.Vector2[],System.Single,System.Boolean,System.Int32)
extern "C"  bool UIBasicSprite_RadialCut_m2873244104 (Il2CppObject * __this /* static, unused */, Vector2U5BU5D_t4024180168* ___xy0, Vector2U5BU5D_t4024180168* ___uv1, float ___fill2, bool ___invert3, int32_t ___corner4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::RadialCut(UnityEngine.Vector2[],System.Single,System.Single,System.Boolean,System.Int32)
extern "C"  void UIBasicSprite_RadialCut_m3418604943 (Il2CppObject * __this /* static, unused */, Vector2U5BU5D_t4024180168* ___xy0, float ___cos1, float ___sin2, bool ___invert3, int32_t ___corner4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::Fill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,UnityEngine.Color)
extern "C"  void UIBasicSprite_Fill_m3528246370 (Il2CppObject * __this /* static, unused */, BetterList_1_t1484067282 * ___verts0, BetterList_1_t1484067281 * ___uvs1, BetterList_1_t2095821700 * ___cols2, float ___v0x3, float ___v1x4, float ___v0y5, float ___v1y6, float ___u0x7, float ___u1x8, float ___u0y9, float ___u1y10, Color_t4194546905  ___col11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::ilo_MarkAsChanged1(UIWidget)
extern "C"  void UIBasicSprite_ilo_MarkAsChanged1_m3285983695 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIBasicSprite/Type UIBasicSprite::ilo_get_type2(UIBasicSprite)
extern "C"  int32_t UIBasicSprite_ilo_get_type2_m287883514 (Il2CppObject * __this /* static, unused */, UIBasicSprite_t2501337439 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UIBasicSprite::ilo_get_border3(UIWidget)
extern "C"  Vector4_t4282066567  UIBasicSprite_ilo_get_border3_m2561573206 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIBasicSprite::ilo_get_minHeight4(UIWidget)
extern "C"  int32_t UIBasicSprite_ilo_get_minHeight4_m1718781353 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UIBasicSprite::ilo_get_color5(UIWidget)
extern "C"  Color_t4194546905  UIBasicSprite_ilo_get_color5_m1033627399 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIBasicSprite::ilo_get_premultipliedAlpha6(UIBasicSprite)
extern "C"  bool UIBasicSprite_ilo_get_premultipliedAlpha6_m2866622915 (Il2CppObject * __this /* static, unused */, UIBasicSprite_t2501337439 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UIBasicSprite::ilo_ApplyPMA7(UnityEngine.Color)
extern "C"  Color_t4194546905  UIBasicSprite_ilo_ApplyPMA7_m3368013831 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::ilo_SlicedFill8(UIBasicSprite,BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void UIBasicSprite_ilo_SlicedFill8_m4088081960 (Il2CppObject * __this /* static, unused */, UIBasicSprite_t2501337439 * ____this0, BetterList_1_t1484067282 * ___verts1, BetterList_1_t1484067281 * ___uvs2, BetterList_1_t2095821700 * ___cols3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UIBasicSprite::ilo_get_drawingDimensions9(UIWidget)
extern "C"  Vector4_t4282066567  UIBasicSprite_ilo_get_drawingDimensions9_m956345681 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UIBasicSprite::ilo_get_drawingUVs10(UIBasicSprite)
extern "C"  Vector4_t4282066567  UIBasicSprite_ilo_get_drawingUVs10_m2661462341 (Il2CppObject * __this /* static, unused */, UIBasicSprite_t2501337439 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::ilo_SimpleFill11(UIBasicSprite,BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void UIBasicSprite_ilo_SimpleFill11_m1417357570 (Il2CppObject * __this /* static, unused */, UIBasicSprite_t2501337439 * ____this0, BetterList_1_t1484067282 * ___verts1, BetterList_1_t1484067281 * ___uvs2, BetterList_1_t2095821700 * ___cols3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32 UIBasicSprite::ilo_get_drawingColor12(UIBasicSprite)
extern "C"  Color32_t598853688  UIBasicSprite_ilo_get_drawingColor12_m2406266305 (Il2CppObject * __this /* static, unused */, UIBasicSprite_t2501337439 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIBasicSprite::ilo_get_pixelSize13(UIBasicSprite)
extern "C"  float UIBasicSprite_ilo_get_pixelSize13_m1274362640 (Il2CppObject * __this /* static, unused */, UIBasicSprite_t2501337439 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIBasicSprite::ilo_RepeatIndex14(System.Int32,System.Int32)
extern "C"  int32_t UIBasicSprite_ilo_RepeatIndex14_m1426342823 (Il2CppObject * __this /* static, unused */, int32_t ___val0, int32_t ___max1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIBasicSprite::ilo_RadialCut15(UnityEngine.Vector2[],UnityEngine.Vector2[],System.Single,System.Boolean,System.Int32)
extern "C"  bool UIBasicSprite_ilo_RadialCut15_m1610920727 (Il2CppObject * __this /* static, unused */, Vector2U5BU5D_t4024180168* ___xy0, Vector2U5BU5D_t4024180168* ___uv1, float ___fill2, bool ___invert3, int32_t ___corner4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::ilo_Fill16(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,UnityEngine.Color)
extern "C"  void UIBasicSprite_ilo_Fill16_m1276993354 (Il2CppObject * __this /* static, unused */, BetterList_1_t1484067282 * ___verts0, BetterList_1_t1484067281 * ___uvs1, BetterList_1_t2095821700 * ___cols2, float ___v0x3, float ___v1x4, float ___v0y5, float ___v1y6, float ___u0x7, float ___u1x8, float ___u0y9, float ___u1y10, Color_t4194546905  ___col11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::ilo_RadialCut17(UnityEngine.Vector2[],System.Single,System.Single,System.Boolean,System.Int32)
extern "C"  void UIBasicSprite_ilo_RadialCut17_m2294420124 (Il2CppObject * __this /* static, unused */, Vector2U5BU5D_t4024180168* ___xy0, float ___cos1, float ___sin2, bool ___invert3, int32_t ___corner4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
