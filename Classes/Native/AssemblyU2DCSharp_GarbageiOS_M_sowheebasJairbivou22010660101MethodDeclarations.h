﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_sowheebasJairbivou29
struct M_sowheebasJairbivou29_t2010660101;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_sowheebasJairbivou22010660101.h"

// System.Void GarbageiOS.M_sowheebasJairbivou29::.ctor()
extern "C"  void M_sowheebasJairbivou29__ctor_m1494311630 (M_sowheebasJairbivou29_t2010660101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sowheebasJairbivou29::M_youcow0(System.String[],System.Int32)
extern "C"  void M_sowheebasJairbivou29_M_youcow0_m800874229 (M_sowheebasJairbivou29_t2010660101 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sowheebasJairbivou29::M_draytruso1(System.String[],System.Int32)
extern "C"  void M_sowheebasJairbivou29_M_draytruso1_m2689194883 (M_sowheebasJairbivou29_t2010660101 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sowheebasJairbivou29::M_meredame2(System.String[],System.Int32)
extern "C"  void M_sowheebasJairbivou29_M_meredame2_m1137379491 (M_sowheebasJairbivou29_t2010660101 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sowheebasJairbivou29::M_favawkeNaga3(System.String[],System.Int32)
extern "C"  void M_sowheebasJairbivou29_M_favawkeNaga3_m2832813978 (M_sowheebasJairbivou29_t2010660101 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sowheebasJairbivou29::ilo_M_youcow01(GarbageiOS.M_sowheebasJairbivou29,System.String[],System.Int32)
extern "C"  void M_sowheebasJairbivou29_ilo_M_youcow01_m3191185744 (Il2CppObject * __this /* static, unused */, M_sowheebasJairbivou29_t2010660101 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
