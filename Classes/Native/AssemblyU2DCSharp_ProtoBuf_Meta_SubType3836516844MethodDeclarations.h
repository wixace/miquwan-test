﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.SubType
struct SubType_t3836516844;
// ProtoBuf.Meta.MetaType
struct MetaType_t448283965;
// ProtoBuf.Serializers.IProtoSerializer
struct IProtoSerializer_t3033312651;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_MetaType448283965.h"
#include "AssemblyU2DCSharp_ProtoBuf_DataFormat274207885.h"

// System.Void ProtoBuf.Meta.SubType::.ctor(System.Int32,ProtoBuf.Meta.MetaType,ProtoBuf.DataFormat)
extern "C"  void SubType__ctor_m1997143649 (SubType_t3836516844 * __this, int32_t ___fieldNumber0, MetaType_t448283965 * ___derivedType1, int32_t ___format2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.SubType::get_FieldNumber()
extern "C"  int32_t SubType_get_FieldNumber_m3017116381 (SubType_t3836516844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.MetaType ProtoBuf.Meta.SubType::get_DerivedType()
extern "C"  MetaType_t448283965 * SubType_get_DerivedType_m2469943547 (SubType_t3836516844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Serializers.IProtoSerializer ProtoBuf.Meta.SubType::get_Serializer()
extern "C"  Il2CppObject * SubType_get_Serializer_m1801850294 (SubType_t3836516844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Serializers.IProtoSerializer ProtoBuf.Meta.SubType::BuildSerializer()
extern "C"  Il2CppObject * SubType_BuildSerializer_m255444359 (SubType_t3836516844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
