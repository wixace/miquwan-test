﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PerTest
struct PerTest_t985279279;

#include "codegen/il2cpp-codegen.h"

// System.Void PerTest::.ctor()
extern "C"  void PerTest__ctor_m405481756 (PerTest_t985279279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
