﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_lusearmi90
struct M_lusearmi90_t1515270671;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_lusearmi901515270671.h"

// System.Void GarbageiOS.M_lusearmi90::.ctor()
extern "C"  void M_lusearmi90__ctor_m2764831492 (M_lusearmi90_t1515270671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lusearmi90::M_hecheyalLerxalaw0(System.String[],System.Int32)
extern "C"  void M_lusearmi90_M_hecheyalLerxalaw0_m3331938070 (M_lusearmi90_t1515270671 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lusearmi90::M_surwawlo1(System.String[],System.Int32)
extern "C"  void M_lusearmi90_M_surwawlo1_m1200371032 (M_lusearmi90_t1515270671 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lusearmi90::M_jefereStalbou2(System.String[],System.Int32)
extern "C"  void M_lusearmi90_M_jefereStalbou2_m3934180236 (M_lusearmi90_t1515270671 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lusearmi90::M_yowsuweNilow3(System.String[],System.Int32)
extern "C"  void M_lusearmi90_M_yowsuweNilow3_m2932891730 (M_lusearmi90_t1515270671 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lusearmi90::ilo_M_surwawlo11(GarbageiOS.M_lusearmi90,System.String[],System.Int32)
extern "C"  void M_lusearmi90_ilo_M_surwawlo11_m351120599 (Il2CppObject * __this /* static, unused */, M_lusearmi90_t1515270671 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
