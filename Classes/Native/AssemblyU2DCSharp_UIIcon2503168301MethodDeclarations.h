﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIIcon
struct UIIcon_t2503168301;

#include "codegen/il2cpp-codegen.h"

// System.Void UIIcon::.ctor()
extern "C"  void UIIcon__ctor_m237508110 (UIIcon_t2503168301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIIcon::Start()
extern "C"  void UIIcon_Start_m3479613198 (UIIcon_t2503168301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIIcon::<Start>m__48B(System.Boolean)
extern "C"  void UIIcon_U3CStartU3Em__48B_m4100293196 (UIIcon_t2503168301 * __this, bool ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
