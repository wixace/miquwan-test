﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_black_over
struct Float_black_over_t1090335479;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// FloatTextUnit
struct FloatTextUnit_t2362298029;
// EventDelegate/Callback
struct Callback_t1094463061;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_UITweener_Method1078127180.h"

// System.Void Float_black_over::.ctor()
extern "C"  void Float_black_over__ctor_m2129418628 (Float_black_over_t1090335479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_black_over::FloatID()
extern "C"  int32_t Float_black_over_FloatID_m3706902480 (Float_black_over_t1090335479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_black_over::Init()
extern "C"  void Float_black_over_Init_m3899552848 (Float_black_over_t1090335479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_black_over::SetFile(System.Object[])
extern "C"  void Float_black_over_SetFile_m2250637266 (Float_black_over_t1090335479 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_black_over::ilo_MoveAlpha1(FloatTextUnit,System.Single,System.Single,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void Float_black_over_ilo_MoveAlpha1_m2674796971 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, float ___from1, float ___to2, float ___duration3, float ___delay4, Callback_t1094463061 * ___call5, GameObject_t3674682005 * ___go6, int32_t ___method7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
