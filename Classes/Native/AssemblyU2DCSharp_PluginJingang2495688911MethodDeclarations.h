﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginJingang
struct PluginJingang_t2495688911;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PluginJingang2495688911.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginJingang::.ctor()
extern "C"  void PluginJingang__ctor_m1739696508 (PluginJingang_t2495688911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::Init()
extern "C"  void PluginJingang_Init_m977487192 (PluginJingang_t2495688911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginJingang::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginJingang_ReqSDKHttpLogin_m1468487789 (PluginJingang_t2495688911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginJingang::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginJingang_IsLoginSuccess_m623026031 (PluginJingang_t2495688911 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::OpenUserLogin()
extern "C"  void PluginJingang_OpenUserLogin_m1591185358 (PluginJingang_t2495688911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::UserPay(CEvent.ZEvent)
extern "C"  void PluginJingang_UserPay_m3320791012 (PluginJingang_t2495688911 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::OnInitSuccess(System.String)
extern "C"  void PluginJingang_OnInitSuccess_m1714807732 (PluginJingang_t2495688911 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::OnInitFail(System.String)
extern "C"  void PluginJingang_OnInitFail_m2660583949 (PluginJingang_t2495688911 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::OnLoginSuccess(System.String)
extern "C"  void PluginJingang_OnLoginSuccess_m2009144193 (PluginJingang_t2495688911 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::RolelvUpinfo(CEvent.ZEvent)
extern "C"  void PluginJingang_RolelvUpinfo_m179370666 (PluginJingang_t2495688911 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::OnLoginFail(System.String)
extern "C"  void PluginJingang_OnLoginFail_m3798527328 (PluginJingang_t2495688911 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::OnUserSwitchSuccess(System.String)
extern "C"  void PluginJingang_OnUserSwitchSuccess_m190408643 (PluginJingang_t2495688911 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::OnUserSwitchFail(System.String)
extern "C"  void PluginJingang_OnUserSwitchFail_m3747862622 (PluginJingang_t2495688911 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::OnPaySuccess(System.String)
extern "C"  void PluginJingang_OnPaySuccess_m2005559808 (PluginJingang_t2495688911 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::OnPayFail(System.String)
extern "C"  void PluginJingang_OnPayFail_m574310209 (PluginJingang_t2495688911 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::OnExitGameSuccess(System.String)
extern "C"  void PluginJingang_OnExitGameSuccess_m720342484 (PluginJingang_t2495688911 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::OnExitGameFail(System.String)
extern "C"  void PluginJingang_OnExitGameFail_m2611532781 (PluginJingang_t2495688911 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::OnLogoutSuccess(System.String)
extern "C"  void PluginJingang_OnLogoutSuccess_m2157470510 (PluginJingang_t2495688911 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::OnLogoutFail(System.String)
extern "C"  void PluginJingang_OnLogoutFail_m1603112147 (PluginJingang_t2495688911 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::InitJingang(System.String,System.String,System.String)
extern "C"  void PluginJingang_InitJingang_m3184216366 (PluginJingang_t2495688911 * __this, String_t* ___gameid0, String_t* ___gameKey1, String_t* ___channelID2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::LoginSdk()
extern "C"  void PluginJingang_LoginSdk_m123525273 (PluginJingang_t2495688911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::Role(System.String,System.String,System.String,System.String)
extern "C"  void PluginJingang_Role_m1909505336 (PluginJingang_t2495688911 * __this, String_t* ___roleId0, String_t* ___roleName1, String_t* ___roleLevel2, String_t* ___zoneId3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginJingang_pay_m885564544 (PluginJingang_t2495688911 * __this, String_t* ___money0, String_t* ___serverId1, String_t* ___gold2, String_t* ___roleid3, String_t* ___rolename4, String_t* ___orderid5, String_t* ___productid6, String_t* ___productname7, String_t* ___extra8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::<OnLogoutSuccess>m__432()
extern "C"  void PluginJingang_U3COnLogoutSuccessU3Em__432_m379562688 (PluginJingang_t2495688911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginJingang_ilo_AddEventListener1_m1678706680 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginJingang::ilo_get_PluginsSdkMgr2()
extern "C"  PluginsSdkMgr_t3884624670 * PluginJingang_ilo_get_PluginsSdkMgr2_m2692107683 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::ilo_OpenUserLogin3(PluginJingang)
extern "C"  void PluginJingang_ilo_OpenUserLogin3_m2728740105 (Il2CppObject * __this /* static, unused */, PluginJingang_t2495688911 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.PayInfo PluginJingang::ilo_Parse4(System.Object[])
extern "C"  PayInfo_t1775308120 * PluginJingang_ilo_Parse4_m176270877 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___payOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::ilo_ReqSDKHttpLogin5(PluginsSdkMgr)
extern "C"  void PluginJingang_ilo_ReqSDKHttpLogin5_m1553113609 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::ilo_DispatchEvent6(CEvent.ZEvent)
extern "C"  void PluginJingang_ilo_DispatchEvent6_m439750506 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::ilo_Log7(System.Object,System.Boolean)
extern "C"  void PluginJingang_ilo_Log7_m1056128983 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJingang::ilo_Logout8(System.Action)
extern "C"  void PluginJingang_ilo_Logout8_m3179962408 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
