﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleShopManager
struct SimpleShopManager_t1021689509;

#include "codegen/il2cpp-codegen.h"

// System.Void SimpleShopManager::.ctor()
extern "C"  void SimpleShopManager__ctor_m31037542 (SimpleShopManager_t1021689509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleShopManager::Buy(System.Int32)
extern "C"  void SimpleShopManager_Buy_m2968931739 (SimpleShopManager_t1021689509 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
