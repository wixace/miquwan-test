﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUIGenerated/<GUI_ModalWindow_GetDelegate_member55_arg2>c__AnonStoreyE9
struct U3CGUI_ModalWindow_GetDelegate_member55_arg2U3Ec__AnonStoreyE9_t3541890859;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_GUIGenerated/<GUI_ModalWindow_GetDelegate_member55_arg2>c__AnonStoreyE9::.ctor()
extern "C"  void U3CGUI_ModalWindow_GetDelegate_member55_arg2U3Ec__AnonStoreyE9__ctor_m120700064 (U3CGUI_ModalWindow_GetDelegate_member55_arg2U3Ec__AnonStoreyE9_t3541890859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated/<GUI_ModalWindow_GetDelegate_member55_arg2>c__AnonStoreyE9::<>m__1BC(System.Int32)
extern "C"  void U3CGUI_ModalWindow_GetDelegate_member55_arg2U3Ec__AnonStoreyE9_U3CU3Em__1BC_m1659695132 (U3CGUI_ModalWindow_GetDelegate_member55_arg2U3Ec__AnonStoreyE9_t3541890859 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
