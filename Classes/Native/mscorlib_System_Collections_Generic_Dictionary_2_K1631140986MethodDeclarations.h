﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1499758504MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1950709300(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1631140986 *, Dictionary_2_t4381535 *, const MethodInfo*))KeyCollection__ctor_m4137106658_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.String>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1903415970(__this, ___item0, method) ((  void (*) (KeyCollection_t1631140986 *, uint32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3142588212_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.String>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3295819929(__this, method) ((  void (*) (KeyCollection_t1631140986 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m660240811_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.String>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m920777128(__this, ___item0, method) ((  bool (*) (KeyCollection_t1631140986 *, uint32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3107174486_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.String>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1355721997(__this, ___item0, method) ((  bool (*) (KeyCollection_t1631140986 *, uint32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2292074299_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.String>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m177578325(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1631140986 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3531386983_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m2676948875(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1631140986 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m4032050589_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3569224646(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1631140986 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2531852888_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.String>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2984615689(__this, method) ((  bool (*) (KeyCollection_t1631140986 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3873468983_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m820522747(__this, method) ((  bool (*) (KeyCollection_t1631140986 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4199187369_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.String>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m4284445159(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1631140986 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3281150229_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.String>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m955821289(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1631140986 *, UInt32U5BU5D_t3230734560*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2183179159_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.String>::GetEnumerator()
#define KeyCollection_GetEnumerator_m246886476(__this, method) ((  Enumerator_t619317589  (*) (KeyCollection_t1631140986 *, const MethodInfo*))KeyCollection_GetEnumerator_m4267000826_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,System.String>::get_Count()
#define KeyCollection_get_Count_m2799471553(__this, method) ((  int32_t (*) (KeyCollection_t1631140986 *, const MethodInfo*))KeyCollection_get_Count_m168011375_gshared)(__this, method)
