﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type[]
struct TypeU5BU5D_t3339007067;
// System.Xml.XmlNameTable
struct XmlNameTable_t1216706026;
// System.String
struct String_t;
// System.Xml.XmlImplementation
struct XmlImplementation_t3716119739;
// System.Xml.XmlResolver
struct XmlResolver_t3822670287;
// System.Collections.Hashtable
struct Hashtable_t1407064410;
// System.Xml.XmlNameEntryCache
struct XmlNameEntryCache_t3943949348;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t901819716;
// System.Xml.XmlAttribute
struct XmlAttribute_t6647939;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_t4125473774;
// System.Xml.Schema.IXmlSchemaInfo
struct IXmlSchemaInfo_t4085280001;
// System.Xml.XmlNodeChangedEventHandler
struct XmlNodeChangedEventHandler_t3074502249;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;

#include "System_Xml_System_Xml_XmlNode856910923.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocument
struct  XmlDocument_t730752740  : public XmlNode_t856910923
{
public:
	// System.Boolean System.Xml.XmlDocument::optimal_create_element
	bool ___optimal_create_element_8;
	// System.Boolean System.Xml.XmlDocument::optimal_create_attribute
	bool ___optimal_create_attribute_9;
	// System.Xml.XmlNameTable System.Xml.XmlDocument::nameTable
	XmlNameTable_t1216706026 * ___nameTable_10;
	// System.String System.Xml.XmlDocument::baseURI
	String_t* ___baseURI_11;
	// System.Xml.XmlImplementation System.Xml.XmlDocument::implementation
	XmlImplementation_t3716119739 * ___implementation_12;
	// System.Boolean System.Xml.XmlDocument::preserveWhitespace
	bool ___preserveWhitespace_13;
	// System.Xml.XmlResolver System.Xml.XmlDocument::resolver
	XmlResolver_t3822670287 * ___resolver_14;
	// System.Collections.Hashtable System.Xml.XmlDocument::idTable
	Hashtable_t1407064410 * ___idTable_15;
	// System.Xml.XmlNameEntryCache System.Xml.XmlDocument::nameCache
	XmlNameEntryCache_t3943949348 * ___nameCache_16;
	// System.Xml.XmlLinkedNode System.Xml.XmlDocument::lastLinkedChild
	XmlLinkedNode_t901819716 * ___lastLinkedChild_17;
	// System.Xml.XmlAttribute System.Xml.XmlDocument::nsNodeXml
	XmlAttribute_t6647939 * ___nsNodeXml_18;
	// System.Xml.Schema.XmlSchemaSet System.Xml.XmlDocument::schemas
	XmlSchemaSet_t4125473774 * ___schemas_19;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlDocument::schemaInfo
	Il2CppObject * ___schemaInfo_20;
	// System.Boolean System.Xml.XmlDocument::loadMode
	bool ___loadMode_21;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeChanged
	XmlNodeChangedEventHandler_t3074502249 * ___NodeChanged_22;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeChanging
	XmlNodeChangedEventHandler_t3074502249 * ___NodeChanging_23;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeInserted
	XmlNodeChangedEventHandler_t3074502249 * ___NodeInserted_24;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeInserting
	XmlNodeChangedEventHandler_t3074502249 * ___NodeInserting_25;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeRemoved
	XmlNodeChangedEventHandler_t3074502249 * ___NodeRemoved_26;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeRemoving
	XmlNodeChangedEventHandler_t3074502249 * ___NodeRemoving_27;

public:
	inline static int32_t get_offset_of_optimal_create_element_8() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740, ___optimal_create_element_8)); }
	inline bool get_optimal_create_element_8() const { return ___optimal_create_element_8; }
	inline bool* get_address_of_optimal_create_element_8() { return &___optimal_create_element_8; }
	inline void set_optimal_create_element_8(bool value)
	{
		___optimal_create_element_8 = value;
	}

	inline static int32_t get_offset_of_optimal_create_attribute_9() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740, ___optimal_create_attribute_9)); }
	inline bool get_optimal_create_attribute_9() const { return ___optimal_create_attribute_9; }
	inline bool* get_address_of_optimal_create_attribute_9() { return &___optimal_create_attribute_9; }
	inline void set_optimal_create_attribute_9(bool value)
	{
		___optimal_create_attribute_9 = value;
	}

	inline static int32_t get_offset_of_nameTable_10() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740, ___nameTable_10)); }
	inline XmlNameTable_t1216706026 * get_nameTable_10() const { return ___nameTable_10; }
	inline XmlNameTable_t1216706026 ** get_address_of_nameTable_10() { return &___nameTable_10; }
	inline void set_nameTable_10(XmlNameTable_t1216706026 * value)
	{
		___nameTable_10 = value;
		Il2CppCodeGenWriteBarrier(&___nameTable_10, value);
	}

	inline static int32_t get_offset_of_baseURI_11() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740, ___baseURI_11)); }
	inline String_t* get_baseURI_11() const { return ___baseURI_11; }
	inline String_t** get_address_of_baseURI_11() { return &___baseURI_11; }
	inline void set_baseURI_11(String_t* value)
	{
		___baseURI_11 = value;
		Il2CppCodeGenWriteBarrier(&___baseURI_11, value);
	}

	inline static int32_t get_offset_of_implementation_12() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740, ___implementation_12)); }
	inline XmlImplementation_t3716119739 * get_implementation_12() const { return ___implementation_12; }
	inline XmlImplementation_t3716119739 ** get_address_of_implementation_12() { return &___implementation_12; }
	inline void set_implementation_12(XmlImplementation_t3716119739 * value)
	{
		___implementation_12 = value;
		Il2CppCodeGenWriteBarrier(&___implementation_12, value);
	}

	inline static int32_t get_offset_of_preserveWhitespace_13() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740, ___preserveWhitespace_13)); }
	inline bool get_preserveWhitespace_13() const { return ___preserveWhitespace_13; }
	inline bool* get_address_of_preserveWhitespace_13() { return &___preserveWhitespace_13; }
	inline void set_preserveWhitespace_13(bool value)
	{
		___preserveWhitespace_13 = value;
	}

	inline static int32_t get_offset_of_resolver_14() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740, ___resolver_14)); }
	inline XmlResolver_t3822670287 * get_resolver_14() const { return ___resolver_14; }
	inline XmlResolver_t3822670287 ** get_address_of_resolver_14() { return &___resolver_14; }
	inline void set_resolver_14(XmlResolver_t3822670287 * value)
	{
		___resolver_14 = value;
		Il2CppCodeGenWriteBarrier(&___resolver_14, value);
	}

	inline static int32_t get_offset_of_idTable_15() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740, ___idTable_15)); }
	inline Hashtable_t1407064410 * get_idTable_15() const { return ___idTable_15; }
	inline Hashtable_t1407064410 ** get_address_of_idTable_15() { return &___idTable_15; }
	inline void set_idTable_15(Hashtable_t1407064410 * value)
	{
		___idTable_15 = value;
		Il2CppCodeGenWriteBarrier(&___idTable_15, value);
	}

	inline static int32_t get_offset_of_nameCache_16() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740, ___nameCache_16)); }
	inline XmlNameEntryCache_t3943949348 * get_nameCache_16() const { return ___nameCache_16; }
	inline XmlNameEntryCache_t3943949348 ** get_address_of_nameCache_16() { return &___nameCache_16; }
	inline void set_nameCache_16(XmlNameEntryCache_t3943949348 * value)
	{
		___nameCache_16 = value;
		Il2CppCodeGenWriteBarrier(&___nameCache_16, value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_17() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740, ___lastLinkedChild_17)); }
	inline XmlLinkedNode_t901819716 * get_lastLinkedChild_17() const { return ___lastLinkedChild_17; }
	inline XmlLinkedNode_t901819716 ** get_address_of_lastLinkedChild_17() { return &___lastLinkedChild_17; }
	inline void set_lastLinkedChild_17(XmlLinkedNode_t901819716 * value)
	{
		___lastLinkedChild_17 = value;
		Il2CppCodeGenWriteBarrier(&___lastLinkedChild_17, value);
	}

	inline static int32_t get_offset_of_nsNodeXml_18() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740, ___nsNodeXml_18)); }
	inline XmlAttribute_t6647939 * get_nsNodeXml_18() const { return ___nsNodeXml_18; }
	inline XmlAttribute_t6647939 ** get_address_of_nsNodeXml_18() { return &___nsNodeXml_18; }
	inline void set_nsNodeXml_18(XmlAttribute_t6647939 * value)
	{
		___nsNodeXml_18 = value;
		Il2CppCodeGenWriteBarrier(&___nsNodeXml_18, value);
	}

	inline static int32_t get_offset_of_schemas_19() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740, ___schemas_19)); }
	inline XmlSchemaSet_t4125473774 * get_schemas_19() const { return ___schemas_19; }
	inline XmlSchemaSet_t4125473774 ** get_address_of_schemas_19() { return &___schemas_19; }
	inline void set_schemas_19(XmlSchemaSet_t4125473774 * value)
	{
		___schemas_19 = value;
		Il2CppCodeGenWriteBarrier(&___schemas_19, value);
	}

	inline static int32_t get_offset_of_schemaInfo_20() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740, ___schemaInfo_20)); }
	inline Il2CppObject * get_schemaInfo_20() const { return ___schemaInfo_20; }
	inline Il2CppObject ** get_address_of_schemaInfo_20() { return &___schemaInfo_20; }
	inline void set_schemaInfo_20(Il2CppObject * value)
	{
		___schemaInfo_20 = value;
		Il2CppCodeGenWriteBarrier(&___schemaInfo_20, value);
	}

	inline static int32_t get_offset_of_loadMode_21() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740, ___loadMode_21)); }
	inline bool get_loadMode_21() const { return ___loadMode_21; }
	inline bool* get_address_of_loadMode_21() { return &___loadMode_21; }
	inline void set_loadMode_21(bool value)
	{
		___loadMode_21 = value;
	}

	inline static int32_t get_offset_of_NodeChanged_22() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740, ___NodeChanged_22)); }
	inline XmlNodeChangedEventHandler_t3074502249 * get_NodeChanged_22() const { return ___NodeChanged_22; }
	inline XmlNodeChangedEventHandler_t3074502249 ** get_address_of_NodeChanged_22() { return &___NodeChanged_22; }
	inline void set_NodeChanged_22(XmlNodeChangedEventHandler_t3074502249 * value)
	{
		___NodeChanged_22 = value;
		Il2CppCodeGenWriteBarrier(&___NodeChanged_22, value);
	}

	inline static int32_t get_offset_of_NodeChanging_23() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740, ___NodeChanging_23)); }
	inline XmlNodeChangedEventHandler_t3074502249 * get_NodeChanging_23() const { return ___NodeChanging_23; }
	inline XmlNodeChangedEventHandler_t3074502249 ** get_address_of_NodeChanging_23() { return &___NodeChanging_23; }
	inline void set_NodeChanging_23(XmlNodeChangedEventHandler_t3074502249 * value)
	{
		___NodeChanging_23 = value;
		Il2CppCodeGenWriteBarrier(&___NodeChanging_23, value);
	}

	inline static int32_t get_offset_of_NodeInserted_24() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740, ___NodeInserted_24)); }
	inline XmlNodeChangedEventHandler_t3074502249 * get_NodeInserted_24() const { return ___NodeInserted_24; }
	inline XmlNodeChangedEventHandler_t3074502249 ** get_address_of_NodeInserted_24() { return &___NodeInserted_24; }
	inline void set_NodeInserted_24(XmlNodeChangedEventHandler_t3074502249 * value)
	{
		___NodeInserted_24 = value;
		Il2CppCodeGenWriteBarrier(&___NodeInserted_24, value);
	}

	inline static int32_t get_offset_of_NodeInserting_25() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740, ___NodeInserting_25)); }
	inline XmlNodeChangedEventHandler_t3074502249 * get_NodeInserting_25() const { return ___NodeInserting_25; }
	inline XmlNodeChangedEventHandler_t3074502249 ** get_address_of_NodeInserting_25() { return &___NodeInserting_25; }
	inline void set_NodeInserting_25(XmlNodeChangedEventHandler_t3074502249 * value)
	{
		___NodeInserting_25 = value;
		Il2CppCodeGenWriteBarrier(&___NodeInserting_25, value);
	}

	inline static int32_t get_offset_of_NodeRemoved_26() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740, ___NodeRemoved_26)); }
	inline XmlNodeChangedEventHandler_t3074502249 * get_NodeRemoved_26() const { return ___NodeRemoved_26; }
	inline XmlNodeChangedEventHandler_t3074502249 ** get_address_of_NodeRemoved_26() { return &___NodeRemoved_26; }
	inline void set_NodeRemoved_26(XmlNodeChangedEventHandler_t3074502249 * value)
	{
		___NodeRemoved_26 = value;
		Il2CppCodeGenWriteBarrier(&___NodeRemoved_26, value);
	}

	inline static int32_t get_offset_of_NodeRemoving_27() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740, ___NodeRemoving_27)); }
	inline XmlNodeChangedEventHandler_t3074502249 * get_NodeRemoving_27() const { return ___NodeRemoving_27; }
	inline XmlNodeChangedEventHandler_t3074502249 ** get_address_of_NodeRemoving_27() { return &___NodeRemoving_27; }
	inline void set_NodeRemoving_27(XmlNodeChangedEventHandler_t3074502249 * value)
	{
		___NodeRemoving_27 = value;
		Il2CppCodeGenWriteBarrier(&___NodeRemoving_27, value);
	}
};

struct XmlDocument_t730752740_StaticFields
{
public:
	// System.Type[] System.Xml.XmlDocument::optimal_create_types
	TypeU5BU5D_t3339007067* ___optimal_create_types_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlDocument::<>f__switch$map4B
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map4B_28;

public:
	inline static int32_t get_offset_of_optimal_create_types_7() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740_StaticFields, ___optimal_create_types_7)); }
	inline TypeU5BU5D_t3339007067* get_optimal_create_types_7() const { return ___optimal_create_types_7; }
	inline TypeU5BU5D_t3339007067** get_address_of_optimal_create_types_7() { return &___optimal_create_types_7; }
	inline void set_optimal_create_types_7(TypeU5BU5D_t3339007067* value)
	{
		___optimal_create_types_7 = value;
		Il2CppCodeGenWriteBarrier(&___optimal_create_types_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4B_28() { return static_cast<int32_t>(offsetof(XmlDocument_t730752740_StaticFields, ___U3CU3Ef__switchU24map4B_28)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map4B_28() const { return ___U3CU3Ef__switchU24map4B_28; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map4B_28() { return &___U3CU3Ef__switchU24map4B_28; }
	inline void set_U3CU3Ef__switchU24map4B_28(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map4B_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map4B_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
