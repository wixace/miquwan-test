﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_mapaSarrisker238
struct M_mapaSarrisker238_t3098885612;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_mapaSarrisker238::.ctor()
extern "C"  void M_mapaSarrisker238__ctor_m1645750727 (M_mapaSarrisker238_t3098885612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_mapaSarrisker238::M_kooliselYalwasri0(System.String[],System.Int32)
extern "C"  void M_mapaSarrisker238_M_kooliselYalwasri0_m1732350476 (M_mapaSarrisker238_t3098885612 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
