﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<FloatTextUnit>::.ctor()
#define List_1__ctor_m3491501708(__this, method) ((  void (*) (List_1_t3730483581 *, const MethodInfo*))List_1__ctor_m1239231859_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m694589820(__this, ___collection0, method) ((  void (*) (List_1_t3730483581 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::.ctor(System.Int32)
#define List_1__ctor_m2073949172(__this, ___capacity0, method) ((  void (*) (List_1_t3730483581 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::.cctor()
#define List_1__cctor_m4069020906(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<FloatTextUnit>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1625605557(__this, method) ((  Il2CppObject* (*) (List_1_t3730483581 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1806224257(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3730483581 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<FloatTextUnit>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m44674748(__this, method) ((  Il2CppObject * (*) (List_1_t3730483581 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<FloatTextUnit>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m93557685(__this, ___item0, method) ((  int32_t (*) (List_1_t3730483581 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<FloatTextUnit>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m2650209591(__this, ___item0, method) ((  bool (*) (List_1_t3730483581 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<FloatTextUnit>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m3569936269(__this, ___item0, method) ((  int32_t (*) (List_1_t3730483581 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m3547024440(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3730483581 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m4142457328(__this, ___item0, method) ((  void (*) (List_1_t3730483581 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<FloatTextUnit>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3287094392(__this, method) ((  bool (*) (List_1_t3730483581 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FloatTextUnit>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2300066501(__this, method) ((  bool (*) (List_1_t3730483581 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<FloatTextUnit>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m3632228465(__this, method) ((  Il2CppObject * (*) (List_1_t3730483581 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FloatTextUnit>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m729194982(__this, method) ((  bool (*) (List_1_t3730483581 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FloatTextUnit>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m3664119891(__this, method) ((  bool (*) (List_1_t3730483581 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<FloatTextUnit>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m4123722168(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3730483581 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m3395373583(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3730483581 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::Add(T)
#define List_1_Add_m3185862524(__this, ___item0, method) ((  void (*) (List_1_t3730483581 *, FloatTextUnit_t2362298029 *, const MethodInfo*))List_1_Add_m933592675_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m2774109495(__this, ___newCount0, method) ((  void (*) (List_1_t3730483581 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m145817168(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3730483581 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m590371457_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m170870837(__this, ___collection0, method) ((  void (*) (List_1_t3730483581 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m3726810357(__this, ___enumerable0, method) ((  void (*) (List_1_t3730483581 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m3423816834(__this, ___collection0, method) ((  void (*) (List_1_t3730483581 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<FloatTextUnit>::AsReadOnly()
#define List_1_AsReadOnly_m3676917607(__this, method) ((  ReadOnlyCollection_1_t3919375565 * (*) (List_1_t3730483581 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<FloatTextUnit>::BinarySearch(T)
#define List_1_BinarySearch_m85213560(__this, ___item0, method) ((  int32_t (*) (List_1_t3730483581 *, FloatTextUnit_t2362298029 *, const MethodInfo*))List_1_BinarySearch_m2761349225_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::Clear()
#define List_1_Clear_m897634999(__this, method) ((  void (*) (List_1_t3730483581 *, const MethodInfo*))List_1_Clear_m2940332446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FloatTextUnit>::Contains(T)
#define List_1_Contains_m2894396796(__this, ___item0, method) ((  bool (*) (List_1_t3730483581 *, FloatTextUnit_t2362298029 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m965727724(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3730483581 *, FloatTextUnitU5BU5D_t1909147648*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<FloatTextUnit>::Find(System.Predicate`1<T>)
#define List_1_Find_m231246460(__this, ___match0, method) ((  FloatTextUnit_t2362298029 * (*) (List_1_t3730483581 *, Predicate_1_t1973354912 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m3563467383(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1973354912 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<FloatTextUnit>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m3093171356(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3730483581 *, int32_t, int32_t, Predicate_1_t1973354912 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<FloatTextUnit>::GetEnumerator()
#define List_1_GetEnumerator_m3945288889(__this, method) ((  Enumerator_t3750156351  (*) (List_1_t3730483581 *, const MethodInfo*))List_1_GetEnumerator_m1919240000_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<FloatTextUnit>::IndexOf(T)
#define List_1_IndexOf_m3234656368(__this, ___item0, method) ((  int32_t (*) (List_1_t3730483581 *, FloatTextUnit_t2362298029 *, const MethodInfo*))List_1_IndexOf_m2776798407_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m1180237891(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3730483581 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m712094908(__this, ___index0, method) ((  void (*) (List_1_t3730483581 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::Insert(System.Int32,T)
#define List_1_Insert_m3334633315(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3730483581 *, int32_t, FloatTextUnit_t2362298029 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m1524785560(__this, ___collection0, method) ((  void (*) (List_1_t3730483581 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<FloatTextUnit>::Remove(T)
#define List_1_Remove_m1687350391(__this, ___item0, method) ((  bool (*) (List_1_t3730483581 *, FloatTextUnit_t2362298029 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<FloatTextUnit>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m306627635(__this, ___match0, method) ((  int32_t (*) (List_1_t3730483581 *, Predicate_1_t1973354912 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m1208486185(__this, ___index0, method) ((  void (*) (List_1_t3730483581 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2208877785_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m4255544140(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3730483581 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m856857915_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::Reverse()
#define List_1_Reverse_m3683488419(__this, method) ((  void (*) (List_1_t3730483581 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::Sort()
#define List_1_Sort_m2094032863(__this, method) ((  void (*) (List_1_t3730483581 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m3864921317(__this, ___comparer0, method) ((  void (*) (List_1_t3730483581 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m1077129970(__this, ___comparison0, method) ((  void (*) (List_1_t3730483581 *, Comparison_1_t1078659216 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<FloatTextUnit>::ToArray()
#define List_1_ToArray_m3472995234(__this, method) ((  FloatTextUnitU5BU5D_t1909147648* (*) (List_1_t3730483581 *, const MethodInfo*))List_1_ToArray_m1449333879_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::TrimExcess()
#define List_1_TrimExcess_m1063511096(__this, method) ((  void (*) (List_1_t3730483581 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<FloatTextUnit>::get_Capacity()
#define List_1_get_Capacity_m2535403680(__this, method) ((  int32_t (*) (List_1_t3730483581 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m4279265609(__this, ___value0, method) ((  void (*) (List_1_t3730483581 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<FloatTextUnit>::get_Count()
#define List_1_get_Count_m1302458690(__this, method) ((  int32_t (*) (List_1_t3730483581 *, const MethodInfo*))List_1_get_Count_m3549598589_gshared)(__this, method)
// T System.Collections.Generic.List`1<FloatTextUnit>::get_Item(System.Int32)
#define List_1_get_Item_m757597331(__this, ___index0, method) ((  FloatTextUnit_t2362298029 * (*) (List_1_t3730483581 *, int32_t, const MethodInfo*))List_1_get_Item_m404118264_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FloatTextUnit>::set_Item(System.Int32,T)
#define List_1_set_Item_m2346609530(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3730483581 *, int32_t, FloatTextUnit_t2362298029 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
