﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIWidgetGenerated/<UIWidget_onRender_GetDelegate_member0_arg0>c__AnonStoreyD8
struct U3CUIWidget_onRender_GetDelegate_member0_arg0U3Ec__AnonStoreyD8_t383975243;
// UnityEngine.Material
struct Material_t3870600107;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"

// System.Void UIWidgetGenerated/<UIWidget_onRender_GetDelegate_member0_arg0>c__AnonStoreyD8::.ctor()
extern "C"  void U3CUIWidget_onRender_GetDelegate_member0_arg0U3Ec__AnonStoreyD8__ctor_m839853696 (U3CUIWidget_onRender_GetDelegate_member0_arg0U3Ec__AnonStoreyD8_t383975243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated/<UIWidget_onRender_GetDelegate_member0_arg0>c__AnonStoreyD8::<>m__175(UnityEngine.Material)
extern "C"  void U3CUIWidget_onRender_GetDelegate_member0_arg0U3Ec__AnonStoreyD8_U3CU3Em__175_m2901772202 (U3CUIWidget_onRender_GetDelegate_member0_arg0U3Ec__AnonStoreyD8_t383975243 * __this, Material_t3870600107 * ___mat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
