﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_DiscreteGestureRecognizer_1_gen120140296.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TapRecognizer
struct  TapRecognizer_t3788301703  : public DiscreteGestureRecognizer_1_t120140296
{
public:
	// System.Int32 TapRecognizer::RequiredTaps
	int32_t ___RequiredTaps_19;
	// System.Single TapRecognizer::MoveTolerance
	float ___MoveTolerance_20;
	// System.Single TapRecognizer::MaxDuration
	float ___MaxDuration_21;
	// System.Single TapRecognizer::MaxDelayBetweenTaps
	float ___MaxDelayBetweenTaps_22;

public:
	inline static int32_t get_offset_of_RequiredTaps_19() { return static_cast<int32_t>(offsetof(TapRecognizer_t3788301703, ___RequiredTaps_19)); }
	inline int32_t get_RequiredTaps_19() const { return ___RequiredTaps_19; }
	inline int32_t* get_address_of_RequiredTaps_19() { return &___RequiredTaps_19; }
	inline void set_RequiredTaps_19(int32_t value)
	{
		___RequiredTaps_19 = value;
	}

	inline static int32_t get_offset_of_MoveTolerance_20() { return static_cast<int32_t>(offsetof(TapRecognizer_t3788301703, ___MoveTolerance_20)); }
	inline float get_MoveTolerance_20() const { return ___MoveTolerance_20; }
	inline float* get_address_of_MoveTolerance_20() { return &___MoveTolerance_20; }
	inline void set_MoveTolerance_20(float value)
	{
		___MoveTolerance_20 = value;
	}

	inline static int32_t get_offset_of_MaxDuration_21() { return static_cast<int32_t>(offsetof(TapRecognizer_t3788301703, ___MaxDuration_21)); }
	inline float get_MaxDuration_21() const { return ___MaxDuration_21; }
	inline float* get_address_of_MaxDuration_21() { return &___MaxDuration_21; }
	inline void set_MaxDuration_21(float value)
	{
		___MaxDuration_21 = value;
	}

	inline static int32_t get_offset_of_MaxDelayBetweenTaps_22() { return static_cast<int32_t>(offsetof(TapRecognizer_t3788301703, ___MaxDelayBetweenTaps_22)); }
	inline float get_MaxDelayBetweenTaps_22() const { return ___MaxDelayBetweenTaps_22; }
	inline float* get_address_of_MaxDelayBetweenTaps_22() { return &___MaxDelayBetweenTaps_22; }
	inline void set_MaxDelayBetweenTaps_22(float value)
	{
		___MaxDelayBetweenTaps_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
