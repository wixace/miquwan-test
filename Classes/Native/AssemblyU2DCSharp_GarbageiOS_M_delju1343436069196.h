﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_delju134
struct  M_delju134_t3436069196  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_delju134::_xelelTerpur
	String_t* ____xelelTerpur_0;
	// System.String GarbageiOS.M_delju134::_haymeTaircarlee
	String_t* ____haymeTaircarlee_1;
	// System.String GarbageiOS.M_delju134::_tefa
	String_t* ____tefa_2;
	// System.Boolean GarbageiOS.M_delju134::_qorner
	bool ____qorner_3;
	// System.Single GarbageiOS.M_delju134::_nire
	float ____nire_4;
	// System.Single GarbageiOS.M_delju134::_bumasDelou
	float ____bumasDelou_5;
	// System.Int32 GarbageiOS.M_delju134::_lonujaw
	int32_t ____lonujaw_6;
	// System.Boolean GarbageiOS.M_delju134::_lewhe
	bool ____lewhe_7;
	// System.Single GarbageiOS.M_delju134::_miswoureJoojorve
	float ____miswoureJoojorve_8;
	// System.String GarbageiOS.M_delju134::_fazo
	String_t* ____fazo_9;
	// System.String GarbageiOS.M_delju134::_rairceljear
	String_t* ____rairceljear_10;
	// System.String GarbageiOS.M_delju134::_kusallrereMeacorsa
	String_t* ____kusallrereMeacorsa_11;
	// System.String GarbageiOS.M_delju134::_zikerfayPeze
	String_t* ____zikerfayPeze_12;
	// System.Int32 GarbageiOS.M_delju134::_jace
	int32_t ____jace_13;
	// System.UInt32 GarbageiOS.M_delju134::_keacooWerfusu
	uint32_t ____keacooWerfusu_14;
	// System.String GarbageiOS.M_delju134::_hebaitawJejafou
	String_t* ____hebaitawJejafou_15;
	// System.Single GarbageiOS.M_delju134::_sahedorSaijeedrou
	float ____sahedorSaijeedrou_16;
	// System.Boolean GarbageiOS.M_delju134::_wallhata
	bool ____wallhata_17;
	// System.String GarbageiOS.M_delju134::_rurgisre
	String_t* ____rurgisre_18;
	// System.Boolean GarbageiOS.M_delju134::_mikoozirStatabel
	bool ____mikoozirStatabel_19;
	// System.Single GarbageiOS.M_delju134::_faiqoucelWaychebor
	float ____faiqoucelWaychebor_20;
	// System.Boolean GarbageiOS.M_delju134::_trallputarRousa
	bool ____trallputarRousa_21;
	// System.String GarbageiOS.M_delju134::_dowdrouPelqoupe
	String_t* ____dowdrouPelqoupe_22;
	// System.Single GarbageiOS.M_delju134::_mawho
	float ____mawho_23;
	// System.String GarbageiOS.M_delju134::_cagajallVeeqear
	String_t* ____cagajallVeeqear_24;
	// System.Int32 GarbageiOS.M_delju134::_gasmouler
	int32_t ____gasmouler_25;
	// System.Boolean GarbageiOS.M_delju134::_hisfalmear
	bool ____hisfalmear_26;
	// System.String GarbageiOS.M_delju134::_casere
	String_t* ____casere_27;

public:
	inline static int32_t get_offset_of__xelelTerpur_0() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____xelelTerpur_0)); }
	inline String_t* get__xelelTerpur_0() const { return ____xelelTerpur_0; }
	inline String_t** get_address_of__xelelTerpur_0() { return &____xelelTerpur_0; }
	inline void set__xelelTerpur_0(String_t* value)
	{
		____xelelTerpur_0 = value;
		Il2CppCodeGenWriteBarrier(&____xelelTerpur_0, value);
	}

	inline static int32_t get_offset_of__haymeTaircarlee_1() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____haymeTaircarlee_1)); }
	inline String_t* get__haymeTaircarlee_1() const { return ____haymeTaircarlee_1; }
	inline String_t** get_address_of__haymeTaircarlee_1() { return &____haymeTaircarlee_1; }
	inline void set__haymeTaircarlee_1(String_t* value)
	{
		____haymeTaircarlee_1 = value;
		Il2CppCodeGenWriteBarrier(&____haymeTaircarlee_1, value);
	}

	inline static int32_t get_offset_of__tefa_2() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____tefa_2)); }
	inline String_t* get__tefa_2() const { return ____tefa_2; }
	inline String_t** get_address_of__tefa_2() { return &____tefa_2; }
	inline void set__tefa_2(String_t* value)
	{
		____tefa_2 = value;
		Il2CppCodeGenWriteBarrier(&____tefa_2, value);
	}

	inline static int32_t get_offset_of__qorner_3() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____qorner_3)); }
	inline bool get__qorner_3() const { return ____qorner_3; }
	inline bool* get_address_of__qorner_3() { return &____qorner_3; }
	inline void set__qorner_3(bool value)
	{
		____qorner_3 = value;
	}

	inline static int32_t get_offset_of__nire_4() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____nire_4)); }
	inline float get__nire_4() const { return ____nire_4; }
	inline float* get_address_of__nire_4() { return &____nire_4; }
	inline void set__nire_4(float value)
	{
		____nire_4 = value;
	}

	inline static int32_t get_offset_of__bumasDelou_5() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____bumasDelou_5)); }
	inline float get__bumasDelou_5() const { return ____bumasDelou_5; }
	inline float* get_address_of__bumasDelou_5() { return &____bumasDelou_5; }
	inline void set__bumasDelou_5(float value)
	{
		____bumasDelou_5 = value;
	}

	inline static int32_t get_offset_of__lonujaw_6() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____lonujaw_6)); }
	inline int32_t get__lonujaw_6() const { return ____lonujaw_6; }
	inline int32_t* get_address_of__lonujaw_6() { return &____lonujaw_6; }
	inline void set__lonujaw_6(int32_t value)
	{
		____lonujaw_6 = value;
	}

	inline static int32_t get_offset_of__lewhe_7() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____lewhe_7)); }
	inline bool get__lewhe_7() const { return ____lewhe_7; }
	inline bool* get_address_of__lewhe_7() { return &____lewhe_7; }
	inline void set__lewhe_7(bool value)
	{
		____lewhe_7 = value;
	}

	inline static int32_t get_offset_of__miswoureJoojorve_8() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____miswoureJoojorve_8)); }
	inline float get__miswoureJoojorve_8() const { return ____miswoureJoojorve_8; }
	inline float* get_address_of__miswoureJoojorve_8() { return &____miswoureJoojorve_8; }
	inline void set__miswoureJoojorve_8(float value)
	{
		____miswoureJoojorve_8 = value;
	}

	inline static int32_t get_offset_of__fazo_9() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____fazo_9)); }
	inline String_t* get__fazo_9() const { return ____fazo_9; }
	inline String_t** get_address_of__fazo_9() { return &____fazo_9; }
	inline void set__fazo_9(String_t* value)
	{
		____fazo_9 = value;
		Il2CppCodeGenWriteBarrier(&____fazo_9, value);
	}

	inline static int32_t get_offset_of__rairceljear_10() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____rairceljear_10)); }
	inline String_t* get__rairceljear_10() const { return ____rairceljear_10; }
	inline String_t** get_address_of__rairceljear_10() { return &____rairceljear_10; }
	inline void set__rairceljear_10(String_t* value)
	{
		____rairceljear_10 = value;
		Il2CppCodeGenWriteBarrier(&____rairceljear_10, value);
	}

	inline static int32_t get_offset_of__kusallrereMeacorsa_11() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____kusallrereMeacorsa_11)); }
	inline String_t* get__kusallrereMeacorsa_11() const { return ____kusallrereMeacorsa_11; }
	inline String_t** get_address_of__kusallrereMeacorsa_11() { return &____kusallrereMeacorsa_11; }
	inline void set__kusallrereMeacorsa_11(String_t* value)
	{
		____kusallrereMeacorsa_11 = value;
		Il2CppCodeGenWriteBarrier(&____kusallrereMeacorsa_11, value);
	}

	inline static int32_t get_offset_of__zikerfayPeze_12() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____zikerfayPeze_12)); }
	inline String_t* get__zikerfayPeze_12() const { return ____zikerfayPeze_12; }
	inline String_t** get_address_of__zikerfayPeze_12() { return &____zikerfayPeze_12; }
	inline void set__zikerfayPeze_12(String_t* value)
	{
		____zikerfayPeze_12 = value;
		Il2CppCodeGenWriteBarrier(&____zikerfayPeze_12, value);
	}

	inline static int32_t get_offset_of__jace_13() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____jace_13)); }
	inline int32_t get__jace_13() const { return ____jace_13; }
	inline int32_t* get_address_of__jace_13() { return &____jace_13; }
	inline void set__jace_13(int32_t value)
	{
		____jace_13 = value;
	}

	inline static int32_t get_offset_of__keacooWerfusu_14() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____keacooWerfusu_14)); }
	inline uint32_t get__keacooWerfusu_14() const { return ____keacooWerfusu_14; }
	inline uint32_t* get_address_of__keacooWerfusu_14() { return &____keacooWerfusu_14; }
	inline void set__keacooWerfusu_14(uint32_t value)
	{
		____keacooWerfusu_14 = value;
	}

	inline static int32_t get_offset_of__hebaitawJejafou_15() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____hebaitawJejafou_15)); }
	inline String_t* get__hebaitawJejafou_15() const { return ____hebaitawJejafou_15; }
	inline String_t** get_address_of__hebaitawJejafou_15() { return &____hebaitawJejafou_15; }
	inline void set__hebaitawJejafou_15(String_t* value)
	{
		____hebaitawJejafou_15 = value;
		Il2CppCodeGenWriteBarrier(&____hebaitawJejafou_15, value);
	}

	inline static int32_t get_offset_of__sahedorSaijeedrou_16() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____sahedorSaijeedrou_16)); }
	inline float get__sahedorSaijeedrou_16() const { return ____sahedorSaijeedrou_16; }
	inline float* get_address_of__sahedorSaijeedrou_16() { return &____sahedorSaijeedrou_16; }
	inline void set__sahedorSaijeedrou_16(float value)
	{
		____sahedorSaijeedrou_16 = value;
	}

	inline static int32_t get_offset_of__wallhata_17() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____wallhata_17)); }
	inline bool get__wallhata_17() const { return ____wallhata_17; }
	inline bool* get_address_of__wallhata_17() { return &____wallhata_17; }
	inline void set__wallhata_17(bool value)
	{
		____wallhata_17 = value;
	}

	inline static int32_t get_offset_of__rurgisre_18() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____rurgisre_18)); }
	inline String_t* get__rurgisre_18() const { return ____rurgisre_18; }
	inline String_t** get_address_of__rurgisre_18() { return &____rurgisre_18; }
	inline void set__rurgisre_18(String_t* value)
	{
		____rurgisre_18 = value;
		Il2CppCodeGenWriteBarrier(&____rurgisre_18, value);
	}

	inline static int32_t get_offset_of__mikoozirStatabel_19() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____mikoozirStatabel_19)); }
	inline bool get__mikoozirStatabel_19() const { return ____mikoozirStatabel_19; }
	inline bool* get_address_of__mikoozirStatabel_19() { return &____mikoozirStatabel_19; }
	inline void set__mikoozirStatabel_19(bool value)
	{
		____mikoozirStatabel_19 = value;
	}

	inline static int32_t get_offset_of__faiqoucelWaychebor_20() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____faiqoucelWaychebor_20)); }
	inline float get__faiqoucelWaychebor_20() const { return ____faiqoucelWaychebor_20; }
	inline float* get_address_of__faiqoucelWaychebor_20() { return &____faiqoucelWaychebor_20; }
	inline void set__faiqoucelWaychebor_20(float value)
	{
		____faiqoucelWaychebor_20 = value;
	}

	inline static int32_t get_offset_of__trallputarRousa_21() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____trallputarRousa_21)); }
	inline bool get__trallputarRousa_21() const { return ____trallputarRousa_21; }
	inline bool* get_address_of__trallputarRousa_21() { return &____trallputarRousa_21; }
	inline void set__trallputarRousa_21(bool value)
	{
		____trallputarRousa_21 = value;
	}

	inline static int32_t get_offset_of__dowdrouPelqoupe_22() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____dowdrouPelqoupe_22)); }
	inline String_t* get__dowdrouPelqoupe_22() const { return ____dowdrouPelqoupe_22; }
	inline String_t** get_address_of__dowdrouPelqoupe_22() { return &____dowdrouPelqoupe_22; }
	inline void set__dowdrouPelqoupe_22(String_t* value)
	{
		____dowdrouPelqoupe_22 = value;
		Il2CppCodeGenWriteBarrier(&____dowdrouPelqoupe_22, value);
	}

	inline static int32_t get_offset_of__mawho_23() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____mawho_23)); }
	inline float get__mawho_23() const { return ____mawho_23; }
	inline float* get_address_of__mawho_23() { return &____mawho_23; }
	inline void set__mawho_23(float value)
	{
		____mawho_23 = value;
	}

	inline static int32_t get_offset_of__cagajallVeeqear_24() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____cagajallVeeqear_24)); }
	inline String_t* get__cagajallVeeqear_24() const { return ____cagajallVeeqear_24; }
	inline String_t** get_address_of__cagajallVeeqear_24() { return &____cagajallVeeqear_24; }
	inline void set__cagajallVeeqear_24(String_t* value)
	{
		____cagajallVeeqear_24 = value;
		Il2CppCodeGenWriteBarrier(&____cagajallVeeqear_24, value);
	}

	inline static int32_t get_offset_of__gasmouler_25() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____gasmouler_25)); }
	inline int32_t get__gasmouler_25() const { return ____gasmouler_25; }
	inline int32_t* get_address_of__gasmouler_25() { return &____gasmouler_25; }
	inline void set__gasmouler_25(int32_t value)
	{
		____gasmouler_25 = value;
	}

	inline static int32_t get_offset_of__hisfalmear_26() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____hisfalmear_26)); }
	inline bool get__hisfalmear_26() const { return ____hisfalmear_26; }
	inline bool* get_address_of__hisfalmear_26() { return &____hisfalmear_26; }
	inline void set__hisfalmear_26(bool value)
	{
		____hisfalmear_26 = value;
	}

	inline static int32_t get_offset_of__casere_27() { return static_cast<int32_t>(offsetof(M_delju134_t3436069196, ____casere_27)); }
	inline String_t* get__casere_27() const { return ____casere_27; }
	inline String_t** get_address_of__casere_27() { return &____casere_27; }
	inline void set__casere_27(String_t* value)
	{
		____casere_27 = value;
		Il2CppCodeGenWriteBarrier(&____casere_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
