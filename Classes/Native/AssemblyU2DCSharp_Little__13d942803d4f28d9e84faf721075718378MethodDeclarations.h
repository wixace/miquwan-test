﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._13d942803d4f28d9e84faf724f6873b3
struct _13d942803d4f28d9e84faf724f6873b3_t1075718378;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__13d942803d4f28d9e84faf721075718378.h"

// System.Void Little._13d942803d4f28d9e84faf724f6873b3::.ctor()
extern "C"  void _13d942803d4f28d9e84faf724f6873b3__ctor_m2184406147 (_13d942803d4f28d9e84faf724f6873b3_t1075718378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._13d942803d4f28d9e84faf724f6873b3::_13d942803d4f28d9e84faf724f6873b3m2(System.Int32)
extern "C"  int32_t _13d942803d4f28d9e84faf724f6873b3__13d942803d4f28d9e84faf724f6873b3m2_m3385858393 (_13d942803d4f28d9e84faf724f6873b3_t1075718378 * __this, int32_t ____13d942803d4f28d9e84faf724f6873b3a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._13d942803d4f28d9e84faf724f6873b3::_13d942803d4f28d9e84faf724f6873b3m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _13d942803d4f28d9e84faf724f6873b3__13d942803d4f28d9e84faf724f6873b3m_m30983549 (_13d942803d4f28d9e84faf724f6873b3_t1075718378 * __this, int32_t ____13d942803d4f28d9e84faf724f6873b3a0, int32_t ____13d942803d4f28d9e84faf724f6873b3511, int32_t ____13d942803d4f28d9e84faf724f6873b3c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._13d942803d4f28d9e84faf724f6873b3::ilo__13d942803d4f28d9e84faf724f6873b3m21(Little._13d942803d4f28d9e84faf724f6873b3,System.Int32)
extern "C"  int32_t _13d942803d4f28d9e84faf724f6873b3_ilo__13d942803d4f28d9e84faf724f6873b3m21_m3199969777 (Il2CppObject * __this /* static, unused */, _13d942803d4f28d9e84faf724f6873b3_t1075718378 * ____this0, int32_t ____13d942803d4f28d9e84faf724f6873b3a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
