﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSRobOtherPlayer
struct CSRobOtherPlayer_t3654572956;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// CSHeroUnit
struct CSHeroUnit_t3764358446;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"

// System.Void CSRobOtherPlayer::.ctor(Newtonsoft.Json.Linq.JToken)
extern "C"  void CSRobOtherPlayer__ctor_m1534020683 (CSRobOtherPlayer_t3654572956 * __this, JToken_t3412245951 * ___jToken0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSHeroUnit CSRobOtherPlayer::GetHeroInfo(System.UInt32)
extern "C"  CSHeroUnit_t3764358446 * CSRobOtherPlayer_GetHeroInfo_m961076750 (CSRobOtherPlayer_t3654572956 * __this, uint32_t ___heroID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
