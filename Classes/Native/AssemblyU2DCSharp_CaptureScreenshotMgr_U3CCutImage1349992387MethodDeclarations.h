﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CaptureScreenshotMgr/<CutImage>c__Iterator31
struct U3CCutImageU3Ec__Iterator31_t1349992387;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CaptureScreenshotMgr/<CutImage>c__Iterator31::.ctor()
extern "C"  void U3CCutImageU3Ec__Iterator31__ctor_m1611292216 (U3CCutImageU3Ec__Iterator31_t1349992387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CaptureScreenshotMgr/<CutImage>c__Iterator31::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCutImageU3Ec__Iterator31_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1683089188 (U3CCutImageU3Ec__Iterator31_t1349992387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CaptureScreenshotMgr/<CutImage>c__Iterator31::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCutImageU3Ec__Iterator31_System_Collections_IEnumerator_get_Current_m585020088 (U3CCutImageU3Ec__Iterator31_t1349992387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CaptureScreenshotMgr/<CutImage>c__Iterator31::MoveNext()
extern "C"  bool U3CCutImageU3Ec__Iterator31_MoveNext_m882986660 (U3CCutImageU3Ec__Iterator31_t1349992387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureScreenshotMgr/<CutImage>c__Iterator31::Dispose()
extern "C"  void U3CCutImageU3Ec__Iterator31_Dispose_m2161876085 (U3CCutImageU3Ec__Iterator31_t1349992387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureScreenshotMgr/<CutImage>c__Iterator31::Reset()
extern "C"  void U3CCutImageU3Ec__Iterator31_Reset_m3552692453 (U3CCutImageU3Ec__Iterator31_t1349992387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
