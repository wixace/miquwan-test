﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.JointAngleLimits2D
struct JointAngleLimits2D_t2258250679;
struct JointAngleLimits2D_t2258250679_marshaled_pinvoke;
struct JointAngleLimits2D_t2258250679_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_JointAngleLimits2D2258250679.h"

// System.Single UnityEngine.JointAngleLimits2D::get_min()
extern "C"  float JointAngleLimits2D_get_min_m1613806069 (JointAngleLimits2D_t2258250679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointAngleLimits2D::set_min(System.Single)
extern "C"  void JointAngleLimits2D_set_min_m653194046 (JointAngleLimits2D_t2258250679 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.JointAngleLimits2D::get_max()
extern "C"  float JointAngleLimits2D_get_max_m1613577351 (JointAngleLimits2D_t2258250679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointAngleLimits2D::set_max(System.Single)
extern "C"  void JointAngleLimits2D_set_max_m1901243884 (JointAngleLimits2D_t2258250679 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct JointAngleLimits2D_t2258250679;
struct JointAngleLimits2D_t2258250679_marshaled_pinvoke;

extern "C" void JointAngleLimits2D_t2258250679_marshal_pinvoke(const JointAngleLimits2D_t2258250679& unmarshaled, JointAngleLimits2D_t2258250679_marshaled_pinvoke& marshaled);
extern "C" void JointAngleLimits2D_t2258250679_marshal_pinvoke_back(const JointAngleLimits2D_t2258250679_marshaled_pinvoke& marshaled, JointAngleLimits2D_t2258250679& unmarshaled);
extern "C" void JointAngleLimits2D_t2258250679_marshal_pinvoke_cleanup(JointAngleLimits2D_t2258250679_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct JointAngleLimits2D_t2258250679;
struct JointAngleLimits2D_t2258250679_marshaled_com;

extern "C" void JointAngleLimits2D_t2258250679_marshal_com(const JointAngleLimits2D_t2258250679& unmarshaled, JointAngleLimits2D_t2258250679_marshaled_com& marshaled);
extern "C" void JointAngleLimits2D_t2258250679_marshal_com_back(const JointAngleLimits2D_t2258250679_marshaled_com& marshaled, JointAngleLimits2D_t2258250679& unmarshaled);
extern "C" void JointAngleLimits2D_t2258250679_marshal_com_cleanup(JointAngleLimits2D_t2258250679_marshaled_com& marshaled);
