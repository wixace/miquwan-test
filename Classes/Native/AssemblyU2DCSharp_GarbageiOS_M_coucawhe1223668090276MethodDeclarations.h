﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_coucawhe122
struct M_coucawhe122_t3668090276;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_coucawhe122::.ctor()
extern "C"  void M_coucawhe122__ctor_m2614425151 (M_coucawhe122_t3668090276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_coucawhe122::M_seecem0(System.String[],System.Int32)
extern "C"  void M_coucawhe122_M_seecem0_m1768616666 (M_coucawhe122_t3668090276 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
