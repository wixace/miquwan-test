﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.DefaultSerializationBinder
struct DefaultSerializationBinder_t3048163941;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_De2971844791.h"
#include "mscorlib_System_String7231557.h"

// System.Void Newtonsoft.Json.Serialization.DefaultSerializationBinder::.ctor()
extern "C"  void DefaultSerializationBinder__ctor_m1656508655 (DefaultSerializationBinder_t3048163941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultSerializationBinder::.cctor()
extern "C"  void DefaultSerializationBinder__cctor_m3625031838 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.DefaultSerializationBinder::GetTypeFromTypeNameKey(Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey)
extern "C"  Type_t * DefaultSerializationBinder_GetTypeFromTypeNameKey_m2222586008 (Il2CppObject * __this /* static, unused */, TypeNameKey_t2971844791  ___typeNameKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.DefaultSerializationBinder::BindToType(System.String,System.String)
extern "C"  Type_t * DefaultSerializationBinder_BindToType_m1908004253 (DefaultSerializationBinder_t3048163941 * __this, String_t* ___assemblyName0, String_t* ___typeName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.DefaultSerializationBinder::ilo_FormatWith1(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* DefaultSerializationBinder_ilo_FormatWith1_m2206212553 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
