﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.String
struct String_t;
struct Vector3_t4282066566_marshaled_pinvoke;
struct Vector2_t4282066565_marshaled_pinvoke;
struct Vector3_t4282066566_marshaled_com;
struct Vector2_t4282066565_marshaled_com;

#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.ObjImporter/meshStruct
struct  meshStruct_t2145059442 
{
public:
	// UnityEngine.Vector3[] Pathfinding.ObjImporter/meshStruct::vertices
	Vector3U5BU5D_t215400611* ___vertices_0;
	// UnityEngine.Vector3[] Pathfinding.ObjImporter/meshStruct::normals
	Vector3U5BU5D_t215400611* ___normals_1;
	// UnityEngine.Vector2[] Pathfinding.ObjImporter/meshStruct::uv
	Vector2U5BU5D_t4024180168* ___uv_2;
	// UnityEngine.Vector2[] Pathfinding.ObjImporter/meshStruct::uv1
	Vector2U5BU5D_t4024180168* ___uv1_3;
	// UnityEngine.Vector2[] Pathfinding.ObjImporter/meshStruct::uv2
	Vector2U5BU5D_t4024180168* ___uv2_4;
	// System.Int32[] Pathfinding.ObjImporter/meshStruct::triangles
	Int32U5BU5D_t3230847821* ___triangles_5;
	// System.Int32[] Pathfinding.ObjImporter/meshStruct::faceVerts
	Int32U5BU5D_t3230847821* ___faceVerts_6;
	// System.Int32[] Pathfinding.ObjImporter/meshStruct::faceUVs
	Int32U5BU5D_t3230847821* ___faceUVs_7;
	// UnityEngine.Vector3[] Pathfinding.ObjImporter/meshStruct::faceData
	Vector3U5BU5D_t215400611* ___faceData_8;
	// System.String Pathfinding.ObjImporter/meshStruct::name
	String_t* ___name_9;
	// System.String Pathfinding.ObjImporter/meshStruct::fileName
	String_t* ___fileName_10;

public:
	inline static int32_t get_offset_of_vertices_0() { return static_cast<int32_t>(offsetof(meshStruct_t2145059442, ___vertices_0)); }
	inline Vector3U5BU5D_t215400611* get_vertices_0() const { return ___vertices_0; }
	inline Vector3U5BU5D_t215400611** get_address_of_vertices_0() { return &___vertices_0; }
	inline void set_vertices_0(Vector3U5BU5D_t215400611* value)
	{
		___vertices_0 = value;
		Il2CppCodeGenWriteBarrier(&___vertices_0, value);
	}

	inline static int32_t get_offset_of_normals_1() { return static_cast<int32_t>(offsetof(meshStruct_t2145059442, ___normals_1)); }
	inline Vector3U5BU5D_t215400611* get_normals_1() const { return ___normals_1; }
	inline Vector3U5BU5D_t215400611** get_address_of_normals_1() { return &___normals_1; }
	inline void set_normals_1(Vector3U5BU5D_t215400611* value)
	{
		___normals_1 = value;
		Il2CppCodeGenWriteBarrier(&___normals_1, value);
	}

	inline static int32_t get_offset_of_uv_2() { return static_cast<int32_t>(offsetof(meshStruct_t2145059442, ___uv_2)); }
	inline Vector2U5BU5D_t4024180168* get_uv_2() const { return ___uv_2; }
	inline Vector2U5BU5D_t4024180168** get_address_of_uv_2() { return &___uv_2; }
	inline void set_uv_2(Vector2U5BU5D_t4024180168* value)
	{
		___uv_2 = value;
		Il2CppCodeGenWriteBarrier(&___uv_2, value);
	}

	inline static int32_t get_offset_of_uv1_3() { return static_cast<int32_t>(offsetof(meshStruct_t2145059442, ___uv1_3)); }
	inline Vector2U5BU5D_t4024180168* get_uv1_3() const { return ___uv1_3; }
	inline Vector2U5BU5D_t4024180168** get_address_of_uv1_3() { return &___uv1_3; }
	inline void set_uv1_3(Vector2U5BU5D_t4024180168* value)
	{
		___uv1_3 = value;
		Il2CppCodeGenWriteBarrier(&___uv1_3, value);
	}

	inline static int32_t get_offset_of_uv2_4() { return static_cast<int32_t>(offsetof(meshStruct_t2145059442, ___uv2_4)); }
	inline Vector2U5BU5D_t4024180168* get_uv2_4() const { return ___uv2_4; }
	inline Vector2U5BU5D_t4024180168** get_address_of_uv2_4() { return &___uv2_4; }
	inline void set_uv2_4(Vector2U5BU5D_t4024180168* value)
	{
		___uv2_4 = value;
		Il2CppCodeGenWriteBarrier(&___uv2_4, value);
	}

	inline static int32_t get_offset_of_triangles_5() { return static_cast<int32_t>(offsetof(meshStruct_t2145059442, ___triangles_5)); }
	inline Int32U5BU5D_t3230847821* get_triangles_5() const { return ___triangles_5; }
	inline Int32U5BU5D_t3230847821** get_address_of_triangles_5() { return &___triangles_5; }
	inline void set_triangles_5(Int32U5BU5D_t3230847821* value)
	{
		___triangles_5 = value;
		Il2CppCodeGenWriteBarrier(&___triangles_5, value);
	}

	inline static int32_t get_offset_of_faceVerts_6() { return static_cast<int32_t>(offsetof(meshStruct_t2145059442, ___faceVerts_6)); }
	inline Int32U5BU5D_t3230847821* get_faceVerts_6() const { return ___faceVerts_6; }
	inline Int32U5BU5D_t3230847821** get_address_of_faceVerts_6() { return &___faceVerts_6; }
	inline void set_faceVerts_6(Int32U5BU5D_t3230847821* value)
	{
		___faceVerts_6 = value;
		Il2CppCodeGenWriteBarrier(&___faceVerts_6, value);
	}

	inline static int32_t get_offset_of_faceUVs_7() { return static_cast<int32_t>(offsetof(meshStruct_t2145059442, ___faceUVs_7)); }
	inline Int32U5BU5D_t3230847821* get_faceUVs_7() const { return ___faceUVs_7; }
	inline Int32U5BU5D_t3230847821** get_address_of_faceUVs_7() { return &___faceUVs_7; }
	inline void set_faceUVs_7(Int32U5BU5D_t3230847821* value)
	{
		___faceUVs_7 = value;
		Il2CppCodeGenWriteBarrier(&___faceUVs_7, value);
	}

	inline static int32_t get_offset_of_faceData_8() { return static_cast<int32_t>(offsetof(meshStruct_t2145059442, ___faceData_8)); }
	inline Vector3U5BU5D_t215400611* get_faceData_8() const { return ___faceData_8; }
	inline Vector3U5BU5D_t215400611** get_address_of_faceData_8() { return &___faceData_8; }
	inline void set_faceData_8(Vector3U5BU5D_t215400611* value)
	{
		___faceData_8 = value;
		Il2CppCodeGenWriteBarrier(&___faceData_8, value);
	}

	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(meshStruct_t2145059442, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier(&___name_9, value);
	}

	inline static int32_t get_offset_of_fileName_10() { return static_cast<int32_t>(offsetof(meshStruct_t2145059442, ___fileName_10)); }
	inline String_t* get_fileName_10() const { return ___fileName_10; }
	inline String_t** get_address_of_fileName_10() { return &___fileName_10; }
	inline void set_fileName_10(String_t* value)
	{
		___fileName_10 = value;
		Il2CppCodeGenWriteBarrier(&___fileName_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Pathfinding.ObjImporter/meshStruct
struct meshStruct_t2145059442_marshaled_pinvoke
{
	Vector3_t4282066566_marshaled_pinvoke* ___vertices_0;
	Vector3_t4282066566_marshaled_pinvoke* ___normals_1;
	Vector2_t4282066565_marshaled_pinvoke* ___uv_2;
	Vector2_t4282066565_marshaled_pinvoke* ___uv1_3;
	Vector2_t4282066565_marshaled_pinvoke* ___uv2_4;
	int32_t* ___triangles_5;
	int32_t* ___faceVerts_6;
	int32_t* ___faceUVs_7;
	Vector3_t4282066566_marshaled_pinvoke* ___faceData_8;
	char* ___name_9;
	char* ___fileName_10;
};
// Native definition for marshalling of: Pathfinding.ObjImporter/meshStruct
struct meshStruct_t2145059442_marshaled_com
{
	Vector3_t4282066566_marshaled_com* ___vertices_0;
	Vector3_t4282066566_marshaled_com* ___normals_1;
	Vector2_t4282066565_marshaled_com* ___uv_2;
	Vector2_t4282066565_marshaled_com* ___uv1_3;
	Vector2_t4282066565_marshaled_com* ___uv2_4;
	int32_t* ___triangles_5;
	int32_t* ___faceVerts_6;
	int32_t* ___faceUVs_7;
	Vector3_t4282066566_marshaled_com* ___faceData_8;
	Il2CppChar* ___name_9;
	Il2CppChar* ___fileName_10;
};
