﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.D1
struct D1_t2098671325;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void Newtonsoft.Json.Utilities.D1::.ctor(System.Object,System.IntPtr)
extern "C"  void D1__ctor_m408689037 (D1_t2098671325 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.D1::Invoke()
extern "C"  Il2CppObject * D1_Invoke_m2395195676 (D1_t2098671325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Newtonsoft.Json.Utilities.D1::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * D1_BeginInvoke_m3651516668 (D1_t2098671325 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.D1::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * D1_EndInvoke_m433566034 (D1_t2098671325 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
