﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2927776761MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,HatredCtrl/stValue>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m869790255(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1090463813 *, Dictionary_2_t3758671658 *, const MethodInfo*))KeyCollection__ctor_m523256344_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,HatredCtrl/stValue>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m696026567(__this, ___item0, method) ((  void (*) (KeyCollection_t1090463813 *, CombatEntity_t684137495 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2255104062_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,HatredCtrl/stValue>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3303502078(__this, method) ((  void (*) (KeyCollection_t1090463813 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1593394485_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,HatredCtrl/stValue>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3116097187(__this, ___item0, method) ((  bool (*) (KeyCollection_t1090463813 *, CombatEntity_t684137495 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2639252176_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,HatredCtrl/stValue>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2913311944(__this, ___item0, method) ((  bool (*) (KeyCollection_t1090463813 *, CombatEntity_t684137495 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3887116341_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,HatredCtrl/stValue>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4005702202(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1090463813 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1898391879_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,HatredCtrl/stValue>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m1537329264(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1090463813 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m599523879_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,HatredCtrl/stValue>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4285479147(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1090463813 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1005180150_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,HatredCtrl/stValue>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3858250052(__this, method) ((  bool (*) (KeyCollection_t1090463813 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m876727857_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,HatredCtrl/stValue>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1991263990(__this, method) ((  bool (*) (KeyCollection_t1090463813 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m230010915_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,HatredCtrl/stValue>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m769172386(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1090463813 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2647228245_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,HatredCtrl/stValue>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m2623657188(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1090463813 *, CombatEntityU5BU5D_t3566310830*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2884372685_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,HatredCtrl/stValue>::GetEnumerator()
#define KeyCollection_GetEnumerator_m3539252039(__this, method) ((  Enumerator_t78640416  (*) (KeyCollection_t1090463813 *, const MethodInfo*))KeyCollection_GetEnumerator_m3262716634_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,HatredCtrl/stValue>::get_Count()
#define KeyCollection_get_Count_m4243863484(__this, method) ((  int32_t (*) (KeyCollection_t1090463813 *, const MethodInfo*))KeyCollection_get_Count_m1550142813_gshared)(__this, method)
