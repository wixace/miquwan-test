﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct DefaultComparer_t1081757877;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr_ErrorIn2633981210.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m3103776966_gshared (DefaultComparer_t1081757877 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3103776966(__this, method) ((  void (*) (DefaultComparer_t1081757877 *, const MethodInfo*))DefaultComparer__ctor_m3103776966_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Mihua.Assets.SubAssetMgr/ErrorInfo>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4155865709_gshared (DefaultComparer_t1081757877 * __this, ErrorInfo_t2633981210  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m4155865709(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1081757877 *, ErrorInfo_t2633981210 , const MethodInfo*))DefaultComparer_GetHashCode_m4155865709_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Mihua.Assets.SubAssetMgr/ErrorInfo>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m852917595_gshared (DefaultComparer_t1081757877 * __this, ErrorInfo_t2633981210  ___x0, ErrorInfo_t2633981210  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m852917595(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1081757877 *, ErrorInfo_t2633981210 , ErrorInfo_t2633981210 , const MethodInfo*))DefaultComparer_Equals_m852917595_gshared)(__this, ___x0, ___y1, method)
