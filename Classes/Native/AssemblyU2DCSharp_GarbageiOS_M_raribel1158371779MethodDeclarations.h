﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_raribel11
struct M_raribel11_t58371779;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_raribel1158371779.h"

// System.Void GarbageiOS.M_raribel11::.ctor()
extern "C"  void M_raribel11__ctor_m2032098816 (M_raribel11_t58371779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_raribel11::M_hawkeatiCirwem0(System.String[],System.Int32)
extern "C"  void M_raribel11_M_hawkeatiCirwem0_m1088128962 (M_raribel11_t58371779 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_raribel11::M_ritelyi1(System.String[],System.Int32)
extern "C"  void M_raribel11_M_ritelyi1_m1621317546 (M_raribel11_t58371779 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_raribel11::M_zinajerTrobu2(System.String[],System.Int32)
extern "C"  void M_raribel11_M_zinajerTrobu2_m2576025222 (M_raribel11_t58371779 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_raribel11::M_sterairChayci3(System.String[],System.Int32)
extern "C"  void M_raribel11_M_sterairChayci3_m3121788993 (M_raribel11_t58371779 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_raribel11::M_selde4(System.String[],System.Int32)
extern "C"  void M_raribel11_M_selde4_m1973088006 (M_raribel11_t58371779 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_raribel11::ilo_M_hawkeatiCirwem01(GarbageiOS.M_raribel11,System.String[],System.Int32)
extern "C"  void M_raribel11_ilo_M_hawkeatiCirwem01_m2455358589 (Il2CppObject * __this /* static, unused */, M_raribel11_t58371779 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_raribel11::ilo_M_ritelyi12(GarbageiOS.M_raribel11,System.String[],System.Int32)
extern "C"  void M_raribel11_ilo_M_ritelyi12_m619297738 (Il2CppObject * __this /* static, unused */, M_raribel11_t58371779 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_raribel11::ilo_M_zinajerTrobu23(GarbageiOS.M_raribel11,System.String[],System.Int32)
extern "C"  void M_raribel11_ilo_M_zinajerTrobu23_m3340456895 (Il2CppObject * __this /* static, unused */, M_raribel11_t58371779 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
