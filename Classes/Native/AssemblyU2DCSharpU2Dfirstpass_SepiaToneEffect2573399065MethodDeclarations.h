﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SepiaToneEffect
struct SepiaToneEffect_t2573399065;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void SepiaToneEffect::.ctor()
extern "C"  void SepiaToneEffect__ctor_m2963821118 (SepiaToneEffect_t2573399065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SepiaToneEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void SepiaToneEffect_OnRenderImage_m3713837920 (SepiaToneEffect_t2573399065 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
