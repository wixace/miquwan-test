﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_teresorheLecerow112
struct M_teresorheLecerow112_t1897548578;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_teresorheLecerow112::.ctor()
extern "C"  void M_teresorheLecerow112__ctor_m4074372737 (M_teresorheLecerow112_t1897548578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_teresorheLecerow112::M_hikalltarPisjouwhi0(System.String[],System.Int32)
extern "C"  void M_teresorheLecerow112_M_hikalltarPisjouwhi0_m92096384 (M_teresorheLecerow112_t1897548578 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_teresorheLecerow112::M_cowzoomeaJane1(System.String[],System.Int32)
extern "C"  void M_teresorheLecerow112_M_cowzoomeaJane1_m665200917 (M_teresorheLecerow112_t1897548578 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_teresorheLecerow112::M_qerooKurhar2(System.String[],System.Int32)
extern "C"  void M_teresorheLecerow112_M_qerooKurhar2_m756244623 (M_teresorheLecerow112_t1897548578 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
