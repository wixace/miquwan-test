﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_CubemapGenerated
struct UnityEngine_CubemapGenerated_t3683889584;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_CubemapGenerated::.ctor()
extern "C"  void UnityEngine_CubemapGenerated__ctor_m1193408235 (UnityEngine_CubemapGenerated_t3683889584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CubemapGenerated::Cubemap_Cubemap1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CubemapGenerated_Cubemap_Cubemap1_m4228701343 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CubemapGenerated::Cubemap_mipmapCount(JSVCall)
extern "C"  void UnityEngine_CubemapGenerated_Cubemap_mipmapCount_m4193911855 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CubemapGenerated::Cubemap_format(JSVCall)
extern "C"  void UnityEngine_CubemapGenerated_Cubemap_format_m3787285311 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CubemapGenerated::Cubemap_Apply__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CubemapGenerated_Cubemap_Apply__Boolean__Boolean_m1221917691 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CubemapGenerated::Cubemap_Apply__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CubemapGenerated_Cubemap_Apply__Boolean_m169951759 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CubemapGenerated::Cubemap_Apply(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CubemapGenerated_Cubemap_Apply_m2039354747 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CubemapGenerated::Cubemap_GetPixel__CubemapFace__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CubemapGenerated_Cubemap_GetPixel__CubemapFace__Int32__Int32_m1528577 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CubemapGenerated::Cubemap_GetPixels__CubemapFace__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CubemapGenerated_Cubemap_GetPixels__CubemapFace__Int32_m4241936634 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CubemapGenerated::Cubemap_GetPixels__CubemapFace(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CubemapGenerated_Cubemap_GetPixels__CubemapFace_m1561919894 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CubemapGenerated::Cubemap_SetPixel__CubemapFace__Int32__Int32__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CubemapGenerated_Cubemap_SetPixel__CubemapFace__Int32__Int32__Color_m1546232504 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CubemapGenerated::Cubemap_SetPixels__Color_Array__CubemapFace__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CubemapGenerated_Cubemap_SetPixels__Color_Array__CubemapFace__Int32_m3750836525 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CubemapGenerated::Cubemap_SetPixels__Color_Array__CubemapFace(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CubemapGenerated_Cubemap_SetPixels__Color_Array__CubemapFace_m3220118723 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CubemapGenerated::Cubemap_SmoothEdges__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CubemapGenerated_Cubemap_SmoothEdges__Int32_m448739035 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CubemapGenerated::Cubemap_SmoothEdges(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CubemapGenerated_Cubemap_SmoothEdges_m1799030485 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CubemapGenerated::__Register()
extern "C"  void UnityEngine_CubemapGenerated___Register_m3293323452 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine_CubemapGenerated::<Cubemap_SetPixels__Color_Array__CubemapFace__Int32>m__19E()
extern "C"  ColorU5BU5D_t2441545636* UnityEngine_CubemapGenerated_U3CCubemap_SetPixels__Color_Array__CubemapFace__Int32U3Em__19E_m3199352544 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine_CubemapGenerated::<Cubemap_SetPixels__Color_Array__CubemapFace>m__19F()
extern "C"  ColorU5BU5D_t2441545636* UnityEngine_CubemapGenerated_U3CCubemap_SetPixels__Color_Array__CubemapFaceU3Em__19F_m2497835857 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CubemapGenerated::ilo_getInt321(System.Int32)
extern "C"  int32_t UnityEngine_CubemapGenerated_ilo_getInt321_m4129987762 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CubemapGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_CubemapGenerated_ilo_addJSCSRel2_m3519912104 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CubemapGenerated::ilo_setEnum3(System.Int32,System.Int32)
extern "C"  void UnityEngine_CubemapGenerated_ilo_setEnum3_m47949642 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CubemapGenerated::ilo_moveSaveID2Arr4(System.Int32)
extern "C"  void UnityEngine_CubemapGenerated_ilo_moveSaveID2Arr4_m2183824387 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CubemapGenerated::ilo_getEnum5(System.Int32)
extern "C"  int32_t UnityEngine_CubemapGenerated_ilo_getEnum5_m3906481097 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_CubemapGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_CubemapGenerated_ilo_getObject6_m2767573289 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CubemapGenerated::ilo_getArrayLength7(System.Int32)
extern "C"  int32_t UnityEngine_CubemapGenerated_ilo_getArrayLength7_m1361313863 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CubemapGenerated::ilo_getElement8(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_CubemapGenerated_ilo_getElement8_m3530196908 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
