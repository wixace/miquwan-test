﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._f28cf315aa393e21752336c1d24f0e4f
struct _f28cf315aa393e21752336c1d24f0e4f_t2589423855;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._f28cf315aa393e21752336c1d24f0e4f::.ctor()
extern "C"  void _f28cf315aa393e21752336c1d24f0e4f__ctor_m1357072926 (_f28cf315aa393e21752336c1d24f0e4f_t2589423855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f28cf315aa393e21752336c1d24f0e4f::_f28cf315aa393e21752336c1d24f0e4fm2(System.Int32)
extern "C"  int32_t _f28cf315aa393e21752336c1d24f0e4f__f28cf315aa393e21752336c1d24f0e4fm2_m2505732921 (_f28cf315aa393e21752336c1d24f0e4f_t2589423855 * __this, int32_t ____f28cf315aa393e21752336c1d24f0e4fa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f28cf315aa393e21752336c1d24f0e4f::_f28cf315aa393e21752336c1d24f0e4fm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _f28cf315aa393e21752336c1d24f0e4f__f28cf315aa393e21752336c1d24f0e4fm_m2443003805 (_f28cf315aa393e21752336c1d24f0e4f_t2589423855 * __this, int32_t ____f28cf315aa393e21752336c1d24f0e4fa0, int32_t ____f28cf315aa393e21752336c1d24f0e4f551, int32_t ____f28cf315aa393e21752336c1d24f0e4fc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
