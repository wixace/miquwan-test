﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSGuideData
struct CSGuideData_t659641910;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CSGuideData659641910.h"

// System.Void CSGuideData::.ctor()
extern "C"  void CSGuideData__ctor_m175686133 (CSGuideData_t659641910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGuideData::LoadData(System.String)
extern "C"  void CSGuideData_LoadData_m1900340739 (CSGuideData_t659641910 * __this, String_t* ___jsonStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGuideData::AssinGuideVOListByNpcId()
extern "C"  void CSGuideData_AssinGuideVOListByNpcId_m1878426603 (CSGuideData_t659641910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSGuideData::CanTriggerNpcHpGuide(System.Int32,System.Int32)
extern "C"  bool CSGuideData_CanTriggerNpcHpGuide_m1870520976 (CSGuideData_t659641910 * __this, int32_t ___npcId0, int32_t ___preHp1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGuideData::UnLoadData()
extern "C"  void CSGuideData_UnLoadData_m3556194136 (CSGuideData_t659641910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGuideData::ilo_AssinGuideVOListByNpcId1(CSGuideData)
extern "C"  void CSGuideData_ilo_AssinGuideVOListByNpcId1_m2699258081 (Il2CppObject * __this /* static, unused */, CSGuideData_t659641910 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
