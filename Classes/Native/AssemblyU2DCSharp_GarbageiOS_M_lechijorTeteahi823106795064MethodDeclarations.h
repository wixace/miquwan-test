﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_lechijorTeteahi82
struct M_lechijorTeteahi82_t3106795064;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_lechijorTeteahi823106795064.h"

// System.Void GarbageiOS.M_lechijorTeteahi82::.ctor()
extern "C"  void M_lechijorTeteahi82__ctor_m4113701419 (M_lechijorTeteahi82_t3106795064 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lechijorTeteahi82::M_fereperRusoo0(System.String[],System.Int32)
extern "C"  void M_lechijorTeteahi82_M_fereperRusoo0_m2025022777 (M_lechijorTeteahi82_t3106795064 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lechijorTeteahi82::M_stuxowmem1(System.String[],System.Int32)
extern "C"  void M_lechijorTeteahi82_M_stuxowmem1_m298938282 (M_lechijorTeteahi82_t3106795064 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lechijorTeteahi82::M_caypelqi2(System.String[],System.Int32)
extern "C"  void M_lechijorTeteahi82_M_caypelqi2_m1846271724 (M_lechijorTeteahi82_t3106795064 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lechijorTeteahi82::ilo_M_fereperRusoo01(GarbageiOS.M_lechijorTeteahi82,System.String[],System.Int32)
extern "C"  void M_lechijorTeteahi82_ilo_M_fereperRusoo01_m4101770857 (Il2CppObject * __this /* static, unused */, M_lechijorTeteahi82_t3106795064 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
