﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayMgrGenerated
struct ReplayMgrGenerated_t1020011518;
// JSVCall
struct JSVCall_t3708497963;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String
struct String_t;
// ReplayMgr
struct ReplayMgr_t1549183121;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ReplayMgr1549183121.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void ReplayMgrGenerated::.ctor()
extern "C"  void ReplayMgrGenerated__ctor_m2888792669 (ReplayMgrGenerated_t1020011518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayMgrGenerated::ReplayMgr_ReplayMgr1(JSVCall,System.Int32)
extern "C"  bool ReplayMgrGenerated_ReplayMgr_ReplayMgr1_m3911989145 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgrGenerated::ReplayMgr_REC_COMPRESS(JSVCall)
extern "C"  void ReplayMgrGenerated_ReplayMgr_REC_COMPRESS_m2152543681 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgrGenerated::ReplayMgr_EnterCheckPoint(JSVCall)
extern "C"  void ReplayMgrGenerated_ReplayMgr_EnterCheckPoint_m1080447066 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgrGenerated::ReplayMgr_OpenHurtInfo(JSVCall)
extern "C"  void ReplayMgrGenerated_ReplayMgr_OpenHurtInfo_m2224517867 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgrGenerated::ReplayMgr_curJson(JSVCall)
extern "C"  void ReplayMgrGenerated_ReplayMgr_curJson_m2272517202 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgrGenerated::ReplayMgr_Instance(JSVCall)
extern "C"  void ReplayMgrGenerated_ReplayMgr_Instance_m2648248797 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgrGenerated::ReplayMgr_IsPlayREC(JSVCall)
extern "C"  void ReplayMgrGenerated_ReplayMgr_IsPlayREC_m383259080 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayMgrGenerated::ReplayMgr_AddData__ReplayExecuteType__CombatEntity__Object_Array(JSVCall,System.Int32)
extern "C"  bool ReplayMgrGenerated_ReplayMgr_AddData__ReplayExecuteType__CombatEntity__Object_Array_m1147544006 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayMgrGenerated::ReplayMgr_AddData__ReplayExecuteType__Object_Array(JSVCall,System.Int32)
extern "C"  bool ReplayMgrGenerated_ReplayMgr_AddData__ReplayExecuteType__Object_Array_m306054671 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayMgrGenerated::ReplayMgr_EndREC__Boolean(JSVCall,System.Int32)
extern "C"  bool ReplayMgrGenerated_ReplayMgr_EndREC__Boolean_m4148918012 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayMgrGenerated::ReplayMgr_ExitREC(JSVCall,System.Int32)
extern "C"  bool ReplayMgrGenerated_ReplayMgr_ExitREC_m2891966011 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayMgrGenerated::ReplayMgr_GetEnemyCSHeroUnits(JSVCall,System.Int32)
extern "C"  bool ReplayMgrGenerated_ReplayMgr_GetEnemyCSHeroUnits_m1284602076 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayMgrGenerated::ReplayMgr_GetEnemyHeroIDList(JSVCall,System.Int32)
extern "C"  bool ReplayMgrGenerated_ReplayMgr_GetEnemyHeroIDList_m3698249854 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayMgrGenerated::ReplayMgr_GetEnemyName(JSVCall,System.Int32)
extern "C"  bool ReplayMgrGenerated_ReplayMgr_GetEnemyName_m2994784182 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayMgrGenerated::ReplayMgr_GetEnemyPower(JSVCall,System.Int32)
extern "C"  bool ReplayMgrGenerated_ReplayMgr_GetEnemyPower_m778186044 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayMgrGenerated::ReplayMgr_GetMineCSHeroUnits(JSVCall,System.Int32)
extern "C"  bool ReplayMgrGenerated_ReplayMgr_GetMineCSHeroUnits_m3397747093 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayMgrGenerated::ReplayMgr_GetMineHeroIDList(JSVCall,System.Int32)
extern "C"  bool ReplayMgrGenerated_ReplayMgr_GetMineHeroIDList_m2380942501 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayMgrGenerated::ReplayMgr_GetMineName(JSVCall,System.Int32)
extern "C"  bool ReplayMgrGenerated_ReplayMgr_GetMineName_m2495229213 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayMgrGenerated::ReplayMgr_GetMinePower(JSVCall,System.Int32)
extern "C"  bool ReplayMgrGenerated_ReplayMgr_GetMinePower_m2471851189 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayMgrGenerated::ReplayMgr_ResetREC(JSVCall,System.Int32)
extern "C"  bool ReplayMgrGenerated_ReplayMgr_ResetREC_m209268218 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayMgrGenerated::ReplayMgr_StartREC(JSVCall,System.Int32)
extern "C"  bool ReplayMgrGenerated_ReplayMgr_StartREC_m1933546055 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayMgrGenerated::ReplayMgr_StartRecording(JSVCall,System.Int32)
extern "C"  bool ReplayMgrGenerated_ReplayMgr_StartRecording_m1003690088 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayMgrGenerated::ReplayMgr_ZUpdate(JSVCall,System.Int32)
extern "C"  bool ReplayMgrGenerated_ReplayMgr_ZUpdate_m1957691084 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgrGenerated::__Register()
extern "C"  void ReplayMgrGenerated___Register_m3435197578 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] ReplayMgrGenerated::<ReplayMgr_AddData__ReplayExecuteType__CombatEntity__Object_Array>m__92()
extern "C"  ObjectU5BU5D_t1108656482* ReplayMgrGenerated_U3CReplayMgr_AddData__ReplayExecuteType__CombatEntity__Object_ArrayU3Em__92_m3136405863 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] ReplayMgrGenerated::<ReplayMgr_AddData__ReplayExecuteType__Object_Array>m__93()
extern "C"  ObjectU5BU5D_t1108656482* ReplayMgrGenerated_U3CReplayMgr_AddData__ReplayExecuteType__Object_ArrayU3Em__93_m2759745777 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ReplayMgrGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t ReplayMgrGenerated_ilo_getObject1_m228623701 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgrGenerated::ilo_setStringS2(System.Int32,System.String)
extern "C"  void ReplayMgrGenerated_ilo_setStringS2_m649758503 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayMgrGenerated::ilo_get_IsPlayREC3(ReplayMgr)
extern "C"  bool ReplayMgrGenerated_ilo_get_IsPlayREC3_m2543069897 (Il2CppObject * __this /* static, unused */, ReplayMgr_t1549183121 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ReplayMgrGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * ReplayMgrGenerated_ilo_getObject4_m723568565 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgrGenerated::ilo_EndREC5(ReplayMgr,System.Boolean)
extern "C"  void ReplayMgrGenerated_ilo_EndREC5_m2142058980 (Il2CppObject * __this /* static, unused */, ReplayMgr_t1549183121 * ____this0, bool ___isWin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> ReplayMgrGenerated::ilo_GetEnemyHeroIDList6(ReplayMgr)
extern "C"  List_1_t2522024052 * ReplayMgrGenerated_ilo_GetEnemyHeroIDList6_m3834897162 (Il2CppObject * __this /* static, unused */, ReplayMgr_t1549183121 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ReplayMgrGenerated::ilo_setObject7(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t ReplayMgrGenerated_ilo_setObject7_m1457025619 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ReplayMgrGenerated::ilo_GetMinePower8(ReplayMgr)
extern "C"  float ReplayMgrGenerated_ilo_GetMinePower8_m3270794281 (Il2CppObject * __this /* static, unused */, ReplayMgr_t1549183121 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgrGenerated::ilo_setSingle9(System.Int32,System.Single)
extern "C"  void ReplayMgrGenerated_ilo_setSingle9_m342621439 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgrGenerated::ilo_StartRecording10(ReplayMgr)
extern "C"  void ReplayMgrGenerated_ilo_StartRecording10_m2581658489 (Il2CppObject * __this /* static, unused */, ReplayMgr_t1549183121 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMgrGenerated::ilo_ZUpdate11(ReplayMgr)
extern "C"  void ReplayMgrGenerated_ilo_ZUpdate11_m3782853686 (Il2CppObject * __this /* static, unused */, ReplayMgr_t1549183121 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ReplayMgrGenerated::ilo_getArrayLength12(System.Int32)
extern "C"  int32_t ReplayMgrGenerated_ilo_getArrayLength12_m653950019 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ReplayMgrGenerated::ilo_getElement13(System.Int32,System.Int32)
extern "C"  int32_t ReplayMgrGenerated_ilo_getElement13_m3445115158 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
