﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEffectMgrGenerated
struct UIEffectMgrGenerated_t4141446204;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// UIEffectMgr
struct UIEffectMgr_t952662675;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_UIEffectMgr952662675.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UIEffectMgrGenerated::.ctor()
extern "C"  void UIEffectMgrGenerated__ctor_m1290267103 (UIEffectMgrGenerated_t4141446204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIEffectMgrGenerated::UIEffectMgr_UIEffectMgr1(JSVCall,System.Int32)
extern "C"  bool UIEffectMgrGenerated_UIEffectMgr_UIEffectMgr1_m1461077715 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffectMgrGenerated::UIEffectMgr_curEffid(JSVCall)
extern "C"  void UIEffectMgrGenerated_UIEffectMgr_curEffid_m1899171478 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffectMgrGenerated::UIEffectMgr_effectParent(JSVCall)
extern "C"  void UIEffectMgrGenerated_UIEffectMgr_effectParent_m2846036155 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIEffectMgrGenerated::UIEffectMgr_Clear(JSVCall,System.Int32)
extern "C"  bool UIEffectMgrGenerated_UIEffectMgr_Clear_m2278542266 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIEffectMgrGenerated::UIEffectMgr_PlayUIEffect__Int32(JSVCall,System.Int32)
extern "C"  bool UIEffectMgrGenerated_UIEffectMgr_PlayUIEffect__Int32_m3493397090 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffectMgrGenerated::__Register()
extern "C"  void UIEffectMgrGenerated___Register_m650896968 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIEffectMgrGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UIEffectMgrGenerated_ilo_attachFinalizerObject1_m1582386144 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffectMgrGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UIEffectMgrGenerated_ilo_addJSCSRel2_m2063081884 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffectMgrGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void UIEffectMgrGenerated_ilo_setInt323_m1903603861 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIEffectMgrGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIEffectMgrGenerated_ilo_setObject4_m1232204238 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffectMgrGenerated::ilo_set_effectParent5(UIEffectMgr,UnityEngine.GameObject)
extern "C"  void UIEffectMgrGenerated_ilo_set_effectParent5_m161750726 (Il2CppObject * __this /* static, unused */, UIEffectMgr_t952662675 * ____this0, GameObject_t3674682005 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffectMgrGenerated::ilo_Clear6(UIEffectMgr)
extern "C"  void UIEffectMgrGenerated_ilo_Clear6_m985720862 (Il2CppObject * __this /* static, unused */, UIEffectMgr_t952662675 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
