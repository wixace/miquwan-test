﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DistanceUnit3471059769.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.String FingerGesturesExtensions::Abreviation(DistanceUnit)
extern "C"  String_t* FingerGesturesExtensions_Abreviation_m2716527076 (Il2CppObject * __this /* static, unused */, int32_t ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FingerGesturesExtensions::Convert(System.Single,DistanceUnit,DistanceUnit)
extern "C"  float FingerGesturesExtensions_Convert_m1546907730 (Il2CppObject * __this /* static, unused */, float ___value0, int32_t ___fromUnit1, int32_t ___toUnit2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FingerGesturesExtensions::In(System.Single,DistanceUnit)
extern "C"  float FingerGesturesExtensions_In_m3064477955 (Il2CppObject * __this /* static, unused */, float ___valueInPixels0, int32_t ___toUnit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FingerGesturesExtensions::Centimeters(System.Single)
extern "C"  float FingerGesturesExtensions_Centimeters_m4091367898 (Il2CppObject * __this /* static, unused */, float ___valueInPixels0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FingerGesturesExtensions::Inches(System.Single)
extern "C"  float FingerGesturesExtensions_Inches_m259715063 (Il2CppObject * __this /* static, unused */, float ___valueInPixels0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 FingerGesturesExtensions::Convert(UnityEngine.Vector2,DistanceUnit,DistanceUnit)
extern "C"  Vector2_t4282066565  FingerGesturesExtensions_Convert_m2327259888 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___v0, int32_t ___fromUnit1, int32_t ___toUnit2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 FingerGesturesExtensions::In(UnityEngine.Vector2,DistanceUnit)
extern "C"  Vector2_t4282066565  FingerGesturesExtensions_In_m3232573155 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___vecInPixels0, int32_t ___toUnit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 FingerGesturesExtensions::Centimeters(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  FingerGesturesExtensions_Centimeters_m2585303160 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___vecInPixels0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 FingerGesturesExtensions::Inches(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  FingerGesturesExtensions_Inches_m3395426263 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___vecInPixels0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 FingerGesturesExtensions::ilo_In1(UnityEngine.Vector2,DistanceUnit)
extern "C"  Vector2_t4282066565  FingerGesturesExtensions_ilo_In1_m1115596419 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___vecInPixels0, int32_t ___toUnit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
