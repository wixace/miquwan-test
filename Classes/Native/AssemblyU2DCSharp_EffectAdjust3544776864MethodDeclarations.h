﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectAdjust
struct EffectAdjust_t3544776864;

#include "codegen/il2cpp-codegen.h"

// System.Void EffectAdjust::.ctor()
extern "C"  void EffectAdjust__ctor_m75867643 (EffectAdjust_t3544776864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectAdjust::Start()
extern "C"  void EffectAdjust_Start_m3317972731 (EffectAdjust_t3544776864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectAdjust::Update()
extern "C"  void EffectAdjust_Update_m4078759026 (EffectAdjust_t3544776864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
