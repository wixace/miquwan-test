﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginTianYing
struct PluginTianYing_t4234189246;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.String
struct String_t;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// VersionMgr
struct VersionMgr_t1322950208;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// VersionInfo
struct VersionInfo_t2356638086;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "AssemblyU2DCSharp_PluginTianYing4234189246.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"

// System.Void PluginTianYing::.ctor()
extern "C"  void PluginTianYing__ctor_m2295548061 (PluginTianYing_t4234189246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTianYing::Init()
extern "C"  void PluginTianYing_Init_m718323223 (PluginTianYing_t4234189246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginTianYing::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginTianYing_ReqSDKHttpLogin_m2704266290 (PluginTianYing_t4234189246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginTianYing::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginTianYing_IsLoginSuccess_m3700259094 (PluginTianYing_t4234189246 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTianYing::OpenUserLogin()
extern "C"  void PluginTianYing_OpenUserLogin_m1824292335 (PluginTianYing_t4234189246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTianYing::UserPay(System.String)
extern "C"  void PluginTianYing_UserPay_m928146730 (PluginTianYing_t4234189246 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTianYing::UserPay(CEvent.ZEvent)
extern "C"  void PluginTianYing_UserPay_m3410582179 (PluginTianYing_t4234189246 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTianYing::OnLoginSuccess(System.String)
extern "C"  void PluginTianYing_OnLoginSuccess_m4172692450 (PluginTianYing_t4234189246 * __this, String_t* ___userID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTianYing::OnLogoutSuccess(System.String)
extern "C"  void PluginTianYing_OnLogoutSuccess_m507989741 (PluginTianYing_t4234189246 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTianYing::OnLogout(System.String)
extern "C"  void PluginTianYing_OnLogout_m92754162 (PluginTianYing_t4234189246 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTianYing::initSdk(System.String)
extern "C"  void PluginTianYing_initSdk_m2656921917 (PluginTianYing_t4234189246 * __this, String_t* ___channelId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTianYing::login(System.String,System.String)
extern "C"  void PluginTianYing_login_m4069804602 (PluginTianYing_t4234189246 * __this, String_t* ___userId0, String_t* ___userName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTianYing::logout()
extern "C"  void PluginTianYing_logout_m515697265 (PluginTianYing_t4234189246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTianYing::pay(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginTianYing_pay_m4066448079 (PluginTianYing_t4234189246 * __this, String_t* ___productId0, String_t* ___productName1, String_t* ___amount2, String_t* ___extra3, String_t* ___payUrl4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTianYing::<OnLogoutSuccess>m__459()
extern "C"  void PluginTianYing_U3COnLogoutSuccessU3Em__459_m3878626982 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTianYing::<OnLogout>m__45A()
extern "C"  void PluginTianYing_U3COnLogoutU3Em__45A_m2735148051 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginTianYing::ilo_get_isSdkLogin1(VersionMgr)
extern "C"  bool PluginTianYing_ilo_get_isSdkLogin1_m2870121251 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginTianYing::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginTianYing_ilo_get_Instance2_m3129942367 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginTianYing::ilo_GetProductID3(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginTianYing_ilo_GetProductID3_m3362042824 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginTianYing::ilo_get_currentVS4(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginTianYing_ilo_get_currentVS4_m2183921198 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTianYing::ilo_pay5(PluginTianYing,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginTianYing_ilo_pay5_m1773559061 (Il2CppObject * __this /* static, unused */, PluginTianYing_t4234189246 * ____this0, String_t* ___productId1, String_t* ___productName2, String_t* ___amount3, String_t* ___extra4, String_t* ___payUrl5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTianYing::ilo_ReqSDKHttpLogin6(PluginsSdkMgr)
extern "C"  void PluginTianYing_ilo_ReqSDKHttpLogin6_m1174279049 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginTianYing::ilo_get_PluginsSdkMgr7()
extern "C"  PluginsSdkMgr_t3884624670 * PluginTianYing_ilo_get_PluginsSdkMgr7_m1340574449 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
