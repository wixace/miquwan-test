﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory
struct DynamicReflectionDelegateFactory_t1511666027;
// System.Reflection.Emit.DynamicMethod
struct DynamicMethod_t2315379190;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3339007067;
// System.Reflection.MethodBase
struct MethodBase_t318515428;
// System.Reflection.Emit.ILGenerator
struct ILGenerator_t1499877190;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_MethodBase318515428.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator1499877190.h"
#include "mscorlib_System_Reflection_PropertyInfo924268725.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Dynami1511666027.h"

// System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::.ctor()
extern "C"  void DynamicReflectionDelegateFactory__ctor_m1900676263 (DynamicReflectionDelegateFactory_t1511666027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::.cctor()
extern "C"  void DynamicReflectionDelegateFactory__cctor_m2604293094 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.DynamicMethod Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::CreateDynamicMethod(System.String,System.Type,System.Type[],System.Type)
extern "C"  DynamicMethod_t2315379190 * DynamicReflectionDelegateFactory_CreateDynamicMethod_m1036401540 (Il2CppObject * __this /* static, unused */, String_t* ___name0, Type_t * ___returnType1, TypeU5BU5D_t3339007067* ___parameterTypes2, Type_t * ___owner3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::GenerateCreateMethodCallIL(System.Reflection.MethodBase,System.Reflection.Emit.ILGenerator)
extern "C"  void DynamicReflectionDelegateFactory_GenerateCreateMethodCallIL_m4127889203 (DynamicReflectionDelegateFactory_t1511666027 * __this, MethodBase_t318515428 * ___method0, ILGenerator_t1499877190 * ___generator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::GenerateCreateDefaultConstructorIL(System.Type,System.Reflection.Emit.ILGenerator)
extern "C"  void DynamicReflectionDelegateFactory_GenerateCreateDefaultConstructorIL_m1512040190 (DynamicReflectionDelegateFactory_t1511666027 * __this, Type_t * ___type0, ILGenerator_t1499877190 * ___generator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::GenerateCreateGetPropertyIL(System.Reflection.PropertyInfo,System.Reflection.Emit.ILGenerator)
extern "C"  void DynamicReflectionDelegateFactory_GenerateCreateGetPropertyIL_m3725731922 (DynamicReflectionDelegateFactory_t1511666027 * __this, PropertyInfo_t * ___propertyInfo0, ILGenerator_t1499877190 * ___generator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::GenerateCreateGetFieldIL(System.Reflection.FieldInfo,System.Reflection.Emit.ILGenerator)
extern "C"  void DynamicReflectionDelegateFactory_GenerateCreateGetFieldIL_m188962484 (DynamicReflectionDelegateFactory_t1511666027 * __this, FieldInfo_t * ___fieldInfo0, ILGenerator_t1499877190 * ___generator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::GenerateCreateSetFieldIL(System.Reflection.FieldInfo,System.Reflection.Emit.ILGenerator)
extern "C"  void DynamicReflectionDelegateFactory_GenerateCreateSetFieldIL_m1217332264 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___fieldInfo0, ILGenerator_t1499877190 * ___generator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::GenerateCreateSetPropertyIL(System.Reflection.PropertyInfo,System.Reflection.Emit.ILGenerator)
extern "C"  void DynamicReflectionDelegateFactory_GenerateCreateSetPropertyIL_m1165684422 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___propertyInfo0, ILGenerator_t1499877190 * ___generator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::ilo_CallMethod1(System.Reflection.Emit.ILGenerator,System.Reflection.MethodInfo)
extern "C"  void DynamicReflectionDelegateFactory_ilo_CallMethod1_m3664650216 (Il2CppObject * __this /* static, unused */, ILGenerator_t1499877190 * ___generator0, MethodInfo_t * ___methodInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::ilo_Return2(System.Reflection.Emit.ILGenerator)
extern "C"  void DynamicReflectionDelegateFactory_ilo_Return2_m3371250491 (Il2CppObject * __this /* static, unused */, ILGenerator_t1499877190 * ___generator0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.DynamicMethod Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::ilo_CreateDynamicMethod3(System.String,System.Type,System.Type[],System.Type)
extern "C"  DynamicMethod_t2315379190 * DynamicReflectionDelegateFactory_ilo_CreateDynamicMethod3_m2219200006 (Il2CppObject * __this /* static, unused */, String_t* ___name0, Type_t * ___returnType1, TypeU5BU5D_t3339007067* ___parameterTypes2, Type_t * ___owner3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::ilo_GenerateCreateGetPropertyIL4(Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory,System.Reflection.PropertyInfo,System.Reflection.Emit.ILGenerator)
extern "C"  void DynamicReflectionDelegateFactory_ilo_GenerateCreateGetPropertyIL4_m3012489313 (Il2CppObject * __this /* static, unused */, DynamicReflectionDelegateFactory_t1511666027 * ____this0, PropertyInfo_t * ___propertyInfo1, ILGenerator_t1499877190 * ___generator2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::ilo_FormatWith5(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* DynamicReflectionDelegateFactory_ilo_FormatWith5_m4207643973 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::ilo_PushInstance6(System.Reflection.Emit.ILGenerator,System.Type)
extern "C"  void DynamicReflectionDelegateFactory_ilo_PushInstance6_m2951462515 (Il2CppObject * __this /* static, unused */, ILGenerator_t1499877190 * ___generator0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::ilo_BoxIfNeeded7(System.Reflection.Emit.ILGenerator,System.Type)
extern "C"  void DynamicReflectionDelegateFactory_ilo_BoxIfNeeded7_m2998585060 (Il2CppObject * __this /* static, unused */, ILGenerator_t1499877190 * ___generator0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::ilo_GenerateCreateSetFieldIL8(System.Reflection.FieldInfo,System.Reflection.Emit.ILGenerator)
extern "C"  void DynamicReflectionDelegateFactory_ilo_GenerateCreateSetFieldIL8_m1145950205 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___fieldInfo0, ILGenerator_t1499877190 * ___generator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
