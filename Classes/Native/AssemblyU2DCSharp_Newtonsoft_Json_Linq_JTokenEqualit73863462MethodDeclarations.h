﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JTokenEqualityComparer
struct JTokenEqualityComparer_t73863462;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"

// System.Void Newtonsoft.Json.Linq.JTokenEqualityComparer::.ctor()
extern "C"  void JTokenEqualityComparer__ctor_m3499548700 (JTokenEqualityComparer_t73863462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JTokenEqualityComparer::Equals(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern "C"  bool JTokenEqualityComparer_Equals_m3185440251 (JTokenEqualityComparer_t73863462 * __this, JToken_t3412245951 * ___x0, JToken_t3412245951 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JTokenEqualityComparer::GetHashCode(Newtonsoft.Json.Linq.JToken)
extern "C"  int32_t JTokenEqualityComparer_GetHashCode_m178059825 (JTokenEqualityComparer_t73863462 * __this, JToken_t3412245951 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JTokenEqualityComparer::ilo_DeepEquals1(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern "C"  bool JTokenEqualityComparer_ilo_DeepEquals1_m2055250253 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___t10, JToken_t3412245951 * ___t21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
