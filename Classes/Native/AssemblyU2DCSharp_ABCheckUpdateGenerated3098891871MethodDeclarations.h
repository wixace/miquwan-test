﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ABCheckUpdateGenerated
struct ABCheckUpdateGenerated_t3098891871;
// JSVCall
struct JSVCall_t3708497963;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;
// ABCheckUpdate
struct ABCheckUpdate_t527832336;
// VersionInfo
struct VersionInfo_t2356638086;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ABCheckUpdate527832336.h"
#include "AssemblyU2DCSharp_VersionInfo2356638086.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void ABCheckUpdateGenerated::.ctor()
extern "C"  void ABCheckUpdateGenerated__ctor_m891853084 (ABCheckUpdateGenerated_t3098891871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ABCheckUpdateGenerated::ABCheckUpdate_ABCheckUpdate1(JSVCall,System.Int32)
extern "C"  bool ABCheckUpdateGenerated_ABCheckUpdate_ABCheckUpdate1_m2557384700 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdateGenerated::ABCheckUpdate_FLOW_CHECK_UPDATE(JSVCall)
extern "C"  void ABCheckUpdateGenerated_ABCheckUpdate_FLOW_CHECK_UPDATE_m607989643 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdateGenerated::ABCheckUpdate_clientVer(JSVCall)
extern "C"  void ABCheckUpdateGenerated_ABCheckUpdate_clientVer_m3467532548 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdateGenerated::ABCheckUpdate_serverVer(JSVCall)
extern "C"  void ABCheckUpdateGenerated_ABCheckUpdate_serverVer_m4211613820 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdateGenerated::ABCheckUpdate_isloadServer(JSVCall)
extern "C"  void ABCheckUpdateGenerated_ABCheckUpdate_isloadServer_m4079271517 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdateGenerated::ABCheckUpdate_GetUpdateTargetVS(JSVCall)
extern "C"  void ABCheckUpdateGenerated_ABCheckUpdate_GetUpdateTargetVS_m3501030831 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdateGenerated::ABCheckUpdate_Instance(JSVCall)
extern "C"  void ABCheckUpdateGenerated_ABCheckUpdate_Instance_m3617468763 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdateGenerated::ABCheckUpdate_Version(JSVCall)
extern "C"  void ABCheckUpdateGenerated_ABCheckUpdate_Version_m3503980964 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdateGenerated::ABCheckUpdate_IsDownSuccess(JSVCall)
extern "C"  void ABCheckUpdateGenerated_ABCheckUpdate_IsDownSuccess_m1997658981 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdateGenerated::ABCheckUpdate_WriteProgress(JSVCall)
extern "C"  void ABCheckUpdateGenerated_ABCheckUpdate_WriteProgress_m3036740944 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdateGenerated::ABCheckUpdate_isPkged(JSVCall)
extern "C"  void ABCheckUpdateGenerated_ABCheckUpdate_isPkged_m2582649883 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdateGenerated::ABCheckUpdate_PkgProgress(JSVCall)
extern "C"  void ABCheckUpdateGenerated_ABCheckUpdate_PkgProgress_m1713766531 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdateGenerated::ABCheckUpdate_isVerifyed(JSVCall)
extern "C"  void ABCheckUpdateGenerated_ABCheckUpdate_isVerifyed_m1259986958 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdateGenerated::ABCheckUpdate_VerifyProgress(JSVCall)
extern "C"  void ABCheckUpdateGenerated_ABCheckUpdate_VerifyProgress_m93398986 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdateGenerated::ABCheckUpdate_isStartDown(JSVCall)
extern "C"  void ABCheckUpdateGenerated_ABCheckUpdate_isStartDown_m4185074562 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdateGenerated::ABCheckUpdate_Progress(JSVCall)
extern "C"  void ABCheckUpdateGenerated_ABCheckUpdate_Progress_m4003319491 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdateGenerated::ABCheckUpdate_ProgressMsg(JSVCall)
extern "C"  void ABCheckUpdateGenerated_ABCheckUpdate_ProgressMsg_m1071527144 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ABCheckUpdateGenerated::ABCheckUpdate_Start(JSVCall,System.Int32)
extern "C"  bool ABCheckUpdateGenerated_ABCheckUpdate_Start_m3087784969 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ABCheckUpdateGenerated::ABCheckUpdate_StartDown(JSVCall,System.Int32)
extern "C"  bool ABCheckUpdateGenerated_ABCheckUpdate_StartDown_m1719032651 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ABCheckUpdateGenerated::ABCheckUpdate_UpdateVersion__VersionInfo(JSVCall,System.Int32)
extern "C"  bool ABCheckUpdateGenerated_ABCheckUpdate_UpdateVersion__VersionInfo_m442934706 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ABCheckUpdateGenerated::ABCheckUpdate_BytesToString__Byte_Array__Encoding(JSVCall,System.Int32)
extern "C"  bool ABCheckUpdateGenerated_ABCheckUpdate_BytesToString__Byte_Array__Encoding_m89832819 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdateGenerated::__Register()
extern "C"  void ABCheckUpdateGenerated___Register_m1857565995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ABCheckUpdateGenerated::<ABCheckUpdate_BytesToString__Byte_Array__Encoding>m__0()
extern "C"  ByteU5BU5D_t4260760469* ABCheckUpdateGenerated_U3CABCheckUpdate_BytesToString__Byte_Array__EncodingU3Em__0_m1015476176 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdateGenerated::ilo_setStringS1(System.Int32,System.String)
extern "C"  void ABCheckUpdateGenerated_ilo_setStringS1_m2464386533 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ABCheckUpdateGenerated::ilo_getStringS2(System.Int32)
extern "C"  String_t* ABCheckUpdateGenerated_ilo_getStringS2_m1537357147 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdateGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void ABCheckUpdateGenerated_ilo_setBooleanS3_m4172389835 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ABCheckUpdate ABCheckUpdateGenerated::ilo_get_Instance4()
extern "C"  ABCheckUpdate_t527832336 * ABCheckUpdateGenerated_ilo_get_Instance4_m1064350618 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ABCheckUpdateGenerated::ilo_get_isPkged5(ABCheckUpdate)
extern "C"  bool ABCheckUpdateGenerated_ilo_get_isPkged5_m2065491446 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ABCheckUpdateGenerated::ilo_get_PkgProgress6(ABCheckUpdate)
extern "C"  float ABCheckUpdateGenerated_ilo_get_PkgProgress6_m3215443413 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdateGenerated::ilo_setSingle7(System.Int32,System.Single)
extern "C"  void ABCheckUpdateGenerated_ilo_setSingle7_m838427870 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ABCheckUpdateGenerated::ilo_get_isVerifyed8(ABCheckUpdate)
extern "C"  bool ABCheckUpdateGenerated_ilo_get_isVerifyed8_m1253615714 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdateGenerated::ilo_UpdateVersion9(ABCheckUpdate,VersionInfo)
extern "C"  void ABCheckUpdateGenerated_ilo_UpdateVersion9_m1396370883 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, VersionInfo_t2356638086 * ___vsInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ABCheckUpdateGenerated::ilo_getObject10(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * ABCheckUpdateGenerated_ilo_getObject10_m2005258765 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ABCheckUpdateGenerated::ilo_getObject11(System.Int32)
extern "C"  int32_t ABCheckUpdateGenerated_ilo_getObject11_m492089247 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ABCheckUpdateGenerated::ilo_getElement12(System.Int32,System.Int32)
extern "C"  int32_t ABCheckUpdateGenerated_ilo_getElement12_m3107374070 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
