﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_doolo107
struct  M_doolo107_t3808727073  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_doolo107::_sowaw
	String_t* ____sowaw_0;
	// System.Single GarbageiOS.M_doolo107::_zerowpa
	float ____zerowpa_1;
	// System.Boolean GarbageiOS.M_doolo107::_fizisHowfearwou
	bool ____fizisHowfearwou_2;
	// System.String GarbageiOS.M_doolo107::_jerenou
	String_t* ____jerenou_3;
	// System.String GarbageiOS.M_doolo107::_selkar
	String_t* ____selkar_4;
	// System.Int32 GarbageiOS.M_doolo107::_delcur
	int32_t ____delcur_5;
	// System.Boolean GarbageiOS.M_doolo107::_qaqeara
	bool ____qaqeara_6;
	// System.Int32 GarbageiOS.M_doolo107::_rerejearpoo
	int32_t ____rerejearpoo_7;
	// System.String GarbageiOS.M_doolo107::_sahe
	String_t* ____sahe_8;
	// System.Single GarbageiOS.M_doolo107::_cowmeeselTutre
	float ____cowmeeselTutre_9;
	// System.Int32 GarbageiOS.M_doolo107::_punerewa
	int32_t ____punerewa_10;
	// System.Boolean GarbageiOS.M_doolo107::_como
	bool ____como_11;
	// System.Int32 GarbageiOS.M_doolo107::_halmel
	int32_t ____halmel_12;
	// System.Boolean GarbageiOS.M_doolo107::_jiraki
	bool ____jiraki_13;
	// System.UInt32 GarbageiOS.M_doolo107::_drirball
	uint32_t ____drirball_14;
	// System.Single GarbageiOS.M_doolo107::_trusiwoo
	float ____trusiwoo_15;
	// System.Boolean GarbageiOS.M_doolo107::_dasmawnorMiche
	bool ____dasmawnorMiche_16;
	// System.Boolean GarbageiOS.M_doolo107::_kebu
	bool ____kebu_17;
	// System.String GarbageiOS.M_doolo107::_bata
	String_t* ____bata_18;
	// System.UInt32 GarbageiOS.M_doolo107::_kesouPearma
	uint32_t ____kesouPearma_19;
	// System.Single GarbageiOS.M_doolo107::_nairselzaFirnamu
	float ____nairselzaFirnamu_20;
	// System.Boolean GarbageiOS.M_doolo107::_sischimarLeceremo
	bool ____sischimarLeceremo_21;
	// System.UInt32 GarbageiOS.M_doolo107::_serrabairTayi
	uint32_t ____serrabairTayi_22;
	// System.UInt32 GarbageiOS.M_doolo107::_mellaras
	uint32_t ____mellaras_23;
	// System.Single GarbageiOS.M_doolo107::_kovemnir
	float ____kovemnir_24;
	// System.Int32 GarbageiOS.M_doolo107::_lorow
	int32_t ____lorow_25;
	// System.String GarbageiOS.M_doolo107::_lore
	String_t* ____lore_26;
	// System.String GarbageiOS.M_doolo107::_mearfairLaynaiho
	String_t* ____mearfairLaynaiho_27;
	// System.Single GarbageiOS.M_doolo107::_trurpaDorbalur
	float ____trurpaDorbalur_28;
	// System.Int32 GarbageiOS.M_doolo107::_teadreCheno
	int32_t ____teadreCheno_29;
	// System.String GarbageiOS.M_doolo107::_nese
	String_t* ____nese_30;
	// System.Int32 GarbageiOS.M_doolo107::_qutidisKejairho
	int32_t ____qutidisKejairho_31;

public:
	inline static int32_t get_offset_of__sowaw_0() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____sowaw_0)); }
	inline String_t* get__sowaw_0() const { return ____sowaw_0; }
	inline String_t** get_address_of__sowaw_0() { return &____sowaw_0; }
	inline void set__sowaw_0(String_t* value)
	{
		____sowaw_0 = value;
		Il2CppCodeGenWriteBarrier(&____sowaw_0, value);
	}

	inline static int32_t get_offset_of__zerowpa_1() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____zerowpa_1)); }
	inline float get__zerowpa_1() const { return ____zerowpa_1; }
	inline float* get_address_of__zerowpa_1() { return &____zerowpa_1; }
	inline void set__zerowpa_1(float value)
	{
		____zerowpa_1 = value;
	}

	inline static int32_t get_offset_of__fizisHowfearwou_2() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____fizisHowfearwou_2)); }
	inline bool get__fizisHowfearwou_2() const { return ____fizisHowfearwou_2; }
	inline bool* get_address_of__fizisHowfearwou_2() { return &____fizisHowfearwou_2; }
	inline void set__fizisHowfearwou_2(bool value)
	{
		____fizisHowfearwou_2 = value;
	}

	inline static int32_t get_offset_of__jerenou_3() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____jerenou_3)); }
	inline String_t* get__jerenou_3() const { return ____jerenou_3; }
	inline String_t** get_address_of__jerenou_3() { return &____jerenou_3; }
	inline void set__jerenou_3(String_t* value)
	{
		____jerenou_3 = value;
		Il2CppCodeGenWriteBarrier(&____jerenou_3, value);
	}

	inline static int32_t get_offset_of__selkar_4() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____selkar_4)); }
	inline String_t* get__selkar_4() const { return ____selkar_4; }
	inline String_t** get_address_of__selkar_4() { return &____selkar_4; }
	inline void set__selkar_4(String_t* value)
	{
		____selkar_4 = value;
		Il2CppCodeGenWriteBarrier(&____selkar_4, value);
	}

	inline static int32_t get_offset_of__delcur_5() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____delcur_5)); }
	inline int32_t get__delcur_5() const { return ____delcur_5; }
	inline int32_t* get_address_of__delcur_5() { return &____delcur_5; }
	inline void set__delcur_5(int32_t value)
	{
		____delcur_5 = value;
	}

	inline static int32_t get_offset_of__qaqeara_6() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____qaqeara_6)); }
	inline bool get__qaqeara_6() const { return ____qaqeara_6; }
	inline bool* get_address_of__qaqeara_6() { return &____qaqeara_6; }
	inline void set__qaqeara_6(bool value)
	{
		____qaqeara_6 = value;
	}

	inline static int32_t get_offset_of__rerejearpoo_7() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____rerejearpoo_7)); }
	inline int32_t get__rerejearpoo_7() const { return ____rerejearpoo_7; }
	inline int32_t* get_address_of__rerejearpoo_7() { return &____rerejearpoo_7; }
	inline void set__rerejearpoo_7(int32_t value)
	{
		____rerejearpoo_7 = value;
	}

	inline static int32_t get_offset_of__sahe_8() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____sahe_8)); }
	inline String_t* get__sahe_8() const { return ____sahe_8; }
	inline String_t** get_address_of__sahe_8() { return &____sahe_8; }
	inline void set__sahe_8(String_t* value)
	{
		____sahe_8 = value;
		Il2CppCodeGenWriteBarrier(&____sahe_8, value);
	}

	inline static int32_t get_offset_of__cowmeeselTutre_9() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____cowmeeselTutre_9)); }
	inline float get__cowmeeselTutre_9() const { return ____cowmeeselTutre_9; }
	inline float* get_address_of__cowmeeselTutre_9() { return &____cowmeeselTutre_9; }
	inline void set__cowmeeselTutre_9(float value)
	{
		____cowmeeselTutre_9 = value;
	}

	inline static int32_t get_offset_of__punerewa_10() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____punerewa_10)); }
	inline int32_t get__punerewa_10() const { return ____punerewa_10; }
	inline int32_t* get_address_of__punerewa_10() { return &____punerewa_10; }
	inline void set__punerewa_10(int32_t value)
	{
		____punerewa_10 = value;
	}

	inline static int32_t get_offset_of__como_11() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____como_11)); }
	inline bool get__como_11() const { return ____como_11; }
	inline bool* get_address_of__como_11() { return &____como_11; }
	inline void set__como_11(bool value)
	{
		____como_11 = value;
	}

	inline static int32_t get_offset_of__halmel_12() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____halmel_12)); }
	inline int32_t get__halmel_12() const { return ____halmel_12; }
	inline int32_t* get_address_of__halmel_12() { return &____halmel_12; }
	inline void set__halmel_12(int32_t value)
	{
		____halmel_12 = value;
	}

	inline static int32_t get_offset_of__jiraki_13() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____jiraki_13)); }
	inline bool get__jiraki_13() const { return ____jiraki_13; }
	inline bool* get_address_of__jiraki_13() { return &____jiraki_13; }
	inline void set__jiraki_13(bool value)
	{
		____jiraki_13 = value;
	}

	inline static int32_t get_offset_of__drirball_14() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____drirball_14)); }
	inline uint32_t get__drirball_14() const { return ____drirball_14; }
	inline uint32_t* get_address_of__drirball_14() { return &____drirball_14; }
	inline void set__drirball_14(uint32_t value)
	{
		____drirball_14 = value;
	}

	inline static int32_t get_offset_of__trusiwoo_15() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____trusiwoo_15)); }
	inline float get__trusiwoo_15() const { return ____trusiwoo_15; }
	inline float* get_address_of__trusiwoo_15() { return &____trusiwoo_15; }
	inline void set__trusiwoo_15(float value)
	{
		____trusiwoo_15 = value;
	}

	inline static int32_t get_offset_of__dasmawnorMiche_16() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____dasmawnorMiche_16)); }
	inline bool get__dasmawnorMiche_16() const { return ____dasmawnorMiche_16; }
	inline bool* get_address_of__dasmawnorMiche_16() { return &____dasmawnorMiche_16; }
	inline void set__dasmawnorMiche_16(bool value)
	{
		____dasmawnorMiche_16 = value;
	}

	inline static int32_t get_offset_of__kebu_17() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____kebu_17)); }
	inline bool get__kebu_17() const { return ____kebu_17; }
	inline bool* get_address_of__kebu_17() { return &____kebu_17; }
	inline void set__kebu_17(bool value)
	{
		____kebu_17 = value;
	}

	inline static int32_t get_offset_of__bata_18() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____bata_18)); }
	inline String_t* get__bata_18() const { return ____bata_18; }
	inline String_t** get_address_of__bata_18() { return &____bata_18; }
	inline void set__bata_18(String_t* value)
	{
		____bata_18 = value;
		Il2CppCodeGenWriteBarrier(&____bata_18, value);
	}

	inline static int32_t get_offset_of__kesouPearma_19() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____kesouPearma_19)); }
	inline uint32_t get__kesouPearma_19() const { return ____kesouPearma_19; }
	inline uint32_t* get_address_of__kesouPearma_19() { return &____kesouPearma_19; }
	inline void set__kesouPearma_19(uint32_t value)
	{
		____kesouPearma_19 = value;
	}

	inline static int32_t get_offset_of__nairselzaFirnamu_20() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____nairselzaFirnamu_20)); }
	inline float get__nairselzaFirnamu_20() const { return ____nairselzaFirnamu_20; }
	inline float* get_address_of__nairselzaFirnamu_20() { return &____nairselzaFirnamu_20; }
	inline void set__nairselzaFirnamu_20(float value)
	{
		____nairselzaFirnamu_20 = value;
	}

	inline static int32_t get_offset_of__sischimarLeceremo_21() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____sischimarLeceremo_21)); }
	inline bool get__sischimarLeceremo_21() const { return ____sischimarLeceremo_21; }
	inline bool* get_address_of__sischimarLeceremo_21() { return &____sischimarLeceremo_21; }
	inline void set__sischimarLeceremo_21(bool value)
	{
		____sischimarLeceremo_21 = value;
	}

	inline static int32_t get_offset_of__serrabairTayi_22() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____serrabairTayi_22)); }
	inline uint32_t get__serrabairTayi_22() const { return ____serrabairTayi_22; }
	inline uint32_t* get_address_of__serrabairTayi_22() { return &____serrabairTayi_22; }
	inline void set__serrabairTayi_22(uint32_t value)
	{
		____serrabairTayi_22 = value;
	}

	inline static int32_t get_offset_of__mellaras_23() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____mellaras_23)); }
	inline uint32_t get__mellaras_23() const { return ____mellaras_23; }
	inline uint32_t* get_address_of__mellaras_23() { return &____mellaras_23; }
	inline void set__mellaras_23(uint32_t value)
	{
		____mellaras_23 = value;
	}

	inline static int32_t get_offset_of__kovemnir_24() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____kovemnir_24)); }
	inline float get__kovemnir_24() const { return ____kovemnir_24; }
	inline float* get_address_of__kovemnir_24() { return &____kovemnir_24; }
	inline void set__kovemnir_24(float value)
	{
		____kovemnir_24 = value;
	}

	inline static int32_t get_offset_of__lorow_25() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____lorow_25)); }
	inline int32_t get__lorow_25() const { return ____lorow_25; }
	inline int32_t* get_address_of__lorow_25() { return &____lorow_25; }
	inline void set__lorow_25(int32_t value)
	{
		____lorow_25 = value;
	}

	inline static int32_t get_offset_of__lore_26() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____lore_26)); }
	inline String_t* get__lore_26() const { return ____lore_26; }
	inline String_t** get_address_of__lore_26() { return &____lore_26; }
	inline void set__lore_26(String_t* value)
	{
		____lore_26 = value;
		Il2CppCodeGenWriteBarrier(&____lore_26, value);
	}

	inline static int32_t get_offset_of__mearfairLaynaiho_27() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____mearfairLaynaiho_27)); }
	inline String_t* get__mearfairLaynaiho_27() const { return ____mearfairLaynaiho_27; }
	inline String_t** get_address_of__mearfairLaynaiho_27() { return &____mearfairLaynaiho_27; }
	inline void set__mearfairLaynaiho_27(String_t* value)
	{
		____mearfairLaynaiho_27 = value;
		Il2CppCodeGenWriteBarrier(&____mearfairLaynaiho_27, value);
	}

	inline static int32_t get_offset_of__trurpaDorbalur_28() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____trurpaDorbalur_28)); }
	inline float get__trurpaDorbalur_28() const { return ____trurpaDorbalur_28; }
	inline float* get_address_of__trurpaDorbalur_28() { return &____trurpaDorbalur_28; }
	inline void set__trurpaDorbalur_28(float value)
	{
		____trurpaDorbalur_28 = value;
	}

	inline static int32_t get_offset_of__teadreCheno_29() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____teadreCheno_29)); }
	inline int32_t get__teadreCheno_29() const { return ____teadreCheno_29; }
	inline int32_t* get_address_of__teadreCheno_29() { return &____teadreCheno_29; }
	inline void set__teadreCheno_29(int32_t value)
	{
		____teadreCheno_29 = value;
	}

	inline static int32_t get_offset_of__nese_30() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____nese_30)); }
	inline String_t* get__nese_30() const { return ____nese_30; }
	inline String_t** get_address_of__nese_30() { return &____nese_30; }
	inline void set__nese_30(String_t* value)
	{
		____nese_30 = value;
		Il2CppCodeGenWriteBarrier(&____nese_30, value);
	}

	inline static int32_t get_offset_of__qutidisKejairho_31() { return static_cast<int32_t>(offsetof(M_doolo107_t3808727073, ____qutidisKejairho_31)); }
	inline int32_t get__qutidisKejairho_31() const { return ____qutidisKejairho_31; }
	inline int32_t* get_address_of__qutidisKejairho_31() { return &____qutidisKejairho_31; }
	inline void set__qutidisKejairho_31(int32_t value)
	{
		____qutidisKejairho_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
