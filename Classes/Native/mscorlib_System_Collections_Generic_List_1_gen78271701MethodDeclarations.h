﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::.ctor()
#define List_1__ctor_m98740260(__this, method) ((  void (*) (List_1_t78271701 *, const MethodInfo*))List_1__ctor_m1239231859_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m2488886962(__this, ___collection0, method) ((  void (*) (List_1_t78271701 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::.ctor(System.Int32)
#define List_1__ctor_m519927166(__this, ___capacity0, method) ((  void (*) (List_1_t78271701 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::.cctor()
#define List_1__cctor_m3968492960(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Pathfinding.MeshNode>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2383251575(__this, method) ((  Il2CppObject* (*) (List_1_t78271701 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1870217783(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t78271701 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Pathfinding.MeshNode>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1111957766(__this, method) ((  Il2CppObject * (*) (List_1_t78271701 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.MeshNode>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m59108343(__this, ___item0, method) ((  int32_t (*) (List_1_t78271701 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.MeshNode>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1419938985(__this, ___item0, method) ((  bool (*) (List_1_t78271701 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.MeshNode>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1701924559(__this, ___item0, method) ((  int32_t (*) (List_1_t78271701 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m4023188418(__this, ___index0, ___item1, method) ((  void (*) (List_1_t78271701 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m1435417126(__this, ___item0, method) ((  void (*) (List_1_t78271701 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.MeshNode>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1854504298(__this, method) ((  bool (*) (List_1_t78271701 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.MeshNode>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m264379923(__this, method) ((  bool (*) (List_1_t78271701 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.MeshNode>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m2208607557(__this, method) ((  Il2CppObject * (*) (List_1_t78271701 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.MeshNode>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m4028051160(__this, method) ((  bool (*) (List_1_t78271701 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.MeshNode>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m2246513953(__this, method) ((  bool (*) (List_1_t78271701 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.MeshNode>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1823629708(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t78271701 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1427455769(__this, ___index0, ___value1, method) ((  void (*) (List_1_t78271701 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::Add(T)
#define List_1_Add_m4088068372(__this, ___item0, method) ((  void (*) (List_1_t78271701 *, MeshNode_t3005053445 *, const MethodInfo*))List_1_Add_m933592675_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m3222871149(__this, ___newCount0, method) ((  void (*) (List_1_t78271701 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m3949654362(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t78271701 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m590371457_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1987346283(__this, ___collection0, method) ((  void (*) (List_1_t78271701 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m1248318507(__this, ___enumerable0, method) ((  void (*) (List_1_t78271701 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m2167008140(__this, ___collection0, method) ((  void (*) (List_1_t78271701 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Pathfinding.MeshNode>::AsReadOnly()
#define List_1_AsReadOnly_m3914157305(__this, method) ((  ReadOnlyCollection_1_t267163685 * (*) (List_1_t78271701 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.MeshNode>::BinarySearch(T)
#define List_1_BinarySearch_m261763258(__this, ___item0, method) ((  int32_t (*) (List_1_t78271701 *, MeshNode_t3005053445 *, const MethodInfo*))List_1_BinarySearch_m2761349225_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::Clear()
#define List_1_Clear_m1799840847(__this, method) ((  void (*) (List_1_t78271701 *, const MethodInfo*))List_1_Clear_m2940332446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.MeshNode>::Contains(T)
#define List_1_Contains_m1704904394(__this, ___item0, method) ((  bool (*) (List_1_t78271701 *, MeshNode_t3005053445 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m3862280866(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t78271701 *, MeshNodeU5BU5D_t2521861192*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Pathfinding.MeshNode>::Find(System.Predicate`1<T>)
#define List_1_Find_m861475748(__this, ___match0, method) ((  MeshNode_t3005053445 * (*) (List_1_t78271701 *, Predicate_1_t2616110328 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m2790734465(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2616110328 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.MeshNode>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m4259826014(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t78271701 *, int32_t, int32_t, Predicate_1_t2616110328 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Pathfinding.MeshNode>::GetEnumerator()
#define List_1_GetEnumerator_m2074415879(__this, method) ((  Enumerator_t97944471  (*) (List_1_t78271701 *, const MethodInfo*))List_1_GetEnumerator_m1919240000_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.MeshNode>::IndexOf(T)
#define List_1_IndexOf_m716898286(__this, ___item0, method) ((  int32_t (*) (List_1_t78271701 *, MeshNode_t3005053445 *, const MethodInfo*))List_1_IndexOf_m2776798407_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m3649163001(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t78271701 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m3608648050(__this, ___index0, method) ((  void (*) (List_1_t78271701 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::Insert(System.Int32,T)
#define List_1_Insert_m2917536153(__this, ___index0, ___item1, method) ((  void (*) (List_1_t78271701 *, int32_t, MeshNode_t3005053445 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m3400966990(__this, ___collection0, method) ((  void (*) (List_1_t78271701 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.MeshNode>::Remove(T)
#define List_1_Remove_m1015722309(__this, ___item0, method) ((  bool (*) (List_1_t78271701 *, MeshNode_t3005053445 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.MeshNode>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m2719547697(__this, ___match0, method) ((  int32_t (*) (List_1_t78271701 *, Predicate_1_t2616110328 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m791389023(__this, ___index0, method) ((  void (*) (List_1_t78271701 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2208877785_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m1915412866(__this, ___index0, ___count1, method) ((  void (*) (List_1_t78271701 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m856857915_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::Reverse()
#define List_1_Reverse_m567122093(__this, method) ((  void (*) (List_1_t78271701 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::Sort()
#define List_1_Sort_m1919626773(__this, method) ((  void (*) (List_1_t78271701 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m1551053679(__this, ___comparer0, method) ((  void (*) (List_1_t78271701 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m2840349864(__this, ___comparison0, method) ((  void (*) (List_1_t78271701 *, Comparison_1_t1721414632 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Pathfinding.MeshNode>::ToArray()
#define List_1_ToArray_m951351468(__this, method) ((  MeshNodeU5BU5D_t2521861192* (*) (List_1_t78271701 *, const MethodInfo*))List_1_ToArray_m1449333879_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::TrimExcess()
#define List_1_TrimExcess_m1407363566(__this, method) ((  void (*) (List_1_t78271701 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.MeshNode>::get_Capacity()
#define List_1_get_Capacity_m878530846(__this, method) ((  int32_t (*) (List_1_t78271701 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m433059967(__this, ___value0, method) ((  void (*) (List_1_t78271701 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.MeshNode>::get_Count()
#define List_1_get_Count_m566317901(__this, method) ((  int32_t (*) (List_1_t78271701 *, const MethodInfo*))List_1_get_Count_m3549598589_gshared)(__this, method)
// T System.Collections.Generic.List`1<Pathfinding.MeshNode>::get_Item(System.Int32)
#define List_1_get_Item_m1348776069(__this, ___index0, method) ((  MeshNode_t3005053445 * (*) (List_1_t78271701 *, int32_t, const MethodInfo*))List_1_get_Item_m404118264_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.MeshNode>::set_Item(System.Int32,T)
#define List_1_set_Item_m948195376(__this, ___index0, ___value1, method) ((  void (*) (List_1_t78271701 *, int32_t, MeshNode_t3005053445 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
