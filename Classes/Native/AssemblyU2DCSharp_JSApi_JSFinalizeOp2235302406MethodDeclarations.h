﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSApi/JSFinalizeOp
struct JSFinalizeOp_t2235302406;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void JSApi/JSFinalizeOp::.ctor(System.Object,System.IntPtr)
extern "C"  void JSFinalizeOp__ctor_m4124871085 (JSFinalizeOp_t2235302406 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi/JSFinalizeOp::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void JSFinalizeOp_Invoke_m318948923 (JSFinalizeOp_t2235302406 * __this, IntPtr_t ___freeOp0, IntPtr_t ___obj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult JSApi/JSFinalizeOp::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * JSFinalizeOp_BeginInvoke_m813061312 (JSFinalizeOp_t2235302406 * __this, IntPtr_t ___freeOp0, IntPtr_t ___obj1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi/JSFinalizeOp::EndInvoke(System.IAsyncResult)
extern "C"  void JSFinalizeOp_EndInvoke_m2099894845 (JSFinalizeOp_t2235302406 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
