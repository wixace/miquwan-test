﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSCWaveNpcConfig
struct JSCWaveNpcConfig_t4221504656;
// ProtoBuf.IExtension
struct IExtension_t1606339106;
// System.Collections.Generic.List`1<JSCLevelMonsterConfig>
struct List_1_t3292265250;

#include "codegen/il2cpp-codegen.h"

// System.Void JSCWaveNpcConfig::.ctor()
extern "C"  void JSCWaveNpcConfig__ctor_m1781742603 (JSCWaveNpcConfig_t4221504656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension JSCWaveNpcConfig::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * JSCWaveNpcConfig_ProtoBuf_IExtensible_GetExtensionObject_m3127601115 (JSCWaveNpcConfig_t4221504656 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSCWaveNpcConfig::get_delay()
extern "C"  float JSCWaveNpcConfig_get_delay_m2961289431 (JSCWaveNpcConfig_t4221504656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCWaveNpcConfig::set_delay(System.Single)
extern "C"  void JSCWaveNpcConfig_set_delay_m773716700 (JSCWaveNpcConfig_t4221504656 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSCWaveNpcConfig::get_distance()
extern "C"  float JSCWaveNpcConfig_get_distance_m882140099 (JSCWaveNpcConfig_t4221504656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCWaveNpcConfig::set_distance(System.Single)
extern "C"  void JSCWaveNpcConfig_set_distance_m2931399968 (JSCWaveNpcConfig_t4221504656 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<JSCLevelMonsterConfig> JSCWaveNpcConfig::get_wave()
extern "C"  List_1_t3292265250 * JSCWaveNpcConfig_get_wave_m817456398 (JSCWaveNpcConfig_t4221504656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
