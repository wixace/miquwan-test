﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t291504320;
// UISprite
struct UISprite_t661437049;

#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Float_normal
struct  Float_normal_t2052312138  : public FloatTextUnit_t2362298029
{
public:
	// UILabel Float_normal::LabelFile
	UILabel_t291504320 * ___LabelFile_7;
	// UISprite Float_normal::SpriteDitu
	UISprite_t661437049 * ___SpriteDitu_8;

public:
	inline static int32_t get_offset_of_LabelFile_7() { return static_cast<int32_t>(offsetof(Float_normal_t2052312138, ___LabelFile_7)); }
	inline UILabel_t291504320 * get_LabelFile_7() const { return ___LabelFile_7; }
	inline UILabel_t291504320 ** get_address_of_LabelFile_7() { return &___LabelFile_7; }
	inline void set_LabelFile_7(UILabel_t291504320 * value)
	{
		___LabelFile_7 = value;
		Il2CppCodeGenWriteBarrier(&___LabelFile_7, value);
	}

	inline static int32_t get_offset_of_SpriteDitu_8() { return static_cast<int32_t>(offsetof(Float_normal_t2052312138, ___SpriteDitu_8)); }
	inline UISprite_t661437049 * get_SpriteDitu_8() const { return ___SpriteDitu_8; }
	inline UISprite_t661437049 ** get_address_of_SpriteDitu_8() { return &___SpriteDitu_8; }
	inline void set_SpriteDitu_8(UISprite_t661437049 * value)
	{
		___SpriteDitu_8 = value;
		Il2CppCodeGenWriteBarrier(&___SpriteDitu_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
