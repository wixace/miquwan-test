﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Events_UnityEvent77Generated/<UnityEventA1_RemoveListener_GetDelegate_member2_arg0>c__AnonStoreyE3`1<System.Object>
struct U3CUnityEventA1_RemoveListener_GetDelegate_member2_arg0U3Ec__AnonStoreyE3_1_t2326124099;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_Events_UnityEvent77Generated/<UnityEventA1_RemoveListener_GetDelegate_member2_arg0>c__AnonStoreyE3`1<System.Object>::.ctor()
extern "C"  void U3CUnityEventA1_RemoveListener_GetDelegate_member2_arg0U3Ec__AnonStoreyE3_1__ctor_m3437836498_gshared (U3CUnityEventA1_RemoveListener_GetDelegate_member2_arg0U3Ec__AnonStoreyE3_1_t2326124099 * __this, const MethodInfo* method);
#define U3CUnityEventA1_RemoveListener_GetDelegate_member2_arg0U3Ec__AnonStoreyE3_1__ctor_m3437836498(__this, method) ((  void (*) (U3CUnityEventA1_RemoveListener_GetDelegate_member2_arg0U3Ec__AnonStoreyE3_1_t2326124099 *, const MethodInfo*))U3CUnityEventA1_RemoveListener_GetDelegate_member2_arg0U3Ec__AnonStoreyE3_1__ctor_m3437836498_gshared)(__this, method)
// System.Void UnityEngine_Events_UnityEvent77Generated/<UnityEventA1_RemoveListener_GetDelegate_member2_arg0>c__AnonStoreyE3`1<System.Object>::<>m__1AE(T0)
extern "C"  void U3CUnityEventA1_RemoveListener_GetDelegate_member2_arg0U3Ec__AnonStoreyE3_1_U3CU3Em__1AE_m436829536_gshared (U3CUnityEventA1_RemoveListener_GetDelegate_member2_arg0U3Ec__AnonStoreyE3_1_t2326124099 * __this, Il2CppObject * ___arg00, const MethodInfo* method);
#define U3CUnityEventA1_RemoveListener_GetDelegate_member2_arg0U3Ec__AnonStoreyE3_1_U3CU3Em__1AE_m436829536(__this, ___arg00, method) ((  void (*) (U3CUnityEventA1_RemoveListener_GetDelegate_member2_arg0U3Ec__AnonStoreyE3_1_t2326124099 *, Il2CppObject *, const MethodInfo*))U3CUnityEventA1_RemoveListener_GetDelegate_member2_arg0U3Ec__AnonStoreyE3_1_U3CU3Em__1AE_m436829536_gshared)(__this, ___arg00, method)
