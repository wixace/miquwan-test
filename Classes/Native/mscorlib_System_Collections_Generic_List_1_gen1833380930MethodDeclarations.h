﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>
struct List_1_t1833380930;
// System.Collections.Generic.IEnumerable`1<Pathfinding.AdvancedSmooth/Turn>
struct IEnumerable_1_t3766108335;
// System.Collections.Generic.IEnumerator`1<Pathfinding.AdvancedSmooth/Turn>
struct IEnumerator_1_t2377060427;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Pathfinding.AdvancedSmooth/Turn>
struct ICollection_1_t1359785365;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>
struct ReadOnlyCollection_1_t2022272914;
// Pathfinding.AdvancedSmooth/Turn[]
struct TurnU5BU5D_t2705444999;
// System.Predicate`1<Pathfinding.AdvancedSmooth/Turn>
struct Predicate_1_t76252261;
// System.Collections.Generic.IComparer`1<Pathfinding.AdvancedSmooth/Turn>
struct IComparer_1_t3040209420;
// System.Comparison`1<Pathfinding.AdvancedSmooth/Turn>
struct Comparison_1_t3476523861;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_AdvancedSmooth_Turn465195378.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1853053700.h"

// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::.ctor()
extern "C"  void List_1__ctor_m2746356881_gshared (List_1_t1833380930 * __this, const MethodInfo* method);
#define List_1__ctor_m2746356881(__this, method) ((  void (*) (List_1_t1833380930 *, const MethodInfo*))List_1__ctor_m2746356881_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m679909911_gshared (List_1_t1833380930 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m679909911(__this, ___collection0, method) ((  void (*) (List_1_t1833380930 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m679909911_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1687313081_gshared (List_1_t1833380930 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1687313081(__this, ___capacity0, method) ((  void (*) (List_1_t1833380930 *, int32_t, const MethodInfo*))List_1__ctor_m1687313081_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::.cctor()
extern "C"  void List_1__cctor_m4105205573_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m4105205573(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m4105205573_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1016684474_gshared (List_1_t1833380930 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1016684474(__this, method) ((  Il2CppObject* (*) (List_1_t1833380930 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1016684474_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3668058332_gshared (List_1_t1833380930 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3668058332(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1833380930 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3668058332_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3771693911_gshared (List_1_t1833380930 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3771693911(__this, method) ((  Il2CppObject * (*) (List_1_t1833380930 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3771693911_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m4223609466_gshared (List_1_t1833380930 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m4223609466(__this, ___item0, method) ((  int32_t (*) (List_1_t1833380930 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m4223609466_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m4234316818_gshared (List_1_t1833380930 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m4234316818(__this, ___item0, method) ((  bool (*) (List_1_t1833380930 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m4234316818_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m168924114_gshared (List_1_t1833380930 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m168924114(__this, ___item0, method) ((  int32_t (*) (List_1_t1833380930 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m168924114_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3233991165_gshared (List_1_t1833380930 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3233991165(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1833380930 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3233991165_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m2940168203_gshared (List_1_t1833380930 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m2940168203(__this, ___item0, method) ((  void (*) (List_1_t1833380930 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m2940168203_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2472807827_gshared (List_1_t1833380930 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2472807827(__this, method) ((  bool (*) (List_1_t1833380930 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2472807827_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m1337817610_gshared (List_1_t1833380930 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1337817610(__this, method) ((  bool (*) (List_1_t1833380930 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1337817610_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3288297014_gshared (List_1_t1833380930 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m3288297014(__this, method) ((  Il2CppObject * (*) (List_1_t1833380930 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3288297014_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3925946625_gshared (List_1_t1833380930 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3925946625(__this, method) ((  bool (*) (List_1_t1833380930 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3925946625_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m4044335576_gshared (List_1_t1833380930 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m4044335576(__this, method) ((  bool (*) (List_1_t1833380930 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m4044335576_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m23112253_gshared (List_1_t1833380930 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m23112253(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1833380930 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m23112253_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3218107028_gshared (List_1_t1833380930 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3218107028(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1833380930 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3218107028_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::Add(T)
extern "C"  void List_1_Add_m2440717697_gshared (List_1_t1833380930 * __this, Turn_t465195378  ___item0, const MethodInfo* method);
#define List_1_Add_m2440717697(__this, ___item0, method) ((  void (*) (List_1_t1833380930 *, Turn_t465195378 , const MethodInfo*))List_1_Add_m2440717697_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m705556178_gshared (List_1_t1833380930 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m705556178(__this, ___newCount0, method) ((  void (*) (List_1_t1833380930 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m705556178_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m1135943125_gshared (List_1_t1833380930 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m1135943125(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t1833380930 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1135943125_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2931719632_gshared (List_1_t1833380930 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2931719632(__this, ___collection0, method) ((  void (*) (List_1_t1833380930 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2931719632_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2192691856_gshared (List_1_t1833380930 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2192691856(__this, ___enumerable0, method) ((  void (*) (List_1_t1833380930 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2192691856_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m4181312007_gshared (List_1_t1833380930 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m4181312007(__this, ___collection0, method) ((  void (*) (List_1_t1833380930 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m4181312007_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2022272914 * List_1_AsReadOnly_m1301476290_gshared (List_1_t1833380930 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1301476290(__this, method) ((  ReadOnlyCollection_1_t2022272914 * (*) (List_1_t1833380930 *, const MethodInfo*))List_1_AsReadOnly_m1301476290_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m2604089789_gshared (List_1_t1833380930 * __this, Turn_t465195378  ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m2604089789(__this, ___item0, method) ((  int32_t (*) (List_1_t1833380930 *, Turn_t465195378 , const MethodInfo*))List_1_BinarySearch_m2604089789_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::Clear()
extern "C"  void List_1_Clear_m152490172_gshared (List_1_t1833380930 * __this, const MethodInfo* method);
#define List_1_Clear_m152490172(__this, method) ((  void (*) (List_1_t1833380930 *, const MethodInfo*))List_1_Clear_m152490172_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::Contains(T)
extern "C"  bool List_1_Contains_m2227611073_gshared (List_1_t1833380930 * __this, Turn_t465195378  ___item0, const MethodInfo* method);
#define List_1_Contains_m2227611073(__this, ___item0, method) ((  bool (*) (List_1_t1833380930 *, Turn_t465195378 , const MethodInfo*))List_1_Contains_m2227611073_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m664134215_gshared (List_1_t1833380930 * __this, TurnU5BU5D_t2705444999* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m664134215(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1833380930 *, TurnU5BU5D_t2705444999*, int32_t, const MethodInfo*))List_1_CopyTo_m664134215_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::Find(System.Predicate`1<T>)
extern "C"  Turn_t465195378  List_1_Find_m3977844225_gshared (List_1_t1833380930 * __this, Predicate_1_t76252261 * ___match0, const MethodInfo* method);
#define List_1_Find_m3977844225(__this, ___match0, method) ((  Turn_t465195378  (*) (List_1_t1833380930 *, Predicate_1_t76252261 *, const MethodInfo*))List_1_Find_m3977844225_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m2491997308_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t76252261 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m2491997308(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t76252261 *, const MethodInfo*))List_1_CheckMatch_m2491997308_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3177440929_gshared (List_1_t1833380930 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t76252261 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3177440929(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1833380930 *, int32_t, int32_t, Predicate_1_t76252261 *, const MethodInfo*))List_1_GetIndex_m3177440929_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::GetEnumerator()
extern "C"  Enumerator_t1853053700  List_1_GetEnumerator_m696767102_gshared (List_1_t1833380930 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m696767102(__this, method) ((  Enumerator_t1853053700  (*) (List_1_t1833380930 *, const MethodInfo*))List_1_GetEnumerator_m696767102_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m35789067_gshared (List_1_t1833380930 * __this, Turn_t465195378  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m35789067(__this, ___item0, method) ((  int32_t (*) (List_1_t1833380930 *, Turn_t465195378 , const MethodInfo*))List_1_IndexOf_m35789067_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3085336990_gshared (List_1_t1833380930 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3085336990(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1833380930 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3085336990_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m410501399_gshared (List_1_t1833380930 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m410501399(__this, ___index0, method) ((  void (*) (List_1_t1833380930 *, int32_t, const MethodInfo*))List_1_CheckIndex_m410501399_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m4161134206_gshared (List_1_t1833380930 * __this, int32_t ___index0, Turn_t465195378  ___item1, const MethodInfo* method);
#define List_1_Insert_m4161134206(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1833380930 *, int32_t, Turn_t465195378 , const MethodInfo*))List_1_Insert_m4161134206_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m410688627_gshared (List_1_t1833380930 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m410688627(__this, ___collection0, method) ((  void (*) (List_1_t1833380930 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m410688627_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::Remove(T)
extern "C"  bool List_1_Remove_m1127997948_gshared (List_1_t1833380930 * __this, Turn_t465195378  ___item0, const MethodInfo* method);
#define List_1_Remove_m1127997948(__this, ___item0, method) ((  bool (*) (List_1_t1833380930 *, Turn_t465195378 , const MethodInfo*))List_1_Remove_m1127997948_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m567654030_gshared (List_1_t1833380930 * __this, Predicate_1_t76252261 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m567654030(__this, ___match0, method) ((  int32_t (*) (List_1_t1833380930 *, Predicate_1_t76252261 *, const MethodInfo*))List_1_RemoveAll_m567654030_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2034987076_gshared (List_1_t1833380930 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m2034987076(__this, ___index0, method) ((  void (*) (List_1_t1833380930 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2034987076_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m589710439_gshared (List_1_t1833380930 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m589710439(__this, ___index0, ___count1, method) ((  void (*) (List_1_t1833380930 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m589710439_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::Reverse()
extern "C"  void List_1_Reverse_m510245800_gshared (List_1_t1833380930 * __this, const MethodInfo* method);
#define List_1_Reverse_m510245800(__this, method) ((  void (*) (List_1_t1833380930 *, const MethodInfo*))List_1_Reverse_m510245800_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::Sort()
extern "C"  void List_1_Sort_m1020348593_gshared (List_1_t1833380930 * __this, const MethodInfo* method);
#define List_1_Sort_m1020348593(__this, method) ((  void (*) (List_1_t1833380930 *, const MethodInfo*))List_1_Sort_m1020348593_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m953696810_gshared (List_1_t1833380930 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m953696810(__this, ___comparer0, method) ((  void (*) (List_1_t1833380930 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m953696810_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1767250381_gshared (List_1_t1833380930 * __this, Comparison_1_t3476523861 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1767250381(__this, ___comparison0, method) ((  void (*) (List_1_t1833380930 *, Comparison_1_t3476523861 *, const MethodInfo*))List_1_Sort_m1767250381_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::ToArray()
extern "C"  TurnU5BU5D_t2705444999* List_1_ToArray_m2782851559_gshared (List_1_t1833380930 * __this, const MethodInfo* method);
#define List_1_ToArray_m2782851559(__this, method) ((  TurnU5BU5D_t2705444999* (*) (List_1_t1833380930 *, const MethodInfo*))List_1_ToArray_m2782851559_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::TrimExcess()
extern "C"  void List_1_TrimExcess_m3517800723_gshared (List_1_t1833380930 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m3517800723(__this, method) ((  void (*) (List_1_t1833380930 *, const MethodInfo*))List_1_TrimExcess_m3517800723_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1646826427_gshared (List_1_t1833380930 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1646826427(__this, method) ((  int32_t (*) (List_1_t1833380930 *, const MethodInfo*))List_1_get_Capacity_m1646826427_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m2210712292_gshared (List_1_t1833380930 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m2210712292(__this, ___value0, method) ((  void (*) (List_1_t1833380930 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2210712292_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::get_Count()
extern "C"  int32_t List_1_get_Count_m1034509383_gshared (List_1_t1833380930 * __this, const MethodInfo* method);
#define List_1_get_Count_m1034509383(__this, method) ((  int32_t (*) (List_1_t1833380930 *, const MethodInfo*))List_1_get_Count_m1034509383_gshared)(__this, method)
// T System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::get_Item(System.Int32)
extern "C"  Turn_t465195378  List_1_get_Item_m3409914990_gshared (List_1_t1833380930 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3409914990(__this, ___index0, method) ((  Turn_t465195378  (*) (List_1_t1833380930 *, int32_t, const MethodInfo*))List_1_get_Item_m3409914990_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m2045016021_gshared (List_1_t1833380930 * __this, int32_t ___index0, Turn_t465195378  ___value1, const MethodInfo* method);
#define List_1_set_Item_m2045016021(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1833380930 *, int32_t, Turn_t465195378 , const MethodInfo*))List_1_set_Item_m2045016021_gshared)(__this, ___index0, ___value1, method)
