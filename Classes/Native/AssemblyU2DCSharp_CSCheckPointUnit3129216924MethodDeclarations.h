﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSCheckPointUnit
struct CSCheckPointUnit_t3129216924;

#include "codegen/il2cpp-codegen.h"

// System.Void CSCheckPointUnit::.ctor(System.UInt32)
extern "C"  void CSCheckPointUnit__ctor_m190009563 (CSCheckPointUnit_t3129216924 * __this, uint32_t ___checkId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
