﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Voxels.VoxelSpan
struct VoxelSpan_t3943867046;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Voxels.VoxelSpan::.ctor(System.UInt32,System.UInt32,System.Int32)
extern "C"  void VoxelSpan__ctor_m362606763 (VoxelSpan_t3943867046 * __this, uint32_t ___b0, uint32_t ___t1, int32_t ___area2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
