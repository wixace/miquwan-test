﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_rebecasBejooxal138
struct  M_rebecasBejooxal138_t1831173035  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_rebecasBejooxal138::_bota
	int32_t ____bota_0;
	// System.Int32 GarbageiOS.M_rebecasBejooxal138::_koojo
	int32_t ____koojo_1;
	// System.String GarbageiOS.M_rebecasBejooxal138::_roorastel
	String_t* ____roorastel_2;
	// System.Boolean GarbageiOS.M_rebecasBejooxal138::_gapedrayKecou
	bool ____gapedrayKecou_3;
	// System.UInt32 GarbageiOS.M_rebecasBejooxal138::_pajaDoufay
	uint32_t ____pajaDoufay_4;
	// System.UInt32 GarbageiOS.M_rebecasBejooxal138::_bajaw
	uint32_t ____bajaw_5;
	// System.Boolean GarbageiOS.M_rebecasBejooxal138::_loupeeNepooche
	bool ____loupeeNepooche_6;
	// System.UInt32 GarbageiOS.M_rebecasBejooxal138::_masarpair
	uint32_t ____masarpair_7;
	// System.UInt32 GarbageiOS.M_rebecasBejooxal138::_rearbir
	uint32_t ____rearbir_8;
	// System.Single GarbageiOS.M_rebecasBejooxal138::_jasravuNaqike
	float ____jasravuNaqike_9;
	// System.Single GarbageiOS.M_rebecasBejooxal138::_sonaw
	float ____sonaw_10;
	// System.UInt32 GarbageiOS.M_rebecasBejooxal138::_jalvirbeWisor
	uint32_t ____jalvirbeWisor_11;
	// System.Single GarbageiOS.M_rebecasBejooxal138::_gallgemalGallve
	float ____gallgemalGallve_12;
	// System.String GarbageiOS.M_rebecasBejooxal138::_paykeesaw
	String_t* ____paykeesaw_13;
	// System.Int32 GarbageiOS.M_rebecasBejooxal138::_bebirce
	int32_t ____bebirce_14;
	// System.Int32 GarbageiOS.M_rebecasBejooxal138::_joutergor
	int32_t ____joutergor_15;
	// System.UInt32 GarbageiOS.M_rebecasBejooxal138::_noodem
	uint32_t ____noodem_16;
	// System.UInt32 GarbageiOS.M_rebecasBejooxal138::_piseJeartorrai
	uint32_t ____piseJeartorrai_17;
	// System.Int32 GarbageiOS.M_rebecasBejooxal138::_metee
	int32_t ____metee_18;
	// System.String GarbageiOS.M_rebecasBejooxal138::_sirhurPowni
	String_t* ____sirhurPowni_19;
	// System.Boolean GarbageiOS.M_rebecasBejooxal138::_xuqefor
	bool ____xuqefor_20;
	// System.UInt32 GarbageiOS.M_rebecasBejooxal138::_gemnas
	uint32_t ____gemnas_21;
	// System.UInt32 GarbageiOS.M_rebecasBejooxal138::_recaykar
	uint32_t ____recaykar_22;
	// System.Boolean GarbageiOS.M_rebecasBejooxal138::_chemgalkaw
	bool ____chemgalkaw_23;
	// System.String GarbageiOS.M_rebecasBejooxal138::_dage
	String_t* ____dage_24;
	// System.Boolean GarbageiOS.M_rebecasBejooxal138::_canire
	bool ____canire_25;
	// System.Single GarbageiOS.M_rebecasBejooxal138::_telyayLowsaw
	float ____telyayLowsaw_26;

public:
	inline static int32_t get_offset_of__bota_0() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____bota_0)); }
	inline int32_t get__bota_0() const { return ____bota_0; }
	inline int32_t* get_address_of__bota_0() { return &____bota_0; }
	inline void set__bota_0(int32_t value)
	{
		____bota_0 = value;
	}

	inline static int32_t get_offset_of__koojo_1() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____koojo_1)); }
	inline int32_t get__koojo_1() const { return ____koojo_1; }
	inline int32_t* get_address_of__koojo_1() { return &____koojo_1; }
	inline void set__koojo_1(int32_t value)
	{
		____koojo_1 = value;
	}

	inline static int32_t get_offset_of__roorastel_2() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____roorastel_2)); }
	inline String_t* get__roorastel_2() const { return ____roorastel_2; }
	inline String_t** get_address_of__roorastel_2() { return &____roorastel_2; }
	inline void set__roorastel_2(String_t* value)
	{
		____roorastel_2 = value;
		Il2CppCodeGenWriteBarrier(&____roorastel_2, value);
	}

	inline static int32_t get_offset_of__gapedrayKecou_3() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____gapedrayKecou_3)); }
	inline bool get__gapedrayKecou_3() const { return ____gapedrayKecou_3; }
	inline bool* get_address_of__gapedrayKecou_3() { return &____gapedrayKecou_3; }
	inline void set__gapedrayKecou_3(bool value)
	{
		____gapedrayKecou_3 = value;
	}

	inline static int32_t get_offset_of__pajaDoufay_4() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____pajaDoufay_4)); }
	inline uint32_t get__pajaDoufay_4() const { return ____pajaDoufay_4; }
	inline uint32_t* get_address_of__pajaDoufay_4() { return &____pajaDoufay_4; }
	inline void set__pajaDoufay_4(uint32_t value)
	{
		____pajaDoufay_4 = value;
	}

	inline static int32_t get_offset_of__bajaw_5() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____bajaw_5)); }
	inline uint32_t get__bajaw_5() const { return ____bajaw_5; }
	inline uint32_t* get_address_of__bajaw_5() { return &____bajaw_5; }
	inline void set__bajaw_5(uint32_t value)
	{
		____bajaw_5 = value;
	}

	inline static int32_t get_offset_of__loupeeNepooche_6() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____loupeeNepooche_6)); }
	inline bool get__loupeeNepooche_6() const { return ____loupeeNepooche_6; }
	inline bool* get_address_of__loupeeNepooche_6() { return &____loupeeNepooche_6; }
	inline void set__loupeeNepooche_6(bool value)
	{
		____loupeeNepooche_6 = value;
	}

	inline static int32_t get_offset_of__masarpair_7() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____masarpair_7)); }
	inline uint32_t get__masarpair_7() const { return ____masarpair_7; }
	inline uint32_t* get_address_of__masarpair_7() { return &____masarpair_7; }
	inline void set__masarpair_7(uint32_t value)
	{
		____masarpair_7 = value;
	}

	inline static int32_t get_offset_of__rearbir_8() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____rearbir_8)); }
	inline uint32_t get__rearbir_8() const { return ____rearbir_8; }
	inline uint32_t* get_address_of__rearbir_8() { return &____rearbir_8; }
	inline void set__rearbir_8(uint32_t value)
	{
		____rearbir_8 = value;
	}

	inline static int32_t get_offset_of__jasravuNaqike_9() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____jasravuNaqike_9)); }
	inline float get__jasravuNaqike_9() const { return ____jasravuNaqike_9; }
	inline float* get_address_of__jasravuNaqike_9() { return &____jasravuNaqike_9; }
	inline void set__jasravuNaqike_9(float value)
	{
		____jasravuNaqike_9 = value;
	}

	inline static int32_t get_offset_of__sonaw_10() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____sonaw_10)); }
	inline float get__sonaw_10() const { return ____sonaw_10; }
	inline float* get_address_of__sonaw_10() { return &____sonaw_10; }
	inline void set__sonaw_10(float value)
	{
		____sonaw_10 = value;
	}

	inline static int32_t get_offset_of__jalvirbeWisor_11() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____jalvirbeWisor_11)); }
	inline uint32_t get__jalvirbeWisor_11() const { return ____jalvirbeWisor_11; }
	inline uint32_t* get_address_of__jalvirbeWisor_11() { return &____jalvirbeWisor_11; }
	inline void set__jalvirbeWisor_11(uint32_t value)
	{
		____jalvirbeWisor_11 = value;
	}

	inline static int32_t get_offset_of__gallgemalGallve_12() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____gallgemalGallve_12)); }
	inline float get__gallgemalGallve_12() const { return ____gallgemalGallve_12; }
	inline float* get_address_of__gallgemalGallve_12() { return &____gallgemalGallve_12; }
	inline void set__gallgemalGallve_12(float value)
	{
		____gallgemalGallve_12 = value;
	}

	inline static int32_t get_offset_of__paykeesaw_13() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____paykeesaw_13)); }
	inline String_t* get__paykeesaw_13() const { return ____paykeesaw_13; }
	inline String_t** get_address_of__paykeesaw_13() { return &____paykeesaw_13; }
	inline void set__paykeesaw_13(String_t* value)
	{
		____paykeesaw_13 = value;
		Il2CppCodeGenWriteBarrier(&____paykeesaw_13, value);
	}

	inline static int32_t get_offset_of__bebirce_14() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____bebirce_14)); }
	inline int32_t get__bebirce_14() const { return ____bebirce_14; }
	inline int32_t* get_address_of__bebirce_14() { return &____bebirce_14; }
	inline void set__bebirce_14(int32_t value)
	{
		____bebirce_14 = value;
	}

	inline static int32_t get_offset_of__joutergor_15() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____joutergor_15)); }
	inline int32_t get__joutergor_15() const { return ____joutergor_15; }
	inline int32_t* get_address_of__joutergor_15() { return &____joutergor_15; }
	inline void set__joutergor_15(int32_t value)
	{
		____joutergor_15 = value;
	}

	inline static int32_t get_offset_of__noodem_16() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____noodem_16)); }
	inline uint32_t get__noodem_16() const { return ____noodem_16; }
	inline uint32_t* get_address_of__noodem_16() { return &____noodem_16; }
	inline void set__noodem_16(uint32_t value)
	{
		____noodem_16 = value;
	}

	inline static int32_t get_offset_of__piseJeartorrai_17() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____piseJeartorrai_17)); }
	inline uint32_t get__piseJeartorrai_17() const { return ____piseJeartorrai_17; }
	inline uint32_t* get_address_of__piseJeartorrai_17() { return &____piseJeartorrai_17; }
	inline void set__piseJeartorrai_17(uint32_t value)
	{
		____piseJeartorrai_17 = value;
	}

	inline static int32_t get_offset_of__metee_18() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____metee_18)); }
	inline int32_t get__metee_18() const { return ____metee_18; }
	inline int32_t* get_address_of__metee_18() { return &____metee_18; }
	inline void set__metee_18(int32_t value)
	{
		____metee_18 = value;
	}

	inline static int32_t get_offset_of__sirhurPowni_19() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____sirhurPowni_19)); }
	inline String_t* get__sirhurPowni_19() const { return ____sirhurPowni_19; }
	inline String_t** get_address_of__sirhurPowni_19() { return &____sirhurPowni_19; }
	inline void set__sirhurPowni_19(String_t* value)
	{
		____sirhurPowni_19 = value;
		Il2CppCodeGenWriteBarrier(&____sirhurPowni_19, value);
	}

	inline static int32_t get_offset_of__xuqefor_20() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____xuqefor_20)); }
	inline bool get__xuqefor_20() const { return ____xuqefor_20; }
	inline bool* get_address_of__xuqefor_20() { return &____xuqefor_20; }
	inline void set__xuqefor_20(bool value)
	{
		____xuqefor_20 = value;
	}

	inline static int32_t get_offset_of__gemnas_21() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____gemnas_21)); }
	inline uint32_t get__gemnas_21() const { return ____gemnas_21; }
	inline uint32_t* get_address_of__gemnas_21() { return &____gemnas_21; }
	inline void set__gemnas_21(uint32_t value)
	{
		____gemnas_21 = value;
	}

	inline static int32_t get_offset_of__recaykar_22() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____recaykar_22)); }
	inline uint32_t get__recaykar_22() const { return ____recaykar_22; }
	inline uint32_t* get_address_of__recaykar_22() { return &____recaykar_22; }
	inline void set__recaykar_22(uint32_t value)
	{
		____recaykar_22 = value;
	}

	inline static int32_t get_offset_of__chemgalkaw_23() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____chemgalkaw_23)); }
	inline bool get__chemgalkaw_23() const { return ____chemgalkaw_23; }
	inline bool* get_address_of__chemgalkaw_23() { return &____chemgalkaw_23; }
	inline void set__chemgalkaw_23(bool value)
	{
		____chemgalkaw_23 = value;
	}

	inline static int32_t get_offset_of__dage_24() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____dage_24)); }
	inline String_t* get__dage_24() const { return ____dage_24; }
	inline String_t** get_address_of__dage_24() { return &____dage_24; }
	inline void set__dage_24(String_t* value)
	{
		____dage_24 = value;
		Il2CppCodeGenWriteBarrier(&____dage_24, value);
	}

	inline static int32_t get_offset_of__canire_25() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____canire_25)); }
	inline bool get__canire_25() const { return ____canire_25; }
	inline bool* get_address_of__canire_25() { return &____canire_25; }
	inline void set__canire_25(bool value)
	{
		____canire_25 = value;
	}

	inline static int32_t get_offset_of__telyayLowsaw_26() { return static_cast<int32_t>(offsetof(M_rebecasBejooxal138_t1831173035, ____telyayLowsaw_26)); }
	inline float get__telyayLowsaw_26() const { return ____telyayLowsaw_26; }
	inline float* get_address_of__telyayLowsaw_26() { return &____telyayLowsaw_26; }
	inline void set__telyayLowsaw_26(float value)
	{
		____telyayLowsaw_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
