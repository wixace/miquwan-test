﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// suodingEff
struct  suodingEff_t3997235994  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Transform suodingEff::targetTf
	Transform_t1659122786 * ___targetTf_2;
	// UnityEngine.Transform suodingEff::mTf
	Transform_t1659122786 * ___mTf_3;

public:
	inline static int32_t get_offset_of_targetTf_2() { return static_cast<int32_t>(offsetof(suodingEff_t3997235994, ___targetTf_2)); }
	inline Transform_t1659122786 * get_targetTf_2() const { return ___targetTf_2; }
	inline Transform_t1659122786 ** get_address_of_targetTf_2() { return &___targetTf_2; }
	inline void set_targetTf_2(Transform_t1659122786 * value)
	{
		___targetTf_2 = value;
		Il2CppCodeGenWriteBarrier(&___targetTf_2, value);
	}

	inline static int32_t get_offset_of_mTf_3() { return static_cast<int32_t>(offsetof(suodingEff_t3997235994, ___mTf_3)); }
	inline Transform_t1659122786 * get_mTf_3() const { return ___mTf_3; }
	inline Transform_t1659122786 ** get_address_of_mTf_3() { return &___mTf_3; }
	inline void set_mTf_3(Transform_t1659122786 * value)
	{
		___mTf_3 = value;
		Il2CppCodeGenWriteBarrier(&___mTf_3, value);
	}
};

struct suodingEff_t3997235994_StaticFields
{
public:
	// UnityEngine.Vector3 suodingEff::offset
	Vector3_t4282066566  ___offset_4;

public:
	inline static int32_t get_offset_of_offset_4() { return static_cast<int32_t>(offsetof(suodingEff_t3997235994_StaticFields, ___offset_4)); }
	inline Vector3_t4282066566  get_offset_4() const { return ___offset_4; }
	inline Vector3_t4282066566 * get_address_of_offset_4() { return &___offset_4; }
	inline void set_offset_4(Vector3_t4282066566  value)
	{
		___offset_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
