﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_rocuwear201
struct  M_rocuwear201_t3787691205  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_rocuwear201::_todo
	int32_t ____todo_0;
	// System.String GarbageiOS.M_rocuwear201::_dachilere
	String_t* ____dachilere_1;
	// System.Single GarbageiOS.M_rocuwear201::_tenearCeelu
	float ____tenearCeelu_2;
	// System.UInt32 GarbageiOS.M_rocuwear201::_kuryulere
	uint32_t ____kuryulere_3;
	// System.UInt32 GarbageiOS.M_rocuwear201::_nalljair
	uint32_t ____nalljair_4;
	// System.String GarbageiOS.M_rocuwear201::_nerer
	String_t* ____nerer_5;
	// System.Single GarbageiOS.M_rocuwear201::_lorpa
	float ____lorpa_6;
	// System.String GarbageiOS.M_rocuwear201::_sarwejeMibear
	String_t* ____sarwejeMibear_7;
	// System.Boolean GarbageiOS.M_rocuwear201::_norvairZepearni
	bool ____norvairZepearni_8;
	// System.Boolean GarbageiOS.M_rocuwear201::_vateMowhayga
	bool ____vateMowhayga_9;
	// System.String GarbageiOS.M_rocuwear201::_marabo
	String_t* ____marabo_10;
	// System.Boolean GarbageiOS.M_rocuwear201::_soway
	bool ____soway_11;
	// System.String GarbageiOS.M_rocuwear201::_whordowMinemem
	String_t* ____whordowMinemem_12;
	// System.Single GarbageiOS.M_rocuwear201::_jirta
	float ____jirta_13;
	// System.UInt32 GarbageiOS.M_rocuwear201::_sesotem
	uint32_t ____sesotem_14;
	// System.UInt32 GarbageiOS.M_rocuwear201::_sosiJatowkal
	uint32_t ____sosiJatowkal_15;
	// System.Int32 GarbageiOS.M_rocuwear201::_nastawCeedel
	int32_t ____nastawCeedel_16;
	// System.String GarbageiOS.M_rocuwear201::_caslallla
	String_t* ____caslallla_17;
	// System.Single GarbageiOS.M_rocuwear201::_purcousorCaykea
	float ____purcousorCaykea_18;
	// System.Single GarbageiOS.M_rocuwear201::_ruciMiswir
	float ____ruciMiswir_19;
	// System.String GarbageiOS.M_rocuwear201::_bihaw
	String_t* ____bihaw_20;
	// System.UInt32 GarbageiOS.M_rocuwear201::_ceagiHertou
	uint32_t ____ceagiHertou_21;
	// System.Boolean GarbageiOS.M_rocuwear201::_parwosowCofisma
	bool ____parwosowCofisma_22;
	// System.UInt32 GarbageiOS.M_rocuwear201::_pawticiSowkusem
	uint32_t ____pawticiSowkusem_23;
	// System.Boolean GarbageiOS.M_rocuwear201::_purwawbur
	bool ____purwawbur_24;
	// System.String GarbageiOS.M_rocuwear201::_zeesou
	String_t* ____zeesou_25;
	// System.Single GarbageiOS.M_rocuwear201::_kefirTairmisrel
	float ____kefirTairmisrel_26;
	// System.Boolean GarbageiOS.M_rocuwear201::_xekallstaw
	bool ____xekallstaw_27;
	// System.UInt32 GarbageiOS.M_rocuwear201::_whornimoJini
	uint32_t ____whornimoJini_28;
	// System.UInt32 GarbageiOS.M_rocuwear201::_lehisSoulelgi
	uint32_t ____lehisSoulelgi_29;
	// System.UInt32 GarbageiOS.M_rocuwear201::_rerrearJoujo
	uint32_t ____rerrearJoujo_30;
	// System.Int32 GarbageiOS.M_rocuwear201::_ruse
	int32_t ____ruse_31;
	// System.String GarbageiOS.M_rocuwear201::_qorxo
	String_t* ____qorxo_32;
	// System.UInt32 GarbageiOS.M_rocuwear201::_masallce
	uint32_t ____masallce_33;

public:
	inline static int32_t get_offset_of__todo_0() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____todo_0)); }
	inline int32_t get__todo_0() const { return ____todo_0; }
	inline int32_t* get_address_of__todo_0() { return &____todo_0; }
	inline void set__todo_0(int32_t value)
	{
		____todo_0 = value;
	}

	inline static int32_t get_offset_of__dachilere_1() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____dachilere_1)); }
	inline String_t* get__dachilere_1() const { return ____dachilere_1; }
	inline String_t** get_address_of__dachilere_1() { return &____dachilere_1; }
	inline void set__dachilere_1(String_t* value)
	{
		____dachilere_1 = value;
		Il2CppCodeGenWriteBarrier(&____dachilere_1, value);
	}

	inline static int32_t get_offset_of__tenearCeelu_2() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____tenearCeelu_2)); }
	inline float get__tenearCeelu_2() const { return ____tenearCeelu_2; }
	inline float* get_address_of__tenearCeelu_2() { return &____tenearCeelu_2; }
	inline void set__tenearCeelu_2(float value)
	{
		____tenearCeelu_2 = value;
	}

	inline static int32_t get_offset_of__kuryulere_3() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____kuryulere_3)); }
	inline uint32_t get__kuryulere_3() const { return ____kuryulere_3; }
	inline uint32_t* get_address_of__kuryulere_3() { return &____kuryulere_3; }
	inline void set__kuryulere_3(uint32_t value)
	{
		____kuryulere_3 = value;
	}

	inline static int32_t get_offset_of__nalljair_4() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____nalljair_4)); }
	inline uint32_t get__nalljair_4() const { return ____nalljair_4; }
	inline uint32_t* get_address_of__nalljair_4() { return &____nalljair_4; }
	inline void set__nalljair_4(uint32_t value)
	{
		____nalljair_4 = value;
	}

	inline static int32_t get_offset_of__nerer_5() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____nerer_5)); }
	inline String_t* get__nerer_5() const { return ____nerer_5; }
	inline String_t** get_address_of__nerer_5() { return &____nerer_5; }
	inline void set__nerer_5(String_t* value)
	{
		____nerer_5 = value;
		Il2CppCodeGenWriteBarrier(&____nerer_5, value);
	}

	inline static int32_t get_offset_of__lorpa_6() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____lorpa_6)); }
	inline float get__lorpa_6() const { return ____lorpa_6; }
	inline float* get_address_of__lorpa_6() { return &____lorpa_6; }
	inline void set__lorpa_6(float value)
	{
		____lorpa_6 = value;
	}

	inline static int32_t get_offset_of__sarwejeMibear_7() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____sarwejeMibear_7)); }
	inline String_t* get__sarwejeMibear_7() const { return ____sarwejeMibear_7; }
	inline String_t** get_address_of__sarwejeMibear_7() { return &____sarwejeMibear_7; }
	inline void set__sarwejeMibear_7(String_t* value)
	{
		____sarwejeMibear_7 = value;
		Il2CppCodeGenWriteBarrier(&____sarwejeMibear_7, value);
	}

	inline static int32_t get_offset_of__norvairZepearni_8() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____norvairZepearni_8)); }
	inline bool get__norvairZepearni_8() const { return ____norvairZepearni_8; }
	inline bool* get_address_of__norvairZepearni_8() { return &____norvairZepearni_8; }
	inline void set__norvairZepearni_8(bool value)
	{
		____norvairZepearni_8 = value;
	}

	inline static int32_t get_offset_of__vateMowhayga_9() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____vateMowhayga_9)); }
	inline bool get__vateMowhayga_9() const { return ____vateMowhayga_9; }
	inline bool* get_address_of__vateMowhayga_9() { return &____vateMowhayga_9; }
	inline void set__vateMowhayga_9(bool value)
	{
		____vateMowhayga_9 = value;
	}

	inline static int32_t get_offset_of__marabo_10() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____marabo_10)); }
	inline String_t* get__marabo_10() const { return ____marabo_10; }
	inline String_t** get_address_of__marabo_10() { return &____marabo_10; }
	inline void set__marabo_10(String_t* value)
	{
		____marabo_10 = value;
		Il2CppCodeGenWriteBarrier(&____marabo_10, value);
	}

	inline static int32_t get_offset_of__soway_11() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____soway_11)); }
	inline bool get__soway_11() const { return ____soway_11; }
	inline bool* get_address_of__soway_11() { return &____soway_11; }
	inline void set__soway_11(bool value)
	{
		____soway_11 = value;
	}

	inline static int32_t get_offset_of__whordowMinemem_12() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____whordowMinemem_12)); }
	inline String_t* get__whordowMinemem_12() const { return ____whordowMinemem_12; }
	inline String_t** get_address_of__whordowMinemem_12() { return &____whordowMinemem_12; }
	inline void set__whordowMinemem_12(String_t* value)
	{
		____whordowMinemem_12 = value;
		Il2CppCodeGenWriteBarrier(&____whordowMinemem_12, value);
	}

	inline static int32_t get_offset_of__jirta_13() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____jirta_13)); }
	inline float get__jirta_13() const { return ____jirta_13; }
	inline float* get_address_of__jirta_13() { return &____jirta_13; }
	inline void set__jirta_13(float value)
	{
		____jirta_13 = value;
	}

	inline static int32_t get_offset_of__sesotem_14() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____sesotem_14)); }
	inline uint32_t get__sesotem_14() const { return ____sesotem_14; }
	inline uint32_t* get_address_of__sesotem_14() { return &____sesotem_14; }
	inline void set__sesotem_14(uint32_t value)
	{
		____sesotem_14 = value;
	}

	inline static int32_t get_offset_of__sosiJatowkal_15() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____sosiJatowkal_15)); }
	inline uint32_t get__sosiJatowkal_15() const { return ____sosiJatowkal_15; }
	inline uint32_t* get_address_of__sosiJatowkal_15() { return &____sosiJatowkal_15; }
	inline void set__sosiJatowkal_15(uint32_t value)
	{
		____sosiJatowkal_15 = value;
	}

	inline static int32_t get_offset_of__nastawCeedel_16() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____nastawCeedel_16)); }
	inline int32_t get__nastawCeedel_16() const { return ____nastawCeedel_16; }
	inline int32_t* get_address_of__nastawCeedel_16() { return &____nastawCeedel_16; }
	inline void set__nastawCeedel_16(int32_t value)
	{
		____nastawCeedel_16 = value;
	}

	inline static int32_t get_offset_of__caslallla_17() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____caslallla_17)); }
	inline String_t* get__caslallla_17() const { return ____caslallla_17; }
	inline String_t** get_address_of__caslallla_17() { return &____caslallla_17; }
	inline void set__caslallla_17(String_t* value)
	{
		____caslallla_17 = value;
		Il2CppCodeGenWriteBarrier(&____caslallla_17, value);
	}

	inline static int32_t get_offset_of__purcousorCaykea_18() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____purcousorCaykea_18)); }
	inline float get__purcousorCaykea_18() const { return ____purcousorCaykea_18; }
	inline float* get_address_of__purcousorCaykea_18() { return &____purcousorCaykea_18; }
	inline void set__purcousorCaykea_18(float value)
	{
		____purcousorCaykea_18 = value;
	}

	inline static int32_t get_offset_of__ruciMiswir_19() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____ruciMiswir_19)); }
	inline float get__ruciMiswir_19() const { return ____ruciMiswir_19; }
	inline float* get_address_of__ruciMiswir_19() { return &____ruciMiswir_19; }
	inline void set__ruciMiswir_19(float value)
	{
		____ruciMiswir_19 = value;
	}

	inline static int32_t get_offset_of__bihaw_20() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____bihaw_20)); }
	inline String_t* get__bihaw_20() const { return ____bihaw_20; }
	inline String_t** get_address_of__bihaw_20() { return &____bihaw_20; }
	inline void set__bihaw_20(String_t* value)
	{
		____bihaw_20 = value;
		Il2CppCodeGenWriteBarrier(&____bihaw_20, value);
	}

	inline static int32_t get_offset_of__ceagiHertou_21() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____ceagiHertou_21)); }
	inline uint32_t get__ceagiHertou_21() const { return ____ceagiHertou_21; }
	inline uint32_t* get_address_of__ceagiHertou_21() { return &____ceagiHertou_21; }
	inline void set__ceagiHertou_21(uint32_t value)
	{
		____ceagiHertou_21 = value;
	}

	inline static int32_t get_offset_of__parwosowCofisma_22() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____parwosowCofisma_22)); }
	inline bool get__parwosowCofisma_22() const { return ____parwosowCofisma_22; }
	inline bool* get_address_of__parwosowCofisma_22() { return &____parwosowCofisma_22; }
	inline void set__parwosowCofisma_22(bool value)
	{
		____parwosowCofisma_22 = value;
	}

	inline static int32_t get_offset_of__pawticiSowkusem_23() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____pawticiSowkusem_23)); }
	inline uint32_t get__pawticiSowkusem_23() const { return ____pawticiSowkusem_23; }
	inline uint32_t* get_address_of__pawticiSowkusem_23() { return &____pawticiSowkusem_23; }
	inline void set__pawticiSowkusem_23(uint32_t value)
	{
		____pawticiSowkusem_23 = value;
	}

	inline static int32_t get_offset_of__purwawbur_24() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____purwawbur_24)); }
	inline bool get__purwawbur_24() const { return ____purwawbur_24; }
	inline bool* get_address_of__purwawbur_24() { return &____purwawbur_24; }
	inline void set__purwawbur_24(bool value)
	{
		____purwawbur_24 = value;
	}

	inline static int32_t get_offset_of__zeesou_25() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____zeesou_25)); }
	inline String_t* get__zeesou_25() const { return ____zeesou_25; }
	inline String_t** get_address_of__zeesou_25() { return &____zeesou_25; }
	inline void set__zeesou_25(String_t* value)
	{
		____zeesou_25 = value;
		Il2CppCodeGenWriteBarrier(&____zeesou_25, value);
	}

	inline static int32_t get_offset_of__kefirTairmisrel_26() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____kefirTairmisrel_26)); }
	inline float get__kefirTairmisrel_26() const { return ____kefirTairmisrel_26; }
	inline float* get_address_of__kefirTairmisrel_26() { return &____kefirTairmisrel_26; }
	inline void set__kefirTairmisrel_26(float value)
	{
		____kefirTairmisrel_26 = value;
	}

	inline static int32_t get_offset_of__xekallstaw_27() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____xekallstaw_27)); }
	inline bool get__xekallstaw_27() const { return ____xekallstaw_27; }
	inline bool* get_address_of__xekallstaw_27() { return &____xekallstaw_27; }
	inline void set__xekallstaw_27(bool value)
	{
		____xekallstaw_27 = value;
	}

	inline static int32_t get_offset_of__whornimoJini_28() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____whornimoJini_28)); }
	inline uint32_t get__whornimoJini_28() const { return ____whornimoJini_28; }
	inline uint32_t* get_address_of__whornimoJini_28() { return &____whornimoJini_28; }
	inline void set__whornimoJini_28(uint32_t value)
	{
		____whornimoJini_28 = value;
	}

	inline static int32_t get_offset_of__lehisSoulelgi_29() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____lehisSoulelgi_29)); }
	inline uint32_t get__lehisSoulelgi_29() const { return ____lehisSoulelgi_29; }
	inline uint32_t* get_address_of__lehisSoulelgi_29() { return &____lehisSoulelgi_29; }
	inline void set__lehisSoulelgi_29(uint32_t value)
	{
		____lehisSoulelgi_29 = value;
	}

	inline static int32_t get_offset_of__rerrearJoujo_30() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____rerrearJoujo_30)); }
	inline uint32_t get__rerrearJoujo_30() const { return ____rerrearJoujo_30; }
	inline uint32_t* get_address_of__rerrearJoujo_30() { return &____rerrearJoujo_30; }
	inline void set__rerrearJoujo_30(uint32_t value)
	{
		____rerrearJoujo_30 = value;
	}

	inline static int32_t get_offset_of__ruse_31() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____ruse_31)); }
	inline int32_t get__ruse_31() const { return ____ruse_31; }
	inline int32_t* get_address_of__ruse_31() { return &____ruse_31; }
	inline void set__ruse_31(int32_t value)
	{
		____ruse_31 = value;
	}

	inline static int32_t get_offset_of__qorxo_32() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____qorxo_32)); }
	inline String_t* get__qorxo_32() const { return ____qorxo_32; }
	inline String_t** get_address_of__qorxo_32() { return &____qorxo_32; }
	inline void set__qorxo_32(String_t* value)
	{
		____qorxo_32 = value;
		Il2CppCodeGenWriteBarrier(&____qorxo_32, value);
	}

	inline static int32_t get_offset_of__masallce_33() { return static_cast<int32_t>(offsetof(M_rocuwear201_t3787691205, ____masallce_33)); }
	inline uint32_t get__masallce_33() const { return ____masallce_33; }
	inline uint32_t* get_address_of__masallce_33() { return &____masallce_33; }
	inline void set__masallce_33(uint32_t value)
	{
		____masallce_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
