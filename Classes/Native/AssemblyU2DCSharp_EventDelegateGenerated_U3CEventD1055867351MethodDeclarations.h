﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EventDelegateGenerated/<EventDelegate_Remove_GetDelegate_member12_arg1>c__AnonStorey5F
struct U3CEventDelegate_Remove_GetDelegate_member12_arg1U3Ec__AnonStorey5F_t1055867351;

#include "codegen/il2cpp-codegen.h"

// System.Void EventDelegateGenerated/<EventDelegate_Remove_GetDelegate_member12_arg1>c__AnonStorey5F::.ctor()
extern "C"  void U3CEventDelegate_Remove_GetDelegate_member12_arg1U3Ec__AnonStorey5F__ctor_m1927763108 (U3CEventDelegate_Remove_GetDelegate_member12_arg1U3Ec__AnonStorey5F_t1055867351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventDelegateGenerated/<EventDelegate_Remove_GetDelegate_member12_arg1>c__AnonStorey5F::<>m__4A()
extern "C"  void U3CEventDelegate_Remove_GetDelegate_member12_arg1U3Ec__AnonStorey5F_U3CU3Em__4A_m3757587898 (U3CEventDelegate_Remove_GetDelegate_member12_arg1U3Ec__AnonStorey5F_t1055867351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
