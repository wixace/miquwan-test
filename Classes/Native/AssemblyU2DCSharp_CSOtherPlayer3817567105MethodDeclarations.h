﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSOtherPlayer
struct CSOtherPlayer_t3817567105;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// ReplayOtherPlayer
struct ReplayOtherPlayer_t1774842762;
// CSHeroUnit
struct CSHeroUnit_t3764358446;
// System.Collections.Generic.List`1<CSHeroUnit>
struct List_1_t837576702;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "AssemblyU2DCSharp_CSHeroUnit3764358446.h"

// System.Void CSOtherPlayer::.ctor()
extern "C"  void CSOtherPlayer__ctor_m3865202442 (CSOtherPlayer_t3817567105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSOtherPlayer::.ctor(Newtonsoft.Json.Linq.JToken)
extern "C"  void CSOtherPlayer__ctor_m4005796064 (CSOtherPlayer_t3817567105 * __this, JToken_t3412245951 * ___jToken0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReplayOtherPlayer CSOtherPlayer::GetRECOtherPlayer()
extern "C"  ReplayOtherPlayer_t1774842762 * CSOtherPlayer_GetRECOtherPlayer_m3549282056 (CSOtherPlayer_t3817567105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSOtherPlayer::GetCSPVPRobotNPCLevelById(System.Int32)
extern "C"  int32_t CSOtherPlayer_GetCSPVPRobotNPCLevelById_m1129579714 (CSOtherPlayer_t3817567105 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSHeroUnit CSOtherPlayer::GetHeroInfo(System.UInt32)
extern "C"  CSHeroUnit_t3764358446 * CSOtherPlayer_GetHeroInfo_m1340561125 (CSOtherPlayer_t3817567105 * __this, uint32_t ___heroID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSHeroUnit> CSOtherPlayer::GetHeroInfos()
extern "C"  List_1_t837576702 * CSOtherPlayer_GetHeroInfos_m467657230 (CSOtherPlayer_t3817567105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSOtherPlayer::ilo_get_id1(CSHeroUnit)
extern "C"  uint32_t CSOtherPlayer_ilo_get_id1_m495771183 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
