﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplePlayerModel
struct  SimplePlayerModel_t3948357526 
{
public:
	// System.Int32 SimplePlayerModel::Ticket
	int32_t ___Ticket_0;
	// System.Int32 SimplePlayerModel::Money
	int32_t ___Money_1;
	// System.Int32 SimplePlayerModel::Icon
	int32_t ___Icon_2;

public:
	inline static int32_t get_offset_of_Ticket_0() { return static_cast<int32_t>(offsetof(SimplePlayerModel_t3948357526, ___Ticket_0)); }
	inline int32_t get_Ticket_0() const { return ___Ticket_0; }
	inline int32_t* get_address_of_Ticket_0() { return &___Ticket_0; }
	inline void set_Ticket_0(int32_t value)
	{
		___Ticket_0 = value;
	}

	inline static int32_t get_offset_of_Money_1() { return static_cast<int32_t>(offsetof(SimplePlayerModel_t3948357526, ___Money_1)); }
	inline int32_t get_Money_1() const { return ___Money_1; }
	inline int32_t* get_address_of_Money_1() { return &___Money_1; }
	inline void set_Money_1(int32_t value)
	{
		___Money_1 = value;
	}

	inline static int32_t get_offset_of_Icon_2() { return static_cast<int32_t>(offsetof(SimplePlayerModel_t3948357526, ___Icon_2)); }
	inline int32_t get_Icon_2() const { return ___Icon_2; }
	inline int32_t* get_address_of_Icon_2() { return &___Icon_2; }
	inline void set_Icon_2(int32_t value)
	{
		___Icon_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: SimplePlayerModel
struct SimplePlayerModel_t3948357526_marshaled_pinvoke
{
	int32_t ___Ticket_0;
	int32_t ___Money_1;
	int32_t ___Icon_2;
};
// Native definition for marshalling of: SimplePlayerModel
struct SimplePlayerModel_t3948357526_marshaled_com
{
	int32_t ___Ticket_0;
	int32_t ___Money_1;
	int32_t ___Icon_2;
};
