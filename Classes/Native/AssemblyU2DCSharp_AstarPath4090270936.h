﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Pathfinding.AstarData
struct AstarData_t3283402719;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.AstarColor
struct AstarColor_t3758560998;
// System.String[]
struct StringU5BU5D_t4054002952;
// Pathfinding.Path
struct Path_t1974241691;
// OnVoidDelegate
struct OnVoidDelegate_t2787120856;
// OnGraphDelegate
struct OnGraphDelegate_t381382964;
// OnPathDelegate
struct OnPathDelegate_t598607977;
// OnScanDelegate
struct OnScanDelegate_t3165885121;
// System.Collections.Generic.Queue`1<Pathfinding.GraphUpdateObject>
struct Queue_1_t2667086133;
// System.Collections.Generic.Stack`1<Pathfinding.GraphNode>
struct Stack_1_t3122173294;
// Pathfinding.ThreadControlQueue
struct ThreadControlQueue_t1865010388;
// System.Threading.Thread[]
struct ThreadU5BU5D_t3166308279;
// System.Threading.Thread
struct Thread_t1973216770;
// Pathfinding.PathThreadInfo[]
struct PathThreadInfoU5BU5D_t3711851138;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// Pathfinding.Util.LockFreeStack
struct LockFreeStack_t746776019;
// Pathfinding.EuclideanEmbedding
struct EuclideanEmbedding_t908139023;
// System.Collections.Generic.Stack`1<System.Int32>
struct Stack_1_t4252399424;
// System.Collections.Generic.Queue`1<AstarPath/GUOSingle>
struct Queue_1_t1598615119;
// System.Object
struct Il2CppObject;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t874642578;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t924017833;
// System.Collections.Generic.Queue`1<AstarPath/AstarWorkItem>
struct Queue_1_t507969021;
// Pathfinding.GraphNodeDelegateCancelable
struct GraphNodeDelegateCancelable_t3591372971;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_AstarPath_AstarDistribution904179392.h"
#include "AssemblyU2DCSharp_GraphDebugMode2460853512.h"
#include "AssemblyU2DCSharp_PathLog873181375.h"
#include "AssemblyU2DCSharp_Heuristic2765431530.h"
#include "AssemblyU2DCSharp_ThreadCount3572904197.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AstarPath
struct  AstarPath_t4090270936  : public MonoBehaviour_t667441552
{
public:
	// Pathfinding.AstarData AstarPath::astarData
	AstarData_t3283402719 * ___astarData_5;
	// System.Boolean AstarPath::showNavGraphs
	bool ___showNavGraphs_7;
	// System.Boolean AstarPath::showUnwalkableNodes
	bool ___showUnwalkableNodes_8;
	// GraphDebugMode AstarPath::debugMode
	int32_t ___debugMode_9;
	// System.Single AstarPath::debugFloor
	float ___debugFloor_10;
	// System.Single AstarPath::debugRoof
	float ___debugRoof_11;
	// System.Boolean AstarPath::manualDebugFloorRoof
	bool ___manualDebugFloorRoof_12;
	// System.Boolean AstarPath::showSearchTree
	bool ___showSearchTree_13;
	// System.Single AstarPath::unwalkableNodeDebugSize
	float ___unwalkableNodeDebugSize_14;
	// PathLog AstarPath::logPathResults
	int32_t ___logPathResults_15;
	// System.Single AstarPath::maxNearestNodeDistance
	float ___maxNearestNodeDistance_16;
	// System.Boolean AstarPath::scanOnStartup
	bool ___scanOnStartup_17;
	// System.Boolean AstarPath::fullGetNearestSearch
	bool ___fullGetNearestSearch_18;
	// System.Boolean AstarPath::prioritizeGraphs
	bool ___prioritizeGraphs_19;
	// System.Single AstarPath::prioritizeGraphsLimit
	float ___prioritizeGraphsLimit_20;
	// Pathfinding.AstarColor AstarPath::colorSettings
	AstarColor_t3758560998 * ___colorSettings_21;
	// System.String[] AstarPath::tagNames
	StringU5BU5D_t4054002952* ___tagNames_22;
	// Heuristic AstarPath::heuristic
	int32_t ___heuristic_23;
	// System.Single AstarPath::heuristicScale
	float ___heuristicScale_24;
	// ThreadCount AstarPath::threadCount
	int32_t ___threadCount_25;
	// System.Single AstarPath::maxFrameTime
	float ___maxFrameTime_26;
	// System.Int32 AstarPath::minAreaSize
	int32_t ___minAreaSize_27;
	// System.Boolean AstarPath::limitGraphUpdates
	bool ___limitGraphUpdates_28;
	// System.Single AstarPath::maxGraphUpdateFreq
	float ___maxGraphUpdateFreq_29;
	// System.Single AstarPath::lastScanTime
	float ___lastScanTime_31;
	// Pathfinding.Path AstarPath::debugPath
	Path_t1974241691 * ___debugPath_32;
	// System.String AstarPath::inGameDebugPath
	String_t* ___inGameDebugPath_33;
	// System.Boolean AstarPath::isScanning
	bool ___isScanning_34;
	// System.Boolean AstarPath::graphUpdateRoutineRunning
	bool ___graphUpdateRoutineRunning_35;
	// System.Boolean AstarPath::isRegisteredForUpdate
	bool ___isRegisteredForUpdate_36;
	// System.Boolean AstarPath::workItemsQueued
	bool ___workItemsQueued_37;
	// System.Boolean AstarPath::queuedWorkItemFloodFill
	bool ___queuedWorkItemFloodFill_38;
	// OnVoidDelegate AstarPath::OnDrawGizmosCallback
	OnVoidDelegate_t2787120856 * ___OnDrawGizmosCallback_50;
	// OnVoidDelegate AstarPath::OnGraphsWillBeUpdated
	OnVoidDelegate_t2787120856 * ___OnGraphsWillBeUpdated_51;
	// OnVoidDelegate AstarPath::OnGraphsWillBeUpdated2
	OnVoidDelegate_t2787120856 * ___OnGraphsWillBeUpdated2_52;
	// System.Collections.Generic.Queue`1<Pathfinding.GraphUpdateObject> AstarPath::graphUpdateQueue
	Queue_1_t2667086133 * ___graphUpdateQueue_53;
	// System.Collections.Generic.Stack`1<Pathfinding.GraphNode> AstarPath::floodStack
	Stack_1_t3122173294 * ___floodStack_54;
	// Pathfinding.ThreadControlQueue AstarPath::pathQueue
	ThreadControlQueue_t1865010388 * ___pathQueue_55;
	// System.Threading.Thread AstarPath::graphUpdateThread
	Thread_t1973216770 * ___graphUpdateThread_57;
	// Pathfinding.EuclideanEmbedding AstarPath::euclideanEmbedding
	EuclideanEmbedding_t908139023 * ___euclideanEmbedding_61;
	// System.Int32 AstarPath::nextNodeIndex
	int32_t ___nextNodeIndex_62;
	// System.Collections.Generic.Stack`1<System.Int32> AstarPath::nodeIndexPool
	Stack_1_t4252399424 * ___nodeIndexPool_63;
	// Pathfinding.Path AstarPath::pathReturnPop
	Path_t1974241691 * ___pathReturnPop_64;
	// System.Collections.Generic.Queue`1<AstarPath/GUOSingle> AstarPath::graphUpdateQueueAsync
	Queue_1_t1598615119 * ___graphUpdateQueueAsync_65;
	// System.Collections.Generic.Queue`1<AstarPath/GUOSingle> AstarPath::graphUpdateQueueRegular
	Queue_1_t1598615119 * ___graphUpdateQueueRegular_66;
	// System.Boolean AstarPath::showGraphs
	bool ___showGraphs_67;
	// System.UInt32 AstarPath::lastUniqueAreaIndex
	uint32_t ___lastUniqueAreaIndex_69;
	// System.Threading.AutoResetEvent AstarPath::graphUpdateAsyncEvent
	AutoResetEvent_t874642578 * ___graphUpdateAsyncEvent_71;
	// System.Threading.ManualResetEvent AstarPath::processingGraphUpdatesAsync
	ManualResetEvent_t924017833 * ___processingGraphUpdatesAsync_72;
	// System.Single AstarPath::lastGraphUpdate
	float ___lastGraphUpdate_73;
	// System.UInt16 AstarPath::nextFreePathID
	uint16_t ___nextFreePathID_74;
	// System.Collections.Generic.Queue`1<AstarPath/AstarWorkItem> AstarPath::workItems
	Queue_1_t507969021 * ___workItems_75;
	// System.Boolean AstarPath::processingWorkItems
	bool ___processingWorkItems_76;

public:
	inline static int32_t get_offset_of_astarData_5() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___astarData_5)); }
	inline AstarData_t3283402719 * get_astarData_5() const { return ___astarData_5; }
	inline AstarData_t3283402719 ** get_address_of_astarData_5() { return &___astarData_5; }
	inline void set_astarData_5(AstarData_t3283402719 * value)
	{
		___astarData_5 = value;
		Il2CppCodeGenWriteBarrier(&___astarData_5, value);
	}

	inline static int32_t get_offset_of_showNavGraphs_7() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___showNavGraphs_7)); }
	inline bool get_showNavGraphs_7() const { return ___showNavGraphs_7; }
	inline bool* get_address_of_showNavGraphs_7() { return &___showNavGraphs_7; }
	inline void set_showNavGraphs_7(bool value)
	{
		___showNavGraphs_7 = value;
	}

	inline static int32_t get_offset_of_showUnwalkableNodes_8() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___showUnwalkableNodes_8)); }
	inline bool get_showUnwalkableNodes_8() const { return ___showUnwalkableNodes_8; }
	inline bool* get_address_of_showUnwalkableNodes_8() { return &___showUnwalkableNodes_8; }
	inline void set_showUnwalkableNodes_8(bool value)
	{
		___showUnwalkableNodes_8 = value;
	}

	inline static int32_t get_offset_of_debugMode_9() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___debugMode_9)); }
	inline int32_t get_debugMode_9() const { return ___debugMode_9; }
	inline int32_t* get_address_of_debugMode_9() { return &___debugMode_9; }
	inline void set_debugMode_9(int32_t value)
	{
		___debugMode_9 = value;
	}

	inline static int32_t get_offset_of_debugFloor_10() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___debugFloor_10)); }
	inline float get_debugFloor_10() const { return ___debugFloor_10; }
	inline float* get_address_of_debugFloor_10() { return &___debugFloor_10; }
	inline void set_debugFloor_10(float value)
	{
		___debugFloor_10 = value;
	}

	inline static int32_t get_offset_of_debugRoof_11() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___debugRoof_11)); }
	inline float get_debugRoof_11() const { return ___debugRoof_11; }
	inline float* get_address_of_debugRoof_11() { return &___debugRoof_11; }
	inline void set_debugRoof_11(float value)
	{
		___debugRoof_11 = value;
	}

	inline static int32_t get_offset_of_manualDebugFloorRoof_12() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___manualDebugFloorRoof_12)); }
	inline bool get_manualDebugFloorRoof_12() const { return ___manualDebugFloorRoof_12; }
	inline bool* get_address_of_manualDebugFloorRoof_12() { return &___manualDebugFloorRoof_12; }
	inline void set_manualDebugFloorRoof_12(bool value)
	{
		___manualDebugFloorRoof_12 = value;
	}

	inline static int32_t get_offset_of_showSearchTree_13() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___showSearchTree_13)); }
	inline bool get_showSearchTree_13() const { return ___showSearchTree_13; }
	inline bool* get_address_of_showSearchTree_13() { return &___showSearchTree_13; }
	inline void set_showSearchTree_13(bool value)
	{
		___showSearchTree_13 = value;
	}

	inline static int32_t get_offset_of_unwalkableNodeDebugSize_14() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___unwalkableNodeDebugSize_14)); }
	inline float get_unwalkableNodeDebugSize_14() const { return ___unwalkableNodeDebugSize_14; }
	inline float* get_address_of_unwalkableNodeDebugSize_14() { return &___unwalkableNodeDebugSize_14; }
	inline void set_unwalkableNodeDebugSize_14(float value)
	{
		___unwalkableNodeDebugSize_14 = value;
	}

	inline static int32_t get_offset_of_logPathResults_15() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___logPathResults_15)); }
	inline int32_t get_logPathResults_15() const { return ___logPathResults_15; }
	inline int32_t* get_address_of_logPathResults_15() { return &___logPathResults_15; }
	inline void set_logPathResults_15(int32_t value)
	{
		___logPathResults_15 = value;
	}

	inline static int32_t get_offset_of_maxNearestNodeDistance_16() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___maxNearestNodeDistance_16)); }
	inline float get_maxNearestNodeDistance_16() const { return ___maxNearestNodeDistance_16; }
	inline float* get_address_of_maxNearestNodeDistance_16() { return &___maxNearestNodeDistance_16; }
	inline void set_maxNearestNodeDistance_16(float value)
	{
		___maxNearestNodeDistance_16 = value;
	}

	inline static int32_t get_offset_of_scanOnStartup_17() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___scanOnStartup_17)); }
	inline bool get_scanOnStartup_17() const { return ___scanOnStartup_17; }
	inline bool* get_address_of_scanOnStartup_17() { return &___scanOnStartup_17; }
	inline void set_scanOnStartup_17(bool value)
	{
		___scanOnStartup_17 = value;
	}

	inline static int32_t get_offset_of_fullGetNearestSearch_18() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___fullGetNearestSearch_18)); }
	inline bool get_fullGetNearestSearch_18() const { return ___fullGetNearestSearch_18; }
	inline bool* get_address_of_fullGetNearestSearch_18() { return &___fullGetNearestSearch_18; }
	inline void set_fullGetNearestSearch_18(bool value)
	{
		___fullGetNearestSearch_18 = value;
	}

	inline static int32_t get_offset_of_prioritizeGraphs_19() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___prioritizeGraphs_19)); }
	inline bool get_prioritizeGraphs_19() const { return ___prioritizeGraphs_19; }
	inline bool* get_address_of_prioritizeGraphs_19() { return &___prioritizeGraphs_19; }
	inline void set_prioritizeGraphs_19(bool value)
	{
		___prioritizeGraphs_19 = value;
	}

	inline static int32_t get_offset_of_prioritizeGraphsLimit_20() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___prioritizeGraphsLimit_20)); }
	inline float get_prioritizeGraphsLimit_20() const { return ___prioritizeGraphsLimit_20; }
	inline float* get_address_of_prioritizeGraphsLimit_20() { return &___prioritizeGraphsLimit_20; }
	inline void set_prioritizeGraphsLimit_20(float value)
	{
		___prioritizeGraphsLimit_20 = value;
	}

	inline static int32_t get_offset_of_colorSettings_21() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___colorSettings_21)); }
	inline AstarColor_t3758560998 * get_colorSettings_21() const { return ___colorSettings_21; }
	inline AstarColor_t3758560998 ** get_address_of_colorSettings_21() { return &___colorSettings_21; }
	inline void set_colorSettings_21(AstarColor_t3758560998 * value)
	{
		___colorSettings_21 = value;
		Il2CppCodeGenWriteBarrier(&___colorSettings_21, value);
	}

	inline static int32_t get_offset_of_tagNames_22() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___tagNames_22)); }
	inline StringU5BU5D_t4054002952* get_tagNames_22() const { return ___tagNames_22; }
	inline StringU5BU5D_t4054002952** get_address_of_tagNames_22() { return &___tagNames_22; }
	inline void set_tagNames_22(StringU5BU5D_t4054002952* value)
	{
		___tagNames_22 = value;
		Il2CppCodeGenWriteBarrier(&___tagNames_22, value);
	}

	inline static int32_t get_offset_of_heuristic_23() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___heuristic_23)); }
	inline int32_t get_heuristic_23() const { return ___heuristic_23; }
	inline int32_t* get_address_of_heuristic_23() { return &___heuristic_23; }
	inline void set_heuristic_23(int32_t value)
	{
		___heuristic_23 = value;
	}

	inline static int32_t get_offset_of_heuristicScale_24() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___heuristicScale_24)); }
	inline float get_heuristicScale_24() const { return ___heuristicScale_24; }
	inline float* get_address_of_heuristicScale_24() { return &___heuristicScale_24; }
	inline void set_heuristicScale_24(float value)
	{
		___heuristicScale_24 = value;
	}

	inline static int32_t get_offset_of_threadCount_25() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___threadCount_25)); }
	inline int32_t get_threadCount_25() const { return ___threadCount_25; }
	inline int32_t* get_address_of_threadCount_25() { return &___threadCount_25; }
	inline void set_threadCount_25(int32_t value)
	{
		___threadCount_25 = value;
	}

	inline static int32_t get_offset_of_maxFrameTime_26() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___maxFrameTime_26)); }
	inline float get_maxFrameTime_26() const { return ___maxFrameTime_26; }
	inline float* get_address_of_maxFrameTime_26() { return &___maxFrameTime_26; }
	inline void set_maxFrameTime_26(float value)
	{
		___maxFrameTime_26 = value;
	}

	inline static int32_t get_offset_of_minAreaSize_27() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___minAreaSize_27)); }
	inline int32_t get_minAreaSize_27() const { return ___minAreaSize_27; }
	inline int32_t* get_address_of_minAreaSize_27() { return &___minAreaSize_27; }
	inline void set_minAreaSize_27(int32_t value)
	{
		___minAreaSize_27 = value;
	}

	inline static int32_t get_offset_of_limitGraphUpdates_28() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___limitGraphUpdates_28)); }
	inline bool get_limitGraphUpdates_28() const { return ___limitGraphUpdates_28; }
	inline bool* get_address_of_limitGraphUpdates_28() { return &___limitGraphUpdates_28; }
	inline void set_limitGraphUpdates_28(bool value)
	{
		___limitGraphUpdates_28 = value;
	}

	inline static int32_t get_offset_of_maxGraphUpdateFreq_29() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___maxGraphUpdateFreq_29)); }
	inline float get_maxGraphUpdateFreq_29() const { return ___maxGraphUpdateFreq_29; }
	inline float* get_address_of_maxGraphUpdateFreq_29() { return &___maxGraphUpdateFreq_29; }
	inline void set_maxGraphUpdateFreq_29(float value)
	{
		___maxGraphUpdateFreq_29 = value;
	}

	inline static int32_t get_offset_of_lastScanTime_31() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___lastScanTime_31)); }
	inline float get_lastScanTime_31() const { return ___lastScanTime_31; }
	inline float* get_address_of_lastScanTime_31() { return &___lastScanTime_31; }
	inline void set_lastScanTime_31(float value)
	{
		___lastScanTime_31 = value;
	}

	inline static int32_t get_offset_of_debugPath_32() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___debugPath_32)); }
	inline Path_t1974241691 * get_debugPath_32() const { return ___debugPath_32; }
	inline Path_t1974241691 ** get_address_of_debugPath_32() { return &___debugPath_32; }
	inline void set_debugPath_32(Path_t1974241691 * value)
	{
		___debugPath_32 = value;
		Il2CppCodeGenWriteBarrier(&___debugPath_32, value);
	}

	inline static int32_t get_offset_of_inGameDebugPath_33() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___inGameDebugPath_33)); }
	inline String_t* get_inGameDebugPath_33() const { return ___inGameDebugPath_33; }
	inline String_t** get_address_of_inGameDebugPath_33() { return &___inGameDebugPath_33; }
	inline void set_inGameDebugPath_33(String_t* value)
	{
		___inGameDebugPath_33 = value;
		Il2CppCodeGenWriteBarrier(&___inGameDebugPath_33, value);
	}

	inline static int32_t get_offset_of_isScanning_34() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___isScanning_34)); }
	inline bool get_isScanning_34() const { return ___isScanning_34; }
	inline bool* get_address_of_isScanning_34() { return &___isScanning_34; }
	inline void set_isScanning_34(bool value)
	{
		___isScanning_34 = value;
	}

	inline static int32_t get_offset_of_graphUpdateRoutineRunning_35() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___graphUpdateRoutineRunning_35)); }
	inline bool get_graphUpdateRoutineRunning_35() const { return ___graphUpdateRoutineRunning_35; }
	inline bool* get_address_of_graphUpdateRoutineRunning_35() { return &___graphUpdateRoutineRunning_35; }
	inline void set_graphUpdateRoutineRunning_35(bool value)
	{
		___graphUpdateRoutineRunning_35 = value;
	}

	inline static int32_t get_offset_of_isRegisteredForUpdate_36() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___isRegisteredForUpdate_36)); }
	inline bool get_isRegisteredForUpdate_36() const { return ___isRegisteredForUpdate_36; }
	inline bool* get_address_of_isRegisteredForUpdate_36() { return &___isRegisteredForUpdate_36; }
	inline void set_isRegisteredForUpdate_36(bool value)
	{
		___isRegisteredForUpdate_36 = value;
	}

	inline static int32_t get_offset_of_workItemsQueued_37() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___workItemsQueued_37)); }
	inline bool get_workItemsQueued_37() const { return ___workItemsQueued_37; }
	inline bool* get_address_of_workItemsQueued_37() { return &___workItemsQueued_37; }
	inline void set_workItemsQueued_37(bool value)
	{
		___workItemsQueued_37 = value;
	}

	inline static int32_t get_offset_of_queuedWorkItemFloodFill_38() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___queuedWorkItemFloodFill_38)); }
	inline bool get_queuedWorkItemFloodFill_38() const { return ___queuedWorkItemFloodFill_38; }
	inline bool* get_address_of_queuedWorkItemFloodFill_38() { return &___queuedWorkItemFloodFill_38; }
	inline void set_queuedWorkItemFloodFill_38(bool value)
	{
		___queuedWorkItemFloodFill_38 = value;
	}

	inline static int32_t get_offset_of_OnDrawGizmosCallback_50() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___OnDrawGizmosCallback_50)); }
	inline OnVoidDelegate_t2787120856 * get_OnDrawGizmosCallback_50() const { return ___OnDrawGizmosCallback_50; }
	inline OnVoidDelegate_t2787120856 ** get_address_of_OnDrawGizmosCallback_50() { return &___OnDrawGizmosCallback_50; }
	inline void set_OnDrawGizmosCallback_50(OnVoidDelegate_t2787120856 * value)
	{
		___OnDrawGizmosCallback_50 = value;
		Il2CppCodeGenWriteBarrier(&___OnDrawGizmosCallback_50, value);
	}

	inline static int32_t get_offset_of_OnGraphsWillBeUpdated_51() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___OnGraphsWillBeUpdated_51)); }
	inline OnVoidDelegate_t2787120856 * get_OnGraphsWillBeUpdated_51() const { return ___OnGraphsWillBeUpdated_51; }
	inline OnVoidDelegate_t2787120856 ** get_address_of_OnGraphsWillBeUpdated_51() { return &___OnGraphsWillBeUpdated_51; }
	inline void set_OnGraphsWillBeUpdated_51(OnVoidDelegate_t2787120856 * value)
	{
		___OnGraphsWillBeUpdated_51 = value;
		Il2CppCodeGenWriteBarrier(&___OnGraphsWillBeUpdated_51, value);
	}

	inline static int32_t get_offset_of_OnGraphsWillBeUpdated2_52() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___OnGraphsWillBeUpdated2_52)); }
	inline OnVoidDelegate_t2787120856 * get_OnGraphsWillBeUpdated2_52() const { return ___OnGraphsWillBeUpdated2_52; }
	inline OnVoidDelegate_t2787120856 ** get_address_of_OnGraphsWillBeUpdated2_52() { return &___OnGraphsWillBeUpdated2_52; }
	inline void set_OnGraphsWillBeUpdated2_52(OnVoidDelegate_t2787120856 * value)
	{
		___OnGraphsWillBeUpdated2_52 = value;
		Il2CppCodeGenWriteBarrier(&___OnGraphsWillBeUpdated2_52, value);
	}

	inline static int32_t get_offset_of_graphUpdateQueue_53() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___graphUpdateQueue_53)); }
	inline Queue_1_t2667086133 * get_graphUpdateQueue_53() const { return ___graphUpdateQueue_53; }
	inline Queue_1_t2667086133 ** get_address_of_graphUpdateQueue_53() { return &___graphUpdateQueue_53; }
	inline void set_graphUpdateQueue_53(Queue_1_t2667086133 * value)
	{
		___graphUpdateQueue_53 = value;
		Il2CppCodeGenWriteBarrier(&___graphUpdateQueue_53, value);
	}

	inline static int32_t get_offset_of_floodStack_54() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___floodStack_54)); }
	inline Stack_1_t3122173294 * get_floodStack_54() const { return ___floodStack_54; }
	inline Stack_1_t3122173294 ** get_address_of_floodStack_54() { return &___floodStack_54; }
	inline void set_floodStack_54(Stack_1_t3122173294 * value)
	{
		___floodStack_54 = value;
		Il2CppCodeGenWriteBarrier(&___floodStack_54, value);
	}

	inline static int32_t get_offset_of_pathQueue_55() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___pathQueue_55)); }
	inline ThreadControlQueue_t1865010388 * get_pathQueue_55() const { return ___pathQueue_55; }
	inline ThreadControlQueue_t1865010388 ** get_address_of_pathQueue_55() { return &___pathQueue_55; }
	inline void set_pathQueue_55(ThreadControlQueue_t1865010388 * value)
	{
		___pathQueue_55 = value;
		Il2CppCodeGenWriteBarrier(&___pathQueue_55, value);
	}

	inline static int32_t get_offset_of_graphUpdateThread_57() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___graphUpdateThread_57)); }
	inline Thread_t1973216770 * get_graphUpdateThread_57() const { return ___graphUpdateThread_57; }
	inline Thread_t1973216770 ** get_address_of_graphUpdateThread_57() { return &___graphUpdateThread_57; }
	inline void set_graphUpdateThread_57(Thread_t1973216770 * value)
	{
		___graphUpdateThread_57 = value;
		Il2CppCodeGenWriteBarrier(&___graphUpdateThread_57, value);
	}

	inline static int32_t get_offset_of_euclideanEmbedding_61() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___euclideanEmbedding_61)); }
	inline EuclideanEmbedding_t908139023 * get_euclideanEmbedding_61() const { return ___euclideanEmbedding_61; }
	inline EuclideanEmbedding_t908139023 ** get_address_of_euclideanEmbedding_61() { return &___euclideanEmbedding_61; }
	inline void set_euclideanEmbedding_61(EuclideanEmbedding_t908139023 * value)
	{
		___euclideanEmbedding_61 = value;
		Il2CppCodeGenWriteBarrier(&___euclideanEmbedding_61, value);
	}

	inline static int32_t get_offset_of_nextNodeIndex_62() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___nextNodeIndex_62)); }
	inline int32_t get_nextNodeIndex_62() const { return ___nextNodeIndex_62; }
	inline int32_t* get_address_of_nextNodeIndex_62() { return &___nextNodeIndex_62; }
	inline void set_nextNodeIndex_62(int32_t value)
	{
		___nextNodeIndex_62 = value;
	}

	inline static int32_t get_offset_of_nodeIndexPool_63() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___nodeIndexPool_63)); }
	inline Stack_1_t4252399424 * get_nodeIndexPool_63() const { return ___nodeIndexPool_63; }
	inline Stack_1_t4252399424 ** get_address_of_nodeIndexPool_63() { return &___nodeIndexPool_63; }
	inline void set_nodeIndexPool_63(Stack_1_t4252399424 * value)
	{
		___nodeIndexPool_63 = value;
		Il2CppCodeGenWriteBarrier(&___nodeIndexPool_63, value);
	}

	inline static int32_t get_offset_of_pathReturnPop_64() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___pathReturnPop_64)); }
	inline Path_t1974241691 * get_pathReturnPop_64() const { return ___pathReturnPop_64; }
	inline Path_t1974241691 ** get_address_of_pathReturnPop_64() { return &___pathReturnPop_64; }
	inline void set_pathReturnPop_64(Path_t1974241691 * value)
	{
		___pathReturnPop_64 = value;
		Il2CppCodeGenWriteBarrier(&___pathReturnPop_64, value);
	}

	inline static int32_t get_offset_of_graphUpdateQueueAsync_65() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___graphUpdateQueueAsync_65)); }
	inline Queue_1_t1598615119 * get_graphUpdateQueueAsync_65() const { return ___graphUpdateQueueAsync_65; }
	inline Queue_1_t1598615119 ** get_address_of_graphUpdateQueueAsync_65() { return &___graphUpdateQueueAsync_65; }
	inline void set_graphUpdateQueueAsync_65(Queue_1_t1598615119 * value)
	{
		___graphUpdateQueueAsync_65 = value;
		Il2CppCodeGenWriteBarrier(&___graphUpdateQueueAsync_65, value);
	}

	inline static int32_t get_offset_of_graphUpdateQueueRegular_66() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___graphUpdateQueueRegular_66)); }
	inline Queue_1_t1598615119 * get_graphUpdateQueueRegular_66() const { return ___graphUpdateQueueRegular_66; }
	inline Queue_1_t1598615119 ** get_address_of_graphUpdateQueueRegular_66() { return &___graphUpdateQueueRegular_66; }
	inline void set_graphUpdateQueueRegular_66(Queue_1_t1598615119 * value)
	{
		___graphUpdateQueueRegular_66 = value;
		Il2CppCodeGenWriteBarrier(&___graphUpdateQueueRegular_66, value);
	}

	inline static int32_t get_offset_of_showGraphs_67() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___showGraphs_67)); }
	inline bool get_showGraphs_67() const { return ___showGraphs_67; }
	inline bool* get_address_of_showGraphs_67() { return &___showGraphs_67; }
	inline void set_showGraphs_67(bool value)
	{
		___showGraphs_67 = value;
	}

	inline static int32_t get_offset_of_lastUniqueAreaIndex_69() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___lastUniqueAreaIndex_69)); }
	inline uint32_t get_lastUniqueAreaIndex_69() const { return ___lastUniqueAreaIndex_69; }
	inline uint32_t* get_address_of_lastUniqueAreaIndex_69() { return &___lastUniqueAreaIndex_69; }
	inline void set_lastUniqueAreaIndex_69(uint32_t value)
	{
		___lastUniqueAreaIndex_69 = value;
	}

	inline static int32_t get_offset_of_graphUpdateAsyncEvent_71() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___graphUpdateAsyncEvent_71)); }
	inline AutoResetEvent_t874642578 * get_graphUpdateAsyncEvent_71() const { return ___graphUpdateAsyncEvent_71; }
	inline AutoResetEvent_t874642578 ** get_address_of_graphUpdateAsyncEvent_71() { return &___graphUpdateAsyncEvent_71; }
	inline void set_graphUpdateAsyncEvent_71(AutoResetEvent_t874642578 * value)
	{
		___graphUpdateAsyncEvent_71 = value;
		Il2CppCodeGenWriteBarrier(&___graphUpdateAsyncEvent_71, value);
	}

	inline static int32_t get_offset_of_processingGraphUpdatesAsync_72() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___processingGraphUpdatesAsync_72)); }
	inline ManualResetEvent_t924017833 * get_processingGraphUpdatesAsync_72() const { return ___processingGraphUpdatesAsync_72; }
	inline ManualResetEvent_t924017833 ** get_address_of_processingGraphUpdatesAsync_72() { return &___processingGraphUpdatesAsync_72; }
	inline void set_processingGraphUpdatesAsync_72(ManualResetEvent_t924017833 * value)
	{
		___processingGraphUpdatesAsync_72 = value;
		Il2CppCodeGenWriteBarrier(&___processingGraphUpdatesAsync_72, value);
	}

	inline static int32_t get_offset_of_lastGraphUpdate_73() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___lastGraphUpdate_73)); }
	inline float get_lastGraphUpdate_73() const { return ___lastGraphUpdate_73; }
	inline float* get_address_of_lastGraphUpdate_73() { return &___lastGraphUpdate_73; }
	inline void set_lastGraphUpdate_73(float value)
	{
		___lastGraphUpdate_73 = value;
	}

	inline static int32_t get_offset_of_nextFreePathID_74() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___nextFreePathID_74)); }
	inline uint16_t get_nextFreePathID_74() const { return ___nextFreePathID_74; }
	inline uint16_t* get_address_of_nextFreePathID_74() { return &___nextFreePathID_74; }
	inline void set_nextFreePathID_74(uint16_t value)
	{
		___nextFreePathID_74 = value;
	}

	inline static int32_t get_offset_of_workItems_75() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___workItems_75)); }
	inline Queue_1_t507969021 * get_workItems_75() const { return ___workItems_75; }
	inline Queue_1_t507969021 ** get_address_of_workItems_75() { return &___workItems_75; }
	inline void set_workItems_75(Queue_1_t507969021 * value)
	{
		___workItems_75 = value;
		Il2CppCodeGenWriteBarrier(&___workItems_75, value);
	}

	inline static int32_t get_offset_of_processingWorkItems_76() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936, ___processingWorkItems_76)); }
	inline bool get_processingWorkItems_76() const { return ___processingWorkItems_76; }
	inline bool* get_address_of_processingWorkItems_76() { return &___processingWorkItems_76; }
	inline void set_processingWorkItems_76(bool value)
	{
		___processingWorkItems_76 = value;
	}
};

struct AstarPath_t4090270936_StaticFields
{
public:
	// AstarPath/AstarDistribution AstarPath::Distribution
	int32_t ___Distribution_2;
	// System.String AstarPath::Branch
	String_t* ___Branch_3;
	// System.Boolean AstarPath::HasPro
	bool ___HasPro_4;
	// AstarPath AstarPath::active
	AstarPath_t4090270936 * ___active_6;
	// System.Int32 AstarPath::PathsCompleted
	int32_t ___PathsCompleted_30;
	// OnVoidDelegate AstarPath::OnAwakeSettings
	OnVoidDelegate_t2787120856 * ___OnAwakeSettings_39;
	// OnGraphDelegate AstarPath::OnGraphPreScan
	OnGraphDelegate_t381382964 * ___OnGraphPreScan_40;
	// OnGraphDelegate AstarPath::OnGraphPostScan
	OnGraphDelegate_t381382964 * ___OnGraphPostScan_41;
	// OnPathDelegate AstarPath::OnPathPreSearch
	OnPathDelegate_t598607977 * ___OnPathPreSearch_42;
	// OnPathDelegate AstarPath::OnPathPostSearch
	OnPathDelegate_t598607977 * ___OnPathPostSearch_43;
	// OnScanDelegate AstarPath::OnPreScan
	OnScanDelegate_t3165885121 * ___OnPreScan_44;
	// OnScanDelegate AstarPath::OnPostScan
	OnScanDelegate_t3165885121 * ___OnPostScan_45;
	// OnScanDelegate AstarPath::OnLatePostScan
	OnScanDelegate_t3165885121 * ___OnLatePostScan_46;
	// OnScanDelegate AstarPath::OnGraphsUpdated
	OnScanDelegate_t3165885121 * ___OnGraphsUpdated_47;
	// OnVoidDelegate AstarPath::On65KOverflow
	OnVoidDelegate_t2787120856 * ___On65KOverflow_48;
	// OnVoidDelegate AstarPath::OnThreadSafeCallback
	OnVoidDelegate_t2787120856 * ___OnThreadSafeCallback_49;
	// System.Threading.Thread[] AstarPath::threads
	ThreadU5BU5D_t3166308279* ___threads_56;
	// Pathfinding.PathThreadInfo[] AstarPath::threadInfos
	PathThreadInfoU5BU5D_t3711851138* ___threadInfos_58;
	// System.Collections.IEnumerator AstarPath::threadEnumerator
	Il2CppObject * ___threadEnumerator_59;
	// Pathfinding.Util.LockFreeStack AstarPath::pathReturnStack
	LockFreeStack_t746776019 * ___pathReturnStack_60;
	// System.Boolean AstarPath::isEditor
	bool ___isEditor_68;
	// System.Object AstarPath::safeUpdateLock
	Il2CppObject * ___safeUpdateLock_70;
	// System.Int32 AstarPath::waitForPathDepth
	int32_t ___waitForPathDepth_77;
	// Pathfinding.GraphNodeDelegateCancelable AstarPath::<>f__am$cache4C
	GraphNodeDelegateCancelable_t3591372971 * ___U3CU3Ef__amU24cache4C_78;
	// Pathfinding.GraphNodeDelegateCancelable AstarPath::<>f__am$cache4D
	GraphNodeDelegateCancelable_t3591372971 * ___U3CU3Ef__amU24cache4D_79;

public:
	inline static int32_t get_offset_of_Distribution_2() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___Distribution_2)); }
	inline int32_t get_Distribution_2() const { return ___Distribution_2; }
	inline int32_t* get_address_of_Distribution_2() { return &___Distribution_2; }
	inline void set_Distribution_2(int32_t value)
	{
		___Distribution_2 = value;
	}

	inline static int32_t get_offset_of_Branch_3() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___Branch_3)); }
	inline String_t* get_Branch_3() const { return ___Branch_3; }
	inline String_t** get_address_of_Branch_3() { return &___Branch_3; }
	inline void set_Branch_3(String_t* value)
	{
		___Branch_3 = value;
		Il2CppCodeGenWriteBarrier(&___Branch_3, value);
	}

	inline static int32_t get_offset_of_HasPro_4() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___HasPro_4)); }
	inline bool get_HasPro_4() const { return ___HasPro_4; }
	inline bool* get_address_of_HasPro_4() { return &___HasPro_4; }
	inline void set_HasPro_4(bool value)
	{
		___HasPro_4 = value;
	}

	inline static int32_t get_offset_of_active_6() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___active_6)); }
	inline AstarPath_t4090270936 * get_active_6() const { return ___active_6; }
	inline AstarPath_t4090270936 ** get_address_of_active_6() { return &___active_6; }
	inline void set_active_6(AstarPath_t4090270936 * value)
	{
		___active_6 = value;
		Il2CppCodeGenWriteBarrier(&___active_6, value);
	}

	inline static int32_t get_offset_of_PathsCompleted_30() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___PathsCompleted_30)); }
	inline int32_t get_PathsCompleted_30() const { return ___PathsCompleted_30; }
	inline int32_t* get_address_of_PathsCompleted_30() { return &___PathsCompleted_30; }
	inline void set_PathsCompleted_30(int32_t value)
	{
		___PathsCompleted_30 = value;
	}

	inline static int32_t get_offset_of_OnAwakeSettings_39() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___OnAwakeSettings_39)); }
	inline OnVoidDelegate_t2787120856 * get_OnAwakeSettings_39() const { return ___OnAwakeSettings_39; }
	inline OnVoidDelegate_t2787120856 ** get_address_of_OnAwakeSettings_39() { return &___OnAwakeSettings_39; }
	inline void set_OnAwakeSettings_39(OnVoidDelegate_t2787120856 * value)
	{
		___OnAwakeSettings_39 = value;
		Il2CppCodeGenWriteBarrier(&___OnAwakeSettings_39, value);
	}

	inline static int32_t get_offset_of_OnGraphPreScan_40() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___OnGraphPreScan_40)); }
	inline OnGraphDelegate_t381382964 * get_OnGraphPreScan_40() const { return ___OnGraphPreScan_40; }
	inline OnGraphDelegate_t381382964 ** get_address_of_OnGraphPreScan_40() { return &___OnGraphPreScan_40; }
	inline void set_OnGraphPreScan_40(OnGraphDelegate_t381382964 * value)
	{
		___OnGraphPreScan_40 = value;
		Il2CppCodeGenWriteBarrier(&___OnGraphPreScan_40, value);
	}

	inline static int32_t get_offset_of_OnGraphPostScan_41() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___OnGraphPostScan_41)); }
	inline OnGraphDelegate_t381382964 * get_OnGraphPostScan_41() const { return ___OnGraphPostScan_41; }
	inline OnGraphDelegate_t381382964 ** get_address_of_OnGraphPostScan_41() { return &___OnGraphPostScan_41; }
	inline void set_OnGraphPostScan_41(OnGraphDelegate_t381382964 * value)
	{
		___OnGraphPostScan_41 = value;
		Il2CppCodeGenWriteBarrier(&___OnGraphPostScan_41, value);
	}

	inline static int32_t get_offset_of_OnPathPreSearch_42() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___OnPathPreSearch_42)); }
	inline OnPathDelegate_t598607977 * get_OnPathPreSearch_42() const { return ___OnPathPreSearch_42; }
	inline OnPathDelegate_t598607977 ** get_address_of_OnPathPreSearch_42() { return &___OnPathPreSearch_42; }
	inline void set_OnPathPreSearch_42(OnPathDelegate_t598607977 * value)
	{
		___OnPathPreSearch_42 = value;
		Il2CppCodeGenWriteBarrier(&___OnPathPreSearch_42, value);
	}

	inline static int32_t get_offset_of_OnPathPostSearch_43() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___OnPathPostSearch_43)); }
	inline OnPathDelegate_t598607977 * get_OnPathPostSearch_43() const { return ___OnPathPostSearch_43; }
	inline OnPathDelegate_t598607977 ** get_address_of_OnPathPostSearch_43() { return &___OnPathPostSearch_43; }
	inline void set_OnPathPostSearch_43(OnPathDelegate_t598607977 * value)
	{
		___OnPathPostSearch_43 = value;
		Il2CppCodeGenWriteBarrier(&___OnPathPostSearch_43, value);
	}

	inline static int32_t get_offset_of_OnPreScan_44() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___OnPreScan_44)); }
	inline OnScanDelegate_t3165885121 * get_OnPreScan_44() const { return ___OnPreScan_44; }
	inline OnScanDelegate_t3165885121 ** get_address_of_OnPreScan_44() { return &___OnPreScan_44; }
	inline void set_OnPreScan_44(OnScanDelegate_t3165885121 * value)
	{
		___OnPreScan_44 = value;
		Il2CppCodeGenWriteBarrier(&___OnPreScan_44, value);
	}

	inline static int32_t get_offset_of_OnPostScan_45() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___OnPostScan_45)); }
	inline OnScanDelegate_t3165885121 * get_OnPostScan_45() const { return ___OnPostScan_45; }
	inline OnScanDelegate_t3165885121 ** get_address_of_OnPostScan_45() { return &___OnPostScan_45; }
	inline void set_OnPostScan_45(OnScanDelegate_t3165885121 * value)
	{
		___OnPostScan_45 = value;
		Il2CppCodeGenWriteBarrier(&___OnPostScan_45, value);
	}

	inline static int32_t get_offset_of_OnLatePostScan_46() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___OnLatePostScan_46)); }
	inline OnScanDelegate_t3165885121 * get_OnLatePostScan_46() const { return ___OnLatePostScan_46; }
	inline OnScanDelegate_t3165885121 ** get_address_of_OnLatePostScan_46() { return &___OnLatePostScan_46; }
	inline void set_OnLatePostScan_46(OnScanDelegate_t3165885121 * value)
	{
		___OnLatePostScan_46 = value;
		Il2CppCodeGenWriteBarrier(&___OnLatePostScan_46, value);
	}

	inline static int32_t get_offset_of_OnGraphsUpdated_47() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___OnGraphsUpdated_47)); }
	inline OnScanDelegate_t3165885121 * get_OnGraphsUpdated_47() const { return ___OnGraphsUpdated_47; }
	inline OnScanDelegate_t3165885121 ** get_address_of_OnGraphsUpdated_47() { return &___OnGraphsUpdated_47; }
	inline void set_OnGraphsUpdated_47(OnScanDelegate_t3165885121 * value)
	{
		___OnGraphsUpdated_47 = value;
		Il2CppCodeGenWriteBarrier(&___OnGraphsUpdated_47, value);
	}

	inline static int32_t get_offset_of_On65KOverflow_48() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___On65KOverflow_48)); }
	inline OnVoidDelegate_t2787120856 * get_On65KOverflow_48() const { return ___On65KOverflow_48; }
	inline OnVoidDelegate_t2787120856 ** get_address_of_On65KOverflow_48() { return &___On65KOverflow_48; }
	inline void set_On65KOverflow_48(OnVoidDelegate_t2787120856 * value)
	{
		___On65KOverflow_48 = value;
		Il2CppCodeGenWriteBarrier(&___On65KOverflow_48, value);
	}

	inline static int32_t get_offset_of_OnThreadSafeCallback_49() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___OnThreadSafeCallback_49)); }
	inline OnVoidDelegate_t2787120856 * get_OnThreadSafeCallback_49() const { return ___OnThreadSafeCallback_49; }
	inline OnVoidDelegate_t2787120856 ** get_address_of_OnThreadSafeCallback_49() { return &___OnThreadSafeCallback_49; }
	inline void set_OnThreadSafeCallback_49(OnVoidDelegate_t2787120856 * value)
	{
		___OnThreadSafeCallback_49 = value;
		Il2CppCodeGenWriteBarrier(&___OnThreadSafeCallback_49, value);
	}

	inline static int32_t get_offset_of_threads_56() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___threads_56)); }
	inline ThreadU5BU5D_t3166308279* get_threads_56() const { return ___threads_56; }
	inline ThreadU5BU5D_t3166308279** get_address_of_threads_56() { return &___threads_56; }
	inline void set_threads_56(ThreadU5BU5D_t3166308279* value)
	{
		___threads_56 = value;
		Il2CppCodeGenWriteBarrier(&___threads_56, value);
	}

	inline static int32_t get_offset_of_threadInfos_58() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___threadInfos_58)); }
	inline PathThreadInfoU5BU5D_t3711851138* get_threadInfos_58() const { return ___threadInfos_58; }
	inline PathThreadInfoU5BU5D_t3711851138** get_address_of_threadInfos_58() { return &___threadInfos_58; }
	inline void set_threadInfos_58(PathThreadInfoU5BU5D_t3711851138* value)
	{
		___threadInfos_58 = value;
		Il2CppCodeGenWriteBarrier(&___threadInfos_58, value);
	}

	inline static int32_t get_offset_of_threadEnumerator_59() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___threadEnumerator_59)); }
	inline Il2CppObject * get_threadEnumerator_59() const { return ___threadEnumerator_59; }
	inline Il2CppObject ** get_address_of_threadEnumerator_59() { return &___threadEnumerator_59; }
	inline void set_threadEnumerator_59(Il2CppObject * value)
	{
		___threadEnumerator_59 = value;
		Il2CppCodeGenWriteBarrier(&___threadEnumerator_59, value);
	}

	inline static int32_t get_offset_of_pathReturnStack_60() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___pathReturnStack_60)); }
	inline LockFreeStack_t746776019 * get_pathReturnStack_60() const { return ___pathReturnStack_60; }
	inline LockFreeStack_t746776019 ** get_address_of_pathReturnStack_60() { return &___pathReturnStack_60; }
	inline void set_pathReturnStack_60(LockFreeStack_t746776019 * value)
	{
		___pathReturnStack_60 = value;
		Il2CppCodeGenWriteBarrier(&___pathReturnStack_60, value);
	}

	inline static int32_t get_offset_of_isEditor_68() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___isEditor_68)); }
	inline bool get_isEditor_68() const { return ___isEditor_68; }
	inline bool* get_address_of_isEditor_68() { return &___isEditor_68; }
	inline void set_isEditor_68(bool value)
	{
		___isEditor_68 = value;
	}

	inline static int32_t get_offset_of_safeUpdateLock_70() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___safeUpdateLock_70)); }
	inline Il2CppObject * get_safeUpdateLock_70() const { return ___safeUpdateLock_70; }
	inline Il2CppObject ** get_address_of_safeUpdateLock_70() { return &___safeUpdateLock_70; }
	inline void set_safeUpdateLock_70(Il2CppObject * value)
	{
		___safeUpdateLock_70 = value;
		Il2CppCodeGenWriteBarrier(&___safeUpdateLock_70, value);
	}

	inline static int32_t get_offset_of_waitForPathDepth_77() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___waitForPathDepth_77)); }
	inline int32_t get_waitForPathDepth_77() const { return ___waitForPathDepth_77; }
	inline int32_t* get_address_of_waitForPathDepth_77() { return &___waitForPathDepth_77; }
	inline void set_waitForPathDepth_77(int32_t value)
	{
		___waitForPathDepth_77 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4C_78() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___U3CU3Ef__amU24cache4C_78)); }
	inline GraphNodeDelegateCancelable_t3591372971 * get_U3CU3Ef__amU24cache4C_78() const { return ___U3CU3Ef__amU24cache4C_78; }
	inline GraphNodeDelegateCancelable_t3591372971 ** get_address_of_U3CU3Ef__amU24cache4C_78() { return &___U3CU3Ef__amU24cache4C_78; }
	inline void set_U3CU3Ef__amU24cache4C_78(GraphNodeDelegateCancelable_t3591372971 * value)
	{
		___U3CU3Ef__amU24cache4C_78 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4C_78, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4D_79() { return static_cast<int32_t>(offsetof(AstarPath_t4090270936_StaticFields, ___U3CU3Ef__amU24cache4D_79)); }
	inline GraphNodeDelegateCancelable_t3591372971 * get_U3CU3Ef__amU24cache4D_79() const { return ___U3CU3Ef__amU24cache4D_79; }
	inline GraphNodeDelegateCancelable_t3591372971 ** get_address_of_U3CU3Ef__amU24cache4D_79() { return &___U3CU3Ef__amU24cache4D_79; }
	inline void set_U3CU3Ef__amU24cache4D_79(GraphNodeDelegateCancelable_t3591372971 * value)
	{
		___U3CU3Ef__amU24cache4D_79 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4D_79, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
