﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_IEnumerableGenerated
struct System_Collections_IEnumerableGenerated_t3506779160;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Collections_IEnumerableGenerated::.ctor()
extern "C"  void System_Collections_IEnumerableGenerated__ctor_m2148234835 (System_Collections_IEnumerableGenerated_t3506779160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_IEnumerableGenerated::IEnumerable_GetEnumerator(JSVCall,System.Int32)
extern "C"  bool System_Collections_IEnumerableGenerated_IEnumerable_GetEnumerator_m931895841 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_IEnumerableGenerated::__Register()
extern "C"  void System_Collections_IEnumerableGenerated___Register_m502399572 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Collections_IEnumerableGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_Collections_IEnumerableGenerated_ilo_setObject1_m3193988923 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
