﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>
struct Dictionary_2_t2163003329;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Val94836737.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24287931429.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1828353326_gshared (Enumerator_t94836737 * __this, Dictionary_2_t2163003329 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1828353326(__this, ___host0, method) ((  void (*) (Enumerator_t94836737 *, Dictionary_2_t2163003329 *, const MethodInfo*))Enumerator__ctor_m1828353326_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1376736307_gshared (Enumerator_t94836737 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1376736307(__this, method) ((  Il2CppObject * (*) (Enumerator_t94836737 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1376736307_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2341763783_gshared (Enumerator_t94836737 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2341763783(__this, method) ((  void (*) (Enumerator_t94836737 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2341763783_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::Dispose()
extern "C"  void Enumerator_Dispose_m3044213584_gshared (Enumerator_t94836737 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3044213584(__this, method) ((  void (*) (Enumerator_t94836737 *, const MethodInfo*))Enumerator_Dispose_m3044213584_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m255611443_gshared (Enumerator_t94836737 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m255611443(__this, method) ((  bool (*) (Enumerator_t94836737 *, const MethodInfo*))Enumerator_MoveNext_m255611443_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Current()
extern "C"  KeyValuePair_2_t4287931429  Enumerator_get_Current_m343446383_gshared (Enumerator_t94836737 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m343446383(__this, method) ((  KeyValuePair_2_t4287931429  (*) (Enumerator_t94836737 *, const MethodInfo*))Enumerator_get_Current_m343446383_gshared)(__this, method)
