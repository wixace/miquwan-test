﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpringPanelGenerated
struct SpringPanelGenerated_t1604819832;
// JSVCall
struct JSVCall_t3708497963;
// SpringPanel/OnFinished
struct OnFinished_t3316389065;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Delegate
struct Delegate_t3310234105;
// SpringPanel
struct SpringPanel_t929169367;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void SpringPanelGenerated::.ctor()
extern "C"  void SpringPanelGenerated__ctor_m1565271587 (SpringPanelGenerated_t1604819832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SpringPanelGenerated::SpringPanel_SpringPanel1(JSVCall,System.Int32)
extern "C"  bool SpringPanelGenerated_SpringPanel_SpringPanel1_m2539694215 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPanelGenerated::SpringPanel_current(JSVCall)
extern "C"  void SpringPanelGenerated_SpringPanel_current_m3708125493 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPanelGenerated::SpringPanel_target(JSVCall)
extern "C"  void SpringPanelGenerated_SpringPanel_target_m3794056461 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPanelGenerated::SpringPanel_strength(JSVCall)
extern "C"  void SpringPanelGenerated_SpringPanel_strength_m3684417117 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SpringPanel/OnFinished SpringPanelGenerated::SpringPanel_onFinished_GetDelegate_member3_arg0(CSRepresentedObject)
extern "C"  OnFinished_t3316389065 * SpringPanelGenerated_SpringPanel_onFinished_GetDelegate_member3_arg0_m1978452261 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPanelGenerated::SpringPanel_onFinished(JSVCall)
extern "C"  void SpringPanelGenerated_SpringPanel_onFinished_m3441337133 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SpringPanelGenerated::SpringPanel_Begin__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool SpringPanelGenerated_SpringPanel_Begin__GameObject__Vector3__Single_m3582633803 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPanelGenerated::__Register()
extern "C"  void SpringPanelGenerated___Register_m2344006788 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SpringPanel/OnFinished SpringPanelGenerated::<SpringPanel_onFinished>m__9A()
extern "C"  OnFinished_t3316389065 * SpringPanelGenerated_U3CSpringPanel_onFinishedU3Em__9A_m3245641817 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SpringPanelGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t SpringPanelGenerated_ilo_getObject1_m1005569487 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SpringPanelGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool SpringPanelGenerated_ilo_attachFinalizerObject2_m4071625309 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPanelGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void SpringPanelGenerated_ilo_addJSCSRel3_m3771887713 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SpringPanelGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t SpringPanelGenerated_ilo_setObject4_m277403658 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SpringPanelGenerated::ilo_getVector3S5(System.Int32)
extern "C"  Vector3_t4282066566  SpringPanelGenerated_ilo_getVector3S5_m3650250043 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPanelGenerated::ilo_addJSFunCSDelegateRel6(System.Int32,System.Delegate)
extern "C"  void SpringPanelGenerated_ilo_addJSFunCSDelegateRel6_m3895469140 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SpringPanelGenerated::ilo_getObject7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * SpringPanelGenerated_ilo_getObject7_m1081095154 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SpringPanel SpringPanelGenerated::ilo_Begin8(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  SpringPanel_t929169367 * SpringPanelGenerated_ilo_Begin8_m2390796847 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, Vector3_t4282066566  ___pos1, float ___strength2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SpringPanel/OnFinished SpringPanelGenerated::ilo_SpringPanel_onFinished_GetDelegate_member3_arg09(CSRepresentedObject)
extern "C"  OnFinished_t3316389065 * SpringPanelGenerated_ilo_SpringPanel_onFinished_GetDelegate_member3_arg09_m4180621477 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
