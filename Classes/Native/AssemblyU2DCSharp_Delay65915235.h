﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Delay
struct  Delay_t65915235  : public MonoBehaviour_t667441552
{
public:
	// System.Single Delay::delayTime
	float ___delayTime_2;
	// System.Boolean Delay::isDelayCall
	bool ___isDelayCall_3;

public:
	inline static int32_t get_offset_of_delayTime_2() { return static_cast<int32_t>(offsetof(Delay_t65915235, ___delayTime_2)); }
	inline float get_delayTime_2() const { return ___delayTime_2; }
	inline float* get_address_of_delayTime_2() { return &___delayTime_2; }
	inline void set_delayTime_2(float value)
	{
		___delayTime_2 = value;
	}

	inline static int32_t get_offset_of_isDelayCall_3() { return static_cast<int32_t>(offsetof(Delay_t65915235, ___isDelayCall_3)); }
	inline bool get_isDelayCall_3() const { return ___isDelayCall_3; }
	inline bool* get_address_of_isDelayCall_3() { return &___isDelayCall_3; }
	inline void set_isDelayCall_3(bool value)
	{
		___isDelayCall_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
