﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CSRepresentedObject
struct CSRepresentedObject_t3994124630;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventDelegateGenerated/<EventDelegate_Add_GetDelegate_member5_arg1>c__AnonStorey5D
struct  U3CEventDelegate_Add_GetDelegate_member5_arg1U3Ec__AnonStorey5D_t2306767574  : public Il2CppObject
{
public:
	// CSRepresentedObject EventDelegateGenerated/<EventDelegate_Add_GetDelegate_member5_arg1>c__AnonStorey5D::objFunction
	CSRepresentedObject_t3994124630 * ___objFunction_0;

public:
	inline static int32_t get_offset_of_objFunction_0() { return static_cast<int32_t>(offsetof(U3CEventDelegate_Add_GetDelegate_member5_arg1U3Ec__AnonStorey5D_t2306767574, ___objFunction_0)); }
	inline CSRepresentedObject_t3994124630 * get_objFunction_0() const { return ___objFunction_0; }
	inline CSRepresentedObject_t3994124630 ** get_address_of_objFunction_0() { return &___objFunction_0; }
	inline void set_objFunction_0(CSRepresentedObject_t3994124630 * value)
	{
		___objFunction_0 = value;
		Il2CppCodeGenWriteBarrier(&___objFunction_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
