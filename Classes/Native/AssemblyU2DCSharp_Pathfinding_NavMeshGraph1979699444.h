﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Mesh
struct Mesh_t4241756145;
// Pathfinding.TriangleMeshNode[]
struct TriangleMeshNodeU5BU5D_t2064970368;
// Pathfinding.BBTree
struct BBTree_t1216325332;
// Pathfinding.Int3[]
struct Int3U5BU5D_t516284607;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "AssemblyU2DCSharp_Pathfinding_NavGraph1254319713.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.NavMeshGraph
struct  NavMeshGraph_t1979699444  : public NavGraph_t1254319713
{
public:
	// UnityEngine.Mesh Pathfinding.NavMeshGraph::sourceMesh
	Mesh_t4241756145 * ___sourceMesh_11;
	// UnityEngine.Vector3 Pathfinding.NavMeshGraph::offset
	Vector3_t4282066566  ___offset_12;
	// UnityEngine.Vector3 Pathfinding.NavMeshGraph::rotation
	Vector3_t4282066566  ___rotation_13;
	// System.Single Pathfinding.NavMeshGraph::scale
	float ___scale_14;
	// System.Boolean Pathfinding.NavMeshGraph::accurateNearestNode
	bool ___accurateNearestNode_15;
	// Pathfinding.TriangleMeshNode[] Pathfinding.NavMeshGraph::nodes
	TriangleMeshNodeU5BU5D_t2064970368* ___nodes_16;
	// Pathfinding.BBTree Pathfinding.NavMeshGraph::_bbTree
	BBTree_t1216325332 * ____bbTree_17;
	// Pathfinding.Int3[] Pathfinding.NavMeshGraph::_vertices
	Int3U5BU5D_t516284607* ____vertices_18;
	// UnityEngine.Vector3[] Pathfinding.NavMeshGraph::originalVertices
	Vector3U5BU5D_t215400611* ___originalVertices_19;
	// System.Int32[] Pathfinding.NavMeshGraph::triangles
	Int32U5BU5D_t3230847821* ___triangles_20;

public:
	inline static int32_t get_offset_of_sourceMesh_11() { return static_cast<int32_t>(offsetof(NavMeshGraph_t1979699444, ___sourceMesh_11)); }
	inline Mesh_t4241756145 * get_sourceMesh_11() const { return ___sourceMesh_11; }
	inline Mesh_t4241756145 ** get_address_of_sourceMesh_11() { return &___sourceMesh_11; }
	inline void set_sourceMesh_11(Mesh_t4241756145 * value)
	{
		___sourceMesh_11 = value;
		Il2CppCodeGenWriteBarrier(&___sourceMesh_11, value);
	}

	inline static int32_t get_offset_of_offset_12() { return static_cast<int32_t>(offsetof(NavMeshGraph_t1979699444, ___offset_12)); }
	inline Vector3_t4282066566  get_offset_12() const { return ___offset_12; }
	inline Vector3_t4282066566 * get_address_of_offset_12() { return &___offset_12; }
	inline void set_offset_12(Vector3_t4282066566  value)
	{
		___offset_12 = value;
	}

	inline static int32_t get_offset_of_rotation_13() { return static_cast<int32_t>(offsetof(NavMeshGraph_t1979699444, ___rotation_13)); }
	inline Vector3_t4282066566  get_rotation_13() const { return ___rotation_13; }
	inline Vector3_t4282066566 * get_address_of_rotation_13() { return &___rotation_13; }
	inline void set_rotation_13(Vector3_t4282066566  value)
	{
		___rotation_13 = value;
	}

	inline static int32_t get_offset_of_scale_14() { return static_cast<int32_t>(offsetof(NavMeshGraph_t1979699444, ___scale_14)); }
	inline float get_scale_14() const { return ___scale_14; }
	inline float* get_address_of_scale_14() { return &___scale_14; }
	inline void set_scale_14(float value)
	{
		___scale_14 = value;
	}

	inline static int32_t get_offset_of_accurateNearestNode_15() { return static_cast<int32_t>(offsetof(NavMeshGraph_t1979699444, ___accurateNearestNode_15)); }
	inline bool get_accurateNearestNode_15() const { return ___accurateNearestNode_15; }
	inline bool* get_address_of_accurateNearestNode_15() { return &___accurateNearestNode_15; }
	inline void set_accurateNearestNode_15(bool value)
	{
		___accurateNearestNode_15 = value;
	}

	inline static int32_t get_offset_of_nodes_16() { return static_cast<int32_t>(offsetof(NavMeshGraph_t1979699444, ___nodes_16)); }
	inline TriangleMeshNodeU5BU5D_t2064970368* get_nodes_16() const { return ___nodes_16; }
	inline TriangleMeshNodeU5BU5D_t2064970368** get_address_of_nodes_16() { return &___nodes_16; }
	inline void set_nodes_16(TriangleMeshNodeU5BU5D_t2064970368* value)
	{
		___nodes_16 = value;
		Il2CppCodeGenWriteBarrier(&___nodes_16, value);
	}

	inline static int32_t get_offset_of__bbTree_17() { return static_cast<int32_t>(offsetof(NavMeshGraph_t1979699444, ____bbTree_17)); }
	inline BBTree_t1216325332 * get__bbTree_17() const { return ____bbTree_17; }
	inline BBTree_t1216325332 ** get_address_of__bbTree_17() { return &____bbTree_17; }
	inline void set__bbTree_17(BBTree_t1216325332 * value)
	{
		____bbTree_17 = value;
		Il2CppCodeGenWriteBarrier(&____bbTree_17, value);
	}

	inline static int32_t get_offset_of__vertices_18() { return static_cast<int32_t>(offsetof(NavMeshGraph_t1979699444, ____vertices_18)); }
	inline Int3U5BU5D_t516284607* get__vertices_18() const { return ____vertices_18; }
	inline Int3U5BU5D_t516284607** get_address_of__vertices_18() { return &____vertices_18; }
	inline void set__vertices_18(Int3U5BU5D_t516284607* value)
	{
		____vertices_18 = value;
		Il2CppCodeGenWriteBarrier(&____vertices_18, value);
	}

	inline static int32_t get_offset_of_originalVertices_19() { return static_cast<int32_t>(offsetof(NavMeshGraph_t1979699444, ___originalVertices_19)); }
	inline Vector3U5BU5D_t215400611* get_originalVertices_19() const { return ___originalVertices_19; }
	inline Vector3U5BU5D_t215400611** get_address_of_originalVertices_19() { return &___originalVertices_19; }
	inline void set_originalVertices_19(Vector3U5BU5D_t215400611* value)
	{
		___originalVertices_19 = value;
		Il2CppCodeGenWriteBarrier(&___originalVertices_19, value);
	}

	inline static int32_t get_offset_of_triangles_20() { return static_cast<int32_t>(offsetof(NavMeshGraph_t1979699444, ___triangles_20)); }
	inline Int32U5BU5D_t3230847821* get_triangles_20() const { return ___triangles_20; }
	inline Int32U5BU5D_t3230847821** get_address_of_triangles_20() { return &___triangles_20; }
	inline void set_triangles_20(Int32U5BU5D_t3230847821* value)
	{
		___triangles_20 = value;
		Il2CppCodeGenWriteBarrier(&___triangles_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
