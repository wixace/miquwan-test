﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action
struct Action_t3771233898;
// HttpDownLoad
struct HttpDownLoad_t2310403440;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HttpDownLoad/<DownLoad>c__AnonStorey15D
struct  U3CDownLoadU3Ec__AnonStorey15D_t664264562  : public Il2CppObject
{
public:
	// System.String HttpDownLoad/<DownLoad>c__AnonStorey15D::saveFullPath
	String_t* ___saveFullPath_0;
	// System.String HttpDownLoad/<DownLoad>c__AnonStorey15D::url
	String_t* ___url_1;
	// System.Action HttpDownLoad/<DownLoad>c__AnonStorey15D::callBack
	Action_t3771233898 * ___callBack_2;
	// HttpDownLoad HttpDownLoad/<DownLoad>c__AnonStorey15D::<>f__this
	HttpDownLoad_t2310403440 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_saveFullPath_0() { return static_cast<int32_t>(offsetof(U3CDownLoadU3Ec__AnonStorey15D_t664264562, ___saveFullPath_0)); }
	inline String_t* get_saveFullPath_0() const { return ___saveFullPath_0; }
	inline String_t** get_address_of_saveFullPath_0() { return &___saveFullPath_0; }
	inline void set_saveFullPath_0(String_t* value)
	{
		___saveFullPath_0 = value;
		Il2CppCodeGenWriteBarrier(&___saveFullPath_0, value);
	}

	inline static int32_t get_offset_of_url_1() { return static_cast<int32_t>(offsetof(U3CDownLoadU3Ec__AnonStorey15D_t664264562, ___url_1)); }
	inline String_t* get_url_1() const { return ___url_1; }
	inline String_t** get_address_of_url_1() { return &___url_1; }
	inline void set_url_1(String_t* value)
	{
		___url_1 = value;
		Il2CppCodeGenWriteBarrier(&___url_1, value);
	}

	inline static int32_t get_offset_of_callBack_2() { return static_cast<int32_t>(offsetof(U3CDownLoadU3Ec__AnonStorey15D_t664264562, ___callBack_2)); }
	inline Action_t3771233898 * get_callBack_2() const { return ___callBack_2; }
	inline Action_t3771233898 ** get_address_of_callBack_2() { return &___callBack_2; }
	inline void set_callBack_2(Action_t3771233898 * value)
	{
		___callBack_2 = value;
		Il2CppCodeGenWriteBarrier(&___callBack_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CDownLoadU3Ec__AnonStorey15D_t664264562, ___U3CU3Ef__this_3)); }
	inline HttpDownLoad_t2310403440 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline HttpDownLoad_t2310403440 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(HttpDownLoad_t2310403440 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
