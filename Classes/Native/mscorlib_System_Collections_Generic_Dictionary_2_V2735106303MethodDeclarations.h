﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3616407643MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PushType,PluginPush/NotificationData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m3145896437(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2735106303 *, Dictionary_2_t4034500590 *, const MethodInfo*))ValueCollection__ctor_m1124126716_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PushType,PluginPush/NotificationData>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1217861533(__this, ___item0, method) ((  void (*) (ValueCollection_t2735106303 *, NotificationData_t3289515031 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m182811830_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PushType,PluginPush/NotificationData>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m266033894(__this, method) ((  void (*) (ValueCollection_t2735106303 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1121072255_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PushType,PluginPush/NotificationData>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m226273261(__this, ___item0, method) ((  bool (*) (ValueCollection_t2735106303 *, NotificationData_t3289515031 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m728430068_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PushType,PluginPush/NotificationData>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1967708114(__this, ___item0, method) ((  bool (*) (ValueCollection_t2735106303 *, NotificationData_t3289515031 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3434150809_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<PushType,PluginPush/NotificationData>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m45335654(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2735106303 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1878098111_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PushType,PluginPush/NotificationData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2996966506(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2735106303 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m38568643_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<PushType,PluginPush/NotificationData>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3638942329(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2735106303 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1572261522_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PushType,PluginPush/NotificationData>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2796416416(__this, method) ((  bool (*) (ValueCollection_t2735106303 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3298573223_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PushType,PluginPush/NotificationData>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2138284928(__this, method) ((  bool (*) (ValueCollection_t2735106303 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1672199_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<PushType,PluginPush/NotificationData>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m152865330(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2735106303 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1865478457_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PushType,PluginPush/NotificationData>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m3041706236(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2735106303 *, NotificationDataU5BU5D_t2759200174*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2200859971_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<PushType,PluginPush/NotificationData>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3998258853(__this, method) ((  Enumerator_t1966333998  (*) (ValueCollection_t2735106303 *, const MethodInfo*))ValueCollection_GetEnumerator_m3067676780_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<PushType,PluginPush/NotificationData>::get_Count()
#define ValueCollection_get_Count_m1930143290(__this, method) ((  int32_t (*) (ValueCollection_t2735106303 *, const MethodInfo*))ValueCollection_get_Count_m1841598273_gshared)(__this, method)
