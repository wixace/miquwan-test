﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Buff>
struct List_1_t1370267459;
// CombatEntity
struct CombatEntity_t684137495;
// RemoveEffect
struct RemoveEffect_t3191766869;
// System.Collections.Generic.List`1<BuffState>
struct List_1_t3417094830;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_BuffEnum_EBuffState2159802479.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuffCtrl
struct  BuffCtrl_t2836564350  : public Il2CppObject
{
public:
	// System.Single BuffCtrl::lastTime
	float ___lastTime_0;
	// System.Collections.Generic.List`1<Buff> BuffCtrl::buffList
	List_1_t1370267459 * ___buffList_1;
	// System.Collections.Generic.List`1<Buff> BuffCtrl::cacheBuffList
	List_1_t1370267459 * ___cacheBuffList_2;
	// CombatEntity BuffCtrl::entity
	CombatEntity_t684137495 * ___entity_3;
	// RemoveEffect BuffCtrl::reEvent
	RemoveEffect_t3191766869 * ___reEvent_4;
	// System.Single BuffCtrl::srcCureValue
	float ___srcCureValue_5;
	// BuffEnum.EBuffState BuffCtrl::realBuffState
	uint64_t ___realBuffState_6;
	// BuffEnum.EBuffState BuffCtrl::lastBuffState
	uint64_t ___lastBuffState_7;
	// System.Boolean BuffCtrl::isAddBuff
	bool ___isAddBuff_8;
	// System.Collections.Generic.List`1<BuffState> BuffCtrl::buffStateList
	List_1_t3417094830 * ___buffStateList_9;
	// System.Collections.Generic.List`1<System.Int32> BuffCtrl::buffStateIDList
	List_1_t2522024052 * ___buffStateIDList_10;
	// System.Collections.Generic.List`1<BuffState> BuffCtrl::timeBombBuffState
	List_1_t3417094830 * ___timeBombBuffState_11;
	// System.Int32 BuffCtrl::<curnum>k__BackingField
	int32_t ___U3CcurnumU3Ek__BackingField_12;
	// System.Int32 BuffCtrl::<mAnitNum>k__BackingField
	int32_t ___U3CmAnitNumU3Ek__BackingField_13;
	// System.Int32 BuffCtrl::<mAuraAddHPRadius>k__BackingField
	int32_t ___U3CmAuraAddHPRadiusU3Ek__BackingField_14;
	// System.Int32 BuffCtrl::<mAuraReduceHPRadius>k__BackingField
	int32_t ___U3CmAuraReduceHPRadiusU3Ek__BackingField_15;
	// System.Int32 BuffCtrl::<mHpLineNum>k__BackingField
	int32_t ___U3CmHpLineNumU3Ek__BackingField_16;
	// System.Int32 BuffCtrl::<mRuneTakeEffectNum>k__BackingField
	int32_t ___U3CmRuneTakeEffectNumU3Ek__BackingField_17;
	// System.Single BuffCtrl::<mGodDownTweenTime>k__BackingField
	float ___U3CmGodDownTweenTimeU3Ek__BackingField_18;
	// System.Int32 BuffCtrl::<mAntiDebuffDiscreteAddBuffID>k__BackingField
	int32_t ___U3CmAntiDebuffDiscreteAddBuffIDU3Ek__BackingField_19;
	// System.Int32 BuffCtrl::<mAntiDebuffDiscreteID>k__BackingField
	int32_t ___U3CmAntiDebuffDiscreteIDU3Ek__BackingField_20;
	// System.Single BuffCtrl::<mPetrifiedChangeTime>k__BackingField
	float ___U3CmPetrifiedChangeTimeU3Ek__BackingField_21;
	// System.Int32 BuffCtrl::<mChangeOldSkillID>k__BackingField
	int32_t ___U3CmChangeOldSkillIDU3Ek__BackingField_22;
	// System.Int32 BuffCtrl::<mWhenCritAddBuffID>k__BackingField
	int32_t ___U3CmWhenCritAddBuffIDU3Ek__BackingField_23;
	// System.Int32 BuffCtrl::<mThumpSkillID>k__BackingField
	int32_t ___U3CmThumpSkillIDU3Ek__BackingField_24;
	// System.Int32 BuffCtrl::<mTankBuffID>k__BackingField
	int32_t ___U3CmTankBuffIDU3Ek__BackingField_25;
	// System.Int32 BuffCtrl::<mReduceRageHpNum>k__BackingField
	int32_t ___U3CmReduceRageHpNumU3Ek__BackingField_26;
	// System.Int32 BuffCtrl::<mGhostIndex>k__BackingField
	int32_t ___U3CmGhostIndexU3Ek__BackingField_27;
	// System.Int32 BuffCtrl::<mSusanooIndex>k__BackingField
	int32_t ___U3CmSusanooIndexU3Ek__BackingField_28;
	// System.Single BuffCtrl::<mDistorTionDelayTime>k__BackingField
	float ___U3CmDistorTionDelayTimeU3Ek__BackingField_29;
	// System.Int32 BuffCtrl::<mDeathHarvestLine>k__BackingField
	int32_t ___U3CmDeathHarvestLineU3Ek__BackingField_30;
	// System.Int32 BuffCtrl::<mResistanceCountryType>k__BackingField
	int32_t ___U3CmResistanceCountryTypeU3Ek__BackingField_31;
	// System.Int32 BuffCtrl::<mTauntDis>k__BackingField
	int32_t ___U3CmTauntDisU3Ek__BackingField_32;
	// System.Int32 BuffCtrl::<mBuffATNum>k__BackingField
	int32_t ___U3CmBuffATNumU3Ek__BackingField_33;
	// System.Int32 BuffCtrl::<mBuffPDNum>k__BackingField
	int32_t ___U3CmBuffPDNumU3Ek__BackingField_34;
	// System.Int32 BuffCtrl::<mBuffMDNum>k__BackingField
	int32_t ___U3CmBuffMDNumU3Ek__BackingField_35;
	// System.Int32 BuffCtrl::<mBuffMaxHPNum>k__BackingField
	int32_t ___U3CmBuffMaxHPNumU3Ek__BackingField_36;
	// System.Int32 BuffCtrl::<mBuffHitsNum>k__BackingField
	int32_t ___U3CmBuffHitsNumU3Ek__BackingField_37;
	// System.Int32 BuffCtrl::<mBuffDodgeNum>k__BackingField
	int32_t ___U3CmBuffDodgeNumU3Ek__BackingField_38;
	// System.Int32 BuffCtrl::<mBuffWreckNum>k__BackingField
	int32_t ___U3CmBuffWreckNumU3Ek__BackingField_39;
	// System.Int32 BuffCtrl::<mBuffWithstandNum>k__BackingField
	int32_t ___U3CmBuffWithstandNumU3Ek__BackingField_40;
	// System.Int32 BuffCtrl::<mBuffCritNum>k__BackingField
	int32_t ___U3CmBuffCritNumU3Ek__BackingField_41;
	// System.Int32 BuffCtrl::<mBuffTenacityNum>k__BackingField
	int32_t ___U3CmBuffTenacityNumU3Ek__BackingField_42;
	// System.UInt32 BuffCtrl::<mBuffRHurtNum>k__BackingField
	uint32_t ___U3CmBuffRHurtNumU3Ek__BackingField_43;
	// System.Int32 BuffCtrl::<mBuffAT>k__BackingField
	int32_t ___U3CmBuffATU3Ek__BackingField_44;
	// System.Int32 BuffCtrl::<mBuffPD>k__BackingField
	int32_t ___U3CmBuffPDU3Ek__BackingField_45;
	// System.Int32 BuffCtrl::<mBuffMD>k__BackingField
	int32_t ___U3CmBuffMDU3Ek__BackingField_46;
	// System.Int32 BuffCtrl::<mBuffMaxHP>k__BackingField
	int32_t ___U3CmBuffMaxHPU3Ek__BackingField_47;
	// System.Int32 BuffCtrl::<mBuffHits>k__BackingField
	int32_t ___U3CmBuffHitsU3Ek__BackingField_48;
	// System.Int32 BuffCtrl::<mBuffDodge>k__BackingField
	int32_t ___U3CmBuffDodgeU3Ek__BackingField_49;
	// System.Int32 BuffCtrl::<mBuffWreck>k__BackingField
	int32_t ___U3CmBuffWreckU3Ek__BackingField_50;
	// System.Int32 BuffCtrl::<mBuffWithstand>k__BackingField
	int32_t ___U3CmBuffWithstandU3Ek__BackingField_51;
	// System.Int32 BuffCtrl::<mBuffCrit>k__BackingField
	int32_t ___U3CmBuffCritU3Ek__BackingField_52;
	// System.Int32 BuffCtrl::<mBuffTenacity>k__BackingField
	int32_t ___U3CmBuffTenacityU3Ek__BackingField_53;
	// System.UInt32 BuffCtrl::<mBuffRHurt>k__BackingField
	uint32_t ___U3CmBuffRHurtU3Ek__BackingField_54;
	// System.Single BuffCtrl::<mHPSecDecPer>k__BackingField
	float ___U3CmHPSecDecPerU3Ek__BackingField_55;
	// System.Single BuffCtrl::<mHPSecDecNum>k__BackingField
	float ___U3CmHPSecDecNumU3Ek__BackingField_56;
	// System.Single BuffCtrl::<mHPRstNum>k__BackingField
	float ___U3CmHPRstNumU3Ek__BackingField_57;
	// System.Single BuffCtrl::<mHPRstPer>k__BackingField
	float ___U3CmHPRstPerU3Ek__BackingField_58;
	// System.Single BuffCtrl::<mAttackSpeed>k__BackingField
	float ___U3CmAttackSpeedU3Ek__BackingField_59;
	// System.Single BuffCtrl::<mModelScale>k__BackingField
	float ___U3CmModelScaleU3Ek__BackingField_60;
	// System.Single BuffCtrl::<mTreatmentEffect>k__BackingField
	float ___U3CmTreatmentEffectU3Ek__BackingField_61;
	// System.Single BuffCtrl::<mHurtEffectPlus>k__BackingField
	float ___U3CmHurtEffectPlusU3Ek__BackingField_62;
	// System.Single BuffCtrl::<mHurtEffectReduction>k__BackingField
	float ___U3CmHurtEffectReductionU3Ek__BackingField_63;
	// System.Single BuffCtrl::<mVampire>k__BackingField
	float ___U3CmVampireU3Ek__BackingField_64;
	// System.Int32 BuffCtrl::<mAngerSecDecNum>k__BackingField
	int32_t ___U3CmAngerSecDecNumU3Ek__BackingField_65;
	// System.Single BuffCtrl::<mRunspeed>k__BackingField
	float ___U3CmRunspeedU3Ek__BackingField_66;
	// System.Int32 BuffCtrl::<mHidePart>k__BackingField
	int32_t ___U3CmHidePartU3Ek__BackingField_67;
	// System.Single BuffCtrl::<mHPSecDecNumATK>k__BackingField
	float ___U3CmHPSecDecNumATKU3Ek__BackingField_68;
	// System.Single BuffCtrl::<mHPRstNumATK>k__BackingField
	float ___U3CmHPRstNumATKU3Ek__BackingField_69;
	// System.Int32 BuffCtrl::<mRestrintCountryType>k__BackingField
	int32_t ___U3CmRestrintCountryTypeU3Ek__BackingField_70;
	// System.Single BuffCtrl::<mRestrintPer>k__BackingField
	float ___U3CmRestrintPerU3Ek__BackingField_71;
	// System.Int32 BuffCtrl::<mRestrintStateType>k__BackingField
	int32_t ___U3CmRestrintStateTypeU3Ek__BackingField_72;
	// System.Single BuffCtrl::<mRestrintStatePer>k__BackingField
	float ___U3CmRestrintStatePerU3Ek__BackingField_73;
	// System.Int32 BuffCtrl::<mAtkPerHp>k__BackingField
	int32_t ___U3CmAtkPerHpU3Ek__BackingField_74;
	// System.Int32 BuffCtrl::<mAtkNumHp>k__BackingField
	int32_t ___U3CmAtkNumHpU3Ek__BackingField_75;
	// System.Int32 BuffCtrl::<mNDPAddBuff>k__BackingField
	int32_t ___U3CmNDPAddBuffU3Ek__BackingField_76;
	// System.Int32 BuffCtrl::<mBeAttackedLaterSkillID>k__BackingField
	int32_t ___U3CmBeAttackedLaterSkillIDU3Ek__BackingField_77;
	// System.Int32 BuffCtrl::<mBeAttackedLaterEffectID>k__BackingField
	int32_t ___U3CmBeAttackedLaterEffectIDU3Ek__BackingField_78;
	// System.Int32 BuffCtrl::<mDeathLaterEffectID>k__BackingField
	int32_t ___U3CmDeathLaterEffectIDU3Ek__BackingField_79;
	// System.Int32 BuffCtrl::<mDeathLaterHeroID>k__BackingField
	int32_t ___U3CmDeathLaterHeroIDU3Ek__BackingField_80;
	// System.Int32 BuffCtrl::<mBeattackedLaterSpeed>k__BackingField
	int32_t ___U3CmBeattackedLaterSpeedU3Ek__BackingField_81;
	// System.Int32 BuffCtrl::<mDeathLaterSpeed>k__BackingField
	int32_t ___U3CmDeathLaterSpeedU3Ek__BackingField_82;
	// System.Int32 BuffCtrl::<mAnitEndEffectID>k__BackingField
	int32_t ___U3CmAnitEndEffectIDU3Ek__BackingField_83;
	// System.Int32 BuffCtrl::<mAuraAddHPNum>k__BackingField
	int32_t ___U3CmAuraAddHPNumU3Ek__BackingField_84;
	// System.Int32 BuffCtrl::<mAuraAddHPPer>k__BackingField
	int32_t ___U3CmAuraAddHPPerU3Ek__BackingField_85;
	// System.Int32 BuffCtrl::<mAuraReduceHPNum>k__BackingField
	int32_t ___U3CmAuraReduceHPNumU3Ek__BackingField_86;
	// System.Int32 BuffCtrl::<mAuraReduceHPPer>k__BackingField
	int32_t ___U3CmAuraReduceHPPerU3Ek__BackingField_87;
	// System.Int32 BuffCtrl::<mActiveSkillDamageAddPer>k__BackingField
	int32_t ___U3CmActiveSkillDamageAddPerU3Ek__BackingField_88;
	// System.Int32 BuffCtrl::<mAddAngerRatio>k__BackingField
	int32_t ___U3CmAddAngerRatioU3Ek__BackingField_89;
	// System.Boolean BuffCtrl::<mFreeze>k__BackingField
	bool ___U3CmFreezeU3Ek__BackingField_90;
	// System.Boolean BuffCtrl::<mMorph>k__BackingField
	bool ___U3CmMorphU3Ek__BackingField_91;
	// System.Boolean BuffCtrl::<mAntiControl>k__BackingField
	bool ___U3CmAntiControlU3Ek__BackingField_92;
	// System.Collections.Generic.List`1<System.Int32> BuffCtrl::<mAddDamageSkillIDs>k__BackingField
	List_1_t2522024052 * ___U3CmAddDamageSkillIDsU3Ek__BackingField_93;
	// System.Collections.Generic.List`1<System.Int32> BuffCtrl::<mAddDamageValue>k__BackingField
	List_1_t2522024052 * ___U3CmAddDamageValueU3Ek__BackingField_94;
	// System.Int32 BuffCtrl::<mHpLineBuffID>k__BackingField
	int32_t ___U3CmHpLineBuffIDU3Ek__BackingField_95;
	// System.Int32 BuffCtrl::<mControlBuffTimeBonus>k__BackingField
	int32_t ___U3CmControlBuffTimeBonusU3Ek__BackingField_96;
	// System.Int32 BuffCtrl::<mControlBuffTimeBonusPer>k__BackingField
	int32_t ___U3CmControlBuffTimeBonusPerU3Ek__BackingField_97;
	// System.Collections.Generic.List`1<System.Int32> BuffCtrl::<mAddDamageValuePer>k__BackingField
	List_1_t2522024052 * ___U3CmAddDamageValuePerU3Ek__BackingField_98;
	// System.Int32 BuffCtrl::<mRuneTakeEffectBuffID>k__BackingField
	int32_t ___U3CmRuneTakeEffectBuffIDU3Ek__BackingField_99;
	// System.Int32 BuffCtrl::<mRuneTakeEffectBuffID01>k__BackingField
	int32_t ___U3CmRuneTakeEffectBuffID01U3Ek__BackingField_100;
	// System.Int32 BuffCtrl::<mRuneTakeEffectBuffID02>k__BackingField
	int32_t ___U3CmRuneTakeEffectBuffID02U3Ek__BackingField_101;
	// System.Int32 BuffCtrl::<mStable>k__BackingField
	int32_t ___U3CmStableU3Ek__BackingField_102;
	// System.Int32 BuffCtrl::<mNotInvade>k__BackingField
	int32_t ___U3CmNotInvadeU3Ek__BackingField_103;
	// System.Int32 BuffCtrl::<mUnyielding>k__BackingField
	int32_t ___U3CmUnyieldingU3Ek__BackingField_104;
	// System.Int32 BuffCtrl::<mDiscreteBuffIDTime>k__BackingField
	int32_t ___U3CmDiscreteBuffIDTimeU3Ek__BackingField_105;
	// System.Int32 BuffCtrl::<mChangeNewSkillID>k__BackingField
	int32_t ___U3CmChangeNewSkillIDU3Ek__BackingField_106;
	// System.Int32 BuffCtrl::<mChangeSkillPro>k__BackingField
	int32_t ___U3CmChangeSkillProU3Ek__BackingField_107;
	// System.Int32 BuffCtrl::<mReduceCDSkillID>k__BackingField
	int32_t ___U3CmReduceCDSkillIDU3Ek__BackingField_108;
	// System.Int32 BuffCtrl::<mThumpAddValue>k__BackingField
	int32_t ___U3CmThumpAddValueU3Ek__BackingField_109;
	// System.Int32 BuffCtrl::<mShieldOverSkillID>k__BackingField
	int32_t ___U3CmShieldOverSkillIDU3Ek__BackingField_110;
	// System.Int32 BuffCtrl::<mCleaveSkillID>k__BackingField
	int32_t ___U3CmCleaveSkillIDU3Ek__BackingField_111;
	// System.Int32 BuffCtrl::<mCleaveBuffID>k__BackingField
	int32_t ___U3CmCleaveBuffIDU3Ek__BackingField_112;
	// System.Int32 BuffCtrl::<mHpLineUpBuffID>k__BackingField
	int32_t ___U3CmHpLineUpBuffIDU3Ek__BackingField_113;
	// System.Int32 BuffCtrl::<mBuffCritPer>k__BackingField
	int32_t ___U3CmBuffCritPerU3Ek__BackingField_114;
	// System.Int32 BuffCtrl::<mBuffCritHurt>k__BackingField
	int32_t ___U3CmBuffCritHurtU3Ek__BackingField_115;
	// System.Int32 BuffCtrl::<mAddHpNum>k__BackingField
	int32_t ___U3CmAddHpNumU3Ek__BackingField_116;
	// System.Int32 BuffCtrl::<mHTBuffID>k__BackingField
	int32_t ___U3CmHTBuffIDU3Ek__BackingField_117;
	// System.Int32 BuffCtrl::<mDeathHarvestPer>k__BackingField
	int32_t ___U3CmDeathHarvestPerU3Ek__BackingField_118;
	// System.Int32 BuffCtrl::<mHpLineCleaveAddBuff>k__BackingField
	int32_t ___U3CmHpLineCleaveAddBuffU3Ek__BackingField_119;
	// System.Int32 BuffCtrl::<mResistanceCountryPer>k__BackingField
	int32_t ___U3CmResistanceCountryPerU3Ek__BackingField_120;
	// System.Int32 BuffCtrl::<mNDPAddBuff02>k__BackingField
	int32_t ___U3CmNDPAddBuff02U3Ek__BackingField_121;

public:
	inline static int32_t get_offset_of_lastTime_0() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___lastTime_0)); }
	inline float get_lastTime_0() const { return ___lastTime_0; }
	inline float* get_address_of_lastTime_0() { return &___lastTime_0; }
	inline void set_lastTime_0(float value)
	{
		___lastTime_0 = value;
	}

	inline static int32_t get_offset_of_buffList_1() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___buffList_1)); }
	inline List_1_t1370267459 * get_buffList_1() const { return ___buffList_1; }
	inline List_1_t1370267459 ** get_address_of_buffList_1() { return &___buffList_1; }
	inline void set_buffList_1(List_1_t1370267459 * value)
	{
		___buffList_1 = value;
		Il2CppCodeGenWriteBarrier(&___buffList_1, value);
	}

	inline static int32_t get_offset_of_cacheBuffList_2() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___cacheBuffList_2)); }
	inline List_1_t1370267459 * get_cacheBuffList_2() const { return ___cacheBuffList_2; }
	inline List_1_t1370267459 ** get_address_of_cacheBuffList_2() { return &___cacheBuffList_2; }
	inline void set_cacheBuffList_2(List_1_t1370267459 * value)
	{
		___cacheBuffList_2 = value;
		Il2CppCodeGenWriteBarrier(&___cacheBuffList_2, value);
	}

	inline static int32_t get_offset_of_entity_3() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___entity_3)); }
	inline CombatEntity_t684137495 * get_entity_3() const { return ___entity_3; }
	inline CombatEntity_t684137495 ** get_address_of_entity_3() { return &___entity_3; }
	inline void set_entity_3(CombatEntity_t684137495 * value)
	{
		___entity_3 = value;
		Il2CppCodeGenWriteBarrier(&___entity_3, value);
	}

	inline static int32_t get_offset_of_reEvent_4() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___reEvent_4)); }
	inline RemoveEffect_t3191766869 * get_reEvent_4() const { return ___reEvent_4; }
	inline RemoveEffect_t3191766869 ** get_address_of_reEvent_4() { return &___reEvent_4; }
	inline void set_reEvent_4(RemoveEffect_t3191766869 * value)
	{
		___reEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___reEvent_4, value);
	}

	inline static int32_t get_offset_of_srcCureValue_5() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___srcCureValue_5)); }
	inline float get_srcCureValue_5() const { return ___srcCureValue_5; }
	inline float* get_address_of_srcCureValue_5() { return &___srcCureValue_5; }
	inline void set_srcCureValue_5(float value)
	{
		___srcCureValue_5 = value;
	}

	inline static int32_t get_offset_of_realBuffState_6() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___realBuffState_6)); }
	inline uint64_t get_realBuffState_6() const { return ___realBuffState_6; }
	inline uint64_t* get_address_of_realBuffState_6() { return &___realBuffState_6; }
	inline void set_realBuffState_6(uint64_t value)
	{
		___realBuffState_6 = value;
	}

	inline static int32_t get_offset_of_lastBuffState_7() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___lastBuffState_7)); }
	inline uint64_t get_lastBuffState_7() const { return ___lastBuffState_7; }
	inline uint64_t* get_address_of_lastBuffState_7() { return &___lastBuffState_7; }
	inline void set_lastBuffState_7(uint64_t value)
	{
		___lastBuffState_7 = value;
	}

	inline static int32_t get_offset_of_isAddBuff_8() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___isAddBuff_8)); }
	inline bool get_isAddBuff_8() const { return ___isAddBuff_8; }
	inline bool* get_address_of_isAddBuff_8() { return &___isAddBuff_8; }
	inline void set_isAddBuff_8(bool value)
	{
		___isAddBuff_8 = value;
	}

	inline static int32_t get_offset_of_buffStateList_9() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___buffStateList_9)); }
	inline List_1_t3417094830 * get_buffStateList_9() const { return ___buffStateList_9; }
	inline List_1_t3417094830 ** get_address_of_buffStateList_9() { return &___buffStateList_9; }
	inline void set_buffStateList_9(List_1_t3417094830 * value)
	{
		___buffStateList_9 = value;
		Il2CppCodeGenWriteBarrier(&___buffStateList_9, value);
	}

	inline static int32_t get_offset_of_buffStateIDList_10() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___buffStateIDList_10)); }
	inline List_1_t2522024052 * get_buffStateIDList_10() const { return ___buffStateIDList_10; }
	inline List_1_t2522024052 ** get_address_of_buffStateIDList_10() { return &___buffStateIDList_10; }
	inline void set_buffStateIDList_10(List_1_t2522024052 * value)
	{
		___buffStateIDList_10 = value;
		Il2CppCodeGenWriteBarrier(&___buffStateIDList_10, value);
	}

	inline static int32_t get_offset_of_timeBombBuffState_11() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___timeBombBuffState_11)); }
	inline List_1_t3417094830 * get_timeBombBuffState_11() const { return ___timeBombBuffState_11; }
	inline List_1_t3417094830 ** get_address_of_timeBombBuffState_11() { return &___timeBombBuffState_11; }
	inline void set_timeBombBuffState_11(List_1_t3417094830 * value)
	{
		___timeBombBuffState_11 = value;
		Il2CppCodeGenWriteBarrier(&___timeBombBuffState_11, value);
	}

	inline static int32_t get_offset_of_U3CcurnumU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CcurnumU3Ek__BackingField_12)); }
	inline int32_t get_U3CcurnumU3Ek__BackingField_12() const { return ___U3CcurnumU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CcurnumU3Ek__BackingField_12() { return &___U3CcurnumU3Ek__BackingField_12; }
	inline void set_U3CcurnumU3Ek__BackingField_12(int32_t value)
	{
		___U3CcurnumU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CmAnitNumU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmAnitNumU3Ek__BackingField_13)); }
	inline int32_t get_U3CmAnitNumU3Ek__BackingField_13() const { return ___U3CmAnitNumU3Ek__BackingField_13; }
	inline int32_t* get_address_of_U3CmAnitNumU3Ek__BackingField_13() { return &___U3CmAnitNumU3Ek__BackingField_13; }
	inline void set_U3CmAnitNumU3Ek__BackingField_13(int32_t value)
	{
		___U3CmAnitNumU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CmAuraAddHPRadiusU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmAuraAddHPRadiusU3Ek__BackingField_14)); }
	inline int32_t get_U3CmAuraAddHPRadiusU3Ek__BackingField_14() const { return ___U3CmAuraAddHPRadiusU3Ek__BackingField_14; }
	inline int32_t* get_address_of_U3CmAuraAddHPRadiusU3Ek__BackingField_14() { return &___U3CmAuraAddHPRadiusU3Ek__BackingField_14; }
	inline void set_U3CmAuraAddHPRadiusU3Ek__BackingField_14(int32_t value)
	{
		___U3CmAuraAddHPRadiusU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CmAuraReduceHPRadiusU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmAuraReduceHPRadiusU3Ek__BackingField_15)); }
	inline int32_t get_U3CmAuraReduceHPRadiusU3Ek__BackingField_15() const { return ___U3CmAuraReduceHPRadiusU3Ek__BackingField_15; }
	inline int32_t* get_address_of_U3CmAuraReduceHPRadiusU3Ek__BackingField_15() { return &___U3CmAuraReduceHPRadiusU3Ek__BackingField_15; }
	inline void set_U3CmAuraReduceHPRadiusU3Ek__BackingField_15(int32_t value)
	{
		___U3CmAuraReduceHPRadiusU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CmHpLineNumU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmHpLineNumU3Ek__BackingField_16)); }
	inline int32_t get_U3CmHpLineNumU3Ek__BackingField_16() const { return ___U3CmHpLineNumU3Ek__BackingField_16; }
	inline int32_t* get_address_of_U3CmHpLineNumU3Ek__BackingField_16() { return &___U3CmHpLineNumU3Ek__BackingField_16; }
	inline void set_U3CmHpLineNumU3Ek__BackingField_16(int32_t value)
	{
		___U3CmHpLineNumU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CmRuneTakeEffectNumU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmRuneTakeEffectNumU3Ek__BackingField_17)); }
	inline int32_t get_U3CmRuneTakeEffectNumU3Ek__BackingField_17() const { return ___U3CmRuneTakeEffectNumU3Ek__BackingField_17; }
	inline int32_t* get_address_of_U3CmRuneTakeEffectNumU3Ek__BackingField_17() { return &___U3CmRuneTakeEffectNumU3Ek__BackingField_17; }
	inline void set_U3CmRuneTakeEffectNumU3Ek__BackingField_17(int32_t value)
	{
		___U3CmRuneTakeEffectNumU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CmGodDownTweenTimeU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmGodDownTweenTimeU3Ek__BackingField_18)); }
	inline float get_U3CmGodDownTweenTimeU3Ek__BackingField_18() const { return ___U3CmGodDownTweenTimeU3Ek__BackingField_18; }
	inline float* get_address_of_U3CmGodDownTweenTimeU3Ek__BackingField_18() { return &___U3CmGodDownTweenTimeU3Ek__BackingField_18; }
	inline void set_U3CmGodDownTweenTimeU3Ek__BackingField_18(float value)
	{
		___U3CmGodDownTweenTimeU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CmAntiDebuffDiscreteAddBuffIDU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmAntiDebuffDiscreteAddBuffIDU3Ek__BackingField_19)); }
	inline int32_t get_U3CmAntiDebuffDiscreteAddBuffIDU3Ek__BackingField_19() const { return ___U3CmAntiDebuffDiscreteAddBuffIDU3Ek__BackingField_19; }
	inline int32_t* get_address_of_U3CmAntiDebuffDiscreteAddBuffIDU3Ek__BackingField_19() { return &___U3CmAntiDebuffDiscreteAddBuffIDU3Ek__BackingField_19; }
	inline void set_U3CmAntiDebuffDiscreteAddBuffIDU3Ek__BackingField_19(int32_t value)
	{
		___U3CmAntiDebuffDiscreteAddBuffIDU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CmAntiDebuffDiscreteIDU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmAntiDebuffDiscreteIDU3Ek__BackingField_20)); }
	inline int32_t get_U3CmAntiDebuffDiscreteIDU3Ek__BackingField_20() const { return ___U3CmAntiDebuffDiscreteIDU3Ek__BackingField_20; }
	inline int32_t* get_address_of_U3CmAntiDebuffDiscreteIDU3Ek__BackingField_20() { return &___U3CmAntiDebuffDiscreteIDU3Ek__BackingField_20; }
	inline void set_U3CmAntiDebuffDiscreteIDU3Ek__BackingField_20(int32_t value)
	{
		___U3CmAntiDebuffDiscreteIDU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CmPetrifiedChangeTimeU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmPetrifiedChangeTimeU3Ek__BackingField_21)); }
	inline float get_U3CmPetrifiedChangeTimeU3Ek__BackingField_21() const { return ___U3CmPetrifiedChangeTimeU3Ek__BackingField_21; }
	inline float* get_address_of_U3CmPetrifiedChangeTimeU3Ek__BackingField_21() { return &___U3CmPetrifiedChangeTimeU3Ek__BackingField_21; }
	inline void set_U3CmPetrifiedChangeTimeU3Ek__BackingField_21(float value)
	{
		___U3CmPetrifiedChangeTimeU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CmChangeOldSkillIDU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmChangeOldSkillIDU3Ek__BackingField_22)); }
	inline int32_t get_U3CmChangeOldSkillIDU3Ek__BackingField_22() const { return ___U3CmChangeOldSkillIDU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3CmChangeOldSkillIDU3Ek__BackingField_22() { return &___U3CmChangeOldSkillIDU3Ek__BackingField_22; }
	inline void set_U3CmChangeOldSkillIDU3Ek__BackingField_22(int32_t value)
	{
		___U3CmChangeOldSkillIDU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CmWhenCritAddBuffIDU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmWhenCritAddBuffIDU3Ek__BackingField_23)); }
	inline int32_t get_U3CmWhenCritAddBuffIDU3Ek__BackingField_23() const { return ___U3CmWhenCritAddBuffIDU3Ek__BackingField_23; }
	inline int32_t* get_address_of_U3CmWhenCritAddBuffIDU3Ek__BackingField_23() { return &___U3CmWhenCritAddBuffIDU3Ek__BackingField_23; }
	inline void set_U3CmWhenCritAddBuffIDU3Ek__BackingField_23(int32_t value)
	{
		___U3CmWhenCritAddBuffIDU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CmThumpSkillIDU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmThumpSkillIDU3Ek__BackingField_24)); }
	inline int32_t get_U3CmThumpSkillIDU3Ek__BackingField_24() const { return ___U3CmThumpSkillIDU3Ek__BackingField_24; }
	inline int32_t* get_address_of_U3CmThumpSkillIDU3Ek__BackingField_24() { return &___U3CmThumpSkillIDU3Ek__BackingField_24; }
	inline void set_U3CmThumpSkillIDU3Ek__BackingField_24(int32_t value)
	{
		___U3CmThumpSkillIDU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CmTankBuffIDU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmTankBuffIDU3Ek__BackingField_25)); }
	inline int32_t get_U3CmTankBuffIDU3Ek__BackingField_25() const { return ___U3CmTankBuffIDU3Ek__BackingField_25; }
	inline int32_t* get_address_of_U3CmTankBuffIDU3Ek__BackingField_25() { return &___U3CmTankBuffIDU3Ek__BackingField_25; }
	inline void set_U3CmTankBuffIDU3Ek__BackingField_25(int32_t value)
	{
		___U3CmTankBuffIDU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CmReduceRageHpNumU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmReduceRageHpNumU3Ek__BackingField_26)); }
	inline int32_t get_U3CmReduceRageHpNumU3Ek__BackingField_26() const { return ___U3CmReduceRageHpNumU3Ek__BackingField_26; }
	inline int32_t* get_address_of_U3CmReduceRageHpNumU3Ek__BackingField_26() { return &___U3CmReduceRageHpNumU3Ek__BackingField_26; }
	inline void set_U3CmReduceRageHpNumU3Ek__BackingField_26(int32_t value)
	{
		___U3CmReduceRageHpNumU3Ek__BackingField_26 = value;
	}

	inline static int32_t get_offset_of_U3CmGhostIndexU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmGhostIndexU3Ek__BackingField_27)); }
	inline int32_t get_U3CmGhostIndexU3Ek__BackingField_27() const { return ___U3CmGhostIndexU3Ek__BackingField_27; }
	inline int32_t* get_address_of_U3CmGhostIndexU3Ek__BackingField_27() { return &___U3CmGhostIndexU3Ek__BackingField_27; }
	inline void set_U3CmGhostIndexU3Ek__BackingField_27(int32_t value)
	{
		___U3CmGhostIndexU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3CmSusanooIndexU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmSusanooIndexU3Ek__BackingField_28)); }
	inline int32_t get_U3CmSusanooIndexU3Ek__BackingField_28() const { return ___U3CmSusanooIndexU3Ek__BackingField_28; }
	inline int32_t* get_address_of_U3CmSusanooIndexU3Ek__BackingField_28() { return &___U3CmSusanooIndexU3Ek__BackingField_28; }
	inline void set_U3CmSusanooIndexU3Ek__BackingField_28(int32_t value)
	{
		___U3CmSusanooIndexU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CmDistorTionDelayTimeU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmDistorTionDelayTimeU3Ek__BackingField_29)); }
	inline float get_U3CmDistorTionDelayTimeU3Ek__BackingField_29() const { return ___U3CmDistorTionDelayTimeU3Ek__BackingField_29; }
	inline float* get_address_of_U3CmDistorTionDelayTimeU3Ek__BackingField_29() { return &___U3CmDistorTionDelayTimeU3Ek__BackingField_29; }
	inline void set_U3CmDistorTionDelayTimeU3Ek__BackingField_29(float value)
	{
		___U3CmDistorTionDelayTimeU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CmDeathHarvestLineU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmDeathHarvestLineU3Ek__BackingField_30)); }
	inline int32_t get_U3CmDeathHarvestLineU3Ek__BackingField_30() const { return ___U3CmDeathHarvestLineU3Ek__BackingField_30; }
	inline int32_t* get_address_of_U3CmDeathHarvestLineU3Ek__BackingField_30() { return &___U3CmDeathHarvestLineU3Ek__BackingField_30; }
	inline void set_U3CmDeathHarvestLineU3Ek__BackingField_30(int32_t value)
	{
		___U3CmDeathHarvestLineU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CmResistanceCountryTypeU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmResistanceCountryTypeU3Ek__BackingField_31)); }
	inline int32_t get_U3CmResistanceCountryTypeU3Ek__BackingField_31() const { return ___U3CmResistanceCountryTypeU3Ek__BackingField_31; }
	inline int32_t* get_address_of_U3CmResistanceCountryTypeU3Ek__BackingField_31() { return &___U3CmResistanceCountryTypeU3Ek__BackingField_31; }
	inline void set_U3CmResistanceCountryTypeU3Ek__BackingField_31(int32_t value)
	{
		___U3CmResistanceCountryTypeU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CmTauntDisU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmTauntDisU3Ek__BackingField_32)); }
	inline int32_t get_U3CmTauntDisU3Ek__BackingField_32() const { return ___U3CmTauntDisU3Ek__BackingField_32; }
	inline int32_t* get_address_of_U3CmTauntDisU3Ek__BackingField_32() { return &___U3CmTauntDisU3Ek__BackingField_32; }
	inline void set_U3CmTauntDisU3Ek__BackingField_32(int32_t value)
	{
		___U3CmTauntDisU3Ek__BackingField_32 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffATNumU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffATNumU3Ek__BackingField_33)); }
	inline int32_t get_U3CmBuffATNumU3Ek__BackingField_33() const { return ___U3CmBuffATNumU3Ek__BackingField_33; }
	inline int32_t* get_address_of_U3CmBuffATNumU3Ek__BackingField_33() { return &___U3CmBuffATNumU3Ek__BackingField_33; }
	inline void set_U3CmBuffATNumU3Ek__BackingField_33(int32_t value)
	{
		___U3CmBuffATNumU3Ek__BackingField_33 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffPDNumU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffPDNumU3Ek__BackingField_34)); }
	inline int32_t get_U3CmBuffPDNumU3Ek__BackingField_34() const { return ___U3CmBuffPDNumU3Ek__BackingField_34; }
	inline int32_t* get_address_of_U3CmBuffPDNumU3Ek__BackingField_34() { return &___U3CmBuffPDNumU3Ek__BackingField_34; }
	inline void set_U3CmBuffPDNumU3Ek__BackingField_34(int32_t value)
	{
		___U3CmBuffPDNumU3Ek__BackingField_34 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffMDNumU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffMDNumU3Ek__BackingField_35)); }
	inline int32_t get_U3CmBuffMDNumU3Ek__BackingField_35() const { return ___U3CmBuffMDNumU3Ek__BackingField_35; }
	inline int32_t* get_address_of_U3CmBuffMDNumU3Ek__BackingField_35() { return &___U3CmBuffMDNumU3Ek__BackingField_35; }
	inline void set_U3CmBuffMDNumU3Ek__BackingField_35(int32_t value)
	{
		___U3CmBuffMDNumU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffMaxHPNumU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffMaxHPNumU3Ek__BackingField_36)); }
	inline int32_t get_U3CmBuffMaxHPNumU3Ek__BackingField_36() const { return ___U3CmBuffMaxHPNumU3Ek__BackingField_36; }
	inline int32_t* get_address_of_U3CmBuffMaxHPNumU3Ek__BackingField_36() { return &___U3CmBuffMaxHPNumU3Ek__BackingField_36; }
	inline void set_U3CmBuffMaxHPNumU3Ek__BackingField_36(int32_t value)
	{
		___U3CmBuffMaxHPNumU3Ek__BackingField_36 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffHitsNumU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffHitsNumU3Ek__BackingField_37)); }
	inline int32_t get_U3CmBuffHitsNumU3Ek__BackingField_37() const { return ___U3CmBuffHitsNumU3Ek__BackingField_37; }
	inline int32_t* get_address_of_U3CmBuffHitsNumU3Ek__BackingField_37() { return &___U3CmBuffHitsNumU3Ek__BackingField_37; }
	inline void set_U3CmBuffHitsNumU3Ek__BackingField_37(int32_t value)
	{
		___U3CmBuffHitsNumU3Ek__BackingField_37 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffDodgeNumU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffDodgeNumU3Ek__BackingField_38)); }
	inline int32_t get_U3CmBuffDodgeNumU3Ek__BackingField_38() const { return ___U3CmBuffDodgeNumU3Ek__BackingField_38; }
	inline int32_t* get_address_of_U3CmBuffDodgeNumU3Ek__BackingField_38() { return &___U3CmBuffDodgeNumU3Ek__BackingField_38; }
	inline void set_U3CmBuffDodgeNumU3Ek__BackingField_38(int32_t value)
	{
		___U3CmBuffDodgeNumU3Ek__BackingField_38 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffWreckNumU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffWreckNumU3Ek__BackingField_39)); }
	inline int32_t get_U3CmBuffWreckNumU3Ek__BackingField_39() const { return ___U3CmBuffWreckNumU3Ek__BackingField_39; }
	inline int32_t* get_address_of_U3CmBuffWreckNumU3Ek__BackingField_39() { return &___U3CmBuffWreckNumU3Ek__BackingField_39; }
	inline void set_U3CmBuffWreckNumU3Ek__BackingField_39(int32_t value)
	{
		___U3CmBuffWreckNumU3Ek__BackingField_39 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffWithstandNumU3Ek__BackingField_40() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffWithstandNumU3Ek__BackingField_40)); }
	inline int32_t get_U3CmBuffWithstandNumU3Ek__BackingField_40() const { return ___U3CmBuffWithstandNumU3Ek__BackingField_40; }
	inline int32_t* get_address_of_U3CmBuffWithstandNumU3Ek__BackingField_40() { return &___U3CmBuffWithstandNumU3Ek__BackingField_40; }
	inline void set_U3CmBuffWithstandNumU3Ek__BackingField_40(int32_t value)
	{
		___U3CmBuffWithstandNumU3Ek__BackingField_40 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffCritNumU3Ek__BackingField_41() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffCritNumU3Ek__BackingField_41)); }
	inline int32_t get_U3CmBuffCritNumU3Ek__BackingField_41() const { return ___U3CmBuffCritNumU3Ek__BackingField_41; }
	inline int32_t* get_address_of_U3CmBuffCritNumU3Ek__BackingField_41() { return &___U3CmBuffCritNumU3Ek__BackingField_41; }
	inline void set_U3CmBuffCritNumU3Ek__BackingField_41(int32_t value)
	{
		___U3CmBuffCritNumU3Ek__BackingField_41 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffTenacityNumU3Ek__BackingField_42() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffTenacityNumU3Ek__BackingField_42)); }
	inline int32_t get_U3CmBuffTenacityNumU3Ek__BackingField_42() const { return ___U3CmBuffTenacityNumU3Ek__BackingField_42; }
	inline int32_t* get_address_of_U3CmBuffTenacityNumU3Ek__BackingField_42() { return &___U3CmBuffTenacityNumU3Ek__BackingField_42; }
	inline void set_U3CmBuffTenacityNumU3Ek__BackingField_42(int32_t value)
	{
		___U3CmBuffTenacityNumU3Ek__BackingField_42 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffRHurtNumU3Ek__BackingField_43() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffRHurtNumU3Ek__BackingField_43)); }
	inline uint32_t get_U3CmBuffRHurtNumU3Ek__BackingField_43() const { return ___U3CmBuffRHurtNumU3Ek__BackingField_43; }
	inline uint32_t* get_address_of_U3CmBuffRHurtNumU3Ek__BackingField_43() { return &___U3CmBuffRHurtNumU3Ek__BackingField_43; }
	inline void set_U3CmBuffRHurtNumU3Ek__BackingField_43(uint32_t value)
	{
		___U3CmBuffRHurtNumU3Ek__BackingField_43 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffATU3Ek__BackingField_44() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffATU3Ek__BackingField_44)); }
	inline int32_t get_U3CmBuffATU3Ek__BackingField_44() const { return ___U3CmBuffATU3Ek__BackingField_44; }
	inline int32_t* get_address_of_U3CmBuffATU3Ek__BackingField_44() { return &___U3CmBuffATU3Ek__BackingField_44; }
	inline void set_U3CmBuffATU3Ek__BackingField_44(int32_t value)
	{
		___U3CmBuffATU3Ek__BackingField_44 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffPDU3Ek__BackingField_45() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffPDU3Ek__BackingField_45)); }
	inline int32_t get_U3CmBuffPDU3Ek__BackingField_45() const { return ___U3CmBuffPDU3Ek__BackingField_45; }
	inline int32_t* get_address_of_U3CmBuffPDU3Ek__BackingField_45() { return &___U3CmBuffPDU3Ek__BackingField_45; }
	inline void set_U3CmBuffPDU3Ek__BackingField_45(int32_t value)
	{
		___U3CmBuffPDU3Ek__BackingField_45 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffMDU3Ek__BackingField_46() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffMDU3Ek__BackingField_46)); }
	inline int32_t get_U3CmBuffMDU3Ek__BackingField_46() const { return ___U3CmBuffMDU3Ek__BackingField_46; }
	inline int32_t* get_address_of_U3CmBuffMDU3Ek__BackingField_46() { return &___U3CmBuffMDU3Ek__BackingField_46; }
	inline void set_U3CmBuffMDU3Ek__BackingField_46(int32_t value)
	{
		___U3CmBuffMDU3Ek__BackingField_46 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffMaxHPU3Ek__BackingField_47() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffMaxHPU3Ek__BackingField_47)); }
	inline int32_t get_U3CmBuffMaxHPU3Ek__BackingField_47() const { return ___U3CmBuffMaxHPU3Ek__BackingField_47; }
	inline int32_t* get_address_of_U3CmBuffMaxHPU3Ek__BackingField_47() { return &___U3CmBuffMaxHPU3Ek__BackingField_47; }
	inline void set_U3CmBuffMaxHPU3Ek__BackingField_47(int32_t value)
	{
		___U3CmBuffMaxHPU3Ek__BackingField_47 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffHitsU3Ek__BackingField_48() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffHitsU3Ek__BackingField_48)); }
	inline int32_t get_U3CmBuffHitsU3Ek__BackingField_48() const { return ___U3CmBuffHitsU3Ek__BackingField_48; }
	inline int32_t* get_address_of_U3CmBuffHitsU3Ek__BackingField_48() { return &___U3CmBuffHitsU3Ek__BackingField_48; }
	inline void set_U3CmBuffHitsU3Ek__BackingField_48(int32_t value)
	{
		___U3CmBuffHitsU3Ek__BackingField_48 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffDodgeU3Ek__BackingField_49() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffDodgeU3Ek__BackingField_49)); }
	inline int32_t get_U3CmBuffDodgeU3Ek__BackingField_49() const { return ___U3CmBuffDodgeU3Ek__BackingField_49; }
	inline int32_t* get_address_of_U3CmBuffDodgeU3Ek__BackingField_49() { return &___U3CmBuffDodgeU3Ek__BackingField_49; }
	inline void set_U3CmBuffDodgeU3Ek__BackingField_49(int32_t value)
	{
		___U3CmBuffDodgeU3Ek__BackingField_49 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffWreckU3Ek__BackingField_50() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffWreckU3Ek__BackingField_50)); }
	inline int32_t get_U3CmBuffWreckU3Ek__BackingField_50() const { return ___U3CmBuffWreckU3Ek__BackingField_50; }
	inline int32_t* get_address_of_U3CmBuffWreckU3Ek__BackingField_50() { return &___U3CmBuffWreckU3Ek__BackingField_50; }
	inline void set_U3CmBuffWreckU3Ek__BackingField_50(int32_t value)
	{
		___U3CmBuffWreckU3Ek__BackingField_50 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffWithstandU3Ek__BackingField_51() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffWithstandU3Ek__BackingField_51)); }
	inline int32_t get_U3CmBuffWithstandU3Ek__BackingField_51() const { return ___U3CmBuffWithstandU3Ek__BackingField_51; }
	inline int32_t* get_address_of_U3CmBuffWithstandU3Ek__BackingField_51() { return &___U3CmBuffWithstandU3Ek__BackingField_51; }
	inline void set_U3CmBuffWithstandU3Ek__BackingField_51(int32_t value)
	{
		___U3CmBuffWithstandU3Ek__BackingField_51 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffCritU3Ek__BackingField_52() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffCritU3Ek__BackingField_52)); }
	inline int32_t get_U3CmBuffCritU3Ek__BackingField_52() const { return ___U3CmBuffCritU3Ek__BackingField_52; }
	inline int32_t* get_address_of_U3CmBuffCritU3Ek__BackingField_52() { return &___U3CmBuffCritU3Ek__BackingField_52; }
	inline void set_U3CmBuffCritU3Ek__BackingField_52(int32_t value)
	{
		___U3CmBuffCritU3Ek__BackingField_52 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffTenacityU3Ek__BackingField_53() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffTenacityU3Ek__BackingField_53)); }
	inline int32_t get_U3CmBuffTenacityU3Ek__BackingField_53() const { return ___U3CmBuffTenacityU3Ek__BackingField_53; }
	inline int32_t* get_address_of_U3CmBuffTenacityU3Ek__BackingField_53() { return &___U3CmBuffTenacityU3Ek__BackingField_53; }
	inline void set_U3CmBuffTenacityU3Ek__BackingField_53(int32_t value)
	{
		___U3CmBuffTenacityU3Ek__BackingField_53 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffRHurtU3Ek__BackingField_54() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffRHurtU3Ek__BackingField_54)); }
	inline uint32_t get_U3CmBuffRHurtU3Ek__BackingField_54() const { return ___U3CmBuffRHurtU3Ek__BackingField_54; }
	inline uint32_t* get_address_of_U3CmBuffRHurtU3Ek__BackingField_54() { return &___U3CmBuffRHurtU3Ek__BackingField_54; }
	inline void set_U3CmBuffRHurtU3Ek__BackingField_54(uint32_t value)
	{
		___U3CmBuffRHurtU3Ek__BackingField_54 = value;
	}

	inline static int32_t get_offset_of_U3CmHPSecDecPerU3Ek__BackingField_55() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmHPSecDecPerU3Ek__BackingField_55)); }
	inline float get_U3CmHPSecDecPerU3Ek__BackingField_55() const { return ___U3CmHPSecDecPerU3Ek__BackingField_55; }
	inline float* get_address_of_U3CmHPSecDecPerU3Ek__BackingField_55() { return &___U3CmHPSecDecPerU3Ek__BackingField_55; }
	inline void set_U3CmHPSecDecPerU3Ek__BackingField_55(float value)
	{
		___U3CmHPSecDecPerU3Ek__BackingField_55 = value;
	}

	inline static int32_t get_offset_of_U3CmHPSecDecNumU3Ek__BackingField_56() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmHPSecDecNumU3Ek__BackingField_56)); }
	inline float get_U3CmHPSecDecNumU3Ek__BackingField_56() const { return ___U3CmHPSecDecNumU3Ek__BackingField_56; }
	inline float* get_address_of_U3CmHPSecDecNumU3Ek__BackingField_56() { return &___U3CmHPSecDecNumU3Ek__BackingField_56; }
	inline void set_U3CmHPSecDecNumU3Ek__BackingField_56(float value)
	{
		___U3CmHPSecDecNumU3Ek__BackingField_56 = value;
	}

	inline static int32_t get_offset_of_U3CmHPRstNumU3Ek__BackingField_57() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmHPRstNumU3Ek__BackingField_57)); }
	inline float get_U3CmHPRstNumU3Ek__BackingField_57() const { return ___U3CmHPRstNumU3Ek__BackingField_57; }
	inline float* get_address_of_U3CmHPRstNumU3Ek__BackingField_57() { return &___U3CmHPRstNumU3Ek__BackingField_57; }
	inline void set_U3CmHPRstNumU3Ek__BackingField_57(float value)
	{
		___U3CmHPRstNumU3Ek__BackingField_57 = value;
	}

	inline static int32_t get_offset_of_U3CmHPRstPerU3Ek__BackingField_58() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmHPRstPerU3Ek__BackingField_58)); }
	inline float get_U3CmHPRstPerU3Ek__BackingField_58() const { return ___U3CmHPRstPerU3Ek__BackingField_58; }
	inline float* get_address_of_U3CmHPRstPerU3Ek__BackingField_58() { return &___U3CmHPRstPerU3Ek__BackingField_58; }
	inline void set_U3CmHPRstPerU3Ek__BackingField_58(float value)
	{
		___U3CmHPRstPerU3Ek__BackingField_58 = value;
	}

	inline static int32_t get_offset_of_U3CmAttackSpeedU3Ek__BackingField_59() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmAttackSpeedU3Ek__BackingField_59)); }
	inline float get_U3CmAttackSpeedU3Ek__BackingField_59() const { return ___U3CmAttackSpeedU3Ek__BackingField_59; }
	inline float* get_address_of_U3CmAttackSpeedU3Ek__BackingField_59() { return &___U3CmAttackSpeedU3Ek__BackingField_59; }
	inline void set_U3CmAttackSpeedU3Ek__BackingField_59(float value)
	{
		___U3CmAttackSpeedU3Ek__BackingField_59 = value;
	}

	inline static int32_t get_offset_of_U3CmModelScaleU3Ek__BackingField_60() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmModelScaleU3Ek__BackingField_60)); }
	inline float get_U3CmModelScaleU3Ek__BackingField_60() const { return ___U3CmModelScaleU3Ek__BackingField_60; }
	inline float* get_address_of_U3CmModelScaleU3Ek__BackingField_60() { return &___U3CmModelScaleU3Ek__BackingField_60; }
	inline void set_U3CmModelScaleU3Ek__BackingField_60(float value)
	{
		___U3CmModelScaleU3Ek__BackingField_60 = value;
	}

	inline static int32_t get_offset_of_U3CmTreatmentEffectU3Ek__BackingField_61() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmTreatmentEffectU3Ek__BackingField_61)); }
	inline float get_U3CmTreatmentEffectU3Ek__BackingField_61() const { return ___U3CmTreatmentEffectU3Ek__BackingField_61; }
	inline float* get_address_of_U3CmTreatmentEffectU3Ek__BackingField_61() { return &___U3CmTreatmentEffectU3Ek__BackingField_61; }
	inline void set_U3CmTreatmentEffectU3Ek__BackingField_61(float value)
	{
		___U3CmTreatmentEffectU3Ek__BackingField_61 = value;
	}

	inline static int32_t get_offset_of_U3CmHurtEffectPlusU3Ek__BackingField_62() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmHurtEffectPlusU3Ek__BackingField_62)); }
	inline float get_U3CmHurtEffectPlusU3Ek__BackingField_62() const { return ___U3CmHurtEffectPlusU3Ek__BackingField_62; }
	inline float* get_address_of_U3CmHurtEffectPlusU3Ek__BackingField_62() { return &___U3CmHurtEffectPlusU3Ek__BackingField_62; }
	inline void set_U3CmHurtEffectPlusU3Ek__BackingField_62(float value)
	{
		___U3CmHurtEffectPlusU3Ek__BackingField_62 = value;
	}

	inline static int32_t get_offset_of_U3CmHurtEffectReductionU3Ek__BackingField_63() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmHurtEffectReductionU3Ek__BackingField_63)); }
	inline float get_U3CmHurtEffectReductionU3Ek__BackingField_63() const { return ___U3CmHurtEffectReductionU3Ek__BackingField_63; }
	inline float* get_address_of_U3CmHurtEffectReductionU3Ek__BackingField_63() { return &___U3CmHurtEffectReductionU3Ek__BackingField_63; }
	inline void set_U3CmHurtEffectReductionU3Ek__BackingField_63(float value)
	{
		___U3CmHurtEffectReductionU3Ek__BackingField_63 = value;
	}

	inline static int32_t get_offset_of_U3CmVampireU3Ek__BackingField_64() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmVampireU3Ek__BackingField_64)); }
	inline float get_U3CmVampireU3Ek__BackingField_64() const { return ___U3CmVampireU3Ek__BackingField_64; }
	inline float* get_address_of_U3CmVampireU3Ek__BackingField_64() { return &___U3CmVampireU3Ek__BackingField_64; }
	inline void set_U3CmVampireU3Ek__BackingField_64(float value)
	{
		___U3CmVampireU3Ek__BackingField_64 = value;
	}

	inline static int32_t get_offset_of_U3CmAngerSecDecNumU3Ek__BackingField_65() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmAngerSecDecNumU3Ek__BackingField_65)); }
	inline int32_t get_U3CmAngerSecDecNumU3Ek__BackingField_65() const { return ___U3CmAngerSecDecNumU3Ek__BackingField_65; }
	inline int32_t* get_address_of_U3CmAngerSecDecNumU3Ek__BackingField_65() { return &___U3CmAngerSecDecNumU3Ek__BackingField_65; }
	inline void set_U3CmAngerSecDecNumU3Ek__BackingField_65(int32_t value)
	{
		___U3CmAngerSecDecNumU3Ek__BackingField_65 = value;
	}

	inline static int32_t get_offset_of_U3CmRunspeedU3Ek__BackingField_66() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmRunspeedU3Ek__BackingField_66)); }
	inline float get_U3CmRunspeedU3Ek__BackingField_66() const { return ___U3CmRunspeedU3Ek__BackingField_66; }
	inline float* get_address_of_U3CmRunspeedU3Ek__BackingField_66() { return &___U3CmRunspeedU3Ek__BackingField_66; }
	inline void set_U3CmRunspeedU3Ek__BackingField_66(float value)
	{
		___U3CmRunspeedU3Ek__BackingField_66 = value;
	}

	inline static int32_t get_offset_of_U3CmHidePartU3Ek__BackingField_67() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmHidePartU3Ek__BackingField_67)); }
	inline int32_t get_U3CmHidePartU3Ek__BackingField_67() const { return ___U3CmHidePartU3Ek__BackingField_67; }
	inline int32_t* get_address_of_U3CmHidePartU3Ek__BackingField_67() { return &___U3CmHidePartU3Ek__BackingField_67; }
	inline void set_U3CmHidePartU3Ek__BackingField_67(int32_t value)
	{
		___U3CmHidePartU3Ek__BackingField_67 = value;
	}

	inline static int32_t get_offset_of_U3CmHPSecDecNumATKU3Ek__BackingField_68() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmHPSecDecNumATKU3Ek__BackingField_68)); }
	inline float get_U3CmHPSecDecNumATKU3Ek__BackingField_68() const { return ___U3CmHPSecDecNumATKU3Ek__BackingField_68; }
	inline float* get_address_of_U3CmHPSecDecNumATKU3Ek__BackingField_68() { return &___U3CmHPSecDecNumATKU3Ek__BackingField_68; }
	inline void set_U3CmHPSecDecNumATKU3Ek__BackingField_68(float value)
	{
		___U3CmHPSecDecNumATKU3Ek__BackingField_68 = value;
	}

	inline static int32_t get_offset_of_U3CmHPRstNumATKU3Ek__BackingField_69() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmHPRstNumATKU3Ek__BackingField_69)); }
	inline float get_U3CmHPRstNumATKU3Ek__BackingField_69() const { return ___U3CmHPRstNumATKU3Ek__BackingField_69; }
	inline float* get_address_of_U3CmHPRstNumATKU3Ek__BackingField_69() { return &___U3CmHPRstNumATKU3Ek__BackingField_69; }
	inline void set_U3CmHPRstNumATKU3Ek__BackingField_69(float value)
	{
		___U3CmHPRstNumATKU3Ek__BackingField_69 = value;
	}

	inline static int32_t get_offset_of_U3CmRestrintCountryTypeU3Ek__BackingField_70() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmRestrintCountryTypeU3Ek__BackingField_70)); }
	inline int32_t get_U3CmRestrintCountryTypeU3Ek__BackingField_70() const { return ___U3CmRestrintCountryTypeU3Ek__BackingField_70; }
	inline int32_t* get_address_of_U3CmRestrintCountryTypeU3Ek__BackingField_70() { return &___U3CmRestrintCountryTypeU3Ek__BackingField_70; }
	inline void set_U3CmRestrintCountryTypeU3Ek__BackingField_70(int32_t value)
	{
		___U3CmRestrintCountryTypeU3Ek__BackingField_70 = value;
	}

	inline static int32_t get_offset_of_U3CmRestrintPerU3Ek__BackingField_71() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmRestrintPerU3Ek__BackingField_71)); }
	inline float get_U3CmRestrintPerU3Ek__BackingField_71() const { return ___U3CmRestrintPerU3Ek__BackingField_71; }
	inline float* get_address_of_U3CmRestrintPerU3Ek__BackingField_71() { return &___U3CmRestrintPerU3Ek__BackingField_71; }
	inline void set_U3CmRestrintPerU3Ek__BackingField_71(float value)
	{
		___U3CmRestrintPerU3Ek__BackingField_71 = value;
	}

	inline static int32_t get_offset_of_U3CmRestrintStateTypeU3Ek__BackingField_72() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmRestrintStateTypeU3Ek__BackingField_72)); }
	inline int32_t get_U3CmRestrintStateTypeU3Ek__BackingField_72() const { return ___U3CmRestrintStateTypeU3Ek__BackingField_72; }
	inline int32_t* get_address_of_U3CmRestrintStateTypeU3Ek__BackingField_72() { return &___U3CmRestrintStateTypeU3Ek__BackingField_72; }
	inline void set_U3CmRestrintStateTypeU3Ek__BackingField_72(int32_t value)
	{
		___U3CmRestrintStateTypeU3Ek__BackingField_72 = value;
	}

	inline static int32_t get_offset_of_U3CmRestrintStatePerU3Ek__BackingField_73() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmRestrintStatePerU3Ek__BackingField_73)); }
	inline float get_U3CmRestrintStatePerU3Ek__BackingField_73() const { return ___U3CmRestrintStatePerU3Ek__BackingField_73; }
	inline float* get_address_of_U3CmRestrintStatePerU3Ek__BackingField_73() { return &___U3CmRestrintStatePerU3Ek__BackingField_73; }
	inline void set_U3CmRestrintStatePerU3Ek__BackingField_73(float value)
	{
		___U3CmRestrintStatePerU3Ek__BackingField_73 = value;
	}

	inline static int32_t get_offset_of_U3CmAtkPerHpU3Ek__BackingField_74() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmAtkPerHpU3Ek__BackingField_74)); }
	inline int32_t get_U3CmAtkPerHpU3Ek__BackingField_74() const { return ___U3CmAtkPerHpU3Ek__BackingField_74; }
	inline int32_t* get_address_of_U3CmAtkPerHpU3Ek__BackingField_74() { return &___U3CmAtkPerHpU3Ek__BackingField_74; }
	inline void set_U3CmAtkPerHpU3Ek__BackingField_74(int32_t value)
	{
		___U3CmAtkPerHpU3Ek__BackingField_74 = value;
	}

	inline static int32_t get_offset_of_U3CmAtkNumHpU3Ek__BackingField_75() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmAtkNumHpU3Ek__BackingField_75)); }
	inline int32_t get_U3CmAtkNumHpU3Ek__BackingField_75() const { return ___U3CmAtkNumHpU3Ek__BackingField_75; }
	inline int32_t* get_address_of_U3CmAtkNumHpU3Ek__BackingField_75() { return &___U3CmAtkNumHpU3Ek__BackingField_75; }
	inline void set_U3CmAtkNumHpU3Ek__BackingField_75(int32_t value)
	{
		___U3CmAtkNumHpU3Ek__BackingField_75 = value;
	}

	inline static int32_t get_offset_of_U3CmNDPAddBuffU3Ek__BackingField_76() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmNDPAddBuffU3Ek__BackingField_76)); }
	inline int32_t get_U3CmNDPAddBuffU3Ek__BackingField_76() const { return ___U3CmNDPAddBuffU3Ek__BackingField_76; }
	inline int32_t* get_address_of_U3CmNDPAddBuffU3Ek__BackingField_76() { return &___U3CmNDPAddBuffU3Ek__BackingField_76; }
	inline void set_U3CmNDPAddBuffU3Ek__BackingField_76(int32_t value)
	{
		___U3CmNDPAddBuffU3Ek__BackingField_76 = value;
	}

	inline static int32_t get_offset_of_U3CmBeAttackedLaterSkillIDU3Ek__BackingField_77() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBeAttackedLaterSkillIDU3Ek__BackingField_77)); }
	inline int32_t get_U3CmBeAttackedLaterSkillIDU3Ek__BackingField_77() const { return ___U3CmBeAttackedLaterSkillIDU3Ek__BackingField_77; }
	inline int32_t* get_address_of_U3CmBeAttackedLaterSkillIDU3Ek__BackingField_77() { return &___U3CmBeAttackedLaterSkillIDU3Ek__BackingField_77; }
	inline void set_U3CmBeAttackedLaterSkillIDU3Ek__BackingField_77(int32_t value)
	{
		___U3CmBeAttackedLaterSkillIDU3Ek__BackingField_77 = value;
	}

	inline static int32_t get_offset_of_U3CmBeAttackedLaterEffectIDU3Ek__BackingField_78() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBeAttackedLaterEffectIDU3Ek__BackingField_78)); }
	inline int32_t get_U3CmBeAttackedLaterEffectIDU3Ek__BackingField_78() const { return ___U3CmBeAttackedLaterEffectIDU3Ek__BackingField_78; }
	inline int32_t* get_address_of_U3CmBeAttackedLaterEffectIDU3Ek__BackingField_78() { return &___U3CmBeAttackedLaterEffectIDU3Ek__BackingField_78; }
	inline void set_U3CmBeAttackedLaterEffectIDU3Ek__BackingField_78(int32_t value)
	{
		___U3CmBeAttackedLaterEffectIDU3Ek__BackingField_78 = value;
	}

	inline static int32_t get_offset_of_U3CmDeathLaterEffectIDU3Ek__BackingField_79() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmDeathLaterEffectIDU3Ek__BackingField_79)); }
	inline int32_t get_U3CmDeathLaterEffectIDU3Ek__BackingField_79() const { return ___U3CmDeathLaterEffectIDU3Ek__BackingField_79; }
	inline int32_t* get_address_of_U3CmDeathLaterEffectIDU3Ek__BackingField_79() { return &___U3CmDeathLaterEffectIDU3Ek__BackingField_79; }
	inline void set_U3CmDeathLaterEffectIDU3Ek__BackingField_79(int32_t value)
	{
		___U3CmDeathLaterEffectIDU3Ek__BackingField_79 = value;
	}

	inline static int32_t get_offset_of_U3CmDeathLaterHeroIDU3Ek__BackingField_80() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmDeathLaterHeroIDU3Ek__BackingField_80)); }
	inline int32_t get_U3CmDeathLaterHeroIDU3Ek__BackingField_80() const { return ___U3CmDeathLaterHeroIDU3Ek__BackingField_80; }
	inline int32_t* get_address_of_U3CmDeathLaterHeroIDU3Ek__BackingField_80() { return &___U3CmDeathLaterHeroIDU3Ek__BackingField_80; }
	inline void set_U3CmDeathLaterHeroIDU3Ek__BackingField_80(int32_t value)
	{
		___U3CmDeathLaterHeroIDU3Ek__BackingField_80 = value;
	}

	inline static int32_t get_offset_of_U3CmBeattackedLaterSpeedU3Ek__BackingField_81() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBeattackedLaterSpeedU3Ek__BackingField_81)); }
	inline int32_t get_U3CmBeattackedLaterSpeedU3Ek__BackingField_81() const { return ___U3CmBeattackedLaterSpeedU3Ek__BackingField_81; }
	inline int32_t* get_address_of_U3CmBeattackedLaterSpeedU3Ek__BackingField_81() { return &___U3CmBeattackedLaterSpeedU3Ek__BackingField_81; }
	inline void set_U3CmBeattackedLaterSpeedU3Ek__BackingField_81(int32_t value)
	{
		___U3CmBeattackedLaterSpeedU3Ek__BackingField_81 = value;
	}

	inline static int32_t get_offset_of_U3CmDeathLaterSpeedU3Ek__BackingField_82() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmDeathLaterSpeedU3Ek__BackingField_82)); }
	inline int32_t get_U3CmDeathLaterSpeedU3Ek__BackingField_82() const { return ___U3CmDeathLaterSpeedU3Ek__BackingField_82; }
	inline int32_t* get_address_of_U3CmDeathLaterSpeedU3Ek__BackingField_82() { return &___U3CmDeathLaterSpeedU3Ek__BackingField_82; }
	inline void set_U3CmDeathLaterSpeedU3Ek__BackingField_82(int32_t value)
	{
		___U3CmDeathLaterSpeedU3Ek__BackingField_82 = value;
	}

	inline static int32_t get_offset_of_U3CmAnitEndEffectIDU3Ek__BackingField_83() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmAnitEndEffectIDU3Ek__BackingField_83)); }
	inline int32_t get_U3CmAnitEndEffectIDU3Ek__BackingField_83() const { return ___U3CmAnitEndEffectIDU3Ek__BackingField_83; }
	inline int32_t* get_address_of_U3CmAnitEndEffectIDU3Ek__BackingField_83() { return &___U3CmAnitEndEffectIDU3Ek__BackingField_83; }
	inline void set_U3CmAnitEndEffectIDU3Ek__BackingField_83(int32_t value)
	{
		___U3CmAnitEndEffectIDU3Ek__BackingField_83 = value;
	}

	inline static int32_t get_offset_of_U3CmAuraAddHPNumU3Ek__BackingField_84() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmAuraAddHPNumU3Ek__BackingField_84)); }
	inline int32_t get_U3CmAuraAddHPNumU3Ek__BackingField_84() const { return ___U3CmAuraAddHPNumU3Ek__BackingField_84; }
	inline int32_t* get_address_of_U3CmAuraAddHPNumU3Ek__BackingField_84() { return &___U3CmAuraAddHPNumU3Ek__BackingField_84; }
	inline void set_U3CmAuraAddHPNumU3Ek__BackingField_84(int32_t value)
	{
		___U3CmAuraAddHPNumU3Ek__BackingField_84 = value;
	}

	inline static int32_t get_offset_of_U3CmAuraAddHPPerU3Ek__BackingField_85() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmAuraAddHPPerU3Ek__BackingField_85)); }
	inline int32_t get_U3CmAuraAddHPPerU3Ek__BackingField_85() const { return ___U3CmAuraAddHPPerU3Ek__BackingField_85; }
	inline int32_t* get_address_of_U3CmAuraAddHPPerU3Ek__BackingField_85() { return &___U3CmAuraAddHPPerU3Ek__BackingField_85; }
	inline void set_U3CmAuraAddHPPerU3Ek__BackingField_85(int32_t value)
	{
		___U3CmAuraAddHPPerU3Ek__BackingField_85 = value;
	}

	inline static int32_t get_offset_of_U3CmAuraReduceHPNumU3Ek__BackingField_86() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmAuraReduceHPNumU3Ek__BackingField_86)); }
	inline int32_t get_U3CmAuraReduceHPNumU3Ek__BackingField_86() const { return ___U3CmAuraReduceHPNumU3Ek__BackingField_86; }
	inline int32_t* get_address_of_U3CmAuraReduceHPNumU3Ek__BackingField_86() { return &___U3CmAuraReduceHPNumU3Ek__BackingField_86; }
	inline void set_U3CmAuraReduceHPNumU3Ek__BackingField_86(int32_t value)
	{
		___U3CmAuraReduceHPNumU3Ek__BackingField_86 = value;
	}

	inline static int32_t get_offset_of_U3CmAuraReduceHPPerU3Ek__BackingField_87() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmAuraReduceHPPerU3Ek__BackingField_87)); }
	inline int32_t get_U3CmAuraReduceHPPerU3Ek__BackingField_87() const { return ___U3CmAuraReduceHPPerU3Ek__BackingField_87; }
	inline int32_t* get_address_of_U3CmAuraReduceHPPerU3Ek__BackingField_87() { return &___U3CmAuraReduceHPPerU3Ek__BackingField_87; }
	inline void set_U3CmAuraReduceHPPerU3Ek__BackingField_87(int32_t value)
	{
		___U3CmAuraReduceHPPerU3Ek__BackingField_87 = value;
	}

	inline static int32_t get_offset_of_U3CmActiveSkillDamageAddPerU3Ek__BackingField_88() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmActiveSkillDamageAddPerU3Ek__BackingField_88)); }
	inline int32_t get_U3CmActiveSkillDamageAddPerU3Ek__BackingField_88() const { return ___U3CmActiveSkillDamageAddPerU3Ek__BackingField_88; }
	inline int32_t* get_address_of_U3CmActiveSkillDamageAddPerU3Ek__BackingField_88() { return &___U3CmActiveSkillDamageAddPerU3Ek__BackingField_88; }
	inline void set_U3CmActiveSkillDamageAddPerU3Ek__BackingField_88(int32_t value)
	{
		___U3CmActiveSkillDamageAddPerU3Ek__BackingField_88 = value;
	}

	inline static int32_t get_offset_of_U3CmAddAngerRatioU3Ek__BackingField_89() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmAddAngerRatioU3Ek__BackingField_89)); }
	inline int32_t get_U3CmAddAngerRatioU3Ek__BackingField_89() const { return ___U3CmAddAngerRatioU3Ek__BackingField_89; }
	inline int32_t* get_address_of_U3CmAddAngerRatioU3Ek__BackingField_89() { return &___U3CmAddAngerRatioU3Ek__BackingField_89; }
	inline void set_U3CmAddAngerRatioU3Ek__BackingField_89(int32_t value)
	{
		___U3CmAddAngerRatioU3Ek__BackingField_89 = value;
	}

	inline static int32_t get_offset_of_U3CmFreezeU3Ek__BackingField_90() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmFreezeU3Ek__BackingField_90)); }
	inline bool get_U3CmFreezeU3Ek__BackingField_90() const { return ___U3CmFreezeU3Ek__BackingField_90; }
	inline bool* get_address_of_U3CmFreezeU3Ek__BackingField_90() { return &___U3CmFreezeU3Ek__BackingField_90; }
	inline void set_U3CmFreezeU3Ek__BackingField_90(bool value)
	{
		___U3CmFreezeU3Ek__BackingField_90 = value;
	}

	inline static int32_t get_offset_of_U3CmMorphU3Ek__BackingField_91() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmMorphU3Ek__BackingField_91)); }
	inline bool get_U3CmMorphU3Ek__BackingField_91() const { return ___U3CmMorphU3Ek__BackingField_91; }
	inline bool* get_address_of_U3CmMorphU3Ek__BackingField_91() { return &___U3CmMorphU3Ek__BackingField_91; }
	inline void set_U3CmMorphU3Ek__BackingField_91(bool value)
	{
		___U3CmMorphU3Ek__BackingField_91 = value;
	}

	inline static int32_t get_offset_of_U3CmAntiControlU3Ek__BackingField_92() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmAntiControlU3Ek__BackingField_92)); }
	inline bool get_U3CmAntiControlU3Ek__BackingField_92() const { return ___U3CmAntiControlU3Ek__BackingField_92; }
	inline bool* get_address_of_U3CmAntiControlU3Ek__BackingField_92() { return &___U3CmAntiControlU3Ek__BackingField_92; }
	inline void set_U3CmAntiControlU3Ek__BackingField_92(bool value)
	{
		___U3CmAntiControlU3Ek__BackingField_92 = value;
	}

	inline static int32_t get_offset_of_U3CmAddDamageSkillIDsU3Ek__BackingField_93() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmAddDamageSkillIDsU3Ek__BackingField_93)); }
	inline List_1_t2522024052 * get_U3CmAddDamageSkillIDsU3Ek__BackingField_93() const { return ___U3CmAddDamageSkillIDsU3Ek__BackingField_93; }
	inline List_1_t2522024052 ** get_address_of_U3CmAddDamageSkillIDsU3Ek__BackingField_93() { return &___U3CmAddDamageSkillIDsU3Ek__BackingField_93; }
	inline void set_U3CmAddDamageSkillIDsU3Ek__BackingField_93(List_1_t2522024052 * value)
	{
		___U3CmAddDamageSkillIDsU3Ek__BackingField_93 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmAddDamageSkillIDsU3Ek__BackingField_93, value);
	}

	inline static int32_t get_offset_of_U3CmAddDamageValueU3Ek__BackingField_94() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmAddDamageValueU3Ek__BackingField_94)); }
	inline List_1_t2522024052 * get_U3CmAddDamageValueU3Ek__BackingField_94() const { return ___U3CmAddDamageValueU3Ek__BackingField_94; }
	inline List_1_t2522024052 ** get_address_of_U3CmAddDamageValueU3Ek__BackingField_94() { return &___U3CmAddDamageValueU3Ek__BackingField_94; }
	inline void set_U3CmAddDamageValueU3Ek__BackingField_94(List_1_t2522024052 * value)
	{
		___U3CmAddDamageValueU3Ek__BackingField_94 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmAddDamageValueU3Ek__BackingField_94, value);
	}

	inline static int32_t get_offset_of_U3CmHpLineBuffIDU3Ek__BackingField_95() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmHpLineBuffIDU3Ek__BackingField_95)); }
	inline int32_t get_U3CmHpLineBuffIDU3Ek__BackingField_95() const { return ___U3CmHpLineBuffIDU3Ek__BackingField_95; }
	inline int32_t* get_address_of_U3CmHpLineBuffIDU3Ek__BackingField_95() { return &___U3CmHpLineBuffIDU3Ek__BackingField_95; }
	inline void set_U3CmHpLineBuffIDU3Ek__BackingField_95(int32_t value)
	{
		___U3CmHpLineBuffIDU3Ek__BackingField_95 = value;
	}

	inline static int32_t get_offset_of_U3CmControlBuffTimeBonusU3Ek__BackingField_96() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmControlBuffTimeBonusU3Ek__BackingField_96)); }
	inline int32_t get_U3CmControlBuffTimeBonusU3Ek__BackingField_96() const { return ___U3CmControlBuffTimeBonusU3Ek__BackingField_96; }
	inline int32_t* get_address_of_U3CmControlBuffTimeBonusU3Ek__BackingField_96() { return &___U3CmControlBuffTimeBonusU3Ek__BackingField_96; }
	inline void set_U3CmControlBuffTimeBonusU3Ek__BackingField_96(int32_t value)
	{
		___U3CmControlBuffTimeBonusU3Ek__BackingField_96 = value;
	}

	inline static int32_t get_offset_of_U3CmControlBuffTimeBonusPerU3Ek__BackingField_97() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmControlBuffTimeBonusPerU3Ek__BackingField_97)); }
	inline int32_t get_U3CmControlBuffTimeBonusPerU3Ek__BackingField_97() const { return ___U3CmControlBuffTimeBonusPerU3Ek__BackingField_97; }
	inline int32_t* get_address_of_U3CmControlBuffTimeBonusPerU3Ek__BackingField_97() { return &___U3CmControlBuffTimeBonusPerU3Ek__BackingField_97; }
	inline void set_U3CmControlBuffTimeBonusPerU3Ek__BackingField_97(int32_t value)
	{
		___U3CmControlBuffTimeBonusPerU3Ek__BackingField_97 = value;
	}

	inline static int32_t get_offset_of_U3CmAddDamageValuePerU3Ek__BackingField_98() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmAddDamageValuePerU3Ek__BackingField_98)); }
	inline List_1_t2522024052 * get_U3CmAddDamageValuePerU3Ek__BackingField_98() const { return ___U3CmAddDamageValuePerU3Ek__BackingField_98; }
	inline List_1_t2522024052 ** get_address_of_U3CmAddDamageValuePerU3Ek__BackingField_98() { return &___U3CmAddDamageValuePerU3Ek__BackingField_98; }
	inline void set_U3CmAddDamageValuePerU3Ek__BackingField_98(List_1_t2522024052 * value)
	{
		___U3CmAddDamageValuePerU3Ek__BackingField_98 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmAddDamageValuePerU3Ek__BackingField_98, value);
	}

	inline static int32_t get_offset_of_U3CmRuneTakeEffectBuffIDU3Ek__BackingField_99() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmRuneTakeEffectBuffIDU3Ek__BackingField_99)); }
	inline int32_t get_U3CmRuneTakeEffectBuffIDU3Ek__BackingField_99() const { return ___U3CmRuneTakeEffectBuffIDU3Ek__BackingField_99; }
	inline int32_t* get_address_of_U3CmRuneTakeEffectBuffIDU3Ek__BackingField_99() { return &___U3CmRuneTakeEffectBuffIDU3Ek__BackingField_99; }
	inline void set_U3CmRuneTakeEffectBuffIDU3Ek__BackingField_99(int32_t value)
	{
		___U3CmRuneTakeEffectBuffIDU3Ek__BackingField_99 = value;
	}

	inline static int32_t get_offset_of_U3CmRuneTakeEffectBuffID01U3Ek__BackingField_100() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmRuneTakeEffectBuffID01U3Ek__BackingField_100)); }
	inline int32_t get_U3CmRuneTakeEffectBuffID01U3Ek__BackingField_100() const { return ___U3CmRuneTakeEffectBuffID01U3Ek__BackingField_100; }
	inline int32_t* get_address_of_U3CmRuneTakeEffectBuffID01U3Ek__BackingField_100() { return &___U3CmRuneTakeEffectBuffID01U3Ek__BackingField_100; }
	inline void set_U3CmRuneTakeEffectBuffID01U3Ek__BackingField_100(int32_t value)
	{
		___U3CmRuneTakeEffectBuffID01U3Ek__BackingField_100 = value;
	}

	inline static int32_t get_offset_of_U3CmRuneTakeEffectBuffID02U3Ek__BackingField_101() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmRuneTakeEffectBuffID02U3Ek__BackingField_101)); }
	inline int32_t get_U3CmRuneTakeEffectBuffID02U3Ek__BackingField_101() const { return ___U3CmRuneTakeEffectBuffID02U3Ek__BackingField_101; }
	inline int32_t* get_address_of_U3CmRuneTakeEffectBuffID02U3Ek__BackingField_101() { return &___U3CmRuneTakeEffectBuffID02U3Ek__BackingField_101; }
	inline void set_U3CmRuneTakeEffectBuffID02U3Ek__BackingField_101(int32_t value)
	{
		___U3CmRuneTakeEffectBuffID02U3Ek__BackingField_101 = value;
	}

	inline static int32_t get_offset_of_U3CmStableU3Ek__BackingField_102() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmStableU3Ek__BackingField_102)); }
	inline int32_t get_U3CmStableU3Ek__BackingField_102() const { return ___U3CmStableU3Ek__BackingField_102; }
	inline int32_t* get_address_of_U3CmStableU3Ek__BackingField_102() { return &___U3CmStableU3Ek__BackingField_102; }
	inline void set_U3CmStableU3Ek__BackingField_102(int32_t value)
	{
		___U3CmStableU3Ek__BackingField_102 = value;
	}

	inline static int32_t get_offset_of_U3CmNotInvadeU3Ek__BackingField_103() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmNotInvadeU3Ek__BackingField_103)); }
	inline int32_t get_U3CmNotInvadeU3Ek__BackingField_103() const { return ___U3CmNotInvadeU3Ek__BackingField_103; }
	inline int32_t* get_address_of_U3CmNotInvadeU3Ek__BackingField_103() { return &___U3CmNotInvadeU3Ek__BackingField_103; }
	inline void set_U3CmNotInvadeU3Ek__BackingField_103(int32_t value)
	{
		___U3CmNotInvadeU3Ek__BackingField_103 = value;
	}

	inline static int32_t get_offset_of_U3CmUnyieldingU3Ek__BackingField_104() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmUnyieldingU3Ek__BackingField_104)); }
	inline int32_t get_U3CmUnyieldingU3Ek__BackingField_104() const { return ___U3CmUnyieldingU3Ek__BackingField_104; }
	inline int32_t* get_address_of_U3CmUnyieldingU3Ek__BackingField_104() { return &___U3CmUnyieldingU3Ek__BackingField_104; }
	inline void set_U3CmUnyieldingU3Ek__BackingField_104(int32_t value)
	{
		___U3CmUnyieldingU3Ek__BackingField_104 = value;
	}

	inline static int32_t get_offset_of_U3CmDiscreteBuffIDTimeU3Ek__BackingField_105() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmDiscreteBuffIDTimeU3Ek__BackingField_105)); }
	inline int32_t get_U3CmDiscreteBuffIDTimeU3Ek__BackingField_105() const { return ___U3CmDiscreteBuffIDTimeU3Ek__BackingField_105; }
	inline int32_t* get_address_of_U3CmDiscreteBuffIDTimeU3Ek__BackingField_105() { return &___U3CmDiscreteBuffIDTimeU3Ek__BackingField_105; }
	inline void set_U3CmDiscreteBuffIDTimeU3Ek__BackingField_105(int32_t value)
	{
		___U3CmDiscreteBuffIDTimeU3Ek__BackingField_105 = value;
	}

	inline static int32_t get_offset_of_U3CmChangeNewSkillIDU3Ek__BackingField_106() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmChangeNewSkillIDU3Ek__BackingField_106)); }
	inline int32_t get_U3CmChangeNewSkillIDU3Ek__BackingField_106() const { return ___U3CmChangeNewSkillIDU3Ek__BackingField_106; }
	inline int32_t* get_address_of_U3CmChangeNewSkillIDU3Ek__BackingField_106() { return &___U3CmChangeNewSkillIDU3Ek__BackingField_106; }
	inline void set_U3CmChangeNewSkillIDU3Ek__BackingField_106(int32_t value)
	{
		___U3CmChangeNewSkillIDU3Ek__BackingField_106 = value;
	}

	inline static int32_t get_offset_of_U3CmChangeSkillProU3Ek__BackingField_107() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmChangeSkillProU3Ek__BackingField_107)); }
	inline int32_t get_U3CmChangeSkillProU3Ek__BackingField_107() const { return ___U3CmChangeSkillProU3Ek__BackingField_107; }
	inline int32_t* get_address_of_U3CmChangeSkillProU3Ek__BackingField_107() { return &___U3CmChangeSkillProU3Ek__BackingField_107; }
	inline void set_U3CmChangeSkillProU3Ek__BackingField_107(int32_t value)
	{
		___U3CmChangeSkillProU3Ek__BackingField_107 = value;
	}

	inline static int32_t get_offset_of_U3CmReduceCDSkillIDU3Ek__BackingField_108() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmReduceCDSkillIDU3Ek__BackingField_108)); }
	inline int32_t get_U3CmReduceCDSkillIDU3Ek__BackingField_108() const { return ___U3CmReduceCDSkillIDU3Ek__BackingField_108; }
	inline int32_t* get_address_of_U3CmReduceCDSkillIDU3Ek__BackingField_108() { return &___U3CmReduceCDSkillIDU3Ek__BackingField_108; }
	inline void set_U3CmReduceCDSkillIDU3Ek__BackingField_108(int32_t value)
	{
		___U3CmReduceCDSkillIDU3Ek__BackingField_108 = value;
	}

	inline static int32_t get_offset_of_U3CmThumpAddValueU3Ek__BackingField_109() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmThumpAddValueU3Ek__BackingField_109)); }
	inline int32_t get_U3CmThumpAddValueU3Ek__BackingField_109() const { return ___U3CmThumpAddValueU3Ek__BackingField_109; }
	inline int32_t* get_address_of_U3CmThumpAddValueU3Ek__BackingField_109() { return &___U3CmThumpAddValueU3Ek__BackingField_109; }
	inline void set_U3CmThumpAddValueU3Ek__BackingField_109(int32_t value)
	{
		___U3CmThumpAddValueU3Ek__BackingField_109 = value;
	}

	inline static int32_t get_offset_of_U3CmShieldOverSkillIDU3Ek__BackingField_110() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmShieldOverSkillIDU3Ek__BackingField_110)); }
	inline int32_t get_U3CmShieldOverSkillIDU3Ek__BackingField_110() const { return ___U3CmShieldOverSkillIDU3Ek__BackingField_110; }
	inline int32_t* get_address_of_U3CmShieldOverSkillIDU3Ek__BackingField_110() { return &___U3CmShieldOverSkillIDU3Ek__BackingField_110; }
	inline void set_U3CmShieldOverSkillIDU3Ek__BackingField_110(int32_t value)
	{
		___U3CmShieldOverSkillIDU3Ek__BackingField_110 = value;
	}

	inline static int32_t get_offset_of_U3CmCleaveSkillIDU3Ek__BackingField_111() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmCleaveSkillIDU3Ek__BackingField_111)); }
	inline int32_t get_U3CmCleaveSkillIDU3Ek__BackingField_111() const { return ___U3CmCleaveSkillIDU3Ek__BackingField_111; }
	inline int32_t* get_address_of_U3CmCleaveSkillIDU3Ek__BackingField_111() { return &___U3CmCleaveSkillIDU3Ek__BackingField_111; }
	inline void set_U3CmCleaveSkillIDU3Ek__BackingField_111(int32_t value)
	{
		___U3CmCleaveSkillIDU3Ek__BackingField_111 = value;
	}

	inline static int32_t get_offset_of_U3CmCleaveBuffIDU3Ek__BackingField_112() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmCleaveBuffIDU3Ek__BackingField_112)); }
	inline int32_t get_U3CmCleaveBuffIDU3Ek__BackingField_112() const { return ___U3CmCleaveBuffIDU3Ek__BackingField_112; }
	inline int32_t* get_address_of_U3CmCleaveBuffIDU3Ek__BackingField_112() { return &___U3CmCleaveBuffIDU3Ek__BackingField_112; }
	inline void set_U3CmCleaveBuffIDU3Ek__BackingField_112(int32_t value)
	{
		___U3CmCleaveBuffIDU3Ek__BackingField_112 = value;
	}

	inline static int32_t get_offset_of_U3CmHpLineUpBuffIDU3Ek__BackingField_113() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmHpLineUpBuffIDU3Ek__BackingField_113)); }
	inline int32_t get_U3CmHpLineUpBuffIDU3Ek__BackingField_113() const { return ___U3CmHpLineUpBuffIDU3Ek__BackingField_113; }
	inline int32_t* get_address_of_U3CmHpLineUpBuffIDU3Ek__BackingField_113() { return &___U3CmHpLineUpBuffIDU3Ek__BackingField_113; }
	inline void set_U3CmHpLineUpBuffIDU3Ek__BackingField_113(int32_t value)
	{
		___U3CmHpLineUpBuffIDU3Ek__BackingField_113 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffCritPerU3Ek__BackingField_114() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffCritPerU3Ek__BackingField_114)); }
	inline int32_t get_U3CmBuffCritPerU3Ek__BackingField_114() const { return ___U3CmBuffCritPerU3Ek__BackingField_114; }
	inline int32_t* get_address_of_U3CmBuffCritPerU3Ek__BackingField_114() { return &___U3CmBuffCritPerU3Ek__BackingField_114; }
	inline void set_U3CmBuffCritPerU3Ek__BackingField_114(int32_t value)
	{
		___U3CmBuffCritPerU3Ek__BackingField_114 = value;
	}

	inline static int32_t get_offset_of_U3CmBuffCritHurtU3Ek__BackingField_115() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmBuffCritHurtU3Ek__BackingField_115)); }
	inline int32_t get_U3CmBuffCritHurtU3Ek__BackingField_115() const { return ___U3CmBuffCritHurtU3Ek__BackingField_115; }
	inline int32_t* get_address_of_U3CmBuffCritHurtU3Ek__BackingField_115() { return &___U3CmBuffCritHurtU3Ek__BackingField_115; }
	inline void set_U3CmBuffCritHurtU3Ek__BackingField_115(int32_t value)
	{
		___U3CmBuffCritHurtU3Ek__BackingField_115 = value;
	}

	inline static int32_t get_offset_of_U3CmAddHpNumU3Ek__BackingField_116() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmAddHpNumU3Ek__BackingField_116)); }
	inline int32_t get_U3CmAddHpNumU3Ek__BackingField_116() const { return ___U3CmAddHpNumU3Ek__BackingField_116; }
	inline int32_t* get_address_of_U3CmAddHpNumU3Ek__BackingField_116() { return &___U3CmAddHpNumU3Ek__BackingField_116; }
	inline void set_U3CmAddHpNumU3Ek__BackingField_116(int32_t value)
	{
		___U3CmAddHpNumU3Ek__BackingField_116 = value;
	}

	inline static int32_t get_offset_of_U3CmHTBuffIDU3Ek__BackingField_117() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmHTBuffIDU3Ek__BackingField_117)); }
	inline int32_t get_U3CmHTBuffIDU3Ek__BackingField_117() const { return ___U3CmHTBuffIDU3Ek__BackingField_117; }
	inline int32_t* get_address_of_U3CmHTBuffIDU3Ek__BackingField_117() { return &___U3CmHTBuffIDU3Ek__BackingField_117; }
	inline void set_U3CmHTBuffIDU3Ek__BackingField_117(int32_t value)
	{
		___U3CmHTBuffIDU3Ek__BackingField_117 = value;
	}

	inline static int32_t get_offset_of_U3CmDeathHarvestPerU3Ek__BackingField_118() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmDeathHarvestPerU3Ek__BackingField_118)); }
	inline int32_t get_U3CmDeathHarvestPerU3Ek__BackingField_118() const { return ___U3CmDeathHarvestPerU3Ek__BackingField_118; }
	inline int32_t* get_address_of_U3CmDeathHarvestPerU3Ek__BackingField_118() { return &___U3CmDeathHarvestPerU3Ek__BackingField_118; }
	inline void set_U3CmDeathHarvestPerU3Ek__BackingField_118(int32_t value)
	{
		___U3CmDeathHarvestPerU3Ek__BackingField_118 = value;
	}

	inline static int32_t get_offset_of_U3CmHpLineCleaveAddBuffU3Ek__BackingField_119() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmHpLineCleaveAddBuffU3Ek__BackingField_119)); }
	inline int32_t get_U3CmHpLineCleaveAddBuffU3Ek__BackingField_119() const { return ___U3CmHpLineCleaveAddBuffU3Ek__BackingField_119; }
	inline int32_t* get_address_of_U3CmHpLineCleaveAddBuffU3Ek__BackingField_119() { return &___U3CmHpLineCleaveAddBuffU3Ek__BackingField_119; }
	inline void set_U3CmHpLineCleaveAddBuffU3Ek__BackingField_119(int32_t value)
	{
		___U3CmHpLineCleaveAddBuffU3Ek__BackingField_119 = value;
	}

	inline static int32_t get_offset_of_U3CmResistanceCountryPerU3Ek__BackingField_120() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmResistanceCountryPerU3Ek__BackingField_120)); }
	inline int32_t get_U3CmResistanceCountryPerU3Ek__BackingField_120() const { return ___U3CmResistanceCountryPerU3Ek__BackingField_120; }
	inline int32_t* get_address_of_U3CmResistanceCountryPerU3Ek__BackingField_120() { return &___U3CmResistanceCountryPerU3Ek__BackingField_120; }
	inline void set_U3CmResistanceCountryPerU3Ek__BackingField_120(int32_t value)
	{
		___U3CmResistanceCountryPerU3Ek__BackingField_120 = value;
	}

	inline static int32_t get_offset_of_U3CmNDPAddBuff02U3Ek__BackingField_121() { return static_cast<int32_t>(offsetof(BuffCtrl_t2836564350, ___U3CmNDPAddBuff02U3Ek__BackingField_121)); }
	inline int32_t get_U3CmNDPAddBuff02U3Ek__BackingField_121() const { return ___U3CmNDPAddBuff02U3Ek__BackingField_121; }
	inline int32_t* get_address_of_U3CmNDPAddBuff02U3Ek__BackingField_121() { return &___U3CmNDPAddBuff02U3Ek__BackingField_121; }
	inline void set_U3CmNDPAddBuff02U3Ek__BackingField_121(int32_t value)
	{
		___U3CmNDPAddBuff02U3Ek__BackingField_121 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
