﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_TrueForAll__PredicateT1_T>c__AnonStorey98
struct U3CListA1_TrueForAll__PredicateT1_TU3Ec__AnonStorey98_t4100405302;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_TrueForAll__PredicateT1_T>c__AnonStorey98::.ctor()
extern "C"  void U3CListA1_TrueForAll__PredicateT1_TU3Ec__AnonStorey98__ctor_m3061425957 (U3CListA1_TrueForAll__PredicateT1_TU3Ec__AnonStorey98_t4100405302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Collections_Generic_List77Generated/<ListA1_TrueForAll__PredicateT1_T>c__AnonStorey98::<>m__BD()
extern "C"  Il2CppObject * U3CListA1_TrueForAll__PredicateT1_TU3Ec__AnonStorey98_U3CU3Em__BD_m452752923 (U3CListA1_TrueForAll__PredicateT1_TU3Ec__AnonStorey98_t4100405302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
