﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.ValueMember
struct ValueMember_t110398141;
// ProtoBuf.Meta.RuntimeTypeModel
struct RuntimeTypeModel_t242172789;
// System.Type
struct Type_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.Serializers.IProtoSerializer
struct IProtoSerializer_t3033312651;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// ProtoBuf.Meta.MetaType
struct MetaType_t448283965;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_RuntimeTypeModel242172789.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "AssemblyU2DCSharp_ProtoBuf_DataFormat274207885.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "AssemblyU2DCSharp_ProtoBuf_WireType2355646059.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_ValueMember110398141.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoTypeCode2227754741.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_MetaType448283965.h"

// System.Void ProtoBuf.Meta.ValueMember::.ctor(ProtoBuf.Meta.RuntimeTypeModel,System.Type,System.Int32,System.Reflection.MemberInfo,System.Type,System.Type,System.Type,ProtoBuf.DataFormat,System.Object)
extern "C"  void ValueMember__ctor_m3112248622 (ValueMember_t110398141 * __this, RuntimeTypeModel_t242172789 * ___model0, Type_t * ___parentType1, int32_t ___fieldNumber2, MemberInfo_t * ___member3, Type_t * ___memberType4, Type_t * ___itemType5, Type_t * ___defaultType6, int32_t ___dataFormat7, Il2CppObject * ___defaultValue8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.ValueMember::.ctor(ProtoBuf.Meta.RuntimeTypeModel,System.Int32,System.Type,System.Type,System.Type,ProtoBuf.DataFormat)
extern "C"  void ValueMember__ctor_m3590269551 (ValueMember_t110398141 * __this, RuntimeTypeModel_t242172789 * ___model0, int32_t ___fieldNumber1, Type_t * ___memberType2, Type_t * ___itemType3, Type_t * ___defaultType4, int32_t ___dataFormat5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.ValueMember::get_FieldNumber()
extern "C"  int32_t ValueMember_get_FieldNumber_m1731015532 (ValueMember_t110398141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo ProtoBuf.Meta.ValueMember::get_Member()
extern "C"  MemberInfo_t * ValueMember_get_Member_m1499304560 (ValueMember_t110398141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.ValueMember::get_ItemType()
extern "C"  Type_t * ValueMember_get_ItemType_m760828378 (ValueMember_t110398141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.ValueMember::get_MemberType()
extern "C"  Type_t * ValueMember_get_MemberType_m706954529 (ValueMember_t110398141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.ValueMember::get_DefaultType()
extern "C"  Type_t * ValueMember_get_DefaultType_m3829957808 (ValueMember_t110398141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.ValueMember::get_ParentType()
extern "C"  Type_t * ValueMember_get_ParentType_m2157508401 (ValueMember_t110398141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.ValueMember::get_DefaultValue()
extern "C"  Il2CppObject * ValueMember_get_DefaultValue_m3100147746 (ValueMember_t110398141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.ValueMember::set_DefaultValue(System.Object)
extern "C"  void ValueMember_set_DefaultValue_m2649934665 (ValueMember_t110398141 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.ValueMember::GetRawEnumValue()
extern "C"  Il2CppObject * ValueMember_GetRawEnumValue_m452694327 (ValueMember_t110398141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.ValueMember::ParseDefaultValue(System.Type,System.Object)
extern "C"  Il2CppObject * ValueMember_ParseDefaultValue_m2556727851 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Serializers.IProtoSerializer ProtoBuf.Meta.ValueMember::get_Serializer()
extern "C"  Il2CppObject * ValueMember_get_Serializer_m2239166791 (ValueMember_t110398141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.DataFormat ProtoBuf.Meta.ValueMember::get_DataFormat()
extern "C"  int32_t ValueMember_get_DataFormat_m2119862969 (ValueMember_t110398141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.ValueMember::set_DataFormat(ProtoBuf.DataFormat)
extern "C"  void ValueMember_set_DataFormat_m4266033234 (ValueMember_t110398141 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.ValueMember::get_IsStrict()
extern "C"  bool ValueMember_get_IsStrict_m4173328806 (ValueMember_t110398141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.ValueMember::set_IsStrict(System.Boolean)
extern "C"  void ValueMember_set_IsStrict_m2710801989 (ValueMember_t110398141 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.ValueMember::get_IsPacked()
extern "C"  bool ValueMember_get_IsPacked_m3129618325 (ValueMember_t110398141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.ValueMember::set_IsPacked(System.Boolean)
extern "C"  void ValueMember_set_IsPacked_m2785492468 (ValueMember_t110398141 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.ValueMember::get_OverwriteList()
extern "C"  bool ValueMember_get_OverwriteList_m1302240248 (ValueMember_t110398141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.ValueMember::set_OverwriteList(System.Boolean)
extern "C"  void ValueMember_set_OverwriteList_m1969751751 (ValueMember_t110398141 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.ValueMember::get_IsRequired()
extern "C"  bool ValueMember_get_IsRequired_m1097157116 (ValueMember_t110398141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.ValueMember::set_IsRequired(System.Boolean)
extern "C"  void ValueMember_set_IsRequired_m684379675 (ValueMember_t110398141 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.ValueMember::get_AsReference()
extern "C"  bool ValueMember_get_AsReference_m1620084712 (ValueMember_t110398141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.ValueMember::set_AsReference(System.Boolean)
extern "C"  void ValueMember_set_AsReference_m622871735 (ValueMember_t110398141 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.ValueMember::get_DynamicType()
extern "C"  bool ValueMember_get_DynamicType_m1235620040 (ValueMember_t110398141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.ValueMember::set_DynamicType(System.Boolean)
extern "C"  void ValueMember_set_DynamicType_m904185239 (ValueMember_t110398141 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.ValueMember::SetSpecified(System.Reflection.MethodInfo,System.Reflection.MethodInfo)
extern "C"  void ValueMember_SetSpecified_m58825402 (ValueMember_t110398141 * __this, MethodInfo_t * ___getSpecified0, MethodInfo_t * ___setSpecified1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.ValueMember::ThrowIfFrozen()
extern "C"  void ValueMember_ThrowIfFrozen_m1633540743 (ValueMember_t110398141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Serializers.IProtoSerializer ProtoBuf.Meta.ValueMember::BuildSerializer()
extern "C"  Il2CppObject * ValueMember_BuildSerializer_m927353878 (ValueMember_t110398141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.WireType ProtoBuf.Meta.ValueMember::GetIntWireType(ProtoBuf.DataFormat,System.Int32)
extern "C"  int32_t ValueMember_GetIntWireType_m2699655310 (Il2CppObject * __this /* static, unused */, int32_t ___format0, int32_t ___width1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.WireType ProtoBuf.Meta.ValueMember::GetDateTimeWireType(ProtoBuf.DataFormat)
extern "C"  int32_t ValueMember_GetDateTimeWireType_m539567145 (Il2CppObject * __this /* static, unused */, int32_t ___format0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Serializers.IProtoSerializer ProtoBuf.Meta.ValueMember::TryGetCoreSerializer(ProtoBuf.Meta.RuntimeTypeModel,ProtoBuf.DataFormat,System.Type,ProtoBuf.WireType&,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern "C"  Il2CppObject * ValueMember_TryGetCoreSerializer_m2767692236 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ___model0, int32_t ___dataFormat1, Type_t * ___type2, int32_t* ___defaultWireType3, bool ___asReference4, bool ___dynamicType5, bool ___overwriteList6, bool ___allowComplexTypes7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.ValueMember::SetName(System.String)
extern "C"  void ValueMember_SetName_m3542439281 (ValueMember_t110398141 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.Meta.ValueMember::get_Name()
extern "C"  String_t* ValueMember_get_Name_m2761864335 (ValueMember_t110398141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.ValueMember::HasFlag(System.Byte)
extern "C"  bool ValueMember_HasFlag_m1889575405 (ValueMember_t110398141 * __this, uint8_t ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.ValueMember::SetFlag(System.Byte,System.Boolean,System.Boolean)
extern "C"  void ValueMember_SetFlag_m3904385785 (ValueMember_t110398141 * __this, uint8_t ___flag0, bool ___value1, bool ___throwIfFrozen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.ValueMember::get_SupportNull()
extern "C"  bool ValueMember_get_SupportNull_m3874102117 (ValueMember_t110398141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.ValueMember::set_SupportNull(System.Boolean)
extern "C"  void ValueMember_set_SupportNull_m4192172404 (ValueMember_t110398141 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.Meta.ValueMember::GetSchemaTypeName(System.Boolean,System.Boolean&)
extern "C"  String_t* ValueMember_GetSchemaTypeName_m338095945 (ValueMember_t110398141 * __this, bool ___applyNetObjectProxy0, bool* ___requiresBclImport1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Serializers.IProtoSerializer ProtoBuf.Meta.ValueMember::ilo_BuildSerializer1(ProtoBuf.Meta.ValueMember)
extern "C"  Il2CppObject * ValueMember_ilo_BuildSerializer1_m3242909533 (Il2CppObject * __this /* static, unused */, ValueMember_t110398141 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.ValueMember::ilo_HasFlag2(ProtoBuf.Meta.ValueMember,System.Byte)
extern "C"  bool ValueMember_ilo_HasFlag2_m1529100425 (Il2CppObject * __this /* static, unused */, ValueMember_t110398141 * ____this0, uint8_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.ValueMember::ilo_SetFlag3(ProtoBuf.Meta.ValueMember,System.Byte,System.Boolean,System.Boolean)
extern "C"  void ValueMember_ilo_SetFlag3_m1279981788 (Il2CppObject * __this /* static, unused */, ValueMember_t110398141 * ____this0, uint8_t ___flag1, bool ___value2, bool ___throwIfFrozen3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.ValueMember::ilo_ThrowIfFrozen4(ProtoBuf.Meta.ValueMember)
extern "C"  void ValueMember_ilo_ThrowIfFrozen4_m1471363307 (Il2CppObject * __this /* static, unused */, ValueMember_t110398141 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.ValueMember::ilo_get_IsStrict5(ProtoBuf.Meta.ValueMember)
extern "C"  bool ValueMember_ilo_get_IsStrict5_m3024712623 (Il2CppObject * __this /* static, unused */, ValueMember_t110398141 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.ValueMember::ilo_CanWrite6(ProtoBuf.Meta.TypeModel,System.Reflection.MemberInfo)
extern "C"  bool ValueMember_ilo_CanWrite6_m1430512189 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, MemberInfo_t * ___member1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.ValueMember::ilo_get_OverwriteList7(ProtoBuf.Meta.ValueMember)
extern "C"  bool ValueMember_ilo_get_OverwriteList7_m41622713 (Il2CppObject * __this /* static, unused */, ValueMember_t110398141 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.ValueMember::ilo_MapType8(ProtoBuf.Meta.TypeModel,System.Type)
extern "C"  Type_t * ValueMember_ilo_MapType8_m2215589027 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.ValueMember::ilo_GetUnderlyingType9(System.Type)
extern "C"  Type_t * ValueMember_ilo_GetUnderlyingType9_m1194524502 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.ProtoTypeCode ProtoBuf.Meta.ValueMember::ilo_GetTypeCode10(System.Type)
extern "C"  int32_t ValueMember_ilo_GetTypeCode10_m3587870811 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.WireType ProtoBuf.Meta.ValueMember::ilo_GetIntWireType11(ProtoBuf.DataFormat,System.Int32)
extern "C"  int32_t ValueMember_ilo_GetIntWireType11_m3819997985 (Il2CppObject * __this /* static, unused */, int32_t ___format0, int32_t ___width1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.WireType ProtoBuf.Meta.ValueMember::ilo_GetDateTimeWireType12(ProtoBuf.DataFormat)
extern "C"  int32_t ValueMember_ilo_GetDateTimeWireType12_m1978807835 (Il2CppObject * __this /* static, unused */, int32_t ___format0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.ValueMember::ilo_GetKey13(ProtoBuf.Meta.RuntimeTypeModel,System.Type,System.Boolean,System.Boolean)
extern "C"  int32_t ValueMember_ilo_GetKey13_m3355619876 (Il2CppObject * __this /* static, unused */, RuntimeTypeModel_t242172789 * ____this0, Type_t * ___type1, bool ___demand2, bool ___getBaseKey3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.ValueMember::ilo_get_UseConstructor14(ProtoBuf.Meta.MetaType)
extern "C"  bool ValueMember_ilo_get_UseConstructor14_m4149094903 (Il2CppObject * __this /* static, unused */, MetaType_t448283965 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.ValueMember::ilo_IsNullOrEmpty15(System.String)
extern "C"  bool ValueMember_ilo_IsNullOrEmpty15_m2308363456 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
