﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066747055.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m4066247973_gshared (KeyValuePair_2_t4066747055 * __this, uint32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m4066247973(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4066747055 *, uint32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m4066247973_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::get_Key()
extern "C"  uint32_t KeyValuePair_2_get_Key_m1758015651_gshared (KeyValuePair_2_t4066747055 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1758015651(__this, method) ((  uint32_t (*) (KeyValuePair_2_t4066747055 *, const MethodInfo*))KeyValuePair_2_get_Key_m1758015651_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1636954596_gshared (KeyValuePair_2_t4066747055 * __this, uint32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m1636954596(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4066747055 *, uint32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1636954596_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2692750471_gshared (KeyValuePair_2_t4066747055 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2692750471(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t4066747055 *, const MethodInfo*))KeyValuePair_2_get_Value_m2692750471_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1981395940_gshared (KeyValuePair_2_t4066747055 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1981395940(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4066747055 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m1981395940_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2106229668_gshared (KeyValuePair_2_t4066747055 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2106229668(__this, method) ((  String_t* (*) (KeyValuePair_2_t4066747055 *, const MethodInfo*))KeyValuePair_2_ToString_m2106229668_gshared)(__this, method)
