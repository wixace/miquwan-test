﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSBossUnit
struct  CSBossUnit_t2214856481  : public Il2CppObject
{
public:
	// System.String CSBossUnit::bossId
	String_t* ___bossId_0;
	// System.UInt32 CSBossUnit::type
	uint32_t ___type_1;
	// System.UInt32 CSBossUnit::progress
	uint32_t ___progress_2;
	// System.UInt32 CSBossUnit::isOwner
	uint32_t ___isOwner_3;
	// System.UInt32 CSBossUnit::invited
	uint32_t ___invited_4;
	// System.UInt32 CSBossUnit::lv
	uint32_t ___lv_5;
	// System.UInt32 CSBossUnit::hp
	uint32_t ___hp_6;
	// System.UInt32 CSBossUnit::monsterID
	uint32_t ___monsterID_7;
	// System.UInt32 CSBossUnit::survivalTime
	uint32_t ___survivalTime_8;
	// System.UInt32 CSBossUnit::maxHp
	uint32_t ___maxHp_9;

public:
	inline static int32_t get_offset_of_bossId_0() { return static_cast<int32_t>(offsetof(CSBossUnit_t2214856481, ___bossId_0)); }
	inline String_t* get_bossId_0() const { return ___bossId_0; }
	inline String_t** get_address_of_bossId_0() { return &___bossId_0; }
	inline void set_bossId_0(String_t* value)
	{
		___bossId_0 = value;
		Il2CppCodeGenWriteBarrier(&___bossId_0, value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(CSBossUnit_t2214856481, ___type_1)); }
	inline uint32_t get_type_1() const { return ___type_1; }
	inline uint32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(uint32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_progress_2() { return static_cast<int32_t>(offsetof(CSBossUnit_t2214856481, ___progress_2)); }
	inline uint32_t get_progress_2() const { return ___progress_2; }
	inline uint32_t* get_address_of_progress_2() { return &___progress_2; }
	inline void set_progress_2(uint32_t value)
	{
		___progress_2 = value;
	}

	inline static int32_t get_offset_of_isOwner_3() { return static_cast<int32_t>(offsetof(CSBossUnit_t2214856481, ___isOwner_3)); }
	inline uint32_t get_isOwner_3() const { return ___isOwner_3; }
	inline uint32_t* get_address_of_isOwner_3() { return &___isOwner_3; }
	inline void set_isOwner_3(uint32_t value)
	{
		___isOwner_3 = value;
	}

	inline static int32_t get_offset_of_invited_4() { return static_cast<int32_t>(offsetof(CSBossUnit_t2214856481, ___invited_4)); }
	inline uint32_t get_invited_4() const { return ___invited_4; }
	inline uint32_t* get_address_of_invited_4() { return &___invited_4; }
	inline void set_invited_4(uint32_t value)
	{
		___invited_4 = value;
	}

	inline static int32_t get_offset_of_lv_5() { return static_cast<int32_t>(offsetof(CSBossUnit_t2214856481, ___lv_5)); }
	inline uint32_t get_lv_5() const { return ___lv_5; }
	inline uint32_t* get_address_of_lv_5() { return &___lv_5; }
	inline void set_lv_5(uint32_t value)
	{
		___lv_5 = value;
	}

	inline static int32_t get_offset_of_hp_6() { return static_cast<int32_t>(offsetof(CSBossUnit_t2214856481, ___hp_6)); }
	inline uint32_t get_hp_6() const { return ___hp_6; }
	inline uint32_t* get_address_of_hp_6() { return &___hp_6; }
	inline void set_hp_6(uint32_t value)
	{
		___hp_6 = value;
	}

	inline static int32_t get_offset_of_monsterID_7() { return static_cast<int32_t>(offsetof(CSBossUnit_t2214856481, ___monsterID_7)); }
	inline uint32_t get_monsterID_7() const { return ___monsterID_7; }
	inline uint32_t* get_address_of_monsterID_7() { return &___monsterID_7; }
	inline void set_monsterID_7(uint32_t value)
	{
		___monsterID_7 = value;
	}

	inline static int32_t get_offset_of_survivalTime_8() { return static_cast<int32_t>(offsetof(CSBossUnit_t2214856481, ___survivalTime_8)); }
	inline uint32_t get_survivalTime_8() const { return ___survivalTime_8; }
	inline uint32_t* get_address_of_survivalTime_8() { return &___survivalTime_8; }
	inline void set_survivalTime_8(uint32_t value)
	{
		___survivalTime_8 = value;
	}

	inline static int32_t get_offset_of_maxHp_9() { return static_cast<int32_t>(offsetof(CSBossUnit_t2214856481, ___maxHp_9)); }
	inline uint32_t get_maxHp_9() const { return ___maxHp_9; }
	inline uint32_t* get_address_of_maxHp_9() { return &___maxHp_9; }
	inline void set_maxHp_9(uint32_t value)
	{
		___maxHp_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
