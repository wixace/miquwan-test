﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_cazehemSairnaxas224
struct M_cazehemSairnaxas224_t3478829243;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_cazehemSairnaxas2243478829243.h"

// System.Void GarbageiOS.M_cazehemSairnaxas224::.ctor()
extern "C"  void M_cazehemSairnaxas224__ctor_m3785936136 (M_cazehemSairnaxas224_t3478829243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cazehemSairnaxas224::M_cemstemfaLodispas0(System.String[],System.Int32)
extern "C"  void M_cazehemSairnaxas224_M_cemstemfaLodispas0_m2757564527 (M_cazehemSairnaxas224_t3478829243 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cazehemSairnaxas224::M_whurfoutrisBawpo1(System.String[],System.Int32)
extern "C"  void M_cazehemSairnaxas224_M_whurfoutrisBawpo1_m190293099 (M_cazehemSairnaxas224_t3478829243 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cazehemSairnaxas224::M_rajou2(System.String[],System.Int32)
extern "C"  void M_cazehemSairnaxas224_M_rajou2_m2696634582 (M_cazehemSairnaxas224_t3478829243 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cazehemSairnaxas224::ilo_M_whurfoutrisBawpo11(GarbageiOS.M_cazehemSairnaxas224,System.String[],System.Int32)
extern "C"  void M_cazehemSairnaxas224_ilo_M_whurfoutrisBawpo11_m4058755614 (Il2CppObject * __this /* static, unused */, M_cazehemSairnaxas224_t3478829243 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
