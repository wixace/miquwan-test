﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.RVO.ObstacleVertex>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m212848531(__this, ___l0, method) ((  void (*) (Enumerator_t1263198125 *, List_1_t1243525355 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.RVO.ObstacleVertex>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2064291039(__this, method) ((  void (*) (Enumerator_t1263198125 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.RVO.ObstacleVertex>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2562943957(__this, method) ((  Il2CppObject * (*) (Enumerator_t1263198125 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.RVO.ObstacleVertex>::Dispose()
#define Enumerator_Dispose_m3067032120(__this, method) ((  void (*) (Enumerator_t1263198125 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.RVO.ObstacleVertex>::VerifyState()
#define Enumerator_VerifyState_m3828290929(__this, method) ((  void (*) (Enumerator_t1263198125 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.RVO.ObstacleVertex>::MoveNext()
#define Enumerator_MoveNext_m3460896655(__this, method) ((  bool (*) (Enumerator_t1263198125 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.RVO.ObstacleVertex>::get_Current()
#define Enumerator_get_Current_m4097399626(__this, method) ((  ObstacleVertex_t4170307099 * (*) (Enumerator_t1263198125 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
