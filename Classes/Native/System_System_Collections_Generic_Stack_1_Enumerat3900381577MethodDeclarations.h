﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerat2532196025MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<System.Object>>::.ctor(System.Collections.Generic.Stack`1<T>)
#define Enumerator__ctor_m3802843961(__this, ___t0, method) ((  void (*) (Enumerator_t3900381577 *, Stack_1_t47628255 *, const MethodInfo*))Enumerator__ctor_m1003414509_gshared)(__this, ___t0, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<System.Object>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m211126845(__this, method) ((  void (*) (Enumerator_t3900381577 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3054975473_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<System.Object>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m147848617(__this, method) ((  Il2CppObject * (*) (Enumerator_t3900381577 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2470832103_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<System.Object>>::Dispose()
#define Enumerator_Dispose_m2316230682(__this, method) ((  void (*) (Enumerator_t3900381577 *, const MethodInfo*))Enumerator_Dispose_m1634653158_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<System.Object>>::MoveNext()
#define Enumerator_MoveNext_m92258965(__this, method) ((  bool (*) (Enumerator_t3900381577 *, const MethodInfo*))Enumerator_MoveNext_m3012756789_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<System.Object>>::get_Current()
#define Enumerator_get_Current_m734837322(__this, method) ((  List_1_t1244034627 * (*) (Enumerator_t3900381577 *, const MethodInfo*))Enumerator_get_Current_m2483819640_gshared)(__this, method)
