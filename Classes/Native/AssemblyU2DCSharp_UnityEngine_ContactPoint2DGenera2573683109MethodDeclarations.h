﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ContactPoint2DGenerated
struct UnityEngine_ContactPoint2DGenerated_t2573683109;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_ContactPoint2DGenerated::.ctor()
extern "C"  void UnityEngine_ContactPoint2DGenerated__ctor_m4140565350 (UnityEngine_ContactPoint2DGenerated_t2573683109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ContactPoint2DGenerated::.cctor()
extern "C"  void UnityEngine_ContactPoint2DGenerated__cctor_m3321378055 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ContactPoint2DGenerated::ContactPoint2D_ContactPoint2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ContactPoint2DGenerated_ContactPoint2D_ContactPoint2D1_m1148860108 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ContactPoint2DGenerated::ContactPoint2D_point(JSVCall)
extern "C"  void UnityEngine_ContactPoint2DGenerated_ContactPoint2D_point_m405788534 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ContactPoint2DGenerated::ContactPoint2D_normal(JSVCall)
extern "C"  void UnityEngine_ContactPoint2DGenerated_ContactPoint2D_normal_m2475882239 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ContactPoint2DGenerated::ContactPoint2D_collider(JSVCall)
extern "C"  void UnityEngine_ContactPoint2DGenerated_ContactPoint2D_collider_m3593575058 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ContactPoint2DGenerated::ContactPoint2D_otherCollider(JSVCall)
extern "C"  void UnityEngine_ContactPoint2DGenerated_ContactPoint2D_otherCollider_m4164569186 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ContactPoint2DGenerated::__Register()
extern "C"  void UnityEngine_ContactPoint2DGenerated___Register_m2599266593 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ContactPoint2DGenerated::ilo_setVector2S1(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_ContactPoint2DGenerated_ilo_setVector2S1_m1199455105 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ContactPoint2DGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_ContactPoint2DGenerated_ilo_setObject2_m3071389513 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
