﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<PointCloudRegognizer/Point>
struct List_1_t3207017302;
// System.Collections.Generic.IEnumerable`1<PointCloudRegognizer/Point>
struct IEnumerable_1_t844777411;
// System.Collections.Generic.IEnumerator`1<PointCloudRegognizer/Point>
struct IEnumerator_1_t3750696799;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<PointCloudRegognizer/Point>
struct ICollection_1_t2733421737;
// System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>
struct ReadOnlyCollection_1_t3395909286;
// PointCloudRegognizer/Point[]
struct PointU5BU5D_t2313848483;
// System.Predicate`1<PointCloudRegognizer/Point>
struct Predicate_1_t1449888633;
// System.Collections.Generic.IComparer`1<PointCloudRegognizer/Point>
struct IComparer_1_t118878496;
// System.Comparison`1<PointCloudRegognizer/Point>
struct Comparison_1_t555192937;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PointCloudRegognizer_Point1838831750.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3226690072.h"

// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::.ctor()
extern "C"  void List_1__ctor_m2053899057_gshared (List_1_t3207017302 * __this, const MethodInfo* method);
#define List_1__ctor_m2053899057(__this, method) ((  void (*) (List_1_t3207017302 *, const MethodInfo*))List_1__ctor_m2053899057_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m2178482251_gshared (List_1_t3207017302 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m2178482251(__this, ___collection0, method) ((  void (*) (List_1_t3207017302 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m2178482251_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m2132899586_gshared (List_1_t3207017302 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m2132899586(__this, ___capacity0, method) ((  void (*) (List_1_t3207017302 *, int32_t, const MethodInfo*))List_1__ctor_m2132899586_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::.cctor()
extern "C"  void List_1__cctor_m2853674739_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m2853674739(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m2853674739_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<PointCloudRegognizer/Point>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m958973572_gshared (List_1_t3207017302 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m958973572(__this, method) ((  Il2CppObject* (*) (List_1_t3207017302 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m958973572_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1191601802_gshared (List_1_t3207017302 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1191601802(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3207017302 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1191601802_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<PointCloudRegognizer/Point>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3728186521_gshared (List_1_t3207017302 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3728186521(__this, method) ((  Il2CppObject * (*) (List_1_t3207017302 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3728186521_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PointCloudRegognizer/Point>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1140151492_gshared (List_1_t3207017302 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1140151492(__this, ___item0, method) ((  int32_t (*) (List_1_t3207017302 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1140151492_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PointCloudRegognizer/Point>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m944427132_gshared (List_1_t3207017302 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m944427132(__this, ___item0, method) ((  bool (*) (List_1_t3207017302 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m944427132_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PointCloudRegognizer/Point>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2603976988_gshared (List_1_t3207017302 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m2603976988(__this, ___item0, method) ((  int32_t (*) (List_1_t3207017302 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m2603976988_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m817442703_gshared (List_1_t3207017302 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m817442703(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3207017302 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m817442703_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3820576057_gshared (List_1_t3207017302 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3820576057(__this, ___item0, method) ((  void (*) (List_1_t3207017302 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3820576057_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PointCloudRegognizer/Point>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1937331581_gshared (List_1_t3207017302 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1937331581(__this, method) ((  bool (*) (List_1_t3207017302 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1937331581_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PointCloudRegognizer/Point>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3387876704_gshared (List_1_t3207017302 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3387876704(__this, method) ((  bool (*) (List_1_t3207017302 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3387876704_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PointCloudRegognizer/Point>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2113355794_gshared (List_1_t3207017302 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2113355794(__this, method) ((  Il2CppObject * (*) (List_1_t3207017302 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2113355794_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PointCloudRegognizer/Point>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m954698987_gshared (List_1_t3207017302 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m954698987(__this, method) ((  bool (*) (List_1_t3207017302 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m954698987_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PointCloudRegognizer/Point>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3948488878_gshared (List_1_t3207017302 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m3948488878(__this, method) ((  bool (*) (List_1_t3207017302 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3948488878_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PointCloudRegognizer/Point>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m480998681_gshared (List_1_t3207017302 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m480998681(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3207017302 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m480998681_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m197374886_gshared (List_1_t3207017302 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m197374886(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3207017302 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m197374886_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::Add(T)
extern "C"  void List_1_Add_m1748259873_gshared (List_1_t3207017302 * __this, Point_t1838831750  ___item0, const MethodInfo* method);
#define List_1_Add_m1748259873(__this, ___item0, method) ((  void (*) (List_1_t3207017302 *, Point_t1838831750 , const MethodInfo*))List_1_Add_m1748259873_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2993145600_gshared (List_1_t3207017302 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2993145600(__this, ___newCount0, method) ((  void (*) (List_1_t3207017302 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2993145600_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m2133851367_gshared (List_1_t3207017302 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m2133851367(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3207017302 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m2133851367_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m914103806_gshared (List_1_t3207017302 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m914103806(__this, ___collection0, method) ((  void (*) (List_1_t3207017302 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m914103806_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m175076030_gshared (List_1_t3207017302 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m175076030(__this, ___enumerable0, method) ((  void (*) (List_1_t3207017302 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m175076030_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1630115113_gshared (List_1_t3207017302 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m1630115113(__this, ___collection0, method) ((  void (*) (List_1_t3207017302 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m1630115113_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<PointCloudRegognizer/Point>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3395909286 * List_1_AsReadOnly_m1184272972_gshared (List_1_t3207017302 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1184272972(__this, method) ((  ReadOnlyCollection_1_t3395909286 * (*) (List_1_t3207017302 *, const MethodInfo*))List_1_AsReadOnly_m1184272972_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PointCloudRegognizer/Point>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m699588359_gshared (List_1_t3207017302 * __this, Point_t1838831750  ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m699588359(__this, ___item0, method) ((  int32_t (*) (List_1_t3207017302 *, Point_t1838831750 , const MethodInfo*))List_1_BinarySearch_m699588359_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::Clear()
extern "C"  void List_1_Clear_m3754999644_gshared (List_1_t3207017302 * __this, const MethodInfo* method);
#define List_1_Clear_m3754999644(__this, method) ((  void (*) (List_1_t3207017302 *, const MethodInfo*))List_1_Clear_m3754999644_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PointCloudRegognizer/Point>::Contains(T)
extern "C"  bool List_1_Contains_m4065763095_gshared (List_1_t3207017302 * __this, Point_t1838831750  ___item0, const MethodInfo* method);
#define List_1_Contains_m4065763095(__this, ___item0, method) ((  bool (*) (List_1_t3207017302 *, Point_t1838831750 , const MethodInfo*))List_1_Contains_m4065763095_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3812879861_gshared (List_1_t3207017302 * __this, PointU5BU5D_t2313848483* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m3812879861(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3207017302 *, PointU5BU5D_t2313848483*, int32_t, const MethodInfo*))List_1_CopyTo_m3812879861_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<PointCloudRegognizer/Point>::Find(System.Predicate`1<T>)
extern "C"  Point_t1838831750  List_1_Find_m3182465073_gshared (List_1_t3207017302 * __this, Predicate_1_t1449888633 * ___match0, const MethodInfo* method);
#define List_1_Find_m3182465073(__this, ___match0, method) ((  Point_t1838831750  (*) (List_1_t3207017302 *, Predicate_1_t1449888633 *, const MethodInfo*))List_1_Find_m3182465073_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1315050638_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t1449888633 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1315050638(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1449888633 *, const MethodInfo*))List_1_CheckMatch_m1315050638_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<PointCloudRegognizer/Point>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1744127851_gshared (List_1_t3207017302 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t1449888633 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1744127851(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3207017302 *, int32_t, int32_t, Predicate_1_t1449888633 *, const MethodInfo*))List_1_GetIndex_m1744127851_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<PointCloudRegognizer/Point>::GetEnumerator()
extern "C"  Enumerator_t3226690072  List_1_GetEnumerator_m716274900_gshared (List_1_t3207017302 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m716274900(__this, method) ((  Enumerator_t3226690072  (*) (List_1_t3207017302 *, const MethodInfo*))List_1_GetEnumerator_m716274900_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PointCloudRegognizer/Point>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m825742721_gshared (List_1_t3207017302 * __this, Point_t1838831750  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m825742721(__this, ___item0, method) ((  int32_t (*) (List_1_t3207017302 *, Point_t1838831750 , const MethodInfo*))List_1_IndexOf_m825742721_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2631728972_gshared (List_1_t3207017302 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2631728972(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3207017302 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2631728972_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3559247045_gshared (List_1_t3207017302 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3559247045(__this, ___index0, method) ((  void (*) (List_1_t3207017302 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3559247045_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3529179802_gshared (List_1_t3207017302 * __this, int32_t ___index0, Point_t1838831750  ___item1, const MethodInfo* method);
#define List_1_Insert_m3529179802(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3207017302 *, int32_t, Point_t1838831750 , const MethodInfo*))List_1_Insert_m3529179802_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2807097633_gshared (List_1_t3207017302 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2807097633(__this, ___collection0, method) ((  void (*) (List_1_t3207017302 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2807097633_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<PointCloudRegognizer/Point>::Remove(T)
extern "C"  bool List_1_Remove_m973486290_gshared (List_1_t3207017302 * __this, Point_t1838831750  ___item0, const MethodInfo* method);
#define List_1_Remove_m973486290(__this, ___item0, method) ((  bool (*) (List_1_t3207017302 *, Point_t1838831750 , const MethodInfo*))List_1_Remove_m973486290_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PointCloudRegognizer/Point>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1116935812_gshared (List_1_t3207017302 * __this, Predicate_1_t1449888633 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1116935812(__this, ___match0, method) ((  int32_t (*) (List_1_t3207017302 *, Predicate_1_t1449888633 *, const MethodInfo*))List_1_RemoveAll_m1116935812_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2932117362_gshared (List_1_t3207017302 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m2932117362(__this, ___index0, method) ((  void (*) (List_1_t3207017302 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2932117362_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m1460094869_gshared (List_1_t3207017302 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m1460094869(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3207017302 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1460094869_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::Reverse()
extern "C"  void List_1_Reverse_m367495610_gshared (List_1_t3207017302 * __this, const MethodInfo* method);
#define List_1_Reverse_m367495610(__this, method) ((  void (*) (List_1_t3207017302 *, const MethodInfo*))List_1_Reverse_m367495610_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::Sort()
extern "C"  void List_1_Sort_m2995560488_gshared (List_1_t3207017302 * __this, const MethodInfo* method);
#define List_1_Sort_m2995560488(__this, method) ((  void (*) (List_1_t3207017302 *, const MethodInfo*))List_1_Sort_m2995560488_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m2476536508_gshared (List_1_t3207017302 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m2476536508(__this, ___comparer0, method) ((  void (*) (List_1_t3207017302 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m2476536508_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1117429371_gshared (List_1_t3207017302 * __this, Comparison_1_t555192937 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1117429371(__this, ___comparison0, method) ((  void (*) (List_1_t3207017302 *, Comparison_1_t555192937 *, const MethodInfo*))List_1_Sort_m1117429371_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<PointCloudRegognizer/Point>::ToArray()
extern "C"  PointU5BU5D_t2313848483* List_1_ToArray_m1917213459_gshared (List_1_t3207017302 * __this, const MethodInfo* method);
#define List_1_ToArray_m1917213459(__this, method) ((  PointU5BU5D_t2313848483* (*) (List_1_t3207017302 *, const MethodInfo*))List_1_ToArray_m1917213459_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2864513473_gshared (List_1_t3207017302 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2864513473(__this, method) ((  void (*) (List_1_t3207017302 *, const MethodInfo*))List_1_TrimExcess_m2864513473_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PointCloudRegognizer/Point>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1585390897_gshared (List_1_t3207017302 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1585390897(__this, method) ((  int32_t (*) (List_1_t3207017302 *, const MethodInfo*))List_1_get_Capacity_m1585390897_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m203334418_gshared (List_1_t3207017302 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m203334418(__this, ___value0, method) ((  void (*) (List_1_t3207017302 *, int32_t, const MethodInfo*))List_1_set_Capacity_m203334418_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<PointCloudRegognizer/Point>::get_Count()
extern "C"  int32_t List_1_get_Count_m3213738819_gshared (List_1_t3207017302 * __this, const MethodInfo* method);
#define List_1_get_Count_m3213738819(__this, method) ((  int32_t (*) (List_1_t3207017302 *, const MethodInfo*))List_1_get_Count_m3213738819_gshared)(__this, method)
// T System.Collections.Generic.List`1<PointCloudRegognizer/Point>::get_Item(System.Int32)
extern "C"  Point_t1838831750  List_1_get_Item_m1304028082_gshared (List_1_t3207017302 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m1304028082(__this, ___index0, method) ((  Point_t1838831750  (*) (List_1_t3207017302 *, int32_t, const MethodInfo*))List_1_get_Item_m1304028082_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PointCloudRegognizer/Point>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3122316771_gshared (List_1_t3207017302 * __this, int32_t ___index0, Point_t1838831750  ___value1, const MethodInfo* method);
#define List_1_set_Item_m3122316771(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3207017302 *, int32_t, Point_t1838831750 , const MethodInfo*))List_1_set_Item_m3122316771_gshared)(__this, ___index0, ___value1, method)
