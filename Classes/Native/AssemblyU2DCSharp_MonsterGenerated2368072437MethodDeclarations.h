﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MonsterGenerated
struct MonsterGenerated_t2368072437;
// JSVCall
struct JSVCall_t3708497963;
// System.Action
struct Action_t3771233898;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// Monster
struct Monster_t2901270458;
// AICtrl
struct AICtrl_t1930422963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// CombatEntity
struct CombatEntity_t684137495;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_Monster2901270458.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void MonsterGenerated::.ctor()
extern "C"  void MonsterGenerated__ctor_m1082113862 (MonsterGenerated_t2368072437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_Monster1(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_Monster1_m3990454654 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::Monster_summonEffectId(JSVCall)
extern "C"  void MonsterGenerated_Monster_summonEffectId_m2575968919 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::Monster_drop_growup(JSVCall)
extern "C"  void MonsterGenerated_Monster_drop_growup_m1394338250 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::Monster_isHideSummon(JSVCall)
extern "C"  void MonsterGenerated_Monster_isHideSummon_m2355985175 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::Monster_mapEditorType(JSVCall)
extern "C"  void MonsterGenerated_Monster_mapEditorType_m2886424421 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::Monster_aiCtrl(JSVCall)
extern "C"  void MonsterGenerated_Monster_aiCtrl_m3610884913 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::Monster_isInFinish(JSVCall)
extern "C"  void MonsterGenerated_Monster_isInFinish_m672600674 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::Monster_isInNum(JSVCall)
extern "C"  void MonsterGenerated_Monster_isInNum_m2049322769 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::Monster_hp(JSVCall)
extern "C"  void MonsterGenerated_Monster_hp_m3831569276 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_AddEventListenerOnTB(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_AddEventListenerOnTB_m2259077409 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_AGAINATTACK(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_AGAINATTACK_m2645647587 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_AIBeAttacked(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_AIBeAttacked_m1701998873 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_AIMonsterDead__Int32(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_AIMonsterDead__Int32_m1780644287 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_AIOtherDead__ZEvent(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_AIOtherDead__ZEvent_m868834535 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_Answer__ZEvent(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_Answer__ZEvent_m1933540901 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_ATKDEBUFF(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_ATKDEBUFF_m3923124135 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_CalcuFinalProp(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_CalcuFinalProp_m2481933568 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_ClosePlot__ZEvent(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_ClosePlot__ZEvent_m4281518388 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_Create__monstersCfg__JSCLevelMonsterConfig__CombatEntity__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_Create__monstersCfg__JSCLevelMonsterConfig__CombatEntity__Boolean__Boolean_m3021975169 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_Create__Portal__monstersCfg__JSCLevelMonsterConfig(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_Create__Portal__monstersCfg__JSCLevelMonsterConfig_m3713030134 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_Create__monstersCfg__JSCLevelMonsterConfig(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_Create__monstersCfg__JSCLevelMonsterConfig_m3121057930 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_Create__CSHeroUnit__JSCLevelMonsterConfig(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_Create__CSHeroUnit__JSCLevelMonsterConfig_m4238549731 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_MoceCameraEnd__ZEvent(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_MoceCameraEnd__ZEvent_m2089564621 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_OnAttckTime(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_OnAttckTime_m680962066 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_OnDelayTime(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_OnDelayTime_m1400941132 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_OnLevelTime(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_OnLevelTime_m1913396493 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_OnTargetReached(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_OnTargetReached_m1864358301 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_OnTriggerFun__CombatEntity__Int32(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_OnTriggerFun__CombatEntity__Int32_m2905589132 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_OnTriggerFun__Int32(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_OnTriggerFun__Int32_m2698804515 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_OnUseingSkill__Int32(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_OnUseingSkill__Int32_m3065069790 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_ResidualHp__Single(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_ResidualHp__Single_m1207565020 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_ResidualMp__Single(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_ResidualMp__Single_m4041947703 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_SetMosterAttribute__String__Int32(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_SetMosterAttribute__String__Int32_m2232490318 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_Update(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_Update_m4081402544 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action MonsterGenerated::Monster_UpdateTargetPos_GetDelegate_member25_arg1(CSRepresentedObject)
extern "C"  Action_t3771233898 * MonsterGenerated_Monster_UpdateTargetPos_GetDelegate_member25_arg1_m1085711143 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::Monster_UpdateTargetPos__Vector3__Action(JSVCall,System.Int32)
extern "C"  bool MonsterGenerated_Monster_UpdateTargetPos__Vector3__Action_m3589307059 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::__Register()
extern "C"  void MonsterGenerated___Register_m26499393 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action MonsterGenerated::<Monster_UpdateTargetPos__Vector3__Action>m__84()
extern "C"  Action_t3771233898 * MonsterGenerated_U3CMonster_UpdateTargetPos__Vector3__ActionU3Em__84_m1021438910 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MonsterGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t MonsterGenerated_ilo_getObject1_m1260504972 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::ilo_setInt322(System.Int32,System.Int32)
extern "C"  void MonsterGenerated_ilo_setInt322_m2716097711 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void MonsterGenerated_ilo_setBooleanS3_m3919893621 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MonsterGenerated::ilo_get_mapEditorType4(Monster)
extern "C"  int32_t MonsterGenerated_ilo_get_mapEditorType4_m2930943327 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AICtrl MonsterGenerated::ilo_get_aiCtrl5(Monster)
extern "C"  AICtrl_t1930422963 * MonsterGenerated_ilo_get_aiCtrl5_m412731064 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MonsterGenerated::ilo_setObject6(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t MonsterGenerated_ilo_setObject6_m2192634441 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::ilo_set_hp7(Monster,System.Single)
extern "C"  void MonsterGenerated_ilo_set_hp7_m1326353658 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::ilo_AddEventListenerOnTB8(Monster)
extern "C"  void MonsterGenerated_ilo_AddEventListenerOnTB8_m1626936393 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::ilo_AIMonsterDead9(Monster,System.Int32)
extern "C"  void MonsterGenerated_ilo_AIMonsterDead9_m4213574803 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, int32_t ___murderID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MonsterGenerated::ilo_getObject10(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * MonsterGenerated_ilo_getObject10_m760898167 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::ilo_AIOtherDead11(Monster,CEvent.ZEvent)
extern "C"  void MonsterGenerated_ilo_AIOtherDead11_m968644080 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, ZEvent_t3638018500 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::ilo_ATKDEBUFF12(Monster)
extern "C"  void MonsterGenerated_ilo_ATKDEBUFF12_m4030787610 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::ilo_CalcuFinalProp13(Monster)
extern "C"  void MonsterGenerated_ilo_CalcuFinalProp13_m4132496780 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::ilo_ClosePlot14(Monster,CEvent.ZEvent)
extern "C"  void MonsterGenerated_ilo_ClosePlot14_m2992380064 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, ZEvent_t3638018500 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::ilo_getBooleanS15(System.Int32)
extern "C"  bool MonsterGenerated_ilo_getBooleanS15_m3103560915 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::ilo_MoceCameraEnd16(Monster,CEvent.ZEvent)
extern "C"  void MonsterGenerated_ilo_MoceCameraEnd16_m3229625637 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, ZEvent_t3638018500 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::ilo_OnDelayTime17(Monster)
extern "C"  void MonsterGenerated_ilo_OnDelayTime17_m999076272 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::ilo_OnLevelTime18(Monster)
extern "C"  void MonsterGenerated_ilo_OnLevelTime18_m3906395822 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::ilo_OnTargetReached19(Monster)
extern "C"  void MonsterGenerated_ilo_OnTargetReached19_m3329978429 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::ilo_OnTriggerFun20(Monster,CombatEntity,System.Int32)
extern "C"  void MonsterGenerated_ilo_OnTriggerFun20_m3159419819 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, CombatEntity_t684137495 * ___main1, int32_t ___effectId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::ilo_OnTriggerFun21(Monster,System.Int32)
extern "C"  void MonsterGenerated_ilo_OnTriggerFun21_m1770363637 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, int32_t ___effectId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterGenerated::ilo_ResidualHp22(Monster,System.Single)
extern "C"  void MonsterGenerated_ilo_ResidualHp22_m3343163775 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, float ___r_hp1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MonsterGenerated::ilo_getInt3223(System.Int32)
extern "C"  int32_t MonsterGenerated_ilo_getInt3223_m2598624585 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 MonsterGenerated::ilo_getVector3S24(System.Int32)
extern "C"  Vector3_t4282066566  MonsterGenerated_ilo_getVector3S24_m3434092611 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonsterGenerated::ilo_isFunctionS25(System.Int32)
extern "C"  bool MonsterGenerated_ilo_isFunctionS25_m1155450242 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
