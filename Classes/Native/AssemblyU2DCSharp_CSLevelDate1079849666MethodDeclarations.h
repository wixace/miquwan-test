﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSLevelDate
struct CSLevelDate_t1079849666;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"

// System.Void CSLevelDate::.ctor()
extern "C"  void CSLevelDate__ctor_m2737717993 (CSLevelDate_t1079849666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSLevelDate::LoadData(System.String)
extern "C"  void CSLevelDate_LoadData_m3504471799 (CSLevelDate_t1079849666 * __this, String_t* ___jsonStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSLevelDate::UnLoadData()
extern "C"  void CSLevelDate_UnLoadData_m1896885988 (CSLevelDate_t1079849666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CSLevelDate::ilo_DeserializeObject1(System.String)
extern "C"  Il2CppObject * CSLevelDate_ilo_DeserializeObject1_m2934041878 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken CSLevelDate::ilo_get_Item2(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * CSLevelDate_ilo_get_Item2_m556748353 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
