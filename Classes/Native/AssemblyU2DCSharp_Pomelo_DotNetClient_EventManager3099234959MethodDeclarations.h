﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pomelo.DotNetClient.EventManager
struct EventManager_t3099234959;
// Pomelo.DotNetClient.PomeloClient
struct PomeloClient_t4215271137;
// System.String
struct String_t;
// IZUpdate
struct IZUpdate_t3482043738;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_PomeloClient4215271137.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_EventManager3099234959.h"

// System.Void Pomelo.DotNetClient.EventManager::.ctor(Pomelo.DotNetClient.PomeloClient)
extern "C"  void EventManager__ctor_m2465203949 (EventManager_t3099234959 * __this, PomeloClient_t4215271137 * ____pc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.EventManager::AddPush(System.String,System.String)
extern "C"  void EventManager_AddPush_m17331704 (EventManager_t3099234959 * __this, String_t* ___route0, String_t* ___msgString1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.EventManager::AddResponse(System.UInt32,System.UInt32,System.String)
extern "C"  void EventManager_AddResponse_m2582380573 (EventManager_t3099234959 * __this, uint32_t ___id0, uint32_t ___code1, String_t* ___msgString2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.EventManager::Dispose()
extern "C"  void EventManager_Dispose_m2349921226 (EventManager_t3099234959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.EventManager::Dispose(System.Boolean)
extern "C"  void EventManager_Dispose_m1601454017 (EventManager_t3099234959 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.EventManager::ZUpdate()
extern "C"  void EventManager_ZUpdate_m2020167310 (EventManager_t3099234959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.EventManager::ilo_Dispose1(Pomelo.DotNetClient.EventManager,System.Boolean)
extern "C"  void EventManager_ilo_Dispose1_m1610116303 (Il2CppObject * __this /* static, unused */, EventManager_t3099234959 * ____this0, bool ___disposing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.EventManager::ilo_RemoveUpdate2(IZUpdate)
extern "C"  void EventManager_ilo_RemoveUpdate2_m465146467 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.EventManager::ilo_OnResponseMsg3(Pomelo.DotNetClient.PomeloClient,System.UInt32,System.UInt32,System.String)
extern "C"  void EventManager_ilo_OnResponseMsg3_m871288576 (Il2CppObject * __this /* static, unused */, PomeloClient_t4215271137 * ____this0, uint32_t ___id1, uint32_t ___code2, String_t* ___msgString3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
