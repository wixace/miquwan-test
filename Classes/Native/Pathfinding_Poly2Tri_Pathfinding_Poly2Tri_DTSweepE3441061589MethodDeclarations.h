﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Poly2Tri.DTSweepEdgeEvent
struct DTSweepEdgeEvent_t3441061589;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Poly2Tri.DTSweepEdgeEvent::.ctor()
extern "C"  void DTSweepEdgeEvent__ctor_m434092618 (DTSweepEdgeEvent_t3441061589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
