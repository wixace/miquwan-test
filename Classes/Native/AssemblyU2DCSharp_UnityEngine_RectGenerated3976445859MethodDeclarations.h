﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_RectGenerated
struct UnityEngine_RectGenerated_t3976445859;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine_RectGenerated::.ctor()
extern "C"  void UnityEngine_RectGenerated__ctor_m2029677864 (UnityEngine_RectGenerated_t3976445859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectGenerated::.cctor()
extern "C"  void UnityEngine_RectGenerated__cctor_m2308375429 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectGenerated::Rect_Rect1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectGenerated_Rect_Rect1_m293027658 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectGenerated::Rect_Rect2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectGenerated_Rect_Rect2_m1537792139 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectGenerated::Rect_Rect3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectGenerated_Rect_Rect3_m2782556620 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectGenerated::Rect_Rect4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectGenerated_Rect_Rect4_m4027321101 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectGenerated::Rect_x(JSVCall)
extern "C"  void UnityEngine_RectGenerated_Rect_x_m1566216590 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectGenerated::Rect_y(JSVCall)
extern "C"  void UnityEngine_RectGenerated_Rect_y_m1369703085 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectGenerated::Rect_position(JSVCall)
extern "C"  void UnityEngine_RectGenerated_Rect_position_m2918925565 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectGenerated::Rect_center(JSVCall)
extern "C"  void UnityEngine_RectGenerated_Rect_center_m821986129 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectGenerated::Rect_min(JSVCall)
extern "C"  void UnityEngine_RectGenerated_Rect_min_m3087761652 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectGenerated::Rect_max(JSVCall)
extern "C"  void UnityEngine_RectGenerated_Rect_max_m2613335586 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectGenerated::Rect_width(JSVCall)
extern "C"  void UnityEngine_RectGenerated_Rect_width_m2964634624 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectGenerated::Rect_height(JSVCall)
extern "C"  void UnityEngine_RectGenerated_Rect_height_m2364803775 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectGenerated::Rect_size(JSVCall)
extern "C"  void UnityEngine_RectGenerated_Rect_size_m951808293 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectGenerated::Rect_xMin(JSVCall)
extern "C"  void UnityEngine_RectGenerated_Rect_xMin_m3175192204 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectGenerated::Rect_yMin(JSVCall)
extern "C"  void UnityEngine_RectGenerated_Rect_yMin_m2881789197 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectGenerated::Rect_xMax(JSVCall)
extern "C"  void UnityEngine_RectGenerated_Rect_xMax_m2700766138 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectGenerated::Rect_yMax(JSVCall)
extern "C"  void UnityEngine_RectGenerated_Rect_yMax_m2407363131 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectGenerated::Rect_Contains__Vector3__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectGenerated_Rect_Contains__Vector3__Boolean_m1738463580 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectGenerated::Rect_Contains__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectGenerated_Rect_Contains__Vector2_m1068632653 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectGenerated::Rect_Contains__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectGenerated_Rect_Contains__Vector3_m2313397134 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectGenerated::Rect_Equals__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectGenerated_Rect_Equals__Object_m3566940387 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectGenerated::Rect_GetHashCode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectGenerated_Rect_GetHashCode_m2620918286 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectGenerated::Rect_Overlaps__Rect__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectGenerated_Rect_Overlaps__Rect__Boolean_m3654749045 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectGenerated::Rect_Overlaps__Rect(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectGenerated_Rect_Overlaps__Rect_m3971678549 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectGenerated::Rect_Set__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectGenerated_Rect_Set__Single__Single__Single__Single_m1775089471 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectGenerated::Rect_ToString__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectGenerated_Rect_ToString__String_m3771991330 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectGenerated::Rect_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectGenerated_Rect_ToString_m3741165521 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectGenerated::Rect_MinMaxRect__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectGenerated_Rect_MinMaxRect__Single__Single__Single__Single_m807710907 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectGenerated::Rect_NormalizedToPoint__Rect__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectGenerated_Rect_NormalizedToPoint__Rect__Vector2_m1185879026 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectGenerated::Rect_op_Equality__Rect__Rect(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectGenerated_Rect_op_Equality__Rect__Rect_m4264603613 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectGenerated::Rect_op_Inequality__Rect__Rect(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectGenerated_Rect_op_Inequality__Rect__Rect_m3431482434 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectGenerated::Rect_PointToNormalized__Rect__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectGenerated_Rect_PointToNormalized__Rect__Vector2_m2673448398 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectGenerated::__Register()
extern "C"  void UnityEngine_RectGenerated___Register_m3177515167 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RectGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_RectGenerated_ilo_getObject1_m2351261582 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_RectGenerated::ilo_getSingle2(System.Int32)
extern "C"  float UnityEngine_RectGenerated_ilo_getSingle2_m2400707880 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UnityEngine_RectGenerated_ilo_addJSCSRel3_m728783078 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_RectGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_RectGenerated_ilo_getObject4_m1710490112 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void UnityEngine_RectGenerated_ilo_setSingle5_m2408442960 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectGenerated::ilo_changeJSObj6(System.Int32,System.Object)
extern "C"  void UnityEngine_RectGenerated_ilo_changeJSObj6_m997771206 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_RectGenerated::ilo_getVector2S7(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_RectGenerated_ilo_getVector2S7_m1065211298 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectGenerated::ilo_getBooleanS8(System.Int32)
extern "C"  bool UnityEngine_RectGenerated_ilo_getBooleanS8_m1206305667 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectGenerated::ilo_setVector2S9(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_RectGenerated_ilo_setVector2S9_m1505102471 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectGenerated::ilo_setBooleanS10(System.Int32,System.Boolean)
extern "C"  void UnityEngine_RectGenerated_ilo_setBooleanS10_m3594209997 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
