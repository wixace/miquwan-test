﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSHeroUnitGenerated
struct CSHeroUnitGenerated_t4220395521;
// JSVCall
struct JSVCall_t3708497963;
// CSSkillData[]
struct CSSkillDataU5BU5D_t1071816810;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// CSHeroUnit
struct CSHeroUnit_t3764358446;
// System.UInt32[]
struct UInt32U5BU5D_t3230734560;
// herosCfg
struct herosCfg_t3676934635;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CSHeroUnit3764358446.h"

// System.Void CSHeroUnitGenerated::.ctor()
extern "C"  void CSHeroUnitGenerated__ctor_m3904726154 (CSHeroUnitGenerated_t4220395521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSHeroUnitGenerated::CSHeroUnit_CSHeroUnit1(JSVCall,System.Int32)
extern "C"  bool CSHeroUnitGenerated_CSHeroUnit_CSHeroUnit1_m2416087592 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSHeroUnitGenerated::CSHeroUnit_CSHeroUnit2(JSVCall,System.Int32)
extern "C"  bool CSHeroUnitGenerated_CSHeroUnit_CSHeroUnit2_m3660852073 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSHeroUnitGenerated::CSHeroUnit_CSHeroUnit3(JSVCall,System.Int32)
extern "C"  bool CSHeroUnitGenerated_CSHeroUnit_CSHeroUnit3_m610649258 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_uuid(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_uuid_m3848801523 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_NameQty(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_NameQty_m1837848595 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_state(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_state_m2208646221 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_lineup(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_lineup_m2612636575 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_quatily(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_quatily_m3194672031 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_skillList(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_skillList_m4021300239 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_godEquipAwakenSkillIDs(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_godEquipAwakenSkillIDs_m3705053482 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_godEquipAwakenSkillGradeIDs(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_godEquipAwakenSkillGradeIDs_m3386560809 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_atkrange(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_atkrange_m1650017865 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_movespeed(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_movespeed_m3321017192 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_id(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_id_m2686652819 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_lv(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_lv_m2348490244 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_exp(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_exp_m3289033281 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_starLevel(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_starLevel_m2156465580 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_soulLevel(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_soulLevel_m1903726477 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_soulValue(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_soulValue_m3715036224 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_power(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_power_m2366980697 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_location(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_location_m1848809017 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_awakenLv(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_awakenLv_m2520381627 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_attack(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_attack_m2929430278 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_phy_def(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_phy_def_m2392025943 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_mag_def(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_mag_def_m2470821061 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_hp(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_hp_m2125442118 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_residual_hp(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_residual_hp_m3611881020 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_residual_mp(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_residual_mp_m3217058817 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_residual_hpPer(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_residual_hpPer_m2472328371 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_residual_mpPer(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_residual_mpPer_m4239502542 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_hitrate(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_hitrate_m3561785899 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_dodge(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_dodge_m646918087 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_destroy(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_destroy_m2446393604 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_block(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_block_m1376944081 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_crit(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_crit_m1303624116 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_anticrit(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_anticrit_m3334234514 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_crit_hurt(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_crit_hurt_m261289194 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_damage_reflect(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_damage_reflect_m2812040449 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_heal_bonus(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_heal_bonus_m2806181446 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_damage_bonus(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_damage_bonus_m3611557855 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_damage_reduce(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_damage_reduce_m2590744136 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_real_damage(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_real_damage_m2171797646 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_firm(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_firm_m1644572112 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_pure(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_pure_m1831642710 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_stubborn(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_stubborn_m1704837943 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_hitrate_per(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_hitrate_per_m3038402829 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_dodge_per(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_dodge_per_m874041513 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_destory_per(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_destory_per_m71458956 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_block_per(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_block_per_m1308691379 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_crit_per(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_crit_per_m3235683350 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_anticrit_per(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_anticrit_per_m3010602228 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_anger_initial(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_anger_initial_m26766386 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_anger_max(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_anger_max_m1309424146 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_anger_growth(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_anger_growth_m12960815 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_beHurtAnger(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_beHurtAnger_m629046057 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_name(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_name_m231616547 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_unAdd(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_unAdd_m2005231478 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_ResourcesName(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_ResourcesName_m3845058958 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_sexType(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_sexType_m391414526 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_Icon(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_Icon_m1688201685 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_HeadIcon(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_HeadIcon_m4199760853 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_Intelligence(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_Intelligence_m2101915215 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_AttackNature(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_AttackNature_m3759293951 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_HalfWidth(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_HalfWidth_m3334405611 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_AttackType(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_AttackType_m1656731852 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_Country(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_Country_m4216347912 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_HeroType(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_HeroType_m2376217178 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_HeroElement(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_HeroElement_m1869249660 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_AtkEffectDelay(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_AtkEffectDelay_m1132801204 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_IsLucency(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_IsLucency_m3353111919 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_ifattacked(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_ifattacked_m3747480522 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_LucencyTime(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_LucencyTime_m3375051352 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_AiId(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_AiId_m2044764715 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_NextPiece(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_NextPiece_m1010624643 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_PieceCall(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_PieceCall_m1034353906 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_PieceChange(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_PieceChange_m1208018272 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_WithEage(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_WithEage_m1713689038 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_Resume(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_Resume_m4048928417 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_SoundIdleId(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_SoundIdleId_m3602312096 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_SoundWinId(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_SoundWinId_m343390950 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_SoundAttackId(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_SoundAttackId_m3276029548 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_SoundTackId(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_SoundTackId_m3642344767 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_storyID(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_storyID_m829397230 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_Shadow_hover(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_Shadow_hover_m2818404721 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_Shadow_size(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_Shadow_size_m986579998 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_skills(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_skills_m43226700 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_effect_leader(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_effect_leader_m283238439 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::CSHeroUnit_config(JSVCall)
extern "C"  void CSHeroUnitGenerated_CSHeroUnit_config_m882504524 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSHeroUnitGenerated::CSHeroUnit_Init(JSVCall,System.Int32)
extern "C"  bool CSHeroUnitGenerated_CSHeroUnit_Init_m2999093293 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::__Register()
extern "C"  void CSHeroUnitGenerated___Register_m4213137021 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSSkillData[] CSHeroUnitGenerated::<CSHeroUnit_skillList>m__24()
extern "C"  CSSkillDataU5BU5D_t1071816810* CSHeroUnitGenerated_U3CCSHeroUnit_skillListU3Em__24_m1783192345 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] CSHeroUnitGenerated::<CSHeroUnit_godEquipAwakenSkillIDs>m__25()
extern "C"  Int32U5BU5D_t3230847821* CSHeroUnitGenerated_U3CCSHeroUnit_godEquipAwakenSkillIDsU3Em__25_m2222718489 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] CSHeroUnitGenerated::<CSHeroUnit_godEquipAwakenSkillGradeIDs>m__26()
extern "C"  Int32U5BU5D_t3230847821* CSHeroUnitGenerated_U3CCSHeroUnit_godEquipAwakenSkillGradeIDsU3Em__26_m3036498407 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnitGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t CSHeroUnitGenerated_ilo_getObject1_m3774959340 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSHeroUnitGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool CSHeroUnitGenerated_ilo_attachFinalizerObject2_m41115374 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnitGenerated::ilo_getUInt323(System.Int32)
extern "C"  uint32_t CSHeroUnitGenerated_ilo_getUInt323_m81604071 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_addJSCSRel4(System.Int32,System.Object)
extern "C"  void CSHeroUnitGenerated_ilo_addJSCSRel4_m4275219401 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_setStringS5(System.Int32,System.String)
extern "C"  void CSHeroUnitGenerated_ilo_setStringS5_m948653143 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnitGenerated::ilo_getEnum6(System.Int32)
extern "C"  int32_t CSHeroUnitGenerated_ilo_getEnum6_m4175889967 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_setInt327(System.Int32,System.Int32)
extern "C"  void CSHeroUnitGenerated_ilo_setInt327_m523539654 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_setArrayS8(System.Int32,System.Int32,System.Boolean)
extern "C"  void CSHeroUnitGenerated_ilo_setArrayS8_m2279768320 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_setSingle9(System.Int32,System.Single)
extern "C"  void CSHeroUnitGenerated_ilo_setSingle9_m2640493490 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_setUInt3210(System.Int32,System.UInt32)
extern "C"  void CSHeroUnitGenerated_ilo_setUInt3210_m3520130484 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_set_lv11(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnitGenerated_ilo_set_lv11_m1760860282 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_set_exp12(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnitGenerated_ilo_set_exp12_m1785503196 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnitGenerated::ilo_get_soulLevel13(CSHeroUnit)
extern "C"  uint32_t CSHeroUnitGenerated_ilo_get_soulLevel13_m1571569324 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_set_soulLevel14(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnitGenerated_ilo_set_soulLevel14_m1836357458 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_set_power15(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnitGenerated_ilo_set_power15_m1241303367 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_set_location16(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnitGenerated_ilo_set_location16_m956466474 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnitGenerated::ilo_getInt3217(System.Int32)
extern "C"  int32_t CSHeroUnitGenerated_ilo_getInt3217_m3063885774 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnitGenerated::ilo_get_mag_def18(CSHeroUnit)
extern "C"  uint32_t CSHeroUnitGenerated_ilo_get_mag_def18_m2756281465 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_set_residual_hpPer19(CSHeroUnit,System.Int32)
extern "C"  void CSHeroUnitGenerated_ilo_set_residual_hpPer19_m4083050808 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_set_dodge20(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnitGenerated_ilo_set_dodge20_m2434763955 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_set_block21(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnitGenerated_ilo_set_block21_m291351850 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_set_crit22(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnitGenerated_ilo_set_crit22_m1001068202 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnitGenerated::ilo_get_anticrit23(CSHeroUnit)
extern "C"  uint32_t CSHeroUnitGenerated_ilo_get_anticrit23_m2424881770 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnitGenerated::ilo_get_crit_hurt24(CSHeroUnit)
extern "C"  uint32_t CSHeroUnitGenerated_ilo_get_crit_hurt24_m1214020495 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_set_crit_hurt25(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnitGenerated_ilo_set_crit_hurt25_m4271573749 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnitGenerated::ilo_get_heal_bonus26(CSHeroUnit)
extern "C"  uint32_t CSHeroUnitGenerated_ilo_get_heal_bonus26_m3020486969 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_set_heal_bonus27(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnitGenerated_ilo_set_heal_bonus27_m1351342045 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnitGenerated::ilo_get_damage_bonus28(CSHeroUnit)
extern "C"  uint32_t CSHeroUnitGenerated_ilo_get_damage_bonus28_m2890533826 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_set_damage_reduce29(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnitGenerated_ilo_set_damage_reduce29_m1440103515 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnitGenerated::ilo_get_stubborn30(CSHeroUnit)
extern "C"  uint32_t CSHeroUnitGenerated_ilo_get_stubborn30_m3426768385 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_set_destory_per31(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnitGenerated_ilo_set_destory_per31_m3496178862 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSHeroUnitGenerated::ilo_get_block_per32(CSHeroUnit)
extern "C"  uint32_t CSHeroUnitGenerated_ilo_get_block_per32_m3307337475 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_set_anticrit_per33(CSHeroUnit,System.UInt32)
extern "C"  void CSHeroUnitGenerated_ilo_set_anticrit_per33_m291707530 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnitGenerated::ilo_get_anger_growth34(CSHeroUnit)
extern "C"  int32_t CSHeroUnitGenerated_ilo_get_anger_growth34_m3637484986 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSHeroUnitGenerated::ilo_get_name35(CSHeroUnit)
extern "C"  String_t* CSHeroUnitGenerated_ilo_get_name35_m525598482 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSHeroUnitGenerated::ilo_get_ResourcesName36(CSHeroUnit)
extern "C"  String_t* CSHeroUnitGenerated_ilo_get_ResourcesName36_m3885830932 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSHeroUnitGenerated::ilo_get_Icon37(CSHeroUnit)
extern "C"  String_t* CSHeroUnitGenerated_ilo_get_Icon37_m544379298 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_setEnum38(System.Int32,System.Int32)
extern "C"  void CSHeroUnitGenerated_ilo_setEnum38_m2113813225 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnitGenerated::ilo_get_ifattacked39(CSHeroUnit)
extern "C"  int32_t CSHeroUnitGenerated_ilo_get_ifattacked39_m3274723140 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CSHeroUnitGenerated::ilo_get_LucencyTime40(CSHeroUnit)
extern "C"  float CSHeroUnitGenerated_ilo_get_LucencyTime40_m313622156 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnitGenerated::ilo_get_NextPiece41(CSHeroUnit)
extern "C"  int32_t CSHeroUnitGenerated_ilo_get_NextPiece41_m3921015236 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32[] CSHeroUnitGenerated::ilo_get_WithEage42(CSHeroUnit)
extern "C"  UInt32U5BU5D_t3230734560* CSHeroUnitGenerated_ilo_get_WithEage42_m628295565 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroUnitGenerated::ilo_moveSaveID2Arr43(System.Int32)
extern "C"  void CSHeroUnitGenerated_ilo_moveSaveID2Arr43_m328373685 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnitGenerated::ilo_get_SoundIdleId44(CSHeroUnit)
extern "C"  int32_t CSHeroUnitGenerated_ilo_get_SoundIdleId44_m521055242 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnitGenerated::ilo_get_SoundWinId45(CSHeroUnit)
extern "C"  int32_t CSHeroUnitGenerated_ilo_get_SoundWinId45_m1948932419 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// herosCfg CSHeroUnitGenerated::ilo_get_config46(CSHeroUnit)
extern "C"  herosCfg_t3676934635 * CSHeroUnitGenerated_ilo_get_config46_m120306938 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroUnitGenerated::ilo_getArrayLength47(System.Int32)
extern "C"  int32_t CSHeroUnitGenerated_ilo_getArrayLength47_m1353459068 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
