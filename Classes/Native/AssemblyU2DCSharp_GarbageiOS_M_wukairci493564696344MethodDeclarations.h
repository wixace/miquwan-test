﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_wukairci49
struct M_wukairci49_t3564696344;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_wukairci493564696344.h"

// System.Void GarbageiOS.M_wukairci49::.ctor()
extern "C"  void M_wukairci49__ctor_m2606912539 (M_wukairci49_t3564696344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_wukairci49::M_xishea0(System.String[],System.Int32)
extern "C"  void M_wukairci49_M_xishea0_m472135276 (M_wukairci49_t3564696344 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_wukairci49::M_faipealawPuzem1(System.String[],System.Int32)
extern "C"  void M_wukairci49_M_faipealawPuzem1_m4019824310 (M_wukairci49_t3564696344 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_wukairci49::M_fowjexeFaixoodre2(System.String[],System.Int32)
extern "C"  void M_wukairci49_M_fowjexeFaixoodre2_m2945185977 (M_wukairci49_t3564696344 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_wukairci49::M_wacenair3(System.String[],System.Int32)
extern "C"  void M_wukairci49_M_wacenair3_m674221129 (M_wukairci49_t3564696344 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_wukairci49::M_nocusou4(System.String[],System.Int32)
extern "C"  void M_wukairci49_M_nocusou4_m266136896 (M_wukairci49_t3564696344 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_wukairci49::ilo_M_faipealawPuzem11(GarbageiOS.M_wukairci49,System.String[],System.Int32)
extern "C"  void M_wukairci49_ilo_M_faipealawPuzem11_m4173684162 (Il2CppObject * __this /* static, unused */, M_wukairci49_t3564696344 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_wukairci49::ilo_M_wacenair32(GarbageiOS.M_wukairci49,System.String[],System.Int32)
extern "C"  void M_wukairci49_ilo_M_wacenair32_m1784548112 (Il2CppObject * __this /* static, unused */, M_wukairci49_t3564696344 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
