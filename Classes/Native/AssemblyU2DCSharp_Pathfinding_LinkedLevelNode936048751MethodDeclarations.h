﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.LinkedLevelNode
struct LinkedLevelNode_t936048751;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.LinkedLevelNode::.ctor()
extern "C"  void LinkedLevelNode__ctor_m1128085416 (LinkedLevelNode_t936048751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
