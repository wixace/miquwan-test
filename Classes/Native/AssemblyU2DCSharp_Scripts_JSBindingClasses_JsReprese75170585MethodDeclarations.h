﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Scripts.JSBindingClasses.JsRepresentClass/<GeneratedJSFuntion1>c__AnonStoreyFD
struct U3CGeneratedJSFuntion1U3Ec__AnonStoreyFD_t75170585;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Scripts.JSBindingClasses.JsRepresentClass/<GeneratedJSFuntion1>c__AnonStoreyFD::.ctor()
extern "C"  void U3CGeneratedJSFuntion1U3Ec__AnonStoreyFD__ctor_m1095595426 (U3CGeneratedJSFuntion1U3Ec__AnonStoreyFD_t75170585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Scripts.JSBindingClasses.JsRepresentClass/<GeneratedJSFuntion1>c__AnonStoreyFD::<>m__321(System.Object)
extern "C"  Il2CppObject * U3CGeneratedJSFuntion1U3Ec__AnonStoreyFD_U3CU3Em__321_m1593751894 (U3CGeneratedJSFuntion1U3Ec__AnonStoreyFD_t75170585 * __this, Il2CppObject * ___objects0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
