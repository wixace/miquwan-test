﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen3582642444MethodDeclarations.h"

// System.Void System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3615419077(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1822157838 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m2631096295_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m2460450541(__this, ___arg10, method) ((  bool (*) (Func_2_t1822157838 *, KeyValuePair_2_t2834614897 , const MethodInfo*))Func_2_Invoke_m1949974171_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m922213020(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1822157838 *, KeyValuePair_2_t2834614897 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m3011302474_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m104155911(__this, ___result0, method) ((  bool (*) (Func_2_t1822157838 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1761920025_gshared)(__this, ___result0, method)
