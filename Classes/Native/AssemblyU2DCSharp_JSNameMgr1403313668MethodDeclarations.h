﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_String7231557.h"

// System.Void JSNameMgr::.cctor()
extern "C"  void JSNameMgr__cctor_m3334835366 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSNameMgr::GetTypeFileName(System.Type)
extern "C"  String_t* JSNameMgr_GetTypeFileName_m936213050 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSNameMgr::HandleFunctionName(System.String)
extern "C"  String_t* JSNameMgr_HandleFunctionName_m685647901 (Il2CppObject * __this /* static, unused */, String_t* ___functionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSNameMgr::GetTypeFullName(System.Type,System.Boolean)
extern "C"  String_t* JSNameMgr_GetTypeFullName_m2175467286 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, bool ___withT1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSNameMgr::SetJSTypeFullNameMapping(System.String,System.String)
extern "C"  void JSNameMgr_SetJSTypeFullNameMapping_m142872978 (Il2CppObject * __this /* static, unused */, String_t* ___csName0, String_t* ___jsName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSNameMgr::GetJSTypeFullName(System.Type)
extern "C"  String_t* JSNameMgr_GetJSTypeFullName_m426177438 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSNameMgr::ilo_GetTypeFullName1(System.Type,System.Boolean)
extern "C"  String_t* JSNameMgr_ilo_GetTypeFullName1_m2936923704 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, bool ___withT1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
