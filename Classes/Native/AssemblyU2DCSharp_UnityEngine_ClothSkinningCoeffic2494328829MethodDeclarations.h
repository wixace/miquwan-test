﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ClothSkinningCoefficientGenerated
struct UnityEngine_ClothSkinningCoefficientGenerated_t2494328829;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_ClothSkinningCoefficientGenerated::.ctor()
extern "C"  void UnityEngine_ClothSkinningCoefficientGenerated__ctor_m777125390 (UnityEngine_ClothSkinningCoefficientGenerated_t2494328829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothSkinningCoefficientGenerated::.cctor()
extern "C"  void UnityEngine_ClothSkinningCoefficientGenerated__cctor_m2133954399 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ClothSkinningCoefficientGenerated::ClothSkinningCoefficient_ClothSkinningCoefficient1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ClothSkinningCoefficientGenerated_ClothSkinningCoefficient_ClothSkinningCoefficient1_m1190100708 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothSkinningCoefficientGenerated::ClothSkinningCoefficient_maxDistance(JSVCall)
extern "C"  void UnityEngine_ClothSkinningCoefficientGenerated_ClothSkinningCoefficient_maxDistance_m455771885 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothSkinningCoefficientGenerated::ClothSkinningCoefficient_collisionSphereDistance(JSVCall)
extern "C"  void UnityEngine_ClothSkinningCoefficientGenerated_ClothSkinningCoefficient_collisionSphereDistance_m3693558802 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothSkinningCoefficientGenerated::__Register()
extern "C"  void UnityEngine_ClothSkinningCoefficientGenerated___Register_m146937721 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothSkinningCoefficientGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_ClothSkinningCoefficientGenerated_ilo_addJSCSRel1_m4268238282 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothSkinningCoefficientGenerated::ilo_changeJSObj2(System.Int32,System.Object)
extern "C"  void UnityEngine_ClothSkinningCoefficientGenerated_ilo_changeJSObj2_m596925596 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_ClothSkinningCoefficientGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_ClothSkinningCoefficientGenerated_ilo_getSingle3_m2712635459 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
