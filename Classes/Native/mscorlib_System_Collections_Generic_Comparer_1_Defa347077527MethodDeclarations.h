﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<PointCloudRegognizer/Point>
struct DefaultComparer_t347077527;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PointCloudRegognizer_Point1838831750.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<PointCloudRegognizer/Point>::.ctor()
extern "C"  void DefaultComparer__ctor_m2479207052_gshared (DefaultComparer_t347077527 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2479207052(__this, method) ((  void (*) (DefaultComparer_t347077527 *, const MethodInfo*))DefaultComparer__ctor_m2479207052_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<PointCloudRegognizer/Point>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m696616043_gshared (DefaultComparer_t347077527 * __this, Point_t1838831750  ___x0, Point_t1838831750  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m696616043(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t347077527 *, Point_t1838831750 , Point_t1838831750 , const MethodInfo*))DefaultComparer_Compare_m696616043_gshared)(__this, ___x0, ___y1, method)
