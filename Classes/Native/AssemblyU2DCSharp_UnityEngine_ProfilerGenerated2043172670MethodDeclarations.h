﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ProfilerGenerated
struct UnityEngine_ProfilerGenerated_t2043172670;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_ProfilerGenerated::.ctor()
extern "C"  void UnityEngine_ProfilerGenerated__ctor_m4235100141 (UnityEngine_ProfilerGenerated_t2043172670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProfilerGenerated::Profiler_Profiler1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProfilerGenerated_Profiler_Profiler1_m3557468165 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProfilerGenerated::Profiler_supported(JSVCall)
extern "C"  void UnityEngine_ProfilerGenerated_Profiler_supported_m3207924664 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProfilerGenerated::Profiler_logFile(JSVCall)
extern "C"  void UnityEngine_ProfilerGenerated_Profiler_logFile_m1353004486 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProfilerGenerated::Profiler_enableBinaryLog(JSVCall)
extern "C"  void UnityEngine_ProfilerGenerated_Profiler_enableBinaryLog_m2676063078 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProfilerGenerated::Profiler_enabled(JSVCall)
extern "C"  void UnityEngine_ProfilerGenerated_Profiler_enabled_m2809746053 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProfilerGenerated::Profiler_maxNumberOfSamplesPerFrame(JSVCall)
extern "C"  void UnityEngine_ProfilerGenerated_Profiler_maxNumberOfSamplesPerFrame_m2611138993 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProfilerGenerated::Profiler_usedHeapSize(JSVCall)
extern "C"  void UnityEngine_ProfilerGenerated_Profiler_usedHeapSize_m3787498172 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProfilerGenerated::Profiler_AddFramesFromFile__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProfilerGenerated_Profiler_AddFramesFromFile__String_m2943556411 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProfilerGenerated::Profiler_BeginSample__String__UEObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProfilerGenerated_Profiler_BeginSample__String__UEObject_m3855306736 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProfilerGenerated::Profiler_BeginSample__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProfilerGenerated_Profiler_BeginSample__String_m2324115297 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProfilerGenerated::Profiler_EndSample(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProfilerGenerated_Profiler_EndSample_m1423107394 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProfilerGenerated::Profiler_GetMonoHeapSize(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProfilerGenerated_Profiler_GetMonoHeapSize_m2334858659 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProfilerGenerated::Profiler_GetMonoUsedSize(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProfilerGenerated_Profiler_GetMonoUsedSize_m3922044980 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProfilerGenerated::Profiler_GetRuntimeMemorySize__UEObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProfilerGenerated_Profiler_GetRuntimeMemorySize__UEObject_m3288919928 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProfilerGenerated::Profiler_GetTotalAllocatedMemory(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProfilerGenerated_Profiler_GetTotalAllocatedMemory_m961995095 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProfilerGenerated::Profiler_GetTotalReservedMemory(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProfilerGenerated_Profiler_GetTotalReservedMemory_m1152889148 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProfilerGenerated::Profiler_GetTotalUnusedReservedMemory(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProfilerGenerated_Profiler_GetTotalUnusedReservedMemory_m3831494322 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProfilerGenerated::__Register()
extern "C"  void UnityEngine_ProfilerGenerated___Register_m1238821114 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_ProfilerGenerated::ilo_getStringS1(System.Int32)
extern "C"  String_t* UnityEngine_ProfilerGenerated_ilo_getStringS1_m3581224209 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProfilerGenerated::ilo_getBooleanS2(System.Int32)
extern "C"  bool UnityEngine_ProfilerGenerated_ilo_getBooleanS2_m2248548248 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProfilerGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void UnityEngine_ProfilerGenerated_ilo_setBooleanS3_m472178652 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProfilerGenerated::ilo_setInt324(System.Int32,System.Int32)
extern "C"  void UnityEngine_ProfilerGenerated_ilo_setInt324_m807967654 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProfilerGenerated::ilo_setUInt325(System.Int32,System.UInt32)
extern "C"  void UnityEngine_ProfilerGenerated_ilo_setUInt325_m459201929 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
