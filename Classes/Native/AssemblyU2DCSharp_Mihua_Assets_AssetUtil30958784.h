﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,FileInfoRes>
struct Dictionary_2_t2037478168;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_NetworkReachability612403035.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mihua.Assets.AssetUtil
struct  AssetUtil_t30958784  : public Il2CppObject
{
public:
	// UnityEngine.NetworkReachability Mihua.Assets.AssetUtil::network
	int32_t ___network_1;

public:
	inline static int32_t get_offset_of_network_1() { return static_cast<int32_t>(offsetof(AssetUtil_t30958784, ___network_1)); }
	inline int32_t get_network_1() const { return ___network_1; }
	inline int32_t* get_address_of_network_1() { return &___network_1; }
	inline void set_network_1(int32_t value)
	{
		___network_1 = value;
	}
};

struct AssetUtil_t30958784_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,FileInfoRes> Mihua.Assets.AssetUtil::assetsDic
	Dictionary_2_t2037478168 * ___assetsDic_2;
	// System.Text.StringBuilder Mihua.Assets.AssetUtil::sb
	StringBuilder_t243639308 * ___sb_3;

public:
	inline static int32_t get_offset_of_assetsDic_2() { return static_cast<int32_t>(offsetof(AssetUtil_t30958784_StaticFields, ___assetsDic_2)); }
	inline Dictionary_2_t2037478168 * get_assetsDic_2() const { return ___assetsDic_2; }
	inline Dictionary_2_t2037478168 ** get_address_of_assetsDic_2() { return &___assetsDic_2; }
	inline void set_assetsDic_2(Dictionary_2_t2037478168 * value)
	{
		___assetsDic_2 = value;
		Il2CppCodeGenWriteBarrier(&___assetsDic_2, value);
	}

	inline static int32_t get_offset_of_sb_3() { return static_cast<int32_t>(offsetof(AssetUtil_t30958784_StaticFields, ___sb_3)); }
	inline StringBuilder_t243639308 * get_sb_3() const { return ___sb_3; }
	inline StringBuilder_t243639308 ** get_address_of_sb_3() { return &___sb_3; }
	inline void set_sb_3(StringBuilder_t243639308 * value)
	{
		___sb_3 = value;
		Il2CppCodeGenWriteBarrier(&___sb_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
