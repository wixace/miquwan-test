﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RaycastModifier
struct RaycastModifier_t1011176258;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.NavGraph
struct NavGraph_t1254319713;
// Pathfinding.IRaycastableGraph
struct IRaycastableGraph_t2032416694;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_ModifierData4046421655.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_NNInfo1570625892.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"

// System.Void Pathfinding.RaycastModifier::.ctor()
extern "C"  void RaycastModifier__ctor_m3528481333 (RaycastModifier_t1011176258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ModifierData Pathfinding.RaycastModifier::get_input()
extern "C"  int32_t RaycastModifier_get_input_m2763803814 (RaycastModifier_t1011176258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ModifierData Pathfinding.RaycastModifier::get_output()
extern "C"  int32_t RaycastModifier_get_output_m3673060039 (RaycastModifier_t1011176258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RaycastModifier::Apply(Pathfinding.Path,Pathfinding.ModifierData)
extern "C"  void RaycastModifier_Apply_m2406114569 (RaycastModifier_t1011176258 * __this, Path_t1974241691 * ___p0, int32_t ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RaycastModifier::ValidateLine(Pathfinding.GraphNode,Pathfinding.GraphNode,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool RaycastModifier_ValidateLine_m1520948915 (RaycastModifier_t1011176258 * __this, GraphNode_t23612370 * ___n10, GraphNode_t23612370 * ___n21, Vector3_t4282066566  ___v12, Vector3_t4282066566  ___v23, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.RaycastModifier::ilo_GetNearest1(AstarPath,UnityEngine.Vector3)
extern "C"  NNInfo_t1570625892  RaycastModifier_ilo_GetNearest1_m941996735 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, Vector3_t4282066566  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NavGraph Pathfinding.RaycastModifier::ilo_GetGraph2(Pathfinding.GraphNode)
extern "C"  NavGraph_t1254319713 * RaycastModifier_ilo_GetGraph2_m2999430922 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RaycastModifier::ilo_Linecast3(Pathfinding.IRaycastableGraph,UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode)
extern "C"  bool RaycastModifier_ilo_Linecast3_m3372160512 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Vector3_t4282066566  ___start1, Vector3_t4282066566  ___end2, GraphNode_t23612370 * ___hint3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
