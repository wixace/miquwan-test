﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,Pathfinding.Int3>
struct Transform_1_t3971894263;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,Pathfinding.Int3>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2624315403_gshared (Transform_1_t3971894263 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m2624315403(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3971894263 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m2624315403_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,Pathfinding.Int3>::Invoke(TKey,TValue)
extern "C"  Int3_t1974045594  Transform_1_Invoke_m2073222417_gshared (Transform_1_t3971894263 * __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m2073222417(__this, ___key0, ___value1, method) ((  Int3_t1974045594  (*) (Transform_1_t3971894263 *, Int3_t1974045594 , Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m2073222417_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,Pathfinding.Int3>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1745051888_gshared (Transform_1_t3971894263 * __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m1745051888(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3971894263 *, Int3_t1974045594 , Il2CppObject *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m1745051888_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Int3,System.Object,Pathfinding.Int3>::EndInvoke(System.IAsyncResult)
extern "C"  Int3_t1974045594  Transform_1_EndInvoke_m3731332825_gshared (Transform_1_t3971894263 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m3731332825(__this, ___result0, method) ((  Int3_t1974045594  (*) (Transform_1_t3971894263 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3731332825_gshared)(__this, ___result0, method)
