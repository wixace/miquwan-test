﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.LocalAvoidanceMover
struct LocalAvoidanceMover_t2872971530;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.LocalAvoidanceMover::.ctor()
extern "C"  void LocalAvoidanceMover__ctor_m1092675949 (LocalAvoidanceMover_t2872971530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LocalAvoidanceMover::Start()
extern "C"  void LocalAvoidanceMover_Start_m39813741 (LocalAvoidanceMover_t2872971530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LocalAvoidanceMover::Update()
extern "C"  void LocalAvoidanceMover_Update_m1240078144 (LocalAvoidanceMover_t2872971530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
