﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_IPointerExitHandlerGenerated
struct UnityEngine_EventSystems_IPointerExitHandlerGenerated_t4197962244;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_EventSystems_IPointerExitHandlerGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_IPointerExitHandlerGenerated__ctor_m1690536935 (UnityEngine_EventSystems_IPointerExitHandlerGenerated_t4197962244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_IPointerExitHandlerGenerated::IPointerExitHandler_OnPointerExit__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_IPointerExitHandlerGenerated_IPointerExitHandler_OnPointerExit__PointerEventData_m4109780421 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_IPointerExitHandlerGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_IPointerExitHandlerGenerated___Register_m1049381184 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
