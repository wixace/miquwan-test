﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginR2
struct PluginR2_t2499992755;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// VersionMgr
struct VersionMgr_t1322950208;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void PluginR2::.ctor()
extern "C"  void PluginR2__ctor_m3796859720 (PluginR2_t2499992755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginR2::Init()
extern "C"  void PluginR2_Init_m1459489292 (PluginR2_t2499992755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginR2::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginR2_ReqSDKHttpLogin_m3131028125 (PluginR2_t2499992755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginR2::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginR2_IsLoginSuccess_m1651068171 (PluginR2_t2499992755 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginR2::OpenUserLogin()
extern "C"  void PluginR2_OpenUserLogin_m4053587354 (PluginR2_t2499992755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginR2::UserPay(CEvent.ZEvent)
extern "C"  void PluginR2_UserPay_m4094827672 (PluginR2_t2499992755 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginR2::OnLoginSuccess(System.String)
extern "C"  void PluginR2_OnLoginSuccess_m205671501 (PluginR2_t2499992755 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginR2::OnLoginFail(System.String)
extern "C"  void PluginR2_OnLoginFail_m1436962836 (PluginR2_t2499992755 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginR2::OnPaySuccess(System.String)
extern "C"  void PluginR2_OnPaySuccess_m1811504588 (PluginR2_t2499992755 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginR2::OnPayFail(System.String)
extern "C"  void PluginR2_OnPayFail_m1394198261 (PluginR2_t2499992755 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginR2::OnUserSwitchSuccess(System.String)
extern "C"  void PluginR2_OnUserSwitchSuccess_m1681053303 (PluginR2_t2499992755 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginR2::OnLogoutSuccess(System.String)
extern "C"  void PluginR2_OnLogoutSuccess_m2084391906 (PluginR2_t2499992755 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginR2::ilo_get_isSdkLogin1(VersionMgr)
extern "C"  bool PluginR2_ilo_get_isSdkLogin1_m3769712078 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginR2::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginR2_ilo_get_Instance2_m716661194 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginR2::ilo_Log3(System.Object,System.Boolean)
extern "C"  void PluginR2_ilo_Log3_m1202393991 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginR2::ilo_DispatchEvent4(CEvent.ZEvent)
extern "C"  void PluginR2_ilo_DispatchEvent4_m1924546168 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
