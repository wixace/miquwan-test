﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture
struct Texture_t2526458961;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.Shader
struct Shader_t3191267369;
// UnityEngine.Texture[]
struct TextureU5BU5D_t606176076;

#include "AssemblyU2DCSharp_UIBasicSprite2501337439.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UITexture
struct  UITexture_t3903132647  : public UIBasicSprite_t2501337439
{
public:
	// UnityEngine.Rect UITexture::mRect
	Rect_t4241904616  ___mRect_67;
	// UnityEngine.Texture UITexture::mTexture
	Texture_t2526458961 * ___mTexture_68;
	// UnityEngine.Material UITexture::mMat
	Material_t3870600107 * ___mMat_69;
	// UnityEngine.Shader UITexture::mShader
	Shader_t3191267369 * ___mShader_70;
	// UnityEngine.Vector4 UITexture::mBorder
	Vector4_t4282066567  ___mBorder_71;
	// System.Boolean UITexture::mFixedAspect
	bool ___mFixedAspect_72;
	// System.Boolean UITexture::IsMathConvert
	bool ___IsMathConvert_73;
	// System.Int32 UITexture::mPMA
	int32_t ___mPMA_74;
	// UnityEngine.Texture[] UITexture::mDynamicCacheTextures
	TextureU5BU5D_t606176076* ___mDynamicCacheTextures_75;

public:
	inline static int32_t get_offset_of_mRect_67() { return static_cast<int32_t>(offsetof(UITexture_t3903132647, ___mRect_67)); }
	inline Rect_t4241904616  get_mRect_67() const { return ___mRect_67; }
	inline Rect_t4241904616 * get_address_of_mRect_67() { return &___mRect_67; }
	inline void set_mRect_67(Rect_t4241904616  value)
	{
		___mRect_67 = value;
	}

	inline static int32_t get_offset_of_mTexture_68() { return static_cast<int32_t>(offsetof(UITexture_t3903132647, ___mTexture_68)); }
	inline Texture_t2526458961 * get_mTexture_68() const { return ___mTexture_68; }
	inline Texture_t2526458961 ** get_address_of_mTexture_68() { return &___mTexture_68; }
	inline void set_mTexture_68(Texture_t2526458961 * value)
	{
		___mTexture_68 = value;
		Il2CppCodeGenWriteBarrier(&___mTexture_68, value);
	}

	inline static int32_t get_offset_of_mMat_69() { return static_cast<int32_t>(offsetof(UITexture_t3903132647, ___mMat_69)); }
	inline Material_t3870600107 * get_mMat_69() const { return ___mMat_69; }
	inline Material_t3870600107 ** get_address_of_mMat_69() { return &___mMat_69; }
	inline void set_mMat_69(Material_t3870600107 * value)
	{
		___mMat_69 = value;
		Il2CppCodeGenWriteBarrier(&___mMat_69, value);
	}

	inline static int32_t get_offset_of_mShader_70() { return static_cast<int32_t>(offsetof(UITexture_t3903132647, ___mShader_70)); }
	inline Shader_t3191267369 * get_mShader_70() const { return ___mShader_70; }
	inline Shader_t3191267369 ** get_address_of_mShader_70() { return &___mShader_70; }
	inline void set_mShader_70(Shader_t3191267369 * value)
	{
		___mShader_70 = value;
		Il2CppCodeGenWriteBarrier(&___mShader_70, value);
	}

	inline static int32_t get_offset_of_mBorder_71() { return static_cast<int32_t>(offsetof(UITexture_t3903132647, ___mBorder_71)); }
	inline Vector4_t4282066567  get_mBorder_71() const { return ___mBorder_71; }
	inline Vector4_t4282066567 * get_address_of_mBorder_71() { return &___mBorder_71; }
	inline void set_mBorder_71(Vector4_t4282066567  value)
	{
		___mBorder_71 = value;
	}

	inline static int32_t get_offset_of_mFixedAspect_72() { return static_cast<int32_t>(offsetof(UITexture_t3903132647, ___mFixedAspect_72)); }
	inline bool get_mFixedAspect_72() const { return ___mFixedAspect_72; }
	inline bool* get_address_of_mFixedAspect_72() { return &___mFixedAspect_72; }
	inline void set_mFixedAspect_72(bool value)
	{
		___mFixedAspect_72 = value;
	}

	inline static int32_t get_offset_of_IsMathConvert_73() { return static_cast<int32_t>(offsetof(UITexture_t3903132647, ___IsMathConvert_73)); }
	inline bool get_IsMathConvert_73() const { return ___IsMathConvert_73; }
	inline bool* get_address_of_IsMathConvert_73() { return &___IsMathConvert_73; }
	inline void set_IsMathConvert_73(bool value)
	{
		___IsMathConvert_73 = value;
	}

	inline static int32_t get_offset_of_mPMA_74() { return static_cast<int32_t>(offsetof(UITexture_t3903132647, ___mPMA_74)); }
	inline int32_t get_mPMA_74() const { return ___mPMA_74; }
	inline int32_t* get_address_of_mPMA_74() { return &___mPMA_74; }
	inline void set_mPMA_74(int32_t value)
	{
		___mPMA_74 = value;
	}

	inline static int32_t get_offset_of_mDynamicCacheTextures_75() { return static_cast<int32_t>(offsetof(UITexture_t3903132647, ___mDynamicCacheTextures_75)); }
	inline TextureU5BU5D_t606176076* get_mDynamicCacheTextures_75() const { return ___mDynamicCacheTextures_75; }
	inline TextureU5BU5D_t606176076** get_address_of_mDynamicCacheTextures_75() { return &___mDynamicCacheTextures_75; }
	inline void set_mDynamicCacheTextures_75(TextureU5BU5D_t606176076* value)
	{
		___mDynamicCacheTextures_75 = value;
		Il2CppCodeGenWriteBarrier(&___mDynamicCacheTextures_75, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
