﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1499871765MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,skill_upgradeCfg>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m929321355(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2414749176 *, Dictionary_2_t787989725 *, const MethodInfo*))KeyCollection__ctor_m3885369225_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,skill_upgradeCfg>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3230727403(__this, ___item0, method) ((  void (*) (KeyCollection_t2414749176 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m445186093_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,skill_upgradeCfg>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4030161186(__this, method) ((  void (*) (KeyCollection_t2414749176 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m192629988_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,skill_upgradeCfg>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3045889407(__this, ___item0, method) ((  bool (*) (KeyCollection_t2414749176 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2289416641_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,skill_upgradeCfg>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1219386020(__this, ___item0, method) ((  bool (*) (KeyCollection_t2414749176 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4172785510_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,skill_upgradeCfg>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2676476830(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2414749176 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2209143350_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,skill_upgradeCfg>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m1760993940(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2414749176 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3187473238_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,skill_upgradeCfg>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3003785615(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2414749176 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2508903269_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,skill_upgradeCfg>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m813082912(__this, method) ((  bool (*) (KeyCollection_t2414749176 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3987195106_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,skill_upgradeCfg>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1059297746(__this, method) ((  bool (*) (KeyCollection_t2414749176 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3494780948_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,skill_upgradeCfg>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1740735166(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2414749176 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3144129094_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,skill_upgradeCfg>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m4116882496(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2414749176 *, Int32U5BU5D_t3230847821*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2172375614_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,skill_upgradeCfg>::GetEnumerator()
#define KeyCollection_GetEnumerator_m554735459(__this, method) ((  Enumerator_t1402925779  (*) (KeyCollection_t2414749176 *, const MethodInfo*))KeyCollection_GetEnumerator_m2291006859_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,skill_upgradeCfg>::get_Count()
#define KeyCollection_get_Count_m1640466456(__this, method) ((  int32_t (*) (KeyCollection_t2414749176 *, const MethodInfo*))KeyCollection_get_Count_m3431456206_gshared)(__this, method)
