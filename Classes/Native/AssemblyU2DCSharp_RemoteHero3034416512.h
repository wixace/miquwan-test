﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Hero2245658.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoteHero
struct  RemoteHero_t3034416512  : public Hero_t2245658
{
public:
	// System.Single RemoteHero::baseTime
	float ___baseTime_333;

public:
	inline static int32_t get_offset_of_baseTime_333() { return static_cast<int32_t>(offsetof(RemoteHero_t3034416512, ___baseTime_333)); }
	inline float get_baseTime_333() const { return ___baseTime_333; }
	inline float* get_address_of_baseTime_333() { return &___baseTime_333; }
	inline void set_baseTime_333(float value)
	{
		___baseTime_333 = value;
	}
};

struct RemoteHero_t3034416512_StaticFields
{
public:
	// System.Single RemoteHero::ForceMoveCDTime
	float ___ForceMoveCDTime_332;

public:
	inline static int32_t get_offset_of_ForceMoveCDTime_332() { return static_cast<int32_t>(offsetof(RemoteHero_t3034416512_StaticFields, ___ForceMoveCDTime_332)); }
	inline float get_ForceMoveCDTime_332() const { return ___ForceMoveCDTime_332; }
	inline float* get_address_of_ForceMoveCDTime_332() { return &___ForceMoveCDTime_332; }
	inline void set_ForceMoveCDTime_332(float value)
	{
		___ForceMoveCDTime_332 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
