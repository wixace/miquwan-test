﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MouseMove
struct  MouseMove_t2590126582  : public MonoBehaviour_t667441552
{
public:
	// System.Single MouseMove::_sensitivity
	float ____sensitivity_2;
	// UnityEngine.Vector3 MouseMove::_originalPos
	Vector3_t4282066566  ____originalPos_3;

public:
	inline static int32_t get_offset_of__sensitivity_2() { return static_cast<int32_t>(offsetof(MouseMove_t2590126582, ____sensitivity_2)); }
	inline float get__sensitivity_2() const { return ____sensitivity_2; }
	inline float* get_address_of__sensitivity_2() { return &____sensitivity_2; }
	inline void set__sensitivity_2(float value)
	{
		____sensitivity_2 = value;
	}

	inline static int32_t get_offset_of__originalPos_3() { return static_cast<int32_t>(offsetof(MouseMove_t2590126582, ____originalPos_3)); }
	inline Vector3_t4282066566  get__originalPos_3() const { return ____originalPos_3; }
	inline Vector3_t4282066566 * get_address_of__originalPos_3() { return &____originalPos_3; }
	inline void set__originalPos_3(Vector3_t4282066566  value)
	{
		____originalPos_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
