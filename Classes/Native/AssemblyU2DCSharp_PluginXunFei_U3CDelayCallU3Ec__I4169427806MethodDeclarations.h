﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginXunFei/<DelayCall>c__Iterator36
struct U3CDelayCallU3Ec__Iterator36_t4169427806;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PluginXunFei/<DelayCall>c__Iterator36::.ctor()
extern "C"  void U3CDelayCallU3Ec__Iterator36__ctor_m4155746253 (U3CDelayCallU3Ec__Iterator36_t4169427806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PluginXunFei/<DelayCall>c__Iterator36::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayCallU3Ec__Iterator36_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m817075557 (U3CDelayCallU3Ec__Iterator36_t4169427806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PluginXunFei/<DelayCall>c__Iterator36::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayCallU3Ec__Iterator36_System_Collections_IEnumerator_get_Current_m515834617 (U3CDelayCallU3Ec__Iterator36_t4169427806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFei/<DelayCall>c__Iterator36::MoveNext()
extern "C"  bool U3CDelayCallU3Ec__Iterator36_MoveNext_m3033366023 (U3CDelayCallU3Ec__Iterator36_t4169427806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei/<DelayCall>c__Iterator36::Dispose()
extern "C"  void U3CDelayCallU3Ec__Iterator36_Dispose_m3545814218 (U3CDelayCallU3Ec__Iterator36_t4169427806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFei/<DelayCall>c__Iterator36::Reset()
extern "C"  void U3CDelayCallU3Ec__Iterator36_Reset_m1802179194 (U3CDelayCallU3Ec__Iterator36_t4169427806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
