﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSDebugMessages/Message
struct Message_t903789774;

#include "codegen/il2cpp-codegen.h"

// System.Void JSDebugMessages/Message::.ctor()
extern "C"  void Message__ctor_m3155962461 (Message_t903789774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSDebugMessages/Message::.cctor()
extern "C"  void Message__cctor_m2863459568 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSDebugMessages/Message JSDebugMessages/Message::Obtain()
extern "C"  Message_t903789774 * Message_Obtain_m3782345159 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSDebugMessages/Message::Release()
extern "C"  void Message_Release_m370023234 (Message_t903789774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
