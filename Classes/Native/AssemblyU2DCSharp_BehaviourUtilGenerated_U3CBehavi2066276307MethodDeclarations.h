﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtilGenerated/<BehaviourUtil_DelayCallT1__Single__ActionT1_T1__T1>c__AnonStorey4A
struct U3CBehaviourUtil_DelayCallT1__Single__ActionT1_T1__T1U3Ec__AnonStorey4A_t2066276307;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtilGenerated/<BehaviourUtil_DelayCallT1__Single__ActionT1_T1__T1>c__AnonStorey4A::.ctor()
extern "C"  void U3CBehaviourUtil_DelayCallT1__Single__ActionT1_T1__T1U3Ec__AnonStorey4A__ctor_m3061179944 (U3CBehaviourUtil_DelayCallT1__Single__ActionT1_T1__T1U3Ec__AnonStorey4A_t2066276307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BehaviourUtilGenerated/<BehaviourUtil_DelayCallT1__Single__ActionT1_T1__T1>c__AnonStorey4A::<>m__9()
extern "C"  Il2CppObject * U3CBehaviourUtil_DelayCallT1__Single__ActionT1_T1__T1U3Ec__AnonStorey4A_U3CU3Em__9_m3429251391 (U3CBehaviourUtil_DelayCallT1__Single__ActionT1_T1__T1U3Ec__AnonStorey4A_t2066276307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
