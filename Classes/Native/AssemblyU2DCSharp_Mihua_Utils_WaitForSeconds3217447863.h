﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mihua.Utils.WaitForSeconds
struct  WaitForSeconds_t3217447863  : public Il2CppObject
{
public:
	// System.Single Mihua.Utils.WaitForSeconds::<lastSeconds>k__BackingField
	float ___U3ClastSecondsU3Ek__BackingField_0;
	// System.Boolean Mihua.Utils.WaitForSeconds::<ispause>k__BackingField
	bool ___U3CispauseU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3ClastSecondsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t3217447863, ___U3ClastSecondsU3Ek__BackingField_0)); }
	inline float get_U3ClastSecondsU3Ek__BackingField_0() const { return ___U3ClastSecondsU3Ek__BackingField_0; }
	inline float* get_address_of_U3ClastSecondsU3Ek__BackingField_0() { return &___U3ClastSecondsU3Ek__BackingField_0; }
	inline void set_U3ClastSecondsU3Ek__BackingField_0(float value)
	{
		___U3ClastSecondsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CispauseU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WaitForSeconds_t3217447863, ___U3CispauseU3Ek__BackingField_1)); }
	inline bool get_U3CispauseU3Ek__BackingField_1() const { return ___U3CispauseU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CispauseU3Ek__BackingField_1() { return &___U3CispauseU3Ek__BackingField_1; }
	inline void set_U3CispauseU3Ek__BackingField_1(bool value)
	{
		___U3CispauseU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
