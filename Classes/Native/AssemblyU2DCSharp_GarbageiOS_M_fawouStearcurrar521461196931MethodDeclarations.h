﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_fawouStearcurrar52
struct M_fawouStearcurrar52_t1461196931;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_fawouStearcurrar52::.ctor()
extern "C"  void M_fawouStearcurrar52__ctor_m2474487568 (M_fawouStearcurrar52_t1461196931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fawouStearcurrar52::M_mapugar0(System.String[],System.Int32)
extern "C"  void M_fawouStearcurrar52_M_mapugar0_m951310830 (M_fawouStearcurrar52_t1461196931 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fawouStearcurrar52::M_wewelbor1(System.String[],System.Int32)
extern "C"  void M_fawouStearcurrar52_M_wewelbor1_m1098268335 (M_fawouStearcurrar52_t1461196931 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fawouStearcurrar52::M_trasallsalMoorar2(System.String[],System.Int32)
extern "C"  void M_fawouStearcurrar52_M_trasallsalMoorar2_m3767379458 (M_fawouStearcurrar52_t1461196931 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
