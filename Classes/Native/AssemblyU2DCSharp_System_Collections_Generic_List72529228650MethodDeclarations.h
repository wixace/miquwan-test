﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_FindIndex__Int32__Int32__PredicateT1_T>c__AnonStorey84
struct U3CListA1_FindIndex__Int32__Int32__PredicateT1_TU3Ec__AnonStorey84_t2529228650;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_FindIndex__Int32__Int32__PredicateT1_T>c__AnonStorey84::.ctor()
extern "C"  void U3CListA1_FindIndex__Int32__Int32__PredicateT1_TU3Ec__AnonStorey84__ctor_m2942301505 (U3CListA1_FindIndex__Int32__Int32__PredicateT1_TU3Ec__AnonStorey84_t2529228650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Collections_Generic_List77Generated/<ListA1_FindIndex__Int32__Int32__PredicateT1_T>c__AnonStorey84::<>m__A9()
extern "C"  Il2CppObject * U3CListA1_FindIndex__Int32__Int32__PredicateT1_TU3Ec__AnonStorey84_U3CU3Em__A9_m1840327191 (U3CListA1_FindIndex__Int32__Int32__PredicateT1_TU3Ec__AnonStorey84_t2529228650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
