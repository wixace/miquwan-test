﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.GraphNode
struct GraphNode_t23612370;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.PathNode
struct PathNode_t417131581;
// Pathfinding.PathHandler
struct PathHandler_t918952263;
// System.Collections.Generic.Stack`1<Pathfinding.GraphNode>
struct Stack_1_t3122173294;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// Pathfinding.Serialization.GraphSerializationContext
struct GraphSerializationContext_t3256954663;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"
#include "AssemblyU2DCSharp_Pathfinding_PathHandler918952263.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_Serialization_GraphS3256954663.h"

// System.Void Pathfinding.GraphNode::.ctor(AstarPath)
extern "C"  void GraphNode__ctor_m1268883541 (GraphNode_t23612370 * __this, AstarPath_t4090270936 * ___astar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.GraphNode::get_Position()
extern "C"  Int3_t1974045594  GraphNode_get_Position_m1279691426 (GraphNode_t23612370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GraphNode::get_walkable()
extern "C"  bool GraphNode_get_walkable_m604196119 (GraphNode_t23612370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::set_walkable(System.Boolean)
extern "C"  void GraphNode_set_walkable_m3559016950 (GraphNode_t23612370 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.GraphNode::get_tags()
extern "C"  uint32_t GraphNode_get_tags_m3413302790 (GraphNode_t23612370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::set_tags(System.UInt32)
extern "C"  void GraphNode_set_tags_m4205487845 (GraphNode_t23612370 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.GraphNode::get_graphIndex()
extern "C"  uint32_t GraphNode_get_graphIndex_m836164177 (GraphNode_t23612370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::set_graphIndex(System.UInt32)
extern "C"  void GraphNode_set_graphIndex_m3780121530 (GraphNode_t23612370 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.GraphNode::get_id()
extern "C"  uint32_t GraphNode_get_id_m2313836936 (GraphNode_t23612370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::set_id(System.UInt32)
extern "C"  void GraphNode_set_id_m2636531427 (GraphNode_t23612370 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::Destroy()
extern "C"  void GraphNode_Destroy_m1054595005 (GraphNode_t23612370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GraphNode::get_Destroyed()
extern "C"  bool GraphNode_get_Destroyed_m3539410151 (GraphNode_t23612370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.GraphNode::get_NodeIndex()
extern "C"  int32_t GraphNode_get_NodeIndex_m3623766232 (GraphNode_t23612370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.GraphNode::get_Flags()
extern "C"  uint32_t GraphNode_get_Flags_m871801852 (GraphNode_t23612370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::set_Flags(System.UInt32)
extern "C"  void GraphNode_set_Flags_m821088877 (GraphNode_t23612370 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.GraphNode::get_Penalty()
extern "C"  uint32_t GraphNode_get_Penalty_m2845227134 (GraphNode_t23612370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::set_Penalty(System.UInt32)
extern "C"  void GraphNode_set_Penalty_m431382891 (GraphNode_t23612370 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GraphNode::get_Walkable()
extern "C"  bool GraphNode_get_Walkable_m2597660983 (GraphNode_t23612370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::set_Walkable(System.Boolean)
extern "C"  void GraphNode_set_Walkable_m1408265750 (GraphNode_t23612370 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.GraphNode::get_Area()
extern "C"  uint32_t GraphNode_get_Area_m1968839066 (GraphNode_t23612370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::set_Area(System.UInt32)
extern "C"  void GraphNode_set_Area_m2446580689 (GraphNode_t23612370 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.GraphNode::get_GraphIndex()
extern "C"  uint32_t GraphNode_get_GraphIndex_m1000484465 (GraphNode_t23612370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::set_GraphIndex(System.UInt32)
extern "C"  void GraphNode_set_GraphIndex_m1531343770 (GraphNode_t23612370 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.GraphNode::get_Tag()
extern "C"  uint32_t GraphNode_get_Tag_m2990045519 (GraphNode_t23612370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::set_Tag(System.UInt32)
extern "C"  void GraphNode_set_Tag_m1698802618 (GraphNode_t23612370 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::UpdateG(Pathfinding.Path,Pathfinding.PathNode)
extern "C"  void GraphNode_UpdateG_m339279235 (GraphNode_t23612370 * __this, Path_t1974241691 * ___path0, PathNode_t417131581 * ___pathNode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::UpdateRecursiveG(Pathfinding.Path,Pathfinding.PathNode,Pathfinding.PathHandler)
extern "C"  void GraphNode_UpdateRecursiveG_m1582884216 (GraphNode_t23612370 * __this, Path_t1974241691 * ___path0, PathNode_t417131581 * ___pathNode1, PathHandler_t918952263 * ___handler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::FloodFill(System.Collections.Generic.Stack`1<Pathfinding.GraphNode>,System.UInt32)
extern "C"  void GraphNode_FloodFill_m3028639736 (GraphNode_t23612370 * __this, Stack_1_t3122173294 * ___stack0, uint32_t ___region1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GraphNode::ContainsConnection(Pathfinding.GraphNode)
extern "C"  bool GraphNode_ContainsConnection_m3919275396 (GraphNode_t23612370 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::RecalculateConnectionCosts()
extern "C"  void GraphNode_RecalculateConnectionCosts_m2977304116 (GraphNode_t23612370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GraphNode::GetPortal(Pathfinding.GraphNode,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Boolean)
extern "C"  bool GraphNode_GetPortal_m2362385802 (GraphNode_t23612370 * __this, GraphNode_t23612370 * ___other0, List_1_t1355284822 * ___left1, List_1_t1355284822 * ___right2, bool ___backwards3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::SerializeNode(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void GraphNode_SerializeNode_m865067304 (GraphNode_t23612370 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::DeserializeNode(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void GraphNode_DeserializeNode_m2401042921 (GraphNode_t23612370 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::SerializeReferences(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void GraphNode_SerializeReferences_m79345122 (GraphNode_t23612370 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::DeserializeReferences(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void GraphNode_DeserializeReferences_m1803715043 (GraphNode_t23612370 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.GraphNode::ilo_get_GraphIndex1(Pathfinding.GraphNode)
extern "C"  uint32_t GraphNode_ilo_get_GraphIndex1_m760514845 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::ilo_ClearConnections2(Pathfinding.GraphNode,System.Boolean)
extern "C"  void GraphNode_ilo_ClearConnections2_m499768747 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, bool ___alsoReverse1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::ilo_DestroyNode3(AstarPath,Pathfinding.GraphNode)
extern "C"  void GraphNode_ilo_DestroyNode3_m2851031575 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.GraphNode::ilo_get_cost4(Pathfinding.PathNode)
extern "C"  uint32_t GraphNode_ilo_get_cost4_m1236624440 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.GraphNode::ilo_GetTraversalCost5(Pathfinding.Path,Pathfinding.GraphNode)
extern "C"  uint32_t GraphNode_ilo_GetTraversalCost5_m3357777442 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.GraphNode::ilo_get_Penalty6(Pathfinding.GraphNode)
extern "C"  uint32_t GraphNode_ilo_get_Penalty6_m4218996159 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.GraphNode::ilo_get_Flags7(Pathfinding.GraphNode)
extern "C"  uint32_t GraphNode_ilo_get_Flags7_m3266716892 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::ilo_set_Penalty8(Pathfinding.GraphNode,System.UInt32)
extern "C"  void GraphNode_ilo_set_Penalty8_m2570507106 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode::ilo_set_Flags9(Pathfinding.GraphNode,System.UInt32)
extern "C"  void GraphNode_ilo_set_Flags9_m2755224575 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
