﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3<System.Object,System.Object,System.Object>
struct U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CCreateSelectManyIteratorU3Ec__Iterator14_3__ctor_m1163605141_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702 * __this, const MethodInfo* method);
#define U3CCreateSelectManyIteratorU3Ec__Iterator14_3__ctor_m1163605141(__this, method) ((  void (*) (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702 *, const MethodInfo*))U3CCreateSelectManyIteratorU3Ec__Iterator14_3__ctor_m1163605141_gshared)(__this, method)
// TResult System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3<System.Object,System.Object,System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3374259554_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702 * __this, const MethodInfo* method);
#define U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3374259554(__this, method) ((  Il2CppObject * (*) (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702 *, const MethodInfo*))U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3374259554_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3<System.Object,System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_IEnumerator_get_Current_m755053297_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702 * __this, const MethodInfo* method);
#define U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_IEnumerator_get_Current_m755053297(__this, method) ((  Il2CppObject * (*) (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702 *, const MethodInfo*))U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_IEnumerator_get_Current_m755053297_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3<System.Object,System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_IEnumerable_GetEnumerator_m2180275532_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702 * __this, const MethodInfo* method);
#define U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_IEnumerable_GetEnumerator_m2180275532(__this, method) ((  Il2CppObject * (*) (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702 *, const MethodInfo*))U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_IEnumerable_GetEnumerator_m2180275532_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3<System.Object,System.Object,System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1881075647_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702 * __this, const MethodInfo* method);
#define U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1881075647(__this, method) ((  Il2CppObject* (*) (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702 *, const MethodInfo*))U3CCreateSelectManyIteratorU3Ec__Iterator14_3_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1881075647_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3<System.Object,System.Object,System.Object>::MoveNext()
extern "C"  bool U3CCreateSelectManyIteratorU3Ec__Iterator14_3_MoveNext_m3291351903_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702 * __this, const MethodInfo* method);
#define U3CCreateSelectManyIteratorU3Ec__Iterator14_3_MoveNext_m3291351903(__this, method) ((  bool (*) (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702 *, const MethodInfo*))U3CCreateSelectManyIteratorU3Ec__Iterator14_3_MoveNext_m3291351903_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3<System.Object,System.Object,System.Object>::Dispose()
extern "C"  void U3CCreateSelectManyIteratorU3Ec__Iterator14_3_Dispose_m1431326610_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702 * __this, const MethodInfo* method);
#define U3CCreateSelectManyIteratorU3Ec__Iterator14_3_Dispose_m1431326610(__this, method) ((  void (*) (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702 *, const MethodInfo*))U3CCreateSelectManyIteratorU3Ec__Iterator14_3_Dispose_m1431326610_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3<System.Object,System.Object,System.Object>::Reset()
extern "C"  void U3CCreateSelectManyIteratorU3Ec__Iterator14_3_Reset_m3105005378_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702 * __this, const MethodInfo* method);
#define U3CCreateSelectManyIteratorU3Ec__Iterator14_3_Reset_m3105005378(__this, method) ((  void (*) (U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702 *, const MethodInfo*))U3CCreateSelectManyIteratorU3Ec__Iterator14_3_Reset_m3105005378_gshared)(__this, method)
