﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_salcu233
struct  M_salcu233_t251942034  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_salcu233::_faryaLearvoupal
	int32_t ____faryaLearvoupal_0;
	// System.UInt32 GarbageiOS.M_salcu233::_qirjerfouBijas
	uint32_t ____qirjerfouBijas_1;
	// System.Boolean GarbageiOS.M_salcu233::_viwhuna
	bool ____viwhuna_2;
	// System.Int32 GarbageiOS.M_salcu233::_callis
	int32_t ____callis_3;
	// System.Int32 GarbageiOS.M_salcu233::_jeekotiBorko
	int32_t ____jeekotiBorko_4;
	// System.Single GarbageiOS.M_salcu233::_mousar
	float ____mousar_5;
	// System.Boolean GarbageiOS.M_salcu233::_derdrirsaFisse
	bool ____derdrirsaFisse_6;
	// System.Single GarbageiOS.M_salcu233::_bayzeedayLealeesai
	float ____bayzeedayLealeesai_7;
	// System.String GarbageiOS.M_salcu233::_fare
	String_t* ____fare_8;
	// System.UInt32 GarbageiOS.M_salcu233::_wirisfaiNegirkas
	uint32_t ____wirisfaiNegirkas_9;
	// System.UInt32 GarbageiOS.M_salcu233::_mesounai
	uint32_t ____mesounai_10;
	// System.Boolean GarbageiOS.M_salcu233::_tawe
	bool ____tawe_11;
	// System.String GarbageiOS.M_salcu233::_hosou
	String_t* ____hosou_12;
	// System.Single GarbageiOS.M_salcu233::_chojojaw
	float ____chojojaw_13;
	// System.Single GarbageiOS.M_salcu233::_toumekaGoolekair
	float ____toumekaGoolekair_14;
	// System.String GarbageiOS.M_salcu233::_rahur
	String_t* ____rahur_15;
	// System.Boolean GarbageiOS.M_salcu233::_tage
	bool ____tage_16;
	// System.Single GarbageiOS.M_salcu233::_koogi
	float ____koogi_17;
	// System.Boolean GarbageiOS.M_salcu233::_kakerpaiGaicesere
	bool ____kakerpaiGaicesere_18;
	// System.UInt32 GarbageiOS.M_salcu233::_ligisharGouso
	uint32_t ____ligisharGouso_19;
	// System.Single GarbageiOS.M_salcu233::_nemawte
	float ____nemawte_20;

public:
	inline static int32_t get_offset_of__faryaLearvoupal_0() { return static_cast<int32_t>(offsetof(M_salcu233_t251942034, ____faryaLearvoupal_0)); }
	inline int32_t get__faryaLearvoupal_0() const { return ____faryaLearvoupal_0; }
	inline int32_t* get_address_of__faryaLearvoupal_0() { return &____faryaLearvoupal_0; }
	inline void set__faryaLearvoupal_0(int32_t value)
	{
		____faryaLearvoupal_0 = value;
	}

	inline static int32_t get_offset_of__qirjerfouBijas_1() { return static_cast<int32_t>(offsetof(M_salcu233_t251942034, ____qirjerfouBijas_1)); }
	inline uint32_t get__qirjerfouBijas_1() const { return ____qirjerfouBijas_1; }
	inline uint32_t* get_address_of__qirjerfouBijas_1() { return &____qirjerfouBijas_1; }
	inline void set__qirjerfouBijas_1(uint32_t value)
	{
		____qirjerfouBijas_1 = value;
	}

	inline static int32_t get_offset_of__viwhuna_2() { return static_cast<int32_t>(offsetof(M_salcu233_t251942034, ____viwhuna_2)); }
	inline bool get__viwhuna_2() const { return ____viwhuna_2; }
	inline bool* get_address_of__viwhuna_2() { return &____viwhuna_2; }
	inline void set__viwhuna_2(bool value)
	{
		____viwhuna_2 = value;
	}

	inline static int32_t get_offset_of__callis_3() { return static_cast<int32_t>(offsetof(M_salcu233_t251942034, ____callis_3)); }
	inline int32_t get__callis_3() const { return ____callis_3; }
	inline int32_t* get_address_of__callis_3() { return &____callis_3; }
	inline void set__callis_3(int32_t value)
	{
		____callis_3 = value;
	}

	inline static int32_t get_offset_of__jeekotiBorko_4() { return static_cast<int32_t>(offsetof(M_salcu233_t251942034, ____jeekotiBorko_4)); }
	inline int32_t get__jeekotiBorko_4() const { return ____jeekotiBorko_4; }
	inline int32_t* get_address_of__jeekotiBorko_4() { return &____jeekotiBorko_4; }
	inline void set__jeekotiBorko_4(int32_t value)
	{
		____jeekotiBorko_4 = value;
	}

	inline static int32_t get_offset_of__mousar_5() { return static_cast<int32_t>(offsetof(M_salcu233_t251942034, ____mousar_5)); }
	inline float get__mousar_5() const { return ____mousar_5; }
	inline float* get_address_of__mousar_5() { return &____mousar_5; }
	inline void set__mousar_5(float value)
	{
		____mousar_5 = value;
	}

	inline static int32_t get_offset_of__derdrirsaFisse_6() { return static_cast<int32_t>(offsetof(M_salcu233_t251942034, ____derdrirsaFisse_6)); }
	inline bool get__derdrirsaFisse_6() const { return ____derdrirsaFisse_6; }
	inline bool* get_address_of__derdrirsaFisse_6() { return &____derdrirsaFisse_6; }
	inline void set__derdrirsaFisse_6(bool value)
	{
		____derdrirsaFisse_6 = value;
	}

	inline static int32_t get_offset_of__bayzeedayLealeesai_7() { return static_cast<int32_t>(offsetof(M_salcu233_t251942034, ____bayzeedayLealeesai_7)); }
	inline float get__bayzeedayLealeesai_7() const { return ____bayzeedayLealeesai_7; }
	inline float* get_address_of__bayzeedayLealeesai_7() { return &____bayzeedayLealeesai_7; }
	inline void set__bayzeedayLealeesai_7(float value)
	{
		____bayzeedayLealeesai_7 = value;
	}

	inline static int32_t get_offset_of__fare_8() { return static_cast<int32_t>(offsetof(M_salcu233_t251942034, ____fare_8)); }
	inline String_t* get__fare_8() const { return ____fare_8; }
	inline String_t** get_address_of__fare_8() { return &____fare_8; }
	inline void set__fare_8(String_t* value)
	{
		____fare_8 = value;
		Il2CppCodeGenWriteBarrier(&____fare_8, value);
	}

	inline static int32_t get_offset_of__wirisfaiNegirkas_9() { return static_cast<int32_t>(offsetof(M_salcu233_t251942034, ____wirisfaiNegirkas_9)); }
	inline uint32_t get__wirisfaiNegirkas_9() const { return ____wirisfaiNegirkas_9; }
	inline uint32_t* get_address_of__wirisfaiNegirkas_9() { return &____wirisfaiNegirkas_9; }
	inline void set__wirisfaiNegirkas_9(uint32_t value)
	{
		____wirisfaiNegirkas_9 = value;
	}

	inline static int32_t get_offset_of__mesounai_10() { return static_cast<int32_t>(offsetof(M_salcu233_t251942034, ____mesounai_10)); }
	inline uint32_t get__mesounai_10() const { return ____mesounai_10; }
	inline uint32_t* get_address_of__mesounai_10() { return &____mesounai_10; }
	inline void set__mesounai_10(uint32_t value)
	{
		____mesounai_10 = value;
	}

	inline static int32_t get_offset_of__tawe_11() { return static_cast<int32_t>(offsetof(M_salcu233_t251942034, ____tawe_11)); }
	inline bool get__tawe_11() const { return ____tawe_11; }
	inline bool* get_address_of__tawe_11() { return &____tawe_11; }
	inline void set__tawe_11(bool value)
	{
		____tawe_11 = value;
	}

	inline static int32_t get_offset_of__hosou_12() { return static_cast<int32_t>(offsetof(M_salcu233_t251942034, ____hosou_12)); }
	inline String_t* get__hosou_12() const { return ____hosou_12; }
	inline String_t** get_address_of__hosou_12() { return &____hosou_12; }
	inline void set__hosou_12(String_t* value)
	{
		____hosou_12 = value;
		Il2CppCodeGenWriteBarrier(&____hosou_12, value);
	}

	inline static int32_t get_offset_of__chojojaw_13() { return static_cast<int32_t>(offsetof(M_salcu233_t251942034, ____chojojaw_13)); }
	inline float get__chojojaw_13() const { return ____chojojaw_13; }
	inline float* get_address_of__chojojaw_13() { return &____chojojaw_13; }
	inline void set__chojojaw_13(float value)
	{
		____chojojaw_13 = value;
	}

	inline static int32_t get_offset_of__toumekaGoolekair_14() { return static_cast<int32_t>(offsetof(M_salcu233_t251942034, ____toumekaGoolekair_14)); }
	inline float get__toumekaGoolekair_14() const { return ____toumekaGoolekair_14; }
	inline float* get_address_of__toumekaGoolekair_14() { return &____toumekaGoolekair_14; }
	inline void set__toumekaGoolekair_14(float value)
	{
		____toumekaGoolekair_14 = value;
	}

	inline static int32_t get_offset_of__rahur_15() { return static_cast<int32_t>(offsetof(M_salcu233_t251942034, ____rahur_15)); }
	inline String_t* get__rahur_15() const { return ____rahur_15; }
	inline String_t** get_address_of__rahur_15() { return &____rahur_15; }
	inline void set__rahur_15(String_t* value)
	{
		____rahur_15 = value;
		Il2CppCodeGenWriteBarrier(&____rahur_15, value);
	}

	inline static int32_t get_offset_of__tage_16() { return static_cast<int32_t>(offsetof(M_salcu233_t251942034, ____tage_16)); }
	inline bool get__tage_16() const { return ____tage_16; }
	inline bool* get_address_of__tage_16() { return &____tage_16; }
	inline void set__tage_16(bool value)
	{
		____tage_16 = value;
	}

	inline static int32_t get_offset_of__koogi_17() { return static_cast<int32_t>(offsetof(M_salcu233_t251942034, ____koogi_17)); }
	inline float get__koogi_17() const { return ____koogi_17; }
	inline float* get_address_of__koogi_17() { return &____koogi_17; }
	inline void set__koogi_17(float value)
	{
		____koogi_17 = value;
	}

	inline static int32_t get_offset_of__kakerpaiGaicesere_18() { return static_cast<int32_t>(offsetof(M_salcu233_t251942034, ____kakerpaiGaicesere_18)); }
	inline bool get__kakerpaiGaicesere_18() const { return ____kakerpaiGaicesere_18; }
	inline bool* get_address_of__kakerpaiGaicesere_18() { return &____kakerpaiGaicesere_18; }
	inline void set__kakerpaiGaicesere_18(bool value)
	{
		____kakerpaiGaicesere_18 = value;
	}

	inline static int32_t get_offset_of__ligisharGouso_19() { return static_cast<int32_t>(offsetof(M_salcu233_t251942034, ____ligisharGouso_19)); }
	inline uint32_t get__ligisharGouso_19() const { return ____ligisharGouso_19; }
	inline uint32_t* get_address_of__ligisharGouso_19() { return &____ligisharGouso_19; }
	inline void set__ligisharGouso_19(uint32_t value)
	{
		____ligisharGouso_19 = value;
	}

	inline static int32_t get_offset_of__nemawte_20() { return static_cast<int32_t>(offsetof(M_salcu233_t251942034, ____nemawte_20)); }
	inline float get__nemawte_20() const { return ____nemawte_20; }
	inline float* get_address_of__nemawte_20() { return &____nemawte_20; }
	inline void set__nemawte_20(float value)
	{
		____nemawte_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
