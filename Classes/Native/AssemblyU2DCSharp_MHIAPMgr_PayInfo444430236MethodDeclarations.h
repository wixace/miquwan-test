﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MHIAPMgr.PayInfo
struct PayInfo_t444430236;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MHIAPMgr_PayInfo_SdkType483686632.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_MHIAPMgr_PayInfo444430236.h"

// System.Void MHIAPMgr.PayInfo::.ctor()
extern "C"  void PayInfo__ctor_m438289784 (PayInfo_t444430236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MHIAPMgr.PayInfo::set_sdkType(MHIAPMgr.PayInfo/SdkType)
extern "C"  void PayInfo_set_sdkType_m2022256901 (PayInfo_t444430236 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MHIAPMgr.PayInfo/SdkType MHIAPMgr.PayInfo::get_sdkType()
extern "C"  int32_t PayInfo_get_sdkType_m3919710572 (PayInfo_t444430236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MHIAPMgr.PayInfo::set_mainInfo(System.String)
extern "C"  void PayInfo_set_mainInfo_m1906956050 (PayInfo_t444430236 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MHIAPMgr.PayInfo::get_mainInfo()
extern "C"  String_t* PayInfo_get_mainInfo_m3958696095 (PayInfo_t444430236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MHIAPMgr.PayInfo::set_roleid(System.String)
extern "C"  void PayInfo_set_roleid_m83954120 (PayInfo_t444430236 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MHIAPMgr.PayInfo::get_roleid()
extern "C"  String_t* PayInfo_get_roleid_m622880233 (PayInfo_t444430236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MHIAPMgr.PayInfo::set_serverId(System.String)
extern "C"  void PayInfo_set_serverId_m3015784571 (PayInfo_t444430236 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MHIAPMgr.PayInfo::get_serverId()
extern "C"  String_t* PayInfo_get_serverId_m1959686294 (PayInfo_t444430236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MHIAPMgr.PayInfo::set_type(System.String)
extern "C"  void PayInfo_set_type_m1312301439 (PayInfo_t444430236 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MHIAPMgr.PayInfo::get_type()
extern "C"  String_t* PayInfo_get_type_m3758874770 (PayInfo_t444430236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MHIAPMgr.PayInfo::set_amount(System.String)
extern "C"  void PayInfo_set_amount_m3189869217 (PayInfo_t444430236 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MHIAPMgr.PayInfo::get_amount()
extern "C"  String_t* PayInfo_get_amount_m3680663664 (PayInfo_t444430236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MHIAPMgr.PayInfo::set_orderId(System.String)
extern "C"  void PayInfo_set_orderId_m3907712032 (PayInfo_t444430236 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MHIAPMgr.PayInfo::get_orderId()
extern "C"  String_t* PayInfo_get_orderId_m1392780691 (PayInfo_t444430236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MHIAPMgr.PayInfo::set_receiptData(System.String)
extern "C"  void PayInfo_set_receiptData_m3829705223 (PayInfo_t444430236 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MHIAPMgr.PayInfo::get_receiptData()
extern "C"  String_t* PayInfo_get_receiptData_m863814604 (PayInfo_t444430236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MHIAPMgr.PayInfo::set_pcbUrl(System.String)
extern "C"  void PayInfo_set_pcbUrl_m2700794841 (PayInfo_t444430236 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MHIAPMgr.PayInfo::get_pcbUrl()
extern "C"  String_t* PayInfo_get_pcbUrl_m3366292536 (PayInfo_t444430236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MHIAPMgr.PayInfo::set_environment(System.String)
extern "C"  void PayInfo_set_environment_m1804283350 (PayInfo_t444430236 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MHIAPMgr.PayInfo::get_environment()
extern "C"  String_t* PayInfo_get_environment_m293682077 (PayInfo_t444430236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MHIAPMgr.PayInfo::set_appKey(System.String)
extern "C"  void PayInfo_set_appKey_m2493701563 (PayInfo_t444430236 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MHIAPMgr.PayInfo::get_appKey()
extern "C"  String_t* PayInfo_get_appKey_m2037785366 (PayInfo_t444430236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MHIAPMgr.PayInfo::set_eargs(System.String)
extern "C"  void PayInfo_set_eargs_m2445546311 (PayInfo_t444430236 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MHIAPMgr.PayInfo::get_eargs()
extern "C"  String_t* PayInfo_get_eargs_m3743193932 (PayInfo_t444430236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MHIAPMgr.PayInfo::set_transactionId(System.String)
extern "C"  void PayInfo_set_transactionId_m517414032 (PayInfo_t444430236 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MHIAPMgr.PayInfo::get_transactionId()
extern "C"  String_t* PayInfo_get_transactionId_m2639064675 (PayInfo_t444430236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MHIAPMgr.PayInfo::Parse(System.String)
extern "C"  void PayInfo_Parse_m1610981657 (PayInfo_t444430236 * __this, String_t* ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm MHIAPMgr.PayInfo::ToWWWForm()
extern "C"  WWWForm_t461342257 * PayInfo_ToWWWForm_m1602629447 (PayInfo_t444430236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MHIAPMgr.PayInfo::CanRequest()
extern "C"  bool PayInfo_CanRequest_m2139112735 (PayInfo_t444430236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MHIAPMgr.PayInfo::OnRequest()
extern "C"  void PayInfo_OnRequest_m336797638 (PayInfo_t444430236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MHIAPMgr.PayInfo::ilo_set_sdkType1(MHIAPMgr.PayInfo,MHIAPMgr.PayInfo/SdkType)
extern "C"  void PayInfo_ilo_set_sdkType1_m4058000520 (Il2CppObject * __this /* static, unused */, PayInfo_t444430236 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MHIAPMgr.PayInfo/SdkType MHIAPMgr.PayInfo::ilo_get_sdkType2(MHIAPMgr.PayInfo)
extern "C"  int32_t PayInfo_ilo_get_sdkType2_m23149176 (Il2CppObject * __this /* static, unused */, PayInfo_t444430236 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MHIAPMgr.PayInfo::ilo_set_environment3(MHIAPMgr.PayInfo,System.String)
extern "C"  void PayInfo_ilo_set_environment3_m2456315509 (Il2CppObject * __this /* static, unused */, PayInfo_t444430236 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MHIAPMgr.PayInfo::ilo_set_roleid4(MHIAPMgr.PayInfo,System.String)
extern "C"  void PayInfo_ilo_set_roleid4_m1023919266 (Il2CppObject * __this /* static, unused */, PayInfo_t444430236 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MHIAPMgr.PayInfo::ilo_set_type5(MHIAPMgr.PayInfo,System.String)
extern "C"  void PayInfo_ilo_set_type5_m331689946 (Il2CppObject * __this /* static, unused */, PayInfo_t444430236 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MHIAPMgr.PayInfo::ilo_set_amount6(MHIAPMgr.PayInfo,System.String)
extern "C"  void PayInfo_ilo_set_amount6_m1881446717 (Il2CppObject * __this /* static, unused */, PayInfo_t444430236 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MHIAPMgr.PayInfo::ilo_set_orderId7(MHIAPMgr.PayInfo,System.String)
extern "C"  void PayInfo_ilo_set_orderId7_m2527801283 (Il2CppObject * __this /* static, unused */, PayInfo_t444430236 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MHIAPMgr.PayInfo::ilo_get_roleid8(MHIAPMgr.PayInfo)
extern "C"  String_t* PayInfo_ilo_get_roleid8_m2659424475 (Il2CppObject * __this /* static, unused */, PayInfo_t444430236 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MHIAPMgr.PayInfo::ilo_get_serverId9(MHIAPMgr.PayInfo)
extern "C"  String_t* PayInfo_ilo_get_serverId9_m28264527 (Il2CppObject * __this /* static, unused */, PayInfo_t444430236 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MHIAPMgr.PayInfo::ilo_get_type10(MHIAPMgr.PayInfo)
extern "C"  String_t* PayInfo_ilo_get_type10_m2752971585 (Il2CppObject * __this /* static, unused */, PayInfo_t444430236 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MHIAPMgr.PayInfo::ilo_get_eargs11(MHIAPMgr.PayInfo)
extern "C"  String_t* PayInfo_ilo_get_eargs11_m2739020342 (Il2CppObject * __this /* static, unused */, PayInfo_t444430236 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
