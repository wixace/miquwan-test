﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenRaycaster
struct ScreenRaycaster_t4188861866;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_ScreenRaycastData1110901127.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "AssemblyU2DCSharp_ScreenRaycaster4188861866.h"

// System.Void ScreenRaycaster::.ctor()
extern "C"  void ScreenRaycaster__ctor_m2080608513 (ScreenRaycaster_t4188861866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenRaycaster::Start()
extern "C"  void ScreenRaycaster_Start_m1027746305 (ScreenRaycaster_t4188861866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScreenRaycaster::Raycast(UnityEngine.Vector2,ScreenRaycastData&)
extern "C"  bool ScreenRaycaster_Raycast_m182103337 (ScreenRaycaster_t4188861866 * __this, Vector2_t4282066565  ___screenPos0, ScreenRaycastData_t1110901127 * ___hitData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScreenRaycaster::Raycast(UnityEngine.Camera,UnityEngine.Vector2,ScreenRaycastData&)
extern "C"  bool ScreenRaycaster_Raycast_m1109228435 (ScreenRaycaster_t4188861866 * __this, Camera_t2727095145 * ___cam0, Vector2_t4282066565  ___screenPos1, ScreenRaycastData_t1110901127 * ___hitData2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScreenRaycaster::ilo_Raycast1(ScreenRaycaster,UnityEngine.Camera,UnityEngine.Vector2,ScreenRaycastData&)
extern "C"  bool ScreenRaycaster_ilo_Raycast1_m239048391 (Il2CppObject * __this /* static, unused */, ScreenRaycaster_t4188861866 * ____this0, Camera_t2727095145 * ___cam1, Vector2_t4282066565  ___screenPos2, ScreenRaycastData_t1110901127 * ___hitData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
