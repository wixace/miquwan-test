﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_jiriba170
struct  M_jiriba170_t2690510709  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_jiriba170::_gowmornor
	float ____gowmornor_0;
	// System.Int32 GarbageiOS.M_jiriba170::_bawpeta
	int32_t ____bawpeta_1;
	// System.UInt32 GarbageiOS.M_jiriba170::_tearmoucaHerajoo
	uint32_t ____tearmoucaHerajoo_2;
	// System.String GarbageiOS.M_jiriba170::_dursortereTukay
	String_t* ____dursortereTukay_3;
	// System.Int32 GarbageiOS.M_jiriba170::_powtorha
	int32_t ____powtorha_4;
	// System.Boolean GarbageiOS.M_jiriba170::_donorVispis
	bool ____donorVispis_5;
	// System.Single GarbageiOS.M_jiriba170::_gisaw
	float ____gisaw_6;
	// System.Single GarbageiOS.M_jiriba170::_salllowwaRouti
	float ____salllowwaRouti_7;
	// System.Boolean GarbageiOS.M_jiriba170::_gereteaceeSallser
	bool ____gereteaceeSallser_8;
	// System.Boolean GarbageiOS.M_jiriba170::_chelcar
	bool ____chelcar_9;
	// System.UInt32 GarbageiOS.M_jiriba170::_laltedairYaspasdai
	uint32_t ____laltedairYaspasdai_10;
	// System.String GarbageiOS.M_jiriba170::_jooneso
	String_t* ____jooneso_11;
	// System.Boolean GarbageiOS.M_jiriba170::_burmir
	bool ____burmir_12;
	// System.UInt32 GarbageiOS.M_jiriba170::_dulufoTemkem
	uint32_t ____dulufoTemkem_13;
	// System.Boolean GarbageiOS.M_jiriba170::_nearpenayBowtarfas
	bool ____nearpenayBowtarfas_14;
	// System.Single GarbageiOS.M_jiriba170::_hetrow
	float ____hetrow_15;
	// System.Single GarbageiOS.M_jiriba170::_rerejeeLaka
	float ____rerejeeLaka_16;
	// System.UInt32 GarbageiOS.M_jiriba170::_soogurWeljinee
	uint32_t ____soogurWeljinee_17;

public:
	inline static int32_t get_offset_of__gowmornor_0() { return static_cast<int32_t>(offsetof(M_jiriba170_t2690510709, ____gowmornor_0)); }
	inline float get__gowmornor_0() const { return ____gowmornor_0; }
	inline float* get_address_of__gowmornor_0() { return &____gowmornor_0; }
	inline void set__gowmornor_0(float value)
	{
		____gowmornor_0 = value;
	}

	inline static int32_t get_offset_of__bawpeta_1() { return static_cast<int32_t>(offsetof(M_jiriba170_t2690510709, ____bawpeta_1)); }
	inline int32_t get__bawpeta_1() const { return ____bawpeta_1; }
	inline int32_t* get_address_of__bawpeta_1() { return &____bawpeta_1; }
	inline void set__bawpeta_1(int32_t value)
	{
		____bawpeta_1 = value;
	}

	inline static int32_t get_offset_of__tearmoucaHerajoo_2() { return static_cast<int32_t>(offsetof(M_jiriba170_t2690510709, ____tearmoucaHerajoo_2)); }
	inline uint32_t get__tearmoucaHerajoo_2() const { return ____tearmoucaHerajoo_2; }
	inline uint32_t* get_address_of__tearmoucaHerajoo_2() { return &____tearmoucaHerajoo_2; }
	inline void set__tearmoucaHerajoo_2(uint32_t value)
	{
		____tearmoucaHerajoo_2 = value;
	}

	inline static int32_t get_offset_of__dursortereTukay_3() { return static_cast<int32_t>(offsetof(M_jiriba170_t2690510709, ____dursortereTukay_3)); }
	inline String_t* get__dursortereTukay_3() const { return ____dursortereTukay_3; }
	inline String_t** get_address_of__dursortereTukay_3() { return &____dursortereTukay_3; }
	inline void set__dursortereTukay_3(String_t* value)
	{
		____dursortereTukay_3 = value;
		Il2CppCodeGenWriteBarrier(&____dursortereTukay_3, value);
	}

	inline static int32_t get_offset_of__powtorha_4() { return static_cast<int32_t>(offsetof(M_jiriba170_t2690510709, ____powtorha_4)); }
	inline int32_t get__powtorha_4() const { return ____powtorha_4; }
	inline int32_t* get_address_of__powtorha_4() { return &____powtorha_4; }
	inline void set__powtorha_4(int32_t value)
	{
		____powtorha_4 = value;
	}

	inline static int32_t get_offset_of__donorVispis_5() { return static_cast<int32_t>(offsetof(M_jiriba170_t2690510709, ____donorVispis_5)); }
	inline bool get__donorVispis_5() const { return ____donorVispis_5; }
	inline bool* get_address_of__donorVispis_5() { return &____donorVispis_5; }
	inline void set__donorVispis_5(bool value)
	{
		____donorVispis_5 = value;
	}

	inline static int32_t get_offset_of__gisaw_6() { return static_cast<int32_t>(offsetof(M_jiriba170_t2690510709, ____gisaw_6)); }
	inline float get__gisaw_6() const { return ____gisaw_6; }
	inline float* get_address_of__gisaw_6() { return &____gisaw_6; }
	inline void set__gisaw_6(float value)
	{
		____gisaw_6 = value;
	}

	inline static int32_t get_offset_of__salllowwaRouti_7() { return static_cast<int32_t>(offsetof(M_jiriba170_t2690510709, ____salllowwaRouti_7)); }
	inline float get__salllowwaRouti_7() const { return ____salllowwaRouti_7; }
	inline float* get_address_of__salllowwaRouti_7() { return &____salllowwaRouti_7; }
	inline void set__salllowwaRouti_7(float value)
	{
		____salllowwaRouti_7 = value;
	}

	inline static int32_t get_offset_of__gereteaceeSallser_8() { return static_cast<int32_t>(offsetof(M_jiriba170_t2690510709, ____gereteaceeSallser_8)); }
	inline bool get__gereteaceeSallser_8() const { return ____gereteaceeSallser_8; }
	inline bool* get_address_of__gereteaceeSallser_8() { return &____gereteaceeSallser_8; }
	inline void set__gereteaceeSallser_8(bool value)
	{
		____gereteaceeSallser_8 = value;
	}

	inline static int32_t get_offset_of__chelcar_9() { return static_cast<int32_t>(offsetof(M_jiriba170_t2690510709, ____chelcar_9)); }
	inline bool get__chelcar_9() const { return ____chelcar_9; }
	inline bool* get_address_of__chelcar_9() { return &____chelcar_9; }
	inline void set__chelcar_9(bool value)
	{
		____chelcar_9 = value;
	}

	inline static int32_t get_offset_of__laltedairYaspasdai_10() { return static_cast<int32_t>(offsetof(M_jiriba170_t2690510709, ____laltedairYaspasdai_10)); }
	inline uint32_t get__laltedairYaspasdai_10() const { return ____laltedairYaspasdai_10; }
	inline uint32_t* get_address_of__laltedairYaspasdai_10() { return &____laltedairYaspasdai_10; }
	inline void set__laltedairYaspasdai_10(uint32_t value)
	{
		____laltedairYaspasdai_10 = value;
	}

	inline static int32_t get_offset_of__jooneso_11() { return static_cast<int32_t>(offsetof(M_jiriba170_t2690510709, ____jooneso_11)); }
	inline String_t* get__jooneso_11() const { return ____jooneso_11; }
	inline String_t** get_address_of__jooneso_11() { return &____jooneso_11; }
	inline void set__jooneso_11(String_t* value)
	{
		____jooneso_11 = value;
		Il2CppCodeGenWriteBarrier(&____jooneso_11, value);
	}

	inline static int32_t get_offset_of__burmir_12() { return static_cast<int32_t>(offsetof(M_jiriba170_t2690510709, ____burmir_12)); }
	inline bool get__burmir_12() const { return ____burmir_12; }
	inline bool* get_address_of__burmir_12() { return &____burmir_12; }
	inline void set__burmir_12(bool value)
	{
		____burmir_12 = value;
	}

	inline static int32_t get_offset_of__dulufoTemkem_13() { return static_cast<int32_t>(offsetof(M_jiriba170_t2690510709, ____dulufoTemkem_13)); }
	inline uint32_t get__dulufoTemkem_13() const { return ____dulufoTemkem_13; }
	inline uint32_t* get_address_of__dulufoTemkem_13() { return &____dulufoTemkem_13; }
	inline void set__dulufoTemkem_13(uint32_t value)
	{
		____dulufoTemkem_13 = value;
	}

	inline static int32_t get_offset_of__nearpenayBowtarfas_14() { return static_cast<int32_t>(offsetof(M_jiriba170_t2690510709, ____nearpenayBowtarfas_14)); }
	inline bool get__nearpenayBowtarfas_14() const { return ____nearpenayBowtarfas_14; }
	inline bool* get_address_of__nearpenayBowtarfas_14() { return &____nearpenayBowtarfas_14; }
	inline void set__nearpenayBowtarfas_14(bool value)
	{
		____nearpenayBowtarfas_14 = value;
	}

	inline static int32_t get_offset_of__hetrow_15() { return static_cast<int32_t>(offsetof(M_jiriba170_t2690510709, ____hetrow_15)); }
	inline float get__hetrow_15() const { return ____hetrow_15; }
	inline float* get_address_of__hetrow_15() { return &____hetrow_15; }
	inline void set__hetrow_15(float value)
	{
		____hetrow_15 = value;
	}

	inline static int32_t get_offset_of__rerejeeLaka_16() { return static_cast<int32_t>(offsetof(M_jiriba170_t2690510709, ____rerejeeLaka_16)); }
	inline float get__rerejeeLaka_16() const { return ____rerejeeLaka_16; }
	inline float* get_address_of__rerejeeLaka_16() { return &____rerejeeLaka_16; }
	inline void set__rerejeeLaka_16(float value)
	{
		____rerejeeLaka_16 = value;
	}

	inline static int32_t get_offset_of__soogurWeljinee_17() { return static_cast<int32_t>(offsetof(M_jiriba170_t2690510709, ____soogurWeljinee_17)); }
	inline uint32_t get__soogurWeljinee_17() const { return ____soogurWeljinee_17; }
	inline uint32_t* get_address_of__soogurWeljinee_17() { return &____soogurWeljinee_17; }
	inline void set__soogurWeljinee_17(uint32_t value)
	{
		____soogurWeljinee_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
