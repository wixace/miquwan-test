﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.CustomCreationConverter`1<System.Object>
struct CustomCreationConverter_1_t2027743283;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer251850770.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "mscorlib_System_Type2863145774.h"

// System.Void Newtonsoft.Json.Converters.CustomCreationConverter`1<System.Object>::.ctor()
extern "C"  void CustomCreationConverter_1__ctor_m299850762_gshared (CustomCreationConverter_1_t2027743283 * __this, const MethodInfo* method);
#define CustomCreationConverter_1__ctor_m299850762(__this, method) ((  void (*) (CustomCreationConverter_1_t2027743283 *, const MethodInfo*))CustomCreationConverter_1__ctor_m299850762_gshared)(__this, method)
// System.Void Newtonsoft.Json.Converters.CustomCreationConverter`1<System.Object>::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  void CustomCreationConverter_1_WriteJson_m946118922_gshared (CustomCreationConverter_1_t2027743283 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonSerializer_t251850770 * ___serializer2, const MethodInfo* method);
#define CustomCreationConverter_1_WriteJson_m946118922(__this, ___writer0, ___value1, ___serializer2, method) ((  void (*) (CustomCreationConverter_1_t2027743283 *, JsonWriter_t972330355 *, Il2CppObject *, JsonSerializer_t251850770 *, const MethodInfo*))CustomCreationConverter_1_WriteJson_m946118922_gshared)(__this, ___writer0, ___value1, ___serializer2, method)
// System.Object Newtonsoft.Json.Converters.CustomCreationConverter`1<System.Object>::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  Il2CppObject * CustomCreationConverter_1_ReadJson_m3440594451_gshared (CustomCreationConverter_1_t2027743283 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, Il2CppObject * ___existingValue2, JsonSerializer_t251850770 * ___serializer3, const MethodInfo* method);
#define CustomCreationConverter_1_ReadJson_m3440594451(__this, ___reader0, ___objectType1, ___existingValue2, ___serializer3, method) ((  Il2CppObject * (*) (CustomCreationConverter_1_t2027743283 *, JsonReader_t816925123 *, Type_t *, Il2CppObject *, JsonSerializer_t251850770 *, const MethodInfo*))CustomCreationConverter_1_ReadJson_m3440594451_gshared)(__this, ___reader0, ___objectType1, ___existingValue2, ___serializer3, method)
// System.Boolean Newtonsoft.Json.Converters.CustomCreationConverter`1<System.Object>::CanConvert(System.Type)
extern "C"  bool CustomCreationConverter_1_CanConvert_m968142800_gshared (CustomCreationConverter_1_t2027743283 * __this, Type_t * ___objectType0, const MethodInfo* method);
#define CustomCreationConverter_1_CanConvert_m968142800(__this, ___objectType0, method) ((  bool (*) (CustomCreationConverter_1_t2027743283 *, Type_t *, const MethodInfo*))CustomCreationConverter_1_CanConvert_m968142800_gshared)(__this, ___objectType0, method)
// System.Boolean Newtonsoft.Json.Converters.CustomCreationConverter`1<System.Object>::get_CanWrite()
extern "C"  bool CustomCreationConverter_1_get_CanWrite_m2205801086_gshared (CustomCreationConverter_1_t2027743283 * __this, const MethodInfo* method);
#define CustomCreationConverter_1_get_CanWrite_m2205801086(__this, method) ((  bool (*) (CustomCreationConverter_1_t2027743283 *, const MethodInfo*))CustomCreationConverter_1_get_CanWrite_m2205801086_gshared)(__this, method)
// System.Void Newtonsoft.Json.Converters.CustomCreationConverter`1<System.Object>::ilo_Populate1(Newtonsoft.Json.JsonSerializer,Newtonsoft.Json.JsonReader,System.Object)
extern "C"  void CustomCreationConverter_1_ilo_Populate1_m691901447_gshared (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, JsonReader_t816925123 * ___reader1, Il2CppObject * ___target2, const MethodInfo* method);
#define CustomCreationConverter_1_ilo_Populate1_m691901447(__this /* static, unused */, ____this0, ___reader1, ___target2, method) ((  void (*) (Il2CppObject * /* static, unused */, JsonSerializer_t251850770 *, JsonReader_t816925123 *, Il2CppObject *, const MethodInfo*))CustomCreationConverter_1_ilo_Populate1_m691901447_gshared)(__this /* static, unused */, ____this0, ___reader1, ___target2, method)
