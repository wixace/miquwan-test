﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen756388269.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"

// System.Void System.Array/InternalEnumerator`1<Pathfinding.Int2>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m763635614_gshared (InternalEnumerator_1_t756388269 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m763635614(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t756388269 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m763635614_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.Int2>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m97844546_gshared (InternalEnumerator_1_t756388269 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m97844546(__this, method) ((  void (*) (InternalEnumerator_1_t756388269 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m97844546_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Pathfinding.Int2>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1740972014_gshared (InternalEnumerator_1_t756388269 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1740972014(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t756388269 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1740972014_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.Int2>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m690159349_gshared (InternalEnumerator_1_t756388269 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m690159349(__this, method) ((  void (*) (InternalEnumerator_1_t756388269 *, const MethodInfo*))InternalEnumerator_1_Dispose_m690159349_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.Int2>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2460860974_gshared (InternalEnumerator_1_t756388269 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2460860974(__this, method) ((  bool (*) (InternalEnumerator_1_t756388269 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2460860974_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Pathfinding.Int2>::get_Current()
extern "C"  Int2_t1974045593  InternalEnumerator_1_get_Current_m2466398885_gshared (InternalEnumerator_1_t756388269 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2466398885(__this, method) ((  Int2_t1974045593  (*) (InternalEnumerator_1_t756388269 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2466398885_gshared)(__this, method)
