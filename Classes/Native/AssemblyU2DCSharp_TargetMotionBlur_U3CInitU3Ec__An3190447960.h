﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenPosition
struct TweenPosition_t3684358292;
// TargetMotionBlur
struct TargetMotionBlur_t1745103310;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TargetMotionBlur/<Init>c__AnonStorey150
struct  U3CInitU3Ec__AnonStorey150_t3190447960  : public Il2CppObject
{
public:
	// TweenPosition TargetMotionBlur/<Init>c__AnonStorey150::posAni
	TweenPosition_t3684358292 * ___posAni_0;
	// TargetMotionBlur TargetMotionBlur/<Init>c__AnonStorey150::<>f__this
	TargetMotionBlur_t1745103310 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_posAni_0() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey150_t3190447960, ___posAni_0)); }
	inline TweenPosition_t3684358292 * get_posAni_0() const { return ___posAni_0; }
	inline TweenPosition_t3684358292 ** get_address_of_posAni_0() { return &___posAni_0; }
	inline void set_posAni_0(TweenPosition_t3684358292 * value)
	{
		___posAni_0 = value;
		Il2CppCodeGenWriteBarrier(&___posAni_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey150_t3190447960, ___U3CU3Ef__this_1)); }
	inline TargetMotionBlur_t1745103310 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline TargetMotionBlur_t1745103310 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(TargetMotionBlur_t1745103310 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
