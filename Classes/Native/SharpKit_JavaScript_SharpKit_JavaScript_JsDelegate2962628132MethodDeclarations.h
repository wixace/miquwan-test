﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SharpKit.JavaScript.JsDelegateAttribute
struct JsDelegateAttribute_t2962628132;

#include "codegen/il2cpp-codegen.h"

// System.Void SharpKit.JavaScript.JsDelegateAttribute::set_NativeDelegates(System.Boolean)
extern "C"  void JsDelegateAttribute_set_NativeDelegates_m1853445002 (JsDelegateAttribute_t2962628132 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharpKit.JavaScript.JsDelegateAttribute::.ctor()
extern "C"  void JsDelegateAttribute__ctor_m2868386651 (JsDelegateAttribute_t2962628132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
