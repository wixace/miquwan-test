﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Poly2Tri.TriangulationPoint
struct TriangulationPoint_t3810082933;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Poly2Tri.TriangulationConstraint
struct  TriangulationConstraint_t137695394  : public Il2CppObject
{
public:
	// Pathfinding.Poly2Tri.TriangulationPoint Pathfinding.Poly2Tri.TriangulationConstraint::P
	TriangulationPoint_t3810082933 * ___P_0;
	// Pathfinding.Poly2Tri.TriangulationPoint Pathfinding.Poly2Tri.TriangulationConstraint::Q
	TriangulationPoint_t3810082933 * ___Q_1;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(TriangulationConstraint_t137695394, ___P_0)); }
	inline TriangulationPoint_t3810082933 * get_P_0() const { return ___P_0; }
	inline TriangulationPoint_t3810082933 ** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(TriangulationPoint_t3810082933 * value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier(&___P_0, value);
	}

	inline static int32_t get_offset_of_Q_1() { return static_cast<int32_t>(offsetof(TriangulationConstraint_t137695394, ___Q_1)); }
	inline TriangulationPoint_t3810082933 * get_Q_1() const { return ___Q_1; }
	inline TriangulationPoint_t3810082933 ** get_address_of_Q_1() { return &___Q_1; }
	inline void set_Q_1(TriangulationPoint_t3810082933 * value)
	{
		___Q_1 = value;
		Il2CppCodeGenWriteBarrier(&___Q_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
