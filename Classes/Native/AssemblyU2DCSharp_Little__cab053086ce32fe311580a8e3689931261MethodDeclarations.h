﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._cab053086ce32fe311580a8ece834b8a
struct _cab053086ce32fe311580a8ece834b8a_t3689931261;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._cab053086ce32fe311580a8ece834b8a::.ctor()
extern "C"  void _cab053086ce32fe311580a8ece834b8a__ctor_m3585421520 (_cab053086ce32fe311580a8ece834b8a_t3689931261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._cab053086ce32fe311580a8ece834b8a::_cab053086ce32fe311580a8ece834b8am2(System.Int32)
extern "C"  int32_t _cab053086ce32fe311580a8ece834b8a__cab053086ce32fe311580a8ece834b8am2_m3705432185 (_cab053086ce32fe311580a8ece834b8a_t3689931261 * __this, int32_t ____cab053086ce32fe311580a8ece834b8aa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._cab053086ce32fe311580a8ece834b8a::_cab053086ce32fe311580a8ece834b8am(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _cab053086ce32fe311580a8ece834b8a__cab053086ce32fe311580a8ece834b8am_m2149431901 (_cab053086ce32fe311580a8ece834b8a_t3689931261 * __this, int32_t ____cab053086ce32fe311580a8ece834b8aa0, int32_t ____cab053086ce32fe311580a8ece834b8a801, int32_t ____cab053086ce32fe311580a8ece834b8ac2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
