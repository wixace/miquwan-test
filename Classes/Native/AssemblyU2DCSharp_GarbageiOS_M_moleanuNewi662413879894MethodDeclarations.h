﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_moleanuNewi66
struct M_moleanuNewi66_t2413879894;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_moleanuNewi662413879894.h"

// System.Void GarbageiOS.M_moleanuNewi66::.ctor()
extern "C"  void M_moleanuNewi66__ctor_m2949297101 (M_moleanuNewi66_t2413879894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_moleanuNewi66::M_fairallMewe0(System.String[],System.Int32)
extern "C"  void M_moleanuNewi66_M_fairallMewe0_m569954797 (M_moleanuNewi66_t2413879894 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_moleanuNewi66::M_fuweaSorhar1(System.String[],System.Int32)
extern "C"  void M_moleanuNewi66_M_fuweaSorhar1_m3790712618 (M_moleanuNewi66_t2413879894 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_moleanuNewi66::M_baqiParmou2(System.String[],System.Int32)
extern "C"  void M_moleanuNewi66_M_baqiParmou2_m892209561 (M_moleanuNewi66_t2413879894 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_moleanuNewi66::ilo_M_fairallMewe01(GarbageiOS.M_moleanuNewi66,System.String[],System.Int32)
extern "C"  void M_moleanuNewi66_ilo_M_fairallMewe01_m291046465 (Il2CppObject * __this /* static, unused */, M_moleanuNewi66_t2413879894 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
