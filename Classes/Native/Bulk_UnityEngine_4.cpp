﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.Projector
struct Projector_t4006592434;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.PropertyAttribute
struct PropertyAttribute_t3531521085;
// UnityEngine.QualitySettings
struct QualitySettings_t719345784;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Random
struct Random_t3156561159;
// UnityEngine.RangeAttribute
struct RangeAttribute_t912008995;
// UnityEngine.Collider
struct Collider_t2939674232;
// UnityEngine.Renderer
struct Renderer_t3076687687;
// UnityEngine.Rigidbody
struct Rigidbody_t3346577219;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1743771669;
// UnityEngine.RectOffset
struct RectOffset_t3056157787;
// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t779639188;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.Canvas
struct Canvas_t2727140764;
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_t3587651179;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// UnityEngine.Material[]
struct MaterialU5BU5D_t170856778;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t1322387783;
// System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct List_1_t2448783290;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t1654378665;
// UnityEngine.RenderSettings
struct RenderSettings_t425127197;
// UnityEngine.Cubemap
struct Cubemap_t761092157;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;
// UnityEngine.RequireComponent
struct RequireComponent_t1687166108;
// System.Type
struct Type_t;
// UnityEngine.ResourceRequest
struct ResourceRequest_t3731857623;
// UnityEngine.Object
struct Object_t3071478659;
// UnityEngine.Resources
struct Resources_t2918352667;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1015136018;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3699081103;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t528650843;
// UnityEngine.RPC
struct RPC_t3134615963;
// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t274649809;
// UnityEngine.AnimationClip[]
struct AnimationClipU5BU5D_t4186127791;
// UnityEngine.Screen
struct Screen_t3187157168;
// UnityEngine.Resolution[]
struct ResolutionU5BU5D_t3895287505;
// UnityEngine.ScriptableObject
struct ScriptableObject_t2970544072;
// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct RequiredByNativeCodeAttribute_t3165457172;
// UnityEngine.Scripting.UsedByNativeCodeAttribute
struct UsedByNativeCodeAttribute_t3197444790;
// UnityEngine.Security
struct Security_t225689028;
// System.Reflection.Assembly
struct Assembly_t1418687608;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.SelectionBaseAttribute
struct SelectionBaseAttribute_t204085987;
// UnityEngine.GUILayer
struct GUILayer_t2983897946;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t2216353654;
// UnityEngine.Serialization.ListSerializationSurrogate
struct ListSerializationSurrogate_t3207893374;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t2300150170;
// UnityEngine.Serialization.UnitySurrogateSelector
struct UnitySurrogateSelector_t780726490;
// System.Runtime.Serialization.ISerializationSurrogate
struct ISerializationSurrogate_t132171319;
// UnityEngine.SerializeField
struct SerializeField_t3754825534;
// UnityEngine.SerializePrivateVariables
struct SerializePrivateVariables_t2835496234;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.Shader
struct Shader_t3191267369;
// UnityEngine.Texture
struct Texture_t2526458961;
// UnityEngine.ComputeBuffer
struct ComputeBuffer_t37359565;
// UnityEngine.SharedBetweenAnimatorsAttribute
struct SharedBetweenAnimatorsAttribute_t1486273289;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t3986041494;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3792884695;
// UnityEngine.Mesh
struct Mesh_t4241756145;
// UnityEngine.Skybox
struct Skybox_t3194751310;
// UnityEngine.SleepTimeout
struct SleepTimeout_t1876228494;
// UnityEngine.Event
struct Event_t4196595728;
// UnityEngine.SliderState
struct SliderState_t1233388262;
// UnityEngine.SliderJoint2D
struct SliderJoint2D_t2955194545;
// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform
struct GameCenterPlatform_t3570684786;
// UnityEngine.SocialPlatforms.ILocalUser
struct ILocalUser_t2654168339;
// System.Action`1<System.Boolean>
struct Action_1_t872614854;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct GcAchievementDataU5BU5D_t1726768202;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct GcScoreDataU5BU5D_t1670395707;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>
struct Action_1_t229750097;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>
struct Action_1_t2349069933;
// System.Action`1<UnityEngine.SocialPlatforms.IScore[]>
struct Action_1_t645920862;
// UnityEngine.SocialPlatforms.ILeaderboard
struct ILeaderboard_t3799088250;
// System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>
struct Action_1_t3814920354;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t2378268441;
// UnityEngine.SocialPlatforms.IAchievement
struct IAchievement_t2957812780;
// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t344600729;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
struct AchievementDescription_t2116066607;
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
struct GcLeaderboard_t1820874799;
// UnityEngine.SocialPlatforms.Impl.Leaderboard
struct Leaderboard_t1185876199;
// UnityEngine.SocialPlatforms.Impl.Score
struct Score_t3396031228;
// UnityEngine.SocialPlatforms.Impl.UserProfile
struct UserProfile_t2280656072;
// UnityEngine.SocialPlatforms.IScore
struct IScore_t4279057999;
// UnityEngine.SocialPlatforms.IScore[]
struct IScoreU5BU5D_t250104726;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_Projector4006592434.h"
#include "UnityEngine_UnityEngine_Projector4006592434MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "UnityEngine_UnityEngine_Behaviour200106419MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Int321153838500.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_PropertyAttribute3531521085.h"
#include "UnityEngine_UnityEngine_PropertyAttribute3531521085MethodDeclarations.h"
#include "mscorlib_System_Attribute2523058482MethodDeclarations.h"
#include "UnityEngine_UnityEngine_QualitySettings719345784.h"
#include "UnityEngine_UnityEngine_QualitySettings719345784MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_ShadowProjection4063319827.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_AnisotropicFiltering503731965.h"
#include "UnityEngine_UnityEngine_ColorSpace161844263.h"
#include "UnityEngine_UnityEngine_BlendWeights3700066926.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3456360697MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3456360697.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityString3369712284MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Single4291918972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction577895768.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction577895768MethodDeclarations.h"
#include "UnityEngine_UnityEngine_QueueMode3161759562.h"
#include "UnityEngine_UnityEngine_QueueMode3161759562MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random3156561159.h"
#include "UnityEngine_UnityEngine_Random3156561159MethodDeclarations.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Color4194546905MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RangeAttribute912008995.h"
#include "UnityEngine_UnityEngine_RangeAttribute912008995MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"
#include "UnityEngine_UnityEngine_Ray3134616544MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Ray2D4207993202.h"
#include "UnityEngine_UnityEngine_Ray2D4207993202MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer3076687687MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Renderer3076687687.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219.h"
#include "UnityEngine_UnityEngine_Collider2939674232MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"
#include "UnityEngine_UnityEngine_Rigidbody2D1743771669.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Rect4241904616MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectOffset3056157787.h"
#include "UnityEngine_UnityEngine_RectOffset3056157787MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform972643934.h"
#include "UnityEngine_UnityEngine_RectTransform972643934MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDriven779639188.h"
#include "mscorlib_System_Delegate3310234105MethodDeclarations.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDriven779639188MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_Edge1676794651.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis1676694783.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis1676694783MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_Edge1676794651MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "UnityEngine_UnityEngine_RectTransformUtility3025555048.h"
#include "UnityEngine_UnityEngine_RectTransformUtility3025555048MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_Canvas2727140764.h"
#include "UnityEngine_UnityEngine_Plane4206452690MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Plane4206452690.h"
#include "UnityEngine_UnityEngine_Camera2727095145MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds2711641849MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_ReflectionProbe2412295291.h"
#include "UnityEngine_UnityEngine_ReflectionProbe2412295291MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderBuffer3529837690.h"
#include "UnityEngine_UnityEngine_RenderBuffer3529837690MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_ShadowCastingMod2598729988.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeU3237956588.h"
#include "UnityEngine_UnityEngine_MaterialPropertyBlock1322387783.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2448783290.h"
#include "UnityEngine_UnityEngine_Rendering_AmbientMode3372742489.h"
#include "UnityEngine_UnityEngine_Rendering_AmbientMode3372742489MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_CameraEvent3586806131.h"
#include "UnityEngine_UnityEngine_Rendering_CameraEvent3586806131MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask3214369688.h"
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask3214369688MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_CommandBuffer1654378665.h"
#include "UnityEngine_UnityEngine_Rendering_CommandBuffer1654378665MethodDeclarations.h"
#include "mscorlib_System_GC1614687344MethodDeclarations.h"
#include "mscorlib_System_IntPtr4010401971MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_CompareFunction2661816155.h"
#include "UnityEngine_UnityEngine_Rendering_CompareFunction2661816155MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_DefaultReflectio2706384717.h"
#include "UnityEngine_UnityEngine_Rendering_DefaultReflectio2706384717MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_GraphicsDeviceTy3609427819.h"
#include "UnityEngine_UnityEngine_Rendering_GraphicsDeviceTy3609427819MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_LightEvent2859988980.h"
#include "UnityEngine_UnityEngine_Rendering_LightEvent2859988980MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_OpaqueSortMode3100402674.h"
#include "UnityEngine_UnityEngine_Rendering_OpaqueSortMode3100402674MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeB1080597738.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeB1080597738MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeU3237956588MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_RenderBufferLoad4243858466.h"
#include "UnityEngine_UnityEngine_Rendering_RenderBufferLoad4243858466MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_RenderBufferStor2198970271.h"
#include "UnityEngine_UnityEngine_Rendering_RenderBufferStor2198970271MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_ShadowCastingMod2598729988MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmoni3881980607.h"
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmoni3881980607MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_StencilOp3324967291.h"
#include "UnityEngine_UnityEngine_Rendering_StencilOp3324967291MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderingPath3661620551.h"
#include "UnityEngine_UnityEngine_RenderingPath3661620551MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderMode77252893.h"
#include "UnityEngine_UnityEngine_RenderMode77252893MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderSettings425127197.h"
#include "UnityEngine_UnityEngine_RenderSettings425127197MethodDeclarations.h"
#include "UnityEngine_UnityEngine_FogMode3255732919.h"
#include "UnityEngine_UnityEngine_Cubemap761092157.h"
#include "UnityEngine_UnityEngine_RenderTargetSetup2400315372.h"
#include "UnityEngine_UnityEngine_RenderTargetSetup2400315372MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat2841883826.h"
#include "UnityEngine_UnityEngine_RenderTextureReadWrite2502600968.h"
#include "UnityEngine_UnityEngine_Texture2526458961MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat2841883826MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTextureReadWrite2502600968MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RequireComponent1687166108.h"
#include "UnityEngine_UnityEngine_RequireComponent1687166108MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "UnityEngine_UnityEngine_Resolution1578306928.h"
#include "UnityEngine_UnityEngine_Resolution1578306928MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ResourceRequest3731857623.h"
#include "UnityEngine_UnityEngine_ResourceRequest3731857623MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3699081103MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources2918352667MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources2918352667.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "UnityEngine_UnityEngine_AsyncOperation3699081103.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RigidbodyConstraints1602554285.h"
#include "UnityEngine_UnityEngine_CollisionDetectionMode53735930.h"
#include "UnityEngine_UnityEngine_ForceMode2134283300.h"
#include "UnityEngine_UnityEngine_RigidbodyInterpolation3483171483.h"
#include "UnityEngine_UnityEngine_Rigidbody2D1743771669MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RigidbodyConstraints2D774977535.h"
#include "UnityEngine_UnityEngine_RigidbodyInterpolation2D4161840493.h"
#include "UnityEngine_UnityEngine_RigidbodySleepMode2D3661767779.h"
#include "UnityEngine_UnityEngine_CollisionDetectionMode2D2714190092.h"
#include "UnityEngine_UnityEngine_ForceMode2D665452726.h"
#include "UnityEngine_UnityEngine_RigidbodyConstraints1602554285MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RigidbodyConstraints2D774977535MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RigidbodyInterpolation3483171483MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RigidbodyInterpolation2D4161840493MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RigidbodySleepMode2D3661767779MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RotationDriveMode1324228709.h"
#include "UnityEngine_UnityEngine_RotationDriveMode1324228709MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RPC3134615963.h"
#include "UnityEngine_UnityEngine_RPC3134615963MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController274649809.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController274649809MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationClip2007702890.h"
#include "UnityEngine_UnityEngine_RuntimePlatform3050318497.h"
#include "UnityEngine_UnityEngine_RuntimePlatform3050318497MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScaleMode3023293187.h"
#include "UnityEngine_UnityEngine_ScaleMode3023293187MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM3067001883.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM3067001883MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1080795294.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1080795294MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag2940962239.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag2940962239MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3187157168.h"
#include "UnityEngine_UnityEngine_Screen3187157168MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScreenOrientation1849668026.h"
#include "UnityEngine_UnityEngine_ScreenOrientation1849668026MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject2970544072.h"
#include "UnityEngine_UnityEngine_ScriptableObject2970544072MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative3165457172.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative3165457172MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3197444790.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3197444790MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Security225689028.h"
#include "UnityEngine_UnityEngine_Security225689028MethodDeclarations.h"
#include "mscorlib_System_Reflection_Assembly1418687608.h"
#include "mscorlib_System_Byte2862609660.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttribute204085987.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttribute204085987MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMessageOptions3856946179.h"
#include "UnityEngine_UnityEngine_SendMessageOptions3856946179MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMouseEvents3965481900.h"
#include "UnityEngine_UnityEngine_SendMouseEvents3965481900MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo3209134097.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayer2983897946MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayer2983897946.h"
#include "UnityEngine_UnityEngine_GUIElement3775428101.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_CameraClearFlags2093155523.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo3209134097MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri2216353654.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri2216353654MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Serialization_ListSerializ3207893374.h"
#include "UnityEngine_UnityEngine_Serialization_ListSerializ3207893374MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Activator2714366379MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException1589641621MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException1589641621.h"
#include "UnityEngine_UnityEngine_Serialization_UnitySurrogat780726490.h"
#include "UnityEngine_UnityEngine_Serialization_UnitySurrogat780726490MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException1912495542MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException1912495542.h"
#include "UnityEngine_UnityEngine_SerializeField3754825534.h"
#include "UnityEngine_UnityEngine_SerializeField3754825534MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables2835496234.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables2835496234MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SetupCoroutine1459791391.h"
#include "UnityEngine_UnityEngine_SetupCoroutine1459791391MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "mscorlib_System_Reflection_BindingFlags1523912596.h"
#include "mscorlib_System_Reflection_Binder1074302268.h"
#include "mscorlib_System_Reflection_ParameterModifier741930026.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142.h"
#include "UnityEngine_UnityEngine_Shader3191267369.h"
#include "UnityEngine_UnityEngine_Shader3191267369MethodDeclarations.h"
#include "UnityEngine_UnityEngine_DisableBatchingType382694016.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_ComputeBuffer37359565.h"
#include "UnityEngine_UnityEngine_ShadowProjection4063319827MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr1486273289.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr1486273289MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SkeletonBone421858229.h"
#include "UnityEngine_UnityEngine_SkeletonBone421858229MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SkinnedMeshRenderer3986041494.h"
#include "UnityEngine_UnityEngine_SkinnedMeshRenderer3986041494MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SkinQuality1206517624.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"
#include "UnityEngine_UnityEngine_SkinQuality1206517624MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Skybox3194751310.h"
#include "UnityEngine_UnityEngine_Skybox3194751310MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SleepTimeout1876228494.h"
#include "UnityEngine_UnityEngine_SleepTimeout1876228494MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SliderHandler783692703.h"
#include "UnityEngine_UnityEngine_SliderHandler783692703MethodDeclarations.h"
#include "UnityEngine_UnityEngine_EventType637886954.h"
#include "UnityEngine_UnityEngine_Event4196595728MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI3134605553MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIUtility1028319349MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SystemClock4036018645MethodDeclarations.h"
#include "mscorlib_System_DateTime4283661327MethodDeclarations.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "UnityEngine_UnityEngine_Event4196595728.h"
#include "UnityEngine_UnityEngine_SliderState1233388262.h"
#include "mscorlib_System_Double3868226565.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SliderJoint2D2955194545.h"
#include "UnityEngine_UnityEngine_SliderJoint2D2955194545MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnchoredJoint2D1716788702MethodDeclarations.h"
#include "UnityEngine_UnityEngine_JointMotor2D682576033.h"
#include "UnityEngine_UnityEngine_JointTranslationLimits2D2599234005.h"
#include "UnityEngine_UnityEngine_JointLimitState2D3271314440.h"
#include "UnityEngine_UnityEngine_SliderState1233388262MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3570684786.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3570684786MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3189060351MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie2116066607.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserP2280656072.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3189060351.h"
#include "mscorlib_System_Action_1_gen872614854.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "mscorlib_System_Int641153838595.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope1305796361.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2242891083.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2242891083MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie2116066607MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen229750097MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen229750097.h"
#include "mscorlib_System_Action_1_gen872614854MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_657441114.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_657441114MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Local1307362368MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Local1307362368.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3481375915.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3481375915MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2349069933MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achiev344600729.h"
#include "mscorlib_System_Action_1_gen2349069933.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2181296590.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2181296590MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen645920862MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score3396031228.h"
#include "mscorlib_System_Action_1_gen645920862.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserP2280656072MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1820874799MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leade1185876199MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leade1185876199.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1820874799.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range1533311935.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope1608660171.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3208733121MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3208733121.h"
#include "mscorlib_System_Action_1_gen3814920354MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3814920354.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achiev344600729MethodDeclarations.h"
#include "mscorlib_System_UInt3224667981.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score3396031228MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState1609153288.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range1533311935MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t3076687687_m1231607476(__this, method) ((  Renderer_t3076687687 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t1108656482* Component_GetComponentsInChildren_TisIl2CppObject_m565596074_gshared (Component_t3501516275 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m565596074(__this, p0, method) ((  ObjectU5BU5D_t1108656482* (*) (Component_t3501516275 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m565596074_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.RectTransform>(System.Boolean)
#define Component_GetComponentsInChildren_TisRectTransform_t972643934_m3447711447(__this, p0, method) ((  RectTransformU5BU5D_t3587651179* (*) (Component_t3501516275 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m565596074_gshared)(__this, p0, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUILayer>()
#define Component_GetComponent_TisGUILayer_t2983897946_m2891371969(__this, method) ((  GUILayer_t2983897946 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Projector::.ctor()
extern "C"  void Projector__ctor_m2675483037 (Projector_t4006592434 * __this, const MethodInfo* method)
{
	{
		Behaviour__ctor_m1624944828(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Projector::get_nearClipPlane()
extern "C"  float Projector_get_nearClipPlane_m289968546 (Projector_t4006592434 * __this, const MethodInfo* method)
{
	typedef float (*Projector_get_nearClipPlane_m289968546_ftn) (Projector_t4006592434 *);
	static Projector_get_nearClipPlane_m289968546_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Projector_get_nearClipPlane_m289968546_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Projector::get_nearClipPlane()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Projector::set_nearClipPlane(System.Single)
extern "C"  void Projector_set_nearClipPlane_m2945715785 (Projector_t4006592434 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Projector_set_nearClipPlane_m2945715785_ftn) (Projector_t4006592434 *, float);
	static Projector_set_nearClipPlane_m2945715785_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Projector_set_nearClipPlane_m2945715785_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Projector::set_nearClipPlane(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Projector::get_farClipPlane()
extern "C"  float Projector_get_farClipPlane_m1097904057 (Projector_t4006592434 * __this, const MethodInfo* method)
{
	typedef float (*Projector_get_farClipPlane_m1097904057_ftn) (Projector_t4006592434 *);
	static Projector_get_farClipPlane_m1097904057_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Projector_get_farClipPlane_m1097904057_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Projector::get_farClipPlane()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Projector::set_farClipPlane(System.Single)
extern "C"  void Projector_set_farClipPlane_m1618485138 (Projector_t4006592434 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Projector_set_farClipPlane_m1618485138_ftn) (Projector_t4006592434 *, float);
	static Projector_set_farClipPlane_m1618485138_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Projector_set_farClipPlane_m1618485138_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Projector::set_farClipPlane(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Projector::get_fieldOfView()
extern "C"  float Projector_get_fieldOfView_m4105876852 (Projector_t4006592434 * __this, const MethodInfo* method)
{
	typedef float (*Projector_get_fieldOfView_m4105876852_ftn) (Projector_t4006592434 *);
	static Projector_get_fieldOfView_m4105876852_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Projector_get_fieldOfView_m4105876852_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Projector::get_fieldOfView()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Projector::set_fieldOfView(System.Single)
extern "C"  void Projector_set_fieldOfView_m3721392055 (Projector_t4006592434 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Projector_set_fieldOfView_m3721392055_ftn) (Projector_t4006592434 *, float);
	static Projector_set_fieldOfView_m3721392055_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Projector_set_fieldOfView_m3721392055_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Projector::set_fieldOfView(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Projector::get_aspectRatio()
extern "C"  float Projector_get_aspectRatio_m2979664401 (Projector_t4006592434 * __this, const MethodInfo* method)
{
	typedef float (*Projector_get_aspectRatio_m2979664401_ftn) (Projector_t4006592434 *);
	static Projector_get_aspectRatio_m2979664401_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Projector_get_aspectRatio_m2979664401_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Projector::get_aspectRatio()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Projector::set_aspectRatio(System.Single)
extern "C"  void Projector_set_aspectRatio_m2093660730 (Projector_t4006592434 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Projector_set_aspectRatio_m2093660730_ftn) (Projector_t4006592434 *, float);
	static Projector_set_aspectRatio_m2093660730_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Projector_set_aspectRatio_m2093660730_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Projector::set_aspectRatio(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Projector::get_orthographic()
extern "C"  bool Projector_get_orthographic_m278912762 (Projector_t4006592434 * __this, const MethodInfo* method)
{
	typedef bool (*Projector_get_orthographic_m278912762_ftn) (Projector_t4006592434 *);
	static Projector_get_orthographic_m278912762_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Projector_get_orthographic_m278912762_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Projector::get_orthographic()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Projector::set_orthographic(System.Boolean)
extern "C"  void Projector_set_orthographic_m1966511883 (Projector_t4006592434 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Projector_set_orthographic_m1966511883_ftn) (Projector_t4006592434 *, bool);
	static Projector_set_orthographic_m1966511883_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Projector_set_orthographic_m1966511883_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Projector::set_orthographic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Projector::get_orthographicSize()
extern "C"  float Projector_get_orthographicSize_m806034421 (Projector_t4006592434 * __this, const MethodInfo* method)
{
	typedef float (*Projector_get_orthographicSize_m806034421_ftn) (Projector_t4006592434 *);
	static Projector_get_orthographicSize_m806034421_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Projector_get_orthographicSize_m806034421_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Projector::get_orthographicSize()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Projector::set_orthographicSize(System.Single)
extern "C"  void Projector_set_orthographicSize_m3877893334 (Projector_t4006592434 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Projector_set_orthographicSize_m3877893334_ftn) (Projector_t4006592434 *, float);
	static Projector_set_orthographicSize_m3877893334_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Projector_set_orthographicSize_m3877893334_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Projector::set_orthographicSize(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Projector::get_ignoreLayers()
extern "C"  int32_t Projector_get_ignoreLayers_m273429284 (Projector_t4006592434 * __this, const MethodInfo* method)
{
	typedef int32_t (*Projector_get_ignoreLayers_m273429284_ftn) (Projector_t4006592434 *);
	static Projector_get_ignoreLayers_m273429284_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Projector_get_ignoreLayers_m273429284_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Projector::get_ignoreLayers()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Projector::set_ignoreLayers(System.Int32)
extern "C"  void Projector_set_ignoreLayers_m785104489 (Projector_t4006592434 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Projector_set_ignoreLayers_m785104489_ftn) (Projector_t4006592434 *, int32_t);
	static Projector_set_ignoreLayers_m785104489_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Projector_set_ignoreLayers_m785104489_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Projector::set_ignoreLayers(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material UnityEngine.Projector::get_material()
extern "C"  Material_t3870600107 * Projector_get_material_m245636454 (Projector_t4006592434 * __this, const MethodInfo* method)
{
	typedef Material_t3870600107 * (*Projector_get_material_m245636454_ftn) (Projector_t4006592434 *);
	static Projector_get_material_m245636454_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Projector_get_material_m245636454_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Projector::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Projector::set_material(UnityEngine.Material)
extern "C"  void Projector_set_material_m3870003373 (Projector_t4006592434 * __this, Material_t3870600107 * ___value0, const MethodInfo* method)
{
	typedef void (*Projector_set_material_m3870003373_ftn) (Projector_t4006592434 *, Material_t3870600107 *);
	static Projector_set_material_m3870003373_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Projector_set_material_m3870003373_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Projector::set_material(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m1741701746 (PropertyAttribute_t3531521085 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.QualitySettings::.ctor()
extern "C"  void QualitySettings__ctor_m2119214167 (QualitySettings_t719345784 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m570634126(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String[] UnityEngine.QualitySettings::get_names()
extern "C"  StringU5BU5D_t4054002952* QualitySettings_get_names_m284012725 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef StringU5BU5D_t4054002952* (*QualitySettings_get_names_m284012725_ftn) ();
	static QualitySettings_get_names_m284012725_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_names_m284012725_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_names()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.QualitySettings::GetQualityLevel()
extern "C"  int32_t QualitySettings_GetQualityLevel_m1740241520 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_GetQualityLevel_m1740241520_ftn) ();
	static QualitySettings_GetQualityLevel_m1740241520_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_GetQualityLevel_m1740241520_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::GetQualityLevel()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::SetQualityLevel(System.Int32,System.Boolean)
extern "C"  void QualitySettings_SetQualityLevel_m1369916048 (Il2CppObject * __this /* static, unused */, int32_t ___index0, bool ___applyExpensiveChanges1, const MethodInfo* method)
{
	typedef void (*QualitySettings_SetQualityLevel_m1369916048_ftn) (int32_t, bool);
	static QualitySettings_SetQualityLevel_m1369916048_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_SetQualityLevel_m1369916048_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::SetQualityLevel(System.Int32,System.Boolean)");
	_il2cpp_icall_func(___index0, ___applyExpensiveChanges1);
}
// System.Void UnityEngine.QualitySettings::SetQualityLevel(System.Int32)
extern "C"  void QualitySettings_SetQualityLevel_m962145613 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		int32_t L_0 = ___index0;
		bool L_1 = V_0;
		QualitySettings_SetQualityLevel_m1369916048(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.QualitySettings::IncreaseLevel(System.Boolean)
extern "C"  void QualitySettings_IncreaseLevel_m3712064654 (Il2CppObject * __this /* static, unused */, bool ___applyExpensiveChanges0, const MethodInfo* method)
{
	typedef void (*QualitySettings_IncreaseLevel_m3712064654_ftn) (bool);
	static QualitySettings_IncreaseLevel_m3712064654_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_IncreaseLevel_m3712064654_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::IncreaseLevel(System.Boolean)");
	_il2cpp_icall_func(___applyExpensiveChanges0);
}
// System.Void UnityEngine.QualitySettings::IncreaseLevel()
extern "C"  void QualitySettings_IncreaseLevel_m3950578519 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = V_0;
		QualitySettings_IncreaseLevel_m3712064654(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.QualitySettings::DecreaseLevel(System.Boolean)
extern "C"  void QualitySettings_DecreaseLevel_m2539185074 (Il2CppObject * __this /* static, unused */, bool ___applyExpensiveChanges0, const MethodInfo* method)
{
	typedef void (*QualitySettings_DecreaseLevel_m2539185074_ftn) (bool);
	static QualitySettings_DecreaseLevel_m2539185074_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_DecreaseLevel_m2539185074_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::DecreaseLevel(System.Boolean)");
	_il2cpp_icall_func(___applyExpensiveChanges0);
}
// System.Void UnityEngine.QualitySettings::DecreaseLevel()
extern "C"  void QualitySettings_DecreaseLevel_m962847099 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = V_0;
		QualitySettings_DecreaseLevel_m2539185074(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.QualitySettings::get_pixelLightCount()
extern "C"  int32_t QualitySettings_get_pixelLightCount_m516387691 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_pixelLightCount_m516387691_ftn) ();
	static QualitySettings_get_pixelLightCount_m516387691_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_pixelLightCount_m516387691_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_pixelLightCount()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_pixelLightCount(System.Int32)
extern "C"  void QualitySettings_set_pixelLightCount_m751067080 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_pixelLightCount_m751067080_ftn) (int32_t);
	static QualitySettings_set_pixelLightCount_m751067080_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_pixelLightCount_m751067080_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_pixelLightCount(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.ShadowProjection UnityEngine.QualitySettings::get_shadowProjection()
extern "C"  int32_t QualitySettings_get_shadowProjection_m2809772028 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_shadowProjection_m2809772028_ftn) ();
	static QualitySettings_get_shadowProjection_m2809772028_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_shadowProjection_m2809772028_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_shadowProjection()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_shadowProjection(UnityEngine.ShadowProjection)
extern "C"  void QualitySettings_set_shadowProjection_m3501455027 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_shadowProjection_m3501455027_ftn) (int32_t);
	static QualitySettings_set_shadowProjection_m3501455027_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_shadowProjection_m3501455027_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_shadowProjection(UnityEngine.ShadowProjection)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.QualitySettings::get_shadowCascades()
extern "C"  int32_t QualitySettings_get_shadowCascades_m2367020021 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_shadowCascades_m2367020021_ftn) ();
	static QualitySettings_get_shadowCascades_m2367020021_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_shadowCascades_m2367020021_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_shadowCascades()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_shadowCascades(System.Int32)
extern "C"  void QualitySettings_set_shadowCascades_m4032047290 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_shadowCascades_m4032047290_ftn) (int32_t);
	static QualitySettings_set_shadowCascades_m4032047290_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_shadowCascades_m4032047290_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_shadowCascades(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.QualitySettings::get_shadowDistance()
extern "C"  float QualitySettings_get_shadowDistance_m2372428063 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*QualitySettings_get_shadowDistance_m2372428063_ftn) ();
	static QualitySettings_get_shadowDistance_m2372428063_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_shadowDistance_m2372428063_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_shadowDistance()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_shadowDistance(System.Single)
extern "C"  void QualitySettings_set_shadowDistance_m3102645484 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_shadowDistance_m3102645484_ftn) (float);
	static QualitySettings_set_shadowDistance_m3102645484_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_shadowDistance_m3102645484_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_shadowDistance(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.QualitySettings::get_shadowNearPlaneOffset()
extern "C"  float QualitySettings_get_shadowNearPlaneOffset_m2379033215 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*QualitySettings_get_shadowNearPlaneOffset_m2379033215_ftn) ();
	static QualitySettings_get_shadowNearPlaneOffset_m2379033215_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_shadowNearPlaneOffset_m2379033215_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_shadowNearPlaneOffset()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_shadowNearPlaneOffset(System.Single)
extern "C"  void QualitySettings_set_shadowNearPlaneOffset_m1951001484 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_shadowNearPlaneOffset_m1951001484_ftn) (float);
	static QualitySettings_set_shadowNearPlaneOffset_m1951001484_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_shadowNearPlaneOffset_m1951001484_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_shadowNearPlaneOffset(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.QualitySettings::get_shadowCascade2Split()
extern "C"  float QualitySettings_get_shadowCascade2Split_m2965549044 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*QualitySettings_get_shadowCascade2Split_m2965549044_ftn) ();
	static QualitySettings_get_shadowCascade2Split_m2965549044_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_shadowCascade2Split_m2965549044_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_shadowCascade2Split()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_shadowCascade2Split(System.Single)
extern "C"  void QualitySettings_set_shadowCascade2Split_m4285344823 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_shadowCascade2Split_m4285344823_ftn) (float);
	static QualitySettings_set_shadowCascade2Split_m4285344823_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_shadowCascade2Split_m4285344823_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_shadowCascade2Split(System.Single)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Vector3 UnityEngine.QualitySettings::get_shadowCascade4Split()
extern "C"  Vector3_t4282066566  QualitySettings_get_shadowCascade4Split_m4077801120 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		QualitySettings_INTERNAL_get_shadowCascade4Split_m1025321231(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.QualitySettings::set_shadowCascade4Split(UnityEngine.Vector3)
extern "C"  void QualitySettings_set_shadowCascade4Split_m2216977127 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		QualitySettings_INTERNAL_set_shadowCascade4Split_m2091271963(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.QualitySettings::INTERNAL_get_shadowCascade4Split(UnityEngine.Vector3&)
extern "C"  void QualitySettings_INTERNAL_get_shadowCascade4Split_m1025321231 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_INTERNAL_get_shadowCascade4Split_m1025321231_ftn) (Vector3_t4282066566 *);
	static QualitySettings_INTERNAL_get_shadowCascade4Split_m1025321231_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_INTERNAL_get_shadowCascade4Split_m1025321231_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::INTERNAL_get_shadowCascade4Split(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.QualitySettings::INTERNAL_set_shadowCascade4Split(UnityEngine.Vector3&)
extern "C"  void QualitySettings_INTERNAL_set_shadowCascade4Split_m2091271963 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_INTERNAL_set_shadowCascade4Split_m2091271963_ftn) (Vector3_t4282066566 *);
	static QualitySettings_INTERNAL_set_shadowCascade4Split_m2091271963_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_INTERNAL_set_shadowCascade4Split_m2091271963_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::INTERNAL_set_shadowCascade4Split(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.QualitySettings::get_masterTextureLimit()
extern "C"  int32_t QualitySettings_get_masterTextureLimit_m2907593240 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_masterTextureLimit_m2907593240_ftn) ();
	static QualitySettings_get_masterTextureLimit_m2907593240_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_masterTextureLimit_m2907593240_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_masterTextureLimit()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_masterTextureLimit(System.Int32)
extern "C"  void QualitySettings_set_masterTextureLimit_m1173942365 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_masterTextureLimit_m1173942365_ftn) (int32_t);
	static QualitySettings_set_masterTextureLimit_m1173942365_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_masterTextureLimit_m1173942365_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_masterTextureLimit(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.AnisotropicFiltering UnityEngine.QualitySettings::get_anisotropicFiltering()
extern "C"  int32_t QualitySettings_get_anisotropicFiltering_m2086675408 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_anisotropicFiltering_m2086675408_ftn) ();
	static QualitySettings_get_anisotropicFiltering_m2086675408_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_anisotropicFiltering_m2086675408_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_anisotropicFiltering()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_anisotropicFiltering(UnityEngine.AnisotropicFiltering)
extern "C"  void QualitySettings_set_anisotropicFiltering_m2841520243 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_anisotropicFiltering_m2841520243_ftn) (int32_t);
	static QualitySettings_set_anisotropicFiltering_m2841520243_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_anisotropicFiltering_m2841520243_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_anisotropicFiltering(UnityEngine.AnisotropicFiltering)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.QualitySettings::get_lodBias()
extern "C"  float QualitySettings_get_lodBias_m1207867410 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*QualitySettings_get_lodBias_m1207867410_ftn) ();
	static QualitySettings_get_lodBias_m1207867410_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_lodBias_m1207867410_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_lodBias()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_lodBias(System.Single)
extern "C"  void QualitySettings_set_lodBias_m1212998873 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_lodBias_m1212998873_ftn) (float);
	static QualitySettings_set_lodBias_m1212998873_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_lodBias_m1212998873_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_lodBias(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.QualitySettings::get_maximumLODLevel()
extern "C"  int32_t QualitySettings_get_maximumLODLevel_m933671087 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_maximumLODLevel_m933671087_ftn) ();
	static QualitySettings_get_maximumLODLevel_m933671087_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_maximumLODLevel_m933671087_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_maximumLODLevel()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_maximumLODLevel(System.Int32)
extern "C"  void QualitySettings_set_maximumLODLevel_m2189285132 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_maximumLODLevel_m2189285132_ftn) (int32_t);
	static QualitySettings_set_maximumLODLevel_m2189285132_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_maximumLODLevel_m2189285132_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_maximumLODLevel(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.QualitySettings::get_particleRaycastBudget()
extern "C"  int32_t QualitySettings_get_particleRaycastBudget_m1146460756 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_particleRaycastBudget_m1146460756_ftn) ();
	static QualitySettings_get_particleRaycastBudget_m1146460756_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_particleRaycastBudget_m1146460756_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_particleRaycastBudget()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_particleRaycastBudget(System.Int32)
extern "C"  void QualitySettings_set_particleRaycastBudget_m221747249 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_particleRaycastBudget_m221747249_ftn) (int32_t);
	static QualitySettings_set_particleRaycastBudget_m221747249_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_particleRaycastBudget_m221747249_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_particleRaycastBudget(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.QualitySettings::get_softVegetation()
extern "C"  bool QualitySettings_get_softVegetation_m4089244520 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*QualitySettings_get_softVegetation_m4089244520_ftn) ();
	static QualitySettings_get_softVegetation_m4089244520_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_softVegetation_m4089244520_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_softVegetation()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_softVegetation(System.Boolean)
extern "C"  void QualitySettings_set_softVegetation_m3407837177 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_softVegetation_m3407837177_ftn) (bool);
	static QualitySettings_set_softVegetation_m3407837177_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_softVegetation_m3407837177_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_softVegetation(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.QualitySettings::get_realtimeReflectionProbes()
extern "C"  bool QualitySettings_get_realtimeReflectionProbes_m2548464105 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*QualitySettings_get_realtimeReflectionProbes_m2548464105_ftn) ();
	static QualitySettings_get_realtimeReflectionProbes_m2548464105_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_realtimeReflectionProbes_m2548464105_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_realtimeReflectionProbes()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_realtimeReflectionProbes(System.Boolean)
extern "C"  void QualitySettings_set_realtimeReflectionProbes_m4060018746 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_realtimeReflectionProbes_m4060018746_ftn) (bool);
	static QualitySettings_set_realtimeReflectionProbes_m4060018746_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_realtimeReflectionProbes_m4060018746_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_realtimeReflectionProbes(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.QualitySettings::get_billboardsFaceCameraPosition()
extern "C"  bool QualitySettings_get_billboardsFaceCameraPosition_m1402270191 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*QualitySettings_get_billboardsFaceCameraPosition_m1402270191_ftn) ();
	static QualitySettings_get_billboardsFaceCameraPosition_m1402270191_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_billboardsFaceCameraPosition_m1402270191_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_billboardsFaceCameraPosition()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_billboardsFaceCameraPosition(System.Boolean)
extern "C"  void QualitySettings_set_billboardsFaceCameraPosition_m2744127680 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_billboardsFaceCameraPosition_m2744127680_ftn) (bool);
	static QualitySettings_set_billboardsFaceCameraPosition_m2744127680_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_billboardsFaceCameraPosition_m2744127680_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_billboardsFaceCameraPosition(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.QualitySettings::get_maxQueuedFrames()
extern "C"  int32_t QualitySettings_get_maxQueuedFrames_m1382379177 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_maxQueuedFrames_m1382379177_ftn) ();
	static QualitySettings_get_maxQueuedFrames_m1382379177_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_maxQueuedFrames_m1382379177_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_maxQueuedFrames()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_maxQueuedFrames(System.Int32)
extern "C"  void QualitySettings_set_maxQueuedFrames_m402484742 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_maxQueuedFrames_m402484742_ftn) (int32_t);
	static QualitySettings_set_maxQueuedFrames_m402484742_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_maxQueuedFrames_m402484742_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_maxQueuedFrames(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.QualitySettings::get_vSyncCount()
extern "C"  int32_t QualitySettings_get_vSyncCount_m1698965652 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_vSyncCount_m1698965652_ftn) ();
	static QualitySettings_get_vSyncCount_m1698965652_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_vSyncCount_m1698965652_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_vSyncCount()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_vSyncCount(System.Int32)
extern "C"  void QualitySettings_set_vSyncCount_m2698975449 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_vSyncCount_m2698975449_ftn) (int32_t);
	static QualitySettings_set_vSyncCount_m2698975449_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_vSyncCount_m2698975449_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_vSyncCount(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.QualitySettings::get_antiAliasing()
extern "C"  int32_t QualitySettings_get_antiAliasing_m2055981962 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_antiAliasing_m2055981962_ftn) ();
	static QualitySettings_get_antiAliasing_m2055981962_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_antiAliasing_m2055981962_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_antiAliasing()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_antiAliasing(System.Int32)
extern "C"  void QualitySettings_set_antiAliasing_m3006369231 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_antiAliasing_m3006369231_ftn) (int32_t);
	static QualitySettings_set_antiAliasing_m3006369231_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_antiAliasing_m3006369231_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_antiAliasing(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.ColorSpace UnityEngine.QualitySettings::get_desiredColorSpace()
extern "C"  int32_t QualitySettings_get_desiredColorSpace_m377960350 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_desiredColorSpace_m377960350_ftn) ();
	static QualitySettings_get_desiredColorSpace_m377960350_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_desiredColorSpace_m377960350_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_desiredColorSpace()");
	return _il2cpp_icall_func();
}
// UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
extern "C"  int32_t QualitySettings_get_activeColorSpace_m2993616266 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_activeColorSpace_m2993616266_ftn) ();
	static QualitySettings_get_activeColorSpace_m2993616266_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_activeColorSpace_m2993616266_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_activeColorSpace()");
	return _il2cpp_icall_func();
}
// UnityEngine.BlendWeights UnityEngine.QualitySettings::get_blendWeights()
extern "C"  int32_t QualitySettings_get_blendWeights_m1992404338 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_blendWeights_m1992404338_ftn) ();
	static QualitySettings_get_blendWeights_m1992404338_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_blendWeights_m1992404338_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_blendWeights()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_blendWeights(UnityEngine.BlendWeights)
extern "C"  void QualitySettings_set_blendWeights_m1547724627 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_blendWeights_m1547724627_ftn) (int32_t);
	static QualitySettings_set_blendWeights_m1547724627_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_blendWeights_m1547724627_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_blendWeights(UnityEngine.BlendWeights)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.QualitySettings::get_asyncUploadTimeSlice()
extern "C"  int32_t QualitySettings_get_asyncUploadTimeSlice_m2441990174 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_asyncUploadTimeSlice_m2441990174_ftn) ();
	static QualitySettings_get_asyncUploadTimeSlice_m2441990174_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_asyncUploadTimeSlice_m2441990174_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_asyncUploadTimeSlice()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_asyncUploadTimeSlice(System.Int32)
extern "C"  void QualitySettings_set_asyncUploadTimeSlice_m1242308707 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_asyncUploadTimeSlice_m1242308707_ftn) (int32_t);
	static QualitySettings_set_asyncUploadTimeSlice_m1242308707_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_asyncUploadTimeSlice_m1242308707_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_asyncUploadTimeSlice(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.QualitySettings::get_asyncUploadBufferSize()
extern "C"  int32_t QualitySettings_get_asyncUploadBufferSize_m1555576458 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_asyncUploadBufferSize_m1555576458_ftn) ();
	static QualitySettings_get_asyncUploadBufferSize_m1555576458_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_asyncUploadBufferSize_m1555576458_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_asyncUploadBufferSize()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_asyncUploadBufferSize(System.Int32)
extern "C"  void QualitySettings_set_asyncUploadBufferSize_m3392377703 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_asyncUploadBufferSize_m3392377703_ftn) (int32_t);
	static QualitySettings_set_asyncUploadBufferSize_m3392377703_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_asyncUploadBufferSize_m3392377703_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_asyncUploadBufferSize(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Quaternion__ctor_m1100844011 (Quaternion_t1553702882 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		float L_2 = ___z2;
		__this->set_z_3(L_2);
		float L_3 = ___w3;
		__this->set_w_4(L_3);
		return;
	}
}
extern "C"  void Quaternion__ctor_m1100844011_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	Quaternion__ctor_m1100844011(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// System.Single UnityEngine.Quaternion::get_Item(System.Int32)
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1485379240;
extern const uint32_t Quaternion_get_Item_m358081806_MetadataUsageId;
extern "C"  float Quaternion_get_Item_m358081806 (Quaternion_t1553702882 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_get_Item_m358081806_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_001d;
		}
		if (L_1 == 1)
		{
			goto IL_0024;
		}
		if (L_1 == 2)
		{
			goto IL_002b;
		}
		if (L_1 == 3)
		{
			goto IL_0032;
		}
	}
	{
		goto IL_0039;
	}

IL_001d:
	{
		float L_2 = __this->get_x_1();
		return L_2;
	}

IL_0024:
	{
		float L_3 = __this->get_y_2();
		return L_3;
	}

IL_002b:
	{
		float L_4 = __this->get_z_3();
		return L_4;
	}

IL_0032:
	{
		float L_5 = __this->get_w_4();
		return L_5;
	}

IL_0039:
	{
		IndexOutOfRangeException_t3456360697 * L_6 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1621772274(L_6, _stringLiteral1485379240, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}
}
extern "C"  float Quaternion_get_Item_m358081806_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	return Quaternion_get_Item_m358081806(_thisAdjusted, ___index0, method);
}
// System.Void UnityEngine.Quaternion::set_Item(System.Int32,System.Single)
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1485379240;
extern const uint32_t Quaternion_set_Item_m3245486811_MetadataUsageId;
extern "C"  void Quaternion_set_Item_m3245486811 (Quaternion_t1553702882 * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_set_Item_m3245486811_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_001d;
		}
		if (L_1 == 1)
		{
			goto IL_0029;
		}
		if (L_1 == 2)
		{
			goto IL_0035;
		}
		if (L_1 == 3)
		{
			goto IL_0041;
		}
	}
	{
		goto IL_004d;
	}

IL_001d:
	{
		float L_2 = ___value1;
		__this->set_x_1(L_2);
		goto IL_0058;
	}

IL_0029:
	{
		float L_3 = ___value1;
		__this->set_y_2(L_3);
		goto IL_0058;
	}

IL_0035:
	{
		float L_4 = ___value1;
		__this->set_z_3(L_4);
		goto IL_0058;
	}

IL_0041:
	{
		float L_5 = ___value1;
		__this->set_w_4(L_5);
		goto IL_0058;
	}

IL_004d:
	{
		IndexOutOfRangeException_t3456360697 * L_6 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1621772274(L_6, _stringLiteral1485379240, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0058:
	{
		return;
	}
}
extern "C"  void Quaternion_set_Item_m3245486811_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	Quaternion_set_Item_m3245486811(_thisAdjusted, ___index0, ___value1, method);
}
// System.Void UnityEngine.Quaternion::Set(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Quaternion_Set_m2522415083 (Quaternion_t1553702882 * __this, float ___new_x0, float ___new_y1, float ___new_z2, float ___new_w3, const MethodInfo* method)
{
	{
		float L_0 = ___new_x0;
		__this->set_x_1(L_0);
		float L_1 = ___new_y1;
		__this->set_y_2(L_1);
		float L_2 = ___new_z2;
		__this->set_z_3(L_2);
		float L_3 = ___new_w3;
		__this->set_w_4(L_3);
		return;
	}
}
extern "C"  void Quaternion_Set_m2522415083_AdjustorThunk (Il2CppObject * __this, float ___new_x0, float ___new_y1, float ___new_z2, float ___new_w3, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	Quaternion_Set_m2522415083(_thisAdjusted, ___new_x0, ___new_y1, ___new_z2, ___new_w3, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t1553702882  Quaternion_get_identity_m1743882806 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Quaternion_t1553702882  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Quaternion__ctor_m1100844011(&L_0, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  float Quaternion_Dot_m580284 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___a0, Quaternion_t1553702882  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		float L_6 = (&___a0)->get_w_4();
		float L_7 = (&___b1)->get_w_4();
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
extern "C"  Quaternion_t1553702882  Quaternion_AngleAxis_m644124247 (Il2CppObject * __this /* static, unused */, float ___angle0, Vector3_t4282066566  ___axis1, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___angle0;
		Quaternion_INTERNAL_CALL_AngleAxis_m1562314763(NULL /*static, unused*/, L_0, (&___axis1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_AngleAxis_m1562314763 (Il2CppObject * __this /* static, unused */, float ___angle0, Vector3_t4282066566 * ___axis1, Quaternion_t1553702882 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_AngleAxis_m1562314763_ftn) (float, Vector3_t4282066566 *, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_AngleAxis_m1562314763_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_AngleAxis_m1562314763_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___angle0, ___axis1, ___value2);
}
// System.Void UnityEngine.Quaternion::ToAngleAxis(System.Single&,UnityEngine.Vector3&)
extern "C"  void Quaternion_ToAngleAxis_m791330738 (Quaternion_t1553702882 * __this, float* ___angle0, Vector3_t4282066566 * ___axis1, const MethodInfo* method)
{
	{
		Vector3_t4282066566 * L_0 = ___axis1;
		float* L_1 = ___angle0;
		Quaternion_Internal_ToAxisAngleRad_m3414012038(NULL /*static, unused*/, (*(Quaternion_t1553702882 *)__this), L_0, L_1, /*hidden argument*/NULL);
		float* L_2 = ___angle0;
		float* L_3 = ___angle0;
		*((float*)(L_2)) = (float)((float)((float)(*((float*)L_3))*(float)(57.29578f)));
		return;
	}
}
extern "C"  void Quaternion_ToAngleAxis_m791330738_AdjustorThunk (Il2CppObject * __this, float* ___angle0, Vector3_t4282066566 * ___axis1, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	Quaternion_ToAngleAxis_m791330738(_thisAdjusted, ___angle0, ___axis1, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::FromToRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t1553702882  Quaternion_FromToRotation_m2335489018 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___fromDirection0, Vector3_t4282066566  ___toDirection1, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_FromToRotation_m3717286698(NULL /*static, unused*/, (&___fromDirection0), (&___toDirection1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_FromToRotation_m3717286698 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___fromDirection0, Vector3_t4282066566 * ___toDirection1, Quaternion_t1553702882 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_FromToRotation_m3717286698_ftn) (Vector3_t4282066566 *, Vector3_t4282066566 *, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_FromToRotation_m3717286698_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_FromToRotation_m3717286698_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___fromDirection0, ___toDirection1, ___value2);
}
// System.Void UnityEngine.Quaternion::SetFromToRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Quaternion_SetFromToRotation_m3387926490 (Quaternion_t1553702882 * __this, Vector3_t4282066566  ___fromDirection0, Vector3_t4282066566  ___toDirection1, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___fromDirection0;
		Vector3_t4282066566  L_1 = ___toDirection1;
		Quaternion_t1553702882  L_2 = Quaternion_FromToRotation_m2335489018(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		(*(Quaternion_t1553702882 *)__this) = L_2;
		return;
	}
}
extern "C"  void Quaternion_SetFromToRotation_m3387926490_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___fromDirection0, Vector3_t4282066566  ___toDirection1, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	Quaternion_SetFromToRotation_m3387926490(_thisAdjusted, ___fromDirection0, ___toDirection1, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t1553702882  Quaternion_LookRotation_m2869326048 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___forward0, Vector3_t4282066566  ___upwards1, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_LookRotation_m1501255504(NULL /*static, unused*/, (&___forward0), (&___upwards1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3)
extern "C"  Quaternion_t1553702882  Quaternion_LookRotation_m1257501645 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___forward0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t1553702882  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector3_t4282066566  L_0 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Quaternion_INTERNAL_CALL_LookRotation_m1501255504(NULL /*static, unused*/, (&___forward0), (&V_0), (&V_1), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_LookRotation_m1501255504 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___forward0, Vector3_t4282066566 * ___upwards1, Quaternion_t1553702882 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_LookRotation_m1501255504_ftn) (Vector3_t4282066566 *, Vector3_t4282066566 *, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_LookRotation_m1501255504_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_LookRotation_m1501255504_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___forward0, ___upwards1, ___value2);
}
// System.Void UnityEngine.Quaternion::SetLookRotation(UnityEngine.Vector3)
extern "C"  void Quaternion_SetLookRotation_m363399085 (Quaternion_t1553702882 * __this, Vector3_t4282066566  ___view0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t4282066566  L_0 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_t4282066566  L_1 = ___view0;
		Vector3_t4282066566  L_2 = V_0;
		Quaternion_SetLookRotation_m1766511808(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Quaternion_SetLookRotation_m363399085_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___view0, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	Quaternion_SetLookRotation_m363399085(_thisAdjusted, ___view0, method);
}
// System.Void UnityEngine.Quaternion::SetLookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Quaternion_SetLookRotation_m1766511808 (Quaternion_t1553702882 * __this, Vector3_t4282066566  ___view0, Vector3_t4282066566  ___up1, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___view0;
		Vector3_t4282066566  L_1 = ___up1;
		Quaternion_t1553702882  L_2 = Quaternion_LookRotation_m2869326048(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		(*(Quaternion_t1553702882 *)__this) = L_2;
		return;
	}
}
extern "C"  void Quaternion_SetLookRotation_m1766511808_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___view0, Vector3_t4282066566  ___up1, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	Quaternion_SetLookRotation_m1766511808(_thisAdjusted, ___view0, ___up1, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t1553702882  Quaternion_Slerp_m844700366 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___a0, Quaternion_t1553702882  ___b1, float ___t2, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		Quaternion_INTERNAL_CALL_Slerp_m2927410052(NULL /*static, unused*/, (&___a0), (&___b1), L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Slerp_m2927410052 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___a0, Quaternion_t1553702882 * ___b1, float ___t2, Quaternion_t1553702882 * ___value3, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Slerp_m2927410052_ftn) (Quaternion_t1553702882 *, Quaternion_t1553702882 *, float, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_Slerp_m2927410052_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Slerp_m2927410052_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___a0, ___b1, ___t2, ___value3);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::SlerpUnclamped(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t1553702882  Quaternion_SlerpUnclamped_m1752874085 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___a0, Quaternion_t1553702882  ___b1, float ___t2, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		Quaternion_INTERNAL_CALL_SlerpUnclamped_m2836994545(NULL /*static, unused*/, (&___a0), (&___b1), L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_SlerpUnclamped(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_SlerpUnclamped_m2836994545 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___a0, Quaternion_t1553702882 * ___b1, float ___t2, Quaternion_t1553702882 * ___value3, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_SlerpUnclamped_m2836994545_ftn) (Quaternion_t1553702882 *, Quaternion_t1553702882 *, float, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_SlerpUnclamped_m2836994545_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_SlerpUnclamped_m2836994545_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_SlerpUnclamped(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___a0, ___b1, ___t2, ___value3);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Lerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t1553702882  Quaternion_Lerp_m1693481477 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___a0, Quaternion_t1553702882  ___b1, float ___t2, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		Quaternion_INTERNAL_CALL_Lerp_m3955033425(NULL /*static, unused*/, (&___a0), (&___b1), L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Lerp_m3955033425 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___a0, Quaternion_t1553702882 * ___b1, float ___t2, Quaternion_t1553702882 * ___value3, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Lerp_m3955033425_ftn) (Quaternion_t1553702882 *, Quaternion_t1553702882 *, float, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_Lerp_m3955033425_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Lerp_m3955033425_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___a0, ___b1, ___t2, ___value3);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LerpUnclamped(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t1553702882  Quaternion_LerpUnclamped_m3968174862 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___a0, Quaternion_t1553702882  ___b1, float ___t2, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		Quaternion_INTERNAL_CALL_LerpUnclamped_m3823797956(NULL /*static, unused*/, (&___a0), (&___b1), L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_LerpUnclamped(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_LerpUnclamped_m3823797956 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___a0, Quaternion_t1553702882 * ___b1, float ___t2, Quaternion_t1553702882 * ___value3, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_LerpUnclamped_m3823797956_ftn) (Quaternion_t1553702882 *, Quaternion_t1553702882 *, float, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_LerpUnclamped_m3823797956_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_LerpUnclamped_m3823797956_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_LerpUnclamped(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___a0, ___b1, ___t2, ___value3);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::RotateTowards(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Quaternion_RotateTowards_m180339351_MetadataUsageId;
extern "C"  Quaternion_t1553702882  Quaternion_RotateTowards_m180339351 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___from0, Quaternion_t1553702882  ___to1, float ___maxDegreesDelta2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_RotateTowards_m180339351_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Quaternion_t1553702882  L_0 = ___from0;
		Quaternion_t1553702882  L_1 = ___to1;
		float L_2 = Quaternion_Angle_m835424754(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		if ((!(((float)L_3) == ((float)(0.0f)))))
		{
			goto IL_0015;
		}
	}
	{
		Quaternion_t1553702882  L_4 = ___to1;
		return L_4;
	}

IL_0015:
	{
		float L_5 = ___maxDegreesDelta2;
		float L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Min_m2322067385(NULL /*static, unused*/, (1.0f), ((float)((float)L_5/(float)L_6)), /*hidden argument*/NULL);
		V_1 = L_7;
		Quaternion_t1553702882  L_8 = ___from0;
		Quaternion_t1553702882  L_9 = ___to1;
		float L_10 = V_1;
		Quaternion_t1553702882  L_11 = Quaternion_SlerpUnclamped_m1752874085(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  Quaternion_Inverse_m3542515566 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___rotation0, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_Inverse_m4175627710(NULL /*static, unused*/, (&___rotation0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Inverse_m4175627710 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___rotation0, Quaternion_t1553702882 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Inverse_m4175627710_ftn) (Quaternion_t1553702882 *, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_Inverse_m4175627710_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Inverse_m4175627710_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___rotation0, ___value1);
}
// System.String UnityEngine.Quaternion::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral843281963;
extern const uint32_t Quaternion_ToString_m1793285860_MetadataUsageId;
extern "C"  String_t* Quaternion_ToString_m1793285860 (Quaternion_t1553702882 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_ToString_m1793285860_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_x_1();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		float L_5 = __this->get_y_2();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		float L_9 = __this->get_z_3();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		float L_13 = __this->get_w_4();
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral843281963, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  String_t* Quaternion_ToString_m1793285860_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	return Quaternion_ToString_m1793285860(_thisAdjusted, method);
}
// System.String UnityEngine.Quaternion::ToString(System.String)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4163392375;
extern const uint32_t Quaternion_ToString_m233738910_MetadataUsageId;
extern "C"  String_t* Quaternion_ToString_m233738910 (Quaternion_t1553702882 * __this, String_t* ___format0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_ToString_m233738910_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		float* L_1 = __this->get_address_of_x_1();
		String_t* L_2 = ___format0;
		String_t* L_3 = Single_ToString_m639595682(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		float* L_5 = __this->get_address_of_y_2();
		String_t* L_6 = ___format0;
		String_t* L_7 = Single_ToString_m639595682(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		float* L_9 = __this->get_address_of_z_3();
		String_t* L_10 = ___format0;
		String_t* L_11 = Single_ToString_m639595682(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		float* L_13 = __this->get_address_of_w_4();
		String_t* L_14 = ___format0;
		String_t* L_15 = Single_ToString_m639595682(L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral4163392375, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  String_t* Quaternion_ToString_m233738910_AdjustorThunk (Il2CppObject * __this, String_t* ___format0, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	return Quaternion_ToString_m233738910(_thisAdjusted, ___format0, method);
}
// System.Single UnityEngine.Quaternion::Angle(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Quaternion_Angle_m835424754_MetadataUsageId;
extern "C"  float Quaternion_Angle_m835424754 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___a0, Quaternion_t1553702882  ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Angle_m835424754_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Quaternion_t1553702882  L_0 = ___a0;
		Quaternion_t1553702882  L_1 = ___b1;
		float L_2 = Quaternion_Dot_m580284(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_4 = fabsf(L_3);
		float L_5 = Mathf_Min_m2322067385(NULL /*static, unused*/, L_4, (1.0f), /*hidden argument*/NULL);
		float L_6 = acosf(L_5);
		return ((float)((float)((float)((float)L_6*(float)(2.0f)))*(float)(57.29578f)));
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C"  Vector3_t4282066566  Quaternion_get_eulerAngles_m997303795 (Quaternion_t1553702882 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = Quaternion_Internal_ToEulerRad_m1608666215(NULL /*static, unused*/, (*(Quaternion_t1553702882 *)__this), /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_0, (57.29578f), /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  Vector3_t4282066566  Quaternion_get_eulerAngles_m997303795_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	return Quaternion_get_eulerAngles_m997303795(_thisAdjusted, method);
}
// System.Void UnityEngine.Quaternion::set_eulerAngles(UnityEngine.Vector3)
extern "C"  void Quaternion_set_eulerAngles_m2840805376 (Quaternion_t1553702882 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___value0;
		Vector3_t4282066566  L_1 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_0, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_2 = Quaternion_Internal_FromEulerRad_m3681319598(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		(*(Quaternion_t1553702882 *)__this) = L_2;
		return;
	}
}
extern "C"  void Quaternion_set_eulerAngles_m2840805376_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	Quaternion_set_eulerAngles_m2840805376(_thisAdjusted, ___value0, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t1553702882  Quaternion_Euler_m1204688217 (Il2CppObject * __this /* static, unused */, float ___x0, float ___y1, float ___z2, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		Vector3_t4282066566  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2926210380(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t4282066566  L_4 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_3, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_5 = Quaternion_Internal_FromEulerRad_m3681319598(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
extern "C"  Quaternion_t1553702882  Quaternion_Euler_m1940911101 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___euler0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___euler0;
		Vector3_t4282066566  L_1 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_0, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_2 = Quaternion_Internal_FromEulerRad_m3681319598(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
extern "C"  Vector3_t4282066566  Quaternion_Internal_ToEulerRad_m1608666215 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___rotation0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1660134351(NULL /*static, unused*/, (&___rotation0), (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1660134351 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___rotation0, Vector3_t4282066566 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1660134351_ftn) (Quaternion_t1553702882 *, Vector3_t4282066566 *);
	static Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1660134351_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1660134351_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___rotation0, ___value1);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern "C"  Quaternion_t1553702882  Quaternion_Internal_FromEulerRad_m3681319598 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___euler0, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1312441940(NULL /*static, unused*/, (&___euler0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1312441940 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___euler0, Quaternion_t1553702882 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1312441940_ftn) (Vector3_t4282066566 *, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1312441940_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1312441940_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___euler0, ___value1);
}
// System.Void UnityEngine.Quaternion::Internal_ToAxisAngleRad(UnityEngine.Quaternion,UnityEngine.Vector3&,System.Single&)
extern "C"  void Quaternion_Internal_ToAxisAngleRad_m3414012038 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___q0, Vector3_t4282066566 * ___axis1, float* ___angle2, const MethodInfo* method)
{
	{
		Vector3_t4282066566 * L_0 = ___axis1;
		float* L_1 = ___angle2;
		Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m2360346913(NULL /*static, unused*/, (&___q0), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToAxisAngleRad(UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m2360346913 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___q0, Vector3_t4282066566 * ___axis1, float* ___angle2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m2360346913_ftn) (Quaternion_t1553702882 *, Vector3_t4282066566 *, float*);
	static Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m2360346913_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m2360346913_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToAxisAngleRad(UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&)");
	_il2cpp_icall_func(___q0, ___axis1, ___angle2);
}
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C"  int32_t Quaternion_GetHashCode_m3823258238 (Quaternion_t1553702882 * __this, const MethodInfo* method)
{
	{
		float* L_0 = __this->get_address_of_x_1();
		int32_t L_1 = Single_GetHashCode_m65342520(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_2();
		int32_t L_3 = Single_GetHashCode_m65342520(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_3();
		int32_t L_5 = Single_GetHashCode_m65342520(L_4, /*hidden argument*/NULL);
		float* L_6 = __this->get_address_of_w_4();
		int32_t L_7 = Single_GetHashCode_m65342520(L_6, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
extern "C"  int32_t Quaternion_GetHashCode_m3823258238_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	return Quaternion_GetHashCode_m3823258238(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern Il2CppClass* Quaternion_t1553702882_il2cpp_TypeInfo_var;
extern const uint32_t Quaternion_Equals_m3843409946_MetadataUsageId;
extern "C"  bool Quaternion_Equals_m3843409946 (Quaternion_t1553702882 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Equals_m3843409946_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Quaternion_t1553702882_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Quaternion_t1553702882 *)((Quaternion_t1553702882 *)UnBox (L_1, Quaternion_t1553702882_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_1();
		float L_3 = (&V_0)->get_x_1();
		bool L_4 = Single_Equals_m2110115959(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_2();
		float L_6 = (&V_0)->get_y_2();
		bool L_7 = Single_Equals_m2110115959(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_3();
		float L_9 = (&V_0)->get_z_3();
		bool L_10 = Single_Equals_m2110115959(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = __this->get_address_of_w_4();
		float L_12 = (&V_0)->get_w_4();
		bool L_13 = Single_Equals_m2110115959(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Quaternion_Equals_m3843409946_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	return Quaternion_Equals_m3843409946(_thisAdjusted, ___other0, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  Quaternion_op_Multiply_m3077481361 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___lhs0, Quaternion_t1553702882  ___rhs1, const MethodInfo* method)
{
	{
		float L_0 = (&___lhs0)->get_w_4();
		float L_1 = (&___rhs1)->get_x_1();
		float L_2 = (&___lhs0)->get_x_1();
		float L_3 = (&___rhs1)->get_w_4();
		float L_4 = (&___lhs0)->get_y_2();
		float L_5 = (&___rhs1)->get_z_3();
		float L_6 = (&___lhs0)->get_z_3();
		float L_7 = (&___rhs1)->get_y_2();
		float L_8 = (&___lhs0)->get_w_4();
		float L_9 = (&___rhs1)->get_y_2();
		float L_10 = (&___lhs0)->get_y_2();
		float L_11 = (&___rhs1)->get_w_4();
		float L_12 = (&___lhs0)->get_z_3();
		float L_13 = (&___rhs1)->get_x_1();
		float L_14 = (&___lhs0)->get_x_1();
		float L_15 = (&___rhs1)->get_z_3();
		float L_16 = (&___lhs0)->get_w_4();
		float L_17 = (&___rhs1)->get_z_3();
		float L_18 = (&___lhs0)->get_z_3();
		float L_19 = (&___rhs1)->get_w_4();
		float L_20 = (&___lhs0)->get_x_1();
		float L_21 = (&___rhs1)->get_y_2();
		float L_22 = (&___lhs0)->get_y_2();
		float L_23 = (&___rhs1)->get_x_1();
		float L_24 = (&___lhs0)->get_w_4();
		float L_25 = (&___rhs1)->get_w_4();
		float L_26 = (&___lhs0)->get_x_1();
		float L_27 = (&___rhs1)->get_x_1();
		float L_28 = (&___lhs0)->get_y_2();
		float L_29 = (&___rhs1)->get_y_2();
		float L_30 = (&___lhs0)->get_z_3();
		float L_31 = (&___rhs1)->get_z_3();
		Quaternion_t1553702882  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Quaternion__ctor_m1100844011(&L_32, ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))-(float)((float)((float)L_6*(float)L_7)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))-(float)((float)((float)L_14*(float)L_15)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))-(float)((float)((float)L_22*(float)L_23)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))-(float)((float)((float)L_26*(float)L_27))))-(float)((float)((float)L_28*(float)L_29))))-(float)((float)((float)L_30*(float)L_31)))), /*hidden argument*/NULL);
		return L_32;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Quaternion_op_Multiply_m3771288979 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___rotation0, Vector3_t4282066566  ___point1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	Vector3_t4282066566  V_12;
	memset(&V_12, 0, sizeof(V_12));
	{
		float L_0 = (&___rotation0)->get_x_1();
		V_0 = ((float)((float)L_0*(float)(2.0f)));
		float L_1 = (&___rotation0)->get_y_2();
		V_1 = ((float)((float)L_1*(float)(2.0f)));
		float L_2 = (&___rotation0)->get_z_3();
		V_2 = ((float)((float)L_2*(float)(2.0f)));
		float L_3 = (&___rotation0)->get_x_1();
		float L_4 = V_0;
		V_3 = ((float)((float)L_3*(float)L_4));
		float L_5 = (&___rotation0)->get_y_2();
		float L_6 = V_1;
		V_4 = ((float)((float)L_5*(float)L_6));
		float L_7 = (&___rotation0)->get_z_3();
		float L_8 = V_2;
		V_5 = ((float)((float)L_7*(float)L_8));
		float L_9 = (&___rotation0)->get_x_1();
		float L_10 = V_1;
		V_6 = ((float)((float)L_9*(float)L_10));
		float L_11 = (&___rotation0)->get_x_1();
		float L_12 = V_2;
		V_7 = ((float)((float)L_11*(float)L_12));
		float L_13 = (&___rotation0)->get_y_2();
		float L_14 = V_2;
		V_8 = ((float)((float)L_13*(float)L_14));
		float L_15 = (&___rotation0)->get_w_4();
		float L_16 = V_0;
		V_9 = ((float)((float)L_15*(float)L_16));
		float L_17 = (&___rotation0)->get_w_4();
		float L_18 = V_1;
		V_10 = ((float)((float)L_17*(float)L_18));
		float L_19 = (&___rotation0)->get_w_4();
		float L_20 = V_2;
		V_11 = ((float)((float)L_19*(float)L_20));
		float L_21 = V_4;
		float L_22 = V_5;
		float L_23 = (&___point1)->get_x_1();
		float L_24 = V_6;
		float L_25 = V_11;
		float L_26 = (&___point1)->get_y_2();
		float L_27 = V_7;
		float L_28 = V_10;
		float L_29 = (&___point1)->get_z_3();
		(&V_12)->set_x_1(((float)((float)((float)((float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_21+(float)L_22))))*(float)L_23))+(float)((float)((float)((float)((float)L_24-(float)L_25))*(float)L_26))))+(float)((float)((float)((float)((float)L_27+(float)L_28))*(float)L_29)))));
		float L_30 = V_6;
		float L_31 = V_11;
		float L_32 = (&___point1)->get_x_1();
		float L_33 = V_3;
		float L_34 = V_5;
		float L_35 = (&___point1)->get_y_2();
		float L_36 = V_8;
		float L_37 = V_9;
		float L_38 = (&___point1)->get_z_3();
		(&V_12)->set_y_2(((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))*(float)L_32))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_33+(float)L_34))))*(float)L_35))))+(float)((float)((float)((float)((float)L_36-(float)L_37))*(float)L_38)))));
		float L_39 = V_7;
		float L_40 = V_10;
		float L_41 = (&___point1)->get_x_1();
		float L_42 = V_8;
		float L_43 = V_9;
		float L_44 = (&___point1)->get_y_2();
		float L_45 = V_3;
		float L_46 = V_4;
		float L_47 = (&___point1)->get_z_3();
		(&V_12)->set_z_3(((float)((float)((float)((float)((float)((float)((float)((float)L_39-(float)L_40))*(float)L_41))+(float)((float)((float)((float)((float)L_42+(float)L_43))*(float)L_44))))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_45+(float)L_46))))*(float)L_47)))));
		Vector3_t4282066566  L_48 = V_12;
		return L_48;
	}
}
// System.Boolean UnityEngine.Quaternion::op_Equality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Equality_m3063235495 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___lhs0, Quaternion_t1553702882  ___rhs1, const MethodInfo* method)
{
	{
		Quaternion_t1553702882  L_0 = ___lhs0;
		Quaternion_t1553702882  L_1 = ___rhs1;
		float L_2 = Quaternion_Dot_m580284(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((float)L_2) > ((float)(0.999999f)))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Quaternion::op_Inequality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Inequality_m4197259746 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___lhs0, Quaternion_t1553702882  ___rhs1, const MethodInfo* method)
{
	{
		Quaternion_t1553702882  L_0 = ___lhs0;
		Quaternion_t1553702882  L_1 = ___rhs1;
		float L_2 = Quaternion_Dot_m580284(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)((!(((float)L_2) <= ((float)(0.999999f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.Quaternion
extern "C" void Quaternion_t1553702882_marshal_pinvoke(const Quaternion_t1553702882& unmarshaled, Quaternion_t1553702882_marshaled_pinvoke& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
	marshaled.___w_4 = unmarshaled.get_w_4();
}
extern "C" void Quaternion_t1553702882_marshal_pinvoke_back(const Quaternion_t1553702882_marshaled_pinvoke& marshaled, Quaternion_t1553702882& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
	float unmarshaled_w_temp_3 = 0.0f;
	unmarshaled_w_temp_3 = marshaled.___w_4;
	unmarshaled.set_w_4(unmarshaled_w_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Quaternion
extern "C" void Quaternion_t1553702882_marshal_pinvoke_cleanup(Quaternion_t1553702882_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Quaternion
extern "C" void Quaternion_t1553702882_marshal_com(const Quaternion_t1553702882& unmarshaled, Quaternion_t1553702882_marshaled_com& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
	marshaled.___w_4 = unmarshaled.get_w_4();
}
extern "C" void Quaternion_t1553702882_marshal_com_back(const Quaternion_t1553702882_marshaled_com& marshaled, Quaternion_t1553702882& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
	float unmarshaled_w_temp_3 = 0.0f;
	unmarshaled_w_temp_3 = marshaled.___w_4;
	unmarshaled.set_w_4(unmarshaled_w_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Quaternion
extern "C" void Quaternion_t1553702882_marshal_com_cleanup(Quaternion_t1553702882_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Random::.ctor()
extern "C"  void Random__ctor_m1288200842 (Random_t3156561159 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Random::get_seed()
extern "C"  int32_t Random_get_seed_m1525577876 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Random_get_seed_m1525577876_ftn) ();
	static Random_get_seed_m1525577876_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_get_seed_m1525577876_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::get_seed()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Random::set_seed(System.Int32)
extern "C"  void Random_set_seed_m58891993 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Random_set_seed_m58891993_ftn) (int32_t);
	static Random_set_seed_m58891993_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_set_seed_m58891993_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::set_seed(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C"  float Random_Range_m3362417303 (Il2CppObject * __this /* static, unused */, float ___min0, float ___max1, const MethodInfo* method)
{
	typedef float (*Random_Range_m3362417303_ftn) (float, float);
	static Random_Range_m3362417303_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_Range_m3362417303_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::Range(System.Single,System.Single)");
	return _il2cpp_icall_func(___min0, ___max1);
}
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m75452833 (Il2CppObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___min0;
		int32_t L_1 = ___max1;
		int32_t L_2 = Random_RandomRangeInt_m1203631415(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
extern "C"  int32_t Random_RandomRangeInt_m1203631415 (Il2CppObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const MethodInfo* method)
{
	typedef int32_t (*Random_RandomRangeInt_m1203631415_ftn) (int32_t, int32_t);
	static Random_RandomRangeInt_m1203631415_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_RandomRangeInt_m1203631415_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)");
	return _il2cpp_icall_func(___min0, ___max1);
}
// System.Single UnityEngine.Random::get_value()
extern "C"  float Random_get_value_m2402066692 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Random_get_value_m2402066692_ftn) ();
	static Random_get_value_m2402066692_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_get_value_m2402066692_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::get_value()");
	return _il2cpp_icall_func();
}
// UnityEngine.Vector3 UnityEngine.Random::get_insideUnitSphere()
extern "C"  Vector3_t4282066566  Random_get_insideUnitSphere_m1884270890 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Random_INTERNAL_get_insideUnitSphere_m1119768947(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Random::INTERNAL_get_insideUnitSphere(UnityEngine.Vector3&)
extern "C"  void Random_INTERNAL_get_insideUnitSphere_m1119768947 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Random_INTERNAL_get_insideUnitSphere_m1119768947_ftn) (Vector3_t4282066566 *);
	static Random_INTERNAL_get_insideUnitSphere_m1119768947_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_INTERNAL_get_insideUnitSphere_m1119768947_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::INTERNAL_get_insideUnitSphere(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Random::GetRandomUnitCircle(UnityEngine.Vector2&)
extern "C"  void Random_GetRandomUnitCircle_m3555525383 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___output0, const MethodInfo* method)
{
	typedef void (*Random_GetRandomUnitCircle_m3555525383_ftn) (Vector2_t4282066565 *);
	static Random_GetRandomUnitCircle_m3555525383_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_GetRandomUnitCircle_m3555525383_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::GetRandomUnitCircle(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___output0);
}
// UnityEngine.Vector2 UnityEngine.Random::get_insideUnitCircle()
extern "C"  Vector2_t4282066565  Random_get_insideUnitCircle_m3455477774 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Random_GetRandomUnitCircle_m3555525383(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Random::get_onUnitSphere()
extern "C"  Vector3_t4282066566  Random_get_onUnitSphere_m1999405197 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Random_INTERNAL_get_onUnitSphere_m1824529494(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Random::INTERNAL_get_onUnitSphere(UnityEngine.Vector3&)
extern "C"  void Random_INTERNAL_get_onUnitSphere_m1824529494 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Random_INTERNAL_get_onUnitSphere_m1824529494_ftn) (Vector3_t4282066566 *);
	static Random_INTERNAL_get_onUnitSphere_m1824529494_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_INTERNAL_get_onUnitSphere_m1824529494_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::INTERNAL_get_onUnitSphere(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Quaternion UnityEngine.Random::get_rotation()
extern "C"  Quaternion_t1553702882  Random_get_rotation_m390819995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Random_INTERNAL_get_rotation_m757737008(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Random::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C"  void Random_INTERNAL_get_rotation_m757737008 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___value0, const MethodInfo* method)
{
	typedef void (*Random_INTERNAL_get_rotation_m757737008_ftn) (Quaternion_t1553702882 *);
	static Random_INTERNAL_get_rotation_m757737008_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_INTERNAL_get_rotation_m757737008_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::INTERNAL_get_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Quaternion UnityEngine.Random::get_rotationUniform()
extern "C"  Quaternion_t1553702882  Random_get_rotationUniform_m231969979 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Random_INTERNAL_get_rotationUniform_m4138882876(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Random::INTERNAL_get_rotationUniform(UnityEngine.Quaternion&)
extern "C"  void Random_INTERNAL_get_rotationUniform_m4138882876 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___value0, const MethodInfo* method)
{
	typedef void (*Random_INTERNAL_get_rotationUniform_m4138882876_ftn) (Quaternion_t1553702882 *);
	static Random_INTERNAL_get_rotationUniform_m4138882876_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_INTERNAL_get_rotationUniform_m4138882876_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::INTERNAL_get_rotationUniform(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Color UnityEngine.Random::ColorHSV()
extern "C"  Color_t4194546905  Random_ColorHSV_m3233070633 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0 = Random_ColorHSV_m1574526501(NULL /*static, unused*/, (0.0f), (1.0f), (0.0f), (1.0f), (0.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Random::ColorHSV(System.Single,System.Single)
extern "C"  Color_t4194546905  Random_ColorHSV_m2622911879 (Il2CppObject * __this /* static, unused */, float ___hueMin0, float ___hueMax1, const MethodInfo* method)
{
	{
		float L_0 = ___hueMin0;
		float L_1 = ___hueMax1;
		Color_t4194546905  L_2 = Random_ColorHSV_m1574526501(NULL /*static, unused*/, L_0, L_1, (0.0f), (1.0f), (0.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Color UnityEngine.Random::ColorHSV(System.Single,System.Single,System.Single,System.Single)
extern "C"  Color_t4194546905  Random_ColorHSV_m837293585 (Il2CppObject * __this /* static, unused */, float ___hueMin0, float ___hueMax1, float ___saturationMin2, float ___saturationMax3, const MethodInfo* method)
{
	{
		float L_0 = ___hueMin0;
		float L_1 = ___hueMax1;
		float L_2 = ___saturationMin2;
		float L_3 = ___saturationMax3;
		Color_t4194546905  L_4 = Random_ColorHSV_m1574526501(NULL /*static, unused*/, L_0, L_1, L_2, L_3, (0.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Color UnityEngine.Random::ColorHSV(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  Color_t4194546905  Random_ColorHSV_m3889882011 (Il2CppObject * __this /* static, unused */, float ___hueMin0, float ___hueMax1, float ___saturationMin2, float ___saturationMax3, float ___valueMin4, float ___valueMax5, const MethodInfo* method)
{
	{
		float L_0 = ___hueMin0;
		float L_1 = ___hueMax1;
		float L_2 = ___saturationMin2;
		float L_3 = ___saturationMax3;
		float L_4 = ___valueMin4;
		float L_5 = ___valueMax5;
		Color_t4194546905  L_6 = Random_ColorHSV_m1574526501(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Color UnityEngine.Random::ColorHSV(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Random_ColorHSV_m1574526501_MetadataUsageId;
extern "C"  Color_t4194546905  Random_ColorHSV_m1574526501 (Il2CppObject * __this /* static, unused */, float ___hueMin0, float ___hueMax1, float ___saturationMin2, float ___saturationMax3, float ___valueMin4, float ___valueMax5, float ___alphaMin6, float ___alphaMax7, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Random_ColorHSV_m1574526501_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Color_t4194546905  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		float L_0 = ___hueMin0;
		float L_1 = ___hueMax1;
		float L_2 = Random_get_value_m2402066692(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = ___saturationMin2;
		float L_5 = ___saturationMax3;
		float L_6 = Random_get_value_m2402066692(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = ___valueMin4;
		float L_9 = ___valueMax5;
		float L_10 = Random_get_value_m2402066692(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		float L_12 = V_0;
		float L_13 = V_1;
		float L_14 = V_2;
		Color_t4194546905  L_15 = Color_HSVToRGB_m3997670326(NULL /*static, unused*/, L_12, L_13, L_14, (bool)1, /*hidden argument*/NULL);
		V_3 = L_15;
		float L_16 = ___alphaMin6;
		float L_17 = ___alphaMax7;
		float L_18 = Random_get_value_m2402066692(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_19 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_16, L_17, L_18, /*hidden argument*/NULL);
		(&V_3)->set_a_3(L_19);
		Color_t4194546905  L_20 = V_3;
		return L_20;
	}
}
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern "C"  void RangeAttribute__ctor_m1279576482 (RangeAttribute_t912008995 * __this, float ___min0, float ___max1, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m1741701746(__this, /*hidden argument*/NULL);
		float L_0 = ___min0;
		__this->set_min_0(L_0);
		float L_1 = ___max1;
		__this->set_max_1(L_1);
		return;
	}
}
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Ray__ctor_m2662468509 (Ray_t3134616544 * __this, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___origin0;
		__this->set_m_Origin_0(L_0);
		Vector3_t4282066566  L_1 = Vector3_get_normalized_m2650940353((&___direction1), /*hidden argument*/NULL);
		__this->set_m_Direction_1(L_1);
		return;
	}
}
extern "C"  void Ray__ctor_m2662468509_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	Ray__ctor_m2662468509(_thisAdjusted, ___origin0, ___direction1, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C"  Vector3_t4282066566  Ray_get_origin_m3064983562 (Ray_t3134616544 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Origin_0();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  Ray_get_origin_m3064983562_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	return Ray_get_origin_m3064983562(_thisAdjusted, method);
}
// System.Void UnityEngine.Ray::set_origin(UnityEngine.Vector3)
extern "C"  void Ray_set_origin_m1191170081 (Ray_t3134616544 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___value0;
		__this->set_m_Origin_0(L_0);
		return;
	}
}
extern "C"  void Ray_set_origin_m1191170081_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	Ray_set_origin_m1191170081(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t4282066566  Ray_get_direction_m3201866877 (Ray_t3134616544 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Direction_1();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  Ray_get_direction_m3201866877_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	return Ray_get_direction_m3201866877(_thisAdjusted, method);
}
// System.Void UnityEngine.Ray::set_direction(UnityEngine.Vector3)
extern "C"  void Ray_set_direction_m2672750186 (Ray_t3134616544 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = Vector3_get_normalized_m2650940353((&___value0), /*hidden argument*/NULL);
		__this->set_m_Direction_1(L_0);
		return;
	}
}
extern "C"  void Ray_set_direction_m2672750186_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	Ray_set_direction_m2672750186(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C"  Vector3_t4282066566  Ray_GetPoint_m1171104822 (Ray_t3134616544 * __this, float ___distance0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Origin_0();
		Vector3_t4282066566  L_1 = __this->get_m_Direction_1();
		float L_2 = ___distance0;
		Vector3_t4282066566  L_3 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t4282066566  L_4 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  Vector3_t4282066566  Ray_GetPoint_m1171104822_AdjustorThunk (Il2CppObject * __this, float ___distance0, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	return Ray_GetPoint_m1171104822(_thisAdjusted, ___distance0, method);
}
// System.String UnityEngine.Ray::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1741034756;
extern const uint32_t Ray_ToString_m1391619614_MetadataUsageId;
extern "C"  String_t* Ray_ToString_m1391619614 (Ray_t3134616544 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Ray_ToString_m1391619614_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector3_t4282066566  L_1 = __this->get_m_Origin_0();
		Vector3_t4282066566  L_2 = L_1;
		Il2CppObject * L_3 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		Vector3_t4282066566  L_5 = __this->get_m_Direction_1();
		Vector3_t4282066566  L_6 = L_5;
		Il2CppObject * L_7 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		String_t* L_8 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral1741034756, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  String_t* Ray_ToString_m1391619614_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	return Ray_ToString_m1391619614(_thisAdjusted, method);
}
// System.String UnityEngine.Ray::ToString(System.String)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1741034756;
extern const uint32_t Ray_ToString_m558862756_MetadataUsageId;
extern "C"  String_t* Ray_ToString_m558862756 (Ray_t3134616544 * __this, String_t* ___format0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Ray_ToString_m558862756_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector3_t4282066566 * L_1 = __this->get_address_of_m_Origin_0();
		String_t* L_2 = ___format0;
		String_t* L_3 = Vector3_ToString_m1073378494(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		Vector3_t4282066566 * L_5 = __this->get_address_of_m_Direction_1();
		String_t* L_6 = ___format0;
		String_t* L_7 = Vector3_ToString_m1073378494(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		String_t* L_8 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral1741034756, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  String_t* Ray_ToString_m558862756_AdjustorThunk (Il2CppObject * __this, String_t* ___format0, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	return Ray_ToString_m558862756(_thisAdjusted, ___format0, method);
}
// Conversion methods for marshalling of: UnityEngine.Ray
extern "C" void Ray_t3134616544_marshal_pinvoke(const Ray_t3134616544& unmarshaled, Ray_t3134616544_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_m_Origin_0(), marshaled.___m_Origin_0);
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_m_Direction_1(), marshaled.___m_Direction_1);
}
extern "C" void Ray_t3134616544_marshal_pinvoke_back(const Ray_t3134616544_marshaled_pinvoke& marshaled, Ray_t3134616544& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_Origin_temp_0;
	memset(&unmarshaled_m_Origin_temp_0, 0, sizeof(unmarshaled_m_Origin_temp_0));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___m_Origin_0, unmarshaled_m_Origin_temp_0);
	unmarshaled.set_m_Origin_0(unmarshaled_m_Origin_temp_0);
	Vector3_t4282066566  unmarshaled_m_Direction_temp_1;
	memset(&unmarshaled_m_Direction_temp_1, 0, sizeof(unmarshaled_m_Direction_temp_1));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___m_Direction_1, unmarshaled_m_Direction_temp_1);
	unmarshaled.set_m_Direction_1(unmarshaled_m_Direction_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Ray
extern "C" void Ray_t3134616544_marshal_pinvoke_cleanup(Ray_t3134616544_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___m_Origin_0);
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___m_Direction_1);
}
// Conversion methods for marshalling of: UnityEngine.Ray
extern "C" void Ray_t3134616544_marshal_com(const Ray_t3134616544& unmarshaled, Ray_t3134616544_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com(unmarshaled.get_m_Origin_0(), marshaled.___m_Origin_0);
	Vector3_t4282066566_marshal_com(unmarshaled.get_m_Direction_1(), marshaled.___m_Direction_1);
}
extern "C" void Ray_t3134616544_marshal_com_back(const Ray_t3134616544_marshaled_com& marshaled, Ray_t3134616544& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_Origin_temp_0;
	memset(&unmarshaled_m_Origin_temp_0, 0, sizeof(unmarshaled_m_Origin_temp_0));
	Vector3_t4282066566_marshal_com_back(marshaled.___m_Origin_0, unmarshaled_m_Origin_temp_0);
	unmarshaled.set_m_Origin_0(unmarshaled_m_Origin_temp_0);
	Vector3_t4282066566  unmarshaled_m_Direction_temp_1;
	memset(&unmarshaled_m_Direction_temp_1, 0, sizeof(unmarshaled_m_Direction_temp_1));
	Vector3_t4282066566_marshal_com_back(marshaled.___m_Direction_1, unmarshaled_m_Direction_temp_1);
	unmarshaled.set_m_Direction_1(unmarshaled_m_Direction_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Ray
extern "C" void Ray_t3134616544_marshal_com_cleanup(Ray_t3134616544_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___m_Origin_0);
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___m_Direction_1);
}
// System.Void UnityEngine.Ray2D::.ctor(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void Ray2D__ctor_m3747436081 (Ray2D_t4207993202 * __this, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = ___origin0;
		__this->set_m_Origin_0(L_0);
		Vector2_t4282066565  L_1 = Vector2_get_normalized_m123128511((&___direction1), /*hidden argument*/NULL);
		__this->set_m_Direction_1(L_1);
		return;
	}
}
extern "C"  void Ray2D__ctor_m3747436081_AdjustorThunk (Il2CppObject * __this, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, const MethodInfo* method)
{
	Ray2D_t4207993202 * _thisAdjusted = reinterpret_cast<Ray2D_t4207993202 *>(__this + 1);
	Ray2D__ctor_m3747436081(_thisAdjusted, ___origin0, ___direction1, method);
}
// UnityEngine.Vector2 UnityEngine.Ray2D::get_origin()
extern "C"  Vector2_t4282066565  Ray2D_get_origin_m2436519003 (Ray2D_t4207993202 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = __this->get_m_Origin_0();
		return L_0;
	}
}
extern "C"  Vector2_t4282066565  Ray2D_get_origin_m2436519003_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray2D_t4207993202 * _thisAdjusted = reinterpret_cast<Ray2D_t4207993202 *>(__this + 1);
	return Ray2D_get_origin_m2436519003(_thisAdjusted, method);
}
// System.Void UnityEngine.Ray2D::set_origin(UnityEngine.Vector2)
extern "C"  void Ray2D_set_origin_m1451430128 (Ray2D_t4207993202 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = ___value0;
		__this->set_m_Origin_0(L_0);
		return;
	}
}
extern "C"  void Ray2D_set_origin_m1451430128_AdjustorThunk (Il2CppObject * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	Ray2D_t4207993202 * _thisAdjusted = reinterpret_cast<Ray2D_t4207993202 *>(__this + 1);
	Ray2D_set_origin_m1451430128(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Ray2D::get_direction()
extern "C"  Vector2_t4282066565  Ray2D_get_direction_m2376632972 (Ray2D_t4207993202 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = __this->get_m_Direction_1();
		return L_0;
	}
}
extern "C"  Vector2_t4282066565  Ray2D_get_direction_m2376632972_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray2D_t4207993202 * _thisAdjusted = reinterpret_cast<Ray2D_t4207993202 *>(__this + 1);
	return Ray2D_get_direction_m2376632972(_thisAdjusted, method);
}
// System.Void UnityEngine.Ray2D::set_direction(UnityEngine.Vector2)
extern "C"  void Ray2D_set_direction_m3664764573 (Ray2D_t4207993202 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = Vector2_get_normalized_m123128511((&___value0), /*hidden argument*/NULL);
		__this->set_m_Direction_1(L_0);
		return;
	}
}
extern "C"  void Ray2D_set_direction_m3664764573_AdjustorThunk (Il2CppObject * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	Ray2D_t4207993202 * _thisAdjusted = reinterpret_cast<Ray2D_t4207993202 *>(__this + 1);
	Ray2D_set_direction_m3664764573(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Ray2D::GetPoint(System.Single)
extern "C"  Vector2_t4282066565  Ray2D_GetPoint_m617747269 (Ray2D_t4207993202 * __this, float ___distance0, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = __this->get_m_Origin_0();
		Vector2_t4282066565  L_1 = __this->get_m_Direction_1();
		float L_2 = ___distance0;
		Vector2_t4282066565  L_3 = Vector2_op_Multiply_m1738245082(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  Vector2_t4282066565  Ray2D_GetPoint_m617747269_AdjustorThunk (Il2CppObject * __this, float ___distance0, const MethodInfo* method)
{
	Ray2D_t4207993202 * _thisAdjusted = reinterpret_cast<Ray2D_t4207993202 *>(__this + 1);
	return Ray2D_GetPoint_m617747269(_thisAdjusted, ___distance0, method);
}
// System.String UnityEngine.Ray2D::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector2_t4282066565_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1741034756;
extern const uint32_t Ray2D_ToString_m3464444208_MetadataUsageId;
extern "C"  String_t* Ray2D_ToString_m3464444208 (Ray2D_t4207993202 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Ray2D_ToString_m3464444208_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector2_t4282066565  L_1 = __this->get_m_Origin_0();
		Vector2_t4282066565  L_2 = L_1;
		Il2CppObject * L_3 = Box(Vector2_t4282066565_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		Vector2_t4282066565  L_5 = __this->get_m_Direction_1();
		Vector2_t4282066565  L_6 = L_5;
		Il2CppObject * L_7 = Box(Vector2_t4282066565_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		String_t* L_8 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral1741034756, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  String_t* Ray2D_ToString_m3464444208_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray2D_t4207993202 * _thisAdjusted = reinterpret_cast<Ray2D_t4207993202 *>(__this + 1);
	return Ray2D_ToString_m3464444208(_thisAdjusted, method);
}
// System.String UnityEngine.Ray2D::ToString(System.String)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1741034756;
extern const uint32_t Ray2D_ToString_m2310853586_MetadataUsageId;
extern "C"  String_t* Ray2D_ToString_m2310853586 (Ray2D_t4207993202 * __this, String_t* ___format0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Ray2D_ToString_m2310853586_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector2_t4282066565 * L_1 = __this->get_address_of_m_Origin_0();
		String_t* L_2 = ___format0;
		String_t* L_3 = Vector2_ToString_m1042358687(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		Vector2_t4282066565 * L_5 = __this->get_address_of_m_Direction_1();
		String_t* L_6 = ___format0;
		String_t* L_7 = Vector2_ToString_m1042358687(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		String_t* L_8 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral1741034756, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  String_t* Ray2D_ToString_m2310853586_AdjustorThunk (Il2CppObject * __this, String_t* ___format0, const MethodInfo* method)
{
	Ray2D_t4207993202 * _thisAdjusted = reinterpret_cast<Ray2D_t4207993202 *>(__this + 1);
	return Ray2D_ToString_m2310853586(_thisAdjusted, ___format0, method);
}
// Conversion methods for marshalling of: UnityEngine.Ray2D
extern "C" void Ray2D_t4207993202_marshal_pinvoke(const Ray2D_t4207993202& unmarshaled, Ray2D_t4207993202_marshaled_pinvoke& marshaled)
{
	Vector2_t4282066565_marshal_pinvoke(unmarshaled.get_m_Origin_0(), marshaled.___m_Origin_0);
	Vector2_t4282066565_marshal_pinvoke(unmarshaled.get_m_Direction_1(), marshaled.___m_Direction_1);
}
extern "C" void Ray2D_t4207993202_marshal_pinvoke_back(const Ray2D_t4207993202_marshaled_pinvoke& marshaled, Ray2D_t4207993202& unmarshaled)
{
	Vector2_t4282066565  unmarshaled_m_Origin_temp_0;
	memset(&unmarshaled_m_Origin_temp_0, 0, sizeof(unmarshaled_m_Origin_temp_0));
	Vector2_t4282066565_marshal_pinvoke_back(marshaled.___m_Origin_0, unmarshaled_m_Origin_temp_0);
	unmarshaled.set_m_Origin_0(unmarshaled_m_Origin_temp_0);
	Vector2_t4282066565  unmarshaled_m_Direction_temp_1;
	memset(&unmarshaled_m_Direction_temp_1, 0, sizeof(unmarshaled_m_Direction_temp_1));
	Vector2_t4282066565_marshal_pinvoke_back(marshaled.___m_Direction_1, unmarshaled_m_Direction_temp_1);
	unmarshaled.set_m_Direction_1(unmarshaled_m_Direction_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Ray2D
extern "C" void Ray2D_t4207993202_marshal_pinvoke_cleanup(Ray2D_t4207993202_marshaled_pinvoke& marshaled)
{
	Vector2_t4282066565_marshal_pinvoke_cleanup(marshaled.___m_Origin_0);
	Vector2_t4282066565_marshal_pinvoke_cleanup(marshaled.___m_Direction_1);
}
// Conversion methods for marshalling of: UnityEngine.Ray2D
extern "C" void Ray2D_t4207993202_marshal_com(const Ray2D_t4207993202& unmarshaled, Ray2D_t4207993202_marshaled_com& marshaled)
{
	Vector2_t4282066565_marshal_com(unmarshaled.get_m_Origin_0(), marshaled.___m_Origin_0);
	Vector2_t4282066565_marshal_com(unmarshaled.get_m_Direction_1(), marshaled.___m_Direction_1);
}
extern "C" void Ray2D_t4207993202_marshal_com_back(const Ray2D_t4207993202_marshaled_com& marshaled, Ray2D_t4207993202& unmarshaled)
{
	Vector2_t4282066565  unmarshaled_m_Origin_temp_0;
	memset(&unmarshaled_m_Origin_temp_0, 0, sizeof(unmarshaled_m_Origin_temp_0));
	Vector2_t4282066565_marshal_com_back(marshaled.___m_Origin_0, unmarshaled_m_Origin_temp_0);
	unmarshaled.set_m_Origin_0(unmarshaled_m_Origin_temp_0);
	Vector2_t4282066565  unmarshaled_m_Direction_temp_1;
	memset(&unmarshaled_m_Direction_temp_1, 0, sizeof(unmarshaled_m_Direction_temp_1));
	Vector2_t4282066565_marshal_com_back(marshaled.___m_Direction_1, unmarshaled_m_Direction_temp_1);
	unmarshaled.set_m_Direction_1(unmarshaled_m_Direction_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Ray2D
extern "C" void Ray2D_t4207993202_marshal_com_cleanup(Ray2D_t4207993202_marshaled_com& marshaled)
{
	Vector2_t4282066565_marshal_com_cleanup(marshaled.___m_Origin_0);
	Vector2_t4282066565_marshal_com_cleanup(marshaled.___m_Direction_1);
}
// System.Void UnityEngine.RaycastHit::CalculateRaycastTexCoord(UnityEngine.Vector2&,UnityEngine.Collider,UnityEngine.Vector2,UnityEngine.Vector3,System.Int32,System.Int32)
extern "C"  void RaycastHit_CalculateRaycastTexCoord_m1670734366 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___output0, Collider_t2939674232 * ___col1, Vector2_t4282066565  ___uv2, Vector3_t4282066566  ___point3, int32_t ___face4, int32_t ___index5, const MethodInfo* method)
{
	{
		Vector2_t4282066565 * L_0 = ___output0;
		Collider_t2939674232 * L_1 = ___col1;
		int32_t L_2 = ___face4;
		int32_t L_3 = ___index5;
		RaycastHit_INTERNAL_CALL_CalculateRaycastTexCoord_m1112610553(NULL /*static, unused*/, L_0, L_1, (&___uv2), (&___point3), L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RaycastHit::INTERNAL_CALL_CalculateRaycastTexCoord(UnityEngine.Vector2&,UnityEngine.Collider,UnityEngine.Vector2&,UnityEngine.Vector3&,System.Int32,System.Int32)
extern "C"  void RaycastHit_INTERNAL_CALL_CalculateRaycastTexCoord_m1112610553 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___output0, Collider_t2939674232 * ___col1, Vector2_t4282066565 * ___uv2, Vector3_t4282066566 * ___point3, int32_t ___face4, int32_t ___index5, const MethodInfo* method)
{
	typedef void (*RaycastHit_INTERNAL_CALL_CalculateRaycastTexCoord_m1112610553_ftn) (Vector2_t4282066565 *, Collider_t2939674232 *, Vector2_t4282066565 *, Vector3_t4282066566 *, int32_t, int32_t);
	static RaycastHit_INTERNAL_CALL_CalculateRaycastTexCoord_m1112610553_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RaycastHit_INTERNAL_CALL_CalculateRaycastTexCoord_m1112610553_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RaycastHit::INTERNAL_CALL_CalculateRaycastTexCoord(UnityEngine.Vector2&,UnityEngine.Collider,UnityEngine.Vector2&,UnityEngine.Vector3&,System.Int32,System.Int32)");
	_il2cpp_icall_func(___output0, ___col1, ___uv2, ___point3, ___face4, ___index5);
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C"  Vector3_t4282066566  RaycastHit_get_point_m4165497838 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Point_0();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  RaycastHit_get_point_m4165497838_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_point_m4165497838(_thisAdjusted, method);
}
// System.Void UnityEngine.RaycastHit::set_point(UnityEngine.Vector3)
extern "C"  void RaycastHit_set_point_m4242621861 (RaycastHit_t4003175726 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___value0;
		__this->set_m_Point_0(L_0);
		return;
	}
}
extern "C"  void RaycastHit_set_point_m4242621861_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	RaycastHit_set_point_m4242621861(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C"  Vector3_t4282066566  RaycastHit_get_normal_m1346998891 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Normal_1();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  RaycastHit_get_normal_m1346998891_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_normal_m1346998891(_thisAdjusted, method);
}
// System.Void UnityEngine.RaycastHit::set_normal(UnityEngine.Vector3)
extern "C"  void RaycastHit_set_normal_m2487773780 (RaycastHit_t4003175726 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___value0;
		__this->set_m_Normal_1(L_0);
		return;
	}
}
extern "C"  void RaycastHit_set_normal_m2487773780_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	RaycastHit_set_normal_m2487773780(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_barycentricCoordinate()
extern "C"  Vector3_t4282066566  RaycastHit_get_barycentricCoordinate_m1133032404 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565 * L_0 = __this->get_address_of_m_UV_4();
		float L_1 = L_0->get_y_2();
		Vector2_t4282066565 * L_2 = __this->get_address_of_m_UV_4();
		float L_3 = L_2->get_x_1();
		Vector2_t4282066565 * L_4 = __this->get_address_of_m_UV_4();
		float L_5 = L_4->get_x_1();
		Vector2_t4282066565 * L_6 = __this->get_address_of_m_UV_4();
		float L_7 = L_6->get_y_2();
		Vector3_t4282066566  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector3__ctor_m2926210380(&L_8, ((float)((float)(1.0f)-(float)((float)((float)L_1+(float)L_3)))), L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  Vector3_t4282066566  RaycastHit_get_barycentricCoordinate_m1133032404_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_barycentricCoordinate_m1133032404(_thisAdjusted, method);
}
// System.Void UnityEngine.RaycastHit::set_barycentricCoordinate(UnityEngine.Vector3)
extern "C"  void RaycastHit_set_barycentricCoordinate_m1040518143 (RaycastHit_t4003175726 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___value0;
		Vector2_t4282066565  L_1 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_m_UV_4(L_1);
		return;
	}
}
extern "C"  void RaycastHit_set_barycentricCoordinate_m1040518143_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	RaycastHit_set_barycentricCoordinate_m1040518143(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C"  float RaycastHit_get_distance_m800944203 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Distance_3();
		return L_0;
	}
}
extern "C"  float RaycastHit_get_distance_m800944203_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_distance_m800944203(_thisAdjusted, method);
}
// System.Void UnityEngine.RaycastHit::set_distance(System.Single)
extern "C"  void RaycastHit_set_distance_m1076755160 (RaycastHit_t4003175726 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Distance_3(L_0);
		return;
	}
}
extern "C"  void RaycastHit_set_distance_m1076755160_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	RaycastHit_set_distance_m1076755160(_thisAdjusted, ___value0, method);
}
// System.Int32 UnityEngine.RaycastHit::get_triangleIndex()
extern "C"  int32_t RaycastHit_get_triangleIndex_m2720652482 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_FaceID_2();
		return L_0;
	}
}
extern "C"  int32_t RaycastHit_get_triangleIndex_m2720652482_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_triangleIndex_m2720652482(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.RaycastHit::get_textureCoord()
extern "C"  Vector2_t4282066565  RaycastHit_get_textureCoord_m2319334335 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Collider_t2939674232 * L_0 = RaycastHit_get_collider_m3116882274(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_1 = __this->get_m_UV_4();
		Vector3_t4282066566  L_2 = __this->get_m_Point_0();
		int32_t L_3 = __this->get_m_FaceID_2();
		RaycastHit_CalculateRaycastTexCoord_m1670734366(NULL /*static, unused*/, (&V_0), L_0, L_1, L_2, L_3, 0, /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = V_0;
		return L_4;
	}
}
extern "C"  Vector2_t4282066565  RaycastHit_get_textureCoord_m2319334335_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_textureCoord_m2319334335(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.RaycastHit::get_textureCoord2()
extern "C"  Vector2_t4282066565  RaycastHit_get_textureCoord2_m3179897269 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Collider_t2939674232 * L_0 = RaycastHit_get_collider_m3116882274(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_1 = __this->get_m_UV_4();
		Vector3_t4282066566  L_2 = __this->get_m_Point_0();
		int32_t L_3 = __this->get_m_FaceID_2();
		RaycastHit_CalculateRaycastTexCoord_m1670734366(NULL /*static, unused*/, (&V_0), L_0, L_1, L_2, L_3, 1, /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = V_0;
		return L_4;
	}
}
extern "C"  Vector2_t4282066565  RaycastHit_get_textureCoord2_m3179897269_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_textureCoord2_m3179897269(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.RaycastHit::get_lightmapCoord()
extern const MethodInfo* Component_GetComponent_TisRenderer_t3076687687_m1231607476_MethodInfo_var;
extern const uint32_t RaycastHit_get_lightmapCoord_m3264735532_MetadataUsageId;
extern "C"  Vector2_t4282066565  RaycastHit_get_lightmapCoord_m3264735532 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RaycastHit_get_lightmapCoord_m3264735532_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t4282066567  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Collider_t2939674232 * L_0 = RaycastHit_get_collider_m3116882274(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_1 = __this->get_m_UV_4();
		Vector3_t4282066566  L_2 = __this->get_m_Point_0();
		int32_t L_3 = __this->get_m_FaceID_2();
		RaycastHit_CalculateRaycastTexCoord_m1670734366(NULL /*static, unused*/, (&V_0), L_0, L_1, L_2, L_3, 1, /*hidden argument*/NULL);
		Collider_t2939674232 * L_4 = RaycastHit_get_collider_m3116882274(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Renderer_t3076687687 * L_5 = Component_GetComponent_TisRenderer_t3076687687_m1231607476(L_4, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m1231607476_MethodInfo_var);
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0083;
		}
	}
	{
		Collider_t2939674232 * L_7 = RaycastHit_get_collider_m3116882274(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Renderer_t3076687687 * L_8 = Component_GetComponent_TisRenderer_t3076687687_m1231607476(L_7, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m1231607476_MethodInfo_var);
		NullCheck(L_8);
		Vector4_t4282066567  L_9 = Renderer_get_lightmapScaleOffset_m2291266045(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		float L_10 = (&V_0)->get_x_1();
		float L_11 = (&V_1)->get_x_1();
		float L_12 = (&V_1)->get_z_3();
		(&V_0)->set_x_1(((float)((float)((float)((float)L_10*(float)L_11))+(float)L_12)));
		float L_13 = (&V_0)->get_y_2();
		float L_14 = (&V_1)->get_y_2();
		float L_15 = (&V_1)->get_w_4();
		(&V_0)->set_y_2(((float)((float)((float)((float)L_13*(float)L_14))+(float)L_15)));
	}

IL_0083:
	{
		Vector2_t4282066565  L_16 = V_0;
		return L_16;
	}
}
extern "C"  Vector2_t4282066565  RaycastHit_get_lightmapCoord_m3264735532_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_lightmapCoord_m3264735532(_thisAdjusted, method);
}
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C"  Collider_t2939674232 * RaycastHit_get_collider_m3116882274 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	{
		Collider_t2939674232 * L_0 = __this->get_m_Collider_5();
		return L_0;
	}
}
extern "C"  Collider_t2939674232 * RaycastHit_get_collider_m3116882274_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_collider_m3116882274(_thisAdjusted, method);
}
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern "C"  Rigidbody_t3346577219 * RaycastHit_get_rigidbody_m4137883432 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	Rigidbody_t3346577219 * G_B3_0 = NULL;
	{
		Collider_t2939674232 * L_0 = RaycastHit_get_collider_m3116882274(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Collider_t2939674232 * L_2 = RaycastHit_get_collider_m3116882274(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody_t3346577219 * L_3 = Collider_get_attachedRigidbody_m2821754842(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = ((Rigidbody_t3346577219 *)(NULL));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
extern "C"  Rigidbody_t3346577219 * RaycastHit_get_rigidbody_m4137883432_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_rigidbody_m4137883432(_thisAdjusted, method);
}
// UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
extern "C"  Transform_t1659122786 * RaycastHit_get_transform_m905149094 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	Rigidbody_t3346577219 * V_0 = NULL;
	{
		Rigidbody_t3346577219 * L_0 = RaycastHit_get_rigidbody_m4137883432(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rigidbody_t3346577219 * L_1 = V_0;
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Rigidbody_t3346577219 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = Component_get_transform_m4257140443(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001a:
	{
		Collider_t2939674232 * L_5 = RaycastHit_get_collider_m3116882274(__this, /*hidden argument*/NULL);
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		Collider_t2939674232 * L_7 = RaycastHit_get_collider_m3116882274(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = Component_get_transform_m4257140443(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0037:
	{
		return (Transform_t1659122786 *)NULL;
	}
}
extern "C"  Transform_t1659122786 * RaycastHit_get_transform_m905149094_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_transform_m905149094(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t4003175726_marshal_pinvoke(const RaycastHit_t4003175726& unmarshaled, RaycastHit_t4003175726_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit_t4003175726_marshal_pinvoke_back(const RaycastHit_t4003175726_marshaled_pinvoke& marshaled, RaycastHit_t4003175726& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t4003175726_marshal_pinvoke_cleanup(RaycastHit_t4003175726_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t4003175726_marshal_com(const RaycastHit_t4003175726& unmarshaled, RaycastHit_t4003175726_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit_t4003175726_marshal_com_back(const RaycastHit_t4003175726_marshaled_com& marshaled, RaycastHit_t4003175726& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t4003175726_marshal_com_cleanup(RaycastHit_t4003175726_marshaled_com& marshaled)
{
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_centroid()
extern "C"  Vector2_t4282066565  RaycastHit2D_get_centroid_m3413367543 (RaycastHit2D_t1374744384 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = __this->get_m_Centroid_0();
		return L_0;
	}
}
extern "C"  Vector2_t4282066565  RaycastHit2D_get_centroid_m3413367543_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_get_centroid_m3413367543(_thisAdjusted, method);
}
// System.Void UnityEngine.RaycastHit2D::set_centroid(UnityEngine.Vector2)
extern "C"  void RaycastHit2D_set_centroid_m66125482 (RaycastHit2D_t1374744384 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = ___value0;
		__this->set_m_Centroid_0(L_0);
		return;
	}
}
extern "C"  void RaycastHit2D_set_centroid_m66125482_AdjustorThunk (Il2CppObject * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	RaycastHit2D_set_centroid_m66125482(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern "C"  Vector2_t4282066565  RaycastHit2D_get_point_m2072691227 (RaycastHit2D_t1374744384 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = __this->get_m_Point_1();
		return L_0;
	}
}
extern "C"  Vector2_t4282066565  RaycastHit2D_get_point_m2072691227_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_get_point_m2072691227(_thisAdjusted, method);
}
// System.Void UnityEngine.RaycastHit2D::set_point(UnityEngine.Vector2)
extern "C"  void RaycastHit2D_set_point_m340775896 (RaycastHit2D_t1374744384 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = ___value0;
		__this->set_m_Point_1(L_0);
		return;
	}
}
extern "C"  void RaycastHit2D_set_point_m340775896_AdjustorThunk (Il2CppObject * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	RaycastHit2D_set_point_m340775896(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern "C"  Vector2_t4282066565  RaycastHit2D_get_normal_m894503390 (RaycastHit2D_t1374744384 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = __this->get_m_Normal_2();
		return L_0;
	}
}
extern "C"  Vector2_t4282066565  RaycastHit2D_get_normal_m894503390_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_get_normal_m894503390(_thisAdjusted, method);
}
// System.Void UnityEngine.RaycastHit2D::set_normal(UnityEngine.Vector2)
extern "C"  void RaycastHit2D_set_normal_m1789634083 (RaycastHit2D_t1374744384 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = ___value0;
		__this->set_m_Normal_2(L_0);
		return;
	}
}
extern "C"  void RaycastHit2D_set_normal_m1789634083_AdjustorThunk (Il2CppObject * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	RaycastHit2D_set_normal_m1789634083(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.RaycastHit2D::get_distance()
extern "C"  float RaycastHit2D_get_distance_m467570589 (RaycastHit2D_t1374744384 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Distance_3();
		return L_0;
	}
}
extern "C"  float RaycastHit2D_get_distance_m467570589_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_get_distance_m467570589(_thisAdjusted, method);
}
// System.Void UnityEngine.RaycastHit2D::set_distance(System.Single)
extern "C"  void RaycastHit2D_set_distance_m1501060550 (RaycastHit2D_t1374744384 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Distance_3(L_0);
		return;
	}
}
extern "C"  void RaycastHit2D_set_distance_m1501060550_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	RaycastHit2D_set_distance_m1501060550(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.RaycastHit2D::get_fraction()
extern "C"  float RaycastHit2D_get_fraction_m2313516650 (RaycastHit2D_t1374744384 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Fraction_4();
		return L_0;
	}
}
extern "C"  float RaycastHit2D_get_fraction_m2313516650_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_get_fraction_m2313516650(_thisAdjusted, method);
}
// System.Void UnityEngine.RaycastHit2D::set_fraction(System.Single)
extern "C"  void RaycastHit2D_set_fraction_m2263985177 (RaycastHit2D_t1374744384 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Fraction_4(L_0);
		return;
	}
}
extern "C"  void RaycastHit2D_set_fraction_m2263985177_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	RaycastHit2D_set_fraction_m2263985177(_thisAdjusted, ___value0, method);
}
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C"  Collider2D_t1552025098 * RaycastHit2D_get_collider_m789902306 (RaycastHit2D_t1374744384 * __this, const MethodInfo* method)
{
	{
		Collider2D_t1552025098 * L_0 = __this->get_m_Collider_5();
		return L_0;
	}
}
extern "C"  Collider2D_t1552025098 * RaycastHit2D_get_collider_m789902306_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_get_collider_m789902306(_thisAdjusted, method);
}
// UnityEngine.Rigidbody2D UnityEngine.RaycastHit2D::get_rigidbody()
extern "C"  Rigidbody2D_t1743771669 * RaycastHit2D_get_rigidbody_m1059160360 (RaycastHit2D_t1374744384 * __this, const MethodInfo* method)
{
	Rigidbody2D_t1743771669 * G_B3_0 = NULL;
	{
		Collider2D_t1552025098 * L_0 = RaycastHit2D_get_collider_m789902306(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Collider2D_t1552025098 * L_2 = RaycastHit2D_get_collider_m789902306(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody2D_t1743771669 * L_3 = Collider2D_get_attachedRigidbody_m2908627162(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = ((Rigidbody2D_t1743771669 *)(NULL));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
extern "C"  Rigidbody2D_t1743771669 * RaycastHit2D_get_rigidbody_m1059160360_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_get_rigidbody_m1059160360(_thisAdjusted, method);
}
// UnityEngine.Transform UnityEngine.RaycastHit2D::get_transform()
extern "C"  Transform_t1659122786 * RaycastHit2D_get_transform_m1318597140 (RaycastHit2D_t1374744384 * __this, const MethodInfo* method)
{
	Rigidbody2D_t1743771669 * V_0 = NULL;
	{
		Rigidbody2D_t1743771669 * L_0 = RaycastHit2D_get_rigidbody_m1059160360(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rigidbody2D_t1743771669 * L_1 = V_0;
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Rigidbody2D_t1743771669 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = Component_get_transform_m4257140443(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001a:
	{
		Collider2D_t1552025098 * L_5 = RaycastHit2D_get_collider_m789902306(__this, /*hidden argument*/NULL);
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		Collider2D_t1552025098 * L_7 = RaycastHit2D_get_collider_m789902306(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = Component_get_transform_m4257140443(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0037:
	{
		return (Transform_t1659122786 *)NULL;
	}
}
extern "C"  Transform_t1659122786 * RaycastHit2D_get_transform_m1318597140_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_get_transform_m1318597140(_thisAdjusted, method);
}
// System.Int32 UnityEngine.RaycastHit2D::CompareTo(UnityEngine.RaycastHit2D)
extern "C"  int32_t RaycastHit2D_CompareTo_m1690979580 (RaycastHit2D_t1374744384 * __this, RaycastHit2D_t1374744384  ___other0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Collider2D_t1552025098 * L_0 = RaycastHit2D_get_collider_m789902306(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return 1;
	}

IL_0013:
	{
		Collider2D_t1552025098 * L_2 = RaycastHit2D_get_collider_m789902306((&___other0), /*hidden argument*/NULL);
		bool L_3 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		return (-1);
	}

IL_0027:
	{
		float L_4 = RaycastHit2D_get_fraction_m2313516650(__this, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = RaycastHit2D_get_fraction_m2313516650((&___other0), /*hidden argument*/NULL);
		int32_t L_6 = Single_CompareTo_m3518345828((&V_0), L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  int32_t RaycastHit2D_CompareTo_m1690979580_AdjustorThunk (Il2CppObject * __this, RaycastHit2D_t1374744384  ___other0, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_CompareTo_m1690979580(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.RaycastHit2D::op_Implicit(UnityEngine.RaycastHit2D)
extern "C"  bool RaycastHit2D_op_Implicit_m3517997337 (Il2CppObject * __this /* static, unused */, RaycastHit2D_t1374744384  ___hit0, const MethodInfo* method)
{
	{
		Collider2D_t1552025098 * L_0 = RaycastHit2D_get_collider_m789902306((&___hit0), /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t1374744384_marshal_pinvoke(const RaycastHit2D_t1374744384& unmarshaled, RaycastHit2D_t1374744384_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit2D_t1374744384_marshal_pinvoke_back(const RaycastHit2D_t1374744384_marshaled_pinvoke& marshaled, RaycastHit2D_t1374744384& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t1374744384_marshal_pinvoke_cleanup(RaycastHit2D_t1374744384_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t1374744384_marshal_com(const RaycastHit2D_t1374744384& unmarshaled, RaycastHit2D_t1374744384_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit2D_t1374744384_marshal_com_back(const RaycastHit2D_t1374744384_marshaled_com& marshaled, RaycastHit2D_t1374744384& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t1374744384_marshal_com_cleanup(RaycastHit2D_t1374744384_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m3291325233 (Rect_t4241904616 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_m_XMin_0(L_0);
		float L_1 = ___y1;
		__this->set_m_YMin_1(L_1);
		float L_2 = ___width2;
		__this->set_m_Width_2(L_2);
		float L_3 = ___height3;
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect__ctor_m3291325233_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect__ctor_m3291325233(_thisAdjusted, ___x0, ___y1, ___width2, ___height3, method);
}
// System.Void UnityEngine.Rect::.ctor(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void Rect__ctor_m834295557 (Rect_t4241904616 * __this, Vector2_t4282066565  ___position0, Vector2_t4282066565  ___size1, const MethodInfo* method)
{
	{
		float L_0 = (&___position0)->get_x_1();
		__this->set_m_XMin_0(L_0);
		float L_1 = (&___position0)->get_y_2();
		__this->set_m_YMin_1(L_1);
		float L_2 = (&___size1)->get_x_1();
		__this->set_m_Width_2(L_2);
		float L_3 = (&___size1)->get_y_2();
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect__ctor_m834295557_AdjustorThunk (Il2CppObject * __this, Vector2_t4282066565  ___position0, Vector2_t4282066565  ___size1, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect__ctor_m834295557(_thisAdjusted, ___position0, ___size1, method);
}
// System.Void UnityEngine.Rect::.ctor(UnityEngine.Rect)
extern "C"  void Rect__ctor_m3858381006 (Rect_t4241904616 * __this, Rect_t4241904616  ___source0, const MethodInfo* method)
{
	{
		float L_0 = (&___source0)->get_m_XMin_0();
		__this->set_m_XMin_0(L_0);
		float L_1 = (&___source0)->get_m_YMin_1();
		__this->set_m_YMin_1(L_1);
		float L_2 = (&___source0)->get_m_Width_2();
		__this->set_m_Width_2(L_2);
		float L_3 = (&___source0)->get_m_Height_3();
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect__ctor_m3858381006_AdjustorThunk (Il2CppObject * __this, Rect_t4241904616  ___source0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect__ctor_m3858381006(_thisAdjusted, ___source0, method);
}
// UnityEngine.Rect UnityEngine.Rect::MinMaxRect(System.Single,System.Single,System.Single,System.Single)
extern "C"  Rect_t4241904616  Rect_MinMaxRect_m1534690677 (Il2CppObject * __this /* static, unused */, float ___xmin0, float ___ymin1, float ___xmax2, float ___ymax3, const MethodInfo* method)
{
	{
		float L_0 = ___xmin0;
		float L_1 = ___ymin1;
		float L_2 = ___xmax2;
		float L_3 = ___xmin0;
		float L_4 = ___ymax3;
		float L_5 = ___ymin1;
		Rect_t4241904616  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m3291325233(&L_6, L_0, L_1, ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.Rect::Set(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect_Set_m3128045745 (Rect_t4241904616 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_m_XMin_0(L_0);
		float L_1 = ___y1;
		__this->set_m_YMin_1(L_1);
		float L_2 = ___width2;
		__this->set_m_Width_2(L_2);
		float L_3 = ___height3;
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect_Set_m3128045745_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_Set_m3128045745(_thisAdjusted, ___x0, ___y1, ___width2, ___height3, method);
}
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m982385354 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_XMin_0();
		return L_0;
	}
}
extern "C"  float Rect_get_x_m982385354_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_x_m982385354(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_x(System.Single)
extern "C"  void Rect_set_x_m577970569 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_XMin_0(L_0);
		return;
	}
}
extern "C"  void Rect_set_x_m577970569_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_x_m577970569(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m982386315 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_YMin_1();
		return L_0;
	}
}
extern "C"  float Rect_get_y_m982386315_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_y_m982386315(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C"  void Rect_set_y_m67436392 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_YMin_1(L_0);
		return;
	}
}
extern "C"  void Rect_set_y_m67436392_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_y_m67436392(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern "C"  Vector2_t4282066565  Rect_get_position_m2933356232 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_XMin_0();
		float L_1 = __this->get_m_YMin_1();
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector2_t4282066565  Rect_get_position_m2933356232_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_position_m2933356232(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_position(UnityEngine.Vector2)
extern "C"  void Rect_set_position_m1164046905 (Rect_t4241904616 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		float L_0 = (&___value0)->get_x_1();
		__this->set_m_XMin_0(L_0);
		float L_1 = (&___value0)->get_y_2();
		__this->set_m_YMin_1(L_1);
		return;
	}
}
extern "C"  void Rect_set_position_m1164046905_AdjustorThunk (Il2CppObject * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_position_m1164046905(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern "C"  Vector2_t4282066565  Rect_get_center_m610643572 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = Rect_get_x_m982385354(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_m_Width_2();
		float L_2 = Rect_get_y_m982386315(__this, /*hidden argument*/NULL);
		float L_3 = __this->get_m_Height_3();
		Vector2_t4282066565  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m1517109030(&L_4, ((float)((float)L_0+(float)((float)((float)L_1/(float)(2.0f))))), ((float)((float)L_2+(float)((float)((float)L_3/(float)(2.0f))))), /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  Vector2_t4282066565  Rect_get_center_m610643572_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_center_m610643572(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_center(UnityEngine.Vector2)
extern "C"  void Rect_set_center_m1680296141 (Rect_t4241904616 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		float L_0 = (&___value0)->get_x_1();
		float L_1 = __this->get_m_Width_2();
		__this->set_m_XMin_0(((float)((float)L_0-(float)((float)((float)L_1/(float)(2.0f))))));
		float L_2 = (&___value0)->get_y_2();
		float L_3 = __this->get_m_Height_3();
		__this->set_m_YMin_1(((float)((float)L_2-(float)((float)((float)L_3/(float)(2.0f))))));
		return;
	}
}
extern "C"  void Rect_set_center_m1680296141_AdjustorThunk (Il2CppObject * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_center_m1680296141(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_min()
extern "C"  Vector2_t4282066565  Rect_get_min_m275942709 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = Rect_get_xMin_m371109962(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMin_m399739113(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector2_t4282066565  Rect_get_min_m275942709_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_min_m275942709(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_min(UnityEngine.Vector2)
extern "C"  void Rect_set_min_m1385452990 (Rect_t4241904616 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		float L_0 = (&___value0)->get_x_1();
		Rect_set_xMin_m265803321(__this, L_0, /*hidden argument*/NULL);
		float L_1 = (&___value0)->get_y_2();
		Rect_set_yMin_m3716298746(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Rect_set_min_m1385452990_AdjustorThunk (Il2CppObject * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_min_m1385452990(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_max()
extern "C"  Vector2_t4282066565  Rect_get_max_m275713991 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = Rect_get_xMax_m370881244(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMax_m399510395(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector2_t4282066565  Rect_get_max_m275713991_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_max_m275713991(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_max(UnityEngine.Vector2)
extern "C"  void Rect_set_max_m1111545324 (Rect_t4241904616 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		float L_0 = (&___value0)->get_x_1();
		Rect_set_xMax_m1513853159(__this, L_0, /*hidden argument*/NULL);
		float L_1 = (&___value0)->get_y_2();
		Rect_set_yMax_m669381288(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Rect_set_max_m1111545324_AdjustorThunk (Il2CppObject * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_max_m1111545324(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m2824209432 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Width_2();
		return L_0;
	}
}
extern "C"  float Rect_get_width_m2824209432_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_width_m2824209432(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_width(System.Single)
extern "C"  void Rect_set_width_m3771513595 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Width_2(L_0);
		return;
	}
}
extern "C"  void Rect_set_width_m3771513595_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_width_m3771513595(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m2154960823 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Height_3();
		return L_0;
	}
}
extern "C"  float Rect_get_height_m2154960823_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_height_m2154960823(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_height(System.Single)
extern "C"  void Rect_set_height_m3398820332 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Height_3(L_0);
		return;
	}
}
extern "C"  void Rect_set_height_m3398820332_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_height_m3398820332(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern "C"  Vector2_t4282066565  Rect_get_size_m136480416 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_Height_3();
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector2_t4282066565  Rect_get_size_m136480416_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_size_m136480416(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_size(UnityEngine.Vector2)
extern "C"  void Rect_set_size_m627285729 (Rect_t4241904616 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		float L_0 = (&___value0)->get_x_1();
		__this->set_m_Width_2(L_0);
		float L_1 = (&___value0)->get_y_2();
		__this->set_m_Height_3(L_1);
		return;
	}
}
extern "C"  void Rect_set_size_m627285729_AdjustorThunk (Il2CppObject * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_size_m627285729(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_xMin()
extern "C"  float Rect_get_xMin_m371109962 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_XMin_0();
		return L_0;
	}
}
extern "C"  float Rect_get_xMin_m371109962_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_xMin_m371109962(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_xMin(System.Single)
extern "C"  void Rect_set_xMin_m265803321 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_xMax_m370881244(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		__this->set_m_XMin_0(L_1);
		float L_2 = V_0;
		float L_3 = __this->get_m_XMin_0();
		__this->set_m_Width_2(((float)((float)L_2-(float)L_3)));
		return;
	}
}
extern "C"  void Rect_set_xMin_m265803321_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_xMin_m265803321(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_yMin()
extern "C"  float Rect_get_yMin_m399739113 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_YMin_1();
		return L_0;
	}
}
extern "C"  float Rect_get_yMin_m399739113_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_yMin_m399739113(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_yMin(System.Single)
extern "C"  void Rect_set_yMin_m3716298746 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_yMax_m399510395(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		__this->set_m_YMin_1(L_1);
		float L_2 = V_0;
		float L_3 = __this->get_m_YMin_1();
		__this->set_m_Height_3(((float)((float)L_2-(float)L_3)));
		return;
	}
}
extern "C"  void Rect_set_yMin_m3716298746_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_yMin_m3716298746(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_xMax()
extern "C"  float Rect_get_xMax_m370881244 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_XMin_0();
		return ((float)((float)L_0+(float)L_1));
	}
}
extern "C"  float Rect_get_xMax_m370881244_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_xMax_m370881244(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_xMax(System.Single)
extern "C"  void Rect_set_xMax_m1513853159 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		float L_1 = __this->get_m_XMin_0();
		__this->set_m_Width_2(((float)((float)L_0-(float)L_1)));
		return;
	}
}
extern "C"  void Rect_set_xMax_m1513853159_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_xMax_m1513853159(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_yMax()
extern "C"  float Rect_get_yMax_m399510395 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Height_3();
		float L_1 = __this->get_m_YMin_1();
		return ((float)((float)L_0+(float)L_1));
	}
}
extern "C"  float Rect_get_yMax_m399510395_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_yMax_m399510395(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_yMax(System.Single)
extern "C"  void Rect_set_yMax_m669381288 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		float L_1 = __this->get_m_YMin_1();
		__this->set_m_Height_3(((float)((float)L_0-(float)L_1)));
		return;
	}
}
extern "C"  void Rect_set_yMax_m669381288_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_yMax_m669381288(_thisAdjusted, ___value0, method);
}
// System.String UnityEngine.Rect::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2721079081;
extern const uint32_t Rect_ToString_m2093687658_MetadataUsageId;
extern "C"  String_t* Rect_ToString_m2093687658 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Rect_ToString_m2093687658_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = Rect_get_x_m982385354(__this, /*hidden argument*/NULL);
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		float L_5 = Rect_get_y_m982386315(__this, /*hidden argument*/NULL);
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		float L_9 = Rect_get_width_m2824209432(__this, /*hidden argument*/NULL);
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		float L_13 = Rect_get_height_m2154960823(__this, /*hidden argument*/NULL);
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral2721079081, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  String_t* Rect_ToString_m2093687658_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_ToString_m2093687658(_thisAdjusted, method);
}
// System.String UnityEngine.Rect::ToString(System.String)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2332408857;
extern const uint32_t Rect_ToString_m2480723928_MetadataUsageId;
extern "C"  String_t* Rect_ToString_m2480723928 (Rect_t4241904616 * __this, String_t* ___format0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Rect_ToString_m2480723928_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = Rect_get_x_m982385354(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ___format0;
		String_t* L_3 = Single_ToString_m639595682((&V_0), L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		float L_5 = Rect_get_y_m982386315(__this, /*hidden argument*/NULL);
		V_1 = L_5;
		String_t* L_6 = ___format0;
		String_t* L_7 = Single_ToString_m639595682((&V_1), L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		float L_9 = Rect_get_width_m2824209432(__this, /*hidden argument*/NULL);
		V_2 = L_9;
		String_t* L_10 = ___format0;
		String_t* L_11 = Single_ToString_m639595682((&V_2), L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		float L_13 = Rect_get_height_m2154960823(__this, /*hidden argument*/NULL);
		V_3 = L_13;
		String_t* L_14 = ___format0;
		String_t* L_15 = Single_ToString_m639595682((&V_3), L_14, /*hidden argument*/NULL);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral2332408857, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  String_t* Rect_ToString_m2480723928_AdjustorThunk (Il2CppObject * __this, String_t* ___format0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_ToString_m2480723928(_thisAdjusted, ___format0, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern "C"  bool Rect_Contains_m3556594010 (Rect_t4241904616 * __this, Vector2_t4282066565  ___point0, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_1();
		float L_1 = Rect_get_xMin_m371109962(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = (&___point0)->get_x_1();
		float L_3 = Rect_get_xMax_m370881244(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = (&___point0)->get_y_2();
		float L_5 = Rect_get_yMin_m399739113(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = (&___point0)->get_y_2();
		float L_7 = Rect_get_yMax_m399510395(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool Rect_Contains_m3556594010_AdjustorThunk (Il2CppObject * __this, Vector2_t4282066565  ___point0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_Contains_m3556594010(_thisAdjusted, ___point0, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C"  bool Rect_Contains_m3556594041 (Rect_t4241904616 * __this, Vector3_t4282066566  ___point0, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_1();
		float L_1 = Rect_get_xMin_m371109962(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = (&___point0)->get_x_1();
		float L_3 = Rect_get_xMax_m370881244(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = (&___point0)->get_y_2();
		float L_5 = Rect_get_yMin_m399739113(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = (&___point0)->get_y_2();
		float L_7 = Rect_get_yMax_m399510395(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool Rect_Contains_m3556594041_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___point0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_Contains_m3556594041(_thisAdjusted, ___point0, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3,System.Boolean)
extern "C"  bool Rect_Contains_m3804807396 (Rect_t4241904616 * __this, Vector3_t4282066566  ___point0, bool ___allowInverse1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = ___allowInverse1;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		Vector3_t4282066566  L_1 = ___point0;
		bool L_2 = Rect_Contains_m3556594041(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_000e:
	{
		V_0 = (bool)0;
		float L_3 = Rect_get_width_m2824209432(__this, /*hidden argument*/NULL);
		if ((!(((float)L_3) < ((float)(0.0f)))))
		{
			goto IL_0044;
		}
	}
	{
		float L_4 = (&___point0)->get_x_1();
		float L_5 = Rect_get_xMin_m371109962(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) <= ((float)L_5))))
		{
			goto IL_0044;
		}
	}
	{
		float L_6 = (&___point0)->get_x_1();
		float L_7 = Rect_get_xMax_m370881244(__this, /*hidden argument*/NULL);
		if ((((float)L_6) > ((float)L_7)))
		{
			goto IL_0078;
		}
	}

IL_0044:
	{
		float L_8 = Rect_get_width_m2824209432(__this, /*hidden argument*/NULL);
		if ((!(((float)L_8) >= ((float)(0.0f)))))
		{
			goto IL_007a;
		}
	}
	{
		float L_9 = (&___point0)->get_x_1();
		float L_10 = Rect_get_xMin_m371109962(__this, /*hidden argument*/NULL);
		if ((!(((float)L_9) >= ((float)L_10))))
		{
			goto IL_007a;
		}
	}
	{
		float L_11 = (&___point0)->get_x_1();
		float L_12 = Rect_get_xMax_m370881244(__this, /*hidden argument*/NULL);
		if ((!(((float)L_11) < ((float)L_12))))
		{
			goto IL_007a;
		}
	}

IL_0078:
	{
		V_0 = (bool)1;
	}

IL_007a:
	{
		bool L_13 = V_0;
		if (!L_13)
		{
			goto IL_00ea;
		}
	}
	{
		float L_14 = Rect_get_height_m2154960823(__this, /*hidden argument*/NULL);
		if ((!(((float)L_14) < ((float)(0.0f)))))
		{
			goto IL_00b4;
		}
	}
	{
		float L_15 = (&___point0)->get_y_2();
		float L_16 = Rect_get_yMin_m399739113(__this, /*hidden argument*/NULL);
		if ((!(((float)L_15) <= ((float)L_16))))
		{
			goto IL_00b4;
		}
	}
	{
		float L_17 = (&___point0)->get_y_2();
		float L_18 = Rect_get_yMax_m399510395(__this, /*hidden argument*/NULL);
		if ((((float)L_17) > ((float)L_18)))
		{
			goto IL_00e8;
		}
	}

IL_00b4:
	{
		float L_19 = Rect_get_height_m2154960823(__this, /*hidden argument*/NULL);
		if ((!(((float)L_19) >= ((float)(0.0f)))))
		{
			goto IL_00ea;
		}
	}
	{
		float L_20 = (&___point0)->get_y_2();
		float L_21 = Rect_get_yMin_m399739113(__this, /*hidden argument*/NULL);
		if ((!(((float)L_20) >= ((float)L_21))))
		{
			goto IL_00ea;
		}
	}
	{
		float L_22 = (&___point0)->get_y_2();
		float L_23 = Rect_get_yMax_m399510395(__this, /*hidden argument*/NULL);
		if ((!(((float)L_22) < ((float)L_23))))
		{
			goto IL_00ea;
		}
	}

IL_00e8:
	{
		return (bool)1;
	}

IL_00ea:
	{
		return (bool)0;
	}
}
extern "C"  bool Rect_Contains_m3804807396_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___point0, bool ___allowInverse1, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_Contains_m3804807396(_thisAdjusted, ___point0, ___allowInverse1, method);
}
// UnityEngine.Rect UnityEngine.Rect::OrderMinMax(UnityEngine.Rect)
extern "C"  Rect_t4241904616  Rect_OrderMinMax_m3424313368 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___rect0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = Rect_get_xMin_m371109962((&___rect0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMax_m370881244((&___rect0), /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0031;
		}
	}
	{
		float L_2 = Rect_get_xMin_m371109962((&___rect0), /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_xMax_m370881244((&___rect0), /*hidden argument*/NULL);
		Rect_set_xMin_m265803321((&___rect0), L_3, /*hidden argument*/NULL);
		float L_4 = V_0;
		Rect_set_xMax_m1513853159((&___rect0), L_4, /*hidden argument*/NULL);
	}

IL_0031:
	{
		float L_5 = Rect_get_yMin_m399739113((&___rect0), /*hidden argument*/NULL);
		float L_6 = Rect_get_yMax_m399510395((&___rect0), /*hidden argument*/NULL);
		if ((!(((float)L_5) > ((float)L_6))))
		{
			goto IL_0062;
		}
	}
	{
		float L_7 = Rect_get_yMin_m399739113((&___rect0), /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = Rect_get_yMax_m399510395((&___rect0), /*hidden argument*/NULL);
		Rect_set_yMin_m3716298746((&___rect0), L_8, /*hidden argument*/NULL);
		float L_9 = V_1;
		Rect_set_yMax_m669381288((&___rect0), L_9, /*hidden argument*/NULL);
	}

IL_0062:
	{
		Rect_t4241904616  L_10 = ___rect0;
		return L_10;
	}
}
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect)
extern "C"  bool Rect_Overlaps_m669681106 (Rect_t4241904616 * __this, Rect_t4241904616  ___other0, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_xMax_m370881244((&___other0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMin_m371109962(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = Rect_get_xMin_m371109962((&___other0), /*hidden argument*/NULL);
		float L_3 = Rect_get_xMax_m370881244(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = Rect_get_yMax_m399510395((&___other0), /*hidden argument*/NULL);
		float L_5 = Rect_get_yMin_m399739113(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) > ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = Rect_get_yMin_m399739113((&___other0), /*hidden argument*/NULL);
		float L_7 = Rect_get_yMax_m399510395(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool Rect_Overlaps_m669681106_AdjustorThunk (Il2CppObject * __this, Rect_t4241904616  ___other0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_Overlaps_m669681106(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect,System.Boolean)
extern "C"  bool Rect_Overlaps_m2751672171 (Rect_t4241904616 * __this, Rect_t4241904616  ___other0, bool ___allowInverse1, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		V_0 = (*(Rect_t4241904616 *)__this);
		bool L_0 = ___allowInverse1;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Rect_t4241904616  L_1 = V_0;
		Rect_t4241904616  L_2 = Rect_OrderMinMax_m3424313368(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Rect_t4241904616  L_3 = ___other0;
		Rect_t4241904616  L_4 = Rect_OrderMinMax_m3424313368(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		___other0 = L_4;
	}

IL_001c:
	{
		Rect_t4241904616  L_5 = ___other0;
		bool L_6 = Rect_Overlaps_m669681106((&V_0), L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  bool Rect_Overlaps_m2751672171_AdjustorThunk (Il2CppObject * __this, Rect_t4241904616  ___other0, bool ___allowInverse1, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_Overlaps_m2751672171(_thisAdjusted, ___other0, ___allowInverse1, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::NormalizedToPoint(UnityEngine.Rect,UnityEngine.Vector2)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Rect_NormalizedToPoint_m1773668035_MetadataUsageId;
extern "C"  Vector2_t4282066565  Rect_NormalizedToPoint_m1773668035 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___rectangle0, Vector2_t4282066565  ___normalizedRectCoordinates1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Rect_NormalizedToPoint_m1773668035_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = Rect_get_x_m982385354((&___rectangle0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMax_m370881244((&___rectangle0), /*hidden argument*/NULL);
		float L_2 = (&___normalizedRectCoordinates1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		float L_4 = Rect_get_y_m982386315((&___rectangle0), /*hidden argument*/NULL);
		float L_5 = Rect_get_yMax_m399510395((&___rectangle0), /*hidden argument*/NULL);
		float L_6 = (&___normalizedRectCoordinates1)->get_y_2();
		float L_7 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		Vector2_t4282066565  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector2__ctor_m1517109030(&L_8, L_3, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector2 UnityEngine.Rect::PointToNormalized(UnityEngine.Rect,UnityEngine.Vector2)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Rect_PointToNormalized_m643678311_MetadataUsageId;
extern "C"  Vector2_t4282066565  Rect_PointToNormalized_m643678311 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___rectangle0, Vector2_t4282066565  ___point1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Rect_PointToNormalized_m643678311_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = Rect_get_x_m982385354((&___rectangle0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMax_m370881244((&___rectangle0), /*hidden argument*/NULL);
		float L_2 = (&___point1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_3 = Mathf_InverseLerp_m152689993(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		float L_4 = Rect_get_y_m982386315((&___rectangle0), /*hidden argument*/NULL);
		float L_5 = Rect_get_yMax_m399510395((&___rectangle0), /*hidden argument*/NULL);
		float L_6 = (&___point1)->get_y_2();
		float L_7 = Mathf_InverseLerp_m152689993(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		Vector2_t4282066565  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector2__ctor_m1517109030(&L_8, L_3, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C"  int32_t Rect_GetHashCode_m89026168 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		float L_0 = Rect_get_x_m982385354(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Single_GetHashCode_m65342520((&V_0), /*hidden argument*/NULL);
		float L_2 = Rect_get_width_m2824209432(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Single_GetHashCode_m65342520((&V_1), /*hidden argument*/NULL);
		float L_4 = Rect_get_y_m982386315(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Single_GetHashCode_m65342520((&V_2), /*hidden argument*/NULL);
		float L_6 = Rect_get_height_m2154960823(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Single_GetHashCode_m65342520((&V_3), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
extern "C"  int32_t Rect_GetHashCode_m89026168_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_GetHashCode_m89026168(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern Il2CppClass* Rect_t4241904616_il2cpp_TypeInfo_var;
extern const uint32_t Rect_Equals_m1091722644_MetadataUsageId;
extern "C"  bool Rect_Equals_m1091722644 (Rect_t4241904616 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Rect_Equals_m1091722644_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Rect_t4241904616_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Rect_t4241904616 *)((Rect_t4241904616 *)UnBox (L_1, Rect_t4241904616_il2cpp_TypeInfo_var))));
		float L_2 = Rect_get_x_m982385354(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = Rect_get_x_m982385354((&V_0), /*hidden argument*/NULL);
		bool L_4 = Single_Equals_m2110115959((&V_1), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_007a;
		}
	}
	{
		float L_5 = Rect_get_y_m982386315(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Rect_get_y_m982386315((&V_0), /*hidden argument*/NULL);
		bool L_7 = Single_Equals_m2110115959((&V_2), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_007a;
		}
	}
	{
		float L_8 = Rect_get_width_m2824209432(__this, /*hidden argument*/NULL);
		V_3 = L_8;
		float L_9 = Rect_get_width_m2824209432((&V_0), /*hidden argument*/NULL);
		bool L_10 = Single_Equals_m2110115959((&V_3), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_007a;
		}
	}
	{
		float L_11 = Rect_get_height_m2154960823(__this, /*hidden argument*/NULL);
		V_4 = L_11;
		float L_12 = Rect_get_height_m2154960823((&V_0), /*hidden argument*/NULL);
		bool L_13 = Single_Equals_m2110115959((&V_4), L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_007b;
	}

IL_007a:
	{
		G_B7_0 = 0;
	}

IL_007b:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Rect_Equals_m1091722644_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_Equals_m1091722644(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.Rect::op_Inequality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Inequality_m2236552616 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___lhs0, Rect_t4241904616  ___rhs1, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m982385354((&___lhs0), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m982385354((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004e;
		}
	}
	{
		float L_2 = Rect_get_y_m982386315((&___lhs0), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m982386315((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004e;
		}
	}
	{
		float L_4 = Rect_get_width_m2824209432((&___lhs0), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m2824209432((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004e;
		}
	}
	{
		float L_6 = Rect_get_height_m2154960823((&___lhs0), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m2154960823((&___rhs1), /*hidden argument*/NULL);
		G_B5_0 = ((((int32_t)((((float)L_6) == ((float)L_7))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_004f;
	}

IL_004e:
	{
		G_B5_0 = 1;
	}

IL_004f:
	{
		return (bool)G_B5_0;
	}
}
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Equality_m1552341101 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___lhs0, Rect_t4241904616  ___rhs1, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m982385354((&___lhs0), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m982385354((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004b;
		}
	}
	{
		float L_2 = Rect_get_y_m982386315((&___lhs0), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m982386315((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004b;
		}
	}
	{
		float L_4 = Rect_get_width_m2824209432((&___lhs0), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m2824209432((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004b;
		}
	}
	{
		float L_6 = Rect_get_height_m2154960823((&___lhs0), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m2154960823((&___rhs1), /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) == ((float)L_7))? 1 : 0);
		goto IL_004c;
	}

IL_004b:
	{
		G_B5_0 = 0;
	}

IL_004c:
	{
		return (bool)G_B5_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.Rect
extern "C" void Rect_t4241904616_marshal_pinvoke(const Rect_t4241904616& unmarshaled, Rect_t4241904616_marshaled_pinvoke& marshaled)
{
	marshaled.___m_XMin_0 = unmarshaled.get_m_XMin_0();
	marshaled.___m_YMin_1 = unmarshaled.get_m_YMin_1();
	marshaled.___m_Width_2 = unmarshaled.get_m_Width_2();
	marshaled.___m_Height_3 = unmarshaled.get_m_Height_3();
}
extern "C" void Rect_t4241904616_marshal_pinvoke_back(const Rect_t4241904616_marshaled_pinvoke& marshaled, Rect_t4241904616& unmarshaled)
{
	float unmarshaled_m_XMin_temp_0 = 0.0f;
	unmarshaled_m_XMin_temp_0 = marshaled.___m_XMin_0;
	unmarshaled.set_m_XMin_0(unmarshaled_m_XMin_temp_0);
	float unmarshaled_m_YMin_temp_1 = 0.0f;
	unmarshaled_m_YMin_temp_1 = marshaled.___m_YMin_1;
	unmarshaled.set_m_YMin_1(unmarshaled_m_YMin_temp_1);
	float unmarshaled_m_Width_temp_2 = 0.0f;
	unmarshaled_m_Width_temp_2 = marshaled.___m_Width_2;
	unmarshaled.set_m_Width_2(unmarshaled_m_Width_temp_2);
	float unmarshaled_m_Height_temp_3 = 0.0f;
	unmarshaled_m_Height_temp_3 = marshaled.___m_Height_3;
	unmarshaled.set_m_Height_3(unmarshaled_m_Height_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Rect
extern "C" void Rect_t4241904616_marshal_pinvoke_cleanup(Rect_t4241904616_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Rect
extern "C" void Rect_t4241904616_marshal_com(const Rect_t4241904616& unmarshaled, Rect_t4241904616_marshaled_com& marshaled)
{
	marshaled.___m_XMin_0 = unmarshaled.get_m_XMin_0();
	marshaled.___m_YMin_1 = unmarshaled.get_m_YMin_1();
	marshaled.___m_Width_2 = unmarshaled.get_m_Width_2();
	marshaled.___m_Height_3 = unmarshaled.get_m_Height_3();
}
extern "C" void Rect_t4241904616_marshal_com_back(const Rect_t4241904616_marshaled_com& marshaled, Rect_t4241904616& unmarshaled)
{
	float unmarshaled_m_XMin_temp_0 = 0.0f;
	unmarshaled_m_XMin_temp_0 = marshaled.___m_XMin_0;
	unmarshaled.set_m_XMin_0(unmarshaled_m_XMin_temp_0);
	float unmarshaled_m_YMin_temp_1 = 0.0f;
	unmarshaled_m_YMin_temp_1 = marshaled.___m_YMin_1;
	unmarshaled.set_m_YMin_1(unmarshaled_m_YMin_temp_1);
	float unmarshaled_m_Width_temp_2 = 0.0f;
	unmarshaled_m_Width_temp_2 = marshaled.___m_Width_2;
	unmarshaled.set_m_Width_2(unmarshaled_m_Width_temp_2);
	float unmarshaled_m_Height_temp_3 = 0.0f;
	unmarshaled_m_Height_temp_3 = marshaled.___m_Height_3;
	unmarshaled.set_m_Height_3(unmarshaled_m_Height_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Rect
extern "C" void Rect_t4241904616_marshal_com_cleanup(Rect_t4241904616_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.RectOffset::.ctor()
extern "C"  void RectOffset__ctor_m2395783478 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		RectOffset_Init_m3353955934(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(UnityEngine.GUIStyle,System.IntPtr)
extern "C"  void RectOffset__ctor_m358348983 (RectOffset_t3056157787 * __this, GUIStyle_t2990928826 * ___sourceStyle0, IntPtr_t ___source1, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_0 = ___sourceStyle0;
		__this->set_m_SourceStyle_1(L_0);
		IntPtr_t L_1 = ___source1;
		__this->set_m_Ptr_0(L_1);
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void RectOffset__ctor_m2631865360 (RectOffset_t3056157787 * __this, int32_t ___left0, int32_t ___right1, int32_t ___top2, int32_t ___bottom3, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		RectOffset_Init_m3353955934(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___left0;
		RectOffset_set_left_m901965251(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___right1;
		RectOffset_set_right_m2119805444(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___top2;
		RectOffset_set_top_m3043172093(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___bottom3;
		RectOffset_set_bottom_m3840454247(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::Init()
extern "C"  void RectOffset_Init_m3353955934 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Init_m3353955934_ftn) (RectOffset_t3056157787 *);
	static RectOffset_Init_m3353955934_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Init_m3353955934_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::Cleanup()
extern "C"  void RectOffset_Cleanup_m2914212664 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Cleanup_m2914212664_ftn) (RectOffset_t3056157787 *);
	static RectOffset_Cleanup_m2914212664_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Cleanup_m2914212664_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C"  int32_t RectOffset_get_left_m4104523390 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_left_m4104523390_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_left_m4104523390_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_left_m4104523390_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_left()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_left(System.Int32)
extern "C"  void RectOffset_set_left_m901965251 (RectOffset_t3056157787 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_left_m901965251_ftn) (RectOffset_t3056157787 *, int32_t);
	static RectOffset_set_left_m901965251_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_left_m901965251_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_left(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_right()
extern "C"  int32_t RectOffset_get_right_m3831383975 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_right_m3831383975_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_right_m3831383975_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_right_m3831383975_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_right()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_right(System.Int32)
extern "C"  void RectOffset_set_right_m2119805444 (RectOffset_t3056157787 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_right_m2119805444_ftn) (RectOffset_t3056157787 *, int32_t);
	static RectOffset_set_right_m2119805444_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_right_m2119805444_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_right(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C"  int32_t RectOffset_get_top_m140097312 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_top_m140097312_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_top_m140097312_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_top_m140097312_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_top()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_top(System.Int32)
extern "C"  void RectOffset_set_top_m3043172093 (RectOffset_t3056157787 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_top_m3043172093_ftn) (RectOffset_t3056157787 *, int32_t);
	static RectOffset_set_top_m3043172093_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_top_m3043172093_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_top(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C"  int32_t RectOffset_get_bottom_m2106858018 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_bottom_m2106858018_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_bottom_m2106858018_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_bottom_m2106858018_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_bottom()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
extern "C"  void RectOffset_set_bottom_m3840454247 (RectOffset_t3056157787 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_bottom_m3840454247_ftn) (RectOffset_t3056157787 *, int32_t);
	static RectOffset_set_bottom_m3840454247_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_bottom_m3840454247_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_bottom(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_horizontal()
extern "C"  int32_t RectOffset_get_horizontal_m1186440923 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_horizontal_m1186440923_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_horizontal_m1186440923_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_horizontal_m1186440923_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_horizontal()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_vertical()
extern "C"  int32_t RectOffset_get_vertical_m3650431789 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_vertical_m3650431789_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_vertical_m3650431789_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_vertical_m3650431789_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_vertical()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rect UnityEngine.RectOffset::Add(UnityEngine.Rect)
extern "C"  Rect_t4241904616  RectOffset_Add_m396754502 (RectOffset_t3056157787 * __this, Rect_t4241904616  ___rect0, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectOffset_INTERNAL_CALL_Add_m775012682(NULL /*static, unused*/, __this, (&___rect0), (&V_0), /*hidden argument*/NULL);
		Rect_t4241904616  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectOffset::INTERNAL_CALL_Add(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)
extern "C"  void RectOffset_INTERNAL_CALL_Add_m775012682 (Il2CppObject * __this /* static, unused */, RectOffset_t3056157787 * ___self0, Rect_t4241904616 * ___rect1, Rect_t4241904616 * ___value2, const MethodInfo* method)
{
	typedef void (*RectOffset_INTERNAL_CALL_Add_m775012682_ftn) (RectOffset_t3056157787 *, Rect_t4241904616 *, Rect_t4241904616 *);
	static RectOffset_INTERNAL_CALL_Add_m775012682_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_INTERNAL_CALL_Add_m775012682_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::INTERNAL_CALL_Add(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)");
	_il2cpp_icall_func(___self0, ___rect1, ___value2);
}
// UnityEngine.Rect UnityEngine.RectOffset::Remove(UnityEngine.Rect)
extern "C"  Rect_t4241904616  RectOffset_Remove_m843726027 (RectOffset_t3056157787 * __this, Rect_t4241904616  ___rect0, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectOffset_INTERNAL_CALL_Remove_m1782283077(NULL /*static, unused*/, __this, (&___rect0), (&V_0), /*hidden argument*/NULL);
		Rect_t4241904616  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)
extern "C"  void RectOffset_INTERNAL_CALL_Remove_m1782283077 (Il2CppObject * __this /* static, unused */, RectOffset_t3056157787 * ___self0, Rect_t4241904616 * ___rect1, Rect_t4241904616 * ___value2, const MethodInfo* method)
{
	typedef void (*RectOffset_INTERNAL_CALL_Remove_m1782283077_ftn) (RectOffset_t3056157787 *, Rect_t4241904616 *, Rect_t4241904616 *);
	static RectOffset_INTERNAL_CALL_Remove_m1782283077_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_INTERNAL_CALL_Remove_m1782283077_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)");
	_il2cpp_icall_func(___self0, ___rect1, ___value2);
}
// System.Void UnityEngine.RectOffset::Finalize()
extern "C"  void RectOffset_Finalize_m3416542060 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			GUIStyle_t2990928826 * L_0 = __this->get_m_SourceStyle_1();
			if (L_0)
			{
				goto IL_0011;
			}
		}

IL_000b:
		{
			RectOffset_Cleanup_m2914212664(__this, /*hidden argument*/NULL);
		}

IL_0011:
		{
			IL2CPP_LEAVE(0x1D, FINALLY_0016);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0016;
	}

FINALLY_0016:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(22)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(22)
	{
		IL2CPP_JUMP_TBL(0x1D, IL_001d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_001d:
	{
		return;
	}
}
// System.String UnityEngine.RectOffset::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2126166178;
extern const uint32_t RectOffset_ToString_m2231965149_MetadataUsageId;
extern "C"  String_t* RectOffset_ToString_m2231965149 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectOffset_ToString_m2231965149_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_1 = RectOffset_get_left_m4104523390(__this, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		int32_t L_5 = RectOffset_get_right_m3831383975(__this, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		int32_t L_9 = RectOffset_get_top_m140097312(__this, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		int32_t L_13 = RectOffset_get_bottom_m2106858018(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral2126166178, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3056157787_marshal_pinvoke(const RectOffset_t3056157787& unmarshaled, RectOffset_t3056157787_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
extern "C" void RectOffset_t3056157787_marshal_pinvoke_back(const RectOffset_t3056157787_marshaled_pinvoke& marshaled, RectOffset_t3056157787& unmarshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3056157787_marshal_pinvoke_cleanup(RectOffset_t3056157787_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3056157787_marshal_com(const RectOffset_t3056157787& unmarshaled, RectOffset_t3056157787_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
extern "C" void RectOffset_t3056157787_marshal_com_back(const RectOffset_t3056157787_marshaled_com& marshaled, RectOffset_t3056157787& unmarshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3056157787_marshal_com_cleanup(RectOffset_t3056157787_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.RectTransform::.ctor()
extern "C"  void RectTransform__ctor_m106314993 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	{
		Transform__ctor_m3148116717(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::add_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern Il2CppClass* RectTransform_t972643934_il2cpp_TypeInfo_var;
extern Il2CppClass* ReapplyDrivenProperties_t779639188_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_add_reapplyDrivenProperties_m1968705467_MetadataUsageId;
extern "C"  void RectTransform_add_reapplyDrivenProperties_m1968705467 (Il2CppObject * __this /* static, unused */, ReapplyDrivenProperties_t779639188 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_add_reapplyDrivenProperties_m1968705467_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t779639188 * L_0 = ((RectTransform_t972643934_StaticFields*)RectTransform_t972643934_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		ReapplyDrivenProperties_t779639188 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((RectTransform_t972643934_StaticFields*)RectTransform_t972643934_il2cpp_TypeInfo_var->static_fields)->set_reapplyDrivenProperties_2(((ReapplyDrivenProperties_t779639188 *)CastclassSealed(L_2, ReapplyDrivenProperties_t779639188_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.RectTransform::remove_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern Il2CppClass* RectTransform_t972643934_il2cpp_TypeInfo_var;
extern Il2CppClass* ReapplyDrivenProperties_t779639188_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_remove_reapplyDrivenProperties_m2607613076_MetadataUsageId;
extern "C"  void RectTransform_remove_reapplyDrivenProperties_m2607613076 (Il2CppObject * __this /* static, unused */, ReapplyDrivenProperties_t779639188 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_remove_reapplyDrivenProperties_m2607613076_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t779639188 * L_0 = ((RectTransform_t972643934_StaticFields*)RectTransform_t972643934_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		ReapplyDrivenProperties_t779639188 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((RectTransform_t972643934_StaticFields*)RectTransform_t972643934_il2cpp_TypeInfo_var->static_fields)->set_reapplyDrivenProperties_2(((ReapplyDrivenProperties_t779639188 *)CastclassSealed(L_2, ReapplyDrivenProperties_t779639188_il2cpp_TypeInfo_var)));
		return;
	}
}
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C"  Rect_t4241904616  RectTransform_get_rect_m1566017036 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_rect_m1980775561(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t4241904616  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void RectTransform_INTERNAL_get_rect_m1980775561 (RectTransform_t972643934 * __this, Rect_t4241904616 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_rect_m1980775561_ftn) (RectTransform_t972643934 *, Rect_t4241904616 *);
	static RectTransform_INTERNAL_get_rect_m1980775561_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_rect_m1980775561_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
extern "C"  Vector2_t4282066565  RectTransform_get_anchorMin_m688674174 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_anchorMin_m1139643287(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMin_m989253483 (RectTransform_t972643934 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchorMin_m370577571(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMin_m1139643287 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMin_m1139643287_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_get_anchorMin_m1139643287_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMin_m1139643287_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMin_m370577571 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMin_m370577571_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_set_anchorMin_m370577571_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMin_m370577571_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
extern "C"  Vector2_t4282066565  RectTransform_get_anchorMax_m688445456 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_anchorMax_m1238440233(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMax_m715345817 (RectTransform_t972643934 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchorMax_m469374517(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMax_m1238440233 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMax_m1238440233_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_get_anchorMax_m1238440233_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMax_m1238440233_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMax_m469374517 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMax_m469374517_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_set_anchorMax_m469374517_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMax_m469374517_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.RectTransform::get_anchoredPosition3D()
extern "C"  Vector3_t4282066566  RectTransform_get_anchoredPosition3D_m1317976368 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector2_t4282066565  L_0 = RectTransform_get_anchoredPosition_m2318455998(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_1();
		float L_2 = (&V_0)->get_y_2();
		Vector3_t4282066566  L_3 = Transform_get_localPosition_m668140784(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = (&V_1)->get_z_3();
		Vector3_t4282066566  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector3__ctor_m2926210380(&L_5, L_1, L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void UnityEngine.RectTransform::set_anchoredPosition3D(UnityEngine.Vector3)
extern "C"  void RectTransform_set_anchoredPosition3D_m3457056443 (RectTransform_t972643934 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___value0)->get_x_1();
		float L_1 = (&___value0)->get_y_2();
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, L_0, L_1, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m1498949997(__this, L_2, /*hidden argument*/NULL);
		Vector3_t4282066566  L_3 = Transform_get_localPosition_m668140784(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = (&___value0)->get_z_3();
		(&V_0)->set_z_3(L_4);
		Vector3_t4282066566  L_5 = V_0;
		Transform_set_localPosition_m3504330903(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern "C"  Vector2_t4282066565  RectTransform_get_anchoredPosition_m2318455998 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_anchoredPosition_m840986985(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m1498949997 (RectTransform_t972643934 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchoredPosition_m2329865949(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchoredPosition_m840986985 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchoredPosition_m840986985_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_get_anchoredPosition_m840986985_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchoredPosition_m840986985_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchoredPosition_m2329865949 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchoredPosition_m2329865949_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_set_anchoredPosition_m2329865949_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchoredPosition_m2329865949_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C"  Vector2_t4282066565  RectTransform_get_sizeDelta_m4279424984 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_sizeDelta_m4117062897(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C"  void RectTransform_set_sizeDelta_m1223846609 (RectTransform_t972643934 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_sizeDelta_m3347997181(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_sizeDelta_m4117062897 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_sizeDelta_m4117062897_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_get_sizeDelta_m4117062897_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_sizeDelta_m4117062897_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_sizeDelta_m3347997181 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_sizeDelta_m3347997181_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_set_sizeDelta_m3347997181_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_sizeDelta_m3347997181_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern "C"  Vector2_t4282066565  RectTransform_get_pivot_m3785570595 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_pivot_m322514492(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern "C"  void RectTransform_set_pivot_m457344806 (RectTransform_t972643934 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_pivot_m237146440(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_pivot_m322514492 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_pivot_m322514492_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_get_pivot_m322514492_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_pivot_m322514492_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_pivot_m237146440 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_pivot_m237146440_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_set_pivot_m237146440_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_pivot_m237146440_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
extern Il2CppClass* RectTransform_t972643934_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_SendReapplyDrivenProperties_m2261331528_MetadataUsageId;
extern "C"  void RectTransform_SendReapplyDrivenProperties_m2261331528 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___driven0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_SendReapplyDrivenProperties_m2261331528_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t779639188 * L_0 = ((RectTransform_t972643934_StaticFields*)RectTransform_t972643934_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		ReapplyDrivenProperties_t779639188 * L_1 = ((RectTransform_t972643934_StaticFields*)RectTransform_t972643934_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		RectTransform_t972643934 * L_2 = ___driven0;
		NullCheck(L_1);
		ReapplyDrivenProperties_Invoke_m3880635155(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4014291545;
extern const uint32_t RectTransform_GetLocalCorners_m1867617311_MetadataUsageId;
extern "C"  void RectTransform_GetLocalCorners_m1867617311 (RectTransform_t972643934 * __this, Vector3U5BU5D_t215400611* ___fourCornersArray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetLocalCorners_m1867617311_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Vector3U5BU5D_t215400611* L_0 = ___fourCornersArray0;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_1 = ___fourCornersArray0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_001a;
		}
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral4014291545, /*hidden argument*/NULL);
		return;
	}

IL_001a:
	{
		Rect_t4241904616  L_2 = RectTransform_get_rect_m1566017036(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_x_m982385354((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = Rect_get_y_m982386315((&V_0), /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = Rect_get_xMax_m370881244((&V_0), /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_yMax_m399510395((&V_0), /*hidden argument*/NULL);
		V_4 = L_6;
		Vector3U5BU5D_t215400611* L_7 = ___fourCornersArray0;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		float L_8 = V_1;
		float L_9 = V_2;
		Vector3_t4282066566  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m2926210380(&L_10, L_8, L_9, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_10;
		Vector3U5BU5D_t215400611* L_11 = ___fourCornersArray0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		float L_12 = V_1;
		float L_13 = V_4;
		Vector3_t4282066566  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2926210380(&L_14, L_12, L_13, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_14;
		Vector3U5BU5D_t215400611* L_15 = ___fourCornersArray0;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		float L_16 = V_3;
		float L_17 = V_4;
		Vector3_t4282066566  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m2926210380(&L_18, L_16, L_17, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_18;
		Vector3U5BU5D_t215400611* L_19 = ___fourCornersArray0;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 3);
		float L_20 = V_3;
		float L_21 = V_2;
		Vector3_t4282066566  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector3__ctor_m2926210380(&L_22, L_20, L_21, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_22;
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetWorldCorners(UnityEngine.Vector3[])
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1157240146;
extern const uint32_t RectTransform_GetWorldCorners_m1829917190_MetadataUsageId;
extern "C"  void RectTransform_GetWorldCorners_m1829917190 (RectTransform_t972643934 * __this, Vector3U5BU5D_t215400611* ___fourCornersArray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetWorldCorners_m1829917190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t1659122786 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Vector3U5BU5D_t215400611* L_0 = ___fourCornersArray0;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_1 = ___fourCornersArray0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_001a;
		}
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral1157240146, /*hidden argument*/NULL);
		return;
	}

IL_001a:
	{
		Vector3U5BU5D_t215400611* L_2 = ___fourCornersArray0;
		RectTransform_GetLocalCorners_m1867617311(__this, L_2, /*hidden argument*/NULL);
		Transform_t1659122786 * L_3 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0051;
	}

IL_002f:
	{
		Vector3U5BU5D_t215400611* L_4 = ___fourCornersArray0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Transform_t1659122786 * L_6 = V_0;
		Vector3U5BU5D_t215400611* L_7 = ___fourCornersArray0;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		NullCheck(L_6);
		Vector3_t4282066566  L_9 = Transform_TransformPoint_m437395512(L_6, (*(Vector3_t4282066566 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5)))) = L_9;
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0051:
	{
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) < ((int32_t)4)))
		{
			goto IL_002f;
		}
	}
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_offsetMin()
extern "C"  Vector2_t4282066565  RectTransform_get_offsetMin_m3509071456 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = RectTransform_get_anchoredPosition_m2318455998(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_1 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_2 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_3 = Vector2_Scale_m1743563745(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.RectTransform::set_offsetMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_offsetMin_m951793481 (RectTransform_t972643934 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565  L_0 = ___value0;
		Vector2_t4282066565  L_1 = RectTransform_get_anchoredPosition_m2318455998(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_2 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_3 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = Vector2_Scale_m1743563745(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Vector2_t4282066565  L_5 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		Vector2_t4282066565  L_6 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector2_t4282066565  L_7 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_8 = V_0;
		Vector2_t4282066565  L_9 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m1223846609(__this, L_9, /*hidden argument*/NULL);
		Vector2_t4282066565  L_10 = RectTransform_get_anchoredPosition_m2318455998(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_11 = V_0;
		Vector2_t4282066565  L_12 = Vector2_get_one_m2767488832(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t4282066565  L_13 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_14 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Vector2_t4282066565  L_15 = Vector2_Scale_m1743563745(NULL /*static, unused*/, L_11, L_14, /*hidden argument*/NULL);
		Vector2_t4282066565  L_16 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_10, L_15, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m1498949997(__this, L_16, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_offsetMax()
extern "C"  Vector2_t4282066565  RectTransform_get_offsetMax_m3508842738 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = RectTransform_get_anchoredPosition_m2318455998(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_1 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_2 = Vector2_get_one_m2767488832(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t4282066565  L_3 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Vector2_t4282066565  L_5 = Vector2_Scale_m1743563745(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		Vector2_t4282066565  L_6 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.RectTransform::set_offsetMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_offsetMax_m677885815 (RectTransform_t972643934 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565  L_0 = ___value0;
		Vector2_t4282066565  L_1 = RectTransform_get_anchoredPosition_m2318455998(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_2 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_3 = Vector2_get_one_m2767488832(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_5 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Vector2_t4282066565  L_6 = Vector2_Scale_m1743563745(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		Vector2_t4282066565  L_7 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_1, L_6, /*hidden argument*/NULL);
		Vector2_t4282066565  L_8 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_0, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		Vector2_t4282066565  L_9 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_10 = V_0;
		Vector2_t4282066565  L_11 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m1223846609(__this, L_11, /*hidden argument*/NULL);
		Vector2_t4282066565  L_12 = RectTransform_get_anchoredPosition_m2318455998(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_13 = V_0;
		Vector2_t4282066565  L_14 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_15 = Vector2_Scale_m1743563745(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Vector2_t4282066565  L_16 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_12, L_15, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m1498949997(__this, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetInsetAndSizeFromParentEdge(UnityEngine.RectTransform/Edge,System.Single,System.Single)
extern "C"  void RectTransform_SetInsetAndSizeFromParentEdge_m1924354604 (RectTransform_t972643934 * __this, int32_t ___edge0, float ___inset1, float ___size2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	float V_2 = 0.0f;
	Vector2_t4282066565  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t4282066565  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t4282066565  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector2_t4282066565  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t4282066565  V_7;
	memset(&V_7, 0, sizeof(V_7));
	int32_t G_B4_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B12_0 = 0;
	Vector2_t4282066565 * G_B12_1 = NULL;
	int32_t G_B11_0 = 0;
	Vector2_t4282066565 * G_B11_1 = NULL;
	float G_B13_0 = 0.0f;
	int32_t G_B13_1 = 0;
	Vector2_t4282066565 * G_B13_2 = NULL;
	{
		int32_t L_0 = ___edge0;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___edge0;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0014;
		}
	}

IL_000e:
	{
		G_B4_0 = 1;
		goto IL_0015;
	}

IL_0014:
	{
		G_B4_0 = 0;
	}

IL_0015:
	{
		V_0 = G_B4_0;
		int32_t L_2 = ___edge0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_3 = ___edge0;
		G_B7_0 = ((((int32_t)L_3) == ((int32_t)1))? 1 : 0);
		goto IL_0024;
	}

IL_0023:
	{
		G_B7_0 = 1;
	}

IL_0024:
	{
		V_1 = (bool)G_B7_0;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		G_B10_0 = 1;
		goto IL_0032;
	}

IL_0031:
	{
		G_B10_0 = 0;
	}

IL_0032:
	{
		V_2 = (((float)((float)G_B10_0)));
		Vector2_t4282066565  L_5 = RectTransform_get_anchorMin_m688674174(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		int32_t L_6 = V_0;
		float L_7 = V_2;
		Vector2_set_Item_m2767519328((&V_3), L_6, L_7, /*hidden argument*/NULL);
		Vector2_t4282066565  L_8 = V_3;
		RectTransform_set_anchorMin_m989253483(__this, L_8, /*hidden argument*/NULL);
		Vector2_t4282066565  L_9 = RectTransform_get_anchorMax_m688445456(__this, /*hidden argument*/NULL);
		V_3 = L_9;
		int32_t L_10 = V_0;
		float L_11 = V_2;
		Vector2_set_Item_m2767519328((&V_3), L_10, L_11, /*hidden argument*/NULL);
		Vector2_t4282066565  L_12 = V_3;
		RectTransform_set_anchorMax_m715345817(__this, L_12, /*hidden argument*/NULL);
		Vector2_t4282066565  L_13 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		V_4 = L_13;
		int32_t L_14 = V_0;
		float L_15 = ___size2;
		Vector2_set_Item_m2767519328((&V_4), L_14, L_15, /*hidden argument*/NULL);
		Vector2_t4282066565  L_16 = V_4;
		RectTransform_set_sizeDelta_m1223846609(__this, L_16, /*hidden argument*/NULL);
		Vector2_t4282066565  L_17 = RectTransform_get_anchoredPosition_m2318455998(__this, /*hidden argument*/NULL);
		V_5 = L_17;
		int32_t L_18 = V_0;
		bool L_19 = V_1;
		G_B11_0 = L_18;
		G_B11_1 = (&V_5);
		if (!L_19)
		{
			G_B12_0 = L_18;
			G_B12_1 = (&V_5);
			goto IL_00ac;
		}
	}
	{
		float L_20 = ___inset1;
		float L_21 = ___size2;
		Vector2_t4282066565  L_22 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		V_6 = L_22;
		int32_t L_23 = V_0;
		float L_24 = Vector2_get_Item_m2185542843((&V_6), L_23, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)((-L_20))-(float)((float)((float)L_21*(float)((float)((float)(1.0f)-(float)L_24))))));
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_00c0;
	}

IL_00ac:
	{
		float L_25 = ___inset1;
		float L_26 = ___size2;
		Vector2_t4282066565  L_27 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		V_7 = L_27;
		int32_t L_28 = V_0;
		float L_29 = Vector2_get_Item_m2185542843((&V_7), L_28, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)L_25+(float)((float)((float)L_26*(float)L_29))));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_00c0:
	{
		Vector2_set_Item_m2767519328(G_B13_2, G_B13_1, G_B13_0, /*hidden argument*/NULL);
		Vector2_t4282066565  L_30 = V_5;
		RectTransform_set_anchoredPosition_m1498949997(__this, L_30, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetSizeWithCurrentAnchors(UnityEngine.RectTransform/Axis,System.Single)
extern "C"  void RectTransform_SetSizeWithCurrentAnchors_m4019722691 (RectTransform_t972643934 * __this, int32_t ___axis0, float ___size1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Vector2_t4282066565  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t4282066565  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t4282066565  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t4282066565  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		int32_t L_0 = ___axis0;
		V_0 = L_0;
		Vector2_t4282066565  L_1 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_0;
		float L_3 = ___size1;
		Vector2_t4282066565  L_4 = RectTransform_GetParentSize_m3092718635(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = V_0;
		float L_6 = Vector2_get_Item_m2185542843((&V_2), L_5, /*hidden argument*/NULL);
		Vector2_t4282066565  L_7 = RectTransform_get_anchorMax_m688445456(__this, /*hidden argument*/NULL);
		V_3 = L_7;
		int32_t L_8 = V_0;
		float L_9 = Vector2_get_Item_m2185542843((&V_3), L_8, /*hidden argument*/NULL);
		Vector2_t4282066565  L_10 = RectTransform_get_anchorMin_m688674174(__this, /*hidden argument*/NULL);
		V_4 = L_10;
		int32_t L_11 = V_0;
		float L_12 = Vector2_get_Item_m2185542843((&V_4), L_11, /*hidden argument*/NULL);
		Vector2_set_Item_m2767519328((&V_1), L_2, ((float)((float)L_3-(float)((float)((float)L_6*(float)((float)((float)L_9-(float)L_12)))))), /*hidden argument*/NULL);
		Vector2_t4282066565  L_13 = V_1;
		RectTransform_set_sizeDelta_m1223846609(__this, L_13, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
extern Il2CppClass* RectTransform_t972643934_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_GetParentSize_m3092718635_MetadataUsageId;
extern "C"  Vector2_t4282066565  RectTransform_GetParentSize_m3092718635 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetParentSize_m3092718635_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t972643934 * V_0 = NULL;
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t1659122786 * L_0 = Transform_get_parent_m2236876972(__this, /*hidden argument*/NULL);
		V_0 = ((RectTransform_t972643934 *)IsInstSealed(L_0, RectTransform_t972643934_il2cpp_TypeInfo_var));
		RectTransform_t972643934 * L_1 = V_0;
		bool L_2 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		Vector2_t4282066565  L_3 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_001d:
	{
		RectTransform_t972643934 * L_4 = V_0;
		NullCheck(L_4);
		Rect_t4241904616  L_5 = RectTransform_get_rect_m1566017036(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector2_t4282066565  L_6 = Rect_get_size_m136480416((&V_1), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::.ctor(System.Object,System.IntPtr)
extern "C"  void ReapplyDrivenProperties__ctor_m3710908308 (ReapplyDrivenProperties_t779639188 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C"  void ReapplyDrivenProperties_Invoke_m3880635155 (ReapplyDrivenProperties_t779639188 * __this, RectTransform_t972643934 * ___driven0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ReapplyDrivenProperties_Invoke_m3880635155((ReapplyDrivenProperties_t779639188 *)__this->get_prev_9(),___driven0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, RectTransform_t972643934 * ___driven0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, RectTransform_t972643934 * ___driven0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.RectTransform/ReapplyDrivenProperties::BeginInvoke(UnityEngine.RectTransform,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ReapplyDrivenProperties_BeginInvoke_m1851329218 (ReapplyDrivenProperties_t779639188 * __this, RectTransform_t972643934 * ___driven0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___driven0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::EndInvoke(System.IAsyncResult)
extern "C"  void ReapplyDrivenProperties_EndInvoke_m3686159268 (ReapplyDrivenProperties_t779639188 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.RectTransformUtility::.cctor()
extern Il2CppClass* Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility__cctor_m4293768260_MetadataUsageId;
extern "C"  void RectTransformUtility__cctor_m4293768260 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility__cctor_m4293768260_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((RectTransformUtility_t3025555048_StaticFields*)RectTransformUtility_t3025555048_il2cpp_TypeInfo_var->static_fields)->set_s_Corners_0(((Vector3U5BU5D_t215400611*)SZArrayNew(Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var, (uint32_t)4)));
		return;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_RectangleContainsScreenPoint_m2681210526_MetadataUsageId;
extern "C"  bool RectTransformUtility_RectangleContainsScreenPoint_m2681210526 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rect0, Vector2_t4282066565  ___screenPoint1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_RectangleContainsScreenPoint_m2681210526_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t972643934 * L_0 = ___rect0;
		Vector2_t4282066565  L_1 = ___screenPoint1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		bool L_2 = RectTransformUtility_RectangleContainsScreenPoint_m1460676684(NULL /*static, unused*/, L_0, L_1, (Camera_t2727095145 *)NULL, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_RectangleContainsScreenPoint_m1460676684_MetadataUsageId;
extern "C"  bool RectTransformUtility_RectangleContainsScreenPoint_m1460676684 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rect0, Vector2_t4282066565  ___screenPoint1, Camera_t2727095145 * ___cam2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_RectangleContainsScreenPoint_m1460676684_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t972643934 * L_0 = ___rect0;
		Camera_t2727095145 * L_1 = ___cam2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		bool L_2 = RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m1592514141(NULL /*static, unused*/, L_0, (&___screenPoint1), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
extern "C"  bool RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m1592514141 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rect0, Vector2_t4282066565 * ___screenPoint1, Camera_t2727095145 * ___cam2, const MethodInfo* method)
{
	typedef bool (*RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m1592514141_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *, Camera_t2727095145 *);
	static RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m1592514141_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m1592514141_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)");
	return _il2cpp_icall_func(___rect0, ___screenPoint1, ___cam2);
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_PixelAdjustPoint_m2518308759_MetadataUsageId;
extern "C"  Vector2_t4282066565  RectTransformUtility_PixelAdjustPoint_m2518308759 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, Transform_t1659122786 * ___elementTransform1, Canvas_t2727140764 * ___canvas2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustPoint_m2518308759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565  L_0 = ___point0;
		Transform_t1659122786 * L_1 = ___elementTransform1;
		Canvas_t2727140764 * L_2 = ___canvas2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		RectTransformUtility_PixelAdjustPoint_m1313063708(NULL /*static, unused*/, L_0, L_1, L_2, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_PixelAdjustPoint_m1313063708_MetadataUsageId;
extern "C"  void RectTransformUtility_PixelAdjustPoint_m1313063708 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, Transform_t1659122786 * ___elementTransform1, Canvas_t2727140764 * ___canvas2, Vector2_t4282066565 * ___output3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustPoint_m1313063708_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t1659122786 * L_0 = ___elementTransform1;
		Canvas_t2727140764 * L_1 = ___canvas2;
		Vector2_t4282066565 * L_2 = ___output3;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m1509640863(NULL /*static, unused*/, (&___point0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m1509640863 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___point0, Transform_t1659122786 * ___elementTransform1, Canvas_t2727140764 * ___canvas2, Vector2_t4282066565 * ___output3, const MethodInfo* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m1509640863_ftn) (Vector2_t4282066565 *, Transform_t1659122786 *, Canvas_t2727140764 *, Vector2_t4282066565 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m1509640863_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m1509640863_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___point0, ___elementTransform1, ___canvas2, ___output3);
}
// UnityEngine.Rect UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_PixelAdjustRect_m3727716130_MetadataUsageId;
extern "C"  Rect_t4241904616  RectTransformUtility_PixelAdjustRect_m3727716130 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rectTransform0, Canvas_t2727140764 * ___canvas1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustRect_m3727716130_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_t972643934 * L_0 = ___rectTransform0;
		Canvas_t2727140764 * L_1 = ___canvas1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1038323288(NULL /*static, unused*/, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Rect_t4241904616  L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1038323288 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rectTransform0, Canvas_t2727140764 * ___canvas1, Rect_t4241904616 * ___value2, const MethodInfo* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1038323288_ftn) (RectTransform_t972643934 *, Canvas_t2727140764 *, Rect_t4241904616 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1038323288_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1038323288_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)");
	_il2cpp_icall_func(___rectTransform0, ___canvas1, ___value2);
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToWorldPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector3&)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_ScreenPointToWorldPointInRectangle_m3856201718_MetadataUsageId;
extern "C"  bool RectTransformUtility_ScreenPointToWorldPointInRectangle_m3856201718 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rect0, Vector2_t4282066565  ___screenPoint1, Camera_t2727095145 * ___cam2, Vector3_t4282066566 * ___worldPoint3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToWorldPointInRectangle_m3856201718_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Ray_t3134616544  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Plane_t4206452690  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	{
		Vector3_t4282066566 * L_0 = ___worldPoint3;
		Vector2_t4282066565  L_1 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_2 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)L_0) = L_2;
		Camera_t2727095145 * L_3 = ___cam2;
		Vector2_t4282066565  L_4 = ___screenPoint1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		Ray_t3134616544  L_5 = RectTransformUtility_ScreenPointToRay_m1216104542(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		RectTransform_t972643934 * L_6 = ___rect0;
		NullCheck(L_6);
		Quaternion_t1553702882  L_7 = Transform_get_rotation_m11483428(L_6, /*hidden argument*/NULL);
		Vector3_t4282066566  L_8 = Vector3_get_back_m1326515313(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_9 = Quaternion_op_Multiply_m3771288979(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_10 = ___rect0;
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = Transform_get_position_m2211398607(L_10, /*hidden argument*/NULL);
		Plane__ctor_m2201046863((&V_1), L_9, L_11, /*hidden argument*/NULL);
		Ray_t3134616544  L_12 = V_0;
		bool L_13 = Plane_Raycast_m2829769106((&V_1), L_12, (&V_2), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0046;
		}
	}
	{
		return (bool)0;
	}

IL_0046:
	{
		Vector3_t4282066566 * L_14 = ___worldPoint3;
		float L_15 = V_2;
		Vector3_t4282066566  L_16 = Ray_GetPoint_m1171104822((&V_0), L_15, /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)L_14) = L_16;
		return (bool)1;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_ScreenPointToLocalPointInRectangle_m666650172_MetadataUsageId;
extern "C"  bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m666650172 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rect0, Vector2_t4282066565  ___screenPoint1, Camera_t2727095145 * ___cam2, Vector2_t4282066565 * ___localPoint3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToLocalPointInRectangle_m666650172_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565 * L_0 = ___localPoint3;
		Vector2_t4282066565  L_1 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)L_0) = L_1;
		RectTransform_t972643934 * L_2 = ___rect0;
		Vector2_t4282066565  L_3 = ___screenPoint1;
		Camera_t2727095145 * L_4 = ___cam2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		bool L_5 = RectTransformUtility_ScreenPointToWorldPointInRectangle_m3856201718(NULL /*static, unused*/, L_2, L_3, L_4, (&V_0), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		Vector2_t4282066565 * L_6 = ___localPoint3;
		RectTransform_t972643934 * L_7 = ___rect0;
		Vector3_t4282066566  L_8 = V_0;
		NullCheck(L_7);
		Vector3_t4282066566  L_9 = Transform_InverseTransformPoint_m1626812000(L_7, L_8, /*hidden argument*/NULL);
		Vector2_t4282066565  L_10 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)L_6) = L_10;
		return (bool)1;
	}

IL_002e:
	{
		return (bool)0;
	}
}
// UnityEngine.Ray UnityEngine.RectTransformUtility::ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector2)
extern "C"  Ray_t3134616544  RectTransformUtility_ScreenPointToRay_m1216104542 (Il2CppObject * __this /* static, unused */, Camera_t2727095145 * ___cam0, Vector2_t4282066565  ___screenPos1, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_t2727095145 * L_0 = ___cam0;
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Camera_t2727095145 * L_2 = ___cam0;
		Vector2_t4282066565  L_3 = ___screenPos1;
		Vector3_t4282066566  L_4 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Ray_t3134616544  L_5 = Camera_ScreenPointToRay_m1733083890(L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0019:
	{
		Vector2_t4282066565  L_6 = ___screenPos1;
		Vector3_t4282066566  L_7 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Vector3_t4282066566 * L_8 = (&V_0);
		float L_9 = L_8->get_z_3();
		L_8->set_z_3(((float)((float)L_9-(float)(100.0f))));
		Vector3_t4282066566  L_10 = V_0;
		Vector3_t4282066566  L_11 = Vector3_get_forward_m1039372701(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray_t3134616544  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Ray__ctor_m2662468509(&L_12, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3)
extern "C"  Vector2_t4282066565  RectTransformUtility_WorldToScreenPoint_m2409513860 (Il2CppObject * __this /* static, unused */, Camera_t2727095145 * ___cam0, Vector3_t4282066566  ___worldPoint1, const MethodInfo* method)
{
	{
		Camera_t2727095145 * L_0 = ___cam0;
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		float L_2 = (&___worldPoint1)->get_x_1();
		float L_3 = (&___worldPoint1)->get_y_2();
		Vector2_t4282066565  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m1517109030(&L_4, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0020:
	{
		Camera_t2727095145 * L_5 = ___cam0;
		Vector3_t4282066566  L_6 = ___worldPoint1;
		NullCheck(L_5);
		Vector3_t4282066566  L_7 = Camera_WorldToScreenPoint_m2400233676(L_5, L_6, /*hidden argument*/NULL);
		Vector2_t4282066565  L_8 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Bounds UnityEngine.RectTransformUtility::CalculateRelativeRectTransformBounds(UnityEngine.Transform,UnityEngine.Transform)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRectTransform_t972643934_m3447711447_MethodInfo_var;
extern const uint32_t RectTransformUtility_CalculateRelativeRectTransformBounds_m785483371_MetadataUsageId;
extern "C"  Bounds_t2711641849  RectTransformUtility_CalculateRelativeRectTransformBounds_m785483371 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___root0, Transform_t1659122786 * ___child1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_CalculateRelativeRectTransformBounds_m785483371_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RectTransformU5BU5D_t3587651179* V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Matrix4x4_t1651859333  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Bounds_t2711641849  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		Transform_t1659122786 * L_0 = ___child1;
		NullCheck(L_0);
		RectTransformU5BU5D_t3587651179* L_1 = Component_GetComponentsInChildren_TisRectTransform_t972643934_m3447711447(L_0, (bool)0, /*hidden argument*/Component_GetComponentsInChildren_TisRectTransform_t972643934_m3447711447_MethodInfo_var);
		V_0 = L_1;
		RectTransformU5BU5D_t3587651179* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_00c8;
		}
	}
	{
		Vector3__ctor_m2926210380((&V_1), (std::numeric_limits<float>::max()), (std::numeric_limits<float>::max()), (std::numeric_limits<float>::max()), /*hidden argument*/NULL);
		Vector3__ctor_m2926210380((&V_2), (-std::numeric_limits<float>::max()), (-std::numeric_limits<float>::max()), (-std::numeric_limits<float>::max()), /*hidden argument*/NULL);
		Transform_t1659122786 * L_3 = ___root0;
		NullCheck(L_3);
		Matrix4x4_t1651859333  L_4 = Transform_get_worldToLocalMatrix_m3792395652(L_3, /*hidden argument*/NULL);
		V_3 = L_4;
		V_4 = 0;
		RectTransformU5BU5D_t3587651179* L_5 = V_0;
		NullCheck(L_5);
		V_5 = (((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))));
		goto IL_00a7;
	}

IL_0051:
	{
		RectTransformU5BU5D_t3587651179* L_6 = V_0;
		int32_t L_7 = V_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		RectTransform_t972643934 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		Vector3U5BU5D_t215400611* L_10 = ((RectTransformUtility_t3025555048_StaticFields*)RectTransformUtility_t3025555048_il2cpp_TypeInfo_var->static_fields)->get_s_Corners_0();
		NullCheck(L_9);
		RectTransform_GetWorldCorners_m1829917190(L_9, L_10, /*hidden argument*/NULL);
		V_6 = 0;
		goto IL_0099;
	}

IL_0067:
	{
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		Vector3U5BU5D_t215400611* L_11 = ((RectTransformUtility_t3025555048_StaticFields*)RectTransformUtility_t3025555048_il2cpp_TypeInfo_var->static_fields)->get_s_Corners_0();
		int32_t L_12 = V_6;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		Vector3_t4282066566  L_13 = Matrix4x4_MultiplyPoint3x4_m2198174902((&V_3), (*(Vector3_t4282066566 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))), /*hidden argument*/NULL);
		V_7 = L_13;
		Vector3_t4282066566  L_14 = V_7;
		Vector3_t4282066566  L_15 = V_1;
		Vector3_t4282066566  L_16 = Vector3_Min_m10392601(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		Vector3_t4282066566  L_17 = V_7;
		Vector3_t4282066566  L_18 = V_2;
		Vector3_t4282066566  L_19 = Vector3_Max_m545730887(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		V_2 = L_19;
		int32_t L_20 = V_6;
		V_6 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0099:
	{
		int32_t L_21 = V_6;
		if ((((int32_t)L_21) < ((int32_t)4)))
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_22 = V_4;
		V_4 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_00a7:
	{
		int32_t L_23 = V_4;
		int32_t L_24 = V_5;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0051;
		}
	}
	{
		Vector3_t4282066566  L_25 = V_1;
		Vector3_t4282066566  L_26 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		Bounds__ctor_m4160293652((&V_8), L_25, L_26, /*hidden argument*/NULL);
		Vector3_t4282066566  L_27 = V_2;
		Bounds_Encapsulate_m3624685234((&V_8), L_27, /*hidden argument*/NULL);
		Bounds_t2711641849  L_28 = V_8;
		return L_28;
	}

IL_00c8:
	{
		Vector3_t4282066566  L_29 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_30 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		Bounds_t2711641849  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Bounds__ctor_m4160293652(&L_31, L_29, L_30, /*hidden argument*/NULL);
		return L_31;
	}
}
// UnityEngine.Bounds UnityEngine.RectTransformUtility::CalculateRelativeRectTransformBounds(UnityEngine.Transform)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_CalculateRelativeRectTransformBounds_m1194850036_MetadataUsageId;
extern "C"  Bounds_t2711641849  RectTransformUtility_CalculateRelativeRectTransformBounds_m1194850036 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___trans0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_CalculateRelativeRectTransformBounds_m1194850036_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t1659122786 * L_0 = ___trans0;
		Transform_t1659122786 * L_1 = ___trans0;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		Bounds_t2711641849  L_2 = RectTransformUtility_CalculateRelativeRectTransformBounds_m785483371(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutOnAxis(UnityEngine.RectTransform,System.Int32,System.Boolean,System.Boolean)
extern Il2CppClass* RectTransform_t972643934_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_FlipLayoutOnAxis_m3487429352_MetadataUsageId;
extern "C"  void RectTransformUtility_FlipLayoutOnAxis_m3487429352 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rect0, int32_t ___axis1, bool ___keepPositioning2, bool ___recursive3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_FlipLayoutOnAxis_m3487429352_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t972643934 * V_1 = NULL;
	Vector2_t4282066565  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t4282066565  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t4282066565  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t4282066565  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	{
		RectTransform_t972643934 * L_0 = ___rect0;
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___recursive3;
		if (!L_2)
		{
			goto IL_004c;
		}
	}
	{
		V_0 = 0;
		goto IL_0040;
	}

IL_001a:
	{
		RectTransform_t972643934 * L_3 = ___rect0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t1659122786 * L_5 = Transform_GetChild_m4040462992(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t972643934 *)IsInstSealed(L_5, RectTransform_t972643934_il2cpp_TypeInfo_var));
		RectTransform_t972643934 * L_6 = V_1;
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003c;
		}
	}
	{
		RectTransform_t972643934 * L_8 = V_1;
		int32_t L_9 = ___axis1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutOnAxis_m3487429352(NULL /*static, unused*/, L_8, L_9, (bool)0, (bool)1, /*hidden argument*/NULL);
	}

IL_003c:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_11 = V_0;
		RectTransform_t972643934 * L_12 = ___rect0;
		NullCheck(L_12);
		int32_t L_13 = Transform_get_childCount_m2107810675(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_001a;
		}
	}

IL_004c:
	{
		RectTransform_t972643934 * L_14 = ___rect0;
		NullCheck(L_14);
		Vector2_t4282066565  L_15 = RectTransform_get_pivot_m3785570595(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		int32_t L_16 = ___axis1;
		int32_t L_17 = ___axis1;
		float L_18 = Vector2_get_Item_m2185542843((&V_2), L_17, /*hidden argument*/NULL);
		Vector2_set_Item_m2767519328((&V_2), L_16, ((float)((float)(1.0f)-(float)L_18)), /*hidden argument*/NULL);
		RectTransform_t972643934 * L_19 = ___rect0;
		Vector2_t4282066565  L_20 = V_2;
		NullCheck(L_19);
		RectTransform_set_pivot_m457344806(L_19, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning2;
		if (!L_21)
		{
			goto IL_0077;
		}
	}
	{
		return;
	}

IL_0077:
	{
		RectTransform_t972643934 * L_22 = ___rect0;
		NullCheck(L_22);
		Vector2_t4282066565  L_23 = RectTransform_get_anchoredPosition_m2318455998(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		int32_t L_24 = ___axis1;
		int32_t L_25 = ___axis1;
		float L_26 = Vector2_get_Item_m2185542843((&V_3), L_25, /*hidden argument*/NULL);
		Vector2_set_Item_m2767519328((&V_3), L_24, ((-L_26)), /*hidden argument*/NULL);
		RectTransform_t972643934 * L_27 = ___rect0;
		Vector2_t4282066565  L_28 = V_3;
		NullCheck(L_27);
		RectTransform_set_anchoredPosition_m1498949997(L_27, L_28, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_29 = ___rect0;
		NullCheck(L_29);
		Vector2_t4282066565  L_30 = RectTransform_get_anchorMin_m688674174(L_29, /*hidden argument*/NULL);
		V_4 = L_30;
		RectTransform_t972643934 * L_31 = ___rect0;
		NullCheck(L_31);
		Vector2_t4282066565  L_32 = RectTransform_get_anchorMax_m688445456(L_31, /*hidden argument*/NULL);
		V_5 = L_32;
		int32_t L_33 = ___axis1;
		float L_34 = Vector2_get_Item_m2185542843((&V_4), L_33, /*hidden argument*/NULL);
		V_6 = L_34;
		int32_t L_35 = ___axis1;
		int32_t L_36 = ___axis1;
		float L_37 = Vector2_get_Item_m2185542843((&V_5), L_36, /*hidden argument*/NULL);
		Vector2_set_Item_m2767519328((&V_4), L_35, ((float)((float)(1.0f)-(float)L_37)), /*hidden argument*/NULL);
		int32_t L_38 = ___axis1;
		float L_39 = V_6;
		Vector2_set_Item_m2767519328((&V_5), L_38, ((float)((float)(1.0f)-(float)L_39)), /*hidden argument*/NULL);
		RectTransform_t972643934 * L_40 = ___rect0;
		Vector2_t4282066565  L_41 = V_4;
		NullCheck(L_40);
		RectTransform_set_anchorMin_m989253483(L_40, L_41, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_42 = ___rect0;
		Vector2_t4282066565  L_43 = V_5;
		NullCheck(L_42);
		RectTransform_set_anchorMax_m715345817(L_42, L_43, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutAxes(UnityEngine.RectTransform,System.Boolean,System.Boolean)
extern Il2CppClass* RectTransform_t972643934_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_FlipLayoutAxes_m2163490602_MetadataUsageId;
extern "C"  void RectTransformUtility_FlipLayoutAxes_m2163490602 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rect0, bool ___keepPositioning1, bool ___recursive2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_FlipLayoutAxes_m2163490602_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t972643934 * V_1 = NULL;
	{
		RectTransform_t972643934 * L_0 = ___rect0;
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___recursive2;
		if (!L_2)
		{
			goto IL_004b;
		}
	}
	{
		V_0 = 0;
		goto IL_003f;
	}

IL_001a:
	{
		RectTransform_t972643934 * L_3 = ___rect0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t1659122786 * L_5 = Transform_GetChild_m4040462992(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t972643934 *)IsInstSealed(L_5, RectTransform_t972643934_il2cpp_TypeInfo_var));
		RectTransform_t972643934 * L_6 = V_1;
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003b;
		}
	}
	{
		RectTransform_t972643934 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutAxes_m2163490602(NULL /*static, unused*/, L_8, (bool)0, (bool)1, /*hidden argument*/NULL);
	}

IL_003b:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_10 = V_0;
		RectTransform_t972643934 * L_11 = ___rect0;
		NullCheck(L_11);
		int32_t L_12 = Transform_get_childCount_m2107810675(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_001a;
		}
	}

IL_004b:
	{
		RectTransform_t972643934 * L_13 = ___rect0;
		RectTransform_t972643934 * L_14 = ___rect0;
		NullCheck(L_14);
		Vector2_t4282066565  L_15 = RectTransform_get_pivot_m3785570595(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		Vector2_t4282066565  L_16 = RectTransformUtility_GetTransposed_m2060823533(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		RectTransform_set_pivot_m457344806(L_13, L_16, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_17 = ___rect0;
		RectTransform_t972643934 * L_18 = ___rect0;
		NullCheck(L_18);
		Vector2_t4282066565  L_19 = RectTransform_get_sizeDelta_m4279424984(L_18, /*hidden argument*/NULL);
		Vector2_t4282066565  L_20 = RectTransformUtility_GetTransposed_m2060823533(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		RectTransform_set_sizeDelta_m1223846609(L_17, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning1;
		if (!L_21)
		{
			goto IL_0074;
		}
	}
	{
		return;
	}

IL_0074:
	{
		RectTransform_t972643934 * L_22 = ___rect0;
		RectTransform_t972643934 * L_23 = ___rect0;
		NullCheck(L_23);
		Vector2_t4282066565  L_24 = RectTransform_get_anchoredPosition_m2318455998(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		Vector2_t4282066565  L_25 = RectTransformUtility_GetTransposed_m2060823533(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		RectTransform_set_anchoredPosition_m1498949997(L_22, L_25, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_26 = ___rect0;
		RectTransform_t972643934 * L_27 = ___rect0;
		NullCheck(L_27);
		Vector2_t4282066565  L_28 = RectTransform_get_anchorMin_m688674174(L_27, /*hidden argument*/NULL);
		Vector2_t4282066565  L_29 = RectTransformUtility_GetTransposed_m2060823533(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		RectTransform_set_anchorMin_m989253483(L_26, L_29, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_30 = ___rect0;
		RectTransform_t972643934 * L_31 = ___rect0;
		NullCheck(L_31);
		Vector2_t4282066565  L_32 = RectTransform_get_anchorMax_m688445456(L_31, /*hidden argument*/NULL);
		Vector2_t4282066565  L_33 = RectTransformUtility_GetTransposed_m2060823533(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		RectTransform_set_anchorMax_m715345817(L_30, L_33, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::GetTransposed(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  RectTransformUtility_GetTransposed_m2060823533 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___input0, const MethodInfo* method)
{
	{
		float L_0 = (&___input0)->get_y_2();
		float L_1 = (&___input0)->get_x_1();
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.IntPtr UnityEngine.RenderBuffer::GetNativeRenderBufferPtr()
extern "C"  IntPtr_t RenderBuffer_GetNativeRenderBufferPtr_m2564329965 (RenderBuffer_t3529837690 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_m_BufferPtr_1();
		return L_0;
	}
}
extern "C"  IntPtr_t RenderBuffer_GetNativeRenderBufferPtr_m2564329965_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RenderBuffer_t3529837690 * _thisAdjusted = reinterpret_cast<RenderBuffer_t3529837690 *>(__this + 1);
	return RenderBuffer_GetNativeRenderBufferPtr_m2564329965(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RenderBuffer
extern "C" void RenderBuffer_t3529837690_marshal_pinvoke(const RenderBuffer_t3529837690& unmarshaled, RenderBuffer_t3529837690_marshaled_pinvoke& marshaled)
{
	marshaled.___m_RenderTextureInstanceID_0 = unmarshaled.get_m_RenderTextureInstanceID_0();
	marshaled.___m_BufferPtr_1 = reinterpret_cast<intptr_t>((unmarshaled.get_m_BufferPtr_1()).get_m_value_0());
}
extern "C" void RenderBuffer_t3529837690_marshal_pinvoke_back(const RenderBuffer_t3529837690_marshaled_pinvoke& marshaled, RenderBuffer_t3529837690& unmarshaled)
{
	int32_t unmarshaled_m_RenderTextureInstanceID_temp_0 = 0;
	unmarshaled_m_RenderTextureInstanceID_temp_0 = marshaled.___m_RenderTextureInstanceID_0;
	unmarshaled.set_m_RenderTextureInstanceID_0(unmarshaled_m_RenderTextureInstanceID_temp_0);
	IntPtr_t unmarshaled_m_BufferPtr_temp_1;
	memset(&unmarshaled_m_BufferPtr_temp_1, 0, sizeof(unmarshaled_m_BufferPtr_temp_1));
	IntPtr_t unmarshaled_m_BufferPtr_temp_1_temp;
	unmarshaled_m_BufferPtr_temp_1_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_BufferPtr_1));
	unmarshaled_m_BufferPtr_temp_1 = unmarshaled_m_BufferPtr_temp_1_temp;
	unmarshaled.set_m_BufferPtr_1(unmarshaled_m_BufferPtr_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.RenderBuffer
extern "C" void RenderBuffer_t3529837690_marshal_pinvoke_cleanup(RenderBuffer_t3529837690_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RenderBuffer
extern "C" void RenderBuffer_t3529837690_marshal_com(const RenderBuffer_t3529837690& unmarshaled, RenderBuffer_t3529837690_marshaled_com& marshaled)
{
	marshaled.___m_RenderTextureInstanceID_0 = unmarshaled.get_m_RenderTextureInstanceID_0();
	marshaled.___m_BufferPtr_1 = reinterpret_cast<intptr_t>((unmarshaled.get_m_BufferPtr_1()).get_m_value_0());
}
extern "C" void RenderBuffer_t3529837690_marshal_com_back(const RenderBuffer_t3529837690_marshaled_com& marshaled, RenderBuffer_t3529837690& unmarshaled)
{
	int32_t unmarshaled_m_RenderTextureInstanceID_temp_0 = 0;
	unmarshaled_m_RenderTextureInstanceID_temp_0 = marshaled.___m_RenderTextureInstanceID_0;
	unmarshaled.set_m_RenderTextureInstanceID_0(unmarshaled_m_RenderTextureInstanceID_temp_0);
	IntPtr_t unmarshaled_m_BufferPtr_temp_1;
	memset(&unmarshaled_m_BufferPtr_temp_1, 0, sizeof(unmarshaled_m_BufferPtr_temp_1));
	IntPtr_t unmarshaled_m_BufferPtr_temp_1_temp;
	unmarshaled_m_BufferPtr_temp_1_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_BufferPtr_1));
	unmarshaled_m_BufferPtr_temp_1 = unmarshaled_m_BufferPtr_temp_1_temp;
	unmarshaled.set_m_BufferPtr_1(unmarshaled_m_BufferPtr_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.RenderBuffer
extern "C" void RenderBuffer_t3529837690_marshal_com_cleanup(RenderBuffer_t3529837690_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Renderer::.ctor()
extern "C"  void Renderer__ctor_m3477754890 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	{
		Component__ctor_m4238603388(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Renderer::set_staticBatchRootTransform(UnityEngine.Transform)
extern "C"  void Renderer_set_staticBatchRootTransform_m1058244712 (Renderer_t3076687687 * __this, Transform_t1659122786 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_staticBatchRootTransform_m1058244712_ftn) (Renderer_t3076687687 *, Transform_t1659122786 *);
	static Renderer_set_staticBatchRootTransform_m1058244712_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_staticBatchRootTransform_m1058244712_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_staticBatchRootTransform(UnityEngine.Transform)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Renderer::get_staticBatchIndex()
extern "C"  int32_t Renderer_get_staticBatchIndex_m767652649 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_staticBatchIndex_m767652649_ftn) (Renderer_t3076687687 *);
	static Renderer_get_staticBatchIndex_m767652649_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_staticBatchIndex_m767652649_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_staticBatchIndex()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::SetSubsetIndex(System.Int32,System.Int32)
extern "C"  void Renderer_SetSubsetIndex_m3697684702 (Renderer_t3076687687 * __this, int32_t ___index0, int32_t ___subSetIndexForMaterial1, const MethodInfo* method)
{
	typedef void (*Renderer_SetSubsetIndex_m3697684702_ftn) (Renderer_t3076687687 *, int32_t, int32_t);
	static Renderer_SetSubsetIndex_m3697684702_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_SetSubsetIndex_m3697684702_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::SetSubsetIndex(System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___index0, ___subSetIndexForMaterial1);
}
// System.Boolean UnityEngine.Renderer::get_isPartOfStaticBatch()
extern "C"  bool Renderer_get_isPartOfStaticBatch_m795521425 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef bool (*Renderer_get_isPartOfStaticBatch_m795521425_ftn) (Renderer_t3076687687 *);
	static Renderer_get_isPartOfStaticBatch_m795521425_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_isPartOfStaticBatch_m795521425_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_isPartOfStaticBatch()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Matrix4x4 UnityEngine.Renderer::get_worldToLocalMatrix()
extern "C"  Matrix4x4_t1651859333  Renderer_get_worldToLocalMatrix_m396175837 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Renderer_INTERNAL_get_worldToLocalMatrix_m515997572(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t1651859333  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Renderer::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Renderer_INTERNAL_get_worldToLocalMatrix_m515997572 (Renderer_t3076687687 * __this, Matrix4x4_t1651859333 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_INTERNAL_get_worldToLocalMatrix_m515997572_ftn) (Renderer_t3076687687 *, Matrix4x4_t1651859333 *);
	static Renderer_INTERNAL_get_worldToLocalMatrix_m515997572_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_INTERNAL_get_worldToLocalMatrix_m515997572_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Matrix4x4 UnityEngine.Renderer::get_localToWorldMatrix()
extern "C"  Matrix4x4_t1651859333  Renderer_get_localToWorldMatrix_m174800395 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Renderer_INTERNAL_get_localToWorldMatrix_m401743154(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t1651859333  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Renderer::INTERNAL_get_localToWorldMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Renderer_INTERNAL_get_localToWorldMatrix_m401743154 (Renderer_t3076687687 * __this, Matrix4x4_t1651859333 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_INTERNAL_get_localToWorldMatrix_m401743154_ftn) (Renderer_t3076687687 *, Matrix4x4_t1651859333 *);
	static Renderer_INTERNAL_get_localToWorldMatrix_m401743154_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_INTERNAL_get_localToWorldMatrix_m401743154_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::INTERNAL_get_localToWorldMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Renderer::get_enabled()
extern "C"  bool Renderer_get_enabled_m1971819706 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef bool (*Renderer_get_enabled_m1971819706_ftn) (Renderer_t3076687687 *);
	static Renderer_get_enabled_m1971819706_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_enabled_m1971819706_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_enabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C"  void Renderer_set_enabled_m2514140131 (Renderer_t3076687687 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_enabled_m2514140131_ftn) (Renderer_t3076687687 *, bool);
	static Renderer_set_enabled_m2514140131_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_enabled_m2514140131_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Rendering.ShadowCastingMode UnityEngine.Renderer::get_shadowCastingMode()
extern "C"  int32_t Renderer_get_shadowCastingMode_m2787374815 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_shadowCastingMode_m2787374815_ftn) (Renderer_t3076687687 *);
	static Renderer_get_shadowCastingMode_m2787374815_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_shadowCastingMode_m2787374815_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_shadowCastingMode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_shadowCastingMode(UnityEngine.Rendering.ShadowCastingMode)
extern "C"  void Renderer_set_shadowCastingMode_m3937248916 (Renderer_t3076687687 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_shadowCastingMode_m3937248916_ftn) (Renderer_t3076687687 *, int32_t);
	static Renderer_set_shadowCastingMode_m3937248916_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_shadowCastingMode_m3937248916_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_shadowCastingMode(UnityEngine.Rendering.ShadowCastingMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Renderer::get_receiveShadows()
extern "C"  bool Renderer_get_receiveShadows_m194827321 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef bool (*Renderer_get_receiveShadows_m194827321_ftn) (Renderer_t3076687687 *);
	static Renderer_get_receiveShadows_m194827321_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_receiveShadows_m194827321_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_receiveShadows()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_receiveShadows(System.Boolean)
extern "C"  void Renderer_set_receiveShadows_m2634864254 (Renderer_t3076687687 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_receiveShadows_m2634864254_ftn) (Renderer_t3076687687 *, bool);
	static Renderer_set_receiveShadows_m2634864254_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_receiveShadows_m2634864254_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_receiveShadows(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t3870600107 * Renderer_get_material_m2720864603 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef Material_t3870600107 * (*Renderer_get_material_m2720864603_ftn) (Renderer_t3076687687 *);
	static Renderer_get_material_m2720864603_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_material_m2720864603_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
extern "C"  void Renderer_set_material_m1012580896 (Renderer_t3076687687 * __this, Material_t3870600107 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_material_m1012580896_ftn) (Renderer_t3076687687 *, Material_t3870600107 *);
	static Renderer_set_material_m1012580896_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_material_m1012580896_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_material(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material UnityEngine.Renderer::get_sharedMaterial()
extern "C"  Material_t3870600107 * Renderer_get_sharedMaterial_m835478880 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef Material_t3870600107 * (*Renderer_get_sharedMaterial_m835478880_ftn) (Renderer_t3076687687 *);
	static Renderer_get_sharedMaterial_m835478880_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sharedMaterial_m835478880_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sharedMaterial()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)
extern "C"  void Renderer_set_sharedMaterial_m1064371045 (Renderer_t3076687687 * __this, Material_t3870600107 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_sharedMaterial_m1064371045_ftn) (Renderer_t3076687687 *, Material_t3870600107 *);
	static Renderer_set_sharedMaterial_m1064371045_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_sharedMaterial_m1064371045_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material[] UnityEngine.Renderer::get_materials()
extern "C"  MaterialU5BU5D_t170856778* Renderer_get_materials_m3755041148 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef MaterialU5BU5D_t170856778* (*Renderer_get_materials_m3755041148_ftn) (Renderer_t3076687687 *);
	static Renderer_get_materials_m3755041148_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_materials_m3755041148_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_materials()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_materials(UnityEngine.Material[])
extern "C"  void Renderer_set_materials_m268031319 (Renderer_t3076687687 * __this, MaterialU5BU5D_t170856778* ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_materials_m268031319_ftn) (Renderer_t3076687687 *, MaterialU5BU5D_t170856778*);
	static Renderer_set_materials_m268031319_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_materials_m268031319_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_materials(UnityEngine.Material[])");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material[] UnityEngine.Renderer::get_sharedMaterials()
extern "C"  MaterialU5BU5D_t170856778* Renderer_get_sharedMaterials_m1981818007 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef MaterialU5BU5D_t170856778* (*Renderer_get_sharedMaterials_m1981818007_ftn) (Renderer_t3076687687 *);
	static Renderer_get_sharedMaterials_m1981818007_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sharedMaterials_m1981818007_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sharedMaterials()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])
extern "C"  void Renderer_set_sharedMaterials_m1255100914 (Renderer_t3076687687 * __this, MaterialU5BU5D_t170856778* ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_sharedMaterials_m1255100914_ftn) (Renderer_t3076687687 *, MaterialU5BU5D_t170856778*);
	static Renderer_set_sharedMaterials_m1255100914_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_sharedMaterials_m1255100914_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Bounds UnityEngine.Renderer::get_bounds()
extern "C"  Bounds_t2711641849  Renderer_get_bounds_m1533373851 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	Bounds_t2711641849  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Renderer_INTERNAL_get_bounds_m3488131760(__this, (&V_0), /*hidden argument*/NULL);
		Bounds_t2711641849  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Renderer::INTERNAL_get_bounds(UnityEngine.Bounds&)
extern "C"  void Renderer_INTERNAL_get_bounds_m3488131760 (Renderer_t3076687687 * __this, Bounds_t2711641849 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_INTERNAL_get_bounds_m3488131760_ftn) (Renderer_t3076687687 *, Bounds_t2711641849 *);
	static Renderer_INTERNAL_get_bounds_m3488131760_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_INTERNAL_get_bounds_m3488131760_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::INTERNAL_get_bounds(UnityEngine.Bounds&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Renderer::get_lightmapIndex()
extern "C"  int32_t Renderer_get_lightmapIndex_m3207153739 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_lightmapIndex_m3207153739_ftn) (Renderer_t3076687687 *);
	static Renderer_get_lightmapIndex_m3207153739_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_lightmapIndex_m3207153739_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_lightmapIndex()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_lightmapIndex(System.Int32)
extern "C"  void Renderer_set_lightmapIndex_m2188945576 (Renderer_t3076687687 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_lightmapIndex_m2188945576_ftn) (Renderer_t3076687687 *, int32_t);
	static Renderer_set_lightmapIndex_m2188945576_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_lightmapIndex_m2188945576_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_lightmapIndex(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Renderer::get_realtimeLightmapIndex()
extern "C"  int32_t Renderer_get_realtimeLightmapIndex_m4084805984 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_realtimeLightmapIndex_m4084805984_ftn) (Renderer_t3076687687 *);
	static Renderer_get_realtimeLightmapIndex_m4084805984_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_realtimeLightmapIndex_m4084805984_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_realtimeLightmapIndex()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_realtimeLightmapIndex(System.Int32)
extern "C"  void Renderer_set_realtimeLightmapIndex_m367808061 (Renderer_t3076687687 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_realtimeLightmapIndex_m367808061_ftn) (Renderer_t3076687687 *, int32_t);
	static Renderer_set_realtimeLightmapIndex_m367808061_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_realtimeLightmapIndex_m367808061_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_realtimeLightmapIndex(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector4 UnityEngine.Renderer::get_lightmapScaleOffset()
extern "C"  Vector4_t4282066567  Renderer_get_lightmapScaleOffset_m2291266045 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	Vector4_t4282066567  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Renderer_INTERNAL_get_lightmapScaleOffset_m214316378(__this, (&V_0), /*hidden argument*/NULL);
		Vector4_t4282066567  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Renderer::set_lightmapScaleOffset(UnityEngine.Vector4)
extern "C"  void Renderer_set_lightmapScaleOffset_m3708167926 (Renderer_t3076687687 * __this, Vector4_t4282066567  ___value0, const MethodInfo* method)
{
	{
		Renderer_INTERNAL_set_lightmapScaleOffset_m1280267110(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Renderer::INTERNAL_get_lightmapScaleOffset(UnityEngine.Vector4&)
extern "C"  void Renderer_INTERNAL_get_lightmapScaleOffset_m214316378 (Renderer_t3076687687 * __this, Vector4_t4282066567 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_INTERNAL_get_lightmapScaleOffset_m214316378_ftn) (Renderer_t3076687687 *, Vector4_t4282066567 *);
	static Renderer_INTERNAL_get_lightmapScaleOffset_m214316378_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_INTERNAL_get_lightmapScaleOffset_m214316378_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::INTERNAL_get_lightmapScaleOffset(UnityEngine.Vector4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Renderer::INTERNAL_set_lightmapScaleOffset(UnityEngine.Vector4&)
extern "C"  void Renderer_INTERNAL_set_lightmapScaleOffset_m1280267110 (Renderer_t3076687687 * __this, Vector4_t4282066567 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_INTERNAL_set_lightmapScaleOffset_m1280267110_ftn) (Renderer_t3076687687 *, Vector4_t4282066567 *);
	static Renderer_INTERNAL_set_lightmapScaleOffset_m1280267110_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_INTERNAL_set_lightmapScaleOffset_m1280267110_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::INTERNAL_set_lightmapScaleOffset(UnityEngine.Vector4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector4 UnityEngine.Renderer::get_realtimeLightmapScaleOffset()
extern "C"  Vector4_t4282066567  Renderer_get_realtimeLightmapScaleOffset_m2679390290 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	Vector4_t4282066567  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Renderer_INTERNAL_get_realtimeLightmapScaleOffset_m3401707055(__this, (&V_0), /*hidden argument*/NULL);
		Vector4_t4282066567  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Renderer::set_realtimeLightmapScaleOffset(UnityEngine.Vector4)
extern "C"  void Renderer_set_realtimeLightmapScaleOffset_m628947841 (Renderer_t3076687687 * __this, Vector4_t4282066567  ___value0, const MethodInfo* method)
{
	{
		Renderer_INTERNAL_set_realtimeLightmapScaleOffset_m1770143803(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Renderer::INTERNAL_get_realtimeLightmapScaleOffset(UnityEngine.Vector4&)
extern "C"  void Renderer_INTERNAL_get_realtimeLightmapScaleOffset_m3401707055 (Renderer_t3076687687 * __this, Vector4_t4282066567 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_INTERNAL_get_realtimeLightmapScaleOffset_m3401707055_ftn) (Renderer_t3076687687 *, Vector4_t4282066567 *);
	static Renderer_INTERNAL_get_realtimeLightmapScaleOffset_m3401707055_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_INTERNAL_get_realtimeLightmapScaleOffset_m3401707055_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::INTERNAL_get_realtimeLightmapScaleOffset(UnityEngine.Vector4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Renderer::INTERNAL_set_realtimeLightmapScaleOffset(UnityEngine.Vector4&)
extern "C"  void Renderer_INTERNAL_set_realtimeLightmapScaleOffset_m1770143803 (Renderer_t3076687687 * __this, Vector4_t4282066567 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_INTERNAL_set_realtimeLightmapScaleOffset_m1770143803_ftn) (Renderer_t3076687687 *, Vector4_t4282066567 *);
	static Renderer_INTERNAL_set_realtimeLightmapScaleOffset_m1770143803_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_INTERNAL_set_realtimeLightmapScaleOffset_m1770143803_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::INTERNAL_set_realtimeLightmapScaleOffset(UnityEngine.Vector4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Renderer::get_isVisible()
extern "C"  bool Renderer_get_isVisible_m1011967393 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef bool (*Renderer_get_isVisible_m1011967393_ftn) (Renderer_t3076687687 *);
	static Renderer_get_isVisible_m1011967393_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_isVisible_m1011967393_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_isVisible()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Renderer::get_useLightProbes()
extern "C"  bool Renderer_get_useLightProbes_m733872187 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef bool (*Renderer_get_useLightProbes_m733872187_ftn) (Renderer_t3076687687 *);
	static Renderer_get_useLightProbes_m733872187_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_useLightProbes_m733872187_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_useLightProbes()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_useLightProbes(System.Boolean)
extern "C"  void Renderer_set_useLightProbes_m3647461120 (Renderer_t3076687687 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_useLightProbes_m3647461120_ftn) (Renderer_t3076687687 *, bool);
	static Renderer_set_useLightProbes_m3647461120_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_useLightProbes_m3647461120_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_useLightProbes(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Transform UnityEngine.Renderer::get_probeAnchor()
extern "C"  Transform_t1659122786 * Renderer_get_probeAnchor_m665261894 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef Transform_t1659122786 * (*Renderer_get_probeAnchor_m665261894_ftn) (Renderer_t3076687687 *);
	static Renderer_get_probeAnchor_m665261894_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_probeAnchor_m665261894_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_probeAnchor()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_probeAnchor(UnityEngine.Transform)
extern "C"  void Renderer_set_probeAnchor_m1253820557 (Renderer_t3076687687 * __this, Transform_t1659122786 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_probeAnchor_m1253820557_ftn) (Renderer_t3076687687 *, Transform_t1659122786 *);
	static Renderer_set_probeAnchor_m1253820557_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_probeAnchor_m1253820557_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_probeAnchor(UnityEngine.Transform)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Rendering.ReflectionProbeUsage UnityEngine.Renderer::get_reflectionProbeUsage()
extern "C"  int32_t Renderer_get_reflectionProbeUsage_m628866809 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_reflectionProbeUsage_m628866809_ftn) (Renderer_t3076687687 *);
	static Renderer_get_reflectionProbeUsage_m628866809_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_reflectionProbeUsage_m628866809_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_reflectionProbeUsage()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_reflectionProbeUsage(UnityEngine.Rendering.ReflectionProbeUsage)
extern "C"  void Renderer_set_reflectionProbeUsage_m2375616446 (Renderer_t3076687687 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_reflectionProbeUsage_m2375616446_ftn) (Renderer_t3076687687 *, int32_t);
	static Renderer_set_reflectionProbeUsage_m2375616446_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_reflectionProbeUsage_m2375616446_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_reflectionProbeUsage(UnityEngine.Rendering.ReflectionProbeUsage)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Renderer::SetPropertyBlock(UnityEngine.MaterialPropertyBlock)
extern "C"  void Renderer_SetPropertyBlock_m1621888008 (Renderer_t3076687687 * __this, MaterialPropertyBlock_t1322387783 * ___properties0, const MethodInfo* method)
{
	typedef void (*Renderer_SetPropertyBlock_m1621888008_ftn) (Renderer_t3076687687 *, MaterialPropertyBlock_t1322387783 *);
	static Renderer_SetPropertyBlock_m1621888008_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_SetPropertyBlock_m1621888008_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::SetPropertyBlock(UnityEngine.MaterialPropertyBlock)");
	_il2cpp_icall_func(__this, ___properties0);
}
// System.Void UnityEngine.Renderer::GetPropertyBlock(UnityEngine.MaterialPropertyBlock)
extern "C"  void Renderer_GetPropertyBlock_m908502780 (Renderer_t3076687687 * __this, MaterialPropertyBlock_t1322387783 * ___dest0, const MethodInfo* method)
{
	typedef void (*Renderer_GetPropertyBlock_m908502780_ftn) (Renderer_t3076687687 *, MaterialPropertyBlock_t1322387783 *);
	static Renderer_GetPropertyBlock_m908502780_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_GetPropertyBlock_m908502780_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::GetPropertyBlock(UnityEngine.MaterialPropertyBlock)");
	_il2cpp_icall_func(__this, ___dest0);
}
// System.String UnityEngine.Renderer::get_sortingLayerName()
extern "C"  String_t* Renderer_get_sortingLayerName_m3524413854 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef String_t* (*Renderer_get_sortingLayerName_m3524413854_ftn) (Renderer_t3076687687 *);
	static Renderer_get_sortingLayerName_m3524413854_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingLayerName_m3524413854_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingLayerName()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_sortingLayerName(System.String)
extern "C"  void Renderer_set_sortingLayerName_m2995255859 (Renderer_t3076687687 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_sortingLayerName_m2995255859_ftn) (Renderer_t3076687687 *, String_t*);
	static Renderer_set_sortingLayerName_m2995255859_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_sortingLayerName_m2995255859_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_sortingLayerName(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Renderer::get_sortingLayerID()
extern "C"  int32_t Renderer_get_sortingLayerID_m1954594923 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_sortingLayerID_m1954594923_ftn) (Renderer_t3076687687 *);
	static Renderer_get_sortingLayerID_m1954594923_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingLayerID_m1954594923_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingLayerID()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_sortingLayerID(System.Int32)
extern "C"  void Renderer_set_sortingLayerID_m1509940016 (Renderer_t3076687687 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_sortingLayerID_m1509940016_ftn) (Renderer_t3076687687 *, int32_t);
	static Renderer_set_sortingLayerID_m1509940016_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_sortingLayerID_m1509940016_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_sortingLayerID(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Renderer::get_sortingOrder()
extern "C"  int32_t Renderer_get_sortingOrder_m3623465101 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_sortingOrder_m3623465101_ftn) (Renderer_t3076687687 *);
	static Renderer_get_sortingOrder_m3623465101_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingOrder_m3623465101_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_sortingOrder(System.Int32)
extern "C"  void Renderer_set_sortingOrder_m3971126610 (Renderer_t3076687687 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_sortingOrder_m3971126610_ftn) (Renderer_t3076687687 *, int32_t);
	static Renderer_set_sortingOrder_m3971126610_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_sortingOrder_m3971126610_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_sortingOrder(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Renderer::GetClosestReflectionProbesInternal(System.Object)
extern "C"  void Renderer_GetClosestReflectionProbesInternal_m1728533228 (Renderer_t3076687687 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	typedef void (*Renderer_GetClosestReflectionProbesInternal_m1728533228_ftn) (Renderer_t3076687687 *, Il2CppObject *);
	static Renderer_GetClosestReflectionProbesInternal_m1728533228_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_GetClosestReflectionProbesInternal_m1728533228_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::GetClosestReflectionProbesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___result0);
}
// System.Void UnityEngine.Renderer::GetClosestReflectionProbes(System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>)
extern "C"  void Renderer_GetClosestReflectionProbes_m3363352602 (Renderer_t3076687687 * __this, List_1_t2448783290 * ___result0, const MethodInfo* method)
{
	{
		List_1_t2448783290 * L_0 = ___result0;
		Renderer_GetClosestReflectionProbesInternal_m1728533228(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::Finalize()
extern "C"  void CommandBuffer_Finalize_m2830299474 (CommandBuffer_t1654378665 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		CommandBuffer_Dispose_m3527026692(__this, (bool)0, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::Dispose()
extern "C"  void CommandBuffer_Dispose_m1371749709 (CommandBuffer_t1654378665 * __this, const MethodInfo* method)
{
	{
		CommandBuffer_Dispose_m3527026692(__this, (bool)1, /*hidden argument*/NULL);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::Dispose(System.Boolean)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t CommandBuffer_Dispose_m3527026692_MetadataUsageId;
extern "C"  void CommandBuffer_Dispose_m3527026692 (CommandBuffer_t1654378665 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CommandBuffer_Dispose_m3527026692_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CommandBuffer_ReleaseBuffer_m3519305365(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		__this->set_m_Ptr_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::ReleaseBuffer()
extern "C"  void CommandBuffer_ReleaseBuffer_m3519305365 (CommandBuffer_t1654378665 * __this, const MethodInfo* method)
{
	typedef void (*CommandBuffer_ReleaseBuffer_m3519305365_ftn) (CommandBuffer_t1654378665 *);
	static CommandBuffer_ReleaseBuffer_m3519305365_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CommandBuffer_ReleaseBuffer_m3519305365_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.CommandBuffer::ReleaseBuffer()");
	_il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.Rendering.ReflectionProbeBlendInfo
extern "C" void ReflectionProbeBlendInfo_t1080597738_marshal_pinvoke(const ReflectionProbeBlendInfo_t1080597738& unmarshaled, ReflectionProbeBlendInfo_t1080597738_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___probe_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'probe' of type 'ReflectionProbeBlendInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___probe_0Exception);
}
extern "C" void ReflectionProbeBlendInfo_t1080597738_marshal_pinvoke_back(const ReflectionProbeBlendInfo_t1080597738_marshaled_pinvoke& marshaled, ReflectionProbeBlendInfo_t1080597738& unmarshaled)
{
	Il2CppCodeGenException* ___probe_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'probe' of type 'ReflectionProbeBlendInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___probe_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Rendering.ReflectionProbeBlendInfo
extern "C" void ReflectionProbeBlendInfo_t1080597738_marshal_pinvoke_cleanup(ReflectionProbeBlendInfo_t1080597738_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Rendering.ReflectionProbeBlendInfo
extern "C" void ReflectionProbeBlendInfo_t1080597738_marshal_com(const ReflectionProbeBlendInfo_t1080597738& unmarshaled, ReflectionProbeBlendInfo_t1080597738_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___probe_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'probe' of type 'ReflectionProbeBlendInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___probe_0Exception);
}
extern "C" void ReflectionProbeBlendInfo_t1080597738_marshal_com_back(const ReflectionProbeBlendInfo_t1080597738_marshaled_com& marshaled, ReflectionProbeBlendInfo_t1080597738& unmarshaled)
{
	Il2CppCodeGenException* ___probe_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'probe' of type 'ReflectionProbeBlendInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___probe_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Rendering.ReflectionProbeBlendInfo
extern "C" void ReflectionProbeBlendInfo_t1080597738_marshal_com_cleanup(ReflectionProbeBlendInfo_t1080597738_marshaled_com& marshaled)
{
}
// System.Int32 UnityEngine.Rendering.SphericalHarmonicsL2::GetHashCode()
extern "C"  int32_t SphericalHarmonicsL2_GetHashCode_m4110797003 (SphericalHarmonicsL2_t3881980607 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = ((int32_t)17);
		int32_t L_0 = V_0;
		float* L_1 = __this->get_address_of_shr0_0();
		int32_t L_2 = Single_GetHashCode_m65342520(L_1, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_0*(int32_t)((int32_t)23)))+(int32_t)L_2));
		int32_t L_3 = V_0;
		float* L_4 = __this->get_address_of_shr1_1();
		int32_t L_5 = Single_GetHashCode_m65342520(L_4, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_3*(int32_t)((int32_t)23)))+(int32_t)L_5));
		int32_t L_6 = V_0;
		float* L_7 = __this->get_address_of_shr2_2();
		int32_t L_8 = Single_GetHashCode_m65342520(L_7, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6*(int32_t)((int32_t)23)))+(int32_t)L_8));
		int32_t L_9 = V_0;
		float* L_10 = __this->get_address_of_shr3_3();
		int32_t L_11 = Single_GetHashCode_m65342520(L_10, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_9*(int32_t)((int32_t)23)))+(int32_t)L_11));
		int32_t L_12 = V_0;
		float* L_13 = __this->get_address_of_shr4_4();
		int32_t L_14 = Single_GetHashCode_m65342520(L_13, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_12*(int32_t)((int32_t)23)))+(int32_t)L_14));
		int32_t L_15 = V_0;
		float* L_16 = __this->get_address_of_shr5_5();
		int32_t L_17 = Single_GetHashCode_m65342520(L_16, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_15*(int32_t)((int32_t)23)))+(int32_t)L_17));
		int32_t L_18 = V_0;
		float* L_19 = __this->get_address_of_shr6_6();
		int32_t L_20 = Single_GetHashCode_m65342520(L_19, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_18*(int32_t)((int32_t)23)))+(int32_t)L_20));
		int32_t L_21 = V_0;
		float* L_22 = __this->get_address_of_shr7_7();
		int32_t L_23 = Single_GetHashCode_m65342520(L_22, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_21*(int32_t)((int32_t)23)))+(int32_t)L_23));
		int32_t L_24 = V_0;
		float* L_25 = __this->get_address_of_shr8_8();
		int32_t L_26 = Single_GetHashCode_m65342520(L_25, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_24*(int32_t)((int32_t)23)))+(int32_t)L_26));
		int32_t L_27 = V_0;
		float* L_28 = __this->get_address_of_shg0_9();
		int32_t L_29 = Single_GetHashCode_m65342520(L_28, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_27*(int32_t)((int32_t)23)))+(int32_t)L_29));
		int32_t L_30 = V_0;
		float* L_31 = __this->get_address_of_shg1_10();
		int32_t L_32 = Single_GetHashCode_m65342520(L_31, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_30*(int32_t)((int32_t)23)))+(int32_t)L_32));
		int32_t L_33 = V_0;
		float* L_34 = __this->get_address_of_shg2_11();
		int32_t L_35 = Single_GetHashCode_m65342520(L_34, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_33*(int32_t)((int32_t)23)))+(int32_t)L_35));
		int32_t L_36 = V_0;
		float* L_37 = __this->get_address_of_shg3_12();
		int32_t L_38 = Single_GetHashCode_m65342520(L_37, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_36*(int32_t)((int32_t)23)))+(int32_t)L_38));
		int32_t L_39 = V_0;
		float* L_40 = __this->get_address_of_shg4_13();
		int32_t L_41 = Single_GetHashCode_m65342520(L_40, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_39*(int32_t)((int32_t)23)))+(int32_t)L_41));
		int32_t L_42 = V_0;
		float* L_43 = __this->get_address_of_shg5_14();
		int32_t L_44 = Single_GetHashCode_m65342520(L_43, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_42*(int32_t)((int32_t)23)))+(int32_t)L_44));
		int32_t L_45 = V_0;
		float* L_46 = __this->get_address_of_shg6_15();
		int32_t L_47 = Single_GetHashCode_m65342520(L_46, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_45*(int32_t)((int32_t)23)))+(int32_t)L_47));
		int32_t L_48 = V_0;
		float* L_49 = __this->get_address_of_shg7_16();
		int32_t L_50 = Single_GetHashCode_m65342520(L_49, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_48*(int32_t)((int32_t)23)))+(int32_t)L_50));
		int32_t L_51 = V_0;
		float* L_52 = __this->get_address_of_shg8_17();
		int32_t L_53 = Single_GetHashCode_m65342520(L_52, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_51*(int32_t)((int32_t)23)))+(int32_t)L_53));
		int32_t L_54 = V_0;
		float* L_55 = __this->get_address_of_shb0_18();
		int32_t L_56 = Single_GetHashCode_m65342520(L_55, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_54*(int32_t)((int32_t)23)))+(int32_t)L_56));
		int32_t L_57 = V_0;
		float* L_58 = __this->get_address_of_shb1_19();
		int32_t L_59 = Single_GetHashCode_m65342520(L_58, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_57*(int32_t)((int32_t)23)))+(int32_t)L_59));
		int32_t L_60 = V_0;
		float* L_61 = __this->get_address_of_shb2_20();
		int32_t L_62 = Single_GetHashCode_m65342520(L_61, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_60*(int32_t)((int32_t)23)))+(int32_t)L_62));
		int32_t L_63 = V_0;
		float* L_64 = __this->get_address_of_shb3_21();
		int32_t L_65 = Single_GetHashCode_m65342520(L_64, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_63*(int32_t)((int32_t)23)))+(int32_t)L_65));
		int32_t L_66 = V_0;
		float* L_67 = __this->get_address_of_shb4_22();
		int32_t L_68 = Single_GetHashCode_m65342520(L_67, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_66*(int32_t)((int32_t)23)))+(int32_t)L_68));
		int32_t L_69 = V_0;
		float* L_70 = __this->get_address_of_shb5_23();
		int32_t L_71 = Single_GetHashCode_m65342520(L_70, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_69*(int32_t)((int32_t)23)))+(int32_t)L_71));
		int32_t L_72 = V_0;
		float* L_73 = __this->get_address_of_shb6_24();
		int32_t L_74 = Single_GetHashCode_m65342520(L_73, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_72*(int32_t)((int32_t)23)))+(int32_t)L_74));
		int32_t L_75 = V_0;
		float* L_76 = __this->get_address_of_shb7_25();
		int32_t L_77 = Single_GetHashCode_m65342520(L_76, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_75*(int32_t)((int32_t)23)))+(int32_t)L_77));
		int32_t L_78 = V_0;
		float* L_79 = __this->get_address_of_shb8_26();
		int32_t L_80 = Single_GetHashCode_m65342520(L_79, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_78*(int32_t)((int32_t)23)))+(int32_t)L_80));
		int32_t L_81 = V_0;
		return L_81;
	}
}
extern "C"  int32_t SphericalHarmonicsL2_GetHashCode_m4110797003_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SphericalHarmonicsL2_t3881980607 * _thisAdjusted = reinterpret_cast<SphericalHarmonicsL2_t3881980607 *>(__this + 1);
	return SphericalHarmonicsL2_GetHashCode_m4110797003(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rendering.SphericalHarmonicsL2::Equals(System.Object)
extern Il2CppClass* SphericalHarmonicsL2_t3881980607_il2cpp_TypeInfo_var;
extern const uint32_t SphericalHarmonicsL2_Equals_m2524805223_MetadataUsageId;
extern "C"  bool SphericalHarmonicsL2_Equals_m2524805223 (SphericalHarmonicsL2_t3881980607 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SphericalHarmonicsL2_Equals_m2524805223_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SphericalHarmonicsL2_t3881980607  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, SphericalHarmonicsL2_t3881980607_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(SphericalHarmonicsL2_t3881980607 *)((SphericalHarmonicsL2_t3881980607 *)UnBox (L_1, SphericalHarmonicsL2_t3881980607_il2cpp_TypeInfo_var))));
		SphericalHarmonicsL2_t3881980607  L_2 = V_0;
		bool L_3 = SphericalHarmonicsL2_op_Equality_m3170363866(NULL /*static, unused*/, (*(SphericalHarmonicsL2_t3881980607 *)__this), L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
extern "C"  bool SphericalHarmonicsL2_Equals_m2524805223_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	SphericalHarmonicsL2_t3881980607 * _thisAdjusted = reinterpret_cast<SphericalHarmonicsL2_t3881980607 *>(__this + 1);
	return SphericalHarmonicsL2_Equals_m2524805223(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.Rendering.SphericalHarmonicsL2::op_Equality(UnityEngine.Rendering.SphericalHarmonicsL2,UnityEngine.Rendering.SphericalHarmonicsL2)
extern "C"  bool SphericalHarmonicsL2_op_Equality_m3170363866 (Il2CppObject * __this /* static, unused */, SphericalHarmonicsL2_t3881980607  ___lhs0, SphericalHarmonicsL2_t3881980607  ___rhs1, const MethodInfo* method)
{
	int32_t G_B28_0 = 0;
	{
		float L_0 = (&___lhs0)->get_shr0_0();
		float L_1 = (&___rhs1)->get_shr0_0();
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_0200;
		}
	}
	{
		float L_2 = (&___lhs0)->get_shr1_1();
		float L_3 = (&___rhs1)->get_shr1_1();
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_0200;
		}
	}
	{
		float L_4 = (&___lhs0)->get_shr2_2();
		float L_5 = (&___rhs1)->get_shr2_2();
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_0200;
		}
	}
	{
		float L_6 = (&___lhs0)->get_shr3_3();
		float L_7 = (&___rhs1)->get_shr3_3();
		if ((!(((float)L_6) == ((float)L_7))))
		{
			goto IL_0200;
		}
	}
	{
		float L_8 = (&___lhs0)->get_shr4_4();
		float L_9 = (&___rhs1)->get_shr4_4();
		if ((!(((float)L_8) == ((float)L_9))))
		{
			goto IL_0200;
		}
	}
	{
		float L_10 = (&___lhs0)->get_shr5_5();
		float L_11 = (&___rhs1)->get_shr5_5();
		if ((!(((float)L_10) == ((float)L_11))))
		{
			goto IL_0200;
		}
	}
	{
		float L_12 = (&___lhs0)->get_shr6_6();
		float L_13 = (&___rhs1)->get_shr6_6();
		if ((!(((float)L_12) == ((float)L_13))))
		{
			goto IL_0200;
		}
	}
	{
		float L_14 = (&___lhs0)->get_shr7_7();
		float L_15 = (&___rhs1)->get_shr7_7();
		if ((!(((float)L_14) == ((float)L_15))))
		{
			goto IL_0200;
		}
	}
	{
		float L_16 = (&___lhs0)->get_shr8_8();
		float L_17 = (&___rhs1)->get_shr8_8();
		if ((!(((float)L_16) == ((float)L_17))))
		{
			goto IL_0200;
		}
	}
	{
		float L_18 = (&___lhs0)->get_shg0_9();
		float L_19 = (&___rhs1)->get_shg0_9();
		if ((!(((float)L_18) == ((float)L_19))))
		{
			goto IL_0200;
		}
	}
	{
		float L_20 = (&___lhs0)->get_shg1_10();
		float L_21 = (&___rhs1)->get_shg1_10();
		if ((!(((float)L_20) == ((float)L_21))))
		{
			goto IL_0200;
		}
	}
	{
		float L_22 = (&___lhs0)->get_shg2_11();
		float L_23 = (&___rhs1)->get_shg2_11();
		if ((!(((float)L_22) == ((float)L_23))))
		{
			goto IL_0200;
		}
	}
	{
		float L_24 = (&___lhs0)->get_shg3_12();
		float L_25 = (&___rhs1)->get_shg3_12();
		if ((!(((float)L_24) == ((float)L_25))))
		{
			goto IL_0200;
		}
	}
	{
		float L_26 = (&___lhs0)->get_shg4_13();
		float L_27 = (&___rhs1)->get_shg4_13();
		if ((!(((float)L_26) == ((float)L_27))))
		{
			goto IL_0200;
		}
	}
	{
		float L_28 = (&___lhs0)->get_shg5_14();
		float L_29 = (&___rhs1)->get_shg5_14();
		if ((!(((float)L_28) == ((float)L_29))))
		{
			goto IL_0200;
		}
	}
	{
		float L_30 = (&___lhs0)->get_shg6_15();
		float L_31 = (&___rhs1)->get_shg6_15();
		if ((!(((float)L_30) == ((float)L_31))))
		{
			goto IL_0200;
		}
	}
	{
		float L_32 = (&___lhs0)->get_shg7_16();
		float L_33 = (&___rhs1)->get_shg7_16();
		if ((!(((float)L_32) == ((float)L_33))))
		{
			goto IL_0200;
		}
	}
	{
		float L_34 = (&___lhs0)->get_shg8_17();
		float L_35 = (&___rhs1)->get_shg8_17();
		if ((!(((float)L_34) == ((float)L_35))))
		{
			goto IL_0200;
		}
	}
	{
		float L_36 = (&___lhs0)->get_shb0_18();
		float L_37 = (&___rhs1)->get_shb0_18();
		if ((!(((float)L_36) == ((float)L_37))))
		{
			goto IL_0200;
		}
	}
	{
		float L_38 = (&___lhs0)->get_shb1_19();
		float L_39 = (&___rhs1)->get_shb1_19();
		if ((!(((float)L_38) == ((float)L_39))))
		{
			goto IL_0200;
		}
	}
	{
		float L_40 = (&___lhs0)->get_shb2_20();
		float L_41 = (&___rhs1)->get_shb2_20();
		if ((!(((float)L_40) == ((float)L_41))))
		{
			goto IL_0200;
		}
	}
	{
		float L_42 = (&___lhs0)->get_shb3_21();
		float L_43 = (&___rhs1)->get_shb3_21();
		if ((!(((float)L_42) == ((float)L_43))))
		{
			goto IL_0200;
		}
	}
	{
		float L_44 = (&___lhs0)->get_shb4_22();
		float L_45 = (&___rhs1)->get_shb4_22();
		if ((!(((float)L_44) == ((float)L_45))))
		{
			goto IL_0200;
		}
	}
	{
		float L_46 = (&___lhs0)->get_shb5_23();
		float L_47 = (&___rhs1)->get_shb5_23();
		if ((!(((float)L_46) == ((float)L_47))))
		{
			goto IL_0200;
		}
	}
	{
		float L_48 = (&___lhs0)->get_shb6_24();
		float L_49 = (&___rhs1)->get_shb6_24();
		if ((!(((float)L_48) == ((float)L_49))))
		{
			goto IL_0200;
		}
	}
	{
		float L_50 = (&___lhs0)->get_shb7_25();
		float L_51 = (&___rhs1)->get_shb7_25();
		if ((!(((float)L_50) == ((float)L_51))))
		{
			goto IL_0200;
		}
	}
	{
		float L_52 = (&___lhs0)->get_shb8_26();
		float L_53 = (&___rhs1)->get_shb8_26();
		G_B28_0 = ((((float)L_52) == ((float)L_53))? 1 : 0);
		goto IL_0201;
	}

IL_0200:
	{
		G_B28_0 = 0;
	}

IL_0201:
	{
		return (bool)G_B28_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.Rendering.SphericalHarmonicsL2
extern "C" void SphericalHarmonicsL2_t3881980607_marshal_pinvoke(const SphericalHarmonicsL2_t3881980607& unmarshaled, SphericalHarmonicsL2_t3881980607_marshaled_pinvoke& marshaled)
{
	marshaled.___shr0_0 = unmarshaled.get_shr0_0();
	marshaled.___shr1_1 = unmarshaled.get_shr1_1();
	marshaled.___shr2_2 = unmarshaled.get_shr2_2();
	marshaled.___shr3_3 = unmarshaled.get_shr3_3();
	marshaled.___shr4_4 = unmarshaled.get_shr4_4();
	marshaled.___shr5_5 = unmarshaled.get_shr5_5();
	marshaled.___shr6_6 = unmarshaled.get_shr6_6();
	marshaled.___shr7_7 = unmarshaled.get_shr7_7();
	marshaled.___shr8_8 = unmarshaled.get_shr8_8();
	marshaled.___shg0_9 = unmarshaled.get_shg0_9();
	marshaled.___shg1_10 = unmarshaled.get_shg1_10();
	marshaled.___shg2_11 = unmarshaled.get_shg2_11();
	marshaled.___shg3_12 = unmarshaled.get_shg3_12();
	marshaled.___shg4_13 = unmarshaled.get_shg4_13();
	marshaled.___shg5_14 = unmarshaled.get_shg5_14();
	marshaled.___shg6_15 = unmarshaled.get_shg6_15();
	marshaled.___shg7_16 = unmarshaled.get_shg7_16();
	marshaled.___shg8_17 = unmarshaled.get_shg8_17();
	marshaled.___shb0_18 = unmarshaled.get_shb0_18();
	marshaled.___shb1_19 = unmarshaled.get_shb1_19();
	marshaled.___shb2_20 = unmarshaled.get_shb2_20();
	marshaled.___shb3_21 = unmarshaled.get_shb3_21();
	marshaled.___shb4_22 = unmarshaled.get_shb4_22();
	marshaled.___shb5_23 = unmarshaled.get_shb5_23();
	marshaled.___shb6_24 = unmarshaled.get_shb6_24();
	marshaled.___shb7_25 = unmarshaled.get_shb7_25();
	marshaled.___shb8_26 = unmarshaled.get_shb8_26();
}
extern "C" void SphericalHarmonicsL2_t3881980607_marshal_pinvoke_back(const SphericalHarmonicsL2_t3881980607_marshaled_pinvoke& marshaled, SphericalHarmonicsL2_t3881980607& unmarshaled)
{
	float unmarshaled_shr0_temp_0 = 0.0f;
	unmarshaled_shr0_temp_0 = marshaled.___shr0_0;
	unmarshaled.set_shr0_0(unmarshaled_shr0_temp_0);
	float unmarshaled_shr1_temp_1 = 0.0f;
	unmarshaled_shr1_temp_1 = marshaled.___shr1_1;
	unmarshaled.set_shr1_1(unmarshaled_shr1_temp_1);
	float unmarshaled_shr2_temp_2 = 0.0f;
	unmarshaled_shr2_temp_2 = marshaled.___shr2_2;
	unmarshaled.set_shr2_2(unmarshaled_shr2_temp_2);
	float unmarshaled_shr3_temp_3 = 0.0f;
	unmarshaled_shr3_temp_3 = marshaled.___shr3_3;
	unmarshaled.set_shr3_3(unmarshaled_shr3_temp_3);
	float unmarshaled_shr4_temp_4 = 0.0f;
	unmarshaled_shr4_temp_4 = marshaled.___shr4_4;
	unmarshaled.set_shr4_4(unmarshaled_shr4_temp_4);
	float unmarshaled_shr5_temp_5 = 0.0f;
	unmarshaled_shr5_temp_5 = marshaled.___shr5_5;
	unmarshaled.set_shr5_5(unmarshaled_shr5_temp_5);
	float unmarshaled_shr6_temp_6 = 0.0f;
	unmarshaled_shr6_temp_6 = marshaled.___shr6_6;
	unmarshaled.set_shr6_6(unmarshaled_shr6_temp_6);
	float unmarshaled_shr7_temp_7 = 0.0f;
	unmarshaled_shr7_temp_7 = marshaled.___shr7_7;
	unmarshaled.set_shr7_7(unmarshaled_shr7_temp_7);
	float unmarshaled_shr8_temp_8 = 0.0f;
	unmarshaled_shr8_temp_8 = marshaled.___shr8_8;
	unmarshaled.set_shr8_8(unmarshaled_shr8_temp_8);
	float unmarshaled_shg0_temp_9 = 0.0f;
	unmarshaled_shg0_temp_9 = marshaled.___shg0_9;
	unmarshaled.set_shg0_9(unmarshaled_shg0_temp_9);
	float unmarshaled_shg1_temp_10 = 0.0f;
	unmarshaled_shg1_temp_10 = marshaled.___shg1_10;
	unmarshaled.set_shg1_10(unmarshaled_shg1_temp_10);
	float unmarshaled_shg2_temp_11 = 0.0f;
	unmarshaled_shg2_temp_11 = marshaled.___shg2_11;
	unmarshaled.set_shg2_11(unmarshaled_shg2_temp_11);
	float unmarshaled_shg3_temp_12 = 0.0f;
	unmarshaled_shg3_temp_12 = marshaled.___shg3_12;
	unmarshaled.set_shg3_12(unmarshaled_shg3_temp_12);
	float unmarshaled_shg4_temp_13 = 0.0f;
	unmarshaled_shg4_temp_13 = marshaled.___shg4_13;
	unmarshaled.set_shg4_13(unmarshaled_shg4_temp_13);
	float unmarshaled_shg5_temp_14 = 0.0f;
	unmarshaled_shg5_temp_14 = marshaled.___shg5_14;
	unmarshaled.set_shg5_14(unmarshaled_shg5_temp_14);
	float unmarshaled_shg6_temp_15 = 0.0f;
	unmarshaled_shg6_temp_15 = marshaled.___shg6_15;
	unmarshaled.set_shg6_15(unmarshaled_shg6_temp_15);
	float unmarshaled_shg7_temp_16 = 0.0f;
	unmarshaled_shg7_temp_16 = marshaled.___shg7_16;
	unmarshaled.set_shg7_16(unmarshaled_shg7_temp_16);
	float unmarshaled_shg8_temp_17 = 0.0f;
	unmarshaled_shg8_temp_17 = marshaled.___shg8_17;
	unmarshaled.set_shg8_17(unmarshaled_shg8_temp_17);
	float unmarshaled_shb0_temp_18 = 0.0f;
	unmarshaled_shb0_temp_18 = marshaled.___shb0_18;
	unmarshaled.set_shb0_18(unmarshaled_shb0_temp_18);
	float unmarshaled_shb1_temp_19 = 0.0f;
	unmarshaled_shb1_temp_19 = marshaled.___shb1_19;
	unmarshaled.set_shb1_19(unmarshaled_shb1_temp_19);
	float unmarshaled_shb2_temp_20 = 0.0f;
	unmarshaled_shb2_temp_20 = marshaled.___shb2_20;
	unmarshaled.set_shb2_20(unmarshaled_shb2_temp_20);
	float unmarshaled_shb3_temp_21 = 0.0f;
	unmarshaled_shb3_temp_21 = marshaled.___shb3_21;
	unmarshaled.set_shb3_21(unmarshaled_shb3_temp_21);
	float unmarshaled_shb4_temp_22 = 0.0f;
	unmarshaled_shb4_temp_22 = marshaled.___shb4_22;
	unmarshaled.set_shb4_22(unmarshaled_shb4_temp_22);
	float unmarshaled_shb5_temp_23 = 0.0f;
	unmarshaled_shb5_temp_23 = marshaled.___shb5_23;
	unmarshaled.set_shb5_23(unmarshaled_shb5_temp_23);
	float unmarshaled_shb6_temp_24 = 0.0f;
	unmarshaled_shb6_temp_24 = marshaled.___shb6_24;
	unmarshaled.set_shb6_24(unmarshaled_shb6_temp_24);
	float unmarshaled_shb7_temp_25 = 0.0f;
	unmarshaled_shb7_temp_25 = marshaled.___shb7_25;
	unmarshaled.set_shb7_25(unmarshaled_shb7_temp_25);
	float unmarshaled_shb8_temp_26 = 0.0f;
	unmarshaled_shb8_temp_26 = marshaled.___shb8_26;
	unmarshaled.set_shb8_26(unmarshaled_shb8_temp_26);
}
// Conversion method for clean up from marshalling of: UnityEngine.Rendering.SphericalHarmonicsL2
extern "C" void SphericalHarmonicsL2_t3881980607_marshal_pinvoke_cleanup(SphericalHarmonicsL2_t3881980607_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Rendering.SphericalHarmonicsL2
extern "C" void SphericalHarmonicsL2_t3881980607_marshal_com(const SphericalHarmonicsL2_t3881980607& unmarshaled, SphericalHarmonicsL2_t3881980607_marshaled_com& marshaled)
{
	marshaled.___shr0_0 = unmarshaled.get_shr0_0();
	marshaled.___shr1_1 = unmarshaled.get_shr1_1();
	marshaled.___shr2_2 = unmarshaled.get_shr2_2();
	marshaled.___shr3_3 = unmarshaled.get_shr3_3();
	marshaled.___shr4_4 = unmarshaled.get_shr4_4();
	marshaled.___shr5_5 = unmarshaled.get_shr5_5();
	marshaled.___shr6_6 = unmarshaled.get_shr6_6();
	marshaled.___shr7_7 = unmarshaled.get_shr7_7();
	marshaled.___shr8_8 = unmarshaled.get_shr8_8();
	marshaled.___shg0_9 = unmarshaled.get_shg0_9();
	marshaled.___shg1_10 = unmarshaled.get_shg1_10();
	marshaled.___shg2_11 = unmarshaled.get_shg2_11();
	marshaled.___shg3_12 = unmarshaled.get_shg3_12();
	marshaled.___shg4_13 = unmarshaled.get_shg4_13();
	marshaled.___shg5_14 = unmarshaled.get_shg5_14();
	marshaled.___shg6_15 = unmarshaled.get_shg6_15();
	marshaled.___shg7_16 = unmarshaled.get_shg7_16();
	marshaled.___shg8_17 = unmarshaled.get_shg8_17();
	marshaled.___shb0_18 = unmarshaled.get_shb0_18();
	marshaled.___shb1_19 = unmarshaled.get_shb1_19();
	marshaled.___shb2_20 = unmarshaled.get_shb2_20();
	marshaled.___shb3_21 = unmarshaled.get_shb3_21();
	marshaled.___shb4_22 = unmarshaled.get_shb4_22();
	marshaled.___shb5_23 = unmarshaled.get_shb5_23();
	marshaled.___shb6_24 = unmarshaled.get_shb6_24();
	marshaled.___shb7_25 = unmarshaled.get_shb7_25();
	marshaled.___shb8_26 = unmarshaled.get_shb8_26();
}
extern "C" void SphericalHarmonicsL2_t3881980607_marshal_com_back(const SphericalHarmonicsL2_t3881980607_marshaled_com& marshaled, SphericalHarmonicsL2_t3881980607& unmarshaled)
{
	float unmarshaled_shr0_temp_0 = 0.0f;
	unmarshaled_shr0_temp_0 = marshaled.___shr0_0;
	unmarshaled.set_shr0_0(unmarshaled_shr0_temp_0);
	float unmarshaled_shr1_temp_1 = 0.0f;
	unmarshaled_shr1_temp_1 = marshaled.___shr1_1;
	unmarshaled.set_shr1_1(unmarshaled_shr1_temp_1);
	float unmarshaled_shr2_temp_2 = 0.0f;
	unmarshaled_shr2_temp_2 = marshaled.___shr2_2;
	unmarshaled.set_shr2_2(unmarshaled_shr2_temp_2);
	float unmarshaled_shr3_temp_3 = 0.0f;
	unmarshaled_shr3_temp_3 = marshaled.___shr3_3;
	unmarshaled.set_shr3_3(unmarshaled_shr3_temp_3);
	float unmarshaled_shr4_temp_4 = 0.0f;
	unmarshaled_shr4_temp_4 = marshaled.___shr4_4;
	unmarshaled.set_shr4_4(unmarshaled_shr4_temp_4);
	float unmarshaled_shr5_temp_5 = 0.0f;
	unmarshaled_shr5_temp_5 = marshaled.___shr5_5;
	unmarshaled.set_shr5_5(unmarshaled_shr5_temp_5);
	float unmarshaled_shr6_temp_6 = 0.0f;
	unmarshaled_shr6_temp_6 = marshaled.___shr6_6;
	unmarshaled.set_shr6_6(unmarshaled_shr6_temp_6);
	float unmarshaled_shr7_temp_7 = 0.0f;
	unmarshaled_shr7_temp_7 = marshaled.___shr7_7;
	unmarshaled.set_shr7_7(unmarshaled_shr7_temp_7);
	float unmarshaled_shr8_temp_8 = 0.0f;
	unmarshaled_shr8_temp_8 = marshaled.___shr8_8;
	unmarshaled.set_shr8_8(unmarshaled_shr8_temp_8);
	float unmarshaled_shg0_temp_9 = 0.0f;
	unmarshaled_shg0_temp_9 = marshaled.___shg0_9;
	unmarshaled.set_shg0_9(unmarshaled_shg0_temp_9);
	float unmarshaled_shg1_temp_10 = 0.0f;
	unmarshaled_shg1_temp_10 = marshaled.___shg1_10;
	unmarshaled.set_shg1_10(unmarshaled_shg1_temp_10);
	float unmarshaled_shg2_temp_11 = 0.0f;
	unmarshaled_shg2_temp_11 = marshaled.___shg2_11;
	unmarshaled.set_shg2_11(unmarshaled_shg2_temp_11);
	float unmarshaled_shg3_temp_12 = 0.0f;
	unmarshaled_shg3_temp_12 = marshaled.___shg3_12;
	unmarshaled.set_shg3_12(unmarshaled_shg3_temp_12);
	float unmarshaled_shg4_temp_13 = 0.0f;
	unmarshaled_shg4_temp_13 = marshaled.___shg4_13;
	unmarshaled.set_shg4_13(unmarshaled_shg4_temp_13);
	float unmarshaled_shg5_temp_14 = 0.0f;
	unmarshaled_shg5_temp_14 = marshaled.___shg5_14;
	unmarshaled.set_shg5_14(unmarshaled_shg5_temp_14);
	float unmarshaled_shg6_temp_15 = 0.0f;
	unmarshaled_shg6_temp_15 = marshaled.___shg6_15;
	unmarshaled.set_shg6_15(unmarshaled_shg6_temp_15);
	float unmarshaled_shg7_temp_16 = 0.0f;
	unmarshaled_shg7_temp_16 = marshaled.___shg7_16;
	unmarshaled.set_shg7_16(unmarshaled_shg7_temp_16);
	float unmarshaled_shg8_temp_17 = 0.0f;
	unmarshaled_shg8_temp_17 = marshaled.___shg8_17;
	unmarshaled.set_shg8_17(unmarshaled_shg8_temp_17);
	float unmarshaled_shb0_temp_18 = 0.0f;
	unmarshaled_shb0_temp_18 = marshaled.___shb0_18;
	unmarshaled.set_shb0_18(unmarshaled_shb0_temp_18);
	float unmarshaled_shb1_temp_19 = 0.0f;
	unmarshaled_shb1_temp_19 = marshaled.___shb1_19;
	unmarshaled.set_shb1_19(unmarshaled_shb1_temp_19);
	float unmarshaled_shb2_temp_20 = 0.0f;
	unmarshaled_shb2_temp_20 = marshaled.___shb2_20;
	unmarshaled.set_shb2_20(unmarshaled_shb2_temp_20);
	float unmarshaled_shb3_temp_21 = 0.0f;
	unmarshaled_shb3_temp_21 = marshaled.___shb3_21;
	unmarshaled.set_shb3_21(unmarshaled_shb3_temp_21);
	float unmarshaled_shb4_temp_22 = 0.0f;
	unmarshaled_shb4_temp_22 = marshaled.___shb4_22;
	unmarshaled.set_shb4_22(unmarshaled_shb4_temp_22);
	float unmarshaled_shb5_temp_23 = 0.0f;
	unmarshaled_shb5_temp_23 = marshaled.___shb5_23;
	unmarshaled.set_shb5_23(unmarshaled_shb5_temp_23);
	float unmarshaled_shb6_temp_24 = 0.0f;
	unmarshaled_shb6_temp_24 = marshaled.___shb6_24;
	unmarshaled.set_shb6_24(unmarshaled_shb6_temp_24);
	float unmarshaled_shb7_temp_25 = 0.0f;
	unmarshaled_shb7_temp_25 = marshaled.___shb7_25;
	unmarshaled.set_shb7_25(unmarshaled_shb7_temp_25);
	float unmarshaled_shb8_temp_26 = 0.0f;
	unmarshaled_shb8_temp_26 = marshaled.___shb8_26;
	unmarshaled.set_shb8_26(unmarshaled_shb8_temp_26);
}
// Conversion method for clean up from marshalling of: UnityEngine.Rendering.SphericalHarmonicsL2
extern "C" void SphericalHarmonicsL2_t3881980607_marshal_com_cleanup(SphericalHarmonicsL2_t3881980607_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.RenderSettings::.ctor()
extern "C"  void RenderSettings__ctor_m3642059316 (RenderSettings_t425127197 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m570634126(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.RenderSettings::get_fog()
extern "C"  bool RenderSettings_get_fog_m4042411809 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*RenderSettings_get_fog_m4042411809_ftn) ();
	static RenderSettings_get_fog_m4042411809_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_fog_m4042411809_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_fog()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_fog(System.Boolean)
extern "C"  void RenderSettings_set_fog_m1757489802 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_fog_m1757489802_ftn) (bool);
	static RenderSettings_set_fog_m1757489802_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_fog_m1757489802_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_fog(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.FogMode UnityEngine.RenderSettings::get_fogMode()
extern "C"  int32_t RenderSettings_get_fogMode_m1271225857 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*RenderSettings_get_fogMode_m1271225857_ftn) ();
	static RenderSettings_get_fogMode_m1271225857_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_fogMode_m1271225857_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_fogMode()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_fogMode(UnityEngine.FogMode)
extern "C"  void RenderSettings_set_fogMode_m1549353906 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_fogMode_m1549353906_ftn) (int32_t);
	static RenderSettings_set_fogMode_m1549353906_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_fogMode_m1549353906_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_fogMode(UnityEngine.FogMode)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Color UnityEngine.RenderSettings::get_fogColor()
extern "C"  Color_t4194546905  RenderSettings_get_fogColor_m4156514373 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RenderSettings_INTERNAL_get_fogColor_m650951080(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Color_t4194546905  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RenderSettings::set_fogColor(UnityEngine.Color)
extern "C"  void RenderSettings_set_fogColor_m1507677940 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method)
{
	{
		RenderSettings_INTERNAL_set_fogColor_m2299508764(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_get_fogColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_fogColor_m650951080 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_get_fogColor_m650951080_ftn) (Color_t4194546905 *);
	static RenderSettings_INTERNAL_get_fogColor_m650951080_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_get_fogColor_m650951080_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_get_fogColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::INTERNAL_set_fogColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_fogColor_m2299508764 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_set_fogColor_m2299508764_ftn) (Color_t4194546905 *);
	static RenderSettings_INTERNAL_set_fogColor_m2299508764_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_set_fogColor_m2299508764_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_set_fogColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.RenderSettings::get_fogDensity()
extern "C"  float RenderSettings_get_fogDensity_m2444985551 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*RenderSettings_get_fogDensity_m2444985551_ftn) ();
	static RenderSettings_get_fogDensity_m2444985551_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_fogDensity_m2444985551_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_fogDensity()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_fogDensity(System.Single)
extern "C"  void RenderSettings_set_fogDensity_m998518996 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_fogDensity_m998518996_ftn) (float);
	static RenderSettings_set_fogDensity_m998518996_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_fogDensity_m998518996_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_fogDensity(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.RenderSettings::get_fogStartDistance()
extern "C"  float RenderSettings_get_fogStartDistance_m3752408606 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*RenderSettings_get_fogStartDistance_m3752408606_ftn) ();
	static RenderSettings_get_fogStartDistance_m3752408606_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_fogStartDistance_m3752408606_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_fogStartDistance()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_fogStartDistance(System.Single)
extern "C"  void RenderSettings_set_fogStartDistance_m1396065765 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_fogStartDistance_m1396065765_ftn) (float);
	static RenderSettings_set_fogStartDistance_m1396065765_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_fogStartDistance_m1396065765_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_fogStartDistance(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.RenderSettings::get_fogEndDistance()
extern "C"  float RenderSettings_get_fogEndDistance_m167008471 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*RenderSettings_get_fogEndDistance_m167008471_ftn) ();
	static RenderSettings_get_fogEndDistance_m167008471_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_fogEndDistance_m167008471_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_fogEndDistance()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_fogEndDistance(System.Single)
extern "C"  void RenderSettings_set_fogEndDistance_m2793577932 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_fogEndDistance_m2793577932_ftn) (float);
	static RenderSettings_set_fogEndDistance_m2793577932_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_fogEndDistance_m2793577932_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_fogEndDistance(System.Single)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Rendering.AmbientMode UnityEngine.RenderSettings::get_ambientMode()
extern "C"  int32_t RenderSettings_get_ambientMode_m3682295539 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*RenderSettings_get_ambientMode_m3682295539_ftn) ();
	static RenderSettings_get_ambientMode_m3682295539_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_ambientMode_m3682295539_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_ambientMode()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_ambientMode(UnityEngine.Rendering.AmbientMode)
extern "C"  void RenderSettings_set_ambientMode_m3321344960 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_ambientMode_m3321344960_ftn) (int32_t);
	static RenderSettings_set_ambientMode_m3321344960_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_ambientMode_m3321344960_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_ambientMode(UnityEngine.Rendering.AmbientMode)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Color UnityEngine.RenderSettings::get_ambientSkyColor()
extern "C"  Color_t4194546905  RenderSettings_get_ambientSkyColor_m1963190236 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RenderSettings_INTERNAL_get_ambientSkyColor_m4179463781(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Color_t4194546905  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RenderSettings::set_ambientSkyColor(UnityEngine.Color)
extern "C"  void RenderSettings_set_ambientSkyColor_m2161476599 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method)
{
	{
		RenderSettings_INTERNAL_set_ambientSkyColor_m1697195377(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientSkyColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_ambientSkyColor_m4179463781 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_get_ambientSkyColor_m4179463781_ftn) (Color_t4194546905 *);
	static RenderSettings_INTERNAL_get_ambientSkyColor_m4179463781_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_get_ambientSkyColor_m4179463781_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_get_ambientSkyColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientSkyColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_ambientSkyColor_m1697195377 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_set_ambientSkyColor_m1697195377_ftn) (Color_t4194546905 *);
	static RenderSettings_INTERNAL_set_ambientSkyColor_m1697195377_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_set_ambientSkyColor_m1697195377_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_set_ambientSkyColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Color UnityEngine.RenderSettings::get_ambientEquatorColor()
extern "C"  Color_t4194546905  RenderSettings_get_ambientEquatorColor_m276013214 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RenderSettings_INTERNAL_get_ambientEquatorColor_m952963367(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Color_t4194546905  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RenderSettings::set_ambientEquatorColor(UnityEngine.Color)
extern "C"  void RenderSettings_set_ambientEquatorColor_m3763161333 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method)
{
	{
		RenderSettings_INTERNAL_set_ambientEquatorColor_m4158538291(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientEquatorColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_ambientEquatorColor_m952963367 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_get_ambientEquatorColor_m952963367_ftn) (Color_t4194546905 *);
	static RenderSettings_INTERNAL_get_ambientEquatorColor_m952963367_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_get_ambientEquatorColor_m952963367_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_get_ambientEquatorColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientEquatorColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_ambientEquatorColor_m4158538291 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_set_ambientEquatorColor_m4158538291_ftn) (Color_t4194546905 *);
	static RenderSettings_INTERNAL_set_ambientEquatorColor_m4158538291_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_set_ambientEquatorColor_m4158538291_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_set_ambientEquatorColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Color UnityEngine.RenderSettings::get_ambientGroundColor()
extern "C"  Color_t4194546905  RenderSettings_get_ambientGroundColor_m847528164 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RenderSettings_INTERNAL_get_ambientGroundColor_m1254844487(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Color_t4194546905  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RenderSettings::set_ambientGroundColor(UnityEngine.Color)
extern "C"  void RenderSettings_set_ambientGroundColor_m2739647605 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method)
{
	{
		RenderSettings_INTERNAL_set_ambientGroundColor_m2743723451(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientGroundColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_ambientGroundColor_m1254844487 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_get_ambientGroundColor_m1254844487_ftn) (Color_t4194546905 *);
	static RenderSettings_INTERNAL_get_ambientGroundColor_m1254844487_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_get_ambientGroundColor_m1254844487_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_get_ambientGroundColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientGroundColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_ambientGroundColor_m2743723451 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_set_ambientGroundColor_m2743723451_ftn) (Color_t4194546905 *);
	static RenderSettings_INTERNAL_set_ambientGroundColor_m2743723451_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_set_ambientGroundColor_m2743723451_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_set_ambientGroundColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Color UnityEngine.RenderSettings::get_ambientLight()
extern "C"  Color_t4194546905  RenderSettings_get_ambientLight_m929316510 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RenderSettings_INTERNAL_get_ambientLight_m691711297(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Color_t4194546905  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RenderSettings::set_ambientLight(UnityEngine.Color)
extern "C"  void RenderSettings_set_ambientLight_m791635003 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method)
{
	{
		RenderSettings_INTERNAL_set_ambientLight_m2620477877(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientLight(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_ambientLight_m691711297 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_get_ambientLight_m691711297_ftn) (Color_t4194546905 *);
	static RenderSettings_INTERNAL_get_ambientLight_m691711297_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_get_ambientLight_m691711297_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_get_ambientLight(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientLight(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_ambientLight_m2620477877 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_set_ambientLight_m2620477877_ftn) (Color_t4194546905 *);
	static RenderSettings_INTERNAL_set_ambientLight_m2620477877_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_set_ambientLight_m2620477877_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_set_ambientLight(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.RenderSettings::get_ambientIntensity()
extern "C"  float RenderSettings_get_ambientIntensity_m1272966080 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*RenderSettings_get_ambientIntensity_m1272966080_ftn) ();
	static RenderSettings_get_ambientIntensity_m1272966080_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_ambientIntensity_m1272966080_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_ambientIntensity()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_ambientIntensity(System.Single)
extern "C"  void RenderSettings_set_ambientIntensity_m2896643971 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_ambientIntensity_m2896643971_ftn) (float);
	static RenderSettings_set_ambientIntensity_m2896643971_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_ambientIntensity_m2896643971_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_ambientIntensity(System.Single)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Rendering.SphericalHarmonicsL2 UnityEngine.RenderSettings::get_ambientProbe()
extern "C"  SphericalHarmonicsL2_t3881980607  RenderSettings_get_ambientProbe_m3418661496 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	SphericalHarmonicsL2_t3881980607  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RenderSettings_INTERNAL_get_ambientProbe_m1788208671(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		SphericalHarmonicsL2_t3881980607  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RenderSettings::set_ambientProbe(UnityEngine.Rendering.SphericalHarmonicsL2)
extern "C"  void RenderSettings_set_ambientProbe_m3366119677 (Il2CppObject * __this /* static, unused */, SphericalHarmonicsL2_t3881980607  ___value0, const MethodInfo* method)
{
	{
		RenderSettings_INTERNAL_set_ambientProbe_m672754731(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientProbe(UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C"  void RenderSettings_INTERNAL_get_ambientProbe_m1788208671 (Il2CppObject * __this /* static, unused */, SphericalHarmonicsL2_t3881980607 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_get_ambientProbe_m1788208671_ftn) (SphericalHarmonicsL2_t3881980607 *);
	static RenderSettings_INTERNAL_get_ambientProbe_m1788208671_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_get_ambientProbe_m1788208671_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_get_ambientProbe(UnityEngine.Rendering.SphericalHarmonicsL2&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientProbe(UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C"  void RenderSettings_INTERNAL_set_ambientProbe_m672754731 (Il2CppObject * __this /* static, unused */, SphericalHarmonicsL2_t3881980607 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_set_ambientProbe_m672754731_ftn) (SphericalHarmonicsL2_t3881980607 *);
	static RenderSettings_INTERNAL_set_ambientProbe_m672754731_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_set_ambientProbe_m672754731_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_set_ambientProbe(UnityEngine.Rendering.SphericalHarmonicsL2&)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.RenderSettings::get_reflectionIntensity()
extern "C"  float RenderSettings_get_reflectionIntensity_m1862510533 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*RenderSettings_get_reflectionIntensity_m1862510533_ftn) ();
	static RenderSettings_get_reflectionIntensity_m1862510533_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_reflectionIntensity_m1862510533_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_reflectionIntensity()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_reflectionIntensity(System.Single)
extern "C"  void RenderSettings_set_reflectionIntensity_m4083040622 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_reflectionIntensity_m4083040622_ftn) (float);
	static RenderSettings_set_reflectionIntensity_m4083040622_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_reflectionIntensity_m4083040622_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_reflectionIntensity(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.RenderSettings::get_reflectionBounces()
extern "C"  int32_t RenderSettings_get_reflectionBounces_m2858699465 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*RenderSettings_get_reflectionBounces_m2858699465_ftn) ();
	static RenderSettings_get_reflectionBounces_m2858699465_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_reflectionBounces_m2858699465_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_reflectionBounces()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_reflectionBounces(System.Int32)
extern "C"  void RenderSettings_set_reflectionBounces_m2938034214 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_reflectionBounces_m2938034214_ftn) (int32_t);
	static RenderSettings_set_reflectionBounces_m2938034214_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_reflectionBounces_m2938034214_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_reflectionBounces(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.RenderSettings::get_haloStrength()
extern "C"  float RenderSettings_get_haloStrength_m4164965730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*RenderSettings_get_haloStrength_m4164965730_ftn) ();
	static RenderSettings_get_haloStrength_m4164965730_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_haloStrength_m4164965730_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_haloStrength()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_haloStrength(System.Single)
extern "C"  void RenderSettings_set_haloStrength_m1179623713 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_haloStrength_m1179623713_ftn) (float);
	static RenderSettings_set_haloStrength_m1179623713_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_haloStrength_m1179623713_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_haloStrength(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.RenderSettings::get_flareStrength()
extern "C"  float RenderSettings_get_flareStrength_m2160918764 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*RenderSettings_get_flareStrength_m2160918764_ftn) ();
	static RenderSettings_get_flareStrength_m2160918764_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_flareStrength_m2160918764_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_flareStrength()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_flareStrength(System.Single)
extern "C"  void RenderSettings_set_flareStrength_m38596007 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_flareStrength_m38596007_ftn) (float);
	static RenderSettings_set_flareStrength_m38596007_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_flareStrength_m38596007_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_flareStrength(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.RenderSettings::get_flareFadeSpeed()
extern "C"  float RenderSettings_get_flareFadeSpeed_m1178866370 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*RenderSettings_get_flareFadeSpeed_m1178866370_ftn) ();
	static RenderSettings_get_flareFadeSpeed_m1178866370_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_flareFadeSpeed_m1178866370_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_flareFadeSpeed()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_flareFadeSpeed(System.Single)
extern "C"  void RenderSettings_set_flareFadeSpeed_m3219102145 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_flareFadeSpeed_m3219102145_ftn) (float);
	static RenderSettings_set_flareFadeSpeed_m3219102145_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_flareFadeSpeed_m3219102145_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_flareFadeSpeed(System.Single)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Material UnityEngine.RenderSettings::get_skybox()
extern "C"  Material_t3870600107 * RenderSettings_get_skybox_m505912468 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Material_t3870600107 * (*RenderSettings_get_skybox_m505912468_ftn) ();
	static RenderSettings_get_skybox_m505912468_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_skybox_m505912468_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_skybox()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_skybox(UnityEngine.Material)
extern "C"  void RenderSettings_set_skybox_m3777670233 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_skybox_m3777670233_ftn) (Material_t3870600107 *);
	static RenderSettings_set_skybox_m3777670233_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_skybox_m3777670233_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_skybox(UnityEngine.Material)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Rendering.DefaultReflectionMode UnityEngine.RenderSettings::get_defaultReflectionMode()
extern "C"  int32_t RenderSettings_get_defaultReflectionMode_m524523611 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*RenderSettings_get_defaultReflectionMode_m524523611_ftn) ();
	static RenderSettings_get_defaultReflectionMode_m524523611_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_defaultReflectionMode_m524523611_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_defaultReflectionMode()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_defaultReflectionMode(UnityEngine.Rendering.DefaultReflectionMode)
extern "C"  void RenderSettings_set_defaultReflectionMode_m2028982232 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_defaultReflectionMode_m2028982232_ftn) (int32_t);
	static RenderSettings_set_defaultReflectionMode_m2028982232_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_defaultReflectionMode_m2028982232_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_defaultReflectionMode(UnityEngine.Rendering.DefaultReflectionMode)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.RenderSettings::get_defaultReflectionResolution()
extern "C"  int32_t RenderSettings_get_defaultReflectionResolution_m3866804641 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*RenderSettings_get_defaultReflectionResolution_m3866804641_ftn) ();
	static RenderSettings_get_defaultReflectionResolution_m3866804641_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_defaultReflectionResolution_m3866804641_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_defaultReflectionResolution()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_defaultReflectionResolution(System.Int32)
extern "C"  void RenderSettings_set_defaultReflectionResolution_m1493912574 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_defaultReflectionResolution_m1493912574_ftn) (int32_t);
	static RenderSettings_set_defaultReflectionResolution_m1493912574_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_defaultReflectionResolution_m1493912574_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_defaultReflectionResolution(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Cubemap UnityEngine.RenderSettings::get_customReflection()
extern "C"  Cubemap_t761092157 * RenderSettings_get_customReflection_m1648275032 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Cubemap_t761092157 * (*RenderSettings_get_customReflection_m1648275032_ftn) ();
	static RenderSettings_get_customReflection_m1648275032_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_customReflection_m1648275032_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_customReflection()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_customReflection(UnityEngine.Cubemap)
extern "C"  void RenderSettings_set_customReflection_m1314253177 (Il2CppObject * __this /* static, unused */, Cubemap_t761092157 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_customReflection_m1314253177_ftn) (Cubemap_t761092157 *);
	static RenderSettings_set_customReflection_m1314253177_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_customReflection_m1314253177_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_customReflection(UnityEngine.Cubemap)");
	_il2cpp_icall_func(___value0);
}
// Conversion methods for marshalling of: UnityEngine.RenderTargetSetup
extern "C" void RenderTargetSetup_t2400315372_marshal_pinvoke(const RenderTargetSetup_t2400315372& unmarshaled, RenderTargetSetup_t2400315372_marshaled_pinvoke& marshaled)
{
	marshaled.___color_0 = il2cpp_codegen_marshal_array<RenderBuffer_t3529837690_marshaled_pinvoke>((Il2CppCodeGenArray*)unmarshaled.get_color_0());
	RenderBuffer_t3529837690_marshal_pinvoke(unmarshaled.get_depth_1(), marshaled.___depth_1);
	marshaled.___mipLevel_2 = unmarshaled.get_mipLevel_2();
	marshaled.___cubemapFace_3 = unmarshaled.get_cubemapFace_3();
	uint32_t _unmarshaled_colorLoad_Length = 0;
	if (unmarshaled.get_colorLoad_4() != NULL)
	{
		_unmarshaled_colorLoad_Length = ((Il2CppCodeGenArray*)unmarshaled.get_colorLoad_4())->max_length;
		marshaled.___colorLoad_4 = il2cpp_codegen_marshal_allocate_array<int32_t>(_unmarshaled_colorLoad_Length);
	}
	for (uint32_t i = 0; i < _unmarshaled_colorLoad_Length; i++)
	{
		(marshaled.___colorLoad_4)[i] = (unmarshaled.get_colorLoad_4())->GetAt(static_cast<il2cpp_array_size_t>(i));
	}
	uint32_t _unmarshaled_colorStore_Length = 0;
	if (unmarshaled.get_colorStore_5() != NULL)
	{
		_unmarshaled_colorStore_Length = ((Il2CppCodeGenArray*)unmarshaled.get_colorStore_5())->max_length;
		marshaled.___colorStore_5 = il2cpp_codegen_marshal_allocate_array<int32_t>(_unmarshaled_colorStore_Length);
	}
	for (uint32_t i = 0; i < _unmarshaled_colorStore_Length; i++)
	{
		(marshaled.___colorStore_5)[i] = (unmarshaled.get_colorStore_5())->GetAt(static_cast<il2cpp_array_size_t>(i));
	}
	marshaled.___depthLoad_6 = unmarshaled.get_depthLoad_6();
	marshaled.___depthStore_7 = unmarshaled.get_depthStore_7();
}
extern Il2CppClass* RenderBuffer_t3529837690_il2cpp_TypeInfo_var;
extern Il2CppClass* RenderBufferLoadActionU5BU5D_t3840699671_il2cpp_TypeInfo_var;
extern Il2CppClass* RenderBufferStoreActionU5BU5D_t2354850566_il2cpp_TypeInfo_var;
extern const uint32_t RenderTargetSetup_t2400315372_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern "C" void RenderTargetSetup_t2400315372_marshal_pinvoke_back(const RenderTargetSetup_t2400315372_marshaled_pinvoke& marshaled, RenderTargetSetup_t2400315372& unmarshaled)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RenderTargetSetup_t2400315372_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	unmarshaled.set_color_0((RenderBufferU5BU5D_t1970987103*)il2cpp_codegen_marshal_array_result(RenderBuffer_t3529837690_il2cpp_TypeInfo_var, marshaled.___color_0, 1));
	RenderBuffer_t3529837690  unmarshaled_depth_temp_1;
	memset(&unmarshaled_depth_temp_1, 0, sizeof(unmarshaled_depth_temp_1));
	RenderBuffer_t3529837690_marshal_pinvoke_back(marshaled.___depth_1, unmarshaled_depth_temp_1);
	unmarshaled.set_depth_1(unmarshaled_depth_temp_1);
	int32_t unmarshaled_mipLevel_temp_2 = 0;
	unmarshaled_mipLevel_temp_2 = marshaled.___mipLevel_2;
	unmarshaled.set_mipLevel_2(unmarshaled_mipLevel_temp_2);
	int32_t unmarshaled_cubemapFace_temp_3 = 0;
	unmarshaled_cubemapFace_temp_3 = marshaled.___cubemapFace_3;
	unmarshaled.set_cubemapFace_3(unmarshaled_cubemapFace_temp_3);
	if (marshaled.___colorLoad_4 != NULL)
	{
		uint32_t _loopCount;
		if (unmarshaled.get_colorLoad_4() == NULL)
		{
			_loopCount = 1;
			unmarshaled.set_colorLoad_4(reinterpret_cast<RenderBufferLoadActionU5BU5D_t3840699671*>(SZArrayNew(RenderBufferLoadActionU5BU5D_t3840699671_il2cpp_TypeInfo_var, _loopCount)));
		}
		else
		{
			_loopCount = ((Il2CppCodeGenArray*)unmarshaled.get_colorLoad_4())->max_length;
		}
		for (uint32_t i = 0; i < _loopCount; i++)
		{
			int32_t _item = 0;
			_item = (marshaled.___colorLoad_4)[i];
			(unmarshaled.get_colorLoad_4())->SetAt(static_cast<il2cpp_array_size_t>(i), _item);
		}
	}
	if (marshaled.___colorStore_5 != NULL)
	{
		uint32_t _loopCount;
		if (unmarshaled.get_colorStore_5() == NULL)
		{
			_loopCount = 1;
			unmarshaled.set_colorStore_5(reinterpret_cast<RenderBufferStoreActionU5BU5D_t2354850566*>(SZArrayNew(RenderBufferStoreActionU5BU5D_t2354850566_il2cpp_TypeInfo_var, _loopCount)));
		}
		else
		{
			_loopCount = ((Il2CppCodeGenArray*)unmarshaled.get_colorStore_5())->max_length;
		}
		for (uint32_t i = 0; i < _loopCount; i++)
		{
			int32_t _item = 0;
			_item = (marshaled.___colorStore_5)[i];
			(unmarshaled.get_colorStore_5())->SetAt(static_cast<il2cpp_array_size_t>(i), _item);
		}
	}
	int32_t unmarshaled_depthLoad_temp_6 = 0;
	unmarshaled_depthLoad_temp_6 = marshaled.___depthLoad_6;
	unmarshaled.set_depthLoad_6(unmarshaled_depthLoad_temp_6);
	int32_t unmarshaled_depthStore_temp_7 = 0;
	unmarshaled_depthStore_temp_7 = marshaled.___depthStore_7;
	unmarshaled.set_depthStore_7(unmarshaled_depthStore_temp_7);
}
// Conversion method for clean up from marshalling of: UnityEngine.RenderTargetSetup
extern "C" void RenderTargetSetup_t2400315372_marshal_pinvoke_cleanup(RenderTargetSetup_t2400315372_marshaled_pinvoke& marshaled)
{
	RenderBuffer_t3529837690_marshal_pinvoke_cleanup(marshaled.___depth_1);
	const uint32_t marshaled____colorLoad_4_CleanupLoopCount = 1;
	for (uint32_t i = 0; i < marshaled____colorLoad_4_CleanupLoopCount; i++)
	{
	}
	il2cpp_codegen_marshal_free(marshaled.___colorLoad_4);
	marshaled.___colorLoad_4 = NULL;
	const uint32_t marshaled____colorStore_5_CleanupLoopCount = 1;
	for (uint32_t i = 0; i < marshaled____colorStore_5_CleanupLoopCount; i++)
	{
	}
	il2cpp_codegen_marshal_free(marshaled.___colorStore_5);
	marshaled.___colorStore_5 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.RenderTargetSetup
extern "C" void RenderTargetSetup_t2400315372_marshal_com(const RenderTargetSetup_t2400315372& unmarshaled, RenderTargetSetup_t2400315372_marshaled_com& marshaled)
{
	marshaled.___color_0 = il2cpp_codegen_marshal_array<RenderBuffer_t3529837690_marshaled_com>((Il2CppCodeGenArray*)unmarshaled.get_color_0());
	RenderBuffer_t3529837690_marshal_com(unmarshaled.get_depth_1(), marshaled.___depth_1);
	marshaled.___mipLevel_2 = unmarshaled.get_mipLevel_2();
	marshaled.___cubemapFace_3 = unmarshaled.get_cubemapFace_3();
	uint32_t _unmarshaled_colorLoad_Length = 0;
	if (unmarshaled.get_colorLoad_4() != NULL)
	{
		_unmarshaled_colorLoad_Length = ((Il2CppCodeGenArray*)unmarshaled.get_colorLoad_4())->max_length;
		marshaled.___colorLoad_4 = il2cpp_codegen_marshal_allocate_array<int32_t>(_unmarshaled_colorLoad_Length);
	}
	for (uint32_t i = 0; i < _unmarshaled_colorLoad_Length; i++)
	{
		(marshaled.___colorLoad_4)[i] = (unmarshaled.get_colorLoad_4())->GetAt(static_cast<il2cpp_array_size_t>(i));
	}
	uint32_t _unmarshaled_colorStore_Length = 0;
	if (unmarshaled.get_colorStore_5() != NULL)
	{
		_unmarshaled_colorStore_Length = ((Il2CppCodeGenArray*)unmarshaled.get_colorStore_5())->max_length;
		marshaled.___colorStore_5 = il2cpp_codegen_marshal_allocate_array<int32_t>(_unmarshaled_colorStore_Length);
	}
	for (uint32_t i = 0; i < _unmarshaled_colorStore_Length; i++)
	{
		(marshaled.___colorStore_5)[i] = (unmarshaled.get_colorStore_5())->GetAt(static_cast<il2cpp_array_size_t>(i));
	}
	marshaled.___depthLoad_6 = unmarshaled.get_depthLoad_6();
	marshaled.___depthStore_7 = unmarshaled.get_depthStore_7();
}
extern Il2CppClass* RenderBuffer_t3529837690_il2cpp_TypeInfo_var;
extern Il2CppClass* RenderBufferLoadActionU5BU5D_t3840699671_il2cpp_TypeInfo_var;
extern Il2CppClass* RenderBufferStoreActionU5BU5D_t2354850566_il2cpp_TypeInfo_var;
extern const uint32_t RenderTargetSetup_t2400315372_com_FromNativeMethodDefinition_MetadataUsageId;
extern "C" void RenderTargetSetup_t2400315372_marshal_com_back(const RenderTargetSetup_t2400315372_marshaled_com& marshaled, RenderTargetSetup_t2400315372& unmarshaled)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RenderTargetSetup_t2400315372_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	unmarshaled.set_color_0((RenderBufferU5BU5D_t1970987103*)il2cpp_codegen_marshal_array_result(RenderBuffer_t3529837690_il2cpp_TypeInfo_var, marshaled.___color_0, 1));
	RenderBuffer_t3529837690  unmarshaled_depth_temp_1;
	memset(&unmarshaled_depth_temp_1, 0, sizeof(unmarshaled_depth_temp_1));
	RenderBuffer_t3529837690_marshal_com_back(marshaled.___depth_1, unmarshaled_depth_temp_1);
	unmarshaled.set_depth_1(unmarshaled_depth_temp_1);
	int32_t unmarshaled_mipLevel_temp_2 = 0;
	unmarshaled_mipLevel_temp_2 = marshaled.___mipLevel_2;
	unmarshaled.set_mipLevel_2(unmarshaled_mipLevel_temp_2);
	int32_t unmarshaled_cubemapFace_temp_3 = 0;
	unmarshaled_cubemapFace_temp_3 = marshaled.___cubemapFace_3;
	unmarshaled.set_cubemapFace_3(unmarshaled_cubemapFace_temp_3);
	if (marshaled.___colorLoad_4 != NULL)
	{
		uint32_t _loopCount;
		if (unmarshaled.get_colorLoad_4() == NULL)
		{
			_loopCount = 1;
			unmarshaled.set_colorLoad_4(reinterpret_cast<RenderBufferLoadActionU5BU5D_t3840699671*>(SZArrayNew(RenderBufferLoadActionU5BU5D_t3840699671_il2cpp_TypeInfo_var, _loopCount)));
		}
		else
		{
			_loopCount = ((Il2CppCodeGenArray*)unmarshaled.get_colorLoad_4())->max_length;
		}
		for (uint32_t i = 0; i < _loopCount; i++)
		{
			int32_t _item = 0;
			_item = (marshaled.___colorLoad_4)[i];
			(unmarshaled.get_colorLoad_4())->SetAt(static_cast<il2cpp_array_size_t>(i), _item);
		}
	}
	if (marshaled.___colorStore_5 != NULL)
	{
		uint32_t _loopCount;
		if (unmarshaled.get_colorStore_5() == NULL)
		{
			_loopCount = 1;
			unmarshaled.set_colorStore_5(reinterpret_cast<RenderBufferStoreActionU5BU5D_t2354850566*>(SZArrayNew(RenderBufferStoreActionU5BU5D_t2354850566_il2cpp_TypeInfo_var, _loopCount)));
		}
		else
		{
			_loopCount = ((Il2CppCodeGenArray*)unmarshaled.get_colorStore_5())->max_length;
		}
		for (uint32_t i = 0; i < _loopCount; i++)
		{
			int32_t _item = 0;
			_item = (marshaled.___colorStore_5)[i];
			(unmarshaled.get_colorStore_5())->SetAt(static_cast<il2cpp_array_size_t>(i), _item);
		}
	}
	int32_t unmarshaled_depthLoad_temp_6 = 0;
	unmarshaled_depthLoad_temp_6 = marshaled.___depthLoad_6;
	unmarshaled.set_depthLoad_6(unmarshaled_depthLoad_temp_6);
	int32_t unmarshaled_depthStore_temp_7 = 0;
	unmarshaled_depthStore_temp_7 = marshaled.___depthStore_7;
	unmarshaled.set_depthStore_7(unmarshaled_depthStore_temp_7);
}
// Conversion method for clean up from marshalling of: UnityEngine.RenderTargetSetup
extern "C" void RenderTargetSetup_t2400315372_marshal_com_cleanup(RenderTargetSetup_t2400315372_marshaled_com& marshaled)
{
	RenderBuffer_t3529837690_marshal_com_cleanup(marshaled.___depth_1);
	const uint32_t marshaled____colorLoad_4_CleanupLoopCount = 1;
	for (uint32_t i = 0; i < marshaled____colorLoad_4_CleanupLoopCount; i++)
	{
	}
	il2cpp_codegen_marshal_free(marshaled.___colorLoad_4);
	marshaled.___colorLoad_4 = NULL;
	const uint32_t marshaled____colorStore_5_CleanupLoopCount = 1;
	for (uint32_t i = 0; i < marshaled____colorStore_5_CleanupLoopCount; i++)
	{
	}
	il2cpp_codegen_marshal_free(marshaled.___colorStore_5);
	marshaled.___colorStore_5 = NULL;
}
// System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite)
extern "C"  void RenderTexture__ctor_m2961722047 (RenderTexture_t1963041563 * __this, int32_t ___width0, int32_t ___height1, int32_t ___depth2, int32_t ___format3, int32_t ___readWrite4, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Texture__ctor_m516856734(__this, /*hidden argument*/NULL);
		RenderTexture_Internal_CreateRenderTexture_m1444121965(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		RenderTexture_set_width_m2449777612(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___height1;
		RenderTexture_set_height_m4249514117(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___depth2;
		RenderTexture_set_depth_m68043273(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___format3;
		RenderTexture_set_format_m2539201833(__this, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___readWrite4;
		V_0 = (bool)((((int32_t)L_4) == ((int32_t)2))? 1 : 0);
		int32_t L_5 = ___readWrite4;
		if (L_5)
		{
			goto IL_003f;
		}
	}
	{
		int32_t L_6 = QualitySettings_get_activeColorSpace_m2993616266(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_6) == ((int32_t)1))? 1 : 0);
	}

IL_003f:
	{
		bool L_7 = V_0;
		RenderTexture_Internal_SetSRGBReadWrite_m2486231130(NULL /*static, unused*/, __this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat)
extern "C"  void RenderTexture__ctor_m2927948204 (RenderTexture_t1963041563 * __this, int32_t ___width0, int32_t ___height1, int32_t ___depth2, int32_t ___format3, const MethodInfo* method)
{
	{
		Texture__ctor_m516856734(__this, /*hidden argument*/NULL);
		RenderTexture_Internal_CreateRenderTexture_m1444121965(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		RenderTexture_set_width_m2449777612(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___height1;
		RenderTexture_set_height_m4249514117(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___depth2;
		RenderTexture_set_depth_m68043273(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___format3;
		RenderTexture_set_format_m2539201833(__this, L_3, /*hidden argument*/NULL);
		int32_t L_4 = QualitySettings_get_activeColorSpace_m2993616266(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_Internal_SetSRGBReadWrite_m2486231130(NULL /*static, unused*/, __this, (bool)((((int32_t)L_4) == ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32)
extern "C"  void RenderTexture__ctor_m591418693 (RenderTexture_t1963041563 * __this, int32_t ___width0, int32_t ___height1, int32_t ___depth2, const MethodInfo* method)
{
	{
		Texture__ctor_m516856734(__this, /*hidden argument*/NULL);
		RenderTexture_Internal_CreateRenderTexture_m1444121965(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		RenderTexture_set_width_m2449777612(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___height1;
		RenderTexture_set_height_m4249514117(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___depth2;
		RenderTexture_set_depth_m68043273(__this, L_2, /*hidden argument*/NULL);
		RenderTexture_set_format_m2539201833(__this, 7, /*hidden argument*/NULL);
		int32_t L_3 = QualitySettings_get_activeColorSpace_m2993616266(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_Internal_SetSRGBReadWrite_m2486231130(NULL /*static, unused*/, __this, (bool)((((int32_t)L_3) == ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_Internal_CreateRenderTexture_m1444121965 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___rt0, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_CreateRenderTexture_m1444121965_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_Internal_CreateRenderTexture_m1444121965_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_CreateRenderTexture_m1444121965_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___rt0);
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32)
extern "C"  RenderTexture_t1963041563 * RenderTexture_GetTemporary_m800385162 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, int32_t ___format3, int32_t ___readWrite4, int32_t ___antiAliasing5, const MethodInfo* method)
{
	typedef RenderTexture_t1963041563 * (*RenderTexture_GetTemporary_m800385162_ftn) (int32_t, int32_t, int32_t, int32_t, int32_t, int32_t);
	static RenderTexture_GetTemporary_m800385162_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_GetTemporary_m800385162_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32)");
	return _il2cpp_icall_func(___width0, ___height1, ___depthBuffer2, ___format3, ___readWrite4, ___antiAliasing5);
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite)
extern "C"  RenderTexture_t1963041563 * RenderTexture_GetTemporary_m2920164717 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, int32_t ___format3, int32_t ___readWrite4, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 1;
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___depthBuffer2;
		int32_t L_3 = ___format3;
		int32_t L_4 = ___readWrite4;
		int32_t L_5 = V_0;
		RenderTexture_t1963041563 * L_6 = RenderTexture_GetTemporary_m800385162(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat)
extern "C"  RenderTexture_t1963041563 * RenderTexture_GetTemporary_m1328055742 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, int32_t ___format3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 1;
		V_1 = 0;
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___depthBuffer2;
		int32_t L_3 = ___format3;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		RenderTexture_t1963041563 * L_6 = RenderTexture_GetTemporary_m800385162(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32)
extern "C"  RenderTexture_t1963041563 * RenderTexture_GetTemporary_m52998487 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 1;
		V_1 = 0;
		V_2 = 7;
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___depthBuffer2;
		int32_t L_3 = V_2;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		RenderTexture_t1963041563 * L_6 = RenderTexture_GetTemporary_m800385162(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32)
extern "C"  RenderTexture_t1963041563 * RenderTexture_GetTemporary_m469965696 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		V_0 = 1;
		V_1 = 0;
		V_2 = 7;
		V_3 = 0;
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = V_3;
		int32_t L_3 = V_2;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		RenderTexture_t1963041563 * L_6 = RenderTexture_GetTemporary_m800385162(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_ReleaseTemporary_m3045961066 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___temp0, const MethodInfo* method)
{
	typedef void (*RenderTexture_ReleaseTemporary_m3045961066_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_ReleaseTemporary_m3045961066_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_ReleaseTemporary_m3045961066_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___temp0);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetWidth_m1030655936 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___mono0, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetWidth_m1030655936_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_Internal_GetWidth_m1030655936_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetWidth_m1030655936_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono0);
}
// System.Void UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)
extern "C"  void RenderTexture_Internal_SetWidth_m3535686411 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___mono0, int32_t ___width1, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetWidth_m3535686411_ftn) (RenderTexture_t1963041563 *, int32_t);
	static RenderTexture_Internal_SetWidth_m3535686411_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetWidth_m3535686411_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)");
	_il2cpp_icall_func(___mono0, ___width1);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetHeight_m1940011033 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___mono0, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetHeight_m1940011033_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_Internal_GetHeight_m1940011033_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetHeight_m1940011033_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono0);
}
// System.Void UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)
extern "C"  void RenderTexture_Internal_SetHeight_m4218891754 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___mono0, int32_t ___width1, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetHeight_m4218891754_ftn) (RenderTexture_t1963041563 *, int32_t);
	static RenderTexture_Internal_SetHeight_m4218891754_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetHeight_m4218891754_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)");
	_il2cpp_icall_func(___mono0, ___width1);
}
// System.Void UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)
extern "C"  void RenderTexture_Internal_SetSRGBReadWrite_m2486231130 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___mono0, bool ___sRGB1, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetSRGBReadWrite_m2486231130_ftn) (RenderTexture_t1963041563 *, bool);
	static RenderTexture_Internal_SetSRGBReadWrite_m2486231130_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetSRGBReadWrite_m2486231130_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)");
	_il2cpp_icall_func(___mono0, ___sRGB1);
}
// System.Int32 UnityEngine.RenderTexture::get_width()
extern "C"  int32_t RenderTexture_get_width_m1498578543 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = RenderTexture_Internal_GetWidth_m1030655936(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.RenderTexture::set_width(System.Int32)
extern "C"  void RenderTexture_set_width_m2449777612 (RenderTexture_t1963041563 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		RenderTexture_Internal_SetWidth_m3535686411(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.RenderTexture::get_height()
extern "C"  int32_t RenderTexture_get_height_m4010076224 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = RenderTexture_Internal_GetHeight_m1940011033(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.RenderTexture::set_height(System.Int32)
extern "C"  void RenderTexture_set_height_m4249514117 (RenderTexture_t1963041563 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		RenderTexture_Internal_SetHeight_m4218891754(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.RenderTexture::get_depth()
extern "C"  int32_t RenderTexture_get_depth_m1712443436 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_get_depth_m1712443436_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_get_depth_m1712443436_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_get_depth_m1712443436_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::get_depth()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RenderTexture::set_depth(System.Int32)
extern "C"  void RenderTexture_set_depth_m68043273 (RenderTexture_t1963041563 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_depth_m68043273_ftn) (RenderTexture_t1963041563 *, int32_t);
	static RenderTexture_set_depth_m68043273_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_depth_m68043273_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_depth(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.RenderTexture::get_isPowerOfTwo()
extern "C"  bool RenderTexture_get_isPowerOfTwo_m604265325 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	typedef bool (*RenderTexture_get_isPowerOfTwo_m604265325_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_get_isPowerOfTwo_m604265325_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_get_isPowerOfTwo_m604265325_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::get_isPowerOfTwo()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RenderTexture::set_isPowerOfTwo(System.Boolean)
extern "C"  void RenderTexture_set_isPowerOfTwo_m2113768254 (RenderTexture_t1963041563 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_isPowerOfTwo_m2113768254_ftn) (RenderTexture_t1963041563 *, bool);
	static RenderTexture_set_isPowerOfTwo_m2113768254_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_isPowerOfTwo_m2113768254_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_isPowerOfTwo(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.RenderTexture::get_sRGB()
extern "C"  bool RenderTexture_get_sRGB_m3558052109 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	typedef bool (*RenderTexture_get_sRGB_m3558052109_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_get_sRGB_m3558052109_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_get_sRGB_m3558052109_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::get_sRGB()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.RenderTextureFormat UnityEngine.RenderTexture::get_format()
extern "C"  int32_t RenderTexture_get_format_m3502109954 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_get_format_m3502109954_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_get_format_m3502109954_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_get_format_m3502109954_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::get_format()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)
extern "C"  void RenderTexture_set_format_m2539201833 (RenderTexture_t1963041563 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_format_m2539201833_ftn) (RenderTexture_t1963041563 *, int32_t);
	static RenderTexture_set_format_m2539201833_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_format_m2539201833_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.RenderTexture::get_useMipMap()
extern "C"  bool RenderTexture_get_useMipMap_m2930306078 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	typedef bool (*RenderTexture_get_useMipMap_m2930306078_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_get_useMipMap_m2930306078_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_get_useMipMap_m2930306078_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::get_useMipMap()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RenderTexture::set_useMipMap(System.Boolean)
extern "C"  void RenderTexture_set_useMipMap_m623656507 (RenderTexture_t1963041563 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_useMipMap_m623656507_ftn) (RenderTexture_t1963041563 *, bool);
	static RenderTexture_set_useMipMap_m623656507_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_useMipMap_m623656507_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_useMipMap(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.RenderTexture::get_generateMips()
extern "C"  bool RenderTexture_get_generateMips_m2256992487 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	typedef bool (*RenderTexture_get_generateMips_m2256992487_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_get_generateMips_m2256992487_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_get_generateMips_m2256992487_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::get_generateMips()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RenderTexture::set_generateMips(System.Boolean)
extern "C"  void RenderTexture_set_generateMips_m3082423096 (RenderTexture_t1963041563 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_generateMips_m3082423096_ftn) (RenderTexture_t1963041563 *, bool);
	static RenderTexture_set_generateMips_m3082423096_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_generateMips_m3082423096_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_generateMips(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.RenderTexture::get_isCubemap()
extern "C"  bool RenderTexture_get_isCubemap_m3132805708 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	typedef bool (*RenderTexture_get_isCubemap_m3132805708_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_get_isCubemap_m3132805708_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_get_isCubemap_m3132805708_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::get_isCubemap()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RenderTexture::set_isCubemap(System.Boolean)
extern "C"  void RenderTexture_set_isCubemap_m2254486505 (RenderTexture_t1963041563 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_isCubemap_m2254486505_ftn) (RenderTexture_t1963041563 *, bool);
	static RenderTexture_set_isCubemap_m2254486505_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_isCubemap_m2254486505_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_isCubemap(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.RenderTexture::get_isVolume()
extern "C"  bool RenderTexture_get_isVolume_m4221813527 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	typedef bool (*RenderTexture_get_isVolume_m4221813527_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_get_isVolume_m4221813527_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_get_isVolume_m4221813527_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::get_isVolume()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RenderTexture::set_isVolume(System.Boolean)
extern "C"  void RenderTexture_set_isVolume_m3741310568 (RenderTexture_t1963041563 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_isVolume_m3741310568_ftn) (RenderTexture_t1963041563 *, bool);
	static RenderTexture_set_isVolume_m3741310568_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_isVolume_m3741310568_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_isVolume(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RenderTexture::get_volumeDepth()
extern "C"  int32_t RenderTexture_get_volumeDepth_m2632298930 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_get_volumeDepth_m2632298930_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_get_volumeDepth_m2632298930_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_get_volumeDepth_m2632298930_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::get_volumeDepth()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RenderTexture::set_volumeDepth(System.Int32)
extern "C"  void RenderTexture_set_volumeDepth_m2171072911 (RenderTexture_t1963041563 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_volumeDepth_m2171072911_ftn) (RenderTexture_t1963041563 *, int32_t);
	static RenderTexture_set_volumeDepth_m2171072911_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_volumeDepth_m2171072911_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_volumeDepth(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RenderTexture::get_antiAliasing()
extern "C"  int32_t RenderTexture_get_antiAliasing_m313135853 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_get_antiAliasing_m313135853_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_get_antiAliasing_m313135853_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_get_antiAliasing_m313135853_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::get_antiAliasing()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RenderTexture::set_antiAliasing(System.Int32)
extern "C"  void RenderTexture_set_antiAliasing_m3431921842 (RenderTexture_t1963041563 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_antiAliasing_m3431921842_ftn) (RenderTexture_t1963041563 *, int32_t);
	static RenderTexture_set_antiAliasing_m3431921842_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_antiAliasing_m3431921842_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_antiAliasing(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.RenderTexture::get_enableRandomWrite()
extern "C"  bool RenderTexture_get_enableRandomWrite_m724821032 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	typedef bool (*RenderTexture_get_enableRandomWrite_m724821032_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_get_enableRandomWrite_m724821032_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_get_enableRandomWrite_m724821032_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::get_enableRandomWrite()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RenderTexture::set_enableRandomWrite(System.Boolean)
extern "C"  void RenderTexture_set_enableRandomWrite_m3467040453 (RenderTexture_t1963041563 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_enableRandomWrite_m3467040453_ftn) (RenderTexture_t1963041563 *, bool);
	static RenderTexture_set_enableRandomWrite_m3467040453_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_enableRandomWrite_m3467040453_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_enableRandomWrite(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.RenderTexture::Create()
extern "C"  bool RenderTexture_Create_m703203206 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	{
		bool L_0 = RenderTexture_INTERNAL_CALL_Create_m2783419455(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.RenderTexture::INTERNAL_CALL_Create(UnityEngine.RenderTexture)
extern "C"  bool RenderTexture_INTERNAL_CALL_Create_m2783419455 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___self0, const MethodInfo* method)
{
	typedef bool (*RenderTexture_INTERNAL_CALL_Create_m2783419455_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_INTERNAL_CALL_Create_m2783419455_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_Create_m2783419455_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_Create(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.RenderTexture::Release()
extern "C"  void RenderTexture_Release_m2820287481 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	{
		RenderTexture_INTERNAL_CALL_Release_m2325488906(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::INTERNAL_CALL_Release(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_INTERNAL_CALL_Release_m2325488906 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___self0, const MethodInfo* method)
{
	typedef void (*RenderTexture_INTERNAL_CALL_Release_m2325488906_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_INTERNAL_CALL_Release_m2325488906_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_Release_m2325488906_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_Release(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.RenderTexture::IsCreated()
extern "C"  bool RenderTexture_IsCreated_m599737014 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	{
		bool L_0 = RenderTexture_INTERNAL_CALL_IsCreated_m885429165(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.RenderTexture::INTERNAL_CALL_IsCreated(UnityEngine.RenderTexture)
extern "C"  bool RenderTexture_INTERNAL_CALL_IsCreated_m885429165 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___self0, const MethodInfo* method)
{
	typedef bool (*RenderTexture_INTERNAL_CALL_IsCreated_m885429165_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_INTERNAL_CALL_IsCreated_m885429165_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_IsCreated_m885429165_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_IsCreated(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.RenderTexture::DiscardContents()
extern "C"  void RenderTexture_DiscardContents_m1898044554 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	{
		RenderTexture_INTERNAL_CALL_DiscardContents_m3754318489(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::INTERNAL_CALL_DiscardContents(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_INTERNAL_CALL_DiscardContents_m3754318489 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___self0, const MethodInfo* method)
{
	typedef void (*RenderTexture_INTERNAL_CALL_DiscardContents_m3754318489_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_INTERNAL_CALL_DiscardContents_m3754318489_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_DiscardContents_m3754318489_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_DiscardContents(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.RenderTexture::DiscardContents(System.Boolean,System.Boolean)
extern "C"  void RenderTexture_DiscardContents_m1003964636 (RenderTexture_t1963041563 * __this, bool ___discardColor0, bool ___discardDepth1, const MethodInfo* method)
{
	typedef void (*RenderTexture_DiscardContents_m1003964636_ftn) (RenderTexture_t1963041563 *, bool, bool);
	static RenderTexture_DiscardContents_m1003964636_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_DiscardContents_m1003964636_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::DiscardContents(System.Boolean,System.Boolean)");
	_il2cpp_icall_func(__this, ___discardColor0, ___discardDepth1);
}
// System.Void UnityEngine.RenderTexture::MarkRestoreExpected()
extern "C"  void RenderTexture_MarkRestoreExpected_m2220245707 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	{
		RenderTexture_INTERNAL_CALL_MarkRestoreExpected_m3308541944(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::INTERNAL_CALL_MarkRestoreExpected(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_INTERNAL_CALL_MarkRestoreExpected_m3308541944 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___self0, const MethodInfo* method)
{
	typedef void (*RenderTexture_INTERNAL_CALL_MarkRestoreExpected_m3308541944_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_INTERNAL_CALL_MarkRestoreExpected_m3308541944_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_MarkRestoreExpected_m3308541944_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_MarkRestoreExpected(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___self0);
}
// UnityEngine.RenderBuffer UnityEngine.RenderTexture::get_colorBuffer()
extern "C"  RenderBuffer_t3529837690  RenderTexture_get_colorBuffer_m1260375694 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	RenderBuffer_t3529837690  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RenderTexture_GetColorBuffer_m4145048604(__this, (&V_0), /*hidden argument*/NULL);
		RenderBuffer_t3529837690  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.RenderBuffer UnityEngine.RenderTexture::get_depthBuffer()
extern "C"  RenderBuffer_t3529837690  RenderTexture_get_depthBuffer_m990382574 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	RenderBuffer_t3529837690  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RenderTexture_GetDepthBuffer_m63945916(__this, (&V_0), /*hidden argument*/NULL);
		RenderBuffer_t3529837690  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RenderTexture::GetColorBuffer(UnityEngine.RenderBuffer&)
extern "C"  void RenderTexture_GetColorBuffer_m4145048604 (RenderTexture_t1963041563 * __this, RenderBuffer_t3529837690 * ___res0, const MethodInfo* method)
{
	typedef void (*RenderTexture_GetColorBuffer_m4145048604_ftn) (RenderTexture_t1963041563 *, RenderBuffer_t3529837690 *);
	static RenderTexture_GetColorBuffer_m4145048604_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_GetColorBuffer_m4145048604_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::GetColorBuffer(UnityEngine.RenderBuffer&)");
	_il2cpp_icall_func(__this, ___res0);
}
// System.Void UnityEngine.RenderTexture::GetDepthBuffer(UnityEngine.RenderBuffer&)
extern "C"  void RenderTexture_GetDepthBuffer_m63945916 (RenderTexture_t1963041563 * __this, RenderBuffer_t3529837690 * ___res0, const MethodInfo* method)
{
	typedef void (*RenderTexture_GetDepthBuffer_m63945916_ftn) (RenderTexture_t1963041563 *, RenderBuffer_t3529837690 *);
	static RenderTexture_GetDepthBuffer_m63945916_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_GetDepthBuffer_m63945916_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::GetDepthBuffer(UnityEngine.RenderBuffer&)");
	_il2cpp_icall_func(__this, ___res0);
}
// System.Void UnityEngine.RenderTexture::SetGlobalShaderProperty(System.String)
extern "C"  void RenderTexture_SetGlobalShaderProperty_m501884881 (RenderTexture_t1963041563 * __this, String_t* ___propertyName0, const MethodInfo* method)
{
	typedef void (*RenderTexture_SetGlobalShaderProperty_m501884881_ftn) (RenderTexture_t1963041563 *, String_t*);
	static RenderTexture_SetGlobalShaderProperty_m501884881_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_SetGlobalShaderProperty_m501884881_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::SetGlobalShaderProperty(System.String)");
	_il2cpp_icall_func(__this, ___propertyName0);
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::get_active()
extern "C"  RenderTexture_t1963041563 * RenderTexture_get_active_m1725644858 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef RenderTexture_t1963041563 * (*RenderTexture_get_active_m1725644858_ftn) ();
	static RenderTexture_get_active_m1725644858_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_get_active_m1725644858_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::get_active()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_set_active_m1002947377 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_active_m1002947377_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_set_active_m1002947377_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_active_m1002947377_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderTexture::Internal_GetTexelOffset(UnityEngine.RenderTexture,UnityEngine.Vector2&)
extern "C"  void RenderTexture_Internal_GetTexelOffset_m3174753363 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___tex0, Vector2_t4282066565 * ___output1, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_GetTexelOffset_m3174753363_ftn) (RenderTexture_t1963041563 *, Vector2_t4282066565 *);
	static RenderTexture_Internal_GetTexelOffset_m3174753363_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetTexelOffset_m3174753363_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetTexelOffset(UnityEngine.RenderTexture,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___tex0, ___output1);
}
// UnityEngine.Vector2 UnityEngine.RenderTexture::GetTexelOffset()
extern "C"  Vector2_t4282066565  RenderTexture_GetTexelOffset_m101165888 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RenderTexture_Internal_GetTexelOffset_m3174753363(NULL /*static, unused*/, __this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.RenderTexture::SupportsStencil(UnityEngine.RenderTexture)
extern "C"  bool RenderTexture_SupportsStencil_m1724553716 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___rt0, const MethodInfo* method)
{
	typedef bool (*RenderTexture_SupportsStencil_m1724553716_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_SupportsStencil_m1724553716_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_SupportsStencil_m1724553716_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::SupportsStencil(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___rt0);
}
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern "C"  void RequireComponent__ctor_m2023271172 (RequireComponent_t1687166108 * __this, Type_t * ___requiredComponent0, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___requiredComponent0;
		__this->set_m_Type0_0(L_0);
		return;
	}
}
// System.Int32 UnityEngine.Resolution::get_width()
extern "C"  int32_t Resolution_get_width_m3522089436 (Resolution_t1578306928 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Width_0();
		return L_0;
	}
}
extern "C"  int32_t Resolution_get_width_m3522089436_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Resolution_t1578306928 * _thisAdjusted = reinterpret_cast<Resolution_t1578306928 *>(__this + 1);
	return Resolution_get_width_m3522089436(_thisAdjusted, method);
}
// System.Void UnityEngine.Resolution::set_width(System.Int32)
extern "C"  void Resolution_set_width_m2525727929 (Resolution_t1578306928 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_Width_0(L_0);
		return;
	}
}
extern "C"  void Resolution_set_width_m2525727929_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Resolution_t1578306928 * _thisAdjusted = reinterpret_cast<Resolution_t1578306928 *>(__this + 1);
	Resolution_set_width_m2525727929(_thisAdjusted, ___value0, method);
}
// System.Int32 UnityEngine.Resolution::get_height()
extern "C"  int32_t Resolution_get_height_m2314404467 (Resolution_t1578306928 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Height_1();
		return L_0;
	}
}
extern "C"  int32_t Resolution_get_height_m2314404467_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Resolution_t1578306928 * _thisAdjusted = reinterpret_cast<Resolution_t1578306928 *>(__this + 1);
	return Resolution_get_height_m2314404467(_thisAdjusted, method);
}
// System.Void UnityEngine.Resolution::set_height(System.Int32)
extern "C"  void Resolution_set_height_m2309006648 (Resolution_t1578306928 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_Height_1(L_0);
		return;
	}
}
extern "C"  void Resolution_set_height_m2309006648_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Resolution_t1578306928 * _thisAdjusted = reinterpret_cast<Resolution_t1578306928 *>(__this + 1);
	Resolution_set_height_m2309006648(_thisAdjusted, ___value0, method);
}
// System.Int32 UnityEngine.Resolution::get_refreshRate()
extern "C"  int32_t Resolution_get_refreshRate_m1440202097 (Resolution_t1578306928 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_RefreshRate_2();
		return L_0;
	}
}
extern "C"  int32_t Resolution_get_refreshRate_m1440202097_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Resolution_t1578306928 * _thisAdjusted = reinterpret_cast<Resolution_t1578306928 *>(__this + 1);
	return Resolution_get_refreshRate_m1440202097(_thisAdjusted, method);
}
// System.Void UnityEngine.Resolution::set_refreshRate(System.Int32)
extern "C"  void Resolution_set_refreshRate_m3261589454 (Resolution_t1578306928 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_RefreshRate_2(L_0);
		return;
	}
}
extern "C"  void Resolution_set_refreshRate_m3261589454_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Resolution_t1578306928 * _thisAdjusted = reinterpret_cast<Resolution_t1578306928 *>(__this + 1);
	Resolution_set_refreshRate_m3261589454(_thisAdjusted, ___value0, method);
}
// System.String UnityEngine.Resolution::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral284898143;
extern const uint32_t Resolution_ToString_m139257714_MetadataUsageId;
extern "C"  String_t* Resolution_ToString_m139257714 (Resolution_t1578306928 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Resolution_ToString_m139257714_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)3));
		int32_t L_1 = __this->get_m_Width_0();
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		int32_t L_5 = __this->get_m_Height_1();
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		int32_t L_9 = __this->get_m_RefreshRate_2();
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		String_t* L_12 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral284898143, L_8, /*hidden argument*/NULL);
		return L_12;
	}
}
extern "C"  String_t* Resolution_ToString_m139257714_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Resolution_t1578306928 * _thisAdjusted = reinterpret_cast<Resolution_t1578306928 *>(__this + 1);
	return Resolution_ToString_m139257714(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.Resolution
extern "C" void Resolution_t1578306928_marshal_pinvoke(const Resolution_t1578306928& unmarshaled, Resolution_t1578306928_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Width_0 = unmarshaled.get_m_Width_0();
	marshaled.___m_Height_1 = unmarshaled.get_m_Height_1();
	marshaled.___m_RefreshRate_2 = unmarshaled.get_m_RefreshRate_2();
}
extern "C" void Resolution_t1578306928_marshal_pinvoke_back(const Resolution_t1578306928_marshaled_pinvoke& marshaled, Resolution_t1578306928& unmarshaled)
{
	int32_t unmarshaled_m_Width_temp_0 = 0;
	unmarshaled_m_Width_temp_0 = marshaled.___m_Width_0;
	unmarshaled.set_m_Width_0(unmarshaled_m_Width_temp_0);
	int32_t unmarshaled_m_Height_temp_1 = 0;
	unmarshaled_m_Height_temp_1 = marshaled.___m_Height_1;
	unmarshaled.set_m_Height_1(unmarshaled_m_Height_temp_1);
	int32_t unmarshaled_m_RefreshRate_temp_2 = 0;
	unmarshaled_m_RefreshRate_temp_2 = marshaled.___m_RefreshRate_2;
	unmarshaled.set_m_RefreshRate_2(unmarshaled_m_RefreshRate_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.Resolution
extern "C" void Resolution_t1578306928_marshal_pinvoke_cleanup(Resolution_t1578306928_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Resolution
extern "C" void Resolution_t1578306928_marshal_com(const Resolution_t1578306928& unmarshaled, Resolution_t1578306928_marshaled_com& marshaled)
{
	marshaled.___m_Width_0 = unmarshaled.get_m_Width_0();
	marshaled.___m_Height_1 = unmarshaled.get_m_Height_1();
	marshaled.___m_RefreshRate_2 = unmarshaled.get_m_RefreshRate_2();
}
extern "C" void Resolution_t1578306928_marshal_com_back(const Resolution_t1578306928_marshaled_com& marshaled, Resolution_t1578306928& unmarshaled)
{
	int32_t unmarshaled_m_Width_temp_0 = 0;
	unmarshaled_m_Width_temp_0 = marshaled.___m_Width_0;
	unmarshaled.set_m_Width_0(unmarshaled_m_Width_temp_0);
	int32_t unmarshaled_m_Height_temp_1 = 0;
	unmarshaled_m_Height_temp_1 = marshaled.___m_Height_1;
	unmarshaled.set_m_Height_1(unmarshaled_m_Height_temp_1);
	int32_t unmarshaled_m_RefreshRate_temp_2 = 0;
	unmarshaled_m_RefreshRate_temp_2 = marshaled.___m_RefreshRate_2;
	unmarshaled.set_m_RefreshRate_2(unmarshaled_m_RefreshRate_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.Resolution
extern "C" void Resolution_t1578306928_marshal_com_cleanup(Resolution_t1578306928_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ResourceRequest::.ctor()
extern "C"  void ResourceRequest__ctor_m2863879896 (ResourceRequest_t3731857623 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m162101250(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern "C"  Object_t3071478659 * ResourceRequest_get_asset_m670320982 (ResourceRequest_t3731857623 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_Path_1();
		Type_t * L_1 = __this->get_m_Type_2();
		Object_t3071478659 * L_2 = Resources_Load_m3601699608(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t3731857623_marshal_pinvoke(const ResourceRequest_t3731857623& unmarshaled, ResourceRequest_t3731857623_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
extern "C" void ResourceRequest_t3731857623_marshal_pinvoke_back(const ResourceRequest_t3731857623_marshaled_pinvoke& marshaled, ResourceRequest_t3731857623& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t3731857623_marshal_pinvoke_cleanup(ResourceRequest_t3731857623_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t3731857623_marshal_com(const ResourceRequest_t3731857623& unmarshaled, ResourceRequest_t3731857623_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
extern "C" void ResourceRequest_t3731857623_marshal_com_back(const ResourceRequest_t3731857623_marshaled_com& marshaled, ResourceRequest_t3731857623& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t3731857623_marshal_com_cleanup(ResourceRequest_t3731857623_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Resources::.ctor()
extern "C"  void Resources__ctor_m262479956 (Resources_t2918352667 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Resources::FindObjectsOfTypeAll(System.Type)
extern "C"  ObjectU5BU5D_t1015136018* Resources_FindObjectsOfTypeAll_m1346325355 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t1015136018* (*Resources_FindObjectsOfTypeAll_m1346325355_ftn) (Type_t *);
	static Resources_FindObjectsOfTypeAll_m1346325355_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_FindObjectsOfTypeAll_m1346325355_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::FindObjectsOfTypeAll(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern const Il2CppType* Object_t3071478659_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Resources_Load_m2187391845_MetadataUsageId;
extern "C"  Object_t3071478659 * Resources_Load_m2187391845 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Resources_Load_m2187391845_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Object_t3071478659_0_0_0_var), /*hidden argument*/NULL);
		Object_t3071478659 * L_2 = Resources_Load_m3601699608(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C"  Object_t3071478659 * Resources_Load_m3601699608 (Il2CppObject * __this /* static, unused */, String_t* ___path0, Type_t * ___systemTypeInstance1, const MethodInfo* method)
{
	typedef Object_t3071478659 * (*Resources_Load_m3601699608_ftn) (String_t*, Type_t *);
	static Resources_Load_m3601699608_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_Load_m3601699608_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::Load(System.String,System.Type)");
	return _il2cpp_icall_func(___path0, ___systemTypeInstance1);
}
// UnityEngine.ResourceRequest UnityEngine.Resources::LoadAsync(System.String)
extern const Il2CppType* Object_t3071478659_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Resources_LoadAsync_m486232049_MetadataUsageId;
extern "C"  ResourceRequest_t3731857623 * Resources_LoadAsync_m486232049 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Resources_LoadAsync_m486232049_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Object_t3071478659_0_0_0_var), /*hidden argument*/NULL);
		ResourceRequest_t3731857623 * L_2 = Resources_LoadAsync_m3762650532(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.ResourceRequest UnityEngine.Resources::LoadAsync(System.String,System.Type)
extern "C"  ResourceRequest_t3731857623 * Resources_LoadAsync_m3762650532 (Il2CppObject * __this /* static, unused */, String_t* ___path0, Type_t * ___type1, const MethodInfo* method)
{
	typedef ResourceRequest_t3731857623 * (*Resources_LoadAsync_m3762650532_ftn) (String_t*, Type_t *);
	static Resources_LoadAsync_m3762650532_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_LoadAsync_m3762650532_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::LoadAsync(System.String,System.Type)");
	return _il2cpp_icall_func(___path0, ___type1);
}
// UnityEngine.Object[] UnityEngine.Resources::LoadAll(System.String,System.Type)
extern "C"  ObjectU5BU5D_t1015136018* Resources_LoadAll_m2472905169 (Il2CppObject * __this /* static, unused */, String_t* ___path0, Type_t * ___systemTypeInstance1, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t1015136018* (*Resources_LoadAll_m2472905169_ftn) (String_t*, Type_t *);
	static Resources_LoadAll_m2472905169_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_LoadAll_m2472905169_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::LoadAll(System.String,System.Type)");
	return _il2cpp_icall_func(___path0, ___systemTypeInstance1);
}
// UnityEngine.Object[] UnityEngine.Resources::LoadAll(System.String)
extern const Il2CppType* Object_t3071478659_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Resources_LoadAll_m2517792670_MetadataUsageId;
extern "C"  ObjectU5BU5D_t1015136018* Resources_LoadAll_m2517792670 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Resources_LoadAll_m2517792670_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Object_t3071478659_0_0_0_var), /*hidden argument*/NULL);
		ObjectU5BU5D_t1015136018* L_2 = Resources_LoadAll_m2472905169(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)
extern "C"  Object_t3071478659 * Resources_GetBuiltinResource_m1250078467 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, String_t* ___path1, const MethodInfo* method)
{
	typedef Object_t3071478659 * (*Resources_GetBuiltinResource_m1250078467_ftn) (Type_t *, String_t*);
	static Resources_GetBuiltinResource_m1250078467_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_GetBuiltinResource_m1250078467_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)");
	return _il2cpp_icall_func(___type0, ___path1);
}
// System.Void UnityEngine.Resources::UnloadAsset(UnityEngine.Object)
extern "C"  void Resources_UnloadAsset_m3142802605 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___assetToUnload0, const MethodInfo* method)
{
	typedef void (*Resources_UnloadAsset_m3142802605_ftn) (Object_t3071478659 *);
	static Resources_UnloadAsset_m3142802605_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_UnloadAsset_m3142802605_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::UnloadAsset(UnityEngine.Object)");
	_il2cpp_icall_func(___assetToUnload0);
}
// UnityEngine.AsyncOperation UnityEngine.Resources::UnloadUnusedAssets()
extern "C"  AsyncOperation_t3699081103 * Resources_UnloadUnusedAssets_m3831138427 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef AsyncOperation_t3699081103 * (*Resources_UnloadUnusedAssets_m3831138427_ftn) ();
	static Resources_UnloadUnusedAssets_m3831138427_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_UnloadUnusedAssets_m3831138427_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::UnloadUnusedAssets()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Rigidbody::.ctor()
extern "C"  void Rigidbody__ctor_m1032756524 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	{
		Component__ctor_m4238603388(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
extern "C"  Vector3_t4282066566  Rigidbody_get_velocity_m2696244068 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody_INTERNAL_get_velocity_m1063590501(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
extern "C"  void Rigidbody_set_velocity_m799562119 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_velocity_m484592601(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_velocity_m1063590501 (Rigidbody_t3346577219 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_velocity_m1063590501_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_get_velocity_m1063590501_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_velocity_m1063590501_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_set_velocity_m484592601 (Rigidbody_t3346577219 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_velocity_m484592601_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_set_velocity_m484592601_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_velocity_m484592601_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_angularVelocity()
extern "C"  Vector3_t4282066566  Rigidbody_get_angularVelocity_m1572208282 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody_INTERNAL_get_angularVelocity_m3705279231(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::set_angularVelocity(UnityEngine.Vector3)
extern "C"  void Rigidbody_set_angularVelocity_m3510265645 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_angularVelocity_m1952192267(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_angularVelocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_angularVelocity_m3705279231 (Rigidbody_t3346577219 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_angularVelocity_m3705279231_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_get_angularVelocity_m3705279231_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_angularVelocity_m3705279231_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_angularVelocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_angularVelocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_set_angularVelocity_m1952192267 (Rigidbody_t3346577219 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_angularVelocity_m1952192267_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_set_angularVelocity_m1952192267_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_angularVelocity_m1952192267_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_angularVelocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody::get_drag()
extern "C"  float Rigidbody_get_drag_m3710595753 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody_get_drag_m3710595753_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_get_drag_m3710595753_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_drag_m3710595753_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_drag()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_drag(System.Single)
extern "C"  void Rigidbody_set_drag_m4061586082 (Rigidbody_t3346577219 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_drag_m4061586082_ftn) (Rigidbody_t3346577219 *, float);
	static Rigidbody_set_drag_m4061586082_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_drag_m4061586082_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_drag(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody::get_angularDrag()
extern "C"  float Rigidbody_get_angularDrag_m2925737411 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody_get_angularDrag_m2925737411_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_get_angularDrag_m2925737411_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_angularDrag_m2925737411_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_angularDrag()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_angularDrag(System.Single)
extern "C"  void Rigidbody_set_angularDrag_m2909317064 (Rigidbody_t3346577219 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_angularDrag_m2909317064_ftn) (Rigidbody_t3346577219 *, float);
	static Rigidbody_set_angularDrag_m2909317064_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_angularDrag_m2909317064_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_angularDrag(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody::get_mass()
extern "C"  float Rigidbody_get_mass_m3953106025 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody_get_mass_m3953106025_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_get_mass_m3953106025_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_mass_m3953106025_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_mass()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_mass(System.Single)
extern "C"  void Rigidbody_set_mass_m1579962594 (Rigidbody_t3346577219 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_mass_m1579962594_ftn) (Rigidbody_t3346577219 *, float);
	static Rigidbody_set_mass_m1579962594_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_mass_m1579962594_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_mass(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::SetDensity(System.Single)
extern "C"  void Rigidbody_SetDensity_m1920963693 (Rigidbody_t3346577219 * __this, float ___density0, const MethodInfo* method)
{
	{
		float L_0 = ___density0;
		Rigidbody_INTERNAL_CALL_SetDensity_m613574724(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_SetDensity(UnityEngine.Rigidbody,System.Single)
extern "C"  void Rigidbody_INTERNAL_CALL_SetDensity_m613574724 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, float ___density1, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_SetDensity_m613574724_ftn) (Rigidbody_t3346577219 *, float);
	static Rigidbody_INTERNAL_CALL_SetDensity_m613574724_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_SetDensity_m613574724_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_SetDensity(UnityEngine.Rigidbody,System.Single)");
	_il2cpp_icall_func(___self0, ___density1);
}
// System.Boolean UnityEngine.Rigidbody::get_useGravity()
extern "C"  bool Rigidbody_get_useGravity_m746424162 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	typedef bool (*Rigidbody_get_useGravity_m746424162_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_get_useGravity_m746424162_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_useGravity_m746424162_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_useGravity()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_useGravity(System.Boolean)
extern "C"  void Rigidbody_set_useGravity_m2620827635 (Rigidbody_t3346577219 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_useGravity_m2620827635_ftn) (Rigidbody_t3346577219 *, bool);
	static Rigidbody_set_useGravity_m2620827635_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_useGravity_m2620827635_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_useGravity(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody::get_maxDepenetrationVelocity()
extern "C"  float Rigidbody_get_maxDepenetrationVelocity_m4182760408 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody_get_maxDepenetrationVelocity_m4182760408_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_get_maxDepenetrationVelocity_m4182760408_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_maxDepenetrationVelocity_m4182760408_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_maxDepenetrationVelocity()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_maxDepenetrationVelocity(System.Single)
extern "C"  void Rigidbody_set_maxDepenetrationVelocity_m2217631187 (Rigidbody_t3346577219 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_maxDepenetrationVelocity_m2217631187_ftn) (Rigidbody_t3346577219 *, float);
	static Rigidbody_set_maxDepenetrationVelocity_m2217631187_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_maxDepenetrationVelocity_m2217631187_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_maxDepenetrationVelocity(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody::get_isKinematic()
extern "C"  bool Rigidbody_get_isKinematic_m3963857442 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	typedef bool (*Rigidbody_get_isKinematic_m3963857442_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_get_isKinematic_m3963857442_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_isKinematic_m3963857442_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_isKinematic()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean)
extern "C"  void Rigidbody_set_isKinematic_m294703295 (Rigidbody_t3346577219 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_isKinematic_m294703295_ftn) (Rigidbody_t3346577219 *, bool);
	static Rigidbody_set_isKinematic_m294703295_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_isKinematic_m294703295_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_isKinematic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody::get_freezeRotation()
extern "C"  bool Rigidbody_get_freezeRotation_m3153444688 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	typedef bool (*Rigidbody_get_freezeRotation_m3153444688_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_get_freezeRotation_m3153444688_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_freezeRotation_m3153444688_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_freezeRotation()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)
extern "C"  void Rigidbody_set_freezeRotation_m3989473889 (Rigidbody_t3346577219 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_freezeRotation_m3989473889_ftn) (Rigidbody_t3346577219 *, bool);
	static Rigidbody_set_freezeRotation_m3989473889_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_freezeRotation_m3989473889_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.RigidbodyConstraints UnityEngine.Rigidbody::get_constraints()
extern "C"  int32_t Rigidbody_get_constraints_m3809531910 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	typedef int32_t (*Rigidbody_get_constraints_m3809531910_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_get_constraints_m3809531910_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_constraints_m3809531910_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_constraints()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_constraints(UnityEngine.RigidbodyConstraints)
extern "C"  void Rigidbody_set_constraints_m3626026339 (Rigidbody_t3346577219 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_constraints_m3626026339_ftn) (Rigidbody_t3346577219 *, int32_t);
	static Rigidbody_set_constraints_m3626026339_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_constraints_m3626026339_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_constraints(UnityEngine.RigidbodyConstraints)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.CollisionDetectionMode UnityEngine.Rigidbody::get_collisionDetectionMode()
extern "C"  int32_t Rigidbody_get_collisionDetectionMode_m1955568853 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	typedef int32_t (*Rigidbody_get_collisionDetectionMode_m1955568853_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_get_collisionDetectionMode_m1955568853_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_collisionDetectionMode_m1955568853_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_collisionDetectionMode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_collisionDetectionMode(UnityEngine.CollisionDetectionMode)
extern "C"  void Rigidbody_set_collisionDetectionMode_m2364259870 (Rigidbody_t3346577219 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_collisionDetectionMode_m2364259870_ftn) (Rigidbody_t3346577219 *, int32_t);
	static Rigidbody_set_collisionDetectionMode_m2364259870_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_collisionDetectionMode_m2364259870_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_collisionDetectionMode(UnityEngine.CollisionDetectionMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddForce_m557267180 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___force0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_INTERNAL_CALL_AddForce_m3651654387(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3)
extern "C"  void Rigidbody_AddForce_m3682301239 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___force0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody_INTERNAL_CALL_AddForce_m3651654387(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddForce_m3651654387 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___force1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddForce_m3651654387_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddForce_m3651654387_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddForce_m3651654387_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___force1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddForce(System.Single,System.Single,System.Single)
extern "C"  void Rigidbody_AddForce_m3268382483 (Rigidbody_t3346577219 * __this, float ___x0, float ___y1, float ___z2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		int32_t L_3 = V_0;
		Rigidbody_AddForce_m2887927240(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddForce(System.Single,System.Single,System.Single,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddForce_m2887927240 (Rigidbody_t3346577219 * __this, float ___x0, float ___y1, float ___z2, int32_t ___mode3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		Vector3_t4282066566  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2926210380(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___mode3;
		Rigidbody_AddForce_m557267180(__this, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddRelativeForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddRelativeForce_m2803598808 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___force0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_INTERNAL_CALL_AddRelativeForce_m3589442951(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddRelativeForce(UnityEngine.Vector3)
extern "C"  void Rigidbody_AddRelativeForce_m1706572579 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___force0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody_INTERNAL_CALL_AddRelativeForce_m3589442951(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddRelativeForce_m3589442951 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___force1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddRelativeForce_m3589442951_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddRelativeForce_m3589442951_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddRelativeForce_m3589442951_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___force1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddRelativeForce(System.Single,System.Single,System.Single)
extern "C"  void Rigidbody_AddRelativeForce_m1219746815 (Rigidbody_t3346577219 * __this, float ___x0, float ___y1, float ___z2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		int32_t L_3 = V_0;
		Rigidbody_AddRelativeForce_m238999988(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddRelativeForce(System.Single,System.Single,System.Single,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddRelativeForce_m238999988 (Rigidbody_t3346577219 * __this, float ___x0, float ___y1, float ___z2, int32_t ___mode3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		Vector3_t4282066566  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2926210380(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___mode3;
		Rigidbody_AddRelativeForce_m2803598808(__this, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddTorque_m3009708185 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___torque0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_INTERNAL_CALL_AddTorque_m4194065160(NULL /*static, unused*/, __this, (&___torque0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3)
extern "C"  void Rigidbody_AddTorque_m3012148388 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___torque0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody_INTERNAL_CALL_AddTorque_m4194065160(NULL /*static, unused*/, __this, (&___torque0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddTorque_m4194065160 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___torque1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddTorque_m4194065160_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddTorque_m4194065160_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddTorque_m4194065160_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___torque1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddTorque(System.Single,System.Single,System.Single)
extern "C"  void Rigidbody_AddTorque_m1425856192 (Rigidbody_t3346577219 * __this, float ___x0, float ___y1, float ___z2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		int32_t L_3 = V_0;
		Rigidbody_AddTorque_m2476006837(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddTorque(System.Single,System.Single,System.Single,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddTorque_m2476006837 (Rigidbody_t3346577219 * __this, float ___x0, float ___y1, float ___z2, int32_t ___mode3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		Vector3_t4282066566  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2926210380(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___mode3;
		Rigidbody_AddTorque_m3009708185(__this, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddRelativeTorque(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddRelativeTorque_m3926511917 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___torque0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_INTERNAL_CALL_AddRelativeTorque_m2265510644(NULL /*static, unused*/, __this, (&___torque0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddRelativeTorque(UnityEngine.Vector3)
extern "C"  void Rigidbody_AddRelativeTorque_m1894102072 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___torque0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody_INTERNAL_CALL_AddRelativeTorque_m2265510644(NULL /*static, unused*/, __this, (&___torque0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddRelativeTorque_m2265510644 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___torque1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddRelativeTorque_m2265510644_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddRelativeTorque_m2265510644_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddRelativeTorque_m2265510644_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___torque1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddRelativeTorque(System.Single,System.Single,System.Single)
extern "C"  void Rigidbody_AddRelativeTorque_m2342659924 (Rigidbody_t3346577219 * __this, float ___x0, float ___y1, float ___z2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		int32_t L_3 = V_0;
		Rigidbody_AddRelativeTorque_m1963640649(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddRelativeTorque(System.Single,System.Single,System.Single,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddRelativeTorque_m1963640649 (Rigidbody_t3346577219 * __this, float ___x0, float ___y1, float ___z2, int32_t ___mode3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		Vector3_t4282066566  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2926210380(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___mode3;
		Rigidbody_AddRelativeTorque_m3926511917(__this, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddForceAtPosition(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddForceAtPosition_m1266619363 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___force0, Vector3_t4282066566  ___position1, int32_t ___mode2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode2;
		Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1493622644(NULL /*static, unused*/, __this, (&___force0), (&___position1), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddForceAtPosition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Rigidbody_AddForceAtPosition_m1572987758 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___force0, Vector3_t4282066566  ___position1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1493622644(NULL /*static, unused*/, __this, (&___force0), (&___position1), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1493622644 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___force1, Vector3_t4282066566 * ___position2, int32_t ___mode3, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1493622644_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *, Vector3_t4282066566 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1493622644_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1493622644_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___force1, ___position2, ___mode3);
}
// System.Void UnityEngine.Rigidbody::AddExplosionForce(System.Single,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddExplosionForce_m196999228 (Rigidbody_t3346577219 * __this, float ___explosionForce0, Vector3_t4282066566  ___explosionPosition1, float ___explosionRadius2, float ___upwardsModifier3, int32_t ___mode4, const MethodInfo* method)
{
	{
		float L_0 = ___explosionForce0;
		float L_1 = ___explosionRadius2;
		float L_2 = ___upwardsModifier3;
		int32_t L_3 = ___mode4;
		Rigidbody_INTERNAL_CALL_AddExplosionForce_m3109367769(NULL /*static, unused*/, __this, L_0, (&___explosionPosition1), L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddExplosionForce(System.Single,UnityEngine.Vector3,System.Single,System.Single)
extern "C"  void Rigidbody_AddExplosionForce_m2669187207 (Rigidbody_t3346577219 * __this, float ___explosionForce0, Vector3_t4282066566  ___explosionPosition1, float ___explosionRadius2, float ___upwardsModifier3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		float L_0 = ___explosionForce0;
		float L_1 = ___explosionRadius2;
		float L_2 = ___upwardsModifier3;
		int32_t L_3 = V_0;
		Rigidbody_INTERNAL_CALL_AddExplosionForce_m3109367769(NULL /*static, unused*/, __this, L_0, (&___explosionPosition1), L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddExplosionForce(System.Single,UnityEngine.Vector3,System.Single)
extern "C"  void Rigidbody_AddExplosionForce_m2719328546 (Rigidbody_t3346577219 * __this, float ___explosionForce0, Vector3_t4282066566  ___explosionPosition1, float ___explosionRadius2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		V_0 = 0;
		V_1 = (0.0f);
		float L_0 = ___explosionForce0;
		float L_1 = ___explosionRadius2;
		float L_2 = V_1;
		int32_t L_3 = V_0;
		Rigidbody_INTERNAL_CALL_AddExplosionForce_m3109367769(NULL /*static, unused*/, __this, L_0, (&___explosionPosition1), L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddExplosionForce(UnityEngine.Rigidbody,System.Single,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddExplosionForce_m3109367769 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, float ___explosionForce1, Vector3_t4282066566 * ___explosionPosition2, float ___explosionRadius3, float ___upwardsModifier4, int32_t ___mode5, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddExplosionForce_m3109367769_ftn) (Rigidbody_t3346577219 *, float, Vector3_t4282066566 *, float, float, int32_t);
	static Rigidbody_INTERNAL_CALL_AddExplosionForce_m3109367769_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddExplosionForce_m3109367769_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddExplosionForce(UnityEngine.Rigidbody,System.Single,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___explosionForce1, ___explosionPosition2, ___explosionRadius3, ___upwardsModifier4, ___mode5);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::ClosestPointOnBounds(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Rigidbody_ClosestPointOnBounds_m1462101584 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody_INTERNAL_CALL_ClosestPointOnBounds_m231853946(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_ClosestPointOnBounds(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_CALL_ClosestPointOnBounds_m231853946 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___position1, Vector3_t4282066566 * ___value2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_ClosestPointOnBounds_m231853946_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_CALL_ClosestPointOnBounds_m231853946_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_ClosestPointOnBounds_m231853946_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_ClosestPointOnBounds(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::GetRelativePointVelocity(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Rigidbody_GetRelativePointVelocity_m3122993328 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___relativePoint0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody_INTERNAL_CALL_GetRelativePointVelocity_m295069274(NULL /*static, unused*/, __this, (&___relativePoint0), (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_GetRelativePointVelocity(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_CALL_GetRelativePointVelocity_m295069274 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___relativePoint1, Vector3_t4282066566 * ___value2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_GetRelativePointVelocity_m295069274_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_CALL_GetRelativePointVelocity_m295069274_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_GetRelativePointVelocity_m295069274_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_GetRelativePointVelocity(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___relativePoint1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::GetPointVelocity(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Rigidbody_GetPointVelocity_m574053828 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___worldPoint0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody_INTERNAL_CALL_GetPointVelocity_m3199677550(NULL /*static, unused*/, __this, (&___worldPoint0), (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_GetPointVelocity(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_CALL_GetPointVelocity_m3199677550 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___worldPoint1, Vector3_t4282066566 * ___value2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_GetPointVelocity_m3199677550_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_CALL_GetPointVelocity_m3199677550_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_GetPointVelocity_m3199677550_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_GetPointVelocity(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___worldPoint1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_centerOfMass()
extern "C"  Vector3_t4282066566  Rigidbody_get_centerOfMass_m3434430695 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody_INTERNAL_get_centerOfMass_m3640748392(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::set_centerOfMass(UnityEngine.Vector3)
extern "C"  void Rigidbody_set_centerOfMass_m760020516 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_centerOfMass_m1759559900(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_centerOfMass(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_centerOfMass_m3640748392 (Rigidbody_t3346577219 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_centerOfMass_m3640748392_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_get_centerOfMass_m3640748392_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_centerOfMass_m3640748392_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_centerOfMass(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_centerOfMass(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_set_centerOfMass_m1759559900 (Rigidbody_t3346577219 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_centerOfMass_m1759559900_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_set_centerOfMass_m1759559900_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_centerOfMass_m1759559900_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_centerOfMass(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_worldCenterOfMass()
extern "C"  Vector3_t4282066566  Rigidbody_get_worldCenterOfMass_m3756357965 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody_INTERNAL_get_worldCenterOfMass_m2025777586(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_worldCenterOfMass(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_worldCenterOfMass_m2025777586 (Rigidbody_t3346577219 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_worldCenterOfMass_m2025777586_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_get_worldCenterOfMass_m2025777586_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_worldCenterOfMass_m2025777586_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_worldCenterOfMass(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Quaternion UnityEngine.Rigidbody::get_inertiaTensorRotation()
extern "C"  Quaternion_t1553702882  Rigidbody_get_inertiaTensorRotation_m1228165580 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody_INTERNAL_get_inertiaTensorRotation_m1637382083(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::set_inertiaTensorRotation(UnityEngine.Quaternion)
extern "C"  void Rigidbody_set_inertiaTensorRotation_m214325353 (Rigidbody_t3346577219 * __this, Quaternion_t1553702882  ___value0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_inertiaTensorRotation_m3045867831(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_inertiaTensorRotation(UnityEngine.Quaternion&)
extern "C"  void Rigidbody_INTERNAL_get_inertiaTensorRotation_m1637382083 (Rigidbody_t3346577219 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_inertiaTensorRotation_m1637382083_ftn) (Rigidbody_t3346577219 *, Quaternion_t1553702882 *);
	static Rigidbody_INTERNAL_get_inertiaTensorRotation_m1637382083_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_inertiaTensorRotation_m1637382083_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_inertiaTensorRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_inertiaTensorRotation(UnityEngine.Quaternion&)
extern "C"  void Rigidbody_INTERNAL_set_inertiaTensorRotation_m3045867831 (Rigidbody_t3346577219 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_inertiaTensorRotation_m3045867831_ftn) (Rigidbody_t3346577219 *, Quaternion_t1553702882 *);
	static Rigidbody_INTERNAL_set_inertiaTensorRotation_m3045867831_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_inertiaTensorRotation_m3045867831_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_inertiaTensorRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_inertiaTensor()
extern "C"  Vector3_t4282066566  Rigidbody_get_inertiaTensor_m2977227502 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody_INTERNAL_get_inertiaTensor_m2132144595(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::set_inertiaTensor(UnityEngine.Vector3)
extern "C"  void Rigidbody_set_inertiaTensor_m37850585 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_inertiaTensor_m3944843487(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_inertiaTensor(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_inertiaTensor_m2132144595 (Rigidbody_t3346577219 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_inertiaTensor_m2132144595_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_get_inertiaTensor_m2132144595_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_inertiaTensor_m2132144595_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_inertiaTensor(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_inertiaTensor(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_set_inertiaTensor_m3944843487 (Rigidbody_t3346577219 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_inertiaTensor_m3944843487_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_set_inertiaTensor_m3944843487_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_inertiaTensor_m3944843487_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_inertiaTensor(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody::get_detectCollisions()
extern "C"  bool Rigidbody_get_detectCollisions_m3012248671 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	typedef bool (*Rigidbody_get_detectCollisions_m3012248671_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_get_detectCollisions_m3012248671_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_detectCollisions_m3012248671_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_detectCollisions()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_detectCollisions(System.Boolean)
extern "C"  void Rigidbody_set_detectCollisions_m1164741296 (Rigidbody_t3346577219 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_detectCollisions_m1164741296_ftn) (Rigidbody_t3346577219 *, bool);
	static Rigidbody_set_detectCollisions_m1164741296_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_detectCollisions_m1164741296_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_detectCollisions(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody::get_useConeFriction()
extern "C"  bool Rigidbody_get_useConeFriction_m4149201227 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	typedef bool (*Rigidbody_get_useConeFriction_m4149201227_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_get_useConeFriction_m4149201227_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_useConeFriction_m4149201227_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_useConeFriction()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_useConeFriction(System.Boolean)
extern "C"  void Rigidbody_set_useConeFriction_m280490280 (Rigidbody_t3346577219 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_useConeFriction_m280490280_ftn) (Rigidbody_t3346577219 *, bool);
	static Rigidbody_set_useConeFriction_m280490280_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_useConeFriction_m280490280_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_useConeFriction(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_position()
extern "C"  Vector3_t4282066566  Rigidbody_get_position_m1751901360 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody_INTERNAL_get_position_m428733873(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::set_position(UnityEngine.Vector3)
extern "C"  void Rigidbody_set_position_m2995840187 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_position_m4144703269(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_position_m428733873 (Rigidbody_t3346577219 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_position_m428733873_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_get_position_m428733873_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_position_m428733873_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_position(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_set_position_m4144703269 (Rigidbody_t3346577219 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_position_m4144703269_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_set_position_m4144703269_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_position_m4144703269_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Quaternion UnityEngine.Rigidbody::get_rotation()
extern "C"  Quaternion_t1553702882  Rigidbody_get_rotation_m3846953477 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody_INTERNAL_get_rotation_m1932073294(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::set_rotation(UnityEngine.Quaternion)
extern "C"  void Rigidbody_set_rotation_m3622714110 (Rigidbody_t3346577219 * __this, Quaternion_t1553702882  ___value0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_rotation_m1594295130(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C"  void Rigidbody_INTERNAL_get_rotation_m1932073294 (Rigidbody_t3346577219 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_rotation_m1932073294_ftn) (Rigidbody_t3346577219 *, Quaternion_t1553702882 *);
	static Rigidbody_INTERNAL_get_rotation_m1932073294_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_rotation_m1932073294_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_rotation(UnityEngine.Quaternion&)
extern "C"  void Rigidbody_INTERNAL_set_rotation_m1594295130 (Rigidbody_t3346577219 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_rotation_m1594295130_ftn) (Rigidbody_t3346577219 *, Quaternion_t1553702882 *);
	static Rigidbody_INTERNAL_set_rotation_m1594295130_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_rotation_m1594295130_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::MovePosition(UnityEngine.Vector3)
extern "C"  void Rigidbody_MovePosition_m1515094375 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_MovePosition_m2416276686(NULL /*static, unused*/, __this, (&___position0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_CALL_MovePosition_m2416276686 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___position1, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_MovePosition_m2416276686_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_CALL_MovePosition_m2416276686_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_MovePosition_m2416276686_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1);
}
// System.Void UnityEngine.Rigidbody::MoveRotation(UnityEngine.Quaternion)
extern "C"  void Rigidbody_MoveRotation_m38358738 (Rigidbody_t3346577219 * __this, Quaternion_t1553702882  ___rot0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_MoveRotation_m4110814929(NULL /*static, unused*/, __this, (&___rot0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)
extern "C"  void Rigidbody_INTERNAL_CALL_MoveRotation_m4110814929 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Quaternion_t1553702882 * ___rot1, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_MoveRotation_m4110814929_ftn) (Rigidbody_t3346577219 *, Quaternion_t1553702882 *);
	static Rigidbody_INTERNAL_CALL_MoveRotation_m4110814929_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_MoveRotation_m4110814929_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___self0, ___rot1);
}
// UnityEngine.RigidbodyInterpolation UnityEngine.Rigidbody::get_interpolation()
extern "C"  int32_t Rigidbody_get_interpolation_m4156318470 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	typedef int32_t (*Rigidbody_get_interpolation_m4156318470_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_get_interpolation_m4156318470_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_interpolation_m4156318470_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_interpolation()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_interpolation(UnityEngine.RigidbodyInterpolation)
extern "C"  void Rigidbody_set_interpolation_m1536742691 (Rigidbody_t3346577219 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_interpolation_m1536742691_ftn) (Rigidbody_t3346577219 *, int32_t);
	static Rigidbody_set_interpolation_m1536742691_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_interpolation_m1536742691_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_interpolation(UnityEngine.RigidbodyInterpolation)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::Sleep()
extern "C"  void Rigidbody_Sleep_m4049131361 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_Sleep_m1292822714(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_Sleep(UnityEngine.Rigidbody)
extern "C"  void Rigidbody_INTERNAL_CALL_Sleep_m1292822714 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_Sleep_m1292822714_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_INTERNAL_CALL_Sleep_m1292822714_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_Sleep_m1292822714_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_Sleep(UnityEngine.Rigidbody)");
	_il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.Rigidbody::IsSleeping()
extern "C"  bool Rigidbody_IsSleeping_m435617895 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Rigidbody_INTERNAL_CALL_IsSleeping_m4112513622(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Rigidbody::INTERNAL_CALL_IsSleeping(UnityEngine.Rigidbody)
extern "C"  bool Rigidbody_INTERNAL_CALL_IsSleeping_m4112513622 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, const MethodInfo* method)
{
	typedef bool (*Rigidbody_INTERNAL_CALL_IsSleeping_m4112513622_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_INTERNAL_CALL_IsSleeping_m4112513622_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_IsSleeping_m4112513622_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_IsSleeping(UnityEngine.Rigidbody)");
	return _il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.Rigidbody::WakeUp()
extern "C"  void Rigidbody_WakeUp_m2643728503 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_WakeUp_m2627563334(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_WakeUp(UnityEngine.Rigidbody)
extern "C"  void Rigidbody_INTERNAL_CALL_WakeUp_m2627563334 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_WakeUp_m2627563334_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_INTERNAL_CALL_WakeUp_m2627563334_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_WakeUp_m2627563334_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_WakeUp(UnityEngine.Rigidbody)");
	_il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.Rigidbody::ResetCenterOfMass()
extern "C"  void Rigidbody_ResetCenterOfMass_m1278308889 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_ResetCenterOfMass_m1859922562(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_ResetCenterOfMass(UnityEngine.Rigidbody)
extern "C"  void Rigidbody_INTERNAL_CALL_ResetCenterOfMass_m1859922562 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_ResetCenterOfMass_m1859922562_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_INTERNAL_CALL_ResetCenterOfMass_m1859922562_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_ResetCenterOfMass_m1859922562_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_ResetCenterOfMass(UnityEngine.Rigidbody)");
	_il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.Rigidbody::ResetInertiaTensor()
extern "C"  void Rigidbody_ResetInertiaTensor_m561960956 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_ResetInertiaTensor_m4258642785(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_ResetInertiaTensor(UnityEngine.Rigidbody)
extern "C"  void Rigidbody_INTERNAL_CALL_ResetInertiaTensor_m4258642785 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_ResetInertiaTensor_m4258642785_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_INTERNAL_CALL_ResetInertiaTensor_m4258642785_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_ResetInertiaTensor_m4258642785_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_ResetInertiaTensor(UnityEngine.Rigidbody)");
	_il2cpp_icall_func(___self0);
}
// System.Int32 UnityEngine.Rigidbody::get_solverIterationCount()
extern "C"  int32_t Rigidbody_get_solverIterationCount_m3161737574 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	typedef int32_t (*Rigidbody_get_solverIterationCount_m3161737574_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_get_solverIterationCount_m3161737574_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_solverIterationCount_m3161737574_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_solverIterationCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_solverIterationCount(System.Int32)
extern "C"  void Rigidbody_set_solverIterationCount_m1105808811 (Rigidbody_t3346577219 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_solverIterationCount_m1105808811_ftn) (Rigidbody_t3346577219 *, int32_t);
	static Rigidbody_set_solverIterationCount_m1105808811_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_solverIterationCount_m1105808811_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_solverIterationCount(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody::get_sleepThreshold()
extern "C"  float Rigidbody_get_sleepThreshold_m1205949385 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody_get_sleepThreshold_m1205949385_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_get_sleepThreshold_m1205949385_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_sleepThreshold_m1205949385_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_sleepThreshold()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_sleepThreshold(System.Single)
extern "C"  void Rigidbody_set_sleepThreshold_m2470729090 (Rigidbody_t3346577219 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_sleepThreshold_m2470729090_ftn) (Rigidbody_t3346577219 *, float);
	static Rigidbody_set_sleepThreshold_m2470729090_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_sleepThreshold_m2470729090_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_sleepThreshold(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody::get_maxAngularVelocity()
extern "C"  float Rigidbody_get_maxAngularVelocity_m3540461456 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody_get_maxAngularVelocity_m3540461456_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_get_maxAngularVelocity_m3540461456_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_maxAngularVelocity_m3540461456_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_maxAngularVelocity()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_maxAngularVelocity(System.Single)
extern "C"  void Rigidbody_set_maxAngularVelocity_m3320880411 (Rigidbody_t3346577219 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_maxAngularVelocity_m3320880411_ftn) (Rigidbody_t3346577219 *, float);
	static Rigidbody_set_maxAngularVelocity_m3320880411_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_maxAngularVelocity_m3320880411_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_maxAngularVelocity(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody::SweepTest(UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Rigidbody_SweepTest_m1837275672 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___direction0, RaycastHit_t4003175726 * ___hitInfo1, float ___maxDistance2, int32_t ___queryTriggerInteraction3, const MethodInfo* method)
{
	{
		RaycastHit_t4003175726 * L_0 = ___hitInfo1;
		float L_1 = ___maxDistance2;
		int32_t L_2 = ___queryTriggerInteraction3;
		bool L_3 = Rigidbody_INTERNAL_CALL_SweepTest_m2073810905(NULL /*static, unused*/, __this, (&___direction0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.Rigidbody::SweepTest(UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Rigidbody_SweepTest_m2589578903 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___direction0, RaycastHit_t4003175726 * ___hitInfo1, float ___maxDistance2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		RaycastHit_t4003175726 * L_0 = ___hitInfo1;
		float L_1 = ___maxDistance2;
		int32_t L_2 = V_0;
		bool L_3 = Rigidbody_INTERNAL_CALL_SweepTest_m2073810905(NULL /*static, unused*/, __this, (&___direction0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.Rigidbody::SweepTest(UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern "C"  bool Rigidbody_SweepTest_m3557018418 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___direction0, RaycastHit_t4003175726 * ___hitInfo1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		V_0 = 0;
		V_1 = (std::numeric_limits<float>::infinity());
		RaycastHit_t4003175726 * L_0 = ___hitInfo1;
		float L_1 = V_1;
		int32_t L_2 = V_0;
		bool L_3 = Rigidbody_INTERNAL_CALL_SweepTest_m2073810905(NULL /*static, unused*/, __this, (&___direction0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.Rigidbody::INTERNAL_CALL_SweepTest(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Rigidbody_INTERNAL_CALL_SweepTest_m2073810905 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___direction1, RaycastHit_t4003175726 * ___hitInfo2, float ___maxDistance3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	typedef bool (*Rigidbody_INTERNAL_CALL_SweepTest_m2073810905_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *, RaycastHit_t4003175726 *, float, int32_t);
	static Rigidbody_INTERNAL_CALL_SweepTest_m2073810905_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_SweepTest_m2073810905_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_SweepTest(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___self0, ___direction1, ___hitInfo2, ___maxDistance3, ___queryTriggerInteraction4);
}
// UnityEngine.RaycastHit[] UnityEngine.Rigidbody::SweepTestAll(UnityEngine.Vector3,System.Single,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t528650843* Rigidbody_SweepTestAll_m3374008048 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___direction0, float ___maxDistance1, int32_t ___queryTriggerInteraction2, const MethodInfo* method)
{
	{
		float L_0 = ___maxDistance1;
		int32_t L_1 = ___queryTriggerInteraction2;
		RaycastHitU5BU5D_t528650843* L_2 = Rigidbody_INTERNAL_CALL_SweepTestAll_m4060761201(NULL /*static, unused*/, __this, (&___direction0), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Rigidbody::SweepTestAll(UnityEngine.Vector3,System.Single)
extern "C"  RaycastHitU5BU5D_t528650843* Rigidbody_SweepTestAll_m4279504751 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___direction0, float ___maxDistance1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		float L_0 = ___maxDistance1;
		int32_t L_1 = V_0;
		RaycastHitU5BU5D_t528650843* L_2 = Rigidbody_INTERNAL_CALL_SweepTestAll_m4060761201(NULL /*static, unused*/, __this, (&___direction0), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Rigidbody::SweepTestAll(UnityEngine.Vector3)
extern "C"  RaycastHitU5BU5D_t528650843* Rigidbody_SweepTestAll_m841211402 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___direction0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		V_0 = 0;
		V_1 = (std::numeric_limits<float>::infinity());
		float L_0 = V_1;
		int32_t L_1 = V_0;
		RaycastHitU5BU5D_t528650843* L_2 = Rigidbody_INTERNAL_CALL_SweepTestAll_m4060761201(NULL /*static, unused*/, __this, (&___direction0), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Rigidbody::INTERNAL_CALL_SweepTestAll(UnityEngine.Rigidbody,UnityEngine.Vector3&,System.Single,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t528650843* Rigidbody_INTERNAL_CALL_SweepTestAll_m4060761201 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___direction1, float ___maxDistance2, int32_t ___queryTriggerInteraction3, const MethodInfo* method)
{
	typedef RaycastHitU5BU5D_t528650843* (*Rigidbody_INTERNAL_CALL_SweepTestAll_m4060761201_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *, float, int32_t);
	static Rigidbody_INTERNAL_CALL_SweepTestAll_m4060761201_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_SweepTestAll_m4060761201_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_SweepTestAll(UnityEngine.Rigidbody,UnityEngine.Vector3&,System.Single,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___self0, ___direction1, ___maxDistance2, ___queryTriggerInteraction3);
}
// System.Void UnityEngine.Rigidbody2D::.ctor()
extern "C"  void Rigidbody2D__ctor_m3171486938 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	{
		Component__ctor_m4238603388(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_position()
extern "C"  Vector2_t4282066565  Rigidbody2D_get_position_m3766784193 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody2D_INTERNAL_get_position_m80650910(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody2D::set_position(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_set_position_m2296903370 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		Rigidbody2D_INTERNAL_set_position_m3796620306(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_get_position(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_get_position_m80650910 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_get_position_m80650910_ftn) (Rigidbody2D_t1743771669 *, Vector2_t4282066565 *);
	static Rigidbody2D_INTERNAL_get_position_m80650910_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_get_position_m80650910_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_get_position(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_set_position(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_set_position_m3796620306 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_set_position_m3796620306_ftn) (Rigidbody2D_t1743771669 *, Vector2_t4282066565 *);
	static Rigidbody2D_INTERNAL_set_position_m3796620306_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_set_position_m3796620306_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_set_position(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody2D::get_rotation()
extern "C"  float Rigidbody2D_get_rotation_m3576148997 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody2D_get_rotation_m3576148997_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_get_rotation_m3576148997_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_rotation_m3576148997_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_rotation()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_rotation(System.Single)
extern "C"  void Rigidbody2D_set_rotation_m4035430854 (Rigidbody2D_t1743771669 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_rotation_m4035430854_ftn) (Rigidbody2D_t1743771669 *, float);
	static Rigidbody2D_set_rotation_m4035430854_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_rotation_m4035430854_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_rotation(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody2D::MovePosition(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_MovePosition_m816157558 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___position0, const MethodInfo* method)
{
	{
		Rigidbody2D_INTERNAL_CALL_MovePosition_m3017526385(NULL /*static, unused*/, __this, (&___position0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody2D,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_CALL_MovePosition_m3017526385 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___self0, Vector2_t4282066565 * ___position1, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_CALL_MovePosition_m3017526385_ftn) (Rigidbody2D_t1743771669 *, Vector2_t4282066565 *);
	static Rigidbody2D_INTERNAL_CALL_MovePosition_m3017526385_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_CALL_MovePosition_m3017526385_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody2D,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___self0, ___position1);
}
// System.Void UnityEngine.Rigidbody2D::MoveRotation(System.Single)
extern "C"  void Rigidbody2D_MoveRotation_m724785010 (Rigidbody2D_t1743771669 * __this, float ___angle0, const MethodInfo* method)
{
	{
		float L_0 = ___angle0;
		Rigidbody2D_INTERNAL_CALL_MoveRotation_m800044407(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody2D,System.Single)
extern "C"  void Rigidbody2D_INTERNAL_CALL_MoveRotation_m800044407 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___self0, float ___angle1, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_CALL_MoveRotation_m800044407_ftn) (Rigidbody2D_t1743771669 *, float);
	static Rigidbody2D_INTERNAL_CALL_MoveRotation_m800044407_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_CALL_MoveRotation_m800044407_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody2D,System.Single)");
	_il2cpp_icall_func(___self0, ___angle1);
}
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_velocity()
extern "C"  Vector2_t4282066565  Rigidbody2D_get_velocity_m416159605 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody2D_INTERNAL_get_velocity_m715507538(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_set_velocity_m100625302 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		Rigidbody2D_INTERNAL_set_velocity_m136509638(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_get_velocity(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_get_velocity_m715507538 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_get_velocity_m715507538_ftn) (Rigidbody2D_t1743771669 *, Vector2_t4282066565 *);
	static Rigidbody2D_INTERNAL_get_velocity_m715507538_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_get_velocity_m715507538_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_get_velocity(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_set_velocity(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_set_velocity_m136509638 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_set_velocity_m136509638_ftn) (Rigidbody2D_t1743771669 *, Vector2_t4282066565 *);
	static Rigidbody2D_INTERNAL_set_velocity_m136509638_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_set_velocity_m136509638_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_set_velocity(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody2D::get_angularVelocity()
extern "C"  float Rigidbody2D_get_angularVelocity_m3714473050 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody2D_get_angularVelocity_m3714473050_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_get_angularVelocity_m3714473050_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_angularVelocity_m3714473050_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_angularVelocity()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_angularVelocity(System.Single)
extern "C"  void Rigidbody2D_set_angularVelocity_m1393720209 (Rigidbody2D_t1743771669 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_angularVelocity_m1393720209_ftn) (Rigidbody2D_t1743771669 *, float);
	static Rigidbody2D_set_angularVelocity_m1393720209_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_angularVelocity_m1393720209_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_angularVelocity(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody2D::get_useAutoMass()
extern "C"  bool Rigidbody2D_get_useAutoMass_m3316137215 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef bool (*Rigidbody2D_get_useAutoMass_m3316137215_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_get_useAutoMass_m3316137215_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_useAutoMass_m3316137215_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_useAutoMass()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_useAutoMass(System.Boolean)
extern "C"  void Rigidbody2D_set_useAutoMass_m673357404 (Rigidbody2D_t1743771669 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_useAutoMass_m673357404_ftn) (Rigidbody2D_t1743771669 *, bool);
	static Rigidbody2D_set_useAutoMass_m673357404_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_useAutoMass_m673357404_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_useAutoMass(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody2D::get_mass()
extern "C"  float Rigidbody2D_get_mass_m3688503547 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody2D_get_mass_m3688503547_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_get_mass_m3688503547_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_mass_m3688503547_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_mass()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_mass(System.Single)
extern "C"  void Rigidbody2D_set_mass_m610116752 (Rigidbody2D_t1743771669 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_mass_m610116752_ftn) (Rigidbody2D_t1743771669 *, float);
	static Rigidbody2D_set_mass_m610116752_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_mass_m610116752_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_mass(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_centerOfMass()
extern "C"  Vector2_t4282066565  Rigidbody2D_get_centerOfMass_m4052188280 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody2D_INTERNAL_get_centerOfMass_m3724414805(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody2D::set_centerOfMass(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_set_centerOfMass_m5458227 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		Rigidbody2D_INTERNAL_set_centerOfMass_m1843226313(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_get_centerOfMass(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_get_centerOfMass_m3724414805 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_get_centerOfMass_m3724414805_ftn) (Rigidbody2D_t1743771669 *, Vector2_t4282066565 *);
	static Rigidbody2D_INTERNAL_get_centerOfMass_m3724414805_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_get_centerOfMass_m3724414805_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_get_centerOfMass(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_set_centerOfMass(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_set_centerOfMass_m1843226313 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_set_centerOfMass_m1843226313_ftn) (Rigidbody2D_t1743771669 *, Vector2_t4282066565 *);
	static Rigidbody2D_INTERNAL_set_centerOfMass_m1843226313_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_set_centerOfMass_m1843226313_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_set_centerOfMass(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_worldCenterOfMass()
extern "C"  Vector2_t4282066565  Rigidbody2D_get_worldCenterOfMass_m2477707356 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody2D_INTERNAL_get_worldCenterOfMass_m3173980419(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_get_worldCenterOfMass(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_get_worldCenterOfMass_m3173980419 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_get_worldCenterOfMass_m3173980419_ftn) (Rigidbody2D_t1743771669 *, Vector2_t4282066565 *);
	static Rigidbody2D_INTERNAL_get_worldCenterOfMass_m3173980419_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_get_worldCenterOfMass_m3173980419_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_get_worldCenterOfMass(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody2D::get_inertia()
extern "C"  float Rigidbody2D_get_inertia_m2090858869 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody2D_get_inertia_m2090858869_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_get_inertia_m2090858869_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_inertia_m2090858869_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_inertia()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_inertia(System.Single)
extern "C"  void Rigidbody2D_set_inertia_m2553823830 (Rigidbody2D_t1743771669 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_inertia_m2553823830_ftn) (Rigidbody2D_t1743771669 *, float);
	static Rigidbody2D_set_inertia_m2553823830_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_inertia_m2553823830_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_inertia(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody2D::get_drag()
extern "C"  float Rigidbody2D_get_drag_m3445993275 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody2D_get_drag_m3445993275_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_get_drag_m3445993275_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_drag_m3445993275_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_drag()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_drag(System.Single)
extern "C"  void Rigidbody2D_set_drag_m3091740240 (Rigidbody2D_t1743771669 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_drag_m3091740240_ftn) (Rigidbody2D_t1743771669 *, float);
	static Rigidbody2D_set_drag_m3091740240_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_drag_m3091740240_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_drag(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody2D::get_angularDrag()
extern "C"  float Rigidbody2D_get_angularDrag_m957403377 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody2D_get_angularDrag_m957403377_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_get_angularDrag_m957403377_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_angularDrag_m957403377_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_angularDrag()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_angularDrag(System.Single)
extern "C"  void Rigidbody2D_set_angularDrag_m1521513562 (Rigidbody2D_t1743771669 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_angularDrag_m1521513562_ftn) (Rigidbody2D_t1743771669 *, float);
	static Rigidbody2D_set_angularDrag_m1521513562_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_angularDrag_m1521513562_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_angularDrag(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody2D::get_gravityScale()
extern "C"  float Rigidbody2D_get_gravityScale_m1347094947 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody2D_get_gravityScale_m1347094947_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_get_gravityScale_m1347094947_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_gravityScale_m1347094947_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_gravityScale()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_gravityScale(System.Single)
extern "C"  void Rigidbody2D_set_gravityScale_m2024998120 (Rigidbody2D_t1743771669 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_gravityScale_m2024998120_ftn) (Rigidbody2D_t1743771669 *, float);
	static Rigidbody2D_set_gravityScale_m2024998120_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_gravityScale_m2024998120_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_gravityScale(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody2D::get_isKinematic()
extern "C"  bool Rigidbody2D_get_isKinematic_m957232848 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef bool (*Rigidbody2D_get_isKinematic_m957232848_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_get_isKinematic_m957232848_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_isKinematic_m957232848_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_isKinematic()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_isKinematic(System.Boolean)
extern "C"  void Rigidbody2D_set_isKinematic_m222467693 (Rigidbody2D_t1743771669 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_isKinematic_m222467693_ftn) (Rigidbody2D_t1743771669 *, bool);
	static Rigidbody2D_set_isKinematic_m222467693_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_isKinematic_m222467693_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_isKinematic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody2D::get_freezeRotation()
extern "C"  bool Rigidbody2D_get_freezeRotation_m48155618 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef bool (*Rigidbody2D_get_freezeRotation_m48155618_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_get_freezeRotation_m48155618_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_freezeRotation_m48155618_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_freezeRotation()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_freezeRotation(System.Boolean)
extern "C"  void Rigidbody2D_set_freezeRotation_m3797270003 (Rigidbody2D_t1743771669 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_freezeRotation_m3797270003_ftn) (Rigidbody2D_t1743771669 *, bool);
	static Rigidbody2D_set_freezeRotation_m3797270003_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_freezeRotation_m3797270003_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_freezeRotation(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.RigidbodyConstraints2D UnityEngine.Rigidbody2D::get_constraints()
extern "C"  int32_t Rigidbody2D_get_constraints_m3440684386 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef int32_t (*Rigidbody2D_get_constraints_m3440684386_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_get_constraints_m3440684386_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_constraints_m3440684386_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_constraints()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_constraints(UnityEngine.RigidbodyConstraints2D)
extern "C"  void Rigidbody2D_set_constraints_m3508094335 (Rigidbody2D_t1743771669 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_constraints_m3508094335_ftn) (Rigidbody2D_t1743771669 *, int32_t);
	static Rigidbody2D_set_constraints_m3508094335_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_constraints_m3508094335_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_constraints(UnityEngine.RigidbodyConstraints2D)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody2D::IsSleeping()
extern "C"  bool Rigidbody2D_IsSleeping_m4134977273 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef bool (*Rigidbody2D_IsSleeping_m4134977273_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_IsSleeping_m4134977273_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_IsSleeping_m4134977273_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::IsSleeping()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Rigidbody2D::IsAwake()
extern "C"  bool Rigidbody2D_IsAwake_m3225469273 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef bool (*Rigidbody2D_IsAwake_m3225469273_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_IsAwake_m3225469273_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_IsAwake_m3225469273_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::IsAwake()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::Sleep()
extern "C"  void Rigidbody2D_Sleep_m1892894479 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_Sleep_m1892894479_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_Sleep_m1892894479_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_Sleep_m1892894479_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::Sleep()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::WakeUp()
extern "C"  void Rigidbody2D_WakeUp_m224894601 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_WakeUp_m224894601_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_WakeUp_m224894601_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_WakeUp_m224894601_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::WakeUp()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Rigidbody2D::get_simulated()
extern "C"  bool Rigidbody2D_get_simulated_m1958222677 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef bool (*Rigidbody2D_get_simulated_m1958222677_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_get_simulated_m1958222677_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_simulated_m1958222677_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_simulated()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_simulated(System.Boolean)
extern "C"  void Rigidbody2D_set_simulated_m1953955762 (Rigidbody2D_t1743771669 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_simulated_m1953955762_ftn) (Rigidbody2D_t1743771669 *, bool);
	static Rigidbody2D_set_simulated_m1953955762_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_simulated_m1953955762_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_simulated(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.RigidbodyInterpolation2D UnityEngine.Rigidbody2D::get_interpolation()
extern "C"  int32_t Rigidbody2D_get_interpolation_m3806763106 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef int32_t (*Rigidbody2D_get_interpolation_m3806763106_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_get_interpolation_m3806763106_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_interpolation_m3806763106_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_interpolation()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_interpolation(UnityEngine.RigidbodyInterpolation2D)
extern "C"  void Rigidbody2D_set_interpolation_m360668735 (Rigidbody2D_t1743771669 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_interpolation_m360668735_ftn) (Rigidbody2D_t1743771669 *, int32_t);
	static Rigidbody2D_set_interpolation_m360668735_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_interpolation_m360668735_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_interpolation(UnityEngine.RigidbodyInterpolation2D)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.RigidbodySleepMode2D UnityEngine.Rigidbody2D::get_sleepMode()
extern "C"  int32_t Rigidbody2D_get_sleepMode_m3719802274 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef int32_t (*Rigidbody2D_get_sleepMode_m3719802274_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_get_sleepMode_m3719802274_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_sleepMode_m3719802274_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_sleepMode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_sleepMode(UnityEngine.RigidbodySleepMode2D)
extern "C"  void Rigidbody2D_set_sleepMode_m1448458239 (Rigidbody2D_t1743771669 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_sleepMode_m1448458239_ftn) (Rigidbody2D_t1743771669 *, int32_t);
	static Rigidbody2D_set_sleepMode_m1448458239_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_sleepMode_m1448458239_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_sleepMode(UnityEngine.RigidbodySleepMode2D)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.CollisionDetectionMode2D UnityEngine.Rigidbody2D::get_collisionDetectionMode()
extern "C"  int32_t Rigidbody2D_get_collisionDetectionMode_m4178159481 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef int32_t (*Rigidbody2D_get_collisionDetectionMode_m4178159481_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_get_collisionDetectionMode_m4178159481_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_collisionDetectionMode_m4178159481_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_collisionDetectionMode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_collisionDetectionMode(UnityEngine.CollisionDetectionMode2D)
extern "C"  void Rigidbody2D_set_collisionDetectionMode_m4242997470 (Rigidbody2D_t1743771669 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_collisionDetectionMode_m4242997470_ftn) (Rigidbody2D_t1743771669 *, int32_t);
	static Rigidbody2D_set_collisionDetectionMode_m4242997470_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_collisionDetectionMode_m4242997470_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_collisionDetectionMode(UnityEngine.CollisionDetectionMode2D)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody2D::IsTouching(UnityEngine.Collider2D)
extern "C"  bool Rigidbody2D_IsTouching_m1617274740 (Rigidbody2D_t1743771669 * __this, Collider2D_t1552025098 * ___collider0, const MethodInfo* method)
{
	typedef bool (*Rigidbody2D_IsTouching_m1617274740_ftn) (Rigidbody2D_t1743771669 *, Collider2D_t1552025098 *);
	static Rigidbody2D_IsTouching_m1617274740_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_IsTouching_m1617274740_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::IsTouching(UnityEngine.Collider2D)");
	return _il2cpp_icall_func(__this, ___collider0);
}
// System.Boolean UnityEngine.Rigidbody2D::IsTouchingLayers(System.Int32)
extern "C"  bool Rigidbody2D_IsTouchingLayers_m2262791492 (Rigidbody2D_t1743771669 * __this, int32_t ___layerMask0, const MethodInfo* method)
{
	typedef bool (*Rigidbody2D_IsTouchingLayers_m2262791492_ftn) (Rigidbody2D_t1743771669 *, int32_t);
	static Rigidbody2D_IsTouchingLayers_m2262791492_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_IsTouchingLayers_m2262791492_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::IsTouchingLayers(System.Int32)");
	return _il2cpp_icall_func(__this, ___layerMask0);
}
// System.Boolean UnityEngine.Rigidbody2D::IsTouchingLayers()
extern "C"  bool Rigidbody2D_IsTouchingLayers_m244569203 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (-1);
		int32_t L_0 = V_0;
		bool L_1 = Rigidbody2D_IsTouchingLayers_m2262791492(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_AddForce_m4161385513 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___force0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody2D_INTERNAL_CALL_AddForce_m2763823108(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_AddForce_m312397382 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___force0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody2D_INTERNAL_CALL_AddForce_m2763823108(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_INTERNAL_CALL_AddForce_m2763823108 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___self0, Vector2_t4282066565 * ___force1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_CALL_AddForce_m2763823108_ftn) (Rigidbody2D_t1743771669 *, Vector2_t4282066565 *, int32_t);
	static Rigidbody2D_INTERNAL_CALL_AddForce_m2763823108_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_CALL_AddForce_m2763823108_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)");
	_il2cpp_icall_func(___self0, ___force1, ___mode2);
}
// System.Void UnityEngine.Rigidbody2D::AddRelativeForce(UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_AddRelativeForce_m3569051669 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___relativeForce0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody2D_INTERNAL_CALL_AddRelativeForce_m3603347096(NULL /*static, unused*/, __this, (&___relativeForce0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::AddRelativeForce(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_AddRelativeForce_m952010290 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___relativeForce0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody2D_INTERNAL_CALL_AddRelativeForce_m3603347096(NULL /*static, unused*/, __this, (&___relativeForce0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_AddRelativeForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_INTERNAL_CALL_AddRelativeForce_m3603347096 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___self0, Vector2_t4282066565 * ___relativeForce1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_CALL_AddRelativeForce_m3603347096_ftn) (Rigidbody2D_t1743771669 *, Vector2_t4282066565 *, int32_t);
	static Rigidbody2D_INTERNAL_CALL_AddRelativeForce_m3603347096_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_CALL_AddRelativeForce_m3603347096_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_CALL_AddRelativeForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)");
	_il2cpp_icall_func(___self0, ___relativeForce1, ___mode2);
}
// System.Void UnityEngine.Rigidbody2D::AddForceAtPosition(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_AddForceAtPosition_m174512961 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___force0, Vector2_t4282066565  ___position1, int32_t ___mode2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode2;
		Rigidbody2D_INTERNAL_CALL_AddForceAtPosition_m1988757918(NULL /*static, unused*/, __this, (&___force0), (&___position1), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::AddForceAtPosition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void Rigidbody2D_AddForceAtPosition_m3734093150 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___force0, Vector2_t4282066565  ___position1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody2D_INTERNAL_CALL_AddForceAtPosition_m1988757918(NULL /*static, unused*/, __this, (&___force0), (&___position1), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_INTERNAL_CALL_AddForceAtPosition_m1988757918 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___self0, Vector2_t4282066565 * ___force1, Vector2_t4282066565 * ___position2, int32_t ___mode3, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_CALL_AddForceAtPosition_m1988757918_ftn) (Rigidbody2D_t1743771669 *, Vector2_t4282066565 *, Vector2_t4282066565 *, int32_t);
	static Rigidbody2D_INTERNAL_CALL_AddForceAtPosition_m1988757918_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_CALL_AddForceAtPosition_m1988757918_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ForceMode2D)");
	_il2cpp_icall_func(___self0, ___force1, ___position2, ___mode3);
}
// System.Void UnityEngine.Rigidbody2D::AddTorque(System.Single,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_AddTorque_m3138315435 (Rigidbody2D_t1743771669 * __this, float ___torque0, int32_t ___mode1, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_AddTorque_m3138315435_ftn) (Rigidbody2D_t1743771669 *, float, int32_t);
	static Rigidbody2D_AddTorque_m3138315435_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_AddTorque_m3138315435_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::AddTorque(System.Single,UnityEngine.ForceMode2D)");
	_il2cpp_icall_func(__this, ___torque0, ___mode1);
}
// System.Void UnityEngine.Rigidbody2D::AddTorque(System.Single)
extern "C"  void Rigidbody2D_AddTorque_m780068040 (Rigidbody2D_t1743771669 * __this, float ___torque0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		float L_0 = ___torque0;
		int32_t L_1 = V_0;
		Rigidbody2D_AddTorque_m3138315435(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::GetPoint(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  Rigidbody2D_GetPoint_m311990097 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___point0, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565  L_0 = ___point0;
		Rigidbody2D_Rigidbody2D_CUSTOM_INTERNAL_GetPoint_m965507570(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Rigidbody2D::Rigidbody2D_CUSTOM_INTERNAL_GetPoint(UnityEngine.Rigidbody2D,UnityEngine.Vector2,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_Rigidbody2D_CUSTOM_INTERNAL_GetPoint_m965507570 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565  ___point1, Vector2_t4282066565 * ___value2, const MethodInfo* method)
{
	{
		Rigidbody2D_t1743771669 * L_0 = ___rigidbody0;
		Vector2_t4282066565 * L_1 = ___value2;
		Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPoint_m2094726099(NULL /*static, unused*/, L_0, (&___point1), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPoint(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPoint_m2094726099 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565 * ___point1, Vector2_t4282066565 * ___value2, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPoint_m2094726099_ftn) (Rigidbody2D_t1743771669 *, Vector2_t4282066565 *, Vector2_t4282066565 *);
	static Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPoint_m2094726099_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPoint_m2094726099_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPoint(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___rigidbody0, ___point1, ___value2);
}
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::GetRelativePoint(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  Rigidbody2D_GetRelativePoint_m143441725 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___relativePoint0, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565  L_0 = ___relativePoint0;
		Rigidbody2D_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePoint_m3486130822(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Rigidbody2D::Rigidbody2D_CUSTOM_INTERNAL_GetRelativePoint(UnityEngine.Rigidbody2D,UnityEngine.Vector2,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePoint_m3486130822 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565  ___relativePoint1, Vector2_t4282066565 * ___value2, const MethodInfo* method)
{
	{
		Rigidbody2D_t1743771669 * L_0 = ___rigidbody0;
		Vector2_t4282066565 * L_1 = ___value2;
		Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePoint_m2355868863(NULL /*static, unused*/, L_0, (&___relativePoint1), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePoint(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePoint_m2355868863 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565 * ___relativePoint1, Vector2_t4282066565 * ___value2, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePoint_m2355868863_ftn) (Rigidbody2D_t1743771669 *, Vector2_t4282066565 *, Vector2_t4282066565 *);
	static Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePoint_m2355868863_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePoint_m2355868863_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePoint(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___rigidbody0, ___relativePoint1, ___value2);
}
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::GetVector(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  Rigidbody2D_GetVector_m1217773294 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___vector0, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565  L_0 = ___vector0;
		Rigidbody2D_Rigidbody2D_CUSTOM_INTERNAL_GetVector_m2817843703(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Rigidbody2D::Rigidbody2D_CUSTOM_INTERNAL_GetVector(UnityEngine.Rigidbody2D,UnityEngine.Vector2,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_Rigidbody2D_CUSTOM_INTERNAL_GetVector_m2817843703 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565  ___vector1, Vector2_t4282066565 * ___value2, const MethodInfo* method)
{
	{
		Rigidbody2D_t1743771669 * L_0 = ___rigidbody0;
		Vector2_t4282066565 * L_1 = ___value2;
		Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetVector_m2679858764(NULL /*static, unused*/, L_0, (&___vector1), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetVector(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetVector_m2679858764 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565 * ___vector1, Vector2_t4282066565 * ___value2, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetVector_m2679858764_ftn) (Rigidbody2D_t1743771669 *, Vector2_t4282066565 *, Vector2_t4282066565 *);
	static Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetVector_m2679858764_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetVector_m2679858764_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetVector(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___rigidbody0, ___vector1, ___value2);
}
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::GetRelativeVector(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  Rigidbody2D_GetRelativeVector_m287741058 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___relativeVector0, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565  L_0 = ___relativeVector0;
		Rigidbody2D_Rigidbody2D_CUSTOM_INTERNAL_GetRelativeVector_m3647753187(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Rigidbody2D::Rigidbody2D_CUSTOM_INTERNAL_GetRelativeVector(UnityEngine.Rigidbody2D,UnityEngine.Vector2,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_Rigidbody2D_CUSTOM_INTERNAL_GetRelativeVector_m3647753187 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565  ___relativeVector1, Vector2_t4282066565 * ___value2, const MethodInfo* method)
{
	{
		Rigidbody2D_t1743771669 * L_0 = ___rigidbody0;
		Vector2_t4282066565 * L_1 = ___value2;
		Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativeVector_m2185349856(NULL /*static, unused*/, L_0, (&___relativeVector1), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativeVector(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativeVector_m2185349856 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565 * ___relativeVector1, Vector2_t4282066565 * ___value2, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativeVector_m2185349856_ftn) (Rigidbody2D_t1743771669 *, Vector2_t4282066565 *, Vector2_t4282066565 *);
	static Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativeVector_m2185349856_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativeVector_m2185349856_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativeVector(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___rigidbody0, ___relativeVector1, ___value2);
}
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::GetPointVelocity(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  Rigidbody2D_GetPointVelocity_m3610750580 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___point0, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565  L_0 = ___point0;
		Rigidbody2D_Rigidbody2D_CUSTOM_INTERNAL_GetPointVelocity_m1128726703(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Rigidbody2D::Rigidbody2D_CUSTOM_INTERNAL_GetPointVelocity(UnityEngine.Rigidbody2D,UnityEngine.Vector2,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_Rigidbody2D_CUSTOM_INTERNAL_GetPointVelocity_m1128726703 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565  ___point1, Vector2_t4282066565 * ___value2, const MethodInfo* method)
{
	{
		Rigidbody2D_t1743771669 * L_0 = ___rigidbody0;
		Vector2_t4282066565 * L_1 = ___value2;
		Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPointVelocity_m2290785206(NULL /*static, unused*/, L_0, (&___point1), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPointVelocity(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPointVelocity_m2290785206 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565 * ___point1, Vector2_t4282066565 * ___value2, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPointVelocity_m2290785206_ftn) (Rigidbody2D_t1743771669 *, Vector2_t4282066565 *, Vector2_t4282066565 *);
	static Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPointVelocity_m2290785206_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPointVelocity_m2290785206_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPointVelocity(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___rigidbody0, ___point1, ___value2);
}
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::GetRelativePointVelocity(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  Rigidbody2D_GetRelativePointVelocity_m3039504992 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___relativePoint0, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565  L_0 = ___relativePoint0;
		Rigidbody2D_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePointVelocity_m3974944067(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Rigidbody2D::Rigidbody2D_CUSTOM_INTERNAL_GetRelativePointVelocity(UnityEngine.Rigidbody2D,UnityEngine.Vector2,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePointVelocity_m3974944067 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565  ___relativePoint1, Vector2_t4282066565 * ___value2, const MethodInfo* method)
{
	{
		Rigidbody2D_t1743771669 * L_0 = ___rigidbody0;
		Vector2_t4282066565 * L_1 = ___value2;
		Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePointVelocity_m3875121314(NULL /*static, unused*/, L_0, (&___relativePoint1), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePointVelocity(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePointVelocity_m3875121314 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___rigidbody0, Vector2_t4282066565 * ___relativePoint1, Vector2_t4282066565 * ___value2, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePointVelocity_m3875121314_ftn) (Rigidbody2D_t1743771669 *, Vector2_t4282066565 *, Vector2_t4282066565 *);
	static Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePointVelocity_m3875121314_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePointVelocity_m3875121314_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePointVelocity(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___rigidbody0, ___relativePoint1, ___value2);
}
// System.Void UnityEngine.RPC::.ctor()
extern "C"  void RPC__ctor_m281827604 (RPC_t3134615963 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RuntimeAnimatorController::.ctor()
extern "C"  void RuntimeAnimatorController__ctor_m1877476190 (RuntimeAnimatorController_t274649809 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m570634126(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AnimationClip[] UnityEngine.RuntimeAnimatorController::get_animationClips()
extern "C"  AnimationClipU5BU5D_t4186127791* RuntimeAnimatorController_get_animationClips_m3376046746 (RuntimeAnimatorController_t274649809 * __this, const MethodInfo* method)
{
	typedef AnimationClipU5BU5D_t4186127791* (*RuntimeAnimatorController_get_animationClips_m3376046746_ftn) (RuntimeAnimatorController_t274649809 *);
	static RuntimeAnimatorController_get_animationClips_m3376046746_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RuntimeAnimatorController_get_animationClips_m3376046746_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RuntimeAnimatorController::get_animationClips()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.SceneManagement.Scene::get_handle()
extern "C"  int32_t Scene_get_handle_m2277248521 (Scene_t1080795294 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Handle_0();
		return L_0;
	}
}
extern "C"  int32_t Scene_get_handle_m2277248521_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t1080795294 * _thisAdjusted = reinterpret_cast<Scene_t1080795294 *>(__this + 1);
	return Scene_get_handle_m2277248521(_thisAdjusted, method);
}
// System.String UnityEngine.SceneManagement.Scene::get_name()
extern "C"  String_t* Scene_get_name_m894591657 (Scene_t1080795294 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Scene_get_handle_m2277248521(__this, /*hidden argument*/NULL);
		String_t* L_1 = Scene_GetNameInternal_m2496405436(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  String_t* Scene_get_name_m894591657_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t1080795294 * _thisAdjusted = reinterpret_cast<Scene_t1080795294 *>(__this + 1);
	return Scene_get_name_m894591657(_thisAdjusted, method);
}
// System.Int32 UnityEngine.SceneManagement.Scene::GetHashCode()
extern "C"  int32_t Scene_GetHashCode_m2000109307 (Scene_t1080795294 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Handle_0();
		return L_0;
	}
}
extern "C"  int32_t Scene_GetHashCode_m2000109307_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t1080795294 * _thisAdjusted = reinterpret_cast<Scene_t1080795294 *>(__this + 1);
	return Scene_GetHashCode_m2000109307(_thisAdjusted, method);
}
// System.Boolean UnityEngine.SceneManagement.Scene::Equals(System.Object)
extern Il2CppClass* Scene_t1080795294_il2cpp_TypeInfo_var;
extern const uint32_t Scene_Equals_m93578403_MetadataUsageId;
extern "C"  bool Scene_Equals_m93578403 (Scene_t1080795294 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Scene_Equals_m93578403_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Scene_t1080795294  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Scene_t1080795294_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Scene_t1080795294 *)((Scene_t1080795294 *)UnBox (L_1, Scene_t1080795294_il2cpp_TypeInfo_var))));
		int32_t L_2 = Scene_get_handle_m2277248521(__this, /*hidden argument*/NULL);
		int32_t L_3 = Scene_get_handle_m2277248521((&V_0), /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
	}
}
extern "C"  bool Scene_Equals_m93578403_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Scene_t1080795294 * _thisAdjusted = reinterpret_cast<Scene_t1080795294 *>(__this + 1);
	return Scene_Equals_m93578403(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.SceneManagement.Scene::GetNameInternal(System.Int32)
extern "C"  String_t* Scene_GetNameInternal_m2496405436 (Il2CppObject * __this /* static, unused */, int32_t ___sceneHandle0, const MethodInfo* method)
{
	typedef String_t* (*Scene_GetNameInternal_m2496405436_ftn) (int32_t);
	static Scene_GetNameInternal_m2496405436_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Scene_GetNameInternal_m2496405436_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.Scene::GetNameInternal(System.Int32)");
	return _il2cpp_icall_func(___sceneHandle0);
}
// Conversion methods for marshalling of: UnityEngine.SceneManagement.Scene
extern "C" void Scene_t1080795294_marshal_pinvoke(const Scene_t1080795294& unmarshaled, Scene_t1080795294_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Handle_0 = unmarshaled.get_m_Handle_0();
}
extern "C" void Scene_t1080795294_marshal_pinvoke_back(const Scene_t1080795294_marshaled_pinvoke& marshaled, Scene_t1080795294& unmarshaled)
{
	int32_t unmarshaled_m_Handle_temp_0 = 0;
	unmarshaled_m_Handle_temp_0 = marshaled.___m_Handle_0;
	unmarshaled.set_m_Handle_0(unmarshaled_m_Handle_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.SceneManagement.Scene
extern "C" void Scene_t1080795294_marshal_pinvoke_cleanup(Scene_t1080795294_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SceneManagement.Scene
extern "C" void Scene_t1080795294_marshal_com(const Scene_t1080795294& unmarshaled, Scene_t1080795294_marshaled_com& marshaled)
{
	marshaled.___m_Handle_0 = unmarshaled.get_m_Handle_0();
}
extern "C" void Scene_t1080795294_marshal_com_back(const Scene_t1080795294_marshaled_com& marshaled, Scene_t1080795294& unmarshaled)
{
	int32_t unmarshaled_m_Handle_temp_0 = 0;
	unmarshaled_m_Handle_temp_0 = marshaled.___m_Handle_0;
	unmarshaled.set_m_Handle_0(unmarshaled_m_Handle_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.SceneManagement.Scene
extern "C" void Scene_t1080795294_marshal_com_cleanup(Scene_t1080795294_marshaled_com& marshaled)
{
}
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C"  Scene_t1080795294  SceneManager_GetActiveScene_m3062973092 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Scene_t1080795294  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		SceneManager_INTERNAL_CALL_GetActiveScene_m3594338006(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Scene_t1080795294  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetActiveScene(UnityEngine.SceneManagement.Scene&)
extern "C"  void SceneManager_INTERNAL_CALL_GetActiveScene_m3594338006 (Il2CppObject * __this /* static, unused */, Scene_t1080795294 * ___value0, const MethodInfo* method)
{
	typedef void (*SceneManager_INTERNAL_CALL_GetActiveScene_m3594338006_ftn) (Scene_t1080795294 *);
	static SceneManager_INTERNAL_CALL_GetActiveScene_m3594338006_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_INTERNAL_CALL_GetActiveScene_m3594338006_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetActiveScene(UnityEngine.SceneManagement.Scene&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C"  void SceneManager_LoadScene_m2167814033 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = V_0;
		SceneManager_LoadScene_m3907168970(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SceneManager_LoadScene_m3907168970 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___mode1, const MethodInfo* method)
{
	int32_t G_B2_0 = 0;
	String_t* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	String_t* G_B3_2 = NULL;
	{
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = ___mode1;
		G_B1_0 = (-1);
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B2_0 = (-1);
			G_B2_1 = L_0;
			goto IL_000f;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0010;
	}

IL_000f:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0010:
	{
		SceneManager_LoadSceneAsyncNameIndexInternal_m3775081569(NULL /*static, unused*/, G_B3_2, G_B3_1, (bool)G_B3_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsync(System.String)
extern "C"  AsyncOperation_t3699081103 * SceneManager_LoadSceneAsync_m1034954248 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = V_0;
		AsyncOperation_t3699081103 * L_2 = SceneManager_LoadSceneAsync_m1685951617(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsync(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  AsyncOperation_t3699081103 * SceneManager_LoadSceneAsync_m1685951617 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___mode1, const MethodInfo* method)
{
	int32_t G_B2_0 = 0;
	String_t* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	String_t* G_B3_2 = NULL;
	{
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = ___mode1;
		G_B1_0 = (-1);
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B2_0 = (-1);
			G_B2_1 = L_0;
			goto IL_000f;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0010;
	}

IL_000f:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0010:
	{
		AsyncOperation_t3699081103 * L_2 = SceneManager_LoadSceneAsyncNameIndexInternal_m3775081569(NULL /*static, unused*/, G_B3_2, G_B3_1, (bool)G_B3_0, (bool)0, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C"  AsyncOperation_t3699081103 * SceneManager_LoadSceneAsyncNameIndexInternal_m3775081569 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___sceneBuildIndex1, bool ___isAdditive2, bool ___mustCompleteNextFrame3, const MethodInfo* method)
{
	typedef AsyncOperation_t3699081103 * (*SceneManager_LoadSceneAsyncNameIndexInternal_m3775081569_ftn) (String_t*, int32_t, bool, bool);
	static SceneManager_LoadSceneAsyncNameIndexInternal_m3775081569_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_LoadSceneAsyncNameIndexInternal_m3775081569_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)");
	return _il2cpp_icall_func(___sceneName0, ___sceneBuildIndex1, ___isAdditive2, ___mustCompleteNextFrame3);
}
// System.Void UnityEngine.Screen::.ctor()
extern "C"  void Screen__ctor_m1333236993 (Screen_t3187157168 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Resolution[] UnityEngine.Screen::get_resolutions()
extern "C"  ResolutionU5BU5D_t3895287505* Screen_get_resolutions_m4088641747 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef ResolutionU5BU5D_t3895287505* (*Screen_get_resolutions_m4088641747_ftn) ();
	static Screen_get_resolutions_m4088641747_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_resolutions_m4088641747_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_resolutions()");
	return _il2cpp_icall_func();
}
// UnityEngine.Resolution UnityEngine.Screen::get_currentResolution()
extern "C"  Resolution_t1578306928  Screen_get_currentResolution_m2532370351 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Resolution_t1578306928  (*Screen_get_currentResolution_m2532370351_ftn) ();
	static Screen_get_currentResolution_m2532370351_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_currentResolution_m2532370351_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_currentResolution()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::SetResolution(System.Int32,System.Int32,System.Boolean,System.Int32)
extern "C"  void Screen_SetResolution_m1695402355 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, bool ___fullscreen2, int32_t ___preferredRefreshRate3, const MethodInfo* method)
{
	typedef void (*Screen_SetResolution_m1695402355_ftn) (int32_t, int32_t, bool, int32_t);
	static Screen_SetResolution_m1695402355_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_SetResolution_m1695402355_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::SetResolution(System.Int32,System.Int32,System.Boolean,System.Int32)");
	_il2cpp_icall_func(___width0, ___height1, ___fullscreen2, ___preferredRefreshRate3);
}
// System.Void UnityEngine.Screen::SetResolution(System.Int32,System.Int32,System.Boolean)
extern "C"  void Screen_SetResolution_m3920430564 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, bool ___fullscreen2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		bool L_2 = ___fullscreen2;
		int32_t L_3 = V_0;
		Screen_SetResolution_m1695402355(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m3080333084 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_width_m3080333084_ftn) ();
	static Screen_get_width_m3080333084_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_width_m3080333084_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_width()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1504859443 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_height_m1504859443_ftn) ();
	static Screen_get_height_m1504859443_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_height_m1504859443_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_height()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Screen::get_dpi()
extern "C"  float Screen_get_dpi_m3780069159 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Screen_get_dpi_m3780069159_ftn) ();
	static Screen_get_dpi_m3780069159_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_dpi_m3780069159_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_dpi()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Screen::get_fullScreen()
extern "C"  bool Screen_get_fullScreen_m4283694285 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Screen_get_fullScreen_m4283694285_ftn) ();
	static Screen_get_fullScreen_m4283694285_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_fullScreen_m4283694285_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_fullScreen()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_fullScreen(System.Boolean)
extern "C"  void Screen_set_fullScreen_m2604999058 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_fullScreen_m2604999058_ftn) (bool);
	static Screen_set_fullScreen_m2604999058_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_fullScreen_m2604999058_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_fullScreen(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Screen::get_autorotateToPortrait()
extern "C"  bool Screen_get_autorotateToPortrait_m187839762 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Screen_get_autorotateToPortrait_m187839762_ftn) ();
	static Screen_get_autorotateToPortrait_m187839762_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_autorotateToPortrait_m187839762_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_autorotateToPortrait()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_autorotateToPortrait(System.Boolean)
extern "C"  void Screen_set_autorotateToPortrait_m3130934167 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_autorotateToPortrait_m3130934167_ftn) (bool);
	static Screen_set_autorotateToPortrait_m3130934167_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_autorotateToPortrait_m3130934167_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_autorotateToPortrait(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Screen::get_autorotateToPortraitUpsideDown()
extern "C"  bool Screen_get_autorotateToPortraitUpsideDown_m631759142 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Screen_get_autorotateToPortraitUpsideDown_m631759142_ftn) ();
	static Screen_get_autorotateToPortraitUpsideDown_m631759142_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_autorotateToPortraitUpsideDown_m631759142_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_autorotateToPortraitUpsideDown()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_autorotateToPortraitUpsideDown(System.Boolean)
extern "C"  void Screen_set_autorotateToPortraitUpsideDown_m493773611 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_autorotateToPortraitUpsideDown_m493773611_ftn) (bool);
	static Screen_set_autorotateToPortraitUpsideDown_m493773611_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_autorotateToPortraitUpsideDown_m493773611_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_autorotateToPortraitUpsideDown(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Screen::get_autorotateToLandscapeLeft()
extern "C"  bool Screen_get_autorotateToLandscapeLeft_m3574433901 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Screen_get_autorotateToLandscapeLeft_m3574433901_ftn) ();
	static Screen_get_autorotateToLandscapeLeft_m3574433901_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_autorotateToLandscapeLeft_m3574433901_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_autorotateToLandscapeLeft()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_autorotateToLandscapeLeft(System.Boolean)
extern "C"  void Screen_set_autorotateToLandscapeLeft_m3928884054 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_autorotateToLandscapeLeft_m3928884054_ftn) (bool);
	static Screen_set_autorotateToLandscapeLeft_m3928884054_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_autorotateToLandscapeLeft_m3928884054_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_autorotateToLandscapeLeft(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Screen::get_autorotateToLandscapeRight()
extern "C"  bool Screen_get_autorotateToLandscapeRight_m283511704 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Screen_get_autorotateToLandscapeRight_m283511704_ftn) ();
	static Screen_get_autorotateToLandscapeRight_m283511704_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_autorotateToLandscapeRight_m283511704_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_autorotateToLandscapeRight()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_autorotateToLandscapeRight(System.Boolean)
extern "C"  void Screen_set_autorotateToLandscapeRight_m158995741 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_autorotateToLandscapeRight_m158995741_ftn) (bool);
	static Screen_set_autorotateToLandscapeRight_m158995741_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_autorotateToLandscapeRight_m158995741_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_autorotateToLandscapeRight(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.ScreenOrientation UnityEngine.Screen::get_orientation()
extern "C"  int32_t Screen_get_orientation_m1193220576 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_orientation_m1193220576_ftn) ();
	static Screen_get_orientation_m1193220576_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_orientation_m1193220576_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_orientation()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_orientation(UnityEngine.ScreenOrientation)
extern "C"  void Screen_set_orientation_m931760051 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_orientation_m931760051_ftn) (int32_t);
	static Screen_set_orientation_m931760051_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_orientation_m931760051_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_orientation(UnityEngine.ScreenOrientation)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.Screen::get_sleepTimeout()
extern "C"  int32_t Screen_get_sleepTimeout_m4077361558 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_sleepTimeout_m4077361558_ftn) ();
	static Screen_get_sleepTimeout_m4077361558_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_sleepTimeout_m4077361558_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_sleepTimeout()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_sleepTimeout(System.Int32)
extern "C"  void Screen_set_sleepTimeout_m4188281051 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_sleepTimeout_m4188281051_ftn) (int32_t);
	static Screen_set_sleepTimeout_m4188281051_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_sleepTimeout_m4188281051_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_sleepTimeout(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m1827087273 (ScriptableObject_t2970544072 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m570634126(__this, /*hidden argument*/NULL);
		ScriptableObject_Internal_CreateScriptableObject_m2334361070(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
extern "C"  void ScriptableObject_Internal_CreateScriptableObject_m2334361070 (Il2CppObject * __this /* static, unused */, ScriptableObject_t2970544072 * ___self0, const MethodInfo* method)
{
	typedef void (*ScriptableObject_Internal_CreateScriptableObject_m2334361070_ftn) (ScriptableObject_t2970544072 *);
	static ScriptableObject_Internal_CreateScriptableObject_m2334361070_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_Internal_CreateScriptableObject_m2334361070_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)");
	_il2cpp_icall_func(___self0);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.String)
extern "C"  ScriptableObject_t2970544072 * ScriptableObject_CreateInstance_m750914562 (Il2CppObject * __this /* static, unused */, String_t* ___className0, const MethodInfo* method)
{
	typedef ScriptableObject_t2970544072 * (*ScriptableObject_CreateInstance_m750914562_ftn) (String_t*);
	static ScriptableObject_CreateInstance_m750914562_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstance_m750914562_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstance(System.String)");
	return _il2cpp_icall_func(___className0);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
extern "C"  ScriptableObject_t2970544072 * ScriptableObject_CreateInstance_m3255479417 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		ScriptableObject_t2970544072 * L_1 = ScriptableObject_CreateInstanceFromType_m3795352533(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
extern "C"  ScriptableObject_t2970544072 * ScriptableObject_CreateInstanceFromType_m3795352533 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ScriptableObject_t2970544072 * (*ScriptableObject_CreateInstanceFromType_m3795352533_ftn) (Type_t *);
	static ScriptableObject_CreateInstanceFromType_m3795352533_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstanceFromType_m3795352533_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2970544072_marshal_pinvoke(const ScriptableObject_t2970544072& unmarshaled, ScriptableObject_t2970544072_marshaled_pinvoke& marshaled)
{
	marshaled.___m_InstanceID_0 = unmarshaled.get_m_InstanceID_0();
	marshaled.___m_CachedPtr_1 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_1()).get_m_value_0());
}
extern "C" void ScriptableObject_t2970544072_marshal_pinvoke_back(const ScriptableObject_t2970544072_marshaled_pinvoke& marshaled, ScriptableObject_t2970544072& unmarshaled)
{
	int32_t unmarshaled_m_InstanceID_temp_0 = 0;
	unmarshaled_m_InstanceID_temp_0 = marshaled.___m_InstanceID_0;
	unmarshaled.set_m_InstanceID_0(unmarshaled_m_InstanceID_temp_0);
	IntPtr_t unmarshaled_m_CachedPtr_temp_1;
	memset(&unmarshaled_m_CachedPtr_temp_1, 0, sizeof(unmarshaled_m_CachedPtr_temp_1));
	IntPtr_t unmarshaled_m_CachedPtr_temp_1_temp;
	unmarshaled_m_CachedPtr_temp_1_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_CachedPtr_1));
	unmarshaled_m_CachedPtr_temp_1 = unmarshaled_m_CachedPtr_temp_1_temp;
	unmarshaled.set_m_CachedPtr_1(unmarshaled_m_CachedPtr_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2970544072_marshal_pinvoke_cleanup(ScriptableObject_t2970544072_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2970544072_marshal_com(const ScriptableObject_t2970544072& unmarshaled, ScriptableObject_t2970544072_marshaled_com& marshaled)
{
	marshaled.___m_InstanceID_0 = unmarshaled.get_m_InstanceID_0();
	marshaled.___m_CachedPtr_1 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_1()).get_m_value_0());
}
extern "C" void ScriptableObject_t2970544072_marshal_com_back(const ScriptableObject_t2970544072_marshaled_com& marshaled, ScriptableObject_t2970544072& unmarshaled)
{
	int32_t unmarshaled_m_InstanceID_temp_0 = 0;
	unmarshaled_m_InstanceID_temp_0 = marshaled.___m_InstanceID_0;
	unmarshaled.set_m_InstanceID_0(unmarshaled_m_InstanceID_temp_0);
	IntPtr_t unmarshaled_m_CachedPtr_temp_1;
	memset(&unmarshaled_m_CachedPtr_temp_1, 0, sizeof(unmarshaled_m_CachedPtr_temp_1));
	IntPtr_t unmarshaled_m_CachedPtr_temp_1_temp;
	unmarshaled_m_CachedPtr_temp_1_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_CachedPtr_1));
	unmarshaled_m_CachedPtr_temp_1 = unmarshaled_m_CachedPtr_temp_1_temp;
	unmarshaled.set_m_CachedPtr_1(unmarshaled_m_CachedPtr_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2970544072_marshal_com_cleanup(ScriptableObject_t2970544072_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::.ctor()
extern "C"  void RequiredByNativeCodeAttribute__ctor_m940065582 (RequiredByNativeCodeAttribute_t3165457172 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.UsedByNativeCodeAttribute::.ctor()
extern "C"  void UsedByNativeCodeAttribute__ctor_m3320039756 (UsedByNativeCodeAttribute_t3197444790 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Security::.ctor()
extern "C"  void Security__ctor_m2182224429 (Security_t225689028 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Security::PrefetchSocketPolicy(System.String,System.Int32)
extern "C"  bool Security_PrefetchSocketPolicy_m682431054 (Il2CppObject * __this /* static, unused */, String_t* ___ip0, int32_t ___atPort1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = ((int32_t)3000);
		String_t* L_0 = ___ip0;
		int32_t L_1 = ___atPort1;
		int32_t L_2 = V_0;
		bool L_3 = Security_PrefetchSocketPolicy_m1901508937(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.Security::PrefetchSocketPolicy(System.String,System.Int32,System.Int32)
extern "C"  bool Security_PrefetchSocketPolicy_m1901508937 (Il2CppObject * __this /* static, unused */, String_t* ___ip0, int32_t ___atPort1, int32_t ___timeout2, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Reflection.Assembly UnityEngine.Security::LoadAndVerifyAssembly(System.Byte[],System.String)
extern "C"  Assembly_t1418687608 * Security_LoadAndVerifyAssembly_m2336320367 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___assemblyData0, String_t* ___authorizationKey1, const MethodInfo* method)
{
	{
		return (Assembly_t1418687608 *)NULL;
	}
}
// System.Reflection.Assembly UnityEngine.Security::LoadAndVerifyAssembly(System.Byte[])
extern "C"  Assembly_t1418687608 * Security_LoadAndVerifyAssembly_m428181619 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___assemblyData0, const MethodInfo* method)
{
	{
		return (Assembly_t1418687608 *)NULL;
	}
}
// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern "C"  void SelectionBaseAttribute__ctor_m3830336046 (SelectionBaseAttribute_t204085987 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::.cctor()
extern Il2CppClass* SendMouseEvents_t3965481900_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfoU5BU5D_t3452915852_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfo_t3209134097_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents__cctor_m2731695210_MetadataUsageId;
extern "C"  void SendMouseEvents__cctor_m2731695210 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents__cctor_m2731695210_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	HitInfo_t3209134097  V_0;
	memset(&V_0, 0, sizeof(V_0));
	HitInfo_t3209134097  V_1;
	memset(&V_1, 0, sizeof(V_1));
	HitInfo_t3209134097  V_2;
	memset(&V_2, 0, sizeof(V_2));
	HitInfo_t3209134097  V_3;
	memset(&V_3, 0, sizeof(V_3));
	HitInfo_t3209134097  V_4;
	memset(&V_4, 0, sizeof(V_4));
	HitInfo_t3209134097  V_5;
	memset(&V_5, 0, sizeof(V_5));
	HitInfo_t3209134097  V_6;
	memset(&V_6, 0, sizeof(V_6));
	HitInfo_t3209134097  V_7;
	memset(&V_7, 0, sizeof(V_7));
	HitInfo_t3209134097  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_s_MouseUsed_0((bool)0);
		HitInfoU5BU5D_t3452915852* L_0 = ((HitInfoU5BU5D_t3452915852*)SZArrayNew(HitInfoU5BU5D_t3452915852_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_0));
		HitInfo_t3209134097  L_1 = V_0;
		(*(HitInfo_t3209134097 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_1;
		HitInfoU5BU5D_t3452915852* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_1));
		HitInfo_t3209134097  L_3 = V_1;
		(*(HitInfo_t3209134097 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_3;
		HitInfoU5BU5D_t3452915852* L_4 = L_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t3209134097  L_5 = V_2;
		(*(HitInfo_t3209134097 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_5;
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_m_LastHit_1(L_4);
		HitInfoU5BU5D_t3452915852* L_6 = ((HitInfoU5BU5D_t3452915852*)SZArrayNew(HitInfoU5BU5D_t3452915852_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_3));
		HitInfo_t3209134097  L_7 = V_3;
		(*(HitInfo_t3209134097 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_7;
		HitInfoU5BU5D_t3452915852* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_4));
		HitInfo_t3209134097  L_9 = V_4;
		(*(HitInfo_t3209134097 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_9;
		HitInfoU5BU5D_t3452915852* L_10 = L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_5));
		HitInfo_t3209134097  L_11 = V_5;
		(*(HitInfo_t3209134097 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_11;
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_m_MouseDownHit_2(L_10);
		HitInfoU5BU5D_t3452915852* L_12 = ((HitInfoU5BU5D_t3452915852*)SZArrayNew(HitInfoU5BU5D_t3452915852_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_6));
		HitInfo_t3209134097  L_13 = V_6;
		(*(HitInfo_t3209134097 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_13;
		HitInfoU5BU5D_t3452915852* L_14 = L_12;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_7));
		HitInfo_t3209134097  L_15 = V_7;
		(*(HitInfo_t3209134097 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_15;
		HitInfoU5BU5D_t3452915852* L_16 = L_14;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_8));
		HitInfo_t3209134097  L_17 = V_8;
		(*(HitInfo_t3209134097 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_17;
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_m_CurrentHit_3(L_16);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SetMouseMoved()
extern Il2CppClass* SendMouseEvents_t3965481900_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents_SetMouseMoved_m2590456785_MetadataUsageId;
extern "C"  void SendMouseEvents_SetMouseMoved_m2590456785 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_SetMouseMoved_m2590456785_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_s_MouseUsed_0((bool)1);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::DoSendMouseEvents(System.Int32)
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* SendMouseEvents_t3965481900_il2cpp_TypeInfo_var;
extern Il2CppClass* CameraU5BU5D_t2716570836_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfo_t3209134097_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisGUILayer_t2983897946_m2891371969_MethodInfo_var;
extern const uint32_t SendMouseEvents_DoSendMouseEvents_m4104134333_MetadataUsageId;
extern "C"  void SendMouseEvents_DoSendMouseEvents_m4104134333 (Il2CppObject * __this /* static, unused */, int32_t ___skipRTCameras0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_DoSendMouseEvents_m4104134333_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Camera_t2727095145 * V_3 = NULL;
	CameraU5BU5D_t2716570836* V_4 = NULL;
	int32_t V_5 = 0;
	Rect_t4241904616  V_6;
	memset(&V_6, 0, sizeof(V_6));
	GUILayer_t2983897946 * V_7 = NULL;
	GUIElement_t3775428101 * V_8 = NULL;
	Ray_t3134616544  V_9;
	memset(&V_9, 0, sizeof(V_9));
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	GameObject_t3674682005 * V_12 = NULL;
	GameObject_t3674682005 * V_13 = NULL;
	int32_t V_14 = 0;
	HitInfo_t3209134097  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector3_t4282066566  V_16;
	memset(&V_16, 0, sizeof(V_16));
	float G_B23_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_0 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Camera_get_allCamerasCount_m3993434431(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		CameraU5BU5D_t2716570836* L_2 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		CameraU5BU5D_t2716570836* L_3 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		NullCheck(L_3);
		int32_t L_4 = V_1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))) == ((int32_t)L_4)))
		{
			goto IL_002e;
		}
	}

IL_0023:
	{
		int32_t L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_m_Cameras_4(((CameraU5BU5D_t2716570836*)SZArrayNew(CameraU5BU5D_t2716570836_il2cpp_TypeInfo_var, (uint32_t)L_5)));
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		CameraU5BU5D_t2716570836* L_6 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		Camera_GetAllCameras_m3771867787(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_005e;
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_7 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_15));
		HitInfo_t3209134097  L_9 = V_15;
		(*(HitInfo_t3209134097 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))) = L_9;
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_12 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		bool L_13 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_s_MouseUsed_0();
		if (L_13)
		{
			goto IL_02c3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		CameraU5BU5D_t2716570836* L_14 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		V_4 = L_14;
		V_5 = 0;
		goto IL_02b8;
	}

IL_0084:
	{
		CameraU5BU5D_t2716570836* L_15 = V_4;
		int32_t L_16 = V_5;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		Camera_t2727095145 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_3 = L_18;
		Camera_t2727095145 * L_19 = V_3;
		bool L_20 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_19, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_21 = ___skipRTCameras0;
		if (!L_21)
		{
			goto IL_00b2;
		}
	}
	{
		Camera_t2727095145 * L_22 = V_3;
		NullCheck(L_22);
		RenderTexture_t1963041563 * L_23 = Camera_get_targetTexture_m1468336738(L_22, /*hidden argument*/NULL);
		bool L_24 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_23, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00b2;
		}
	}

IL_00ad:
	{
		goto IL_02b2;
	}

IL_00b2:
	{
		Camera_t2727095145 * L_25 = V_3;
		NullCheck(L_25);
		Rect_t4241904616  L_26 = Camera_get_pixelRect_m936851539(L_25, /*hidden argument*/NULL);
		V_6 = L_26;
		Vector3_t4282066566  L_27 = V_0;
		bool L_28 = Rect_Contains_m3556594041((&V_6), L_27, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_00cc;
		}
	}
	{
		goto IL_02b2;
	}

IL_00cc:
	{
		Camera_t2727095145 * L_29 = V_3;
		NullCheck(L_29);
		GUILayer_t2983897946 * L_30 = Component_GetComponent_TisGUILayer_t2983897946_m2891371969(L_29, /*hidden argument*/Component_GetComponent_TisGUILayer_t2983897946_m2891371969_MethodInfo_var);
		V_7 = L_30;
		GUILayer_t2983897946 * L_31 = V_7;
		bool L_32 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0145;
		}
	}
	{
		GUILayer_t2983897946 * L_33 = V_7;
		Vector3_t4282066566  L_34 = V_0;
		NullCheck(L_33);
		GUIElement_t3775428101 * L_35 = GUILayer_HitTest_m3356120918(L_33, L_34, /*hidden argument*/NULL);
		V_8 = L_35;
		GUIElement_t3775428101 * L_36 = V_8;
		bool L_37 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0123;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_38 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		GUIElement_t3775428101 * L_39 = V_8;
		NullCheck(L_39);
		GameObject_t3674682005 * L_40 = Component_get_gameObject_m1170635899(L_39, /*hidden argument*/NULL);
		((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_target_0(L_40);
		HitInfoU5BU5D_t3452915852* L_41 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 0);
		Camera_t2727095145 * L_42 = V_3;
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_camera_1(L_42);
		goto IL_0145;
	}

IL_0123:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_43 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 0);
		((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_target_0((GameObject_t3674682005 *)NULL);
		HitInfoU5BU5D_t3452915852* L_44 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, 0);
		((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_camera_1((Camera_t2727095145 *)NULL);
	}

IL_0145:
	{
		Camera_t2727095145 * L_45 = V_3;
		NullCheck(L_45);
		int32_t L_46 = Camera_get_eventMask_m3669132771(L_45, /*hidden argument*/NULL);
		if (L_46)
		{
			goto IL_0155;
		}
	}
	{
		goto IL_02b2;
	}

IL_0155:
	{
		Camera_t2727095145 * L_47 = V_3;
		Vector3_t4282066566  L_48 = V_0;
		NullCheck(L_47);
		Ray_t3134616544  L_49 = Camera_ScreenPointToRay_m1733083890(L_47, L_48, /*hidden argument*/NULL);
		V_9 = L_49;
		Vector3_t4282066566  L_50 = Ray_get_direction_m3201866877((&V_9), /*hidden argument*/NULL);
		V_16 = L_50;
		float L_51 = (&V_16)->get_z_3();
		V_10 = L_51;
		float L_52 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		bool L_53 = Mathf_Approximately_m1395529776(NULL /*static, unused*/, (0.0f), L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_018b;
		}
	}
	{
		G_B23_0 = (std::numeric_limits<float>::infinity());
		goto IL_01a0;
	}

IL_018b:
	{
		Camera_t2727095145 * L_54 = V_3;
		NullCheck(L_54);
		float L_55 = Camera_get_farClipPlane_m388706726(L_54, /*hidden argument*/NULL);
		Camera_t2727095145 * L_56 = V_3;
		NullCheck(L_56);
		float L_57 = Camera_get_nearClipPlane_m4074655061(L_56, /*hidden argument*/NULL);
		float L_58 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_59 = fabsf(((float)((float)((float)((float)L_55-(float)L_57))/(float)L_58)));
		G_B23_0 = L_59;
	}

IL_01a0:
	{
		V_11 = G_B23_0;
		Camera_t2727095145 * L_60 = V_3;
		Ray_t3134616544  L_61 = V_9;
		float L_62 = V_11;
		Camera_t2727095145 * L_63 = V_3;
		NullCheck(L_63);
		int32_t L_64 = Camera_get_cullingMask_m1045975289(L_63, /*hidden argument*/NULL);
		Camera_t2727095145 * L_65 = V_3;
		NullCheck(L_65);
		int32_t L_66 = Camera_get_eventMask_m3669132771(L_65, /*hidden argument*/NULL);
		NullCheck(L_60);
		GameObject_t3674682005 * L_67 = Camera_RaycastTry_m569221064(L_60, L_61, L_62, ((int32_t)((int32_t)L_64&(int32_t)L_66)), /*hidden argument*/NULL);
		V_12 = L_67;
		GameObject_t3674682005 * L_68 = V_12;
		bool L_69 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_68, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_01f0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_70 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, 1);
		GameObject_t3674682005 * L_71 = V_12;
		((L_70)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_target_0(L_71);
		HitInfoU5BU5D_t3452915852* L_72 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_72);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_72, 1);
		Camera_t2727095145 * L_73 = V_3;
		((L_72)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_camera_1(L_73);
		goto IL_022a;
	}

IL_01f0:
	{
		Camera_t2727095145 * L_74 = V_3;
		NullCheck(L_74);
		int32_t L_75 = Camera_get_clearFlags_m192466552(L_74, /*hidden argument*/NULL);
		if ((((int32_t)L_75) == ((int32_t)1)))
		{
			goto IL_0208;
		}
	}
	{
		Camera_t2727095145 * L_76 = V_3;
		NullCheck(L_76);
		int32_t L_77 = Camera_get_clearFlags_m192466552(L_76, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_77) == ((uint32_t)2))))
		{
			goto IL_022a;
		}
	}

IL_0208:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_78 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_78);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_78, 1);
		((L_78)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_target_0((GameObject_t3674682005 *)NULL);
		HitInfoU5BU5D_t3452915852* L_79 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, 1);
		((L_79)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_camera_1((Camera_t2727095145 *)NULL);
	}

IL_022a:
	{
		Camera_t2727095145 * L_80 = V_3;
		Ray_t3134616544  L_81 = V_9;
		float L_82 = V_11;
		Camera_t2727095145 * L_83 = V_3;
		NullCheck(L_83);
		int32_t L_84 = Camera_get_cullingMask_m1045975289(L_83, /*hidden argument*/NULL);
		Camera_t2727095145 * L_85 = V_3;
		NullCheck(L_85);
		int32_t L_86 = Camera_get_eventMask_m3669132771(L_85, /*hidden argument*/NULL);
		NullCheck(L_80);
		GameObject_t3674682005 * L_87 = Camera_RaycastTry2D_m3256311322(L_80, L_81, L_82, ((int32_t)((int32_t)L_84&(int32_t)L_86)), /*hidden argument*/NULL);
		V_13 = L_87;
		GameObject_t3674682005 * L_88 = V_13;
		bool L_89 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_88, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_89)
		{
			goto IL_0278;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_90 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_90);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_90, 2);
		GameObject_t3674682005 * L_91 = V_13;
		((L_90)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_target_0(L_91);
		HitInfoU5BU5D_t3452915852* L_92 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_92);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_92, 2);
		Camera_t2727095145 * L_93 = V_3;
		((L_92)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_camera_1(L_93);
		goto IL_02b2;
	}

IL_0278:
	{
		Camera_t2727095145 * L_94 = V_3;
		NullCheck(L_94);
		int32_t L_95 = Camera_get_clearFlags_m192466552(L_94, /*hidden argument*/NULL);
		if ((((int32_t)L_95) == ((int32_t)1)))
		{
			goto IL_0290;
		}
	}
	{
		Camera_t2727095145 * L_96 = V_3;
		NullCheck(L_96);
		int32_t L_97 = Camera_get_clearFlags_m192466552(L_96, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_97) == ((uint32_t)2))))
		{
			goto IL_02b2;
		}
	}

IL_0290:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_98 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_98);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_98, 2);
		((L_98)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_target_0((GameObject_t3674682005 *)NULL);
		HitInfoU5BU5D_t3452915852* L_99 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_99);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_99, 2);
		((L_99)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_camera_1((Camera_t2727095145 *)NULL);
	}

IL_02b2:
	{
		int32_t L_100 = V_5;
		V_5 = ((int32_t)((int32_t)L_100+(int32_t)1));
	}

IL_02b8:
	{
		int32_t L_101 = V_5;
		CameraU5BU5D_t2716570836* L_102 = V_4;
		NullCheck(L_102);
		if ((((int32_t)L_101) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_102)->max_length)))))))
		{
			goto IL_0084;
		}
	}

IL_02c3:
	{
		V_14 = 0;
		goto IL_02e9;
	}

IL_02cb:
	{
		int32_t L_103 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_104 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		int32_t L_105 = V_14;
		NullCheck(L_104);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_104, L_105);
		SendMouseEvents_SendEvents_m3877180750(NULL /*static, unused*/, L_103, (*(HitInfo_t3209134097 *)((L_104)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_105)))), /*hidden argument*/NULL);
		int32_t L_106 = V_14;
		V_14 = ((int32_t)((int32_t)L_106+(int32_t)1));
	}

IL_02e9:
	{
		int32_t L_107 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_108 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_108);
		if ((((int32_t)L_107) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_108)->max_length)))))))
		{
			goto IL_02cb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_s_MouseUsed_0((bool)0);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* SendMouseEvents_t3965481900_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfo_t3209134097_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2222972840;
extern Il2CppCodeGenString* _stringLiteral2438936645;
extern Il2CppCodeGenString* _stringLiteral288346913;
extern Il2CppCodeGenString* _stringLiteral2222975034;
extern Il2CppCodeGenString* _stringLiteral2223306714;
extern Il2CppCodeGenString* _stringLiteral2223010852;
extern Il2CppCodeGenString* _stringLiteral193571986;
extern const uint32_t SendMouseEvents_SendEvents_m3877180750_MetadataUsageId;
extern "C"  void SendMouseEvents_SendEvents_m3877180750 (Il2CppObject * __this /* static, unused */, int32_t ___i0, HitInfo_t3209134097  ___hit1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_SendEvents_m3877180750_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	HitInfo_t3209134097  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m2031691843(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = Input_GetMouseButton_m4080958081(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_004a;
		}
	}
	{
		HitInfo_t3209134097  L_3 = ___hit1;
		bool L_4 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0045;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_5 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_6 = ___i0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		HitInfo_t3209134097  L_7 = ___hit1;
		(*(HitInfo_t3209134097 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))) = L_7;
		HitInfoU5BU5D_t3452915852* L_8 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_9 = ___i0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		HitInfo_SendMessage_m2569183060(((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))), _stringLiteral2222972840, /*hidden argument*/NULL);
	}

IL_0045:
	{
		goto IL_00fc;
	}

IL_004a:
	{
		bool L_10 = V_1;
		if (L_10)
		{
			goto IL_00cd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_11 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_12 = ___i0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		bool L_13 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, (*(HitInfo_t3209134097 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))), /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00c8;
		}
	}
	{
		HitInfo_t3209134097  L_14 = ___hit1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_15 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_16 = ___i0;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		bool L_17 = HitInfo_Compare_m4083757090(NULL /*static, unused*/, L_14, (*(HitInfo_t3209134097 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16)))), /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_009a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_18 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_19 = ___i0;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		HitInfo_SendMessage_m2569183060(((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_19))), _stringLiteral2438936645, /*hidden argument*/NULL);
	}

IL_009a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_20 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_21 = ___i0;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		HitInfo_SendMessage_m2569183060(((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21))), _stringLiteral288346913, /*hidden argument*/NULL);
		HitInfoU5BU5D_t3452915852* L_22 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_23 = ___i0;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t3209134097  L_24 = V_2;
		(*(HitInfo_t3209134097 *)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_23)))) = L_24;
	}

IL_00c8:
	{
		goto IL_00fc;
	}

IL_00cd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_25 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_26 = ___i0;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		bool L_27 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, (*(HitInfo_t3209134097 *)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))), /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00fc;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_28 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_29 = ___i0;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		HitInfo_SendMessage_m2569183060(((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_29))), _stringLiteral2222975034, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		HitInfo_t3209134097  L_30 = ___hit1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_31 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_32 = ___i0;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		bool L_33 = HitInfo_Compare_m4083757090(NULL /*static, unused*/, L_30, (*(HitInfo_t3209134097 *)((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_32)))), /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0133;
		}
	}
	{
		HitInfo_t3209134097  L_34 = ___hit1;
		bool L_35 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_012e;
		}
	}
	{
		HitInfo_SendMessage_m2569183060((&___hit1), _stringLiteral2223306714, /*hidden argument*/NULL);
	}

IL_012e:
	{
		goto IL_0185;
	}

IL_0133:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_36 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_37 = ___i0;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		bool L_38 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, (*(HitInfo_t3209134097 *)((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))), /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0162;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_39 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_40 = ___i0;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		HitInfo_SendMessage_m2569183060(((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40))), _stringLiteral2223010852, /*hidden argument*/NULL);
	}

IL_0162:
	{
		HitInfo_t3209134097  L_41 = ___hit1;
		bool L_42 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0185;
		}
	}
	{
		HitInfo_SendMessage_m2569183060((&___hit1), _stringLiteral193571986, /*hidden argument*/NULL);
		HitInfo_SendMessage_m2569183060((&___hit1), _stringLiteral2223306714, /*hidden argument*/NULL);
	}

IL_0185:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_43 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_44 = ___i0;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		HitInfo_t3209134097  L_45 = ___hit1;
		(*(HitInfo_t3209134097 *)((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_44)))) = L_45;
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern "C"  void HitInfo_SendMessage_m2569183060 (HitInfo_t3209134097 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = __this->get_target_0();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		GameObject_SendMessage_m423373689(L_0, L_1, NULL, 1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void HitInfo_SendMessage_m2569183060_AdjustorThunk (Il2CppObject * __this, String_t* ___name0, const MethodInfo* method)
{
	HitInfo_t3209134097 * _thisAdjusted = reinterpret_cast<HitInfo_t3209134097 *>(__this + 1);
	HitInfo_SendMessage_m2569183060(_thisAdjusted, ___name0, method);
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_Compare_m4083757090 (Il2CppObject * __this /* static, unused */, HitInfo_t3209134097  ___lhs0, HitInfo_t3209134097  ___rhs1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		GameObject_t3674682005 * L_0 = (&___lhs0)->get_target_0();
		GameObject_t3674682005 * L_1 = (&___rhs1)->get_target_0();
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		Camera_t2727095145 * L_3 = (&___lhs0)->get_camera_1();
		Camera_t2727095145 * L_4 = (&___rhs1)->get_camera_1();
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_op_Implicit_m1943931337 (Il2CppObject * __this /* static, unused */, HitInfo_t3209134097  ___exists0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		GameObject_t3674682005 * L_0 = (&___exists0)->get_target_0();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Camera_t2727095145 * L_2 = (&___exists0)->get_camera_1();
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return (bool)G_B3_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t3209134097_marshal_pinvoke(const HitInfo_t3209134097& unmarshaled, HitInfo_t3209134097_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
extern "C" void HitInfo_t3209134097_marshal_pinvoke_back(const HitInfo_t3209134097_marshaled_pinvoke& marshaled, HitInfo_t3209134097& unmarshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t3209134097_marshal_pinvoke_cleanup(HitInfo_t3209134097_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t3209134097_marshal_com(const HitInfo_t3209134097& unmarshaled, HitInfo_t3209134097_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
extern "C" void HitInfo_t3209134097_marshal_com_back(const HitInfo_t3209134097_marshaled_com& marshaled, HitInfo_t3209134097& unmarshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t3209134097_marshal_com_cleanup(HitInfo_t3209134097_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern "C"  void FormerlySerializedAsAttribute__ctor_m2703462003 (FormerlySerializedAsAttribute_t2216353654 * __this, String_t* ___oldName0, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___oldName0;
		__this->set_m_oldName_0(L_0);
		return;
	}
}
// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::get_oldName()
extern "C"  String_t* FormerlySerializedAsAttribute_get_oldName_m2935479929 (FormerlySerializedAsAttribute_t2216353654 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_oldName_0();
		return L_0;
	}
}
// System.Void UnityEngine.Serialization.ListSerializationSurrogate::.ctor()
extern "C"  void ListSerializationSurrogate__ctor_m2833832457 (ListSerializationSurrogate_t3207893374 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Serialization.ListSerializationSurrogate::.cctor()
extern Il2CppClass* ListSerializationSurrogate_t3207893374_il2cpp_TypeInfo_var;
extern const uint32_t ListSerializationSurrogate__cctor_m1467364036_MetadataUsageId;
extern "C"  void ListSerializationSurrogate__cctor_m1467364036 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListSerializationSurrogate__cctor_m1467364036_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ListSerializationSurrogate_t3207893374 * L_0 = (ListSerializationSurrogate_t3207893374 *)il2cpp_codegen_object_new(ListSerializationSurrogate_t3207893374_il2cpp_TypeInfo_var);
		ListSerializationSurrogate__ctor_m2833832457(L_0, /*hidden argument*/NULL);
		((ListSerializationSurrogate_t3207893374_StaticFields*)ListSerializationSurrogate_t3207893374_il2cpp_TypeInfo_var->static_fields)->set_Default_0(L_0);
		return;
	}
}
// System.Object UnityEngine.Serialization.ListSerializationSurrogate::SetObjectData(System.Object,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector)
extern const Il2CppType* IEnumerable_t3464557803_0_0_0_var;
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91265248;
extern Il2CppCodeGenString* _stringLiteral2820295361;
extern const uint32_t ListSerializationSurrogate_SetObjectData_m2586542907_MetadataUsageId;
extern "C"  Il2CppObject * ListSerializationSurrogate_SetObjectData_m2586542907 (ListSerializationSurrogate_t3207893374 * __this, Il2CppObject * ___obj0, SerializationInfo_t2185721892 * ___info1, StreamingContext_t2761351129  ___context2, Il2CppObject * ___selector3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListSerializationSurrogate_SetObjectData_m2586542907_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	int32_t V_3 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m2022236990(L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = ((Il2CppObject *)Castclass(L_2, IList_t1751339649_il2cpp_TypeInfo_var));
		SerializationInfo_t2185721892 * L_3 = ___info1;
		NullCheck(L_3);
		int32_t L_4 = SerializationInfo_GetInt32_m4048035953(L_3, _stringLiteral91265248, /*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5 = V_1;
		if (L_5)
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject * L_6 = V_0;
		return L_6;
	}

IL_0025:
	{
		SerializationInfo_t2185721892 * L_7 = ___info1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(IEnumerable_t3464557803_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_7);
		Il2CppObject * L_9 = SerializationInfo_GetValue_m4125471336(L_7, _stringLiteral2820295361, L_8, /*hidden argument*/NULL);
		NullCheck(((Il2CppObject *)Castclass(L_9, IEnumerable_t3464557803_il2cpp_TypeInfo_var)));
		Il2CppObject * L_10 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, ((Il2CppObject *)Castclass(L_9, IEnumerable_t3464557803_il2cpp_TypeInfo_var)));
		V_2 = L_10;
		V_3 = 0;
		goto IL_006e;
	}

IL_004c:
	{
		Il2CppObject * L_11 = V_2;
		NullCheck(L_11);
		bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_11);
		if (L_12)
		{
			goto IL_005d;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_13 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_005d:
	{
		Il2CppObject * L_14 = V_0;
		Il2CppObject * L_15 = V_2;
		NullCheck(L_15);
		Il2CppObject * L_16 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_15);
		NullCheck(L_14);
		InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t1751339649_il2cpp_TypeInfo_var, L_14, L_16);
		int32_t L_17 = V_3;
		V_3 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_006e:
	{
		int32_t L_18 = V_3;
		int32_t L_19 = V_1;
		if ((((int32_t)L_18) < ((int32_t)L_19)))
		{
			goto IL_004c;
		}
	}
	{
		Il2CppObject * L_20 = V_0;
		return L_20;
	}
}
// System.Void UnityEngine.Serialization.UnitySurrogateSelector::.ctor()
extern "C"  void UnitySurrogateSelector__ctor_m3965242029 (UnitySurrogateSelector_t780726490 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Runtime.Serialization.ISerializationSurrogate UnityEngine.Serialization.UnitySurrogateSelector::GetSurrogate(System.Type,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector&)
extern const Il2CppType* List_1_t951551555_0_0_0_var;
extern const Il2CppType* Dictionary_2_t898371836_0_0_0_var;
extern const Il2CppType* DictionarySerializationSurrogate_2_t375940808_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ListSerializationSurrogate_t3207893374_il2cpp_TypeInfo_var;
extern Il2CppClass* ISerializationSurrogate_t132171319_il2cpp_TypeInfo_var;
extern const uint32_t UnitySurrogateSelector_GetSurrogate_m1626387878_MetadataUsageId;
extern "C"  Il2CppObject * UnitySurrogateSelector_GetSurrogate_m1626387878 (UnitySurrogateSelector_t780726490 * __this, Type_t * ___type0, StreamingContext_t2761351129  ___context1, Il2CppObject ** ___selector2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnitySurrogateSelector_GetSurrogate_m1626387878_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(96 /* System.Boolean System.Type::get_IsGenericType() */, L_0);
		if (!L_1)
		{
			goto IL_0060;
		}
	}
	{
		Type_t * L_2 = ___type0;
		NullCheck(L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(95 /* System.Type System.Type::GetGenericTypeDefinition() */, L_2);
		V_0 = L_3;
		Type_t * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(List_1_t951551555_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5))))
		{
			goto IL_002b;
		}
	}
	{
		Il2CppObject ** L_6 = ___selector2;
		*((Il2CppObject **)(L_6)) = (Il2CppObject *)__this;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_6), (Il2CppObject *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(ListSerializationSurrogate_t3207893374_il2cpp_TypeInfo_var);
		Il2CppObject * L_7 = ((ListSerializationSurrogate_t3207893374_StaticFields*)ListSerializationSurrogate_t3207893374_il2cpp_TypeInfo_var->static_fields)->get_Default_0();
		return L_7;
	}

IL_002b:
	{
		Type_t * L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Dictionary_2_t898371836_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_8) == ((Il2CppObject*)(Type_t *)L_9))))
		{
			goto IL_0060;
		}
	}
	{
		Il2CppObject ** L_10 = ___selector2;
		*((Il2CppObject **)(L_10)) = (Il2CppObject *)__this;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_10), (Il2CppObject *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(DictionarySerializationSurrogate_2_t375940808_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_12 = ___type0;
		NullCheck(L_12);
		TypeU5BU5D_t3339007067* L_13 = VirtFuncInvoker0< TypeU5BU5D_t3339007067* >::Invoke(92 /* System.Type[] System.Type::GetGenericArguments() */, L_12);
		NullCheck(L_11);
		Type_t * L_14 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(97 /* System.Type System.Type::MakeGenericType(System.Type[]) */, L_11, L_13);
		V_1 = L_14;
		Type_t * L_15 = V_1;
		Il2CppObject * L_16 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_16, ISerializationSurrogate_t132171319_il2cpp_TypeInfo_var));
	}

IL_0060:
	{
		Il2CppObject ** L_17 = ___selector2;
		*((Il2CppObject **)(L_17)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_17), (Il2CppObject *)NULL);
		return (Il2CppObject *)NULL;
	}
}
// System.Void UnityEngine.Serialization.UnitySurrogateSelector::ChainSelector(System.Runtime.Serialization.ISurrogateSelector)
extern Il2CppClass* NotImplementedException_t1912495542_il2cpp_TypeInfo_var;
extern const uint32_t UnitySurrogateSelector_ChainSelector_m1112345606_MetadataUsageId;
extern "C"  void UnitySurrogateSelector_ChainSelector_m1112345606 (UnitySurrogateSelector_t780726490 * __this, Il2CppObject * ___selector0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnitySurrogateSelector_ChainSelector_m1112345606_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1912495542 * L_0 = (NotImplementedException_t1912495542 *)il2cpp_codegen_object_new(NotImplementedException_t1912495542_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2063223793(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Runtime.Serialization.ISurrogateSelector UnityEngine.Serialization.UnitySurrogateSelector::GetNextSelector()
extern Il2CppClass* NotImplementedException_t1912495542_il2cpp_TypeInfo_var;
extern const uint32_t UnitySurrogateSelector_GetNextSelector_m2435567713_MetadataUsageId;
extern "C"  Il2CppObject * UnitySurrogateSelector_GetNextSelector_m2435567713 (UnitySurrogateSelector_t780726490 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnitySurrogateSelector_GetNextSelector_m2435567713_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1912495542 * L_0 = (NotImplementedException_t1912495542 *)il2cpp_codegen_object_new(NotImplementedException_t1912495542_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2063223793(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.SerializeField::.ctor()
extern "C"  void SerializeField__ctor_m4068807987 (SerializeField_t3754825534 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SerializePrivateVariables::.ctor()
extern "C"  void SerializePrivateVariables__ctor_m889466149 (SerializePrivateVariables_t2835496234 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SetupCoroutine::InvokeMoveNext(System.Collections.IEnumerator,System.IntPtr)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1859371625;
extern Il2CppCodeGenString* _stringLiteral2712312467;
extern const uint32_t SetupCoroutine_InvokeMoveNext_m3556339879_MetadataUsageId;
extern "C"  void SetupCoroutine_InvokeMoveNext_m3556339879 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___enumerator0, IntPtr_t ___returnValueAddress1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetupCoroutine_InvokeMoveNext_m3556339879_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___returnValueAddress1;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m72843924(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		ArgumentException_t928607144 * L_3 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_3, _stringLiteral1859371625, _stringLiteral2712312467, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0020:
	{
		IntPtr_t L_4 = ___returnValueAddress1;
		void* L_5 = IntPtr_op_Explicit_m2322222010(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Il2CppObject * L_6 = ___enumerator0;
		NullCheck(L_6);
		bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_6);
		*((int8_t*)(L_5)) = (int8_t)L_7;
		return;
	}
}
// System.Object UnityEngine.SetupCoroutine::InvokeMember(System.Object,System.String,System.Object)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t SetupCoroutine_InvokeMember_m3691215301_MetadataUsageId;
extern "C"  Il2CppObject * SetupCoroutine_InvokeMember_m3691215301 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___behaviour0, String_t* ___name1, Il2CppObject * ___variable2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetupCoroutine_InvokeMember_m3691215301_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1108656482* V_0 = NULL;
	{
		V_0 = (ObjectU5BU5D_t1108656482*)NULL;
		Il2CppObject * L_0 = ___variable2;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		V_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		ObjectU5BU5D_t1108656482* L_1 = V_0;
		Il2CppObject * L_2 = ___variable2;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
	}

IL_0013:
	{
		Il2CppObject * L_3 = ___behaviour0;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m2022236990(L_3, /*hidden argument*/NULL);
		String_t* L_5 = ___name1;
		Il2CppObject * L_6 = ___behaviour0;
		ObjectU5BU5D_t1108656482* L_7 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_8 = VirtFuncInvoker8< Il2CppObject *, String_t*, int32_t, Binder_t1074302268 *, Il2CppObject *, ObjectU5BU5D_t1108656482*, ParameterModifierU5BU5D_t3896472559*, CultureInfo_t1065375142 *, StringU5BU5D_t4054002952* >::Invoke(91 /* System.Object System.Type::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[]) */, L_4, L_5, ((int32_t)308), (Binder_t1074302268 *)NULL, L_6, L_7, (ParameterModifierU5BU5D_t3896472559*)(ParameterModifierU5BU5D_t3896472559*)NULL, (CultureInfo_t1065375142 *)NULL, (StringU5BU5D_t4054002952*)(StringU5BU5D_t4054002952*)NULL);
		return L_8;
	}
}
// System.Void UnityEngine.Shader::.ctor()
extern "C"  void Shader__ctor_m2878158248 (Shader_t3191267369 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m570634126(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Shader UnityEngine.Shader::Find(System.String)
extern "C"  Shader_t3191267369 * Shader_Find_m4048047578 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	typedef Shader_t3191267369 * (*Shader_Find_m4048047578_ftn) (String_t*);
	static Shader_Find_m4048047578_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_Find_m4048047578_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::Find(System.String)");
	return _il2cpp_icall_func(___name0);
}
// System.Boolean UnityEngine.Shader::get_isSupported()
extern "C"  bool Shader_get_isSupported_m1422621179 (Shader_t3191267369 * __this, const MethodInfo* method)
{
	typedef bool (*Shader_get_isSupported_m1422621179_ftn) (Shader_t3191267369 *);
	static Shader_get_isSupported_m1422621179_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_get_isSupported_m1422621179_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::get_isSupported()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Shader::EnableKeyword(System.String)
extern "C"  void Shader_EnableKeyword_m944528214 (Il2CppObject * __this /* static, unused */, String_t* ___keyword0, const MethodInfo* method)
{
	typedef void (*Shader_EnableKeyword_m944528214_ftn) (String_t*);
	static Shader_EnableKeyword_m944528214_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_EnableKeyword_m944528214_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::EnableKeyword(System.String)");
	_il2cpp_icall_func(___keyword0);
}
// System.Void UnityEngine.Shader::DisableKeyword(System.String)
extern "C"  void Shader_DisableKeyword_m2163321765 (Il2CppObject * __this /* static, unused */, String_t* ___keyword0, const MethodInfo* method)
{
	typedef void (*Shader_DisableKeyword_m2163321765_ftn) (String_t*);
	static Shader_DisableKeyword_m2163321765_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_DisableKeyword_m2163321765_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::DisableKeyword(System.String)");
	_il2cpp_icall_func(___keyword0);
}
// System.Boolean UnityEngine.Shader::IsKeywordEnabled(System.String)
extern "C"  bool Shader_IsKeywordEnabled_m1621717150 (Il2CppObject * __this /* static, unused */, String_t* ___keyword0, const MethodInfo* method)
{
	typedef bool (*Shader_IsKeywordEnabled_m1621717150_ftn) (String_t*);
	static Shader_IsKeywordEnabled_m1621717150_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_IsKeywordEnabled_m1621717150_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::IsKeywordEnabled(System.String)");
	return _il2cpp_icall_func(___keyword0);
}
// System.Int32 UnityEngine.Shader::get_maximumLOD()
extern "C"  int32_t Shader_get_maximumLOD_m2404270150 (Shader_t3191267369 * __this, const MethodInfo* method)
{
	typedef int32_t (*Shader_get_maximumLOD_m2404270150_ftn) (Shader_t3191267369 *);
	static Shader_get_maximumLOD_m2404270150_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_get_maximumLOD_m2404270150_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::get_maximumLOD()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Shader::set_maximumLOD(System.Int32)
extern "C"  void Shader_set_maximumLOD_m2797626507 (Shader_t3191267369 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Shader_set_maximumLOD_m2797626507_ftn) (Shader_t3191267369 *, int32_t);
	static Shader_set_maximumLOD_m2797626507_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_set_maximumLOD_m2797626507_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::set_maximumLOD(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Shader::get_globalMaximumLOD()
extern "C"  int32_t Shader_get_globalMaximumLOD_m585517193 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Shader_get_globalMaximumLOD_m585517193_ftn) ();
	static Shader_get_globalMaximumLOD_m585517193_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_get_globalMaximumLOD_m585517193_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::get_globalMaximumLOD()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Shader::set_globalMaximumLOD(System.Int32)
extern "C"  void Shader_set_globalMaximumLOD_m2733529422 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Shader_set_globalMaximumLOD_m2733529422_ftn) (int32_t);
	static Shader_set_globalMaximumLOD_m2733529422_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_set_globalMaximumLOD_m2733529422_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::set_globalMaximumLOD(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.Shader::get_renderQueue()
extern "C"  int32_t Shader_get_renderQueue_m801092952 (Shader_t3191267369 * __this, const MethodInfo* method)
{
	typedef int32_t (*Shader_get_renderQueue_m801092952_ftn) (Shader_t3191267369 *);
	static Shader_get_renderQueue_m801092952_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_get_renderQueue_m801092952_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::get_renderQueue()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.DisableBatchingType UnityEngine.Shader::get_disableBatching()
extern "C"  int32_t Shader_get_disableBatching_m2523136045 (Shader_t3191267369 * __this, const MethodInfo* method)
{
	typedef int32_t (*Shader_get_disableBatching_m2523136045_ftn) (Shader_t3191267369 *);
	static Shader_get_disableBatching_m2523136045_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_get_disableBatching_m2523136045_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::get_disableBatching()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Shader::SetGlobalColor(System.String,UnityEngine.Color)
extern "C"  void Shader_SetGlobalColor_m1669397640 (Il2CppObject * __this /* static, unused */, String_t* ___propertyName0, Color_t4194546905  ___color1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m3019342011(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t4194546905  L_2 = ___color1;
		Shader_SetGlobalColor_m1709492491(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Shader::SetGlobalColor(System.Int32,UnityEngine.Color)
extern "C"  void Shader_SetGlobalColor_m1709492491 (Il2CppObject * __this /* static, unused */, int32_t ___nameID0, Color_t4194546905  ___color1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___nameID0;
		Shader_INTERNAL_CALL_SetGlobalColor_m1549785026(NULL /*static, unused*/, L_0, (&___color1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Shader::INTERNAL_CALL_SetGlobalColor(System.Int32,UnityEngine.Color&)
extern "C"  void Shader_INTERNAL_CALL_SetGlobalColor_m1549785026 (Il2CppObject * __this /* static, unused */, int32_t ___nameID0, Color_t4194546905 * ___color1, const MethodInfo* method)
{
	typedef void (*Shader_INTERNAL_CALL_SetGlobalColor_m1549785026_ftn) (int32_t, Color_t4194546905 *);
	static Shader_INTERNAL_CALL_SetGlobalColor_m1549785026_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_INTERNAL_CALL_SetGlobalColor_m1549785026_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::INTERNAL_CALL_SetGlobalColor(System.Int32,UnityEngine.Color&)");
	_il2cpp_icall_func(___nameID0, ___color1);
}
// System.Void UnityEngine.Shader::SetGlobalVector(System.String,UnityEngine.Vector4)
extern "C"  void Shader_SetGlobalVector_m1990013606 (Il2CppObject * __this /* static, unused */, String_t* ___propertyName0, Vector4_t4282066567  ___vec1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		Vector4_t4282066567  L_1 = ___vec1;
		Color_t4194546905  L_2 = Color_op_Implicit_m343095162(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Shader_SetGlobalColor_m1669397640(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Shader::SetGlobalVector(System.Int32,UnityEngine.Vector4)
extern "C"  void Shader_SetGlobalVector_m1932877649 (Il2CppObject * __this /* static, unused */, int32_t ___nameID0, Vector4_t4282066567  ___vec1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___nameID0;
		Vector4_t4282066567  L_1 = ___vec1;
		Color_t4194546905  L_2 = Color_op_Implicit_m343095162(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Shader_SetGlobalColor_m1709492491(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Shader::SetGlobalFloat(System.String,System.Single)
extern "C"  void Shader_SetGlobalFloat_m2785620148 (Il2CppObject * __this /* static, unused */, String_t* ___propertyName0, float ___value1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m3019342011(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ___value1;
		Shader_SetGlobalFloat_m3553472137(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Shader::SetGlobalFloat(System.Int32,System.Single)
extern "C"  void Shader_SetGlobalFloat_m3553472137 (Il2CppObject * __this /* static, unused */, int32_t ___nameID0, float ___value1, const MethodInfo* method)
{
	typedef void (*Shader_SetGlobalFloat_m3553472137_ftn) (int32_t, float);
	static Shader_SetGlobalFloat_m3553472137_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_SetGlobalFloat_m3553472137_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::SetGlobalFloat(System.Int32,System.Single)");
	_il2cpp_icall_func(___nameID0, ___value1);
}
// System.Void UnityEngine.Shader::SetGlobalInt(System.String,System.Int32)
extern "C"  void Shader_SetGlobalInt_m2536714683 (Il2CppObject * __this /* static, unused */, String_t* ___propertyName0, int32_t ___value1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = ___value1;
		Shader_SetGlobalFloat_m2785620148(NULL /*static, unused*/, L_0, (((float)((float)L_1))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Shader::SetGlobalInt(System.Int32,System.Int32)
extern "C"  void Shader_SetGlobalInt_m1449584064 (Il2CppObject * __this /* static, unused */, int32_t ___nameID0, int32_t ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___nameID0;
		int32_t L_1 = ___value1;
		Shader_SetGlobalFloat_m3553472137(NULL /*static, unused*/, L_0, (((float)((float)L_1))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Shader::SetGlobalTexture(System.String,UnityEngine.Texture)
extern "C"  void Shader_SetGlobalTexture_m2110804504 (Il2CppObject * __this /* static, unused */, String_t* ___propertyName0, Texture_t2526458961 * ___tex1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m3019342011(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Texture_t2526458961 * L_2 = ___tex1;
		Shader_SetGlobalTexture_m2332174155(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Shader::SetGlobalTexture(System.Int32,UnityEngine.Texture)
extern "C"  void Shader_SetGlobalTexture_m2332174155 (Il2CppObject * __this /* static, unused */, int32_t ___nameID0, Texture_t2526458961 * ___tex1, const MethodInfo* method)
{
	typedef void (*Shader_SetGlobalTexture_m2332174155_ftn) (int32_t, Texture_t2526458961 *);
	static Shader_SetGlobalTexture_m2332174155_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_SetGlobalTexture_m2332174155_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::SetGlobalTexture(System.Int32,UnityEngine.Texture)");
	_il2cpp_icall_func(___nameID0, ___tex1);
}
// System.Void UnityEngine.Shader::SetGlobalMatrix(System.String,UnityEngine.Matrix4x4)
extern "C"  void Shader_SetGlobalMatrix_m3693328362 (Il2CppObject * __this /* static, unused */, String_t* ___propertyName0, Matrix4x4_t1651859333  ___mat1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m3019342011(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Matrix4x4_t1651859333  L_2 = ___mat1;
		Shader_SetGlobalMatrix_m4125037841(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Shader::SetGlobalMatrix(System.Int32,UnityEngine.Matrix4x4)
extern "C"  void Shader_SetGlobalMatrix_m4125037841 (Il2CppObject * __this /* static, unused */, int32_t ___nameID0, Matrix4x4_t1651859333  ___mat1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___nameID0;
		Shader_INTERNAL_CALL_SetGlobalMatrix_m1567336922(NULL /*static, unused*/, L_0, (&___mat1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Shader::INTERNAL_CALL_SetGlobalMatrix(System.Int32,UnityEngine.Matrix4x4&)
extern "C"  void Shader_INTERNAL_CALL_SetGlobalMatrix_m1567336922 (Il2CppObject * __this /* static, unused */, int32_t ___nameID0, Matrix4x4_t1651859333 * ___mat1, const MethodInfo* method)
{
	typedef void (*Shader_INTERNAL_CALL_SetGlobalMatrix_m1567336922_ftn) (int32_t, Matrix4x4_t1651859333 *);
	static Shader_INTERNAL_CALL_SetGlobalMatrix_m1567336922_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_INTERNAL_CALL_SetGlobalMatrix_m1567336922_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::INTERNAL_CALL_SetGlobalMatrix(System.Int32,UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(___nameID0, ___mat1);
}
// System.Void UnityEngine.Shader::SetGlobalBuffer(System.String,UnityEngine.ComputeBuffer)
extern "C"  void Shader_SetGlobalBuffer_m1902070403 (Il2CppObject * __this /* static, unused */, String_t* ___propertyName0, ComputeBuffer_t37359565 * ___buffer1, const MethodInfo* method)
{
	typedef void (*Shader_SetGlobalBuffer_m1902070403_ftn) (String_t*, ComputeBuffer_t37359565 *);
	static Shader_SetGlobalBuffer_m1902070403_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_SetGlobalBuffer_m1902070403_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::SetGlobalBuffer(System.String,UnityEngine.ComputeBuffer)");
	_il2cpp_icall_func(___propertyName0, ___buffer1);
}
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern "C"  int32_t Shader_PropertyToID_m3019342011 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	typedef int32_t (*Shader_PropertyToID_m3019342011_ftn) (String_t*);
	static Shader_PropertyToID_m3019342011_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_PropertyToID_m3019342011_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::PropertyToID(System.String)");
	return _il2cpp_icall_func(___name0);
}
// System.Void UnityEngine.Shader::WarmupAllShaders()
extern "C"  void Shader_WarmupAllShaders_m355796777 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*Shader_WarmupAllShaders_m355796777_ftn) ();
	static Shader_WarmupAllShaders_m355796777_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_WarmupAllShaders_m355796777_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::WarmupAllShaders()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SharedBetweenAnimatorsAttribute::.ctor()
extern "C"  void SharedBetweenAnimatorsAttribute__ctor_m2764338918 (SharedBetweenAnimatorsAttribute_t1486273289 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t421858229_marshal_pinvoke(const SkeletonBone_t421858229& unmarshaled, SkeletonBone_t421858229_marshaled_pinvoke& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_name_0());
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_position_1(), marshaled.___position_1);
	Quaternion_t1553702882_marshal_pinvoke(unmarshaled.get_rotation_2(), marshaled.___rotation_2);
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_scale_3(), marshaled.___scale_3);
	marshaled.___transformModified_4 = unmarshaled.get_transformModified_4();
}
extern "C" void SkeletonBone_t421858229_marshal_pinvoke_back(const SkeletonBone_t421858229_marshaled_pinvoke& marshaled, SkeletonBone_t421858229& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_string_result(marshaled.___name_0));
	Vector3_t4282066566  unmarshaled_position_temp_1;
	memset(&unmarshaled_position_temp_1, 0, sizeof(unmarshaled_position_temp_1));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___position_1, unmarshaled_position_temp_1);
	unmarshaled.set_position_1(unmarshaled_position_temp_1);
	Quaternion_t1553702882  unmarshaled_rotation_temp_2;
	memset(&unmarshaled_rotation_temp_2, 0, sizeof(unmarshaled_rotation_temp_2));
	Quaternion_t1553702882_marshal_pinvoke_back(marshaled.___rotation_2, unmarshaled_rotation_temp_2);
	unmarshaled.set_rotation_2(unmarshaled_rotation_temp_2);
	Vector3_t4282066566  unmarshaled_scale_temp_3;
	memset(&unmarshaled_scale_temp_3, 0, sizeof(unmarshaled_scale_temp_3));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___scale_3, unmarshaled_scale_temp_3);
	unmarshaled.set_scale_3(unmarshaled_scale_temp_3);
	int32_t unmarshaled_transformModified_temp_4 = 0;
	unmarshaled_transformModified_temp_4 = marshaled.___transformModified_4;
	unmarshaled.set_transformModified_4(unmarshaled_transformModified_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t421858229_marshal_pinvoke_cleanup(SkeletonBone_t421858229_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___position_1);
	Quaternion_t1553702882_marshal_pinvoke_cleanup(marshaled.___rotation_2);
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___scale_3);
}
// Conversion methods for marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t421858229_marshal_com(const SkeletonBone_t421858229& unmarshaled, SkeletonBone_t421858229_marshaled_com& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_name_0());
	Vector3_t4282066566_marshal_com(unmarshaled.get_position_1(), marshaled.___position_1);
	Quaternion_t1553702882_marshal_com(unmarshaled.get_rotation_2(), marshaled.___rotation_2);
	Vector3_t4282066566_marshal_com(unmarshaled.get_scale_3(), marshaled.___scale_3);
	marshaled.___transformModified_4 = unmarshaled.get_transformModified_4();
}
extern "C" void SkeletonBone_t421858229_marshal_com_back(const SkeletonBone_t421858229_marshaled_com& marshaled, SkeletonBone_t421858229& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___name_0));
	Vector3_t4282066566  unmarshaled_position_temp_1;
	memset(&unmarshaled_position_temp_1, 0, sizeof(unmarshaled_position_temp_1));
	Vector3_t4282066566_marshal_com_back(marshaled.___position_1, unmarshaled_position_temp_1);
	unmarshaled.set_position_1(unmarshaled_position_temp_1);
	Quaternion_t1553702882  unmarshaled_rotation_temp_2;
	memset(&unmarshaled_rotation_temp_2, 0, sizeof(unmarshaled_rotation_temp_2));
	Quaternion_t1553702882_marshal_com_back(marshaled.___rotation_2, unmarshaled_rotation_temp_2);
	unmarshaled.set_rotation_2(unmarshaled_rotation_temp_2);
	Vector3_t4282066566  unmarshaled_scale_temp_3;
	memset(&unmarshaled_scale_temp_3, 0, sizeof(unmarshaled_scale_temp_3));
	Vector3_t4282066566_marshal_com_back(marshaled.___scale_3, unmarshaled_scale_temp_3);
	unmarshaled.set_scale_3(unmarshaled_scale_temp_3);
	int32_t unmarshaled_transformModified_temp_4 = 0;
	unmarshaled_transformModified_temp_4 = marshaled.___transformModified_4;
	unmarshaled.set_transformModified_4(unmarshaled_transformModified_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t421858229_marshal_com_cleanup(SkeletonBone_t421858229_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___name_0);
	marshaled.___name_0 = NULL;
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___position_1);
	Quaternion_t1553702882_marshal_com_cleanup(marshaled.___rotation_2);
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___scale_3);
}
// System.Void UnityEngine.SkinnedMeshRenderer::.ctor()
extern "C"  void SkinnedMeshRenderer__ctor_m847473785 (SkinnedMeshRenderer_t3986041494 * __this, const MethodInfo* method)
{
	{
		Renderer__ctor_m3477754890(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform[] UnityEngine.SkinnedMeshRenderer::get_bones()
extern "C"  TransformU5BU5D_t3792884695* SkinnedMeshRenderer_get_bones_m2029101177 (SkinnedMeshRenderer_t3986041494 * __this, const MethodInfo* method)
{
	typedef TransformU5BU5D_t3792884695* (*SkinnedMeshRenderer_get_bones_m2029101177_ftn) (SkinnedMeshRenderer_t3986041494 *);
	static SkinnedMeshRenderer_get_bones_m2029101177_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_get_bones_m2029101177_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::get_bones()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SkinnedMeshRenderer::set_bones(UnityEngine.Transform[])
extern "C"  void SkinnedMeshRenderer_set_bones_m3788446386 (SkinnedMeshRenderer_t3986041494 * __this, TransformU5BU5D_t3792884695* ___value0, const MethodInfo* method)
{
	typedef void (*SkinnedMeshRenderer_set_bones_m3788446386_ftn) (SkinnedMeshRenderer_t3986041494 *, TransformU5BU5D_t3792884695*);
	static SkinnedMeshRenderer_set_bones_m3788446386_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_set_bones_m3788446386_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::set_bones(UnityEngine.Transform[])");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Transform UnityEngine.SkinnedMeshRenderer::get_rootBone()
extern "C"  Transform_t1659122786 * SkinnedMeshRenderer_get_rootBone_m2468735484 (SkinnedMeshRenderer_t3986041494 * __this, const MethodInfo* method)
{
	typedef Transform_t1659122786 * (*SkinnedMeshRenderer_get_rootBone_m2468735484_ftn) (SkinnedMeshRenderer_t3986041494 *);
	static SkinnedMeshRenderer_get_rootBone_m2468735484_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_get_rootBone_m2468735484_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::get_rootBone()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SkinnedMeshRenderer::set_rootBone(UnityEngine.Transform)
extern "C"  void SkinnedMeshRenderer_set_rootBone_m104990511 (SkinnedMeshRenderer_t3986041494 * __this, Transform_t1659122786 * ___value0, const MethodInfo* method)
{
	typedef void (*SkinnedMeshRenderer_set_rootBone_m104990511_ftn) (SkinnedMeshRenderer_t3986041494 *, Transform_t1659122786 *);
	static SkinnedMeshRenderer_set_rootBone_m104990511_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_set_rootBone_m104990511_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::set_rootBone(UnityEngine.Transform)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.SkinQuality UnityEngine.SkinnedMeshRenderer::get_quality()
extern "C"  int32_t SkinnedMeshRenderer_get_quality_m730302869 (SkinnedMeshRenderer_t3986041494 * __this, const MethodInfo* method)
{
	typedef int32_t (*SkinnedMeshRenderer_get_quality_m730302869_ftn) (SkinnedMeshRenderer_t3986041494 *);
	static SkinnedMeshRenderer_get_quality_m730302869_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_get_quality_m730302869_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::get_quality()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SkinnedMeshRenderer::set_quality(UnityEngine.SkinQuality)
extern "C"  void SkinnedMeshRenderer_set_quality_m2719599022 (SkinnedMeshRenderer_t3986041494 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*SkinnedMeshRenderer_set_quality_m2719599022_ftn) (SkinnedMeshRenderer_t3986041494 *, int32_t);
	static SkinnedMeshRenderer_set_quality_m2719599022_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_set_quality_m2719599022_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::set_quality(UnityEngine.SkinQuality)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Mesh UnityEngine.SkinnedMeshRenderer::get_sharedMesh()
extern "C"  Mesh_t4241756145 * SkinnedMeshRenderer_get_sharedMesh_m1298086811 (SkinnedMeshRenderer_t3986041494 * __this, const MethodInfo* method)
{
	typedef Mesh_t4241756145 * (*SkinnedMeshRenderer_get_sharedMesh_m1298086811_ftn) (SkinnedMeshRenderer_t3986041494 *);
	static SkinnedMeshRenderer_get_sharedMesh_m1298086811_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_get_sharedMesh_m1298086811_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::get_sharedMesh()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SkinnedMeshRenderer::set_sharedMesh(UnityEngine.Mesh)
extern "C"  void SkinnedMeshRenderer_set_sharedMesh_m3249072150 (SkinnedMeshRenderer_t3986041494 * __this, Mesh_t4241756145 * ___value0, const MethodInfo* method)
{
	typedef void (*SkinnedMeshRenderer_set_sharedMesh_m3249072150_ftn) (SkinnedMeshRenderer_t3986041494 *, Mesh_t4241756145 *);
	static SkinnedMeshRenderer_set_sharedMesh_m3249072150_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_set_sharedMesh_m3249072150_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::set_sharedMesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.SkinnedMeshRenderer::get_updateWhenOffscreen()
extern "C"  bool SkinnedMeshRenderer_get_updateWhenOffscreen_m1513102252 (SkinnedMeshRenderer_t3986041494 * __this, const MethodInfo* method)
{
	typedef bool (*SkinnedMeshRenderer_get_updateWhenOffscreen_m1513102252_ftn) (SkinnedMeshRenderer_t3986041494 *);
	static SkinnedMeshRenderer_get_updateWhenOffscreen_m1513102252_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_get_updateWhenOffscreen_m1513102252_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::get_updateWhenOffscreen()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SkinnedMeshRenderer::set_updateWhenOffscreen(System.Boolean)
extern "C"  void SkinnedMeshRenderer_set_updateWhenOffscreen_m3438381641 (SkinnedMeshRenderer_t3986041494 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*SkinnedMeshRenderer_set_updateWhenOffscreen_m3438381641_ftn) (SkinnedMeshRenderer_t3986041494 *, bool);
	static SkinnedMeshRenderer_set_updateWhenOffscreen_m3438381641_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_set_updateWhenOffscreen_m3438381641_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::set_updateWhenOffscreen(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Bounds UnityEngine.SkinnedMeshRenderer::get_localBounds()
extern "C"  Bounds_t2711641849  SkinnedMeshRenderer_get_localBounds_m2234798417 (SkinnedMeshRenderer_t3986041494 * __this, const MethodInfo* method)
{
	Bounds_t2711641849  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		SkinnedMeshRenderer_INTERNAL_get_localBounds_m3779851992(__this, (&V_0), /*hidden argument*/NULL);
		Bounds_t2711641849  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.SkinnedMeshRenderer::set_localBounds(UnityEngine.Bounds)
extern "C"  void SkinnedMeshRenderer_set_localBounds_m1518546862 (SkinnedMeshRenderer_t3986041494 * __this, Bounds_t2711641849  ___value0, const MethodInfo* method)
{
	{
		SkinnedMeshRenderer_INTERNAL_set_localBounds_m1413651276(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SkinnedMeshRenderer::INTERNAL_get_localBounds(UnityEngine.Bounds&)
extern "C"  void SkinnedMeshRenderer_INTERNAL_get_localBounds_m3779851992 (SkinnedMeshRenderer_t3986041494 * __this, Bounds_t2711641849 * ___value0, const MethodInfo* method)
{
	typedef void (*SkinnedMeshRenderer_INTERNAL_get_localBounds_m3779851992_ftn) (SkinnedMeshRenderer_t3986041494 *, Bounds_t2711641849 *);
	static SkinnedMeshRenderer_INTERNAL_get_localBounds_m3779851992_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_INTERNAL_get_localBounds_m3779851992_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::INTERNAL_get_localBounds(UnityEngine.Bounds&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.SkinnedMeshRenderer::INTERNAL_set_localBounds(UnityEngine.Bounds&)
extern "C"  void SkinnedMeshRenderer_INTERNAL_set_localBounds_m1413651276 (SkinnedMeshRenderer_t3986041494 * __this, Bounds_t2711641849 * ___value0, const MethodInfo* method)
{
	typedef void (*SkinnedMeshRenderer_INTERNAL_set_localBounds_m1413651276_ftn) (SkinnedMeshRenderer_t3986041494 *, Bounds_t2711641849 *);
	static SkinnedMeshRenderer_INTERNAL_set_localBounds_m1413651276_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_INTERNAL_set_localBounds_m1413651276_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::INTERNAL_set_localBounds(UnityEngine.Bounds&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.SkinnedMeshRenderer::BakeMesh(UnityEngine.Mesh)
extern "C"  void SkinnedMeshRenderer_BakeMesh_m3302464365 (SkinnedMeshRenderer_t3986041494 * __this, Mesh_t4241756145 * ___mesh0, const MethodInfo* method)
{
	typedef void (*SkinnedMeshRenderer_BakeMesh_m3302464365_ftn) (SkinnedMeshRenderer_t3986041494 *, Mesh_t4241756145 *);
	static SkinnedMeshRenderer_BakeMesh_m3302464365_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_BakeMesh_m3302464365_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::BakeMesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___mesh0);
}
// System.Single UnityEngine.SkinnedMeshRenderer::GetBlendShapeWeight(System.Int32)
extern "C"  float SkinnedMeshRenderer_GetBlendShapeWeight_m2551512402 (SkinnedMeshRenderer_t3986041494 * __this, int32_t ___index0, const MethodInfo* method)
{
	typedef float (*SkinnedMeshRenderer_GetBlendShapeWeight_m2551512402_ftn) (SkinnedMeshRenderer_t3986041494 *, int32_t);
	static SkinnedMeshRenderer_GetBlendShapeWeight_m2551512402_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_GetBlendShapeWeight_m2551512402_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::GetBlendShapeWeight(System.Int32)");
	return _il2cpp_icall_func(__this, ___index0);
}
// System.Void UnityEngine.SkinnedMeshRenderer::SetBlendShapeWeight(System.Int32,System.Single)
extern "C"  void SkinnedMeshRenderer_SetBlendShapeWeight_m2024253623 (SkinnedMeshRenderer_t3986041494 * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	typedef void (*SkinnedMeshRenderer_SetBlendShapeWeight_m2024253623_ftn) (SkinnedMeshRenderer_t3986041494 *, int32_t, float);
	static SkinnedMeshRenderer_SetBlendShapeWeight_m2024253623_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SkinnedMeshRenderer_SetBlendShapeWeight_m2024253623_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SkinnedMeshRenderer::SetBlendShapeWeight(System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___index0, ___value1);
}
// System.Void UnityEngine.Skybox::.ctor()
extern "C"  void Skybox__ctor_m682853923 (Skybox_t3194751310 * __this, const MethodInfo* method)
{
	{
		Behaviour__ctor_m1624944828(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material UnityEngine.Skybox::get_material()
extern "C"  Material_t3870600107 * Skybox_get_material_m2228910434 (Skybox_t3194751310 * __this, const MethodInfo* method)
{
	typedef Material_t3870600107 * (*Skybox_get_material_m2228910434_ftn) (Skybox_t3194751310 *);
	static Skybox_get_material_m2228910434_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Skybox_get_material_m2228910434_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Skybox::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Skybox::set_material(UnityEngine.Material)
extern "C"  void Skybox_set_material_m2704017511 (Skybox_t3194751310 * __this, Material_t3870600107 * ___value0, const MethodInfo* method)
{
	typedef void (*Skybox_set_material_m2704017511_ftn) (Skybox_t3194751310 *, Material_t3870600107 *);
	static Skybox_set_material_m2704017511_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Skybox_set_material_m2704017511_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Skybox::set_material(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.SleepTimeout::.ctor()
extern "C"  void SleepTimeout__ctor_m457701667 (SleepTimeout_t1876228494 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SliderHandler::.ctor(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,System.Boolean,System.Int32)
extern "C"  void SliderHandler__ctor_m4217573891 (SliderHandler_t783692703 * __this, Rect_t4241904616  ___position0, float ___currentValue1, float ___size2, float ___start3, float ___end4, GUIStyle_t2990928826 * ___slider5, GUIStyle_t2990928826 * ___thumb6, bool ___horiz7, int32_t ___id8, const MethodInfo* method)
{
	{
		Rect_t4241904616  L_0 = ___position0;
		__this->set_position_0(L_0);
		float L_1 = ___currentValue1;
		__this->set_currentValue_1(L_1);
		float L_2 = ___size2;
		__this->set_size_2(L_2);
		float L_3 = ___start3;
		__this->set_start_3(L_3);
		float L_4 = ___end4;
		__this->set_end_4(L_4);
		GUIStyle_t2990928826 * L_5 = ___slider5;
		__this->set_slider_5(L_5);
		GUIStyle_t2990928826 * L_6 = ___thumb6;
		__this->set_thumb_6(L_6);
		bool L_7 = ___horiz7;
		__this->set_horiz_7(L_7);
		int32_t L_8 = ___id8;
		__this->set_id_8(L_8);
		return;
	}
}
extern "C"  void SliderHandler__ctor_m4217573891_AdjustorThunk (Il2CppObject * __this, Rect_t4241904616  ___position0, float ___currentValue1, float ___size2, float ___start3, float ___end4, GUIStyle_t2990928826 * ___slider5, GUIStyle_t2990928826 * ___thumb6, bool ___horiz7, int32_t ___id8, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	SliderHandler__ctor_m4217573891(_thisAdjusted, ___position0, ___currentValue1, ___size2, ___start3, ___end4, ___slider5, ___thumb6, ___horiz7, ___id8, method);
}
// System.Single UnityEngine.SliderHandler::Handle()
extern "C"  float SliderHandler_Handle_m2409586512 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		GUIStyle_t2990928826 * L_0 = __this->get_slider_5();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		GUIStyle_t2990928826 * L_1 = __this->get_thumb_6();
		if (L_1)
		{
			goto IL_001d;
		}
	}

IL_0016:
	{
		float L_2 = __this->get_currentValue_1();
		return L_2;
	}

IL_001d:
	{
		int32_t L_3 = SliderHandler_CurrentEventType_m3978233689(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if (L_4 == 0)
		{
			goto IL_004f;
		}
		if (L_4 == 1)
		{
			goto IL_005d;
		}
		if (L_4 == 2)
		{
			goto IL_006b;
		}
		if (L_4 == 3)
		{
			goto IL_0056;
		}
		if (L_4 == 4)
		{
			goto IL_006b;
		}
		if (L_4 == 5)
		{
			goto IL_006b;
		}
		if (L_4 == 6)
		{
			goto IL_006b;
		}
		if (L_4 == 7)
		{
			goto IL_0064;
		}
	}
	{
		goto IL_006b;
	}

IL_004f:
	{
		float L_5 = SliderHandler_OnMouseDown_m3958002946(__this, /*hidden argument*/NULL);
		return L_5;
	}

IL_0056:
	{
		float L_6 = SliderHandler_OnMouseDrag_m3960111380(__this, /*hidden argument*/NULL);
		return L_6;
	}

IL_005d:
	{
		float L_7 = SliderHandler_OnMouseUp_m1318588539(__this, /*hidden argument*/NULL);
		return L_7;
	}

IL_0064:
	{
		float L_8 = SliderHandler_OnRepaint_m4264550822(__this, /*hidden argument*/NULL);
		return L_8;
	}

IL_006b:
	{
		float L_9 = __this->get_currentValue_1();
		return L_9;
	}
}
extern "C"  float SliderHandler_Handle_m2409586512_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_Handle_m2409586512(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::OnMouseDown()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern Il2CppClass* SystemClock_t4036018645_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_OnMouseDown_m3958002946_MetadataUsageId;
extern "C"  float SliderHandler_OnMouseDown_m3958002946 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_OnMouseDown_m3958002946_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t4241904616  V_2;
	memset(&V_2, 0, sizeof(V_2));
	DateTime_t4283661327  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Rect_t4241904616  L_0 = __this->get_position_0();
		V_1 = L_0;
		Event_t4196595728 * L_1 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t4282066565  L_2 = Event_get_mousePosition_m3610425949(L_1, /*hidden argument*/NULL);
		bool L_3 = Rect_Contains_m3556594010((&V_1), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		bool L_4 = SliderHandler_IsEmptySlider_m2523580504(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0030;
		}
	}

IL_0029:
	{
		float L_5 = __this->get_currentValue_1();
		return L_5;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_scrollTroughSide_m1228634973(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_id_8();
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m300477798(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Event_t4196595728 * L_7 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Event_Use_m310777444(L_7, /*hidden argument*/NULL);
		Rect_t4241904616  L_8 = SliderHandler_ThumbSelectionRect_m3697323546(__this, /*hidden argument*/NULL);
		V_2 = L_8;
		Event_t4196595728 * L_9 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector2_t4282066565  L_10 = Event_get_mousePosition_m3610425949(L_9, /*hidden argument*/NULL);
		bool L_11 = Rect_Contains_m3556594010((&V_2), L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_007d;
		}
	}
	{
		float L_12 = SliderHandler_ClampedCurrentValue_m3006511340(__this, /*hidden argument*/NULL);
		SliderHandler_StartDraggingWithValue_m3874943933(__this, L_12, /*hidden argument*/NULL);
		float L_13 = __this->get_currentValue_1();
		return L_13;
	}

IL_007d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_changed_m727947722(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		bool L_14 = SliderHandler_SupportsPageMovements_m4216612037(__this, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00c7;
		}
	}
	{
		SliderState_t1233388262 * L_15 = SliderHandler_SliderState_m1456191352(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		L_15->set_isDragging_2((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t4036018645_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_16 = SystemClock_get_now_m175136990(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_16;
		DateTime_t4283661327  L_17 = DateTime_AddMilliseconds_m1717403134((&V_3), (250.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_nextScrollStepTime_m3820512796(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		int32_t L_18 = SliderHandler_CurrentScrollTroughSide_m1606169264(__this, /*hidden argument*/NULL);
		GUI_set_scrollTroughSide_m1228634973(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		float L_19 = SliderHandler_PageMovementValue_m2660885357(__this, /*hidden argument*/NULL);
		return L_19;
	}

IL_00c7:
	{
		float L_20 = SliderHandler_ValueForCurrentMousePosition_m493574741(__this, /*hidden argument*/NULL);
		V_0 = L_20;
		float L_21 = V_0;
		SliderHandler_StartDraggingWithValue_m3874943933(__this, L_21, /*hidden argument*/NULL);
		float L_22 = V_0;
		float L_23 = SliderHandler_Clamp_m4218954710(__this, L_22, /*hidden argument*/NULL);
		return L_23;
	}
}
extern "C"  float SliderHandler_OnMouseDown_m3958002946_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_OnMouseDown_m3958002946(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::OnMouseDrag()
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_OnMouseDrag_m3960111380_MetadataUsageId;
extern "C"  float SliderHandler_OnMouseDrag_m3960111380 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_OnMouseDrag_m3960111380_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SliderState_t1233388262 * V_0 = NULL;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_get_hotControl_m4135893409(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_id_8();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0017;
		}
	}
	{
		float L_2 = __this->get_currentValue_1();
		return L_2;
	}

IL_0017:
	{
		SliderState_t1233388262 * L_3 = SliderHandler_SliderState_m1456191352(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		SliderState_t1233388262 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = L_4->get_isDragging_2();
		if (L_5)
		{
			goto IL_0030;
		}
	}
	{
		float L_6 = __this->get_currentValue_1();
		return L_6;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_changed_m727947722(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		Event_t4196595728 * L_7 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Event_Use_m310777444(L_7, /*hidden argument*/NULL);
		float L_8 = SliderHandler_MousePosition_m303335016(__this, /*hidden argument*/NULL);
		SliderState_t1233388262 * L_9 = V_0;
		NullCheck(L_9);
		float L_10 = L_9->get_dragStartPos_0();
		V_1 = ((float)((float)L_8-(float)L_10));
		SliderState_t1233388262 * L_11 = V_0;
		NullCheck(L_11);
		float L_12 = L_11->get_dragStartValue_1();
		float L_13 = V_1;
		float L_14 = SliderHandler_ValuesPerPixel_m41869331(__this, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_12+(float)((float)((float)L_13/(float)L_14))));
		float L_15 = V_2;
		float L_16 = SliderHandler_Clamp_m4218954710(__this, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  float SliderHandler_OnMouseDrag_m3960111380_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_OnMouseDrag_m3960111380(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::OnMouseUp()
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_OnMouseUp_m1318588539_MetadataUsageId;
extern "C"  float SliderHandler_OnMouseUp_m1318588539 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_OnMouseUp_m1318588539_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_get_hotControl_m4135893409(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_id_8();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0021;
		}
	}
	{
		Event_t4196595728 * L_2 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Event_Use_m310777444(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m300477798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_0021:
	{
		float L_3 = __this->get_currentValue_1();
		return L_3;
	}
}
extern "C"  float SliderHandler_OnMouseUp_m1318588539_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_OnMouseUp_m1318588539(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::OnRepaint()
extern Il2CppClass* GUIContent_t2094828418_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* SystemClock_t4036018645_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_OnRepaint_m4264550822_MetadataUsageId;
extern "C"  float SliderHandler_OnRepaint_m4264550822 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_OnRepaint_m4264550822_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	DateTime_t4283661327  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		GUIStyle_t2990928826 * L_0 = __this->get_slider_5();
		Rect_t4241904616  L_1 = __this->get_position_0();
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent_t2094828418 * L_2 = ((GUIContent_t2094828418_StaticFields*)GUIContent_t2094828418_il2cpp_TypeInfo_var->static_fields)->get_none_6();
		int32_t L_3 = __this->get_id_8();
		NullCheck(L_0);
		GUIStyle_Draw_m2994577084(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		bool L_4 = SliderHandler_IsEmptySlider_m2523580504(__this, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		GUIStyle_t2990928826 * L_5 = __this->get_thumb_6();
		Rect_t4241904616  L_6 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent_t2094828418 * L_7 = ((GUIContent_t2094828418_StaticFields*)GUIContent_t2094828418_il2cpp_TypeInfo_var->static_fields)->get_none_6();
		int32_t L_8 = __this->get_id_8();
		NullCheck(L_5);
		GUIStyle_Draw_m2994577084(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
	}

IL_0043:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		int32_t L_9 = GUIUtility_get_hotControl_m4135893409(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_10 = __this->get_id_8();
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_007c;
		}
	}
	{
		Rect_t4241904616  L_11 = __this->get_position_0();
		V_0 = L_11;
		Event_t4196595728 * L_12 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector2_t4282066565  L_13 = Event_get_mousePosition_m3610425949(L_12, /*hidden argument*/NULL);
		bool L_14 = Rect_Contains_m3556594010((&V_0), L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_007c;
		}
	}
	{
		bool L_15 = SliderHandler_IsEmptySlider_m2523580504(__this, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0083;
		}
	}

IL_007c:
	{
		float L_16 = __this->get_currentValue_1();
		return L_16;
	}

IL_0083:
	{
		Rect_t4241904616  L_17 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_1 = L_17;
		Event_t4196595728 * L_18 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector2_t4282066565  L_19 = Event_get_mousePosition_m3610425949(L_18, /*hidden argument*/NULL);
		bool L_20 = Rect_Contains_m3556594010((&V_1), L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00b8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		int32_t L_21 = GUI_get_scrollTroughSide_m3369891864(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m300477798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_00b1:
	{
		float L_22 = __this->get_currentValue_1();
		return L_22;
	}

IL_00b8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_InternalRepaintEditorWindow_m3223206407(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t4036018645_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_23 = SystemClock_get_now_m175136990(NULL /*static, unused*/, /*hidden argument*/NULL);
		DateTime_t4283661327  L_24 = GUI_get_nextScrollStepTime_m719800559(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		bool L_25 = DateTime_op_LessThan_m35073816(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00d8;
		}
	}
	{
		float L_26 = __this->get_currentValue_1();
		return L_26;
	}

IL_00d8:
	{
		int32_t L_27 = SliderHandler_CurrentScrollTroughSide_m1606169264(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		int32_t L_28 = GUI_get_scrollTroughSide_m3369891864(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_27) == ((int32_t)L_28)))
		{
			goto IL_00ef;
		}
	}
	{
		float L_29 = __this->get_currentValue_1();
		return L_29;
	}

IL_00ef:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t4036018645_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_30 = SystemClock_get_now_m175136990(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_30;
		DateTime_t4283661327  L_31 = DateTime_AddMilliseconds_m1717403134((&V_2), (30.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_nextScrollStepTime_m3820512796(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		bool L_32 = SliderHandler_SupportsPageMovements_m4216612037(__this, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_012e;
		}
	}
	{
		SliderState_t1233388262 * L_33 = SliderHandler_SliderState_m1456191352(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		L_33->set_isDragging_2((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_changed_m727947722(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		float L_34 = SliderHandler_PageMovementValue_m2660885357(__this, /*hidden argument*/NULL);
		return L_34;
	}

IL_012e:
	{
		float L_35 = SliderHandler_ClampedCurrentValue_m3006511340(__this, /*hidden argument*/NULL);
		return L_35;
	}
}
extern "C"  float SliderHandler_OnRepaint_m4264550822_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_OnRepaint_m4264550822(_thisAdjusted, method);
}
// UnityEngine.EventType UnityEngine.SliderHandler::CurrentEventType()
extern "C"  int32_t SliderHandler_CurrentEventType_m3978233689 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	{
		Event_t4196595728 * L_0 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_id_8();
		NullCheck(L_0);
		int32_t L_2 = Event_GetTypeForControl_m854773288(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t SliderHandler_CurrentEventType_m3978233689_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_CurrentEventType_m3978233689(_thisAdjusted, method);
}
// System.Int32 UnityEngine.SliderHandler::CurrentScrollTroughSide()
extern "C"  int32_t SliderHandler_CurrentScrollTroughSide_m1606169264 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector2_t4282066565  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t4282066565  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t4241904616  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t4241904616  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float G_B3_0 = 0.0f;
	float G_B6_0 = 0.0f;
	int32_t G_B9_0 = 0;
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		Event_t4196595728 * L_1 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t4282066565  L_2 = Event_get_mousePosition_m3610425949(L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		float L_3 = (&V_2)->get_x_1();
		G_B3_0 = L_3;
		goto IL_0036;
	}

IL_0023:
	{
		Event_t4196595728 * L_4 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector2_t4282066565  L_5 = Event_get_mousePosition_m3610425949(L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = (&V_3)->get_y_2();
		G_B3_0 = L_6;
	}

IL_0036:
	{
		V_0 = G_B3_0;
		bool L_7 = __this->get_horiz_7();
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		Rect_t4241904616  L_8 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = Rect_get_x_m982385354((&V_4), /*hidden argument*/NULL);
		G_B6_0 = L_9;
		goto IL_0065;
	}

IL_0056:
	{
		Rect_t4241904616  L_10 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_5 = L_10;
		float L_11 = Rect_get_y_m982386315((&V_5), /*hidden argument*/NULL);
		G_B6_0 = L_11;
	}

IL_0065:
	{
		V_1 = G_B6_0;
		float L_12 = V_0;
		float L_13 = V_1;
		if ((!(((float)L_12) > ((float)L_13))))
		{
			goto IL_0073;
		}
	}
	{
		G_B9_0 = 1;
		goto IL_0074;
	}

IL_0073:
	{
		G_B9_0 = (-1);
	}

IL_0074:
	{
		return G_B9_0;
	}
}
extern "C"  int32_t SliderHandler_CurrentScrollTroughSide_m1606169264_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_CurrentScrollTroughSide_m1606169264(_thisAdjusted, method);
}
// System.Boolean UnityEngine.SliderHandler::IsEmptySlider()
extern "C"  bool SliderHandler_IsEmptySlider_m2523580504 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_start_3();
		float L_1 = __this->get_end_4();
		return (bool)((((float)L_0) == ((float)L_1))? 1 : 0);
	}
}
extern "C"  bool SliderHandler_IsEmptySlider_m2523580504_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_IsEmptySlider_m2523580504(_thisAdjusted, method);
}
// System.Boolean UnityEngine.SliderHandler::SupportsPageMovements()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_SupportsPageMovements_m4216612037_MetadataUsageId;
extern "C"  bool SliderHandler_SupportsPageMovements_m4216612037 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_SupportsPageMovements_m4216612037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		float L_0 = __this->get_size_2();
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_1 = GUI_get_usePageScrollbars_m944581596(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_1));
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 0;
	}

IL_0018:
	{
		return (bool)G_B3_0;
	}
}
extern "C"  bool SliderHandler_SupportsPageMovements_m4216612037_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_SupportsPageMovements_m4216612037(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::PageMovementValue()
extern "C"  float SliderHandler_PageMovementValue_m2660885357 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	{
		float L_0 = __this->get_currentValue_1();
		V_0 = L_0;
		float L_1 = __this->get_start_3();
		float L_2 = __this->get_end_4();
		if ((!(((float)L_1) > ((float)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		G_B3_0 = (-1);
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 1;
	}

IL_001f:
	{
		V_1 = G_B3_0;
		float L_3 = SliderHandler_MousePosition_m303335016(__this, /*hidden argument*/NULL);
		float L_4 = SliderHandler_PageUpMovementBound_m2543741343(__this, /*hidden argument*/NULL);
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0048;
		}
	}
	{
		float L_5 = V_0;
		float L_6 = __this->get_size_2();
		int32_t L_7 = V_1;
		V_0 = ((float)((float)L_5+(float)((float)((float)((float)((float)L_6*(float)(((float)((float)L_7)))))*(float)(0.9f)))));
		goto IL_005a;
	}

IL_0048:
	{
		float L_8 = V_0;
		float L_9 = __this->get_size_2();
		int32_t L_10 = V_1;
		V_0 = ((float)((float)L_8-(float)((float)((float)((float)((float)L_9*(float)(((float)((float)L_10)))))*(float)(0.9f)))));
	}

IL_005a:
	{
		float L_11 = V_0;
		float L_12 = SliderHandler_Clamp_m4218954710(__this, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
extern "C"  float SliderHandler_PageMovementValue_m2660885357_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_PageMovementValue_m2660885357(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::PageUpMovementBound()
extern "C"  float SliderHandler_PageUpMovementBound_m2543741343 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t4241904616  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t4241904616  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		Rect_t4241904616  L_1 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = Rect_get_xMax_m370881244((&V_0), /*hidden argument*/NULL);
		Rect_t4241904616  L_3 = __this->get_position_0();
		V_1 = L_3;
		float L_4 = Rect_get_x_m982385354((&V_1), /*hidden argument*/NULL);
		return ((float)((float)L_2-(float)L_4));
	}

IL_0029:
	{
		Rect_t4241904616  L_5 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Rect_get_yMax_m399510395((&V_2), /*hidden argument*/NULL);
		Rect_t4241904616  L_7 = __this->get_position_0();
		V_3 = L_7;
		float L_8 = Rect_get_y_m982386315((&V_3), /*hidden argument*/NULL);
		return ((float)((float)L_6-(float)L_8));
	}
}
extern "C"  float SliderHandler_PageUpMovementBound_m2543741343_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_PageUpMovementBound_m2543741343(_thisAdjusted, method);
}
// UnityEngine.Event UnityEngine.SliderHandler::CurrentEvent()
extern "C"  Event_t4196595728 * SliderHandler_CurrentEvent_m721596197 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	{
		Event_t4196595728 * L_0 = Event_get_current_m238587645(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  Event_t4196595728 * SliderHandler_CurrentEvent_m721596197_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_CurrentEvent_m721596197(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::ValueForCurrentMousePosition()
extern "C"  float SliderHandler_ValueForCurrentMousePosition_m493574741 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		float L_1 = SliderHandler_MousePosition_m303335016(__this, /*hidden argument*/NULL);
		Rect_t4241904616  L_2 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_width_m2824209432((&V_0), /*hidden argument*/NULL);
		float L_4 = SliderHandler_ValuesPerPixel_m41869331(__this, /*hidden argument*/NULL);
		float L_5 = __this->get_start_3();
		float L_6 = __this->get_size_2();
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_1-(float)((float)((float)L_3*(float)(0.5f)))))/(float)L_4))+(float)L_5))-(float)((float)((float)L_6*(float)(0.5f)))));
	}

IL_0042:
	{
		float L_7 = SliderHandler_MousePosition_m303335016(__this, /*hidden argument*/NULL);
		Rect_t4241904616  L_8 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_1 = L_8;
		float L_9 = Rect_get_height_m2154960823((&V_1), /*hidden argument*/NULL);
		float L_10 = SliderHandler_ValuesPerPixel_m41869331(__this, /*hidden argument*/NULL);
		float L_11 = __this->get_start_3();
		float L_12 = __this->get_size_2();
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_7-(float)((float)((float)L_9*(float)(0.5f)))))/(float)L_10))+(float)L_11))-(float)((float)((float)L_12*(float)(0.5f)))));
	}
}
extern "C"  float SliderHandler_ValueForCurrentMousePosition_m493574741_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_ValueForCurrentMousePosition_m493574741(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::Clamp(System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_Clamp_m4218954710_MetadataUsageId;
extern "C"  float SliderHandler_Clamp_m4218954710 (SliderHandler_t783692703 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_Clamp_m4218954710_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value0;
		float L_1 = SliderHandler_MinValue_m2516678631(__this, /*hidden argument*/NULL);
		float L_2 = SliderHandler_MaxValue_m44679317(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
extern "C"  float SliderHandler_Clamp_m4218954710_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_Clamp_m4218954710(_thisAdjusted, ___value0, method);
}
// UnityEngine.Rect UnityEngine.SliderHandler::ThumbSelectionRect()
extern "C"  Rect_t4241904616  SliderHandler_ThumbSelectionRect_m3697323546 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		Rect_t4241904616  L_0 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = ((int32_t)12);
		float L_1 = Rect_get_width_m2824209432((&V_0), /*hidden argument*/NULL);
		int32_t L_2 = V_1;
		if ((!(((float)L_1) < ((float)(((float)((float)L_2)))))))
		{
			goto IL_003f;
		}
	}
	{
		Rect_t4241904616 * L_3 = (&V_0);
		float L_4 = Rect_get_x_m982385354(L_3, /*hidden argument*/NULL);
		int32_t L_5 = V_1;
		float L_6 = Rect_get_width_m2824209432((&V_0), /*hidden argument*/NULL);
		Rect_set_x_m577970569(L_3, ((float)((float)L_4-(float)((float)((float)((float)((float)(((float)((float)L_5)))-(float)L_6))/(float)(2.0f))))), /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		Rect_set_width_m3771513595((&V_0), (((float)((float)L_7))), /*hidden argument*/NULL);
	}

IL_003f:
	{
		float L_8 = Rect_get_height_m2154960823((&V_0), /*hidden argument*/NULL);
		int32_t L_9 = V_1;
		if ((!(((float)L_8) < ((float)(((float)((float)L_9)))))))
		{
			goto IL_0074;
		}
	}
	{
		Rect_t4241904616 * L_10 = (&V_0);
		float L_11 = Rect_get_y_m982386315(L_10, /*hidden argument*/NULL);
		int32_t L_12 = V_1;
		float L_13 = Rect_get_height_m2154960823((&V_0), /*hidden argument*/NULL);
		Rect_set_y_m67436392(L_10, ((float)((float)L_11-(float)((float)((float)((float)((float)(((float)((float)L_12)))-(float)L_13))/(float)(2.0f))))), /*hidden argument*/NULL);
		int32_t L_14 = V_1;
		Rect_set_height_m3398820332((&V_0), (((float)((float)L_14))), /*hidden argument*/NULL);
	}

IL_0074:
	{
		Rect_t4241904616  L_15 = V_0;
		return L_15;
	}
}
extern "C"  Rect_t4241904616  SliderHandler_ThumbSelectionRect_m3697323546_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_ThumbSelectionRect_m3697323546(_thisAdjusted, method);
}
// System.Void UnityEngine.SliderHandler::StartDraggingWithValue(System.Single)
extern "C"  void SliderHandler_StartDraggingWithValue_m3874943933 (SliderHandler_t783692703 * __this, float ___dragStartValue0, const MethodInfo* method)
{
	SliderState_t1233388262 * V_0 = NULL;
	{
		SliderState_t1233388262 * L_0 = SliderHandler_SliderState_m1456191352(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		SliderState_t1233388262 * L_1 = V_0;
		float L_2 = SliderHandler_MousePosition_m303335016(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_dragStartPos_0(L_2);
		SliderState_t1233388262 * L_3 = V_0;
		float L_4 = ___dragStartValue0;
		NullCheck(L_3);
		L_3->set_dragStartValue_1(L_4);
		SliderState_t1233388262 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_isDragging_2((bool)1);
		return;
	}
}
extern "C"  void SliderHandler_StartDraggingWithValue_m3874943933_AdjustorThunk (Il2CppObject * __this, float ___dragStartValue0, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	SliderHandler_StartDraggingWithValue_m3874943933(_thisAdjusted, ___dragStartValue0, method);
}
// UnityEngine.SliderState UnityEngine.SliderHandler::SliderState()
extern const Il2CppType* SliderState_t1233388262_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern Il2CppClass* SliderState_t1233388262_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_SliderState_m1456191352_MetadataUsageId;
extern "C"  SliderState_t1233388262 * SliderHandler_SliderState_m1456191352 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_SliderState_m1456191352_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(SliderState_t1233388262_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_1 = __this->get_id_8();
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = GUIUtility_GetStateObject_m2379308309(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((SliderState_t1233388262 *)CastclassClass(L_2, SliderState_t1233388262_il2cpp_TypeInfo_var));
	}
}
extern "C"  SliderState_t1233388262 * SliderHandler_SliderState_m1456191352_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_SliderState_m1456191352(_thisAdjusted, method);
}
// UnityEngine.Rect UnityEngine.SliderHandler::ThumbRect()
extern "C"  Rect_t4241904616  SliderHandler_ThumbRect_m1881961532 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	Rect_t4241904616  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Rect_t4241904616  L_1 = SliderHandler_HorizontalThumbRect_m2689559608(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Rect_t4241904616  L_2 = SliderHandler_VerticalThumbRect_m1406344678(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
extern "C"  Rect_t4241904616  SliderHandler_ThumbRect_m1881961532_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_ThumbRect_m1881961532(_thisAdjusted, method);
}
// UnityEngine.Rect UnityEngine.SliderHandler::VerticalThumbRect()
extern "C"  Rect_t4241904616  SliderHandler_VerticalThumbRect_m1406344678 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t4241904616  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t4241904616  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t4241904616  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t4241904616  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t4241904616  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		float L_0 = SliderHandler_ValuesPerPixel_m41869331(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = __this->get_start_3();
		float L_2 = __this->get_end_4();
		if ((!(((float)L_1) < ((float)L_2))))
		{
			goto IL_009d;
		}
	}
	{
		Rect_t4241904616  L_3 = __this->get_position_0();
		V_1 = L_3;
		float L_4 = Rect_get_x_m982385354((&V_1), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_5 = __this->get_slider_5();
		NullCheck(L_5);
		RectOffset_t3056157787 * L_6 = GUIStyle_get_padding_m3072941276(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_left_m4104523390(L_6, /*hidden argument*/NULL);
		float L_8 = SliderHandler_ClampedCurrentValue_m3006511340(__this, /*hidden argument*/NULL);
		float L_9 = __this->get_start_3();
		float L_10 = V_0;
		Rect_t4241904616  L_11 = __this->get_position_0();
		V_2 = L_11;
		float L_12 = Rect_get_y_m982386315((&V_2), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_13 = __this->get_slider_5();
		NullCheck(L_13);
		RectOffset_t3056157787 * L_14 = GUIStyle_get_padding_m3072941276(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = RectOffset_get_top_m140097312(L_14, /*hidden argument*/NULL);
		Rect_t4241904616  L_16 = __this->get_position_0();
		V_3 = L_16;
		float L_17 = Rect_get_width_m2824209432((&V_3), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_18 = __this->get_slider_5();
		NullCheck(L_18);
		RectOffset_t3056157787 * L_19 = GUIStyle_get_padding_m3072941276(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = RectOffset_get_horizontal_m1186440923(L_19, /*hidden argument*/NULL);
		float L_21 = __this->get_size_2();
		float L_22 = V_0;
		float L_23 = SliderHandler_ThumbSize_m3034411697(__this, /*hidden argument*/NULL);
		Rect_t4241904616  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m3291325233(&L_24, ((float)((float)L_4+(float)(((float)((float)L_7))))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8-(float)L_9))*(float)L_10))+(float)L_12))+(float)(((float)((float)L_15))))), ((float)((float)L_17-(float)(((float)((float)L_20))))), ((float)((float)((float)((float)L_21*(float)L_22))+(float)L_23)), /*hidden argument*/NULL);
		return L_24;
	}

IL_009d:
	{
		Rect_t4241904616  L_25 = __this->get_position_0();
		V_4 = L_25;
		float L_26 = Rect_get_x_m982385354((&V_4), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_27 = __this->get_slider_5();
		NullCheck(L_27);
		RectOffset_t3056157787 * L_28 = GUIStyle_get_padding_m3072941276(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		int32_t L_29 = RectOffset_get_left_m4104523390(L_28, /*hidden argument*/NULL);
		float L_30 = SliderHandler_ClampedCurrentValue_m3006511340(__this, /*hidden argument*/NULL);
		float L_31 = __this->get_size_2();
		float L_32 = __this->get_start_3();
		float L_33 = V_0;
		Rect_t4241904616  L_34 = __this->get_position_0();
		V_5 = L_34;
		float L_35 = Rect_get_y_m982386315((&V_5), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_36 = __this->get_slider_5();
		NullCheck(L_36);
		RectOffset_t3056157787 * L_37 = GUIStyle_get_padding_m3072941276(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		int32_t L_38 = RectOffset_get_top_m140097312(L_37, /*hidden argument*/NULL);
		Rect_t4241904616  L_39 = __this->get_position_0();
		V_6 = L_39;
		float L_40 = Rect_get_width_m2824209432((&V_6), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_41 = __this->get_slider_5();
		NullCheck(L_41);
		RectOffset_t3056157787 * L_42 = GUIStyle_get_padding_m3072941276(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		int32_t L_43 = RectOffset_get_horizontal_m1186440923(L_42, /*hidden argument*/NULL);
		float L_44 = __this->get_size_2();
		float L_45 = V_0;
		float L_46 = SliderHandler_ThumbSize_m3034411697(__this, /*hidden argument*/NULL);
		Rect_t4241904616  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Rect__ctor_m3291325233(&L_47, ((float)((float)L_26+(float)(((float)((float)L_29))))), ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))-(float)L_32))*(float)L_33))+(float)L_35))+(float)(((float)((float)L_38))))), ((float)((float)L_40-(float)(((float)((float)L_43))))), ((float)((float)((float)((float)L_44*(float)((-L_45))))+(float)L_46)), /*hidden argument*/NULL);
		return L_47;
	}
}
extern "C"  Rect_t4241904616  SliderHandler_VerticalThumbRect_m1406344678_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_VerticalThumbRect_m1406344678(_thisAdjusted, method);
}
// UnityEngine.Rect UnityEngine.SliderHandler::HorizontalThumbRect()
extern "C"  Rect_t4241904616  SliderHandler_HorizontalThumbRect_m2689559608 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t4241904616  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t4241904616  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t4241904616  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t4241904616  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t4241904616  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		float L_0 = SliderHandler_ValuesPerPixel_m41869331(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = __this->get_start_3();
		float L_2 = __this->get_end_4();
		if ((!(((float)L_1) < ((float)L_2))))
		{
			goto IL_009d;
		}
	}
	{
		float L_3 = SliderHandler_ClampedCurrentValue_m3006511340(__this, /*hidden argument*/NULL);
		float L_4 = __this->get_start_3();
		float L_5 = V_0;
		Rect_t4241904616  L_6 = __this->get_position_0();
		V_1 = L_6;
		float L_7 = Rect_get_x_m982385354((&V_1), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_8 = __this->get_slider_5();
		NullCheck(L_8);
		RectOffset_t3056157787 * L_9 = GUIStyle_get_padding_m3072941276(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = RectOffset_get_left_m4104523390(L_9, /*hidden argument*/NULL);
		Rect_t4241904616  L_11 = __this->get_position_0();
		V_2 = L_11;
		float L_12 = Rect_get_y_m982386315((&V_2), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_13 = __this->get_slider_5();
		NullCheck(L_13);
		RectOffset_t3056157787 * L_14 = GUIStyle_get_padding_m3072941276(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = RectOffset_get_top_m140097312(L_14, /*hidden argument*/NULL);
		float L_16 = __this->get_size_2();
		float L_17 = V_0;
		float L_18 = SliderHandler_ThumbSize_m3034411697(__this, /*hidden argument*/NULL);
		Rect_t4241904616  L_19 = __this->get_position_0();
		V_3 = L_19;
		float L_20 = Rect_get_height_m2154960823((&V_3), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_21 = __this->get_slider_5();
		NullCheck(L_21);
		RectOffset_t3056157787 * L_22 = GUIStyle_get_padding_m3072941276(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = RectOffset_get_vertical_m3650431789(L_22, /*hidden argument*/NULL);
		Rect_t4241904616  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m3291325233(&L_24, ((float)((float)((float)((float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5))+(float)L_7))+(float)(((float)((float)L_10))))), ((float)((float)L_12+(float)(((float)((float)L_15))))), ((float)((float)((float)((float)L_16*(float)L_17))+(float)L_18)), ((float)((float)L_20-(float)(((float)((float)L_23))))), /*hidden argument*/NULL);
		return L_24;
	}

IL_009d:
	{
		float L_25 = SliderHandler_ClampedCurrentValue_m3006511340(__this, /*hidden argument*/NULL);
		float L_26 = __this->get_size_2();
		float L_27 = __this->get_start_3();
		float L_28 = V_0;
		Rect_t4241904616  L_29 = __this->get_position_0();
		V_4 = L_29;
		float L_30 = Rect_get_x_m982385354((&V_4), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_31 = __this->get_slider_5();
		NullCheck(L_31);
		RectOffset_t3056157787 * L_32 = GUIStyle_get_padding_m3072941276(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		int32_t L_33 = RectOffset_get_left_m4104523390(L_32, /*hidden argument*/NULL);
		Rect_t4241904616  L_34 = __this->get_position_0();
		V_5 = L_34;
		float L_35 = Rect_get_y_m982386315((&V_5), /*hidden argument*/NULL);
		float L_36 = __this->get_size_2();
		float L_37 = V_0;
		float L_38 = SliderHandler_ThumbSize_m3034411697(__this, /*hidden argument*/NULL);
		Rect_t4241904616  L_39 = __this->get_position_0();
		V_6 = L_39;
		float L_40 = Rect_get_height_m2154960823((&V_6), /*hidden argument*/NULL);
		Rect_t4241904616  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Rect__ctor_m3291325233(&L_41, ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_25+(float)L_26))-(float)L_27))*(float)L_28))+(float)L_30))+(float)(((float)((float)L_33))))), L_35, ((float)((float)((float)((float)L_36*(float)((-L_37))))+(float)L_38)), L_40, /*hidden argument*/NULL);
		return L_41;
	}
}
extern "C"  Rect_t4241904616  SliderHandler_HorizontalThumbRect_m2689559608_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_HorizontalThumbRect_m2689559608(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::ClampedCurrentValue()
extern "C"  float SliderHandler_ClampedCurrentValue_m3006511340 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_currentValue_1();
		float L_1 = SliderHandler_Clamp_m4218954710(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  float SliderHandler_ClampedCurrentValue_m3006511340_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_ClampedCurrentValue_m3006511340(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::MousePosition()
extern "C"  float SliderHandler_MousePosition_m303335016 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t4282066565  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t4241904616  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		Event_t4196595728 * L_1 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t4282066565  L_2 = Event_get_mousePosition_m3610425949(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_x_1();
		Rect_t4241904616  L_4 = __this->get_position_0();
		V_1 = L_4;
		float L_5 = Rect_get_x_m982385354((&V_1), /*hidden argument*/NULL);
		return ((float)((float)L_3-(float)L_5));
	}

IL_002e:
	{
		Event_t4196595728 * L_6 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector2_t4282066565  L_7 = Event_get_mousePosition_m3610425949(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		float L_8 = (&V_2)->get_y_2();
		Rect_t4241904616  L_9 = __this->get_position_0();
		V_3 = L_9;
		float L_10 = Rect_get_y_m982386315((&V_3), /*hidden argument*/NULL);
		return ((float)((float)L_8-(float)L_10));
	}
}
extern "C"  float SliderHandler_MousePosition_m303335016_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_MousePosition_m303335016(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::ValuesPerPixel()
extern "C"  float SliderHandler_ValuesPerPixel_m41869331 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		Rect_t4241904616  L_1 = __this->get_position_0();
		V_0 = L_1;
		float L_2 = Rect_get_width_m2824209432((&V_0), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_3 = __this->get_slider_5();
		NullCheck(L_3);
		RectOffset_t3056157787 * L_4 = GUIStyle_get_padding_m3072941276(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = RectOffset_get_horizontal_m1186440923(L_4, /*hidden argument*/NULL);
		float L_6 = SliderHandler_ThumbSize_m3034411697(__this, /*hidden argument*/NULL);
		float L_7 = __this->get_end_4();
		float L_8 = __this->get_start_3();
		return ((float)((float)((float)((float)((float)((float)L_2-(float)(((float)((float)L_5)))))-(float)L_6))/(float)((float)((float)L_7-(float)L_8))));
	}

IL_0041:
	{
		Rect_t4241904616  L_9 = __this->get_position_0();
		V_1 = L_9;
		float L_10 = Rect_get_height_m2154960823((&V_1), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_11 = __this->get_slider_5();
		NullCheck(L_11);
		RectOffset_t3056157787 * L_12 = GUIStyle_get_padding_m3072941276(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = RectOffset_get_vertical_m3650431789(L_12, /*hidden argument*/NULL);
		float L_14 = SliderHandler_ThumbSize_m3034411697(__this, /*hidden argument*/NULL);
		float L_15 = __this->get_end_4();
		float L_16 = __this->get_start_3();
		return ((float)((float)((float)((float)((float)((float)L_10-(float)(((float)((float)L_13)))))-(float)L_14))/(float)((float)((float)L_15-(float)L_16))));
	}
}
extern "C"  float SliderHandler_ValuesPerPixel_m41869331_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_ValuesPerPixel_m41869331(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::ThumbSize()
extern "C"  float SliderHandler_ThumbSize_m3034411697 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	float G_B4_0 = 0.0f;
	float G_B8_0 = 0.0f;
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		GUIStyle_t2990928826 * L_1 = __this->get_thumb_6();
		NullCheck(L_1);
		float L_2 = GUIStyle_get_fixedWidth_m3249098964(L_1, /*hidden argument*/NULL);
		if ((((float)L_2) == ((float)(0.0f))))
		{
			goto IL_0030;
		}
	}
	{
		GUIStyle_t2990928826 * L_3 = __this->get_thumb_6();
		NullCheck(L_3);
		float L_4 = GUIStyle_get_fixedWidth_m3249098964(L_3, /*hidden argument*/NULL);
		G_B4_0 = L_4;
		goto IL_0041;
	}

IL_0030:
	{
		GUIStyle_t2990928826 * L_5 = __this->get_thumb_6();
		NullCheck(L_5);
		RectOffset_t3056157787 * L_6 = GUIStyle_get_padding_m3072941276(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_horizontal_m1186440923(L_6, /*hidden argument*/NULL);
		G_B4_0 = (((float)((float)L_7)));
	}

IL_0041:
	{
		return G_B4_0;
	}

IL_0042:
	{
		GUIStyle_t2990928826 * L_8 = __this->get_thumb_6();
		NullCheck(L_8);
		float L_9 = GUIStyle_get_fixedHeight_m2441634427(L_8, /*hidden argument*/NULL);
		if ((((float)L_9) == ((float)(0.0f))))
		{
			goto IL_0067;
		}
	}
	{
		GUIStyle_t2990928826 * L_10 = __this->get_thumb_6();
		NullCheck(L_10);
		float L_11 = GUIStyle_get_fixedHeight_m2441634427(L_10, /*hidden argument*/NULL);
		G_B8_0 = L_11;
		goto IL_0078;
	}

IL_0067:
	{
		GUIStyle_t2990928826 * L_12 = __this->get_thumb_6();
		NullCheck(L_12);
		RectOffset_t3056157787 * L_13 = GUIStyle_get_padding_m3072941276(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14 = RectOffset_get_vertical_m3650431789(L_13, /*hidden argument*/NULL);
		G_B8_0 = (((float)((float)L_14)));
	}

IL_0078:
	{
		return G_B8_0;
	}
}
extern "C"  float SliderHandler_ThumbSize_m3034411697_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_ThumbSize_m3034411697(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::MaxValue()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_MaxValue_m44679317_MetadataUsageId;
extern "C"  float SliderHandler_MaxValue_m44679317 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_MaxValue_m44679317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_start_3();
		float L_1 = __this->get_end_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Max_m3923796455(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = __this->get_size_2();
		return ((float)((float)L_2-(float)L_3));
	}
}
extern "C"  float SliderHandler_MaxValue_m44679317_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_MaxValue_m44679317(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::MinValue()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_MinValue_m2516678631_MetadataUsageId;
extern "C"  float SliderHandler_MinValue_m2516678631 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_MinValue_m2516678631_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_start_3();
		float L_1 = __this->get_end_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Min_m2322067385(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  float SliderHandler_MinValue_m2516678631_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_MinValue_m2516678631(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.SliderHandler
extern "C" void SliderHandler_t783692703_marshal_pinvoke(const SliderHandler_t783692703& unmarshaled, SliderHandler_t783692703_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___slider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'slider' of type 'SliderHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___slider_5Exception);
}
extern "C" void SliderHandler_t783692703_marshal_pinvoke_back(const SliderHandler_t783692703_marshaled_pinvoke& marshaled, SliderHandler_t783692703& unmarshaled)
{
	Il2CppCodeGenException* ___slider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'slider' of type 'SliderHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___slider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SliderHandler
extern "C" void SliderHandler_t783692703_marshal_pinvoke_cleanup(SliderHandler_t783692703_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SliderHandler
extern "C" void SliderHandler_t783692703_marshal_com(const SliderHandler_t783692703& unmarshaled, SliderHandler_t783692703_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___slider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'slider' of type 'SliderHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___slider_5Exception);
}
extern "C" void SliderHandler_t783692703_marshal_com_back(const SliderHandler_t783692703_marshaled_com& marshaled, SliderHandler_t783692703& unmarshaled)
{
	Il2CppCodeGenException* ___slider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'slider' of type 'SliderHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___slider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SliderHandler
extern "C" void SliderHandler_t783692703_marshal_com_cleanup(SliderHandler_t783692703_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.SliderJoint2D::.ctor()
extern "C"  void SliderJoint2D__ctor_m177657854 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method)
{
	{
		AnchoredJoint2D__ctor_m1301123505(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.SliderJoint2D::get_autoConfigureAngle()
extern "C"  bool SliderJoint2D_get_autoConfigureAngle_m4042937573 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method)
{
	typedef bool (*SliderJoint2D_get_autoConfigureAngle_m4042937573_ftn) (SliderJoint2D_t2955194545 *);
	static SliderJoint2D_get_autoConfigureAngle_m4042937573_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SliderJoint2D_get_autoConfigureAngle_m4042937573_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SliderJoint2D::get_autoConfigureAngle()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SliderJoint2D::set_autoConfigureAngle(System.Boolean)
extern "C"  void SliderJoint2D_set_autoConfigureAngle_m16111414 (SliderJoint2D_t2955194545 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*SliderJoint2D_set_autoConfigureAngle_m16111414_ftn) (SliderJoint2D_t2955194545 *, bool);
	static SliderJoint2D_set_autoConfigureAngle_m16111414_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SliderJoint2D_set_autoConfigureAngle_m16111414_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SliderJoint2D::set_autoConfigureAngle(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.SliderJoint2D::get_angle()
extern "C"  float SliderJoint2D_get_angle_m2311873010 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method)
{
	typedef float (*SliderJoint2D_get_angle_m2311873010_ftn) (SliderJoint2D_t2955194545 *);
	static SliderJoint2D_get_angle_m2311873010_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SliderJoint2D_get_angle_m2311873010_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SliderJoint2D::get_angle()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SliderJoint2D::set_angle(System.Single)
extern "C"  void SliderJoint2D_set_angle_m1872959993 (SliderJoint2D_t2955194545 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*SliderJoint2D_set_angle_m1872959993_ftn) (SliderJoint2D_t2955194545 *, float);
	static SliderJoint2D_set_angle_m1872959993_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SliderJoint2D_set_angle_m1872959993_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SliderJoint2D::set_angle(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.SliderJoint2D::get_useMotor()
extern "C"  bool SliderJoint2D_get_useMotor_m4239753335 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method)
{
	typedef bool (*SliderJoint2D_get_useMotor_m4239753335_ftn) (SliderJoint2D_t2955194545 *);
	static SliderJoint2D_get_useMotor_m4239753335_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SliderJoint2D_get_useMotor_m4239753335_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SliderJoint2D::get_useMotor()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SliderJoint2D::set_useMotor(System.Boolean)
extern "C"  void SliderJoint2D_set_useMotor_m3725243336 (SliderJoint2D_t2955194545 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*SliderJoint2D_set_useMotor_m3725243336_ftn) (SliderJoint2D_t2955194545 *, bool);
	static SliderJoint2D_set_useMotor_m3725243336_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SliderJoint2D_set_useMotor_m3725243336_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SliderJoint2D::set_useMotor(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.SliderJoint2D::get_useLimits()
extern "C"  bool SliderJoint2D_get_useLimits_m3899623160 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method)
{
	typedef bool (*SliderJoint2D_get_useLimits_m3899623160_ftn) (SliderJoint2D_t2955194545 *);
	static SliderJoint2D_get_useLimits_m3899623160_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SliderJoint2D_get_useLimits_m3899623160_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SliderJoint2D::get_useLimits()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SliderJoint2D::set_useLimits(System.Boolean)
extern "C"  void SliderJoint2D_set_useLimits_m1328927637 (SliderJoint2D_t2955194545 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*SliderJoint2D_set_useLimits_m1328927637_ftn) (SliderJoint2D_t2955194545 *, bool);
	static SliderJoint2D_set_useLimits_m1328927637_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SliderJoint2D_set_useLimits_m1328927637_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SliderJoint2D::set_useLimits(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.JointMotor2D UnityEngine.SliderJoint2D::get_motor()
extern "C"  JointMotor2D_t682576033  SliderJoint2D_get_motor_m543999139 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method)
{
	JointMotor2D_t682576033  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		SliderJoint2D_INTERNAL_get_motor_m2924520624(__this, (&V_0), /*hidden argument*/NULL);
		JointMotor2D_t682576033  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.SliderJoint2D::set_motor(UnityEngine.JointMotor2D)
extern "C"  void SliderJoint2D_set_motor_m3207411008 (SliderJoint2D_t2955194545 * __this, JointMotor2D_t682576033  ___value0, const MethodInfo* method)
{
	{
		SliderJoint2D_INTERNAL_set_motor_m558319908(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SliderJoint2D::INTERNAL_get_motor(UnityEngine.JointMotor2D&)
extern "C"  void SliderJoint2D_INTERNAL_get_motor_m2924520624 (SliderJoint2D_t2955194545 * __this, JointMotor2D_t682576033 * ___value0, const MethodInfo* method)
{
	typedef void (*SliderJoint2D_INTERNAL_get_motor_m2924520624_ftn) (SliderJoint2D_t2955194545 *, JointMotor2D_t682576033 *);
	static SliderJoint2D_INTERNAL_get_motor_m2924520624_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SliderJoint2D_INTERNAL_get_motor_m2924520624_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SliderJoint2D::INTERNAL_get_motor(UnityEngine.JointMotor2D&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.SliderJoint2D::INTERNAL_set_motor(UnityEngine.JointMotor2D&)
extern "C"  void SliderJoint2D_INTERNAL_set_motor_m558319908 (SliderJoint2D_t2955194545 * __this, JointMotor2D_t682576033 * ___value0, const MethodInfo* method)
{
	typedef void (*SliderJoint2D_INTERNAL_set_motor_m558319908_ftn) (SliderJoint2D_t2955194545 *, JointMotor2D_t682576033 *);
	static SliderJoint2D_INTERNAL_set_motor_m558319908_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SliderJoint2D_INTERNAL_set_motor_m558319908_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SliderJoint2D::INTERNAL_set_motor(UnityEngine.JointMotor2D&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.JointTranslationLimits2D UnityEngine.SliderJoint2D::get_limits()
extern "C"  JointTranslationLimits2D_t2599234005  SliderJoint2D_get_limits_m1555470976 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method)
{
	JointTranslationLimits2D_t2599234005  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		SliderJoint2D_INTERNAL_get_limits_m3170078997(__this, (&V_0), /*hidden argument*/NULL);
		JointTranslationLimits2D_t2599234005  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.SliderJoint2D::set_limits(UnityEngine.JointTranslationLimits2D)
extern "C"  void SliderJoint2D_set_limits_m2283242867 (SliderJoint2D_t2955194545 * __this, JointTranslationLimits2D_t2599234005  ___value0, const MethodInfo* method)
{
	{
		SliderJoint2D_INTERNAL_set_limits_m3076966689(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SliderJoint2D::INTERNAL_get_limits(UnityEngine.JointTranslationLimits2D&)
extern "C"  void SliderJoint2D_INTERNAL_get_limits_m3170078997 (SliderJoint2D_t2955194545 * __this, JointTranslationLimits2D_t2599234005 * ___value0, const MethodInfo* method)
{
	typedef void (*SliderJoint2D_INTERNAL_get_limits_m3170078997_ftn) (SliderJoint2D_t2955194545 *, JointTranslationLimits2D_t2599234005 *);
	static SliderJoint2D_INTERNAL_get_limits_m3170078997_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SliderJoint2D_INTERNAL_get_limits_m3170078997_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SliderJoint2D::INTERNAL_get_limits(UnityEngine.JointTranslationLimits2D&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.SliderJoint2D::INTERNAL_set_limits(UnityEngine.JointTranslationLimits2D&)
extern "C"  void SliderJoint2D_INTERNAL_set_limits_m3076966689 (SliderJoint2D_t2955194545 * __this, JointTranslationLimits2D_t2599234005 * ___value0, const MethodInfo* method)
{
	typedef void (*SliderJoint2D_INTERNAL_set_limits_m3076966689_ftn) (SliderJoint2D_t2955194545 *, JointTranslationLimits2D_t2599234005 *);
	static SliderJoint2D_INTERNAL_set_limits_m3076966689_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SliderJoint2D_INTERNAL_set_limits_m3076966689_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SliderJoint2D::INTERNAL_set_limits(UnityEngine.JointTranslationLimits2D&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.JointLimitState2D UnityEngine.SliderJoint2D::get_limitState()
extern "C"  int32_t SliderJoint2D_get_limitState_m1291794381 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method)
{
	typedef int32_t (*SliderJoint2D_get_limitState_m1291794381_ftn) (SliderJoint2D_t2955194545 *);
	static SliderJoint2D_get_limitState_m1291794381_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SliderJoint2D_get_limitState_m1291794381_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SliderJoint2D::get_limitState()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.SliderJoint2D::get_referenceAngle()
extern "C"  float SliderJoint2D_get_referenceAngle_m539557931 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method)
{
	typedef float (*SliderJoint2D_get_referenceAngle_m539557931_ftn) (SliderJoint2D_t2955194545 *);
	static SliderJoint2D_get_referenceAngle_m539557931_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SliderJoint2D_get_referenceAngle_m539557931_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SliderJoint2D::get_referenceAngle()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.SliderJoint2D::get_jointTranslation()
extern "C"  float SliderJoint2D_get_jointTranslation_m4164567914 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method)
{
	typedef float (*SliderJoint2D_get_jointTranslation_m4164567914_ftn) (SliderJoint2D_t2955194545 *);
	static SliderJoint2D_get_jointTranslation_m4164567914_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SliderJoint2D_get_jointTranslation_m4164567914_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SliderJoint2D::get_jointTranslation()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.SliderJoint2D::get_jointSpeed()
extern "C"  float SliderJoint2D_get_jointSpeed_m1002478976 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method)
{
	typedef float (*SliderJoint2D_get_jointSpeed_m1002478976_ftn) (SliderJoint2D_t2955194545 *);
	static SliderJoint2D_get_jointSpeed_m1002478976_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SliderJoint2D_get_jointSpeed_m1002478976_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SliderJoint2D::get_jointSpeed()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.SliderJoint2D::GetMotorForce(System.Single)
extern "C"  float SliderJoint2D_GetMotorForce_m3044414359 (SliderJoint2D_t2955194545 * __this, float ___timeStep0, const MethodInfo* method)
{
	{
		float L_0 = ___timeStep0;
		float L_1 = SliderJoint2D_INTERNAL_CALL_GetMotorForce_m2702916190(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single UnityEngine.SliderJoint2D::INTERNAL_CALL_GetMotorForce(UnityEngine.SliderJoint2D,System.Single)
extern "C"  float SliderJoint2D_INTERNAL_CALL_GetMotorForce_m2702916190 (Il2CppObject * __this /* static, unused */, SliderJoint2D_t2955194545 * ___self0, float ___timeStep1, const MethodInfo* method)
{
	typedef float (*SliderJoint2D_INTERNAL_CALL_GetMotorForce_m2702916190_ftn) (SliderJoint2D_t2955194545 *, float);
	static SliderJoint2D_INTERNAL_CALL_GetMotorForce_m2702916190_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SliderJoint2D_INTERNAL_CALL_GetMotorForce_m2702916190_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SliderJoint2D::INTERNAL_CALL_GetMotorForce(UnityEngine.SliderJoint2D,System.Single)");
	return _il2cpp_icall_func(___self0, ___timeStep1);
}
// System.Void UnityEngine.SliderState::.ctor()
extern "C"  void SliderState__ctor_m3732503849 (SliderState_t1233388262 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::.ctor()
extern "C"  void GameCenterPlatform__ctor_m573039859 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::.cctor()
extern Il2CppClass* AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3189060351_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1721872494_MethodInfo_var;
extern const uint32_t GameCenterPlatform__cctor_m102270234_MetadataUsageId;
extern "C"  void GameCenterPlatform__cctor_m102270234 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform__cctor_m102270234_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_adCache_9(((AchievementDescriptionU5BU5D_t759444790*)SZArrayNew(AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var, (uint32_t)0)));
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_friends_10(((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)0)));
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_users_11(((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)0)));
		List_1_t3189060351 * L_0 = (List_1_t3189060351 *)il2cpp_codegen_object_new(List_1_t3189060351_il2cpp_TypeInfo_var);
		List_1__ctor_m1721872494(L_0, /*hidden argument*/List_1__ctor_m1721872494_MethodInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_m_GcBoards_14(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::UnityEngine.SocialPlatforms.ISocialPlatform.LoadFriends(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m2468032119_MetadataUsageId;
extern "C"  void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m2468032119 (GameCenterPlatform_t3570684786 * __this, Il2CppObject * ___user0, Action_1_t872614854 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m2468032119_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t872614854 * L_0 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_FriendsCallback_1(L_0);
		GameCenterPlatform_Internal_LoadFriends_m1921936862(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::UnityEngine.SocialPlatforms.ISocialPlatform.Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m305325355_MetadataUsageId;
extern "C"  void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m305325355 (GameCenterPlatform_t3570684786 * __this, Il2CppObject * ___user0, Action_1_t872614854 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m305325355_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t872614854 * L_0 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_AuthenticateCallback_0(L_0);
		GameCenterPlatform_Internal_Authenticate_m582381960(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()
extern "C"  void GameCenterPlatform_Internal_Authenticate_m582381960 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_Authenticate_m582381960_ftn) ();
	static GameCenterPlatform_Internal_Authenticate_m582381960_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Authenticate_m582381960_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()");
	_il2cpp_icall_func();
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()
extern "C"  bool GameCenterPlatform_Internal_Authenticated_m2780967960 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GameCenterPlatform_Internal_Authenticated_m2780967960_ftn) ();
	static GameCenterPlatform_Internal_Authenticated_m2780967960_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Authenticated_m2780967960_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()
extern "C"  String_t* GameCenterPlatform_Internal_UserName_m1252299660 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GameCenterPlatform_Internal_UserName_m1252299660_ftn) ();
	static GameCenterPlatform_Internal_UserName_m1252299660_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserName_m1252299660_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()
extern "C"  String_t* GameCenterPlatform_Internal_UserID_m385481212 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GameCenterPlatform_Internal_UserID_m385481212_ftn) ();
	static GameCenterPlatform_Internal_UserID_m385481212_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserID_m385481212_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()
extern "C"  bool GameCenterPlatform_Internal_Underage_m4169738944 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GameCenterPlatform_Internal_Underage_m4169738944_ftn) ();
	static GameCenterPlatform_Internal_Underage_m4169738944_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Underage_m4169738944_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()");
	return _il2cpp_icall_func();
}
// UnityEngine.Texture2D UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()
extern "C"  Texture2D_t3884108195 * GameCenterPlatform_Internal_UserImage_m3175776130 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Texture2D_t3884108195 * (*GameCenterPlatform_Internal_UserImage_m3175776130_ftn) ();
	static GameCenterPlatform_Internal_UserImage_m3175776130_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserImage_m3175776130_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends()
extern "C"  void GameCenterPlatform_Internal_LoadFriends_m1921936862 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadFriends_m1921936862_ftn) ();
	static GameCenterPlatform_Internal_LoadFriends_m1921936862_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadFriends_m1921936862_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions()
extern "C"  void GameCenterPlatform_Internal_LoadAchievementDescriptions_m1394384079 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadAchievementDescriptions_m1394384079_ftn) ();
	static GameCenterPlatform_Internal_LoadAchievementDescriptions_m1394384079_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadAchievementDescriptions_m1394384079_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements()
extern "C"  void GameCenterPlatform_Internal_LoadAchievements_m817891229 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadAchievements_m817891229_ftn) ();
	static GameCenterPlatform_Internal_LoadAchievements_m817891229_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadAchievements_m817891229_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double)
extern "C"  void GameCenterPlatform_Internal_ReportProgress_m2511520970 (Il2CppObject * __this /* static, unused */, String_t* ___id0, double ___progress1, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ReportProgress_m2511520970_ftn) (String_t*, double);
	static GameCenterPlatform_Internal_ReportProgress_m2511520970_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ReportProgress_m2511520970_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double)");
	_il2cpp_icall_func(___id0, ___progress1);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String)
extern "C"  void GameCenterPlatform_Internal_ReportScore_m408601947 (Il2CppObject * __this /* static, unused */, int64_t ___score0, String_t* ___category1, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ReportScore_m408601947_ftn) (int64_t, String_t*);
	static GameCenterPlatform_Internal_ReportScore_m408601947_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ReportScore_m408601947_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String)");
	_il2cpp_icall_func(___score0, ___category1);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String)
extern "C"  void GameCenterPlatform_Internal_LoadScores_m523283944 (Il2CppObject * __this /* static, unused */, String_t* ___category0, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadScores_m523283944_ftn) (String_t*);
	static GameCenterPlatform_Internal_LoadScores_m523283944_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadScores_m523283944_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String)");
	_il2cpp_icall_func(___category0);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()
extern "C"  void GameCenterPlatform_Internal_ShowAchievementsUI_m1934331464 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowAchievementsUI_m1934331464_ftn) ();
	static GameCenterPlatform_Internal_ShowAchievementsUI_m1934331464_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowAchievementsUI_m1934331464_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()
extern "C"  void GameCenterPlatform_Internal_ShowLeaderboardUI_m3057704739 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowLeaderboardUI_m3057704739_ftn) ();
	static GameCenterPlatform_Internal_ShowLeaderboardUI_m3057704739_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowLeaderboardUI_m3057704739_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[])
extern "C"  void GameCenterPlatform_Internal_LoadUsers_m4218820079 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t4054002952* ___userIds0, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadUsers_m4218820079_ftn) (StringU5BU5D_t4054002952*);
	static GameCenterPlatform_Internal_LoadUsers_m4218820079_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadUsers_m4218820079_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[])");
	_il2cpp_icall_func(___userIds0);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()
extern "C"  void GameCenterPlatform_Internal_ResetAllAchievements_m165059209 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ResetAllAchievements_m165059209_ftn) ();
	static GameCenterPlatform_Internal_ResetAllAchievements_m165059209_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ResetAllAchievements_m165059209_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)
extern "C"  void GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3108376897 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3108376897_ftn) (bool);
	static GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3108376897_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3108376897_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ResetAllAchievements(System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ResetAllAchievements_m878609996_MetadataUsageId;
extern "C"  void GameCenterPlatform_ResetAllAchievements_m878609996 (Il2CppObject * __this /* static, unused */, Action_1_t872614854 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ResetAllAchievements_m878609996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t872614854 * L_0 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_ResetAchievements_12(L_0);
		GameCenterPlatform_Internal_ResetAllAchievements_m165059209(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowDefaultAchievementCompletionBanner(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m2516168699_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m2516168699 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m2516168699_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3108376897(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowLeaderboardUI(System.String,UnityEngine.SocialPlatforms.TimeScope)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowLeaderboardUI_m3791866548_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowLeaderboardUI_m3791866548 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardID0, int32_t ___timeScope1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowLeaderboardUI_m3791866548_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___leaderboardID0;
		int32_t L_1 = ___timeScope1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1768304742(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)
extern "C"  void GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1768304742 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardID0, int32_t ___timeScope1, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1768304742_ftn) (String_t*, int32_t);
	static GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1768304742_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1768304742_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)");
	_il2cpp_icall_func(___leaderboardID0, ___timeScope1);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearAchievementDescriptions(System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ClearAchievementDescriptions_m3158758843_MetadataUsageId;
extern "C"  void GameCenterPlatform_ClearAchievementDescriptions_m3158758843 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ClearAchievementDescriptions_m3158758843_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		NullCheck(L_1);
		int32_t L_2 = ___size0;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) == ((int32_t)L_2)))
		{
			goto IL_0022;
		}
	}

IL_0017:
	{
		int32_t L_3 = ___size0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_adCache_9(((AchievementDescriptionU5BU5D_t759444790*)SZArrayNew(AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var, (uint32_t)L_3)));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetAchievementDescription(UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetAchievementDescription_m3174496109_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetAchievementDescription_m3174496109 (Il2CppObject * __this /* static, unused */, GcAchievementDescriptionData_t2242891083  ___data0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetAchievementDescription_m3174496109_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		int32_t L_1 = ___number1;
		AchievementDescription_t2116066607 * L_2 = GcAchievementDescriptionData_ToAchievementDescription_m3125480712((&___data0), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		ArrayElementTypeCheck (L_0, L_2);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (AchievementDescription_t2116066607 *)L_2);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetAchievementDescriptionImage(UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2873143867;
extern const uint32_t GameCenterPlatform_SetAchievementDescriptionImage_m3728674360_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetAchievementDescriptionImage_m3728674360 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetAchievementDescriptionImage_m3728674360_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		NullCheck(L_0);
		int32_t L_1 = ___number1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = ___number1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_001f;
		}
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2873143867, /*hidden argument*/NULL);
		return;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_3 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		int32_t L_4 = ___number1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		AchievementDescription_t2116066607 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Texture2D_t3884108195 * L_7 = ___texture0;
		NullCheck(L_6);
		AchievementDescription_SetImage_m1092175896(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerAchievementDescriptionCallback()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2766540343_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral48254646;
extern const uint32_t GameCenterPlatform_TriggerAchievementDescriptionCallback_m1497473051_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerAchievementDescriptionCallback_m1497473051 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerAchievementDescriptionCallback_m1497473051_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t229750097 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AchievementDescriptionLoaderCallback_2();
		if (!L_0)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_2 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		NullCheck(L_2);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))
		{
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral48254646, /*hidden argument*/NULL);
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t229750097 * L_3 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AchievementDescriptionLoaderCallback_2();
		AchievementDescriptionU5BU5D_t759444790* L_4 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		NullCheck(L_3);
		Action_1_Invoke_m2766540343(L_3, (IAchievementDescriptionU5BU5D_t4128901257*)(IAchievementDescriptionU5BU5D_t4128901257*)L_4, /*hidden argument*/Action_1_Invoke_m2766540343_MethodInfo_var);
	}

IL_0039:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::AuthenticateCallbackWrapper(System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_AuthenticateCallbackWrapper_m2896042779_MetadataUsageId;
extern "C"  void GameCenterPlatform_AuthenticateCallbackWrapper_m2896042779 (Il2CppObject * __this /* static, unused */, int32_t ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_AuthenticateCallbackWrapper_m2896042779_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t872614854 * G_B3_0 = NULL;
	Action_1_t872614854 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	Action_1_t872614854 * G_B4_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_PopulateLocalUser_m2583301917(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_1_t872614854 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AuthenticateCallback_0();
		if (!L_0)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AuthenticateCallback_0();
		int32_t L_2 = ___result0;
		G_B2_0 = L_1;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			G_B3_0 = L_1;
			goto IL_0021;
		}
	}
	{
		G_B4_0 = 1;
		G_B4_1 = G_B2_0;
		goto IL_0022;
	}

IL_0021:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0022:
	{
		NullCheck(G_B4_1);
		Action_1_Invoke_m3594021162(G_B4_1, (bool)G_B4_0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_AuthenticateCallback_0((Action_1_t872614854 *)NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearFriends(System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ClearFriends_m1761222218_MetadataUsageId;
extern "C"  void GameCenterPlatform_ClearFriends_m1761222218 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ClearFriends_m1761222218_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		int32_t L_0 = ___size0;
		GameCenterPlatform_SafeClearArray_m2546851889(NULL /*static, unused*/, (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_friends_10()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetFriends(UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetFriends_m3378244042_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetFriends_m3378244042 (Il2CppObject * __this /* static, unused */, GcUserProfileData_t657441114  ___data0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetFriends_m3378244042_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		int32_t L_0 = ___number1;
		GcUserProfileData_AddToArray_m3757655355((&___data0), (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_friends_10()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetFriendImage(UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetFriendImage_m4228294663_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetFriendImage_m4228294663 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetFriendImage_m4228294663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Texture2D_t3884108195 * L_0 = ___texture0;
		int32_t L_1 = ___number1;
		GameCenterPlatform_SafeSetUserImage_m3650098397(NULL /*static, unused*/, (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_friends_10()), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerFriendsCallbackWrapper(System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_TriggerFriendsCallbackWrapper_m3845044787_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerFriendsCallbackWrapper_m3845044787 (Il2CppObject * __this /* static, unused */, int32_t ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerFriendsCallbackWrapper_m3845044787_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t872614854 * G_B5_0 = NULL;
	Action_1_t872614854 * G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	Action_1_t872614854 * G_B6_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		UserProfileU5BU5D_t2378268441* L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_friends_10();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		LocalUser_t1307362368 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		UserProfileU5BU5D_t2378268441* L_2 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_friends_10();
		NullCheck(L_1);
		LocalUser_SetFriends_m3475409220(L_1, (IUserProfileU5BU5D_t3419104218*)(IUserProfileU5BU5D_t3419104218*)L_2, /*hidden argument*/NULL);
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_3 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_FriendsCallback_1();
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_4 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_FriendsCallback_1();
		int32_t L_5 = ___result0;
		G_B4_0 = L_4;
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			G_B5_0 = L_4;
			goto IL_0035;
		}
	}
	{
		G_B6_0 = 1;
		G_B6_1 = G_B4_0;
		goto IL_0036;
	}

IL_0035:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_0036:
	{
		NullCheck(G_B6_1);
		Action_1_Invoke_m3594021162(G_B6_1, (bool)G_B6_0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_003b:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::AchievementCallbackWrapper(UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[])
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* AchievementU5BU5D_t912418020_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m222677977_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3379715660;
extern const uint32_t GameCenterPlatform_AchievementCallbackWrapper_m2031411110_MetadataUsageId;
extern "C"  void GameCenterPlatform_AchievementCallbackWrapper_m2031411110 (Il2CppObject * __this /* static, unused */, GcAchievementDataU5BU5D_t1726768202* ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_AchievementCallbackWrapper_m2031411110_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AchievementU5BU5D_t912418020* V_0 = NULL;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t2349069933 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AchievementLoaderCallback_3();
		if (!L_0)
		{
			goto IL_0053;
		}
	}
	{
		GcAchievementDataU5BU5D_t1726768202* L_1 = ___result0;
		NullCheck(L_1);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral3379715660, /*hidden argument*/NULL);
	}

IL_001c:
	{
		GcAchievementDataU5BU5D_t1726768202* L_2 = ___result0;
		NullCheck(L_2);
		V_0 = ((AchievementU5BU5D_t912418020*)SZArrayNew(AchievementU5BU5D_t912418020_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))));
		V_1 = 0;
		goto IL_003f;
	}

IL_002c:
	{
		AchievementU5BU5D_t912418020* L_3 = V_0;
		int32_t L_4 = V_1;
		GcAchievementDataU5BU5D_t1726768202* L_5 = ___result0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		Achievement_t344600729 * L_7 = GcAchievementData_ToAchievement_m3239514930(((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6))), /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		ArrayElementTypeCheck (L_3, L_7);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (Achievement_t344600729 *)L_7);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_9 = V_1;
		GcAchievementDataU5BU5D_t1726768202* L_10 = ___result0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t2349069933 * L_11 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AchievementLoaderCallback_3();
		AchievementU5BU5D_t912418020* L_12 = V_0;
		NullCheck(L_11);
		Action_1_Invoke_m222677977(L_11, (IAchievementU5BU5D_t1953253797*)(IAchievementU5BU5D_t1953253797*)L_12, /*hidden argument*/Action_1_Invoke_m222677977_MethodInfo_var);
	}

IL_0053:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ProgressCallbackWrapper(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ProgressCallbackWrapper_m165794409_MetadataUsageId;
extern "C"  void GameCenterPlatform_ProgressCallbackWrapper_m165794409 (Il2CppObject * __this /* static, unused */, bool ___success0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ProgressCallbackWrapper_m165794409_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ProgressCallback_4();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ProgressCallback_4();
		bool L_2 = ___success0;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ScoreCallbackWrapper(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ScoreCallbackWrapper_m2797312324_MetadataUsageId;
extern "C"  void GameCenterPlatform_ScoreCallbackWrapper_m2797312324 (Il2CppObject * __this /* static, unused */, bool ___success0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ScoreCallbackWrapper_m2797312324_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ScoreCallback_5();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ScoreCallback_5();
		bool L_2 = ___success0;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ScoreLoaderCallbackWrapper(UnityEngine.SocialPlatforms.GameCenter.GcScoreData[])
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m50340758_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ScoreLoaderCallbackWrapper_m2588839053_MetadataUsageId;
extern "C"  void GameCenterPlatform_ScoreLoaderCallbackWrapper_m2588839053 (Il2CppObject * __this /* static, unused */, GcScoreDataU5BU5D_t1670395707* ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ScoreLoaderCallbackWrapper_m2588839053_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ScoreU5BU5D_t2926278037* V_0 = NULL;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t645920862 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ScoreLoaderCallback_6();
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		GcScoreDataU5BU5D_t1670395707* L_1 = ___result0;
		NullCheck(L_1);
		V_0 = ((ScoreU5BU5D_t2926278037*)SZArrayNew(ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))));
		V_1 = 0;
		goto IL_002d;
	}

IL_001a:
	{
		ScoreU5BU5D_t2926278037* L_2 = V_0;
		int32_t L_3 = V_1;
		GcScoreDataU5BU5D_t1670395707* L_4 = ___result0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Score_t3396031228 * L_6 = GcScoreData_ToScore_m2728389301(((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5))), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_6);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Score_t3396031228 *)L_6);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_8 = V_1;
		GcScoreDataU5BU5D_t1670395707* L_9 = ___result0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t645920862 * L_10 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ScoreLoaderCallback_6();
		ScoreU5BU5D_t2926278037* L_11 = V_0;
		NullCheck(L_10);
		Action_1_Invoke_m50340758(L_10, (IScoreU5BU5D_t250104726*)(IScoreU5BU5D_t250104726*)L_11, /*hidden argument*/Action_1_Invoke_m50340758_MethodInfo_var);
	}

IL_0041:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.ILocalUser UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::get_localUser()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalUser_t1307362368_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral48;
extern const uint32_t GameCenterPlatform_get_localUser_m1634439374_MetadataUsageId;
extern "C"  Il2CppObject * GameCenterPlatform_get_localUser_m1634439374 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_get_localUser_m1634439374_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		LocalUser_t1307362368 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		LocalUser_t1307362368 * L_1 = (LocalUser_t1307362368 *)il2cpp_codegen_object_new(LocalUser_t1307362368_il2cpp_TypeInfo_var);
		LocalUser__ctor_m1052633066(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_m_LocalUser_13(L_1);
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		bool L_2 = GameCenterPlatform_Internal_Authenticated_m2780967960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		LocalUser_t1307362368 * L_3 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		NullCheck(L_3);
		String_t* L_4 = UserProfile_get_id_m2095754825(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_4, _stringLiteral48, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_PopulateLocalUser_m2583301917(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		LocalUser_t1307362368 * L_6 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		return L_6;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::PopulateLocalUser()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_PopulateLocalUser_m2583301917_MetadataUsageId;
extern "C"  void GameCenterPlatform_PopulateLocalUser_m2583301917 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_PopulateLocalUser_m2583301917_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		LocalUser_t1307362368 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		bool L_1 = GameCenterPlatform_Internal_Authenticated_m2780967960(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		LocalUser_SetAuthenticated_m653377406(L_0, L_1, /*hidden argument*/NULL);
		LocalUser_t1307362368 * L_2 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		String_t* L_3 = GameCenterPlatform_Internal_UserName_m1252299660(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		UserProfile_SetUserName_m914181770(L_2, L_3, /*hidden argument*/NULL);
		LocalUser_t1307362368 * L_4 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		String_t* L_5 = GameCenterPlatform_Internal_UserID_m385481212(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		UserProfile_SetUserID_m1515238170(L_4, L_5, /*hidden argument*/NULL);
		LocalUser_t1307362368 * L_6 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		bool L_7 = GameCenterPlatform_Internal_Underage_m4169738944(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		LocalUser_SetUnderage_m2968368872(L_6, L_7, /*hidden argument*/NULL);
		LocalUser_t1307362368 * L_8 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		Texture2D_t3884108195 * L_9 = GameCenterPlatform_Internal_UserImage_m3175776130(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		UserProfile_SetImage_m1928130753(L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadAchievementDescriptions(System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>)
extern Il2CppClass* AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2766540343_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadAchievementDescriptions_m232801667_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadAchievementDescriptions_m232801667 (GameCenterPlatform_t3570684786 * __this, Action_1_t229750097 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadAchievementDescriptions_m232801667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t229750097 * L_1 = ___callback0;
		NullCheck(L_1);
		Action_1_Invoke_m2766540343(L_1, (IAchievementDescriptionU5BU5D_t4128901257*)(IAchievementDescriptionU5BU5D_t4128901257*)((AchievementDescriptionU5BU5D_t759444790*)SZArrayNew(AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m2766540343_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t229750097 * L_2 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_AchievementDescriptionLoaderCallback_2(L_2);
		GameCenterPlatform_Internal_LoadAchievementDescriptions_m1394384079(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ReportProgress(System.String,System.Double,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ReportProgress_m4110499833_MetadataUsageId;
extern "C"  void GameCenterPlatform_ReportProgress_m4110499833 (GameCenterPlatform_t3570684786 * __this, String_t* ___id0, double ___progress1, Action_1_t872614854 * ___callback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ReportProgress_m4110499833_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t872614854 * L_1 = ___callback2;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, (bool)0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t872614854 * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_ProgressCallback_4(L_2);
		String_t* L_3 = ___id0;
		double L_4 = ___progress1;
		GameCenterPlatform_Internal_ReportProgress_m2511520970(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadAchievements(System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>)
extern Il2CppClass* AchievementU5BU5D_t912418020_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m222677977_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadAchievements_m2745782249_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadAchievements_m2745782249 (GameCenterPlatform_t3570684786 * __this, Action_1_t2349069933 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadAchievements_m2745782249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t2349069933 * L_1 = ___callback0;
		NullCheck(L_1);
		Action_1_Invoke_m222677977(L_1, (IAchievementU5BU5D_t1953253797*)(IAchievementU5BU5D_t1953253797*)((AchievementU5BU5D_t912418020*)SZArrayNew(AchievementU5BU5D_t912418020_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m222677977_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t2349069933 * L_2 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_AchievementLoaderCallback_3(L_2);
		GameCenterPlatform_Internal_LoadAchievements_m817891229(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ReportScore(System.Int64,System.String,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ReportScore_m1009544586_MetadataUsageId;
extern "C"  void GameCenterPlatform_ReportScore_m1009544586 (GameCenterPlatform_t3570684786 * __this, int64_t ___score0, String_t* ___board1, Action_1_t872614854 * ___callback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ReportScore_m1009544586_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t872614854 * L_1 = ___callback2;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, (bool)0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t872614854 * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_ScoreCallback_5(L_2);
		int64_t L_3 = ___score0;
		String_t* L_4 = ___board1;
		GameCenterPlatform_Internal_ReportScore_m408601947(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadScores(System.String,System.Action`1<UnityEngine.SocialPlatforms.IScore[]>)
extern Il2CppClass* ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m50340758_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadScores_m3562614827_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadScores_m3562614827 (GameCenterPlatform_t3570684786 * __this, String_t* ___category0, Action_1_t645920862 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadScores_m3562614827_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t645920862 * L_1 = ___callback1;
		NullCheck(L_1);
		Action_1_Invoke_m50340758(L_1, (IScoreU5BU5D_t250104726*)(IScoreU5BU5D_t250104726*)((ScoreU5BU5D_t2926278037*)SZArrayNew(ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m50340758_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t645920862 * L_2 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_ScoreLoaderCallback_6(L_2);
		String_t* L_3 = ___category0;
		GameCenterPlatform_Internal_LoadScores_m523283944(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadScores(UnityEngine.SocialPlatforms.ILeaderboard,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* Leaderboard_t1185876199_il2cpp_TypeInfo_var;
extern Il2CppClass* GcLeaderboard_t1820874799_il2cpp_TypeInfo_var;
extern Il2CppClass* ILeaderboard_t3799088250_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1416233310_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadScores_m2883394111_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadScores_m2883394111 (GameCenterPlatform_t3570684786 * __this, Il2CppObject * ___board0, Action_1_t872614854 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadScores_m2883394111_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Leaderboard_t1185876199 * V_0 = NULL;
	GcLeaderboard_t1820874799 * V_1 = NULL;
	Range_t1533311935  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Range_t1533311935  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t872614854 * L_1 = ___callback1;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, (bool)0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t872614854 * L_2 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_LeaderboardCallback_7(L_2);
		Il2CppObject * L_3 = ___board0;
		V_0 = ((Leaderboard_t1185876199 *)CastclassClass(L_3, Leaderboard_t1185876199_il2cpp_TypeInfo_var));
		Leaderboard_t1185876199 * L_4 = V_0;
		GcLeaderboard_t1820874799 * L_5 = (GcLeaderboard_t1820874799 *)il2cpp_codegen_object_new(GcLeaderboard_t1820874799_il2cpp_TypeInfo_var);
		GcLeaderboard__ctor_m4042810199(L_5, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		List_1_t3189060351 * L_6 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_GcBoards_14();
		GcLeaderboard_t1820874799 * L_7 = V_1;
		NullCheck(L_6);
		List_1_Add_m1416233310(L_6, L_7, /*hidden argument*/List_1_Add_m1416233310_MethodInfo_var);
		Leaderboard_t1185876199 * L_8 = V_0;
		NullCheck(L_8);
		StringU5BU5D_t4054002952* L_9 = Leaderboard_GetUserFilter_m3119905721(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		GcLeaderboard_t1820874799 * L_10 = V_1;
		Il2CppObject * L_11 = ___board0;
		NullCheck(L_11);
		String_t* L_12 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_11);
		Il2CppObject * L_13 = ___board0;
		NullCheck(L_13);
		int32_t L_14 = InterfaceFuncInvoker0< int32_t >::Invoke(3 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_13);
		Leaderboard_t1185876199 * L_15 = V_0;
		NullCheck(L_15);
		StringU5BU5D_t4054002952* L_16 = Leaderboard_GetUserFilter_m3119905721(L_15, /*hidden argument*/NULL);
		NullCheck(L_10);
		GcLeaderboard_Internal_LoadScoresWithUsers_m1315210452(L_10, L_12, L_14, L_16, /*hidden argument*/NULL);
		goto IL_0091;
	}

IL_005d:
	{
		GcLeaderboard_t1820874799 * L_17 = V_1;
		Il2CppObject * L_18 = ___board0;
		NullCheck(L_18);
		String_t* L_19 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_18);
		Il2CppObject * L_20 = ___board0;
		NullCheck(L_20);
		Range_t1533311935  L_21 = InterfaceFuncInvoker0< Range_t1533311935  >::Invoke(2 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_20);
		V_2 = L_21;
		int32_t L_22 = (&V_2)->get_from_0();
		Il2CppObject * L_23 = ___board0;
		NullCheck(L_23);
		Range_t1533311935  L_24 = InterfaceFuncInvoker0< Range_t1533311935  >::Invoke(2 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_23);
		V_3 = L_24;
		int32_t L_25 = (&V_3)->get_count_1();
		Il2CppObject * L_26 = ___board0;
		NullCheck(L_26);
		int32_t L_27 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.ILeaderboard::get_userScope() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_26);
		Il2CppObject * L_28 = ___board0;
		NullCheck(L_28);
		int32_t L_29 = InterfaceFuncInvoker0< int32_t >::Invoke(3 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_28);
		NullCheck(L_17);
		GcLeaderboard_Internal_LoadScores_m1783152707(L_17, L_19, L_22, L_25, L_27, L_29, /*hidden argument*/NULL);
	}

IL_0091:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LeaderboardCallbackWrapper(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LeaderboardCallbackWrapper_m2165153529_MetadataUsageId;
extern "C"  void GameCenterPlatform_LeaderboardCallbackWrapper_m2165153529 (Il2CppObject * __this /* static, unused */, bool ___success0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LeaderboardCallbackWrapper_m2165153529_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_LeaderboardCallback_7();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_LeaderboardCallback_7();
		bool L_2 = ___success0;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::GetLoading(UnityEngine.SocialPlatforms.ILeaderboard)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* Leaderboard_t1185876199_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t3208733121_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m173797613_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3622080807_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4196185109_MethodInfo_var;
extern const uint32_t GameCenterPlatform_GetLoading_m2084830155_MetadataUsageId;
extern "C"  bool GameCenterPlatform_GetLoading_m2084830155 (GameCenterPlatform_t3570684786 * __this, Il2CppObject * ___board0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_GetLoading_m2084830155_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GcLeaderboard_t1820874799 * V_0 = NULL;
	Enumerator_t3208733121  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		List_1_t3189060351 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_GcBoards_14();
		NullCheck(L_1);
		Enumerator_t3208733121  L_2 = List_1_GetEnumerator_m173797613(L_1, /*hidden argument*/List_1_GetEnumerator_m173797613_MethodInfo_var);
		V_1 = L_2;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0042;
		}

IL_001d:
		{
			GcLeaderboard_t1820874799 * L_3 = Enumerator_get_Current_m3622080807((&V_1), /*hidden argument*/Enumerator_get_Current_m3622080807_MethodInfo_var);
			V_0 = L_3;
			GcLeaderboard_t1820874799 * L_4 = V_0;
			Il2CppObject * L_5 = ___board0;
			NullCheck(L_4);
			bool L_6 = GcLeaderboard_Contains_m100384368(L_4, ((Leaderboard_t1185876199 *)CastclassClass(L_5, Leaderboard_t1185876199_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_0042;
			}
		}

IL_0036:
		{
			GcLeaderboard_t1820874799 * L_7 = V_0;
			NullCheck(L_7);
			bool L_8 = GcLeaderboard_Loading_m294711596(L_7, /*hidden argument*/NULL);
			V_2 = L_8;
			IL2CPP_LEAVE(0x61, FINALLY_0053);
		}

IL_0042:
		{
			bool L_9 = Enumerator_MoveNext_m4196185109((&V_1), /*hidden argument*/Enumerator_MoveNext_m4196185109_MethodInfo_var);
			if (L_9)
			{
				goto IL_001d;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0x5F, FINALLY_0053);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0053;
	}

FINALLY_0053:
	{ // begin finally (depth: 1)
		Enumerator_t3208733121  L_10 = V_1;
		Enumerator_t3208733121  L_11 = L_10;
		Il2CppObject * L_12 = Box(Enumerator_t3208733121_il2cpp_TypeInfo_var, &L_11);
		NullCheck((Il2CppObject *)L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
		IL2CPP_END_FINALLY(83)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(83)
	{
		IL2CPP_JUMP_TBL(0x61, IL_0061)
		IL2CPP_JUMP_TBL(0x5F, IL_005f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_005f:
	{
		return (bool)0;
	}

IL_0061:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::VerifyAuthentication()
extern Il2CppClass* ILocalUser_t2654168339_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral708567292;
extern const uint32_t GameCenterPlatform_VerifyAuthentication_m4096949980_MetadataUsageId;
extern "C"  bool GameCenterPlatform_VerifyAuthentication_m4096949980 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_VerifyAuthentication_m4096949980_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = GameCenterPlatform_get_localUser_m1634439374(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated() */, ILocalUser_t2654168339_il2cpp_TypeInfo_var, L_0);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral708567292, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_001c:
	{
		return (bool)1;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowAchievementsUI()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowAchievementsUI_m2437339590_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowAchievementsUI_m2437339590 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowAchievementsUI_m2437339590_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowAchievementsUI_m1934331464(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowLeaderboardUI()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowLeaderboardUI_m302984165_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowLeaderboardUI_m302984165 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowLeaderboardUI_m302984165_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowLeaderboardUI_m3057704739(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearUsers(System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ClearUsers_m4212910653_MetadataUsageId;
extern "C"  void GameCenterPlatform_ClearUsers_m4212910653 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ClearUsers_m4212910653_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		int32_t L_0 = ___size0;
		GameCenterPlatform_SafeClearArray_m2546851889(NULL /*static, unused*/, (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_users_11()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetUser(UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetUser_m847940592_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetUser_m847940592 (Il2CppObject * __this /* static, unused */, GcUserProfileData_t657441114  ___data0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetUser_m847940592_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		int32_t L_0 = ___number1;
		GcUserProfileData_AddToArray_m3757655355((&___data0), (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_users_11()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetUserImage(UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetUserImage_m3066589434_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetUserImage_m3066589434 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetUserImage_m3066589434_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Texture2D_t3884108195 * L_0 = ___texture0;
		int32_t L_1 = ___number1;
		GameCenterPlatform_SafeSetUserImage_m3650098397(NULL /*static, unused*/, (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_users_11()), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerUsersCallbackWrapper()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2276731850_MethodInfo_var;
extern const uint32_t GameCenterPlatform_TriggerUsersCallbackWrapper_m2446471631_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerUsersCallbackWrapper_m2446471631 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerUsersCallbackWrapper_m2446471631_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t3814920354 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_UsersCallback_8();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t3814920354 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_UsersCallback_8();
		UserProfileU5BU5D_t2378268441* L_2 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_users_11();
		NullCheck(L_1);
		Action_1_Invoke_m2276731850(L_1, (IUserProfileU5BU5D_t3419104218*)(IUserProfileU5BU5D_t3419104218*)L_2, /*hidden argument*/Action_1_Invoke_m2276731850_MethodInfo_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadUsers(System.String[],System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>)
extern Il2CppClass* UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2276731850_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadUsers_m981272890_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadUsers_m981272890 (GameCenterPlatform_t3570684786 * __this, StringU5BU5D_t4054002952* ___userIds0, Action_1_t3814920354 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadUsers_m981272890_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t3814920354 * L_1 = ___callback1;
		NullCheck(L_1);
		Action_1_Invoke_m2276731850(L_1, (IUserProfileU5BU5D_t3419104218*)(IUserProfileU5BU5D_t3419104218*)((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m2276731850_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t3814920354 * L_2 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_UsersCallback_8(L_2);
		StringU5BU5D_t4054002952* L_3 = ___userIds0;
		GameCenterPlatform_Internal_LoadUsers_m4218820079(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SafeSetUserImage(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3884108195_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral183309550;
extern Il2CppCodeGenString* _stringLiteral2135574971;
extern const uint32_t GameCenterPlatform_SafeSetUserImage_m3650098397_MetadataUsageId;
extern "C"  void GameCenterPlatform_SafeSetUserImage_m3650098397 (Il2CppObject * __this /* static, unused */, UserProfileU5BU5D_t2378268441** ___array0, Texture2D_t3884108195 * ___texture1, int32_t ___number2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SafeSetUserImage_m3650098397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t2378268441** L_0 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_0)));
		int32_t L_1 = ___number2;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2378268441**)L_0)))->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = ___number2;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0026;
		}
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral183309550, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_3 = (Texture2D_t3884108195 *)il2cpp_codegen_object_new(Texture2D_t3884108195_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1883511258(L_3, ((int32_t)76), ((int32_t)76), /*hidden argument*/NULL);
		___texture1 = L_3;
	}

IL_0026:
	{
		UserProfileU5BU5D_t2378268441** L_4 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_4)));
		int32_t L_5 = ___number2;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2378268441**)L_4)))->max_length))))) <= ((int32_t)L_5)))
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_6 = ___number2;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_0046;
		}
	}
	{
		UserProfileU5BU5D_t2378268441** L_7 = ___array0;
		int32_t L_8 = ___number2;
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_7)));
		IL2CPP_ARRAY_BOUNDS_CHECK((*((UserProfileU5BU5D_t2378268441**)L_7)), L_8);
		int32_t L_9 = L_8;
		UserProfile_t2280656072 * L_10 = ((*((UserProfileU5BU5D_t2378268441**)L_7)))->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		Texture2D_t3884108195 * L_11 = ___texture1;
		NullCheck(L_10);
		UserProfile_SetImage_m1928130753(L_10, L_11, /*hidden argument*/NULL);
		goto IL_0050;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2135574971, /*hidden argument*/NULL);
	}

IL_0050:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SafeClearArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern Il2CppClass* UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SafeClearArray_m2546851889_MetadataUsageId;
extern "C"  void GameCenterPlatform_SafeClearArray_m2546851889 (Il2CppObject * __this /* static, unused */, UserProfileU5BU5D_t2378268441** ___array0, int32_t ___size1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SafeClearArray_m2546851889_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t2378268441** L_0 = ___array0;
		if (!(*((UserProfileU5BU5D_t2378268441**)L_0)))
		{
			goto IL_0011;
		}
	}
	{
		UserProfileU5BU5D_t2378268441** L_1 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_1)));
		int32_t L_2 = ___size1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2378268441**)L_1)))->max_length))))) == ((int32_t)L_2)))
		{
			goto IL_0019;
		}
	}

IL_0011:
	{
		UserProfileU5BU5D_t2378268441** L_3 = ___array0;
		int32_t L_4 = ___size1;
		*((Il2CppObject **)(L_3)) = (Il2CppObject *)((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)L_4));
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_3), (Il2CppObject *)((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)L_4)));
	}

IL_0019:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.ILeaderboard UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::CreateLeaderboard()
extern Il2CppClass* Leaderboard_t1185876199_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_CreateLeaderboard_m2295049883_MetadataUsageId;
extern "C"  Il2CppObject * GameCenterPlatform_CreateLeaderboard_m2295049883 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_CreateLeaderboard_m2295049883_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Leaderboard_t1185876199 * V_0 = NULL;
	{
		Leaderboard_t1185876199 * L_0 = (Leaderboard_t1185876199 *)il2cpp_codegen_object_new(Leaderboard_t1185876199_il2cpp_TypeInfo_var);
		Leaderboard__ctor_m596857571(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Leaderboard_t1185876199 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.SocialPlatforms.IAchievement UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::CreateAchievement()
extern Il2CppClass* Achievement_t344600729_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_CreateAchievement_m1828880347_MetadataUsageId;
extern "C"  Il2CppObject * GameCenterPlatform_CreateAchievement_m1828880347 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_CreateAchievement_m1828880347_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Achievement_t344600729 * V_0 = NULL;
	{
		Achievement_t344600729 * L_0 = (Achievement_t344600729 *)il2cpp_codegen_object_new(Achievement_t344600729_il2cpp_TypeInfo_var);
		Achievement__ctor_m3345265521(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Achievement_t344600729 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerResetAchievementCallback(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_TriggerResetAchievementCallback_m1285257317_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerResetAchievementCallback_m1285257317 (Il2CppObject * __this /* static, unused */, bool ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerResetAchievementCallback_m1285257317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ResetAchievements_12();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ResetAchievements_12();
		bool L_2 = ___result0;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
extern Il2CppClass* Achievement_t344600729_il2cpp_TypeInfo_var;
extern const uint32_t GcAchievementData_ToAchievement_m3239514930_MetadataUsageId;
extern "C"  Achievement_t344600729 * GcAchievementData_ToAchievement_m3239514930 (GcAchievementData_t3481375915 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcAchievementData_ToAchievement_m3239514930_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t4283661327  V_0;
	memset(&V_0, 0, sizeof(V_0));
	double G_B2_0 = 0.0;
	String_t* G_B2_1 = NULL;
	double G_B1_0 = 0.0;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	double G_B3_1 = 0.0;
	String_t* G_B3_2 = NULL;
	int32_t G_B5_0 = 0;
	double G_B5_1 = 0.0;
	String_t* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	double G_B4_1 = 0.0;
	String_t* G_B4_2 = NULL;
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	double G_B6_2 = 0.0;
	String_t* G_B6_3 = NULL;
	{
		String_t* L_0 = __this->get_m_Identifier_0();
		double L_1 = __this->get_m_PercentCompleted_1();
		int32_t L_2 = __this->get_m_Completed_2();
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_001d;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_001e:
	{
		int32_t L_3 = __this->get_m_Hidden_3();
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
		if (L_3)
		{
			G_B5_0 = G_B3_0;
			G_B5_1 = G_B3_1;
			G_B5_2 = G_B3_2;
			goto IL_002f;
		}
	}
	{
		G_B6_0 = 0;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0030;
	}

IL_002f:
	{
		G_B6_0 = 1;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0030:
	{
		DateTime__ctor_m1594789867((&V_0), ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_m_LastReportedDate_4();
		DateTime_t4283661327  L_5 = DateTime_AddSeconds_m2515640243((&V_0), (((double)((double)L_4))), /*hidden argument*/NULL);
		Achievement_t344600729 * L_6 = (Achievement_t344600729 *)il2cpp_codegen_object_new(Achievement_t344600729_il2cpp_TypeInfo_var);
		Achievement__ctor_m377036415(L_6, G_B6_3, G_B6_2, (bool)G_B6_1, (bool)G_B6_0, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  Achievement_t344600729 * GcAchievementData_ToAchievement_m3239514930_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GcAchievementData_t3481375915 * _thisAdjusted = reinterpret_cast<GcAchievementData_t3481375915 *>(__this + 1);
	return GcAchievementData_ToAchievement_m3239514930(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t3481375915_marshal_pinvoke(const GcAchievementData_t3481375915& unmarshaled, GcAchievementData_t3481375915_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Identifier_0 = il2cpp_codegen_marshal_string(unmarshaled.get_m_Identifier_0());
	marshaled.___m_PercentCompleted_1 = unmarshaled.get_m_PercentCompleted_1();
	marshaled.___m_Completed_2 = unmarshaled.get_m_Completed_2();
	marshaled.___m_Hidden_3 = unmarshaled.get_m_Hidden_3();
	marshaled.___m_LastReportedDate_4 = unmarshaled.get_m_LastReportedDate_4();
}
extern "C" void GcAchievementData_t3481375915_marshal_pinvoke_back(const GcAchievementData_t3481375915_marshaled_pinvoke& marshaled, GcAchievementData_t3481375915& unmarshaled)
{
	unmarshaled.set_m_Identifier_0(il2cpp_codegen_marshal_string_result(marshaled.___m_Identifier_0));
	double unmarshaled_m_PercentCompleted_temp_1 = 0.0;
	unmarshaled_m_PercentCompleted_temp_1 = marshaled.___m_PercentCompleted_1;
	unmarshaled.set_m_PercentCompleted_1(unmarshaled_m_PercentCompleted_temp_1);
	int32_t unmarshaled_m_Completed_temp_2 = 0;
	unmarshaled_m_Completed_temp_2 = marshaled.___m_Completed_2;
	unmarshaled.set_m_Completed_2(unmarshaled_m_Completed_temp_2);
	int32_t unmarshaled_m_Hidden_temp_3 = 0;
	unmarshaled_m_Hidden_temp_3 = marshaled.___m_Hidden_3;
	unmarshaled.set_m_Hidden_3(unmarshaled_m_Hidden_temp_3);
	int32_t unmarshaled_m_LastReportedDate_temp_4 = 0;
	unmarshaled_m_LastReportedDate_temp_4 = marshaled.___m_LastReportedDate_4;
	unmarshaled.set_m_LastReportedDate_4(unmarshaled_m_LastReportedDate_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t3481375915_marshal_pinvoke_cleanup(GcAchievementData_t3481375915_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Identifier_0);
	marshaled.___m_Identifier_0 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t3481375915_marshal_com(const GcAchievementData_t3481375915& unmarshaled, GcAchievementData_t3481375915_marshaled_com& marshaled)
{
	marshaled.___m_Identifier_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_Identifier_0());
	marshaled.___m_PercentCompleted_1 = unmarshaled.get_m_PercentCompleted_1();
	marshaled.___m_Completed_2 = unmarshaled.get_m_Completed_2();
	marshaled.___m_Hidden_3 = unmarshaled.get_m_Hidden_3();
	marshaled.___m_LastReportedDate_4 = unmarshaled.get_m_LastReportedDate_4();
}
extern "C" void GcAchievementData_t3481375915_marshal_com_back(const GcAchievementData_t3481375915_marshaled_com& marshaled, GcAchievementData_t3481375915& unmarshaled)
{
	unmarshaled.set_m_Identifier_0(il2cpp_codegen_marshal_bstring_result(marshaled.___m_Identifier_0));
	double unmarshaled_m_PercentCompleted_temp_1 = 0.0;
	unmarshaled_m_PercentCompleted_temp_1 = marshaled.___m_PercentCompleted_1;
	unmarshaled.set_m_PercentCompleted_1(unmarshaled_m_PercentCompleted_temp_1);
	int32_t unmarshaled_m_Completed_temp_2 = 0;
	unmarshaled_m_Completed_temp_2 = marshaled.___m_Completed_2;
	unmarshaled.set_m_Completed_2(unmarshaled_m_Completed_temp_2);
	int32_t unmarshaled_m_Hidden_temp_3 = 0;
	unmarshaled_m_Hidden_temp_3 = marshaled.___m_Hidden_3;
	unmarshaled.set_m_Hidden_3(unmarshaled_m_Hidden_temp_3);
	int32_t unmarshaled_m_LastReportedDate_temp_4 = 0;
	unmarshaled_m_LastReportedDate_temp_4 = marshaled.___m_LastReportedDate_4;
	unmarshaled.set_m_LastReportedDate_4(unmarshaled_m_LastReportedDate_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t3481375915_marshal_com_cleanup(GcAchievementData_t3481375915_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_Identifier_0);
	marshaled.___m_Identifier_0 = NULL;
}
// UnityEngine.SocialPlatforms.Impl.AchievementDescription UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::ToAchievementDescription()
extern Il2CppClass* AchievementDescription_t2116066607_il2cpp_TypeInfo_var;
extern const uint32_t GcAchievementDescriptionData_ToAchievementDescription_m3125480712_MetadataUsageId;
extern "C"  AchievementDescription_t2116066607 * GcAchievementDescriptionData_ToAchievementDescription_m3125480712 (GcAchievementDescriptionData_t2242891083 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcAchievementDescriptionData_ToAchievementDescription_m3125480712_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B2_1 = NULL;
	Texture2D_t3884108195 * G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	String_t* G_B2_4 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B1_1 = NULL;
	Texture2D_t3884108195 * G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	String_t* G_B1_4 = NULL;
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = NULL;
	String_t* G_B3_2 = NULL;
	Texture2D_t3884108195 * G_B3_3 = NULL;
	String_t* G_B3_4 = NULL;
	String_t* G_B3_5 = NULL;
	{
		String_t* L_0 = __this->get_m_Identifier_0();
		String_t* L_1 = __this->get_m_Title_1();
		Texture2D_t3884108195 * L_2 = __this->get_m_Image_2();
		String_t* L_3 = __this->get_m_AchievedDescription_3();
		String_t* L_4 = __this->get_m_UnachievedDescription_4();
		int32_t L_5 = __this->get_m_Hidden_5();
		G_B1_0 = L_4;
		G_B1_1 = L_3;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		G_B1_4 = L_0;
		if (L_5)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_3;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			G_B2_4 = L_0;
			goto IL_002f;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		G_B3_5 = G_B1_4;
		goto IL_0030;
	}

IL_002f:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
		G_B3_5 = G_B2_4;
	}

IL_0030:
	{
		int32_t L_6 = __this->get_m_Points_6();
		AchievementDescription_t2116066607 * L_7 = (AchievementDescription_t2116066607 *)il2cpp_codegen_object_new(AchievementDescription_t2116066607_il2cpp_TypeInfo_var);
		AchievementDescription__ctor_m3032164909(L_7, G_B3_5, G_B3_4, G_B3_3, G_B3_2, G_B3_1, (bool)G_B3_0, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  AchievementDescription_t2116066607 * GcAchievementDescriptionData_ToAchievementDescription_m3125480712_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GcAchievementDescriptionData_t2242891083 * _thisAdjusted = reinterpret_cast<GcAchievementDescriptionData_t2242891083 *>(__this + 1);
	return GcAchievementDescriptionData_ToAchievementDescription_m3125480712(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_pinvoke(const GcAchievementDescriptionData_t2242891083& unmarshaled, GcAchievementDescriptionData_t2242891083_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Image_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Image' of type 'GcAchievementDescriptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Image_2Exception);
}
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_pinvoke_back(const GcAchievementDescriptionData_t2242891083_marshaled_pinvoke& marshaled, GcAchievementDescriptionData_t2242891083& unmarshaled)
{
	Il2CppCodeGenException* ___m_Image_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Image' of type 'GcAchievementDescriptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Image_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_pinvoke_cleanup(GcAchievementDescriptionData_t2242891083_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_com(const GcAchievementDescriptionData_t2242891083& unmarshaled, GcAchievementDescriptionData_t2242891083_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Image_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Image' of type 'GcAchievementDescriptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Image_2Exception);
}
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_com_back(const GcAchievementDescriptionData_t2242891083_marshaled_com& marshaled, GcAchievementDescriptionData_t2242891083& unmarshaled)
{
	Il2CppCodeGenException* ___m_Image_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Image' of type 'GcAchievementDescriptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Image_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_com_cleanup(GcAchievementDescriptionData_t2242891083_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::.ctor(UnityEngine.SocialPlatforms.Impl.Leaderboard)
extern "C"  void GcLeaderboard__ctor_m4042810199 (GcLeaderboard_t1820874799 * __this, Leaderboard_t1185876199 * ___board0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Leaderboard_t1185876199 * L_0 = ___board0;
		__this->set_m_GenericLeaderboard_1(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Finalize()
extern "C"  void GcLeaderboard_Finalize_m4015840938 (GcLeaderboard_t1820874799 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		GcLeaderboard_Dispose_m301614325(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Contains(UnityEngine.SocialPlatforms.Impl.Leaderboard)
extern "C"  bool GcLeaderboard_Contains_m100384368 (GcLeaderboard_t1820874799 * __this, Leaderboard_t1185876199 * ___board0, const MethodInfo* method)
{
	{
		Leaderboard_t1185876199 * L_0 = __this->get_m_GenericLeaderboard_1();
		Leaderboard_t1185876199 * L_1 = ___board0;
		return (bool)((((Il2CppObject*)(Leaderboard_t1185876199 *)L_0) == ((Il2CppObject*)(Leaderboard_t1185876199 *)L_1))? 1 : 0);
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetScores(UnityEngine.SocialPlatforms.GameCenter.GcScoreData[])
extern Il2CppClass* ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var;
extern const uint32_t GcLeaderboard_SetScores_m1734279820_MetadataUsageId;
extern "C"  void GcLeaderboard_SetScores_m1734279820 (GcLeaderboard_t1820874799 * __this, GcScoreDataU5BU5D_t1670395707* ___scoreDatas0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcLeaderboard_SetScores_m1734279820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ScoreU5BU5D_t2926278037* V_0 = NULL;
	int32_t V_1 = 0;
	{
		Leaderboard_t1185876199 * L_0 = __this->get_m_GenericLeaderboard_1();
		if (!L_0)
		{
			goto IL_0043;
		}
	}
	{
		GcScoreDataU5BU5D_t1670395707* L_1 = ___scoreDatas0;
		NullCheck(L_1);
		V_0 = ((ScoreU5BU5D_t2926278037*)SZArrayNew(ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))));
		V_1 = 0;
		goto IL_002e;
	}

IL_001b:
	{
		ScoreU5BU5D_t2926278037* L_2 = V_0;
		int32_t L_3 = V_1;
		GcScoreDataU5BU5D_t1670395707* L_4 = ___scoreDatas0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Score_t3396031228 * L_6 = GcScoreData_ToScore_m2728389301(((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5))), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_6);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Score_t3396031228 *)L_6);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_8 = V_1;
		GcScoreDataU5BU5D_t1670395707* L_9 = ___scoreDatas0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		Leaderboard_t1185876199 * L_10 = __this->get_m_GenericLeaderboard_1();
		ScoreU5BU5D_t2926278037* L_11 = V_0;
		NullCheck(L_10);
		Leaderboard_SetScores_m1463319879(L_10, (IScoreU5BU5D_t250104726*)(IScoreU5BU5D_t250104726*)L_11, /*hidden argument*/NULL);
	}

IL_0043:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetLocalScore(UnityEngine.SocialPlatforms.GameCenter.GcScoreData)
extern "C"  void GcLeaderboard_SetLocalScore_m3970132532 (GcLeaderboard_t1820874799 * __this, GcScoreData_t2181296590  ___scoreData0, const MethodInfo* method)
{
	{
		Leaderboard_t1185876199 * L_0 = __this->get_m_GenericLeaderboard_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Leaderboard_t1185876199 * L_1 = __this->get_m_GenericLeaderboard_1();
		Score_t3396031228 * L_2 = GcScoreData_ToScore_m2728389301((&___scoreData0), /*hidden argument*/NULL);
		NullCheck(L_1);
		Leaderboard_SetLocalUserScore_m700491556(L_1, L_2, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetMaxRange(System.UInt32)
extern "C"  void GcLeaderboard_SetMaxRange_m3160374921 (GcLeaderboard_t1820874799 * __this, uint32_t ___maxRange0, const MethodInfo* method)
{
	{
		Leaderboard_t1185876199 * L_0 = __this->get_m_GenericLeaderboard_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Leaderboard_t1185876199 * L_1 = __this->get_m_GenericLeaderboard_1();
		uint32_t L_2 = ___maxRange0;
		NullCheck(L_1);
		Leaderboard_SetMaxRange_m3779908734(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetTitle(System.String)
extern "C"  void GcLeaderboard_SetTitle_m3781988000 (GcLeaderboard_t1820874799 * __this, String_t* ___title0, const MethodInfo* method)
{
	{
		Leaderboard_t1185876199 * L_0 = __this->get_m_GenericLeaderboard_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Leaderboard_t1185876199 * L_1 = __this->get_m_GenericLeaderboard_1();
		String_t* L_2 = ___title0;
		NullCheck(L_1);
		Leaderboard_SetTitle_m771163339(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void GcLeaderboard_Internal_LoadScores_m1783152707 (GcLeaderboard_t1820874799 * __this, String_t* ___category0, int32_t ___from1, int32_t ___count2, int32_t ___playerScope3, int32_t ___timeScope4, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Internal_LoadScores_m1783152707_ftn) (GcLeaderboard_t1820874799 *, String_t*, int32_t, int32_t, int32_t, int32_t);
	static GcLeaderboard_Internal_LoadScores_m1783152707_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Internal_LoadScores_m1783152707_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___category0, ___from1, ___count2, ___playerScope3, ___timeScope4);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScoresWithUsers(System.String,System.Int32,System.String[])
extern "C"  void GcLeaderboard_Internal_LoadScoresWithUsers_m1315210452 (GcLeaderboard_t1820874799 * __this, String_t* ___category0, int32_t ___timeScope1, StringU5BU5D_t4054002952* ___userIDs2, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Internal_LoadScoresWithUsers_m1315210452_ftn) (GcLeaderboard_t1820874799 *, String_t*, int32_t, StringU5BU5D_t4054002952*);
	static GcLeaderboard_Internal_LoadScoresWithUsers_m1315210452_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Internal_LoadScoresWithUsers_m1315210452_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScoresWithUsers(System.String,System.Int32,System.String[])");
	_il2cpp_icall_func(__this, ___category0, ___timeScope1, ___userIDs2);
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()
extern "C"  bool GcLeaderboard_Loading_m294711596 (GcLeaderboard_t1820874799 * __this, const MethodInfo* method)
{
	typedef bool (*GcLeaderboard_Loading_m294711596_ftn) (GcLeaderboard_t1820874799 *);
	static GcLeaderboard_Loading_m294711596_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Loading_m294711596_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()
extern "C"  void GcLeaderboard_Dispose_m301614325 (GcLeaderboard_t1820874799 * __this, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Dispose_m301614325_ftn) (GcLeaderboard_t1820874799 *);
	static GcLeaderboard_Dispose_m301614325_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Dispose_m301614325_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()");
	_il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern "C" void GcLeaderboard_t1820874799_marshal_pinvoke(const GcLeaderboard_t1820874799& unmarshaled, GcLeaderboard_t1820874799_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_GenericLeaderboard_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_GenericLeaderboard' of type 'GcLeaderboard': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_GenericLeaderboard_1Exception);
}
extern "C" void GcLeaderboard_t1820874799_marshal_pinvoke_back(const GcLeaderboard_t1820874799_marshaled_pinvoke& marshaled, GcLeaderboard_t1820874799& unmarshaled)
{
	Il2CppCodeGenException* ___m_GenericLeaderboard_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_GenericLeaderboard' of type 'GcLeaderboard': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_GenericLeaderboard_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern "C" void GcLeaderboard_t1820874799_marshal_pinvoke_cleanup(GcLeaderboard_t1820874799_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern "C" void GcLeaderboard_t1820874799_marshal_com(const GcLeaderboard_t1820874799& unmarshaled, GcLeaderboard_t1820874799_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_GenericLeaderboard_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_GenericLeaderboard' of type 'GcLeaderboard': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_GenericLeaderboard_1Exception);
}
extern "C" void GcLeaderboard_t1820874799_marshal_com_back(const GcLeaderboard_t1820874799_marshaled_com& marshaled, GcLeaderboard_t1820874799& unmarshaled)
{
	Il2CppCodeGenException* ___m_GenericLeaderboard_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_GenericLeaderboard' of type 'GcLeaderboard': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_GenericLeaderboard_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern "C" void GcLeaderboard_t1820874799_marshal_com_cleanup(GcLeaderboard_t1820874799_marshaled_com& marshaled)
{
}
// UnityEngine.SocialPlatforms.Impl.Score UnityEngine.SocialPlatforms.GameCenter.GcScoreData::ToScore()
extern Il2CppClass* Score_t3396031228_il2cpp_TypeInfo_var;
extern const uint32_t GcScoreData_ToScore_m2728389301_MetadataUsageId;
extern "C"  Score_t3396031228 * GcScoreData_ToScore_m2728389301 (GcScoreData_t2181296590 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcScoreData_ToScore_m2728389301_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t4283661327  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = __this->get_m_Category_0();
		int32_t L_1 = __this->get_m_ValueHigh_2();
		int32_t L_2 = __this->get_m_ValueLow_1();
		String_t* L_3 = __this->get_m_PlayerID_5();
		DateTime__ctor_m1594789867((&V_0), ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_m_Date_3();
		DateTime_t4283661327  L_5 = DateTime_AddSeconds_m2515640243((&V_0), (((double)((double)L_4))), /*hidden argument*/NULL);
		String_t* L_6 = __this->get_m_FormattedValue_4();
		int32_t L_7 = __this->get_m_Rank_6();
		Score_t3396031228 * L_8 = (Score_t3396031228 *)il2cpp_codegen_object_new(Score_t3396031228_il2cpp_TypeInfo_var);
		Score__ctor_m3768037481(L_8, L_0, ((int64_t)((int64_t)((int64_t)((int64_t)(((int64_t)((int64_t)L_1)))<<(int32_t)((int32_t)32)))+(int64_t)(((int64_t)((int64_t)L_2))))), L_3, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  Score_t3396031228 * GcScoreData_ToScore_m2728389301_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GcScoreData_t2181296590 * _thisAdjusted = reinterpret_cast<GcScoreData_t2181296590 *>(__this + 1);
	return GcScoreData_ToScore_m2728389301(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t2181296590_marshal_pinvoke(const GcScoreData_t2181296590& unmarshaled, GcScoreData_t2181296590_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Category_0 = il2cpp_codegen_marshal_string(unmarshaled.get_m_Category_0());
	marshaled.___m_ValueLow_1 = unmarshaled.get_m_ValueLow_1();
	marshaled.___m_ValueHigh_2 = unmarshaled.get_m_ValueHigh_2();
	marshaled.___m_Date_3 = unmarshaled.get_m_Date_3();
	marshaled.___m_FormattedValue_4 = il2cpp_codegen_marshal_string(unmarshaled.get_m_FormattedValue_4());
	marshaled.___m_PlayerID_5 = il2cpp_codegen_marshal_string(unmarshaled.get_m_PlayerID_5());
	marshaled.___m_Rank_6 = unmarshaled.get_m_Rank_6();
}
extern "C" void GcScoreData_t2181296590_marshal_pinvoke_back(const GcScoreData_t2181296590_marshaled_pinvoke& marshaled, GcScoreData_t2181296590& unmarshaled)
{
	unmarshaled.set_m_Category_0(il2cpp_codegen_marshal_string_result(marshaled.___m_Category_0));
	int32_t unmarshaled_m_ValueLow_temp_1 = 0;
	unmarshaled_m_ValueLow_temp_1 = marshaled.___m_ValueLow_1;
	unmarshaled.set_m_ValueLow_1(unmarshaled_m_ValueLow_temp_1);
	int32_t unmarshaled_m_ValueHigh_temp_2 = 0;
	unmarshaled_m_ValueHigh_temp_2 = marshaled.___m_ValueHigh_2;
	unmarshaled.set_m_ValueHigh_2(unmarshaled_m_ValueHigh_temp_2);
	int32_t unmarshaled_m_Date_temp_3 = 0;
	unmarshaled_m_Date_temp_3 = marshaled.___m_Date_3;
	unmarshaled.set_m_Date_3(unmarshaled_m_Date_temp_3);
	unmarshaled.set_m_FormattedValue_4(il2cpp_codegen_marshal_string_result(marshaled.___m_FormattedValue_4));
	unmarshaled.set_m_PlayerID_5(il2cpp_codegen_marshal_string_result(marshaled.___m_PlayerID_5));
	int32_t unmarshaled_m_Rank_temp_6 = 0;
	unmarshaled_m_Rank_temp_6 = marshaled.___m_Rank_6;
	unmarshaled.set_m_Rank_6(unmarshaled_m_Rank_temp_6);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t2181296590_marshal_pinvoke_cleanup(GcScoreData_t2181296590_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Category_0);
	marshaled.___m_Category_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_FormattedValue_4);
	marshaled.___m_FormattedValue_4 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_PlayerID_5);
	marshaled.___m_PlayerID_5 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t2181296590_marshal_com(const GcScoreData_t2181296590& unmarshaled, GcScoreData_t2181296590_marshaled_com& marshaled)
{
	marshaled.___m_Category_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_Category_0());
	marshaled.___m_ValueLow_1 = unmarshaled.get_m_ValueLow_1();
	marshaled.___m_ValueHigh_2 = unmarshaled.get_m_ValueHigh_2();
	marshaled.___m_Date_3 = unmarshaled.get_m_Date_3();
	marshaled.___m_FormattedValue_4 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_FormattedValue_4());
	marshaled.___m_PlayerID_5 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_PlayerID_5());
	marshaled.___m_Rank_6 = unmarshaled.get_m_Rank_6();
}
extern "C" void GcScoreData_t2181296590_marshal_com_back(const GcScoreData_t2181296590_marshaled_com& marshaled, GcScoreData_t2181296590& unmarshaled)
{
	unmarshaled.set_m_Category_0(il2cpp_codegen_marshal_bstring_result(marshaled.___m_Category_0));
	int32_t unmarshaled_m_ValueLow_temp_1 = 0;
	unmarshaled_m_ValueLow_temp_1 = marshaled.___m_ValueLow_1;
	unmarshaled.set_m_ValueLow_1(unmarshaled_m_ValueLow_temp_1);
	int32_t unmarshaled_m_ValueHigh_temp_2 = 0;
	unmarshaled_m_ValueHigh_temp_2 = marshaled.___m_ValueHigh_2;
	unmarshaled.set_m_ValueHigh_2(unmarshaled_m_ValueHigh_temp_2);
	int32_t unmarshaled_m_Date_temp_3 = 0;
	unmarshaled_m_Date_temp_3 = marshaled.___m_Date_3;
	unmarshaled.set_m_Date_3(unmarshaled_m_Date_temp_3);
	unmarshaled.set_m_FormattedValue_4(il2cpp_codegen_marshal_bstring_result(marshaled.___m_FormattedValue_4));
	unmarshaled.set_m_PlayerID_5(il2cpp_codegen_marshal_bstring_result(marshaled.___m_PlayerID_5));
	int32_t unmarshaled_m_Rank_temp_6 = 0;
	unmarshaled_m_Rank_temp_6 = marshaled.___m_Rank_6;
	unmarshaled.set_m_Rank_6(unmarshaled_m_Rank_temp_6);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t2181296590_marshal_com_cleanup(GcScoreData_t2181296590_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_Category_0);
	marshaled.___m_Category_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_FormattedValue_4);
	marshaled.___m_FormattedValue_4 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_PlayerID_5);
	marshaled.___m_PlayerID_5 = NULL;
}
// UnityEngine.SocialPlatforms.Impl.UserProfile UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::ToUserProfile()
extern Il2CppClass* UserProfile_t2280656072_il2cpp_TypeInfo_var;
extern const uint32_t GcUserProfileData_ToUserProfile_m1509702721_MetadataUsageId;
extern "C"  UserProfile_t2280656072 * GcUserProfileData_ToUserProfile_m1509702721 (GcUserProfileData_t657441114 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcUserProfileData_ToUserProfile_m1509702721_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = NULL;
	String_t* G_B3_2 = NULL;
	{
		String_t* L_0 = __this->get_userName_0();
		String_t* L_1 = __this->get_userID_1();
		int32_t L_2 = __this->get_isFriend_2();
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_001e;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_001f:
	{
		Texture2D_t3884108195 * L_3 = __this->get_image_3();
		UserProfile_t2280656072 * L_4 = (UserProfile_t2280656072 *)il2cpp_codegen_object_new(UserProfile_t2280656072_il2cpp_TypeInfo_var);
		UserProfile__ctor_m2682768015(L_4, G_B3_2, G_B3_1, (bool)G_B3_0, 3, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  UserProfile_t2280656072 * GcUserProfileData_ToUserProfile_m1509702721_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GcUserProfileData_t657441114 * _thisAdjusted = reinterpret_cast<GcUserProfileData_t657441114 *>(__this + 1);
	return GcUserProfileData_ToUserProfile_m1509702721(_thisAdjusted, method);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::AddToArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1596528006;
extern const uint32_t GcUserProfileData_AddToArray_m3757655355_MetadataUsageId;
extern "C"  void GcUserProfileData_AddToArray_m3757655355 (GcUserProfileData_t657441114 * __this, UserProfileU5BU5D_t2378268441** ___array0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcUserProfileData_AddToArray_m3757655355_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t2378268441** L_0 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_0)));
		int32_t L_1 = ___number1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2378268441**)L_0)))->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = ___number1;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		UserProfileU5BU5D_t2378268441** L_3 = ___array0;
		int32_t L_4 = ___number1;
		UserProfile_t2280656072 * L_5 = GcUserProfileData_ToUserProfile_m1509702721(__this, /*hidden argument*/NULL);
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_3)));
		IL2CPP_ARRAY_BOUNDS_CHECK((*((UserProfileU5BU5D_t2378268441**)L_3)), L_4);
		ArrayElementTypeCheck ((*((UserProfileU5BU5D_t2378268441**)L_3)), L_5);
		((*((UserProfileU5BU5D_t2378268441**)L_3)))->SetAt(static_cast<il2cpp_array_size_t>(L_4), (UserProfile_t2280656072 *)L_5);
		goto IL_002a;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1596528006, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
extern "C"  void GcUserProfileData_AddToArray_m3757655355_AdjustorThunk (Il2CppObject * __this, UserProfileU5BU5D_t2378268441** ___array0, int32_t ___number1, const MethodInfo* method)
{
	GcUserProfileData_t657441114 * _thisAdjusted = reinterpret_cast<GcUserProfileData_t657441114 *>(__this + 1);
	GcUserProfileData_AddToArray_m3757655355(_thisAdjusted, ___array0, ___number1, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern "C" void GcUserProfileData_t657441114_marshal_pinvoke(const GcUserProfileData_t657441114& unmarshaled, GcUserProfileData_t657441114_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___image_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'image' of type 'GcUserProfileData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___image_3Exception);
}
extern "C" void GcUserProfileData_t657441114_marshal_pinvoke_back(const GcUserProfileData_t657441114_marshaled_pinvoke& marshaled, GcUserProfileData_t657441114& unmarshaled)
{
	Il2CppCodeGenException* ___image_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'image' of type 'GcUserProfileData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___image_3Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern "C" void GcUserProfileData_t657441114_marshal_pinvoke_cleanup(GcUserProfileData_t657441114_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern "C" void GcUserProfileData_t657441114_marshal_com(const GcUserProfileData_t657441114& unmarshaled, GcUserProfileData_t657441114_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___image_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'image' of type 'GcUserProfileData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___image_3Exception);
}
extern "C" void GcUserProfileData_t657441114_marshal_com_back(const GcUserProfileData_t657441114_marshaled_com& marshaled, GcUserProfileData_t657441114& unmarshaled)
{
	Il2CppCodeGenException* ___image_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'image' of type 'GcUserProfileData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___image_3Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern "C" void GcUserProfileData_t657441114_marshal_com_cleanup(GcUserProfileData_t657441114_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double,System.Boolean,System.Boolean,System.DateTime)
extern "C"  void Achievement__ctor_m377036415 (Achievement_t344600729 * __this, String_t* ___id0, double ___percentCompleted1, bool ___completed2, bool ___hidden3, DateTime_t4283661327  ___lastReportedDate4, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id0;
		Achievement_set_id_m3123429815(__this, L_0, /*hidden argument*/NULL);
		double L_1 = ___percentCompleted1;
		Achievement_set_percentCompleted_m1005987436(__this, L_1, /*hidden argument*/NULL);
		bool L_2 = ___completed2;
		__this->set_m_Completed_0(L_2);
		bool L_3 = ___hidden3;
		__this->set_m_Hidden_1(L_3);
		DateTime_t4283661327  L_4 = ___lastReportedDate4;
		__this->set_m_LastReportedDate_2(L_4);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double)
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern const uint32_t Achievement__ctor_m2960680429_MetadataUsageId;
extern "C"  void Achievement__ctor_m2960680429 (Achievement_t344600729 * __this, String_t* ___id0, double ___percent1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Achievement__ctor_m2960680429_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id0;
		Achievement_set_id_m3123429815(__this, L_0, /*hidden argument*/NULL);
		double L_1 = ___percent1;
		Achievement_set_percentCompleted_m1005987436(__this, L_1, /*hidden argument*/NULL);
		__this->set_m_Hidden_1((bool)0);
		__this->set_m_Completed_0((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_2 = ((DateTime_t4283661327_StaticFields*)DateTime_t4283661327_il2cpp_TypeInfo_var->static_fields)->get_MinValue_13();
		__this->set_m_LastReportedDate_2(L_2);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor()
extern Il2CppCodeGenString* _stringLiteral4010126410;
extern const uint32_t Achievement__ctor_m3345265521_MetadataUsageId;
extern "C"  void Achievement__ctor_m3345265521 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Achievement__ctor_m3345265521_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Achievement__ctor_m2960680429(__this, _stringLiteral4010126410, (0.0), /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32179;
extern const uint32_t Achievement_ToString_m2974157186_MetadataUsageId;
extern "C"  String_t* Achievement_ToString_m2974157186 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Achievement_ToString_m2974157186_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9)));
		String_t* L_1 = Achievement_get_id_m1680539866(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t1108656482* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral32179);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_3 = L_2;
		double L_4 = Achievement_get_percentCompleted_m2492759109(__this, /*hidden argument*/NULL);
		double L_5 = L_4;
		Il2CppObject * L_6 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_6);
		ObjectU5BU5D_t1108656482* L_7 = L_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral32179);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_8 = L_7;
		bool L_9 = Achievement_get_completed_m3689853355(__this, /*hidden argument*/NULL);
		bool L_10 = L_9;
		Il2CppObject * L_11 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 5);
		ArrayElementTypeCheck (L_12, _stringLiteral32179);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_13 = L_12;
		bool L_14 = Achievement_get_hidden_m2954555244(__this, /*hidden argument*/NULL);
		bool L_15 = L_14;
		Il2CppObject * L_16 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 6);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_16);
		ObjectU5BU5D_t1108656482* L_17 = L_13;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 7);
		ArrayElementTypeCheck (L_17, _stringLiteral32179);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_18 = L_17;
		DateTime_t4283661327  L_19 = Achievement_get_lastReportedDate_m445111052(__this, /*hidden argument*/NULL);
		DateTime_t4283661327  L_20 = L_19;
		Il2CppObject * L_21 = Box(DateTime_t4283661327_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 8);
		ArrayElementTypeCheck (L_18, L_21);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m3016520001(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::get_id()
extern "C"  String_t* Achievement_get_id_m1680539866 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CidU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String)
extern "C"  void Achievement_set_id_m3123429815 (Achievement_t344600729 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CidU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Double UnityEngine.SocialPlatforms.Impl.Achievement::get_percentCompleted()
extern "C"  double Achievement_get_percentCompleted_m2492759109 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	{
		double L_0 = __this->get_U3CpercentCompletedU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double)
extern "C"  void Achievement_set_percentCompleted_m1005987436 (Achievement_t344600729 * __this, double ___value0, const MethodInfo* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CpercentCompletedU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_completed()
extern "C"  bool Achievement_get_completed_m3689853355 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Completed_0();
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_hidden()
extern "C"  bool Achievement_get_hidden_m2954555244 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Hidden_1();
		return L_0;
	}
}
// System.DateTime UnityEngine.SocialPlatforms.Impl.Achievement::get_lastReportedDate()
extern "C"  DateTime_t4283661327  Achievement_get_lastReportedDate_m445111052 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	{
		DateTime_t4283661327  L_0 = __this->get_m_LastReportedDate_2();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::.ctor(System.String,System.String,UnityEngine.Texture2D,System.String,System.String,System.Boolean,System.Int32)
extern "C"  void AchievementDescription__ctor_m3032164909 (AchievementDescription_t2116066607 * __this, String_t* ___id0, String_t* ___title1, Texture2D_t3884108195 * ___image2, String_t* ___achievedDescription3, String_t* ___unachievedDescription4, bool ___hidden5, int32_t ___points6, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id0;
		AchievementDescription_set_id_m2215766207(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___title1;
		__this->set_m_Title_0(L_1);
		Texture2D_t3884108195 * L_2 = ___image2;
		__this->set_m_Image_1(L_2);
		String_t* L_3 = ___achievedDescription3;
		__this->set_m_AchievedDescription_2(L_3);
		String_t* L_4 = ___unachievedDescription4;
		__this->set_m_UnachievedDescription_3(L_4);
		bool L_5 = ___hidden5;
		__this->set_m_Hidden_4(L_5);
		int32_t L_6 = ___points6;
		__this->set_m_Points_5(L_6);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32179;
extern const uint32_t AchievementDescription_ToString_m1633092820_MetadataUsageId;
extern "C"  String_t* AchievementDescription_ToString_m1633092820 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AchievementDescription_ToString_m1633092820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)11)));
		String_t* L_1 = AchievementDescription_get_id_m3162941612(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t1108656482* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral32179);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_3 = L_2;
		String_t* L_4 = AchievementDescription_get_title_m1294089737(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_4);
		ObjectU5BU5D_t1108656482* L_5 = L_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, _stringLiteral32179);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_6 = L_5;
		String_t* L_7 = AchievementDescription_get_achievedDescription_m4248956218(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 5);
		ArrayElementTypeCheck (L_8, _stringLiteral32179);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_9 = L_8;
		String_t* L_10 = AchievementDescription_get_unachievedDescription_m3429874753(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 6);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_10);
		ObjectU5BU5D_t1108656482* L_11 = L_9;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 7);
		ArrayElementTypeCheck (L_11, _stringLiteral32179);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_12 = L_11;
		int32_t L_13 = AchievementDescription_get_points_m4158769271(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 8);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)9));
		ArrayElementTypeCheck (L_16, _stringLiteral32179);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_17 = L_16;
		bool L_18 = AchievementDescription_get_hidden_m734162136(__this, /*hidden argument*/NULL);
		bool L_19 = L_18;
		Il2CppObject * L_20 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)10));
		ArrayElementTypeCheck (L_17, L_20);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m3016520001(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::SetImage(UnityEngine.Texture2D)
extern "C"  void AchievementDescription_SetImage_m1092175896 (AchievementDescription_t2116066607 * __this, Texture2D_t3884108195 * ___image0, const MethodInfo* method)
{
	{
		Texture2D_t3884108195 * L_0 = ___image0;
		__this->set_m_Image_1(L_0);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_id()
extern "C"  String_t* AchievementDescription_get_id_m3162941612 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CidU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::set_id(System.String)
extern "C"  void AchievementDescription_set_id_m2215766207 (AchievementDescription_t2116066607 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CidU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_title()
extern "C"  String_t* AchievementDescription_get_title_m1294089737 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_Title_0();
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_achievedDescription()
extern "C"  String_t* AchievementDescription_get_achievedDescription_m4248956218 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_AchievedDescription_2();
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_unachievedDescription()
extern "C"  String_t* AchievementDescription_get_unachievedDescription_m3429874753 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_UnachievedDescription_3();
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_hidden()
extern "C"  bool AchievementDescription_get_hidden_m734162136 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Hidden_4();
		return L_0;
	}
}
// System.Int32 UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_points()
extern "C"  int32_t AchievementDescription_get_points_m4158769271 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Points_5();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::.ctor()
extern Il2CppClass* Score_t3396031228_il2cpp_TypeInfo_var;
extern Il2CppClass* ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3624438231;
extern const uint32_t Leaderboard__ctor_m596857571_MetadataUsageId;
extern "C"  void Leaderboard__ctor_m596857571 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Leaderboard__ctor_m596857571_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Leaderboard_set_id_m2022535593(__this, _stringLiteral3624438231, /*hidden argument*/NULL);
		Range_t1533311935  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Range__ctor_m872630735(&L_0, 1, ((int32_t)10), /*hidden argument*/NULL);
		Leaderboard_set_range_m2830170470(__this, L_0, /*hidden argument*/NULL);
		Leaderboard_set_userScope_m3914830286(__this, 0, /*hidden argument*/NULL);
		Leaderboard_set_timeScope_m3669793618(__this, 2, /*hidden argument*/NULL);
		__this->set_m_Loading_0((bool)0);
		Score_t3396031228 * L_1 = (Score_t3396031228 *)il2cpp_codegen_object_new(Score_t3396031228_il2cpp_TypeInfo_var);
		Score__ctor_m113497156(L_1, _stringLiteral3624438231, (((int64_t)((int64_t)0))), /*hidden argument*/NULL);
		__this->set_m_LocalUserScore_1(L_1);
		__this->set_m_MaxRange_2(0);
		__this->set_m_Scores_3((IScoreU5BU5D_t250104726*)((ScoreU5BU5D_t2926278037*)SZArrayNew(ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_m_Title_4(_stringLiteral3624438231);
		__this->set_m_UserIDs_5(((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)0)));
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t24667981_il2cpp_TypeInfo_var;
extern Il2CppClass* UserScope_t1608660171_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeScope_t1305796361_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral69499590;
extern Il2CppCodeGenString* _stringLiteral1411513442;
extern Il2CppCodeGenString* _stringLiteral1472989374;
extern Il2CppCodeGenString* _stringLiteral3534372753;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral2271991173;
extern Il2CppCodeGenString* _stringLiteral778717479;
extern Il2CppCodeGenString* _stringLiteral4163059537;
extern Il2CppCodeGenString* _stringLiteral978617427;
extern Il2CppCodeGenString* _stringLiteral1548524389;
extern const uint32_t Leaderboard_ToString_m114482384_MetadataUsageId;
extern "C"  String_t* Leaderboard_ToString_m114482384 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Leaderboard_ToString_m114482384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Range_t1533311935  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Range_t1533311935  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)20)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral69499590);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral69499590);
		ObjectU5BU5D_t1108656482* L_1 = L_0;
		String_t* L_2 = Leaderboard_get_id_m2379239336(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_2);
		ObjectU5BU5D_t1108656482* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral1411513442);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1411513442);
		ObjectU5BU5D_t1108656482* L_4 = L_3;
		String_t* L_5 = __this->get_m_Title_4();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_5);
		ObjectU5BU5D_t1108656482* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, _stringLiteral1472989374);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral1472989374);
		ObjectU5BU5D_t1108656482* L_7 = L_6;
		bool L_8 = __this->get_m_Loading_0();
		bool L_9 = L_8;
		Il2CppObject * L_10 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 5);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_10);
		ObjectU5BU5D_t1108656482* L_11 = L_7;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 6);
		ArrayElementTypeCheck (L_11, _stringLiteral3534372753);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral3534372753);
		ObjectU5BU5D_t1108656482* L_12 = L_11;
		Range_t1533311935  L_13 = Leaderboard_get_range_m234965965(__this, /*hidden argument*/NULL);
		V_0 = L_13;
		int32_t L_14 = (&V_0)->get_from_0();
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 7);
		ArrayElementTypeCheck (L_12, L_16);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_16);
		ObjectU5BU5D_t1108656482* L_17 = L_12;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 8);
		ArrayElementTypeCheck (L_17, _stringLiteral44);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral44);
		ObjectU5BU5D_t1108656482* L_18 = L_17;
		Range_t1533311935  L_19 = Leaderboard_get_range_m234965965(__this, /*hidden argument*/NULL);
		V_1 = L_19;
		int32_t L_20 = (&V_1)->get_count_1();
		int32_t L_21 = L_20;
		Il2CppObject * L_22 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)9));
		ArrayElementTypeCheck (L_18, L_22);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_22);
		ObjectU5BU5D_t1108656482* L_23 = L_18;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, ((int32_t)10));
		ArrayElementTypeCheck (L_23, _stringLiteral2271991173);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral2271991173);
		ObjectU5BU5D_t1108656482* L_24 = L_23;
		uint32_t L_25 = __this->get_m_MaxRange_2();
		uint32_t L_26 = L_25;
		Il2CppObject * L_27 = Box(UInt32_t24667981_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)11));
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)L_27);
		ObjectU5BU5D_t1108656482* L_28 = L_24;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, ((int32_t)12));
		ArrayElementTypeCheck (L_28, _stringLiteral778717479);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral778717479);
		ObjectU5BU5D_t1108656482* L_29 = L_28;
		IScoreU5BU5D_t250104726* L_30 = __this->get_m_Scores_3();
		NullCheck(L_30);
		int32_t L_31 = (((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length))));
		Il2CppObject * L_32 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_31);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, ((int32_t)13));
		ArrayElementTypeCheck (L_29, L_32);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)L_32);
		ObjectU5BU5D_t1108656482* L_33 = L_29;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, ((int32_t)14));
		ArrayElementTypeCheck (L_33, _stringLiteral4163059537);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral4163059537);
		ObjectU5BU5D_t1108656482* L_34 = L_33;
		int32_t L_35 = Leaderboard_get_userScope_m3547770469(__this, /*hidden argument*/NULL);
		int32_t L_36 = L_35;
		Il2CppObject * L_37 = Box(UserScope_t1608660171_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)15));
		ArrayElementTypeCheck (L_34, L_37);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)L_37);
		ObjectU5BU5D_t1108656482* L_38 = L_34;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)16));
		ArrayElementTypeCheck (L_38, _stringLiteral978617427);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral978617427);
		ObjectU5BU5D_t1108656482* L_39 = L_38;
		int32_t L_40 = Leaderboard_get_timeScope_m2356081377(__this, /*hidden argument*/NULL);
		int32_t L_41 = L_40;
		Il2CppObject * L_42 = Box(TimeScope_t1305796361_il2cpp_TypeInfo_var, &L_41);
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, ((int32_t)17));
		ArrayElementTypeCheck (L_39, L_42);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)L_42);
		ObjectU5BU5D_t1108656482* L_43 = L_39;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, ((int32_t)18));
		ArrayElementTypeCheck (L_43, _stringLiteral1548524389);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)_stringLiteral1548524389);
		ObjectU5BU5D_t1108656482* L_44 = L_43;
		StringU5BU5D_t4054002952* L_45 = __this->get_m_UserIDs_5();
		NullCheck(L_45);
		int32_t L_46 = (((int32_t)((int32_t)(((Il2CppArray *)L_45)->max_length))));
		Il2CppObject * L_47 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)19));
		ArrayElementTypeCheck (L_44, L_47);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (Il2CppObject *)L_47);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m3016520001(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		return L_48;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetLocalUserScore(UnityEngine.SocialPlatforms.IScore)
extern "C"  void Leaderboard_SetLocalUserScore_m700491556 (Leaderboard_t1185876199 * __this, Il2CppObject * ___score0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___score0;
		__this->set_m_LocalUserScore_1(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetMaxRange(System.UInt32)
extern "C"  void Leaderboard_SetMaxRange_m3779908734 (Leaderboard_t1185876199 * __this, uint32_t ___maxRange0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___maxRange0;
		__this->set_m_MaxRange_2(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetScores(UnityEngine.SocialPlatforms.IScore[])
extern "C"  void Leaderboard_SetScores_m1463319879 (Leaderboard_t1185876199 * __this, IScoreU5BU5D_t250104726* ___scores0, const MethodInfo* method)
{
	{
		IScoreU5BU5D_t250104726* L_0 = ___scores0;
		__this->set_m_Scores_3(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetTitle(System.String)
extern "C"  void Leaderboard_SetTitle_m771163339 (Leaderboard_t1185876199 * __this, String_t* ___title0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___title0;
		__this->set_m_Title_4(L_0);
		return;
	}
}
// System.String[] UnityEngine.SocialPlatforms.Impl.Leaderboard::GetUserFilter()
extern "C"  StringU5BU5D_t4054002952* Leaderboard_GetUserFilter_m3119905721 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t4054002952* L_0 = __this->get_m_UserIDs_5();
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::get_id()
extern "C"  String_t* Leaderboard_get_id_m2379239336 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CidU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_id(System.String)
extern "C"  void Leaderboard_set_id_m2022535593 (Leaderboard_t1185876199 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CidU3Ek__BackingField_6(L_0);
		return;
	}
}
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_userScope()
extern "C"  int32_t Leaderboard_get_userScope_m3547770469 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CuserScopeU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_userScope(UnityEngine.SocialPlatforms.UserScope)
extern "C"  void Leaderboard_set_userScope_m3914830286 (Leaderboard_t1185876199 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CuserScopeU3Ek__BackingField_7(L_0);
		return;
	}
}
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::get_range()
extern "C"  Range_t1533311935  Leaderboard_get_range_m234965965 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	{
		Range_t1533311935  L_0 = __this->get_U3CrangeU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_range(UnityEngine.SocialPlatforms.Range)
extern "C"  void Leaderboard_set_range_m2830170470 (Leaderboard_t1185876199 * __this, Range_t1533311935  ___value0, const MethodInfo* method)
{
	{
		Range_t1533311935  L_0 = ___value0;
		__this->set_U3CrangeU3Ek__BackingField_8(L_0);
		return;
	}
}
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_timeScope()
extern "C"  int32_t Leaderboard_get_timeScope_m2356081377 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CtimeScopeU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_timeScope(UnityEngine.SocialPlatforms.TimeScope)
extern "C"  void Leaderboard_set_timeScope_m3669793618 (Leaderboard_t1185876199 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CtimeScopeU3Ek__BackingField_9(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
