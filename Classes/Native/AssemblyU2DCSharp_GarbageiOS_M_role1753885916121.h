﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_role175
struct  M_role175_t3885916121  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_role175::_qisbaySaidura
	bool ____qisbaySaidura_0;
	// System.Single GarbageiOS.M_role175::_gerirkouGerso
	float ____gerirkouGerso_1;
	// System.String GarbageiOS.M_role175::_meetaiheVerjay
	String_t* ____meetaiheVerjay_2;
	// System.Int32 GarbageiOS.M_role175::_mearsarwalBedi
	int32_t ____mearsarwalBedi_3;
	// System.UInt32 GarbageiOS.M_role175::_dabearca
	uint32_t ____dabearca_4;
	// System.Int32 GarbageiOS.M_role175::_ciceera
	int32_t ____ciceera_5;
	// System.Int32 GarbageiOS.M_role175::_dawxartearHaitrere
	int32_t ____dawxartearHaitrere_6;
	// System.Int32 GarbageiOS.M_role175::_storcairsouSape
	int32_t ____storcairsouSape_7;
	// System.String GarbageiOS.M_role175::_muhechear
	String_t* ____muhechear_8;
	// System.Int32 GarbageiOS.M_role175::_rempepeGearjunar
	int32_t ____rempepeGearjunar_9;
	// System.Single GarbageiOS.M_role175::_baswhairCakorjow
	float ____baswhairCakorjow_10;
	// System.Int32 GarbageiOS.M_role175::_jabeteeQehaichu
	int32_t ____jabeteeQehaichu_11;
	// System.Int32 GarbageiOS.M_role175::_deevuKarsirmar
	int32_t ____deevuKarsirmar_12;
	// System.UInt32 GarbageiOS.M_role175::_rirvoriMisaytre
	uint32_t ____rirvoriMisaytre_13;
	// System.Single GarbageiOS.M_role175::_sairdallSeebeesur
	float ____sairdallSeebeesur_14;
	// System.Int32 GarbageiOS.M_role175::_sejeReldalwur
	int32_t ____sejeReldalwur_15;
	// System.Int32 GarbageiOS.M_role175::_tacearqirTeaba
	int32_t ____tacearqirTeaba_16;
	// System.Single GarbageiOS.M_role175::_velmonur
	float ____velmonur_17;
	// System.Single GarbageiOS.M_role175::_jaskifair
	float ____jaskifair_18;
	// System.Single GarbageiOS.M_role175::_sabea
	float ____sabea_19;
	// System.Single GarbageiOS.M_role175::_chemmawjay
	float ____chemmawjay_20;
	// System.String GarbageiOS.M_role175::_staqalHaiselou
	String_t* ____staqalHaiselou_21;
	// System.UInt32 GarbageiOS.M_role175::_rermo
	uint32_t ____rermo_22;
	// System.UInt32 GarbageiOS.M_role175::_lubu
	uint32_t ____lubu_23;
	// System.String GarbageiOS.M_role175::_celqaiTairfereqis
	String_t* ____celqaiTairfereqis_24;
	// System.UInt32 GarbageiOS.M_role175::_cicisarLearvirtra
	uint32_t ____cicisarLearvirtra_25;
	// System.String GarbageiOS.M_role175::_yairtasDerdu
	String_t* ____yairtasDerdu_26;
	// System.Boolean GarbageiOS.M_role175::_drekituZanorri
	bool ____drekituZanorri_27;
	// System.UInt32 GarbageiOS.M_role175::_beryiru
	uint32_t ____beryiru_28;
	// System.String GarbageiOS.M_role175::_piryay
	String_t* ____piryay_29;
	// System.Boolean GarbageiOS.M_role175::_delmooyee
	bool ____delmooyee_30;
	// System.String GarbageiOS.M_role175::_rasaiStormu
	String_t* ____rasaiStormu_31;
	// System.String GarbageiOS.M_role175::_memel
	String_t* ____memel_32;
	// System.Single GarbageiOS.M_role175::_rucenoKerfawsow
	float ____rucenoKerfawsow_33;
	// System.Single GarbageiOS.M_role175::_whawsa
	float ____whawsa_34;
	// System.Single GarbageiOS.M_role175::_pallstouLoxou
	float ____pallstouLoxou_35;
	// System.Single GarbageiOS.M_role175::_tirjay
	float ____tirjay_36;
	// System.String GarbageiOS.M_role175::_trelcem
	String_t* ____trelcem_37;
	// System.Single GarbageiOS.M_role175::_wasperJerehel
	float ____wasperJerehel_38;
	// System.UInt32 GarbageiOS.M_role175::_virxairkear
	uint32_t ____virxairkear_39;
	// System.Boolean GarbageiOS.M_role175::_neamu
	bool ____neamu_40;
	// System.Int32 GarbageiOS.M_role175::_dawlowgowJeebormea
	int32_t ____dawlowgowJeebormea_41;
	// System.String GarbageiOS.M_role175::_treciCutor
	String_t* ____treciCutor_42;
	// System.Boolean GarbageiOS.M_role175::_veatrurbirGawsow
	bool ____veatrurbirGawsow_43;
	// System.Single GarbageiOS.M_role175::_zurhaTartreeroo
	float ____zurhaTartreeroo_44;
	// System.Single GarbageiOS.M_role175::_nearka
	float ____nearka_45;
	// System.UInt32 GarbageiOS.M_role175::_sawjoSairose
	uint32_t ____sawjoSairose_46;
	// System.String GarbageiOS.M_role175::_wallhorcow
	String_t* ____wallhorcow_47;

public:
	inline static int32_t get_offset_of__qisbaySaidura_0() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____qisbaySaidura_0)); }
	inline bool get__qisbaySaidura_0() const { return ____qisbaySaidura_0; }
	inline bool* get_address_of__qisbaySaidura_0() { return &____qisbaySaidura_0; }
	inline void set__qisbaySaidura_0(bool value)
	{
		____qisbaySaidura_0 = value;
	}

	inline static int32_t get_offset_of__gerirkouGerso_1() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____gerirkouGerso_1)); }
	inline float get__gerirkouGerso_1() const { return ____gerirkouGerso_1; }
	inline float* get_address_of__gerirkouGerso_1() { return &____gerirkouGerso_1; }
	inline void set__gerirkouGerso_1(float value)
	{
		____gerirkouGerso_1 = value;
	}

	inline static int32_t get_offset_of__meetaiheVerjay_2() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____meetaiheVerjay_2)); }
	inline String_t* get__meetaiheVerjay_2() const { return ____meetaiheVerjay_2; }
	inline String_t** get_address_of__meetaiheVerjay_2() { return &____meetaiheVerjay_2; }
	inline void set__meetaiheVerjay_2(String_t* value)
	{
		____meetaiheVerjay_2 = value;
		Il2CppCodeGenWriteBarrier(&____meetaiheVerjay_2, value);
	}

	inline static int32_t get_offset_of__mearsarwalBedi_3() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____mearsarwalBedi_3)); }
	inline int32_t get__mearsarwalBedi_3() const { return ____mearsarwalBedi_3; }
	inline int32_t* get_address_of__mearsarwalBedi_3() { return &____mearsarwalBedi_3; }
	inline void set__mearsarwalBedi_3(int32_t value)
	{
		____mearsarwalBedi_3 = value;
	}

	inline static int32_t get_offset_of__dabearca_4() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____dabearca_4)); }
	inline uint32_t get__dabearca_4() const { return ____dabearca_4; }
	inline uint32_t* get_address_of__dabearca_4() { return &____dabearca_4; }
	inline void set__dabearca_4(uint32_t value)
	{
		____dabearca_4 = value;
	}

	inline static int32_t get_offset_of__ciceera_5() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____ciceera_5)); }
	inline int32_t get__ciceera_5() const { return ____ciceera_5; }
	inline int32_t* get_address_of__ciceera_5() { return &____ciceera_5; }
	inline void set__ciceera_5(int32_t value)
	{
		____ciceera_5 = value;
	}

	inline static int32_t get_offset_of__dawxartearHaitrere_6() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____dawxartearHaitrere_6)); }
	inline int32_t get__dawxartearHaitrere_6() const { return ____dawxartearHaitrere_6; }
	inline int32_t* get_address_of__dawxartearHaitrere_6() { return &____dawxartearHaitrere_6; }
	inline void set__dawxartearHaitrere_6(int32_t value)
	{
		____dawxartearHaitrere_6 = value;
	}

	inline static int32_t get_offset_of__storcairsouSape_7() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____storcairsouSape_7)); }
	inline int32_t get__storcairsouSape_7() const { return ____storcairsouSape_7; }
	inline int32_t* get_address_of__storcairsouSape_7() { return &____storcairsouSape_7; }
	inline void set__storcairsouSape_7(int32_t value)
	{
		____storcairsouSape_7 = value;
	}

	inline static int32_t get_offset_of__muhechear_8() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____muhechear_8)); }
	inline String_t* get__muhechear_8() const { return ____muhechear_8; }
	inline String_t** get_address_of__muhechear_8() { return &____muhechear_8; }
	inline void set__muhechear_8(String_t* value)
	{
		____muhechear_8 = value;
		Il2CppCodeGenWriteBarrier(&____muhechear_8, value);
	}

	inline static int32_t get_offset_of__rempepeGearjunar_9() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____rempepeGearjunar_9)); }
	inline int32_t get__rempepeGearjunar_9() const { return ____rempepeGearjunar_9; }
	inline int32_t* get_address_of__rempepeGearjunar_9() { return &____rempepeGearjunar_9; }
	inline void set__rempepeGearjunar_9(int32_t value)
	{
		____rempepeGearjunar_9 = value;
	}

	inline static int32_t get_offset_of__baswhairCakorjow_10() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____baswhairCakorjow_10)); }
	inline float get__baswhairCakorjow_10() const { return ____baswhairCakorjow_10; }
	inline float* get_address_of__baswhairCakorjow_10() { return &____baswhairCakorjow_10; }
	inline void set__baswhairCakorjow_10(float value)
	{
		____baswhairCakorjow_10 = value;
	}

	inline static int32_t get_offset_of__jabeteeQehaichu_11() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____jabeteeQehaichu_11)); }
	inline int32_t get__jabeteeQehaichu_11() const { return ____jabeteeQehaichu_11; }
	inline int32_t* get_address_of__jabeteeQehaichu_11() { return &____jabeteeQehaichu_11; }
	inline void set__jabeteeQehaichu_11(int32_t value)
	{
		____jabeteeQehaichu_11 = value;
	}

	inline static int32_t get_offset_of__deevuKarsirmar_12() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____deevuKarsirmar_12)); }
	inline int32_t get__deevuKarsirmar_12() const { return ____deevuKarsirmar_12; }
	inline int32_t* get_address_of__deevuKarsirmar_12() { return &____deevuKarsirmar_12; }
	inline void set__deevuKarsirmar_12(int32_t value)
	{
		____deevuKarsirmar_12 = value;
	}

	inline static int32_t get_offset_of__rirvoriMisaytre_13() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____rirvoriMisaytre_13)); }
	inline uint32_t get__rirvoriMisaytre_13() const { return ____rirvoriMisaytre_13; }
	inline uint32_t* get_address_of__rirvoriMisaytre_13() { return &____rirvoriMisaytre_13; }
	inline void set__rirvoriMisaytre_13(uint32_t value)
	{
		____rirvoriMisaytre_13 = value;
	}

	inline static int32_t get_offset_of__sairdallSeebeesur_14() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____sairdallSeebeesur_14)); }
	inline float get__sairdallSeebeesur_14() const { return ____sairdallSeebeesur_14; }
	inline float* get_address_of__sairdallSeebeesur_14() { return &____sairdallSeebeesur_14; }
	inline void set__sairdallSeebeesur_14(float value)
	{
		____sairdallSeebeesur_14 = value;
	}

	inline static int32_t get_offset_of__sejeReldalwur_15() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____sejeReldalwur_15)); }
	inline int32_t get__sejeReldalwur_15() const { return ____sejeReldalwur_15; }
	inline int32_t* get_address_of__sejeReldalwur_15() { return &____sejeReldalwur_15; }
	inline void set__sejeReldalwur_15(int32_t value)
	{
		____sejeReldalwur_15 = value;
	}

	inline static int32_t get_offset_of__tacearqirTeaba_16() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____tacearqirTeaba_16)); }
	inline int32_t get__tacearqirTeaba_16() const { return ____tacearqirTeaba_16; }
	inline int32_t* get_address_of__tacearqirTeaba_16() { return &____tacearqirTeaba_16; }
	inline void set__tacearqirTeaba_16(int32_t value)
	{
		____tacearqirTeaba_16 = value;
	}

	inline static int32_t get_offset_of__velmonur_17() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____velmonur_17)); }
	inline float get__velmonur_17() const { return ____velmonur_17; }
	inline float* get_address_of__velmonur_17() { return &____velmonur_17; }
	inline void set__velmonur_17(float value)
	{
		____velmonur_17 = value;
	}

	inline static int32_t get_offset_of__jaskifair_18() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____jaskifair_18)); }
	inline float get__jaskifair_18() const { return ____jaskifair_18; }
	inline float* get_address_of__jaskifair_18() { return &____jaskifair_18; }
	inline void set__jaskifair_18(float value)
	{
		____jaskifair_18 = value;
	}

	inline static int32_t get_offset_of__sabea_19() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____sabea_19)); }
	inline float get__sabea_19() const { return ____sabea_19; }
	inline float* get_address_of__sabea_19() { return &____sabea_19; }
	inline void set__sabea_19(float value)
	{
		____sabea_19 = value;
	}

	inline static int32_t get_offset_of__chemmawjay_20() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____chemmawjay_20)); }
	inline float get__chemmawjay_20() const { return ____chemmawjay_20; }
	inline float* get_address_of__chemmawjay_20() { return &____chemmawjay_20; }
	inline void set__chemmawjay_20(float value)
	{
		____chemmawjay_20 = value;
	}

	inline static int32_t get_offset_of__staqalHaiselou_21() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____staqalHaiselou_21)); }
	inline String_t* get__staqalHaiselou_21() const { return ____staqalHaiselou_21; }
	inline String_t** get_address_of__staqalHaiselou_21() { return &____staqalHaiselou_21; }
	inline void set__staqalHaiselou_21(String_t* value)
	{
		____staqalHaiselou_21 = value;
		Il2CppCodeGenWriteBarrier(&____staqalHaiselou_21, value);
	}

	inline static int32_t get_offset_of__rermo_22() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____rermo_22)); }
	inline uint32_t get__rermo_22() const { return ____rermo_22; }
	inline uint32_t* get_address_of__rermo_22() { return &____rermo_22; }
	inline void set__rermo_22(uint32_t value)
	{
		____rermo_22 = value;
	}

	inline static int32_t get_offset_of__lubu_23() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____lubu_23)); }
	inline uint32_t get__lubu_23() const { return ____lubu_23; }
	inline uint32_t* get_address_of__lubu_23() { return &____lubu_23; }
	inline void set__lubu_23(uint32_t value)
	{
		____lubu_23 = value;
	}

	inline static int32_t get_offset_of__celqaiTairfereqis_24() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____celqaiTairfereqis_24)); }
	inline String_t* get__celqaiTairfereqis_24() const { return ____celqaiTairfereqis_24; }
	inline String_t** get_address_of__celqaiTairfereqis_24() { return &____celqaiTairfereqis_24; }
	inline void set__celqaiTairfereqis_24(String_t* value)
	{
		____celqaiTairfereqis_24 = value;
		Il2CppCodeGenWriteBarrier(&____celqaiTairfereqis_24, value);
	}

	inline static int32_t get_offset_of__cicisarLearvirtra_25() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____cicisarLearvirtra_25)); }
	inline uint32_t get__cicisarLearvirtra_25() const { return ____cicisarLearvirtra_25; }
	inline uint32_t* get_address_of__cicisarLearvirtra_25() { return &____cicisarLearvirtra_25; }
	inline void set__cicisarLearvirtra_25(uint32_t value)
	{
		____cicisarLearvirtra_25 = value;
	}

	inline static int32_t get_offset_of__yairtasDerdu_26() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____yairtasDerdu_26)); }
	inline String_t* get__yairtasDerdu_26() const { return ____yairtasDerdu_26; }
	inline String_t** get_address_of__yairtasDerdu_26() { return &____yairtasDerdu_26; }
	inline void set__yairtasDerdu_26(String_t* value)
	{
		____yairtasDerdu_26 = value;
		Il2CppCodeGenWriteBarrier(&____yairtasDerdu_26, value);
	}

	inline static int32_t get_offset_of__drekituZanorri_27() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____drekituZanorri_27)); }
	inline bool get__drekituZanorri_27() const { return ____drekituZanorri_27; }
	inline bool* get_address_of__drekituZanorri_27() { return &____drekituZanorri_27; }
	inline void set__drekituZanorri_27(bool value)
	{
		____drekituZanorri_27 = value;
	}

	inline static int32_t get_offset_of__beryiru_28() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____beryiru_28)); }
	inline uint32_t get__beryiru_28() const { return ____beryiru_28; }
	inline uint32_t* get_address_of__beryiru_28() { return &____beryiru_28; }
	inline void set__beryiru_28(uint32_t value)
	{
		____beryiru_28 = value;
	}

	inline static int32_t get_offset_of__piryay_29() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____piryay_29)); }
	inline String_t* get__piryay_29() const { return ____piryay_29; }
	inline String_t** get_address_of__piryay_29() { return &____piryay_29; }
	inline void set__piryay_29(String_t* value)
	{
		____piryay_29 = value;
		Il2CppCodeGenWriteBarrier(&____piryay_29, value);
	}

	inline static int32_t get_offset_of__delmooyee_30() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____delmooyee_30)); }
	inline bool get__delmooyee_30() const { return ____delmooyee_30; }
	inline bool* get_address_of__delmooyee_30() { return &____delmooyee_30; }
	inline void set__delmooyee_30(bool value)
	{
		____delmooyee_30 = value;
	}

	inline static int32_t get_offset_of__rasaiStormu_31() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____rasaiStormu_31)); }
	inline String_t* get__rasaiStormu_31() const { return ____rasaiStormu_31; }
	inline String_t** get_address_of__rasaiStormu_31() { return &____rasaiStormu_31; }
	inline void set__rasaiStormu_31(String_t* value)
	{
		____rasaiStormu_31 = value;
		Il2CppCodeGenWriteBarrier(&____rasaiStormu_31, value);
	}

	inline static int32_t get_offset_of__memel_32() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____memel_32)); }
	inline String_t* get__memel_32() const { return ____memel_32; }
	inline String_t** get_address_of__memel_32() { return &____memel_32; }
	inline void set__memel_32(String_t* value)
	{
		____memel_32 = value;
		Il2CppCodeGenWriteBarrier(&____memel_32, value);
	}

	inline static int32_t get_offset_of__rucenoKerfawsow_33() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____rucenoKerfawsow_33)); }
	inline float get__rucenoKerfawsow_33() const { return ____rucenoKerfawsow_33; }
	inline float* get_address_of__rucenoKerfawsow_33() { return &____rucenoKerfawsow_33; }
	inline void set__rucenoKerfawsow_33(float value)
	{
		____rucenoKerfawsow_33 = value;
	}

	inline static int32_t get_offset_of__whawsa_34() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____whawsa_34)); }
	inline float get__whawsa_34() const { return ____whawsa_34; }
	inline float* get_address_of__whawsa_34() { return &____whawsa_34; }
	inline void set__whawsa_34(float value)
	{
		____whawsa_34 = value;
	}

	inline static int32_t get_offset_of__pallstouLoxou_35() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____pallstouLoxou_35)); }
	inline float get__pallstouLoxou_35() const { return ____pallstouLoxou_35; }
	inline float* get_address_of__pallstouLoxou_35() { return &____pallstouLoxou_35; }
	inline void set__pallstouLoxou_35(float value)
	{
		____pallstouLoxou_35 = value;
	}

	inline static int32_t get_offset_of__tirjay_36() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____tirjay_36)); }
	inline float get__tirjay_36() const { return ____tirjay_36; }
	inline float* get_address_of__tirjay_36() { return &____tirjay_36; }
	inline void set__tirjay_36(float value)
	{
		____tirjay_36 = value;
	}

	inline static int32_t get_offset_of__trelcem_37() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____trelcem_37)); }
	inline String_t* get__trelcem_37() const { return ____trelcem_37; }
	inline String_t** get_address_of__trelcem_37() { return &____trelcem_37; }
	inline void set__trelcem_37(String_t* value)
	{
		____trelcem_37 = value;
		Il2CppCodeGenWriteBarrier(&____trelcem_37, value);
	}

	inline static int32_t get_offset_of__wasperJerehel_38() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____wasperJerehel_38)); }
	inline float get__wasperJerehel_38() const { return ____wasperJerehel_38; }
	inline float* get_address_of__wasperJerehel_38() { return &____wasperJerehel_38; }
	inline void set__wasperJerehel_38(float value)
	{
		____wasperJerehel_38 = value;
	}

	inline static int32_t get_offset_of__virxairkear_39() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____virxairkear_39)); }
	inline uint32_t get__virxairkear_39() const { return ____virxairkear_39; }
	inline uint32_t* get_address_of__virxairkear_39() { return &____virxairkear_39; }
	inline void set__virxairkear_39(uint32_t value)
	{
		____virxairkear_39 = value;
	}

	inline static int32_t get_offset_of__neamu_40() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____neamu_40)); }
	inline bool get__neamu_40() const { return ____neamu_40; }
	inline bool* get_address_of__neamu_40() { return &____neamu_40; }
	inline void set__neamu_40(bool value)
	{
		____neamu_40 = value;
	}

	inline static int32_t get_offset_of__dawlowgowJeebormea_41() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____dawlowgowJeebormea_41)); }
	inline int32_t get__dawlowgowJeebormea_41() const { return ____dawlowgowJeebormea_41; }
	inline int32_t* get_address_of__dawlowgowJeebormea_41() { return &____dawlowgowJeebormea_41; }
	inline void set__dawlowgowJeebormea_41(int32_t value)
	{
		____dawlowgowJeebormea_41 = value;
	}

	inline static int32_t get_offset_of__treciCutor_42() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____treciCutor_42)); }
	inline String_t* get__treciCutor_42() const { return ____treciCutor_42; }
	inline String_t** get_address_of__treciCutor_42() { return &____treciCutor_42; }
	inline void set__treciCutor_42(String_t* value)
	{
		____treciCutor_42 = value;
		Il2CppCodeGenWriteBarrier(&____treciCutor_42, value);
	}

	inline static int32_t get_offset_of__veatrurbirGawsow_43() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____veatrurbirGawsow_43)); }
	inline bool get__veatrurbirGawsow_43() const { return ____veatrurbirGawsow_43; }
	inline bool* get_address_of__veatrurbirGawsow_43() { return &____veatrurbirGawsow_43; }
	inline void set__veatrurbirGawsow_43(bool value)
	{
		____veatrurbirGawsow_43 = value;
	}

	inline static int32_t get_offset_of__zurhaTartreeroo_44() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____zurhaTartreeroo_44)); }
	inline float get__zurhaTartreeroo_44() const { return ____zurhaTartreeroo_44; }
	inline float* get_address_of__zurhaTartreeroo_44() { return &____zurhaTartreeroo_44; }
	inline void set__zurhaTartreeroo_44(float value)
	{
		____zurhaTartreeroo_44 = value;
	}

	inline static int32_t get_offset_of__nearka_45() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____nearka_45)); }
	inline float get__nearka_45() const { return ____nearka_45; }
	inline float* get_address_of__nearka_45() { return &____nearka_45; }
	inline void set__nearka_45(float value)
	{
		____nearka_45 = value;
	}

	inline static int32_t get_offset_of__sawjoSairose_46() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____sawjoSairose_46)); }
	inline uint32_t get__sawjoSairose_46() const { return ____sawjoSairose_46; }
	inline uint32_t* get_address_of__sawjoSairose_46() { return &____sawjoSairose_46; }
	inline void set__sawjoSairose_46(uint32_t value)
	{
		____sawjoSairose_46 = value;
	}

	inline static int32_t get_offset_of__wallhorcow_47() { return static_cast<int32_t>(offsetof(M_role175_t3885916121, ____wallhorcow_47)); }
	inline String_t* get__wallhorcow_47() const { return ____wallhorcow_47; }
	inline String_t** get_address_of__wallhorcow_47() { return &____wallhorcow_47; }
	inline void set__wallhorcow_47(String_t* value)
	{
		____wallhorcow_47 = value;
		Il2CppCodeGenWriteBarrier(&____wallhorcow_47, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
