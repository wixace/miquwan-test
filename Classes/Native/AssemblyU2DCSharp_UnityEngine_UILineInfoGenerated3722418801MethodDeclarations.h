﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_UILineInfoGenerated
struct UnityEngine_UILineInfoGenerated_t3722418801;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_UILineInfoGenerated::.ctor()
extern "C"  void UnityEngine_UILineInfoGenerated__ctor_m1163234330 (UnityEngine_UILineInfoGenerated_t3722418801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UILineInfoGenerated::.cctor()
extern "C"  void UnityEngine_UILineInfoGenerated__cctor_m1218429651 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_UILineInfoGenerated::UILineInfo_UILineInfo1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_UILineInfoGenerated_UILineInfo_UILineInfo1_m1605447576 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UILineInfoGenerated::UILineInfo_startCharIdx(JSVCall)
extern "C"  void UnityEngine_UILineInfoGenerated_UILineInfo_startCharIdx_m3139047393 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UILineInfoGenerated::UILineInfo_height(JSVCall)
extern "C"  void UnityEngine_UILineInfoGenerated_UILineInfo_height_m1875816895 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UILineInfoGenerated::UILineInfo_topY(JSVCall)
extern "C"  void UnityEngine_UILineInfoGenerated_UILineInfo_topY_m1751152098 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UILineInfoGenerated::__Register()
extern "C"  void UnityEngine_UILineInfoGenerated___Register_m1078030573 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UILineInfoGenerated::ilo_changeJSObj1(System.Int32,System.Object)
extern "C"  void UnityEngine_UILineInfoGenerated_ilo_changeJSObj1_m2078343567 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UILineInfoGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void UnityEngine_UILineInfoGenerated_ilo_setSingle2_m499231387 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
