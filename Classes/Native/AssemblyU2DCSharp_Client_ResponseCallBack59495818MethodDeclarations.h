﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Client/ResponseCallBack
struct ResponseCallBack_t59495818;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void Client/ResponseCallBack::.ctor(System.Object,System.IntPtr)
extern "C"  void ResponseCallBack__ctor_m1244318305 (ResponseCallBack_t59495818 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client/ResponseCallBack::Invoke(System.UInt32,System.UInt32,System.String)
extern "C"  void ResponseCallBack_Invoke_m3211744719 (ResponseCallBack_t59495818 * __this, uint32_t ___id0, uint32_t ___code1, String_t* ___msgString2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Client/ResponseCallBack::BeginInvoke(System.UInt32,System.UInt32,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ResponseCallBack_BeginInvoke_m1348135308 (ResponseCallBack_t59495818 * __this, uint32_t ___id0, uint32_t ___code1, String_t* ___msgString2, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Client/ResponseCallBack::EndInvoke(System.IAsyncResult)
extern "C"  void ResponseCallBack_EndInvoke_m58496241 (ResponseCallBack_t59495818 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
