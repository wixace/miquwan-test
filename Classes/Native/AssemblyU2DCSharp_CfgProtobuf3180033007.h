﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pomelo.Protobuf.MsgDecoder
struct MsgDecoder_t242483159;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CfgProtobuf
struct  CfgProtobuf_t3180033007  : public Il2CppObject
{
public:

public:
};

struct CfgProtobuf_t3180033007_StaticFields
{
public:
	// Pomelo.Protobuf.MsgDecoder CfgProtobuf::decoder
	MsgDecoder_t242483159 * ___decoder_0;

public:
	inline static int32_t get_offset_of_decoder_0() { return static_cast<int32_t>(offsetof(CfgProtobuf_t3180033007_StaticFields, ___decoder_0)); }
	inline MsgDecoder_t242483159 * get_decoder_0() const { return ___decoder_0; }
	inline MsgDecoder_t242483159 ** get_address_of_decoder_0() { return &___decoder_0; }
	inline void set_decoder_0(MsgDecoder_t242483159 * value)
	{
		___decoder_0 = value;
		Il2CppCodeGenWriteBarrier(&___decoder_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
