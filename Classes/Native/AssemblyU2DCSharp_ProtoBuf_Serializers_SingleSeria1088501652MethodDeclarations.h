﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.SingleSerializer
struct SingleSerializer_t1088501652;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"

// System.Void ProtoBuf.Serializers.SingleSerializer::.ctor(ProtoBuf.Meta.TypeModel)
extern "C"  void SingleSerializer__ctor_m2377348744 (SingleSerializer_t1088501652 * __this, TypeModel_t2730011105 * ___model0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.SingleSerializer::.cctor()
extern "C"  void SingleSerializer__cctor_m2566536204 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.SingleSerializer::ProtoBuf.Serializers.IProtoSerializer.get_RequiresOldValue()
extern "C"  bool SingleSerializer_ProtoBuf_Serializers_IProtoSerializer_get_RequiresOldValue_m1104347453 (SingleSerializer_t1088501652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.SingleSerializer::ProtoBuf.Serializers.IProtoSerializer.get_ReturnsValue()
extern "C"  bool SingleSerializer_ProtoBuf_Serializers_IProtoSerializer_get_ReturnsValue_m3239254611 (SingleSerializer_t1088501652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.SingleSerializer::get_ExpectedType()
extern "C"  Type_t * SingleSerializer_get_ExpectedType_m2502259236 (SingleSerializer_t1088501652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.SingleSerializer::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * SingleSerializer_Read_m2401890062 (SingleSerializer_t1088501652 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.SingleSerializer::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void SingleSerializer_Write_m649339128 (SingleSerializer_t1088501652 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
