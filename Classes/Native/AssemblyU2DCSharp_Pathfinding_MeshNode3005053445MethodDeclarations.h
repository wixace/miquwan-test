﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.MeshNode
struct MeshNode_t3005053445;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.GraphNodeDelegate
struct GraphNodeDelegate_t1466738551;
// System.Collections.Generic.Stack`1<Pathfinding.GraphNode>
struct Stack_1_t3122173294;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.PathNode
struct PathNode_t417131581;
// Pathfinding.PathHandler
struct PathHandler_t918952263;
// Pathfinding.Serialization.GraphSerializationContext
struct GraphSerializationContext_t3256954663;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNodeDelegate1466738551.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"
#include "AssemblyU2DCSharp_Pathfinding_PathHandler918952263.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_Pathfinding_Serialization_GraphS3256954663.h"
#include "AssemblyU2DCSharp_Pathfinding_MeshNode3005053445.h"

// System.Void Pathfinding.MeshNode::.ctor(AstarPath)
extern "C"  void MeshNode__ctor_m3885612184 (MeshNode_t3005053445 * __this, AstarPath_t4090270936 * ___astar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MeshNode::ClearConnections(System.Boolean)
extern "C"  void MeshNode_ClearConnections_m2412561473 (MeshNode_t3005053445 * __this, bool ___alsoReverse0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MeshNode::GetConnections(Pathfinding.GraphNodeDelegate)
extern "C"  void MeshNode_GetConnections_m2367081574 (MeshNode_t3005053445 * __this, GraphNodeDelegate_t1466738551 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MeshNode::FloodFill(System.Collections.Generic.Stack`1<Pathfinding.GraphNode>,System.UInt32)
extern "C"  void MeshNode_FloodFill_m531226491 (MeshNode_t3005053445 * __this, Stack_1_t3122173294 * ___stack0, uint32_t ___region1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.MeshNode::ContainsConnection(Pathfinding.GraphNode)
extern "C"  bool MeshNode_ContainsConnection_m364398265 (MeshNode_t3005053445 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MeshNode::UpdateRecursiveG(Pathfinding.Path,Pathfinding.PathNode,Pathfinding.PathHandler)
extern "C"  void MeshNode_UpdateRecursiveG_m3543223509 (MeshNode_t3005053445 * __this, Path_t1974241691 * ___path0, PathNode_t417131581 * ___pathNode1, PathHandler_t918952263 * ___handler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MeshNode::AddConnection(Pathfinding.GraphNode,System.UInt32)
extern "C"  void MeshNode_AddConnection_m1586119937 (MeshNode_t3005053445 * __this, GraphNode_t23612370 * ___node0, uint32_t ___cost1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MeshNode::RemoveConnection(Pathfinding.GraphNode)
extern "C"  void MeshNode_RemoveConnection_m1045456360 (MeshNode_t3005053445 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.MeshNode::ContainsPoint(Pathfinding.Int3)
extern "C"  bool MeshNode_ContainsPoint_m2104859599 (MeshNode_t3005053445 * __this, Int3_t1974045594  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MeshNode::SerializeReferences(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void MeshNode_SerializeReferences_m1600990501 (MeshNode_t3005053445 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MeshNode::DeserializeReferences(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void MeshNode_DeserializeReferences_m3816043622 (MeshNode_t3005053445 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MeshNode::ilo_RemoveConnection1(Pathfinding.GraphNode,Pathfinding.GraphNode)
extern "C"  void MeshNode_ilo_RemoveConnection1_m767678550 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MeshNode::ilo_Invoke2(Pathfinding.GraphNodeDelegate,Pathfinding.GraphNode)
extern "C"  void MeshNode_ilo_Invoke2_m599181094 (Il2CppObject * __this /* static, unused */, GraphNodeDelegate_t1466738551 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.MeshNode::ilo_get_Area3(Pathfinding.GraphNode)
extern "C"  uint32_t MeshNode_ilo_get_Area3_m4228745629 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MeshNode::ilo_UpdateG4(Pathfinding.GraphNode,Pathfinding.Path,Pathfinding.PathNode)
extern "C"  void MeshNode_ilo_UpdateG4_m3814296639 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, Path_t1974241691 * ___path1, PathNode_t417131581 * ___pathNode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MeshNode::ilo_PushNode5(Pathfinding.PathHandler,Pathfinding.PathNode)
extern "C"  void MeshNode_ilo_PushNode5_m354478596 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, PathNode_t417131581 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathNode Pathfinding.MeshNode::ilo_GetPathNode6(Pathfinding.PathHandler,Pathfinding.GraphNode)
extern "C"  PathNode_t417131581 * MeshNode_ilo_GetPathNode6_m1753143235 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 Pathfinding.MeshNode::ilo_get_PathID7(Pathfinding.PathHandler)
extern "C"  uint16_t MeshNode_ilo_get_PathID7_m2346285041 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.MeshNode::ilo_UpdateRecursiveG8(Pathfinding.GraphNode,Pathfinding.Path,Pathfinding.PathNode,Pathfinding.PathHandler)
extern "C"  void MeshNode_ilo_UpdateRecursiveG8_m3009415444 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, Path_t1974241691 * ___path1, PathNode_t417131581 * ___pathNode2, PathHandler_t918952263 * ___handler3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.MeshNode::ilo_GetVertex9(Pathfinding.MeshNode,System.Int32)
extern "C"  Int3_t1974045594  MeshNode_ilo_GetVertex9_m4074357037 (Il2CppObject * __this /* static, unused */, MeshNode_t3005053445 * ____this0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.MeshNode::ilo_GetNodeIdentifier10(Pathfinding.Serialization.GraphSerializationContext,Pathfinding.GraphNode)
extern "C"  int32_t MeshNode_ilo_GetNodeIdentifier10_m3366590324 (Il2CppObject * __this /* static, unused */, GraphSerializationContext_t3256954663 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
