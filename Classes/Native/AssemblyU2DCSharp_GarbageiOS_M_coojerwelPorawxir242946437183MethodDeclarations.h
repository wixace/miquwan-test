﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_coojerwelPorawxir243
struct M_coojerwelPorawxir243_t2946437183;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_coojerwelPorawxir242946437183.h"

// System.Void GarbageiOS.M_coojerwelPorawxir243::.ctor()
extern "C"  void M_coojerwelPorawxir243__ctor_m1626050772 (M_coojerwelPorawxir243_t2946437183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_coojerwelPorawxir243::M_gurstepayStemnem0(System.String[],System.Int32)
extern "C"  void M_coojerwelPorawxir243_M_gurstepayStemnem0_m672247138 (M_coojerwelPorawxir243_t2946437183 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_coojerwelPorawxir243::M_bearje1(System.String[],System.Int32)
extern "C"  void M_coojerwelPorawxir243_M_bearje1_m1344004601 (M_coojerwelPorawxir243_t2946437183 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_coojerwelPorawxir243::M_suzearmall2(System.String[],System.Int32)
extern "C"  void M_coojerwelPorawxir243_M_suzearmall2_m3583541559 (M_coojerwelPorawxir243_t2946437183 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_coojerwelPorawxir243::M_gallsarrel3(System.String[],System.Int32)
extern "C"  void M_coojerwelPorawxir243_M_gallsarrel3_m2045311067 (M_coojerwelPorawxir243_t2946437183 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_coojerwelPorawxir243::M_cemnearWhousooga4(System.String[],System.Int32)
extern "C"  void M_coojerwelPorawxir243_M_cemnearWhousooga4_m3213850504 (M_coojerwelPorawxir243_t2946437183 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_coojerwelPorawxir243::ilo_M_suzearmall21(GarbageiOS.M_coojerwelPorawxir243,System.String[],System.Int32)
extern "C"  void M_coojerwelPorawxir243_ilo_M_suzearmall21_m2282117896 (Il2CppObject * __this /* static, unused */, M_coojerwelPorawxir243_t2946437183 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_coojerwelPorawxir243::ilo_M_gallsarrel32(GarbageiOS.M_coojerwelPorawxir243,System.String[],System.Int32)
extern "C"  void M_coojerwelPorawxir243_ilo_M_gallsarrel32_m193849701 (Il2CppObject * __this /* static, unused */, M_coojerwelPorawxir243_t2946437183 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
