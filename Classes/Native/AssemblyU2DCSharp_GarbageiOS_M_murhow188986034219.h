﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_murhow188
struct  M_murhow188_t986034219  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_murhow188::_merbee
	bool ____merbee_0;
	// System.Single GarbageiOS.M_murhow188::_koutiMocaw
	float ____koutiMocaw_1;
	// System.Single GarbageiOS.M_murhow188::_cherekeaWhosi
	float ____cherekeaWhosi_2;
	// System.String GarbageiOS.M_murhow188::_pirnaTrertoho
	String_t* ____pirnaTrertoho_3;
	// System.String GarbageiOS.M_murhow188::_nailairte
	String_t* ____nailairte_4;
	// System.Int32 GarbageiOS.M_murhow188::_qemererar
	int32_t ____qemererar_5;
	// System.Boolean GarbageiOS.M_murhow188::_neejerdu
	bool ____neejerdu_6;
	// System.Single GarbageiOS.M_murhow188::_jemall
	float ____jemall_7;
	// System.UInt32 GarbageiOS.M_murhow188::_napem
	uint32_t ____napem_8;
	// System.Int32 GarbageiOS.M_murhow188::_kifartere
	int32_t ____kifartere_9;
	// System.String GarbageiOS.M_murhow188::_sevallpaDerhar
	String_t* ____sevallpaDerhar_10;
	// System.String GarbageiOS.M_murhow188::_dearseachaw
	String_t* ____dearseachaw_11;
	// System.Single GarbageiOS.M_murhow188::_reerasCeelaygir
	float ____reerasCeelaygir_12;
	// System.String GarbageiOS.M_murhow188::_ceayaysel
	String_t* ____ceayaysel_13;
	// System.UInt32 GarbageiOS.M_murhow188::_dakeero
	uint32_t ____dakeero_14;

public:
	inline static int32_t get_offset_of__merbee_0() { return static_cast<int32_t>(offsetof(M_murhow188_t986034219, ____merbee_0)); }
	inline bool get__merbee_0() const { return ____merbee_0; }
	inline bool* get_address_of__merbee_0() { return &____merbee_0; }
	inline void set__merbee_0(bool value)
	{
		____merbee_0 = value;
	}

	inline static int32_t get_offset_of__koutiMocaw_1() { return static_cast<int32_t>(offsetof(M_murhow188_t986034219, ____koutiMocaw_1)); }
	inline float get__koutiMocaw_1() const { return ____koutiMocaw_1; }
	inline float* get_address_of__koutiMocaw_1() { return &____koutiMocaw_1; }
	inline void set__koutiMocaw_1(float value)
	{
		____koutiMocaw_1 = value;
	}

	inline static int32_t get_offset_of__cherekeaWhosi_2() { return static_cast<int32_t>(offsetof(M_murhow188_t986034219, ____cherekeaWhosi_2)); }
	inline float get__cherekeaWhosi_2() const { return ____cherekeaWhosi_2; }
	inline float* get_address_of__cherekeaWhosi_2() { return &____cherekeaWhosi_2; }
	inline void set__cherekeaWhosi_2(float value)
	{
		____cherekeaWhosi_2 = value;
	}

	inline static int32_t get_offset_of__pirnaTrertoho_3() { return static_cast<int32_t>(offsetof(M_murhow188_t986034219, ____pirnaTrertoho_3)); }
	inline String_t* get__pirnaTrertoho_3() const { return ____pirnaTrertoho_3; }
	inline String_t** get_address_of__pirnaTrertoho_3() { return &____pirnaTrertoho_3; }
	inline void set__pirnaTrertoho_3(String_t* value)
	{
		____pirnaTrertoho_3 = value;
		Il2CppCodeGenWriteBarrier(&____pirnaTrertoho_3, value);
	}

	inline static int32_t get_offset_of__nailairte_4() { return static_cast<int32_t>(offsetof(M_murhow188_t986034219, ____nailairte_4)); }
	inline String_t* get__nailairte_4() const { return ____nailairte_4; }
	inline String_t** get_address_of__nailairte_4() { return &____nailairte_4; }
	inline void set__nailairte_4(String_t* value)
	{
		____nailairte_4 = value;
		Il2CppCodeGenWriteBarrier(&____nailairte_4, value);
	}

	inline static int32_t get_offset_of__qemererar_5() { return static_cast<int32_t>(offsetof(M_murhow188_t986034219, ____qemererar_5)); }
	inline int32_t get__qemererar_5() const { return ____qemererar_5; }
	inline int32_t* get_address_of__qemererar_5() { return &____qemererar_5; }
	inline void set__qemererar_5(int32_t value)
	{
		____qemererar_5 = value;
	}

	inline static int32_t get_offset_of__neejerdu_6() { return static_cast<int32_t>(offsetof(M_murhow188_t986034219, ____neejerdu_6)); }
	inline bool get__neejerdu_6() const { return ____neejerdu_6; }
	inline bool* get_address_of__neejerdu_6() { return &____neejerdu_6; }
	inline void set__neejerdu_6(bool value)
	{
		____neejerdu_6 = value;
	}

	inline static int32_t get_offset_of__jemall_7() { return static_cast<int32_t>(offsetof(M_murhow188_t986034219, ____jemall_7)); }
	inline float get__jemall_7() const { return ____jemall_7; }
	inline float* get_address_of__jemall_7() { return &____jemall_7; }
	inline void set__jemall_7(float value)
	{
		____jemall_7 = value;
	}

	inline static int32_t get_offset_of__napem_8() { return static_cast<int32_t>(offsetof(M_murhow188_t986034219, ____napem_8)); }
	inline uint32_t get__napem_8() const { return ____napem_8; }
	inline uint32_t* get_address_of__napem_8() { return &____napem_8; }
	inline void set__napem_8(uint32_t value)
	{
		____napem_8 = value;
	}

	inline static int32_t get_offset_of__kifartere_9() { return static_cast<int32_t>(offsetof(M_murhow188_t986034219, ____kifartere_9)); }
	inline int32_t get__kifartere_9() const { return ____kifartere_9; }
	inline int32_t* get_address_of__kifartere_9() { return &____kifartere_9; }
	inline void set__kifartere_9(int32_t value)
	{
		____kifartere_9 = value;
	}

	inline static int32_t get_offset_of__sevallpaDerhar_10() { return static_cast<int32_t>(offsetof(M_murhow188_t986034219, ____sevallpaDerhar_10)); }
	inline String_t* get__sevallpaDerhar_10() const { return ____sevallpaDerhar_10; }
	inline String_t** get_address_of__sevallpaDerhar_10() { return &____sevallpaDerhar_10; }
	inline void set__sevallpaDerhar_10(String_t* value)
	{
		____sevallpaDerhar_10 = value;
		Il2CppCodeGenWriteBarrier(&____sevallpaDerhar_10, value);
	}

	inline static int32_t get_offset_of__dearseachaw_11() { return static_cast<int32_t>(offsetof(M_murhow188_t986034219, ____dearseachaw_11)); }
	inline String_t* get__dearseachaw_11() const { return ____dearseachaw_11; }
	inline String_t** get_address_of__dearseachaw_11() { return &____dearseachaw_11; }
	inline void set__dearseachaw_11(String_t* value)
	{
		____dearseachaw_11 = value;
		Il2CppCodeGenWriteBarrier(&____dearseachaw_11, value);
	}

	inline static int32_t get_offset_of__reerasCeelaygir_12() { return static_cast<int32_t>(offsetof(M_murhow188_t986034219, ____reerasCeelaygir_12)); }
	inline float get__reerasCeelaygir_12() const { return ____reerasCeelaygir_12; }
	inline float* get_address_of__reerasCeelaygir_12() { return &____reerasCeelaygir_12; }
	inline void set__reerasCeelaygir_12(float value)
	{
		____reerasCeelaygir_12 = value;
	}

	inline static int32_t get_offset_of__ceayaysel_13() { return static_cast<int32_t>(offsetof(M_murhow188_t986034219, ____ceayaysel_13)); }
	inline String_t* get__ceayaysel_13() const { return ____ceayaysel_13; }
	inline String_t** get_address_of__ceayaysel_13() { return &____ceayaysel_13; }
	inline void set__ceayaysel_13(String_t* value)
	{
		____ceayaysel_13 = value;
		Il2CppCodeGenWriteBarrier(&____ceayaysel_13, value);
	}

	inline static int32_t get_offset_of__dakeero_14() { return static_cast<int32_t>(offsetof(M_murhow188_t986034219, ____dakeero_14)); }
	inline uint32_t get__dakeero_14() const { return ____dakeero_14; }
	inline uint32_t* get_address_of__dakeero_14() { return &____dakeero_14; }
	inline void set__dakeero_14(uint32_t value)
	{
		____dakeero_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
