﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2379543692.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_VoxelContour3597201016.h"

// System.Void System.Array/InternalEnumerator`1<Pathfinding.Voxels.VoxelContour>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m834952336_gshared (InternalEnumerator_1_t2379543692 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m834952336(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2379543692 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m834952336_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4028428048_gshared (InternalEnumerator_1_t2379543692 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4028428048(__this, method) ((  void (*) (InternalEnumerator_1_t2379543692 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4028428048_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3808684934_gshared (InternalEnumerator_1_t2379543692 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3808684934(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2379543692 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3808684934_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.Voxels.VoxelContour>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4051786343_gshared (InternalEnumerator_1_t2379543692 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m4051786343(__this, method) ((  void (*) (InternalEnumerator_1_t2379543692 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4051786343_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.Voxels.VoxelContour>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m217640896_gshared (InternalEnumerator_1_t2379543692 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m217640896(__this, method) ((  bool (*) (InternalEnumerator_1_t2379543692 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m217640896_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Pathfinding.Voxels.VoxelContour>::get_Current()
extern "C"  VoxelContour_t3597201016  InternalEnumerator_1_get_Current_m51939641_gshared (InternalEnumerator_1_t2379543692 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m51939641(__this, method) ((  VoxelContour_t3597201016  (*) (InternalEnumerator_1_t2379543692 *, const MethodInfo*))InternalEnumerator_1_get_Current_m51939641_gshared)(__this, method)
