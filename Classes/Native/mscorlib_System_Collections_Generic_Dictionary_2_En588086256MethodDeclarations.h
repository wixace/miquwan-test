﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m239876006(__this, ___dictionary0, method) ((  void (*) (Enumerator_t588086256 *, Dictionary_2_t3565730160 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2847769019(__this, method) ((  Il2CppObject * (*) (Enumerator_t588086256 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1084693839(__this, method) ((  void (*) (Enumerator_t588086256 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1406511000(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t588086256 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m496322135(__this, method) ((  Il2CppObject * (*) (Enumerator_t588086256 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1272638249(__this, method) ((  Il2CppObject * (*) (Enumerator_t588086256 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>::MoveNext()
#define Enumerator_MoveNext_m2235460283(__this, method) ((  bool (*) (Enumerator_t588086256 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>::get_Current()
#define Enumerator_get_Current_m938577301(__this, method) ((  KeyValuePair_2_t3464510866  (*) (Enumerator_t588086256 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2605670984(__this, method) ((  String_t* (*) (Enumerator_t588086256 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3613845740(__this, method) ((  Dictionary_2_t2745311790 * (*) (Enumerator_t588086256 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>::Reset()
#define Enumerator_Reset_m2802104568(__this, method) ((  void (*) (Enumerator_t588086256 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>::VerifyState()
#define Enumerator_VerifyState_m1068986113(__this, method) ((  void (*) (Enumerator_t588086256 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3543342633(__this, method) ((  void (*) (Enumerator_t588086256 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>::Dispose()
#define Enumerator_Dispose_m2401424328(__this, method) ((  void (*) (Enumerator_t588086256 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
