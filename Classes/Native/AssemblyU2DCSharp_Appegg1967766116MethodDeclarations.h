﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Appegg
struct Appegg_t1967766116;
// System.String
struct String_t;
// Appegg/ServerConfig
struct ServerConfig_t2996416080;
// AppeggView
struct AppeggView_t3951466281;
// UnityEngine.Events.UnityAction
struct UnityAction_t594794173;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_AppeggViewType181198339.h"
#include "AssemblyU2DCSharp_Appegg_ServerConfig2996416080.h"
#include "AssemblyU2DCSharp_Appegg1967766116.h"
#include "AssemblyU2DCSharp_AppeggView3951466281.h"
#include "UnityEngine_UnityEngine_Events_UnityAction594794173.h"

// System.Void Appegg::.ctor()
extern "C"  void Appegg__ctor_m2180979383 (Appegg_t1967766116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Appegg::get_SavePath()
extern "C"  String_t* Appegg_get_SavePath_m1190293723 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::set_SavePath(System.String)
extern "C"  void Appegg_set_SavePath_m4022908950 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::OnEnable()
extern "C"  void Appegg_OnEnable_m1363123567 (Appegg_t1967766116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::Start()
extern "C"  void Appegg_Start_m1128117175 (Appegg_t1967766116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::InitConfigs()
extern "C"  void Appegg_InitConfigs_m1650379478 (Appegg_t1967766116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::InitViews(AppeggViewType)
extern "C"  void Appegg_InitViews_m4120948336 (Appegg_t1967766116 * __this, int32_t ___viewType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::InitUserProperty()
extern "C"  void Appegg_InitUserProperty_m192599453 (Appegg_t1967766116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::InitProfile()
extern "C"  void Appegg_InitProfile_m1449410830 (Appegg_t1967766116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::LoadWebView()
extern "C"  void Appegg_LoadWebView_m978361448 (Appegg_t1967766116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::PlayOfflineGame()
extern "C"  void Appegg_PlayOfflineGame_m1608801078 (Appegg_t1967766116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::PlayBrowserGame()
extern "C"  void Appegg_PlayBrowserGame_m775270587 (Appegg_t1967766116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::PlayHotfixGame()
extern "C"  void Appegg_PlayHotfixGame_m2712045627 (Appegg_t1967766116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::PlayHybridGame()
extern "C"  void Appegg_PlayHybridGame_m379855503 (Appegg_t1967766116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::Connect()
extern "C"  void Appegg_Connect_m3340304703 (Appegg_t1967766116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::OnDataReceived(Appegg/ServerConfig)
extern "C"  void Appegg_OnDataReceived_m3128863723 (Appegg_t1967766116 * __this, ServerConfig_t2996416080 * ___serverConfig0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::<InitViews>m__325()
extern "C"  void Appegg_U3CInitViewsU3Em__325_m3045532506 (Appegg_t1967766116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::<InitViews>m__326()
extern "C"  void Appegg_U3CInitViewsU3Em__326_m3045533467 (Appegg_t1967766116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::<InitUserProperty>m__327(System.String,System.Boolean,System.String)
extern "C"  void Appegg_U3CInitUserPropertyU3Em__327_m57022593 (Appegg_t1967766116 * __this, String_t* ___advertisingId0, bool ___trackingEnabled1, String_t* ___error2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::<Connect>m__328()
extern "C"  void Appegg_U3CConnectU3Em__328_m2131729009 (Appegg_t1967766116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::ilo_Connect1(Appegg)
extern "C"  void Appegg_ilo_Connect1_m2858690691 (Il2CppObject * __this /* static, unused */, Appegg_t1967766116 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::ilo_Init2(AppeggView)
extern "C"  void Appegg_ilo_Init2_m3146716443 (Il2CppObject * __this /* static, unused */, AppeggView_t3951466281 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::ilo_SetPrimaryButtonListener3(AppeggView,UnityEngine.Events.UnityAction)
extern "C"  void Appegg_ilo_SetPrimaryButtonListener3_m2256580115 (Il2CppObject * __this /* static, unused */, AppeggView_t3951466281 * ____this0, UnityAction_t594794173 * ___act1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::ilo_SetSecondaryButtonListener4(AppeggView,UnityEngine.Events.UnityAction)
extern "C"  void Appegg_ilo_SetSecondaryButtonListener4_m42947876 (Il2CppObject * __this /* static, unused */, AppeggView_t3951466281 * ____this0, UnityAction_t594794173 * ___act1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::ilo_InitViews5(Appegg,AppeggViewType)
extern "C"  void Appegg_ilo_InitViews5_m1114261200 (Il2CppObject * __this /* static, unused */, Appegg_t1967766116 * ____this0, int32_t ___viewType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::ilo_set_Url6(System.String)
extern "C"  void Appegg_ilo_set_Url6_m2183306750 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::ilo_LoadWebView7(Appegg)
extern "C"  void Appegg_ilo_LoadWebView7_m313042368 (Il2CppObject * __this /* static, unused */, Appegg_t1967766116 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::ilo_OnDataReceived8(Appegg,Appegg/ServerConfig)
extern "C"  void Appegg_ilo_OnDataReceived8_m4640008 (Il2CppObject * __this /* static, unused */, Appegg_t1967766116 * ____this0, ServerConfig_t2996416080 * ___serverConfig1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::ilo_Show9(AppeggView)
extern "C"  void Appegg_ilo_Show9_m3742995925 (Il2CppObject * __this /* static, unused */, AppeggView_t3951466281 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::ilo_InitProfile10(Appegg)
extern "C"  void Appegg_ilo_InitProfile10_m2763940086 (Il2CppObject * __this /* static, unused */, Appegg_t1967766116 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::ilo_PlayOfflineGame11(Appegg)
extern "C"  void Appegg_ilo_PlayOfflineGame11_m2361238431 (Il2CppObject * __this /* static, unused */, Appegg_t1967766116 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Appegg::ilo_InitUserProperty12(Appegg)
extern "C"  void Appegg_ilo_InitUserProperty12_m3770838349 (Il2CppObject * __this /* static, unused */, Appegg_t1967766116 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
