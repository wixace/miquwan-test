﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginShoumeng_A1
struct PluginShoumeng_A1_t4066769168;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// VersionInfo
struct VersionInfo_t2356638086;
// System.Object
struct Il2CppObject;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_PluginShoumeng_A14066769168.h"

// System.Void PluginShoumeng_A1::.ctor()
extern "C"  void PluginShoumeng_A1__ctor_m984409691 (PluginShoumeng_A1_t4066769168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::Init()
extern "C"  void PluginShoumeng_A1_Init_m814575769 (PluginShoumeng_A1_t4066769168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginShoumeng_A1::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginShoumeng_A1_ReqSDKHttpLogin_m2682692492 (PluginShoumeng_A1_t4066769168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginShoumeng_A1::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginShoumeng_A1_IsLoginSuccess_m2434765232 (PluginShoumeng_A1_t4066769168 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::OpenUserLogin()
extern "C"  void PluginShoumeng_A1_OpenUserLogin_m947092909 (PluginShoumeng_A1_t4066769168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::RolelvUpinfo(CEvent.ZEvent)
extern "C"  void PluginShoumeng_A1_RolelvUpinfo_m1693717001 (PluginShoumeng_A1_t4066769168 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::UserPay(CEvent.ZEvent)
extern "C"  void PluginShoumeng_A1_UserPay_m3953445157 (PluginShoumeng_A1_t4066769168 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::OnInitSuccess(System.String)
extern "C"  void PluginShoumeng_A1_OnInitSuccess_m1414903861 (PluginShoumeng_A1_t4066769168 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::OnInitFail(System.String)
extern "C"  void PluginShoumeng_A1_OnInitFail_m3743722796 (PluginShoumeng_A1_t4066769168 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::OnLoginSuccess(System.String)
extern "C"  void PluginShoumeng_A1_OnLoginSuccess_m1302058784 (PluginShoumeng_A1_t4066769168 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::OnLoginFail(System.String)
extern "C"  void PluginShoumeng_A1_OnLoginFail_m3016093217 (PluginShoumeng_A1_t4066769168 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::OnPaySuccess(System.String)
extern "C"  void PluginShoumeng_A1_OnPaySuccess_m3519906143 (PluginShoumeng_A1_t4066769168 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::OnPayFail(System.String)
extern "C"  void PluginShoumeng_A1_OnPayFail_m2964554818 (PluginShoumeng_A1_t4066769168 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::OnExitGameSuccess(System.String)
extern "C"  void PluginShoumeng_A1_OnExitGameSuccess_m2753509845 (PluginShoumeng_A1_t4066769168 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::LogoutSuccess()
extern "C"  void PluginShoumeng_A1_LogoutSuccess_m898384978 (PluginShoumeng_A1_t4066769168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::OnLogoutSuccess(System.String)
extern "C"  void PluginShoumeng_A1_OnLogoutSuccess_m1712659311 (PluginShoumeng_A1_t4066769168 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::OnLogoutFail(System.String)
extern "C"  void PluginShoumeng_A1_OnLogoutFail_m3117458482 (PluginShoumeng_A1_t4066769168 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::SMSDKA1Init()
extern "C"  void PluginShoumeng_A1_SMSDKA1Init_m2133707353 (PluginShoumeng_A1_t4066769168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::SMSDKA1Login()
extern "C"  void PluginShoumeng_A1_SMSDKA1Login_m114483938 (PluginShoumeng_A1_t4066769168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::SMSDKA1Logout()
extern "C"  void PluginShoumeng_A1_SMSDKA1Logout_m3554824787 (PluginShoumeng_A1_t4066769168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::SMSDKA1Pay(System.String,System.String,System.String,System.String)
extern "C"  void PluginShoumeng_A1_SMSDKA1Pay_m662975157 (PluginShoumeng_A1_t4066769168 * __this, String_t* ___extendInfo0, String_t* ___serverId1, String_t* ___Price2, String_t* ___productId3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::<OnLogoutSuccess>m__455()
extern "C"  void PluginShoumeng_A1_U3COnLogoutSuccessU3Em__455_m1462764000 (PluginShoumeng_A1_t4066769168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginShoumeng_A1_ilo_AddEventListener1_m4045318969 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginShoumeng_A1::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginShoumeng_A1_ilo_get_Instance2_m4144136183 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginShoumeng_A1::ilo_get_currentVS3(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginShoumeng_A1_ilo_get_currentVS3_m3161945333 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::ilo_Log4(System.Object,System.Boolean)
extern "C"  void PluginShoumeng_A1_ilo_Log4_m2128465493 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::ilo_DispatchEvent5(CEvent.ZEvent)
extern "C"  void PluginShoumeng_A1_ilo_DispatchEvent5_m3848930730 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::ilo_Logout6(System.Action)
extern "C"  void PluginShoumeng_A1_ilo_Logout6_m3418596651 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginShoumeng_A1::ilo_OpenUserLogin7(PluginShoumeng_A1)
extern "C"  void PluginShoumeng_A1_ilo_OpenUserLogin7_m1968245635 (Il2CppObject * __this /* static, unused */, PluginShoumeng_A1_t4066769168 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
