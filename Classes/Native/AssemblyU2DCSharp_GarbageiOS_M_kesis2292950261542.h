﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_kesis229
struct  M_kesis229_t2950261542  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_kesis229::_raibem
	int32_t ____raibem_0;
	// System.String GarbageiOS.M_kesis229::_gorgaCawlo
	String_t* ____gorgaCawlo_1;
	// System.Boolean GarbageiOS.M_kesis229::_sofaisir
	bool ____sofaisir_2;
	// System.Int32 GarbageiOS.M_kesis229::_jalkairToyi
	int32_t ____jalkairToyi_3;
	// System.Int32 GarbageiOS.M_kesis229::_kairteWhisir
	int32_t ____kairteWhisir_4;
	// System.Single GarbageiOS.M_kesis229::_cawdihearSalkere
	float ____cawdihearSalkere_5;
	// System.String GarbageiOS.M_kesis229::_milelqerLalwemer
	String_t* ____milelqerLalwemer_6;
	// System.Boolean GarbageiOS.M_kesis229::_darqecalLairmowdre
	bool ____darqecalLairmowdre_7;
	// System.Single GarbageiOS.M_kesis229::_ceriTumo
	float ____ceriTumo_8;
	// System.Single GarbageiOS.M_kesis229::_roosereHunemay
	float ____roosereHunemay_9;
	// System.String GarbageiOS.M_kesis229::_stearjai
	String_t* ____stearjai_10;

public:
	inline static int32_t get_offset_of__raibem_0() { return static_cast<int32_t>(offsetof(M_kesis229_t2950261542, ____raibem_0)); }
	inline int32_t get__raibem_0() const { return ____raibem_0; }
	inline int32_t* get_address_of__raibem_0() { return &____raibem_0; }
	inline void set__raibem_0(int32_t value)
	{
		____raibem_0 = value;
	}

	inline static int32_t get_offset_of__gorgaCawlo_1() { return static_cast<int32_t>(offsetof(M_kesis229_t2950261542, ____gorgaCawlo_1)); }
	inline String_t* get__gorgaCawlo_1() const { return ____gorgaCawlo_1; }
	inline String_t** get_address_of__gorgaCawlo_1() { return &____gorgaCawlo_1; }
	inline void set__gorgaCawlo_1(String_t* value)
	{
		____gorgaCawlo_1 = value;
		Il2CppCodeGenWriteBarrier(&____gorgaCawlo_1, value);
	}

	inline static int32_t get_offset_of__sofaisir_2() { return static_cast<int32_t>(offsetof(M_kesis229_t2950261542, ____sofaisir_2)); }
	inline bool get__sofaisir_2() const { return ____sofaisir_2; }
	inline bool* get_address_of__sofaisir_2() { return &____sofaisir_2; }
	inline void set__sofaisir_2(bool value)
	{
		____sofaisir_2 = value;
	}

	inline static int32_t get_offset_of__jalkairToyi_3() { return static_cast<int32_t>(offsetof(M_kesis229_t2950261542, ____jalkairToyi_3)); }
	inline int32_t get__jalkairToyi_3() const { return ____jalkairToyi_3; }
	inline int32_t* get_address_of__jalkairToyi_3() { return &____jalkairToyi_3; }
	inline void set__jalkairToyi_3(int32_t value)
	{
		____jalkairToyi_3 = value;
	}

	inline static int32_t get_offset_of__kairteWhisir_4() { return static_cast<int32_t>(offsetof(M_kesis229_t2950261542, ____kairteWhisir_4)); }
	inline int32_t get__kairteWhisir_4() const { return ____kairteWhisir_4; }
	inline int32_t* get_address_of__kairteWhisir_4() { return &____kairteWhisir_4; }
	inline void set__kairteWhisir_4(int32_t value)
	{
		____kairteWhisir_4 = value;
	}

	inline static int32_t get_offset_of__cawdihearSalkere_5() { return static_cast<int32_t>(offsetof(M_kesis229_t2950261542, ____cawdihearSalkere_5)); }
	inline float get__cawdihearSalkere_5() const { return ____cawdihearSalkere_5; }
	inline float* get_address_of__cawdihearSalkere_5() { return &____cawdihearSalkere_5; }
	inline void set__cawdihearSalkere_5(float value)
	{
		____cawdihearSalkere_5 = value;
	}

	inline static int32_t get_offset_of__milelqerLalwemer_6() { return static_cast<int32_t>(offsetof(M_kesis229_t2950261542, ____milelqerLalwemer_6)); }
	inline String_t* get__milelqerLalwemer_6() const { return ____milelqerLalwemer_6; }
	inline String_t** get_address_of__milelqerLalwemer_6() { return &____milelqerLalwemer_6; }
	inline void set__milelqerLalwemer_6(String_t* value)
	{
		____milelqerLalwemer_6 = value;
		Il2CppCodeGenWriteBarrier(&____milelqerLalwemer_6, value);
	}

	inline static int32_t get_offset_of__darqecalLairmowdre_7() { return static_cast<int32_t>(offsetof(M_kesis229_t2950261542, ____darqecalLairmowdre_7)); }
	inline bool get__darqecalLairmowdre_7() const { return ____darqecalLairmowdre_7; }
	inline bool* get_address_of__darqecalLairmowdre_7() { return &____darqecalLairmowdre_7; }
	inline void set__darqecalLairmowdre_7(bool value)
	{
		____darqecalLairmowdre_7 = value;
	}

	inline static int32_t get_offset_of__ceriTumo_8() { return static_cast<int32_t>(offsetof(M_kesis229_t2950261542, ____ceriTumo_8)); }
	inline float get__ceriTumo_8() const { return ____ceriTumo_8; }
	inline float* get_address_of__ceriTumo_8() { return &____ceriTumo_8; }
	inline void set__ceriTumo_8(float value)
	{
		____ceriTumo_8 = value;
	}

	inline static int32_t get_offset_of__roosereHunemay_9() { return static_cast<int32_t>(offsetof(M_kesis229_t2950261542, ____roosereHunemay_9)); }
	inline float get__roosereHunemay_9() const { return ____roosereHunemay_9; }
	inline float* get_address_of__roosereHunemay_9() { return &____roosereHunemay_9; }
	inline void set__roosereHunemay_9(float value)
	{
		____roosereHunemay_9 = value;
	}

	inline static int32_t get_offset_of__stearjai_10() { return static_cast<int32_t>(offsetof(M_kesis229_t2950261542, ____stearjai_10)); }
	inline String_t* get__stearjai_10() const { return ____stearjai_10; }
	inline String_t** get_address_of__stearjai_10() { return &____stearjai_10; }
	inline void set__stearjai_10(String_t* value)
	{
		____stearjai_10 = value;
		Il2CppCodeGenWriteBarrier(&____stearjai_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
