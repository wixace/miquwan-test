﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonContainerAttribute
struct JsonContainerAttribute_t1917602971;
// System.Type
struct Type_t;
// Newtonsoft.Json.JObjectAttribute
struct JObjectAttribute_t3411920523;
// Newtonsoft.Json.JsonArrayAttribute
struct JsonArrayAttribute_t2333618243;
// System.Runtime.Serialization.DataContractAttribute
struct DataContractAttribute_t2462274566;
// System.Runtime.Serialization.DataMemberAttribute
struct DataMemberAttribute_t2601848894;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.ICustomAttributeProvider
struct ICustomAttributeProvider_t1425685797;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t2159686854;
// System.ComponentModel.TypeConverter
struct TypeConverter_t1753450284;
// Newtonsoft.Json.Utilities.ReflectionDelegateFactory
struct ReflectionDelegateFactory_t1590616920;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_MemberSerializat1550301796.h"
#include "mscorlib_System_Reflection_PropertyInfo924268725.h"

// System.Void Newtonsoft.Json.Serialization.JsonTypeReflector::.cctor()
extern "C"  void JsonTypeReflector__cctor_m1045703757 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonContainerAttribute Newtonsoft.Json.Serialization.JsonTypeReflector::GetJsonContainerAttribute(System.Type)
extern "C"  JsonContainerAttribute_t1917602971 * JsonTypeReflector_GetJsonContainerAttribute_m1181196189 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JObjectAttribute Newtonsoft.Json.Serialization.JsonTypeReflector::GetJObjectAttribute(System.Type)
extern "C"  JObjectAttribute_t3411920523 * JsonTypeReflector_GetJObjectAttribute_m2022472605 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonArrayAttribute Newtonsoft.Json.Serialization.JsonTypeReflector::GetJsonArrayAttribute(System.Type)
extern "C"  JsonArrayAttribute_t2333618243 * JsonTypeReflector_GetJsonArrayAttribute_m4217886877 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.DataContractAttribute Newtonsoft.Json.Serialization.JsonTypeReflector::GetDataContractAttribute(System.Type)
extern "C"  DataContractAttribute_t2462274566 * JsonTypeReflector_GetDataContractAttribute_m3859448443 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.DataMemberAttribute Newtonsoft.Json.Serialization.JsonTypeReflector::GetDataMemberAttribute(System.Reflection.MemberInfo)
extern "C"  DataMemberAttribute_t2601848894 * JsonTypeReflector_GetDataMemberAttribute_m2774782914 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___memberInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.MemberSerialization Newtonsoft.Json.Serialization.JsonTypeReflector::GetObjectMemberSerialization(System.Type)
extern "C"  int32_t JsonTypeReflector_GetObjectMemberSerialization_m3310198566 (Il2CppObject * __this /* static, unused */, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonTypeReflector::GetJsonConverterType(System.Reflection.ICustomAttributeProvider)
extern "C"  Type_t * JsonTypeReflector_GetJsonConverterType_m3579551887 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___attributeProvider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonTypeReflector::GetJsonConverterTypeFromAttribute(System.Reflection.ICustomAttributeProvider)
extern "C"  Type_t * JsonTypeReflector_GetJsonConverterTypeFromAttribute_m1704420503 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___attributeProvider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonTypeReflector::GetJsonConverter(System.Reflection.ICustomAttributeProvider,System.Type)
extern "C"  JsonConverter_t2159686854 * JsonTypeReflector_GetJsonConverter_m2583059506 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___attributeProvider0, Type_t * ___targetConvertedType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.TypeConverter Newtonsoft.Json.Serialization.JsonTypeReflector::GetTypeConverter(System.Type)
extern "C"  TypeConverter_t1753450284 * JsonTypeReflector_GetTypeConverter_m1748432949 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonTypeReflector::GetAssociatedMetadataType(System.Type)
extern "C"  Type_t * JsonTypeReflector_GetAssociatedMetadataType_m3014405724 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonTypeReflector::GetAssociateMetadataTypeFromAttribute(System.Type)
extern "C"  Type_t * JsonTypeReflector_GetAssociateMetadataTypeFromAttribute_m2509584000 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonTypeReflector::GetMetadataTypeAttributeType()
extern "C"  Type_t * JsonTypeReflector_GetMetadataTypeAttributeType_m2087362945 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonTypeReflector::get_DynamicCodeGeneration()
extern "C"  bool JsonTypeReflector_get_DynamicCodeGeneration_m1078305485 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.ReflectionDelegateFactory Newtonsoft.Json.Serialization.JsonTypeReflector::get_ReflectionDelegateFactory()
extern "C"  ReflectionDelegateFactory_t1590616920 * JsonTypeReflector_get_ReflectionDelegateFactory_m10041857 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonTypeReflector::ilo_IsVirtual1(System.Reflection.PropertyInfo)
extern "C"  bool JsonTypeReflector_ilo_IsVirtual1_m2162606700 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___propertyInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JObjectAttribute Newtonsoft.Json.Serialization.JsonTypeReflector::ilo_GetJObjectAttribute2(System.Type)
extern "C"  JObjectAttribute_t3411920523 * JsonTypeReflector_ilo_GetJObjectAttribute2_m392300782 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.DataContractAttribute Newtonsoft.Json.Serialization.JsonTypeReflector::ilo_GetDataContractAttribute3(System.Type)
extern "C"  DataContractAttribute_t2462274566 * JsonTypeReflector_ilo_GetDataContractAttribute3_m2230061749 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonTypeReflector::ilo_CreateJsonConverterInstance4(System.Type)
extern "C"  JsonConverter_t2159686854 * JsonTypeReflector_ilo_CreateJsonConverterInstance4_m1604769551 (Il2CppObject * __this /* static, unused */, Type_t * ___converterType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonTypeReflector::ilo_GetMetadataTypeAttributeType5()
extern "C"  Type_t * JsonTypeReflector_ilo_GetMetadataTypeAttributeType5_m302294883 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonTypeReflector::ilo_get_DynamicCodeGeneration6()
extern "C"  bool JsonTypeReflector_ilo_get_DynamicCodeGeneration6_m2743078814 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
