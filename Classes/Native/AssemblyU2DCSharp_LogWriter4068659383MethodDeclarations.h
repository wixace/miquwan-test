﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LogWriter
struct LogWriter_t4068659383;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t3076817455;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t1111884825;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_LogLevel2060375744.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Security_Cryptography_X509Certific3076817455.h"
#include "System_System_Security_Cryptography_X509Certificat1111884825.h"
#include "System_System_Net_Security_SslPolicyErrors3099591579.h"

// System.Void LogWriter::.ctor()
extern "C"  void LogWriter__ctor_m2302812820 (LogWriter_t4068659383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LogWriter::.cctor()
extern "C"  void LogWriter__cctor_m2185624473 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LogWriter::Release()
extern "C"  void LogWriter_Release_m831971769 (LogWriter_t4068659383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LogWriter::UploadTodayLog()
extern "C"  void LogWriter_UploadTodayLog_m3251096020 (LogWriter_t4068659383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LogWriter::WriteLog(System.String,LogLevel,System.Boolean)
extern "C"  void LogWriter_WriteLog_m1351003588 (LogWriter_t4068659383 * __this, String_t* ___msg0, int32_t ___level1, bool ___writeEditorLog2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LogWriter::Write(System.String,LogLevel,System.Boolean)
extern "C"  void LogWriter_Write_m3188633032 (LogWriter_t4068659383 * __this, String_t* ___msg0, int32_t ___level1, bool ___writeEditorLog2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LogWriter::SendEmail(System.String,System.String)
extern "C"  bool LogWriter_SendEmail_m3618756772 (LogWriter_t4068659383 * __this, String_t* ___date0, String_t* ___toAddress1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LogWriter::<SendEmail>m__490(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern "C"  bool LogWriter_U3CSendEmailU3Em__490_m4196300617 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___s0, X509Certificate_t3076817455 * ___certificate1, X509Chain_t1111884825 * ___chain2, int32_t ___sslPolicyErrors3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
