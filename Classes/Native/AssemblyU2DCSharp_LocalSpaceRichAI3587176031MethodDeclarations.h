﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocalSpaceRichAI
struct LocalSpaceRichAI_t3587176031;
// Pathfinding.RichFunnel
struct RichFunnel_t2453525928;
// Seeker
struct Seeker_t2472610117;
// Pathfinding.Path
struct Path_t1974241691;
// System.Object
struct Il2CppObject;
// LocalSpaceGraph
struct LocalSpaceGraph_t660012435;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_RichFunnel2453525928.h"
#include "AssemblyU2DCSharp_Seeker2472610117.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "AssemblyU2DCSharp_LocalSpaceGraph660012435.h"

// System.Void LocalSpaceRichAI::.ctor()
extern "C"  void LocalSpaceRichAI__ctor_m1700457244 (LocalSpaceRichAI_t3587176031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalSpaceRichAI::UpdatePath()
extern "C"  void LocalSpaceRichAI_UpdatePath_m597192310 (LocalSpaceRichAI_t3587176031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 LocalSpaceRichAI::UpdateTarget(Pathfinding.RichFunnel)
extern "C"  Vector3_t4282066566  LocalSpaceRichAI_UpdateTarget_m1593042240 (LocalSpaceRichAI_t3587176031 * __this, RichFunnel_t2453525928 * ___fn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalSpaceRichAI::ilo_IsDone1(Seeker)
extern "C"  bool LocalSpaceRichAI_ilo_IsDone1_m523476019 (Il2CppObject * __this /* static, unused */, Seeker_t2472610117 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalSpaceRichAI::ilo_Claim2(Pathfinding.Path,System.Object)
extern "C"  void LocalSpaceRichAI_ilo_Claim2_m3553951600 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, Il2CppObject * ___o1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 LocalSpaceRichAI::ilo_GetMatrix3(LocalSpaceGraph)
extern "C"  Matrix4x4_t1651859333  LocalSpaceRichAI_ilo_GetMatrix3_m2775353901 (Il2CppObject * __this /* static, unused */, LocalSpaceGraph_t660012435 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
