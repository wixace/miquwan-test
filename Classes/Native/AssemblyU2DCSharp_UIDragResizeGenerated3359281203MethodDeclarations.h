﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIDragResizeGenerated
struct UIDragResizeGenerated_t3359281203;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UIDragResizeGenerated::.ctor()
extern "C"  void UIDragResizeGenerated__ctor_m380629656 (UIDragResizeGenerated_t3359281203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDragResizeGenerated::UIDragResize_UIDragResize1(JSVCall,System.Int32)
extern "C"  bool UIDragResizeGenerated_UIDragResize_UIDragResize1_m1717030874 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragResizeGenerated::UIDragResize_target(JSVCall)
extern "C"  void UIDragResizeGenerated_UIDragResize_target_m971298269 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragResizeGenerated::UIDragResize_pivot(JSVCall)
extern "C"  void UIDragResizeGenerated_UIDragResize_pivot_m1924803516 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragResizeGenerated::UIDragResize_minWidth(JSVCall)
extern "C"  void UIDragResizeGenerated_UIDragResize_minWidth_m761556538 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragResizeGenerated::UIDragResize_minHeight(JSVCall)
extern "C"  void UIDragResizeGenerated_UIDragResize_minHeight_m2788859845 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragResizeGenerated::UIDragResize_maxWidth(JSVCall)
extern "C"  void UIDragResizeGenerated_UIDragResize_maxWidth_m2741478860 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragResizeGenerated::UIDragResize_maxHeight(JSVCall)
extern "C"  void UIDragResizeGenerated_UIDragResize_maxHeight_m4036909683 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragResizeGenerated::__Register()
extern "C"  void UIDragResizeGenerated___Register_m477718831 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragResizeGenerated::ilo_setEnum1(System.Int32,System.Int32)
extern "C"  void UIDragResizeGenerated_ilo_setEnum1_m216375353 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIDragResizeGenerated::ilo_getEnum2(System.Int32)
extern "C"  int32_t UIDragResizeGenerated_ilo_getEnum2_m327975901 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIDragResizeGenerated::ilo_getInt323(System.Int32)
extern "C"  int32_t UIDragResizeGenerated_ilo_getInt323_m2054010173 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragResizeGenerated::ilo_setInt324(System.Int32,System.Int32)
extern "C"  void UIDragResizeGenerated_ilo_setInt324_m462776923 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
