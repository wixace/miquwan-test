﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.ObjImporter
struct  ObjImporter_t4130203403  : public Il2CppObject
{
public:

public:
};

struct ObjImporter_t4130203403_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Pathfinding.ObjImporter::<>f__switch$map1
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map1_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Pathfinding.ObjImporter::<>f__switch$map2
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map2_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_0() { return static_cast<int32_t>(offsetof(ObjImporter_t4130203403_StaticFields, ___U3CU3Ef__switchU24map1_0)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map1_0() const { return ___U3CU3Ef__switchU24map1_0; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map1_0() { return &___U3CU3Ef__switchU24map1_0; }
	inline void set_U3CU3Ef__switchU24map1_0(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map1_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2_1() { return static_cast<int32_t>(offsetof(ObjImporter_t4130203403_StaticFields, ___U3CU3Ef__switchU24map2_1)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map2_1() const { return ___U3CU3Ef__switchU24map2_1; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map2_1() { return &___U3CU3Ef__switchU24map2_1; }
	inline void set_U3CU3Ef__switchU24map2_1(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map2_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map2_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
