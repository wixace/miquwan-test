﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AudioListener
struct AudioListener_t3685735200;
// System.Single[]
struct SingleU5BU5D_t2316563989;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AudioVelocityUpdateMode1600323189.h"
#include "UnityEngine_UnityEngine_FFTWindow2499314106.h"

// System.Void UnityEngine.AudioListener::.ctor()
extern "C"  void AudioListener__ctor_m2748512879 (AudioListener_t3685735200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioListener::get_volume()
extern "C"  float AudioListener_get_volume_m84351148 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioListener::set_volume(System.Single)
extern "C"  void AudioListener_set_volume_m1072709503 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AudioListener::get_pause()
extern "C"  bool AudioListener_get_pause_m2103815328 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioListener::set_pause(System.Boolean)
extern "C"  void AudioListener_set_pause_m581137981 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioVelocityUpdateMode UnityEngine.AudioListener::get_velocityUpdateMode()
extern "C"  int32_t AudioListener_get_velocityUpdateMode_m2360620892 (AudioListener_t3685735200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioListener::set_velocityUpdateMode(UnityEngine.AudioVelocityUpdateMode)
extern "C"  void AudioListener_set_velocityUpdateMode_m3019514319 (AudioListener_t3685735200 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioListener::GetOutputDataHelper(System.Single[],System.Int32)
extern "C"  void AudioListener_GetOutputDataHelper_m306421130 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t2316563989* ___samples0, int32_t ___channel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioListener::GetSpectrumDataHelper(System.Single[],System.Int32,UnityEngine.FFTWindow)
extern "C"  void AudioListener_GetSpectrumDataHelper_m2705789049 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t2316563989* ___samples0, int32_t ___channel1, int32_t ___window2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioListener::GetOutputData(System.Single[],System.Int32)
extern "C"  void AudioListener_GetOutputData_m120486908 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t2316563989* ___samples0, int32_t ___channel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioListener::GetSpectrumData(System.Single[],System.Int32,UnityEngine.FFTWindow)
extern "C"  void AudioListener_GetSpectrumData_m2575018347 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t2316563989* ___samples0, int32_t ___channel1, int32_t ___window2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
