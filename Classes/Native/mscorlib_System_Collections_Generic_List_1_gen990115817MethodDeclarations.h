﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>
struct List_1_t990115817;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JTokenType>
struct IEnumerable_1_t2922843222;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JTokenType>
struct IEnumerator_1_t1533795314;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Newtonsoft.Json.Linq.JTokenType>
struct ICollection_1_t516520252;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Linq.JTokenType>
struct ReadOnlyCollection_1_t1179007801;
// Newtonsoft.Json.Linq.JTokenType[]
struct JTokenTypeU5BU5D_t1069135460;
// System.Predicate`1<Newtonsoft.Json.Linq.JTokenType>
struct Predicate_1_t3527954444;
// System.Collections.Generic.IComparer`1<Newtonsoft.Json.Linq.JTokenType>
struct IComparer_1_t2196944307;
// System.Comparison`1<Newtonsoft.Json.Linq.JTokenType>
struct Comparison_1_t2633258748;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType3916897561.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1009788587.h"

// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::.ctor()
extern "C"  void List_1__ctor_m2077553473_gshared (List_1_t990115817 * __this, const MethodInfo* method);
#define List_1__ctor_m2077553473(__this, method) ((  void (*) (List_1_t990115817 *, const MethodInfo*))List_1__ctor_m2077553473_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1426943335_gshared (List_1_t990115817 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1426943335(__this, ___collection0, method) ((  void (*) (List_1_t990115817 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1426943335_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1657982738_gshared (List_1_t990115817 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1657982738(__this, ___capacity0, method) ((  void (*) (List_1_t990115817 *, int32_t, const MethodInfo*))List_1__ctor_m1657982738_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::.cctor()
extern "C"  void List_1__cctor_m552169109_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m552169109(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m552169109_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m464335466_gshared (List_1_t990115817 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m464335466(__this, method) ((  Il2CppObject* (*) (List_1_t990115817 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m464335466_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2446125100_gshared (List_1_t990115817 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m2446125100(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t990115817 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2446125100_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3789846183_gshared (List_1_t990115817 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3789846183(__this, method) ((  Il2CppObject * (*) (List_1_t990115817 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3789846183_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m4155273514_gshared (List_1_t990115817 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m4155273514(__this, ___item0, method) ((  int32_t (*) (List_1_t990115817 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m4155273514_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m203813730_gshared (List_1_t990115817 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m203813730(__this, ___item0, method) ((  bool (*) (List_1_t990115817 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m203813730_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m731644546_gshared (List_1_t990115817 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m731644546(__this, ___item0, method) ((  int32_t (*) (List_1_t990115817 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m731644546_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m707571885_gshared (List_1_t990115817 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m707571885(__this, ___index0, ___item1, method) ((  void (*) (List_1_t990115817 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m707571885_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m2958320475_gshared (List_1_t990115817 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m2958320475(__this, ___item0, method) ((  void (*) (List_1_t990115817 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m2958320475_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2529857763_gshared (List_1_t990115817 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2529857763(__this, method) ((  bool (*) (List_1_t990115817 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2529857763_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3021017274_gshared (List_1_t990115817 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3021017274(__this, method) ((  bool (*) (List_1_t990115817 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3021017274_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2041956582_gshared (List_1_t990115817 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2041956582(__this, method) ((  Il2CppObject * (*) (List_1_t990115817 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2041956582_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1291342929_gshared (List_1_t990115817 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1291342929(__this, method) ((  bool (*) (List_1_t990115817 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1291342929_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3959348360_gshared (List_1_t990115817 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m3959348360(__this, method) ((  bool (*) (List_1_t990115817 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3959348360_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m585832685_gshared (List_1_t990115817 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m585832685(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t990115817 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m585832685_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m1985701188_gshared (List_1_t990115817 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1985701188(__this, ___index0, ___value1, method) ((  void (*) (List_1_t990115817 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1985701188_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Add(T)
extern "C"  void List_1_Add_m1771914289_gshared (List_1_t990115817 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m1771914289(__this, ___item0, method) ((  void (*) (List_1_t990115817 *, int32_t, const MethodInfo*))List_1_Add_m1771914289_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1641394722_gshared (List_1_t990115817 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1641394722(__this, ___newCount0, method) ((  void (*) (List_1_t990115817 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1641394722_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m4060686469_gshared (List_1_t990115817 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m4060686469(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t990115817 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m4060686469_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2988769568_gshared (List_1_t990115817 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2988769568(__this, ___collection0, method) ((  void (*) (List_1_t990115817 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2988769568_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2249741792_gshared (List_1_t990115817 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2249741792(__this, ___enumerable0, method) ((  void (*) (List_1_t990115817 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2249741792_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2533518519_gshared (List_1_t990115817 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2533518519(__this, ___collection0, method) ((  void (*) (List_1_t990115817 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2533518519_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1179007801 * List_1_AsReadOnly_m987742994_gshared (List_1_t990115817 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m987742994(__this, method) ((  ReadOnlyCollection_1_t1179007801 * (*) (List_1_t990115817 *, const MethodInfo*))List_1_AsReadOnly_m987742994_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m2024304749_gshared (List_1_t990115817 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m2024304749(__this, ___item0, method) ((  int32_t (*) (List_1_t990115817 *, int32_t, const MethodInfo*))List_1_BinarySearch_m2024304749_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Clear()
extern "C"  void List_1_Clear_m210443331_gshared (List_1_t990115817 * __this, const MethodInfo* method);
#define List_1_Clear_m210443331(__this, method) ((  void (*) (List_1_t990115817 *, const MethodInfo*))List_1_Clear_m210443331_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Contains(T)
extern "C"  bool List_1_Contains_m3187321969_gshared (List_1_t990115817 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m3187321969(__this, ___item0, method) ((  bool (*) (List_1_t990115817 *, int32_t, const MethodInfo*))List_1_Contains_m3187321969_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m4262869399_gshared (List_1_t990115817 * __this, JTokenTypeU5BU5D_t1069135460* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m4262869399(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t990115817 *, JTokenTypeU5BU5D_t1069135460*, int32_t, const MethodInfo*))List_1_CopyTo_m4262869399_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m2924068017_gshared (List_1_t990115817 * __this, Predicate_1_t3527954444 * ___match0, const MethodInfo* method);
#define List_1_Find_m2924068017(__this, ___match0, method) ((  int32_t (*) (List_1_t990115817 *, Predicate_1_t3527954444 *, const MethodInfo*))List_1_Find_m2924068017_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1517141804_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3527954444 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1517141804(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3527954444 *, const MethodInfo*))List_1_CheckMatch_m1517141804_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2625091921_gshared (List_1_t990115817 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3527954444 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m2625091921(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t990115817 *, int32_t, int32_t, Predicate_1_t3527954444 *, const MethodInfo*))List_1_GetIndex_m2625091921_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::GetEnumerator()
extern "C"  Enumerator_t1009788587  List_1_GetEnumerator_m116982062_gshared (List_1_t990115817 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m116982062(__this, method) ((  Enumerator_t1009788587  (*) (List_1_t990115817 *, const MethodInfo*))List_1_GetEnumerator_m116982062_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m66747483_gshared (List_1_t990115817 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m66747483(__this, ___item0, method) ((  int32_t (*) (List_1_t990115817 *, int32_t, const MethodInfo*))List_1_IndexOf_m66747483_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2776795374_gshared (List_1_t990115817 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2776795374(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t990115817 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2776795374_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m4009236583_gshared (List_1_t990115817 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m4009236583(__this, ___index0, method) ((  void (*) (List_1_t990115817 *, int32_t, const MethodInfo*))List_1_CheckIndex_m4009236583_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m710134222_gshared (List_1_t990115817 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m710134222(__this, ___index0, ___item1, method) ((  void (*) (List_1_t990115817 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m710134222_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3696069571_gshared (List_1_t990115817 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3696069571(__this, ___collection0, method) ((  void (*) (List_1_t990115817 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3696069571_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Remove(T)
extern "C"  bool List_1_Remove_m2653017260_gshared (List_1_t990115817 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m2653017260(__this, ___item0, method) ((  bool (*) (List_1_t990115817 *, int32_t, const MethodInfo*))List_1_Remove_m2653017260_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m259112414_gshared (List_1_t990115817 * __this, Predicate_1_t3527954444 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m259112414(__this, ___match0, method) ((  int32_t (*) (List_1_t990115817 *, Predicate_1_t3527954444 *, const MethodInfo*))List_1_RemoveAll_m259112414_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m133099851_gshared (List_1_t990115817 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m133099851(__this, ___index0, method) ((  void (*) (List_1_t990115817 *, int32_t, const MethodInfo*))List_1_RemoveAt_m133099851_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m1062440887_gshared (List_1_t990115817 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m1062440887(__this, ___index0, ___count1, method) ((  void (*) (List_1_t990115817 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1062440887_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Reverse()
extern "C"  void List_1_Reverse_m2035265112_gshared (List_1_t990115817 * __this, const MethodInfo* method);
#define List_1_Reverse_m2035265112(__this, method) ((  void (*) (List_1_t990115817 *, const MethodInfo*))List_1_Reverse_m2035265112_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Sort()
extern "C"  void List_1_Sort_m1299312714_gshared (List_1_t990115817 * __this, const MethodInfo* method);
#define List_1_Sort_m1299312714(__this, method) ((  void (*) (List_1_t990115817 *, const MethodInfo*))List_1_Sort_m1299312714_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1516417242_gshared (List_1_t990115817 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m1516417242(__this, ___comparer0, method) ((  void (*) (List_1_t990115817 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1516417242_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m3459926301_gshared (List_1_t990115817 * __this, Comparison_1_t2633258748 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m3459926301(__this, ___comparison0, method) ((  void (*) (List_1_t990115817 *, Comparison_1_t2633258748 *, const MethodInfo*))List_1_Sort_m3459926301_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::ToArray()
extern "C"  JTokenTypeU5BU5D_t1069135460* List_1_ToArray_m12903575_gshared (List_1_t990115817 * __this, const MethodInfo* method);
#define List_1_ToArray_m12903575(__this, method) ((  JTokenTypeU5BU5D_t1069135460* (*) (List_1_t990115817 *, const MethodInfo*))List_1_ToArray_m12903575_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::TrimExcess()
extern "C"  void List_1_TrimExcess_m3204067427_gshared (List_1_t990115817 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m3204067427(__this, method) ((  void (*) (List_1_t990115817 *, const MethodInfo*))List_1_TrimExcess_m3204067427_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m796839691_gshared (List_1_t990115817 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m796839691(__this, method) ((  int32_t (*) (List_1_t990115817 *, const MethodInfo*))List_1_get_Capacity_m796839691_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3146550836_gshared (List_1_t990115817 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3146550836(__this, ___value0, method) ((  void (*) (List_1_t990115817 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3146550836_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::get_Count()
extern "C"  int32_t List_1_get_Count_m1994220279_gshared (List_1_t990115817 * __this, const MethodInfo* method);
#define List_1_get_Count_m1994220279(__this, method) ((  int32_t (*) (List_1_t990115817 *, const MethodInfo*))List_1_get_Count_m1994220279_gshared)(__this, method)
// T System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m4253882302_gshared (List_1_t990115817 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m4253882302(__this, ___index0, method) ((  int32_t (*) (List_1_t990115817 *, int32_t, const MethodInfo*))List_1_get_Item_m4253882302_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JTokenType>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3352154611_gshared (List_1_t990115817 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m3352154611(__this, ___index0, ___value1, method) ((  void (*) (List_1_t990115817 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m3352154611_gshared)(__this, ___index0, ___value1, method)
