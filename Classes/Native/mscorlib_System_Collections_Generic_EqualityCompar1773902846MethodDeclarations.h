﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.ClipperLib.IntPoint>
struct DefaultComparer_t1773902846;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.ClipperLib.IntPoint>::.ctor()
extern "C"  void DefaultComparer__ctor_m556725323_gshared (DefaultComparer_t1773902846 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m556725323(__this, method) ((  void (*) (DefaultComparer_t1773902846 *, const MethodInfo*))DefaultComparer__ctor_m556725323_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.ClipperLib.IntPoint>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2568508160_gshared (DefaultComparer_t1773902846 * __this, IntPoint_t3326126179  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2568508160(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1773902846 *, IntPoint_t3326126179 , const MethodInfo*))DefaultComparer_GetHashCode_m2568508160_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.ClipperLib.IntPoint>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2084245148_gshared (DefaultComparer_t1773902846 * __this, IntPoint_t3326126179  ___x0, IntPoint_t3326126179  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2084245148(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1773902846 *, IntPoint_t3326126179 , IntPoint_t3326126179 , const MethodInfo*))DefaultComparer_Equals_m2084245148_gshared)(__this, ___x0, ___y1, method)
