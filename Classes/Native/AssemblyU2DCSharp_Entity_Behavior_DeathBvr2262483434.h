﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.DeathBvr
struct  DeathBvr_t2262483434  : public IBehavior_t770859129
{
public:
	// System.Single Entity.Behavior.DeathBvr::lasttime
	float ___lasttime_2;
	// System.Boolean Entity.Behavior.DeathBvr::isPlaydeathAni
	bool ___isPlaydeathAni_3;

public:
	inline static int32_t get_offset_of_lasttime_2() { return static_cast<int32_t>(offsetof(DeathBvr_t2262483434, ___lasttime_2)); }
	inline float get_lasttime_2() const { return ___lasttime_2; }
	inline float* get_address_of_lasttime_2() { return &___lasttime_2; }
	inline void set_lasttime_2(float value)
	{
		___lasttime_2 = value;
	}

	inline static int32_t get_offset_of_isPlaydeathAni_3() { return static_cast<int32_t>(offsetof(DeathBvr_t2262483434, ___isPlaydeathAni_3)); }
	inline bool get_isPlaydeathAni_3() const { return ___isPlaydeathAni_3; }
	inline bool* get_address_of_isPlaydeathAni_3() { return &___isPlaydeathAni_3; }
	inline void set_isPlaydeathAni_3(bool value)
	{
		___isPlaydeathAni_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
