﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_gaiyis16
struct  M_gaiyis16_t747189417  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_gaiyis16::_degaWearorpoo
	bool ____degaWearorpoo_0;
	// System.UInt32 GarbageiOS.M_gaiyis16::_rice
	uint32_t ____rice_1;
	// System.UInt32 GarbageiOS.M_gaiyis16::_geechir
	uint32_t ____geechir_2;
	// System.UInt32 GarbageiOS.M_gaiyis16::_rijaybai
	uint32_t ____rijaybai_3;
	// System.Single GarbageiOS.M_gaiyis16::_nakalldraWhelee
	float ____nakalldraWhelee_4;
	// System.Int32 GarbageiOS.M_gaiyis16::_meporror
	int32_t ____meporror_5;
	// System.Boolean GarbageiOS.M_gaiyis16::_caxe
	bool ____caxe_6;
	// System.String GarbageiOS.M_gaiyis16::_sibeeChana
	String_t* ____sibeeChana_7;
	// System.Int32 GarbageiOS.M_gaiyis16::_cairjall
	int32_t ____cairjall_8;
	// System.String GarbageiOS.M_gaiyis16::_boosirbirTorjur
	String_t* ____boosirbirTorjur_9;
	// System.Boolean GarbageiOS.M_gaiyis16::_chearmoubaSoute
	bool ____chearmoubaSoute_10;
	// System.Single GarbageiOS.M_gaiyis16::_demxasWalilaw
	float ____demxasWalilaw_11;
	// System.Single GarbageiOS.M_gaiyis16::_ciwhaiSowcere
	float ____ciwhaiSowcere_12;
	// System.UInt32 GarbageiOS.M_gaiyis16::_toudowjaTiwhow
	uint32_t ____toudowjaTiwhow_13;
	// System.Single GarbageiOS.M_gaiyis16::_caipawQaltorsas
	float ____caipawQaltorsas_14;
	// System.String GarbageiOS.M_gaiyis16::_keeleecalPaymere
	String_t* ____keeleecalPaymere_15;
	// System.Int32 GarbageiOS.M_gaiyis16::_jerdorro
	int32_t ____jerdorro_16;
	// System.String GarbageiOS.M_gaiyis16::_yelgelstawTrerleese
	String_t* ____yelgelstawTrerleese_17;
	// System.Boolean GarbageiOS.M_gaiyis16::_viscorci
	bool ____viscorci_18;
	// System.UInt32 GarbageiOS.M_gaiyis16::_berlair
	uint32_t ____berlair_19;
	// System.Single GarbageiOS.M_gaiyis16::_burislooRuyou
	float ____burislooRuyou_20;
	// System.UInt32 GarbageiOS.M_gaiyis16::_teargawcorKuxa
	uint32_t ____teargawcorKuxa_21;
	// System.Int32 GarbageiOS.M_gaiyis16::_souce
	int32_t ____souce_22;
	// System.UInt32 GarbageiOS.M_gaiyis16::_moufouteaWeawear
	uint32_t ____moufouteaWeawear_23;
	// System.Boolean GarbageiOS.M_gaiyis16::_leras
	bool ____leras_24;
	// System.Boolean GarbageiOS.M_gaiyis16::_jechasDresar
	bool ____jechasDresar_25;
	// System.Boolean GarbageiOS.M_gaiyis16::_canoo
	bool ____canoo_26;
	// System.UInt32 GarbageiOS.M_gaiyis16::_jukahi
	uint32_t ____jukahi_27;
	// System.Int32 GarbageiOS.M_gaiyis16::_mouseafi
	int32_t ____mouseafi_28;
	// System.String GarbageiOS.M_gaiyis16::_terbouTadisee
	String_t* ____terbouTadisee_29;
	// System.String GarbageiOS.M_gaiyis16::_korreDrerdri
	String_t* ____korreDrerdri_30;
	// System.Boolean GarbageiOS.M_gaiyis16::_jelpikerChemdowbel
	bool ____jelpikerChemdowbel_31;
	// System.Boolean GarbageiOS.M_gaiyis16::_quje
	bool ____quje_32;
	// System.String GarbageiOS.M_gaiyis16::_basga
	String_t* ____basga_33;
	// System.String GarbageiOS.M_gaiyis16::_hisxoSeakelmair
	String_t* ____hisxoSeakelmair_34;
	// System.Boolean GarbageiOS.M_gaiyis16::_nersasVelchir
	bool ____nersasVelchir_35;
	// System.String GarbageiOS.M_gaiyis16::_lajearyorPabou
	String_t* ____lajearyorPabou_36;
	// System.UInt32 GarbageiOS.M_gaiyis16::_nalmi
	uint32_t ____nalmi_37;
	// System.UInt32 GarbageiOS.M_gaiyis16::_catouChuwehou
	uint32_t ____catouChuwehou_38;
	// System.Int32 GarbageiOS.M_gaiyis16::_wevawhorHearzu
	int32_t ____wevawhorHearzu_39;
	// System.String GarbageiOS.M_gaiyis16::_makairKeatoo
	String_t* ____makairKeatoo_40;
	// System.Boolean GarbageiOS.M_gaiyis16::_nekeHeeci
	bool ____nekeHeeci_41;
	// System.Int32 GarbageiOS.M_gaiyis16::_leerameChase
	int32_t ____leerameChase_42;
	// System.Single GarbageiOS.M_gaiyis16::_trabeWirtowtea
	float ____trabeWirtowtea_43;
	// System.Int32 GarbageiOS.M_gaiyis16::_nastirqe
	int32_t ____nastirqe_44;
	// System.UInt32 GarbageiOS.M_gaiyis16::_hemiBoubefis
	uint32_t ____hemiBoubefis_45;
	// System.String GarbageiOS.M_gaiyis16::_dahisjeeFouqu
	String_t* ____dahisjeeFouqu_46;
	// System.Boolean GarbageiOS.M_gaiyis16::_biwhou
	bool ____biwhou_47;
	// System.String GarbageiOS.M_gaiyis16::_halarbayStewheechas
	String_t* ____halarbayStewheechas_48;
	// System.Single GarbageiOS.M_gaiyis16::_pairailea
	float ____pairailea_49;
	// System.Int32 GarbageiOS.M_gaiyis16::_peafoudrur
	int32_t ____peafoudrur_50;
	// System.String GarbageiOS.M_gaiyis16::_maqoupou
	String_t* ____maqoupou_51;
	// System.String GarbageiOS.M_gaiyis16::_boodeNasbisdis
	String_t* ____boodeNasbisdis_52;
	// System.Boolean GarbageiOS.M_gaiyis16::_weekairwha
	bool ____weekairwha_53;
	// System.String GarbageiOS.M_gaiyis16::_toorowtiFesacear
	String_t* ____toorowtiFesacear_54;
	// System.Boolean GarbageiOS.M_gaiyis16::_keaseLirer
	bool ____keaseLirer_55;
	// System.Int32 GarbageiOS.M_gaiyis16::_votitairDroser
	int32_t ____votitairDroser_56;
	// System.Single GarbageiOS.M_gaiyis16::_semtri
	float ____semtri_57;
	// System.Int32 GarbageiOS.M_gaiyis16::_duhuDrebal
	int32_t ____duhuDrebal_58;
	// System.Single GarbageiOS.M_gaiyis16::_lejoCowmay
	float ____lejoCowmay_59;
	// System.UInt32 GarbageiOS.M_gaiyis16::_soonasManejall
	uint32_t ____soonasManejall_60;
	// System.Single GarbageiOS.M_gaiyis16::_jesidoKeasar
	float ____jesidoKeasar_61;
	// System.Boolean GarbageiOS.M_gaiyis16::_howdorjir
	bool ____howdorjir_62;
	// System.Boolean GarbageiOS.M_gaiyis16::_teferPejairfi
	bool ____teferPejairfi_63;
	// System.String GarbageiOS.M_gaiyis16::_makido
	String_t* ____makido_64;
	// System.Boolean GarbageiOS.M_gaiyis16::_lelkaStiscee
	bool ____lelkaStiscee_65;
	// System.Boolean GarbageiOS.M_gaiyis16::_poujasmowNemtownas
	bool ____poujasmowNemtownas_66;
	// System.UInt32 GarbageiOS.M_gaiyis16::_bemmuseaJagaw
	uint32_t ____bemmuseaJagaw_67;
	// System.Boolean GarbageiOS.M_gaiyis16::_gajouCukarsur
	bool ____gajouCukarsur_68;
	// System.Int32 GarbageiOS.M_gaiyis16::_maysaDeatrerecai
	int32_t ____maysaDeatrerecai_69;
	// System.String GarbageiOS.M_gaiyis16::_peceme
	String_t* ____peceme_70;
	// System.Boolean GarbageiOS.M_gaiyis16::_rawhearNuho
	bool ____rawhearNuho_71;
	// System.Single GarbageiOS.M_gaiyis16::_lemnuCecemow
	float ____lemnuCecemow_72;
	// System.Single GarbageiOS.M_gaiyis16::_vemci
	float ____vemci_73;
	// System.UInt32 GarbageiOS.M_gaiyis16::_setrasstereHisyuvar
	uint32_t ____setrasstereHisyuvar_74;
	// System.Boolean GarbageiOS.M_gaiyis16::_whawtonow
	bool ____whawtonow_75;
	// System.Int32 GarbageiOS.M_gaiyis16::_mallnejeSevargas
	int32_t ____mallnejeSevargas_76;
	// System.UInt32 GarbageiOS.M_gaiyis16::_dallze
	uint32_t ____dallze_77;
	// System.Single GarbageiOS.M_gaiyis16::_becha
	float ____becha_78;
	// System.UInt32 GarbageiOS.M_gaiyis16::_dralre
	uint32_t ____dralre_79;
	// System.String GarbageiOS.M_gaiyis16::_chawpaw
	String_t* ____chawpaw_80;
	// System.Boolean GarbageiOS.M_gaiyis16::_darnou
	bool ____darnou_81;

public:
	inline static int32_t get_offset_of__degaWearorpoo_0() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____degaWearorpoo_0)); }
	inline bool get__degaWearorpoo_0() const { return ____degaWearorpoo_0; }
	inline bool* get_address_of__degaWearorpoo_0() { return &____degaWearorpoo_0; }
	inline void set__degaWearorpoo_0(bool value)
	{
		____degaWearorpoo_0 = value;
	}

	inline static int32_t get_offset_of__rice_1() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____rice_1)); }
	inline uint32_t get__rice_1() const { return ____rice_1; }
	inline uint32_t* get_address_of__rice_1() { return &____rice_1; }
	inline void set__rice_1(uint32_t value)
	{
		____rice_1 = value;
	}

	inline static int32_t get_offset_of__geechir_2() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____geechir_2)); }
	inline uint32_t get__geechir_2() const { return ____geechir_2; }
	inline uint32_t* get_address_of__geechir_2() { return &____geechir_2; }
	inline void set__geechir_2(uint32_t value)
	{
		____geechir_2 = value;
	}

	inline static int32_t get_offset_of__rijaybai_3() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____rijaybai_3)); }
	inline uint32_t get__rijaybai_3() const { return ____rijaybai_3; }
	inline uint32_t* get_address_of__rijaybai_3() { return &____rijaybai_3; }
	inline void set__rijaybai_3(uint32_t value)
	{
		____rijaybai_3 = value;
	}

	inline static int32_t get_offset_of__nakalldraWhelee_4() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____nakalldraWhelee_4)); }
	inline float get__nakalldraWhelee_4() const { return ____nakalldraWhelee_4; }
	inline float* get_address_of__nakalldraWhelee_4() { return &____nakalldraWhelee_4; }
	inline void set__nakalldraWhelee_4(float value)
	{
		____nakalldraWhelee_4 = value;
	}

	inline static int32_t get_offset_of__meporror_5() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____meporror_5)); }
	inline int32_t get__meporror_5() const { return ____meporror_5; }
	inline int32_t* get_address_of__meporror_5() { return &____meporror_5; }
	inline void set__meporror_5(int32_t value)
	{
		____meporror_5 = value;
	}

	inline static int32_t get_offset_of__caxe_6() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____caxe_6)); }
	inline bool get__caxe_6() const { return ____caxe_6; }
	inline bool* get_address_of__caxe_6() { return &____caxe_6; }
	inline void set__caxe_6(bool value)
	{
		____caxe_6 = value;
	}

	inline static int32_t get_offset_of__sibeeChana_7() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____sibeeChana_7)); }
	inline String_t* get__sibeeChana_7() const { return ____sibeeChana_7; }
	inline String_t** get_address_of__sibeeChana_7() { return &____sibeeChana_7; }
	inline void set__sibeeChana_7(String_t* value)
	{
		____sibeeChana_7 = value;
		Il2CppCodeGenWriteBarrier(&____sibeeChana_7, value);
	}

	inline static int32_t get_offset_of__cairjall_8() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____cairjall_8)); }
	inline int32_t get__cairjall_8() const { return ____cairjall_8; }
	inline int32_t* get_address_of__cairjall_8() { return &____cairjall_8; }
	inline void set__cairjall_8(int32_t value)
	{
		____cairjall_8 = value;
	}

	inline static int32_t get_offset_of__boosirbirTorjur_9() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____boosirbirTorjur_9)); }
	inline String_t* get__boosirbirTorjur_9() const { return ____boosirbirTorjur_9; }
	inline String_t** get_address_of__boosirbirTorjur_9() { return &____boosirbirTorjur_9; }
	inline void set__boosirbirTorjur_9(String_t* value)
	{
		____boosirbirTorjur_9 = value;
		Il2CppCodeGenWriteBarrier(&____boosirbirTorjur_9, value);
	}

	inline static int32_t get_offset_of__chearmoubaSoute_10() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____chearmoubaSoute_10)); }
	inline bool get__chearmoubaSoute_10() const { return ____chearmoubaSoute_10; }
	inline bool* get_address_of__chearmoubaSoute_10() { return &____chearmoubaSoute_10; }
	inline void set__chearmoubaSoute_10(bool value)
	{
		____chearmoubaSoute_10 = value;
	}

	inline static int32_t get_offset_of__demxasWalilaw_11() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____demxasWalilaw_11)); }
	inline float get__demxasWalilaw_11() const { return ____demxasWalilaw_11; }
	inline float* get_address_of__demxasWalilaw_11() { return &____demxasWalilaw_11; }
	inline void set__demxasWalilaw_11(float value)
	{
		____demxasWalilaw_11 = value;
	}

	inline static int32_t get_offset_of__ciwhaiSowcere_12() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____ciwhaiSowcere_12)); }
	inline float get__ciwhaiSowcere_12() const { return ____ciwhaiSowcere_12; }
	inline float* get_address_of__ciwhaiSowcere_12() { return &____ciwhaiSowcere_12; }
	inline void set__ciwhaiSowcere_12(float value)
	{
		____ciwhaiSowcere_12 = value;
	}

	inline static int32_t get_offset_of__toudowjaTiwhow_13() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____toudowjaTiwhow_13)); }
	inline uint32_t get__toudowjaTiwhow_13() const { return ____toudowjaTiwhow_13; }
	inline uint32_t* get_address_of__toudowjaTiwhow_13() { return &____toudowjaTiwhow_13; }
	inline void set__toudowjaTiwhow_13(uint32_t value)
	{
		____toudowjaTiwhow_13 = value;
	}

	inline static int32_t get_offset_of__caipawQaltorsas_14() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____caipawQaltorsas_14)); }
	inline float get__caipawQaltorsas_14() const { return ____caipawQaltorsas_14; }
	inline float* get_address_of__caipawQaltorsas_14() { return &____caipawQaltorsas_14; }
	inline void set__caipawQaltorsas_14(float value)
	{
		____caipawQaltorsas_14 = value;
	}

	inline static int32_t get_offset_of__keeleecalPaymere_15() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____keeleecalPaymere_15)); }
	inline String_t* get__keeleecalPaymere_15() const { return ____keeleecalPaymere_15; }
	inline String_t** get_address_of__keeleecalPaymere_15() { return &____keeleecalPaymere_15; }
	inline void set__keeleecalPaymere_15(String_t* value)
	{
		____keeleecalPaymere_15 = value;
		Il2CppCodeGenWriteBarrier(&____keeleecalPaymere_15, value);
	}

	inline static int32_t get_offset_of__jerdorro_16() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____jerdorro_16)); }
	inline int32_t get__jerdorro_16() const { return ____jerdorro_16; }
	inline int32_t* get_address_of__jerdorro_16() { return &____jerdorro_16; }
	inline void set__jerdorro_16(int32_t value)
	{
		____jerdorro_16 = value;
	}

	inline static int32_t get_offset_of__yelgelstawTrerleese_17() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____yelgelstawTrerleese_17)); }
	inline String_t* get__yelgelstawTrerleese_17() const { return ____yelgelstawTrerleese_17; }
	inline String_t** get_address_of__yelgelstawTrerleese_17() { return &____yelgelstawTrerleese_17; }
	inline void set__yelgelstawTrerleese_17(String_t* value)
	{
		____yelgelstawTrerleese_17 = value;
		Il2CppCodeGenWriteBarrier(&____yelgelstawTrerleese_17, value);
	}

	inline static int32_t get_offset_of__viscorci_18() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____viscorci_18)); }
	inline bool get__viscorci_18() const { return ____viscorci_18; }
	inline bool* get_address_of__viscorci_18() { return &____viscorci_18; }
	inline void set__viscorci_18(bool value)
	{
		____viscorci_18 = value;
	}

	inline static int32_t get_offset_of__berlair_19() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____berlair_19)); }
	inline uint32_t get__berlair_19() const { return ____berlair_19; }
	inline uint32_t* get_address_of__berlair_19() { return &____berlair_19; }
	inline void set__berlair_19(uint32_t value)
	{
		____berlair_19 = value;
	}

	inline static int32_t get_offset_of__burislooRuyou_20() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____burislooRuyou_20)); }
	inline float get__burislooRuyou_20() const { return ____burislooRuyou_20; }
	inline float* get_address_of__burislooRuyou_20() { return &____burislooRuyou_20; }
	inline void set__burislooRuyou_20(float value)
	{
		____burislooRuyou_20 = value;
	}

	inline static int32_t get_offset_of__teargawcorKuxa_21() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____teargawcorKuxa_21)); }
	inline uint32_t get__teargawcorKuxa_21() const { return ____teargawcorKuxa_21; }
	inline uint32_t* get_address_of__teargawcorKuxa_21() { return &____teargawcorKuxa_21; }
	inline void set__teargawcorKuxa_21(uint32_t value)
	{
		____teargawcorKuxa_21 = value;
	}

	inline static int32_t get_offset_of__souce_22() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____souce_22)); }
	inline int32_t get__souce_22() const { return ____souce_22; }
	inline int32_t* get_address_of__souce_22() { return &____souce_22; }
	inline void set__souce_22(int32_t value)
	{
		____souce_22 = value;
	}

	inline static int32_t get_offset_of__moufouteaWeawear_23() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____moufouteaWeawear_23)); }
	inline uint32_t get__moufouteaWeawear_23() const { return ____moufouteaWeawear_23; }
	inline uint32_t* get_address_of__moufouteaWeawear_23() { return &____moufouteaWeawear_23; }
	inline void set__moufouteaWeawear_23(uint32_t value)
	{
		____moufouteaWeawear_23 = value;
	}

	inline static int32_t get_offset_of__leras_24() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____leras_24)); }
	inline bool get__leras_24() const { return ____leras_24; }
	inline bool* get_address_of__leras_24() { return &____leras_24; }
	inline void set__leras_24(bool value)
	{
		____leras_24 = value;
	}

	inline static int32_t get_offset_of__jechasDresar_25() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____jechasDresar_25)); }
	inline bool get__jechasDresar_25() const { return ____jechasDresar_25; }
	inline bool* get_address_of__jechasDresar_25() { return &____jechasDresar_25; }
	inline void set__jechasDresar_25(bool value)
	{
		____jechasDresar_25 = value;
	}

	inline static int32_t get_offset_of__canoo_26() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____canoo_26)); }
	inline bool get__canoo_26() const { return ____canoo_26; }
	inline bool* get_address_of__canoo_26() { return &____canoo_26; }
	inline void set__canoo_26(bool value)
	{
		____canoo_26 = value;
	}

	inline static int32_t get_offset_of__jukahi_27() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____jukahi_27)); }
	inline uint32_t get__jukahi_27() const { return ____jukahi_27; }
	inline uint32_t* get_address_of__jukahi_27() { return &____jukahi_27; }
	inline void set__jukahi_27(uint32_t value)
	{
		____jukahi_27 = value;
	}

	inline static int32_t get_offset_of__mouseafi_28() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____mouseafi_28)); }
	inline int32_t get__mouseafi_28() const { return ____mouseafi_28; }
	inline int32_t* get_address_of__mouseafi_28() { return &____mouseafi_28; }
	inline void set__mouseafi_28(int32_t value)
	{
		____mouseafi_28 = value;
	}

	inline static int32_t get_offset_of__terbouTadisee_29() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____terbouTadisee_29)); }
	inline String_t* get__terbouTadisee_29() const { return ____terbouTadisee_29; }
	inline String_t** get_address_of__terbouTadisee_29() { return &____terbouTadisee_29; }
	inline void set__terbouTadisee_29(String_t* value)
	{
		____terbouTadisee_29 = value;
		Il2CppCodeGenWriteBarrier(&____terbouTadisee_29, value);
	}

	inline static int32_t get_offset_of__korreDrerdri_30() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____korreDrerdri_30)); }
	inline String_t* get__korreDrerdri_30() const { return ____korreDrerdri_30; }
	inline String_t** get_address_of__korreDrerdri_30() { return &____korreDrerdri_30; }
	inline void set__korreDrerdri_30(String_t* value)
	{
		____korreDrerdri_30 = value;
		Il2CppCodeGenWriteBarrier(&____korreDrerdri_30, value);
	}

	inline static int32_t get_offset_of__jelpikerChemdowbel_31() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____jelpikerChemdowbel_31)); }
	inline bool get__jelpikerChemdowbel_31() const { return ____jelpikerChemdowbel_31; }
	inline bool* get_address_of__jelpikerChemdowbel_31() { return &____jelpikerChemdowbel_31; }
	inline void set__jelpikerChemdowbel_31(bool value)
	{
		____jelpikerChemdowbel_31 = value;
	}

	inline static int32_t get_offset_of__quje_32() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____quje_32)); }
	inline bool get__quje_32() const { return ____quje_32; }
	inline bool* get_address_of__quje_32() { return &____quje_32; }
	inline void set__quje_32(bool value)
	{
		____quje_32 = value;
	}

	inline static int32_t get_offset_of__basga_33() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____basga_33)); }
	inline String_t* get__basga_33() const { return ____basga_33; }
	inline String_t** get_address_of__basga_33() { return &____basga_33; }
	inline void set__basga_33(String_t* value)
	{
		____basga_33 = value;
		Il2CppCodeGenWriteBarrier(&____basga_33, value);
	}

	inline static int32_t get_offset_of__hisxoSeakelmair_34() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____hisxoSeakelmair_34)); }
	inline String_t* get__hisxoSeakelmair_34() const { return ____hisxoSeakelmair_34; }
	inline String_t** get_address_of__hisxoSeakelmair_34() { return &____hisxoSeakelmair_34; }
	inline void set__hisxoSeakelmair_34(String_t* value)
	{
		____hisxoSeakelmair_34 = value;
		Il2CppCodeGenWriteBarrier(&____hisxoSeakelmair_34, value);
	}

	inline static int32_t get_offset_of__nersasVelchir_35() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____nersasVelchir_35)); }
	inline bool get__nersasVelchir_35() const { return ____nersasVelchir_35; }
	inline bool* get_address_of__nersasVelchir_35() { return &____nersasVelchir_35; }
	inline void set__nersasVelchir_35(bool value)
	{
		____nersasVelchir_35 = value;
	}

	inline static int32_t get_offset_of__lajearyorPabou_36() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____lajearyorPabou_36)); }
	inline String_t* get__lajearyorPabou_36() const { return ____lajearyorPabou_36; }
	inline String_t** get_address_of__lajearyorPabou_36() { return &____lajearyorPabou_36; }
	inline void set__lajearyorPabou_36(String_t* value)
	{
		____lajearyorPabou_36 = value;
		Il2CppCodeGenWriteBarrier(&____lajearyorPabou_36, value);
	}

	inline static int32_t get_offset_of__nalmi_37() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____nalmi_37)); }
	inline uint32_t get__nalmi_37() const { return ____nalmi_37; }
	inline uint32_t* get_address_of__nalmi_37() { return &____nalmi_37; }
	inline void set__nalmi_37(uint32_t value)
	{
		____nalmi_37 = value;
	}

	inline static int32_t get_offset_of__catouChuwehou_38() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____catouChuwehou_38)); }
	inline uint32_t get__catouChuwehou_38() const { return ____catouChuwehou_38; }
	inline uint32_t* get_address_of__catouChuwehou_38() { return &____catouChuwehou_38; }
	inline void set__catouChuwehou_38(uint32_t value)
	{
		____catouChuwehou_38 = value;
	}

	inline static int32_t get_offset_of__wevawhorHearzu_39() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____wevawhorHearzu_39)); }
	inline int32_t get__wevawhorHearzu_39() const { return ____wevawhorHearzu_39; }
	inline int32_t* get_address_of__wevawhorHearzu_39() { return &____wevawhorHearzu_39; }
	inline void set__wevawhorHearzu_39(int32_t value)
	{
		____wevawhorHearzu_39 = value;
	}

	inline static int32_t get_offset_of__makairKeatoo_40() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____makairKeatoo_40)); }
	inline String_t* get__makairKeatoo_40() const { return ____makairKeatoo_40; }
	inline String_t** get_address_of__makairKeatoo_40() { return &____makairKeatoo_40; }
	inline void set__makairKeatoo_40(String_t* value)
	{
		____makairKeatoo_40 = value;
		Il2CppCodeGenWriteBarrier(&____makairKeatoo_40, value);
	}

	inline static int32_t get_offset_of__nekeHeeci_41() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____nekeHeeci_41)); }
	inline bool get__nekeHeeci_41() const { return ____nekeHeeci_41; }
	inline bool* get_address_of__nekeHeeci_41() { return &____nekeHeeci_41; }
	inline void set__nekeHeeci_41(bool value)
	{
		____nekeHeeci_41 = value;
	}

	inline static int32_t get_offset_of__leerameChase_42() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____leerameChase_42)); }
	inline int32_t get__leerameChase_42() const { return ____leerameChase_42; }
	inline int32_t* get_address_of__leerameChase_42() { return &____leerameChase_42; }
	inline void set__leerameChase_42(int32_t value)
	{
		____leerameChase_42 = value;
	}

	inline static int32_t get_offset_of__trabeWirtowtea_43() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____trabeWirtowtea_43)); }
	inline float get__trabeWirtowtea_43() const { return ____trabeWirtowtea_43; }
	inline float* get_address_of__trabeWirtowtea_43() { return &____trabeWirtowtea_43; }
	inline void set__trabeWirtowtea_43(float value)
	{
		____trabeWirtowtea_43 = value;
	}

	inline static int32_t get_offset_of__nastirqe_44() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____nastirqe_44)); }
	inline int32_t get__nastirqe_44() const { return ____nastirqe_44; }
	inline int32_t* get_address_of__nastirqe_44() { return &____nastirqe_44; }
	inline void set__nastirqe_44(int32_t value)
	{
		____nastirqe_44 = value;
	}

	inline static int32_t get_offset_of__hemiBoubefis_45() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____hemiBoubefis_45)); }
	inline uint32_t get__hemiBoubefis_45() const { return ____hemiBoubefis_45; }
	inline uint32_t* get_address_of__hemiBoubefis_45() { return &____hemiBoubefis_45; }
	inline void set__hemiBoubefis_45(uint32_t value)
	{
		____hemiBoubefis_45 = value;
	}

	inline static int32_t get_offset_of__dahisjeeFouqu_46() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____dahisjeeFouqu_46)); }
	inline String_t* get__dahisjeeFouqu_46() const { return ____dahisjeeFouqu_46; }
	inline String_t** get_address_of__dahisjeeFouqu_46() { return &____dahisjeeFouqu_46; }
	inline void set__dahisjeeFouqu_46(String_t* value)
	{
		____dahisjeeFouqu_46 = value;
		Il2CppCodeGenWriteBarrier(&____dahisjeeFouqu_46, value);
	}

	inline static int32_t get_offset_of__biwhou_47() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____biwhou_47)); }
	inline bool get__biwhou_47() const { return ____biwhou_47; }
	inline bool* get_address_of__biwhou_47() { return &____biwhou_47; }
	inline void set__biwhou_47(bool value)
	{
		____biwhou_47 = value;
	}

	inline static int32_t get_offset_of__halarbayStewheechas_48() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____halarbayStewheechas_48)); }
	inline String_t* get__halarbayStewheechas_48() const { return ____halarbayStewheechas_48; }
	inline String_t** get_address_of__halarbayStewheechas_48() { return &____halarbayStewheechas_48; }
	inline void set__halarbayStewheechas_48(String_t* value)
	{
		____halarbayStewheechas_48 = value;
		Il2CppCodeGenWriteBarrier(&____halarbayStewheechas_48, value);
	}

	inline static int32_t get_offset_of__pairailea_49() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____pairailea_49)); }
	inline float get__pairailea_49() const { return ____pairailea_49; }
	inline float* get_address_of__pairailea_49() { return &____pairailea_49; }
	inline void set__pairailea_49(float value)
	{
		____pairailea_49 = value;
	}

	inline static int32_t get_offset_of__peafoudrur_50() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____peafoudrur_50)); }
	inline int32_t get__peafoudrur_50() const { return ____peafoudrur_50; }
	inline int32_t* get_address_of__peafoudrur_50() { return &____peafoudrur_50; }
	inline void set__peafoudrur_50(int32_t value)
	{
		____peafoudrur_50 = value;
	}

	inline static int32_t get_offset_of__maqoupou_51() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____maqoupou_51)); }
	inline String_t* get__maqoupou_51() const { return ____maqoupou_51; }
	inline String_t** get_address_of__maqoupou_51() { return &____maqoupou_51; }
	inline void set__maqoupou_51(String_t* value)
	{
		____maqoupou_51 = value;
		Il2CppCodeGenWriteBarrier(&____maqoupou_51, value);
	}

	inline static int32_t get_offset_of__boodeNasbisdis_52() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____boodeNasbisdis_52)); }
	inline String_t* get__boodeNasbisdis_52() const { return ____boodeNasbisdis_52; }
	inline String_t** get_address_of__boodeNasbisdis_52() { return &____boodeNasbisdis_52; }
	inline void set__boodeNasbisdis_52(String_t* value)
	{
		____boodeNasbisdis_52 = value;
		Il2CppCodeGenWriteBarrier(&____boodeNasbisdis_52, value);
	}

	inline static int32_t get_offset_of__weekairwha_53() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____weekairwha_53)); }
	inline bool get__weekairwha_53() const { return ____weekairwha_53; }
	inline bool* get_address_of__weekairwha_53() { return &____weekairwha_53; }
	inline void set__weekairwha_53(bool value)
	{
		____weekairwha_53 = value;
	}

	inline static int32_t get_offset_of__toorowtiFesacear_54() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____toorowtiFesacear_54)); }
	inline String_t* get__toorowtiFesacear_54() const { return ____toorowtiFesacear_54; }
	inline String_t** get_address_of__toorowtiFesacear_54() { return &____toorowtiFesacear_54; }
	inline void set__toorowtiFesacear_54(String_t* value)
	{
		____toorowtiFesacear_54 = value;
		Il2CppCodeGenWriteBarrier(&____toorowtiFesacear_54, value);
	}

	inline static int32_t get_offset_of__keaseLirer_55() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____keaseLirer_55)); }
	inline bool get__keaseLirer_55() const { return ____keaseLirer_55; }
	inline bool* get_address_of__keaseLirer_55() { return &____keaseLirer_55; }
	inline void set__keaseLirer_55(bool value)
	{
		____keaseLirer_55 = value;
	}

	inline static int32_t get_offset_of__votitairDroser_56() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____votitairDroser_56)); }
	inline int32_t get__votitairDroser_56() const { return ____votitairDroser_56; }
	inline int32_t* get_address_of__votitairDroser_56() { return &____votitairDroser_56; }
	inline void set__votitairDroser_56(int32_t value)
	{
		____votitairDroser_56 = value;
	}

	inline static int32_t get_offset_of__semtri_57() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____semtri_57)); }
	inline float get__semtri_57() const { return ____semtri_57; }
	inline float* get_address_of__semtri_57() { return &____semtri_57; }
	inline void set__semtri_57(float value)
	{
		____semtri_57 = value;
	}

	inline static int32_t get_offset_of__duhuDrebal_58() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____duhuDrebal_58)); }
	inline int32_t get__duhuDrebal_58() const { return ____duhuDrebal_58; }
	inline int32_t* get_address_of__duhuDrebal_58() { return &____duhuDrebal_58; }
	inline void set__duhuDrebal_58(int32_t value)
	{
		____duhuDrebal_58 = value;
	}

	inline static int32_t get_offset_of__lejoCowmay_59() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____lejoCowmay_59)); }
	inline float get__lejoCowmay_59() const { return ____lejoCowmay_59; }
	inline float* get_address_of__lejoCowmay_59() { return &____lejoCowmay_59; }
	inline void set__lejoCowmay_59(float value)
	{
		____lejoCowmay_59 = value;
	}

	inline static int32_t get_offset_of__soonasManejall_60() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____soonasManejall_60)); }
	inline uint32_t get__soonasManejall_60() const { return ____soonasManejall_60; }
	inline uint32_t* get_address_of__soonasManejall_60() { return &____soonasManejall_60; }
	inline void set__soonasManejall_60(uint32_t value)
	{
		____soonasManejall_60 = value;
	}

	inline static int32_t get_offset_of__jesidoKeasar_61() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____jesidoKeasar_61)); }
	inline float get__jesidoKeasar_61() const { return ____jesidoKeasar_61; }
	inline float* get_address_of__jesidoKeasar_61() { return &____jesidoKeasar_61; }
	inline void set__jesidoKeasar_61(float value)
	{
		____jesidoKeasar_61 = value;
	}

	inline static int32_t get_offset_of__howdorjir_62() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____howdorjir_62)); }
	inline bool get__howdorjir_62() const { return ____howdorjir_62; }
	inline bool* get_address_of__howdorjir_62() { return &____howdorjir_62; }
	inline void set__howdorjir_62(bool value)
	{
		____howdorjir_62 = value;
	}

	inline static int32_t get_offset_of__teferPejairfi_63() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____teferPejairfi_63)); }
	inline bool get__teferPejairfi_63() const { return ____teferPejairfi_63; }
	inline bool* get_address_of__teferPejairfi_63() { return &____teferPejairfi_63; }
	inline void set__teferPejairfi_63(bool value)
	{
		____teferPejairfi_63 = value;
	}

	inline static int32_t get_offset_of__makido_64() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____makido_64)); }
	inline String_t* get__makido_64() const { return ____makido_64; }
	inline String_t** get_address_of__makido_64() { return &____makido_64; }
	inline void set__makido_64(String_t* value)
	{
		____makido_64 = value;
		Il2CppCodeGenWriteBarrier(&____makido_64, value);
	}

	inline static int32_t get_offset_of__lelkaStiscee_65() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____lelkaStiscee_65)); }
	inline bool get__lelkaStiscee_65() const { return ____lelkaStiscee_65; }
	inline bool* get_address_of__lelkaStiscee_65() { return &____lelkaStiscee_65; }
	inline void set__lelkaStiscee_65(bool value)
	{
		____lelkaStiscee_65 = value;
	}

	inline static int32_t get_offset_of__poujasmowNemtownas_66() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____poujasmowNemtownas_66)); }
	inline bool get__poujasmowNemtownas_66() const { return ____poujasmowNemtownas_66; }
	inline bool* get_address_of__poujasmowNemtownas_66() { return &____poujasmowNemtownas_66; }
	inline void set__poujasmowNemtownas_66(bool value)
	{
		____poujasmowNemtownas_66 = value;
	}

	inline static int32_t get_offset_of__bemmuseaJagaw_67() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____bemmuseaJagaw_67)); }
	inline uint32_t get__bemmuseaJagaw_67() const { return ____bemmuseaJagaw_67; }
	inline uint32_t* get_address_of__bemmuseaJagaw_67() { return &____bemmuseaJagaw_67; }
	inline void set__bemmuseaJagaw_67(uint32_t value)
	{
		____bemmuseaJagaw_67 = value;
	}

	inline static int32_t get_offset_of__gajouCukarsur_68() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____gajouCukarsur_68)); }
	inline bool get__gajouCukarsur_68() const { return ____gajouCukarsur_68; }
	inline bool* get_address_of__gajouCukarsur_68() { return &____gajouCukarsur_68; }
	inline void set__gajouCukarsur_68(bool value)
	{
		____gajouCukarsur_68 = value;
	}

	inline static int32_t get_offset_of__maysaDeatrerecai_69() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____maysaDeatrerecai_69)); }
	inline int32_t get__maysaDeatrerecai_69() const { return ____maysaDeatrerecai_69; }
	inline int32_t* get_address_of__maysaDeatrerecai_69() { return &____maysaDeatrerecai_69; }
	inline void set__maysaDeatrerecai_69(int32_t value)
	{
		____maysaDeatrerecai_69 = value;
	}

	inline static int32_t get_offset_of__peceme_70() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____peceme_70)); }
	inline String_t* get__peceme_70() const { return ____peceme_70; }
	inline String_t** get_address_of__peceme_70() { return &____peceme_70; }
	inline void set__peceme_70(String_t* value)
	{
		____peceme_70 = value;
		Il2CppCodeGenWriteBarrier(&____peceme_70, value);
	}

	inline static int32_t get_offset_of__rawhearNuho_71() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____rawhearNuho_71)); }
	inline bool get__rawhearNuho_71() const { return ____rawhearNuho_71; }
	inline bool* get_address_of__rawhearNuho_71() { return &____rawhearNuho_71; }
	inline void set__rawhearNuho_71(bool value)
	{
		____rawhearNuho_71 = value;
	}

	inline static int32_t get_offset_of__lemnuCecemow_72() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____lemnuCecemow_72)); }
	inline float get__lemnuCecemow_72() const { return ____lemnuCecemow_72; }
	inline float* get_address_of__lemnuCecemow_72() { return &____lemnuCecemow_72; }
	inline void set__lemnuCecemow_72(float value)
	{
		____lemnuCecemow_72 = value;
	}

	inline static int32_t get_offset_of__vemci_73() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____vemci_73)); }
	inline float get__vemci_73() const { return ____vemci_73; }
	inline float* get_address_of__vemci_73() { return &____vemci_73; }
	inline void set__vemci_73(float value)
	{
		____vemci_73 = value;
	}

	inline static int32_t get_offset_of__setrasstereHisyuvar_74() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____setrasstereHisyuvar_74)); }
	inline uint32_t get__setrasstereHisyuvar_74() const { return ____setrasstereHisyuvar_74; }
	inline uint32_t* get_address_of__setrasstereHisyuvar_74() { return &____setrasstereHisyuvar_74; }
	inline void set__setrasstereHisyuvar_74(uint32_t value)
	{
		____setrasstereHisyuvar_74 = value;
	}

	inline static int32_t get_offset_of__whawtonow_75() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____whawtonow_75)); }
	inline bool get__whawtonow_75() const { return ____whawtonow_75; }
	inline bool* get_address_of__whawtonow_75() { return &____whawtonow_75; }
	inline void set__whawtonow_75(bool value)
	{
		____whawtonow_75 = value;
	}

	inline static int32_t get_offset_of__mallnejeSevargas_76() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____mallnejeSevargas_76)); }
	inline int32_t get__mallnejeSevargas_76() const { return ____mallnejeSevargas_76; }
	inline int32_t* get_address_of__mallnejeSevargas_76() { return &____mallnejeSevargas_76; }
	inline void set__mallnejeSevargas_76(int32_t value)
	{
		____mallnejeSevargas_76 = value;
	}

	inline static int32_t get_offset_of__dallze_77() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____dallze_77)); }
	inline uint32_t get__dallze_77() const { return ____dallze_77; }
	inline uint32_t* get_address_of__dallze_77() { return &____dallze_77; }
	inline void set__dallze_77(uint32_t value)
	{
		____dallze_77 = value;
	}

	inline static int32_t get_offset_of__becha_78() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____becha_78)); }
	inline float get__becha_78() const { return ____becha_78; }
	inline float* get_address_of__becha_78() { return &____becha_78; }
	inline void set__becha_78(float value)
	{
		____becha_78 = value;
	}

	inline static int32_t get_offset_of__dralre_79() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____dralre_79)); }
	inline uint32_t get__dralre_79() const { return ____dralre_79; }
	inline uint32_t* get_address_of__dralre_79() { return &____dralre_79; }
	inline void set__dralre_79(uint32_t value)
	{
		____dralre_79 = value;
	}

	inline static int32_t get_offset_of__chawpaw_80() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____chawpaw_80)); }
	inline String_t* get__chawpaw_80() const { return ____chawpaw_80; }
	inline String_t** get_address_of__chawpaw_80() { return &____chawpaw_80; }
	inline void set__chawpaw_80(String_t* value)
	{
		____chawpaw_80 = value;
		Il2CppCodeGenWriteBarrier(&____chawpaw_80, value);
	}

	inline static int32_t get_offset_of__darnou_81() { return static_cast<int32_t>(offsetof(M_gaiyis16_t747189417, ____darnou_81)); }
	inline bool get__darnou_81() const { return ____darnou_81; }
	inline bool* get_address_of__darnou_81() { return &____darnou_81; }
	inline void set__darnou_81(bool value)
	{
		____darnou_81 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
