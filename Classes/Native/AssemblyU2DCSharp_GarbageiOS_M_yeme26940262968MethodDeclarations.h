﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_yeme26
struct M_yeme26_t940262968;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_yeme26::.ctor()
extern "C"  void M_yeme26__ctor_m1627076859 (M_yeme26_t940262968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_yeme26::M_rerhi0(System.String[],System.Int32)
extern "C"  void M_yeme26_M_rerhi0_m2936972162 (M_yeme26_t940262968 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
