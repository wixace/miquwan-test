﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Transform
struct Transform_t1659122786;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"

// System.Void TransformExtension::ChangeParent(UnityEngine.Transform,UnityEngine.Transform,System.Boolean)
extern "C"  void TransformExtension_ChangeParent_m1410869023 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ____this0, Transform_t1659122786 * ___parent1, bool ___resetMat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
