﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t4229224207;
// System.Xml.XmlNameTable
struct XmlNameTable_t1216706026;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_t4125473774;
// System.Object
struct Il2CppObject;
// System.Xml.Schema.ValidationEventArgs
struct ValidationEventArgs_t3260433748;
// System.Xml.XmlResolver
struct XmlResolver_t3822670287;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_ConformanceLevel233510445.h"
#include "System_Xml_System_Xml_XmlNameTable1216706026.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSet4125473774.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Xml_System_Xml_Schema_ValidationEventArgs3260433748.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidationFlag87720602.h"
#include "System_Xml_System_Xml_ValidationType2719018513.h"
#include "System_Xml_System_Xml_XmlResolver3822670287.h"

// System.Void System.Xml.XmlReaderSettings::.ctor()
extern "C"  void XmlReaderSettings__ctor_m3908638334 (XmlReaderSettings_t4229224207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlReaderSettings System.Xml.XmlReaderSettings::Clone()
extern "C"  XmlReaderSettings_t4229224207 * XmlReaderSettings_Clone_m1457622607 (XmlReaderSettings_t4229224207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlReaderSettings::Reset()
extern "C"  void XmlReaderSettings_Reset_m1555071275 (XmlReaderSettings_t4229224207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlReaderSettings::get_CheckCharacters()
extern "C"  bool XmlReaderSettings_get_CheckCharacters_m3292028349 (XmlReaderSettings_t4229224207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlReaderSettings::get_CloseInput()
extern "C"  bool XmlReaderSettings_get_CloseInput_m1751600969 (XmlReaderSettings_t4229224207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.ConformanceLevel System.Xml.XmlReaderSettings::get_ConformanceLevel()
extern "C"  int32_t XmlReaderSettings_get_ConformanceLevel_m1983953036 (XmlReaderSettings_t4229224207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlReaderSettings::set_ConformanceLevel(System.Xml.ConformanceLevel)
extern "C"  void XmlReaderSettings_set_ConformanceLevel_m3126952379 (XmlReaderSettings_t4229224207 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlReaderSettings::get_IgnoreComments()
extern "C"  bool XmlReaderSettings_get_IgnoreComments_m4053311613 (XmlReaderSettings_t4229224207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlReaderSettings::get_IgnoreProcessingInstructions()
extern "C"  bool XmlReaderSettings_get_IgnoreProcessingInstructions_m1222430689 (XmlReaderSettings_t4229224207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlReaderSettings::get_IgnoreWhitespace()
extern "C"  bool XmlReaderSettings_get_IgnoreWhitespace_m3004537030 (XmlReaderSettings_t4229224207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlReaderSettings::get_ProhibitDtd()
extern "C"  bool XmlReaderSettings_get_ProhibitDtd_m3789401056 (XmlReaderSettings_t4229224207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlReaderSettings::set_NameTable(System.Xml.XmlNameTable)
extern "C"  void XmlReaderSettings_set_NameTable_m2797147342 (XmlReaderSettings_t4229224207 * __this, XmlNameTable_t1216706026 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaSet System.Xml.XmlReaderSettings::get_Schemas()
extern "C"  XmlSchemaSet_t4125473774 * XmlReaderSettings_get_Schemas_m3388445813 (XmlReaderSettings_t4229224207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlReaderSettings::set_Schemas(System.Xml.Schema.XmlSchemaSet)
extern "C"  void XmlReaderSettings_set_Schemas_m1160491920 (XmlReaderSettings_t4229224207 * __this, XmlSchemaSet_t4125473774 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlReaderSettings::OnValidationError(System.Object,System.Xml.Schema.ValidationEventArgs)
extern "C"  void XmlReaderSettings_OnValidationError_m4048558957 (XmlReaderSettings_t4229224207 * __this, Il2CppObject * ___o0, ValidationEventArgs_t3260433748 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaValidationFlags System.Xml.XmlReaderSettings::get_ValidationFlags()
extern "C"  int32_t XmlReaderSettings_get_ValidationFlags_m3016379709 (XmlReaderSettings_t4229224207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlReaderSettings::set_ValidationFlags(System.Xml.Schema.XmlSchemaValidationFlags)
extern "C"  void XmlReaderSettings_set_ValidationFlags_m3906140416 (XmlReaderSettings_t4229224207 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.ValidationType System.Xml.XmlReaderSettings::get_ValidationType()
extern "C"  int32_t XmlReaderSettings_get_ValidationType_m2966142668 (XmlReaderSettings_t4229224207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlReaderSettings::set_ValidationType(System.Xml.ValidationType)
extern "C"  void XmlReaderSettings_set_ValidationType_m251262643 (XmlReaderSettings_t4229224207 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlResolver System.Xml.XmlReaderSettings::get_XmlResolver()
extern "C"  XmlResolver_t3822670287 * XmlReaderSettings_get_XmlResolver_m2944862022 (XmlReaderSettings_t4229224207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlReaderSettings::set_XmlResolver(System.Xml.XmlResolver)
extern "C"  void XmlReaderSettings_set_XmlResolver_m2531183445 (XmlReaderSettings_t4229224207 * __this, XmlResolver_t3822670287 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
