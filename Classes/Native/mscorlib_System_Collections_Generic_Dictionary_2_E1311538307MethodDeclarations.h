﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Single>
struct Dictionary_2_t4289182211;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1311538307.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24187962917.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1911532825_gshared (Enumerator_t1311538307 * __this, Dictionary_2_t4289182211 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1911532825(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1311538307 *, Dictionary_2_t4289182211 *, const MethodInfo*))Enumerator__ctor_m1911532825_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1042733554_gshared (Enumerator_t1311538307 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1042733554(__this, method) ((  Il2CppObject * (*) (Enumerator_t1311538307 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1042733554_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m542885372_gshared (Enumerator_t1311538307 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m542885372(__this, method) ((  void (*) (Enumerator_t1311538307 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m542885372_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1103041459_gshared (Enumerator_t1311538307 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1103041459(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1311538307 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1103041459_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2165528910_gshared (Enumerator_t1311538307 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2165528910(__this, method) ((  Il2CppObject * (*) (Enumerator_t1311538307 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2165528910_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3357547616_gshared (Enumerator_t1311538307 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3357547616(__this, method) ((  Il2CppObject * (*) (Enumerator_t1311538307 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3357547616_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3515215276_gshared (Enumerator_t1311538307 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3515215276(__this, method) ((  bool (*) (Enumerator_t1311538307 *, const MethodInfo*))Enumerator_MoveNext_m3515215276_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::get_Current()
extern "C"  KeyValuePair_2_t4187962917  Enumerator_get_Current_m784259984_gshared (Enumerator_t1311538307 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m784259984(__this, method) ((  KeyValuePair_2_t4187962917  (*) (Enumerator_t1311538307 *, const MethodInfo*))Enumerator_get_Current_m784259984_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m1926980405_gshared (Enumerator_t1311538307 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1926980405(__this, method) ((  int32_t (*) (Enumerator_t1311538307 *, const MethodInfo*))Enumerator_get_CurrentKey_m1926980405_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::get_CurrentValue()
extern "C"  float Enumerator_get_CurrentValue_m2124609589_gshared (Enumerator_t1311538307 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m2124609589(__this, method) ((  float (*) (Enumerator_t1311538307 *, const MethodInfo*))Enumerator_get_CurrentValue_m2124609589_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::Reset()
extern "C"  void Enumerator_Reset_m738046443_gshared (Enumerator_t1311538307 * __this, const MethodInfo* method);
#define Enumerator_Reset_m738046443(__this, method) ((  void (*) (Enumerator_t1311538307 *, const MethodInfo*))Enumerator_Reset_m738046443_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1788913076_gshared (Enumerator_t1311538307 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1788913076(__this, method) ((  void (*) (Enumerator_t1311538307 *, const MethodInfo*))Enumerator_VerifyState_m1788913076_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m3903419420_gshared (Enumerator_t1311538307 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m3903419420(__this, method) ((  void (*) (Enumerator_t1311538307 *, const MethodInfo*))Enumerator_VerifyCurrent_m3903419420_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Single>::Dispose()
extern "C"  void Enumerator_Dispose_m3116456955_gshared (Enumerator_t1311538307 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3116456955(__this, method) ((  void (*) (Enumerator_t1311538307 *, const MethodInfo*))Enumerator_Dispose_m3116456955_gshared)(__this, method)
