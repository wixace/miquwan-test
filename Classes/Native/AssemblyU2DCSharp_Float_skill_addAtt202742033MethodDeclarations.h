﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_skill_addAtt
struct Float_skill_addAtt_t202742033;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// FloatTextUnit
struct FloatTextUnit_t2362298029;
// EventDelegate/Callback
struct Callback_t1094463061;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_UITweener_Method1078127180.h"

// System.Void Float_skill_addAtt::.ctor()
extern "C"  void Float_skill_addAtt__ctor_m2972519082 (Float_skill_addAtt_t202742033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_skill_addAtt::FloatID()
extern "C"  int32_t Float_skill_addAtt_FloatID_m1080559030 (Float_skill_addAtt_t202742033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_skill_addAtt::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_skill_addAtt_OnAwake_m2069106790 (Float_skill_addAtt_t202742033 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_skill_addAtt::OnDestroy()
extern "C"  void Float_skill_addAtt_OnDestroy_m2026207907 (Float_skill_addAtt_t202742033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_skill_addAtt::Init()
extern "C"  void Float_skill_addAtt_Init_m878708330 (Float_skill_addAtt_t202742033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_skill_addAtt::SetFile(System.Object[])
extern "C"  void Float_skill_addAtt_SetFile_m652541804 (Float_skill_addAtt_t202742033 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_skill_addAtt::ilo_MovePosition1(FloatTextUnit,UnityEngine.Vector3,System.Single,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void Float_skill_addAtt_ilo_MovePosition1_m1149452370 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, Vector3_t4282066566  ___from1, float ___toUp2, float ___duration3, float ___delay4, Callback_t1094463061 * ___call5, GameObject_t3674682005 * ___go6, int32_t ___method7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
