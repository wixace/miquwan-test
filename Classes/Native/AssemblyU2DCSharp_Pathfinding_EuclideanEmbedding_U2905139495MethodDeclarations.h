﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.EuclideanEmbedding/<RecalculatePivots>c__AnonStorey11D
struct U3CRecalculatePivotsU3Ec__AnonStorey11D_t2905139495;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.EuclideanEmbedding/<RecalculatePivots>c__AnonStorey11D::.ctor()
extern "C"  void U3CRecalculatePivotsU3Ec__AnonStorey11D__ctor_m2050614100 (U3CRecalculatePivotsU3Ec__AnonStorey11D_t2905139495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.EuclideanEmbedding/<RecalculatePivots>c__AnonStorey11D::<>m__351(Pathfinding.GraphNode)
extern "C"  bool U3CRecalculatePivotsU3Ec__AnonStorey11D_U3CU3Em__351_m1999477124 (U3CRecalculatePivotsU3Ec__AnonStorey11D_t2905139495 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
