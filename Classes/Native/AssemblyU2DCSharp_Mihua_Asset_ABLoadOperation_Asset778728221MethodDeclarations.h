﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;

#include "codegen/il2cpp-codegen.h"

// System.Void Mihua.Asset.ABLoadOperation.AssetOperation::.ctor()
extern "C"  void AssetOperation__ctor_m3758396354 (AssetOperation_t778728221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.ABLoadOperation.AssetOperation::Init()
extern "C"  void AssetOperation_Init_m1181153874 (AssetOperation_t778728221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
