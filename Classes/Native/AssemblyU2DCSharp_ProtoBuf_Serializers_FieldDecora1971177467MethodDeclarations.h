﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.FieldDecorator
struct FieldDecorator_t1971177467;
// System.Type
struct Type_t;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// ProtoBuf.Serializers.IProtoSerializer
struct IProtoSerializer_t3033312651;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"

// System.Void ProtoBuf.Serializers.FieldDecorator::.ctor(System.Type,System.Reflection.FieldInfo,ProtoBuf.Serializers.IProtoSerializer)
extern "C"  void FieldDecorator__ctor_m16718994 (FieldDecorator_t1971177467 * __this, Type_t * ___forType0, FieldInfo_t * ___field1, Il2CppObject * ___tail2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.FieldDecorator::get_ExpectedType()
extern "C"  Type_t * FieldDecorator_get_ExpectedType_m558088331 (FieldDecorator_t1971177467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.FieldDecorator::get_RequiresOldValue()
extern "C"  bool FieldDecorator_get_RequiresOldValue_m3411169911 (FieldDecorator_t1971177467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.FieldDecorator::get_ReturnsValue()
extern "C"  bool FieldDecorator_get_ReturnsValue_m705697933 (FieldDecorator_t1971177467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.FieldDecorator::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void FieldDecorator_Write_m1425575985 (FieldDecorator_t1971177467 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.FieldDecorator::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * FieldDecorator_Read_m878633845 (FieldDecorator_t1971177467 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.FieldDecorator::ilo_Read1(ProtoBuf.Serializers.IProtoSerializer,System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * FieldDecorator_ilo_Read1_m2426474548 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___value1, ProtoReader_t3962509489 * ___source2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
