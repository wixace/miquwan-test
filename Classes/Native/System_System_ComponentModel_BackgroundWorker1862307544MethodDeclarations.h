﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.BackgroundWorker
struct BackgroundWorker_t1862307544;

#include "codegen/il2cpp-codegen.h"

// System.Boolean System.ComponentModel.BackgroundWorker::get_CancellationPending()
extern "C"  bool BackgroundWorker_get_CancellationPending_m543184704 (BackgroundWorker_t1862307544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
