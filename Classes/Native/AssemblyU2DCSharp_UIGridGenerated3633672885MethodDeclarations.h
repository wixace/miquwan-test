﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIGridGenerated
struct UIGridGenerated_t3633672885;
// JSVCall
struct JSVCall_t3708497963;
// UIGrid/OnReposition
struct OnReposition_t1310478256;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.Comparison`1<UnityEngine.Transform>
struct Comparison_1_t375483973;
// System.Delegate
struct Delegate_t3310234105;
// UIGrid
struct UIGrid_t2503122938;
// UnityEngine.Transform
struct Transform_t1659122786;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "AssemblyU2DCSharp_UIGrid2503122938.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UIGridGenerated::.ctor()
extern "C"  void UIGridGenerated__ctor_m1031770198 (UIGridGenerated_t3633672885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGridGenerated::UIGrid_UIGrid1(JSVCall,System.Int32)
extern "C"  bool UIGridGenerated_UIGrid_UIGrid1_m1856938460 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::UIGrid_arrangement(JSVCall)
extern "C"  void UIGridGenerated_UIGrid_arrangement_m2040062452 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::UIGrid_sorting(JSVCall)
extern "C"  void UIGridGenerated_UIGrid_sorting_m1375409338 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::UIGrid_pivot(JSVCall)
extern "C"  void UIGridGenerated_UIGrid_pivot_m3070099900 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::UIGrid_maxPerLine(JSVCall)
extern "C"  void UIGridGenerated_UIGrid_maxPerLine_m4215927873 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::UIGrid_cellWidth(JSVCall)
extern "C"  void UIGridGenerated_UIGrid_cellWidth_m3725687482 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::UIGrid_cellHeight(JSVCall)
extern "C"  void UIGridGenerated_UIGrid_cellHeight_m187638597 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::UIGrid_animateSmoothly(JSVCall)
extern "C"  void UIGridGenerated_UIGrid_animateSmoothly_m3940449154 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::UIGrid_hideInactive(JSVCall)
extern "C"  void UIGridGenerated_UIGrid_hideInactive_m1658520353 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::UIGrid_keepWithinPanel(JSVCall)
extern "C"  void UIGridGenerated_UIGrid_keepWithinPanel_m959511722 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIGrid/OnReposition UIGridGenerated::UIGrid_onReposition_GetDelegate_member9_arg0(CSRepresentedObject)
extern "C"  OnReposition_t1310478256 * UIGridGenerated_UIGrid_onReposition_GetDelegate_member9_arg0_m3676868180 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::UIGrid_onReposition(JSVCall)
extern "C"  void UIGridGenerated_UIGrid_onReposition_m396296979 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Comparison`1<UnityEngine.Transform> UIGridGenerated::UIGrid_onCustomSort_GetDelegate_member10_arg0(CSRepresentedObject)
extern "C"  Comparison_1_t375483973 * UIGridGenerated_UIGrid_onCustomSort_GetDelegate_member10_arg0_m437411399 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::UIGrid_onCustomSort(JSVCall)
extern "C"  void UIGridGenerated_UIGrid_onCustomSort_m3041828896 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::UIGrid_repositionNow(JSVCall)
extern "C"  void UIGridGenerated_UIGrid_repositionNow_m3628859428 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGridGenerated::UIGrid_AddChild__Transform__Boolean(JSVCall,System.Int32)
extern "C"  bool UIGridGenerated_UIGrid_AddChild__Transform__Boolean_m1587376532 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGridGenerated::UIGrid_AddChild__Transform(JSVCall,System.Int32)
extern "C"  bool UIGridGenerated_UIGrid_AddChild__Transform_m1323723862 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGridGenerated::UIGrid_ConstrainWithinPanel(JSVCall,System.Int32)
extern "C"  bool UIGridGenerated_UIGrid_ConstrainWithinPanel_m2797720063 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGridGenerated::UIGrid_GetChild__Int32(JSVCall,System.Int32)
extern "C"  bool UIGridGenerated_UIGrid_GetChild__Int32_m1379399661 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGridGenerated::UIGrid_GetChildList(JSVCall,System.Int32)
extern "C"  bool UIGridGenerated_UIGrid_GetChildList_m4143091521 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGridGenerated::UIGrid_GetIndex__Transform(JSVCall,System.Int32)
extern "C"  bool UIGridGenerated_UIGrid_GetIndex__Transform_m4043664981 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGridGenerated::UIGrid_RemoveChild__Transform(JSVCall,System.Int32)
extern "C"  bool UIGridGenerated_UIGrid_RemoveChild__Transform_m3761490065 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGridGenerated::UIGrid_Reposition(JSVCall,System.Int32)
extern "C"  bool UIGridGenerated_UIGrid_Reposition_m3694759801 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGridGenerated::UIGrid_SelectItem__Int32(JSVCall,System.Int32)
extern "C"  bool UIGridGenerated_UIGrid_SelectItem__Int32_m1675952324 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGridGenerated::UIGrid_SelectPos__Single(JSVCall,System.Int32)
extern "C"  bool UIGridGenerated_UIGrid_SelectPos__Single_m1430234245 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGridGenerated::UIGrid_SortByName__Transform__Transform(JSVCall,System.Int32)
extern "C"  bool UIGridGenerated_UIGrid_SortByName__Transform__Transform_m3274487389 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGridGenerated::UIGrid_SortHorizontal__Transform__Transform(JSVCall,System.Int32)
extern "C"  bool UIGridGenerated_UIGrid_SortHorizontal__Transform__Transform_m1259023103 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGridGenerated::UIGrid_SortVertical__Transform__Transform(JSVCall,System.Int32)
extern "C"  bool UIGridGenerated_UIGrid_SortVertical__Transform__Transform_m3364429265 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::__Register()
extern "C"  void UIGridGenerated___Register_m203207217 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIGrid/OnReposition UIGridGenerated::<UIGrid_onReposition>m__14D()
extern "C"  OnReposition_t1310478256 * UIGridGenerated_U3CUIGrid_onRepositionU3Em__14D_m2345529605 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Comparison`1<UnityEngine.Transform> UIGridGenerated::<UIGrid_onCustomSort>m__14F()
extern "C"  Comparison_1_t375483973 * UIGridGenerated_U3CUIGrid_onCustomSortU3Em__14F_m3404031042 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIGridGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UIGridGenerated_ilo_getObject1_m518677408 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIGridGenerated::ilo_getEnum2(System.Int32)
extern "C"  int32_t UIGridGenerated_ilo_getEnum2_m1212681951 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::ilo_setEnum3(System.Int32,System.Int32)
extern "C"  void UIGridGenerated_ilo_setEnum3_m3451966645 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::ilo_setSingle4(System.Int32,System.Single)
extern "C"  void UIGridGenerated_ilo_setSingle4_m4088014305 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGridGenerated::ilo_getBooleanS5(System.Int32)
extern "C"  bool UIGridGenerated_ilo_getBooleanS5_m1619246290 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::ilo_setBooleanS6(System.Int32,System.Boolean)
extern "C"  void UIGridGenerated_ilo_setBooleanS6_m2337627234 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::ilo_addJSFunCSDelegateRel7(System.Int32,System.Delegate)
extern "C"  void UIGridGenerated_ilo_addJSFunCSDelegateRel7_m2564479682 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::ilo_AddChild8(UIGrid,UnityEngine.Transform,System.Boolean)
extern "C"  void UIGridGenerated_ilo_AddChild8_m2505547106 (Il2CppObject * __this /* static, unused */, UIGrid_t2503122938 * ____this0, Transform_t1659122786 * ___trans1, bool ___sort2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::ilo_AddChild9(UIGrid,UnityEngine.Transform)
extern "C"  void UIGridGenerated_ilo_AddChild9_m1144361212 (Il2CppObject * __this /* static, unused */, UIGrid_t2503122938 * ____this0, Transform_t1659122786 * ___trans1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::ilo_ConstrainWithinPanel10(UIGrid)
extern "C"  void UIGridGenerated_ilo_ConstrainWithinPanel10_m3982182376 (Il2CppObject * __this /* static, unused */, UIGrid_t2503122938 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UIGridGenerated::ilo_GetChild11(UIGrid,System.Int32)
extern "C"  Transform_t1659122786 * UIGridGenerated_ilo_GetChild11_m3746262120 (Il2CppObject * __this /* static, unused */, UIGrid_t2503122938 * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIGridGenerated::ilo_getObject12(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIGridGenerated_ilo_getObject12_m2599824979 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIGridGenerated::ilo_GetIndex13(UIGrid,UnityEngine.Transform)
extern "C"  int32_t UIGridGenerated_ilo_GetIndex13_m2483716750 (Il2CppObject * __this /* static, unused */, UIGrid_t2503122938 * ____this0, Transform_t1659122786 * ___trans1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGridGenerated::ilo_RemoveChild14(UIGrid,UnityEngine.Transform)
extern "C"  bool UIGridGenerated_ilo_RemoveChild14_m789612397 (Il2CppObject * __this /* static, unused */, UIGrid_t2503122938 * ____this0, Transform_t1659122786 * ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIGridGenerated::ilo_getInt3215(System.Int32)
extern "C"  int32_t UIGridGenerated_ilo_getInt3215_m1087467520 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::ilo_SelectItem16(UIGrid,System.Int32)
extern "C"  void UIGridGenerated_ilo_SelectItem16_m2287343420 (Il2CppObject * __this /* static, unused */, UIGrid_t2503122938 * ____this0, int32_t ___selectIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIGridGenerated::ilo_getSingle17(System.Int32)
extern "C"  float UIGridGenerated_ilo_getSingle17_m3383089410 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIGridGenerated::ilo_SortByName18(UnityEngine.Transform,UnityEngine.Transform)
extern "C"  int32_t UIGridGenerated_ilo_SortByName18_m3591359418 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___a0, Transform_t1659122786 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIGridGenerated::ilo_setInt3219(System.Int32,System.Int32)
extern "C"  void UIGridGenerated_ilo_setInt3219_m736354257 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIGridGenerated::ilo_SortVertical20(UnityEngine.Transform,UnityEngine.Transform)
extern "C"  int32_t UIGridGenerated_ilo_SortVertical20_m2171159183 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___a0, Transform_t1659122786 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject UIGridGenerated::ilo_getFunctionS21(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * UIGridGenerated_ilo_getFunctionS21_m2236134841 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIGridGenerated::ilo_isFunctionS22(System.Int32)
extern "C"  bool UIGridGenerated_ilo_isFunctionS22_m3835936439 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
