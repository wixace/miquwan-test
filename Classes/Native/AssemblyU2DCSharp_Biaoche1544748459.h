﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// friendNpcCfg
struct friendNpcCfg_t3890310657;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Single[]
struct SingleU5BU5D_t2316563989;

#include "AssemblyU2DCSharp_Monster2901270458.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Biaoche
struct  Biaoche_t1544748459  : public Monster_t2901270458
{
public:
	// friendNpcCfg Biaoche::_friendNpcCfg
	friendNpcCfg_t3890310657 * ____friendNpcCfg_338;
	// UnityEngine.Vector3[] Biaoche::targetPointArr
	Vector3U5BU5D_t215400611* ___targetPointArr_339;
	// System.Single Biaoche::_curTime
	float ____curTime_340;
	// System.Single Biaoche::_waitTime
	float ____waitTime_341;
	// System.Boolean Biaoche::_isStartMove
	bool ____isStartMove_342;
	// System.Single Biaoche::_maxDis
	float ____maxDis_343;
	// System.Int32 Biaoche::_aimCount
	int32_t ____aimCount_344;
	// System.Single[] Biaoche::_sumWay
	SingleU5BU5D_t2316563989* ____sumWay_345;
	// System.Single Biaoche::<MoveJindu>k__BackingField
	float ___U3CMoveJinduU3Ek__BackingField_346;

public:
	inline static int32_t get_offset_of__friendNpcCfg_338() { return static_cast<int32_t>(offsetof(Biaoche_t1544748459, ____friendNpcCfg_338)); }
	inline friendNpcCfg_t3890310657 * get__friendNpcCfg_338() const { return ____friendNpcCfg_338; }
	inline friendNpcCfg_t3890310657 ** get_address_of__friendNpcCfg_338() { return &____friendNpcCfg_338; }
	inline void set__friendNpcCfg_338(friendNpcCfg_t3890310657 * value)
	{
		____friendNpcCfg_338 = value;
		Il2CppCodeGenWriteBarrier(&____friendNpcCfg_338, value);
	}

	inline static int32_t get_offset_of_targetPointArr_339() { return static_cast<int32_t>(offsetof(Biaoche_t1544748459, ___targetPointArr_339)); }
	inline Vector3U5BU5D_t215400611* get_targetPointArr_339() const { return ___targetPointArr_339; }
	inline Vector3U5BU5D_t215400611** get_address_of_targetPointArr_339() { return &___targetPointArr_339; }
	inline void set_targetPointArr_339(Vector3U5BU5D_t215400611* value)
	{
		___targetPointArr_339 = value;
		Il2CppCodeGenWriteBarrier(&___targetPointArr_339, value);
	}

	inline static int32_t get_offset_of__curTime_340() { return static_cast<int32_t>(offsetof(Biaoche_t1544748459, ____curTime_340)); }
	inline float get__curTime_340() const { return ____curTime_340; }
	inline float* get_address_of__curTime_340() { return &____curTime_340; }
	inline void set__curTime_340(float value)
	{
		____curTime_340 = value;
	}

	inline static int32_t get_offset_of__waitTime_341() { return static_cast<int32_t>(offsetof(Biaoche_t1544748459, ____waitTime_341)); }
	inline float get__waitTime_341() const { return ____waitTime_341; }
	inline float* get_address_of__waitTime_341() { return &____waitTime_341; }
	inline void set__waitTime_341(float value)
	{
		____waitTime_341 = value;
	}

	inline static int32_t get_offset_of__isStartMove_342() { return static_cast<int32_t>(offsetof(Biaoche_t1544748459, ____isStartMove_342)); }
	inline bool get__isStartMove_342() const { return ____isStartMove_342; }
	inline bool* get_address_of__isStartMove_342() { return &____isStartMove_342; }
	inline void set__isStartMove_342(bool value)
	{
		____isStartMove_342 = value;
	}

	inline static int32_t get_offset_of__maxDis_343() { return static_cast<int32_t>(offsetof(Biaoche_t1544748459, ____maxDis_343)); }
	inline float get__maxDis_343() const { return ____maxDis_343; }
	inline float* get_address_of__maxDis_343() { return &____maxDis_343; }
	inline void set__maxDis_343(float value)
	{
		____maxDis_343 = value;
	}

	inline static int32_t get_offset_of__aimCount_344() { return static_cast<int32_t>(offsetof(Biaoche_t1544748459, ____aimCount_344)); }
	inline int32_t get__aimCount_344() const { return ____aimCount_344; }
	inline int32_t* get_address_of__aimCount_344() { return &____aimCount_344; }
	inline void set__aimCount_344(int32_t value)
	{
		____aimCount_344 = value;
	}

	inline static int32_t get_offset_of__sumWay_345() { return static_cast<int32_t>(offsetof(Biaoche_t1544748459, ____sumWay_345)); }
	inline SingleU5BU5D_t2316563989* get__sumWay_345() const { return ____sumWay_345; }
	inline SingleU5BU5D_t2316563989** get_address_of__sumWay_345() { return &____sumWay_345; }
	inline void set__sumWay_345(SingleU5BU5D_t2316563989* value)
	{
		____sumWay_345 = value;
		Il2CppCodeGenWriteBarrier(&____sumWay_345, value);
	}

	inline static int32_t get_offset_of_U3CMoveJinduU3Ek__BackingField_346() { return static_cast<int32_t>(offsetof(Biaoche_t1544748459, ___U3CMoveJinduU3Ek__BackingField_346)); }
	inline float get_U3CMoveJinduU3Ek__BackingField_346() const { return ___U3CMoveJinduU3Ek__BackingField_346; }
	inline float* get_address_of_U3CMoveJinduU3Ek__BackingField_346() { return &___U3CMoveJinduU3Ek__BackingField_346; }
	inline void set_U3CMoveJinduU3Ek__BackingField_346(float value)
	{
		___U3CMoveJinduU3Ek__BackingField_346 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
