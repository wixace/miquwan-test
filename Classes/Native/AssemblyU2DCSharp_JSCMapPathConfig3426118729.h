﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<JSCMapPathPointInfoConfig>
struct List_1_t4071866601;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSCMapPathConfig
struct  JSCMapPathConfig_t3426118729  : public Il2CppObject
{
public:
	// System.Int32 JSCMapPathConfig::_id
	int32_t ____id_0;
	// System.String JSCMapPathConfig::_sceneName
	String_t* ____sceneName_1;
	// System.String JSCMapPathConfig::_viewName
	String_t* ____viewName_2;
	// System.Collections.Generic.List`1<JSCMapPathPointInfoConfig> JSCMapPathConfig::_pathPoints
	List_1_t4071866601 * ____pathPoints_3;
	// ProtoBuf.IExtension JSCMapPathConfig::extensionObject
	Il2CppObject * ___extensionObject_4;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(JSCMapPathConfig_t3426118729, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__sceneName_1() { return static_cast<int32_t>(offsetof(JSCMapPathConfig_t3426118729, ____sceneName_1)); }
	inline String_t* get__sceneName_1() const { return ____sceneName_1; }
	inline String_t** get_address_of__sceneName_1() { return &____sceneName_1; }
	inline void set__sceneName_1(String_t* value)
	{
		____sceneName_1 = value;
		Il2CppCodeGenWriteBarrier(&____sceneName_1, value);
	}

	inline static int32_t get_offset_of__viewName_2() { return static_cast<int32_t>(offsetof(JSCMapPathConfig_t3426118729, ____viewName_2)); }
	inline String_t* get__viewName_2() const { return ____viewName_2; }
	inline String_t** get_address_of__viewName_2() { return &____viewName_2; }
	inline void set__viewName_2(String_t* value)
	{
		____viewName_2 = value;
		Il2CppCodeGenWriteBarrier(&____viewName_2, value);
	}

	inline static int32_t get_offset_of__pathPoints_3() { return static_cast<int32_t>(offsetof(JSCMapPathConfig_t3426118729, ____pathPoints_3)); }
	inline List_1_t4071866601 * get__pathPoints_3() const { return ____pathPoints_3; }
	inline List_1_t4071866601 ** get_address_of__pathPoints_3() { return &____pathPoints_3; }
	inline void set__pathPoints_3(List_1_t4071866601 * value)
	{
		____pathPoints_3 = value;
		Il2CppCodeGenWriteBarrier(&____pathPoints_3, value);
	}

	inline static int32_t get_offset_of_extensionObject_4() { return static_cast<int32_t>(offsetof(JSCMapPathConfig_t3426118729, ___extensionObject_4)); }
	inline Il2CppObject * get_extensionObject_4() const { return ___extensionObject_4; }
	inline Il2CppObject ** get_address_of_extensionObject_4() { return &___extensionObject_4; }
	inline void set_extensionObject_4(Il2CppObject * value)
	{
		___extensionObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObject_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
