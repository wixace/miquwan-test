﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AudioEchoFilter
struct AudioEchoFilter_t1861958825;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.AudioEchoFilter::.ctor()
extern "C"  void AudioEchoFilter__ctor_m1648682822 (AudioEchoFilter_t1861958825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioEchoFilter::get_delay()
extern "C"  float AudioEchoFilter_get_delay_m2674003690 (AudioEchoFilter_t1861958825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioEchoFilter::set_delay(System.Single)
extern "C"  void AudioEchoFilter_set_delay_m4191603969 (AudioEchoFilter_t1861958825 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioEchoFilter::get_decayRatio()
extern "C"  float AudioEchoFilter_get_decayRatio_m2656623564 (AudioEchoFilter_t1861958825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioEchoFilter::set_decayRatio(System.Single)
extern "C"  void AudioEchoFilter_set_decayRatio_m641639263 (AudioEchoFilter_t1861958825 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioEchoFilter::get_dryMix()
extern "C"  float AudioEchoFilter_get_dryMix_m295690508 (AudioEchoFilter_t1861958825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioEchoFilter::set_dryMix(System.Single)
extern "C"  void AudioEchoFilter_set_dryMix_m2055415327 (AudioEchoFilter_t1861958825 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioEchoFilter::get_wetMix()
extern "C"  float AudioEchoFilter_get_wetMix_m253556785 (AudioEchoFilter_t1861958825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioEchoFilter::set_wetMix(System.Single)
extern "C"  void AudioEchoFilter_set_wetMix_m2723255066 (AudioEchoFilter_t1861958825 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
