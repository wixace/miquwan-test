﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimationScrollTexture1
struct AnimationScrollTexture1_t3327804647;

#include "codegen/il2cpp-codegen.h"

// System.Void AnimationScrollTexture1::.ctor()
extern "C"  void AnimationScrollTexture1__ctor_m1776587599 (AnimationScrollTexture1_t3327804647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationScrollTexture1::FixedUpdate()
extern "C"  void AnimationScrollTexture1_FixedUpdate_m3422583178 (AnimationScrollTexture1_t3327804647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationScrollTexture1::Main()
extern "C"  void AnimationScrollTexture1_Main_m3020844942 (AnimationScrollTexture1_t3327804647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
