﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEventListenerGenerated/<UIEventListener_onDrop_GetDelegate_member13_arg0>c__AnonStoreyC1
struct U3CUIEventListener_onDrop_GetDelegate_member13_arg0U3Ec__AnonStoreyC1_t3941882214;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UIEventListenerGenerated/<UIEventListener_onDrop_GetDelegate_member13_arg0>c__AnonStoreyC1::.ctor()
extern "C"  void U3CUIEventListener_onDrop_GetDelegate_member13_arg0U3Ec__AnonStoreyC1__ctor_m3618537973 (U3CUIEventListener_onDrop_GetDelegate_member13_arg0U3Ec__AnonStoreyC1_t3941882214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated/<UIEventListener_onDrop_GetDelegate_member13_arg0>c__AnonStoreyC1::<>m__146(UnityEngine.GameObject,UnityEngine.GameObject)
extern "C"  void U3CUIEventListener_onDrop_GetDelegate_member13_arg0U3Ec__AnonStoreyC1_U3CU3Em__146_m518200975 (U3CUIEventListener_onDrop_GetDelegate_member13_arg0U3Ec__AnonStoreyC1_t3941882214 * __this, GameObject_t3674682005 * ___go0, GameObject_t3674682005 * ___obj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
