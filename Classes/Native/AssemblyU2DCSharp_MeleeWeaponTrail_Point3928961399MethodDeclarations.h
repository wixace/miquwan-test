﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MeleeWeaponTrail/Point
struct Point_t3928961399;

#include "codegen/il2cpp-codegen.h"

// System.Void MeleeWeaponTrail/Point::.ctor()
extern "C"  void Point__ctor_m1450686724 (Point_t3928961399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
