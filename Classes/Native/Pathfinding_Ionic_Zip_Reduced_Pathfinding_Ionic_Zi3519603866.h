﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "Pathfinding_Ionic_Zip_Reduced_Pathfinding_Ionic_Zi3652130261.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Ionic.Zip.ReadProgressEventArgs
struct  ReadProgressEventArgs_t3519603866  : public ZipProgressEventArgs_t3652130261
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
