﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen355664257.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21573321581.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3076045307_gshared (InternalEnumerator_1_t355664257 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3076045307(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t355664257 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3076045307_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3582856133_gshared (InternalEnumerator_1_t355664257 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3582856133(__this, method) ((  void (*) (InternalEnumerator_1_t355664257 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3582856133_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2999767291_gshared (InternalEnumerator_1_t355664257 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2999767291(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t355664257 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2999767291_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4267471250_gshared (InternalEnumerator_1_t355664257 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m4267471250(__this, method) ((  void (*) (InternalEnumerator_1_t355664257 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4267471250_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2031813365_gshared (InternalEnumerator_1_t355664257 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2031813365(__this, method) ((  bool (*) (InternalEnumerator_1_t355664257 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2031813365_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UIModelDisplayType,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t1573321581  InternalEnumerator_1_get_Current_m3937052004_gshared (InternalEnumerator_1_t355664257 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3937052004(__this, method) ((  KeyValuePair_2_t1573321581  (*) (InternalEnumerator_1_t355664257 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3937052004_gshared)(__this, method)
