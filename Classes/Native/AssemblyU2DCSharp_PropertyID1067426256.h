﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_ConstructorID3348888181.h"
#include "AssemblyU2DCSharp_TypeFlag3682875878.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PropertyID
struct  PropertyID_t1067426256  : public ConstructorID_t3348888181
{
public:
	// System.String PropertyID::name
	String_t* ___name_3;
	// System.String PropertyID::retTypeName
	String_t* ___retTypeName_4;
	// TypeFlag PropertyID::retTypeFlag
	int32_t ___retTypeFlag_5;

public:
	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(PropertyID_t1067426256, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_retTypeName_4() { return static_cast<int32_t>(offsetof(PropertyID_t1067426256, ___retTypeName_4)); }
	inline String_t* get_retTypeName_4() const { return ___retTypeName_4; }
	inline String_t** get_address_of_retTypeName_4() { return &___retTypeName_4; }
	inline void set_retTypeName_4(String_t* value)
	{
		___retTypeName_4 = value;
		Il2CppCodeGenWriteBarrier(&___retTypeName_4, value);
	}

	inline static int32_t get_offset_of_retTypeFlag_5() { return static_cast<int32_t>(offsetof(PropertyID_t1067426256, ___retTypeFlag_5)); }
	inline int32_t get_retTypeFlag_5() const { return ___retTypeFlag_5; }
	inline int32_t* get_address_of_retTypeFlag_5() { return &___retTypeFlag_5; }
	inline void set_retTypeFlag_5(int32_t value)
	{
		___retTypeFlag_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
