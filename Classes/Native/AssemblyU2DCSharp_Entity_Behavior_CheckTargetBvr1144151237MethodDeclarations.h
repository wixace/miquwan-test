﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.CheckTargetBvr
struct CheckTargetBvr_t1144151237;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// CombatEntity
struct CombatEntity_t684137495;
// GameMgr
struct GameMgr_t1469029542;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// NpcMgr
struct NpcMgr_t2339534743;
// HeroMgr
struct HeroMgr_t2475965342;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"

// System.Void Entity.Behavior.CheckTargetBvr::.ctor()
extern "C"  void CheckTargetBvr__ctor_m2713533445 (CheckTargetBvr_t1144151237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.CheckTargetBvr::get_id()
extern "C"  uint8_t CheckTargetBvr_get_id_m1318937013 (CheckTargetBvr_t1144151237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.CheckTargetBvr::SetParams(System.Object[])
extern "C"  void CheckTargetBvr_SetParams_m973068711 (CheckTargetBvr_t1144151237 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.CheckTargetBvr::Reason()
extern "C"  void CheckTargetBvr_Reason_m2066546275 (CheckTargetBvr_t1144151237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.CheckTargetBvr::Action()
extern "C"  void CheckTargetBvr_Action_m1263252949 (CheckTargetBvr_t1144151237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.CheckTargetBvr::DoEntering()
extern "C"  void CheckTargetBvr_DoEntering_m2065190036 (CheckTargetBvr_t1144151237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.CheckTargetBvr::GetTargetComatEntity()
extern "C"  CombatEntity_t684137495 * CheckTargetBvr_GetTargetComatEntity_m2708025493 (CheckTargetBvr_t1144151237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.CheckTargetBvr::DoLeaving()
extern "C"  void CheckTargetBvr_DoLeaving_m3559570572 (CheckTargetBvr_t1144151237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.CheckTargetBvr::ilo_GetNearestEnemy1(GameMgr,CombatEntity)
extern "C"  CombatEntity_t684137495 * CheckTargetBvr_ilo_GetNearestEnemy1_m3021444236 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, CombatEntity_t684137495 * ___unit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.CheckTargetBvr::ilo_get_entity2(Entity.Behavior.IBehavior)
extern "C"  CombatEntity_t684137495 * CheckTargetBvr_ilo_get_entity2_m767362040 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.CheckTargetBvr::ilo_set_atkTarget3(CombatEntity,CombatEntity)
extern "C"  void CheckTargetBvr_ilo_set_atkTarget3_m3611866829 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, CombatEntity_t684137495 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Entity.Behavior.CheckTargetBvr::ilo_get_halfwidth4(CombatEntity)
extern "C"  float CheckTargetBvr_ilo_get_halfwidth4_m3480768113 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl Entity.Behavior.CheckTargetBvr::ilo_get_bvrCtrl5(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * CheckTargetBvr_ilo_get_bvrCtrl5_m1311868238 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.CheckTargetBvr::ilo_TranBehavior6(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void CheckTargetBvr_ilo_TranBehavior6_m848794987 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___id1, ObjectU5BU5D_t1108656482* ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Entity.Behavior.CheckTargetBvr::ilo_get_atkRange7(CombatEntity)
extern "C"  float CheckTargetBvr_ilo_get_atkRange7_m840095638 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NpcMgr Entity.Behavior.CheckTargetBvr::ilo_get_instance8()
extern "C"  NpcMgr_t2339534743 * CheckTargetBvr_ilo_get_instance8_m2964632260 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr Entity.Behavior.CheckTargetBvr::ilo_get_instance9()
extern "C"  HeroMgr_t2475965342 * CheckTargetBvr_ilo_get_instance9_m325689910 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
