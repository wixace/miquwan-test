﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1416323886.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr_ErrorIn2633981210.h"

// System.Void System.Array/InternalEnumerator`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3437723947_gshared (InternalEnumerator_1_t1416323886 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3437723947(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1416323886 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3437723947_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3309049493_gshared (InternalEnumerator_1_t1416323886 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3309049493(__this, method) ((  void (*) (InternalEnumerator_1_t1416323886 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3309049493_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m427170369_gshared (InternalEnumerator_1_t1416323886 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m427170369(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1416323886 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m427170369_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1093760194_gshared (InternalEnumerator_1_t1416323886 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1093760194(__this, method) ((  void (*) (InternalEnumerator_1_t1416323886 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1093760194_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2483065473_gshared (InternalEnumerator_1_t1416323886 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2483065473(__this, method) ((  bool (*) (InternalEnumerator_1_t1416323886 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2483065473_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Current()
extern "C"  ErrorInfo_t2633981210  InternalEnumerator_1_get_Current_m1590749234_gshared (InternalEnumerator_1_t1416323886 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1590749234(__this, method) ((  ErrorInfo_t2633981210  (*) (InternalEnumerator_1_t1416323886 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1590749234_gshared)(__this, method)
