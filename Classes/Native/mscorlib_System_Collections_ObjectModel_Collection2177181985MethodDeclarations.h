﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_Collection3356274529MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::.ctor()
#define Collection_1__ctor_m3987218883(__this, method) ((  void (*) (Collection_1_t2177181985 *, const MethodInfo*))Collection_1__ctor_m1690372513_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4116416386(__this, method) ((  bool (*) (Collection_1_t2177181985 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1624016570_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m4025103627(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2177181985 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1285013187_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m3778962630(__this, method) ((  Il2CppObject * (*) (Collection_1_t2177181985 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2828471038_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m3869973483(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2177181985 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1708617267_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m2843595969(__this, ___value0, method) ((  bool (*) (Collection_1_t2177181985 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m504494585_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m3520423107(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2177181985 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1652511499_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m115119214(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2177181985 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2187188150_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m3728243194(__this, ___value0, method) ((  void (*) (Collection_1_t2177181985 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2625864114_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3922509563(__this, method) ((  bool (*) (Collection_1_t2177181985 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1302589123_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m3074446887(__this, method) ((  Il2CppObject * (*) (Collection_1_t2177181985 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1873829551_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m3165677808(__this, method) ((  bool (*) (Collection_1_t2177181985 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2813777960_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1387411465(__this, method) ((  bool (*) (Collection_1_t2177181985 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1376059857_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m671570414(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t2177181985 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2224513142_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m3869334725(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2177181985 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2262756877_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::Add(T)
#define Collection_1_Add_m3681579699(__this, ___item0, method) ((  void (*) (Collection_1_t2177181985 *, MailAddress_t2991723827 *, const MethodInfo*))Collection_1_Add_m321765054_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::Clear()
#define Collection_1_Clear_m1949888132(__this, method) ((  void (*) (Collection_1_t2177181985 *, const MethodInfo*))Collection_1_Clear_m3391473100_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::ClearItems()
#define Collection_1_ClearItems_m3369542910(__this, method) ((  void (*) (Collection_1_t2177181985 *, const MethodInfo*))Collection_1_ClearItems_m2738199222_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::Contains(T)
#define Collection_1_Contains_m4182405042(__this, ___item0, method) ((  bool (*) (Collection_1_t2177181985 *, MailAddress_t2991723827 *, const MethodInfo*))Collection_1_Contains_m1050871674_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m3666940278(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2177181985 *, MailAddressU5BU5D_t3312478370*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1746187054_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::GetEnumerator()
#define Collection_1_GetEnumerator_m264498366(__this, method) ((  Il2CppObject* (*) (Collection_1_t2177181985 *, const MethodInfo*))Collection_1_GetEnumerator_m625631581_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::IndexOf(T)
#define Collection_1_IndexOf_m3254727034(__this, ___item0, method) ((  int32_t (*) (Collection_1_t2177181985 *, MailAddress_t2991723827 *, const MethodInfo*))Collection_1_IndexOf_m3101447730_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::Insert(System.Int32,T)
#define Collection_1_Insert_m3239120237(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2177181985 *, int32_t, MailAddress_t2991723827 *, const MethodInfo*))Collection_1_Insert_m1208073509_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m999898841(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2177181985 *, int32_t, MailAddress_t2991723827 *, const MethodInfo*))Collection_1_InsertItem_m714854616_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::get_Items()
#define Collection_1_get_Items_m3344676632(__this, method) ((  Il2CppObject* (*) (Collection_1_t2177181985 *, const MethodInfo*))Collection_1_get_Items_m1477178336_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::Remove(T)
#define Collection_1_Remove_m3243996205(__this, ___item0, method) ((  bool (*) (Collection_1_t2177181985 *, MailAddress_t2991723827 *, const MethodInfo*))Collection_1_Remove_m2181520885_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m1112973107(__this, ___index0, method) ((  void (*) (Collection_1_t2177181985 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3376893675_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m3019924115(__this, ___index0, method) ((  void (*) (Collection_1_t2177181985 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1099170891_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::get_Count()
#define Collection_1_get_Count_m1798206999(__this, method) ((  int32_t (*) (Collection_1_t2177181985 *, const MethodInfo*))Collection_1_get_Count_m1472906633_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::get_Item(System.Int32)
#define Collection_1_get_Item_m4200879582(__this, ___index0, method) ((  MailAddress_t2991723827 * (*) (Collection_1_t2177181985 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2356360623_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m752854788(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2177181985 *, int32_t, MailAddress_t2991723827 *, const MethodInfo*))Collection_1_set_Item_m3127068860_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m432718096(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2177181985 *, int32_t, MailAddress_t2991723827 *, const MethodInfo*))Collection_1_SetItem_m112162877_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m3545655354(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1993492338_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m1436178198(__this /* static, unused */, ___item0, method) ((  MailAddress_t2991723827 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1655469326_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m1001188918(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m651250670_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m3362457162(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2469749778_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m1207432341(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3893865421_gshared)(__this /* static, unused */, ___list0, method)
