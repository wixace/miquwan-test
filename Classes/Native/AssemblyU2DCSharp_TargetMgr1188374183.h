﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TargetMgr
struct TargetMgr_t1188374183;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint>
struct Dictionary_2_t253816784;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TargetMgr
struct  TargetMgr_t1188374183  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<CombatEntity,TargetMgr/FightPoint> TargetMgr::targetFightPointDic
	Dictionary_2_t253816784 * ___targetFightPointDic_2;
	// System.Int32 TargetMgr::randomCount
	int32_t ___randomCount_3;

public:
	inline static int32_t get_offset_of_targetFightPointDic_2() { return static_cast<int32_t>(offsetof(TargetMgr_t1188374183, ___targetFightPointDic_2)); }
	inline Dictionary_2_t253816784 * get_targetFightPointDic_2() const { return ___targetFightPointDic_2; }
	inline Dictionary_2_t253816784 ** get_address_of_targetFightPointDic_2() { return &___targetFightPointDic_2; }
	inline void set_targetFightPointDic_2(Dictionary_2_t253816784 * value)
	{
		___targetFightPointDic_2 = value;
		Il2CppCodeGenWriteBarrier(&___targetFightPointDic_2, value);
	}

	inline static int32_t get_offset_of_randomCount_3() { return static_cast<int32_t>(offsetof(TargetMgr_t1188374183, ___randomCount_3)); }
	inline int32_t get_randomCount_3() const { return ___randomCount_3; }
	inline int32_t* get_address_of_randomCount_3() { return &___randomCount_3; }
	inline void set_randomCount_3(int32_t value)
	{
		___randomCount_3 = value;
	}
};

struct TargetMgr_t1188374183_StaticFields
{
public:
	// TargetMgr TargetMgr::_inst
	TargetMgr_t1188374183 * ____inst_0;
	// System.String TargetMgr::FINDTARGET
	String_t* ___FINDTARGET_1;

public:
	inline static int32_t get_offset_of__inst_0() { return static_cast<int32_t>(offsetof(TargetMgr_t1188374183_StaticFields, ____inst_0)); }
	inline TargetMgr_t1188374183 * get__inst_0() const { return ____inst_0; }
	inline TargetMgr_t1188374183 ** get_address_of__inst_0() { return &____inst_0; }
	inline void set__inst_0(TargetMgr_t1188374183 * value)
	{
		____inst_0 = value;
		Il2CppCodeGenWriteBarrier(&____inst_0, value);
	}

	inline static int32_t get_offset_of_FINDTARGET_1() { return static_cast<int32_t>(offsetof(TargetMgr_t1188374183_StaticFields, ___FINDTARGET_1)); }
	inline String_t* get_FINDTARGET_1() const { return ___FINDTARGET_1; }
	inline String_t** get_address_of_FINDTARGET_1() { return &___FINDTARGET_1; }
	inline void set_FINDTARGET_1(String_t* value)
	{
		___FINDTARGET_1 = value;
		Il2CppCodeGenWriteBarrier(&___FINDTARGET_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
