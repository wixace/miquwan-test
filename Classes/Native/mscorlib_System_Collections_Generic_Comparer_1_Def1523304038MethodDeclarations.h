﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.IntRect>
struct DefaultComparer_t1523304038;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_IntRect3015058261.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.IntRect>::.ctor()
extern "C"  void DefaultComparer__ctor_m905302125_gshared (DefaultComparer_t1523304038 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m905302125(__this, method) ((  void (*) (DefaultComparer_t1523304038 *, const MethodInfo*))DefaultComparer__ctor_m905302125_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.IntRect>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m2041729762_gshared (DefaultComparer_t1523304038 * __this, IntRect_t3015058261  ___x0, IntRect_t3015058261  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m2041729762(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t1523304038 *, IntRect_t3015058261 , IntRect_t3015058261 , const MethodInfo*))DefaultComparer_Compare_m2041729762_gshared)(__this, ___x0, ___y1, method)
