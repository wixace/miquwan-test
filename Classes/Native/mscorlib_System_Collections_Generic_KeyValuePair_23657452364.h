﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CombatEntity
struct CombatEntity_t684137495;

#include "mscorlib_System_ValueType1744280289.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<CombatEntity,HatredCtrl/stValue>
struct  KeyValuePair_2_t3657452364 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	CombatEntity_t684137495 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	stValue_t3425945410  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3657452364, ___key_0)); }
	inline CombatEntity_t684137495 * get_key_0() const { return ___key_0; }
	inline CombatEntity_t684137495 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(CombatEntity_t684137495 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3657452364, ___value_1)); }
	inline stValue_t3425945410  get_value_1() const { return ___value_1; }
	inline stValue_t3425945410 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(stValue_t3425945410  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
