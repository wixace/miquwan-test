﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>
struct ReadOnlyCollection_1_t3587009337;
// System.Collections.Generic.IList`1<Pathfinding.LocalAvoidance/VOLine>
struct IList_1_t429611708;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.LocalAvoidance/VOLine[]
struct VOLineU5BU5D_t1958725220;
// System.Collections.Generic.IEnumerator`1<Pathfinding.LocalAvoidance/VOLine>
struct IEnumerator_1_t3941796850;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_VOLin2029931801.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3054655031_gshared (ReadOnlyCollection_1_t3587009337 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3054655031(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3587009337 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3054655031_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m300061857_gshared (ReadOnlyCollection_1_t3587009337 * __this, VOLine_t2029931801  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m300061857(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3587009337 *, VOLine_t2029931801 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m300061857_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2718673993_gshared (ReadOnlyCollection_1_t3587009337 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2718673993(__this, method) ((  void (*) (ReadOnlyCollection_1_t3587009337 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2718673993_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m470450760_gshared (ReadOnlyCollection_1_t3587009337 * __this, int32_t ___index0, VOLine_t2029931801  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m470450760(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3587009337 *, int32_t, VOLine_t2029931801 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m470450760_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1295614642_gshared (ReadOnlyCollection_1_t3587009337 * __this, VOLine_t2029931801  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1295614642(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3587009337 *, VOLine_t2029931801 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1295614642_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2639270926_gshared (ReadOnlyCollection_1_t3587009337 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2639270926(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3587009337 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2639270926_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  VOLine_t2029931801  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3690234578_gshared (ReadOnlyCollection_1_t3587009337 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3690234578(__this, ___index0, method) ((  VOLine_t2029931801  (*) (ReadOnlyCollection_1_t3587009337 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3690234578_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2941210911_gshared (ReadOnlyCollection_1_t3587009337 * __this, int32_t ___index0, VOLine_t2029931801  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2941210911(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3587009337 *, int32_t, VOLine_t2029931801 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2941210911_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m284765853_gshared (ReadOnlyCollection_1_t3587009337 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m284765853(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3587009337 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m284765853_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m749332326_gshared (ReadOnlyCollection_1_t3587009337 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m749332326(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3587009337 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m749332326_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1083328865_gshared (ReadOnlyCollection_1_t3587009337 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1083328865(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3587009337 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1083328865_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m904988592_gshared (ReadOnlyCollection_1_t3587009337 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m904988592(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3587009337 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m904988592_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m484314740_gshared (ReadOnlyCollection_1_t3587009337 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m484314740(__this, method) ((  void (*) (ReadOnlyCollection_1_t3587009337 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m484314740_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m636588444_gshared (ReadOnlyCollection_1_t3587009337 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m636588444(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3587009337 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m636588444_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4043695624_gshared (ReadOnlyCollection_1_t3587009337 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4043695624(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3587009337 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4043695624_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3509032243_gshared (ReadOnlyCollection_1_t3587009337 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3509032243(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3587009337 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m3509032243_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1159896341_gshared (ReadOnlyCollection_1_t3587009337 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1159896341(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3587009337 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1159896341_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3182403907_gshared (ReadOnlyCollection_1_t3587009337 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3182403907(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3587009337 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3182403907_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2320699456_gshared (ReadOnlyCollection_1_t3587009337 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2320699456(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3587009337 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2320699456_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1081519212_gshared (ReadOnlyCollection_1_t3587009337 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1081519212(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3587009337 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1081519212_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m377333259_gshared (ReadOnlyCollection_1_t3587009337 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m377333259(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3587009337 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m377333259_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m604728206_gshared (ReadOnlyCollection_1_t3587009337 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m604728206(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3587009337 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m604728206_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1023488755_gshared (ReadOnlyCollection_1_t3587009337 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1023488755(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3587009337 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1023488755_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1244610634_gshared (ReadOnlyCollection_1_t3587009337 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1244610634(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3587009337 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1244610634_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m2674338295_gshared (ReadOnlyCollection_1_t3587009337 * __this, VOLine_t2029931801  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m2674338295(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3587009337 *, VOLine_t2029931801 , const MethodInfo*))ReadOnlyCollection_1_Contains_m2674338295_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3653139153_gshared (ReadOnlyCollection_1_t3587009337 * __this, VOLineU5BU5D_t1958725220* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m3653139153(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3587009337 *, VOLineU5BU5D_t1958725220*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m3653139153_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m1012836442_gshared (ReadOnlyCollection_1_t3587009337 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1012836442(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3587009337 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1012836442_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m3926353173_gshared (ReadOnlyCollection_1_t3587009337 * __this, VOLine_t2029931801  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m3926353173(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3587009337 *, VOLine_t2029931801 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m3926353173_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1275171590_gshared (ReadOnlyCollection_1_t3587009337 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1275171590(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3587009337 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1275171590_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.LocalAvoidance/VOLine>::get_Item(System.Int32)
extern "C"  VOLine_t2029931801  ReadOnlyCollection_1_get_Item_m3354994066_gshared (ReadOnlyCollection_1_t3587009337 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3354994066(__this, ___index0, method) ((  VOLine_t2029931801  (*) (ReadOnlyCollection_1_t3587009337 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3354994066_gshared)(__this, ___index0, method)
