﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Schema.JsonSchema
struct JsonSchema_t460567603;
// Newtonsoft.Json.Schema.ValidationEventHandler
struct ValidationEventHandler_t2238986739;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// Newtonsoft.Json.JsonValidatingReader
struct JsonValidatingReader_t188359606;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema460567603.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_Validatio2238986739.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonValidatingRea188359606.h"

// System.Boolean Newtonsoft.Json.Schema.SchemaExtensions::IsValid(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Schema.JsonSchema)
extern "C"  bool SchemaExtensions_IsValid_m49735123 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___source0, JsonSchema_t460567603 * ___schema1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.SchemaExtensions::Validate(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void SchemaExtensions_Validate_m2989695417 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___source0, JsonSchema_t460567603 * ___schema1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.SchemaExtensions::Validate(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Schema.JsonSchema,Newtonsoft.Json.Schema.ValidationEventHandler)
extern "C"  void SchemaExtensions_Validate_m68313348 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___source0, JsonSchema_t460567603 * ___schema1, ValidationEventHandler_t2238986739 * ___validationEventHandler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.SchemaExtensions::ilo_Validate1(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Schema.JsonSchema,Newtonsoft.Json.Schema.ValidationEventHandler)
extern "C"  void SchemaExtensions_ilo_Validate1_m916307388 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___source0, JsonSchema_t460567603 * ___schema1, ValidationEventHandler_t2238986739 * ___validationEventHandler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.SchemaExtensions::ilo_ArgumentNotNull2(System.Object,System.String)
extern "C"  void SchemaExtensions_ilo_ArgumentNotNull2_m4249791458 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, String_t* ___parameterName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.SchemaExtensions::ilo_Read3(Newtonsoft.Json.JsonValidatingReader)
extern "C"  bool SchemaExtensions_ilo_Read3_m4004430727 (Il2CppObject * __this /* static, unused */, JsonValidatingReader_t188359606 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
