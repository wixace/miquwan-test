﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BMSymbolGenerated
struct BMSymbolGenerated_t790609676;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// BMSymbol
struct BMSymbol_t1170982339;
// UIAtlas
struct UIAtlas_t281921111;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_BMSymbol1170982339.h"
#include "AssemblyU2DCSharp_UIAtlas281921111.h"

// System.Void BMSymbolGenerated::.ctor()
extern "C"  void BMSymbolGenerated__ctor_m980163551 (BMSymbolGenerated_t790609676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BMSymbolGenerated::BMSymbol_BMSymbol1(JSVCall,System.Int32)
extern "C"  bool BMSymbolGenerated_BMSymbol_BMSymbol1_m991128339 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMSymbolGenerated::BMSymbol_sequence(JSVCall)
extern "C"  void BMSymbolGenerated_BMSymbol_sequence_m2988036557 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMSymbolGenerated::BMSymbol_spriteName(JSVCall)
extern "C"  void BMSymbolGenerated_BMSymbol_spriteName_m3208433022 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMSymbolGenerated::BMSymbol_length(JSVCall)
extern "C"  void BMSymbolGenerated_BMSymbol_length_m1293181512 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMSymbolGenerated::BMSymbol_offsetX(JSVCall)
extern "C"  void BMSymbolGenerated_BMSymbol_offsetX_m4032833817 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMSymbolGenerated::BMSymbol_offsetY(JSVCall)
extern "C"  void BMSymbolGenerated_BMSymbol_offsetY_m3836320312 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMSymbolGenerated::BMSymbol_width(JSVCall)
extern "C"  void BMSymbolGenerated_BMSymbol_width_m1185633432 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMSymbolGenerated::BMSymbol_height(JSVCall)
extern "C"  void BMSymbolGenerated_BMSymbol_height_m3050341671 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMSymbolGenerated::BMSymbol_advance(JSVCall)
extern "C"  void BMSymbolGenerated_BMSymbol_advance_m153028444 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMSymbolGenerated::BMSymbol_uvRect(JSVCall)
extern "C"  void BMSymbolGenerated_BMSymbol_uvRect_m3759075305 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BMSymbolGenerated::BMSymbol_MarkAsChanged(JSVCall,System.Int32)
extern "C"  bool BMSymbolGenerated_BMSymbol_MarkAsChanged_m3451197626 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BMSymbolGenerated::BMSymbol_Validate__UIAtlas(JSVCall,System.Int32)
extern "C"  bool BMSymbolGenerated_BMSymbol_Validate__UIAtlas_m421554694 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMSymbolGenerated::__Register()
extern "C"  void BMSymbolGenerated___Register_m1578562632 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BMSymbolGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool BMSymbolGenerated_ilo_attachFinalizerObject1_m4013506296 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMSymbolGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void BMSymbolGenerated_ilo_addJSCSRel2_m3184219036 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String BMSymbolGenerated::ilo_getStringS3(System.Int32)
extern "C"  String_t* BMSymbolGenerated_ilo_getStringS3_m2996326533 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BMSymbolGenerated::ilo_get_length4(BMSymbol)
extern "C"  int32_t BMSymbolGenerated_ilo_get_length4_m1930395994 (Il2CppObject * __this /* static, unused */, BMSymbol_t1170982339 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMSymbolGenerated::ilo_setInt325(System.Int32,System.Int32)
extern "C"  void BMSymbolGenerated_ilo_setInt325_m1516424531 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BMSymbolGenerated::ilo_get_offsetY6(BMSymbol)
extern "C"  int32_t BMSymbolGenerated_ilo_get_offsetY6_m2273550080 (Il2CppObject * __this /* static, unused */, BMSymbol_t1170982339 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BMSymbolGenerated::ilo_get_width7(BMSymbol)
extern "C"  int32_t BMSymbolGenerated_ilo_get_width7_m2376551073 (Il2CppObject * __this /* static, unused */, BMSymbol_t1170982339 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BMSymbolGenerated::ilo_get_advance8(BMSymbol)
extern "C"  int32_t BMSymbolGenerated_ilo_get_advance8_m2384181414 (Il2CppObject * __this /* static, unused */, BMSymbol_t1170982339 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BMSymbolGenerated::ilo_Validate9(BMSymbol,UIAtlas)
extern "C"  bool BMSymbolGenerated_ilo_Validate9_m4104115731 (Il2CppObject * __this /* static, unused */, BMSymbol_t1170982339 * ____this0, UIAtlas_t281921111 * ___atlas1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
