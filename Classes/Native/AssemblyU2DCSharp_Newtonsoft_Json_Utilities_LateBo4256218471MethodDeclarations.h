﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateGet>c__AnonStorey144`1<System.Object>
struct U3CCreateGetU3Ec__AnonStorey144_1_t4256218471;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateGet>c__AnonStorey144`1<System.Object>::.ctor()
extern "C"  void U3CCreateGetU3Ec__AnonStorey144_1__ctor_m647981718_gshared (U3CCreateGetU3Ec__AnonStorey144_1_t4256218471 * __this, const MethodInfo* method);
#define U3CCreateGetU3Ec__AnonStorey144_1__ctor_m647981718(__this, method) ((  void (*) (U3CCreateGetU3Ec__AnonStorey144_1_t4256218471 *, const MethodInfo*))U3CCreateGetU3Ec__AnonStorey144_1__ctor_m647981718_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateGet>c__AnonStorey144`1<System.Object>::<>m__3AB(T)
extern "C"  Il2CppObject * U3CCreateGetU3Ec__AnonStorey144_1_U3CU3Em__3AB_m3822223036_gshared (U3CCreateGetU3Ec__AnonStorey144_1_t4256218471 * __this, Il2CppObject * ___o0, const MethodInfo* method);
#define U3CCreateGetU3Ec__AnonStorey144_1_U3CU3Em__3AB_m3822223036(__this, ___o0, method) ((  Il2CppObject * (*) (U3CCreateGetU3Ec__AnonStorey144_1_t4256218471 *, Il2CppObject *, const MethodInfo*))U3CCreateGetU3Ec__AnonStorey144_1_U3CU3Em__3AB_m3822223036_gshared)(__this, ___o0, method)
