﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZMGGenerated
struct ZMGGenerated_t3324575867;
// JSVCall
struct JSVCall_t3708497963;
// System.Action
struct Action_t3771233898;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// TimeMgr
struct TimeMgr_t350708715;
// EffectMgr
struct EffectMgr_t535289511;
// ZiShiYingMgr
struct ZiShiYingMgr_t4004457962;
// Mihua.Assets.SubAssetMgr
struct SubAssetMgr_t3564963414;
// CameraShotMgr
struct CameraShotMgr_t1580128697;
// AntiCheatMgr
struct AntiCheatMgr_t3474304487;
// System.Delegate
struct Delegate_t3310234105;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "System_Core_System_Action3771233898.h"

// System.Void ZMGGenerated::.ctor()
extern "C"  void ZMGGenerated__ctor_m4278733952 (ZMGGenerated_t3324575867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZMGGenerated::ZMG_ZMG1(JSVCall,System.Int32)
extern "C"  bool ZMGGenerated_ZMG_ZMG1_m513792912 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ZMG_isNull(JSVCall)
extern "C"  void ZMGGenerated_ZMG_isNull_m3609604423 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ZMG_GlobalGOMgr(JSVCall)
extern "C"  void ZMGGenerated_ZMG_GlobalGOMgr_m4109586119 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ZMG_SceneMgr(JSVCall)
extern "C"  void ZMGGenerated_ZMG_SceneMgr_m1396929260 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ZMG_TimeMgr(JSVCall)
extern "C"  void ZMGGenerated_ZMG_TimeMgr_m1779312073 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ZMG_TimeUpdateMgr(JSVCall)
extern "C"  void ZMGGenerated_ZMG_TimeUpdateMgr_m1795071218 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ZMG_EffectMgr(JSVCall)
extern "C"  void ZMGGenerated_ZMG_EffectMgr_m699155213 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ZMG_SoundMgr(JSVCall)
extern "C"  void ZMGGenerated_ZMG_SoundMgr_m2504881903 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ZMG_TeamSkillMgr(JSVCall)
extern "C"  void ZMGGenerated_ZMG_TeamSkillMgr_m3731869524 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ZMG_UIEffectMgr(JSVCall)
extern "C"  void ZMGGenerated_ZMG_UIEffectMgr_m2756015393 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ZMG_FloatTextMgr(JSVCall)
extern "C"  void ZMGGenerated_ZMG_FloatTextMgr_m1356114825 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ZMG_CSGameDataMgr(JSVCall)
extern "C"  void ZMGGenerated_ZMG_CSGameDataMgr_m3630274856 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ZMG_StoryMgr(JSVCall)
extern "C"  void ZMGGenerated_ZMG_StoryMgr_m4198038997 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ZMG_CfgDataMgr(JSVCall)
extern "C"  void ZMGGenerated_ZMG_CfgDataMgr_m861003022 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ZMG_ZiShiYingMgr(JSVCall)
extern "C"  void ZMGGenerated_ZMG_ZiShiYingMgr_m3756397518 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ZMG_PluginsSdkMgr(JSVCall)
extern "C"  void ZMGGenerated_ZMG_PluginsSdkMgr_m3488200054 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ZMG_SubAssetMgr(JSVCall)
extern "C"  void ZMGGenerated_ZMG_SubAssetMgr_m3966223372 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ZMG_GameQutilyCtr(JSVCall)
extern "C"  void ZMGGenerated_ZMG_GameQutilyCtr_m1886013323 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ZMG_CameraShotMgr(JSVCall)
extern "C"  void ZMGGenerated_ZMG_CameraShotMgr_m2895168827 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ZMG_AntiCheatMgr(JSVCall)
extern "C"  void ZMGGenerated_ZMG_AntiCheatMgr_m3992141937 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ZMG_ProductsCfgMgr(JSVCall)
extern "C"  void ZMGGenerated_ZMG_ProductsCfgMgr_m3635094336 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ZMG_MihuaPayMgr(JSVCall)
extern "C"  void ZMGGenerated_ZMG_MihuaPayMgr_m3563284268 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action ZMGGenerated::ZMG_Logout_GetDelegate_member0_arg0(CSRepresentedObject)
extern "C"  Action_t3771233898 * ZMGGenerated_ZMG_Logout_GetDelegate_member0_arg0_m1105017835 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZMGGenerated::ZMG_Logout__Action(JSVCall,System.Int32)
extern "C"  bool ZMGGenerated_ZMG_Logout__Action_m2376746867 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::__Register()
extern "C"  void ZMGGenerated___Register_m374669383 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action ZMGGenerated::<ZMG_Logout__Action>m__2FC()
extern "C"  Action_t3771233898 * ZMGGenerated_U3CZMG_Logout__ActionU3Em__2FC_m2024442395 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void ZMGGenerated_ilo_addJSCSRel1_m256615228 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZMGGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t ZMGGenerated_ilo_setObject2_m4272132939 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TimeMgr ZMGGenerated::ilo_get_TimeMgr3()
extern "C"  TimeMgr_t350708715 * ZMGGenerated_ilo_get_TimeMgr3_m1444622288 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EffectMgr ZMGGenerated::ilo_get_EffectMgr4()
extern "C"  EffectMgr_t535289511 * ZMGGenerated_ilo_get_EffectMgr4_m602051225 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZiShiYingMgr ZMGGenerated::ilo_get_ZiShiYingMgr5()
extern "C"  ZiShiYingMgr_t4004457962 * ZMGGenerated_ilo_get_ZiShiYingMgr5_m4087383770 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Assets.SubAssetMgr ZMGGenerated::ilo_get_SubAssetMgr6()
extern "C"  SubAssetMgr_t3564963414 * ZMGGenerated_ilo_get_SubAssetMgr6_m807025464 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CameraShotMgr ZMGGenerated::ilo_get_CameraShotMgr7()
extern "C"  CameraShotMgr_t1580128697 * ZMGGenerated_ilo_get_CameraShotMgr7_m309359416 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AntiCheatMgr ZMGGenerated::ilo_get_AntiCheatMgr8()
extern "C"  AntiCheatMgr_t3474304487 * ZMGGenerated_ilo_get_AntiCheatMgr8_m808210237 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ilo_addJSFunCSDelegateRel9(System.Int32,System.Delegate)
extern "C"  void ZMGGenerated_ilo_addJSFunCSDelegateRel9_m2650304602 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZMGGenerated::ilo_Logout10(System.Action)
extern "C"  void ZMGGenerated_ilo_Logout10_m2660120157 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZMGGenerated::ilo_isFunctionS11(System.Int32)
extern "C"  bool ZMGGenerated_ilo_isFunctionS11_m2839675865 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject ZMGGenerated::ilo_getFunctionS12(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * ZMGGenerated_ilo_getFunctionS12_m1465031883 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
