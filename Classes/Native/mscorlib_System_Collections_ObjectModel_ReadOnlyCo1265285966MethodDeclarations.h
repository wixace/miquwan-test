﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>
struct ReadOnlyCollection_1_t1265285966;
// System.Collections.Generic.IList`1<UnityEngine.RaycastHit>
struct IList_1_t2402855633;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t528650843;
// System.Collections.Generic.IEnumerator`1<UnityEngine.RaycastHit>
struct IEnumerator_1_t1620073479;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3872051439_gshared (ReadOnlyCollection_1_t1265285966 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3872051439(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1265285966 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3872051439_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3891196761_gshared (ReadOnlyCollection_1_t1265285966 * __this, RaycastHit_t4003175726  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3891196761(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1265285966 *, RaycastHit_t4003175726 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3891196761_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2374706321_gshared (ReadOnlyCollection_1_t1265285966 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2374706321(__this, method) ((  void (*) (ReadOnlyCollection_1_t1265285966 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2374706321_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3508132608_gshared (ReadOnlyCollection_1_t1265285966 * __this, int32_t ___index0, RaycastHit_t4003175726  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3508132608(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1265285966 *, int32_t, RaycastHit_t4003175726 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3508132608_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3427441726_gshared (ReadOnlyCollection_1_t1265285966 * __this, RaycastHit_t4003175726  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3427441726(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1265285966 *, RaycastHit_t4003175726 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3427441726_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1381985478_gshared (ReadOnlyCollection_1_t1265285966 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1381985478(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1265285966 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1381985478_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  RaycastHit_t4003175726  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3214336748_gshared (ReadOnlyCollection_1_t1265285966 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3214336748(__this, ___index0, method) ((  RaycastHit_t4003175726  (*) (ReadOnlyCollection_1_t1265285966 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3214336748_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1575705559_gshared (ReadOnlyCollection_1_t1265285966 * __this, int32_t ___index0, RaycastHit_t4003175726  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1575705559(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1265285966 *, int32_t, RaycastHit_t4003175726 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1575705559_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2035187601_gshared (ReadOnlyCollection_1_t1265285966 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2035187601(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1265285966 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2035187601_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3293697566_gshared (ReadOnlyCollection_1_t1265285966 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3293697566(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1265285966 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3293697566_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2621814701_gshared (ReadOnlyCollection_1_t1265285966 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2621814701(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1265285966 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2621814701_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1015852976_gshared (ReadOnlyCollection_1_t1265285966 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1015852976(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1265285966 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1015852976_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m3895385388_gshared (ReadOnlyCollection_1_t1265285966 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3895385388(__this, method) ((  void (*) (ReadOnlyCollection_1_t1265285966 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3895385388_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m4212507536_gshared (ReadOnlyCollection_1_t1265285966 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m4212507536(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1265285966 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m4212507536_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1905102344_gshared (ReadOnlyCollection_1_t1265285966 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1905102344(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1265285966 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1905102344_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3187889019_gshared (ReadOnlyCollection_1_t1265285966 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3187889019(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1265285966 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m3187889019_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m698829261_gshared (ReadOnlyCollection_1_t1265285966 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m698829261(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1265285966 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m698829261_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1774226315_gshared (ReadOnlyCollection_1_t1265285966 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1774226315(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1265285966 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1774226315_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m157559244_gshared (ReadOnlyCollection_1_t1265285966 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m157559244(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1265285966 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m157559244_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4249264190_gshared (ReadOnlyCollection_1_t1265285966 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4249264190(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1265285966 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4249264190_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3747307263_gshared (ReadOnlyCollection_1_t1265285966 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3747307263(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1265285966 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3747307263_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1960363034_gshared (ReadOnlyCollection_1_t1265285966 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1960363034(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1265285966 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1960363034_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m94599749_gshared (ReadOnlyCollection_1_t1265285966 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m94599749(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1265285966 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m94599749_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1863617682_gshared (ReadOnlyCollection_1_t1265285966 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1863617682(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1265285966 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1863617682_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3621371779_gshared (ReadOnlyCollection_1_t1265285966 * __this, RaycastHit_t4003175726  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3621371779(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1265285966 *, RaycastHit_t4003175726 , const MethodInfo*))ReadOnlyCollection_1_Contains_m3621371779_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m4031086985_gshared (ReadOnlyCollection_1_t1265285966 * __this, RaycastHitU5BU5D_t528650843* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m4031086985(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1265285966 *, RaycastHitU5BU5D_t528650843*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m4031086985_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2012157530_gshared (ReadOnlyCollection_1_t1265285966 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2012157530(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1265285966 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2012157530_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2696330517_gshared (ReadOnlyCollection_1_t1265285966 * __this, RaycastHit_t4003175726  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2696330517(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1265285966 *, RaycastHit_t4003175726 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2696330517_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1799174918_gshared (ReadOnlyCollection_1_t1265285966 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1799174918(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1265285966 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1799174918_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>::get_Item(System.Int32)
extern "C"  RaycastHit_t4003175726  ReadOnlyCollection_1_get_Item_m614369964_gshared (ReadOnlyCollection_1_t1265285966 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m614369964(__this, ___index0, method) ((  RaycastHit_t4003175726  (*) (ReadOnlyCollection_1_t1265285966 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m614369964_gshared)(__this, ___index0, method)
