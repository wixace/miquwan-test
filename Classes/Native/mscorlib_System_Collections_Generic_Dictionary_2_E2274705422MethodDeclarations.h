﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2770839788MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,UnityEngine.GameObject>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2498843511(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2274705422 *, Dictionary_2_t957382030 *, const MethodInfo*))Enumerator__ctor_m2853155855_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,UnityEngine.GameObject>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1426478986(__this, method) ((  Il2CppObject * (*) (Enumerator_t2274705422 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2332623676_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,UnityEngine.GameObject>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m983900382(__this, method) ((  void (*) (Enumerator_t2274705422 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3227554118_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,UnityEngine.GameObject>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2212330855(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2274705422 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2126232061_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,UnityEngine.GameObject>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2287224038(__this, method) ((  Il2CppObject * (*) (Enumerator_t2274705422 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1182786328_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,UnityEngine.GameObject>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m47481336(__this, method) ((  Il2CppObject * (*) (Enumerator_t2274705422 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3834731434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,UnityEngine.GameObject>::MoveNext()
#define Enumerator_MoveNext_m2686456266(__this, method) ((  bool (*) (Enumerator_t2274705422 *, const MethodInfo*))Enumerator_MoveNext_m4054555382_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,UnityEngine.GameObject>::get_Current()
#define Enumerator_get_Current_m4119715110(__this, method) ((  KeyValuePair_2_t856162736  (*) (Enumerator_t2274705422 *, const MethodInfo*))Enumerator_get_Current_m2779016966_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,UnityEngine.GameObject>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m4212480791(__this, method) ((  Int3_t1974045594  (*) (Enumerator_t2274705422 *, const MethodInfo*))Enumerator_get_CurrentKey_m2864030207_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,UnityEngine.GameObject>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m4005989115(__this, method) ((  GameObject_t3674682005 * (*) (Enumerator_t2274705422 *, const MethodInfo*))Enumerator_get_CurrentValue_m2181899647_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,UnityEngine.GameObject>::Reset()
#define Enumerator_Reset_m130853705(__this, method) ((  void (*) (Enumerator_t2274705422 *, const MethodInfo*))Enumerator_Reset_m3428296673_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,UnityEngine.GameObject>::VerifyState()
#define Enumerator_VerifyState_m270180498(__this, method) ((  void (*) (Enumerator_t2274705422 *, const MethodInfo*))Enumerator_VerifyState_m3492530986_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,UnityEngine.GameObject>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m395325306(__this, method) ((  void (*) (Enumerator_t2274705422 *, const MethodInfo*))Enumerator_VerifyCurrent_m402723858_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,UnityEngine.GameObject>::Dispose()
#define Enumerator_Dispose_m3719787993(__this, method) ((  void (*) (Enumerator_t2274705422 *, const MethodInfo*))Enumerator_Dispose_m2876615793_gshared)(__this, method)
