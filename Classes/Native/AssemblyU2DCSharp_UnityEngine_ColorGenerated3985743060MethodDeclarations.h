﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ColorGenerated
struct UnityEngine_ColorGenerated_t3985743060;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_ColorGenerated::.ctor()
extern "C"  void UnityEngine_ColorGenerated__ctor_m3135765063 (UnityEngine_ColorGenerated_t3985743060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::.cctor()
extern "C"  void UnityEngine_ColorGenerated__cctor_m2237340230 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_Color1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_Color1_m1751400971 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_Color2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_Color2_m2996165452 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_Color3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_Color3_m4240929933 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::Color_r(JSVCall)
extern "C"  void UnityEngine_ColorGenerated_Color_r_m1329683916 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::Color_g(JSVCall)
extern "C"  void UnityEngine_ColorGenerated_Color_g_m3491332471 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::Color_b(JSVCall)
extern "C"  void UnityEngine_ColorGenerated_Color_b_m178932700 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::Color_a(JSVCall)
extern "C"  void UnityEngine_ColorGenerated_Color_a_m375446205 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::Color_red(JSVCall)
extern "C"  void UnityEngine_ColorGenerated_Color_red_m1101077133 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::Color_green(JSVCall)
extern "C"  void UnityEngine_ColorGenerated_Color_green_m364144219 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::Color_blue(JSVCall)
extern "C"  void UnityEngine_ColorGenerated_Color_blue_m2162786036 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::Color_white(JSVCall)
extern "C"  void UnityEngine_ColorGenerated_Color_white_m604689397 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::Color_black(JSVCall)
extern "C"  void UnityEngine_ColorGenerated_Color_black_m4024500447 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::Color_yellow(JSVCall)
extern "C"  void UnityEngine_ColorGenerated_Color_yellow_m1426998810 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::Color_cyan(JSVCall)
extern "C"  void UnityEngine_ColorGenerated_Color_cyan_m3358125643 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::Color_magenta(JSVCall)
extern "C"  void UnityEngine_ColorGenerated_Color_magenta_m697506165 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::Color_gray(JSVCall)
extern "C"  void UnityEngine_ColorGenerated_Color_gray_m3414253323 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::Color_grey(JSVCall)
extern "C"  void UnityEngine_ColorGenerated_Color_grey_m521415183 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::Color_clear(JSVCall)
extern "C"  void UnityEngine_ColorGenerated_Color_clear_m1958613585 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::Color_grayscale(JSVCall)
extern "C"  void UnityEngine_ColorGenerated_Color_grayscale_m3811832087 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::Color_linear(JSVCall)
extern "C"  void UnityEngine_ColorGenerated_Color_linear_m161125001 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::Color_gamma(JSVCall)
extern "C"  void UnityEngine_ColorGenerated_Color_gamma_m3153656375 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::Color_maxColorComponent(JSVCall)
extern "C"  void UnityEngine_ColorGenerated_Color_maxColorComponent_m1454413920 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::Color_Item_Int32(JSVCall)
extern "C"  void UnityEngine_ColorGenerated_Color_Item_Int32_m2769363948 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_Equals__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_Equals__Object_m2580938971 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_GetHashCode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_GetHashCode_m2769812758 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_ToString__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_ToString__String_m1117435674 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_ToString_m2628899273 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_HSVToRGB__Single__Single__Single__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_HSVToRGB__Single__Single__Single__Boolean_m1077993326 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_HSVToRGB__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_HSVToRGB__Single__Single__Single_m3507036476 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_Lerp__Color__Color__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_Lerp__Color__Color__Single_m2638444860 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_LerpUnclamped__Color__Color__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_LerpUnclamped__Color__Color__Single_m1414893143 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_op_Addition__Color__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_op_Addition__Color__Color_m3218006207 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_op_Division__Color__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_op_Division__Color__Single_m800434461 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_op_Equality__Color__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_op_Equality__Color__Color_m2389570493 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_op_Implicit__Vector4_to_Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_op_Implicit__Vector4_to_Color_m1233484533 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_op_Implicit__Color_to_Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_op_Implicit__Color_to_Vector4_m3989447697 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_op_Inequality__Color__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_op_Inequality__Color__Color_m2814697698 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_op_Multiply__Color__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_op_Multiply__Color__Color_m3611514855 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_op_Multiply__Color__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_op_Multiply__Color__Single_m3595969030 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_op_Multiply__Single__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_op_Multiply__Single__Color_m755005302 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_op_Subtraction__Color__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_op_Subtraction__Color__Color_m2412477651 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::Color_RGBToHSV__Color__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_Color_RGBToHSV__Color__Single__Single__Single_m854389149 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::__Register()
extern "C"  void UnityEngine_ColorGenerated___Register_m2009978592 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_ColorGenerated_ilo_addJSCSRel1_m957233539 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ColorGenerated::ilo_getObject2(System.Int32)
extern "C"  int32_t UnityEngine_ColorGenerated_ilo_getObject2_m1306884076 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_ColorGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_ColorGenerated_ilo_getSingle3_m742938306 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::ilo_attachFinalizerObject4(System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_ilo_attachFinalizerObject4_m3912405051 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::ilo_changeJSObj5(System.Int32,System.Object)
extern "C"  void UnityEngine_ColorGenerated_ilo_changeJSObj5_m3114298758 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::ilo_setSingle6(System.Int32,System.Single)
extern "C"  void UnityEngine_ColorGenerated_ilo_setSingle6_m3984914258 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ColorGenerated::ilo_setObject7(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_ColorGenerated_ilo_setObject7_m1000022697 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::ilo_setBooleanS8(System.Int32,System.Boolean)
extern "C"  void UnityEngine_ColorGenerated_ilo_setBooleanS8_m1533616337 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ColorGenerated::ilo_setInt329(System.Int32,System.Int32)
extern "C"  void UnityEngine_ColorGenerated_ilo_setInt329_m965114215 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ColorGenerated::ilo_getBooleanS10(System.Int32)
extern "C"  bool UnityEngine_ColorGenerated_ilo_getBooleanS10_m4283349647 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_ColorGenerated::ilo_getObject11(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_ColorGenerated_ilo_getObject11_m728652473 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
