﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FileInfoCrc
struct FileInfoCrc_t1217045770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void FileInfoCrc::.ctor()
extern "C"  void FileInfoCrc__ctor_m3869827489 (FileInfoCrc_t1217045770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FileInfoCrc::ToString()
extern "C"  String_t* FileInfoCrc_ToString_m566327404 (FileInfoCrc_t1217045770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FileInfoCrc::LoadByString(System.String[])
extern "C"  void FileInfoCrc_LoadByString_m2890493231 (FileInfoCrc_t1217045770 * __this, StringU5BU5D_t4054002952* ___infoArr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
