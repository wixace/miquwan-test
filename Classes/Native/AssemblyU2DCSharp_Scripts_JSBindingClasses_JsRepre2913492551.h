﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Scripts_JSBindingClasses_JsRepres508009210.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Scripts.JSBindingClasses.JsRepresentClass
struct  JsRepresentClass_t2913492551  : public Il2CppObject
{
public:
	// System.Int32 Scripts.JSBindingClasses.JsRepresentClass::jsObjID
	int32_t ___jsObjID_0;
	// Scripts.JSBindingClasses.JsRepresentClass/InitStatus Scripts.JSBindingClasses.JsRepresentClass::status
	int32_t ___status_1;
	// System.String Scripts.JSBindingClasses.JsRepresentClass::ScriptName
	String_t* ___ScriptName_2;

public:
	inline static int32_t get_offset_of_jsObjID_0() { return static_cast<int32_t>(offsetof(JsRepresentClass_t2913492551, ___jsObjID_0)); }
	inline int32_t get_jsObjID_0() const { return ___jsObjID_0; }
	inline int32_t* get_address_of_jsObjID_0() { return &___jsObjID_0; }
	inline void set_jsObjID_0(int32_t value)
	{
		___jsObjID_0 = value;
	}

	inline static int32_t get_offset_of_status_1() { return static_cast<int32_t>(offsetof(JsRepresentClass_t2913492551, ___status_1)); }
	inline int32_t get_status_1() const { return ___status_1; }
	inline int32_t* get_address_of_status_1() { return &___status_1; }
	inline void set_status_1(int32_t value)
	{
		___status_1 = value;
	}

	inline static int32_t get_offset_of_ScriptName_2() { return static_cast<int32_t>(offsetof(JsRepresentClass_t2913492551, ___ScriptName_2)); }
	inline String_t* get_ScriptName_2() const { return ___ScriptName_2; }
	inline String_t** get_address_of_ScriptName_2() { return &___ScriptName_2; }
	inline void set_ScriptName_2(String_t* value)
	{
		___ScriptName_2 = value;
		Il2CppCodeGenWriteBarrier(&___ScriptName_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
