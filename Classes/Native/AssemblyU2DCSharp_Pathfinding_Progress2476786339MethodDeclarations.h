﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// Pathfinding.Progress
struct Progress_t2476786339;
struct Progress_t2476786339_marshaled_pinvoke;
struct Progress_t2476786339_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Progress2476786339.h"
#include "mscorlib_System_String7231557.h"

// System.Void Pathfinding.Progress::.ctor(System.Single,System.String)
extern "C"  void Progress__ctor_m1811395843 (Progress_t2476786339 * __this, float ___p0, String_t* ___d1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Progress_t2476786339;
struct Progress_t2476786339_marshaled_pinvoke;

extern "C" void Progress_t2476786339_marshal_pinvoke(const Progress_t2476786339& unmarshaled, Progress_t2476786339_marshaled_pinvoke& marshaled);
extern "C" void Progress_t2476786339_marshal_pinvoke_back(const Progress_t2476786339_marshaled_pinvoke& marshaled, Progress_t2476786339& unmarshaled);
extern "C" void Progress_t2476786339_marshal_pinvoke_cleanup(Progress_t2476786339_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Progress_t2476786339;
struct Progress_t2476786339_marshaled_com;

extern "C" void Progress_t2476786339_marshal_com(const Progress_t2476786339& unmarshaled, Progress_t2476786339_marshaled_com& marshaled);
extern "C" void Progress_t2476786339_marshal_com_back(const Progress_t2476786339_marshaled_com& marshaled, Progress_t2476786339& unmarshaled);
extern "C" void Progress_t2476786339_marshal_com_cleanup(Progress_t2476786339_marshaled_com& marshaled);
