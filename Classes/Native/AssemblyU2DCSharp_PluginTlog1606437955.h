﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Security.Cryptography.HMACSHA1
struct HMACSHA1_t4024365272;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginTlog
struct  PluginTlog_t1606437955  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginTlog::qq_appid
	String_t* ___qq_appid_6;
	// System.String PluginTlog::qq_appkey
	String_t* ___qq_appkey_7;
	// System.String PluginTlog::wx_appid
	String_t* ___wx_appid_8;
	// System.String PluginTlog::wx_appkey
	String_t* ___wx_appkey_9;
	// System.String PluginTlog::appid
	String_t* ___appid_10;
	// System.String PluginTlog::loginType
	String_t* ___loginType_11;
	// System.String PluginTlog::openid
	String_t* ___openid_12;
	// System.String PluginTlog::zoneid
	String_t* ___zoneid_13;
	// System.String PluginTlog::zonename
	String_t* ___zonename_14;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginTlog::headers
	Dictionary_2_t827649927 * ___headers_15;
	// System.Security.Cryptography.HMACSHA1 PluginTlog::_HMACSHA1
	HMACSHA1_t4024365272 * ____HMACSHA1_16;

public:
	inline static int32_t get_offset_of_qq_appid_6() { return static_cast<int32_t>(offsetof(PluginTlog_t1606437955, ___qq_appid_6)); }
	inline String_t* get_qq_appid_6() const { return ___qq_appid_6; }
	inline String_t** get_address_of_qq_appid_6() { return &___qq_appid_6; }
	inline void set_qq_appid_6(String_t* value)
	{
		___qq_appid_6 = value;
		Il2CppCodeGenWriteBarrier(&___qq_appid_6, value);
	}

	inline static int32_t get_offset_of_qq_appkey_7() { return static_cast<int32_t>(offsetof(PluginTlog_t1606437955, ___qq_appkey_7)); }
	inline String_t* get_qq_appkey_7() const { return ___qq_appkey_7; }
	inline String_t** get_address_of_qq_appkey_7() { return &___qq_appkey_7; }
	inline void set_qq_appkey_7(String_t* value)
	{
		___qq_appkey_7 = value;
		Il2CppCodeGenWriteBarrier(&___qq_appkey_7, value);
	}

	inline static int32_t get_offset_of_wx_appid_8() { return static_cast<int32_t>(offsetof(PluginTlog_t1606437955, ___wx_appid_8)); }
	inline String_t* get_wx_appid_8() const { return ___wx_appid_8; }
	inline String_t** get_address_of_wx_appid_8() { return &___wx_appid_8; }
	inline void set_wx_appid_8(String_t* value)
	{
		___wx_appid_8 = value;
		Il2CppCodeGenWriteBarrier(&___wx_appid_8, value);
	}

	inline static int32_t get_offset_of_wx_appkey_9() { return static_cast<int32_t>(offsetof(PluginTlog_t1606437955, ___wx_appkey_9)); }
	inline String_t* get_wx_appkey_9() const { return ___wx_appkey_9; }
	inline String_t** get_address_of_wx_appkey_9() { return &___wx_appkey_9; }
	inline void set_wx_appkey_9(String_t* value)
	{
		___wx_appkey_9 = value;
		Il2CppCodeGenWriteBarrier(&___wx_appkey_9, value);
	}

	inline static int32_t get_offset_of_appid_10() { return static_cast<int32_t>(offsetof(PluginTlog_t1606437955, ___appid_10)); }
	inline String_t* get_appid_10() const { return ___appid_10; }
	inline String_t** get_address_of_appid_10() { return &___appid_10; }
	inline void set_appid_10(String_t* value)
	{
		___appid_10 = value;
		Il2CppCodeGenWriteBarrier(&___appid_10, value);
	}

	inline static int32_t get_offset_of_loginType_11() { return static_cast<int32_t>(offsetof(PluginTlog_t1606437955, ___loginType_11)); }
	inline String_t* get_loginType_11() const { return ___loginType_11; }
	inline String_t** get_address_of_loginType_11() { return &___loginType_11; }
	inline void set_loginType_11(String_t* value)
	{
		___loginType_11 = value;
		Il2CppCodeGenWriteBarrier(&___loginType_11, value);
	}

	inline static int32_t get_offset_of_openid_12() { return static_cast<int32_t>(offsetof(PluginTlog_t1606437955, ___openid_12)); }
	inline String_t* get_openid_12() const { return ___openid_12; }
	inline String_t** get_address_of_openid_12() { return &___openid_12; }
	inline void set_openid_12(String_t* value)
	{
		___openid_12 = value;
		Il2CppCodeGenWriteBarrier(&___openid_12, value);
	}

	inline static int32_t get_offset_of_zoneid_13() { return static_cast<int32_t>(offsetof(PluginTlog_t1606437955, ___zoneid_13)); }
	inline String_t* get_zoneid_13() const { return ___zoneid_13; }
	inline String_t** get_address_of_zoneid_13() { return &___zoneid_13; }
	inline void set_zoneid_13(String_t* value)
	{
		___zoneid_13 = value;
		Il2CppCodeGenWriteBarrier(&___zoneid_13, value);
	}

	inline static int32_t get_offset_of_zonename_14() { return static_cast<int32_t>(offsetof(PluginTlog_t1606437955, ___zonename_14)); }
	inline String_t* get_zonename_14() const { return ___zonename_14; }
	inline String_t** get_address_of_zonename_14() { return &___zonename_14; }
	inline void set_zonename_14(String_t* value)
	{
		___zonename_14 = value;
		Il2CppCodeGenWriteBarrier(&___zonename_14, value);
	}

	inline static int32_t get_offset_of_headers_15() { return static_cast<int32_t>(offsetof(PluginTlog_t1606437955, ___headers_15)); }
	inline Dictionary_2_t827649927 * get_headers_15() const { return ___headers_15; }
	inline Dictionary_2_t827649927 ** get_address_of_headers_15() { return &___headers_15; }
	inline void set_headers_15(Dictionary_2_t827649927 * value)
	{
		___headers_15 = value;
		Il2CppCodeGenWriteBarrier(&___headers_15, value);
	}

	inline static int32_t get_offset_of__HMACSHA1_16() { return static_cast<int32_t>(offsetof(PluginTlog_t1606437955, ____HMACSHA1_16)); }
	inline HMACSHA1_t4024365272 * get__HMACSHA1_16() const { return ____HMACSHA1_16; }
	inline HMACSHA1_t4024365272 ** get_address_of__HMACSHA1_16() { return &____HMACSHA1_16; }
	inline void set__HMACSHA1_16(HMACSHA1_t4024365272 * value)
	{
		____HMACSHA1_16 = value;
		Il2CppCodeGenWriteBarrier(&____HMACSHA1_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
