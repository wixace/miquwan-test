﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_casereWhoulur146
struct M_casereWhoulur146_t3623643156;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_casereWhoulur146::.ctor()
extern "C"  void M_casereWhoulur146__ctor_m3132746911 (M_casereWhoulur146_t3623643156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_casereWhoulur146::M_meldrooTeargelsa0(System.String[],System.Int32)
extern "C"  void M_casereWhoulur146_M_meldrooTeargelsa0_m1070991226 (M_casereWhoulur146_t3623643156 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_casereWhoulur146::M_cise1(System.String[],System.Int32)
extern "C"  void M_casereWhoulur146_M_cise1_m3406489051 (M_casereWhoulur146_t3623643156 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_casereWhoulur146::M_notear2(System.String[],System.Int32)
extern "C"  void M_casereWhoulur146_M_notear2_m4234551249 (M_casereWhoulur146_t3623643156 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_casereWhoulur146::M_kuberkou3(System.String[],System.Int32)
extern "C"  void M_casereWhoulur146_M_kuberkou3_m275047817 (M_casereWhoulur146_t3623643156 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_casereWhoulur146::M_wayberiNererow4(System.String[],System.Int32)
extern "C"  void M_casereWhoulur146_M_wayberiNererow4_m3778773871 (M_casereWhoulur146_t3623643156 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_casereWhoulur146::M_tisfawFoujo5(System.String[],System.Int32)
extern "C"  void M_casereWhoulur146_M_tisfawFoujo5_m2821616560 (M_casereWhoulur146_t3623643156 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_casereWhoulur146::M_meseCelsaldai6(System.String[],System.Int32)
extern "C"  void M_casereWhoulur146_M_meseCelsaldai6_m3296406038 (M_casereWhoulur146_t3623643156 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
