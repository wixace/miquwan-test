﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// shaderBuff
struct shaderBuff_t2982920664;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void shaderBuff::.ctor(System.Single,System.Single,System.Single,UnityEngine.Color,System.Single,UnityEngine.Vector3)
extern "C"  void shaderBuff__ctor_m3581077290 (shaderBuff_t2982920664 * __this, float ___duration0, float ___interval1, float ___Intension2, Color_t4194546905  ____RimColor3, float ___susanooScale4, Vector3_t4282066566  ___offset5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void shaderBuff::.ctor(UnityEngine.Color,UnityEngine.Color,System.Single,UnityEngine.Vector2)
extern "C"  void shaderBuff__ctor_m4073360764 (shaderBuff_t2982920664 * __this, Color_t4194546905  ___colorThreshold0, Color_t4194546905  ___bloomColor1, float ___bloomFactor2, Vector2_t4282066565  ___blurCenter3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
