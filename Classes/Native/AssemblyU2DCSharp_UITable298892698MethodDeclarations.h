﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UITable
struct UITable_t298892698;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t3027308338;
// UIPanel
struct UIPanel_t295209936;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_UIWidget_Pivot240933195.h"
#include "AssemblyU2DCSharp_UITable298892698.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"

// System.Void UITable::.ctor()
extern "C"  void UITable__ctor_m3607830289 (UITable_t298892698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITable::set_repositionNow(System.Boolean)
extern "C"  void UITable_set_repositionNow_m82937923 (UITable_t298892698 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Transform> UITable::GetChildList()
extern "C"  List_1_t3027308338 * UITable_GetChildList_m1703713889 (UITable_t298892698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITable::Sort(System.Collections.Generic.List`1<UnityEngine.Transform>)
extern "C"  void UITable_Sort_m1046555786 (UITable_t298892698 * __this, List_1_t3027308338 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITable::Start()
extern "C"  void UITable_Start_m2554968081 (UITable_t298892698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITable::Init()
extern "C"  void UITable_Init_m4224338211 (UITable_t298892698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITable::LateUpdate()
extern "C"  void UITable_LateUpdate_m1407708642 (UITable_t298892698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITable::OnValidate()
extern "C"  void UITable_OnValidate_m3957087592 (UITable_t298892698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITable::RepositionVariableSize(System.Collections.Generic.List`1<UnityEngine.Transform>)
extern "C"  void UITable_RepositionVariableSize_m3372084677 (UITable_t298892698 * __this, List_1_t3027308338 * ___children0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITable::Reposition()
extern "C"  void UITable_Reposition_m2768170991 (UITable_t298892698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UITable::ilo_GetPivotOffset1(UIWidget/Pivot)
extern "C"  Vector2_t4282066565  UITable_ilo_GetPivotOffset1_m1947166558 (Il2CppObject * __this /* static, unused */, int32_t ___pv0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITable::ilo_Init2(UITable)
extern "C"  void UITable_ilo_Init2_m2157488154 (Il2CppObject * __this /* static, unused */, UITable_t298892698 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Transform> UITable::ilo_GetChildList3(UITable)
extern "C"  List_1_t3027308338 * UITable_ilo_GetChildList3_m4000220791 (Il2CppObject * __this /* static, unused */, UITable_t298892698 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITable::ilo_RepositionVariableSize4(UITable,System.Collections.Generic.List`1<UnityEngine.Transform>)
extern "C"  void UITable_ilo_RepositionVariableSize4_m3156386078 (Il2CppObject * __this /* static, unused */, UITable_t298892698 * ____this0, List_1_t3027308338 * ___children1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITable::ilo_ConstrainTargetToBounds5(UIPanel,UnityEngine.Transform,System.Boolean)
extern "C"  bool UITable_ilo_ConstrainTargetToBounds5_m2660301427 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, Transform_t1659122786 * ___target1, bool ___immediate2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
