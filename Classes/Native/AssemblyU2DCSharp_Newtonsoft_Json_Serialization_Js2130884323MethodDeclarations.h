﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<CreateObjectFromNonDefaultConstructor>c__AnonStorey135
struct U3CCreateObjectFromNonDefaultConstructorU3Ec__AnonStorey135_t2130884323;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21873923011.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22851306027.h"

// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<CreateObjectFromNonDefaultConstructor>c__AnonStorey135::.ctor()
extern "C"  void U3CCreateObjectFromNonDefaultConstructorU3Ec__AnonStorey135__ctor_m1148991768 (U3CCreateObjectFromNonDefaultConstructorU3Ec__AnonStorey135_t2130884323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<CreateObjectFromNonDefaultConstructor>c__AnonStorey135::<>m__38C(System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.JsonProperty,System.Object>)
extern "C"  void U3CCreateObjectFromNonDefaultConstructorU3Ec__AnonStorey135_U3CU3Em__38C_m2928465766 (U3CCreateObjectFromNonDefaultConstructorU3Ec__AnonStorey135_t2130884323 * __this, KeyValuePair_2_t1873923011  ___propertyValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<CreateObjectFromNonDefaultConstructor>c__AnonStorey135::<>m__38D(System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.JsonProperty,System.Object>)
extern "C"  void U3CCreateObjectFromNonDefaultConstructorU3Ec__AnonStorey135_U3CU3Em__38D_m1995111429 (U3CCreateObjectFromNonDefaultConstructorU3Ec__AnonStorey135_t2130884323 * __this, KeyValuePair_2_t1873923011  ___remainingPropertyValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<CreateObjectFromNonDefaultConstructor>c__AnonStorey135::<>m__390(System.Collections.Generic.KeyValuePair`2<System.Reflection.ParameterInfo,System.Object>)
extern "C"  String_t* U3CCreateObjectFromNonDefaultConstructorU3Ec__AnonStorey135_U3CU3Em__390_m2483473318 (Il2CppObject * __this /* static, unused */, KeyValuePair_2_t2851306027  ___kv0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
