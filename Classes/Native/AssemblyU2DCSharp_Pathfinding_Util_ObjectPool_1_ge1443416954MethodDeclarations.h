﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Pathfinding.Util.ObjectPool`1<System.Object>::.cctor()
extern "C"  void ObjectPool_1__cctor_m3373051104_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ObjectPool_1__cctor_m3373051104(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1__cctor_m3373051104_gshared)(__this /* static, unused */, method)
// T Pathfinding.Util.ObjectPool`1<System.Object>::Claim()
extern "C"  Il2CppObject * ObjectPool_1_Claim_m1866735784_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ObjectPool_1_Claim_m1866735784(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1_Claim_m1866735784_gshared)(__this /* static, unused */, method)
// System.Void Pathfinding.Util.ObjectPool`1<System.Object>::Warmup(System.Int32)
extern "C"  void ObjectPool_1_Warmup_m3419418728_gshared (Il2CppObject * __this /* static, unused */, int32_t ___count0, const MethodInfo* method);
#define ObjectPool_1_Warmup_m3419418728(__this /* static, unused */, ___count0, method) ((  void (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))ObjectPool_1_Warmup_m3419418728_gshared)(__this /* static, unused */, ___count0, method)
// System.Void Pathfinding.Util.ObjectPool`1<System.Object>::Release(T)
extern "C"  void ObjectPool_1_Release_m2971981388_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method);
#define ObjectPool_1_Release_m2971981388(__this /* static, unused */, ___obj0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))ObjectPool_1_Release_m2971981388_gshared)(__this /* static, unused */, ___obj0, method)
// System.Void Pathfinding.Util.ObjectPool`1<System.Object>::Clear()
extern "C"  void ObjectPool_1_Clear_m4180764824_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ObjectPool_1_Clear_m4180764824(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1_Clear_m4180764824_gshared)(__this /* static, unused */, method)
// System.Int32 Pathfinding.Util.ObjectPool`1<System.Object>::GetSize()
extern "C"  int32_t ObjectPool_1_GetSize_m3459990516_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ObjectPool_1_GetSize_m3459990516(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1_GetSize_m3459990516_gshared)(__this /* static, unused */, method)
