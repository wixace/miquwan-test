﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SoundMgrGenerated
struct SoundMgrGenerated_t3368594662;
// JSVCall
struct JSVCall_t3708497963;
// SoundMgr
struct SoundMgr_t1807284905;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_SoundMgr1807284905.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_SoundTypeID3626616740.h"

// System.Void SoundMgrGenerated::.ctor()
extern "C"  void SoundMgrGenerated__ctor_m34087749 (SoundMgrGenerated_t3368594662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SoundMgrGenerated::SoundMgr_SoundMgr1(JSVCall,System.Int32)
extern "C"  bool SoundMgrGenerated_SoundMgr_SoundMgr1_m1573596589 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgrGenerated::SoundMgr_SOUND_PATH(JSVCall)
extern "C"  void SoundMgrGenerated_SoundMgr_SOUND_PATH_m894685913 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgrGenerated::SoundMgr_mute(JSVCall)
extern "C"  void SoundMgrGenerated_SoundMgr_mute_m3315595445 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SoundMgrGenerated::SoundMgr_Pause__String(JSVCall,System.Int32)
extern "C"  bool SoundMgrGenerated_SoundMgr_Pause__String_m3050151852 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SoundMgrGenerated::SoundMgr_PlaySound__Int32(JSVCall,System.Int32)
extern "C"  bool SoundMgrGenerated_SoundMgr_PlaySound__Int32_m423113328 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SoundMgrGenerated::SoundMgr_RePlay__String(JSVCall,System.Int32)
extern "C"  bool SoundMgrGenerated_SoundMgr_RePlay__String_m2711261397 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SoundMgrGenerated::SoundMgr_RestoreMute__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool SoundMgrGenerated_SoundMgr_RestoreMute__Boolean__Boolean_m2941911724 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SoundMgrGenerated::SoundMgr_SetBlackCancelPause(JSVCall,System.Int32)
extern "C"  bool SoundMgrGenerated_SoundMgr_SetBlackCancelPause_m2403803460 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SoundMgrGenerated::SoundMgr_SetBlackPause(JSVCall,System.Int32)
extern "C"  bool SoundMgrGenerated_SoundMgr_SetBlackPause_m2003132766 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SoundMgrGenerated::SoundMgr_SetMute__Boolean__SoundTypeID(JSVCall,System.Int32)
extern "C"  bool SoundMgrGenerated_SoundMgr_SetMute__Boolean__SoundTypeID_m926835612 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SoundMgrGenerated::SoundMgr_StopLoopSound(JSVCall,System.Int32)
extern "C"  bool SoundMgrGenerated_SoundMgr_StopLoopSound_m2944351502 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SoundMgrGenerated::SoundMgr_StopSound__Int32(JSVCall,System.Int32)
extern "C"  bool SoundMgrGenerated_SoundMgr_StopSound__Int32_m2554657726 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SoundMgrGenerated::SoundMgr_StopSound__SoundTypeID(JSVCall,System.Int32)
extern "C"  bool SoundMgrGenerated_SoundMgr_StopSound__SoundTypeID_m4015955252 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SoundMgrGenerated::SoundMgr_StopSound(JSVCall,System.Int32)
extern "C"  bool SoundMgrGenerated_SoundMgr_StopSound_m3454511954 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SoundMgrGenerated::SoundMgr_StopSoundByID__Int32(JSVCall,System.Int32)
extern "C"  bool SoundMgrGenerated_SoundMgr_StopSoundByID__Int32_m1834479020 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SoundMgrGenerated::SoundMgr_ZUpdate(JSVCall,System.Int32)
extern "C"  bool SoundMgrGenerated_SoundMgr_ZUpdate_m2868257672 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgrGenerated::__Register()
extern "C"  void SoundMgrGenerated___Register_m2549456034 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SoundMgrGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t SoundMgrGenerated_ilo_getObject1_m614852497 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SoundMgrGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool SoundMgrGenerated_ilo_attachFinalizerObject2_m503162515 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgrGenerated::ilo_set_mute3(SoundMgr,System.Boolean)
extern "C"  void SoundMgrGenerated_ilo_set_mute3_m2411867705 (Il2CppObject * __this /* static, unused */, SoundMgr_t1807284905 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SoundMgrGenerated::ilo_getStringS4(System.Int32)
extern "C"  String_t* SoundMgrGenerated_ilo_getStringS4_m3791884716 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgrGenerated::ilo_Pause5(SoundMgr,System.String)
extern "C"  void SoundMgrGenerated_ilo_Pause5_m1357335556 (Il2CppObject * __this /* static, unused */, SoundMgr_t1807284905 * ____this0, String_t* ___typeKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgrGenerated::ilo_RestoreMute6(SoundMgr,System.Boolean,System.Boolean)
extern "C"  void SoundMgrGenerated_ilo_RestoreMute6_m1710667704 (Il2CppObject * __this /* static, unused */, SoundMgr_t1807284905 * ____this0, bool ___mute1, bool ___isChat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgrGenerated::ilo_SetBlackCancelPause7(SoundMgr)
extern "C"  void SoundMgrGenerated_ilo_SetBlackCancelPause7_m871740737 (Il2CppObject * __this /* static, unused */, SoundMgr_t1807284905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SoundMgrGenerated::ilo_getEnum8(System.Int32)
extern "C"  int32_t SoundMgrGenerated_ilo_getEnum8_m4050278550 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgrGenerated::ilo_SetMute9(SoundMgr,System.Boolean,SoundTypeID)
extern "C"  void SoundMgrGenerated_ilo_SetMute9_m3188479070 (Il2CppObject * __this /* static, unused */, SoundMgr_t1807284905 * ____this0, bool ___mute1, int32_t ___soundType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgrGenerated::ilo_StopLoopSound10(SoundMgr)
extern "C"  void SoundMgrGenerated_ilo_StopLoopSound10_m4190142287 (Il2CppObject * __this /* static, unused */, SoundMgr_t1807284905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgrGenerated::ilo_StopSound11(SoundMgr,System.Int32)
extern "C"  void SoundMgrGenerated_ilo_StopSound11_m3091405443 (Il2CppObject * __this /* static, unused */, SoundMgr_t1807284905 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgrGenerated::ilo_StopSound12(SoundMgr)
extern "C"  void SoundMgrGenerated_ilo_StopSound12_m1069411733 (Il2CppObject * __this /* static, unused */, SoundMgr_t1807284905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgrGenerated::ilo_ZUpdate13(SoundMgr)
extern "C"  void SoundMgrGenerated_ilo_ZUpdate13_m2646210252 (Il2CppObject * __this /* static, unused */, SoundMgr_t1807284905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
