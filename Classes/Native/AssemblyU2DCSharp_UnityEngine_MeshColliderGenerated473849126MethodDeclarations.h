﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_MeshColliderGenerated
struct UnityEngine_MeshColliderGenerated_t473849126;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_MeshColliderGenerated::.ctor()
extern "C"  void UnityEngine_MeshColliderGenerated__ctor_m1568533253 (UnityEngine_MeshColliderGenerated_t473849126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshColliderGenerated::MeshCollider_MeshCollider1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshColliderGenerated_MeshCollider_MeshCollider1_m925430765 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshColliderGenerated::MeshCollider_sharedMesh(JSVCall)
extern "C"  void UnityEngine_MeshColliderGenerated_MeshCollider_sharedMesh_m2570844340 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshColliderGenerated::MeshCollider_convex(JSVCall)
extern "C"  void UnityEngine_MeshColliderGenerated_MeshCollider_convex_m2223024479 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshColliderGenerated::__Register()
extern "C"  void UnityEngine_MeshColliderGenerated___Register_m4188450018 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MeshColliderGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_MeshColliderGenerated_ilo_getObject1_m2884359121 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MeshColliderGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_MeshColliderGenerated_ilo_setObject2_m1837633930 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshColliderGenerated::ilo_getBooleanS3(System.Int32)
extern "C"  bool UnityEngine_MeshColliderGenerated_ilo_getBooleanS3_m127622593 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
