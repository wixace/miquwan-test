﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._cf94381410fe54b0cde1308e3b813427
struct _cf94381410fe54b0cde1308e3b813427_t3623958767;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._cf94381410fe54b0cde1308e3b813427::.ctor()
extern "C"  void _cf94381410fe54b0cde1308e3b813427__ctor_m1982958110 (_cf94381410fe54b0cde1308e3b813427_t3623958767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._cf94381410fe54b0cde1308e3b813427::_cf94381410fe54b0cde1308e3b813427m2(System.Int32)
extern "C"  int32_t _cf94381410fe54b0cde1308e3b813427__cf94381410fe54b0cde1308e3b813427m2_m3338826553 (_cf94381410fe54b0cde1308e3b813427_t3623958767 * __this, int32_t ____cf94381410fe54b0cde1308e3b813427a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._cf94381410fe54b0cde1308e3b813427::_cf94381410fe54b0cde1308e3b813427m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _cf94381410fe54b0cde1308e3b813427__cf94381410fe54b0cde1308e3b813427m_m217401245 (_cf94381410fe54b0cde1308e3b813427_t3623958767 * __this, int32_t ____cf94381410fe54b0cde1308e3b813427a0, int32_t ____cf94381410fe54b0cde1308e3b813427811, int32_t ____cf94381410fe54b0cde1308e3b813427c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
