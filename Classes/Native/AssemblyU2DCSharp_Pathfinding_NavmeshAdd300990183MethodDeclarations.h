﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.NavmeshAdd
struct NavmeshAdd_t300990183;
// System.Collections.Generic.List`1<Pathfinding.NavmeshAdd>
struct List_1_t1669175735;
// Pathfinding.Int3[]
struct Int3U5BU5D_t516284607;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_NavmeshAdd300990183.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

// System.Void Pathfinding.NavmeshAdd::.ctor()
extern "C"  void NavmeshAdd__ctor_m1889976672 (NavmeshAdd_t300990183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshAdd::.cctor()
extern "C"  void NavmeshAdd__cctor_m2272605773 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshAdd::Add(Pathfinding.NavmeshAdd)
extern "C"  void NavmeshAdd_Add_m1824119652 (Il2CppObject * __this /* static, unused */, NavmeshAdd_t300990183 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshAdd::Remove(Pathfinding.NavmeshAdd)
extern "C"  void NavmeshAdd_Remove_m4205749037 (Il2CppObject * __this /* static, unused */, NavmeshAdd_t300990183 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Pathfinding.NavmeshAdd> Pathfinding.NavmeshAdd::GetAllInRange(UnityEngine.Bounds)
extern "C"  List_1_t1669175735 * NavmeshAdd_GetAllInRange_m1306920849 (Il2CppObject * __this /* static, unused */, Bounds_t2711641849  ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavmeshAdd::Intersects(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool NavmeshAdd_Intersects_m4157768058 (Il2CppObject * __this /* static, unused */, Bounds_t2711641849  ___b10, Bounds_t2711641849  ___b21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Pathfinding.NavmeshAdd> Pathfinding.NavmeshAdd::GetAll()
extern "C"  List_1_t1669175735 * NavmeshAdd_GetAll_m3947985341 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshAdd::Awake()
extern "C"  void NavmeshAdd_Awake_m2127581891 (NavmeshAdd_t300990183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshAdd::OnEnable()
extern "C"  void NavmeshAdd_OnEnable_m3640330790 (NavmeshAdd_t300990183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshAdd::OnDestroy()
extern "C"  void NavmeshAdd_OnDestroy_m3799574105 (NavmeshAdd_t300990183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.NavmeshAdd::get_Center()
extern "C"  Vector3_t4282066566  NavmeshAdd_get_Center_m218834940 (NavmeshAdd_t300990183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshAdd::RebuildMesh()
extern "C"  void NavmeshAdd_RebuildMesh_m1010724710 (NavmeshAdd_t300990183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds Pathfinding.NavmeshAdd::GetBounds()
extern "C"  Bounds_t2711641849  NavmeshAdd_GetBounds_m483986424 (NavmeshAdd_t300990183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshAdd::GetMesh(Pathfinding.Int3,Pathfinding.Int3[]&,System.Int32[]&)
extern "C"  void NavmeshAdd_GetMesh_m777417344 (NavmeshAdd_t300990183 * __this, Int3_t1974045594  ___offset0, Int3U5BU5D_t516284607** ___vbuffer1, Int32U5BU5D_t3230847821** ___tbuffer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshAdd::ilo_Add1(Pathfinding.NavmeshAdd)
extern "C"  void NavmeshAdd_ilo_Add1_m2960558412 (Il2CppObject * __this /* static, unused */, NavmeshAdd_t300990183 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshAdd::ilo_Remove2(Pathfinding.NavmeshAdd)
extern "C"  void NavmeshAdd_ilo_Remove2_m924459614 (Il2CppObject * __this /* static, unused */, NavmeshAdd_t300990183 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.NavmeshAdd::ilo_op_Explicit3(UnityEngine.Vector3)
extern "C"  Int3_t1974045594  NavmeshAdd_ilo_op_Explicit3_m2202767506 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.NavmeshAdd::ilo_op_Addition4(Pathfinding.Int3,Pathfinding.Int3)
extern "C"  Int3_t1974045594  NavmeshAdd_ilo_op_Addition4_m239368422 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, Int3_t1974045594  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
