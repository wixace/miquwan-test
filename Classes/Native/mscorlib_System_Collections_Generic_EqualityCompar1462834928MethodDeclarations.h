﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.IntRect>
struct DefaultComparer_t1462834928;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_IntRect3015058261.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.IntRect>::.ctor()
extern "C"  void DefaultComparer__ctor_m150590855_gshared (DefaultComparer_t1462834928 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m150590855(__this, method) ((  void (*) (DefaultComparer_t1462834928 *, const MethodInfo*))DefaultComparer__ctor_m150590855_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.IntRect>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3319242820_gshared (DefaultComparer_t1462834928 * __this, IntRect_t3015058261  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3319242820(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1462834928 *, IntRect_t3015058261 , const MethodInfo*))DefaultComparer_GetHashCode_m3319242820_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.IntRect>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3593739992_gshared (DefaultComparer_t1462834928 * __this, IntRect_t3015058261  ___x0, IntRect_t3015058261  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3593739992(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1462834928 *, IntRect_t3015058261 , IntRect_t3015058261 , const MethodInfo*))DefaultComparer_Equals_m3593739992_gshared)(__this, ___x0, ___y1, method)
