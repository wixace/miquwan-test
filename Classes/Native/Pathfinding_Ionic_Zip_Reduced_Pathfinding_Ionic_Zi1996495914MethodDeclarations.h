﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Ionic.Zip.WriteDelegate
struct WriteDelegate_t1996495914;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IO.Stream
struct Stream_t1561764144;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void Pathfinding.Ionic.Zip.WriteDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void WriteDelegate__ctor_m3877162565 (WriteDelegate_t1996495914 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Ionic.Zip.WriteDelegate::Invoke(System.String,System.IO.Stream)
extern "C"  void WriteDelegate_Invoke_m3416700666 (WriteDelegate_t1996495914 * __this, String_t* ___entryName0, Stream_t1561764144 * ___stream1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Pathfinding.Ionic.Zip.WriteDelegate::BeginInvoke(System.String,System.IO.Stream,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * WriteDelegate_BeginInvoke_m1765896535 (WriteDelegate_t1996495914 * __this, String_t* ___entryName0, Stream_t1561764144 * ___stream1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Ionic.Zip.WriteDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void WriteDelegate_EndInvoke_m2981112533 (WriteDelegate_t1996495914 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
