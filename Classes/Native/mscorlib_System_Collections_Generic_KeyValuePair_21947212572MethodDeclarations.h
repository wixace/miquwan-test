﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21947212572.h"
#include "AssemblyU2DCSharp_SoundTypeID3626616740.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<SoundTypeID,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2265684409_gshared (KeyValuePair_2_t1947212572 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m2265684409(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1947212572 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m2265684409_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<SoundTypeID,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m50681231_gshared (KeyValuePair_2_t1947212572 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m50681231(__this, method) ((  int32_t (*) (KeyValuePair_2_t1947212572 *, const MethodInfo*))KeyValuePair_2_get_Key_m50681231_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<SoundTypeID,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3522613456_gshared (KeyValuePair_2_t1947212572 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3522613456(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1947212572 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3522613456_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<SoundTypeID,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1051595507_gshared (KeyValuePair_2_t1947212572 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1051595507(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1947212572 *, const MethodInfo*))KeyValuePair_2_get_Value_m1051595507_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<SoundTypeID,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1507671248_gshared (KeyValuePair_2_t1947212572 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1507671248(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1947212572 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m1507671248_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<SoundTypeID,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2770852088_gshared (KeyValuePair_2_t1947212572 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2770852088(__this, method) ((  String_t* (*) (KeyValuePair_2_t1947212572 *, const MethodInfo*))KeyValuePair_2_ToString_m2770852088_gshared)(__this, method)
