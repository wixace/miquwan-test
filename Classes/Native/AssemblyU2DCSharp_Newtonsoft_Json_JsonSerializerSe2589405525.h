﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>
struct IList_1_t559366761;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_t507353639;
// Newtonsoft.Json.Serialization.IReferenceResolver
struct IReferenceResolver_t425424564;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_t2137423328;
// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>
struct EventHandler_1_t937589677;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ReferenceLoopHan2761661122.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_MissingMemberHan2077487315.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_NullValueHandlin2754652381.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_DefaultValueHand1569448045.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ObjectCreationHand56081595.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_PreserveReferenc4230591217.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ConstructorHandl2475221485.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_TypeNameHandling2359325474.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_F3005881063.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonSerializerSettings
struct  JsonSerializerSettings_t2589405525  : public Il2CppObject
{
public:
	// Newtonsoft.Json.ReferenceLoopHandling Newtonsoft.Json.JsonSerializerSettings::<ReferenceLoopHandling>k__BackingField
	int32_t ___U3CReferenceLoopHandlingU3Ek__BackingField_10;
	// Newtonsoft.Json.MissingMemberHandling Newtonsoft.Json.JsonSerializerSettings::<MissingMemberHandling>k__BackingField
	int32_t ___U3CMissingMemberHandlingU3Ek__BackingField_11;
	// Newtonsoft.Json.ObjectCreationHandling Newtonsoft.Json.JsonSerializerSettings::<ObjectCreationHandling>k__BackingField
	int32_t ___U3CObjectCreationHandlingU3Ek__BackingField_12;
	// Newtonsoft.Json.NullValueHandling Newtonsoft.Json.JsonSerializerSettings::<NullValueHandling>k__BackingField
	int32_t ___U3CNullValueHandlingU3Ek__BackingField_13;
	// Newtonsoft.Json.DefaultValueHandling Newtonsoft.Json.JsonSerializerSettings::<DefaultValueHandling>k__BackingField
	int32_t ___U3CDefaultValueHandlingU3Ek__BackingField_14;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter> Newtonsoft.Json.JsonSerializerSettings::<Converters>k__BackingField
	Il2CppObject* ___U3CConvertersU3Ek__BackingField_15;
	// Newtonsoft.Json.PreserveReferencesHandling Newtonsoft.Json.JsonSerializerSettings::<PreserveReferencesHandling>k__BackingField
	int32_t ___U3CPreserveReferencesHandlingU3Ek__BackingField_16;
	// Newtonsoft.Json.TypeNameHandling Newtonsoft.Json.JsonSerializerSettings::<TypeNameHandling>k__BackingField
	int32_t ___U3CTypeNameHandlingU3Ek__BackingField_17;
	// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle Newtonsoft.Json.JsonSerializerSettings::<TypeNameAssemblyFormat>k__BackingField
	int32_t ___U3CTypeNameAssemblyFormatU3Ek__BackingField_18;
	// Newtonsoft.Json.ConstructorHandling Newtonsoft.Json.JsonSerializerSettings::<ConstructorHandling>k__BackingField
	int32_t ___U3CConstructorHandlingU3Ek__BackingField_19;
	// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializerSettings::<ContractResolver>k__BackingField
	Il2CppObject * ___U3CContractResolverU3Ek__BackingField_20;
	// Newtonsoft.Json.Serialization.IReferenceResolver Newtonsoft.Json.JsonSerializerSettings::<ReferenceResolver>k__BackingField
	Il2CppObject * ___U3CReferenceResolverU3Ek__BackingField_21;
	// System.Runtime.Serialization.SerializationBinder Newtonsoft.Json.JsonSerializerSettings::<Binder>k__BackingField
	SerializationBinder_t2137423328 * ___U3CBinderU3Ek__BackingField_22;
	// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs> Newtonsoft.Json.JsonSerializerSettings::<Error>k__BackingField
	EventHandler_1_t937589677 * ___U3CErrorU3Ek__BackingField_23;
	// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializerSettings::<Context>k__BackingField
	StreamingContext_t2761351129  ___U3CContextU3Ek__BackingField_24;

public:
	inline static int32_t get_offset_of_U3CReferenceLoopHandlingU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2589405525, ___U3CReferenceLoopHandlingU3Ek__BackingField_10)); }
	inline int32_t get_U3CReferenceLoopHandlingU3Ek__BackingField_10() const { return ___U3CReferenceLoopHandlingU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CReferenceLoopHandlingU3Ek__BackingField_10() { return &___U3CReferenceLoopHandlingU3Ek__BackingField_10; }
	inline void set_U3CReferenceLoopHandlingU3Ek__BackingField_10(int32_t value)
	{
		___U3CReferenceLoopHandlingU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CMissingMemberHandlingU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2589405525, ___U3CMissingMemberHandlingU3Ek__BackingField_11)); }
	inline int32_t get_U3CMissingMemberHandlingU3Ek__BackingField_11() const { return ___U3CMissingMemberHandlingU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CMissingMemberHandlingU3Ek__BackingField_11() { return &___U3CMissingMemberHandlingU3Ek__BackingField_11; }
	inline void set_U3CMissingMemberHandlingU3Ek__BackingField_11(int32_t value)
	{
		___U3CMissingMemberHandlingU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CObjectCreationHandlingU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2589405525, ___U3CObjectCreationHandlingU3Ek__BackingField_12)); }
	inline int32_t get_U3CObjectCreationHandlingU3Ek__BackingField_12() const { return ___U3CObjectCreationHandlingU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CObjectCreationHandlingU3Ek__BackingField_12() { return &___U3CObjectCreationHandlingU3Ek__BackingField_12; }
	inline void set_U3CObjectCreationHandlingU3Ek__BackingField_12(int32_t value)
	{
		___U3CObjectCreationHandlingU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CNullValueHandlingU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2589405525, ___U3CNullValueHandlingU3Ek__BackingField_13)); }
	inline int32_t get_U3CNullValueHandlingU3Ek__BackingField_13() const { return ___U3CNullValueHandlingU3Ek__BackingField_13; }
	inline int32_t* get_address_of_U3CNullValueHandlingU3Ek__BackingField_13() { return &___U3CNullValueHandlingU3Ek__BackingField_13; }
	inline void set_U3CNullValueHandlingU3Ek__BackingField_13(int32_t value)
	{
		___U3CNullValueHandlingU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultValueHandlingU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2589405525, ___U3CDefaultValueHandlingU3Ek__BackingField_14)); }
	inline int32_t get_U3CDefaultValueHandlingU3Ek__BackingField_14() const { return ___U3CDefaultValueHandlingU3Ek__BackingField_14; }
	inline int32_t* get_address_of_U3CDefaultValueHandlingU3Ek__BackingField_14() { return &___U3CDefaultValueHandlingU3Ek__BackingField_14; }
	inline void set_U3CDefaultValueHandlingU3Ek__BackingField_14(int32_t value)
	{
		___U3CDefaultValueHandlingU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CConvertersU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2589405525, ___U3CConvertersU3Ek__BackingField_15)); }
	inline Il2CppObject* get_U3CConvertersU3Ek__BackingField_15() const { return ___U3CConvertersU3Ek__BackingField_15; }
	inline Il2CppObject** get_address_of_U3CConvertersU3Ek__BackingField_15() { return &___U3CConvertersU3Ek__BackingField_15; }
	inline void set_U3CConvertersU3Ek__BackingField_15(Il2CppObject* value)
	{
		___U3CConvertersU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CConvertersU3Ek__BackingField_15, value);
	}

	inline static int32_t get_offset_of_U3CPreserveReferencesHandlingU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2589405525, ___U3CPreserveReferencesHandlingU3Ek__BackingField_16)); }
	inline int32_t get_U3CPreserveReferencesHandlingU3Ek__BackingField_16() const { return ___U3CPreserveReferencesHandlingU3Ek__BackingField_16; }
	inline int32_t* get_address_of_U3CPreserveReferencesHandlingU3Ek__BackingField_16() { return &___U3CPreserveReferencesHandlingU3Ek__BackingField_16; }
	inline void set_U3CPreserveReferencesHandlingU3Ek__BackingField_16(int32_t value)
	{
		___U3CPreserveReferencesHandlingU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CTypeNameHandlingU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2589405525, ___U3CTypeNameHandlingU3Ek__BackingField_17)); }
	inline int32_t get_U3CTypeNameHandlingU3Ek__BackingField_17() const { return ___U3CTypeNameHandlingU3Ek__BackingField_17; }
	inline int32_t* get_address_of_U3CTypeNameHandlingU3Ek__BackingField_17() { return &___U3CTypeNameHandlingU3Ek__BackingField_17; }
	inline void set_U3CTypeNameHandlingU3Ek__BackingField_17(int32_t value)
	{
		___U3CTypeNameHandlingU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CTypeNameAssemblyFormatU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2589405525, ___U3CTypeNameAssemblyFormatU3Ek__BackingField_18)); }
	inline int32_t get_U3CTypeNameAssemblyFormatU3Ek__BackingField_18() const { return ___U3CTypeNameAssemblyFormatU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CTypeNameAssemblyFormatU3Ek__BackingField_18() { return &___U3CTypeNameAssemblyFormatU3Ek__BackingField_18; }
	inline void set_U3CTypeNameAssemblyFormatU3Ek__BackingField_18(int32_t value)
	{
		___U3CTypeNameAssemblyFormatU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CConstructorHandlingU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2589405525, ___U3CConstructorHandlingU3Ek__BackingField_19)); }
	inline int32_t get_U3CConstructorHandlingU3Ek__BackingField_19() const { return ___U3CConstructorHandlingU3Ek__BackingField_19; }
	inline int32_t* get_address_of_U3CConstructorHandlingU3Ek__BackingField_19() { return &___U3CConstructorHandlingU3Ek__BackingField_19; }
	inline void set_U3CConstructorHandlingU3Ek__BackingField_19(int32_t value)
	{
		___U3CConstructorHandlingU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CContractResolverU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2589405525, ___U3CContractResolverU3Ek__BackingField_20)); }
	inline Il2CppObject * get_U3CContractResolverU3Ek__BackingField_20() const { return ___U3CContractResolverU3Ek__BackingField_20; }
	inline Il2CppObject ** get_address_of_U3CContractResolverU3Ek__BackingField_20() { return &___U3CContractResolverU3Ek__BackingField_20; }
	inline void set_U3CContractResolverU3Ek__BackingField_20(Il2CppObject * value)
	{
		___U3CContractResolverU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CContractResolverU3Ek__BackingField_20, value);
	}

	inline static int32_t get_offset_of_U3CReferenceResolverU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2589405525, ___U3CReferenceResolverU3Ek__BackingField_21)); }
	inline Il2CppObject * get_U3CReferenceResolverU3Ek__BackingField_21() const { return ___U3CReferenceResolverU3Ek__BackingField_21; }
	inline Il2CppObject ** get_address_of_U3CReferenceResolverU3Ek__BackingField_21() { return &___U3CReferenceResolverU3Ek__BackingField_21; }
	inline void set_U3CReferenceResolverU3Ek__BackingField_21(Il2CppObject * value)
	{
		___U3CReferenceResolverU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CReferenceResolverU3Ek__BackingField_21, value);
	}

	inline static int32_t get_offset_of_U3CBinderU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2589405525, ___U3CBinderU3Ek__BackingField_22)); }
	inline SerializationBinder_t2137423328 * get_U3CBinderU3Ek__BackingField_22() const { return ___U3CBinderU3Ek__BackingField_22; }
	inline SerializationBinder_t2137423328 ** get_address_of_U3CBinderU3Ek__BackingField_22() { return &___U3CBinderU3Ek__BackingField_22; }
	inline void set_U3CBinderU3Ek__BackingField_22(SerializationBinder_t2137423328 * value)
	{
		___U3CBinderU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CBinderU3Ek__BackingField_22, value);
	}

	inline static int32_t get_offset_of_U3CErrorU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2589405525, ___U3CErrorU3Ek__BackingField_23)); }
	inline EventHandler_1_t937589677 * get_U3CErrorU3Ek__BackingField_23() const { return ___U3CErrorU3Ek__BackingField_23; }
	inline EventHandler_1_t937589677 ** get_address_of_U3CErrorU3Ek__BackingField_23() { return &___U3CErrorU3Ek__BackingField_23; }
	inline void set_U3CErrorU3Ek__BackingField_23(EventHandler_1_t937589677 * value)
	{
		___U3CErrorU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CErrorU3Ek__BackingField_23, value);
	}

	inline static int32_t get_offset_of_U3CContextU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2589405525, ___U3CContextU3Ek__BackingField_24)); }
	inline StreamingContext_t2761351129  get_U3CContextU3Ek__BackingField_24() const { return ___U3CContextU3Ek__BackingField_24; }
	inline StreamingContext_t2761351129 * get_address_of_U3CContextU3Ek__BackingField_24() { return &___U3CContextU3Ek__BackingField_24; }
	inline void set_U3CContextU3Ek__BackingField_24(StreamingContext_t2761351129  value)
	{
		___U3CContextU3Ek__BackingField_24 = value;
	}
};

struct JsonSerializerSettings_t2589405525_StaticFields
{
public:
	// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializerSettings::DefaultContext
	StreamingContext_t2761351129  ___DefaultContext_9;

public:
	inline static int32_t get_offset_of_DefaultContext_9() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2589405525_StaticFields, ___DefaultContext_9)); }
	inline StreamingContext_t2761351129  get_DefaultContext_9() const { return ___DefaultContext_9; }
	inline StreamingContext_t2761351129 * get_address_of_DefaultContext_9() { return &___DefaultContext_9; }
	inline void set_DefaultContext_9(StreamingContext_t2761351129  value)
	{
		___DefaultContext_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
