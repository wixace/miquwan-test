﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_wowmounor168
struct  M_wowmounor168_t2512839334  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_wowmounor168::_hisarhemKabe
	String_t* ____hisarhemKabe_0;
	// System.String GarbageiOS.M_wowmounor168::_xereseaNiscer
	String_t* ____xereseaNiscer_1;
	// System.String GarbageiOS.M_wowmounor168::_xairchallno
	String_t* ____xairchallno_2;
	// System.UInt32 GarbageiOS.M_wowmounor168::_norooteSowli
	uint32_t ____norooteSowli_3;
	// System.UInt32 GarbageiOS.M_wowmounor168::_cowseaSallcordraw
	uint32_t ____cowseaSallcordraw_4;
	// System.Boolean GarbageiOS.M_wowmounor168::_yasaiwiPekear
	bool ____yasaiwiPekear_5;
	// System.String GarbageiOS.M_wowmounor168::_sajeRistai
	String_t* ____sajeRistai_6;
	// System.UInt32 GarbageiOS.M_wowmounor168::_ceyecea
	uint32_t ____ceyecea_7;
	// System.Single GarbageiOS.M_wowmounor168::_rairsalMempaywow
	float ____rairsalMempaywow_8;
	// System.Single GarbageiOS.M_wowmounor168::_stelal
	float ____stelal_9;
	// System.Boolean GarbageiOS.M_wowmounor168::_puteaYirbesur
	bool ____puteaYirbesur_10;
	// System.Int32 GarbageiOS.M_wowmounor168::_palmekuRanou
	int32_t ____palmekuRanou_11;
	// System.UInt32 GarbageiOS.M_wowmounor168::_jereji
	uint32_t ____jereji_12;
	// System.String GarbageiOS.M_wowmounor168::_makay
	String_t* ____makay_13;
	// System.Boolean GarbageiOS.M_wowmounor168::_wona
	bool ____wona_14;
	// System.Single GarbageiOS.M_wowmounor168::_sorgiMorkaw
	float ____sorgiMorkaw_15;
	// System.UInt32 GarbageiOS.M_wowmounor168::_fopoubou
	uint32_t ____fopoubou_16;
	// System.Boolean GarbageiOS.M_wowmounor168::_turcozu
	bool ____turcozu_17;
	// System.String GarbageiOS.M_wowmounor168::_whallbu
	String_t* ____whallbu_18;
	// System.Int32 GarbageiOS.M_wowmounor168::_seteejairMegow
	int32_t ____seteejairMegow_19;

public:
	inline static int32_t get_offset_of__hisarhemKabe_0() { return static_cast<int32_t>(offsetof(M_wowmounor168_t2512839334, ____hisarhemKabe_0)); }
	inline String_t* get__hisarhemKabe_0() const { return ____hisarhemKabe_0; }
	inline String_t** get_address_of__hisarhemKabe_0() { return &____hisarhemKabe_0; }
	inline void set__hisarhemKabe_0(String_t* value)
	{
		____hisarhemKabe_0 = value;
		Il2CppCodeGenWriteBarrier(&____hisarhemKabe_0, value);
	}

	inline static int32_t get_offset_of__xereseaNiscer_1() { return static_cast<int32_t>(offsetof(M_wowmounor168_t2512839334, ____xereseaNiscer_1)); }
	inline String_t* get__xereseaNiscer_1() const { return ____xereseaNiscer_1; }
	inline String_t** get_address_of__xereseaNiscer_1() { return &____xereseaNiscer_1; }
	inline void set__xereseaNiscer_1(String_t* value)
	{
		____xereseaNiscer_1 = value;
		Il2CppCodeGenWriteBarrier(&____xereseaNiscer_1, value);
	}

	inline static int32_t get_offset_of__xairchallno_2() { return static_cast<int32_t>(offsetof(M_wowmounor168_t2512839334, ____xairchallno_2)); }
	inline String_t* get__xairchallno_2() const { return ____xairchallno_2; }
	inline String_t** get_address_of__xairchallno_2() { return &____xairchallno_2; }
	inline void set__xairchallno_2(String_t* value)
	{
		____xairchallno_2 = value;
		Il2CppCodeGenWriteBarrier(&____xairchallno_2, value);
	}

	inline static int32_t get_offset_of__norooteSowli_3() { return static_cast<int32_t>(offsetof(M_wowmounor168_t2512839334, ____norooteSowli_3)); }
	inline uint32_t get__norooteSowli_3() const { return ____norooteSowli_3; }
	inline uint32_t* get_address_of__norooteSowli_3() { return &____norooteSowli_3; }
	inline void set__norooteSowli_3(uint32_t value)
	{
		____norooteSowli_3 = value;
	}

	inline static int32_t get_offset_of__cowseaSallcordraw_4() { return static_cast<int32_t>(offsetof(M_wowmounor168_t2512839334, ____cowseaSallcordraw_4)); }
	inline uint32_t get__cowseaSallcordraw_4() const { return ____cowseaSallcordraw_4; }
	inline uint32_t* get_address_of__cowseaSallcordraw_4() { return &____cowseaSallcordraw_4; }
	inline void set__cowseaSallcordraw_4(uint32_t value)
	{
		____cowseaSallcordraw_4 = value;
	}

	inline static int32_t get_offset_of__yasaiwiPekear_5() { return static_cast<int32_t>(offsetof(M_wowmounor168_t2512839334, ____yasaiwiPekear_5)); }
	inline bool get__yasaiwiPekear_5() const { return ____yasaiwiPekear_5; }
	inline bool* get_address_of__yasaiwiPekear_5() { return &____yasaiwiPekear_5; }
	inline void set__yasaiwiPekear_5(bool value)
	{
		____yasaiwiPekear_5 = value;
	}

	inline static int32_t get_offset_of__sajeRistai_6() { return static_cast<int32_t>(offsetof(M_wowmounor168_t2512839334, ____sajeRistai_6)); }
	inline String_t* get__sajeRistai_6() const { return ____sajeRistai_6; }
	inline String_t** get_address_of__sajeRistai_6() { return &____sajeRistai_6; }
	inline void set__sajeRistai_6(String_t* value)
	{
		____sajeRistai_6 = value;
		Il2CppCodeGenWriteBarrier(&____sajeRistai_6, value);
	}

	inline static int32_t get_offset_of__ceyecea_7() { return static_cast<int32_t>(offsetof(M_wowmounor168_t2512839334, ____ceyecea_7)); }
	inline uint32_t get__ceyecea_7() const { return ____ceyecea_7; }
	inline uint32_t* get_address_of__ceyecea_7() { return &____ceyecea_7; }
	inline void set__ceyecea_7(uint32_t value)
	{
		____ceyecea_7 = value;
	}

	inline static int32_t get_offset_of__rairsalMempaywow_8() { return static_cast<int32_t>(offsetof(M_wowmounor168_t2512839334, ____rairsalMempaywow_8)); }
	inline float get__rairsalMempaywow_8() const { return ____rairsalMempaywow_8; }
	inline float* get_address_of__rairsalMempaywow_8() { return &____rairsalMempaywow_8; }
	inline void set__rairsalMempaywow_8(float value)
	{
		____rairsalMempaywow_8 = value;
	}

	inline static int32_t get_offset_of__stelal_9() { return static_cast<int32_t>(offsetof(M_wowmounor168_t2512839334, ____stelal_9)); }
	inline float get__stelal_9() const { return ____stelal_9; }
	inline float* get_address_of__stelal_9() { return &____stelal_9; }
	inline void set__stelal_9(float value)
	{
		____stelal_9 = value;
	}

	inline static int32_t get_offset_of__puteaYirbesur_10() { return static_cast<int32_t>(offsetof(M_wowmounor168_t2512839334, ____puteaYirbesur_10)); }
	inline bool get__puteaYirbesur_10() const { return ____puteaYirbesur_10; }
	inline bool* get_address_of__puteaYirbesur_10() { return &____puteaYirbesur_10; }
	inline void set__puteaYirbesur_10(bool value)
	{
		____puteaYirbesur_10 = value;
	}

	inline static int32_t get_offset_of__palmekuRanou_11() { return static_cast<int32_t>(offsetof(M_wowmounor168_t2512839334, ____palmekuRanou_11)); }
	inline int32_t get__palmekuRanou_11() const { return ____palmekuRanou_11; }
	inline int32_t* get_address_of__palmekuRanou_11() { return &____palmekuRanou_11; }
	inline void set__palmekuRanou_11(int32_t value)
	{
		____palmekuRanou_11 = value;
	}

	inline static int32_t get_offset_of__jereji_12() { return static_cast<int32_t>(offsetof(M_wowmounor168_t2512839334, ____jereji_12)); }
	inline uint32_t get__jereji_12() const { return ____jereji_12; }
	inline uint32_t* get_address_of__jereji_12() { return &____jereji_12; }
	inline void set__jereji_12(uint32_t value)
	{
		____jereji_12 = value;
	}

	inline static int32_t get_offset_of__makay_13() { return static_cast<int32_t>(offsetof(M_wowmounor168_t2512839334, ____makay_13)); }
	inline String_t* get__makay_13() const { return ____makay_13; }
	inline String_t** get_address_of__makay_13() { return &____makay_13; }
	inline void set__makay_13(String_t* value)
	{
		____makay_13 = value;
		Il2CppCodeGenWriteBarrier(&____makay_13, value);
	}

	inline static int32_t get_offset_of__wona_14() { return static_cast<int32_t>(offsetof(M_wowmounor168_t2512839334, ____wona_14)); }
	inline bool get__wona_14() const { return ____wona_14; }
	inline bool* get_address_of__wona_14() { return &____wona_14; }
	inline void set__wona_14(bool value)
	{
		____wona_14 = value;
	}

	inline static int32_t get_offset_of__sorgiMorkaw_15() { return static_cast<int32_t>(offsetof(M_wowmounor168_t2512839334, ____sorgiMorkaw_15)); }
	inline float get__sorgiMorkaw_15() const { return ____sorgiMorkaw_15; }
	inline float* get_address_of__sorgiMorkaw_15() { return &____sorgiMorkaw_15; }
	inline void set__sorgiMorkaw_15(float value)
	{
		____sorgiMorkaw_15 = value;
	}

	inline static int32_t get_offset_of__fopoubou_16() { return static_cast<int32_t>(offsetof(M_wowmounor168_t2512839334, ____fopoubou_16)); }
	inline uint32_t get__fopoubou_16() const { return ____fopoubou_16; }
	inline uint32_t* get_address_of__fopoubou_16() { return &____fopoubou_16; }
	inline void set__fopoubou_16(uint32_t value)
	{
		____fopoubou_16 = value;
	}

	inline static int32_t get_offset_of__turcozu_17() { return static_cast<int32_t>(offsetof(M_wowmounor168_t2512839334, ____turcozu_17)); }
	inline bool get__turcozu_17() const { return ____turcozu_17; }
	inline bool* get_address_of__turcozu_17() { return &____turcozu_17; }
	inline void set__turcozu_17(bool value)
	{
		____turcozu_17 = value;
	}

	inline static int32_t get_offset_of__whallbu_18() { return static_cast<int32_t>(offsetof(M_wowmounor168_t2512839334, ____whallbu_18)); }
	inline String_t* get__whallbu_18() const { return ____whallbu_18; }
	inline String_t** get_address_of__whallbu_18() { return &____whallbu_18; }
	inline void set__whallbu_18(String_t* value)
	{
		____whallbu_18 = value;
		Il2CppCodeGenWriteBarrier(&____whallbu_18, value);
	}

	inline static int32_t get_offset_of__seteejairMegow_19() { return static_cast<int32_t>(offsetof(M_wowmounor168_t2512839334, ____seteejairMegow_19)); }
	inline int32_t get__seteejairMegow_19() const { return ____seteejairMegow_19; }
	inline int32_t* get_address_of__seteejairMegow_19() { return &____seteejairMegow_19; }
	inline void set__seteejairMegow_19(int32_t value)
	{
		____seteejairMegow_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
