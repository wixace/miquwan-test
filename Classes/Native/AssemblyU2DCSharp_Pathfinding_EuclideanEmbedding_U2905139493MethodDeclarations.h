﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.EuclideanEmbedding/<RecalculatePivots>c__AnonStorey11B
struct U3CRecalculatePivotsU3Ec__AnonStorey11B_t2905139493;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.EuclideanEmbedding/<RecalculatePivots>c__AnonStorey11B::.ctor()
extern "C"  void U3CRecalculatePivotsU3Ec__AnonStorey11B__ctor_m2443641110 (U3CRecalculatePivotsU3Ec__AnonStorey11B_t2905139493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.EuclideanEmbedding/<RecalculatePivots>c__AnonStorey11B::<>m__350(Pathfinding.GraphNode)
extern "C"  bool U3CRecalculatePivotsU3Ec__AnonStorey11B_U3CU3Em__350_m15499879 (U3CRecalculatePivotsU3Ec__AnonStorey11B_t2905139493 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
