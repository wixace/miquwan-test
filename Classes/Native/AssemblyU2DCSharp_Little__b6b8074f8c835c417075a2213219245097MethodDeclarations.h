﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._b6b8074f8c835c417075a221fcd6d9c4
struct _b6b8074f8c835c417075a221fcd6d9c4_t3219245097;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__b6b8074f8c835c417075a2213219245097.h"

// System.Void Little._b6b8074f8c835c417075a221fcd6d9c4::.ctor()
extern "C"  void _b6b8074f8c835c417075a221fcd6d9c4__ctor_m3374677028 (_b6b8074f8c835c417075a221fcd6d9c4_t3219245097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b6b8074f8c835c417075a221fcd6d9c4::_b6b8074f8c835c417075a221fcd6d9c4m2(System.Int32)
extern "C"  int32_t _b6b8074f8c835c417075a221fcd6d9c4__b6b8074f8c835c417075a221fcd6d9c4m2_m1171216633 (_b6b8074f8c835c417075a221fcd6d9c4_t3219245097 * __this, int32_t ____b6b8074f8c835c417075a221fcd6d9c4a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b6b8074f8c835c417075a221fcd6d9c4::_b6b8074f8c835c417075a221fcd6d9c4m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _b6b8074f8c835c417075a221fcd6d9c4__b6b8074f8c835c417075a221fcd6d9c4m_m1442994653 (_b6b8074f8c835c417075a221fcd6d9c4_t3219245097 * __this, int32_t ____b6b8074f8c835c417075a221fcd6d9c4a0, int32_t ____b6b8074f8c835c417075a221fcd6d9c4971, int32_t ____b6b8074f8c835c417075a221fcd6d9c4c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b6b8074f8c835c417075a221fcd6d9c4::ilo__b6b8074f8c835c417075a221fcd6d9c4m21(Little._b6b8074f8c835c417075a221fcd6d9c4,System.Int32)
extern "C"  int32_t _b6b8074f8c835c417075a221fcd6d9c4_ilo__b6b8074f8c835c417075a221fcd6d9c4m21_m3484446224 (Il2CppObject * __this /* static, unused */, _b6b8074f8c835c417075a221fcd6d9c4_t3219245097 * ____this0, int32_t ____b6b8074f8c835c417075a221fcd6d9c4a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
