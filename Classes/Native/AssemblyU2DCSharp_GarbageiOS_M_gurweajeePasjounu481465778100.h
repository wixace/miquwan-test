﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_gurweajeePasjounu48
struct  M_gurweajeePasjounu48_t1465778100  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_gurweajeePasjounu48::_sarkoutee
	int32_t ____sarkoutee_0;
	// System.Int32 GarbageiOS.M_gurweajeePasjounu48::_panaigurKejaldear
	int32_t ____panaigurKejaldear_1;
	// System.UInt32 GarbageiOS.M_gurweajeePasjounu48::_teemar
	uint32_t ____teemar_2;
	// System.UInt32 GarbageiOS.M_gurweajeePasjounu48::_stemsihaCougair
	uint32_t ____stemsihaCougair_3;
	// System.Int32 GarbageiOS.M_gurweajeePasjounu48::_durmechiJepassu
	int32_t ____durmechiJepassu_4;
	// System.UInt32 GarbageiOS.M_gurweajeePasjounu48::_bujoo
	uint32_t ____bujoo_5;
	// System.UInt32 GarbageiOS.M_gurweajeePasjounu48::_kailir
	uint32_t ____kailir_6;
	// System.Single GarbageiOS.M_gurweajeePasjounu48::_sesooteaDerejile
	float ____sesooteaDerejile_7;
	// System.Single GarbageiOS.M_gurweajeePasjounu48::_morerpeeRearra
	float ____morerpeeRearra_8;
	// System.String GarbageiOS.M_gurweajeePasjounu48::_naireBoulay
	String_t* ____naireBoulay_9;
	// System.Boolean GarbageiOS.M_gurweajeePasjounu48::_gounee
	bool ____gounee_10;
	// System.Single GarbageiOS.M_gurweajeePasjounu48::_diditaWeemoucal
	float ____diditaWeemoucal_11;
	// System.Boolean GarbageiOS.M_gurweajeePasjounu48::_papaw
	bool ____papaw_12;
	// System.Int32 GarbageiOS.M_gurweajeePasjounu48::_recidaYempou
	int32_t ____recidaYempou_13;
	// System.Single GarbageiOS.M_gurweajeePasjounu48::_noosemkirTersouyar
	float ____noosemkirTersouyar_14;
	// System.Int32 GarbageiOS.M_gurweajeePasjounu48::_taslirne
	int32_t ____taslirne_15;
	// System.Single GarbageiOS.M_gurweajeePasjounu48::_feacemowSurni
	float ____feacemowSurni_16;
	// System.Boolean GarbageiOS.M_gurweajeePasjounu48::_kofealeeNayme
	bool ____kofealeeNayme_17;
	// System.Boolean GarbageiOS.M_gurweajeePasjounu48::_heresair
	bool ____heresair_18;
	// System.Single GarbageiOS.M_gurweajeePasjounu48::_jofayLarlas
	float ____jofayLarlas_19;
	// System.Int32 GarbageiOS.M_gurweajeePasjounu48::_farsal
	int32_t ____farsal_20;
	// System.Single GarbageiOS.M_gurweajeePasjounu48::_doohooyem
	float ____doohooyem_21;
	// System.Single GarbageiOS.M_gurweajeePasjounu48::_jemowtelTaxawpe
	float ____jemowtelTaxawpe_22;
	// System.Int32 GarbageiOS.M_gurweajeePasjounu48::_tiseDrisdetray
	int32_t ____tiseDrisdetray_23;
	// System.UInt32 GarbageiOS.M_gurweajeePasjounu48::_mairlise
	uint32_t ____mairlise_24;
	// System.Int32 GarbageiOS.M_gurweajeePasjounu48::_chiwaski
	int32_t ____chiwaski_25;
	// System.Single GarbageiOS.M_gurweajeePasjounu48::_tacair
	float ____tacair_26;
	// System.Single GarbageiOS.M_gurweajeePasjounu48::_jarou
	float ____jarou_27;
	// System.Boolean GarbageiOS.M_gurweajeePasjounu48::_bijallHowdrorsear
	bool ____bijallHowdrorsear_28;
	// System.Int32 GarbageiOS.M_gurweajeePasjounu48::_peanuWhatoobo
	int32_t ____peanuWhatoobo_29;
	// System.Boolean GarbageiOS.M_gurweajeePasjounu48::_bamawtow
	bool ____bamawtow_30;
	// System.UInt32 GarbageiOS.M_gurweajeePasjounu48::_celhemcas
	uint32_t ____celhemcas_31;
	// System.String GarbageiOS.M_gurweajeePasjounu48::_cerestem
	String_t* ____cerestem_32;
	// System.String GarbageiOS.M_gurweajeePasjounu48::_sekalu
	String_t* ____sekalu_33;
	// System.UInt32 GarbageiOS.M_gurweajeePasjounu48::_hopar
	uint32_t ____hopar_34;
	// System.Boolean GarbageiOS.M_gurweajeePasjounu48::_dena
	bool ____dena_35;
	// System.Single GarbageiOS.M_gurweajeePasjounu48::_gairday
	float ____gairday_36;
	// System.Boolean GarbageiOS.M_gurweajeePasjounu48::_kiyarMoobasu
	bool ____kiyarMoobasu_37;
	// System.UInt32 GarbageiOS.M_gurweajeePasjounu48::_todaHasperetar
	uint32_t ____todaHasperetar_38;
	// System.Int32 GarbageiOS.M_gurweajeePasjounu48::_heemay
	int32_t ____heemay_39;

public:
	inline static int32_t get_offset_of__sarkoutee_0() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____sarkoutee_0)); }
	inline int32_t get__sarkoutee_0() const { return ____sarkoutee_0; }
	inline int32_t* get_address_of__sarkoutee_0() { return &____sarkoutee_0; }
	inline void set__sarkoutee_0(int32_t value)
	{
		____sarkoutee_0 = value;
	}

	inline static int32_t get_offset_of__panaigurKejaldear_1() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____panaigurKejaldear_1)); }
	inline int32_t get__panaigurKejaldear_1() const { return ____panaigurKejaldear_1; }
	inline int32_t* get_address_of__panaigurKejaldear_1() { return &____panaigurKejaldear_1; }
	inline void set__panaigurKejaldear_1(int32_t value)
	{
		____panaigurKejaldear_1 = value;
	}

	inline static int32_t get_offset_of__teemar_2() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____teemar_2)); }
	inline uint32_t get__teemar_2() const { return ____teemar_2; }
	inline uint32_t* get_address_of__teemar_2() { return &____teemar_2; }
	inline void set__teemar_2(uint32_t value)
	{
		____teemar_2 = value;
	}

	inline static int32_t get_offset_of__stemsihaCougair_3() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____stemsihaCougair_3)); }
	inline uint32_t get__stemsihaCougair_3() const { return ____stemsihaCougair_3; }
	inline uint32_t* get_address_of__stemsihaCougair_3() { return &____stemsihaCougair_3; }
	inline void set__stemsihaCougair_3(uint32_t value)
	{
		____stemsihaCougair_3 = value;
	}

	inline static int32_t get_offset_of__durmechiJepassu_4() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____durmechiJepassu_4)); }
	inline int32_t get__durmechiJepassu_4() const { return ____durmechiJepassu_4; }
	inline int32_t* get_address_of__durmechiJepassu_4() { return &____durmechiJepassu_4; }
	inline void set__durmechiJepassu_4(int32_t value)
	{
		____durmechiJepassu_4 = value;
	}

	inline static int32_t get_offset_of__bujoo_5() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____bujoo_5)); }
	inline uint32_t get__bujoo_5() const { return ____bujoo_5; }
	inline uint32_t* get_address_of__bujoo_5() { return &____bujoo_5; }
	inline void set__bujoo_5(uint32_t value)
	{
		____bujoo_5 = value;
	}

	inline static int32_t get_offset_of__kailir_6() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____kailir_6)); }
	inline uint32_t get__kailir_6() const { return ____kailir_6; }
	inline uint32_t* get_address_of__kailir_6() { return &____kailir_6; }
	inline void set__kailir_6(uint32_t value)
	{
		____kailir_6 = value;
	}

	inline static int32_t get_offset_of__sesooteaDerejile_7() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____sesooteaDerejile_7)); }
	inline float get__sesooteaDerejile_7() const { return ____sesooteaDerejile_7; }
	inline float* get_address_of__sesooteaDerejile_7() { return &____sesooteaDerejile_7; }
	inline void set__sesooteaDerejile_7(float value)
	{
		____sesooteaDerejile_7 = value;
	}

	inline static int32_t get_offset_of__morerpeeRearra_8() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____morerpeeRearra_8)); }
	inline float get__morerpeeRearra_8() const { return ____morerpeeRearra_8; }
	inline float* get_address_of__morerpeeRearra_8() { return &____morerpeeRearra_8; }
	inline void set__morerpeeRearra_8(float value)
	{
		____morerpeeRearra_8 = value;
	}

	inline static int32_t get_offset_of__naireBoulay_9() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____naireBoulay_9)); }
	inline String_t* get__naireBoulay_9() const { return ____naireBoulay_9; }
	inline String_t** get_address_of__naireBoulay_9() { return &____naireBoulay_9; }
	inline void set__naireBoulay_9(String_t* value)
	{
		____naireBoulay_9 = value;
		Il2CppCodeGenWriteBarrier(&____naireBoulay_9, value);
	}

	inline static int32_t get_offset_of__gounee_10() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____gounee_10)); }
	inline bool get__gounee_10() const { return ____gounee_10; }
	inline bool* get_address_of__gounee_10() { return &____gounee_10; }
	inline void set__gounee_10(bool value)
	{
		____gounee_10 = value;
	}

	inline static int32_t get_offset_of__diditaWeemoucal_11() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____diditaWeemoucal_11)); }
	inline float get__diditaWeemoucal_11() const { return ____diditaWeemoucal_11; }
	inline float* get_address_of__diditaWeemoucal_11() { return &____diditaWeemoucal_11; }
	inline void set__diditaWeemoucal_11(float value)
	{
		____diditaWeemoucal_11 = value;
	}

	inline static int32_t get_offset_of__papaw_12() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____papaw_12)); }
	inline bool get__papaw_12() const { return ____papaw_12; }
	inline bool* get_address_of__papaw_12() { return &____papaw_12; }
	inline void set__papaw_12(bool value)
	{
		____papaw_12 = value;
	}

	inline static int32_t get_offset_of__recidaYempou_13() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____recidaYempou_13)); }
	inline int32_t get__recidaYempou_13() const { return ____recidaYempou_13; }
	inline int32_t* get_address_of__recidaYempou_13() { return &____recidaYempou_13; }
	inline void set__recidaYempou_13(int32_t value)
	{
		____recidaYempou_13 = value;
	}

	inline static int32_t get_offset_of__noosemkirTersouyar_14() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____noosemkirTersouyar_14)); }
	inline float get__noosemkirTersouyar_14() const { return ____noosemkirTersouyar_14; }
	inline float* get_address_of__noosemkirTersouyar_14() { return &____noosemkirTersouyar_14; }
	inline void set__noosemkirTersouyar_14(float value)
	{
		____noosemkirTersouyar_14 = value;
	}

	inline static int32_t get_offset_of__taslirne_15() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____taslirne_15)); }
	inline int32_t get__taslirne_15() const { return ____taslirne_15; }
	inline int32_t* get_address_of__taslirne_15() { return &____taslirne_15; }
	inline void set__taslirne_15(int32_t value)
	{
		____taslirne_15 = value;
	}

	inline static int32_t get_offset_of__feacemowSurni_16() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____feacemowSurni_16)); }
	inline float get__feacemowSurni_16() const { return ____feacemowSurni_16; }
	inline float* get_address_of__feacemowSurni_16() { return &____feacemowSurni_16; }
	inline void set__feacemowSurni_16(float value)
	{
		____feacemowSurni_16 = value;
	}

	inline static int32_t get_offset_of__kofealeeNayme_17() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____kofealeeNayme_17)); }
	inline bool get__kofealeeNayme_17() const { return ____kofealeeNayme_17; }
	inline bool* get_address_of__kofealeeNayme_17() { return &____kofealeeNayme_17; }
	inline void set__kofealeeNayme_17(bool value)
	{
		____kofealeeNayme_17 = value;
	}

	inline static int32_t get_offset_of__heresair_18() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____heresair_18)); }
	inline bool get__heresair_18() const { return ____heresair_18; }
	inline bool* get_address_of__heresair_18() { return &____heresair_18; }
	inline void set__heresair_18(bool value)
	{
		____heresair_18 = value;
	}

	inline static int32_t get_offset_of__jofayLarlas_19() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____jofayLarlas_19)); }
	inline float get__jofayLarlas_19() const { return ____jofayLarlas_19; }
	inline float* get_address_of__jofayLarlas_19() { return &____jofayLarlas_19; }
	inline void set__jofayLarlas_19(float value)
	{
		____jofayLarlas_19 = value;
	}

	inline static int32_t get_offset_of__farsal_20() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____farsal_20)); }
	inline int32_t get__farsal_20() const { return ____farsal_20; }
	inline int32_t* get_address_of__farsal_20() { return &____farsal_20; }
	inline void set__farsal_20(int32_t value)
	{
		____farsal_20 = value;
	}

	inline static int32_t get_offset_of__doohooyem_21() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____doohooyem_21)); }
	inline float get__doohooyem_21() const { return ____doohooyem_21; }
	inline float* get_address_of__doohooyem_21() { return &____doohooyem_21; }
	inline void set__doohooyem_21(float value)
	{
		____doohooyem_21 = value;
	}

	inline static int32_t get_offset_of__jemowtelTaxawpe_22() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____jemowtelTaxawpe_22)); }
	inline float get__jemowtelTaxawpe_22() const { return ____jemowtelTaxawpe_22; }
	inline float* get_address_of__jemowtelTaxawpe_22() { return &____jemowtelTaxawpe_22; }
	inline void set__jemowtelTaxawpe_22(float value)
	{
		____jemowtelTaxawpe_22 = value;
	}

	inline static int32_t get_offset_of__tiseDrisdetray_23() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____tiseDrisdetray_23)); }
	inline int32_t get__tiseDrisdetray_23() const { return ____tiseDrisdetray_23; }
	inline int32_t* get_address_of__tiseDrisdetray_23() { return &____tiseDrisdetray_23; }
	inline void set__tiseDrisdetray_23(int32_t value)
	{
		____tiseDrisdetray_23 = value;
	}

	inline static int32_t get_offset_of__mairlise_24() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____mairlise_24)); }
	inline uint32_t get__mairlise_24() const { return ____mairlise_24; }
	inline uint32_t* get_address_of__mairlise_24() { return &____mairlise_24; }
	inline void set__mairlise_24(uint32_t value)
	{
		____mairlise_24 = value;
	}

	inline static int32_t get_offset_of__chiwaski_25() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____chiwaski_25)); }
	inline int32_t get__chiwaski_25() const { return ____chiwaski_25; }
	inline int32_t* get_address_of__chiwaski_25() { return &____chiwaski_25; }
	inline void set__chiwaski_25(int32_t value)
	{
		____chiwaski_25 = value;
	}

	inline static int32_t get_offset_of__tacair_26() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____tacair_26)); }
	inline float get__tacair_26() const { return ____tacair_26; }
	inline float* get_address_of__tacair_26() { return &____tacair_26; }
	inline void set__tacair_26(float value)
	{
		____tacair_26 = value;
	}

	inline static int32_t get_offset_of__jarou_27() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____jarou_27)); }
	inline float get__jarou_27() const { return ____jarou_27; }
	inline float* get_address_of__jarou_27() { return &____jarou_27; }
	inline void set__jarou_27(float value)
	{
		____jarou_27 = value;
	}

	inline static int32_t get_offset_of__bijallHowdrorsear_28() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____bijallHowdrorsear_28)); }
	inline bool get__bijallHowdrorsear_28() const { return ____bijallHowdrorsear_28; }
	inline bool* get_address_of__bijallHowdrorsear_28() { return &____bijallHowdrorsear_28; }
	inline void set__bijallHowdrorsear_28(bool value)
	{
		____bijallHowdrorsear_28 = value;
	}

	inline static int32_t get_offset_of__peanuWhatoobo_29() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____peanuWhatoobo_29)); }
	inline int32_t get__peanuWhatoobo_29() const { return ____peanuWhatoobo_29; }
	inline int32_t* get_address_of__peanuWhatoobo_29() { return &____peanuWhatoobo_29; }
	inline void set__peanuWhatoobo_29(int32_t value)
	{
		____peanuWhatoobo_29 = value;
	}

	inline static int32_t get_offset_of__bamawtow_30() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____bamawtow_30)); }
	inline bool get__bamawtow_30() const { return ____bamawtow_30; }
	inline bool* get_address_of__bamawtow_30() { return &____bamawtow_30; }
	inline void set__bamawtow_30(bool value)
	{
		____bamawtow_30 = value;
	}

	inline static int32_t get_offset_of__celhemcas_31() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____celhemcas_31)); }
	inline uint32_t get__celhemcas_31() const { return ____celhemcas_31; }
	inline uint32_t* get_address_of__celhemcas_31() { return &____celhemcas_31; }
	inline void set__celhemcas_31(uint32_t value)
	{
		____celhemcas_31 = value;
	}

	inline static int32_t get_offset_of__cerestem_32() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____cerestem_32)); }
	inline String_t* get__cerestem_32() const { return ____cerestem_32; }
	inline String_t** get_address_of__cerestem_32() { return &____cerestem_32; }
	inline void set__cerestem_32(String_t* value)
	{
		____cerestem_32 = value;
		Il2CppCodeGenWriteBarrier(&____cerestem_32, value);
	}

	inline static int32_t get_offset_of__sekalu_33() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____sekalu_33)); }
	inline String_t* get__sekalu_33() const { return ____sekalu_33; }
	inline String_t** get_address_of__sekalu_33() { return &____sekalu_33; }
	inline void set__sekalu_33(String_t* value)
	{
		____sekalu_33 = value;
		Il2CppCodeGenWriteBarrier(&____sekalu_33, value);
	}

	inline static int32_t get_offset_of__hopar_34() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____hopar_34)); }
	inline uint32_t get__hopar_34() const { return ____hopar_34; }
	inline uint32_t* get_address_of__hopar_34() { return &____hopar_34; }
	inline void set__hopar_34(uint32_t value)
	{
		____hopar_34 = value;
	}

	inline static int32_t get_offset_of__dena_35() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____dena_35)); }
	inline bool get__dena_35() const { return ____dena_35; }
	inline bool* get_address_of__dena_35() { return &____dena_35; }
	inline void set__dena_35(bool value)
	{
		____dena_35 = value;
	}

	inline static int32_t get_offset_of__gairday_36() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____gairday_36)); }
	inline float get__gairday_36() const { return ____gairday_36; }
	inline float* get_address_of__gairday_36() { return &____gairday_36; }
	inline void set__gairday_36(float value)
	{
		____gairday_36 = value;
	}

	inline static int32_t get_offset_of__kiyarMoobasu_37() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____kiyarMoobasu_37)); }
	inline bool get__kiyarMoobasu_37() const { return ____kiyarMoobasu_37; }
	inline bool* get_address_of__kiyarMoobasu_37() { return &____kiyarMoobasu_37; }
	inline void set__kiyarMoobasu_37(bool value)
	{
		____kiyarMoobasu_37 = value;
	}

	inline static int32_t get_offset_of__todaHasperetar_38() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____todaHasperetar_38)); }
	inline uint32_t get__todaHasperetar_38() const { return ____todaHasperetar_38; }
	inline uint32_t* get_address_of__todaHasperetar_38() { return &____todaHasperetar_38; }
	inline void set__todaHasperetar_38(uint32_t value)
	{
		____todaHasperetar_38 = value;
	}

	inline static int32_t get_offset_of__heemay_39() { return static_cast<int32_t>(offsetof(M_gurweajeePasjounu48_t1465778100, ____heemay_39)); }
	inline int32_t get__heemay_39() const { return ____heemay_39; }
	inline int32_t* get_address_of__heemay_39() { return &____heemay_39; }
	inline void set__heemay_39(int32_t value)
	{
		____heemay_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
