﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_YieldInstructionGenerated
struct UnityEngine_YieldInstructionGenerated_t912798566;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_YieldInstructionGenerated::.ctor()
extern "C"  void UnityEngine_YieldInstructionGenerated__ctor_m2883348677 (UnityEngine_YieldInstructionGenerated_t912798566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_YieldInstructionGenerated::YieldInstruction_YieldInstruction1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_YieldInstructionGenerated_YieldInstruction_YieldInstruction1_m1882064173 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_YieldInstructionGenerated::__Register()
extern "C"  void UnityEngine_YieldInstructionGenerated___Register_m2339424034 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_YieldInstructionGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_YieldInstructionGenerated_ilo_getObject1_m468714769 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_YieldInstructionGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_YieldInstructionGenerated_ilo_attachFinalizerObject2_m117682963 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
