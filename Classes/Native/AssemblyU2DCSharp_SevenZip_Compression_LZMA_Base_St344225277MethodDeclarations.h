﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Compression.LZMA.Base/State
struct State_t344225277;
struct State_t344225277_marshaled_pinvoke;
struct State_t344225277_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Base_St344225277.h"

// System.Void SevenZip.Compression.LZMA.Base/State::Init()
extern "C"  void State_Init_m1563479382 (State_t344225277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Base/State::UpdateChar()
extern "C"  void State_UpdateChar_m1352656005 (State_t344225277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Base/State::UpdateMatch()
extern "C"  void State_UpdateMatch_m3379490200 (State_t344225277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Base/State::UpdateRep()
extern "C"  void State_UpdateRep_m750146288 (State_t344225277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Base/State::UpdateShortRep()
extern "C"  void State_UpdateShortRep_m4100539888 (State_t344225277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.Compression.LZMA.Base/State::IsCharState()
extern "C"  bool State_IsCharState_m3180260217 (State_t344225277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct State_t344225277;
struct State_t344225277_marshaled_pinvoke;

extern "C" void State_t344225277_marshal_pinvoke(const State_t344225277& unmarshaled, State_t344225277_marshaled_pinvoke& marshaled);
extern "C" void State_t344225277_marshal_pinvoke_back(const State_t344225277_marshaled_pinvoke& marshaled, State_t344225277& unmarshaled);
extern "C" void State_t344225277_marshal_pinvoke_cleanup(State_t344225277_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct State_t344225277;
struct State_t344225277_marshaled_com;

extern "C" void State_t344225277_marshal_com(const State_t344225277& unmarshaled, State_t344225277_marshaled_com& marshaled);
extern "C" void State_t344225277_marshal_com_back(const State_t344225277_marshaled_com& marshaled, State_t344225277& unmarshaled);
extern "C" void State_t344225277_marshal_com_cleanup(State_t344225277_marshaled_com& marshaled);
