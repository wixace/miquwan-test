﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.SoftJointLimit
struct SoftJointLimit_t2747465311;
struct SoftJointLimit_t2747465311_marshaled_pinvoke;
struct SoftJointLimit_t2747465311_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_SoftJointLimit2747465311.h"

// System.Single UnityEngine.SoftJointLimit::get_limit()
extern "C"  float SoftJointLimit_get_limit_m4243581302 (SoftJointLimit_t2747465311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SoftJointLimit::set_limit(System.Single)
extern "C"  void SoftJointLimit_set_limit_m1806277981 (SoftJointLimit_t2747465311 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.SoftJointLimit::get_bounciness()
extern "C"  float SoftJointLimit_get_bounciness_m3741823978 (SoftJointLimit_t2747465311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SoftJointLimit::set_bounciness(System.Single)
extern "C"  void SoftJointLimit_set_bounciness_m1948296601 (SoftJointLimit_t2747465311 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.SoftJointLimit::get_contactDistance()
extern "C"  float SoftJointLimit_get_contactDistance_m3676883696 (SoftJointLimit_t2747465311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SoftJointLimit::set_contactDistance(System.Single)
extern "C"  void SoftJointLimit_set_contactDistance_m408080419 (SoftJointLimit_t2747465311 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct SoftJointLimit_t2747465311;
struct SoftJointLimit_t2747465311_marshaled_pinvoke;

extern "C" void SoftJointLimit_t2747465311_marshal_pinvoke(const SoftJointLimit_t2747465311& unmarshaled, SoftJointLimit_t2747465311_marshaled_pinvoke& marshaled);
extern "C" void SoftJointLimit_t2747465311_marshal_pinvoke_back(const SoftJointLimit_t2747465311_marshaled_pinvoke& marshaled, SoftJointLimit_t2747465311& unmarshaled);
extern "C" void SoftJointLimit_t2747465311_marshal_pinvoke_cleanup(SoftJointLimit_t2747465311_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct SoftJointLimit_t2747465311;
struct SoftJointLimit_t2747465311_marshaled_com;

extern "C" void SoftJointLimit_t2747465311_marshal_com(const SoftJointLimit_t2747465311& unmarshaled, SoftJointLimit_t2747465311_marshaled_com& marshaled);
extern "C" void SoftJointLimit_t2747465311_marshal_com_back(const SoftJointLimit_t2747465311_marshaled_com& marshaled, SoftJointLimit_t2747465311& unmarshaled);
extern "C" void SoftJointLimit_t2747465311_marshal_com_cleanup(SoftJointLimit_t2747465311_marshaled_com& marshaled);
