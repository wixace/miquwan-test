﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Collider2D
struct Collider2D_t1552025098;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1743771669;
// UnityEngine.PhysicsMaterial2D
struct PhysicsMaterial2D_t1327932246;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"
#include "UnityEngine_UnityEngine_PhysicsMaterial2D1327932246.h"

// System.Void UnityEngine.Collider2D::.ctor()
extern "C"  void Collider2D__ctor_m3891360871 (Collider2D_t1552025098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Collider2D::get_density()
extern "C"  float Collider2D_get_density_m540365912 (Collider2D_t1552025098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider2D::set_density(System.Single)
extern "C"  void Collider2D_set_density_m818270651 (Collider2D_t1552025098 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Collider2D::get_isTrigger()
extern "C"  bool Collider2D_get_isTrigger_m229979716 (Collider2D_t1552025098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider2D::set_isTrigger(System.Boolean)
extern "C"  void Collider2D_set_isTrigger_m4005580269 (Collider2D_t1552025098 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Collider2D::get_usedByEffector()
extern "C"  bool Collider2D_get_usedByEffector_m4042404052 (Collider2D_t1552025098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider2D::set_usedByEffector(System.Boolean)
extern "C"  void Collider2D_set_usedByEffector_m4032440665 (Collider2D_t1552025098 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Collider2D::get_offset()
extern "C"  Vector2_t4282066565  Collider2D_get_offset_m3854309972 (Collider2D_t1552025098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider2D::set_offset(UnityEngine.Vector2)
extern "C"  void Collider2D_set_offset_m2776803501 (Collider2D_t1552025098 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider2D::INTERNAL_get_offset(UnityEngine.Vector2&)
extern "C"  void Collider2D_INTERNAL_get_offset_m2687145813 (Collider2D_t1552025098 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider2D::INTERNAL_set_offset(UnityEngine.Vector2&)
extern "C"  void Collider2D_INTERNAL_set_offset_m40736201 (Collider2D_t1552025098 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
extern "C"  Rigidbody2D_t1743771669 * Collider2D_get_attachedRigidbody_m2908627162 (Collider2D_t1552025098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Collider2D::get_shapeCount()
extern "C"  int32_t Collider2D_get_shapeCount_m2388185620 (Collider2D_t1552025098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UnityEngine.Collider2D::get_bounds()
extern "C"  Bounds_t2711641849  Collider2D_get_bounds_m1087503006 (Collider2D_t1552025098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider2D::INTERNAL_get_bounds(UnityEngine.Bounds&)
extern "C"  void Collider2D_INTERNAL_get_bounds_m3576678899 (Collider2D_t1552025098 * __this, Bounds_t2711641849 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Collider2D::OverlapPoint(UnityEngine.Vector2)
extern "C"  bool Collider2D_OverlapPoint_m1082765966 (Collider2D_t1552025098 * __this, Vector2_t4282066565  ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Collider2D::INTERNAL_CALL_OverlapPoint(UnityEngine.Collider2D,UnityEngine.Vector2&)
extern "C"  bool Collider2D_INTERNAL_CALL_OverlapPoint_m3954725770 (Il2CppObject * __this /* static, unused */, Collider2D_t1552025098 * ___self0, Vector2_t4282066565 * ___point1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.PhysicsMaterial2D UnityEngine.Collider2D::get_sharedMaterial()
extern "C"  PhysicsMaterial2D_t1327932246 * Collider2D_get_sharedMaterial_m2954504316 (Collider2D_t1552025098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider2D::set_sharedMaterial(UnityEngine.PhysicsMaterial2D)
extern "C"  void Collider2D_set_sharedMaterial_m1288156355 (Collider2D_t1552025098 * __this, PhysicsMaterial2D_t1327932246 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Collider2D::IsTouching(UnityEngine.Collider2D)
extern "C"  bool Collider2D_IsTouching_m1263557075 (Collider2D_t1552025098 * __this, Collider2D_t1552025098 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Collider2D::IsTouchingLayers(System.Int32)
extern "C"  bool Collider2D_IsTouchingLayers_m1250438947 (Collider2D_t1552025098 * __this, int32_t ___layerMask0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Collider2D::IsTouchingLayers()
extern "C"  bool Collider2D_IsTouchingLayers_m387261650 (Collider2D_t1552025098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
