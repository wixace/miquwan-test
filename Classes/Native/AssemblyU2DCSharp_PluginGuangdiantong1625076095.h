﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t4054002952;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginGuangdiantong
struct  PluginGuangdiantong_t1625076095  : public MonoBehaviour_t667441552
{
public:
	// System.String[] PluginGuangdiantong::sdkinfos
	StringU5BU5D_t4054002952* ___sdkinfos_2;
	// System.String[] PluginGuangdiantong::gdtinfos
	StringU5BU5D_t4054002952* ___gdtinfos_3;

public:
	inline static int32_t get_offset_of_sdkinfos_2() { return static_cast<int32_t>(offsetof(PluginGuangdiantong_t1625076095, ___sdkinfos_2)); }
	inline StringU5BU5D_t4054002952* get_sdkinfos_2() const { return ___sdkinfos_2; }
	inline StringU5BU5D_t4054002952** get_address_of_sdkinfos_2() { return &___sdkinfos_2; }
	inline void set_sdkinfos_2(StringU5BU5D_t4054002952* value)
	{
		___sdkinfos_2 = value;
		Il2CppCodeGenWriteBarrier(&___sdkinfos_2, value);
	}

	inline static int32_t get_offset_of_gdtinfos_3() { return static_cast<int32_t>(offsetof(PluginGuangdiantong_t1625076095, ___gdtinfos_3)); }
	inline StringU5BU5D_t4054002952* get_gdtinfos_3() const { return ___gdtinfos_3; }
	inline StringU5BU5D_t4054002952** get_address_of_gdtinfos_3() { return &___gdtinfos_3; }
	inline void set_gdtinfos_3(StringU5BU5D_t4054002952* value)
	{
		___gdtinfos_3 = value;
		Il2CppCodeGenWriteBarrier(&___gdtinfos_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
