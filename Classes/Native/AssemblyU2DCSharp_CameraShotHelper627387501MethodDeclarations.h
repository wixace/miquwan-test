﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraShotHelper
struct CameraShotHelper_t627387501;
// System.String
struct String_t;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// CameraSmoothFollow
struct CameraSmoothFollow_t2624612068;
// HeroMgr
struct HeroMgr_t2475965342;
// TimeMgr
struct TimeMgr_t350708715;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// CombatEntity
struct CombatEntity_t684137495;
// GameMgr
struct GameMgr_t1469029542;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CameraSmoothFollow2624612068.h"
#include "AssemblyU2DCSharp_TimeMgr350708715.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"

// System.Void CameraShotHelper::.ctor()
extern "C"  void CameraShotHelper__ctor_m2806424782 (CameraShotHelper_t627387501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotHelper::.cctor()
extern "C"  void CameraShotHelper__cctor_m617726111 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotHelper::Awake()
extern "C"  void CameraShotHelper_Awake_m3044030001 (CameraShotHelper_t627387501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotHelper::Clear()
extern "C"  void CameraShotHelper_Clear_m212558073 (CameraShotHelper_t627387501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotHelper::Update()
extern "C"  void CameraShotHelper_Update_m2826684415 (CameraShotHelper_t627387501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotHelper::LateUpdate()
extern "C"  void CameraShotHelper_LateUpdate_m2811983429 (CameraShotHelper_t627387501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotHelper::FixedUpdate()
extern "C"  void CameraShotHelper_FixedUpdate_m2148085705 (CameraShotHelper_t627387501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotHelper::lineMoveCtrl(System.Int32,System.Int32,UnityEngine.Vector3,System.Single,System.Single,System.Int32,System.Int32)
extern "C"  void CameraShotHelper_lineMoveCtrl_m1049758109 (CameraShotHelper_t627387501 * __this, int32_t ___targetId0, int32_t ___isHero1, Vector3_t4282066566  ___targetPos2, float ___stopDis3, float ___time4, int32_t ___isDamp5, int32_t ___curCameraShotId6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotHelper::gameSpeedCtrl(System.Single,System.Single,System.Single,System.Single,System.Int32)
extern "C"  void CameraShotHelper_gameSpeedCtrl_m1000546633 (CameraShotHelper_t627387501 * __this, float ___speed0, float ___from1, float ___wait2, float ___to3, int32_t ___id4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotHelper::brightChange(System.Int32,System.Single,System.Int32)
extern "C"  void CameraShotHelper_brightChange_m1776745409 (CameraShotHelper_t627387501 * __this, int32_t ___brightId0, float ___time1, int32_t ___curCameraShotId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotHelper::circleAction(System.Int32,System.String,UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Single,System.Int32,System.Int32)
extern "C"  void CameraShotHelper_circleAction_m4186444688 (CameraShotHelper_t627387501 * __this, int32_t ___id0, String_t* ___boneName1, Vector3_t4282066566  ___tg2, float ___sp3, float ___dt4, float ___hg5, float ___sa6, int32_t ___isHero7, int32_t ___isRight8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotHelper::circleAction_default(System.Int32,System.String,UnityEngine.Vector3,System.Int32,System.Single,System.Int32)
extern "C"  void CameraShotHelper_circleAction_default_m1757102585 (CameraShotHelper_t627387501 * __this, int32_t ___id0, String_t* ___boneName1, Vector3_t4282066566  ___tg2, int32_t ___isRight3, float ___sp4, int32_t ___isHero5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotHelper::circleAction_Effect(System.Int32,System.Single,System.Single,System.Single,System.Single,System.Int32)
extern "C"  void CameraShotHelper_circleAction_Effect_m3713383708 (CameraShotHelper_t627387501 * __this, int32_t ___id0, float ___sp1, float ___dt2, float ___hg3, float ___sa4, int32_t ___isRight5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotHelper::Preview(UnityEngine.Vector3[],UnityEngine.Vector3[],System.Single[],System.Int32[],System.Int32[],System.Single[],System.Single[],System.Single[],System.Int32[],System.Single[],System.Int32,System.Int32[])
extern "C"  void CameraShotHelper_Preview_m3025210710 (CameraShotHelper_t627387501 * __this, Vector3U5BU5D_t215400611* ___points0, Vector3U5BU5D_t215400611* ___rots1, SingleU5BU5D_t2316563989* ___speeds2, Int32U5BU5D_t3230847821* ___focusIds3, Int32U5BU5D_t3230847821* ___isHeros4, SingleU5BU5D_t2316563989* ___smooths5, SingleU5BU5D_t2316563989* ___views6, SingleU5BU5D_t2316563989* ___rotSpeed7, Int32U5BU5D_t3230847821* ___isClockWise8, SingleU5BU5D_t2316563989* ___orthographics9, int32_t ___id10, Int32U5BU5D_t3230847821* ___isConstants11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotHelper::ilo_set_IsOpeningMoveUpdate1(CameraSmoothFollow,System.Boolean)
extern "C"  void CameraShotHelper_ilo_set_IsOpeningMoveUpdate1_m300970962 (Il2CppObject * __this /* static, unused */, CameraSmoothFollow_t2624612068 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr CameraShotHelper::ilo_get_instance2()
extern "C"  HeroMgr_t2475965342 * CameraShotHelper_ilo_get_instance2_m3306275448 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TimeMgr CameraShotHelper::ilo_get_TimeMgr3()
extern "C"  TimeMgr_t350708715 * CameraShotHelper_ilo_get_TimeMgr3_m1742059202 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotHelper::ilo_AddTimeScale4(TimeMgr,System.Single,System.String)
extern "C"  void CameraShotHelper_ilo_AddTimeScale4_m2092494967 (Il2CppObject * __this /* static, unused */, TimeMgr_t350708715 * ____this0, float ___timeScale1, String_t* ___timeKey2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotHelper::ilo_DispatchEvent5(CEvent.ZEvent)
extern "C"  void CameraShotHelper_ilo_DispatchEvent5_m1050462813 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity CameraShotHelper::ilo_GetEntityByID6(GameMgr,System.Boolean,System.Int32)
extern "C"  CombatEntity_t684137495 * CameraShotHelper_ilo_GetEntityByID6_m1967121772 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, bool ___isHero1, int32_t ___id2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
