﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AIObject
struct AIObject_t37280135;

#include "codegen/il2cpp-codegen.h"

// System.Void AIObject::.ctor(System.Int32)
extern "C"  void AIObject__ctor_m1672778821 (AIObject_t37280135 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AIObject::get_isInCD()
extern "C"  bool AIObject_get_isInCD_m3799003517 (AIObject_t37280135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AIObject::get_isComplete()
extern "C"  bool AIObject_get_isComplete_m1483589296 (AIObject_t37280135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIObject::OnUseing()
extern "C"  void AIObject_OnUseing_m3443225482 (AIObject_t37280135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
