﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Geometric/Rectangle
struct Rectangle_t4010590433;
struct Rectangle_t4010590433_marshaled_pinvoke;
struct Rectangle_t4010590433_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Geometric_Rectangle4010590433.h"

// System.Void Geometric/Rectangle::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rectangle__ctor_m381900420 (Rectangle_t4010590433 * __this, float ____x00, float ____y01, float ____x12, float ____y13, float ____x24, float ____y25, float ____x36, float ____y37, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Rectangle_t4010590433;
struct Rectangle_t4010590433_marshaled_pinvoke;

extern "C" void Rectangle_t4010590433_marshal_pinvoke(const Rectangle_t4010590433& unmarshaled, Rectangle_t4010590433_marshaled_pinvoke& marshaled);
extern "C" void Rectangle_t4010590433_marshal_pinvoke_back(const Rectangle_t4010590433_marshaled_pinvoke& marshaled, Rectangle_t4010590433& unmarshaled);
extern "C" void Rectangle_t4010590433_marshal_pinvoke_cleanup(Rectangle_t4010590433_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Rectangle_t4010590433;
struct Rectangle_t4010590433_marshaled_com;

extern "C" void Rectangle_t4010590433_marshal_com(const Rectangle_t4010590433& unmarshaled, Rectangle_t4010590433_marshaled_com& marshaled);
extern "C" void Rectangle_t4010590433_marshal_com_back(const Rectangle_t4010590433_marshaled_com& marshaled, Rectangle_t4010590433& unmarshaled);
extern "C" void Rectangle_t4010590433_marshal_com_cleanup(Rectangle_t4010590433_marshaled_com& marshaled);
