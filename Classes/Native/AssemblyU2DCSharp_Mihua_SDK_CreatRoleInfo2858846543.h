﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mihua.SDK.CreatRoleInfo
struct  CreatRoleInfo_t2858846543  : public Il2CppObject
{
public:
	// System.String Mihua.SDK.CreatRoleInfo::roleId
	String_t* ___roleId_0;
	// System.String Mihua.SDK.CreatRoleInfo::roleName
	String_t* ___roleName_1;
	// System.String Mihua.SDK.CreatRoleInfo::roleLv
	String_t* ___roleLv_2;
	// System.String Mihua.SDK.CreatRoleInfo::serverId
	String_t* ___serverId_3;
	// System.String Mihua.SDK.CreatRoleInfo::serverName
	String_t* ___serverName_4;
	// System.String Mihua.SDK.CreatRoleInfo::CreatRoleTime
	String_t* ___CreatRoleTime_5;
	// System.String Mihua.SDK.CreatRoleInfo::LvUpTime
	String_t* ___LvUpTime_6;
	// System.String Mihua.SDK.CreatRoleInfo::Balance
	String_t* ___Balance_7;
	// System.String Mihua.SDK.CreatRoleInfo::VipLevel
	String_t* ___VipLevel_8;
	// System.String Mihua.SDK.CreatRoleInfo::unionName
	String_t* ___unionName_9;
	// System.String Mihua.SDK.CreatRoleInfo::welcomeAddress
	String_t* ___welcomeAddress_10;
	// System.String Mihua.SDK.CreatRoleInfo::GoldID
	String_t* ___GoldID_11;
	// System.String Mihua.SDK.CreatRoleInfo::curGlodNum
	String_t* ___curGlodNum_12;
	// System.String Mihua.SDK.CreatRoleInfo::Power
	String_t* ___Power_13;
	// System.String Mihua.SDK.CreatRoleInfo::UnionID
	String_t* ___UnionID_14;
	// System.String Mihua.SDK.CreatRoleInfo::BuddyListToJson_360
	String_t* ___BuddyListToJson_360_15;
	// System.String Mihua.SDK.CreatRoleInfo::SDKtoken
	String_t* ___SDKtoken_16;

public:
	inline static int32_t get_offset_of_roleId_0() { return static_cast<int32_t>(offsetof(CreatRoleInfo_t2858846543, ___roleId_0)); }
	inline String_t* get_roleId_0() const { return ___roleId_0; }
	inline String_t** get_address_of_roleId_0() { return &___roleId_0; }
	inline void set_roleId_0(String_t* value)
	{
		___roleId_0 = value;
		Il2CppCodeGenWriteBarrier(&___roleId_0, value);
	}

	inline static int32_t get_offset_of_roleName_1() { return static_cast<int32_t>(offsetof(CreatRoleInfo_t2858846543, ___roleName_1)); }
	inline String_t* get_roleName_1() const { return ___roleName_1; }
	inline String_t** get_address_of_roleName_1() { return &___roleName_1; }
	inline void set_roleName_1(String_t* value)
	{
		___roleName_1 = value;
		Il2CppCodeGenWriteBarrier(&___roleName_1, value);
	}

	inline static int32_t get_offset_of_roleLv_2() { return static_cast<int32_t>(offsetof(CreatRoleInfo_t2858846543, ___roleLv_2)); }
	inline String_t* get_roleLv_2() const { return ___roleLv_2; }
	inline String_t** get_address_of_roleLv_2() { return &___roleLv_2; }
	inline void set_roleLv_2(String_t* value)
	{
		___roleLv_2 = value;
		Il2CppCodeGenWriteBarrier(&___roleLv_2, value);
	}

	inline static int32_t get_offset_of_serverId_3() { return static_cast<int32_t>(offsetof(CreatRoleInfo_t2858846543, ___serverId_3)); }
	inline String_t* get_serverId_3() const { return ___serverId_3; }
	inline String_t** get_address_of_serverId_3() { return &___serverId_3; }
	inline void set_serverId_3(String_t* value)
	{
		___serverId_3 = value;
		Il2CppCodeGenWriteBarrier(&___serverId_3, value);
	}

	inline static int32_t get_offset_of_serverName_4() { return static_cast<int32_t>(offsetof(CreatRoleInfo_t2858846543, ___serverName_4)); }
	inline String_t* get_serverName_4() const { return ___serverName_4; }
	inline String_t** get_address_of_serverName_4() { return &___serverName_4; }
	inline void set_serverName_4(String_t* value)
	{
		___serverName_4 = value;
		Il2CppCodeGenWriteBarrier(&___serverName_4, value);
	}

	inline static int32_t get_offset_of_CreatRoleTime_5() { return static_cast<int32_t>(offsetof(CreatRoleInfo_t2858846543, ___CreatRoleTime_5)); }
	inline String_t* get_CreatRoleTime_5() const { return ___CreatRoleTime_5; }
	inline String_t** get_address_of_CreatRoleTime_5() { return &___CreatRoleTime_5; }
	inline void set_CreatRoleTime_5(String_t* value)
	{
		___CreatRoleTime_5 = value;
		Il2CppCodeGenWriteBarrier(&___CreatRoleTime_5, value);
	}

	inline static int32_t get_offset_of_LvUpTime_6() { return static_cast<int32_t>(offsetof(CreatRoleInfo_t2858846543, ___LvUpTime_6)); }
	inline String_t* get_LvUpTime_6() const { return ___LvUpTime_6; }
	inline String_t** get_address_of_LvUpTime_6() { return &___LvUpTime_6; }
	inline void set_LvUpTime_6(String_t* value)
	{
		___LvUpTime_6 = value;
		Il2CppCodeGenWriteBarrier(&___LvUpTime_6, value);
	}

	inline static int32_t get_offset_of_Balance_7() { return static_cast<int32_t>(offsetof(CreatRoleInfo_t2858846543, ___Balance_7)); }
	inline String_t* get_Balance_7() const { return ___Balance_7; }
	inline String_t** get_address_of_Balance_7() { return &___Balance_7; }
	inline void set_Balance_7(String_t* value)
	{
		___Balance_7 = value;
		Il2CppCodeGenWriteBarrier(&___Balance_7, value);
	}

	inline static int32_t get_offset_of_VipLevel_8() { return static_cast<int32_t>(offsetof(CreatRoleInfo_t2858846543, ___VipLevel_8)); }
	inline String_t* get_VipLevel_8() const { return ___VipLevel_8; }
	inline String_t** get_address_of_VipLevel_8() { return &___VipLevel_8; }
	inline void set_VipLevel_8(String_t* value)
	{
		___VipLevel_8 = value;
		Il2CppCodeGenWriteBarrier(&___VipLevel_8, value);
	}

	inline static int32_t get_offset_of_unionName_9() { return static_cast<int32_t>(offsetof(CreatRoleInfo_t2858846543, ___unionName_9)); }
	inline String_t* get_unionName_9() const { return ___unionName_9; }
	inline String_t** get_address_of_unionName_9() { return &___unionName_9; }
	inline void set_unionName_9(String_t* value)
	{
		___unionName_9 = value;
		Il2CppCodeGenWriteBarrier(&___unionName_9, value);
	}

	inline static int32_t get_offset_of_welcomeAddress_10() { return static_cast<int32_t>(offsetof(CreatRoleInfo_t2858846543, ___welcomeAddress_10)); }
	inline String_t* get_welcomeAddress_10() const { return ___welcomeAddress_10; }
	inline String_t** get_address_of_welcomeAddress_10() { return &___welcomeAddress_10; }
	inline void set_welcomeAddress_10(String_t* value)
	{
		___welcomeAddress_10 = value;
		Il2CppCodeGenWriteBarrier(&___welcomeAddress_10, value);
	}

	inline static int32_t get_offset_of_GoldID_11() { return static_cast<int32_t>(offsetof(CreatRoleInfo_t2858846543, ___GoldID_11)); }
	inline String_t* get_GoldID_11() const { return ___GoldID_11; }
	inline String_t** get_address_of_GoldID_11() { return &___GoldID_11; }
	inline void set_GoldID_11(String_t* value)
	{
		___GoldID_11 = value;
		Il2CppCodeGenWriteBarrier(&___GoldID_11, value);
	}

	inline static int32_t get_offset_of_curGlodNum_12() { return static_cast<int32_t>(offsetof(CreatRoleInfo_t2858846543, ___curGlodNum_12)); }
	inline String_t* get_curGlodNum_12() const { return ___curGlodNum_12; }
	inline String_t** get_address_of_curGlodNum_12() { return &___curGlodNum_12; }
	inline void set_curGlodNum_12(String_t* value)
	{
		___curGlodNum_12 = value;
		Il2CppCodeGenWriteBarrier(&___curGlodNum_12, value);
	}

	inline static int32_t get_offset_of_Power_13() { return static_cast<int32_t>(offsetof(CreatRoleInfo_t2858846543, ___Power_13)); }
	inline String_t* get_Power_13() const { return ___Power_13; }
	inline String_t** get_address_of_Power_13() { return &___Power_13; }
	inline void set_Power_13(String_t* value)
	{
		___Power_13 = value;
		Il2CppCodeGenWriteBarrier(&___Power_13, value);
	}

	inline static int32_t get_offset_of_UnionID_14() { return static_cast<int32_t>(offsetof(CreatRoleInfo_t2858846543, ___UnionID_14)); }
	inline String_t* get_UnionID_14() const { return ___UnionID_14; }
	inline String_t** get_address_of_UnionID_14() { return &___UnionID_14; }
	inline void set_UnionID_14(String_t* value)
	{
		___UnionID_14 = value;
		Il2CppCodeGenWriteBarrier(&___UnionID_14, value);
	}

	inline static int32_t get_offset_of_BuddyListToJson_360_15() { return static_cast<int32_t>(offsetof(CreatRoleInfo_t2858846543, ___BuddyListToJson_360_15)); }
	inline String_t* get_BuddyListToJson_360_15() const { return ___BuddyListToJson_360_15; }
	inline String_t** get_address_of_BuddyListToJson_360_15() { return &___BuddyListToJson_360_15; }
	inline void set_BuddyListToJson_360_15(String_t* value)
	{
		___BuddyListToJson_360_15 = value;
		Il2CppCodeGenWriteBarrier(&___BuddyListToJson_360_15, value);
	}

	inline static int32_t get_offset_of_SDKtoken_16() { return static_cast<int32_t>(offsetof(CreatRoleInfo_t2858846543, ___SDKtoken_16)); }
	inline String_t* get_SDKtoken_16() const { return ___SDKtoken_16; }
	inline String_t** get_address_of_SDKtoken_16() { return &___SDKtoken_16; }
	inline void set_SDKtoken_16(String_t* value)
	{
		___SDKtoken_16 = value;
		Il2CppCodeGenWriteBarrier(&___SDKtoken_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
