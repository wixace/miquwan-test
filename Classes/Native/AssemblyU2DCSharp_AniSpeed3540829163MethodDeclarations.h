﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AniSpeed
struct AniSpeed_t3540829163;

#include "codegen/il2cpp-codegen.h"

// System.Void AniSpeed::.ctor()
extern "C"  void AniSpeed__ctor_m327824144 (AniSpeed_t3540829163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AniSpeed::Start()
extern "C"  void AniSpeed_Start_m3569929232 (AniSpeed_t3540829163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
