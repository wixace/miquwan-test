﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginReYun
struct  PluginReYun_t2552860172  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean PluginReYun::needTrack
	bool ___needTrack_2;
	// System.Boolean PluginReYun::needTrackingIO
	bool ___needTrackingIO_3;

public:
	inline static int32_t get_offset_of_needTrack_2() { return static_cast<int32_t>(offsetof(PluginReYun_t2552860172, ___needTrack_2)); }
	inline bool get_needTrack_2() const { return ___needTrack_2; }
	inline bool* get_address_of_needTrack_2() { return &___needTrack_2; }
	inline void set_needTrack_2(bool value)
	{
		___needTrack_2 = value;
	}

	inline static int32_t get_offset_of_needTrackingIO_3() { return static_cast<int32_t>(offsetof(PluginReYun_t2552860172, ___needTrackingIO_3)); }
	inline bool get_needTrackingIO_3() const { return ___needTrackingIO_3; }
	inline bool* get_address_of_needTrackingIO_3() { return &___needTrackingIO_3; }
	inline void set_needTrackingIO_3(bool value)
	{
		___needTrackingIO_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
