﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DateUtilGenerated
struct DateUtilGenerated_t279352959;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"

// System.Void DateUtilGenerated::.ctor()
extern "C"  void DateUtilGenerated__ctor_m447018444 (DateUtilGenerated_t279352959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DateUtilGenerated::DateUtil_SecondsOf1970(JSVCall)
extern "C"  void DateUtilGenerated_DateUtil_SecondsOf1970_m3375317511 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DateUtilGenerated::DateUtil_NowDay(JSVCall)
extern "C"  void DateUtilGenerated_DateUtil_NowDay_m2522069768 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DateUtilGenerated::DateUtil_NowHour(JSVCall)
extern "C"  void DateUtilGenerated_DateUtil_NowHour_m3415935556 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DateUtilGenerated::DateUtil_NowMillisecond(JSVCall)
extern "C"  void DateUtilGenerated_DateUtil_NowMillisecond_m4060634051 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DateUtilGenerated::DateUtil_NowMinute(JSVCall)
extern "C"  void DateUtilGenerated_DateUtil_NowMinute_m3280441236 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DateUtilGenerated::DateUtil_NowMonth(JSVCall)
extern "C"  void DateUtilGenerated_DateUtil_NowMonth_m1064414500 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DateUtilGenerated::DateUtil_NowSecond(JSVCall)
extern "C"  void DateUtilGenerated_DateUtil_NowSecond_m2009341236 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DateUtilGenerated::DateUtil_NowTicks(JSVCall)
extern "C"  void DateUtilGenerated_DateUtil_NowTicks_m3286991342 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DateUtilGenerated::DateUtil_NowYear(JSVCall)
extern "C"  void DateUtilGenerated_DateUtil_NowYear_m3011513355 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DateUtilGenerated::DateUtil_GetAllDaySeconds__Double(JSVCall,System.Int32)
extern "C"  bool DateUtilGenerated_DateUtil_GetAllDaySeconds__Double_m2488057532 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DateUtilGenerated::DateUtil_GetDay__Double(JSVCall,System.Int32)
extern "C"  bool DateUtilGenerated_DateUtil_GetDay__Double_m850121748 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DateUtilGenerated::DateUtil_GetDayOfWeek__Double(JSVCall,System.Int32)
extern "C"  bool DateUtilGenerated_DateUtil_GetDayOfWeek__Double_m2251004895 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DateUtilGenerated::DateUtil_GetDaysInMonth__Double(JSVCall,System.Int32)
extern "C"  bool DateUtilGenerated_DateUtil_GetDaysInMonth__Double_m599158908 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DateUtilGenerated::DateUtil_GetHour__Double(JSVCall,System.Int32)
extern "C"  bool DateUtilGenerated_DateUtil_GetHour__Double_m3912299632 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DateUtilGenerated::DateUtil_GetMinute__Double(JSVCall,System.Int32)
extern "C"  bool DateUtilGenerated_DateUtil_GetMinute__Double_m639761952 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DateUtilGenerated::DateUtil_GetSecond__Double(JSVCall,System.Int32)
extern "C"  bool DateUtilGenerated_DateUtil_GetSecond__Double_m1791243392 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DateUtilGenerated::DateUtil_GetTimeSpan__Double__Double(JSVCall,System.Int32)
extern "C"  bool DateUtilGenerated_DateUtil_GetTimeSpan__Double__Double_m3645500692 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DateUtilGenerated::DateUtil_GetTimeSpanTotalSeconds__Double__Double(JSVCall,System.Int32)
extern "C"  bool DateUtilGenerated_DateUtil_GetTimeSpanTotalSeconds__Double__Double_m3182798351 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DateUtilGenerated::DateUtil_ToShortDateString__Double(JSVCall,System.Int32)
extern "C"  bool DateUtilGenerated_DateUtil_ToShortDateString__Double_m2813323670 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DateUtilGenerated::__Register()
extern "C"  void DateUtilGenerated___Register_m3236578939 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DateUtilGenerated::ilo_get_NowHour1()
extern "C"  int32_t DateUtilGenerated_ilo_get_NowHour1_m3825817469 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DateUtilGenerated::ilo_setInt322(System.Int32,System.Int32)
extern "C"  void DateUtilGenerated_ilo_setInt322_m308186857 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DateUtilGenerated::ilo_get_NowMillisecond3()
extern "C"  int32_t DateUtilGenerated_ilo_get_NowMillisecond3_m4031016068 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DateUtilGenerated::ilo_get_NowSecond4()
extern "C"  int32_t DateUtilGenerated_ilo_get_NowSecond4_m2640235120 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DateUtilGenerated::ilo_setDouble5(System.Int32,System.Double)
extern "C"  void DateUtilGenerated_ilo_setDouble5_m3979787226 (Il2CppObject * __this /* static, unused */, int32_t ___e0, double ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DateUtilGenerated::ilo_GetDay6(System.Double)
extern "C"  int32_t DateUtilGenerated_ilo_GetDay6_m1160204141 (Il2CppObject * __this /* static, unused */, double ___timer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double DateUtilGenerated::ilo_getDouble7(System.Int32)
extern "C"  double DateUtilGenerated_ilo_getDouble7_m1312176425 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DateUtilGenerated::ilo_GetMinute8(System.Double)
extern "C"  int32_t DateUtilGenerated_ilo_GetMinute8_m1911315991 (Il2CppObject * __this /* static, unused */, double ___timer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DateUtilGenerated::ilo_GetSecond9(System.Double)
extern "C"  int32_t DateUtilGenerated_ilo_GetSecond9_m2520062550 (Il2CppObject * __this /* static, unused */, double ___timer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan DateUtilGenerated::ilo_GetTimeSpan10(System.Double,System.Double)
extern "C"  TimeSpan_t413522987  DateUtilGenerated_ilo_GetTimeSpan10_m1661686238 (Il2CppObject * __this /* static, unused */, double ___timer10, double ___timer21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DateUtilGenerated::ilo_setObject11(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t DateUtilGenerated_ilo_setObject11_m2632754363 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DateUtilGenerated::ilo_ToShortDateString12(System.Double)
extern "C"  String_t* DateUtilGenerated_ilo_ToShortDateString12_m2250331719 (Il2CppObject * __this /* static, unused */, double ___timer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DateUtilGenerated::ilo_setStringS13(System.Int32,System.String)
extern "C"  void DateUtilGenerated_ilo_setStringS13_m1569617690 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
