﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_helitaQougelsou53
struct M_helitaQougelsou53_t199117849;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_helitaQougelsou53::.ctor()
extern "C"  void M_helitaQougelsou53__ctor_m187091306 (M_helitaQougelsou53_t199117849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_helitaQougelsou53::M_cetesu0(System.String[],System.Int32)
extern "C"  void M_helitaQougelsou53_M_cetesu0_m2981178760 (M_helitaQougelsou53_t199117849 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_helitaQougelsou53::M_lertrearta1(System.String[],System.Int32)
extern "C"  void M_helitaQougelsou53_M_lertrearta1_m1042378770 (M_helitaQougelsou53_t199117849 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_helitaQougelsou53::M_rasu2(System.String[],System.Int32)
extern "C"  void M_helitaQougelsou53_M_rasu2_m3972493038 (M_helitaQougelsou53_t199117849 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
