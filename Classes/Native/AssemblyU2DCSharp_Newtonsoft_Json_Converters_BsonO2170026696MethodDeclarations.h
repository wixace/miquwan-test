﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.BsonObjectIdConverter
struct BsonObjectIdConverter_t2170026696;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.Type
struct Type_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// Newtonsoft.Json.Bson.BsonObjectId
struct BsonObjectId_t1082490490;
// System.String
struct String_t;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer251850770.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonObjectI1082490490.h"
#include "mscorlib_System_String7231557.h"

// System.Void Newtonsoft.Json.Converters.BsonObjectIdConverter::.ctor()
extern "C"  void BsonObjectIdConverter__ctor_m334021131 (BsonObjectIdConverter_t2170026696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.BsonObjectIdConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  void BsonObjectIdConverter_WriteJson_m216442985 (BsonObjectIdConverter_t2170026696 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonSerializer_t251850770 * ___serializer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Converters.BsonObjectIdConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  Il2CppObject * BsonObjectIdConverter_ReadJson_m3132749962 (BsonObjectIdConverter_t2170026696 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, Il2CppObject * ___existingValue2, JsonSerializer_t251850770 * ___serializer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.BsonObjectIdConverter::CanConvert(System.Type)
extern "C"  bool BsonObjectIdConverter_CanConvert_m4003919593 (BsonObjectIdConverter_t2170026696 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Newtonsoft.Json.Converters.BsonObjectIdConverter::ilo_get_Value1(Newtonsoft.Json.Bson.BsonObjectId)
extern "C"  ByteU5BU5D_t4260760469* BsonObjectIdConverter_ilo_get_Value1_m1398986400 (Il2CppObject * __this /* static, unused */, BsonObjectId_t1082490490 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.BsonObjectIdConverter::ilo_FormatWith2(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* BsonObjectIdConverter_ilo_FormatWith2_m1253796902 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Converters.BsonObjectIdConverter::ilo_get_Value3(Newtonsoft.Json.JsonReader)
extern "C"  Il2CppObject * BsonObjectIdConverter_ilo_get_Value3_m2215328588 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
