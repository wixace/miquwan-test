﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1433722274(__this, ___l0, method) ((  void (*) (Enumerator_t1787202757 *, List_1_t1767529987 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1277868400(__this, method) ((  void (*) (Enumerator_t1787202757 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m790751526(__this, method) ((  Il2CppObject * (*) (Enumerator_t1787202757 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>::Dispose()
#define Enumerator_Dispose_m572617735(__this, method) ((  void (*) (Enumerator_t1787202757 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>::VerifyState()
#define Enumerator_VerifyState_m4124956608(__this, method) ((  void (*) (Enumerator_t1787202757 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>::MoveNext()
#define Enumerator_MoveNext_m621688480(__this, method) ((  bool (*) (Enumerator_t1787202757 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>::get_Current()
#define Enumerator_get_Current_m198869273(__this, method) ((  List_1_t399344435 * (*) (Enumerator_t1787202757 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
