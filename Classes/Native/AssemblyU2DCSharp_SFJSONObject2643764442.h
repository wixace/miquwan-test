﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Hashtable
struct Hashtable_t1407064410;
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t3324145743;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SFJSONObject
struct  SFJSONObject_t2643764442  : public Il2CppObject
{
public:
	// System.Collections.Hashtable SFJSONObject::nameValuePairs
	Hashtable_t1407064410 * ___nameValuePairs_0;
	// System.String SFJSONObject::ins
	String_t* ___ins_1;
	// System.Char[] SFJSONObject::cins
	CharU5BU5D_t3324145743* ___cins_2;
	// System.Int32 SFJSONObject::pos
	int32_t ___pos_3;

public:
	inline static int32_t get_offset_of_nameValuePairs_0() { return static_cast<int32_t>(offsetof(SFJSONObject_t2643764442, ___nameValuePairs_0)); }
	inline Hashtable_t1407064410 * get_nameValuePairs_0() const { return ___nameValuePairs_0; }
	inline Hashtable_t1407064410 ** get_address_of_nameValuePairs_0() { return &___nameValuePairs_0; }
	inline void set_nameValuePairs_0(Hashtable_t1407064410 * value)
	{
		___nameValuePairs_0 = value;
		Il2CppCodeGenWriteBarrier(&___nameValuePairs_0, value);
	}

	inline static int32_t get_offset_of_ins_1() { return static_cast<int32_t>(offsetof(SFJSONObject_t2643764442, ___ins_1)); }
	inline String_t* get_ins_1() const { return ___ins_1; }
	inline String_t** get_address_of_ins_1() { return &___ins_1; }
	inline void set_ins_1(String_t* value)
	{
		___ins_1 = value;
		Il2CppCodeGenWriteBarrier(&___ins_1, value);
	}

	inline static int32_t get_offset_of_cins_2() { return static_cast<int32_t>(offsetof(SFJSONObject_t2643764442, ___cins_2)); }
	inline CharU5BU5D_t3324145743* get_cins_2() const { return ___cins_2; }
	inline CharU5BU5D_t3324145743** get_address_of_cins_2() { return &___cins_2; }
	inline void set_cins_2(CharU5BU5D_t3324145743* value)
	{
		___cins_2 = value;
		Il2CppCodeGenWriteBarrier(&___cins_2, value);
	}

	inline static int32_t get_offset_of_pos_3() { return static_cast<int32_t>(offsetof(SFJSONObject_t2643764442, ___pos_3)); }
	inline int32_t get_pos_3() const { return ___pos_3; }
	inline int32_t* get_address_of_pos_3() { return &___pos_3; }
	inline void set_pos_3(int32_t value)
	{
		___pos_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
