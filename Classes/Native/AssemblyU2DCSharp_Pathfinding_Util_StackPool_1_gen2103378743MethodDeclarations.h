﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t2974409999;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Util.StackPool`1<System.Object>::.cctor()
extern "C"  void StackPool_1__cctor_m3413873323_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define StackPool_1__cctor_m3413873323(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))StackPool_1__cctor_m3413873323_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.Stack`1<T> Pathfinding.Util.StackPool`1<System.Object>::Claim()
extern "C"  Stack_1_t2974409999 * StackPool_1_Claim_m3952253811_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define StackPool_1_Claim_m3952253811(__this /* static, unused */, method) ((  Stack_1_t2974409999 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))StackPool_1_Claim_m3952253811_gshared)(__this /* static, unused */, method)
// System.Void Pathfinding.Util.StackPool`1<System.Object>::Warmup(System.Int32)
extern "C"  void StackPool_1_Warmup_m4025859507_gshared (Il2CppObject * __this /* static, unused */, int32_t ___count0, const MethodInfo* method);
#define StackPool_1_Warmup_m4025859507(__this /* static, unused */, ___count0, method) ((  void (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))StackPool_1_Warmup_m4025859507_gshared)(__this /* static, unused */, ___count0, method)
// System.Void Pathfinding.Util.StackPool`1<System.Object>::Release(System.Collections.Generic.Stack`1<T>)
extern "C"  void StackPool_1_Release_m206937007_gshared (Il2CppObject * __this /* static, unused */, Stack_1_t2974409999 * ___stack0, const MethodInfo* method);
#define StackPool_1_Release_m206937007(__this /* static, unused */, ___stack0, method) ((  void (*) (Il2CppObject * /* static, unused */, Stack_1_t2974409999 *, const MethodInfo*))StackPool_1_Release_m206937007_gshared)(__this /* static, unused */, ___stack0, method)
// System.Void Pathfinding.Util.StackPool`1<System.Object>::Clear()
extern "C"  void StackPool_1_Clear_m3350797677_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define StackPool_1_Clear_m3350797677(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))StackPool_1_Clear_m3350797677_gshared)(__this /* static, unused */, method)
// System.Int32 Pathfinding.Util.StackPool`1<System.Object>::GetSize()
extern "C"  int32_t StackPool_1_GetSize_m247741669_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define StackPool_1_GetSize_m247741669(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))StackPool_1_GetSize_m247741669_gshared)(__this /* static, unused */, method)
