﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Int32,System.Object,FightEnum.EDamageType,System.Int32>
struct U3CDelayCallExecU3Ec__Iterator42_4_t570329991;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Int32,System.Object,FightEnum.EDamageType,System.Int32>::.ctor()
extern "C"  void U3CDelayCallExecU3Ec__Iterator42_4__ctor_m3320434513_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t570329991 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4__ctor_m3320434513(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator42_4_t570329991 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4__ctor_m3320434513_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Int32,System.Object,FightEnum.EDamageType,System.Int32>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3803903009_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t570329991 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3803903009(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator42_4_t570329991 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3803903009_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Int32,System.Object,FightEnum.EDamageType,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_IEnumerator_get_Current_m1802721717_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t570329991 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_IEnumerator_get_Current_m1802721717(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator42_4_t570329991 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_IEnumerator_get_Current_m1802721717_gshared)(__this, method)
// System.Boolean BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Int32,System.Object,FightEnum.EDamageType,System.Int32>::MoveNext()
extern "C"  bool U3CDelayCallExecU3Ec__Iterator42_4_MoveNext_m4059401987_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t570329991 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4_MoveNext_m4059401987(__this, method) ((  bool (*) (U3CDelayCallExecU3Ec__Iterator42_4_t570329991 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4_MoveNext_m4059401987_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Int32,System.Object,FightEnum.EDamageType,System.Int32>::Dispose()
extern "C"  void U3CDelayCallExecU3Ec__Iterator42_4_Dispose_m3970116430_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t570329991 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4_Dispose_m3970116430(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator42_4_t570329991 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4_Dispose_m3970116430_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Int32,System.Object,FightEnum.EDamageType,System.Int32>::Reset()
extern "C"  void U3CDelayCallExecU3Ec__Iterator42_4_Reset_m966867454_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t570329991 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4_Reset_m966867454(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator42_4_t570329991 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4_Reset_m966867454_gshared)(__this, method)
