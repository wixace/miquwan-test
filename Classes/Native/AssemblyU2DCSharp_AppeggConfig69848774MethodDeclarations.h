﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

// System.Boolean AppeggConfig::get_IsDataCollected()
extern "C"  bool AppeggConfig_get_IsDataCollected_m2831111371 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppeggConfig::set_IsDataCollected(System.Boolean)
extern "C"  void AppeggConfig_set_IsDataCollected_m2931861570 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
