﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConfigAssetMgrGenerated
struct ConfigAssetMgrGenerated_t3414951461;
// JSVCall
struct JSVCall_t3708497963;
// ConfigAssetMgr
struct ConfigAssetMgr_t4036193930;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_ConfigAssetMgr4036193930.h"
#include "mscorlib_System_String7231557.h"

// System.Void ConfigAssetMgrGenerated::.ctor()
extern "C"  void ConfigAssetMgrGenerated__ctor_m793146086 (ConfigAssetMgrGenerated_t3414951461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConfigAssetMgrGenerated::ConfigAssetMgr_ConfigAssetMgr1(JSVCall,System.Int32)
extern "C"  bool ConfigAssetMgrGenerated_ConfigAssetMgr_ConfigAssetMgr1_m2409452108 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConfigAssetMgrGenerated::ConfigAssetMgr_isLoaded(JSVCall)
extern "C"  void ConfigAssetMgrGenerated_ConfigAssetMgr_isLoaded_m820949087 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConfigAssetMgrGenerated::ConfigAssetMgr_isLoading(JSVCall)
extern "C"  void ConfigAssetMgrGenerated_ConfigAssetMgr_isLoading_m582752844 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConfigAssetMgrGenerated::ConfigAssetMgr_loadProgress(JSVCall)
extern "C"  void ConfigAssetMgrGenerated_ConfigAssetMgr_loadProgress_m2580519291 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConfigAssetMgrGenerated::ConfigAssetMgr_AssetNameToABPath__String(JSVCall,System.Int32)
extern "C"  bool ConfigAssetMgrGenerated_ConfigAssetMgr_AssetNameToABPath__String_m1788809906 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConfigAssetMgrGenerated::ConfigAssetMgr_GetIsCompressByABName__String(JSVCall,System.Int32)
extern "C"  bool ConfigAssetMgrGenerated_ConfigAssetMgr_GetIsCompressByABName__String_m884261947 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConfigAssetMgrGenerated::ConfigAssetMgr_IsNullAsset__String(JSVCall,System.Int32)
extern "C"  bool ConfigAssetMgrGenerated_ConfigAssetMgr_IsNullAsset__String_m1027599669 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConfigAssetMgrGenerated::ConfigAssetMgr_LoadConfig(JSVCall,System.Int32)
extern "C"  bool ConfigAssetMgrGenerated_ConfigAssetMgr_LoadConfig_m727370981 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConfigAssetMgrGenerated::ConfigAssetMgr_LoadPathToABFullPath__String(JSVCall,System.Int32)
extern "C"  bool ConfigAssetMgrGenerated_ConfigAssetMgr_LoadPathToABFullPath__String_m2483151177 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConfigAssetMgrGenerated::ConfigAssetMgr_LoadPathToABPath__String(JSVCall,System.Int32)
extern "C"  bool ConfigAssetMgrGenerated_ConfigAssetMgr_LoadPathToABPath__String_m3372345306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConfigAssetMgrGenerated::ConfigAssetMgr_LoadPathToAssetName__String(JSVCall,System.Int32)
extern "C"  bool ConfigAssetMgrGenerated_ConfigAssetMgr_LoadPathToAssetName__String_m883014987 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConfigAssetMgrGenerated::__Register()
extern "C"  void ConfigAssetMgrGenerated___Register_m3809274273 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ConfigAssetMgrGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t ConfigAssetMgrGenerated_ilo_getObject1_m3235309840 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConfigAssetMgrGenerated::ilo_GetIsCompressByABName2(ConfigAssetMgr,System.String)
extern "C"  bool ConfigAssetMgrGenerated_ilo_GetIsCompressByABName2_m3189776764 (Il2CppObject * __this /* static, unused */, ConfigAssetMgr_t4036193930 * ____this0, String_t* ___abName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConfigAssetMgrGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void ConfigAssetMgrGenerated_ilo_setBooleanS3_m3781677077 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConfigAssetMgrGenerated::ilo_LoadPathToABPath4(ConfigAssetMgr,System.String)
extern "C"  String_t* ConfigAssetMgrGenerated_ilo_LoadPathToABPath4_m3265128846 (Il2CppObject * __this /* static, unused */, ConfigAssetMgr_t4036193930 * ____this0, String_t* ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConfigAssetMgrGenerated::ilo_getStringS5(System.Int32)
extern "C"  String_t* ConfigAssetMgrGenerated_ilo_getStringS5_m3441264270 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConfigAssetMgrGenerated::ilo_LoadPathToAssetName6(ConfigAssetMgr,System.String)
extern "C"  String_t* ConfigAssetMgrGenerated_ilo_LoadPathToAssetName6_m3770982433 (Il2CppObject * __this /* static, unused */, ConfigAssetMgr_t4036193930 * ____this0, String_t* ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
