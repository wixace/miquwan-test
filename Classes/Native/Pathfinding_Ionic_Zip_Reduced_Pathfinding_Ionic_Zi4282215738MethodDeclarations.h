﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Ionic.Zip.ZipException
struct ZipException_t4282215738;
// System.String
struct String_t;
// System.Exception
struct Exception_t3991598821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Exception3991598821.h"

// System.Void Pathfinding.Ionic.Zip.ZipException::.ctor()
extern "C"  void ZipException__ctor_m3778666775 (ZipException_t4282215738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Ionic.Zip.ZipException::.ctor(System.String)
extern "C"  void ZipException__ctor_m3001711435 (ZipException_t4282215738 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Ionic.Zip.ZipException::.ctor(System.String,System.Exception)
extern "C"  void ZipException__ctor_m4285262155 (ZipException_t4282215738 * __this, String_t* ___message0, Exception_t3991598821 * ___innerException1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
