﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebUtil/<GetDataFromBmob>c__Iterator1`1<System.Object>
struct U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void WebUtil/<GetDataFromBmob>c__Iterator1`1<System.Object>::.ctor()
extern "C"  void U3CGetDataFromBmobU3Ec__Iterator1_1__ctor_m797840073_gshared (U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690 * __this, const MethodInfo* method);
#define U3CGetDataFromBmobU3Ec__Iterator1_1__ctor_m797840073(__this, method) ((  void (*) (U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690 *, const MethodInfo*))U3CGetDataFromBmobU3Ec__Iterator1_1__ctor_m797840073_gshared)(__this, method)
// System.Object WebUtil/<GetDataFromBmob>c__Iterator1`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetDataFromBmobU3Ec__Iterator1_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1995742451_gshared (U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690 * __this, const MethodInfo* method);
#define U3CGetDataFromBmobU3Ec__Iterator1_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1995742451(__this, method) ((  Il2CppObject * (*) (U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690 *, const MethodInfo*))U3CGetDataFromBmobU3Ec__Iterator1_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1995742451_gshared)(__this, method)
// System.Object WebUtil/<GetDataFromBmob>c__Iterator1`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetDataFromBmobU3Ec__Iterator1_1_System_Collections_IEnumerator_get_Current_m15525511_gshared (U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690 * __this, const MethodInfo* method);
#define U3CGetDataFromBmobU3Ec__Iterator1_1_System_Collections_IEnumerator_get_Current_m15525511(__this, method) ((  Il2CppObject * (*) (U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690 *, const MethodInfo*))U3CGetDataFromBmobU3Ec__Iterator1_1_System_Collections_IEnumerator_get_Current_m15525511_gshared)(__this, method)
// System.Boolean WebUtil/<GetDataFromBmob>c__Iterator1`1<System.Object>::MoveNext()
extern "C"  bool U3CGetDataFromBmobU3Ec__Iterator1_1_MoveNext_m3551933299_gshared (U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690 * __this, const MethodInfo* method);
#define U3CGetDataFromBmobU3Ec__Iterator1_1_MoveNext_m3551933299(__this, method) ((  bool (*) (U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690 *, const MethodInfo*))U3CGetDataFromBmobU3Ec__Iterator1_1_MoveNext_m3551933299_gshared)(__this, method)
// System.Void WebUtil/<GetDataFromBmob>c__Iterator1`1<System.Object>::Dispose()
extern "C"  void U3CGetDataFromBmobU3Ec__Iterator1_1_Dispose_m2118414534_gshared (U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690 * __this, const MethodInfo* method);
#define U3CGetDataFromBmobU3Ec__Iterator1_1_Dispose_m2118414534(__this, method) ((  void (*) (U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690 *, const MethodInfo*))U3CGetDataFromBmobU3Ec__Iterator1_1_Dispose_m2118414534_gshared)(__this, method)
// System.Void WebUtil/<GetDataFromBmob>c__Iterator1`1<System.Object>::Reset()
extern "C"  void U3CGetDataFromBmobU3Ec__Iterator1_1_Reset_m2739240310_gshared (U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690 * __this, const MethodInfo* method);
#define U3CGetDataFromBmobU3Ec__Iterator1_1_Reset_m2739240310(__this, method) ((  void (*) (U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690 *, const MethodInfo*))U3CGetDataFromBmobU3Ec__Iterator1_1_Reset_m2739240310_gshared)(__this, method)
