﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PropertyReferenceGenerated
struct PropertyReferenceGenerated_t2646198617;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// PropertyReference
struct PropertyReference_t614957270;
// UnityEngine.Component
struct Component_t3501516275;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PropertyReference614957270.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Type2863145774.h"

// System.Void PropertyReferenceGenerated::.ctor()
extern "C"  void PropertyReferenceGenerated__ctor_m1096356706 (PropertyReferenceGenerated_t2646198617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyReferenceGenerated::PropertyReference_PropertyReference1(JSVCall,System.Int32)
extern "C"  bool PropertyReferenceGenerated_PropertyReference_PropertyReference1_m3386798058 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyReferenceGenerated::PropertyReference_PropertyReference2(JSVCall,System.Int32)
extern "C"  bool PropertyReferenceGenerated_PropertyReference_PropertyReference2_m336595243 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyReferenceGenerated::PropertyReference_target(JSVCall)
extern "C"  void PropertyReferenceGenerated_PropertyReference_target_m2348801163 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyReferenceGenerated::PropertyReference_name(JSVCall)
extern "C"  void PropertyReferenceGenerated_PropertyReference_name_m1200960081 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyReferenceGenerated::PropertyReference_isValid(JSVCall)
extern "C"  void PropertyReferenceGenerated_PropertyReference_isValid_m2182816286 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyReferenceGenerated::PropertyReference_isEnabled(JSVCall)
extern "C"  void PropertyReferenceGenerated_PropertyReference_isEnabled_m3187675001 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyReferenceGenerated::PropertyReference_Clear(JSVCall,System.Int32)
extern "C"  bool PropertyReferenceGenerated_PropertyReference_Clear_m206452672 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyReferenceGenerated::PropertyReference_Equals__Object(JSVCall,System.Int32)
extern "C"  bool PropertyReferenceGenerated_PropertyReference_Equals__Object_m2990514349 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyReferenceGenerated::PropertyReference_Get(JSVCall,System.Int32)
extern "C"  bool PropertyReferenceGenerated_PropertyReference_Get_m4805769 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyReferenceGenerated::PropertyReference_GetHashCode(JSVCall,System.Int32)
extern "C"  bool PropertyReferenceGenerated_PropertyReference_GetHashCode_m370982532 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyReferenceGenerated::PropertyReference_GetPropertyType(JSVCall,System.Int32)
extern "C"  bool PropertyReferenceGenerated_PropertyReference_GetPropertyType_m1624608280 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyReferenceGenerated::PropertyReference_Reset(JSVCall,System.Int32)
extern "C"  bool PropertyReferenceGenerated_PropertyReference_Reset_m438997314 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyReferenceGenerated::PropertyReference_Set__Component__String(JSVCall,System.Int32)
extern "C"  bool PropertyReferenceGenerated_PropertyReference_Set__Component__String_m1912732827 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyReferenceGenerated::PropertyReference_Set__Object(JSVCall,System.Int32)
extern "C"  bool PropertyReferenceGenerated_PropertyReference_Set__Object_m4277196436 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyReferenceGenerated::PropertyReference_ToString(JSVCall,System.Int32)
extern "C"  bool PropertyReferenceGenerated_PropertyReference_ToString_m763115291 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyReferenceGenerated::PropertyReference_Convert__Object__Type__Type(JSVCall,System.Int32)
extern "C"  bool PropertyReferenceGenerated_PropertyReference_Convert__Object__Type__Type_m1845965081 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyReferenceGenerated::PropertyReference_Convert__Object__Type(JSVCall,System.Int32)
extern "C"  bool PropertyReferenceGenerated_PropertyReference_Convert__Object__Type_m1470000095 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyReferenceGenerated::PropertyReference_Convert__Type__Type(JSVCall,System.Int32)
extern "C"  bool PropertyReferenceGenerated_PropertyReference_Convert__Type__Type_m3090150042 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyReferenceGenerated::PropertyReference_ToString__Component__String(JSVCall,System.Int32)
extern "C"  bool PropertyReferenceGenerated_PropertyReference_ToString__Component__String_m3657903189 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyReferenceGenerated::__Register()
extern "C"  void PropertyReferenceGenerated___Register_m657929893 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PropertyReferenceGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t PropertyReferenceGenerated_ilo_getObject1_m3448587120 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyReferenceGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool PropertyReferenceGenerated_ilo_attachFinalizerObject2_m2376823294 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyReferenceGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void PropertyReferenceGenerated_ilo_addJSCSRel3_m3824806432 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyReferenceGenerated::ilo_set_target4(PropertyReference,UnityEngine.Component)
extern "C"  void PropertyReferenceGenerated_ilo_set_target4_m3529135823 (Il2CppObject * __this /* static, unused */, PropertyReference_t614957270 * ____this0, Component_t3501516275 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PropertyReferenceGenerated::ilo_get_name5(PropertyReference)
extern "C"  String_t* PropertyReferenceGenerated_ilo_get_name5_m1731366481 (Il2CppObject * __this /* static, unused */, PropertyReference_t614957270 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyReferenceGenerated::ilo_set_name6(PropertyReference,System.String)
extern "C"  void PropertyReferenceGenerated_ilo_set_name6_m2448491517 (Il2CppObject * __this /* static, unused */, PropertyReference_t614957270 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyReferenceGenerated::ilo_get_isValid7(PropertyReference)
extern "C"  bool PropertyReferenceGenerated_ilo_get_isValid7_m1430043269 (Il2CppObject * __this /* static, unused */, PropertyReference_t614957270 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyReferenceGenerated::ilo_setBooleanS8(System.Int32,System.Boolean)
extern "C"  void PropertyReferenceGenerated_ilo_setBooleanS8_m3495529900 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyReferenceGenerated::ilo_get_isEnabled9(PropertyReference)
extern "C"  bool PropertyReferenceGenerated_ilo_get_isEnabled9_m3344246984 (Il2CppObject * __this /* static, unused */, PropertyReference_t614957270 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyReferenceGenerated::ilo_Clear10(PropertyReference)
extern "C"  void PropertyReferenceGenerated_ilo_Clear10_m2019134883 (Il2CppObject * __this /* static, unused */, PropertyReference_t614957270 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PropertyReferenceGenerated::ilo_getWhatever11(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * PropertyReferenceGenerated_ilo_getWhatever11_m3129766775 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyReferenceGenerated::ilo_setWhatever12(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  void PropertyReferenceGenerated_ilo_setWhatever12_m131374887 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyReferenceGenerated::ilo_Reset13(PropertyReference)
extern "C"  void PropertyReferenceGenerated_ilo_Reset13_m2878373630 (Il2CppObject * __this /* static, unused */, PropertyReference_t614957270 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PropertyReferenceGenerated::ilo_getStringS14(System.Int32)
extern "C"  String_t* PropertyReferenceGenerated_ilo_getStringS14_m1591231032 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyReferenceGenerated::ilo_Convert15(System.Object&,System.Type,System.Type)
extern "C"  bool PropertyReferenceGenerated_ilo_Convert15_m2386908718 (Il2CppObject * __this /* static, unused */, Il2CppObject ** ___value0, Type_t * ___from1, Type_t * ___to2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PropertyReferenceGenerated::ilo_getObject16(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * PropertyReferenceGenerated_ilo_getObject16_m2603460825 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyReferenceGenerated::ilo_Convert17(System.Object,System.Type)
extern "C"  bool PropertyReferenceGenerated_ilo_Convert17_m4246369269 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, Type_t * ___to1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PropertyReferenceGenerated::ilo_ToString18(UnityEngine.Component,System.String)
extern "C"  String_t* PropertyReferenceGenerated_ilo_ToString18_m2883423901 (Il2CppObject * __this /* static, unused */, Component_t3501516275 * ___comp0, String_t* ___property1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyReferenceGenerated::ilo_setStringS19(System.Int32,System.String)
extern "C"  void PropertyReferenceGenerated_ilo_setStringS19_m1902090826 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
