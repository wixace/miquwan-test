﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr_DGetV_1_gen4048467712MethodDeclarations.h"

// System.Void JSDataExchangeMgr/DGetV`1<UnityEngine.GUIStyle[]>::.ctor(System.Object,System.IntPtr)
#define DGetV_1__ctor_m1385928562(__this, ___object0, ___method1, method) ((  void (*) (DGetV_1_t443305900 *, Il2CppObject *, IntPtr_t, const MethodInfo*))DGetV_1__ctor_m3356914363_gshared)(__this, ___object0, ___method1, method)
// T JSDataExchangeMgr/DGetV`1<UnityEngine.GUIStyle[]>::Invoke()
#define DGetV_1_Invoke_m4121659245(__this, method) ((  GUIStyleU5BU5D_t565654559* (*) (DGetV_1_t443305900 *, const MethodInfo*))DGetV_1_Invoke_m1509146772_gshared)(__this, method)
// System.IAsyncResult JSDataExchangeMgr/DGetV`1<UnityEngine.GUIStyle[]>::BeginInvoke(System.AsyncCallback,System.Object)
#define DGetV_1_BeginInvoke_m2391727807(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (DGetV_1_t443305900 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))DGetV_1_BeginInvoke_m2681184398_gshared)(__this, ___callback0, ___object1, method)
// T JSDataExchangeMgr/DGetV`1<UnityEngine.GUIStyle[]>::EndInvoke(System.IAsyncResult)
#define DGetV_1_EndInvoke_m2465098979(__this, ___result0, method) ((  GUIStyleU5BU5D_t565654559* (*) (DGetV_1_t443305900 *, Il2CppObject *, const MethodInfo*))DGetV_1_EndInvoke_m1776565962_gshared)(__this, ___result0, method)
