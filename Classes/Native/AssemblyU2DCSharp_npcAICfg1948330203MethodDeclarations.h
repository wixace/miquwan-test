﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// npcAICfg
struct npcAICfg_t1948330203;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void npcAICfg::.ctor()
extern "C"  void npcAICfg__ctor_m503340064 (npcAICfg_t1948330203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void npcAICfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void npcAICfg_Init_m3866007397 (npcAICfg_t1948330203 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
