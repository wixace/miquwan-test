﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlotFightTalkMgr/PlotTermParam
struct PlotTermParam_t2297548846;

#include "codegen/il2cpp-codegen.h"

// System.Void PlotFightTalkMgr/PlotTermParam::.ctor()
extern "C"  void PlotTermParam__ctor_m3863289901 (PlotTermParam_t2297548846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
