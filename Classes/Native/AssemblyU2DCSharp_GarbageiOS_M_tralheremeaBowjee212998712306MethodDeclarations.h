﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_tralheremeaBowjee211
struct M_tralheremeaBowjee211_t2998712306;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_tralheremeaBowjee211::.ctor()
extern "C"  void M_tralheremeaBowjee211__ctor_m1184096897 (M_tralheremeaBowjee211_t2998712306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_tralheremeaBowjee211::M_keti0(System.String[],System.Int32)
extern "C"  void M_tralheremeaBowjee211_M_keti0_m3728852229 (M_tralheremeaBowjee211_t2998712306 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
