﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Diagnostics_StopwatchGenerated
struct System_Diagnostics_StopwatchGenerated_t26087333;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Diagnostics_StopwatchGenerated::.ctor()
extern "C"  void System_Diagnostics_StopwatchGenerated__ctor_m3584090982 (System_Diagnostics_StopwatchGenerated_t26087333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Diagnostics_StopwatchGenerated::Stopwatch_Stopwatch1(JSVCall,System.Int32)
extern "C"  bool System_Diagnostics_StopwatchGenerated_Stopwatch_Stopwatch1_m4237702992 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Diagnostics_StopwatchGenerated::Stopwatch_Frequency(JSVCall)
extern "C"  void System_Diagnostics_StopwatchGenerated_Stopwatch_Frequency_m1234486169 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Diagnostics_StopwatchGenerated::Stopwatch_IsHighResolution(JSVCall)
extern "C"  void System_Diagnostics_StopwatchGenerated_Stopwatch_IsHighResolution_m2658825727 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Diagnostics_StopwatchGenerated::Stopwatch_Elapsed(JSVCall)
extern "C"  void System_Diagnostics_StopwatchGenerated_Stopwatch_Elapsed_m201444153 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Diagnostics_StopwatchGenerated::Stopwatch_ElapsedMilliseconds(JSVCall)
extern "C"  void System_Diagnostics_StopwatchGenerated_Stopwatch_ElapsedMilliseconds_m3085721799 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Diagnostics_StopwatchGenerated::Stopwatch_ElapsedTicks(JSVCall)
extern "C"  void System_Diagnostics_StopwatchGenerated_Stopwatch_ElapsedTicks_m2689562141 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Diagnostics_StopwatchGenerated::Stopwatch_IsRunning(JSVCall)
extern "C"  void System_Diagnostics_StopwatchGenerated_Stopwatch_IsRunning_m3645550560 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Diagnostics_StopwatchGenerated::Stopwatch_Reset(JSVCall,System.Int32)
extern "C"  bool System_Diagnostics_StopwatchGenerated_Stopwatch_Reset_m2983873221 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Diagnostics_StopwatchGenerated::Stopwatch_Start(JSVCall,System.Int32)
extern "C"  bool System_Diagnostics_StopwatchGenerated_Stopwatch_Start_m2274759064 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Diagnostics_StopwatchGenerated::Stopwatch_Stop(JSVCall,System.Int32)
extern "C"  bool System_Diagnostics_StopwatchGenerated_Stopwatch_Stop_m2958983502 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Diagnostics_StopwatchGenerated::Stopwatch_GetTimestamp(JSVCall,System.Int32)
extern "C"  bool System_Diagnostics_StopwatchGenerated_Stopwatch_GetTimestamp_m1551625772 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Diagnostics_StopwatchGenerated::Stopwatch_StartNew(JSVCall,System.Int32)
extern "C"  bool System_Diagnostics_StopwatchGenerated_Stopwatch_StartNew_m2014744746 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Diagnostics_StopwatchGenerated::__Register()
extern "C"  void System_Diagnostics_StopwatchGenerated___Register_m505727265 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Diagnostics_StopwatchGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t System_Diagnostics_StopwatchGenerated_ilo_getObject1_m3384158480 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Diagnostics_StopwatchGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool System_Diagnostics_StopwatchGenerated_ilo_attachFinalizerObject2_m3123761298 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Diagnostics_StopwatchGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_Diagnostics_StopwatchGenerated_ilo_setObject3_m4065317130 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Diagnostics_StopwatchGenerated::ilo_setInt644(System.Int32,System.Int64)
extern "C"  void System_Diagnostics_StopwatchGenerated_ilo_setInt644_m1186472621 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int64_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
