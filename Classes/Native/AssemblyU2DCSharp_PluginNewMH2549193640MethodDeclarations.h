﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginNewMH
struct PluginNewMH_t2549193640;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_PluginNewMH2549193640.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void PluginNewMH::.ctor()
extern "C"  void PluginNewMH__ctor_m3792338115 (PluginNewMH_t2549193640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMH::.cctor()
extern "C"  void PluginNewMH__cctor_m1116268362 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMH::Init()
extern "C"  void PluginNewMH_Init_m628059441 (PluginNewMH_t2549193640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginNewMH::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginNewMH_ReqSDKHttpLogin_m4117406324 (PluginNewMH_t2549193640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginNewMH::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginNewMH_IsLoginSuccess_m791809864 (PluginNewMH_t2549193640 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMH::OpenUserLogin()
extern "C"  void PluginNewMH_OpenUserLogin_m2275945493 (PluginNewMH_t2549193640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMH::RoleEnterGame(CEvent.ZEvent)
extern "C"  void PluginNewMH_RoleEnterGame_m1030520486 (PluginNewMH_t2549193640 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMH::UserPay(CEvent.ZEvent)
extern "C"  void PluginNewMH_UserPay_m434804157 (PluginNewMH_t2549193640 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMH::OnLoginSuccess(System.String)
extern "C"  void PluginNewMH_OnLoginSuccess_m2502635400 (PluginNewMH_t2549193640 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMH::OnLoginFail(System.String)
extern "C"  void PluginNewMH_OnLoginFail_m2057547449 (PluginNewMH_t2549193640 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMH::OnPaySuccess(System.String)
extern "C"  void PluginNewMH_OnPaySuccess_m3869758407 (PluginNewMH_t2549193640 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMH::OnPayFail(System.String)
extern "C"  void PluginNewMH_OnPayFail_m1689815770 (PluginNewMH_t2549193640 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMH::OnExitGameSuccess(System.String)
extern "C"  void PluginNewMH_OnExitGameSuccess_m643836013 (PluginNewMH_t2549193640 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMH::OnLogoutSuccess(System.String)
extern "C"  void PluginNewMH_OnLogoutSuccess_m275828743 (PluginNewMH_t2549193640 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMH::OnLogoutFail(System.String)
extern "C"  void PluginNewMH_OnLogoutFail_m3467310746 (PluginNewMH_t2549193640 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMH::_playgo(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginNewMH__playgo_m4227406566 (PluginNewMH_t2549193640 * __this, String_t* ___productId0, String_t* ___roleId1, String_t* ___serverId2, String_t* ___type3, String_t* ___amount4, String_t* ___orderId5, String_t* ___pcbUrl6, String_t* ___appKey7, String_t* ___eargs8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMH::<OnLogoutSuccess>m__443()
extern "C"  void PluginNewMH_U3COnLogoutSuccessU3Em__443_m600527463 (PluginNewMH_t2549193640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginNewMH::ilo_get_Instance1()
extern "C"  VersionMgr_t1322950208 * PluginNewMH_ilo_get_Instance1_m3644938334 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginNewMH::ilo_get_PluginsSdkMgr2()
extern "C"  PluginsSdkMgr_t3884624670 * PluginNewMH_ilo_get_PluginsSdkMgr2_m3668955132 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginNewMH::ilo_get_DeviceID3(PluginsSdkMgr)
extern "C"  String_t* PluginNewMH_ilo_get_DeviceID3_m3463414954 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMH::ilo_OpenUserLogin4(PluginNewMH)
extern "C"  void PluginNewMH_ilo_OpenUserLogin4_m2202522870 (Il2CppObject * __this /* static, unused */, PluginNewMH_t2549193640 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMH::ilo_ReqSDKHttpLogin5(PluginsSdkMgr)
extern "C"  void PluginNewMH_ilo_ReqSDKHttpLogin5_m2976470224 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMH::ilo_DispatchEvent6(CEvent.ZEvent)
extern "C"  void PluginNewMH_ilo_DispatchEvent6_m2363017201 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginNewMH::ilo_Log7(System.Object,System.Boolean)
extern "C"  void PluginNewMH_ilo_Log7_m60933040 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
