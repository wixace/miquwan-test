﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.ProtoBeforeDeserializationAttribute
struct ProtoBeforeDeserializationAttribute_t3572722138;

#include "codegen/il2cpp-codegen.h"

// System.Void ProtoBuf.ProtoBeforeDeserializationAttribute::.ctor()
extern "C"  void ProtoBeforeDeserializationAttribute__ctor_m1162133962 (ProtoBeforeDeserializationAttribute_t3572722138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
