﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.Int3>
struct DefaultComparer_t482291371;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.Int3>::.ctor()
extern "C"  void DefaultComparer__ctor_m4224768916_gshared (DefaultComparer_t482291371 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m4224768916(__this, method) ((  void (*) (DefaultComparer_t482291371 *, const MethodInfo*))DefaultComparer__ctor_m4224768916_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.Int3>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3086036323_gshared (DefaultComparer_t482291371 * __this, Int3_t1974045594  ___x0, Int3_t1974045594  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m3086036323(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t482291371 *, Int3_t1974045594 , Int3_t1974045594 , const MethodInfo*))DefaultComparer_Compare_m3086036323_gshared)(__this, ___x0, ___y1, method)
