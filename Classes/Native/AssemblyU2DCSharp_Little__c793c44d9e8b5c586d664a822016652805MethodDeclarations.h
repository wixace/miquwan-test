﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._c793c44d9e8b5c586d664a82e74c7249
struct _c793c44d9e8b5c586d664a82e74c7249_t2016652805;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__c793c44d9e8b5c586d664a822016652805.h"

// System.Void Little._c793c44d9e8b5c586d664a82e74c7249::.ctor()
extern "C"  void _c793c44d9e8b5c586d664a82e74c7249__ctor_m2101505480 (_c793c44d9e8b5c586d664a82e74c7249_t2016652805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c793c44d9e8b5c586d664a82e74c7249::_c793c44d9e8b5c586d664a82e74c7249m2(System.Int32)
extern "C"  int32_t _c793c44d9e8b5c586d664a82e74c7249__c793c44d9e8b5c586d664a82e74c7249m2_m3388779385 (_c793c44d9e8b5c586d664a82e74c7249_t2016652805 * __this, int32_t ____c793c44d9e8b5c586d664a82e74c7249a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c793c44d9e8b5c586d664a82e74c7249::_c793c44d9e8b5c586d664a82e74c7249m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _c793c44d9e8b5c586d664a82e74c7249__c793c44d9e8b5c586d664a82e74c7249m_m2008995677 (_c793c44d9e8b5c586d664a82e74c7249_t2016652805 * __this, int32_t ____c793c44d9e8b5c586d664a82e74c7249a0, int32_t ____c793c44d9e8b5c586d664a82e74c724921, int32_t ____c793c44d9e8b5c586d664a82e74c7249c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c793c44d9e8b5c586d664a82e74c7249::ilo__c793c44d9e8b5c586d664a82e74c7249m21(Little._c793c44d9e8b5c586d664a82e74c7249,System.Int32)
extern "C"  int32_t _c793c44d9e8b5c586d664a82e74c7249_ilo__c793c44d9e8b5c586d664a82e74c7249m21_m4225672044 (Il2CppObject * __this /* static, unused */, _c793c44d9e8b5c586d664a82e74c7249_t2016652805 * ____this0, int32_t ____c793c44d9e8b5c586d664a82e74c7249a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
