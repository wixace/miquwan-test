﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEventListenerGenerated/<UIEventListener_onScroll_GetDelegate_member7_arg0>c__AnonStoreyBB
struct U3CUIEventListener_onScroll_GetDelegate_member7_arg0U3Ec__AnonStoreyBB_t1897928021;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UIEventListenerGenerated/<UIEventListener_onScroll_GetDelegate_member7_arg0>c__AnonStoreyBB::.ctor()
extern "C"  void U3CUIEventListener_onScroll_GetDelegate_member7_arg0U3Ec__AnonStoreyBB__ctor_m3925312950 (U3CUIEventListener_onScroll_GetDelegate_member7_arg0U3Ec__AnonStoreyBB_t1897928021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated/<UIEventListener_onScroll_GetDelegate_member7_arg0>c__AnonStoreyBB::<>m__13A(UnityEngine.GameObject,System.Single)
extern "C"  void U3CUIEventListener_onScroll_GetDelegate_member7_arg0U3Ec__AnonStoreyBB_U3CU3Em__13A_m294949055 (U3CUIEventListener_onScroll_GetDelegate_member7_arg0U3Ec__AnonStoreyBB_t1897928021 * __this, GameObject_t3674682005 * ___go0, float ___delta1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
