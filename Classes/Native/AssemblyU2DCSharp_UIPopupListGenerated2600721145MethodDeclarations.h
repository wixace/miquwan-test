﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIPopupListGenerated
struct UIPopupListGenerated_t2600721145;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t3071478659;
// UIPopupList
struct UIPopupList_t1804933942;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_UIPopupList1804933942.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"

// System.Void UIPopupListGenerated::.ctor()
extern "C"  void UIPopupListGenerated__ctor_m1430833090 (UIPopupListGenerated_t2600721145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPopupListGenerated::UIPopupList_UIPopupList1(JSVCall,System.Int32)
extern "C"  bool UIPopupListGenerated_UIPopupList_UIPopupList1_m2455829514 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_current(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_current_m1327470455 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_atlas(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_atlas_m153038629 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_bitmapFont(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_bitmapFont_m428908958 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_trueTypeFont(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_trueTypeFont_m1010958341 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_fontSize(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_fontSize_m2297263244 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_fontStyle(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_fontStyle_m1014885870 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_backgroundSprite(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_backgroundSprite_m1275632969 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_highlightSprite(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_highlightSprite_m3637922391 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_position(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_position_m2885069939 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_alignment(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_alignment_m2830000077 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_items(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_items_m3706345360 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_itemData(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_itemData_m121676479 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_padding(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_padding_m270795743 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_textColor(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_textColor_m2709520602 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_backgroundColor(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_backgroundColor_m1643199387 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_highlightColor(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_highlightColor_m3243422925 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_isAnimated(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_isAnimated_m764496815 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_isLocalized(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_isLocalized_m3698528639 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_openOn(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_openOn_m1605876051 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_onChange(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_onChange_m4089276909 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_ambigiousFont(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_ambigiousFont_m407203625 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_isOpen(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_isOpen_m1173663976 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_value(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_value_m577832191 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::UIPopupList_data(JSVCall)
extern "C"  void UIPopupListGenerated_UIPopupList_data_m4086596114 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPopupListGenerated::UIPopupList_AddItem__String__Object(JSVCall,System.Int32)
extern "C"  bool UIPopupListGenerated_UIPopupList_AddItem__String__Object_m2120949591 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPopupListGenerated::UIPopupList_AddItem__String(JSVCall,System.Int32)
extern "C"  bool UIPopupListGenerated_UIPopupList_AddItem__String_m1221700696 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPopupListGenerated::UIPopupList_Clear(JSVCall,System.Int32)
extern "C"  bool UIPopupListGenerated_UIPopupList_Clear_m1766030272 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPopupListGenerated::UIPopupList_Close(JSVCall,System.Int32)
extern "C"  bool UIPopupListGenerated_UIPopupList_Close_m2260582699 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPopupListGenerated::UIPopupList_Show(JSVCall,System.Int32)
extern "C"  bool UIPopupListGenerated_UIPopupList_Show_m1514588140 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::__Register()
extern "C"  void UIPopupListGenerated___Register_m4241047109 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPopupListGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UIPopupListGenerated_ilo_attachFinalizerObject1_m2171270749 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIPopupListGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIPopupListGenerated_ilo_getObject2_m4291977582 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::ilo_setStringS3(System.Int32,System.String)
extern "C"  void UIPopupListGenerated_ilo_setStringS3_m1551346317 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIPopupListGenerated::ilo_getStringS4(System.Int32)
extern "C"  String_t* UIPopupListGenerated_ilo_getStringS4_m1290822467 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::ilo_setEnum5(System.Int32,System.Int32)
extern "C"  void UIPopupListGenerated_ilo_setEnum5_m3549653727 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIPopupListGenerated::ilo_setObject6(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIPopupListGenerated_ilo_setObject6_m2327234637 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::ilo_setBooleanS7(System.Int32,System.Boolean)
extern "C"  void UIPopupListGenerated_ilo_setBooleanS7_m2257735277 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIPopupListGenerated::ilo_getEnum8(System.Int32)
extern "C"  int32_t UIPopupListGenerated_ilo_getEnum8_m1028428373 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UIPopupListGenerated::ilo_get_ambigiousFont9(UIPopupList)
extern "C"  Object_t3071478659 * UIPopupListGenerated_ilo_get_ambigiousFont9_m2439171941 (Il2CppObject * __this /* static, unused */, UIPopupList_t1804933942 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupListGenerated::ilo_set_ambigiousFont10(UIPopupList,UnityEngine.Object)
extern "C"  void UIPopupListGenerated_ilo_set_ambigiousFont10_m282321034 (Il2CppObject * __this /* static, unused */, UIPopupList_t1804933942 * ____this0, Object_t3071478659 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
