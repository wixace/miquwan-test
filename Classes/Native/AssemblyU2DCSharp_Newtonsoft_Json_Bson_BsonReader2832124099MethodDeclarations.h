﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Bson.BsonReader
struct BsonReader_t2832124099;
// System.IO.Stream
struct Stream_t1561764144;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// Newtonsoft.Json.Bson.BsonReader/ContainerContext
struct ContainerContext_t1096623169;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "mscorlib_System_DateTimeKind1472618179.h"
#include "mscorlib_System_Nullable_1_gen2038477154.h"
#include "mscorlib_System_Nullable_1_gen3968840829.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonReader_1096623169.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonType2455132538.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken4173078175.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonReader2832124099.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader_State2338717890.h"

// System.Void Newtonsoft.Json.Bson.BsonReader::.ctor(System.IO.Stream)
extern "C"  void BsonReader__ctor_m1288782854 (BsonReader_t2832124099 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonReader::.ctor(System.IO.Stream,System.Boolean,System.DateTimeKind)
extern "C"  void BsonReader__ctor_m602330229 (BsonReader_t2832124099 * __this, Stream_t1561764144 * ___stream0, bool ___readRootValueAsArray1, int32_t ___dateTimeKindHandling2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonReader::.cctor()
extern "C"  void BsonReader__cctor_m2753265182 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Bson.BsonReader::get_JsonNet35BinaryCompatibility()
extern "C"  bool BsonReader_get_JsonNet35BinaryCompatibility_m1044281218 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonReader::set_JsonNet35BinaryCompatibility(System.Boolean)
extern "C"  void BsonReader_set_JsonNet35BinaryCompatibility_m3617458209 (BsonReader_t2832124099 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Bson.BsonReader::get_ReadRootValueAsArray()
extern "C"  bool BsonReader_get_ReadRootValueAsArray_m4138334360 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonReader::set_ReadRootValueAsArray(System.Boolean)
extern "C"  void BsonReader_set_ReadRootValueAsArray_m3916698295 (BsonReader_t2832124099 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeKind Newtonsoft.Json.Bson.BsonReader::get_DateTimeKindHandling()
extern "C"  int32_t BsonReader_get_DateTimeKindHandling_m1038475661 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonReader::set_DateTimeKindHandling(System.DateTimeKind)
extern "C"  void BsonReader_set_DateTimeKindHandling_m1087010430 (BsonReader_t2832124099 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Bson.BsonReader::ReadElement()
extern "C"  String_t* BsonReader_ReadElement_m2092907382 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Newtonsoft.Json.Bson.BsonReader::ReadAsBytes()
extern "C"  ByteU5BU5D_t4260760469* BsonReader_ReadAsBytes_m3897108762 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Decimal> Newtonsoft.Json.Bson.BsonReader::ReadAsDecimal()
extern "C"  Nullable_1_t2038477154  BsonReader_ReadAsDecimal_m2095667016 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTimeOffset> Newtonsoft.Json.Bson.BsonReader::ReadAsDateTimeOffset()
extern "C"  Nullable_1_t3968840829  BsonReader_ReadAsDateTimeOffset_m8606698 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Bson.BsonReader::Read()
extern "C"  bool BsonReader_Read_m3483779799 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonReader::Close()
extern "C"  void BsonReader_Close_m152658053 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Bson.BsonReader::ReadCodeWScope()
extern "C"  bool BsonReader_ReadCodeWScope_m2410612065 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Bson.BsonReader::ReadReference()
extern "C"  bool BsonReader_ReadReference_m2196060246 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Bson.BsonReader::ReadNormal()
extern "C"  bool BsonReader_ReadNormal_m1860144542 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonReader::PopContext()
extern "C"  void BsonReader_PopContext_m3361882003 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonReader::PushContext(Newtonsoft.Json.Bson.BsonReader/ContainerContext)
extern "C"  void BsonReader_PushContext_m1711245665 (BsonReader_t2832124099 * __this, ContainerContext_t1096623169 * ___newContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Newtonsoft.Json.Bson.BsonReader::ReadByte()
extern "C"  uint8_t BsonReader_ReadByte_m3378240423 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonReader::ReadType(Newtonsoft.Json.Bson.BsonType)
extern "C"  void BsonReader_ReadType_m2577523738 (BsonReader_t2832124099 * __this, int8_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Newtonsoft.Json.Bson.BsonReader::ReadBinary()
extern "C"  ByteU5BU5D_t4260760469* BsonReader_ReadBinary_m1158328770 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Bson.BsonReader::ReadString()
extern "C"  String_t* BsonReader_ReadString_m762481465 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Bson.BsonReader::ReadLengthString()
extern "C"  String_t* BsonReader_ReadLengthString_m853162751 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Bson.BsonReader::GetString(System.Int32)
extern "C"  String_t* BsonReader_GetString_m719053896 (BsonReader_t2832124099 * __this, int32_t ___length0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Bson.BsonReader::GetLastFullCharStop(System.Int32)
extern "C"  int32_t BsonReader_GetLastFullCharStop_m2477586431 (BsonReader_t2832124099 * __this, int32_t ___start0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Bson.BsonReader::BytesInSequence(System.Byte)
extern "C"  int32_t BsonReader_BytesInSequence_m658835775 (BsonReader_t2832124099 * __this, uint8_t ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonReader::EnsureBuffers()
extern "C"  void BsonReader_EnsureBuffers_m1399648610 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Newtonsoft.Json.Bson.BsonReader::ReadDouble()
extern "C"  double BsonReader_ReadDouble_m2344895161 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Bson.BsonReader::ReadInt32()
extern "C"  int32_t BsonReader_ReadInt32_m651205683 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.Bson.BsonReader::ReadInt64()
extern "C"  int64_t BsonReader_ReadInt64_m4197420755 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonReader::ReadType()
extern "C"  int8_t BsonReader_ReadType_m3648727781 (BsonReader_t2832124099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonReader::MovePosition(System.Int32)
extern "C"  void BsonReader_MovePosition_m2914919520 (BsonReader_t2832124099 * __this, int32_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Newtonsoft.Json.Bson.BsonReader::ReadBytes(System.Int32)
extern "C"  ByteU5BU5D_t4260760469* BsonReader_ReadBytes_m408464637 (BsonReader_t2832124099 * __this, int32_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonToken Newtonsoft.Json.Bson.BsonReader::ilo_get_TokenType1(Newtonsoft.Json.JsonReader)
extern "C"  int32_t BsonReader_ilo_get_TokenType1_m1580266072 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Bson.BsonReader::ilo_FormatWith2(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* BsonReader_ilo_FormatWith2_m3308506096 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Bson.BsonReader::ilo_Read3(Newtonsoft.Json.Bson.BsonReader)
extern "C"  bool BsonReader_ilo_Read3_m947662923 (Il2CppObject * __this /* static, unused */, BsonReader_t2832124099 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Bson.BsonReader::ilo_get_Value4(Newtonsoft.Json.JsonReader)
extern "C"  Il2CppObject * BsonReader_ilo_get_Value4_m3661693151 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Bson.BsonReader::ilo_ReadReference5(Newtonsoft.Json.Bson.BsonReader)
extern "C"  bool BsonReader_ilo_ReadReference5_m4037776258 (Il2CppObject * __this /* static, unused */, BsonReader_t2832124099 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Bson.BsonReader::ilo_ReadCodeWScope6(Newtonsoft.Json.Bson.BsonReader)
extern "C"  bool BsonReader_ilo_ReadCodeWScope6_m518423410 (Il2CppObject * __this /* static, unused */, BsonReader_t2832124099 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonReader::ilo_SetToken7(Newtonsoft.Json.JsonReader,Newtonsoft.Json.JsonToken,System.Object)
extern "C"  void BsonReader_ilo_SetToken7_m566253880 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, int32_t ___newToken1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonReader/State Newtonsoft.Json.Bson.BsonReader::ilo_get_CurrentState8(Newtonsoft.Json.JsonReader)
extern "C"  int32_t BsonReader_ilo_get_CurrentState8_m3343067276 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Bson.BsonReader::ilo_ReadLengthString9(Newtonsoft.Json.Bson.BsonReader)
extern "C"  String_t* BsonReader_ilo_ReadLengthString9_m4130819117 (Il2CppObject * __this /* static, unused */, BsonReader_t2832124099 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Bson.BsonReader::ilo_ReadInt3210(Newtonsoft.Json.Bson.BsonReader)
extern "C"  int32_t BsonReader_ilo_ReadInt3210_m50324439 (Il2CppObject * __this /* static, unused */, BsonReader_t2832124099 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonReader::ilo_ReadType11(Newtonsoft.Json.Bson.BsonReader,Newtonsoft.Json.Bson.BsonType)
extern "C"  void BsonReader_ilo_ReadType11_m738686039 (Il2CppObject * __this /* static, unused */, BsonReader_t2832124099 * ____this0, int8_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Bson.BsonReader::ilo_ReadElement12(Newtonsoft.Json.Bson.BsonReader)
extern "C"  String_t* BsonReader_ilo_ReadElement12_m2970125042 (Il2CppObject * __this /* static, unused */, BsonReader_t2832124099 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonReader::ilo_SetToken13(Newtonsoft.Json.JsonReader,Newtonsoft.Json.JsonToken)
extern "C"  void BsonReader_ilo_SetToken13_m1456884497 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, int32_t ___newToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonReader::ilo_MovePosition14(Newtonsoft.Json.Bson.BsonReader,System.Int32)
extern "C"  void BsonReader_ilo_MovePosition14_m3260298566 (Il2CppObject * __this /* static, unused */, BsonReader_t2832124099 * ____this0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Newtonsoft.Json.Bson.BsonReader::ilo_ReadDouble15(Newtonsoft.Json.Bson.BsonReader)
extern "C"  double BsonReader_ilo_ReadDouble15_m3982305574 (Il2CppObject * __this /* static, unused */, BsonReader_t2832124099 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonReader::ilo_PushContext16(Newtonsoft.Json.Bson.BsonReader,Newtonsoft.Json.Bson.BsonReader/ContainerContext)
extern "C"  void BsonReader_ilo_PushContext16_m3886605123 (Il2CppObject * __this /* static, unused */, BsonReader_t2832124099 * ____this0, ContainerContext_t1096623169 * ___newContext1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Newtonsoft.Json.Bson.BsonReader::ilo_ReadBytes17(Newtonsoft.Json.Bson.BsonReader,System.Int32)
extern "C"  ByteU5BU5D_t4260760469* BsonReader_ilo_ReadBytes17_m284556768 (Il2CppObject * __this /* static, unused */, BsonReader_t2832124099 * ____this0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeKind Newtonsoft.Json.Bson.BsonReader::ilo_get_DateTimeKindHandling18(Newtonsoft.Json.Bson.BsonReader)
extern "C"  int32_t BsonReader_ilo_get_DateTimeKindHandling18_m1654275439 (Il2CppObject * __this /* static, unused */, BsonReader_t2832124099 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Bson.BsonReader::ilo_ReadString19(Newtonsoft.Json.Bson.BsonReader)
extern "C"  String_t* BsonReader_ilo_ReadString19_m1607016738 (Il2CppObject * __this /* static, unused */, BsonReader_t2832124099 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Bson.BsonReader::ilo_GetLastFullCharStop20(Newtonsoft.Json.Bson.BsonReader,System.Int32)
extern "C"  int32_t BsonReader_ilo_GetLastFullCharStop20_m3740686394 (Il2CppObject * __this /* static, unused */, BsonReader_t2832124099 * ____this0, int32_t ___start1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
