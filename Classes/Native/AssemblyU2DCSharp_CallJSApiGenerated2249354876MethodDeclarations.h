﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CallJSApiGenerated
struct CallJSApiGenerated_t2249354876;
// JSVCall
struct JSVCall_t3708497963;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"

// System.Void CallJSApiGenerated::.ctor()
extern "C"  void CallJSApiGenerated__ctor_m3900841375 (CallJSApiGenerated_t2249354876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CallJSApiGenerated::CallJSApi_CallJSApi1(JSVCall,System.Int32)
extern "C"  bool CallJSApiGenerated_CallJSApi_CallJSApi1_m261391251 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CallJSApiGenerated::CallJSApi_CallJsFun__String__Object_Array(JSVCall,System.Int32)
extern "C"  bool CallJSApiGenerated_CallJSApi_CallJsFun__String__Object_Array_m3108887663 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CallJSApiGenerated::CallJSApi_CallLoadJsObjToCs(JSVCall,System.Int32)
extern "C"  bool CallJSApiGenerated_CallJSApi_CallLoadJsObjToCs_m2817120322 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CallJSApiGenerated::__Register()
extern "C"  void CallJSApiGenerated___Register_m171707016 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] CallJSApiGenerated::<CallJSApi_CallJsFun__String__Object_Array>m__2B()
extern "C"  ObjectU5BU5D_t1108656482* CallJSApiGenerated_U3CCallJSApi_CallJsFun__String__Object_ArrayU3Em__2B_m2361335569 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CallJSApiGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t CallJSApiGenerated_ilo_getObject1_m3973832531 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CallJSApiGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool CallJSApiGenerated_ilo_attachFinalizerObject2_m3020380001 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CallJSApiGenerated::ilo_getStringS3(System.Int32)
extern "C"  String_t* CallJSApiGenerated_ilo_getStringS3_m421525087 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CallJSApiGenerated::ilo_CallJsFun4(System.String,System.Object[])
extern "C"  void CallJSApiGenerated_ilo_CallJsFun4_m3890053370 (Il2CppObject * __this /* static, unused */, String_t* ___funName0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
