﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr_DGetV_1_gen4048467712MethodDeclarations.h"

// System.Void JSDataExchangeMgr/DGetV`1<UnityEngine.Camera/CameraCallback>::.ctor(System.Object,System.IntPtr)
#define DGetV_1__ctor_m2392839676(__this, ___object0, ___method1, method) ((  void (*) (DGetV_1_t1823234442 *, Il2CppObject *, IntPtr_t, const MethodInfo*))DGetV_1__ctor_m3356914363_gshared)(__this, ___object0, ___method1, method)
// T JSDataExchangeMgr/DGetV`1<UnityEngine.Camera/CameraCallback>::Invoke()
#define DGetV_1_Invoke_m3639926165(__this, method) ((  CameraCallback_t1945583101 * (*) (DGetV_1_t1823234442 *, const MethodInfo*))DGetV_1_Invoke_m1509146772_gshared)(__this, method)
// System.IAsyncResult JSDataExchangeMgr/DGetV`1<UnityEngine.Camera/CameraCallback>::BeginInvoke(System.AsyncCallback,System.Object)
#define DGetV_1_BeginInvoke_m2411919597(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (DGetV_1_t1823234442 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))DGetV_1_BeginInvoke_m2681184398_gshared)(__this, ___callback0, ___object1, method)
// T JSDataExchangeMgr/DGetV`1<UnityEngine.Camera/CameraCallback>::EndInvoke(System.IAsyncResult)
#define DGetV_1_EndInvoke_m716387595(__this, ___result0, method) ((  CameraCallback_t1945583101 * (*) (DGetV_1_t1823234442 *, Il2CppObject *, const MethodInfo*))DGetV_1_EndInvoke_m1776565962_gshared)(__this, ___result0, method)
