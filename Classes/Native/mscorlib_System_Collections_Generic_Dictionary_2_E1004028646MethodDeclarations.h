﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1444361436MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4136294208(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1004028646 *, Dictionary_2_t3981672550 *, const MethodInfo*))Enumerator__ctor_m2407447341_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3460100139(__this, method) ((  Il2CppObject * (*) (Enumerator_t1004028646 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m804217620_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m830910837(__this, method) ((  void (*) (Enumerator_t1004028646 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2949223272_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4007952364(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1004028646 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m951612401_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3104055495(__this, method) ((  Il2CppObject * (*) (Enumerator_t1004028646 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m958726384_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3338463641(__this, method) ((  Il2CppObject * (*) (Enumerator_t1004028646 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3261490050_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::MoveNext()
#define Enumerator_MoveNext_m3699636750(__this, method) ((  bool (*) (Enumerator_t1004028646 *, const MethodInfo*))Enumerator_MoveNext_m104105044_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::get_Current()
#define Enumerator_get_Current_m4170010911(__this, method) ((  KeyValuePair_2_t3880453256  (*) (Enumerator_t1004028646 *, const MethodInfo*))Enumerator_get_Current_m2788080988_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m826871662(__this, method) ((  int32_t (*) (Enumerator_t1004028646 *, const MethodInfo*))Enumerator_get_CurrentKey_m3468423457_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2273098542(__this, method) ((  List_1_t3730483581 * (*) (Enumerator_t1004028646 *, const MethodInfo*))Enumerator_get_CurrentValue_m170021253_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::Reset()
#define Enumerator_Reset_m2209386898(__this, method) ((  void (*) (Enumerator_t1004028646 *, const MethodInfo*))Enumerator_Reset_m1177477631_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::VerifyState()
#define Enumerator_VerifyState_m3298880539(__this, method) ((  void (*) (Enumerator_t1004028646 *, const MethodInfo*))Enumerator_VerifyState_m2838132424_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3283205315(__this, method) ((  void (*) (Enumerator_t1004028646 *, const MethodInfo*))Enumerator_VerifyCurrent_m2885898288_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::Dispose()
#define Enumerator_Dispose_m4030393826(__this, method) ((  void (*) (Enumerator_t1004028646 *, const MethodInfo*))Enumerator_Dispose_m208066319_gshared)(__this, method)
