﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <PrivateImplementationDetails>{99f15b47-93f0-442d-a950-84e2e79a92c8}/$ArrayType=512
struct U24ArrayTypeU3D512_t2175736519;
struct U24ArrayTypeU3D512_t2175736519_marshaled_pinvoke;
struct U24ArrayTypeU3D512_t2175736519_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct U24ArrayTypeU3D512_t2175736519;
struct U24ArrayTypeU3D512_t2175736519_marshaled_pinvoke;

extern "C" void U24ArrayTypeU3D512_t2175736519_marshal_pinvoke(const U24ArrayTypeU3D512_t2175736519& unmarshaled, U24ArrayTypeU3D512_t2175736519_marshaled_pinvoke& marshaled);
extern "C" void U24ArrayTypeU3D512_t2175736519_marshal_pinvoke_back(const U24ArrayTypeU3D512_t2175736519_marshaled_pinvoke& marshaled, U24ArrayTypeU3D512_t2175736519& unmarshaled);
extern "C" void U24ArrayTypeU3D512_t2175736519_marshal_pinvoke_cleanup(U24ArrayTypeU3D512_t2175736519_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct U24ArrayTypeU3D512_t2175736519;
struct U24ArrayTypeU3D512_t2175736519_marshaled_com;

extern "C" void U24ArrayTypeU3D512_t2175736519_marshal_com(const U24ArrayTypeU3D512_t2175736519& unmarshaled, U24ArrayTypeU3D512_t2175736519_marshaled_com& marshaled);
extern "C" void U24ArrayTypeU3D512_t2175736519_marshal_com_back(const U24ArrayTypeU3D512_t2175736519_marshaled_com& marshaled, U24ArrayTypeU3D512_t2175736519& unmarshaled);
extern "C" void U24ArrayTypeU3D512_t2175736519_marshal_com_cleanup(U24ArrayTypeU3D512_t2175736519_marshaled_com& marshaled);
