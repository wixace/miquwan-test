﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<Pathfinding.Int3>
struct Comparer_1_t4225798881;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.Comparer`1<Pathfinding.Int3>::.ctor()
extern "C"  void Comparer_1__ctor_m1712948307_gshared (Comparer_1_t4225798881 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m1712948307(__this, method) ((  void (*) (Comparer_1_t4225798881 *, const MethodInfo*))Comparer_1__ctor_m1712948307_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<Pathfinding.Int3>::.cctor()
extern "C"  void Comparer_1__cctor_m1079693754_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m1079693754(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m1079693754_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<Pathfinding.Int3>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1912408760_gshared (Comparer_1_t4225798881 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m1912408760(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t4225798881 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m1912408760_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Pathfinding.Int3>::get_Default()
extern "C"  Comparer_1_t4225798881 * Comparer_1_get_Default_m188736315_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m188736315(__this /* static, unused */, method) ((  Comparer_1_t4225798881 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m188736315_gshared)(__this /* static, unused */, method)
