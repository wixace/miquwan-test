﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.Compression.Deflater
struct Deflater_t2454063301;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1587604368.h"

// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::.ctor(System.Int32,System.Boolean)
extern "C"  void Deflater__ctor_m4114082383 (Deflater_t2454063301 * __this, int32_t ___level0, bool ___noZlibHeaderOrFooter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::Reset()
extern "C"  void Deflater_Reset_m3657832522 (Deflater_t2454063301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Deflater::get_TotalIn()
extern "C"  int64_t Deflater_get_TotalIn_m2939095598 (Deflater_t2454063301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::Flush()
extern "C"  void Deflater_Flush_m1800379583 (Deflater_t2454063301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::Finish()
extern "C"  void Deflater_Finish_m1400398426 (Deflater_t2454063301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Deflater::get_IsFinished()
extern "C"  bool Deflater_get_IsFinished_m284667774 (Deflater_t2454063301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Deflater::get_IsNeedingInput()
extern "C"  bool Deflater_get_IsNeedingInput_m862754346 (Deflater_t2454063301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::SetInput(System.Byte[],System.Int32,System.Int32)
extern "C"  void Deflater_SetInput_m1771564410 (Deflater_t2454063301 * __this, ByteU5BU5D_t4260760469* ___input0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::SetLevel(System.Int32)
extern "C"  void Deflater_SetLevel_m3168569498 (Deflater_t2454063301 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::SetStrategy(ICSharpCode.SharpZipLib.Zip.Compression.DeflateStrategy)
extern "C"  void Deflater_SetStrategy_m1748227658 (Deflater_t2454063301 * __this, int32_t ___strategy0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Deflater::Deflate(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t Deflater_Deflate_m1433311247 (Deflater_t2454063301 * __this, ByteU5BU5D_t4260760469* ___output0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
