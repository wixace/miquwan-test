﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>
struct List_1_t1195184494;
// System.Collections.Generic.IEnumerable`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>
struct IEnumerable_1_t3127911899;
// System.Collections.Generic.IEnumerator`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>
struct IEnumerator_1_t1738863991;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>
struct ICollection_1_t721588929;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>
struct ReadOnlyCollection_1_t1384076478;
// UnityEngine.MeshSubsetCombineUtility/MeshInstance[]
struct MeshInstanceU5BU5D_t1318761771;
// System.Predicate`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>
struct Predicate_1_t3733023121;
// System.Collections.Generic.IComparer`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>
struct IComparer_1_t2402012984;
// System.Comparison`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>
struct Comparison_1_t2838327425;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_MeshSubsetCombineUtility_M4121966238.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1214857264.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::.ctor()
extern "C"  void List_1__ctor_m1430592358_gshared (List_1_t1195184494 * __this, const MethodInfo* method);
#define List_1__ctor_m1430592358(__this, method) ((  void (*) (List_1_t1195184494 *, const MethodInfo*))List_1__ctor_m1430592358_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m2366528171_gshared (List_1_t1195184494 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m2366528171(__this, ___collection0, method) ((  void (*) (List_1_t1195184494 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m2366528171_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3261422757_gshared (List_1_t1195184494 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m3261422757(__this, ___capacity0, method) ((  void (*) (List_1_t1195184494 *, int32_t, const MethodInfo*))List_1__ctor_m3261422757_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::.cctor()
extern "C"  void List_1__cctor_m3811274969_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3811274969(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3811274969_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3899146150_gshared (List_1_t1195184494 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3899146150(__this, method) ((  Il2CppObject* (*) (List_1_t1195184494 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3899146150_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3379777136_gshared (List_1_t1195184494 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3379777136(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1195184494 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3379777136_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m2765180011_gshared (List_1_t1195184494 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2765180011(__this, method) ((  Il2CppObject * (*) (List_1_t1195184494 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m2765180011_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m3804649318_gshared (List_1_t1195184494 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m3804649318(__this, ___item0, method) ((  int32_t (*) (List_1_t1195184494 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3804649318_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m368420134_gshared (List_1_t1195184494 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m368420134(__this, ___item0, method) ((  bool (*) (List_1_t1195184494 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m368420134_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2392877758_gshared (List_1_t1195184494 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m2392877758(__this, ___item0, method) ((  int32_t (*) (List_1_t1195184494 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m2392877758_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3546836969_gshared (List_1_t1195184494 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3546836969(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1195184494 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3546836969_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m390501023_gshared (List_1_t1195184494 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m390501023(__this, ___item0, method) ((  void (*) (List_1_t1195184494 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m390501023_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m143252903_gshared (List_1_t1195184494 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m143252903(__this, method) ((  bool (*) (List_1_t1195184494 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m143252903_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m1957746806_gshared (List_1_t1195184494 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1957746806(__this, method) ((  bool (*) (List_1_t1195184494 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1957746806_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m624659554_gshared (List_1_t1195184494 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m624659554(__this, method) ((  Il2CppObject * (*) (List_1_t1195184494 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m624659554_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m298952981_gshared (List_1_t1195184494 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m298952981(__this, method) ((  bool (*) (List_1_t1195184494 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m298952981_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m2818957124_gshared (List_1_t1195184494 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m2818957124(__this, method) ((  bool (*) (List_1_t1195184494 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2818957124_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m68021609_gshared (List_1_t1195184494 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m68021609(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1195184494 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m68021609_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3215213952_gshared (List_1_t1195184494 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3215213952(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1195184494 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3215213952_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Add(T)
extern "C"  void List_1_Add_m1124953174_gshared (List_1_t1195184494 * __this, MeshInstance_t4121966238  ___item0, const MethodInfo* method);
#define List_1_Add_m1124953174(__this, ___item0, method) ((  void (*) (List_1_t1195184494 *, MeshInstance_t4121966238 , const MethodInfo*))List_1_Add_m1124953174_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3449629542_gshared (List_1_t1195184494 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3449629542(__this, ___newCount0, method) ((  void (*) (List_1_t1195184494 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3449629542_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m909900993_gshared (List_1_t1195184494 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m909900993(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t1195184494 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m909900993_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2803264100_gshared (List_1_t1195184494 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2803264100(__this, ___collection0, method) ((  void (*) (List_1_t1195184494 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2803264100_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2064236324_gshared (List_1_t1195184494 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2064236324(__this, ___enumerable0, method) ((  void (*) (List_1_t1195184494 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2064236324_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3403499763_gshared (List_1_t1195184494 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3403499763(__this, ___collection0, method) ((  void (*) (List_1_t1195184494 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3403499763_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1384076478 * List_1_AsReadOnly_m239356630_gshared (List_1_t1195184494 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m239356630(__this, method) ((  ReadOnlyCollection_1_t1384076478 * (*) (List_1_t1195184494 *, const MethodInfo*))List_1_AsReadOnly_m239356630_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m2222770345_gshared (List_1_t1195184494 * __this, MeshInstance_t4121966238  ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m2222770345(__this, ___item0, method) ((  int32_t (*) (List_1_t1195184494 *, MeshInstance_t4121966238 , const MethodInfo*))List_1_BinarySearch_m2222770345_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Clear()
extern "C"  void List_1_Clear_m3131692945_gshared (List_1_t1195184494 * __this, const MethodInfo* method);
#define List_1_Clear_m3131692945(__this, method) ((  void (*) (List_1_t1195184494 *, const MethodInfo*))List_1_Clear_m3131692945_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Contains(T)
extern "C"  bool List_1_Contains_m2875842605_gshared (List_1_t1195184494 * __this, MeshInstance_t4121966238  ___item0, const MethodInfo* method);
#define List_1_Contains_m2875842605(__this, ___item0, method) ((  bool (*) (List_1_t1195184494 *, MeshInstance_t4121966238 , const MethodInfo*))List_1_Contains_m2875842605_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3174249435_gshared (List_1_t1195184494 * __this, MeshInstanceU5BU5D_t1318761771* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m3174249435(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1195184494 *, MeshInstanceU5BU5D_t1318761771*, int32_t, const MethodInfo*))List_1_CopyTo_m3174249435_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Find(System.Predicate`1<T>)
extern "C"  MeshInstance_t4121966238  List_1_Find_m2069312941_gshared (List_1_t1195184494 * __this, Predicate_1_t3733023121 * ___match0, const MethodInfo* method);
#define List_1_Find_m2069312941(__this, ___match0, method) ((  MeshInstance_t4121966238  (*) (List_1_t1195184494 *, Predicate_1_t3733023121 *, const MethodInfo*))List_1_Find_m2069312941_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1993085288_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3733023121 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1993085288(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3733023121 *, const MethodInfo*))List_1_CheckMatch_m1993085288_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3635676813_gshared (List_1_t1195184494 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3733023121 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3635676813(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1195184494 *, int32_t, int32_t, Predicate_1_t3733023121 *, const MethodInfo*))List_1_GetIndex_m3635676813_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::GetEnumerator()
extern "C"  Enumerator_t1214857264  List_1_GetEnumerator_m2868213482_gshared (List_1_t1195184494 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m2868213482(__this, method) ((  Enumerator_t1214857264  (*) (List_1_t1195184494 *, const MethodInfo*))List_1_GetEnumerator_m2868213482_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2955341471_gshared (List_1_t1195184494 * __this, MeshInstance_t4121966238  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2955341471(__this, ___item0, method) ((  int32_t (*) (List_1_t1195184494 *, MeshInstance_t4121966238 , const MethodInfo*))List_1_IndexOf_m2955341471_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3207790386_gshared (List_1_t1195184494 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3207790386(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1195184494 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3207790386_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2920616619_gshared (List_1_t1195184494 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2920616619(__this, ___index0, method) ((  void (*) (List_1_t1195184494 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2920616619_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m1714586898_gshared (List_1_t1195184494 * __this, int32_t ___index0, MeshInstance_t4121966238  ___item1, const MethodInfo* method);
#define List_1_Insert_m1714586898(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1195184494 *, int32_t, MeshInstance_t4121966238 , const MethodInfo*))List_1_Insert_m1714586898_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m1518973959_gshared (List_1_t1195184494 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m1518973959(__this, ___collection0, method) ((  void (*) (List_1_t1195184494 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m1518973959_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Remove(T)
extern "C"  bool List_1_Remove_m2147665768_gshared (List_1_t1195184494 * __this, MeshInstance_t4121966238  ___item0, const MethodInfo* method);
#define List_1_Remove_m2147665768(__this, ___item0, method) ((  bool (*) (List_1_t1195184494 *, MeshInstance_t4121966238 , const MethodInfo*))List_1_Remove_m2147665768_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m3725157666_gshared (List_1_t1195184494 * __this, Predicate_1_t3733023121 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m3725157666(__this, ___match0, method) ((  int32_t (*) (List_1_t1195184494 *, Predicate_1_t3733023121 *, const MethodInfo*))List_1_RemoveAll_m3725157666_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3883407064_gshared (List_1_t1195184494 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3883407064(__this, ___index0, method) ((  void (*) (List_1_t1195184494 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3883407064_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m2172338939_gshared (List_1_t1195184494 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m2172338939(__this, ___index0, ___count1, method) ((  void (*) (List_1_t1195184494 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m2172338939_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Reverse()
extern "C"  void List_1_Reverse_m4283298964_gshared (List_1_t1195184494 * __this, const MethodInfo* method);
#define List_1_Reverse_m4283298964(__this, method) ((  void (*) (List_1_t1195184494 *, const MethodInfo*))List_1_Reverse_m4283298964_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Sort()
extern "C"  void List_1_Sort_m2719462286_gshared (List_1_t1195184494 * __this, const MethodInfo* method);
#define List_1_Sort_m2719462286(__this, method) ((  void (*) (List_1_t1195184494 *, const MethodInfo*))List_1_Sort_m2719462286_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3518392854_gshared (List_1_t1195184494 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m3518392854(__this, ___comparer0, method) ((  void (*) (List_1_t1195184494 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3518392854_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1711833441_gshared (List_1_t1195184494 * __this, Comparison_1_t2838327425 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1711833441(__this, ___comparison0, method) ((  void (*) (List_1_t1195184494 *, Comparison_1_t2838327425 *, const MethodInfo*))List_1_Sort_m1711833441_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::ToArray()
extern "C"  MeshInstanceU5BU5D_t1318761771* List_1_ToArray_m3198213156_gshared (List_1_t1195184494 * __this, const MethodInfo* method);
#define List_1_ToArray_m3198213156(__this, method) ((  MeshInstanceU5BU5D_t1318761771* (*) (List_1_t1195184494 *, const MethodInfo*))List_1_ToArray_m3198213156_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2955505831_gshared (List_1_t1195184494 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2955505831(__this, method) ((  void (*) (List_1_t1195184494 *, const MethodInfo*))List_1_TrimExcess_m2955505831_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m803241807_gshared (List_1_t1195184494 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m803241807(__this, method) ((  int32_t (*) (List_1_t1195184494 *, const MethodInfo*))List_1_get_Capacity_m803241807_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m659818360_gshared (List_1_t1195184494 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m659818360(__this, ___value0, method) ((  void (*) (List_1_t1195184494 *, int32_t, const MethodInfo*))List_1_set_Capacity_m659818360_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::get_Count()
extern "C"  int32_t List_1_get_Count_m1822171114_gshared (List_1_t1195184494 * __this, const MethodInfo* method);
#define List_1_get_Count_m1822171114(__this, method) ((  int32_t (*) (List_1_t1195184494 *, const MethodInfo*))List_1_get_Count_m1822171114_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::get_Item(System.Int32)
extern "C"  MeshInstance_t4121966238  List_1_get_Item_m3801722076_gshared (List_1_t1195184494 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3801722076(__this, ___index0, method) ((  MeshInstance_t4121966238  (*) (List_1_t1195184494 *, int32_t, const MethodInfo*))List_1_get_Item_m3801722076_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m260163945_gshared (List_1_t1195184494 * __this, int32_t ___index0, MeshInstance_t4121966238  ___value1, const MethodInfo* method);
#define List_1_set_Item_m260163945(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1195184494 *, int32_t, MeshInstance_t4121966238 , const MethodInfo*))List_1_set_Item_m260163945_gshared)(__this, ___index0, ___value1, method)
