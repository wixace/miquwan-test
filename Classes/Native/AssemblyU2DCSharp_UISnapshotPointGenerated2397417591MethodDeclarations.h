﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UISnapshotPointGenerated
struct UISnapshotPointGenerated_t2397417591;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UISnapshotPointGenerated::.ctor()
extern "C"  void UISnapshotPointGenerated__ctor_m4024792068 (UISnapshotPointGenerated_t2397417591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISnapshotPointGenerated::UISnapshotPoint_UISnapshotPoint1(JSVCall,System.Int32)
extern "C"  bool UISnapshotPointGenerated_UISnapshotPoint_UISnapshotPoint1_m820016644 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISnapshotPointGenerated::UISnapshotPoint_isOrthographic(JSVCall)
extern "C"  void UISnapshotPointGenerated_UISnapshotPoint_isOrthographic_m1219537222 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISnapshotPointGenerated::UISnapshotPoint_nearClip(JSVCall)
extern "C"  void UISnapshotPointGenerated_UISnapshotPoint_nearClip_m1852196200 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISnapshotPointGenerated::UISnapshotPoint_farClip(JSVCall)
extern "C"  void UISnapshotPointGenerated_UISnapshotPoint_farClip_m1552225957 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISnapshotPointGenerated::UISnapshotPoint_fieldOfView(JSVCall)
extern "C"  void UISnapshotPointGenerated_UISnapshotPoint_fieldOfView_m2809795798 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISnapshotPointGenerated::UISnapshotPoint_orthoSize(JSVCall)
extern "C"  void UISnapshotPointGenerated_UISnapshotPoint_orthoSize_m748956819 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISnapshotPointGenerated::UISnapshotPoint_thumbnail(JSVCall)
extern "C"  void UISnapshotPointGenerated_UISnapshotPoint_thumbnail_m3163118816 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISnapshotPointGenerated::__Register()
extern "C"  void UISnapshotPointGenerated___Register_m2513955651 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISnapshotPointGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UISnapshotPointGenerated_ilo_attachFinalizerObject1_m452943963 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISnapshotPointGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void UISnapshotPointGenerated_ilo_setSingle2_m4269402737 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
