﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.RecastBBTreeBox
struct RecastBBTreeBox_t3100392477;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RecastBBTree
struct  RecastBBTree_t4266206694  : public Il2CppObject
{
public:
	// Pathfinding.RecastBBTreeBox Pathfinding.RecastBBTree::root
	RecastBBTreeBox_t3100392477 * ___root_0;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(RecastBBTree_t4266206694, ___root_0)); }
	inline RecastBBTreeBox_t3100392477 * get_root_0() const { return ___root_0; }
	inline RecastBBTreeBox_t3100392477 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(RecastBBTreeBox_t3100392477 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier(&___root_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
