﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Component_ExtensionGenerated
struct Component_ExtensionGenerated_t1198738130;
// JSVCall
struct JSVCall_t3708497963;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type
struct Type_t;
// MethodID
struct MethodID_t3916401116;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_MethodID3916401116.h"

// System.Void Component_ExtensionGenerated::.ctor()
extern "C"  void Component_ExtensionGenerated__ctor_m478118921 (Component_ExtensionGenerated_t1198738130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Component_ExtensionGenerated::.cctor()
extern "C"  void Component_ExtensionGenerated__cctor_m1454688452 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Component_ExtensionGenerated::Component_Extension_GetOrCreateComponentT1__Component(JSVCall,System.Int32)
extern "C"  bool Component_ExtensionGenerated_Component_Extension_GetOrCreateComponentT1__Component_m1498888761 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Component_ExtensionGenerated::Component_Extension_SafeGetComponentT1__Component(JSVCall,System.Int32)
extern "C"  bool Component_ExtensionGenerated_Component_Extension_SafeGetComponentT1__Component_m2299785869 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Component_ExtensionGenerated::__Register()
extern "C"  void Component_ExtensionGenerated___Register_m2408584798 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo Component_ExtensionGenerated::ilo_makeGenericMethod1(System.Type,MethodID,System.Int32)
extern "C"  MethodInfo_t * Component_ExtensionGenerated_ilo_makeGenericMethod1_m694565447 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MethodID_t3916401116 * ___methodID1, int32_t ___TCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
