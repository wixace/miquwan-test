﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.ClipperLib.Join
struct Join_t3970117804;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.ClipperLib.Join::.ctor()
extern "C"  void Join__ctor_m1417981953 (Join_t3970117804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
