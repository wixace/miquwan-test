﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_rihoPole230
struct M_rihoPole230_t2695664953;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_rihoPole2302695664953.h"

// System.Void GarbageiOS.M_rihoPole230::.ctor()
extern "C"  void M_rihoPole230__ctor_m2597303882 (M_rihoPole230_t2695664953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rihoPole230::M_deceljeTuxainere0(System.String[],System.Int32)
extern "C"  void M_rihoPole230_M_deceljeTuxainere0_m1695155640 (M_rihoPole230_t2695664953 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rihoPole230::M_lejuPembomoo1(System.String[],System.Int32)
extern "C"  void M_rihoPole230_M_lejuPembomoo1_m1499688402 (M_rihoPole230_t2695664953 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rihoPole230::M_howcaleStaicalai2(System.String[],System.Int32)
extern "C"  void M_rihoPole230_M_howcaleStaicalai2_m2762158489 (M_rihoPole230_t2695664953 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rihoPole230::M_zerpistrooLerere3(System.String[],System.Int32)
extern "C"  void M_rihoPole230_M_zerpistrooLerere3_m1027300496 (M_rihoPole230_t2695664953 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rihoPole230::M_kalwor4(System.String[],System.Int32)
extern "C"  void M_rihoPole230_M_kalwor4_m1155109501 (M_rihoPole230_t2695664953 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rihoPole230::M_poomeretalJejooge5(System.String[],System.Int32)
extern "C"  void M_rihoPole230_M_poomeretalJejooge5_m187073103 (M_rihoPole230_t2695664953 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rihoPole230::M_borhi6(System.String[],System.Int32)
extern "C"  void M_rihoPole230_M_borhi6_m247915891 (M_rihoPole230_t2695664953 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rihoPole230::M_vabutay7(System.String[],System.Int32)
extern "C"  void M_rihoPole230_M_vabutay7_m300924108 (M_rihoPole230_t2695664953 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rihoPole230::M_drawrisqereTemciqas8(System.String[],System.Int32)
extern "C"  void M_rihoPole230_M_drawrisqereTemciqas8_m829418395 (M_rihoPole230_t2695664953 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rihoPole230::M_licasTreye9(System.String[],System.Int32)
extern "C"  void M_rihoPole230_M_licasTreye9_m2992626891 (M_rihoPole230_t2695664953 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rihoPole230::M_bomaLozihas10(System.String[],System.Int32)
extern "C"  void M_rihoPole230_M_bomaLozihas10_m2519049843 (M_rihoPole230_t2695664953 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rihoPole230::M_sezarde11(System.String[],System.Int32)
extern "C"  void M_rihoPole230_M_sezarde11_m3644795527 (M_rihoPole230_t2695664953 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rihoPole230::M_sairtowkel12(System.String[],System.Int32)
extern "C"  void M_rihoPole230_M_sairtowkel12_m1220394577 (M_rihoPole230_t2695664953 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rihoPole230::M_demaiMairvorsou13(System.String[],System.Int32)
extern "C"  void M_rihoPole230_M_demaiMairvorsou13_m1342515104 (M_rihoPole230_t2695664953 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rihoPole230::ilo_M_howcaleStaicalai21(GarbageiOS.M_rihoPole230,System.String[],System.Int32)
extern "C"  void M_rihoPole230_ilo_M_howcaleStaicalai21_m270902218 (Il2CppObject * __this /* static, unused */, M_rihoPole230_t2695664953 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rihoPole230::ilo_M_borhi62(GarbageiOS.M_rihoPole230,System.String[],System.Int32)
extern "C"  void M_rihoPole230_ilo_M_borhi62_m2374897481 (Il2CppObject * __this /* static, unused */, M_rihoPole230_t2695664953 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rihoPole230::ilo_M_bomaLozihas103(GarbageiOS.M_rihoPole230,System.String[],System.Int32)
extern "C"  void M_rihoPole230_ilo_M_bomaLozihas103_m3003072610 (Il2CppObject * __this /* static, unused */, M_rihoPole230_t2695664953 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rihoPole230::ilo_M_sezarde114(GarbageiOS.M_rihoPole230,System.String[],System.Int32)
extern "C"  void M_rihoPole230_ilo_M_sezarde114_m1412818133 (Il2CppObject * __this /* static, unused */, M_rihoPole230_t2695664953 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
