﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnemyDropData
struct EnemyDropData_t1468587713;

#include "codegen/il2cpp-codegen.h"

// System.Void EnemyDropData::.ctor()
extern "C"  void EnemyDropData__ctor_m1672055754 (EnemyDropData_t1468587713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropData::.ctor(System.Int32,System.Int32)
extern "C"  void EnemyDropData__ctor_m1048886940 (EnemyDropData_t1468587713 * __this, int32_t ___id0, int32_t ___num1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropData::set_dropId(System.Int32)
extern "C"  void EnemyDropData_set_dropId_m515121746 (EnemyDropData_t1468587713 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EnemyDropData::get_dropId()
extern "C"  int32_t EnemyDropData_get_dropId_m1658547007 (EnemyDropData_t1468587713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyDropData::set_dropNum(System.Int32)
extern "C"  void EnemyDropData_set_dropNum_m712498899 (EnemyDropData_t1468587713 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EnemyDropData::get_dropNum()
extern "C"  int32_t EnemyDropData_get_dropNum_m4175507332 (EnemyDropData_t1468587713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
