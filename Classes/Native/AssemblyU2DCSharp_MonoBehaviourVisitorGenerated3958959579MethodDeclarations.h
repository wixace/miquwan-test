﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MonoBehaviourVisitorGenerated
struct MonoBehaviourVisitorGenerated_t3958959579;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void MonoBehaviourVisitorGenerated::.ctor()
extern "C"  void MonoBehaviourVisitorGenerated__ctor_m3650161136 (MonoBehaviourVisitorGenerated_t3958959579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonoBehaviourVisitorGenerated::MonoBehaviourVisitor_FindChild__MonoBehaviour__String(JSVCall,System.Int32)
extern "C"  bool MonoBehaviourVisitorGenerated_MonoBehaviourVisitor_FindChild__MonoBehaviour__String_m3649291269 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MonoBehaviourVisitorGenerated::MonoBehaviourVisitor_FindChildren__MonoBehaviour__String__ListT1_GameObject(JSVCall,System.Int32)
extern "C"  bool MonoBehaviourVisitorGenerated_MonoBehaviourVisitor_FindChildren__MonoBehaviour__String__ListT1_GameObject_m1405957023 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonoBehaviourVisitorGenerated::__Register()
extern "C"  void MonoBehaviourVisitorGenerated___Register_m3554224343 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MonoBehaviourVisitorGenerated::ilo_getObject1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * MonoBehaviourVisitorGenerated_ilo_getObject1_m3193926325 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
