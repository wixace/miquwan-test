﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ReplayBase779703160.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayMove
struct  ReplayMove_t780044408  : public ReplayBase_t779703160
{
public:
	// System.Single ReplayMove::posX
	float ___posX_8;
	// System.Single ReplayMove::posY
	float ___posY_9;
	// System.Single ReplayMove::posZ
	float ___posZ_10;

public:
	inline static int32_t get_offset_of_posX_8() { return static_cast<int32_t>(offsetof(ReplayMove_t780044408, ___posX_8)); }
	inline float get_posX_8() const { return ___posX_8; }
	inline float* get_address_of_posX_8() { return &___posX_8; }
	inline void set_posX_8(float value)
	{
		___posX_8 = value;
	}

	inline static int32_t get_offset_of_posY_9() { return static_cast<int32_t>(offsetof(ReplayMove_t780044408, ___posY_9)); }
	inline float get_posY_9() const { return ___posY_9; }
	inline float* get_address_of_posY_9() { return &___posY_9; }
	inline void set_posY_9(float value)
	{
		___posY_9 = value;
	}

	inline static int32_t get_offset_of_posZ_10() { return static_cast<int32_t>(offsetof(ReplayMove_t780044408, ___posZ_10)); }
	inline float get_posZ_10() const { return ___posZ_10; }
	inline float* get_address_of_posZ_10() { return &___posZ_10; }
	inline void set_posZ_10(float value)
	{
		___posZ_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
