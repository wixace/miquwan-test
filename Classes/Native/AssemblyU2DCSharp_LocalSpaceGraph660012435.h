﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocalSpaceGraph
struct  LocalSpaceGraph_t660012435  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Matrix4x4 LocalSpaceGraph::originalMatrix
	Matrix4x4_t1651859333  ___originalMatrix_2;

public:
	inline static int32_t get_offset_of_originalMatrix_2() { return static_cast<int32_t>(offsetof(LocalSpaceGraph_t660012435, ___originalMatrix_2)); }
	inline Matrix4x4_t1651859333  get_originalMatrix_2() const { return ___originalMatrix_2; }
	inline Matrix4x4_t1651859333 * get_address_of_originalMatrix_2() { return &___originalMatrix_2; }
	inline void set_originalMatrix_2(Matrix4x4_t1651859333  value)
	{
		___originalMatrix_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
