﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._9a4891a569454e2588e81a8314725edb
struct _9a4891a569454e2588e81a8314725edb_t361952051;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._9a4891a569454e2588e81a8314725edb::.ctor()
extern "C"  void _9a4891a569454e2588e81a8314725edb__ctor_m728964186 (_9a4891a569454e2588e81a8314725edb_t361952051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._9a4891a569454e2588e81a8314725edb::_9a4891a569454e2588e81a8314725edbm2(System.Int32)
extern "C"  int32_t _9a4891a569454e2588e81a8314725edb__9a4891a569454e2588e81a8314725edbm2_m998255801 (_9a4891a569454e2588e81a8314725edb_t361952051 * __this, int32_t ____9a4891a569454e2588e81a8314725edba0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._9a4891a569454e2588e81a8314725edb::_9a4891a569454e2588e81a8314725edbm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _9a4891a569454e2588e81a8314725edb__9a4891a569454e2588e81a8314725edbm_m2832416285 (_9a4891a569454e2588e81a8314725edb_t361952051 * __this, int32_t ____9a4891a569454e2588e81a8314725edba0, int32_t ____9a4891a569454e2588e81a8314725edb911, int32_t ____9a4891a569454e2588e81a8314725edbc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
