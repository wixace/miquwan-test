﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroEmbattleCfg
struct  HeroEmbattleCfg_t4134124650  : public CsCfgBase_t69924517
{
public:
	// System.Int32 HeroEmbattleCfg::id
	int32_t ___id_0;
	// System.Int32 HeroEmbattleCfg::level
	int32_t ___level_1;
	// System.Int32 HeroEmbattleCfg::costGold
	int32_t ___costGold_2;
	// System.Int32 HeroEmbattleCfg::attPlus
	int32_t ___attPlus_3;
	// System.Boolean HeroEmbattleCfg::is_open
	bool ___is_open_4;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(HeroEmbattleCfg_t4134124650, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_level_1() { return static_cast<int32_t>(offsetof(HeroEmbattleCfg_t4134124650, ___level_1)); }
	inline int32_t get_level_1() const { return ___level_1; }
	inline int32_t* get_address_of_level_1() { return &___level_1; }
	inline void set_level_1(int32_t value)
	{
		___level_1 = value;
	}

	inline static int32_t get_offset_of_costGold_2() { return static_cast<int32_t>(offsetof(HeroEmbattleCfg_t4134124650, ___costGold_2)); }
	inline int32_t get_costGold_2() const { return ___costGold_2; }
	inline int32_t* get_address_of_costGold_2() { return &___costGold_2; }
	inline void set_costGold_2(int32_t value)
	{
		___costGold_2 = value;
	}

	inline static int32_t get_offset_of_attPlus_3() { return static_cast<int32_t>(offsetof(HeroEmbattleCfg_t4134124650, ___attPlus_3)); }
	inline int32_t get_attPlus_3() const { return ___attPlus_3; }
	inline int32_t* get_address_of_attPlus_3() { return &___attPlus_3; }
	inline void set_attPlus_3(int32_t value)
	{
		___attPlus_3 = value;
	}

	inline static int32_t get_offset_of_is_open_4() { return static_cast<int32_t>(offsetof(HeroEmbattleCfg_t4134124650, ___is_open_4)); }
	inline bool get_is_open_4() const { return ___is_open_4; }
	inline bool* get_address_of_is_open_4() { return &___is_open_4; }
	inline void set_is_open_4(bool value)
	{
		___is_open_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
