﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pomelo.Protobuf.Encoder
struct Encoder_t3832447410;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Pomelo.Protobuf.Encoder::.ctor()
extern "C"  void Encoder__ctor_m28135582 (Encoder_t3832447410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.Protobuf.Encoder::encodeUInt32(System.String)
extern "C"  ByteU5BU5D_t4260760469* Encoder_encodeUInt32_m2569024055 (Il2CppObject * __this /* static, unused */, String_t* ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.Protobuf.Encoder::encodeUInt32(System.UInt32)
extern "C"  ByteU5BU5D_t4260760469* Encoder_encodeUInt32_m3109553199 (Il2CppObject * __this /* static, unused */, uint32_t ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.Protobuf.Encoder::encodeBool(System.Boolean)
extern "C"  ByteU5BU5D_t4260760469* Encoder_encodeBool_m76161779 (Il2CppObject * __this /* static, unused */, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.Protobuf.Encoder::encodeSInt32(System.String)
extern "C"  ByteU5BU5D_t4260760469* Encoder_encodeSInt32_m2146322229 (Il2CppObject * __this /* static, unused */, String_t* ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.Protobuf.Encoder::encodeSInt32(System.Int32)
extern "C"  ByteU5BU5D_t4260760469* Encoder_encodeSInt32_m193051326 (Il2CppObject * __this /* static, unused */, int32_t ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.Protobuf.Encoder::encodeFloat(System.Single)
extern "C"  ByteU5BU5D_t4260760469* Encoder_encodeFloat_m1304025023 (Il2CppObject * __this /* static, unused */, float ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pomelo.Protobuf.Encoder::byteLength(System.String)
extern "C"  int32_t Encoder_byteLength_m965787260 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.Protobuf.Encoder::ilo_encodeSInt321(System.Int32)
extern "C"  ByteU5BU5D_t4260760469* Encoder_ilo_encodeSInt321_m3316681284 (Il2CppObject * __this /* static, unused */, int32_t ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
