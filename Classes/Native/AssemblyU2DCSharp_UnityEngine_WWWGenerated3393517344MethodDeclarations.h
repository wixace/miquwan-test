﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_WWWGenerated
struct UnityEngine_WWWGenerated_t3393517344;
// JSVCall
struct JSVCall_t3708497963;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_WWWGenerated::.ctor()
extern "C"  void UnityEngine_WWWGenerated__ctor_m1758501755 (UnityEngine_WWWGenerated_t3393517344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::WWW_WWW1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_WWW_WWW1_m1821952751 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::WWW_WWW2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_WWW_WWW2_m3066717232 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::WWW_WWW3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_WWW_WWW3_m16514417 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::WWW_WWW4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_WWW_WWW4_m1261278898 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWGenerated::WWW_responseHeaders(JSVCall)
extern "C"  void UnityEngine_WWWGenerated_WWW_responseHeaders_m3868471345 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWGenerated::WWW_text(JSVCall)
extern "C"  void UnityEngine_WWWGenerated_WWW_text_m4174663657 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWGenerated::WWW_bytes(JSVCall)
extern "C"  void UnityEngine_WWWGenerated_WWW_bytes_m3283971723 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWGenerated::WWW_size(JSVCall)
extern "C"  void UnityEngine_WWWGenerated_WWW_size_m43230101 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWGenerated::WWW_error(JSVCall)
extern "C"  void UnityEngine_WWWGenerated_WWW_error_m2969820558 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWGenerated::WWW_texture(JSVCall)
extern "C"  void UnityEngine_WWWGenerated_WWW_texture_m1927781339 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWGenerated::WWW_textureNonReadable(JSVCall)
extern "C"  void UnityEngine_WWWGenerated_WWW_textureNonReadable_m3172089588 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWGenerated::WWW_audioClip(JSVCall)
extern "C"  void UnityEngine_WWWGenerated_WWW_audioClip_m4098225424 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWGenerated::WWW_isDone(JSVCall)
extern "C"  void UnityEngine_WWWGenerated_WWW_isDone_m3878788138 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWGenerated::WWW_progress(JSVCall)
extern "C"  void UnityEngine_WWWGenerated_WWW_progress_m693533417 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWGenerated::WWW_uploadProgress(JSVCall)
extern "C"  void UnityEngine_WWWGenerated_WWW_uploadProgress_m1539103464 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWGenerated::WWW_bytesDownloaded(JSVCall)
extern "C"  void UnityEngine_WWWGenerated_WWW_bytesDownloaded_m3030244 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWGenerated::WWW_url(JSVCall)
extern "C"  void UnityEngine_WWWGenerated_WWW_url_m3274691079 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWGenerated::WWW_assetBundle(JSVCall)
extern "C"  void UnityEngine_WWWGenerated_WWW_assetBundle_m2952571716 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWGenerated::WWW_threadPriority(JSVCall)
extern "C"  void UnityEngine_WWWGenerated_WWW_threadPriority_m2110688360 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::WWW_Dispose(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_WWW_Dispose_m3792016268 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::WWW_GetAudioClip__Boolean__Boolean__AudioType(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_WWW_GetAudioClip__Boolean__Boolean__AudioType_m2411808589 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::WWW_GetAudioClip__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_WWW_GetAudioClip__Boolean__Boolean_m3502189189 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::WWW_GetAudioClip__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_WWW_GetAudioClip__Boolean_m422621253 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::WWW_GetAudioClipCompressed__Boolean__AudioType(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_WWW_GetAudioClipCompressed__Boolean__AudioType_m1823888110 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::WWW_GetAudioClipCompressed__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_WWW_GetAudioClipCompressed__Boolean_m828184964 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::WWW_GetAudioClipCompressed(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_WWW_GetAudioClipCompressed_m1386026598 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::WWW_InitWWW__String__Byte_Array__String_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_WWW_InitWWW__String__Byte_Array__String_Array_m1139688626 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::WWW_LoadImageIntoTexture__Texture2D(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_WWW_LoadImageIntoTexture__Texture2D_m785190164 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::WWW_EscapeURL__String__Encoding(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_WWW_EscapeURL__String__Encoding_m3043801343 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::WWW_EscapeURL__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_WWW_EscapeURL__String_m1222758828 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::WWW_LoadFromCacheOrDownload__String__Int32__UInt32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_WWW_LoadFromCacheOrDownload__String__Int32__UInt32_m2565660974 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::WWW_LoadFromCacheOrDownload__String__Hash128__UInt32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_WWW_LoadFromCacheOrDownload__String__Hash128__UInt32_m1693267529 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::WWW_LoadFromCacheOrDownload__String__Hash128(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_WWW_LoadFromCacheOrDownload__String__Hash128_m802344688 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::WWW_LoadFromCacheOrDownload__String__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_WWW_LoadFromCacheOrDownload__String__Int32_m3832164053 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::WWW_UnEscapeURL__String__Encoding(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_WWW_UnEscapeURL__String__Encoding_m2230975590 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::WWW_UnEscapeURL__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_WWW_UnEscapeURL__String_m2885802451 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWGenerated::__Register()
extern "C"  void UnityEngine_WWWGenerated___Register_m1339938348 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine_WWWGenerated::<WWW_WWW3>m__2EE()
extern "C"  ByteU5BU5D_t4260760469* UnityEngine_WWWGenerated_U3CWWW_WWW3U3Em__2EE_m540430262 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine_WWWGenerated::<WWW_WWW4>m__2EF()
extern "C"  ByteU5BU5D_t4260760469* UnityEngine_WWWGenerated_U3CWWW_WWW4U3Em__2EF_m343917718 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine_WWWGenerated::<WWW_InitWWW__String__Byte_Array__String_Array>m__2F0()
extern "C"  ByteU5BU5D_t4260760469* UnityEngine_WWWGenerated_U3CWWW_InitWWW__String__Byte_Array__String_ArrayU3Em__2F0_m3315448763 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UnityEngine_WWWGenerated::<WWW_InitWWW__String__Byte_Array__String_Array>m__2F1()
extern "C"  StringU5BU5D_t4054002952* UnityEngine_WWWGenerated_U3CWWW_InitWWW__String__Byte_Array__String_ArrayU3Em__2F1_m2216722181 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_ilo_attachFinalizerObject1_m3961078724 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_WWWGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_WWWGenerated_ilo_getObject2_m461905173 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_WWWGenerated::ilo_getStringS3(System.Int32)
extern "C"  String_t* UnityEngine_WWWGenerated_ilo_getStringS3_m383878523 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_WWWGenerated::ilo_getObject4(System.Int32)
extern "C"  int32_t UnityEngine_WWWGenerated_ilo_getObject4_m2778783546 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWGenerated::ilo_setStringS5(System.Int32,System.String)
extern "C"  void UnityEngine_WWWGenerated_ilo_setStringS5_m1387896520 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWGenerated::ilo_setInt326(System.Int32,System.Int32)
extern "C"  void UnityEngine_WWWGenerated_ilo_setInt326_m2062989462 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWGenerated::ilo_getBooleanS7(System.Int32)
extern "C"  bool UnityEngine_WWWGenerated_ilo_getBooleanS7_m119991415 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_WWWGenerated::ilo_setObject8(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_WWWGenerated_ilo_setObject8_m2520492982 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte UnityEngine_WWWGenerated::ilo_getByte9(System.Int32)
extern "C"  uint8_t UnityEngine_WWWGenerated_ilo_getByte9_m3007192148 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
