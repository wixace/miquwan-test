﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.LightmapData
struct LightmapData_t3709883476;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
struct LightmapData_t3709883476_marshaled_pinvoke;
struct LightmapData_t3709883476_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"

// System.Void UnityEngine.LightmapData::.ctor()
extern "C"  void LightmapData__ctor_m888102173 (LightmapData_t3709883476 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.LightmapData::get_lightmapFar()
extern "C"  Texture2D_t3884108195 * LightmapData_get_lightmapFar_m3679410822 (LightmapData_t3709883476 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LightmapData::set_lightmapFar(UnityEngine.Texture2D)
extern "C"  void LightmapData_set_lightmapFar_m2167735373 (LightmapData_t3709883476 * __this, Texture2D_t3884108195 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.LightmapData::get_lightmapNear()
extern "C"  Texture2D_t3884108195 * LightmapData_get_lightmapNear_m2624877755 (LightmapData_t3709883476 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LightmapData::set_lightmapNear(UnityEngine.Texture2D)
extern "C"  void LightmapData_set_lightmapNear_m3628945962 (LightmapData_t3709883476 * __this, Texture2D_t3884108195 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct LightmapData_t3709883476;
struct LightmapData_t3709883476_marshaled_pinvoke;

extern "C" void LightmapData_t3709883476_marshal_pinvoke(const LightmapData_t3709883476& unmarshaled, LightmapData_t3709883476_marshaled_pinvoke& marshaled);
extern "C" void LightmapData_t3709883476_marshal_pinvoke_back(const LightmapData_t3709883476_marshaled_pinvoke& marshaled, LightmapData_t3709883476& unmarshaled);
extern "C" void LightmapData_t3709883476_marshal_pinvoke_cleanup(LightmapData_t3709883476_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct LightmapData_t3709883476;
struct LightmapData_t3709883476_marshaled_com;

extern "C" void LightmapData_t3709883476_marshal_com(const LightmapData_t3709883476& unmarshaled, LightmapData_t3709883476_marshaled_com& marshaled);
extern "C" void LightmapData_t3709883476_marshal_com_back(const LightmapData_t3709883476_marshaled_com& marshaled, LightmapData_t3709883476& unmarshaled);
extern "C" void LightmapData_t3709883476_marshal_com_cleanup(LightmapData_t3709883476_marshaled_com& marshaled);
