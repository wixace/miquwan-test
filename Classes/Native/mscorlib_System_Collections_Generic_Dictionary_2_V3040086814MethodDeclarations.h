﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va154122109MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int3,Pathfinding.PointNode>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m576054304(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3040086814 *, Dictionary_2_t44513805 *, const MethodInfo*))ValueCollection__ctor_m1336469430_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3628815378(__this, ___item0, method) ((  void (*) (ValueCollection_t3040086814 *, PointNode_t2761813780 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1298040252_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1872894427(__this, method) ((  void (*) (ValueCollection_t3040086814 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1653911685_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1947145240(__this, ___item0, method) ((  bool (*) (ValueCollection_t3040086814 *, PointNode_t2761813780 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2630516398_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1196315325(__this, ___item0, method) ((  bool (*) (ValueCollection_t3040086814 *, PointNode_t2761813780 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m334457555_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2461620251(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3040086814 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1796775365_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3463603487(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3040086814 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m461385545_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2107980782(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3040086814 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1233020568_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m222321099(__this, method) ((  bool (*) (ValueCollection_t3040086814 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m905692257_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2424223787(__this, method) ((  bool (*) (ValueCollection_t3040086814 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3754218433_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m977291869(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3040086814 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1684746611_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int3,Pathfinding.PointNode>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1252302951(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3040086814 *, PointNodeU5BU5D_t1844325917*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2608000637_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int3,Pathfinding.PointNode>::GetEnumerator()
#define ValueCollection_GetEnumerator_m4268522640(__this, method) ((  Enumerator_t2271314509  (*) (ValueCollection_t3040086814 *, const MethodInfo*))ValueCollection_GetEnumerator_m678472742_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int3,Pathfinding.PointNode>::get_Count()
#define ValueCollection_get_Count_m279462757(__this, method) ((  int32_t (*) (ValueCollection_t3040086814 *, const MethodInfo*))ValueCollection_get_Count_m4276545275_gshared)(__this, method)
