﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AudioChorusFilterGenerated
struct UnityEngine_AudioChorusFilterGenerated_t3676403619;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_AudioChorusFilterGenerated::.ctor()
extern "C"  void UnityEngine_AudioChorusFilterGenerated__ctor_m3597169752 (UnityEngine_AudioChorusFilterGenerated_t3676403619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioChorusFilterGenerated::AudioChorusFilter_AudioChorusFilter1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioChorusFilterGenerated_AudioChorusFilter_AudioChorusFilter1_m1514924984 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioChorusFilterGenerated::AudioChorusFilter_dryMix(JSVCall)
extern "C"  void UnityEngine_AudioChorusFilterGenerated_AudioChorusFilter_dryMix_m2779794847 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioChorusFilterGenerated::AudioChorusFilter_wetMix1(JSVCall)
extern "C"  void UnityEngine_AudioChorusFilterGenerated_AudioChorusFilter_wetMix1_m2471811489 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioChorusFilterGenerated::AudioChorusFilter_wetMix2(JSVCall)
extern "C"  void UnityEngine_AudioChorusFilterGenerated_AudioChorusFilter_wetMix2_m2275297984 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioChorusFilterGenerated::AudioChorusFilter_wetMix3(JSVCall)
extern "C"  void UnityEngine_AudioChorusFilterGenerated_AudioChorusFilter_wetMix3_m2078784479 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioChorusFilterGenerated::AudioChorusFilter_delay(JSVCall)
extern "C"  void UnityEngine_AudioChorusFilterGenerated_AudioChorusFilter_delay_m897281145 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioChorusFilterGenerated::AudioChorusFilter_rate(JSVCall)
extern "C"  void UnityEngine_AudioChorusFilterGenerated_AudioChorusFilter_rate_m963013840 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioChorusFilterGenerated::AudioChorusFilter_depth(JSVCall)
extern "C"  void UnityEngine_AudioChorusFilterGenerated_AudioChorusFilter_depth_m677036857 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioChorusFilterGenerated::__Register()
extern "C"  void UnityEngine_AudioChorusFilterGenerated___Register_m3633053551 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_AudioChorusFilterGenerated::ilo_getSingle1(System.Int32)
extern "C"  float UnityEngine_AudioChorusFilterGenerated_ilo_getSingle1_m1109383887 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioChorusFilterGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void UnityEngine_AudioChorusFilterGenerated_ilo_setSingle2_m1440049565 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
