﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BloodMgr
struct BloodMgr_t3705885150;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// Blood
struct Blood_t64280026;
// CombatEntity
struct CombatEntity_t684137495;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Blood64280026.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitCamp642829979.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitType643359572.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"

// System.Void BloodMgr::.ctor()
extern "C"  void BloodMgr__ctor_m259565181 (BloodMgr_t3705885150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BloodMgr::.cctor()
extern "C"  void BloodMgr__cctor_m3269457104 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject BloodMgr::get_parentGO()
extern "C"  GameObject_t3674682005 * BloodMgr_get_parentGO_m3003417559 (BloodMgr_t3705885150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BloodMgr::OnStartLoadScene(CEvent.ZEvent)
extern "C"  void BloodMgr_OnStartLoadScene_m3972464273 (BloodMgr_t3705885150 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Blood BloodMgr::AddBlood(CombatEntity)
extern "C"  Blood_t64280026 * BloodMgr_AddBlood_m2204033346 (BloodMgr_t3705885150 * __this, CombatEntity_t684137495 * ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BloodMgr::RemoveBlood(Blood)
extern "C"  void BloodMgr_RemoveBlood_m2501439559 (BloodMgr_t3705885150 * __this, Blood_t64280026 * ___blood0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitCamp BloodMgr::ilo_get_unitCamp1(CombatEntity)
extern "C"  int32_t BloodMgr_ilo_get_unitCamp1_m4139927952 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitType BloodMgr::ilo_get_unitType2(CombatEntity)
extern "C"  int32_t BloodMgr_ilo_get_unitType2_m3863778161 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject BloodMgr::ilo_Instantiate3(UnityEngine.GameObject)
extern "C"  GameObject_t3674682005 * BloodMgr_ilo_Instantiate3_m2246231568 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BloodMgr::ilo_ChangeParent4(UnityEngine.GameObject,UnityEngine.GameObject,System.Boolean)
extern "C"  void BloodMgr_ilo_ChangeParent4_m363578681 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, GameObject_t3674682005 * ___parent1, bool ___resetMat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BloodMgr::ilo_SetFocus5(Blood,UnityEngine.Transform)
extern "C"  void BloodMgr_ilo_SetFocus5_m708376840 (Il2CppObject * __this /* static, unused */, Blood_t64280026 * ____this0, Transform_t1659122786 * ___tr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BloodMgr::ilo_Clear6(Blood)
extern "C"  void BloodMgr_ilo_Clear6_m544555733 (Il2CppObject * __this /* static, unused */, Blood_t64280026 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
