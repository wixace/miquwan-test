﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_SpringJoint2DGenerated
struct UnityEngine_SpringJoint2DGenerated_t151921704;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_SpringJoint2DGenerated::.ctor()
extern "C"  void UnityEngine_SpringJoint2DGenerated__ctor_m4164344179 (UnityEngine_SpringJoint2DGenerated_t151921704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SpringJoint2DGenerated::SpringJoint2D_SpringJoint2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SpringJoint2DGenerated_SpringJoint2D_SpringJoint2D1_m1037321479 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpringJoint2DGenerated::SpringJoint2D_autoConfigureDistance(JSVCall)
extern "C"  void UnityEngine_SpringJoint2DGenerated_SpringJoint2D_autoConfigureDistance_m3248694554 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpringJoint2DGenerated::SpringJoint2D_distance(JSVCall)
extern "C"  void UnityEngine_SpringJoint2DGenerated_SpringJoint2D_distance_m2415821841 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpringJoint2DGenerated::SpringJoint2D_dampingRatio(JSVCall)
extern "C"  void UnityEngine_SpringJoint2DGenerated_SpringJoint2D_dampingRatio_m1816536669 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpringJoint2DGenerated::SpringJoint2D_frequency(JSVCall)
extern "C"  void UnityEngine_SpringJoint2DGenerated_SpringJoint2D_frequency_m1279036618 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpringJoint2DGenerated::__Register()
extern "C"  void UnityEngine_SpringJoint2DGenerated___Register_m789025588 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpringJoint2DGenerated::ilo_setBooleanS1(System.Int32,System.Boolean)
extern "C"  void UnityEngine_SpringJoint2DGenerated_ilo_setBooleanS1_m2366279076 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SpringJoint2DGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void UnityEngine_SpringJoint2DGenerated_ilo_setSingle2_m623892130 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
