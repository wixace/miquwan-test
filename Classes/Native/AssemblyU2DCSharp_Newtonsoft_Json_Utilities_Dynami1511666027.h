﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory
struct DynamicReflectionDelegateFactory_t1511666027;

#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Reflec1590616920.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory
struct  DynamicReflectionDelegateFactory_t1511666027  : public ReflectionDelegateFactory_t1590616920
{
public:

public:
};

struct DynamicReflectionDelegateFactory_t1511666027_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::Instance
	DynamicReflectionDelegateFactory_t1511666027 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(DynamicReflectionDelegateFactory_t1511666027_StaticFields, ___Instance_0)); }
	inline DynamicReflectionDelegateFactory_t1511666027 * get_Instance_0() const { return ___Instance_0; }
	inline DynamicReflectionDelegateFactory_t1511666027 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(DynamicReflectionDelegateFactory_t1511666027 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
