﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_muyoJayzi189
struct  M_muyoJayzi189_t3307371183  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_muyoJayzi189::_vaygecem
	float ____vaygecem_0;
	// System.UInt32 GarbageiOS.M_muyoJayzi189::_hiqarboCeremare
	uint32_t ____hiqarboCeremare_1;
	// System.Int32 GarbageiOS.M_muyoJayzi189::_sirjaseNagato
	int32_t ____sirjaseNagato_2;
	// System.Single GarbageiOS.M_muyoJayzi189::_talsowpis
	float ____talsowpis_3;
	// System.Int32 GarbageiOS.M_muyoJayzi189::_tubeterToudrookoo
	int32_t ____tubeterToudrookoo_4;
	// System.Int32 GarbageiOS.M_muyoJayzi189::_himir
	int32_t ____himir_5;
	// System.Int32 GarbageiOS.M_muyoJayzi189::_drepetal
	int32_t ____drepetal_6;
	// System.Int32 GarbageiOS.M_muyoJayzi189::_sole
	int32_t ____sole_7;
	// System.Boolean GarbageiOS.M_muyoJayzi189::_torjayFairmea
	bool ____torjayFairmea_8;
	// System.Boolean GarbageiOS.M_muyoJayzi189::_balrerePaycido
	bool ____balrerePaycido_9;
	// System.Boolean GarbageiOS.M_muyoJayzi189::_fearneeLissearca
	bool ____fearneeLissearca_10;
	// System.Single GarbageiOS.M_muyoJayzi189::_jaimay
	float ____jaimay_11;
	// System.UInt32 GarbageiOS.M_muyoJayzi189::_wirjiNipal
	uint32_t ____wirjiNipal_12;
	// System.Int32 GarbageiOS.M_muyoJayzi189::_fererlouMojir
	int32_t ____fererlouMojir_13;
	// System.Boolean GarbageiOS.M_muyoJayzi189::_waskawe
	bool ____waskawe_14;
	// System.UInt32 GarbageiOS.M_muyoJayzi189::_cajameVetemnai
	uint32_t ____cajameVetemnai_15;
	// System.String GarbageiOS.M_muyoJayzi189::_yeakoTohea
	String_t* ____yeakoTohea_16;
	// System.String GarbageiOS.M_muyoJayzi189::_meretalSorfa
	String_t* ____meretalSorfa_17;
	// System.UInt32 GarbageiOS.M_muyoJayzi189::_gaygalliSecas
	uint32_t ____gaygalliSecas_18;
	// System.Int32 GarbageiOS.M_muyoJayzi189::_bene
	int32_t ____bene_19;
	// System.UInt32 GarbageiOS.M_muyoJayzi189::_kalloo
	uint32_t ____kalloo_20;
	// System.Int32 GarbageiOS.M_muyoJayzi189::_tairnopow
	int32_t ____tairnopow_21;

public:
	inline static int32_t get_offset_of__vaygecem_0() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____vaygecem_0)); }
	inline float get__vaygecem_0() const { return ____vaygecem_0; }
	inline float* get_address_of__vaygecem_0() { return &____vaygecem_0; }
	inline void set__vaygecem_0(float value)
	{
		____vaygecem_0 = value;
	}

	inline static int32_t get_offset_of__hiqarboCeremare_1() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____hiqarboCeremare_1)); }
	inline uint32_t get__hiqarboCeremare_1() const { return ____hiqarboCeremare_1; }
	inline uint32_t* get_address_of__hiqarboCeremare_1() { return &____hiqarboCeremare_1; }
	inline void set__hiqarboCeremare_1(uint32_t value)
	{
		____hiqarboCeremare_1 = value;
	}

	inline static int32_t get_offset_of__sirjaseNagato_2() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____sirjaseNagato_2)); }
	inline int32_t get__sirjaseNagato_2() const { return ____sirjaseNagato_2; }
	inline int32_t* get_address_of__sirjaseNagato_2() { return &____sirjaseNagato_2; }
	inline void set__sirjaseNagato_2(int32_t value)
	{
		____sirjaseNagato_2 = value;
	}

	inline static int32_t get_offset_of__talsowpis_3() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____talsowpis_3)); }
	inline float get__talsowpis_3() const { return ____talsowpis_3; }
	inline float* get_address_of__talsowpis_3() { return &____talsowpis_3; }
	inline void set__talsowpis_3(float value)
	{
		____talsowpis_3 = value;
	}

	inline static int32_t get_offset_of__tubeterToudrookoo_4() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____tubeterToudrookoo_4)); }
	inline int32_t get__tubeterToudrookoo_4() const { return ____tubeterToudrookoo_4; }
	inline int32_t* get_address_of__tubeterToudrookoo_4() { return &____tubeterToudrookoo_4; }
	inline void set__tubeterToudrookoo_4(int32_t value)
	{
		____tubeterToudrookoo_4 = value;
	}

	inline static int32_t get_offset_of__himir_5() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____himir_5)); }
	inline int32_t get__himir_5() const { return ____himir_5; }
	inline int32_t* get_address_of__himir_5() { return &____himir_5; }
	inline void set__himir_5(int32_t value)
	{
		____himir_5 = value;
	}

	inline static int32_t get_offset_of__drepetal_6() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____drepetal_6)); }
	inline int32_t get__drepetal_6() const { return ____drepetal_6; }
	inline int32_t* get_address_of__drepetal_6() { return &____drepetal_6; }
	inline void set__drepetal_6(int32_t value)
	{
		____drepetal_6 = value;
	}

	inline static int32_t get_offset_of__sole_7() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____sole_7)); }
	inline int32_t get__sole_7() const { return ____sole_7; }
	inline int32_t* get_address_of__sole_7() { return &____sole_7; }
	inline void set__sole_7(int32_t value)
	{
		____sole_7 = value;
	}

	inline static int32_t get_offset_of__torjayFairmea_8() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____torjayFairmea_8)); }
	inline bool get__torjayFairmea_8() const { return ____torjayFairmea_8; }
	inline bool* get_address_of__torjayFairmea_8() { return &____torjayFairmea_8; }
	inline void set__torjayFairmea_8(bool value)
	{
		____torjayFairmea_8 = value;
	}

	inline static int32_t get_offset_of__balrerePaycido_9() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____balrerePaycido_9)); }
	inline bool get__balrerePaycido_9() const { return ____balrerePaycido_9; }
	inline bool* get_address_of__balrerePaycido_9() { return &____balrerePaycido_9; }
	inline void set__balrerePaycido_9(bool value)
	{
		____balrerePaycido_9 = value;
	}

	inline static int32_t get_offset_of__fearneeLissearca_10() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____fearneeLissearca_10)); }
	inline bool get__fearneeLissearca_10() const { return ____fearneeLissearca_10; }
	inline bool* get_address_of__fearneeLissearca_10() { return &____fearneeLissearca_10; }
	inline void set__fearneeLissearca_10(bool value)
	{
		____fearneeLissearca_10 = value;
	}

	inline static int32_t get_offset_of__jaimay_11() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____jaimay_11)); }
	inline float get__jaimay_11() const { return ____jaimay_11; }
	inline float* get_address_of__jaimay_11() { return &____jaimay_11; }
	inline void set__jaimay_11(float value)
	{
		____jaimay_11 = value;
	}

	inline static int32_t get_offset_of__wirjiNipal_12() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____wirjiNipal_12)); }
	inline uint32_t get__wirjiNipal_12() const { return ____wirjiNipal_12; }
	inline uint32_t* get_address_of__wirjiNipal_12() { return &____wirjiNipal_12; }
	inline void set__wirjiNipal_12(uint32_t value)
	{
		____wirjiNipal_12 = value;
	}

	inline static int32_t get_offset_of__fererlouMojir_13() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____fererlouMojir_13)); }
	inline int32_t get__fererlouMojir_13() const { return ____fererlouMojir_13; }
	inline int32_t* get_address_of__fererlouMojir_13() { return &____fererlouMojir_13; }
	inline void set__fererlouMojir_13(int32_t value)
	{
		____fererlouMojir_13 = value;
	}

	inline static int32_t get_offset_of__waskawe_14() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____waskawe_14)); }
	inline bool get__waskawe_14() const { return ____waskawe_14; }
	inline bool* get_address_of__waskawe_14() { return &____waskawe_14; }
	inline void set__waskawe_14(bool value)
	{
		____waskawe_14 = value;
	}

	inline static int32_t get_offset_of__cajameVetemnai_15() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____cajameVetemnai_15)); }
	inline uint32_t get__cajameVetemnai_15() const { return ____cajameVetemnai_15; }
	inline uint32_t* get_address_of__cajameVetemnai_15() { return &____cajameVetemnai_15; }
	inline void set__cajameVetemnai_15(uint32_t value)
	{
		____cajameVetemnai_15 = value;
	}

	inline static int32_t get_offset_of__yeakoTohea_16() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____yeakoTohea_16)); }
	inline String_t* get__yeakoTohea_16() const { return ____yeakoTohea_16; }
	inline String_t** get_address_of__yeakoTohea_16() { return &____yeakoTohea_16; }
	inline void set__yeakoTohea_16(String_t* value)
	{
		____yeakoTohea_16 = value;
		Il2CppCodeGenWriteBarrier(&____yeakoTohea_16, value);
	}

	inline static int32_t get_offset_of__meretalSorfa_17() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____meretalSorfa_17)); }
	inline String_t* get__meretalSorfa_17() const { return ____meretalSorfa_17; }
	inline String_t** get_address_of__meretalSorfa_17() { return &____meretalSorfa_17; }
	inline void set__meretalSorfa_17(String_t* value)
	{
		____meretalSorfa_17 = value;
		Il2CppCodeGenWriteBarrier(&____meretalSorfa_17, value);
	}

	inline static int32_t get_offset_of__gaygalliSecas_18() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____gaygalliSecas_18)); }
	inline uint32_t get__gaygalliSecas_18() const { return ____gaygalliSecas_18; }
	inline uint32_t* get_address_of__gaygalliSecas_18() { return &____gaygalliSecas_18; }
	inline void set__gaygalliSecas_18(uint32_t value)
	{
		____gaygalliSecas_18 = value;
	}

	inline static int32_t get_offset_of__bene_19() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____bene_19)); }
	inline int32_t get__bene_19() const { return ____bene_19; }
	inline int32_t* get_address_of__bene_19() { return &____bene_19; }
	inline void set__bene_19(int32_t value)
	{
		____bene_19 = value;
	}

	inline static int32_t get_offset_of__kalloo_20() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____kalloo_20)); }
	inline uint32_t get__kalloo_20() const { return ____kalloo_20; }
	inline uint32_t* get_address_of__kalloo_20() { return &____kalloo_20; }
	inline void set__kalloo_20(uint32_t value)
	{
		____kalloo_20 = value;
	}

	inline static int32_t get_offset_of__tairnopow_21() { return static_cast<int32_t>(offsetof(M_muyoJayzi189_t3307371183, ____tairnopow_21)); }
	inline int32_t get__tairnopow_21() const { return ____tairnopow_21; }
	inline int32_t* get_address_of__tairnopow_21() { return &____tairnopow_21; }
	inline void set__tairnopow_21(int32_t value)
	{
		____tairnopow_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
