﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Int32>
struct KeyCollection_t3872077533;
// System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>
struct Dictionary_2_t2245318082;
// System.Collections.Generic.IEnumerator`1<Pathfinding.Int2>
struct IEnumerator_1_t3885910642;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.Int2[]
struct Int2U5BU5D_t30096868;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2860254136.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m2325718534_gshared (KeyCollection_t3872077533 * __this, Dictionary_2_t2245318082 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m2325718534(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3872077533 *, Dictionary_2_t2245318082 *, const MethodInfo*))KeyCollection__ctor_m2325718534_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Int32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m199985040_gshared (KeyCollection_t3872077533 * __this, Int2_t1974045593  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m199985040(__this, ___item0, method) ((  void (*) (KeyCollection_t3872077533 *, Int2_t1974045593 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m199985040_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Int32>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m898519303_gshared (KeyCollection_t3872077533 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m898519303(__this, method) ((  void (*) (KeyCollection_t3872077533 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m898519303_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Int32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m147319290_gshared (KeyCollection_t3872077533 * __this, Int2_t1974045593  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m147319290(__this, ___item0, method) ((  bool (*) (KeyCollection_t3872077533 *, Int2_t1974045593 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m147319290_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Int32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1176146399_gshared (KeyCollection_t3872077533 * __this, Int2_t1974045593  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1176146399(__this, ___item0, method) ((  bool (*) (KeyCollection_t3872077533 *, Int2_t1974045593 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1176146399_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Int32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3015116419_gshared (KeyCollection_t3872077533 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3015116419(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3872077533 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3015116419_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m194241273_gshared (KeyCollection_t3872077533 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m194241273(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3872077533 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m194241273_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1085808948_gshared (KeyCollection_t3872077533 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1085808948(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3872077533 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1085808948_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Int32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2720975579_gshared (KeyCollection_t3872077533 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2720975579(__this, method) ((  bool (*) (KeyCollection_t3872077533 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2720975579_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1928108365_gshared (KeyCollection_t3872077533 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1928108365(__this, method) ((  bool (*) (KeyCollection_t3872077533 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1928108365_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3895622265_gshared (KeyCollection_t3872077533 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3895622265(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3872077533 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3895622265_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Int32>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m569812155_gshared (KeyCollection_t3872077533 * __this, Int2U5BU5D_t30096868* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m569812155(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3872077533 *, Int2U5BU5D_t30096868*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m569812155_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t2860254136  KeyCollection_GetEnumerator_m1092434014_gshared (KeyCollection_t3872077533 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1092434014(__this, method) ((  Enumerator_t2860254136  (*) (KeyCollection_t3872077533 *, const MethodInfo*))KeyCollection_GetEnumerator_m1092434014_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,System.Int32>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3223969427_gshared (KeyCollection_t3872077533 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m3223969427(__this, method) ((  int32_t (*) (KeyCollection_t3872077533 *, const MethodInfo*))KeyCollection_get_Count_m3223969427_gshared)(__this, method)
