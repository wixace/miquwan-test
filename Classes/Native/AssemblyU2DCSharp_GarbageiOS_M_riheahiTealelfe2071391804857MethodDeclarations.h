﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_riheahiTealelfe207
struct M_riheahiTealelfe207_t1391804857;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_riheahiTealelfe2071391804857.h"

// System.Void GarbageiOS.M_riheahiTealelfe207::.ctor()
extern "C"  void M_riheahiTealelfe207__ctor_m3940319898 (M_riheahiTealelfe207_t1391804857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_riheahiTealelfe207::M_talfotir0(System.String[],System.Int32)
extern "C"  void M_riheahiTealelfe207_M_talfotir0_m2361545336 (M_riheahiTealelfe207_t1391804857 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_riheahiTealelfe207::M_fearqisKelcere1(System.String[],System.Int32)
extern "C"  void M_riheahiTealelfe207_M_fearqisKelcere1_m3821658706 (M_riheahiTealelfe207_t1391804857 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_riheahiTealelfe207::M_wosall2(System.String[],System.Int32)
extern "C"  void M_riheahiTealelfe207_M_wosall2_m2754362857 (M_riheahiTealelfe207_t1391804857 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_riheahiTealelfe207::M_purperzo3(System.String[],System.Int32)
extern "C"  void M_riheahiTealelfe207_M_purperzo3_m2254432779 (M_riheahiTealelfe207_t1391804857 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_riheahiTealelfe207::M_berurrurPouzear4(System.String[],System.Int32)
extern "C"  void M_riheahiTealelfe207_M_berurrurPouzear4_m273643128 (M_riheahiTealelfe207_t1391804857 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_riheahiTealelfe207::M_challkeDrearlemtor5(System.String[],System.Int32)
extern "C"  void M_riheahiTealelfe207_M_challkeDrearlemtor5_m1880601341 (M_riheahiTealelfe207_t1391804857 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_riheahiTealelfe207::M_ciryeRimay6(System.String[],System.Int32)
extern "C"  void M_riheahiTealelfe207_M_ciryeRimay6_m2870529981 (M_riheahiTealelfe207_t1391804857 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_riheahiTealelfe207::ilo_M_talfotir01(GarbageiOS.M_riheahiTealelfe207,System.String[],System.Int32)
extern "C"  void M_riheahiTealelfe207_ilo_M_talfotir01_m1628224737 (Il2CppObject * __this /* static, unused */, M_riheahiTealelfe207_t1391804857 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
