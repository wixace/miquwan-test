﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.GraphUpdateShape
struct GraphUpdateShape_t2348620960;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphUpdateShape2348620960.h"

// System.Void Pathfinding.GraphUpdateShape::.ctor()
extern "C"  void GraphUpdateShape__ctor_m803885511 (GraphUpdateShape_t2348620960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] Pathfinding.GraphUpdateShape::get_points()
extern "C"  Vector3U5BU5D_t215400611* GraphUpdateShape_get_points_m1312440385 (GraphUpdateShape_t2348620960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateShape::set_points(UnityEngine.Vector3[])
extern "C"  void GraphUpdateShape_set_points_m2441215994 (GraphUpdateShape_t2348620960 * __this, Vector3U5BU5D_t215400611* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GraphUpdateShape::get_convex()
extern "C"  bool GraphUpdateShape_get_convex_m3523542817 (GraphUpdateShape_t2348620960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateShape::set_convex(System.Boolean)
extern "C"  void GraphUpdateShape_set_convex_m1720961432 (GraphUpdateShape_t2348620960 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateShape::CalculateConvexHull()
extern "C"  void GraphUpdateShape_CalculateConvexHull_m195564607 (GraphUpdateShape_t2348620960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds Pathfinding.GraphUpdateShape::GetBounds()
extern "C"  Bounds_t2711641849  GraphUpdateShape_GetBounds_m1384958367 (GraphUpdateShape_t2348620960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GraphUpdateShape::Contains(Pathfinding.GraphNode)
extern "C"  bool GraphUpdateShape_Contains_m2957299516 (GraphUpdateShape_t2348620960 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GraphUpdateShape::Contains(UnityEngine.Vector3)
extern "C"  bool GraphUpdateShape_Contains_m962495529 (GraphUpdateShape_t2348620960 * __this, Vector3_t4282066566  ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateShape::ilo_CalculateConvexHull1(Pathfinding.GraphUpdateShape)
extern "C"  void GraphUpdateShape_ilo_CalculateConvexHull1_m2621255571 (Il2CppObject * __this /* static, unused */, GraphUpdateShape_t2348620960 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] Pathfinding.GraphUpdateShape::ilo_get_points2(Pathfinding.GraphUpdateShape)
extern "C"  Vector3U5BU5D_t215400611* GraphUpdateShape_ilo_get_points2_m1476654796 (Il2CppObject * __this /* static, unused */, GraphUpdateShape_t2348620960 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GraphUpdateShape::ilo_get_convex3(Pathfinding.GraphUpdateShape)
extern "C"  bool GraphUpdateShape_ilo_get_convex3_m1558215213 (Il2CppObject * __this /* static, unused */, GraphUpdateShape_t2348620960 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GraphUpdateShape::ilo_ContainsPoint4(UnityEngine.Vector3[],UnityEngine.Vector3)
extern "C"  bool GraphUpdateShape_ilo_ContainsPoint4_m130273859 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t215400611* ___polyPoints0, Vector3_t4282066566  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GraphUpdateShape::ilo_Left5(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool GraphUpdateShape_ilo_Left5_m3507561043 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, Vector3_t4282066566  ___p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
