﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pomelo.Protobuf.MsgDecoder
struct MsgDecoder_t242483159;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// Pomelo.Protobuf.Util
struct Util_t3483766166;
// System.String
struct String_t;
// Newtonsoft.Json.Linq.JArray
struct JArray_t3394795039;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_Pomelo_Protobuf_Util3483766166.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JArray3394795039.h"
#include "AssemblyU2DCSharp_Pomelo_Protobuf_MsgDecoder242483159.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"

// System.Void Pomelo.Protobuf.MsgDecoder::.ctor(Newtonsoft.Json.Linq.JObject)
extern "C"  void MsgDecoder__ctor_m3582067961 (MsgDecoder_t242483159 * __this, JObject_t1798544199 * ___protos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.MsgDecoder::set_protos(Newtonsoft.Json.Linq.JObject)
extern "C"  void MsgDecoder_set_protos_m1736933363 (MsgDecoder_t242483159 * __this, JObject_t1798544199 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject Pomelo.Protobuf.MsgDecoder::get_protos()
extern "C"  JObject_t1798544199 * MsgDecoder_get_protos_m2682425788 (MsgDecoder_t242483159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.MsgDecoder::set_offset(System.Int32)
extern "C"  void MsgDecoder_set_offset_m4077340 (MsgDecoder_t242483159 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pomelo.Protobuf.MsgDecoder::get_offset()
extern "C"  int32_t MsgDecoder_get_offset_m2827769317 (MsgDecoder_t242483159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.MsgDecoder::set_buffer(System.Byte[])
extern "C"  void MsgDecoder_set_buffer_m3628374033 (MsgDecoder_t242483159 * __this, ByteU5BU5D_t4260760469* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.Protobuf.MsgDecoder::get_buffer()
extern "C"  ByteU5BU5D_t4260760469* MsgDecoder_get_buffer_m3814406958 (MsgDecoder_t242483159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.MsgDecoder::set_util(Pomelo.Protobuf.Util)
extern "C"  void MsgDecoder_set_util_m3415034377 (MsgDecoder_t242483159 * __this, Util_t3483766166 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pomelo.Protobuf.Util Pomelo.Protobuf.MsgDecoder::get_util()
extern "C"  Util_t3483766166 * MsgDecoder_get_util_m452924242 (MsgDecoder_t242483159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject Pomelo.Protobuf.MsgDecoder::decode(System.String,System.Byte[])
extern "C"  JObject_t1798544199 * MsgDecoder_decode_m4170650287 (MsgDecoder_t242483159 * __this, String_t* ___route0, ByteU5BU5D_t4260760469* ___buf1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject Pomelo.Protobuf.MsgDecoder::decodeMsg(Newtonsoft.Json.Linq.JObject,Newtonsoft.Json.Linq.JObject,System.Int32)
extern "C"  JObject_t1798544199 * MsgDecoder_decodeMsg_m3242415166 (MsgDecoder_t242483159 * __this, JObject_t1798544199 * ___msg0, JObject_t1798544199 * ___proto1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.MsgDecoder::decodeArray(Newtonsoft.Json.Linq.JArray,System.String,Newtonsoft.Json.Linq.JObject)
extern "C"  void MsgDecoder_decodeArray_m2676725442 (MsgDecoder_t242483159 * __this, JArray_t3394795039 * ___list0, String_t* ___type1, JObject_t1798544199 * ___proto2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Pomelo.Protobuf.MsgDecoder::decodeProp(System.String,Newtonsoft.Json.Linq.JObject)
extern "C"  JToken_t3412245951 * MsgDecoder_decodeProp_m3269330539 (MsgDecoder_t242483159 * __this, String_t* ___type0, JObject_t1798544199 * ___proto1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject Pomelo.Protobuf.MsgDecoder::decodeObject(System.String,Newtonsoft.Json.Linq.JObject)
extern "C"  JObject_t1798544199 * MsgDecoder_decodeObject_m1721103529 (MsgDecoder_t242483159 * __this, String_t* ___type0, JObject_t1798544199 * ___proto1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pomelo.Protobuf.MsgDecoder::decodeBool()
extern "C"  bool MsgDecoder_decodeBool_m3950440935 (MsgDecoder_t242483159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pomelo.Protobuf.MsgDecoder::decodeString()
extern "C"  String_t* MsgDecoder_decodeString_m1906264829 (MsgDecoder_t242483159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Pomelo.Protobuf.MsgDecoder::decodeDouble()
extern "C"  double MsgDecoder_decodeDouble_m3501425405 (MsgDecoder_t242483159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pomelo.Protobuf.MsgDecoder::decodeFloat()
extern "C"  float MsgDecoder_decodeFloat_m2908837961 (MsgDecoder_t242483159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Pomelo.Protobuf.MsgDecoder::ReadRawLittleEndian64()
extern "C"  uint64_t MsgDecoder_ReadRawLittleEndian64_m3139782924 (MsgDecoder_t242483159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Pomelo.Protobuf.MsgDecoder::getHead()
extern "C"  Dictionary_2_t1974256870 * MsgDecoder_getHead_m2509214456 (MsgDecoder_t242483159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.Protobuf.MsgDecoder::getBytes()
extern "C"  ByteU5BU5D_t4260760469* MsgDecoder_getBytes_m1614363290 (MsgDecoder_t242483159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject Pomelo.Protobuf.MsgDecoder::ilo_get_protos1(Pomelo.Protobuf.MsgDecoder)
extern "C"  JObject_t1798544199 * MsgDecoder_ilo_get_protos1_m1818054322 (Il2CppObject * __this /* static, unused */, MsgDecoder_t242483159 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pomelo.Protobuf.MsgDecoder::ilo_TryGetValue2(Newtonsoft.Json.Linq.JObject,System.String,Newtonsoft.Json.Linq.JToken&)
extern "C"  bool MsgDecoder_ilo_TryGetValue2_m487722718 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, JToken_t3412245951 ** ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pomelo.Protobuf.MsgDecoder::ilo_ToString3(Newtonsoft.Json.Linq.JToken)
extern "C"  String_t* MsgDecoder_ilo_ToString3_m1136872274 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Pomelo.Protobuf.MsgDecoder::ilo_decodeProp4(Pomelo.Protobuf.MsgDecoder,System.String,Newtonsoft.Json.Linq.JObject)
extern "C"  JToken_t3412245951 * MsgDecoder_ilo_decodeProp4_m1905965024 (Il2CppObject * __this /* static, unused */, MsgDecoder_t242483159 * ____this0, String_t* ___type1, JObject_t1798544199 * ___proto2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.Protobuf.MsgDecoder::ilo_Add5(Newtonsoft.Json.Linq.JObject,System.String,Newtonsoft.Json.Linq.JToken)
extern "C"  void MsgDecoder_ilo_Add5_m3275106898 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, JToken_t3412245951 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pomelo.Protobuf.MsgDecoder::ilo_get_offset6(Pomelo.Protobuf.MsgDecoder)
extern "C"  int32_t MsgDecoder_ilo_get_offset6_m1503361006 (Il2CppObject * __this /* static, unused */, MsgDecoder_t242483159 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pomelo.Protobuf.MsgDecoder::ilo_decodeUInt327(System.Byte[])
extern "C"  uint32_t MsgDecoder_ilo_decodeUInt327_m3803342464 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.Protobuf.MsgDecoder::ilo_getBytes8(Pomelo.Protobuf.MsgDecoder)
extern "C"  ByteU5BU5D_t4260760469* MsgDecoder_ilo_getBytes8_m2522339419 (Il2CppObject * __this /* static, unused */, MsgDecoder_t242483159 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pomelo.Protobuf.MsgDecoder::ilo_decodeString9(Pomelo.Protobuf.MsgDecoder)
extern "C"  String_t* MsgDecoder_ilo_decodeString9_m203512985 (Il2CppObject * __this /* static, unused */, MsgDecoder_t242483159 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pomelo.Protobuf.MsgDecoder::ilo_decodeBool10(Pomelo.Protobuf.MsgDecoder)
extern "C"  bool MsgDecoder_ilo_decodeBool10_m3540415335 (Il2CppObject * __this /* static, unused */, MsgDecoder_t242483159 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pomelo.Protobuf.MsgDecoder::ilo_decodeSInt3211(System.Byte[])
extern "C"  int32_t MsgDecoder_ilo_decodeSInt3211_m4037096804 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.Protobuf.MsgDecoder::ilo_get_buffer12(Pomelo.Protobuf.MsgDecoder)
extern "C"  ByteU5BU5D_t4260760469* MsgDecoder_ilo_get_buffer12_m1530124848 (Il2CppObject * __this /* static, unused */, MsgDecoder_t242483159 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Pomelo.Protobuf.MsgDecoder::ilo_ReadRawLittleEndian6413(Pomelo.Protobuf.MsgDecoder)
extern "C"  uint64_t MsgDecoder_ilo_ReadRawLittleEndian6413_m67081289 (Il2CppObject * __this /* static, unused */, MsgDecoder_t242483159 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
