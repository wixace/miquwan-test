﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Hero
struct Hero_t2245658;

#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.FollowHeroBvr
struct  FollowHeroBvr_t249631345  : public IBehavior_t770859129
{
public:
	// Hero Entity.Behavior.FollowHeroBvr::hero
	Hero_t2245658 * ___hero_2;
	// System.Single Entity.Behavior.FollowHeroBvr::curTime
	float ___curTime_3;
	// System.Single Entity.Behavior.FollowHeroBvr::intervalTime
	float ___intervalTime_4;

public:
	inline static int32_t get_offset_of_hero_2() { return static_cast<int32_t>(offsetof(FollowHeroBvr_t249631345, ___hero_2)); }
	inline Hero_t2245658 * get_hero_2() const { return ___hero_2; }
	inline Hero_t2245658 ** get_address_of_hero_2() { return &___hero_2; }
	inline void set_hero_2(Hero_t2245658 * value)
	{
		___hero_2 = value;
		Il2CppCodeGenWriteBarrier(&___hero_2, value);
	}

	inline static int32_t get_offset_of_curTime_3() { return static_cast<int32_t>(offsetof(FollowHeroBvr_t249631345, ___curTime_3)); }
	inline float get_curTime_3() const { return ___curTime_3; }
	inline float* get_address_of_curTime_3() { return &___curTime_3; }
	inline void set_curTime_3(float value)
	{
		___curTime_3 = value;
	}

	inline static int32_t get_offset_of_intervalTime_4() { return static_cast<int32_t>(offsetof(FollowHeroBvr_t249631345, ___intervalTime_4)); }
	inline float get_intervalTime_4() const { return ___intervalTime_4; }
	inline float* get_address_of_intervalTime_4() { return &___intervalTime_4; }
	inline void set_intervalTime_4(float value)
	{
		___intervalTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
