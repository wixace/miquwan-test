﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginBugly
struct PluginBugly_t2538573678;
// System.String
struct String_t;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"

// System.Void PluginBugly::.ctor()
extern "C"  void PluginBugly__ctor_m2189125565 (PluginBugly_t2538573678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginBugly::get_AppId_Bugly_Android()
extern "C"  String_t* PluginBugly_get_AppId_Bugly_Android_m495601059 (PluginBugly_t2538573678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginBugly::get_AppId_Bugly_IOS()
extern "C"  String_t* PluginBugly_get_AppId_Bugly_IOS_m2392040769 (PluginBugly_t2538573678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginBugly::Init()
extern "C"  void PluginBugly_Init_m437795575 (PluginBugly_t2538573678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginBugly::OnDestory()
extern "C"  void PluginBugly_OnDestory_m310389968 (PluginBugly_t2538573678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginBugly::RoleEnterGame(CEvent.ZEvent)
extern "C"  void PluginBugly_RoleEnterGame_m3727299564 (PluginBugly_t2538573678 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginBugly::BuglyInit()
extern "C"  void PluginBugly_BuglyInit_m1096505164 (PluginBugly_t2538573678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginBugly::SetUserTab(System.String,System.String)
extern "C"  void PluginBugly_SetUserTab_m4003024367 (PluginBugly_t2538573678 * __this, String_t* ___userId0, String_t* ___userName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginBugly::ilo_RemoveEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginBugly_ilo_RemoveEventListener1_m625104386 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
