﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct List_1_t2448783290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2468456060.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeB1080597738.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Rendering.ReflectionProbeBlendInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2751890790_gshared (Enumerator_t2468456060 * __this, List_1_t2448783290 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m2751890790(__this, ___l0, method) ((  void (*) (Enumerator_t2468456060 *, List_1_t2448783290 *, const MethodInfo*))Enumerator__ctor_m2751890790_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3721938732_gshared (Enumerator_t2468456060 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3721938732(__this, method) ((  void (*) (Enumerator_t2468456060 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3721938732_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m203056738_gshared (Enumerator_t2468456060 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m203056738(__this, method) ((  Il2CppObject * (*) (Enumerator_t2468456060 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m203056738_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m3321253067_gshared (Enumerator_t2468456060 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3321253067(__this, method) ((  void (*) (Enumerator_t2468456060 *, const MethodInfo*))Enumerator_Dispose_m3321253067_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Rendering.ReflectionProbeBlendInfo>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2119216772_gshared (Enumerator_t2468456060 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2119216772(__this, method) ((  void (*) (Enumerator_t2468456060 *, const MethodInfo*))Enumerator_VerifyState_m2119216772_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Rendering.ReflectionProbeBlendInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2221345628_gshared (Enumerator_t2468456060 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2221345628(__this, method) ((  bool (*) (Enumerator_t2468456060 *, const MethodInfo*))Enumerator_MoveNext_m2221345628_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Rendering.ReflectionProbeBlendInfo>::get_Current()
extern "C"  ReflectionProbeBlendInfo_t1080597738  Enumerator_get_Current_m2084377181_gshared (Enumerator_t2468456060 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2084377181(__this, method) ((  ReflectionProbeBlendInfo_t1080597738  (*) (Enumerator_t2468456060 *, const MethodInfo*))Enumerator_get_Current_m2084377181_gshared)(__this, method)
