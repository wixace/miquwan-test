﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.SplatPrototype
struct SplatPrototype_t2969367140;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
struct SplatPrototype_t2969367140_marshaled_pinvoke;
struct SplatPrototype_t2969367140_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Void UnityEngine.SplatPrototype::.ctor()
extern "C"  void SplatPrototype__ctor_m2881709261 (SplatPrototype_t2969367140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.SplatPrototype::get_texture()
extern "C"  Texture2D_t3884108195 * SplatPrototype_get_texture_m1776357728 (SplatPrototype_t2969367140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SplatPrototype::set_texture(UnityEngine.Texture2D)
extern "C"  void SplatPrototype_set_texture_m2668976243 (SplatPrototype_t2969367140 * __this, Texture2D_t3884108195 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.SplatPrototype::get_normalMap()
extern "C"  Texture2D_t3884108195 * SplatPrototype_get_normalMap_m87076794 (SplatPrototype_t2969367140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SplatPrototype::set_normalMap(UnityEngine.Texture2D)
extern "C"  void SplatPrototype_set_normalMap_m2603353241 (SplatPrototype_t2969367140 * __this, Texture2D_t3884108195 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.SplatPrototype::get_tileSize()
extern "C"  Vector2_t4282066565  SplatPrototype_get_tileSize_m3595207146 (SplatPrototype_t2969367140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SplatPrototype::set_tileSize(UnityEngine.Vector2)
extern "C"  void SplatPrototype_set_tileSize_m3937874455 (SplatPrototype_t2969367140 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.SplatPrototype::get_tileOffset()
extern "C"  Vector2_t4282066565  SplatPrototype_get_tileOffset_m238780412 (SplatPrototype_t2969367140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SplatPrototype::set_tileOffset(UnityEngine.Vector2)
extern "C"  void SplatPrototype_set_tileOffset_m3289415941 (SplatPrototype_t2969367140 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.SplatPrototype::get_specular()
extern "C"  Color_t4194546905  SplatPrototype_get_specular_m118680074 (SplatPrototype_t2969367140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SplatPrototype::set_specular(UnityEngine.Color)
extern "C"  void SplatPrototype_set_specular_m3509579727 (SplatPrototype_t2969367140 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.SplatPrototype::get_metallic()
extern "C"  float SplatPrototype_get_metallic_m1381532587 (SplatPrototype_t2969367140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SplatPrototype::set_metallic(System.Single)
extern "C"  void SplatPrototype_set_metallic_m3318955384 (SplatPrototype_t2969367140 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.SplatPrototype::get_smoothness()
extern "C"  float SplatPrototype_get_smoothness_m3189550673 (SplatPrototype_t2969367140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SplatPrototype::set_smoothness(System.Single)
extern "C"  void SplatPrototype_set_smoothness_m749603474 (SplatPrototype_t2969367140 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct SplatPrototype_t2969367140;
struct SplatPrototype_t2969367140_marshaled_pinvoke;

extern "C" void SplatPrototype_t2969367140_marshal_pinvoke(const SplatPrototype_t2969367140& unmarshaled, SplatPrototype_t2969367140_marshaled_pinvoke& marshaled);
extern "C" void SplatPrototype_t2969367140_marshal_pinvoke_back(const SplatPrototype_t2969367140_marshaled_pinvoke& marshaled, SplatPrototype_t2969367140& unmarshaled);
extern "C" void SplatPrototype_t2969367140_marshal_pinvoke_cleanup(SplatPrototype_t2969367140_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct SplatPrototype_t2969367140;
struct SplatPrototype_t2969367140_marshaled_com;

extern "C" void SplatPrototype_t2969367140_marshal_com(const SplatPrototype_t2969367140& unmarshaled, SplatPrototype_t2969367140_marshaled_com& marshaled);
extern "C" void SplatPrototype_t2969367140_marshal_com_back(const SplatPrototype_t2969367140_marshaled_com& marshaled, SplatPrototype_t2969367140& unmarshaled);
extern "C" void SplatPrototype_t2969367140_marshal_com_cleanup(SplatPrototype_t2969367140_marshaled_com& marshaled);
