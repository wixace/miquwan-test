﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2868685323MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UICamera/MouseOrTouch>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1221768742(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t755245285 *, Dictionary_2_t2054639572 *, const MethodInfo*))ValueCollection__ctor_m30082295_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UICamera/MouseOrTouch>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3649644876(__this, ___item0, method) ((  void (*) (ValueCollection_t755245285 *, MouseOrTouch_t2057376333 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m701709403_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UICamera/MouseOrTouch>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3695156245(__this, method) ((  void (*) (ValueCollection_t755245285 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3824389796_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UICamera/MouseOrTouch>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2651800606(__this, ___item0, method) ((  bool (*) (ValueCollection_t755245285 *, MouseOrTouch_t2057376333 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m91415663_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UICamera/MouseOrTouch>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m495373379(__this, ___item0, method) ((  bool (*) (ValueCollection_t755245285 *, MouseOrTouch_t2057376333 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4014492884_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UICamera/MouseOrTouch>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3609025749(__this, method) ((  Il2CppObject* (*) (ValueCollection_t755245285 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4048472420_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UICamera/MouseOrTouch>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3110463193(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t755245285 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1511207592_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UICamera/MouseOrTouch>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m939293992(__this, method) ((  Il2CppObject * (*) (ValueCollection_t755245285 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3055859895_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UICamera/MouseOrTouch>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m926976465(__this, method) ((  bool (*) (ValueCollection_t755245285 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2661558818_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UICamera/MouseOrTouch>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3648143665(__this, method) ((  bool (*) (ValueCollection_t755245285 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3650032386_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UICamera/MouseOrTouch>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m82026851(__this, method) ((  Il2CppObject * (*) (ValueCollection_t755245285 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m179750644_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UICamera/MouseOrTouch>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1984471277(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t755245285 *, MouseOrTouchU5BU5D_t546398688*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1295975294_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UICamera/MouseOrTouch>::GetEnumerator()
#define ValueCollection_GetEnumerator_m4023611670(__this, method) ((  Enumerator_t4281440276  (*) (ValueCollection_t755245285 *, const MethodInfo*))ValueCollection_GetEnumerator_m848222311_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UICamera/MouseOrTouch>::get_Count()
#define ValueCollection_get_Count_m3030181739(__this, method) ((  int32_t (*) (ValueCollection_t755245285 *, const MethodInfo*))ValueCollection_get_Count_m2227591228_gshared)(__this, method)
