﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21352297102.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1445654159_gshared (KeyValuePair_2_t1352297102 * __this, Int3_t1974045594  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1445654159(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1352297102 *, Int3_t1974045594 , Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m1445654159_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>::get_Key()
extern "C"  Int3_t1974045594  KeyValuePair_2_get_Key_m680403065_gshared (KeyValuePair_2_t1352297102 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m680403065(__this, method) ((  Int3_t1974045594  (*) (KeyValuePair_2_t1352297102 *, const MethodInfo*))KeyValuePair_2_get_Key_m680403065_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m444550714_gshared (KeyValuePair_2_t1352297102 * __this, Int3_t1974045594  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m444550714(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1352297102 *, Int3_t1974045594 , const MethodInfo*))KeyValuePair_2_set_Key_m444550714_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m861079929_gshared (KeyValuePair_2_t1352297102 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m861079929(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1352297102 *, const MethodInfo*))KeyValuePair_2_get_Value_m861079929_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m95745338_gshared (KeyValuePair_2_t1352297102 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m95745338(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1352297102 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m95745338_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3407486440_gshared (KeyValuePair_2_t1352297102 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m3407486440(__this, method) ((  String_t* (*) (KeyValuePair_2_t1352297102 *, const MethodInfo*))KeyValuePair_2_ToString_m3407486440_gshared)(__this, method)
