﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Poly2Tri.Polygon
struct Polygon_t1047479568;
// System.Collections.Generic.IList`1<Pathfinding.Poly2Tri.PolygonPoint>
struct IList_1_t622507661;
// System.Collections.Generic.IEnumerable`1<Pathfinding.Poly2Tri.PolygonPoint>
struct IEnumerable_1_t1228773415;
// System.Collections.Generic.IList`1<Pathfinding.Poly2Tri.TriangulationPoint>
struct IList_1_t2209762840;
// System.Collections.Generic.IList`1<Pathfinding.Poly2Tri.DelaunayTriangle>
struct IList_1_t1234783494;
// System.Collections.Generic.IList`1<Pathfinding.Poly2Tri.Polygon>
struct IList_1_t3742126771;
// Pathfinding.Poly2Tri.DelaunayTriangle
struct DelaunayTriangle_t2835103587;
// System.Collections.Generic.IEnumerable`1<Pathfinding.Poly2Tri.DelaunayTriangle>
struct IEnumerable_1_t1841049248;
// Pathfinding.Poly2Tri.TriangulationContext
struct TriangulationContext_t3528662164;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul1170581608.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Polygon1047479568.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Delaunay2835103587.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3528662164.h"

// System.Void Pathfinding.Poly2Tri.Polygon::.ctor(System.Collections.Generic.IList`1<Pathfinding.Poly2Tri.PolygonPoint>)
extern "C"  void Polygon__ctor_m3492825397 (Polygon_t1047479568 * __this, Il2CppObject* ___points0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.TriangulationMode Pathfinding.Poly2Tri.Polygon::get_TriangulationMode()
extern "C"  int32_t Polygon_get_TriangulationMode_m2314587740 (Polygon_t1047479568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.Polygon::AddHole(Pathfinding.Poly2Tri.Polygon)
extern "C"  void Polygon_AddHole_m3361488645 (Polygon_t1047479568 * __this, Polygon_t1047479568 * ___poly0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.Polygon::AddPoints(System.Collections.Generic.IEnumerable`1<Pathfinding.Poly2Tri.PolygonPoint>)
extern "C"  void Polygon_AddPoints_m1159662621 (Polygon_t1047479568 * __this, Il2CppObject* ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Pathfinding.Poly2Tri.TriangulationPoint> Pathfinding.Poly2Tri.Polygon::get_Points()
extern "C"  Il2CppObject* Polygon_get_Points_m1391827089 (Polygon_t1047479568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Pathfinding.Poly2Tri.DelaunayTriangle> Pathfinding.Poly2Tri.Polygon::get_Triangles()
extern "C"  Il2CppObject* Polygon_get_Triangles_m1867712689 (Polygon_t1047479568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Pathfinding.Poly2Tri.Polygon> Pathfinding.Poly2Tri.Polygon::get_Holes()
extern "C"  Il2CppObject* Polygon_get_Holes_m4062490074 (Polygon_t1047479568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.Polygon::AddTriangle(Pathfinding.Poly2Tri.DelaunayTriangle)
extern "C"  void Polygon_AddTriangle_m2118021308 (Polygon_t1047479568 * __this, DelaunayTriangle_t2835103587 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.Polygon::AddTriangles(System.Collections.Generic.IEnumerable`1<Pathfinding.Poly2Tri.DelaunayTriangle>)
extern "C"  void Polygon_AddTriangles_m1676954668 (Polygon_t1047479568 * __this, Il2CppObject* ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.Polygon::ClearTriangles()
extern "C"  void Polygon_ClearTriangles_m397875649 (Polygon_t1047479568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.Polygon::Prepare(Pathfinding.Poly2Tri.TriangulationContext)
extern "C"  void Polygon_Prepare_m463720493 (Polygon_t1047479568 * __this, TriangulationContext_t3528662164 * ___tcx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
