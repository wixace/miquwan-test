﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>
struct ReadOnlyCollection_1_t3395909286;
// System.Collections.Generic.IList`1<PointCloudRegognizer/Point>
struct IList_1_t238511657;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// PointCloudRegognizer/Point[]
struct PointU5BU5D_t2313848483;
// System.Collections.Generic.IEnumerator`1<PointCloudRegognizer/Point>
struct IEnumerator_1_t3750696799;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PointCloudRegognizer_Point1838831750.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m926369450_gshared (ReadOnlyCollection_1_t3395909286 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m926369450(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3395909286 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m926369450_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2640581524_gshared (ReadOnlyCollection_1_t3395909286 * __this, Point_t1838831750  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2640581524(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3395909286 *, Point_t1838831750 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2640581524_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2260339638_gshared (ReadOnlyCollection_1_t3395909286 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2260339638(__this, method) ((  void (*) (ReadOnlyCollection_1_t3395909286 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2260339638_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3747360123_gshared (ReadOnlyCollection_1_t3395909286 * __this, int32_t ___index0, Point_t1838831750  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3747360123(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3395909286 *, int32_t, Point_t1838831750 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3747360123_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m71367971_gshared (ReadOnlyCollection_1_t3395909286 * __this, Point_t1838831750  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m71367971(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3395909286 *, Point_t1838831750 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m71367971_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1621212993_gshared (ReadOnlyCollection_1_t3395909286 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1621212993(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3395909286 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1621212993_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  Point_t1838831750  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1552162023_gshared (ReadOnlyCollection_1_t3395909286 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1552162023(__this, ___index0, method) ((  Point_t1838831750  (*) (ReadOnlyCollection_1_t3395909286 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1552162023_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3840080786_gshared (ReadOnlyCollection_1_t3395909286 * __this, int32_t ___index0, Point_t1838831750  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3840080786(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3395909286 *, int32_t, Point_t1838831750 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3840080786_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3722003212_gshared (ReadOnlyCollection_1_t3395909286 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3722003212(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3395909286 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3722003212_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3266125913_gshared (ReadOnlyCollection_1_t3395909286 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3266125913(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3395909286 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3266125913_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2715993512_gshared (ReadOnlyCollection_1_t3395909286 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2715993512(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3395909286 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2715993512_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m630958229_gshared (ReadOnlyCollection_1_t3395909286 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m630958229(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3395909286 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m630958229_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m4280931175_gshared (ReadOnlyCollection_1_t3395909286 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m4280931175(__this, method) ((  void (*) (ReadOnlyCollection_1_t3395909286 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m4280931175_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1618777291_gshared (ReadOnlyCollection_1_t3395909286 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1618777291(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3395909286 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1618777291_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3606809709_gshared (ReadOnlyCollection_1_t3395909286 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3606809709(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3395909286 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3606809709_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2014007392_gshared (ReadOnlyCollection_1_t3395909286 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2014007392(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3395909286 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2014007392_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m281885896_gshared (ReadOnlyCollection_1_t3395909286 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m281885896(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3395909286 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m281885896_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1733883888_gshared (ReadOnlyCollection_1_t3395909286 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1733883888(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3395909286 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1733883888_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1096452785_gshared (ReadOnlyCollection_1_t3395909286 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1096452785(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3395909286 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1096452785_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3703488355_gshared (ReadOnlyCollection_1_t3395909286 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3703488355(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3395909286 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3703488355_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1234860154_gshared (ReadOnlyCollection_1_t3395909286 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1234860154(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3395909286 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1234860154_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3957526335_gshared (ReadOnlyCollection_1_t3395909286 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3957526335(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3395909286 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3957526335_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3884999722_gshared (ReadOnlyCollection_1_t3395909286 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3884999722(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3395909286 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3884999722_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3339772983_gshared (ReadOnlyCollection_1_t3395909286 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3339772983(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3395909286 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3339772983_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m4184254824_gshared (ReadOnlyCollection_1_t3395909286 * __this, Point_t1838831750  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m4184254824(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3395909286 *, Point_t1838831750 , const MethodInfo*))ReadOnlyCollection_1_Contains_m4184254824_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m1936450244_gshared (ReadOnlyCollection_1_t3395909286 * __this, PointU5BU5D_t2313848483* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1936450244(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3395909286 *, PointU5BU5D_t2313848483*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1936450244_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3340134591_gshared (ReadOnlyCollection_1_t3395909286 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3340134591(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3395909286 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3340134591_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1896038288_gshared (ReadOnlyCollection_1_t3395909286 * __this, Point_t1838831750  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1896038288(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3395909286 *, Point_t1838831750 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1896038288_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2759919595_gshared (ReadOnlyCollection_1_t3395909286 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m2759919595(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3395909286 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2759919595_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<PointCloudRegognizer/Point>::get_Item(System.Int32)
extern "C"  Point_t1838831750  ReadOnlyCollection_1_get_Item_m3936003623_gshared (ReadOnlyCollection_1_t3395909286 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3936003623(__this, ___index0, method) ((  Point_t1838831750  (*) (ReadOnlyCollection_1_t3395909286 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3936003623_gshared)(__this, ___index0, method)
