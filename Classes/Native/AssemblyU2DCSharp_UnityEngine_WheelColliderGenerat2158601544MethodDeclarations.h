﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_WheelColliderGenerated
struct UnityEngine_WheelColliderGenerated_t2158601544;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_WheelColliderGenerated::.ctor()
extern "C"  void UnityEngine_WheelColliderGenerated__ctor_m1781177427 (UnityEngine_WheelColliderGenerated_t2158601544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WheelColliderGenerated::WheelCollider_WheelCollider1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WheelColliderGenerated_WheelCollider_WheelCollider1_m4266070119 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelColliderGenerated::WheelCollider_center(JSVCall)
extern "C"  void UnityEngine_WheelColliderGenerated_WheelCollider_center_m3769974129 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelColliderGenerated::WheelCollider_radius(JSVCall)
extern "C"  void UnityEngine_WheelColliderGenerated_WheelCollider_radius_m971870676 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelColliderGenerated::WheelCollider_suspensionDistance(JSVCall)
extern "C"  void UnityEngine_WheelColliderGenerated_WheelCollider_suspensionDistance_m560424820 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelColliderGenerated::WheelCollider_suspensionSpring(JSVCall)
extern "C"  void UnityEngine_WheelColliderGenerated_WheelCollider_suspensionSpring_m4064084252 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelColliderGenerated::WheelCollider_forceAppPointDistance(JSVCall)
extern "C"  void UnityEngine_WheelColliderGenerated_WheelCollider_forceAppPointDistance_m780807095 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelColliderGenerated::WheelCollider_mass(JSVCall)
extern "C"  void UnityEngine_WheelColliderGenerated_WheelCollider_mass_m4198378930 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelColliderGenerated::WheelCollider_wheelDampingRate(JSVCall)
extern "C"  void UnityEngine_WheelColliderGenerated_WheelCollider_wheelDampingRate_m1298578271 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelColliderGenerated::WheelCollider_forwardFriction(JSVCall)
extern "C"  void UnityEngine_WheelColliderGenerated_WheelCollider_forwardFriction_m335099623 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelColliderGenerated::WheelCollider_sidewaysFriction(JSVCall)
extern "C"  void UnityEngine_WheelColliderGenerated_WheelCollider_sidewaysFriction_m2622846065 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelColliderGenerated::WheelCollider_motorTorque(JSVCall)
extern "C"  void UnityEngine_WheelColliderGenerated_WheelCollider_motorTorque_m3404330215 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelColliderGenerated::WheelCollider_brakeTorque(JSVCall)
extern "C"  void UnityEngine_WheelColliderGenerated_WheelCollider_brakeTorque_m4061224817 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelColliderGenerated::WheelCollider_steerAngle(JSVCall)
extern "C"  void UnityEngine_WheelColliderGenerated_WheelCollider_steerAngle_m824117988 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelColliderGenerated::WheelCollider_isGrounded(JSVCall)
extern "C"  void UnityEngine_WheelColliderGenerated_WheelCollider_isGrounded_m440469622 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelColliderGenerated::WheelCollider_sprungMass(JSVCall)
extern "C"  void UnityEngine_WheelColliderGenerated_WheelCollider_sprungMass_m4048242457 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelColliderGenerated::WheelCollider_rpm(JSVCall)
extern "C"  void UnityEngine_WheelColliderGenerated_WheelCollider_rpm_m3861620695 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WheelColliderGenerated::WheelCollider_ConfigureVehicleSubsteps__Single__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WheelColliderGenerated_WheelCollider_ConfigureVehicleSubsteps__Single__Int32__Int32_m17623482 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WheelColliderGenerated::WheelCollider_GetGroundHit__WheelHit(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WheelColliderGenerated_WheelCollider_GetGroundHit__WheelHit_m2284392019 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WheelColliderGenerated::WheelCollider_GetWorldPose__Vector3__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WheelColliderGenerated_WheelCollider_GetWorldPose__Vector3__Quaternion_m1359619742 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelColliderGenerated::__Register()
extern "C"  void UnityEngine_WheelColliderGenerated___Register_m2270862420 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_WheelColliderGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_WheelColliderGenerated_ilo_getObject1_m4102257951 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_WheelColliderGenerated::ilo_getVector3S2(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_WheelColliderGenerated_ilo_getVector3S2_m3163774152 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_WheelColliderGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_WheelColliderGenerated_ilo_getSingle3_m3831769142 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_WheelColliderGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_WheelColliderGenerated_ilo_getObject4_m1529115391 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelColliderGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void UnityEngine_WheelColliderGenerated_ilo_setSingle5_m2991969605 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_WheelColliderGenerated::ilo_setObject6(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_WheelColliderGenerated_ilo_setObject6_m853515228 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_WheelColliderGenerated::ilo_incArgIndex7()
extern "C"  int32_t UnityEngine_WheelColliderGenerated_ilo_incArgIndex7_m2027084047 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelColliderGenerated::ilo_setVector3S8(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_WheelColliderGenerated_ilo_setVector3S8_m1550008923 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
