﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimationRunnerGenerated
struct AnimationRunnerGenerated_t3223500859;
// JSVCall
struct JSVCall_t3708497963;
// AnimationRunner
struct AnimationRunner_t1015409588;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniState1127540784.h"
#include "AssemblyU2DCSharp_AnimationRunner1015409588.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"

// System.Void AnimationRunnerGenerated::.ctor()
extern "C"  void AnimationRunnerGenerated__ctor_m3717916864 (AnimationRunnerGenerated_t3223500859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationRunnerGenerated::AnimationRunner_AnimationRunner1(JSVCall,System.Int32)
extern "C"  bool AnimationRunnerGenerated_AnimationRunner_AnimationRunner1_m1866832720 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunnerGenerated::AnimationRunner_curAniName(JSVCall)
extern "C"  void AnimationRunnerGenerated_AnimationRunner_curAniName_m459234225 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunnerGenerated::AnimationRunner_unit(JSVCall)
extern "C"  void AnimationRunnerGenerated_AnimationRunner_unit_m1334911092 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunnerGenerated::AnimationRunner_isSkillFrozen(JSVCall)
extern "C"  void AnimationRunnerGenerated_AnimationRunner_isSkillFrozen_m4246630637 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunnerGenerated::AnimationRunner_aniState(JSVCall)
extern "C"  void AnimationRunnerGenerated_AnimationRunner_aniState_m944693251 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationRunnerGenerated::AnimationRunner_Clear(JSVCall,System.Int32)
extern "C"  bool AnimationRunnerGenerated_AnimationRunner_Clear_m4064811452 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationRunnerGenerated::AnimationRunner_GetActionNormalizedTime__AniType(JSVCall,System.Int32)
extern "C"  bool AnimationRunnerGenerated_AnimationRunner_GetActionNormalizedTime__AniType_m2268527161 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationRunnerGenerated::AnimationRunner_GetActionTime__AniType(JSVCall,System.Int32)
extern "C"  bool AnimationRunnerGenerated_AnimationRunner_GetActionTime__AniType_m3993494576 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationRunnerGenerated::AnimationRunner_GetActionTime__String(JSVCall,System.Int32)
extern "C"  bool AnimationRunnerGenerated_AnimationRunner_GetActionTime__String_m3743608025 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationRunnerGenerated::AnimationRunner_GetActionTime(JSVCall,System.Int32)
extern "C"  bool AnimationRunnerGenerated_AnimationRunner_GetActionTime_m1271871112 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationRunnerGenerated::AnimationRunner_HasExistAni__String(JSVCall,System.Int32)
extern "C"  bool AnimationRunnerGenerated_AnimationRunner_HasExistAni__String_m2229368799 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationRunnerGenerated::AnimationRunner_IsPlay__AniType(JSVCall,System.Int32)
extern "C"  bool AnimationRunnerGenerated_AnimationRunner_IsPlay__AniType_m2656639271 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationRunnerGenerated::AnimationRunner_Play__Boolean__String__Single(JSVCall,System.Int32)
extern "C"  bool AnimationRunnerGenerated_AnimationRunner_Play__Boolean__String__Single_m1521397820 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationRunnerGenerated::AnimationRunner_Play__String__Single__Boolean(JSVCall,System.Int32)
extern "C"  bool AnimationRunnerGenerated_AnimationRunner_Play__String__Single__Boolean_m164824874 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationRunnerGenerated::AnimationRunner_Play__AniType__Single__Boolean(JSVCall,System.Int32)
extern "C"  bool AnimationRunnerGenerated_AnimationRunner_Play__AniType__Single__Boolean_m2241902801 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationRunnerGenerated::AnimationRunner_playOver(JSVCall,System.Int32)
extern "C"  bool AnimationRunnerGenerated_AnimationRunner_playOver_m1785016891 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationRunnerGenerated::AnimationRunner_SetActionTime__String__Single(JSVCall,System.Int32)
extern "C"  bool AnimationRunnerGenerated_AnimationRunner_SetActionTime__String__Single_m400898093 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationRunnerGenerated::AnimationRunner_SetRunSpeed__Single(JSVCall,System.Int32)
extern "C"  bool AnimationRunnerGenerated_AnimationRunner_SetRunSpeed__Single_m1317793653 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationRunnerGenerated::AnimationRunner_SetWrapModel__WrapMode(JSVCall,System.Int32)
extern "C"  bool AnimationRunnerGenerated_AnimationRunner_SetWrapModel__WrapMode_m3564297917 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunnerGenerated::__Register()
extern "C"  void AnimationRunnerGenerated___Register_m607783943 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunnerGenerated::ilo_setBooleanS1(System.Int32,System.Boolean)
extern "C"  void AnimationRunnerGenerated_ilo_setBooleanS1_m3409296177 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnimationRunner/AniState AnimationRunnerGenerated::ilo_get_aniState2(AnimationRunner)
extern "C"  int32_t AnimationRunnerGenerated_ilo_get_aniState2_m376320876 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AnimationRunnerGenerated::ilo_getEnum3(System.Int32)
extern "C"  int32_t AnimationRunnerGenerated_ilo_getEnum3_m924736082 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AnimationRunnerGenerated::ilo_getStringS4(System.Int32)
extern "C"  String_t* AnimationRunnerGenerated_ilo_getStringS4_m3162360641 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunnerGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void AnimationRunnerGenerated_ilo_setSingle5_m1284311480 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationRunnerGenerated::ilo_IsPlay6(AnimationRunner,AnimationRunner/AniType)
extern "C"  bool AnimationRunnerGenerated_ilo_IsPlay6_m2359250848 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, int32_t ___aniType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunnerGenerated::ilo_Play7(AnimationRunner,AnimationRunner/AniType,System.Single,System.Boolean)
extern "C"  void AnimationRunnerGenerated_ilo_Play7_m3003506391 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, int32_t ___aniType1, float ___playTime2, bool ___isskill3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationRunnerGenerated::ilo_getSingle8(System.Int32)
extern "C"  float AnimationRunnerGenerated_ilo_getSingle8_m1177039918 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnimationRunnerGenerated::ilo_getBooleanS9(System.Int32)
extern "C"  bool AnimationRunnerGenerated_ilo_getBooleanS9_m3879228948 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationRunnerGenerated::ilo_SetRunSpeed10(AnimationRunner,System.Single)
extern "C"  void AnimationRunnerGenerated_ilo_SetRunSpeed10_m1473334139 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, float ___runSpeed1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
