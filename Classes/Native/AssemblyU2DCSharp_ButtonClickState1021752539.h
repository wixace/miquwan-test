﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UISprite
struct UISprite_t661437049;
// ButtonClickState/VoidDelegate
struct VoidDelegate_t3553201165;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonClickState
struct  ButtonClickState_t1021752539  : public MonoBehaviour_t667441552
{
public:
	// System.String ButtonClickState::strNormal
	String_t* ___strNormal_2;
	// System.String ButtonClickState::strPress
	String_t* ___strPress_3;
	// System.Single ButtonClickState::interval_max_time
	float ___interval_max_time_4;
	// System.Single ButtonClickState::interval_min_time
	float ___interval_min_time_5;
	// System.Single ButtonClickState::less_speed
	float ___less_speed_6;
	// System.Single ButtonClickState::UsingSumTime
	float ___UsingSumTime_7;
	// System.Single ButtonClickState::_using_time
	float ____using_time_8;
	// System.Single ButtonClickState::_move_time
	float ____move_time_9;
	// System.Single ButtonClickState::_call_time
	float ____call_time_10;
	// System.Boolean ButtonClickState::_fun_enable
	bool ____fun_enable_11;
	// System.Boolean ButtonClickState::_isClick
	bool ____isClick_12;
	// UISprite ButtonClickState::_cur_sprite
	UISprite_t661437049 * ____cur_sprite_13;
	// ButtonClickState/VoidDelegate ButtonClickState::onClick
	VoidDelegate_t3553201165 * ___onClick_14;

public:
	inline static int32_t get_offset_of_strNormal_2() { return static_cast<int32_t>(offsetof(ButtonClickState_t1021752539, ___strNormal_2)); }
	inline String_t* get_strNormal_2() const { return ___strNormal_2; }
	inline String_t** get_address_of_strNormal_2() { return &___strNormal_2; }
	inline void set_strNormal_2(String_t* value)
	{
		___strNormal_2 = value;
		Il2CppCodeGenWriteBarrier(&___strNormal_2, value);
	}

	inline static int32_t get_offset_of_strPress_3() { return static_cast<int32_t>(offsetof(ButtonClickState_t1021752539, ___strPress_3)); }
	inline String_t* get_strPress_3() const { return ___strPress_3; }
	inline String_t** get_address_of_strPress_3() { return &___strPress_3; }
	inline void set_strPress_3(String_t* value)
	{
		___strPress_3 = value;
		Il2CppCodeGenWriteBarrier(&___strPress_3, value);
	}

	inline static int32_t get_offset_of_interval_max_time_4() { return static_cast<int32_t>(offsetof(ButtonClickState_t1021752539, ___interval_max_time_4)); }
	inline float get_interval_max_time_4() const { return ___interval_max_time_4; }
	inline float* get_address_of_interval_max_time_4() { return &___interval_max_time_4; }
	inline void set_interval_max_time_4(float value)
	{
		___interval_max_time_4 = value;
	}

	inline static int32_t get_offset_of_interval_min_time_5() { return static_cast<int32_t>(offsetof(ButtonClickState_t1021752539, ___interval_min_time_5)); }
	inline float get_interval_min_time_5() const { return ___interval_min_time_5; }
	inline float* get_address_of_interval_min_time_5() { return &___interval_min_time_5; }
	inline void set_interval_min_time_5(float value)
	{
		___interval_min_time_5 = value;
	}

	inline static int32_t get_offset_of_less_speed_6() { return static_cast<int32_t>(offsetof(ButtonClickState_t1021752539, ___less_speed_6)); }
	inline float get_less_speed_6() const { return ___less_speed_6; }
	inline float* get_address_of_less_speed_6() { return &___less_speed_6; }
	inline void set_less_speed_6(float value)
	{
		___less_speed_6 = value;
	}

	inline static int32_t get_offset_of_UsingSumTime_7() { return static_cast<int32_t>(offsetof(ButtonClickState_t1021752539, ___UsingSumTime_7)); }
	inline float get_UsingSumTime_7() const { return ___UsingSumTime_7; }
	inline float* get_address_of_UsingSumTime_7() { return &___UsingSumTime_7; }
	inline void set_UsingSumTime_7(float value)
	{
		___UsingSumTime_7 = value;
	}

	inline static int32_t get_offset_of__using_time_8() { return static_cast<int32_t>(offsetof(ButtonClickState_t1021752539, ____using_time_8)); }
	inline float get__using_time_8() const { return ____using_time_8; }
	inline float* get_address_of__using_time_8() { return &____using_time_8; }
	inline void set__using_time_8(float value)
	{
		____using_time_8 = value;
	}

	inline static int32_t get_offset_of__move_time_9() { return static_cast<int32_t>(offsetof(ButtonClickState_t1021752539, ____move_time_9)); }
	inline float get__move_time_9() const { return ____move_time_9; }
	inline float* get_address_of__move_time_9() { return &____move_time_9; }
	inline void set__move_time_9(float value)
	{
		____move_time_9 = value;
	}

	inline static int32_t get_offset_of__call_time_10() { return static_cast<int32_t>(offsetof(ButtonClickState_t1021752539, ____call_time_10)); }
	inline float get__call_time_10() const { return ____call_time_10; }
	inline float* get_address_of__call_time_10() { return &____call_time_10; }
	inline void set__call_time_10(float value)
	{
		____call_time_10 = value;
	}

	inline static int32_t get_offset_of__fun_enable_11() { return static_cast<int32_t>(offsetof(ButtonClickState_t1021752539, ____fun_enable_11)); }
	inline bool get__fun_enable_11() const { return ____fun_enable_11; }
	inline bool* get_address_of__fun_enable_11() { return &____fun_enable_11; }
	inline void set__fun_enable_11(bool value)
	{
		____fun_enable_11 = value;
	}

	inline static int32_t get_offset_of__isClick_12() { return static_cast<int32_t>(offsetof(ButtonClickState_t1021752539, ____isClick_12)); }
	inline bool get__isClick_12() const { return ____isClick_12; }
	inline bool* get_address_of__isClick_12() { return &____isClick_12; }
	inline void set__isClick_12(bool value)
	{
		____isClick_12 = value;
	}

	inline static int32_t get_offset_of__cur_sprite_13() { return static_cast<int32_t>(offsetof(ButtonClickState_t1021752539, ____cur_sprite_13)); }
	inline UISprite_t661437049 * get__cur_sprite_13() const { return ____cur_sprite_13; }
	inline UISprite_t661437049 ** get_address_of__cur_sprite_13() { return &____cur_sprite_13; }
	inline void set__cur_sprite_13(UISprite_t661437049 * value)
	{
		____cur_sprite_13 = value;
		Il2CppCodeGenWriteBarrier(&____cur_sprite_13, value);
	}

	inline static int32_t get_offset_of_onClick_14() { return static_cast<int32_t>(offsetof(ButtonClickState_t1021752539, ___onClick_14)); }
	inline VoidDelegate_t3553201165 * get_onClick_14() const { return ___onClick_14; }
	inline VoidDelegate_t3553201165 ** get_address_of_onClick_14() { return &___onClick_14; }
	inline void set_onClick_14(VoidDelegate_t3553201165 * value)
	{
		___onClick_14 = value;
		Il2CppCodeGenWriteBarrier(&___onClick_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
