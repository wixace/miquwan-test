﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ColorChanger
struct ColorChanger_t3168377343;

#include "codegen/il2cpp-codegen.h"

// System.Void ColorChanger::.ctor()
extern "C"  void ColorChanger__ctor_m1758603132 (ColorChanger_t3168377343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorChanger::Start()
extern "C"  void ColorChanger_Start_m705740924 (ColorChanger_t3168377343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorChanger::Update()
extern "C"  void ColorChanger_Update_m408984337 (ColorChanger_t3168377343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
