﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Texture3DGenerated
struct UnityEngine_Texture3DGenerated_t4248722827;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_Texture3DGenerated::.ctor()
extern "C"  void UnityEngine_Texture3DGenerated__ctor_m3111471984 (UnityEngine_Texture3DGenerated_t4248722827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture3DGenerated::Texture3D_Texture3D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture3DGenerated_Texture3D_Texture3D1_m785438832 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Texture3DGenerated::Texture3D_depth(JSVCall)
extern "C"  void UnityEngine_Texture3DGenerated_Texture3D_depth_m3117959689 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Texture3DGenerated::Texture3D_format(JSVCall)
extern "C"  void UnityEngine_Texture3DGenerated_Texture3D_format_m1247099977 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture3DGenerated::Texture3D_Apply__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture3DGenerated_Texture3D_Apply__Boolean_m2058770757 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture3DGenerated::Texture3D_Apply(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture3DGenerated_Texture3D_Apply_m124119429 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture3DGenerated::Texture3D_GetPixels__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture3DGenerated_Texture3D_GetPixels__Int32_m2580785942 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture3DGenerated::Texture3D_GetPixels(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture3DGenerated_Texture3D_GetPixels_m995692282 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture3DGenerated::Texture3D_GetPixels32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture3DGenerated_Texture3D_GetPixels32__Int32_m3393483607 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture3DGenerated::Texture3D_GetPixels32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture3DGenerated_Texture3D_GetPixels32_m2228395481 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture3DGenerated::Texture3D_SetPixels__Color_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture3DGenerated_Texture3D_SetPixels__Color_Array__Int32_m877109335 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture3DGenerated::Texture3D_SetPixels__Color_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture3DGenerated_Texture3D_SetPixels__Color_Array_m4065857753 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture3DGenerated::Texture3D_SetPixels32__Color32_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture3DGenerated_Texture3D_SetPixels32__Color32_Array__Int32_m330644407 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Texture3DGenerated::Texture3D_SetPixels32__Color32_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Texture3DGenerated_Texture3D_SetPixels32__Color32_Array_m1430815097 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Texture3DGenerated::__Register()
extern "C"  void UnityEngine_Texture3DGenerated___Register_m132241239 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine_Texture3DGenerated::<Texture3D_SetPixels__Color_Array__Int32>m__2E7()
extern "C"  ColorU5BU5D_t2441545636* UnityEngine_Texture3DGenerated_U3CTexture3D_SetPixels__Color_Array__Int32U3Em__2E7_m555009997 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine_Texture3DGenerated::<Texture3D_SetPixels__Color_Array>m__2E8()
extern "C"  ColorU5BU5D_t2441545636* UnityEngine_Texture3DGenerated_U3CTexture3D_SetPixels__Color_ArrayU3Em__2E8_m3237225458 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32[] UnityEngine_Texture3DGenerated::<Texture3D_SetPixels32__Color32_Array__Int32>m__2E9()
extern "C"  Color32U5BU5D_t2960766953* UnityEngine_Texture3DGenerated_U3CTexture3D_SetPixels32__Color32_Array__Int32U3Em__2E9_m2259923598 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32[] UnityEngine_Texture3DGenerated::<Texture3D_SetPixels32__Color32_Array>m__2EA()
extern "C"  Color32U5BU5D_t2960766953* UnityEngine_Texture3DGenerated_U3CTexture3D_SetPixels32__Color32_ArrayU3Em__2EA_m2779577180 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Texture3DGenerated::ilo_getInt321(System.Int32)
extern "C"  int32_t UnityEngine_Texture3DGenerated_ilo_getInt321_m2096224311 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Texture3DGenerated::ilo_setInt322(System.Int32,System.Int32)
extern "C"  void UnityEngine_Texture3DGenerated_ilo_setInt322_m1107721669 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Texture3DGenerated::ilo_setEnum3(System.Int32,System.Int32)
extern "C"  void UnityEngine_Texture3DGenerated_ilo_setEnum3_m2482218959 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Texture3DGenerated::ilo_moveSaveID2Arr4(System.Int32)
extern "C"  void UnityEngine_Texture3DGenerated_ilo_moveSaveID2Arr4_m1130007624 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Texture3DGenerated::ilo_setArrayS5(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_Texture3DGenerated_ilo_setArrayS5_m1374834327 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Texture3DGenerated::ilo_getObject6(System.Int32)
extern "C"  int32_t UnityEngine_Texture3DGenerated_ilo_getObject6_m4250049639 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Texture3DGenerated::ilo_getArrayLength7(System.Int32)
extern "C"  int32_t UnityEngine_Texture3DGenerated_ilo_getArrayLength7_m4059022348 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Texture3DGenerated::ilo_getElement8(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_Texture3DGenerated_ilo_getElement8_m3551408519 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_Texture3DGenerated::ilo_getObject9(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_Texture3DGenerated_ilo_getObject9_m159570887 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
