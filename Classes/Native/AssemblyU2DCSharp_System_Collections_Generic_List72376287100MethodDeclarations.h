﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_Find__PredicateT1_T>c__AnonStorey80
struct U3CListA1_Find__PredicateT1_TU3Ec__AnonStorey80_t2376287100;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_Find__PredicateT1_T>c__AnonStorey80::.ctor()
extern "C"  void U3CListA1_Find__PredicateT1_TU3Ec__AnonStorey80__ctor_m729439391 (U3CListA1_Find__PredicateT1_TU3Ec__AnonStorey80_t2376287100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Collections_Generic_List77Generated/<ListA1_Find__PredicateT1_T>c__AnonStorey80::<>m__A5()
extern "C"  Il2CppObject * U3CListA1_Find__PredicateT1_TU3Ec__AnonStorey80_U3CU3Em__A5_m888693927 (U3CListA1_Find__PredicateT1_TU3Ec__AnonStorey80_t2376287100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
