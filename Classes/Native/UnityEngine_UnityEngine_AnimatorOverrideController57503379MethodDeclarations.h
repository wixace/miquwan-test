﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AnimatorOverrideController
struct AnimatorOverrideController_t57503379;
// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t274649809;
// UnityEngine.AnimationClip
struct AnimationClip_t2007702890;
// System.String
struct String_t;
// UnityEngine.AnimationClipPair[]
struct AnimationClipPairU5BU5D_t3767720461;
// UnityEngine.AnimationClip[]
struct AnimationClipU5BU5D_t4186127791;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AnimatorOverrideController57503379.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController274649809.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_AnimationClip2007702890.h"

// System.Void UnityEngine.AnimatorOverrideController::.ctor()
extern "C"  void AnimatorOverrideController__ctor_m3965744126 (AnimatorOverrideController_t57503379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimatorOverrideController::Internal_CreateAnimationSet(UnityEngine.AnimatorOverrideController)
extern "C"  void AnimatorOverrideController_Internal_CreateAnimationSet_m3067354450 (Il2CppObject * __this /* static, unused */, AnimatorOverrideController_t57503379 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RuntimeAnimatorController UnityEngine.AnimatorOverrideController::get_runtimeAnimatorController()
extern "C"  RuntimeAnimatorController_t274649809 * AnimatorOverrideController_get_runtimeAnimatorController_m131747327 (AnimatorOverrideController_t57503379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimatorOverrideController::set_runtimeAnimatorController(UnityEngine.RuntimeAnimatorController)
extern "C"  void AnimatorOverrideController_set_runtimeAnimatorController_m576560628 (AnimatorOverrideController_t57503379 * __this, RuntimeAnimatorController_t274649809 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationClip UnityEngine.AnimatorOverrideController::get_Item(System.String)
extern "C"  AnimationClip_t2007702890 * AnimatorOverrideController_get_Item_m570518154 (AnimatorOverrideController_t57503379 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimatorOverrideController::set_Item(System.String,UnityEngine.AnimationClip)
extern "C"  void AnimatorOverrideController_set_Item_m2615091419 (AnimatorOverrideController_t57503379 * __this, String_t* ___name0, AnimationClip_t2007702890 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationClip UnityEngine.AnimatorOverrideController::Internal_GetClipByName(System.String,System.Boolean)
extern "C"  AnimationClip_t2007702890 * AnimatorOverrideController_Internal_GetClipByName_m2901370205 (AnimatorOverrideController_t57503379 * __this, String_t* ___name0, bool ___returnEffectiveClip1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimatorOverrideController::Internal_SetClipByName(System.String,UnityEngine.AnimationClip)
extern "C"  void AnimatorOverrideController_Internal_SetClipByName_m935983577 (AnimatorOverrideController_t57503379 * __this, String_t* ___name0, AnimationClip_t2007702890 * ___clip1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationClip UnityEngine.AnimatorOverrideController::get_Item(UnityEngine.AnimationClip)
extern "C"  AnimationClip_t2007702890 * AnimatorOverrideController_get_Item_m2766527037 (AnimatorOverrideController_t57503379 * __this, AnimationClip_t2007702890 * ___clip0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimatorOverrideController::set_Item(UnityEngine.AnimationClip,UnityEngine.AnimationClip)
extern "C"  void AnimatorOverrideController_set_Item_m688899022 (AnimatorOverrideController_t57503379 * __this, AnimationClip_t2007702890 * ___clip0, AnimationClip_t2007702890 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationClip UnityEngine.AnimatorOverrideController::Internal_GetClip(UnityEngine.AnimationClip,System.Boolean)
extern "C"  AnimationClip_t2007702890 * AnimatorOverrideController_Internal_GetClip_m1012546664 (AnimatorOverrideController_t57503379 * __this, AnimationClip_t2007702890 * ___originalClip0, bool ___returnEffectiveClip1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimatorOverrideController::Internal_SetClip(UnityEngine.AnimationClip,UnityEngine.AnimationClip,System.Boolean)
extern "C"  void AnimatorOverrideController_Internal_SetClip_m172608335 (AnimatorOverrideController_t57503379 * __this, AnimationClip_t2007702890 * ___originalClip0, AnimationClip_t2007702890 * ___overrideClip1, bool ___notify2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimatorOverrideController::Internal_SetClip(UnityEngine.AnimationClip,UnityEngine.AnimationClip)
extern "C"  void AnimatorOverrideController_Internal_SetClip_m421797486 (AnimatorOverrideController_t57503379 * __this, AnimationClip_t2007702890 * ___originalClip0, AnimationClip_t2007702890 * ___overrideClip1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimatorOverrideController::Internal_SetDirty()
extern "C"  void AnimatorOverrideController_Internal_SetDirty_m748604430 (AnimatorOverrideController_t57503379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationClipPair[] UnityEngine.AnimatorOverrideController::get_clips()
extern "C"  AnimationClipPairU5BU5D_t3767720461* AnimatorOverrideController_get_clips_m1593793916 (AnimatorOverrideController_t57503379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimatorOverrideController::set_clips(UnityEngine.AnimationClipPair[])
extern "C"  void AnimatorOverrideController_set_clips_m3881693655 (AnimatorOverrideController_t57503379 * __this, AnimationClipPairU5BU5D_t3767720461* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationClip[] UnityEngine.AnimatorOverrideController::GetOriginalClips()
extern "C"  AnimationClipU5BU5D_t4186127791* AnimatorOverrideController_GetOriginalClips_m1334244438 (AnimatorOverrideController_t57503379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
