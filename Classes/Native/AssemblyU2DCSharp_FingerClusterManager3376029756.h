﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<FingerClusterManager/Cluster>
struct List_1_t943449559;
// FingerGestures/FingerList
struct FingerList_t1886137443;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_DistanceUnit3471059769.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FingerClusterManager
struct  FingerClusterManager_t3376029756  : public MonoBehaviour_t667441552
{
public:
	// DistanceUnit FingerClusterManager::DistanceUnit
	int32_t ___DistanceUnit_2;
	// System.Single FingerClusterManager::ClusterRadius
	float ___ClusterRadius_3;
	// System.Single FingerClusterManager::TimeTolerance
	float ___TimeTolerance_4;
	// System.Int32 FingerClusterManager::lastUpdateFrame
	int32_t ___lastUpdateFrame_5;
	// System.Int32 FingerClusterManager::nextClusterId
	int32_t ___nextClusterId_6;
	// System.Collections.Generic.List`1<FingerClusterManager/Cluster> FingerClusterManager::clusters
	List_1_t943449559 * ___clusters_7;
	// System.Collections.Generic.List`1<FingerClusterManager/Cluster> FingerClusterManager::clusterPool
	List_1_t943449559 * ___clusterPool_8;
	// FingerGestures/FingerList FingerClusterManager::fingersAdded
	FingerList_t1886137443 * ___fingersAdded_9;
	// FingerGestures/FingerList FingerClusterManager::fingersRemoved
	FingerList_t1886137443 * ___fingersRemoved_10;

public:
	inline static int32_t get_offset_of_DistanceUnit_2() { return static_cast<int32_t>(offsetof(FingerClusterManager_t3376029756, ___DistanceUnit_2)); }
	inline int32_t get_DistanceUnit_2() const { return ___DistanceUnit_2; }
	inline int32_t* get_address_of_DistanceUnit_2() { return &___DistanceUnit_2; }
	inline void set_DistanceUnit_2(int32_t value)
	{
		___DistanceUnit_2 = value;
	}

	inline static int32_t get_offset_of_ClusterRadius_3() { return static_cast<int32_t>(offsetof(FingerClusterManager_t3376029756, ___ClusterRadius_3)); }
	inline float get_ClusterRadius_3() const { return ___ClusterRadius_3; }
	inline float* get_address_of_ClusterRadius_3() { return &___ClusterRadius_3; }
	inline void set_ClusterRadius_3(float value)
	{
		___ClusterRadius_3 = value;
	}

	inline static int32_t get_offset_of_TimeTolerance_4() { return static_cast<int32_t>(offsetof(FingerClusterManager_t3376029756, ___TimeTolerance_4)); }
	inline float get_TimeTolerance_4() const { return ___TimeTolerance_4; }
	inline float* get_address_of_TimeTolerance_4() { return &___TimeTolerance_4; }
	inline void set_TimeTolerance_4(float value)
	{
		___TimeTolerance_4 = value;
	}

	inline static int32_t get_offset_of_lastUpdateFrame_5() { return static_cast<int32_t>(offsetof(FingerClusterManager_t3376029756, ___lastUpdateFrame_5)); }
	inline int32_t get_lastUpdateFrame_5() const { return ___lastUpdateFrame_5; }
	inline int32_t* get_address_of_lastUpdateFrame_5() { return &___lastUpdateFrame_5; }
	inline void set_lastUpdateFrame_5(int32_t value)
	{
		___lastUpdateFrame_5 = value;
	}

	inline static int32_t get_offset_of_nextClusterId_6() { return static_cast<int32_t>(offsetof(FingerClusterManager_t3376029756, ___nextClusterId_6)); }
	inline int32_t get_nextClusterId_6() const { return ___nextClusterId_6; }
	inline int32_t* get_address_of_nextClusterId_6() { return &___nextClusterId_6; }
	inline void set_nextClusterId_6(int32_t value)
	{
		___nextClusterId_6 = value;
	}

	inline static int32_t get_offset_of_clusters_7() { return static_cast<int32_t>(offsetof(FingerClusterManager_t3376029756, ___clusters_7)); }
	inline List_1_t943449559 * get_clusters_7() const { return ___clusters_7; }
	inline List_1_t943449559 ** get_address_of_clusters_7() { return &___clusters_7; }
	inline void set_clusters_7(List_1_t943449559 * value)
	{
		___clusters_7 = value;
		Il2CppCodeGenWriteBarrier(&___clusters_7, value);
	}

	inline static int32_t get_offset_of_clusterPool_8() { return static_cast<int32_t>(offsetof(FingerClusterManager_t3376029756, ___clusterPool_8)); }
	inline List_1_t943449559 * get_clusterPool_8() const { return ___clusterPool_8; }
	inline List_1_t943449559 ** get_address_of_clusterPool_8() { return &___clusterPool_8; }
	inline void set_clusterPool_8(List_1_t943449559 * value)
	{
		___clusterPool_8 = value;
		Il2CppCodeGenWriteBarrier(&___clusterPool_8, value);
	}

	inline static int32_t get_offset_of_fingersAdded_9() { return static_cast<int32_t>(offsetof(FingerClusterManager_t3376029756, ___fingersAdded_9)); }
	inline FingerList_t1886137443 * get_fingersAdded_9() const { return ___fingersAdded_9; }
	inline FingerList_t1886137443 ** get_address_of_fingersAdded_9() { return &___fingersAdded_9; }
	inline void set_fingersAdded_9(FingerList_t1886137443 * value)
	{
		___fingersAdded_9 = value;
		Il2CppCodeGenWriteBarrier(&___fingersAdded_9, value);
	}

	inline static int32_t get_offset_of_fingersRemoved_10() { return static_cast<int32_t>(offsetof(FingerClusterManager_t3376029756, ___fingersRemoved_10)); }
	inline FingerList_t1886137443 * get_fingersRemoved_10() const { return ___fingersRemoved_10; }
	inline FingerList_t1886137443 ** get_address_of_fingersRemoved_10() { return &___fingersRemoved_10; }
	inline void set_fingersRemoved_10(FingerList_t1886137443 * value)
	{
		___fingersRemoved_10 = value;
		Il2CppCodeGenWriteBarrier(&___fingersRemoved_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
