﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Int32 EArray::IndexOf(System.Array,System.Object)
extern "C"  int32_t EArray_IndexOf_m926102760 (Il2CppObject * __this /* static, unused */, Il2CppArray * ___array0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EArray::IndexOf(System.Array,System.Object,System.Int32)
extern "C"  int32_t EArray_IndexOf_m12787951 (Il2CppObject * __this /* static, unused */, Il2CppArray * ___array0, Il2CppObject * ___value1, int32_t ___startIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EArray::IndexOf(System.Array,System.Object,System.Int32,System.Int32)
extern "C"  int32_t EArray_IndexOf_m444807240 (Il2CppObject * __this /* static, unused */, Il2CppArray * ___array0, Il2CppObject * ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EArray::LastIndexOf(System.Array,System.Object)
extern "C"  int32_t EArray_LastIndexOf_m3176648050 (Il2CppObject * __this /* static, unused */, Il2CppArray * ___array0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EArray::LastIndexOf(System.Array,System.Object,System.Int32)
extern "C"  int32_t EArray_LastIndexOf_m107231397 (Il2CppObject * __this /* static, unused */, Il2CppArray * ___array0, Il2CppObject * ___value1, int32_t ___startIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EArray::LastIndexOf(System.Array,System.Object,System.Int32,System.Int32)
extern "C"  int32_t EArray_LastIndexOf_m2202851410 (Il2CppObject * __this /* static, unused */, Il2CppArray * ___array0, Il2CppObject * ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
