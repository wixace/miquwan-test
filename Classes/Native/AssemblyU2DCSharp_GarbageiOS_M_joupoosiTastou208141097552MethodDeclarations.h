﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_joupoosiTastou208
struct M_joupoosiTastou208_t141097552;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_joupoosiTastou208::.ctor()
extern "C"  void M_joupoosiTastou208__ctor_m4035389715 (M_joupoosiTastou208_t141097552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_joupoosiTastou208::M_lasoorarKifas0(System.String[],System.Int32)
extern "C"  void M_joupoosiTastou208_M_lasoorarKifas0_m2132066581 (M_joupoosiTastou208_t141097552 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_joupoosiTastou208::M_cenere1(System.String[],System.Int32)
extern "C"  void M_joupoosiTastou208_M_cenere1_m3828955387 (M_joupoosiTastou208_t141097552 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_joupoosiTastou208::M_miwonarFowga2(System.String[],System.Int32)
extern "C"  void M_joupoosiTastou208_M_miwonarFowga2_m3572555883 (M_joupoosiTastou208_t141097552 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
