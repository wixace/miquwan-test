﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_haltiKose165
struct  M_haltiKose165_t1903042594  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_haltiKose165::_pearjedo
	String_t* ____pearjedo_0;
	// System.Single GarbageiOS.M_haltiKose165::_sucuWoupoo
	float ____sucuWoupoo_1;
	// System.UInt32 GarbageiOS.M_haltiKose165::_chepecirMalmasne
	uint32_t ____chepecirMalmasne_2;
	// System.Int32 GarbageiOS.M_haltiKose165::_lawai
	int32_t ____lawai_3;
	// System.Boolean GarbageiOS.M_haltiKose165::_nachalHafay
	bool ____nachalHafay_4;
	// System.Single GarbageiOS.M_haltiKose165::_ferexani
	float ____ferexani_5;
	// System.Boolean GarbageiOS.M_haltiKose165::_kursem
	bool ____kursem_6;
	// System.Boolean GarbageiOS.M_haltiKose165::_rirbelsu
	bool ____rirbelsu_7;
	// System.UInt32 GarbageiOS.M_haltiKose165::_rairtear
	uint32_t ____rairtear_8;
	// System.Int32 GarbageiOS.M_haltiKose165::_teegaSaste
	int32_t ____teegaSaste_9;
	// System.Int32 GarbageiOS.M_haltiKose165::_nearmoyurDruxider
	int32_t ____nearmoyurDruxider_10;
	// System.Boolean GarbageiOS.M_haltiKose165::_hatewow
	bool ____hatewow_11;
	// System.String GarbageiOS.M_haltiKose165::_mitelna
	String_t* ____mitelna_12;
	// System.String GarbageiOS.M_haltiKose165::_drelnooHowjar
	String_t* ____drelnooHowjar_13;
	// System.Boolean GarbageiOS.M_haltiKose165::_whoucem
	bool ____whoucem_14;
	// System.Boolean GarbageiOS.M_haltiKose165::_wepemBijer
	bool ____wepemBijer_15;
	// System.String GarbageiOS.M_haltiKose165::_lori
	String_t* ____lori_16;
	// System.Single GarbageiOS.M_haltiKose165::_napalPelkeemo
	float ____napalPelkeemo_17;
	// System.String GarbageiOS.M_haltiKose165::_rooboCawbawder
	String_t* ____rooboCawbawder_18;
	// System.Int32 GarbageiOS.M_haltiKose165::_kalpepor
	int32_t ____kalpepor_19;
	// System.Boolean GarbageiOS.M_haltiKose165::_sarallTefoo
	bool ____sarallTefoo_20;
	// System.String GarbageiOS.M_haltiKose165::_hurnasqoVerjorxere
	String_t* ____hurnasqoVerjorxere_21;
	// System.Int32 GarbageiOS.M_haltiKose165::_seejaDuze
	int32_t ____seejaDuze_22;
	// System.UInt32 GarbageiOS.M_haltiKose165::_tarqere
	uint32_t ____tarqere_23;
	// System.UInt32 GarbageiOS.M_haltiKose165::_mitrumerJemle
	uint32_t ____mitrumerJemle_24;
	// System.String GarbageiOS.M_haltiKose165::_dosorSedee
	String_t* ____dosorSedee_25;
	// System.Single GarbageiOS.M_haltiKose165::_sahukallChasja
	float ____sahukallChasja_26;
	// System.Int32 GarbageiOS.M_haltiKose165::_maychu
	int32_t ____maychu_27;
	// System.Single GarbageiOS.M_haltiKose165::_zookiHistrelxur
	float ____zookiHistrelxur_28;
	// System.Single GarbageiOS.M_haltiKose165::_sicorjayGaijawsem
	float ____sicorjayGaijawsem_29;
	// System.Boolean GarbageiOS.M_haltiKose165::_sowchearLarcemxal
	bool ____sowchearLarcemxal_30;
	// System.Int32 GarbageiOS.M_haltiKose165::_cernairMuhegaw
	int32_t ____cernairMuhegaw_31;
	// System.UInt32 GarbageiOS.M_haltiKose165::_ralcekairWhificas
	uint32_t ____ralcekairWhificas_32;
	// System.Int32 GarbageiOS.M_haltiKose165::_nara
	int32_t ____nara_33;
	// System.Int32 GarbageiOS.M_haltiKose165::_marrelbel
	int32_t ____marrelbel_34;
	// System.String GarbageiOS.M_haltiKose165::_wurhasmay
	String_t* ____wurhasmay_35;
	// System.Single GarbageiOS.M_haltiKose165::_temcar
	float ____temcar_36;
	// System.UInt32 GarbageiOS.M_haltiKose165::_wepiNishaybis
	uint32_t ____wepiNishaybis_37;
	// System.Boolean GarbageiOS.M_haltiKose165::_pirfemWhawrearwhu
	bool ____pirfemWhawrearwhu_38;
	// System.Boolean GarbageiOS.M_haltiKose165::_saryurWemqearcis
	bool ____saryurWemqearcis_39;
	// System.Int32 GarbageiOS.M_haltiKose165::_mirchidiWalsall
	int32_t ____mirchidiWalsall_40;
	// System.Int32 GarbageiOS.M_haltiKose165::_narair
	int32_t ____narair_41;
	// System.Single GarbageiOS.M_haltiKose165::_ritoutroKehay
	float ____ritoutroKehay_42;
	// System.UInt32 GarbageiOS.M_haltiKose165::_bearyis
	uint32_t ____bearyis_43;
	// System.Boolean GarbageiOS.M_haltiKose165::_numeHateemur
	bool ____numeHateemur_44;
	// System.Int32 GarbageiOS.M_haltiKose165::_hercu
	int32_t ____hercu_45;
	// System.UInt32 GarbageiOS.M_haltiKose165::_whelvall
	uint32_t ____whelvall_46;
	// System.Int32 GarbageiOS.M_haltiKose165::_lempiworQelcar
	int32_t ____lempiworQelcar_47;
	// System.String GarbageiOS.M_haltiKose165::_senar
	String_t* ____senar_48;
	// System.String GarbageiOS.M_haltiKose165::_lasostouRassir
	String_t* ____lasostouRassir_49;
	// System.Single GarbageiOS.M_haltiKose165::_fawcheRevoo
	float ____fawcheRevoo_50;
	// System.Int32 GarbageiOS.M_haltiKose165::_fourapeTrearrel
	int32_t ____fourapeTrearrel_51;
	// System.Boolean GarbageiOS.M_haltiKose165::_firbai
	bool ____firbai_52;
	// System.String GarbageiOS.M_haltiKose165::_druke
	String_t* ____druke_53;
	// System.String GarbageiOS.M_haltiKose165::_nuyifeRawge
	String_t* ____nuyifeRawge_54;
	// System.Int32 GarbageiOS.M_haltiKose165::_stooma
	int32_t ____stooma_55;
	// System.Int32 GarbageiOS.M_haltiKose165::_gawmenerFawkiru
	int32_t ____gawmenerFawkiru_56;
	// System.String GarbageiOS.M_haltiKose165::_pelbowdereTowpow
	String_t* ____pelbowdereTowpow_57;

public:
	inline static int32_t get_offset_of__pearjedo_0() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____pearjedo_0)); }
	inline String_t* get__pearjedo_0() const { return ____pearjedo_0; }
	inline String_t** get_address_of__pearjedo_0() { return &____pearjedo_0; }
	inline void set__pearjedo_0(String_t* value)
	{
		____pearjedo_0 = value;
		Il2CppCodeGenWriteBarrier(&____pearjedo_0, value);
	}

	inline static int32_t get_offset_of__sucuWoupoo_1() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____sucuWoupoo_1)); }
	inline float get__sucuWoupoo_1() const { return ____sucuWoupoo_1; }
	inline float* get_address_of__sucuWoupoo_1() { return &____sucuWoupoo_1; }
	inline void set__sucuWoupoo_1(float value)
	{
		____sucuWoupoo_1 = value;
	}

	inline static int32_t get_offset_of__chepecirMalmasne_2() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____chepecirMalmasne_2)); }
	inline uint32_t get__chepecirMalmasne_2() const { return ____chepecirMalmasne_2; }
	inline uint32_t* get_address_of__chepecirMalmasne_2() { return &____chepecirMalmasne_2; }
	inline void set__chepecirMalmasne_2(uint32_t value)
	{
		____chepecirMalmasne_2 = value;
	}

	inline static int32_t get_offset_of__lawai_3() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____lawai_3)); }
	inline int32_t get__lawai_3() const { return ____lawai_3; }
	inline int32_t* get_address_of__lawai_3() { return &____lawai_3; }
	inline void set__lawai_3(int32_t value)
	{
		____lawai_3 = value;
	}

	inline static int32_t get_offset_of__nachalHafay_4() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____nachalHafay_4)); }
	inline bool get__nachalHafay_4() const { return ____nachalHafay_4; }
	inline bool* get_address_of__nachalHafay_4() { return &____nachalHafay_4; }
	inline void set__nachalHafay_4(bool value)
	{
		____nachalHafay_4 = value;
	}

	inline static int32_t get_offset_of__ferexani_5() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____ferexani_5)); }
	inline float get__ferexani_5() const { return ____ferexani_5; }
	inline float* get_address_of__ferexani_5() { return &____ferexani_5; }
	inline void set__ferexani_5(float value)
	{
		____ferexani_5 = value;
	}

	inline static int32_t get_offset_of__kursem_6() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____kursem_6)); }
	inline bool get__kursem_6() const { return ____kursem_6; }
	inline bool* get_address_of__kursem_6() { return &____kursem_6; }
	inline void set__kursem_6(bool value)
	{
		____kursem_6 = value;
	}

	inline static int32_t get_offset_of__rirbelsu_7() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____rirbelsu_7)); }
	inline bool get__rirbelsu_7() const { return ____rirbelsu_7; }
	inline bool* get_address_of__rirbelsu_7() { return &____rirbelsu_7; }
	inline void set__rirbelsu_7(bool value)
	{
		____rirbelsu_7 = value;
	}

	inline static int32_t get_offset_of__rairtear_8() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____rairtear_8)); }
	inline uint32_t get__rairtear_8() const { return ____rairtear_8; }
	inline uint32_t* get_address_of__rairtear_8() { return &____rairtear_8; }
	inline void set__rairtear_8(uint32_t value)
	{
		____rairtear_8 = value;
	}

	inline static int32_t get_offset_of__teegaSaste_9() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____teegaSaste_9)); }
	inline int32_t get__teegaSaste_9() const { return ____teegaSaste_9; }
	inline int32_t* get_address_of__teegaSaste_9() { return &____teegaSaste_9; }
	inline void set__teegaSaste_9(int32_t value)
	{
		____teegaSaste_9 = value;
	}

	inline static int32_t get_offset_of__nearmoyurDruxider_10() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____nearmoyurDruxider_10)); }
	inline int32_t get__nearmoyurDruxider_10() const { return ____nearmoyurDruxider_10; }
	inline int32_t* get_address_of__nearmoyurDruxider_10() { return &____nearmoyurDruxider_10; }
	inline void set__nearmoyurDruxider_10(int32_t value)
	{
		____nearmoyurDruxider_10 = value;
	}

	inline static int32_t get_offset_of__hatewow_11() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____hatewow_11)); }
	inline bool get__hatewow_11() const { return ____hatewow_11; }
	inline bool* get_address_of__hatewow_11() { return &____hatewow_11; }
	inline void set__hatewow_11(bool value)
	{
		____hatewow_11 = value;
	}

	inline static int32_t get_offset_of__mitelna_12() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____mitelna_12)); }
	inline String_t* get__mitelna_12() const { return ____mitelna_12; }
	inline String_t** get_address_of__mitelna_12() { return &____mitelna_12; }
	inline void set__mitelna_12(String_t* value)
	{
		____mitelna_12 = value;
		Il2CppCodeGenWriteBarrier(&____mitelna_12, value);
	}

	inline static int32_t get_offset_of__drelnooHowjar_13() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____drelnooHowjar_13)); }
	inline String_t* get__drelnooHowjar_13() const { return ____drelnooHowjar_13; }
	inline String_t** get_address_of__drelnooHowjar_13() { return &____drelnooHowjar_13; }
	inline void set__drelnooHowjar_13(String_t* value)
	{
		____drelnooHowjar_13 = value;
		Il2CppCodeGenWriteBarrier(&____drelnooHowjar_13, value);
	}

	inline static int32_t get_offset_of__whoucem_14() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____whoucem_14)); }
	inline bool get__whoucem_14() const { return ____whoucem_14; }
	inline bool* get_address_of__whoucem_14() { return &____whoucem_14; }
	inline void set__whoucem_14(bool value)
	{
		____whoucem_14 = value;
	}

	inline static int32_t get_offset_of__wepemBijer_15() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____wepemBijer_15)); }
	inline bool get__wepemBijer_15() const { return ____wepemBijer_15; }
	inline bool* get_address_of__wepemBijer_15() { return &____wepemBijer_15; }
	inline void set__wepemBijer_15(bool value)
	{
		____wepemBijer_15 = value;
	}

	inline static int32_t get_offset_of__lori_16() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____lori_16)); }
	inline String_t* get__lori_16() const { return ____lori_16; }
	inline String_t** get_address_of__lori_16() { return &____lori_16; }
	inline void set__lori_16(String_t* value)
	{
		____lori_16 = value;
		Il2CppCodeGenWriteBarrier(&____lori_16, value);
	}

	inline static int32_t get_offset_of__napalPelkeemo_17() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____napalPelkeemo_17)); }
	inline float get__napalPelkeemo_17() const { return ____napalPelkeemo_17; }
	inline float* get_address_of__napalPelkeemo_17() { return &____napalPelkeemo_17; }
	inline void set__napalPelkeemo_17(float value)
	{
		____napalPelkeemo_17 = value;
	}

	inline static int32_t get_offset_of__rooboCawbawder_18() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____rooboCawbawder_18)); }
	inline String_t* get__rooboCawbawder_18() const { return ____rooboCawbawder_18; }
	inline String_t** get_address_of__rooboCawbawder_18() { return &____rooboCawbawder_18; }
	inline void set__rooboCawbawder_18(String_t* value)
	{
		____rooboCawbawder_18 = value;
		Il2CppCodeGenWriteBarrier(&____rooboCawbawder_18, value);
	}

	inline static int32_t get_offset_of__kalpepor_19() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____kalpepor_19)); }
	inline int32_t get__kalpepor_19() const { return ____kalpepor_19; }
	inline int32_t* get_address_of__kalpepor_19() { return &____kalpepor_19; }
	inline void set__kalpepor_19(int32_t value)
	{
		____kalpepor_19 = value;
	}

	inline static int32_t get_offset_of__sarallTefoo_20() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____sarallTefoo_20)); }
	inline bool get__sarallTefoo_20() const { return ____sarallTefoo_20; }
	inline bool* get_address_of__sarallTefoo_20() { return &____sarallTefoo_20; }
	inline void set__sarallTefoo_20(bool value)
	{
		____sarallTefoo_20 = value;
	}

	inline static int32_t get_offset_of__hurnasqoVerjorxere_21() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____hurnasqoVerjorxere_21)); }
	inline String_t* get__hurnasqoVerjorxere_21() const { return ____hurnasqoVerjorxere_21; }
	inline String_t** get_address_of__hurnasqoVerjorxere_21() { return &____hurnasqoVerjorxere_21; }
	inline void set__hurnasqoVerjorxere_21(String_t* value)
	{
		____hurnasqoVerjorxere_21 = value;
		Il2CppCodeGenWriteBarrier(&____hurnasqoVerjorxere_21, value);
	}

	inline static int32_t get_offset_of__seejaDuze_22() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____seejaDuze_22)); }
	inline int32_t get__seejaDuze_22() const { return ____seejaDuze_22; }
	inline int32_t* get_address_of__seejaDuze_22() { return &____seejaDuze_22; }
	inline void set__seejaDuze_22(int32_t value)
	{
		____seejaDuze_22 = value;
	}

	inline static int32_t get_offset_of__tarqere_23() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____tarqere_23)); }
	inline uint32_t get__tarqere_23() const { return ____tarqere_23; }
	inline uint32_t* get_address_of__tarqere_23() { return &____tarqere_23; }
	inline void set__tarqere_23(uint32_t value)
	{
		____tarqere_23 = value;
	}

	inline static int32_t get_offset_of__mitrumerJemle_24() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____mitrumerJemle_24)); }
	inline uint32_t get__mitrumerJemle_24() const { return ____mitrumerJemle_24; }
	inline uint32_t* get_address_of__mitrumerJemle_24() { return &____mitrumerJemle_24; }
	inline void set__mitrumerJemle_24(uint32_t value)
	{
		____mitrumerJemle_24 = value;
	}

	inline static int32_t get_offset_of__dosorSedee_25() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____dosorSedee_25)); }
	inline String_t* get__dosorSedee_25() const { return ____dosorSedee_25; }
	inline String_t** get_address_of__dosorSedee_25() { return &____dosorSedee_25; }
	inline void set__dosorSedee_25(String_t* value)
	{
		____dosorSedee_25 = value;
		Il2CppCodeGenWriteBarrier(&____dosorSedee_25, value);
	}

	inline static int32_t get_offset_of__sahukallChasja_26() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____sahukallChasja_26)); }
	inline float get__sahukallChasja_26() const { return ____sahukallChasja_26; }
	inline float* get_address_of__sahukallChasja_26() { return &____sahukallChasja_26; }
	inline void set__sahukallChasja_26(float value)
	{
		____sahukallChasja_26 = value;
	}

	inline static int32_t get_offset_of__maychu_27() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____maychu_27)); }
	inline int32_t get__maychu_27() const { return ____maychu_27; }
	inline int32_t* get_address_of__maychu_27() { return &____maychu_27; }
	inline void set__maychu_27(int32_t value)
	{
		____maychu_27 = value;
	}

	inline static int32_t get_offset_of__zookiHistrelxur_28() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____zookiHistrelxur_28)); }
	inline float get__zookiHistrelxur_28() const { return ____zookiHistrelxur_28; }
	inline float* get_address_of__zookiHistrelxur_28() { return &____zookiHistrelxur_28; }
	inline void set__zookiHistrelxur_28(float value)
	{
		____zookiHistrelxur_28 = value;
	}

	inline static int32_t get_offset_of__sicorjayGaijawsem_29() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____sicorjayGaijawsem_29)); }
	inline float get__sicorjayGaijawsem_29() const { return ____sicorjayGaijawsem_29; }
	inline float* get_address_of__sicorjayGaijawsem_29() { return &____sicorjayGaijawsem_29; }
	inline void set__sicorjayGaijawsem_29(float value)
	{
		____sicorjayGaijawsem_29 = value;
	}

	inline static int32_t get_offset_of__sowchearLarcemxal_30() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____sowchearLarcemxal_30)); }
	inline bool get__sowchearLarcemxal_30() const { return ____sowchearLarcemxal_30; }
	inline bool* get_address_of__sowchearLarcemxal_30() { return &____sowchearLarcemxal_30; }
	inline void set__sowchearLarcemxal_30(bool value)
	{
		____sowchearLarcemxal_30 = value;
	}

	inline static int32_t get_offset_of__cernairMuhegaw_31() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____cernairMuhegaw_31)); }
	inline int32_t get__cernairMuhegaw_31() const { return ____cernairMuhegaw_31; }
	inline int32_t* get_address_of__cernairMuhegaw_31() { return &____cernairMuhegaw_31; }
	inline void set__cernairMuhegaw_31(int32_t value)
	{
		____cernairMuhegaw_31 = value;
	}

	inline static int32_t get_offset_of__ralcekairWhificas_32() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____ralcekairWhificas_32)); }
	inline uint32_t get__ralcekairWhificas_32() const { return ____ralcekairWhificas_32; }
	inline uint32_t* get_address_of__ralcekairWhificas_32() { return &____ralcekairWhificas_32; }
	inline void set__ralcekairWhificas_32(uint32_t value)
	{
		____ralcekairWhificas_32 = value;
	}

	inline static int32_t get_offset_of__nara_33() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____nara_33)); }
	inline int32_t get__nara_33() const { return ____nara_33; }
	inline int32_t* get_address_of__nara_33() { return &____nara_33; }
	inline void set__nara_33(int32_t value)
	{
		____nara_33 = value;
	}

	inline static int32_t get_offset_of__marrelbel_34() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____marrelbel_34)); }
	inline int32_t get__marrelbel_34() const { return ____marrelbel_34; }
	inline int32_t* get_address_of__marrelbel_34() { return &____marrelbel_34; }
	inline void set__marrelbel_34(int32_t value)
	{
		____marrelbel_34 = value;
	}

	inline static int32_t get_offset_of__wurhasmay_35() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____wurhasmay_35)); }
	inline String_t* get__wurhasmay_35() const { return ____wurhasmay_35; }
	inline String_t** get_address_of__wurhasmay_35() { return &____wurhasmay_35; }
	inline void set__wurhasmay_35(String_t* value)
	{
		____wurhasmay_35 = value;
		Il2CppCodeGenWriteBarrier(&____wurhasmay_35, value);
	}

	inline static int32_t get_offset_of__temcar_36() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____temcar_36)); }
	inline float get__temcar_36() const { return ____temcar_36; }
	inline float* get_address_of__temcar_36() { return &____temcar_36; }
	inline void set__temcar_36(float value)
	{
		____temcar_36 = value;
	}

	inline static int32_t get_offset_of__wepiNishaybis_37() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____wepiNishaybis_37)); }
	inline uint32_t get__wepiNishaybis_37() const { return ____wepiNishaybis_37; }
	inline uint32_t* get_address_of__wepiNishaybis_37() { return &____wepiNishaybis_37; }
	inline void set__wepiNishaybis_37(uint32_t value)
	{
		____wepiNishaybis_37 = value;
	}

	inline static int32_t get_offset_of__pirfemWhawrearwhu_38() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____pirfemWhawrearwhu_38)); }
	inline bool get__pirfemWhawrearwhu_38() const { return ____pirfemWhawrearwhu_38; }
	inline bool* get_address_of__pirfemWhawrearwhu_38() { return &____pirfemWhawrearwhu_38; }
	inline void set__pirfemWhawrearwhu_38(bool value)
	{
		____pirfemWhawrearwhu_38 = value;
	}

	inline static int32_t get_offset_of__saryurWemqearcis_39() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____saryurWemqearcis_39)); }
	inline bool get__saryurWemqearcis_39() const { return ____saryurWemqearcis_39; }
	inline bool* get_address_of__saryurWemqearcis_39() { return &____saryurWemqearcis_39; }
	inline void set__saryurWemqearcis_39(bool value)
	{
		____saryurWemqearcis_39 = value;
	}

	inline static int32_t get_offset_of__mirchidiWalsall_40() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____mirchidiWalsall_40)); }
	inline int32_t get__mirchidiWalsall_40() const { return ____mirchidiWalsall_40; }
	inline int32_t* get_address_of__mirchidiWalsall_40() { return &____mirchidiWalsall_40; }
	inline void set__mirchidiWalsall_40(int32_t value)
	{
		____mirchidiWalsall_40 = value;
	}

	inline static int32_t get_offset_of__narair_41() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____narair_41)); }
	inline int32_t get__narair_41() const { return ____narair_41; }
	inline int32_t* get_address_of__narair_41() { return &____narair_41; }
	inline void set__narair_41(int32_t value)
	{
		____narair_41 = value;
	}

	inline static int32_t get_offset_of__ritoutroKehay_42() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____ritoutroKehay_42)); }
	inline float get__ritoutroKehay_42() const { return ____ritoutroKehay_42; }
	inline float* get_address_of__ritoutroKehay_42() { return &____ritoutroKehay_42; }
	inline void set__ritoutroKehay_42(float value)
	{
		____ritoutroKehay_42 = value;
	}

	inline static int32_t get_offset_of__bearyis_43() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____bearyis_43)); }
	inline uint32_t get__bearyis_43() const { return ____bearyis_43; }
	inline uint32_t* get_address_of__bearyis_43() { return &____bearyis_43; }
	inline void set__bearyis_43(uint32_t value)
	{
		____bearyis_43 = value;
	}

	inline static int32_t get_offset_of__numeHateemur_44() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____numeHateemur_44)); }
	inline bool get__numeHateemur_44() const { return ____numeHateemur_44; }
	inline bool* get_address_of__numeHateemur_44() { return &____numeHateemur_44; }
	inline void set__numeHateemur_44(bool value)
	{
		____numeHateemur_44 = value;
	}

	inline static int32_t get_offset_of__hercu_45() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____hercu_45)); }
	inline int32_t get__hercu_45() const { return ____hercu_45; }
	inline int32_t* get_address_of__hercu_45() { return &____hercu_45; }
	inline void set__hercu_45(int32_t value)
	{
		____hercu_45 = value;
	}

	inline static int32_t get_offset_of__whelvall_46() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____whelvall_46)); }
	inline uint32_t get__whelvall_46() const { return ____whelvall_46; }
	inline uint32_t* get_address_of__whelvall_46() { return &____whelvall_46; }
	inline void set__whelvall_46(uint32_t value)
	{
		____whelvall_46 = value;
	}

	inline static int32_t get_offset_of__lempiworQelcar_47() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____lempiworQelcar_47)); }
	inline int32_t get__lempiworQelcar_47() const { return ____lempiworQelcar_47; }
	inline int32_t* get_address_of__lempiworQelcar_47() { return &____lempiworQelcar_47; }
	inline void set__lempiworQelcar_47(int32_t value)
	{
		____lempiworQelcar_47 = value;
	}

	inline static int32_t get_offset_of__senar_48() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____senar_48)); }
	inline String_t* get__senar_48() const { return ____senar_48; }
	inline String_t** get_address_of__senar_48() { return &____senar_48; }
	inline void set__senar_48(String_t* value)
	{
		____senar_48 = value;
		Il2CppCodeGenWriteBarrier(&____senar_48, value);
	}

	inline static int32_t get_offset_of__lasostouRassir_49() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____lasostouRassir_49)); }
	inline String_t* get__lasostouRassir_49() const { return ____lasostouRassir_49; }
	inline String_t** get_address_of__lasostouRassir_49() { return &____lasostouRassir_49; }
	inline void set__lasostouRassir_49(String_t* value)
	{
		____lasostouRassir_49 = value;
		Il2CppCodeGenWriteBarrier(&____lasostouRassir_49, value);
	}

	inline static int32_t get_offset_of__fawcheRevoo_50() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____fawcheRevoo_50)); }
	inline float get__fawcheRevoo_50() const { return ____fawcheRevoo_50; }
	inline float* get_address_of__fawcheRevoo_50() { return &____fawcheRevoo_50; }
	inline void set__fawcheRevoo_50(float value)
	{
		____fawcheRevoo_50 = value;
	}

	inline static int32_t get_offset_of__fourapeTrearrel_51() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____fourapeTrearrel_51)); }
	inline int32_t get__fourapeTrearrel_51() const { return ____fourapeTrearrel_51; }
	inline int32_t* get_address_of__fourapeTrearrel_51() { return &____fourapeTrearrel_51; }
	inline void set__fourapeTrearrel_51(int32_t value)
	{
		____fourapeTrearrel_51 = value;
	}

	inline static int32_t get_offset_of__firbai_52() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____firbai_52)); }
	inline bool get__firbai_52() const { return ____firbai_52; }
	inline bool* get_address_of__firbai_52() { return &____firbai_52; }
	inline void set__firbai_52(bool value)
	{
		____firbai_52 = value;
	}

	inline static int32_t get_offset_of__druke_53() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____druke_53)); }
	inline String_t* get__druke_53() const { return ____druke_53; }
	inline String_t** get_address_of__druke_53() { return &____druke_53; }
	inline void set__druke_53(String_t* value)
	{
		____druke_53 = value;
		Il2CppCodeGenWriteBarrier(&____druke_53, value);
	}

	inline static int32_t get_offset_of__nuyifeRawge_54() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____nuyifeRawge_54)); }
	inline String_t* get__nuyifeRawge_54() const { return ____nuyifeRawge_54; }
	inline String_t** get_address_of__nuyifeRawge_54() { return &____nuyifeRawge_54; }
	inline void set__nuyifeRawge_54(String_t* value)
	{
		____nuyifeRawge_54 = value;
		Il2CppCodeGenWriteBarrier(&____nuyifeRawge_54, value);
	}

	inline static int32_t get_offset_of__stooma_55() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____stooma_55)); }
	inline int32_t get__stooma_55() const { return ____stooma_55; }
	inline int32_t* get_address_of__stooma_55() { return &____stooma_55; }
	inline void set__stooma_55(int32_t value)
	{
		____stooma_55 = value;
	}

	inline static int32_t get_offset_of__gawmenerFawkiru_56() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____gawmenerFawkiru_56)); }
	inline int32_t get__gawmenerFawkiru_56() const { return ____gawmenerFawkiru_56; }
	inline int32_t* get_address_of__gawmenerFawkiru_56() { return &____gawmenerFawkiru_56; }
	inline void set__gawmenerFawkiru_56(int32_t value)
	{
		____gawmenerFawkiru_56 = value;
	}

	inline static int32_t get_offset_of__pelbowdereTowpow_57() { return static_cast<int32_t>(offsetof(M_haltiKose165_t1903042594, ____pelbowdereTowpow_57)); }
	inline String_t* get__pelbowdereTowpow_57() const { return ____pelbowdereTowpow_57; }
	inline String_t** get_address_of__pelbowdereTowpow_57() { return &____pelbowdereTowpow_57; }
	inline void set__pelbowdereTowpow_57(String_t* value)
	{
		____pelbowdereTowpow_57 = value;
		Il2CppCodeGenWriteBarrier(&____pelbowdereTowpow_57, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
