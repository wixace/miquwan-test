﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_renercerMoorow232
struct M_renercerMoorow232_t1060498076;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_renercerMoorow232::.ctor()
extern "C"  void M_renercerMoorow232__ctor_m4279619143 (M_renercerMoorow232_t1060498076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_renercerMoorow232::M_cortor0(System.String[],System.Int32)
extern "C"  void M_renercerMoorow232_M_cortor0_m3984654537 (M_renercerMoorow232_t1060498076 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_renercerMoorow232::M_revalLesallhi1(System.String[],System.Int32)
extern "C"  void M_renercerMoorow232_M_revalLesallhi1_m1764802401 (M_renercerMoorow232_t1060498076 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
