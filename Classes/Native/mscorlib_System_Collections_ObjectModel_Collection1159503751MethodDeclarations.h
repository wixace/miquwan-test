﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>
struct Collection_1_t1159503751;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.Int2[]
struct Int2U5BU5D_t30096868;
// System.Collections.Generic.IEnumerator`1<Pathfinding.Int2>
struct IEnumerator_1_t3885910642;
// System.Collections.Generic.IList`1<Pathfinding.Int2>
struct IList_1_t373725500;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"

// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::.ctor()
extern "C"  void Collection_1__ctor_m852604436_gshared (Collection_1_t1159503751 * __this, const MethodInfo* method);
#define Collection_1__ctor_m852604436(__this, method) ((  void (*) (Collection_1_t1159503751 *, const MethodInfo*))Collection_1__ctor_m852604436_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2920331555_gshared (Collection_1_t1159503751 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2920331555(__this, method) ((  bool (*) (Collection_1_t1159503751 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2920331555_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m1251079600_gshared (Collection_1_t1159503751 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1251079600(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1159503751 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1251079600_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1463470399_gshared (Collection_1_t1159503751 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1463470399(__this, method) ((  Il2CppObject * (*) (Collection_1_t1159503751 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1463470399_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m649320926_gshared (Collection_1_t1159503751 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m649320926(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1159503751 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m649320926_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m2100652962_gshared (Collection_1_t1159503751 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m2100652962(__this, ___value0, method) ((  bool (*) (Collection_1_t1159503751 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m2100652962_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1117253942_gshared (Collection_1_t1159503751 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1117253942(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1159503751 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1117253942_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m1225670825_gshared (Collection_1_t1159503751 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1225670825(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1159503751 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1225670825_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1555859935_gshared (Collection_1_t1159503751 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1555859935(__this, ___value0, method) ((  void (*) (Collection_1_t1159503751 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1555859935_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m2873864314_gshared (Collection_1_t1159503751 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2873864314(__this, method) ((  bool (*) (Collection_1_t1159503751 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m2873864314_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2871584428_gshared (Collection_1_t1159503751 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2871584428(__this, method) ((  Il2CppObject * (*) (Collection_1_t1159503751 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2871584428_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m3155623057_gshared (Collection_1_t1159503751 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m3155623057(__this, method) ((  bool (*) (Collection_1_t1159503751 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m3155623057_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m3326749768_gshared (Collection_1_t1159503751 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3326749768(__this, method) ((  bool (*) (Collection_1_t1159503751 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m3326749768_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3289242931_gshared (Collection_1_t1159503751 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3289242931(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1159503751 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3289242931_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1662576192_gshared (Collection_1_t1159503751 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1662576192(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1159503751 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1662576192_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::Add(T)
extern "C"  void Collection_1_Add_m2095855595_gshared (Collection_1_t1159503751 * __this, Int2_t1974045593  ___item0, const MethodInfo* method);
#define Collection_1_Add_m2095855595(__this, ___item0, method) ((  void (*) (Collection_1_t1159503751 *, Int2_t1974045593 , const MethodInfo*))Collection_1_Add_m2095855595_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::Clear()
extern "C"  void Collection_1_Clear_m2553705023_gshared (Collection_1_t1159503751 * __this, const MethodInfo* method);
#define Collection_1_Clear_m2553705023(__this, method) ((  void (*) (Collection_1_t1159503751 *, const MethodInfo*))Collection_1_Clear_m2553705023_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1693302307_gshared (Collection_1_t1159503751 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1693302307(__this, method) ((  void (*) (Collection_1_t1159503751 *, const MethodInfo*))Collection_1_ClearItems_m1693302307_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::Contains(T)
extern "C"  bool Collection_1_Contains_m2518682673_gshared (Collection_1_t1159503751 * __this, Int2_t1974045593  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m2518682673(__this, ___item0, method) ((  bool (*) (Collection_1_t1159503751 *, Int2_t1974045593 , const MethodInfo*))Collection_1_Contains_m2518682673_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3447331611_gshared (Collection_1_t1159503751 * __this, Int2U5BU5D_t30096868* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3447331611(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1159503751 *, Int2U5BU5D_t30096868*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3447331611_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m4002869640_gshared (Collection_1_t1159503751 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m4002869640(__this, method) ((  Il2CppObject* (*) (Collection_1_t1159503751 *, const MethodInfo*))Collection_1_GetEnumerator_m4002869640_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2992594215_gshared (Collection_1_t1159503751 * __this, Int2_t1974045593  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m2992594215(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1159503751 *, Int2_t1974045593 , const MethodInfo*))Collection_1_IndexOf_m2992594215_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3985259602_gshared (Collection_1_t1159503751 * __this, int32_t ___index0, Int2_t1974045593  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3985259602(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1159503751 *, int32_t, Int2_t1974045593 , const MethodInfo*))Collection_1_Insert_m3985259602_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m3427201413_gshared (Collection_1_t1159503751 * __this, int32_t ___index0, Int2_t1974045593  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m3427201413(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1159503751 *, int32_t, Int2_t1974045593 , const MethodInfo*))Collection_1_InsertItem_m3427201413_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m2323839487_gshared (Collection_1_t1159503751 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m2323839487(__this, method) ((  Il2CppObject* (*) (Collection_1_t1159503751 *, const MethodInfo*))Collection_1_get_Items_m2323839487_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::Remove(T)
extern "C"  bool Collection_1_Remove_m3952878700_gshared (Collection_1_t1159503751 * __this, Int2_t1974045593  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m3952878700(__this, ___item0, method) ((  bool (*) (Collection_1_t1159503751 *, Int2_t1974045593 , const MethodInfo*))Collection_1_Remove_m3952878700_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1859112472_gshared (Collection_1_t1159503751 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m1859112472(__this, ___index0, method) ((  void (*) (Collection_1_t1159503751 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1859112472_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2800315448_gshared (Collection_1_t1159503751 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2800315448(__this, ___index0, method) ((  void (*) (Collection_1_t1159503751 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2800315448_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m2393414964_gshared (Collection_1_t1159503751 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m2393414964(__this, method) ((  int32_t (*) (Collection_1_t1159503751 *, const MethodInfo*))Collection_1_get_Count_m2393414964_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::get_Item(System.Int32)
extern "C"  Int2_t1974045593  Collection_1_get_Item_m1437994622_gshared (Collection_1_t1159503751 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m1437994622(__this, ___index0, method) ((  Int2_t1974045593  (*) (Collection_1_t1159503751 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1437994622_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m533246121_gshared (Collection_1_t1159503751 * __this, int32_t ___index0, Int2_t1974045593  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m533246121(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1159503751 *, int32_t, Int2_t1974045593 , const MethodInfo*))Collection_1_set_Item_m533246121_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m305585840_gshared (Collection_1_t1159503751 * __this, int32_t ___index0, Int2_t1974045593  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m305585840(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1159503751 *, int32_t, Int2_t1974045593 , const MethodInfo*))Collection_1_SetItem_m305585840_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m477257179_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m477257179(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m477257179_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::ConvertItem(System.Object)
extern "C"  Int2_t1974045593  Collection_1_ConvertItem_m2854768221_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m2854768221(__this /* static, unused */, ___item0, method) ((  Int2_t1974045593  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m2854768221_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m2526854747_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m2526854747(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m2526854747_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m636720137_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m636720137(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m636720137_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Int2>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m203773750_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m203773750(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m203773750_gshared)(__this /* static, unused */, ___list0, method)
