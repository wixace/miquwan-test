﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PostEffectBase
struct PostEffectBase_t3382443234;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.Shader
struct Shader_t3191267369;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Shader3191267369.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "AssemblyU2DCSharp_PostEffectBase3382443234.h"

// System.Void PostEffectBase::.ctor()
extern "C"  void PostEffectBase__ctor_m789261817 (PostEffectBase_t3382443234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PostEffectBase::CheckResources()
extern "C"  void PostEffectBase_CheckResources_m4251485800 (PostEffectBase_t3382443234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PostEffectBase::CheckSupport()
extern "C"  bool PostEffectBase_CheckSupport_m392624582 (PostEffectBase_t3382443234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PostEffectBase::NotSupproted()
extern "C"  void PostEffectBase_NotSupproted_m1349002860 (PostEffectBase_t3382443234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PostEffectBase::Start()
extern "C"  void PostEffectBase_Start_m4031366905 (PostEffectBase_t3382443234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material PostEffectBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern "C"  Material_t3870600107 * PostEffectBase_CheckShaderAndCreateMaterial_m2257916013 (PostEffectBase_t3382443234 * __this, Shader_t3191267369 * ___shader0, Material_t3870600107 * ___material1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PostEffectBase::ilo_CheckSupport1(PostEffectBase)
extern "C"  bool PostEffectBase_ilo_CheckSupport1_m35001048 (Il2CppObject * __this /* static, unused */, PostEffectBase_t3382443234 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
