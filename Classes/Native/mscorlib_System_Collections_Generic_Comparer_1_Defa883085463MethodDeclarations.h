﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>
struct DefaultComparer_t883085463;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_MeshSubsetCombineUtility_S2374839686.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::.ctor()
extern "C"  void DefaultComparer__ctor_m476051532_gshared (DefaultComparer_t883085463 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m476051532(__this, method) ((  void (*) (DefaultComparer_t883085463 *, const MethodInfo*))DefaultComparer__ctor_m476051532_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3358286251_gshared (DefaultComparer_t883085463 * __this, SubMeshInstance_t2374839686  ___x0, SubMeshInstance_t2374839686  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m3358286251(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t883085463 *, SubMeshInstance_t2374839686 , SubMeshInstance_t2374839686 , const MethodInfo*))DefaultComparer_Compare_m3358286251_gshared)(__this, ___x0, ___y1, method)
