﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ReplayHeroUnit[]
struct ReplayHeroUnitU5BU5D_t676085128;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayOtherPlayer
struct  ReplayOtherPlayer_t1774842762  : public Il2CppObject
{
public:
	// System.UInt32 ReplayOtherPlayer::roleId
	uint32_t ___roleId_0;
	// System.String ReplayOtherPlayer::name
	String_t* ___name_1;
	// System.UInt32 ReplayOtherPlayer::power
	uint32_t ___power_2;
	// ReplayHeroUnit[] ReplayOtherPlayer::heroInfoList
	ReplayHeroUnitU5BU5D_t676085128* ___heroInfoList_3;

public:
	inline static int32_t get_offset_of_roleId_0() { return static_cast<int32_t>(offsetof(ReplayOtherPlayer_t1774842762, ___roleId_0)); }
	inline uint32_t get_roleId_0() const { return ___roleId_0; }
	inline uint32_t* get_address_of_roleId_0() { return &___roleId_0; }
	inline void set_roleId_0(uint32_t value)
	{
		___roleId_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(ReplayOtherPlayer_t1774842762, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_power_2() { return static_cast<int32_t>(offsetof(ReplayOtherPlayer_t1774842762, ___power_2)); }
	inline uint32_t get_power_2() const { return ___power_2; }
	inline uint32_t* get_address_of_power_2() { return &___power_2; }
	inline void set_power_2(uint32_t value)
	{
		___power_2 = value;
	}

	inline static int32_t get_offset_of_heroInfoList_3() { return static_cast<int32_t>(offsetof(ReplayOtherPlayer_t1774842762, ___heroInfoList_3)); }
	inline ReplayHeroUnitU5BU5D_t676085128* get_heroInfoList_3() const { return ___heroInfoList_3; }
	inline ReplayHeroUnitU5BU5D_t676085128** get_address_of_heroInfoList_3() { return &___heroInfoList_3; }
	inline void set_heroInfoList_3(ReplayHeroUnitU5BU5D_t676085128* value)
	{
		___heroInfoList_3 = value;
		Il2CppCodeGenWriteBarrier(&___heroInfoList_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
