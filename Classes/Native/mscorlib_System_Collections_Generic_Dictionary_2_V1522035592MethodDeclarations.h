﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>
struct Dictionary_2_t3590202184;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1522035592.h"
#include "AssemblyU2DCSharp_SoundStatus3592938945.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,SoundStatus>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m77674419_gshared (Enumerator_t1522035592 * __this, Dictionary_2_t3590202184 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m77674419(__this, ___host0, method) ((  void (*) (Enumerator_t1522035592 *, Dictionary_2_t3590202184 *, const MethodInfo*))Enumerator__ctor_m77674419_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,SoundStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1478057432_gshared (Enumerator_t1522035592 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1478057432(__this, method) ((  Il2CppObject * (*) (Enumerator_t1522035592 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1478057432_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,SoundStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4100689698_gshared (Enumerator_t1522035592 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4100689698(__this, method) ((  void (*) (Enumerator_t1522035592 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4100689698_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,SoundStatus>::Dispose()
extern "C"  void Enumerator_Dispose_m858595605_gshared (Enumerator_t1522035592 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m858595605(__this, method) ((  void (*) (Enumerator_t1522035592 *, const MethodInfo*))Enumerator_Dispose_m858595605_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,SoundStatus>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3188375634_gshared (Enumerator_t1522035592 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3188375634(__this, method) ((  bool (*) (Enumerator_t1522035592 *, const MethodInfo*))Enumerator_MoveNext_m3188375634_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,SoundStatus>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m4110158232_gshared (Enumerator_t1522035592 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4110158232(__this, method) ((  int32_t (*) (Enumerator_t1522035592 *, const MethodInfo*))Enumerator_get_Current_m4110158232_gshared)(__this, method)
