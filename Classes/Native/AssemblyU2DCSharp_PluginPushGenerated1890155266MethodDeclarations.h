﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginPushGenerated
struct PluginPushGenerated_t1890155266;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// PluginPush
struct PluginPush_t1606327565;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_PluginPush1606327565.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void PluginPushGenerated::.ctor()
extern "C"  void PluginPushGenerated__ctor_m293722793 (PluginPushGenerated_t1890155266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginPushGenerated::PluginPush_PluginPush1(JSVCall,System.Int32)
extern "C"  bool PluginPushGenerated_PluginPush_PluginPush1_m2736173257 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginPushGenerated::PluginPush_GetPushState__String__PushType(JSVCall,System.Int32)
extern "C"  bool PluginPushGenerated_PluginPush_GetPushState__String__PushType_m1715652387 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginPushGenerated::PluginPush_Init(JSVCall,System.Int32)
extern "C"  bool PluginPushGenerated_PluginPush_Init_m3821648589 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginPushGenerated::PluginPush_OnDestory(JSVCall,System.Int32)
extern "C"  bool PluginPushGenerated_PluginPush_OnDestory_m3574795354 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginPushGenerated::PluginPush_PushLogin__String(JSVCall,System.Int32)
extern "C"  bool PluginPushGenerated_PluginPush_PushLogin__String_m4120955237 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginPushGenerated::PluginPush_RequiredInit(JSVCall,System.Int32)
extern "C"  bool PluginPushGenerated_PluginPush_RequiredInit_m3215769036 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginPushGenerated::PluginPush_ResumePush(JSVCall,System.Int32)
extern "C"  bool PluginPushGenerated_PluginPush_ResumePush_m3177490020 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginPushGenerated::PluginPush_SendNotification__String__PushType__Int32__Single__String__String__Boolean__String(JSVCall,System.Int32)
extern "C"  bool PluginPushGenerated_PluginPush_SendNotification__String__PushType__Int32__Single__String__String__Boolean__String_m3307404630 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginPushGenerated::PluginPush_SetAliasAndTags__String__String(JSVCall,System.Int32)
extern "C"  bool PluginPushGenerated_PluginPush_SetAliasAndTags__String__String_m673147049 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginPushGenerated::PluginPush_SettingPushState__String__PushType__Boolean(JSVCall,System.Int32)
extern "C"  bool PluginPushGenerated_PluginPush_SettingPushState__String__PushType__Boolean_m1016845793 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginPushGenerated::PluginPush_StopPush(JSVCall,System.Int32)
extern "C"  bool PluginPushGenerated_PluginPush_StopPush_m4014018809 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginPushGenerated::PluginPush_NotificationMessage__String__DateTime__Boolean(JSVCall,System.Int32)
extern "C"  bool PluginPushGenerated_PluginPush_NotificationMessage__String__DateTime__Boolean_m2191349725 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginPushGenerated::PluginPush_NotificationMessage__String__String__Boolean(JSVCall,System.Int32)
extern "C"  bool PluginPushGenerated_PluginPush_NotificationMessage__String__String__Boolean_m1502147687 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPushGenerated::__Register()
extern "C"  void PluginPushGenerated___Register_m1033561022 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginPushGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool PluginPushGenerated_ilo_attachFinalizerObject1_m3012755054 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginPushGenerated::ilo_getStringS2(System.Int32)
extern "C"  String_t* PluginPushGenerated_ilo_getStringS2_m2711721166 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PluginPushGenerated::ilo_getEnum3(System.Int32)
extern "C"  int32_t PluginPushGenerated_ilo_getEnum3_m2306442989 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPushGenerated::ilo_setBooleanS4(System.Int32,System.Boolean)
extern "C"  void PluginPushGenerated_ilo_setBooleanS4_m3209099575 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPushGenerated::ilo_OnDestory5(PluginPush)
extern "C"  void PluginPushGenerated_ilo_OnDestory5_m3456099745 (Il2CppObject * __this /* static, unused */, PluginPush_t1606327565 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPushGenerated::ilo_ResumePush6(PluginPush)
extern "C"  void PluginPushGenerated_ilo_ResumePush6_m3123315062 (Il2CppObject * __this /* static, unused */, PluginPush_t1606327565 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PluginPushGenerated::ilo_getSingle7(System.Int32)
extern "C"  float PluginPushGenerated_ilo_getSingle7_m2144603276 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginPushGenerated::ilo_getBooleanS8(System.Int32)
extern "C"  bool PluginPushGenerated_ilo_getBooleanS8_m1190886882 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPushGenerated::ilo_StopPush9(PluginPush)
extern "C"  void PluginPushGenerated_ilo_StopPush9_m892553860 (Il2CppObject * __this /* static, unused */, PluginPush_t1606327565 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PluginPushGenerated::ilo_getObject10(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * PluginPushGenerated_ilo_getObject10_m119639844 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
