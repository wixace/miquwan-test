﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`2<System.Boolean,System.Boolean>
struct Func_2_t3133160109;
// OnVoidDelegate
struct OnVoidDelegate_t2787120856;
// AstarPath/AstarWorkItem
struct AstarWorkItem_t2566693888;
struct AstarWorkItem_t2566693888_marshaled_pinvoke;
struct AstarWorkItem_t2566693888_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AstarPath_AstarWorkItem2566693888.h"
#include "AssemblyU2DCSharp_OnVoidDelegate2787120856.h"

// System.Void AstarPath/AstarWorkItem::.ctor(System.Func`2<System.Boolean,System.Boolean>)
extern "C"  void AstarWorkItem__ctor_m1435613042 (AstarWorkItem_t2566693888 * __this, Func_2_t3133160109 * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarPath/AstarWorkItem::.ctor(OnVoidDelegate,System.Func`2<System.Boolean,System.Boolean>)
extern "C"  void AstarWorkItem__ctor_m4090978010 (AstarWorkItem_t2566693888 * __this, OnVoidDelegate_t2787120856 * ___init0, Func_2_t3133160109 * ___update1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct AstarWorkItem_t2566693888;
struct AstarWorkItem_t2566693888_marshaled_pinvoke;

extern "C" void AstarWorkItem_t2566693888_marshal_pinvoke(const AstarWorkItem_t2566693888& unmarshaled, AstarWorkItem_t2566693888_marshaled_pinvoke& marshaled);
extern "C" void AstarWorkItem_t2566693888_marshal_pinvoke_back(const AstarWorkItem_t2566693888_marshaled_pinvoke& marshaled, AstarWorkItem_t2566693888& unmarshaled);
extern "C" void AstarWorkItem_t2566693888_marshal_pinvoke_cleanup(AstarWorkItem_t2566693888_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct AstarWorkItem_t2566693888;
struct AstarWorkItem_t2566693888_marshaled_com;

extern "C" void AstarWorkItem_t2566693888_marshal_com(const AstarWorkItem_t2566693888& unmarshaled, AstarWorkItem_t2566693888_marshaled_com& marshaled);
extern "C" void AstarWorkItem_t2566693888_marshal_com_back(const AstarWorkItem_t2566693888_marshaled_com& marshaled, AstarWorkItem_t2566693888& unmarshaled);
extern "C" void AstarWorkItem_t2566693888_marshal_com_cleanup(AstarWorkItem_t2566693888_marshaled_com& marshaled);
