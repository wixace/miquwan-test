﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimeUpdateVoGenerated
struct TimeUpdateVoGenerated_t4283433696;
// System.Action
struct Action_t3771233898;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// JSVCall
struct JSVCall_t3708497963;
// TimeUpdateVo
struct TimeUpdateVo_t1829512047;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_TimeUpdateVo1829512047.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void TimeUpdateVoGenerated::.ctor()
extern "C"  void TimeUpdateVoGenerated__ctor_m3510383243 (TimeUpdateVoGenerated_t4283433696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action TimeUpdateVoGenerated::TimeUpdateVo__ctor_GetDelegate_member0_arg1(CSRepresentedObject)
extern "C"  Action_t3771233898 * TimeUpdateVoGenerated_TimeUpdateVo__ctor_GetDelegate_member0_arg1_m3124541489 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimeUpdateVoGenerated::TimeUpdateVo_TimeUpdateVo1(JSVCall,System.Int32)
extern "C"  bool TimeUpdateVoGenerated_TimeUpdateVo_TimeUpdateVo1_m3652314727 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimeUpdateVoGenerated::TimeUpdateVo_Clear(JSVCall,System.Int32)
extern "C"  bool TimeUpdateVoGenerated_TimeUpdateVo_Clear_m788646834 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimeUpdateVoGenerated::TimeUpdateVo_SetDelay__Double(JSVCall,System.Int32)
extern "C"  bool TimeUpdateVoGenerated_TimeUpdateVo_SetDelay__Double_m2313280527 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimeUpdateVoGenerated::TimeUpdateVo_Update(JSVCall,System.Int32)
extern "C"  bool TimeUpdateVoGenerated_TimeUpdateVo_Update_m2436219782 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeUpdateVoGenerated::__Register()
extern "C"  void TimeUpdateVoGenerated___Register_m962996508 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action TimeUpdateVoGenerated::<TimeUpdateVo_TimeUpdateVo1>m__FA()
extern "C"  Action_t3771233898 * TimeUpdateVoGenerated_U3CTimeUpdateVo_TimeUpdateVo1U3Em__FA_m1337282175 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeUpdateVoGenerated::ilo_Clear1(TimeUpdateVo)
extern "C"  void TimeUpdateVoGenerated_ilo_Clear1_m2948583681 (Il2CppObject * __this /* static, unused */, TimeUpdateVo_t1829512047 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double TimeUpdateVoGenerated::ilo_getDouble2(System.Int32)
extern "C"  double TimeUpdateVoGenerated_ilo_getDouble2_m947634181 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimeUpdateVoGenerated::ilo_isFunctionS3(System.Int32)
extern "C"  bool TimeUpdateVoGenerated_ilo_isFunctionS3_m1762944171 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject TimeUpdateVoGenerated::ilo_getFunctionS4(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * TimeUpdateVoGenerated_ilo_getFunctionS4_m4199690643 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TimeUpdateVoGenerated::ilo_getObject5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * TimeUpdateVoGenerated_ilo_getObject5_m2014358718 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
