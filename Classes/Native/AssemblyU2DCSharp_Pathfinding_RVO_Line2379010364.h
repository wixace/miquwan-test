﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RVO.Line
struct  Line_t2379010364 
{
public:
	// UnityEngine.Vector2 Pathfinding.RVO.Line::point
	Vector2_t4282066565  ___point_0;
	// UnityEngine.Vector2 Pathfinding.RVO.Line::dir
	Vector2_t4282066565  ___dir_1;

public:
	inline static int32_t get_offset_of_point_0() { return static_cast<int32_t>(offsetof(Line_t2379010364, ___point_0)); }
	inline Vector2_t4282066565  get_point_0() const { return ___point_0; }
	inline Vector2_t4282066565 * get_address_of_point_0() { return &___point_0; }
	inline void set_point_0(Vector2_t4282066565  value)
	{
		___point_0 = value;
	}

	inline static int32_t get_offset_of_dir_1() { return static_cast<int32_t>(offsetof(Line_t2379010364, ___dir_1)); }
	inline Vector2_t4282066565  get_dir_1() const { return ___dir_1; }
	inline Vector2_t4282066565 * get_address_of_dir_1() { return &___dir_1; }
	inline void set_dir_1(Vector2_t4282066565  value)
	{
		___dir_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Pathfinding.RVO.Line
struct Line_t2379010364_marshaled_pinvoke
{
	Vector2_t4282066565_marshaled_pinvoke ___point_0;
	Vector2_t4282066565_marshaled_pinvoke ___dir_1;
};
// Native definition for marshalling of: Pathfinding.RVO.Line
struct Line_t2379010364_marshaled_com
{
	Vector2_t4282066565_marshaled_com ___point_0;
	Vector2_t4282066565_marshaled_com ___dir_1;
};
