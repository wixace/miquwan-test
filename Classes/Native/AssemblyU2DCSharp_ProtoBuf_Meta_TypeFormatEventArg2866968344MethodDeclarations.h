﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.TypeFormatEventArgs
struct TypeFormatEventArgs_t2866968344;
// System.String
struct String_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"

// System.Void ProtoBuf.Meta.TypeFormatEventArgs::.ctor(System.String)
extern "C"  void TypeFormatEventArgs__ctor_m2460231895 (TypeFormatEventArgs_t2866968344 * __this, String_t* ___formattedName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeFormatEventArgs::.ctor(System.Type)
extern "C"  void TypeFormatEventArgs__ctor_m839383694 (TypeFormatEventArgs_t2866968344 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.TypeFormatEventArgs::get_Type()
extern "C"  Type_t * TypeFormatEventArgs_get_Type_m3150862530 (TypeFormatEventArgs_t2866968344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeFormatEventArgs::set_Type(System.Type)
extern "C"  void TypeFormatEventArgs_set_Type_m2405964393 (TypeFormatEventArgs_t2866968344 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.Meta.TypeFormatEventArgs::get_FormattedName()
extern "C"  String_t* TypeFormatEventArgs_get_FormattedName_m1689647050 (TypeFormatEventArgs_t2866968344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.TypeFormatEventArgs::set_FormattedName(System.String)
extern "C"  void TypeFormatEventArgs_set_FormattedName_m2720503535 (TypeFormatEventArgs_t2866968344 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
