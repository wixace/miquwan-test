﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Bloom
struct Bloom_t64280035;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Void Bloom::.ctor()
extern "C"  void Bloom__ctor_m1759598111 (Bloom_t64280035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Bloom::CheckResources()
extern "C"  bool Bloom_CheckResources_m601689564 (Bloom_t64280035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bloom::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void Bloom_OnRenderImage_m3258681055 (Bloom_t64280035 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bloom::AddTo(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void Bloom_AddTo_m3061610318 (Bloom_t64280035 * __this, float ___intensity_0, RenderTexture_t1963041563 * ___from1, RenderTexture_t1963041563 * ___to2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bloom::BlendFlares(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void Bloom_BlendFlares_m3656378831 (Bloom_t64280035 * __this, RenderTexture_t1963041563 * ___from0, RenderTexture_t1963041563 * ___to1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bloom::BrightFilter(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void Bloom_BrightFilter_m174238832 (Bloom_t64280035 * __this, float ___thresh0, RenderTexture_t1963041563 * ___from1, RenderTexture_t1963041563 * ___to2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bloom::BrightFilter(UnityEngine.Color,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void Bloom_BrightFilter_m3623342315 (Bloom_t64280035 * __this, Color_t4194546905  ___threshColor0, RenderTexture_t1963041563 * ___from1, RenderTexture_t1963041563 * ___to2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bloom::Vignette(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void Bloom_Vignette_m1189655368 (Bloom_t64280035 * __this, float ___amount0, RenderTexture_t1963041563 * ___from1, RenderTexture_t1963041563 * ___to2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bloom::Main()
extern "C"  void Bloom_Main_m3020296894 (Bloom_t64280035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
