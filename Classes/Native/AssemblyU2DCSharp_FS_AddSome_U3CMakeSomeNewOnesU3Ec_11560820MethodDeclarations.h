﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FS_AddSome/<MakeSomeNewOnes>c__Iterator17
struct U3CMakeSomeNewOnesU3Ec__Iterator17_t11560820;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FS_AddSome/<MakeSomeNewOnes>c__Iterator17::.ctor()
extern "C"  void U3CMakeSomeNewOnesU3Ec__Iterator17__ctor_m3330814071 (U3CMakeSomeNewOnesU3Ec__Iterator17_t11560820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FS_AddSome/<MakeSomeNewOnes>c__Iterator17::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMakeSomeNewOnesU3Ec__Iterator17_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m706254203 (U3CMakeSomeNewOnesU3Ec__Iterator17_t11560820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FS_AddSome/<MakeSomeNewOnes>c__Iterator17::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMakeSomeNewOnesU3Ec__Iterator17_System_Collections_IEnumerator_get_Current_m920841999 (U3CMakeSomeNewOnesU3Ec__Iterator17_t11560820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FS_AddSome/<MakeSomeNewOnes>c__Iterator17::MoveNext()
extern "C"  bool U3CMakeSomeNewOnesU3Ec__Iterator17_MoveNext_m996964125 (U3CMakeSomeNewOnesU3Ec__Iterator17_t11560820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_AddSome/<MakeSomeNewOnes>c__Iterator17::Dispose()
extern "C"  void U3CMakeSomeNewOnesU3Ec__Iterator17_Dispose_m1059969780 (U3CMakeSomeNewOnesU3Ec__Iterator17_t11560820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_AddSome/<MakeSomeNewOnes>c__Iterator17::Reset()
extern "C"  void U3CMakeSomeNewOnesU3Ec__Iterator17_Reset_m977247012 (U3CMakeSomeNewOnesU3Ec__Iterator17_t11560820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
