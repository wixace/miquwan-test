﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Schema.XmlValueGetter
struct XmlValueGetter_t2234841801;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Xml.Schema.XmlValueGetter::.ctor(System.Object,System.IntPtr)
extern "C"  void XmlValueGetter__ctor_m3172759481 (XmlValueGetter_t2234841801 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Schema.XmlValueGetter::Invoke()
extern "C"  Il2CppObject * XmlValueGetter_Invoke_m3539050056 (XmlValueGetter_t2234841801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Xml.Schema.XmlValueGetter::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * XmlValueGetter_BeginInvoke_m1037761104 (XmlValueGetter_t2234841801 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Schema.XmlValueGetter::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * XmlValueGetter_EndInvoke_m1397628286 (XmlValueGetter_t2234841801 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
