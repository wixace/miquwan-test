﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen2887177558MethodDeclarations.h"

// System.Void System.Comparison`1<TeamSkill>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m1389872893(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t533259191 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m487232819_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<TeamSkill>::Invoke(T,T)
#define Comparison_1_Invoke_m3828184291(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t533259191 *, TeamSkill_t1816898004 *, TeamSkill_t1816898004 *, const MethodInfo*))Comparison_1_Invoke_m1888033133_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<TeamSkill>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m4276984668(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t533259191 *, TeamSkill_t1816898004 *, TeamSkill_t1816898004 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m3177996774_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<TeamSkill>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m1954435753(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t533259191 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m651541983_gshared)(__this, ___result0, method)
