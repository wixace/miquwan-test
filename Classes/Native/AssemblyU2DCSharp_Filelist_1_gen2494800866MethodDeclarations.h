﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Filelist_1_gen1153604171MethodDeclarations.h"

// System.Void Filelist`1<FileInfoCrc>::.ctor()
#define Filelist_1__ctor_m846080518(__this, method) ((  void (*) (Filelist_1_t2494800866 *, const MethodInfo*))Filelist_1__ctor_m1745091098_gshared)(__this, method)
// Filelist`1<T> Filelist`1<FileInfoCrc>::CreatFromBytes(System.Byte[])
#define Filelist_1_CreatFromBytes_m3901406955(__this /* static, unused */, ___bytes0, method) ((  Filelist_1_t2494800866 * (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t4260760469*, const MethodInfo*))Filelist_1_CreatFromBytes_m2160290303_gshared)(__this /* static, unused */, ___bytes0, method)
// System.Void Filelist`1<FileInfoCrc>::AddResData(T)
#define Filelist_1_AddResData_m330721079(__this, ___data0, method) ((  void (*) (Filelist_1_t2494800866 *, FileInfoCrc_t1217045770 *, const MethodInfo*))Filelist_1_AddResData_m934074443_gshared)(__this, ___data0, method)
// System.Void Filelist`1<FileInfoCrc>::AddOrReplaceResData(T)
#define Filelist_1_AddOrReplaceResData_m3789243168(__this, ___data0, method) ((  void (*) (Filelist_1_t2494800866 *, FileInfoCrc_t1217045770 *, const MethodInfo*))Filelist_1_AddOrReplaceResData_m1397803148_gshared)(__this, ___data0, method)
// System.Void Filelist`1<FileInfoCrc>::ReadBytes(System.Byte[])
#define Filelist_1_ReadBytes_m894658448(__this, ___bytes0, method) ((  void (*) (Filelist_1_t2494800866 *, ByteU5BU5D_t4260760469*, const MethodInfo*))Filelist_1_ReadBytes_m528302588_gshared)(__this, ___bytes0, method)
// System.Void Filelist`1<FileInfoCrc>::RemoveResData(System.String)
#define Filelist_1_RemoveResData_m4151114072(__this, ___dataPath0, method) ((  void (*) (Filelist_1_t2494800866 *, String_t*, const MethodInfo*))Filelist_1_RemoveResData_m2869673412_gshared)(__this, ___dataPath0, method)
// System.Void Filelist`1<FileInfoCrc>::RemoveResData(T)
#define Filelist_1_RemoveResData_m1244639412(__this, ___data0, method) ((  void (*) (Filelist_1_t2494800866 *, FileInfoCrc_t1217045770 *, const MethodInfo*))Filelist_1_RemoveResData_m1306572576_gshared)(__this, ___data0, method)
// T Filelist`1<FileInfoCrc>::GetResData(System.String)
#define Filelist_1_GetResData_m65020431(__this, ___fileName0, method) ((  FileInfoCrc_t1217045770 * (*) (Filelist_1_t2494800866 *, String_t*, const MethodInfo*))Filelist_1_GetResData_m1855791459_gshared)(__this, ___fileName0, method)
// System.String Filelist`1<FileInfoCrc>::ToString()
#define Filelist_1_ToString_m1775912551(__this, method) ((  String_t* (*) (Filelist_1_t2494800866 *, const MethodInfo*))Filelist_1_ToString_m1484309139_gshared)(__this, method)
