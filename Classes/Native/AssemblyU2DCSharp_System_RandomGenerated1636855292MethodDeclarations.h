﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_RandomGenerated
struct System_RandomGenerated_t1636855292;
// JSVCall
struct JSVCall_t3708497963;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_RandomGenerated::.ctor()
extern "C"  void System_RandomGenerated__ctor_m3168911903 (System_RandomGenerated_t1636855292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_RandomGenerated::Random_Random1(JSVCall,System.Int32)
extern "C"  bool System_RandomGenerated_Random_Random1_m2522483643 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_RandomGenerated::Random_Random2(JSVCall,System.Int32)
extern "C"  bool System_RandomGenerated_Random_Random2_m3767248124 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_RandomGenerated::Random_Next__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_RandomGenerated_Random_Next__Int32__Int32_m3589536744 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_RandomGenerated::Random_Next__Int32(JSVCall,System.Int32)
extern "C"  bool System_RandomGenerated_Random_Next__Int32_m1088562792 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_RandomGenerated::Random_Next(JSVCall,System.Int32)
extern "C"  bool System_RandomGenerated_Random_Next_m1240910824 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_RandomGenerated::Random_NextBytes__Byte_Array(JSVCall,System.Int32)
extern "C"  bool System_RandomGenerated_Random_NextBytes__Byte_Array_m386531175 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_RandomGenerated::Random_NextDouble(JSVCall,System.Int32)
extern "C"  bool System_RandomGenerated_Random_NextDouble_m207322073 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_RandomGenerated::__Register()
extern "C"  void System_RandomGenerated___Register_m3463394824 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_RandomGenerated::<Random_NextBytes__Byte_Array>m__C6()
extern "C"  ByteU5BU5D_t4260760469* System_RandomGenerated_U3CRandom_NextBytes__Byte_ArrayU3Em__C6_m390898447 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_RandomGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t System_RandomGenerated_ilo_getObject1_m1402360787 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_RandomGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool System_RandomGenerated_ilo_attachFinalizerObject2_m3335702753 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_RandomGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void System_RandomGenerated_ilo_addJSCSRel3_m3492462173 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_RandomGenerated::ilo_setInt324(System.Int32,System.Int32)
extern "C"  void System_RandomGenerated_ilo_setInt324_m2866407348 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_RandomGenerated::ilo_getArrayLength5(System.Int32)
extern "C"  int32_t System_RandomGenerated_ilo_getArrayLength5_m3743689593 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_RandomGenerated::ilo_getElement6(System.Int32,System.Int32)
extern "C"  int32_t System_RandomGenerated_ilo_getElement6_m656663482 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
