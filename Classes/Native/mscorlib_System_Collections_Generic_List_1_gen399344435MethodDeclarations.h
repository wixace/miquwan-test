﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>
struct List_1_t399344435;
// System.Collections.Generic.IEnumerable`1<Pathfinding.ClipperLib.IntPoint>
struct IEnumerable_1_t2332071840;
// System.Collections.Generic.IEnumerator`1<Pathfinding.ClipperLib.IntPoint>
struct IEnumerator_1_t943023932;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Pathfinding.ClipperLib.IntPoint>
struct ICollection_1_t4220716166;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.IntPoint>
struct ReadOnlyCollection_1_t588236419;
// Pathfinding.ClipperLib.IntPoint[]
struct IntPointU5BU5D_t3364663986;
// System.Predicate`1<Pathfinding.ClipperLib.IntPoint>
struct Predicate_1_t2937183062;
// System.Collections.Generic.IComparer`1<Pathfinding.ClipperLib.IntPoint>
struct IComparer_1_t1606172925;
// System.Comparison`1<Pathfinding.ClipperLib.IntPoint>
struct Comparison_1_t2042487366;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat419017205.h"

// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::.ctor()
extern "C"  void List_1__ctor_m124798912_gshared (List_1_t399344435 * __this, const MethodInfo* method);
#define List_1__ctor_m124798912(__this, method) ((  void (*) (List_1_t399344435 *, const MethodInfo*))List_1__ctor_m124798912_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1408284444_gshared (List_1_t399344435 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1408284444(__this, ___collection0, method) ((  void (*) (List_1_t399344435 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1408284444_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m2937085201_gshared (List_1_t399344435 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m2937085201(__this, ___capacity0, method) ((  void (*) (List_1_t399344435 *, int32_t, const MethodInfo*))List_1__ctor_m2937085201_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::.cctor()
extern "C"  void List_1__cctor_m2330688138_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m2330688138(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m2330688138_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3479864085_gshared (List_1_t399344435 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3479864085(__this, method) ((  Il2CppObject* (*) (List_1_t399344435 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3479864085_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1625198881_gshared (List_1_t399344435 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1625198881(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t399344435 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1625198881_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m586343132_gshared (List_1_t399344435 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m586343132(__this, method) ((  Il2CppObject * (*) (List_1_t399344435 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m586343132_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m3059185941_gshared (List_1_t399344435 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m3059185941(__this, ___item0, method) ((  int32_t (*) (List_1_t399344435 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3059185941_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1128932951_gshared (List_1_t399344435 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1128932951(__this, ___item0, method) ((  bool (*) (List_1_t399344435 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1128932951_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m207297773_gshared (List_1_t399344435 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m207297773(__this, ___item0, method) ((  int32_t (*) (List_1_t399344435 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m207297773_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3822617240_gshared (List_1_t399344435 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3822617240(__this, ___index0, ___item1, method) ((  void (*) (List_1_t399344435 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3822617240_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m4049784720_gshared (List_1_t399344435 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m4049784720(__this, ___item0, method) ((  void (*) (List_1_t399344435 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m4049784720_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1521964440_gshared (List_1_t399344435 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1521964440(__this, method) ((  bool (*) (List_1_t399344435 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1521964440_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m2442591653_gshared (List_1_t399344435 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2442591653(__this, method) ((  bool (*) (List_1_t399344435 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m2442591653_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m968786449_gshared (List_1_t399344435 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m968786449(__this, method) ((  Il2CppObject * (*) (List_1_t399344435 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m968786449_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1948721926_gshared (List_1_t399344435 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1948721926(__this, method) ((  bool (*) (List_1_t399344435 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1948721926_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m655418163_gshared (List_1_t399344435 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m655418163(__this, method) ((  bool (*) (List_1_t399344435 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m655418163_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m61485912_gshared (List_1_t399344435 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m61485912(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t399344435 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m61485912_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m1952082031_gshared (List_1_t399344435 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1952082031(__this, ___index0, ___value1, method) ((  void (*) (List_1_t399344435 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1952082031_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::Add(T)
extern "C"  void List_1_Add_m4114127024_gshared (List_1_t399344435 * __this, IntPoint_t3326126179  ___item0, const MethodInfo* method);
#define List_1_Add_m4114127024(__this, ___item0, method) ((  void (*) (List_1_t399344435 *, IntPoint_t3326126179 , const MethodInfo*))List_1_Add_m4114127024_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m899320535_gshared (List_1_t399344435 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m899320535(__this, ___newCount0, method) ((  void (*) (List_1_t399344435 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m899320535_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m803683504_gshared (List_1_t399344435 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m803683504(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t399344435 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m803683504_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1980876245_gshared (List_1_t399344435 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m1980876245(__this, ___collection0, method) ((  void (*) (List_1_t399344435 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1980876245_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m1241848469_gshared (List_1_t399344435 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m1241848469(__this, ___enumerable0, method) ((  void (*) (List_1_t399344435 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1241848469_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m717277922_gshared (List_1_t399344435 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m717277922(__this, ___collection0, method) ((  void (*) (List_1_t399344435 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m717277922_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t588236419 * List_1_AsReadOnly_m2086718599_gshared (List_1_t399344435 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2086718599(__this, method) ((  ReadOnlyCollection_1_t588236419 * (*) (List_1_t399344435 *, const MethodInfo*))List_1_AsReadOnly_m2086718599_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m1070855896_gshared (List_1_t399344435 * __this, IntPoint_t3326126179  ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m1070855896(__this, ___item0, method) ((  int32_t (*) (List_1_t399344435 *, IntPoint_t3326126179 , const MethodInfo*))List_1_BinarySearch_m1070855896_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::Clear()
extern "C"  void List_1_Clear_m1065078999_gshared (List_1_t399344435 * __this, const MethodInfo* method);
#define List_1_Clear_m1065078999(__this, method) ((  void (*) (List_1_t399344435 *, const MethodInfo*))List_1_Clear_m1065078999_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::Contains(T)
extern "C"  bool List_1_Contains_m36184156_gshared (List_1_t399344435 * __this, IntPoint_t3326126179  ___item0, const MethodInfo* method);
#define List_1_Contains_m36184156(__this, ___item0, method) ((  bool (*) (List_1_t399344435 *, IntPoint_t3326126179 , const MethodInfo*))List_1_Contains_m36184156_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2738076556_gshared (List_1_t399344435 * __this, IntPointU5BU5D_t3364663986* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2738076556(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t399344435 *, IntPointU5BU5D_t3364663986*, int32_t, const MethodInfo*))List_1_CopyTo_m2738076556_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::Find(System.Predicate`1<T>)
extern "C"  IntPoint_t3326126179  List_1_Find_m1394604700_gshared (List_1_t399344435 * __this, Predicate_1_t2937183062 * ___match0, const MethodInfo* method);
#define List_1_Find_m1394604700(__this, ___match0, method) ((  IntPoint_t3326126179  (*) (List_1_t399344435 *, Predicate_1_t2937183062 *, const MethodInfo*))List_1_Find_m1394604700_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m4063928535_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2937183062 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m4063928535(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2937183062 *, const MethodInfo*))List_1_CheckMatch_m4063928535_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1345653244_gshared (List_1_t399344435 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2937183062 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1345653244(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t399344435 *, int32_t, int32_t, Predicate_1_t2937183062 *, const MethodInfo*))List_1_GetIndex_m1345653244_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::GetEnumerator()
extern "C"  Enumerator_t419017205  List_1_GetEnumerator_m3458500505_gshared (List_1_t399344435 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3458500505(__this, method) ((  Enumerator_t419017205  (*) (List_1_t399344435 *, const MethodInfo*))List_1_GetEnumerator_m3458500505_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m4121517840_gshared (List_1_t399344435 * __this, IntPoint_t3326126179  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m4121517840(__this, ___item0, method) ((  int32_t (*) (List_1_t399344435 *, IntPoint_t3326126179 , const MethodInfo*))List_1_IndexOf_m4121517840_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m1057834467_gshared (List_1_t399344435 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m1057834467(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t399344435 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1057834467_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2484443740_gshared (List_1_t399344435 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2484443740(__this, ___index0, method) ((  void (*) (List_1_t399344435 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2484443740_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m1432569091_gshared (List_1_t399344435 * __this, int32_t ___index0, IntPoint_t3326126179  ___item1, const MethodInfo* method);
#define List_1_Insert_m1432569091(__this, ___index0, ___item1, method) ((  void (*) (List_1_t399344435 *, int32_t, IntPoint_t3326126179 , const MethodInfo*))List_1_Insert_m1432569091_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m1478227768_gshared (List_1_t399344435 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m1478227768(__this, ___collection0, method) ((  void (*) (List_1_t399344435 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m1478227768_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::Remove(T)
extern "C"  bool List_1_Remove_m1952532311_gshared (List_1_t399344435 * __this, IntPoint_t3326126179  ___item0, const MethodInfo* method);
#define List_1_Remove_m1952532311(__this, ___item0, method) ((  bool (*) (List_1_t399344435 *, IntPoint_t3326126179 , const MethodInfo*))List_1_Remove_m1952532311_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2835118803_gshared (List_1_t399344435 * __this, Predicate_1_t2937183062 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m2835118803(__this, ___match0, method) ((  int32_t (*) (List_1_t399344435 *, Predicate_1_t2937183062 *, const MethodInfo*))List_1_RemoveAll_m2835118803_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3601389257_gshared (List_1_t399344435 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3601389257(__this, ___index0, method) ((  void (*) (List_1_t399344435 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3601389257_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m3174564076_gshared (List_1_t399344435 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m3174564076(__this, ___index0, ___count1, method) ((  void (*) (List_1_t399344435 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3174564076_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::Reverse()
extern "C"  void List_1_Reverse_m3001388524_gshared (List_1_t399344435 * __this, const MethodInfo* method);
#define List_1_Reverse_m3001388524(__this, method) ((  void (*) (List_1_t399344435 *, const MethodInfo*))List_1_Reverse_m3001388524_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::Sort()
extern "C"  void List_1_Sort_m1627420031_gshared (List_1_t399344435 * __this, const MethodInfo* method);
#define List_1_Sort_m1627420031(__this, method) ((  void (*) (List_1_t399344435 *, const MethodInfo*))List_1_Sort_m1627420031_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m992070469_gshared (List_1_t399344435 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m992070469(__this, ___comparer0, method) ((  void (*) (List_1_t399344435 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m992070469_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m3291203730_gshared (List_1_t399344435 * __this, Comparison_1_t2042487366 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m3291203730(__this, ___comparison0, method) ((  void (*) (List_1_t399344435 *, Comparison_1_t2042487366 *, const MethodInfo*))List_1_Sort_m3291203730_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::ToArray()
extern "C"  IntPointU5BU5D_t3364663986* List_1_ToArray_m3607385922_gshared (List_1_t399344435 * __this, const MethodInfo* method);
#define List_1_ToArray_m3607385922(__this, method) ((  IntPointU5BU5D_t3364663986* (*) (List_1_t399344435 *, const MethodInfo*))List_1_ToArray_m3607385922_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::TrimExcess()
extern "C"  void List_1_TrimExcess_m8075736_gshared (List_1_t399344435 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m8075736(__this, method) ((  void (*) (List_1_t399344435 *, const MethodInfo*))List_1_TrimExcess_m8075736_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m350441280_gshared (List_1_t399344435 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m350441280(__this, method) ((  int32_t (*) (List_1_t399344435 *, const MethodInfo*))List_1_get_Capacity_m350441280_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m2269640076_gshared (List_1_t399344435 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m2269640076(__this, ___value0, method) ((  void (*) (List_1_t399344435 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2269640076_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::get_Count()
extern "C"  int32_t List_1_get_Count_m647655502_gshared (List_1_t399344435 * __this, const MethodInfo* method);
#define List_1_get_Count_m647655502(__this, method) ((  int32_t (*) (List_1_t399344435 *, const MethodInfo*))List_1_get_Count_m647655502_gshared)(__this, method)
// T System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::get_Item(System.Int32)
extern "C"  IntPoint_t3326126179  List_1_get_Item_m788539143_gshared (List_1_t399344435 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m788539143(__this, ___index0, method) ((  IntPoint_t3326126179  (*) (List_1_t399344435 *, int32_t, const MethodInfo*))List_1_get_Item_m788539143_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3328216734_gshared (List_1_t399344435 * __this, int32_t ___index0, IntPoint_t3326126179  ___value1, const MethodInfo* method);
#define List_1_set_Item_m3328216734(__this, ___index0, ___value1, method) ((  void (*) (List_1_t399344435 *, int32_t, IntPoint_t3326126179 , const MethodInfo*))List_1_set_Item_m3328216734_gshared)(__this, ___index0, ___value1, method)
