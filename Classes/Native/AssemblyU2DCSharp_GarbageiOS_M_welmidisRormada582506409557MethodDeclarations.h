﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_welmidisRormada58
struct M_welmidisRormada58_t2506409557;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_welmidisRormada58::.ctor()
extern "C"  void M_welmidisRormada58__ctor_m427823790 (M_welmidisRormada58_t2506409557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_welmidisRormada58::M_xiswawXereser0(System.String[],System.Int32)
extern "C"  void M_welmidisRormada58_M_xiswawXereser0_m3264803770 (M_welmidisRormada58_t2506409557 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_welmidisRormada58::M_jisearJejefaw1(System.String[],System.Int32)
extern "C"  void M_welmidisRormada58_M_jisearJejefaw1_m12788876 (M_welmidisRormada58_t2506409557 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_welmidisRormada58::M_peedahaDoka2(System.String[],System.Int32)
extern "C"  void M_welmidisRormada58_M_peedahaDoka2_m2980922410 (M_welmidisRormada58_t2506409557 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
