﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2868572062MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m2695898758(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2999954544 *, Dictionary_2_t4381535 *, const MethodInfo*))ValueCollection__ctor_m587328820_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,System.String>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2450271468(__this, ___item0, method) ((  void (*) (ValueCollection_t2999954544 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1412421758_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,System.String>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1628794805(__this, method) ((  void (*) (ValueCollection_t2999954544 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2867967047_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,System.String>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2448855738(__this, ___item0, method) ((  bool (*) (ValueCollection_t2999954544 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1938379368_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,System.String>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1625887199(__this, ___item0, method) ((  bool (*) (ValueCollection_t2999954544 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2514740493_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,System.String>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3639936963(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2999954544 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1129618005_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2252321401(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2999954544 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3607423115_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3345010868(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2999954544 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2307639110_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,System.String>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m724031597(__this, method) ((  bool (*) (ValueCollection_t2999954544 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m213555227_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1338350029(__this, method) ((  bool (*) (ValueCollection_t2999954544 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m422047355_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,System.String>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2583026425(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2999954544 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1579731495_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,System.String>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m3808753997(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2999954544 *, StringU5BU5D_t4054002952*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2183660667_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,System.String>::GetEnumerator()
#define ValueCollection_GetEnumerator_m2765205744(__this, method) ((  Enumerator_t2231182239  (*) (ValueCollection_t2999954544 *, const MethodInfo*))ValueCollection_GetEnumerator_m2490352798_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,System.String>::get_Count()
#define ValueCollection_get_Count_m4134362899(__this, method) ((  int32_t (*) (ValueCollection_t2999954544 *, const MethodInfo*))ValueCollection_get_Count_m1502902721_gshared)(__this, method)
