﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_murhow188
struct M_murhow188_t986034219;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_murhow188::.ctor()
extern "C"  void M_murhow188__ctor_m3447419288 (M_murhow188_t986034219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_murhow188::M_jawgu0(System.String[],System.Int32)
extern "C"  void M_murhow188_M_jawgu0_m2664164439 (M_murhow188_t986034219 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
