﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.BasicList
struct BasicList_t528018366;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;
// ProtoBuf.Meta.BasicList/MatchPredicate
struct MatchPredicate_t1402151099;
// System.String
struct String_t;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// ProtoBuf.Meta.BasicList/Node
struct Node_t1496877195;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_BasicList_NodeEnum3394063023.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_BasicList_MatchPre1402151099.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_BasicList_Node1496877195.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_BasicList528018366.h"

// System.Void ProtoBuf.Meta.BasicList::.ctor()
extern "C"  void BasicList__ctor_m3090548773 (BasicList_t528018366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.BasicList::.cctor()
extern "C"  void BasicList__cctor_m835635240 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ProtoBuf.Meta.BasicList::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * BasicList_System_Collections_IEnumerable_GetEnumerator_m4017064860 (BasicList_t528018366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.BasicList::CopyTo(System.Array,System.Int32)
extern "C"  void BasicList_CopyTo_m3812580546 (BasicList_t528018366 * __this, Il2CppArray * ___array0, int32_t ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.BasicList::Add(System.Object)
extern "C"  int32_t BasicList_Add_m2632209186 (BasicList_t528018366 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.BasicList::get_Item(System.Int32)
extern "C"  Il2CppObject * BasicList_get_Item_m3729566807 (BasicList_t528018366 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.BasicList::Trim()
extern "C"  void BasicList_Trim_m785481761 (BasicList_t528018366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.BasicList::get_Count()
extern "C"  int32_t BasicList_get_Count_m2960451767 (BasicList_t528018366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.BasicList/NodeEnumerator ProtoBuf.Meta.BasicList::GetEnumerator()
extern "C"  NodeEnumerator_t3394063023  BasicList_GetEnumerator_m1414647723 (BasicList_t528018366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.BasicList::IndexOf(ProtoBuf.Meta.BasicList/MatchPredicate,System.Object)
extern "C"  int32_t BasicList_IndexOf_m1950750157 (BasicList_t528018366 * __this, MatchPredicate_t1402151099 * ___predicate0, Il2CppObject * ___ctx1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.BasicList::IndexOfString(System.String)
extern "C"  int32_t BasicList_IndexOfString_m98934871 (BasicList_t528018366 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.BasicList::IndexOfReference(System.Object)
extern "C"  int32_t BasicList_IndexOfReference_m3213936961 (BasicList_t528018366 * __this, Il2CppObject * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.BasicList::Contains(System.Object)
extern "C"  bool BasicList_Contains_m800139562 (BasicList_t528018366 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.BasicList ProtoBuf.Meta.BasicList::GetContiguousGroups(System.Int32[],System.Object[])
extern "C"  BasicList_t528018366 * BasicList_GetContiguousGroups_m1986299741 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___keys0, ObjectU5BU5D_t1108656482* ___values1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.BasicList::ilo_get_Length1(ProtoBuf.Meta.BasicList/Node)
extern "C"  int32_t BasicList_ilo_get_Length1_m2884076021 (Il2CppObject * __this /* static, unused */, Node_t1496877195 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.BasicList/NodeEnumerator ProtoBuf.Meta.BasicList::ilo_GetEnumerator2(ProtoBuf.Meta.BasicList)
extern "C"  NodeEnumerator_t3394063023  BasicList_ilo_GetEnumerator2_m1596377648 (Il2CppObject * __this /* static, unused */, BasicList_t528018366 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Meta.BasicList::ilo_Add3(ProtoBuf.Meta.BasicList,System.Object)
extern "C"  int32_t BasicList_ilo_Add3_m2795583716 (Il2CppObject * __this /* static, unused */, BasicList_t528018366 * ____this0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
