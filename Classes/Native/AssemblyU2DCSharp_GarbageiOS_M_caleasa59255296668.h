﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_caleasa59
struct  M_caleasa59_t255296668  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_caleasa59::_dembuterDouke
	uint32_t ____dembuterDouke_0;
	// System.Int32 GarbageiOS.M_caleasa59::_turjenea
	int32_t ____turjenea_1;
	// System.String GarbageiOS.M_caleasa59::_drairdror
	String_t* ____drairdror_2;
	// System.Int32 GarbageiOS.M_caleasa59::_whaigaHanou
	int32_t ____whaigaHanou_3;
	// System.Boolean GarbageiOS.M_caleasa59::_xejeePeetoumis
	bool ____xejeePeetoumis_4;
	// System.String GarbageiOS.M_caleasa59::_naywhoujorJawderewhe
	String_t* ____naywhoujorJawderewhe_5;
	// System.Int32 GarbageiOS.M_caleasa59::_dirur
	int32_t ____dirur_6;
	// System.String GarbageiOS.M_caleasa59::_carisisNowsea
	String_t* ____carisisNowsea_7;
	// System.Single GarbageiOS.M_caleasa59::_matow
	float ____matow_8;
	// System.Int32 GarbageiOS.M_caleasa59::_jougereCairhea
	int32_t ____jougereCairhea_9;
	// System.Single GarbageiOS.M_caleasa59::_fayapouRearbarsur
	float ____fayapouRearbarsur_10;
	// System.String GarbageiOS.M_caleasa59::_jaleteMoraljai
	String_t* ____jaleteMoraljai_11;
	// System.Single GarbageiOS.M_caleasa59::_satrasrel
	float ____satrasrel_12;
	// System.Single GarbageiOS.M_caleasa59::_xurtreredir
	float ____xurtreredir_13;
	// System.UInt32 GarbageiOS.M_caleasa59::_turgofay
	uint32_t ____turgofay_14;
	// System.UInt32 GarbageiOS.M_caleasa59::_hatooNijo
	uint32_t ____hatooNijo_15;
	// System.Boolean GarbageiOS.M_caleasa59::_stowrearyawWemleefe
	bool ____stowrearyawWemleefe_16;
	// System.Boolean GarbageiOS.M_caleasa59::_salawkur
	bool ____salawkur_17;
	// System.Int32 GarbageiOS.M_caleasa59::_vilercowRosallfe
	int32_t ____vilercowRosallfe_18;
	// System.UInt32 GarbageiOS.M_caleasa59::_haynaspirWhoohorye
	uint32_t ____haynaspirWhoohorye_19;
	// System.Single GarbageiOS.M_caleasa59::_rearkas
	float ____rearkas_20;
	// System.String GarbageiOS.M_caleasa59::_joumerzaTitousoo
	String_t* ____joumerzaTitousoo_21;
	// System.Boolean GarbageiOS.M_caleasa59::_trallcutairKerearse
	bool ____trallcutairKerearse_22;
	// System.UInt32 GarbageiOS.M_caleasa59::_werpeehePaya
	uint32_t ____werpeehePaya_23;
	// System.Single GarbageiOS.M_caleasa59::_hearzea
	float ____hearzea_24;
	// System.Single GarbageiOS.M_caleasa59::_howsaw
	float ____howsaw_25;
	// System.Int32 GarbageiOS.M_caleasa59::_joudurou
	int32_t ____joudurou_26;
	// System.String GarbageiOS.M_caleasa59::_memlemar
	String_t* ____memlemar_27;
	// System.Single GarbageiOS.M_caleasa59::_befay
	float ____befay_28;
	// System.UInt32 GarbageiOS.M_caleasa59::_popouJojirpis
	uint32_t ____popouJojirpis_29;
	// System.Boolean GarbageiOS.M_caleasa59::_zachou
	bool ____zachou_30;
	// System.Single GarbageiOS.M_caleasa59::_memhor
	float ____memhor_31;
	// System.Boolean GarbageiOS.M_caleasa59::_tarbeCurgurji
	bool ____tarbeCurgurji_32;
	// System.Int32 GarbageiOS.M_caleasa59::_leargoPoupoo
	int32_t ____leargoPoupoo_33;

public:
	inline static int32_t get_offset_of__dembuterDouke_0() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____dembuterDouke_0)); }
	inline uint32_t get__dembuterDouke_0() const { return ____dembuterDouke_0; }
	inline uint32_t* get_address_of__dembuterDouke_0() { return &____dembuterDouke_0; }
	inline void set__dembuterDouke_0(uint32_t value)
	{
		____dembuterDouke_0 = value;
	}

	inline static int32_t get_offset_of__turjenea_1() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____turjenea_1)); }
	inline int32_t get__turjenea_1() const { return ____turjenea_1; }
	inline int32_t* get_address_of__turjenea_1() { return &____turjenea_1; }
	inline void set__turjenea_1(int32_t value)
	{
		____turjenea_1 = value;
	}

	inline static int32_t get_offset_of__drairdror_2() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____drairdror_2)); }
	inline String_t* get__drairdror_2() const { return ____drairdror_2; }
	inline String_t** get_address_of__drairdror_2() { return &____drairdror_2; }
	inline void set__drairdror_2(String_t* value)
	{
		____drairdror_2 = value;
		Il2CppCodeGenWriteBarrier(&____drairdror_2, value);
	}

	inline static int32_t get_offset_of__whaigaHanou_3() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____whaigaHanou_3)); }
	inline int32_t get__whaigaHanou_3() const { return ____whaigaHanou_3; }
	inline int32_t* get_address_of__whaigaHanou_3() { return &____whaigaHanou_3; }
	inline void set__whaigaHanou_3(int32_t value)
	{
		____whaigaHanou_3 = value;
	}

	inline static int32_t get_offset_of__xejeePeetoumis_4() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____xejeePeetoumis_4)); }
	inline bool get__xejeePeetoumis_4() const { return ____xejeePeetoumis_4; }
	inline bool* get_address_of__xejeePeetoumis_4() { return &____xejeePeetoumis_4; }
	inline void set__xejeePeetoumis_4(bool value)
	{
		____xejeePeetoumis_4 = value;
	}

	inline static int32_t get_offset_of__naywhoujorJawderewhe_5() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____naywhoujorJawderewhe_5)); }
	inline String_t* get__naywhoujorJawderewhe_5() const { return ____naywhoujorJawderewhe_5; }
	inline String_t** get_address_of__naywhoujorJawderewhe_5() { return &____naywhoujorJawderewhe_5; }
	inline void set__naywhoujorJawderewhe_5(String_t* value)
	{
		____naywhoujorJawderewhe_5 = value;
		Il2CppCodeGenWriteBarrier(&____naywhoujorJawderewhe_5, value);
	}

	inline static int32_t get_offset_of__dirur_6() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____dirur_6)); }
	inline int32_t get__dirur_6() const { return ____dirur_6; }
	inline int32_t* get_address_of__dirur_6() { return &____dirur_6; }
	inline void set__dirur_6(int32_t value)
	{
		____dirur_6 = value;
	}

	inline static int32_t get_offset_of__carisisNowsea_7() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____carisisNowsea_7)); }
	inline String_t* get__carisisNowsea_7() const { return ____carisisNowsea_7; }
	inline String_t** get_address_of__carisisNowsea_7() { return &____carisisNowsea_7; }
	inline void set__carisisNowsea_7(String_t* value)
	{
		____carisisNowsea_7 = value;
		Il2CppCodeGenWriteBarrier(&____carisisNowsea_7, value);
	}

	inline static int32_t get_offset_of__matow_8() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____matow_8)); }
	inline float get__matow_8() const { return ____matow_8; }
	inline float* get_address_of__matow_8() { return &____matow_8; }
	inline void set__matow_8(float value)
	{
		____matow_8 = value;
	}

	inline static int32_t get_offset_of__jougereCairhea_9() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____jougereCairhea_9)); }
	inline int32_t get__jougereCairhea_9() const { return ____jougereCairhea_9; }
	inline int32_t* get_address_of__jougereCairhea_9() { return &____jougereCairhea_9; }
	inline void set__jougereCairhea_9(int32_t value)
	{
		____jougereCairhea_9 = value;
	}

	inline static int32_t get_offset_of__fayapouRearbarsur_10() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____fayapouRearbarsur_10)); }
	inline float get__fayapouRearbarsur_10() const { return ____fayapouRearbarsur_10; }
	inline float* get_address_of__fayapouRearbarsur_10() { return &____fayapouRearbarsur_10; }
	inline void set__fayapouRearbarsur_10(float value)
	{
		____fayapouRearbarsur_10 = value;
	}

	inline static int32_t get_offset_of__jaleteMoraljai_11() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____jaleteMoraljai_11)); }
	inline String_t* get__jaleteMoraljai_11() const { return ____jaleteMoraljai_11; }
	inline String_t** get_address_of__jaleteMoraljai_11() { return &____jaleteMoraljai_11; }
	inline void set__jaleteMoraljai_11(String_t* value)
	{
		____jaleteMoraljai_11 = value;
		Il2CppCodeGenWriteBarrier(&____jaleteMoraljai_11, value);
	}

	inline static int32_t get_offset_of__satrasrel_12() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____satrasrel_12)); }
	inline float get__satrasrel_12() const { return ____satrasrel_12; }
	inline float* get_address_of__satrasrel_12() { return &____satrasrel_12; }
	inline void set__satrasrel_12(float value)
	{
		____satrasrel_12 = value;
	}

	inline static int32_t get_offset_of__xurtreredir_13() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____xurtreredir_13)); }
	inline float get__xurtreredir_13() const { return ____xurtreredir_13; }
	inline float* get_address_of__xurtreredir_13() { return &____xurtreredir_13; }
	inline void set__xurtreredir_13(float value)
	{
		____xurtreredir_13 = value;
	}

	inline static int32_t get_offset_of__turgofay_14() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____turgofay_14)); }
	inline uint32_t get__turgofay_14() const { return ____turgofay_14; }
	inline uint32_t* get_address_of__turgofay_14() { return &____turgofay_14; }
	inline void set__turgofay_14(uint32_t value)
	{
		____turgofay_14 = value;
	}

	inline static int32_t get_offset_of__hatooNijo_15() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____hatooNijo_15)); }
	inline uint32_t get__hatooNijo_15() const { return ____hatooNijo_15; }
	inline uint32_t* get_address_of__hatooNijo_15() { return &____hatooNijo_15; }
	inline void set__hatooNijo_15(uint32_t value)
	{
		____hatooNijo_15 = value;
	}

	inline static int32_t get_offset_of__stowrearyawWemleefe_16() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____stowrearyawWemleefe_16)); }
	inline bool get__stowrearyawWemleefe_16() const { return ____stowrearyawWemleefe_16; }
	inline bool* get_address_of__stowrearyawWemleefe_16() { return &____stowrearyawWemleefe_16; }
	inline void set__stowrearyawWemleefe_16(bool value)
	{
		____stowrearyawWemleefe_16 = value;
	}

	inline static int32_t get_offset_of__salawkur_17() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____salawkur_17)); }
	inline bool get__salawkur_17() const { return ____salawkur_17; }
	inline bool* get_address_of__salawkur_17() { return &____salawkur_17; }
	inline void set__salawkur_17(bool value)
	{
		____salawkur_17 = value;
	}

	inline static int32_t get_offset_of__vilercowRosallfe_18() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____vilercowRosallfe_18)); }
	inline int32_t get__vilercowRosallfe_18() const { return ____vilercowRosallfe_18; }
	inline int32_t* get_address_of__vilercowRosallfe_18() { return &____vilercowRosallfe_18; }
	inline void set__vilercowRosallfe_18(int32_t value)
	{
		____vilercowRosallfe_18 = value;
	}

	inline static int32_t get_offset_of__haynaspirWhoohorye_19() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____haynaspirWhoohorye_19)); }
	inline uint32_t get__haynaspirWhoohorye_19() const { return ____haynaspirWhoohorye_19; }
	inline uint32_t* get_address_of__haynaspirWhoohorye_19() { return &____haynaspirWhoohorye_19; }
	inline void set__haynaspirWhoohorye_19(uint32_t value)
	{
		____haynaspirWhoohorye_19 = value;
	}

	inline static int32_t get_offset_of__rearkas_20() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____rearkas_20)); }
	inline float get__rearkas_20() const { return ____rearkas_20; }
	inline float* get_address_of__rearkas_20() { return &____rearkas_20; }
	inline void set__rearkas_20(float value)
	{
		____rearkas_20 = value;
	}

	inline static int32_t get_offset_of__joumerzaTitousoo_21() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____joumerzaTitousoo_21)); }
	inline String_t* get__joumerzaTitousoo_21() const { return ____joumerzaTitousoo_21; }
	inline String_t** get_address_of__joumerzaTitousoo_21() { return &____joumerzaTitousoo_21; }
	inline void set__joumerzaTitousoo_21(String_t* value)
	{
		____joumerzaTitousoo_21 = value;
		Il2CppCodeGenWriteBarrier(&____joumerzaTitousoo_21, value);
	}

	inline static int32_t get_offset_of__trallcutairKerearse_22() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____trallcutairKerearse_22)); }
	inline bool get__trallcutairKerearse_22() const { return ____trallcutairKerearse_22; }
	inline bool* get_address_of__trallcutairKerearse_22() { return &____trallcutairKerearse_22; }
	inline void set__trallcutairKerearse_22(bool value)
	{
		____trallcutairKerearse_22 = value;
	}

	inline static int32_t get_offset_of__werpeehePaya_23() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____werpeehePaya_23)); }
	inline uint32_t get__werpeehePaya_23() const { return ____werpeehePaya_23; }
	inline uint32_t* get_address_of__werpeehePaya_23() { return &____werpeehePaya_23; }
	inline void set__werpeehePaya_23(uint32_t value)
	{
		____werpeehePaya_23 = value;
	}

	inline static int32_t get_offset_of__hearzea_24() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____hearzea_24)); }
	inline float get__hearzea_24() const { return ____hearzea_24; }
	inline float* get_address_of__hearzea_24() { return &____hearzea_24; }
	inline void set__hearzea_24(float value)
	{
		____hearzea_24 = value;
	}

	inline static int32_t get_offset_of__howsaw_25() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____howsaw_25)); }
	inline float get__howsaw_25() const { return ____howsaw_25; }
	inline float* get_address_of__howsaw_25() { return &____howsaw_25; }
	inline void set__howsaw_25(float value)
	{
		____howsaw_25 = value;
	}

	inline static int32_t get_offset_of__joudurou_26() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____joudurou_26)); }
	inline int32_t get__joudurou_26() const { return ____joudurou_26; }
	inline int32_t* get_address_of__joudurou_26() { return &____joudurou_26; }
	inline void set__joudurou_26(int32_t value)
	{
		____joudurou_26 = value;
	}

	inline static int32_t get_offset_of__memlemar_27() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____memlemar_27)); }
	inline String_t* get__memlemar_27() const { return ____memlemar_27; }
	inline String_t** get_address_of__memlemar_27() { return &____memlemar_27; }
	inline void set__memlemar_27(String_t* value)
	{
		____memlemar_27 = value;
		Il2CppCodeGenWriteBarrier(&____memlemar_27, value);
	}

	inline static int32_t get_offset_of__befay_28() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____befay_28)); }
	inline float get__befay_28() const { return ____befay_28; }
	inline float* get_address_of__befay_28() { return &____befay_28; }
	inline void set__befay_28(float value)
	{
		____befay_28 = value;
	}

	inline static int32_t get_offset_of__popouJojirpis_29() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____popouJojirpis_29)); }
	inline uint32_t get__popouJojirpis_29() const { return ____popouJojirpis_29; }
	inline uint32_t* get_address_of__popouJojirpis_29() { return &____popouJojirpis_29; }
	inline void set__popouJojirpis_29(uint32_t value)
	{
		____popouJojirpis_29 = value;
	}

	inline static int32_t get_offset_of__zachou_30() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____zachou_30)); }
	inline bool get__zachou_30() const { return ____zachou_30; }
	inline bool* get_address_of__zachou_30() { return &____zachou_30; }
	inline void set__zachou_30(bool value)
	{
		____zachou_30 = value;
	}

	inline static int32_t get_offset_of__memhor_31() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____memhor_31)); }
	inline float get__memhor_31() const { return ____memhor_31; }
	inline float* get_address_of__memhor_31() { return &____memhor_31; }
	inline void set__memhor_31(float value)
	{
		____memhor_31 = value;
	}

	inline static int32_t get_offset_of__tarbeCurgurji_32() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____tarbeCurgurji_32)); }
	inline bool get__tarbeCurgurji_32() const { return ____tarbeCurgurji_32; }
	inline bool* get_address_of__tarbeCurgurji_32() { return &____tarbeCurgurji_32; }
	inline void set__tarbeCurgurji_32(bool value)
	{
		____tarbeCurgurji_32 = value;
	}

	inline static int32_t get_offset_of__leargoPoupoo_33() { return static_cast<int32_t>(offsetof(M_caleasa59_t255296668, ____leargoPoupoo_33)); }
	inline int32_t get__leargoPoupoo_33() const { return ____leargoPoupoo_33; }
	inline int32_t* get_address_of__leargoPoupoo_33() { return &____leargoPoupoo_33; }
	inline void set__leargoPoupoo_33(int32_t value)
	{
		____leargoPoupoo_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
