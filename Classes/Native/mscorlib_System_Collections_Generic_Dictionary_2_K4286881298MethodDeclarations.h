﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke488048368MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,UnityEngine.GameObject>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m946298280(__this, ___host0, method) ((  void (*) (Enumerator_t4286881298 *, Dictionary_2_t3671945244 *, const MethodInfo*))Enumerator__ctor_m535379646_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,UnityEngine.GameObject>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2146244537(__this, method) ((  Il2CppObject * (*) (Enumerator_t4286881298 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1848869421_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,UnityEngine.GameObject>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2179856909(__this, method) ((  void (*) (Enumerator_t4286881298 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m548984631_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,UnityEngine.GameObject>::Dispose()
#define Enumerator_Dispose_m1636785738(__this, method) ((  void (*) (Enumerator_t4286881298 *, const MethodInfo*))Enumerator_Dispose_m2263765216_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,UnityEngine.GameObject>::MoveNext()
#define Enumerator_MoveNext_m785625328(__this, method) ((  bool (*) (Enumerator_t4286881298 *, const MethodInfo*))Enumerator_MoveNext_m3798960615_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,UnityEngine.GameObject>::get_Current()
#define Enumerator_get_Current_m4130800288(__this, method) ((  int32_t (*) (Enumerator_t4286881298 *, const MethodInfo*))Enumerator_get_Current_m1651525585_gshared)(__this, method)
