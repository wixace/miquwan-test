﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.LocalAvoidance/VO
struct VO_t2172220293;
// Pathfinding.LocalAvoidance/HalfPlane
struct HalfPlane_t3916155773;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_VO2172220293.h"

// System.Void Pathfinding.LocalAvoidance/VO::.ctor()
extern "C"  void VO__ctor_m3566945158 (VO_t2172220293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LocalAvoidance/VO::AddInt(System.Single,System.Boolean,System.Int32)
extern "C"  void VO_AddInt_m4178162521 (VO_t2172220293 * __this, float ___factor0, bool ___inside1, int32_t ___id2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LocalAvoidance/VO::FinalInts(UnityEngine.Vector3,UnityEngine.Vector3,System.Boolean,UnityEngine.Vector3&)
extern "C"  bool VO_FinalInts_m2004239254 (VO_t2172220293 * __this, Vector3_t4282066566  ___target0, Vector3_t4282066566  ___closeEdgeConstraint1, bool ___drawGizmos2, Vector3_t4282066566 * ___closest3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.LocalAvoidance/VO::Contains(UnityEngine.Vector3)
extern "C"  bool VO_Contains_m3439821968 (VO_t2172220293 * __this, Vector3_t4282066566  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.LocalAvoidance/VO::ScoreContains(UnityEngine.Vector3)
extern "C"  float VO_ScoreContains_m1554358840 (VO_t2172220293 * __this, Vector3_t4282066566  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LocalAvoidance/VO::Draw(UnityEngine.Color)
extern "C"  void VO_Draw_m2948114724 (VO_t2172220293 * __this, Color_t4194546905  ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.LocalAvoidance/HalfPlane Pathfinding.LocalAvoidance/VO::op_Explicit(Pathfinding.LocalAvoidance/VO)
extern "C"  HalfPlane_t3916155773 * VO_op_Explicit_m1786787895 (Il2CppObject * __this /* static, unused */, VO_t2172220293 * ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
