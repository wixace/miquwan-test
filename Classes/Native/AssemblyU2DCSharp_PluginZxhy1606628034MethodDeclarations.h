﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginZxhy
struct PluginZxhy_t1606628034;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// VersionMgr
struct VersionMgr_t1322950208;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// VersionInfo
struct VersionInfo_t2356638086;
// Mihua.SDK.SdkwwwCallBack
struct SdkwwwCallBack_t3004559384;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// System.Object
struct Il2CppObject;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "UnityEngine_UnityEngine_WWWForm461342257.h"
#include "AssemblyU2DCSharp_Mihua_SDK_SdkwwwCallBack3004559384.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_PluginZxhy1606628034.h"

// System.Void PluginZxhy::.ctor()
extern "C"  void PluginZxhy__ctor_m3207094297 (PluginZxhy_t1606628034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::Init()
extern "C"  void PluginZxhy_Init_m3657221915 (PluginZxhy_t1606628034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginZxhy::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginZxhy_ReqSDKHttpLogin_m3795548590 (PluginZxhy_t1606628034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginZxhy::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginZxhy_IsLoginSuccess_m154513434 (PluginZxhy_t1606628034 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::OpenUserLogin()
extern "C"  void PluginZxhy_OpenUserLogin_m1966102891 (PluginZxhy_t1606628034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::UserPay(CEvent.ZEvent)
extern "C"  void PluginZxhy_UserPay_m3723156391 (PluginZxhy_t1606628034 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginZxhy_SignCallBack_m2589636800 (PluginZxhy_t1606628034 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginZxhy::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginZxhy_BuildOrderParam2WWWForm_m169363816 (PluginZxhy_t1606628034 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::OnInitSuccess(System.String)
extern "C"  void PluginZxhy_OnInitSuccess_m120611127 (PluginZxhy_t1606628034 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::OnInitFail(System.String)
extern "C"  void PluginZxhy_OnInitFail_m2273866602 (PluginZxhy_t1606628034 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::OnLoginSuccess(System.String)
extern "C"  void PluginZxhy_OnLoginSuccess_m4128656990 (PluginZxhy_t1606628034 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::OnLoginFail(System.String)
extern "C"  void PluginZxhy_OnLoginFail_m400224163 (PluginZxhy_t1606628034 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::OnPaySuccess(System.String)
extern "C"  void PluginZxhy_OnPaySuccess_m4032344093 (PluginZxhy_t1606628034 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::OnPayFail(System.String)
extern "C"  void PluginZxhy_OnPayFail_m700382788 (PluginZxhy_t1606628034 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::OnUserLogoutSuccess(System.String)
extern "C"  void PluginZxhy_OnUserLogoutSuccess_m1058918620 (PluginZxhy_t1606628034 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::OnLogoutSuccess(System.String)
extern "C"  void PluginZxhy_OnLogoutSuccess_m3437857777 (PluginZxhy_t1606628034 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::initSdk(System.String)
extern "C"  void PluginZxhy_initSdk_m2969496129 (PluginZxhy_t1606628034 * __this, String_t* ___appId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::login()
extern "C"  void PluginZxhy_login_m2729109120 (PluginZxhy_t1606628034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::pay(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginZxhy_pay_m801166543 (PluginZxhy_t1606628034 * __this, String_t* ___goodId0, String_t* ___goodName1, String_t* ___orderId2, String_t* ___cbUrl3, String_t* ___price4, String_t* ___body5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::<OnLogoutSuccess>m__485()
extern "C"  void PluginZxhy_U3COnLogoutSuccessU3Em__485_m4287964475 (PluginZxhy_t1606628034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginZxhy_ilo_AddEventListener1_m1880682939 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginZxhy::ilo_get_PluginsSdkMgr2()
extern "C"  PluginsSdkMgr_t3884624670 * PluginZxhy_ilo_get_PluginsSdkMgr2_m3562621616 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginZxhy::ilo_get_Instance3()
extern "C"  VersionMgr_t1322950208 * PluginZxhy_ilo_get_Instance3_m1621306396 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr PluginZxhy::ilo_get_FloatTextMgr4()
extern "C"  FloatTextMgr_t630384591 * PluginZxhy_ilo_get_FloatTextMgr4_m1184276306 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginZxhy::ilo_get_currentVS5(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginZxhy_ilo_get_currentVS5_m1754484787 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::ilo_Request6(System.String,UnityEngine.WWWForm,Mihua.SDK.SdkwwwCallBack)
extern "C"  void PluginZxhy_ilo_Request6_m59425269 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWWForm_t461342257 * ___wwwForm1, SdkwwwCallBack_t3004559384 * ___callBack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginZxhy::ilo_ContainsKey7(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  bool PluginZxhy_ilo_ContainsKey7_m1565217429 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken PluginZxhy::ilo_get_Item8(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * PluginZxhy_ilo_get_Item8_m1710032385 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginZxhy::ilo_get_ProductsCfgMgr9()
extern "C"  ProductsCfgMgr_t2493714872 * PluginZxhy_ilo_get_ProductsCfgMgr9_m4145350903 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginZxhy::ilo_GetProductID10(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginZxhy_ilo_GetProductID10_m1604964376 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::ilo_Log11(System.Object,System.Boolean)
extern "C"  void PluginZxhy_ilo_Log11_m1972287345 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::ilo_DispatchEvent12(CEvent.ZEvent)
extern "C"  void PluginZxhy_ilo_DispatchEvent12_m3211705238 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::ilo_Logout13(System.Action)
extern "C"  void PluginZxhy_ilo_Logout13_m2500271379 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZxhy::ilo_OpenUserLogin14(PluginZxhy)
extern "C"  void PluginZxhy_ilo_OpenUserLogin14_m1150593657 (Il2CppObject * __this /* static, unused */, PluginZxhy_t1606628034 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
