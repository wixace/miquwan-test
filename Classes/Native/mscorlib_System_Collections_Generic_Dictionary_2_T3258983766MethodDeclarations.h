﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Object>
struct Transform_1_t3258983766;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m84775411_gshared (Transform_1_t3258983766 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m84775411(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3258983766 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m84775411_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m1600063717_gshared (Transform_1_t3258983766 * __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m1600063717(__this, ___key0, ___value1, method) ((  Il2CppObject * (*) (Transform_1_t3258983766 *, Il2CppObject *, stValue_t3425945410 , const MethodInfo*))Transform_1_Invoke_m1600063717_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m890314896_gshared (Transform_1_t3258983766 * __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m890314896(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3258983766 *, Il2CppObject *, stValue_t3425945410 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m890314896_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,HatredCtrl/stValue,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m419399301_gshared (Transform_1_t3258983766 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m419399301(__this, ___result0, method) ((  Il2CppObject * (*) (Transform_1_t3258983766 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m419399301_gshared)(__this, ___result0, method)
