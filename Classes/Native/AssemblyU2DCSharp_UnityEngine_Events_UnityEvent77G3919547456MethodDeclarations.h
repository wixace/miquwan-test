﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Events_UnityEvent77Generated/<UnityEventA1_AddListener_GetDelegate_member0_arg0>c__AnonStoreyE1`1<System.Object>
struct U3CUnityEventA1_AddListener_GetDelegate_member0_arg0U3Ec__AnonStoreyE1_1_t3919547456;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_Events_UnityEvent77Generated/<UnityEventA1_AddListener_GetDelegate_member0_arg0>c__AnonStoreyE1`1<System.Object>::.ctor()
extern "C"  void U3CUnityEventA1_AddListener_GetDelegate_member0_arg0U3Ec__AnonStoreyE1_1__ctor_m3347273799_gshared (U3CUnityEventA1_AddListener_GetDelegate_member0_arg0U3Ec__AnonStoreyE1_1_t3919547456 * __this, const MethodInfo* method);
#define U3CUnityEventA1_AddListener_GetDelegate_member0_arg0U3Ec__AnonStoreyE1_1__ctor_m3347273799(__this, method) ((  void (*) (U3CUnityEventA1_AddListener_GetDelegate_member0_arg0U3Ec__AnonStoreyE1_1_t3919547456 *, const MethodInfo*))U3CUnityEventA1_AddListener_GetDelegate_member0_arg0U3Ec__AnonStoreyE1_1__ctor_m3347273799_gshared)(__this, method)
// System.Void UnityEngine_Events_UnityEvent77Generated/<UnityEventA1_AddListener_GetDelegate_member0_arg0>c__AnonStoreyE1`1<System.Object>::<>m__1AC(T0)
extern "C"  void U3CUnityEventA1_AddListener_GetDelegate_member0_arg0U3Ec__AnonStoreyE1_1_U3CU3Em__1AC_m1567985673_gshared (U3CUnityEventA1_AddListener_GetDelegate_member0_arg0U3Ec__AnonStoreyE1_1_t3919547456 * __this, Il2CppObject * ___arg00, const MethodInfo* method);
#define U3CUnityEventA1_AddListener_GetDelegate_member0_arg0U3Ec__AnonStoreyE1_1_U3CU3Em__1AC_m1567985673(__this, ___arg00, method) ((  void (*) (U3CUnityEventA1_AddListener_GetDelegate_member0_arg0U3Ec__AnonStoreyE1_1_t3919547456 *, Il2CppObject *, const MethodInfo*))U3CUnityEventA1_AddListener_GetDelegate_member0_arg0U3Ec__AnonStoreyE1_1_U3CU3Em__1AC_m1567985673_gshared)(__this, ___arg00, method)
