﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_IDragHandlerGenerated
struct UnityEngine_EventSystems_IDragHandlerGenerated_t2629206453;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_EventSystems_IDragHandlerGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_IDragHandlerGenerated__ctor_m506922630 (UnityEngine_EventSystems_IDragHandlerGenerated_t2629206453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_IDragHandlerGenerated::IDragHandler_OnDrag__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_IDragHandlerGenerated_IDragHandler_OnDrag__PointerEventData_m1132179820 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_IDragHandlerGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_IDragHandlerGenerated___Register_m3012008449 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
