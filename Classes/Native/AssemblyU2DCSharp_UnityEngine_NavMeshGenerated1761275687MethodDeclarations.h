﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_NavMeshGenerated
struct UnityEngine_NavMeshGenerated_t1761275687;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_NavMeshGenerated::.ctor()
extern "C"  void UnityEngine_NavMeshGenerated__ctor_m3954372436 (UnityEngine_NavMeshGenerated_t1761275687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshGenerated::NavMesh_NavMesh1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshGenerated_NavMesh_NavMesh1_m3193418372 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshGenerated::NavMesh_AllAreas(JSVCall)
extern "C"  void UnityEngine_NavMeshGenerated_NavMesh_AllAreas_m4184299267 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshGenerated::NavMesh_avoidancePredictionTime(JSVCall)
extern "C"  void UnityEngine_NavMeshGenerated_NavMesh_avoidancePredictionTime_m3003056004 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshGenerated::NavMesh_pathfindingIterationsPerFrame(JSVCall)
extern "C"  void UnityEngine_NavMeshGenerated_NavMesh_pathfindingIterationsPerFrame_m1711745306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshGenerated::NavMesh_CalculatePath__Vector3__Vector3__Int32__NavMeshPath(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshGenerated_NavMesh_CalculatePath__Vector3__Vector3__Int32__NavMeshPath_m3164245297 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshGenerated::NavMesh_CalculateTriangulation(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshGenerated_NavMesh_CalculateTriangulation_m1419442764 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshGenerated::NavMesh_FindClosestEdge__Vector3__NavMeshHit__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshGenerated_NavMesh_FindClosestEdge__Vector3__NavMeshHit__Int32_m2646501623 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshGenerated::NavMesh_GetAreaCost__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshGenerated_NavMesh_GetAreaCost__Int32_m3325352161 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshGenerated::NavMesh_GetAreaFromName__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshGenerated_NavMesh_GetAreaFromName__String_m3373380072 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshGenerated::NavMesh_Raycast__Vector3__Vector3__NavMeshHit__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshGenerated_NavMesh_Raycast__Vector3__Vector3__NavMeshHit__Int32_m676209893 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshGenerated::NavMesh_SamplePosition__Vector3__NavMeshHit__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshGenerated_NavMesh_SamplePosition__Vector3__NavMeshHit__Single__Int32_m2975534857 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshGenerated::NavMesh_SetAreaCost__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshGenerated_NavMesh_SetAreaCost__Int32__Single_m2708000157 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshGenerated::__Register()
extern "C"  void UnityEngine_NavMeshGenerated___Register_m1589838323 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshGenerated::ilo_setInt321(System.Int32,System.Int32)
extern "C"  void UnityEngine_NavMeshGenerated_ilo_setInt321_m3301105410 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_NavMeshGenerated::ilo_getInt322(System.Int32)
extern "C"  int32_t UnityEngine_NavMeshGenerated_ilo_getInt322_m1697099996 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_NavMeshGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_NavMeshGenerated_ilo_getObject3_m1098805405 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_NavMeshGenerated::ilo_getVector3S4(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_NavMeshGenerated_ilo_getVector3S4_m870011049 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void UnityEngine_NavMeshGenerated_ilo_setBooleanS5_m3294290241 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshGenerated::ilo_setArgIndex6(System.Int32)
extern "C"  void UnityEngine_NavMeshGenerated_ilo_setArgIndex6_m4129194476 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshGenerated::ilo_setSingle7(System.Int32,System.Single)
extern "C"  void UnityEngine_NavMeshGenerated_ilo_setSingle7_m3388187558 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_NavMeshGenerated::ilo_getStringS8(System.Int32)
extern "C"  String_t* UnityEngine_NavMeshGenerated_ilo_getStringS8_m3530296665 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_NavMeshGenerated::ilo_incArgIndex9()
extern "C"  int32_t UnityEngine_NavMeshGenerated_ilo_incArgIndex9_m1793430896 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
