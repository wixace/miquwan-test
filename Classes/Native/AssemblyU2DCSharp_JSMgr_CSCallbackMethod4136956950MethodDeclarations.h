﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSMgr/CSCallbackMethod
struct CSCallbackMethod_t4136956950;
// System.Object
struct Il2CppObject;
// JSVCall
struct JSVCall_t3708497963;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void JSMgr/CSCallbackMethod::.ctor(System.Object,System.IntPtr)
extern "C"  void CSCallbackMethod__ctor_m2492137405 (CSCallbackMethod_t4136956950 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSMgr/CSCallbackMethod::Invoke(JSVCall,System.Int32)
extern "C"  bool CSCallbackMethod_Invoke_m638101531 (CSCallbackMethod_t4136956950 * __this, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult JSMgr/CSCallbackMethod::BeginInvoke(JSVCall,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CSCallbackMethod_BeginInvoke_m1859228674 (CSCallbackMethod_t4136956950 * __this, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSMgr/CSCallbackMethod::EndInvoke(System.IAsyncResult)
extern "C"  bool CSCallbackMethod_EndInvoke_m1218787329 (CSCallbackMethod_t4136956950 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
