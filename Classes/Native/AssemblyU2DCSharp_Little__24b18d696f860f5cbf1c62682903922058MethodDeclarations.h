﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._24b18d696f860f5cbf1c6268957365e9
struct _24b18d696f860f5cbf1c6268957365e9_t2903922058;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._24b18d696f860f5cbf1c6268957365e9::.ctor()
extern "C"  void _24b18d696f860f5cbf1c6268957365e9__ctor_m2039704547 (_24b18d696f860f5cbf1c6268957365e9_t2903922058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._24b18d696f860f5cbf1c6268957365e9::_24b18d696f860f5cbf1c6268957365e9m2(System.Int32)
extern "C"  int32_t _24b18d696f860f5cbf1c6268957365e9__24b18d696f860f5cbf1c6268957365e9m2_m1695274329 (_24b18d696f860f5cbf1c6268957365e9_t2903922058 * __this, int32_t ____24b18d696f860f5cbf1c6268957365e9a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._24b18d696f860f5cbf1c6268957365e9::_24b18d696f860f5cbf1c6268957365e9m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _24b18d696f860f5cbf1c6268957365e9__24b18d696f860f5cbf1c6268957365e9m_m2282260861 (_24b18d696f860f5cbf1c6268957365e9_t2903922058 * __this, int32_t ____24b18d696f860f5cbf1c6268957365e9a0, int32_t ____24b18d696f860f5cbf1c6268957365e9321, int32_t ____24b18d696f860f5cbf1c6268957365e9c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
