﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CombatEntity
struct CombatEntity_t684137495;
// System.Collections.Generic.List`1<AIObject>
struct List_1_t1405465687;
// System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>
struct Dictionary_2_t564979888;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AICtrl
struct  AICtrl_t1930422963  : public Il2CppObject
{
public:
	// CombatEntity AICtrl::entity
	CombatEntity_t684137495 * ___entity_0;
	// System.Collections.Generic.List`1<AIObject> AICtrl::aiList
	List_1_t1405465687 * ___aiList_1;
	// System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>> AICtrl::aiObjDic
	Dictionary_2_t564979888 * ___aiObjDic_2;

public:
	inline static int32_t get_offset_of_entity_0() { return static_cast<int32_t>(offsetof(AICtrl_t1930422963, ___entity_0)); }
	inline CombatEntity_t684137495 * get_entity_0() const { return ___entity_0; }
	inline CombatEntity_t684137495 ** get_address_of_entity_0() { return &___entity_0; }
	inline void set_entity_0(CombatEntity_t684137495 * value)
	{
		___entity_0 = value;
		Il2CppCodeGenWriteBarrier(&___entity_0, value);
	}

	inline static int32_t get_offset_of_aiList_1() { return static_cast<int32_t>(offsetof(AICtrl_t1930422963, ___aiList_1)); }
	inline List_1_t1405465687 * get_aiList_1() const { return ___aiList_1; }
	inline List_1_t1405465687 ** get_address_of_aiList_1() { return &___aiList_1; }
	inline void set_aiList_1(List_1_t1405465687 * value)
	{
		___aiList_1 = value;
		Il2CppCodeGenWriteBarrier(&___aiList_1, value);
	}

	inline static int32_t get_offset_of_aiObjDic_2() { return static_cast<int32_t>(offsetof(AICtrl_t1930422963, ___aiObjDic_2)); }
	inline Dictionary_2_t564979888 * get_aiObjDic_2() const { return ___aiObjDic_2; }
	inline Dictionary_2_t564979888 ** get_address_of_aiObjDic_2() { return &___aiObjDic_2; }
	inline void set_aiObjDic_2(Dictionary_2_t564979888 * value)
	{
		___aiObjDic_2 = value;
		Il2CppCodeGenWriteBarrier(&___aiObjDic_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
