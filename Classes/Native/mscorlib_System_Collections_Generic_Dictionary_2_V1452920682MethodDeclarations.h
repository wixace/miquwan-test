﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2868685323MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,JJCCoefficientCfg>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m809868907(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1452920682 *, Dictionary_2_t2752314969 *, const MethodInfo*))ValueCollection__ctor_m30082295_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,JJCCoefficientCfg>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1973789799(__this, ___item0, method) ((  void (*) (ValueCollection_t1452920682 *, JJCCoefficientCfg_t2755051730 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m701709403_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,JJCCoefficientCfg>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3824409776(__this, method) ((  void (*) (ValueCollection_t1452920682 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3824389796_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,JJCCoefficientCfg>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3725775587(__this, ___item0, method) ((  bool (*) (ValueCollection_t1452920682 *, JJCCoefficientCfg_t2755051730 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m91415663_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,JJCCoefficientCfg>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1077495880(__this, ___item0, method) ((  bool (*) (ValueCollection_t1452920682 *, JJCCoefficientCfg_t2755051730 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4014492884_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,JJCCoefficientCfg>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3356126960(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1452920682 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4048472420_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,JJCCoefficientCfg>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m54682036(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1452920682 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1511207592_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,JJCCoefficientCfg>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m332787139(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1452920682 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3055859895_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,JJCCoefficientCfg>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2000951446(__this, method) ((  bool (*) (ValueCollection_t1452920682 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2661558818_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,JJCCoefficientCfg>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2607419254(__this, method) ((  bool (*) (ValueCollection_t1452920682 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3650032386_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,JJCCoefficientCfg>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2764085480(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1452920682 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m179750644_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,JJCCoefficientCfg>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m4230471666(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1452920682 *, JJCCoefficientCfgU5BU5D_t1710217127*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1295975294_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,JJCCoefficientCfg>::GetEnumerator()
#define ValueCollection_GetEnumerator_m631774811(__this, method) ((  Enumerator_t684148377  (*) (ValueCollection_t1452920682 *, const MethodInfo*))ValueCollection_GetEnumerator_m848222311_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,JJCCoefficientCfg>::get_Count()
#define ValueCollection_get_Count_m3148987312(__this, method) ((  int32_t (*) (ValueCollection_t1452920682 *, const MethodInfo*))ValueCollection_get_Count_m2227591228_gshared)(__this, method)
