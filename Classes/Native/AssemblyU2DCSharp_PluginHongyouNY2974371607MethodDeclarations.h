﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginHongyouNY
struct PluginHongyouNY_t2974371607;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.String
struct String_t;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_PluginHongyouNY2974371607.h"

// System.Void PluginHongyouNY::.ctor()
extern "C"  void PluginHongyouNY__ctor_m528515636 (PluginHongyouNY_t2974371607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::Init()
extern "C"  void PluginHongyouNY_Init_m3847910816 (PluginHongyouNY_t2974371607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginHongyouNY::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginHongyouNY_ReqSDKHttpLogin_m1239082917 (PluginHongyouNY_t2974371607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginHongyouNY::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginHongyouNY_IsLoginSuccess_m784151735 (PluginHongyouNY_t2974371607 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::OpenUserLogin()
extern "C"  void PluginHongyouNY_OpenUserLogin_m3078500486 (PluginHongyouNY_t2974371607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::OnSDKInitSuccess(System.String)
extern "C"  void PluginHongyouNY_OnSDKInitSuccess_m3096126458 (PluginHongyouNY_t2974371607 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::OnSDKInitFail(System.String)
extern "C"  void PluginHongyouNY_OnSDKInitFail_m1866686343 (PluginHongyouNY_t2974371607 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::OnLoginSuccess(System.String)
extern "C"  void PluginHongyouNY_OnLoginSuccess_m1845762617 (PluginHongyouNY_t2974371607 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::OnLoginFail(System.String)
extern "C"  void PluginHongyouNY_OnLoginFail_m1426925992 (PluginHongyouNY_t2974371607 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::OnLoginCancel(System.String)
extern "C"  void PluginHongyouNY_OnLoginCancel_m3325168012 (PluginHongyouNY_t2974371607 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::OnLogoutSuccess(System.String)
extern "C"  void PluginHongyouNY_OnLogoutSuccess_m1387608950 (PluginHongyouNY_t2974371607 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::OnSDKLogout(System.String)
extern "C"  void PluginHongyouNY_OnSDKLogout_m2649178059 (PluginHongyouNY_t2974371607 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::OnGameSwitchSuccess(System.String)
extern "C"  void PluginHongyouNY_OnGameSwitchSuccess_m1486228402 (PluginHongyouNY_t2974371607 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::UserPay(CEvent.ZEvent)
extern "C"  void PluginHongyouNY_UserPay_m3966558252 (PluginHongyouNY_t2974371607 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::OnPaySuccess(System.String)
extern "C"  void PluginHongyouNY_OnPaySuccess_m1500362424 (PluginHongyouNY_t2974371607 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::OnPayFail(System.String)
extern "C"  void PluginHongyouNY_OnPayFail_m2681337225 (PluginHongyouNY_t2974371607 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::OnEnterGame(CEvent.ZEvent)
extern "C"  void PluginHongyouNY_OnEnterGame_m3775554558 (PluginHongyouNY_t2974371607 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::OnCreatRole(CEvent.ZEvent)
extern "C"  void PluginHongyouNY_OnCreatRole_m310003113 (PluginHongyouNY_t2974371607 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::ClollectData(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginHongyouNY_ClollectData_m2996871712 (PluginHongyouNY_t2974371607 * __this, String_t* ___type0, String_t* ___serverid1, String_t* ___servername2, String_t* ___roleid3, String_t* ___rolename4, String_t* ___rolelevel5, String_t* ___extend6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::init(System.String,System.String)
extern "C"  void PluginHongyouNY_init_m4230996542 (PluginHongyouNY_t2974371607 * __this, String_t* ___AppID0, String_t* ___channel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::login()
extern "C"  void PluginHongyouNY_login_m50530459 (PluginHongyouNY_t2974371607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::goZF(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginHongyouNY_goZF_m3933299558 (PluginHongyouNY_t2974371607 * __this, String_t* ___UserID0, String_t* ___APPID1, String_t* ___orderId2, String_t* ___ProductName3, String_t* ___ProductNumber4, String_t* ___amount5, String_t* ___extstr6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::coloectData(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginHongyouNY_coloectData_m1429781447 (PluginHongyouNY_t2974371607 * __this, String_t* ___dataType0, String_t* ___serverId1, String_t* ___RoleName2, String_t* ___Extend3, String_t* ___RoleLevel4, String_t* ___ServerName5, String_t* ___RoleID6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::<OnLogoutSuccess>m__429()
extern "C"  void PluginHongyouNY_U3COnLogoutSuccessU3Em__429_m1272867680 (PluginHongyouNY_t2974371607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginHongyouNY_ilo_AddEventListener1_m2608855104 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginHongyouNY::ilo_get_isSdkLogin2(VersionMgr)
extern "C"  bool PluginHongyouNY_ilo_get_isSdkLogin2_m2371703331 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginHongyouNY::ilo_get_PluginsSdkMgr3()
extern "C"  PluginsSdkMgr_t3884624670 * PluginHongyouNY_ilo_get_PluginsSdkMgr3_m610627820 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginHongyouNY::ilo_get_Instance4()
extern "C"  VersionMgr_t1322950208 * PluginHongyouNY_ilo_get_Instance4_m546758930 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::ilo_Logout5(System.Action)
extern "C"  void PluginHongyouNY_ilo_Logout5_m2339963603 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::ilo_goZF6(PluginHongyouNY,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginHongyouNY_ilo_goZF6_m847456702 (Il2CppObject * __this /* static, unused */, PluginHongyouNY_t2974371607 * ____this0, String_t* ___UserID1, String_t* ___APPID2, String_t* ___orderId3, String_t* ___ProductName4, String_t* ___ProductNumber5, String_t* ___amount6, String_t* ___extstr7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::ilo_ClollectData7(PluginHongyouNY,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginHongyouNY_ilo_ClollectData7_m632599971 (Il2CppObject * __this /* static, unused */, PluginHongyouNY_t2974371607 * ____this0, String_t* ___type1, String_t* ___serverid2, String_t* ___servername3, String_t* ___roleid4, String_t* ___rolename5, String_t* ___rolelevel6, String_t* ___extend7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHongyouNY::ilo_OpenUserLogin8(PluginHongyouNY)
extern "C"  void PluginHongyouNY_ilo_OpenUserLogin8_m1348056916 (Il2CppObject * __this /* static, unused */, PluginHongyouNY_t2974371607 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
