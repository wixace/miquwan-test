﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UITweener
struct UITweener_t105489188;
// EventDelegate/Callback
struct Callback_t1094463061;
// EventDelegate
struct EventDelegate_t4004424223;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t1077642479;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AnimationOrTween_Direction1859285089.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_EventDelegate4004424223.h"
#include "AssemblyU2DCSharp_UITweener105489188.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UITweener::.ctor()
extern "C"  void UITweener__ctor_m2521610951 (UITweener_t105489188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UITweener::get_amountPerDelta()
extern "C"  float UITweener_get_amountPerDelta_m342139981 (UITweener_t105489188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UITweener::get_tweenFactor()
extern "C"  float UITweener_get_tweenFactor_m3023342018 (UITweener_t105489188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::set_tweenFactor(System.Single)
extern "C"  void UITweener_set_tweenFactor_m826357225 (UITweener_t105489188 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnimationOrTween.Direction UITweener::get_direction()
extern "C"  int32_t UITweener_get_direction_m1737209411 (UITweener_t105489188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::Reset()
extern "C"  void UITweener_Reset_m168043892 (UITweener_t105489188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::Start()
extern "C"  void UITweener_Start_m1468748743 (UITweener_t105489188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::Update()
extern "C"  void UITweener_Update_m2587390246 (UITweener_t105489188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::SetOnFinished(EventDelegate/Callback)
extern "C"  void UITweener_SetOnFinished_m2532336547 (UITweener_t105489188 * __this, Callback_t1094463061 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::SetOnFinished(EventDelegate)
extern "C"  void UITweener_SetOnFinished_m4129108443 (UITweener_t105489188 * __this, EventDelegate_t4004424223 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::AddOnFinished(EventDelegate/Callback)
extern "C"  void UITweener_AddOnFinished_m1398296610 (UITweener_t105489188 * __this, Callback_t1094463061 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::AddOnFinished(EventDelegate)
extern "C"  void UITweener_AddOnFinished_m2961468028 (UITweener_t105489188 * __this, EventDelegate_t4004424223 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::RemoveOnFinished(EventDelegate)
extern "C"  void UITweener_RemoveOnFinished_m1549062977 (UITweener_t105489188 * __this, EventDelegate_t4004424223 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::OnDisable()
extern "C"  void UITweener_OnDisable_m3864744878 (UITweener_t105489188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::Sample(System.Single,System.Boolean)
extern "C"  void UITweener_Sample_m1154056121 (UITweener_t105489188 * __this, float ___factor0, bool ___isFinished1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UITweener::BounceLogic(System.Single)
extern "C"  float UITweener_BounceLogic_m384101156 (UITweener_t105489188 * __this, float ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::Play()
extern "C"  void UITweener_Play_m3833433041 (UITweener_t105489188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::PlayForward()
extern "C"  void UITweener_PlayForward_m3184544150 (UITweener_t105489188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::PlayReverse()
extern "C"  void UITweener_PlayReverse_m1477146227 (UITweener_t105489188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::Play(System.Boolean)
extern "C"  void UITweener_Play_m1190576008 (UITweener_t105489188 * __this, bool ___forward0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::ResetToBeginning()
extern "C"  void UITweener_ResetToBeginning_m292257456 (UITweener_t105489188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::Toggle()
extern "C"  void UITweener_Toggle_m48266481 (UITweener_t105489188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::SetStartToCurrentValue()
extern "C"  void UITweener_SetStartToCurrentValue_m1490938032 (UITweener_t105489188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::SetEndToCurrentValue()
extern "C"  void UITweener_SetEndToCurrentValue_m3403311529 (UITweener_t105489188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::OnDestory()
extern "C"  void UITweener_OnDestory_m1744628442 (UITweener_t105489188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::ilo_SetStartToCurrentValue1(UITweener)
extern "C"  void UITweener_ilo_SetStartToCurrentValue1_m3582598974 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UITweener::ilo_get_time2()
extern "C"  float UITweener_ilo_get_time2_m3210045338 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate UITweener::ilo_Set3(System.Collections.Generic.List`1<EventDelegate>,EventDelegate/Callback)
extern "C"  EventDelegate_t4004424223 * UITweener_ilo_Set3_m947215841 (Il2CppObject * __this /* static, unused */, List_1_t1077642479 * ___list0, Callback_t1094463061 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate UITweener::ilo_Add4(System.Collections.Generic.List`1<EventDelegate>,EventDelegate/Callback)
extern "C"  EventDelegate_t4004424223 * UITweener_ilo_Add4_m3345455231 (Il2CppObject * __this /* static, unused */, List_1_t1077642479 * ___list0, Callback_t1094463061 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UITweener::ilo_BounceLogic5(UITweener,System.Single)
extern "C"  float UITweener_ilo_BounceLogic5_m1622014416 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, float ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweener::ilo_Play6(UITweener,System.Boolean)
extern "C"  void UITweener_ilo_Play6_m464484739 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, bool ___forward1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UITweener::ilo_get_amountPerDelta7(UITweener)
extern "C"  float UITweener_ilo_get_amountPerDelta7_m28103253 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UITweener::ilo_GetHierarchy8(UnityEngine.GameObject)
extern "C"  String_t* UITweener_ilo_GetHierarchy8_m1626973574 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
