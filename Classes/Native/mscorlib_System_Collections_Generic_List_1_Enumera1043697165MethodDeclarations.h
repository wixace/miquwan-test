﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.TEdge>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m611413560(__this, ___l0, method) ((  void (*) (Enumerator_t1043697165 *, List_1_t1024024395 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.TEdge>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3538920218(__this, method) ((  void (*) (Enumerator_t1043697165 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.TEdge>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3110906640(__this, method) ((  Il2CppObject * (*) (Enumerator_t1043697165 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.TEdge>::Dispose()
#define Enumerator_Dispose_m1784875549(__this, method) ((  void (*) (Enumerator_t1043697165 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.TEdge>::VerifyState()
#define Enumerator_VerifyState_m2023387862(__this, method) ((  void (*) (Enumerator_t1043697165 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.TEdge>::MoveNext()
#define Enumerator_MoveNext_m242914506(__this, method) ((  bool (*) (Enumerator_t1043697165 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.TEdge>::get_Current()
#define Enumerator_get_Current_m2333523183(__this, method) ((  TEdge_t3950806139 * (*) (Enumerator_t1043697165 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
