﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::.ctor()
#define List_1__ctor_m3677632537(__this, method) ((  void (*) (List_1_t3268995646 *, const MethodInfo*))List_1__ctor_m1239231859_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m2506429021(__this, ___collection0, method) ((  void (*) (List_1_t3268995646 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::.ctor(System.Int32)
#define List_1__ctor_m2626900787(__this, ___capacity0, method) ((  void (*) (List_1_t3268995646 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::.cctor()
#define List_1__cctor_m1649805067(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1343156588(__this, method) ((  Il2CppObject* (*) (List_1_t3268995646 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m3603801762(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3268995646 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3028234929(__this, method) ((  Il2CppObject * (*) (List_1_t3268995646 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m3351983532(__this, ___item0, method) ((  int32_t (*) (List_1_t3268995646 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m2600812692(__this, ___item0, method) ((  bool (*) (List_1_t3268995646 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m2380314116(__this, ___item0, method) ((  int32_t (*) (List_1_t3268995646 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m650133623(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3268995646 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m3120624465(__this, ___item0, method) ((  void (*) (List_1_t3268995646 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m962103189(__this, method) ((  bool (*) (List_1_t3268995646 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3890830920(__this, method) ((  bool (*) (List_1_t3268995646 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m1675134714(__this, method) ((  Il2CppObject * (*) (List_1_t3268995646 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m333311747(__this, method) ((  bool (*) (List_1_t3268995646 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m1434592150(__this, method) ((  bool (*) (List_1_t3268995646 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m257335809(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3268995646 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m2622106254(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3268995646 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::Add(T)
#define List_1_Add_m3371993353(__this, ___item0, method) ((  void (*) (List_1_t3268995646 *, CameraShotVO_t1900810094 *, const MethodInfo*))List_1_Add_m933592675_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m2530564376(__this, ___newCount0, method) ((  void (*) (List_1_t3268995646 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m2375723983(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3268995646 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m590371457_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m4233842710(__this, ___collection0, method) ((  void (*) (List_1_t3268995646 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m3494814934(__this, ___enumerable0, method) ((  void (*) (List_1_t3268995646 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m776477697(__this, ___collection0, method) ((  void (*) (List_1_t3268995646 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::AsReadOnly()
#define List_1_AsReadOnly_m1790127716(__this, method) ((  ReadOnlyCollection_1_t3457887630 * (*) (List_1_t3268995646 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::BinarySearch(T)
#define List_1_BinarySearch_m2265689071(__this, ___item0, method) ((  int32_t (*) (List_1_t3268995646 *, CameraShotVO_t1900810094 *, const MethodInfo*))List_1_BinarySearch_m2761349225_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::Clear()
#define List_1_Clear_m1083765828(__this, method) ((  void (*) (List_1_t3268995646 *, const MethodInfo*))List_1_Clear_m2940332446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::Contains(T)
#define List_1_Contains_m2561286143(__this, ___item0, method) ((  bool (*) (List_1_t3268995646 *, CameraShotVO_t1900810094 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m4053739021(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3268995646 *, CameraShotVOU5BU5D_t658558747*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::Find(System.Predicate`1<T>)
#define List_1_Find_m1727349017(__this, ___match0, method) ((  CameraShotVO_t1900810094 * (*) (List_1_t3268995646 *, Predicate_1_t1511866977 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m772306806(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1511866977 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m2128310867(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3268995646 *, int32_t, int32_t, Predicate_1_t1511866977 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::GetEnumerator()
#define List_1_GetEnumerator_m2282375612(__this, method) ((  Enumerator_t3288668416  (*) (List_1_t3268995646 *, const MethodInfo*))List_1_GetEnumerator_m1919240000_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::IndexOf(T)
#define List_1_IndexOf_m3548157849(__this, ___item0, method) ((  int32_t (*) (List_1_t3268995646 *, CameraShotVO_t1900810094 *, const MethodInfo*))List_1_IndexOf_m2776798407_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m4276789092(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3268995646 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m3800106205(__this, ___index0, method) ((  void (*) (List_1_t3268995646 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::Insert(System.Int32,T)
#define List_1_Insert_m821648324(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3268995646 *, int32_t, CameraShotVO_t1900810094 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m1915483449(__this, ___collection0, method) ((  void (*) (List_1_t3268995646 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::Remove(T)
#define List_1_Remove_m2308232122(__this, ___item0, method) ((  bool (*) (List_1_t3268995646 *, CameraShotVO_t1900810094 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m2761995932(__this, ___match0, method) ((  int32_t (*) (List_1_t3268995646 *, Predicate_1_t1511866977 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m816996723(__this, ___index0, method) ((  void (*) (List_1_t3268995646 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2208877785_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m368211373(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3268995646 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m856857915_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::Reverse()
#define List_1_Reverse_m1702241442(__this, method) ((  void (*) (List_1_t3268995646 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::Sort()
#define List_1_Sort_m3740675648(__this, method) ((  void (*) (List_1_t3268995646 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m2252873636(__this, ___comparer0, method) ((  void (*) (List_1_t3268995646 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m3253471891(__this, ___comparison0, method) ((  void (*) (List_1_t3268995646 *, Comparison_1_t617171281 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::ToArray()
#define List_1_ToArray_m3251959291(__this, method) ((  CameraShotVOU5BU5D_t658558747* (*) (List_1_t3268995646 *, const MethodInfo*))List_1_ToArray_m1449333879_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::TrimExcess()
#define List_1_TrimExcess_m3470368217(__this, method) ((  void (*) (List_1_t3268995646 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::get_Capacity()
#define List_1_get_Capacity_m3991214921(__this, method) ((  int32_t (*) (List_1_t3268995646 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m4035720490(__this, ___value0, method) ((  void (*) (List_1_t3268995646 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::get_Count()
#define List_1_get_Count_m1709261867(__this, method) ((  int32_t (*) (List_1_t3268995646 *, const MethodInfo*))List_1_get_Count_m3549598589_gshared)(__this, method)
// T System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::get_Item(System.Int32)
#define List_1_get_Item_m1362379210(__this, ___index0, method) ((  CameraShotVO_t1900810094 * (*) (List_1_t3268995646 *, int32_t, const MethodInfo*))List_1_get_Item_m404118264_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<CameraShotMgr/CameraShotVO>::set_Item(System.Int32,T)
#define List_1_set_Item_m1139653531(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3268995646 *, int32_t, CameraShotVO_t1900810094 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
