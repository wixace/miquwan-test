﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Artngame.SKYMASTER.Displace
struct Displace_t1456355803;

#include "codegen/il2cpp-codegen.h"

// System.Void Artngame.SKYMASTER.Displace::.ctor()
extern "C"  void Displace__ctor_m3794432566 (Displace_t1456355803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Artngame.SKYMASTER.Displace::Awake()
extern "C"  void Displace_Awake_m4032037785 (Displace_t1456355803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Artngame.SKYMASTER.Displace::OnEnable()
extern "C"  void Displace_OnEnable_m2767888784 (Displace_t1456355803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Artngame.SKYMASTER.Displace::OnDisable()
extern "C"  void Displace_OnDisable_m346143645 (Displace_t1456355803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
