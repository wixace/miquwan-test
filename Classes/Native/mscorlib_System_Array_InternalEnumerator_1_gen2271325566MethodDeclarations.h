﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2271325566.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23488982890.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4066720976_gshared (InternalEnumerator_1_t2271325566 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m4066720976(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2271325566 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4066720976_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2944771792_gshared (InternalEnumerator_1_t2271325566 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2944771792(__this, method) ((  void (*) (InternalEnumerator_1_t2271325566 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2944771792_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1146846406_gshared (InternalEnumerator_1_t2271325566 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1146846406(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2271325566 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1146846406_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1131514535_gshared (InternalEnumerator_1_t2271325566 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1131514535(__this, method) ((  void (*) (InternalEnumerator_1_t2271325566 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1131514535_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2874322560_gshared (InternalEnumerator_1_t2271325566 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2874322560(__this, method) ((  bool (*) (InternalEnumerator_1_t2271325566 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2874322560_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>>::get_Current()
extern "C"  KeyValuePair_2_t3488982890  InternalEnumerator_1_get_Current_m1885822201_gshared (InternalEnumerator_1_t2271325566 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1885822201(__this, method) ((  KeyValuePair_2_t3488982890  (*) (InternalEnumerator_1_t2271325566 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1885822201_gshared)(__this, method)
