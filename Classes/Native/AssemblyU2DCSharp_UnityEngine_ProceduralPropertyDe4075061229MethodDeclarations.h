﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ProceduralPropertyDescriptionGenerated
struct UnityEngine_ProceduralPropertyDescriptionGenerated_t4075061229;
// JSVCall
struct JSVCall_t3708497963;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine_ProceduralPropertyDescriptionGenerated::.ctor()
extern "C"  void UnityEngine_ProceduralPropertyDescriptionGenerated__ctor_m3174546254 (UnityEngine_ProceduralPropertyDescriptionGenerated_t4075061229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralPropertyDescriptionGenerated::ProceduralPropertyDescription_ProceduralPropertyDescription1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralPropertyDescriptionGenerated_ProceduralPropertyDescription_ProceduralPropertyDescription1_m3799286678 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralPropertyDescriptionGenerated::ProceduralPropertyDescription_name(JSVCall)
extern "C"  void UnityEngine_ProceduralPropertyDescriptionGenerated_ProceduralPropertyDescription_name_m4157056497 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralPropertyDescriptionGenerated::ProceduralPropertyDescription_label(JSVCall)
extern "C"  void UnityEngine_ProceduralPropertyDescriptionGenerated_ProceduralPropertyDescription_label_m4099788092 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralPropertyDescriptionGenerated::ProceduralPropertyDescription_group(JSVCall)
extern "C"  void UnityEngine_ProceduralPropertyDescriptionGenerated_ProceduralPropertyDescription_group_m3845300753 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralPropertyDescriptionGenerated::ProceduralPropertyDescription_type(JSVCall)
extern "C"  void UnityEngine_ProceduralPropertyDescriptionGenerated_ProceduralPropertyDescription_type_m103769634 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralPropertyDescriptionGenerated::ProceduralPropertyDescription_hasRange(JSVCall)
extern "C"  void UnityEngine_ProceduralPropertyDescriptionGenerated_ProceduralPropertyDescription_hasRange_m769318329 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralPropertyDescriptionGenerated::ProceduralPropertyDescription_minimum(JSVCall)
extern "C"  void UnityEngine_ProceduralPropertyDescriptionGenerated_ProceduralPropertyDescription_minimum_m4275042018 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralPropertyDescriptionGenerated::ProceduralPropertyDescription_maximum(JSVCall)
extern "C"  void UnityEngine_ProceduralPropertyDescriptionGenerated_ProceduralPropertyDescription_maximum_m43943184 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralPropertyDescriptionGenerated::ProceduralPropertyDescription_step(JSVCall)
extern "C"  void UnityEngine_ProceduralPropertyDescriptionGenerated_ProceduralPropertyDescription_step_m176706256 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralPropertyDescriptionGenerated::ProceduralPropertyDescription_enumOptions(JSVCall)
extern "C"  void UnityEngine_ProceduralPropertyDescriptionGenerated_ProceduralPropertyDescription_enumOptions_m3262395507 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralPropertyDescriptionGenerated::ProceduralPropertyDescription_componentLabels(JSVCall)
extern "C"  void UnityEngine_ProceduralPropertyDescriptionGenerated_ProceduralPropertyDescription_componentLabels_m1465425236 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralPropertyDescriptionGenerated::__Register()
extern "C"  void UnityEngine_ProceduralPropertyDescriptionGenerated___Register_m4062224953 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UnityEngine_ProceduralPropertyDescriptionGenerated::<ProceduralPropertyDescription_enumOptions>m__2CB()
extern "C"  StringU5BU5D_t4054002952* UnityEngine_ProceduralPropertyDescriptionGenerated_U3CProceduralPropertyDescription_enumOptionsU3Em__2CB_m3793802849 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UnityEngine_ProceduralPropertyDescriptionGenerated::<ProceduralPropertyDescription_componentLabels>m__2CC()
extern "C"  StringU5BU5D_t4054002952* UnityEngine_ProceduralPropertyDescriptionGenerated_U3CProceduralPropertyDescription_componentLabelsU3Em__2CC_m782119555 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ProceduralPropertyDescriptionGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_ProceduralPropertyDescriptionGenerated_ilo_getObject1_m3694738692 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralPropertyDescriptionGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_ProceduralPropertyDescriptionGenerated_ilo_attachFinalizerObject2_m2692782994 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralPropertyDescriptionGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UnityEngine_ProceduralPropertyDescriptionGenerated_ilo_addJSCSRel3_m3800566796 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_ProceduralPropertyDescriptionGenerated::ilo_getStringS4(System.Int32)
extern "C"  String_t* UnityEngine_ProceduralPropertyDescriptionGenerated_ilo_getStringS4_m3578580239 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralPropertyDescriptionGenerated::ilo_setStringS5(System.Int32,System.String)
extern "C"  void UnityEngine_ProceduralPropertyDescriptionGenerated_ilo_setStringS5_m1167102235 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralPropertyDescriptionGenerated::ilo_setEnum6(System.Int32,System.Int32)
extern "C"  void UnityEngine_ProceduralPropertyDescriptionGenerated_ilo_setEnum6_m2378315210 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralPropertyDescriptionGenerated::ilo_setSingle7(System.Int32,System.Single)
extern "C"  void UnityEngine_ProceduralPropertyDescriptionGenerated_ilo_setSingle7_m291505516 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ProceduralPropertyDescriptionGenerated::ilo_getArrayLength8(System.Int32)
extern "C"  int32_t UnityEngine_ProceduralPropertyDescriptionGenerated_ilo_getArrayLength8_m1713018795 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
