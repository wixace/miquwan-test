﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t2154290273;
// Mihua.Asset.OnLoadAsset
struct OnLoadAsset_t4181543125;

#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mihua.Asset.ABLoadOperation.AssetOperationLoad
struct  AssetOperationLoad_t3754788771  : public AssetOperation_t778728221
{
public:
	// System.String Mihua.Asset.ABLoadOperation.AssetOperationLoad::m_AssetName
	String_t* ___m_AssetName_2;
	// UnityEngine.AssetBundleRequest Mihua.Asset.ABLoadOperation.AssetOperationLoad::request
	AssetBundleRequest_t2154290273 * ___request_3;
	// Mihua.Asset.OnLoadAsset Mihua.Asset.ABLoadOperation.AssetOperationLoad::m_OnLoadAsset
	OnLoadAsset_t4181543125 * ___m_OnLoadAsset_4;

public:
	inline static int32_t get_offset_of_m_AssetName_2() { return static_cast<int32_t>(offsetof(AssetOperationLoad_t3754788771, ___m_AssetName_2)); }
	inline String_t* get_m_AssetName_2() const { return ___m_AssetName_2; }
	inline String_t** get_address_of_m_AssetName_2() { return &___m_AssetName_2; }
	inline void set_m_AssetName_2(String_t* value)
	{
		___m_AssetName_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_AssetName_2, value);
	}

	inline static int32_t get_offset_of_request_3() { return static_cast<int32_t>(offsetof(AssetOperationLoad_t3754788771, ___request_3)); }
	inline AssetBundleRequest_t2154290273 * get_request_3() const { return ___request_3; }
	inline AssetBundleRequest_t2154290273 ** get_address_of_request_3() { return &___request_3; }
	inline void set_request_3(AssetBundleRequest_t2154290273 * value)
	{
		___request_3 = value;
		Il2CppCodeGenWriteBarrier(&___request_3, value);
	}

	inline static int32_t get_offset_of_m_OnLoadAsset_4() { return static_cast<int32_t>(offsetof(AssetOperationLoad_t3754788771, ___m_OnLoadAsset_4)); }
	inline OnLoadAsset_t4181543125 * get_m_OnLoadAsset_4() const { return ___m_OnLoadAsset_4; }
	inline OnLoadAsset_t4181543125 ** get_address_of_m_OnLoadAsset_4() { return &___m_OnLoadAsset_4; }
	inline void set_m_OnLoadAsset_4(OnLoadAsset_t4181543125 * value)
	{
		___m_OnLoadAsset_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_OnLoadAsset_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
