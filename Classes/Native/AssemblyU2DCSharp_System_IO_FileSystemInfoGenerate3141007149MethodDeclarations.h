﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_IO_FileSystemInfoGenerated
struct System_IO_FileSystemInfoGenerated_t3141007149;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_IO_FileSystemInfoGenerated::.ctor()
extern "C"  void System_IO_FileSystemInfoGenerated__ctor_m1655941342 (System_IO_FileSystemInfoGenerated_t3141007149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_FileSystemInfoGenerated::FileSystemInfo_Exists(JSVCall)
extern "C"  void System_IO_FileSystemInfoGenerated_FileSystemInfo_Exists_m2311187209 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_FileSystemInfoGenerated::FileSystemInfo_Name(JSVCall)
extern "C"  void System_IO_FileSystemInfoGenerated_FileSystemInfo_Name_m2752771450 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_FileSystemInfoGenerated::FileSystemInfo_FullName(JSVCall)
extern "C"  void System_IO_FileSystemInfoGenerated_FileSystemInfo_FullName_m338148267 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_FileSystemInfoGenerated::FileSystemInfo_Extension(JSVCall)
extern "C"  void System_IO_FileSystemInfoGenerated_FileSystemInfo_Extension_m3708013928 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_FileSystemInfoGenerated::FileSystemInfo_Attributes(JSVCall)
extern "C"  void System_IO_FileSystemInfoGenerated_FileSystemInfo_Attributes_m809198 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_FileSystemInfoGenerated::FileSystemInfo_CreationTime(JSVCall)
extern "C"  void System_IO_FileSystemInfoGenerated_FileSystemInfo_CreationTime_m3466189785 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_FileSystemInfoGenerated::FileSystemInfo_CreationTimeUtc(JSVCall)
extern "C"  void System_IO_FileSystemInfoGenerated_FileSystemInfo_CreationTimeUtc_m2510365007 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_FileSystemInfoGenerated::FileSystemInfo_LastAccessTime(JSVCall)
extern "C"  void System_IO_FileSystemInfoGenerated_FileSystemInfo_LastAccessTime_m3024182078 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_FileSystemInfoGenerated::FileSystemInfo_LastAccessTimeUtc(JSVCall)
extern "C"  void System_IO_FileSystemInfoGenerated_FileSystemInfo_LastAccessTimeUtc_m3028495306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_FileSystemInfoGenerated::FileSystemInfo_LastWriteTime(JSVCall)
extern "C"  void System_IO_FileSystemInfoGenerated_FileSystemInfo_LastWriteTime_m2411560369 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_FileSystemInfoGenerated::FileSystemInfo_LastWriteTimeUtc(JSVCall)
extern "C"  void System_IO_FileSystemInfoGenerated_FileSystemInfo_LastWriteTimeUtc_m1731203191 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileSystemInfoGenerated::FileSystemInfo_Delete(JSVCall,System.Int32)
extern "C"  bool System_IO_FileSystemInfoGenerated_FileSystemInfo_Delete_m3746023473 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileSystemInfoGenerated::FileSystemInfo_GetObjectData__SerializationInfo__StreamingContext(JSVCall,System.Int32)
extern "C"  bool System_IO_FileSystemInfoGenerated_FileSystemInfo_GetObjectData__SerializationInfo__StreamingContext_m2416681042 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileSystemInfoGenerated::FileSystemInfo_Refresh(JSVCall,System.Int32)
extern "C"  bool System_IO_FileSystemInfoGenerated_FileSystemInfo_Refresh_m166080663 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_FileSystemInfoGenerated::__Register()
extern "C"  void System_IO_FileSystemInfoGenerated___Register_m3821711017 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_FileSystemInfoGenerated::ilo_setBooleanS1(System.Int32,System.Boolean)
extern "C"  void System_IO_FileSystemInfoGenerated_ilo_setBooleanS1_m98268367 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_FileSystemInfoGenerated::ilo_setStringS2(System.Int32,System.String)
extern "C"  void System_IO_FileSystemInfoGenerated_ilo_setStringS2_m4208019752 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_IO_FileSystemInfoGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_IO_FileSystemInfoGenerated_ilo_setObject3_m2729897874 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_IO_FileSystemInfoGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_IO_FileSystemInfoGenerated_ilo_getObject4_m3901983370 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
