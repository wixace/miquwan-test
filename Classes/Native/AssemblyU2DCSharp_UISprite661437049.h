﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIAtlas
struct UIAtlas_t281921111;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UISpriteData
struct UISpriteData_t3578345923;

#include "AssemblyU2DCSharp_UIBasicSprite2501337439.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISprite
struct  UISprite_t661437049  : public UIBasicSprite_t2501337439
{
public:
	// UIAtlas UISprite::mAtlas
	UIAtlas_t281921111 * ___mAtlas_67;
	// System.String UISprite::mSpriteName
	String_t* ___mSpriteName_68;
	// System.Object UISprite::Data
	Il2CppObject * ___Data_69;
	// System.Boolean UISprite::mFillCenter
	bool ___mFillCenter_70;
	// System.Boolean UISprite::IsMathConvert
	bool ___IsMathConvert_71;
	// UISpriteData UISprite::mSprite
	UISpriteData_t3578345923 * ___mSprite_72;
	// System.Boolean UISprite::mSpriteSet
	bool ___mSpriteSet_73;

public:
	inline static int32_t get_offset_of_mAtlas_67() { return static_cast<int32_t>(offsetof(UISprite_t661437049, ___mAtlas_67)); }
	inline UIAtlas_t281921111 * get_mAtlas_67() const { return ___mAtlas_67; }
	inline UIAtlas_t281921111 ** get_address_of_mAtlas_67() { return &___mAtlas_67; }
	inline void set_mAtlas_67(UIAtlas_t281921111 * value)
	{
		___mAtlas_67 = value;
		Il2CppCodeGenWriteBarrier(&___mAtlas_67, value);
	}

	inline static int32_t get_offset_of_mSpriteName_68() { return static_cast<int32_t>(offsetof(UISprite_t661437049, ___mSpriteName_68)); }
	inline String_t* get_mSpriteName_68() const { return ___mSpriteName_68; }
	inline String_t** get_address_of_mSpriteName_68() { return &___mSpriteName_68; }
	inline void set_mSpriteName_68(String_t* value)
	{
		___mSpriteName_68 = value;
		Il2CppCodeGenWriteBarrier(&___mSpriteName_68, value);
	}

	inline static int32_t get_offset_of_Data_69() { return static_cast<int32_t>(offsetof(UISprite_t661437049, ___Data_69)); }
	inline Il2CppObject * get_Data_69() const { return ___Data_69; }
	inline Il2CppObject ** get_address_of_Data_69() { return &___Data_69; }
	inline void set_Data_69(Il2CppObject * value)
	{
		___Data_69 = value;
		Il2CppCodeGenWriteBarrier(&___Data_69, value);
	}

	inline static int32_t get_offset_of_mFillCenter_70() { return static_cast<int32_t>(offsetof(UISprite_t661437049, ___mFillCenter_70)); }
	inline bool get_mFillCenter_70() const { return ___mFillCenter_70; }
	inline bool* get_address_of_mFillCenter_70() { return &___mFillCenter_70; }
	inline void set_mFillCenter_70(bool value)
	{
		___mFillCenter_70 = value;
	}

	inline static int32_t get_offset_of_IsMathConvert_71() { return static_cast<int32_t>(offsetof(UISprite_t661437049, ___IsMathConvert_71)); }
	inline bool get_IsMathConvert_71() const { return ___IsMathConvert_71; }
	inline bool* get_address_of_IsMathConvert_71() { return &___IsMathConvert_71; }
	inline void set_IsMathConvert_71(bool value)
	{
		___IsMathConvert_71 = value;
	}

	inline static int32_t get_offset_of_mSprite_72() { return static_cast<int32_t>(offsetof(UISprite_t661437049, ___mSprite_72)); }
	inline UISpriteData_t3578345923 * get_mSprite_72() const { return ___mSprite_72; }
	inline UISpriteData_t3578345923 ** get_address_of_mSprite_72() { return &___mSprite_72; }
	inline void set_mSprite_72(UISpriteData_t3578345923 * value)
	{
		___mSprite_72 = value;
		Il2CppCodeGenWriteBarrier(&___mSprite_72, value);
	}

	inline static int32_t get_offset_of_mSpriteSet_73() { return static_cast<int32_t>(offsetof(UISprite_t661437049, ___mSpriteSet_73)); }
	inline bool get_mSpriteSet_73() const { return ___mSpriteSet_73; }
	inline bool* get_address_of_mSpriteSet_73() { return &___mSpriteSet_73; }
	inline void set_mSpriteSet_73(bool value)
	{
		___mSpriteSet_73 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
