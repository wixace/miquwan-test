﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.BlobSerializer
struct BlobSerializer_t2233661609;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"

// System.Void ProtoBuf.Serializers.BlobSerializer::.ctor(ProtoBuf.Meta.TypeModel,System.Boolean)
extern "C"  void BlobSerializer__ctor_m1247214656 (BlobSerializer_t2233661609 * __this, TypeModel_t2730011105 * ___model0, bool ___overwriteList1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.BlobSerializer::.cctor()
extern "C"  void BlobSerializer__cctor_m3097595105 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.BlobSerializer::ProtoBuf.Serializers.IProtoSerializer.get_RequiresOldValue()
extern "C"  bool BlobSerializer_ProtoBuf_Serializers_IProtoSerializer_get_RequiresOldValue_m3443442834 (BlobSerializer_t2233661609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.BlobSerializer::ProtoBuf.Serializers.IProtoSerializer.get_ReturnsValue()
extern "C"  bool BlobSerializer_ProtoBuf_Serializers_IProtoSerializer_get_ReturnsValue_m4096249896 (BlobSerializer_t2233661609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.BlobSerializer::get_ExpectedType()
extern "C"  Type_t * BlobSerializer_get_ExpectedType_m711446073 (BlobSerializer_t2233661609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.BlobSerializer::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * BlobSerializer_Read_m4244724387 (BlobSerializer_t2233661609 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.BlobSerializer::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void BlobSerializer_Write_m2695167683 (BlobSerializer_t2233661609 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ProtoBuf.Serializers.BlobSerializer::ilo_AppendBytes1(System.Byte[],ProtoBuf.ProtoReader)
extern "C"  ByteU5BU5D_t4260760469* BlobSerializer_ilo_AppendBytes1_m2735042166 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___value0, ProtoReader_t3962509489 * ___reader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
