﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ReflectionUtils/<GetFieldsAndProperties>c__AnonStorey147
struct U3CGetFieldsAndPropertiesU3Ec__AnonStorey147_t3893559254;
// System.Reflection.MemberInfo
struct MemberInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"

// System.Void Newtonsoft.Json.Utilities.ReflectionUtils/<GetFieldsAndProperties>c__AnonStorey147::.ctor()
extern "C"  void U3CGetFieldsAndPropertiesU3Ec__AnonStorey147__ctor_m4086935941 (U3CGetFieldsAndPropertiesU3Ec__AnonStorey147_t3893559254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils/<GetFieldsAndProperties>c__AnonStorey147::<>m__3B0(System.Reflection.MemberInfo)
extern "C"  bool U3CGetFieldsAndPropertiesU3Ec__AnonStorey147_U3CU3Em__3B0_m1686410109 (U3CGetFieldsAndPropertiesU3Ec__AnonStorey147_t3893559254 * __this, MemberInfo_t * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
