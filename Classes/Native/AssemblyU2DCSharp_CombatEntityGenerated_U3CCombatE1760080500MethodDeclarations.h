﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CombatEntityGenerated/<CombatEntity_RequestAstarPath_GetDelegate_member67_arg1>c__AnonStorey58
struct U3CCombatEntity_RequestAstarPath_GetDelegate_member67_arg1U3Ec__AnonStorey58_t1760080500;
// Pathfinding.Path
struct Path_t1974241691;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"

// System.Void CombatEntityGenerated/<CombatEntity_RequestAstarPath_GetDelegate_member67_arg1>c__AnonStorey58::.ctor()
extern "C"  void U3CCombatEntity_RequestAstarPath_GetDelegate_member67_arg1U3Ec__AnonStorey58__ctor_m4233407655 (U3CCombatEntity_RequestAstarPath_GetDelegate_member67_arg1U3Ec__AnonStorey58_t1760080500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated/<CombatEntity_RequestAstarPath_GetDelegate_member67_arg1>c__AnonStorey58::<>m__36(Pathfinding.Path)
extern "C"  void U3CCombatEntity_RequestAstarPath_GetDelegate_member67_arg1U3Ec__AnonStorey58_U3CU3Em__36_m190258404 (U3CCombatEntity_RequestAstarPath_GetDelegate_member67_arg1U3Ec__AnonStorey58_t1760080500 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
