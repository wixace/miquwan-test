﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginsTaiqi/LoginResult
struct LoginResult_t2998189643;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void PluginsTaiqi/LoginResult::.ctor(System.String)
extern "C"  void LoginResult__ctor_m1843005522 (LoginResult_t2998189643 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginsTaiqi/LoginResult::getToken()
extern "C"  String_t* LoginResult_getToken_m1377355034 (LoginResult_t2998189643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginsTaiqi/LoginResult::getChannelId()
extern "C"  String_t* LoginResult_getChannelId_m378756959 (LoginResult_t2998189643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginsTaiqi/LoginResult::getUserId()
extern "C"  String_t* LoginResult_getUserId_m585377127 (LoginResult_t2998189643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginsTaiqi/LoginResult::getChannelAction()
extern "C"  String_t* LoginResult_getChannelAction_m3867229242 (LoginResult_t2998189643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginsTaiqi/LoginResult::getExtInfo()
extern "C"  String_t* LoginResult_getExtInfo_m3528562704 (LoginResult_t2998189643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginsTaiqi/LoginResult::Clear()
extern "C"  void LoginResult_Clear_m300642523 (LoginResult_t2998189643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
