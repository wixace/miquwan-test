﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Ionic.Zlib.ZlibException
struct ZlibException_t212656338;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Pathfinding.Ionic.Zlib.ZlibException::.ctor(System.String)
extern "C"  void ZlibException__ctor_m3580974673 (ZlibException_t212656338 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
