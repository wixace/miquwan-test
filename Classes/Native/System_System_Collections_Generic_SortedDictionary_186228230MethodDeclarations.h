﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>
struct SortedDictionary_2_t186228230;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_t2450863117;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Object
struct Il2CppObject;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t3856534026;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2483180780;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"

// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void SortedDictionary_2__ctor_m3311600198_gshared (SortedDictionary_2_t186228230 * __this, const MethodInfo* method);
#define SortedDictionary_2__ctor_m3311600198(__this, method) ((  void (*) (SortedDictionary_2_t186228230 *, const MethodInfo*))SortedDictionary_2__ctor_m3311600198_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IComparer`1<TKey>)
extern "C"  void SortedDictionary_2__ctor_m959559319_gshared (SortedDictionary_2_t186228230 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define SortedDictionary_2__ctor_m959559319(__this, ___comparer0, method) ((  void (*) (SortedDictionary_2_t186228230 *, Il2CppObject*, const MethodInfo*))SortedDictionary_2__ctor_m959559319_gshared)(__this, ___comparer0, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m4223290012_gshared (SortedDictionary_2_t186228230 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m4223290012(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t186228230 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m4223290012_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2798599900_gshared (SortedDictionary_2_t186228230 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2798599900(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t186228230 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2798599900_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1906932475_gshared (SortedDictionary_2_t186228230 * __this, KeyValuePair_2_t1944668977  ___item0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1906932475(__this, ___item0, method) ((  void (*) (SortedDictionary_2_t186228230 *, KeyValuePair_2_t1944668977 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1906932475_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m275805075_gshared (SortedDictionary_2_t186228230 * __this, KeyValuePair_2_t1944668977  ___item0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m275805075(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t186228230 *, KeyValuePair_2_t1944668977 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m275805075_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3777037080_gshared (SortedDictionary_2_t186228230 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3777037080(__this, method) ((  bool (*) (SortedDictionary_2_t186228230 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3777037080_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2392613752_gshared (SortedDictionary_2_t186228230 * __this, KeyValuePair_2_t1944668977  ___item0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2392613752(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t186228230 *, KeyValuePair_2_t1944668977 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2392613752_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void SortedDictionary_2_System_Collections_IDictionary_Add_m1772361034_gshared (SortedDictionary_2_t186228230 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_Add_m1772361034(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t186228230 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Add_m1772361034_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool SortedDictionary_2_System_Collections_IDictionary_Contains_m734077824_gshared (SortedDictionary_2_t186228230 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_Contains_m734077824(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t186228230 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Contains_m734077824_gshared)(__this, ___key0, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m2421883621_gshared (SortedDictionary_2_t186228230 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m2421883621(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t186228230 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m2421883621_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool SortedDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1279881967_gshared (SortedDictionary_2_t186228230 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1279881967(__this, method) ((  bool (*) (SortedDictionary_2_t186228230 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1279881967_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool SortedDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2296410666_gshared (SortedDictionary_2_t186228230 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2296410666(__this, method) ((  bool (*) (SortedDictionary_2_t186228230 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2296410666_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_get_Keys_m4151827576_gshared (SortedDictionary_2_t186228230 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_Keys_m4151827576(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t186228230 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Keys_m4151827576_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void SortedDictionary_2_System_Collections_IDictionary_Remove_m1425360101_gshared (SortedDictionary_2_t186228230 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_Remove_m1425360101(__this, ___key0, method) ((  void (*) (SortedDictionary_2_t186228230 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Remove_m1425360101_gshared)(__this, ___key0, method)
// System.Collections.ICollection System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_get_Values_m2249237350_gshared (SortedDictionary_2_t186228230 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_Values_m2249237350(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t186228230 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Values_m2249237350_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_get_Item_m2445888066_gshared (SortedDictionary_2_t186228230 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_Item_m2445888066(__this, ___key0, method) ((  Il2CppObject * (*) (SortedDictionary_2_t186228230 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Item_m2445888066_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void SortedDictionary_2_System_Collections_IDictionary_set_Item_m3158219943_gshared (SortedDictionary_2_t186228230 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_set_Item_m3158219943(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t186228230 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_set_Item_m3158219943_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void SortedDictionary_2_System_Collections_ICollection_CopyTo_m4199329214_gshared (SortedDictionary_2_t186228230 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_ICollection_CopyTo_m4199329214(__this, ___array0, ___index1, method) ((  void (*) (SortedDictionary_2_t186228230 *, Il2CppArray *, int32_t, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_CopyTo_m4199329214_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m2050964436_gshared (SortedDictionary_2_t186228230 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m2050964436(__this, method) ((  bool (*) (SortedDictionary_2_t186228230 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m2050964436_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m1162103572_gshared (SortedDictionary_2_t186228230 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m1162103572(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t186228230 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m1162103572_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m3592182605_gshared (SortedDictionary_2_t186228230 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m3592182605(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t186228230 *, const MethodInfo*))SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m3592182605_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4204116682_gshared (SortedDictionary_2_t186228230 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4204116682(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t186228230 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4204116682_gshared)(__this, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t SortedDictionary_2_get_Count_m838674714_gshared (SortedDictionary_2_t186228230 * __this, const MethodInfo* method);
#define SortedDictionary_2_get_Count_m838674714(__this, method) ((  int32_t (*) (SortedDictionary_2_t186228230 *, const MethodInfo*))SortedDictionary_2_get_Count_m838674714_gshared)(__this, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * SortedDictionary_2_get_Item_m2682842897_gshared (SortedDictionary_2_t186228230 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_get_Item_m2682842897(__this, ___key0, method) ((  Il2CppObject * (*) (SortedDictionary_2_t186228230 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_get_Item_m2682842897_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C"  void SortedDictionary_2_set_Item_m1156236678_gshared (SortedDictionary_2_t186228230 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedDictionary_2_set_Item_m1156236678(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t186228230 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_set_Item_m1156236678_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C"  void SortedDictionary_2_Add_m1694196015_gshared (SortedDictionary_2_t186228230 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedDictionary_2_Add_m1694196015(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t186228230 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_Add_m1694196015_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::Clear()
extern "C"  void SortedDictionary_2_Clear_m717733489_gshared (SortedDictionary_2_t186228230 * __this, const MethodInfo* method);
#define SortedDictionary_2_Clear_m717733489(__this, method) ((  void (*) (SortedDictionary_2_t186228230 *, const MethodInfo*))SortedDictionary_2_Clear_m717733489_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C"  bool SortedDictionary_2_ContainsKey_m3607085699_gshared (SortedDictionary_2_t186228230 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_ContainsKey_m3607085699(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t186228230 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ContainsKey_m3607085699_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::ContainsValue(TValue)
extern "C"  bool SortedDictionary_2_ContainsValue_m3907001603_gshared (SortedDictionary_2_t186228230 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SortedDictionary_2_ContainsValue_m3907001603(__this, ___value0, method) ((  bool (*) (SortedDictionary_2_t186228230 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ContainsValue_m3907001603_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void SortedDictionary_2_CopyTo_m805382202_gshared (SortedDictionary_2_t186228230 * __this, KeyValuePair_2U5BU5D_t2483180780* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define SortedDictionary_2_CopyTo_m805382202(__this, ___array0, ___arrayIndex1, method) ((  void (*) (SortedDictionary_2_t186228230 *, KeyValuePair_2U5BU5D_t2483180780*, int32_t, const MethodInfo*))SortedDictionary_2_CopyTo_m805382202_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::Remove(TKey)
extern "C"  bool SortedDictionary_2_Remove_m4108038285_gshared (SortedDictionary_2_t186228230 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_Remove_m4108038285(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t186228230 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_Remove_m4108038285_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool SortedDictionary_2_TryGetValue_m850323420_gshared (SortedDictionary_2_t186228230 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define SortedDictionary_2_TryGetValue_m850323420(__this, ___key0, ___value1, method) ((  bool (*) (SortedDictionary_2_t186228230 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))SortedDictionary_2_TryGetValue_m850323420_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::ToKey(System.Object)
extern "C"  Il2CppObject * SortedDictionary_2_ToKey_m845290656_gshared (SortedDictionary_2_t186228230 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_ToKey_m845290656(__this, ___key0, method) ((  Il2CppObject * (*) (SortedDictionary_2_t186228230 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToKey_m845290656_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>::ToValue(System.Object)
extern "C"  Il2CppObject * SortedDictionary_2_ToValue_m847512956_gshared (SortedDictionary_2_t186228230 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SortedDictionary_2_ToValue_m847512956(__this, ___value0, method) ((  Il2CppObject * (*) (SortedDictionary_2_t186228230 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToValue_m847512956_gshared)(__this, ___value0, method)
