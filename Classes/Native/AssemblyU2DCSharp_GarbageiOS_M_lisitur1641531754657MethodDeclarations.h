﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_lisitur164
struct M_lisitur164_t1531754657;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_lisitur1641531754657.h"

// System.Void GarbageiOS.M_lisitur164::.ctor()
extern "C"  void M_lisitur164__ctor_m1363785906 (M_lisitur164_t1531754657 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lisitur164::M_sajouge0(System.String[],System.Int32)
extern "C"  void M_lisitur164_M_sajouge0_m3055471339 (M_lisitur164_t1531754657 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lisitur164::M_maytije1(System.String[],System.Int32)
extern "C"  void M_lisitur164_M_maytije1_m4044486231 (M_lisitur164_t1531754657 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lisitur164::M_cheateDearturye2(System.String[],System.Int32)
extern "C"  void M_lisitur164_M_cheateDearturye2_m3822785172 (M_lisitur164_t1531754657 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lisitur164::M_mozeVepis3(System.String[],System.Int32)
extern "C"  void M_lisitur164_M_mozeVepis3_m2251488784 (M_lisitur164_t1531754657 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lisitur164::M_warlage4(System.String[],System.Int32)
extern "C"  void M_lisitur164_M_warlage4_m906006452 (M_lisitur164_t1531754657 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lisitur164::M_cearleHemhowmea5(System.String[],System.Int32)
extern "C"  void M_lisitur164_M_cearleHemhowmea5_m3459508627 (M_lisitur164_t1531754657 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lisitur164::ilo_M_mozeVepis31(GarbageiOS.M_lisitur164,System.String[],System.Int32)
extern "C"  void M_lisitur164_ilo_M_mozeVepis31_m3595295147 (Il2CppObject * __this /* static, unused */, M_lisitur164_t1531754657 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
