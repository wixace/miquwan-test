﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JContainer
struct JContainer_t3364442311;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JToken[]
struct JTokenU5BU5D_t2853253222;
// System.Object
struct Il2CppObject;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerable_1_t2418191612;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.String
struct String_t;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken>
struct IList_1_t1811925858;
// Newtonsoft.Json.Linq.JValue
struct JValue_t3413677367;
// Newtonsoft.Json.IJsonLineInfo
struct IJsonLineInfo_t1008519681;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JContainer3364442311.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JEnumerable_213262205.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType3916897561.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JValue3413677367.h"

// System.Void Newtonsoft.Json.Linq.JContainer::.ctor()
extern "C"  void JContainer__ctor_m4013080987 (JContainer_t3364442311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::.ctor(Newtonsoft.Json.Linq.JContainer)
extern "C"  void JContainer__ctor_m3259796135 (JContainer_t3364442311 * __this, JContainer_t3364442311 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.IList<Newtonsoft.Json.Linq.JToken>.IndexOf(Newtonsoft.Json.Linq.JToken)
extern "C"  int32_t JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_IndexOf_m4062300942 (JContainer_t3364442311 * __this, JToken_t3412245951 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.IList<Newtonsoft.Json.Linq.JToken>.Insert(System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_Insert_m1915937323 (JContainer_t3364442311 * __this, int32_t ___index0, JToken_t3412245951 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.IList<Newtonsoft.Json.Linq.JToken>.RemoveAt(System.Int32)
extern "C"  void JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_RemoveAt_m2431908453 (JContainer_t3364442311 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.IList<Newtonsoft.Json.Linq.JToken>.get_Item(System.Int32)
extern "C"  JToken_t3412245951 * JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_get_Item_m3806947135 (JContainer_t3364442311 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.IList<Newtonsoft.Json.Linq.JToken>.set_Item(System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_set_Item_m2887760002 (JContainer_t3364442311 * __this, int32_t ___index0, JToken_t3412245951 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.ICollection<Newtonsoft.Json.Linq.JToken>.Add(Newtonsoft.Json.Linq.JToken)
extern "C"  void JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_Add_m390893316 (JContainer_t3364442311 * __this, JToken_t3412245951 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.ICollection<Newtonsoft.Json.Linq.JToken>.Clear()
extern "C"  void JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_Clear_m3737391378 (JContainer_t3364442311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.ICollection<Newtonsoft.Json.Linq.JToken>.Contains(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_Contains_m3190548098 (JContainer_t3364442311 * __this, JToken_t3412245951 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.ICollection<Newtonsoft.Json.Linq.JToken>.CopyTo(Newtonsoft.Json.Linq.JToken[],System.Int32)
extern "C"  void JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_CopyTo_m985316380 (JContainer_t3364442311 * __this, JTokenU5BU5D_t2853253222* ___array0, int32_t ___arrayIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.ICollection<Newtonsoft.Json.Linq.JToken>.get_IsReadOnly()
extern "C"  bool JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_get_IsReadOnly_m2026722110 (JContainer_t3364442311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.ICollection<Newtonsoft.Json.Linq.JToken>.Remove(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_Remove_m860809277 (JContainer_t3364442311 * __this, JToken_t3412245951 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JContainer::System.Collections.IList.Add(System.Object)
extern "C"  int32_t JContainer_System_Collections_IList_Add_m2948342231 (JContainer_t3364442311 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.IList.Clear()
extern "C"  void JContainer_System_Collections_IList_Clear_m1504571159 (JContainer_t3364442311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JContainer::System.Collections.IList.Contains(System.Object)
extern "C"  bool JContainer_System_Collections_IList_Contains_m3530825481 (JContainer_t3364442311 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JContainer::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t JContainer_System_Collections_IList_IndexOf_m4258895023 (JContainer_t3364442311 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void JContainer_System_Collections_IList_Insert_m1183016624 (JContainer_t3364442311 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JContainer::System.Collections.IList.get_IsFixedSize()
extern "C"  bool JContainer_System_Collections_IList_get_IsFixedSize_m1167984952 (JContainer_t3364442311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JContainer::System.Collections.IList.get_IsReadOnly()
extern "C"  bool JContainer_System_Collections_IList_get_IsReadOnly_m2431348417 (JContainer_t3364442311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.IList.Remove(System.Object)
extern "C"  void JContainer_System_Collections_IList_Remove_m2673812600 (JContainer_t3364442311 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void JContainer_System_Collections_IList_RemoveAt_m2869167680 (JContainer_t3364442311 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JContainer::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * JContainer_System_Collections_IList_get_Item_m3847900858 (JContainer_t3364442311 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void JContainer_System_Collections_IList_set_Item_m3621561991 (JContainer_t3364442311 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void JContainer_System_Collections_ICollection_CopyTo_m1381399561 (JContainer_t3364442311 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JContainer::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool JContainer_System_Collections_ICollection_get_IsSynchronized_m3067801523 (JContainer_t3364442311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JContainer::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * JContainer_System_Collections_ICollection_get_SyncRoot_m2787249651 (JContainer_t3364442311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::CheckReentrancy()
extern "C"  void JContainer_CheckReentrancy_m3679615134 (JContainer_t3364442311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JContainer::get_HasValues()
extern "C"  bool JContainer_get_HasValues_m1725254496 (JContainer_t3364442311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JContainer::ContentsEqual(Newtonsoft.Json.Linq.JContainer)
extern "C"  bool JContainer_ContentsEqual_m886382491 (JContainer_t3364442311 * __this, JContainer_t3364442311 * ___container0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::get_First()
extern "C"  JToken_t3412245951 * JContainer_get_First_m554020555 (JContainer_t3364442311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::get_Last()
extern "C"  JToken_t3412245951 * JContainer_get_Last_m736476093 (JContainer_t3364442311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JContainer::Children()
extern "C"  JEnumerable_1_t213262205  JContainer_Children_m4135644061 (JContainer_t3364442311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JContainer::Descendants()
extern "C"  Il2CppObject* JContainer_Descendants_m3165833315 (JContainer_t3364442311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JContainer::IsMultiContent(System.Object)
extern "C"  bool JContainer_IsMultiContent_m1059755285 (JContainer_t3364442311 * __this, Il2CppObject * ___content0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::EnsureParentToken(Newtonsoft.Json.Linq.JToken)
extern "C"  JToken_t3412245951 * JContainer_EnsureParentToken_m1817047029 (JContainer_t3364442311 * __this, JToken_t3412245951 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JContainer::IndexOfItem(Newtonsoft.Json.Linq.JToken)
extern "C"  int32_t JContainer_IndexOfItem_m570137607 (JContainer_t3364442311 * __this, JToken_t3412245951 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::InsertItem(System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JContainer_InsertItem_m250018666 (JContainer_t3364442311 * __this, int32_t ___index0, JToken_t3412245951 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::RemoveItemAt(System.Int32)
extern "C"  void JContainer_RemoveItemAt_m4019111652 (JContainer_t3364442311 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JContainer::RemoveItem(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JContainer_RemoveItem_m1201751710 (JContainer_t3364442311 * __this, JToken_t3412245951 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::GetItem(System.Int32)
extern "C"  JToken_t3412245951 * JContainer_GetItem_m2662951902 (JContainer_t3364442311 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::SetItem(System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JContainer_SetItem_m1866115011 (JContainer_t3364442311 * __this, int32_t ___index0, JToken_t3412245951 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::ClearItems()
extern "C"  void JContainer_ClearItems_m3906588412 (JContainer_t3364442311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::ReplaceItem(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern "C"  void JContainer_ReplaceItem_m3610202670 (JContainer_t3364442311 * __this, JToken_t3412245951 * ___existing0, JToken_t3412245951 * ___replacement1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JContainer::ContainsItem(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JContainer_ContainsItem_m545832291 (JContainer_t3364442311 * __this, JToken_t3412245951 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::CopyItemsTo(System.Array,System.Int32)
extern "C"  void JContainer_CopyItemsTo_m3933300786 (JContainer_t3364442311 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JContainer::IsTokenUnchanged(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern "C"  bool JContainer_IsTokenUnchanged_m3217675789 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___currentValue0, JToken_t3412245951 * ___newValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::ValidateToken(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern "C"  void JContainer_ValidateToken_m4251267346 (JContainer_t3364442311 * __this, JToken_t3412245951 * ___o0, JToken_t3412245951 * ___existing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::Add(System.Object)
extern "C"  void JContainer_Add_m1908593338 (JContainer_t3364442311 * __this, Il2CppObject * ___content0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::AddFirst(System.Object)
extern "C"  void JContainer_AddFirst_m2431705468 (JContainer_t3364442311 * __this, Il2CppObject * ___content0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::AddInternal(System.Int32,System.Object)
extern "C"  void JContainer_AddInternal_m1074192214 (JContainer_t3364442311 * __this, int32_t ___index0, Il2CppObject * ___content1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::CreateFromContent(System.Object)
extern "C"  JToken_t3412245951 * JContainer_CreateFromContent_m3830878173 (JContainer_t3364442311 * __this, Il2CppObject * ___content0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonWriter Newtonsoft.Json.Linq.JContainer::CreateWriter()
extern "C"  JsonWriter_t972330355 * JContainer_CreateWriter_m2310930635 (JContainer_t3364442311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::ReplaceAll(System.Object)
extern "C"  void JContainer_ReplaceAll_m130114558 (JContainer_t3364442311 * __this, Il2CppObject * ___content0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::RemoveAll()
extern "C"  void JContainer_RemoveAll_m3062163958 (JContainer_t3364442311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::ReadTokenFrom(Newtonsoft.Json.JsonReader)
extern "C"  void JContainer_ReadTokenFrom_m2101086822 (JContainer_t3364442311 * __this, JsonReader_t816925123 * ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::ReadContentFrom(Newtonsoft.Json.JsonReader)
extern "C"  void JContainer_ReadContentFrom_m1664982118 (JContainer_t3364442311 * __this, JsonReader_t816925123 * ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JContainer::ContentsHashCode()
extern "C"  int32_t JContainer_ContentsHashCode_m256434064 (JContainer_t3364442311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::EnsureValue(System.Object)
extern "C"  JToken_t3412245951 * JContainer_EnsureValue_m452595869 (JContainer_t3364442311 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JContainer::get_Count()
extern "C"  int32_t JContainer_get_Count_m2101807405 (JContainer_t3364442311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::ilo_RemoveItemAt1(Newtonsoft.Json.Linq.JContainer,System.Int32)
extern "C"  void JContainer_ilo_RemoveItemAt1_m2379394178 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::ilo_SetItem2(Newtonsoft.Json.Linq.JContainer,System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JContainer_ilo_SetItem2_m2210899858 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, int32_t ___index1, JToken_t3412245951 * ___item2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::ilo_Add3(Newtonsoft.Json.Linq.JContainer,System.Object)
extern "C"  void JContainer_ilo_Add3_m3237848642 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, Il2CppObject * ___content1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::ilo_ClearItems4(Newtonsoft.Json.Linq.JContainer)
extern "C"  void JContainer_ilo_ClearItems4_m841980027 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JContainer::ilo_ContainsItem5(Newtonsoft.Json.Linq.JContainer,Newtonsoft.Json.Linq.JToken)
extern "C"  bool JContainer_ilo_ContainsItem5_m2324915113 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, JToken_t3412245951 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::ilo_CopyItemsTo6(Newtonsoft.Json.Linq.JContainer,System.Array,System.Int32)
extern "C"  void JContainer_ilo_CopyItemsTo6_m2458622849 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, Il2CppArray * ___array1, int32_t ___arrayIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JContainer::ilo_get_Count7(Newtonsoft.Json.Linq.JContainer)
extern "C"  int32_t JContainer_ilo_get_Count7_m171691587 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JContainer::ilo_IndexOfItem8(Newtonsoft.Json.Linq.JContainer,Newtonsoft.Json.Linq.JToken)
extern "C"  int32_t JContainer_ilo_IndexOfItem8_m120672284 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, JToken_t3412245951 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::ilo_EnsureValue9(Newtonsoft.Json.Linq.JContainer,System.Object)
extern "C"  JToken_t3412245951 * JContainer_ilo_EnsureValue9_m881123705 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JContainer::ilo_RemoveItem10(Newtonsoft.Json.Linq.JContainer,Newtonsoft.Json.Linq.JToken)
extern "C"  bool JContainer_ilo_RemoveItem10_m199136552 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, JToken_t3412245951 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JContainer::ilo_FormatWith11(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* JContainer_ilo_FormatWith11_m3270036052 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::ilo_get_Next12(Newtonsoft.Json.Linq.JToken)
extern "C"  JToken_t3412245951 * JContainer_ilo_get_Next12_m3343048124 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JContainer::ilo_get_ChildrenTokens13(Newtonsoft.Json.Linq.JContainer)
extern "C"  Il2CppObject* JContainer_ilo_get_ChildrenTokens13_m2084035948 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JContainer::ilo_get_Parent14(Newtonsoft.Json.Linq.JToken)
extern "C"  JContainer_t3364442311 * JContainer_ilo_get_Parent14_m87544091 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::ilo_set_Next15(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern "C"  void JContainer_ilo_set_Next15_m1482212670 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, JToken_t3412245951 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::ilo_set_Previous16(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern "C"  void JContainer_ilo_set_Previous16_m2640721369 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, JToken_t3412245951 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::ilo_set_Parent17(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JContainer)
extern "C"  void JContainer_ilo_set_Parent17_m2010698077 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, JContainer_t3364442311 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::ilo_CheckReentrancy18(Newtonsoft.Json.Linq.JContainer)
extern "C"  void JContainer_ilo_CheckReentrancy18_m1418993840 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::ilo_ValidateToken19(Newtonsoft.Json.Linq.JContainer,Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern "C"  void JContainer_ilo_ValidateToken19_m1385429049 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, JToken_t3412245951 * ___o1, JToken_t3412245951 * ___existing2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JContainer::ilo_get_Type20(Newtonsoft.Json.Linq.JValue)
extern "C"  int32_t JContainer_ilo_get_Type20_m2421089926 (Il2CppObject * __this /* static, unused */, JValue_t3413677367 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::ilo_ArgumentNotNull21(System.Object,System.String)
extern "C"  void JContainer_ilo_ArgumentNotNull21_m2606279278 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, String_t* ___parameterName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JContainer::ilo_get_Type22(Newtonsoft.Json.Linq.JToken)
extern "C"  int32_t JContainer_ilo_get_Type22_m2399348284 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::ilo_AddInternal23(Newtonsoft.Json.Linq.JContainer,System.Int32,System.Object)
extern "C"  void JContainer_ilo_AddInternal23_m3266821096 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, int32_t ___index1, Il2CppObject * ___content2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::ilo_CreateFromContent24(Newtonsoft.Json.Linq.JContainer,System.Object)
extern "C"  JToken_t3412245951 * JContainer_ilo_CreateFromContent24_m924590890 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, Il2CppObject * ___content1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::ilo_InsertItem25(Newtonsoft.Json.Linq.JContainer,System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JContainer_ilo_InsertItem25_m2169060356 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, int32_t ___index1, JToken_t3412245951 * ___item2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JContainer::ilo_get_Depth26(Newtonsoft.Json.JsonReader)
extern "C"  int32_t JContainer_ilo_get_Depth26_m589942898 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer::ilo_SetLineInfo27(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.IJsonLineInfo)
extern "C"  void JContainer_ilo_SetLineInfo27_m2904012905 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, Il2CppObject * ___lineInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JContainer::ilo_get_Value28(Newtonsoft.Json.JsonReader)
extern "C"  Il2CppObject * JContainer_ilo_get_Value28_m1707883465 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JContainer::ilo_Read29(Newtonsoft.Json.JsonReader)
extern "C"  bool JContainer_ilo_Read29_m2168678469 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JContainer::ilo_GetDeepHashCode30(Newtonsoft.Json.Linq.JToken)
extern "C"  int32_t JContainer_ilo_GetDeepHashCode30_m1967987580 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
