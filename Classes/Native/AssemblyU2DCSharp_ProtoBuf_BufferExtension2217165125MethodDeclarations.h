﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.BufferExtension
struct BufferExtension_t2217165125;
// System.IO.Stream
struct Stream_t1561764144;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"

// System.Void ProtoBuf.BufferExtension::.ctor()
extern "C"  void BufferExtension__ctor_m1435307711 (BufferExtension_t2217165125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.BufferExtension::ProtoBuf.IExtension.GetLength()
extern "C"  int32_t BufferExtension_ProtoBuf_IExtension_GetLength_m1565508150 (BufferExtension_t2217165125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream ProtoBuf.BufferExtension::ProtoBuf.IExtension.BeginAppend()
extern "C"  Stream_t1561764144 * BufferExtension_ProtoBuf_IExtension_BeginAppend_m314187639 (BufferExtension_t2217165125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.BufferExtension::ProtoBuf.IExtension.EndAppend(System.IO.Stream,System.Boolean)
extern "C"  void BufferExtension_ProtoBuf_IExtension_EndAppend_m3516434473 (BufferExtension_t2217165125 * __this, Stream_t1561764144 * ___stream0, bool ___commit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream ProtoBuf.BufferExtension::ProtoBuf.IExtension.BeginQuery()
extern "C"  Stream_t1561764144 * BufferExtension_ProtoBuf_IExtension_BeginQuery_m3813979757 (BufferExtension_t2217165125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.BufferExtension::ProtoBuf.IExtension.EndQuery(System.IO.Stream)
extern "C"  void BufferExtension_ProtoBuf_IExtension_EndQuery_m372793726 (BufferExtension_t2217165125 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.BufferExtension::ilo_BlockCopy1(System.Byte[],System.Int32,System.Byte[],System.Int32,System.Int32)
extern "C"  void BufferExtension_ilo_BlockCopy1_m2576468728 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___from0, int32_t ___fromIndex1, ByteU5BU5D_t4260760469* ___to2, int32_t ___toIndex3, int32_t ___count4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
