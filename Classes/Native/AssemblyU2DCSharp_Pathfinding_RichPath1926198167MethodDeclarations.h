﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RichPath
struct RichPath_t1926198167;
// Seeker
struct Seeker_t2472610117;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.RichPathPart
struct RichPathPart_t2520985130;
// Pathfinding.RichFunnel
struct RichFunnel_t2453525928;
// Pathfinding.IFunnelGraph
struct IFunnelGraph_t2367181445;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// Pathfinding.NodeLink2
struct NodeLink2_t1645404664;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Seeker2472610117.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Pathfinding_RichFunnel_FunnelSimp683755508.h"
#include "AssemblyU2DCSharp_Pathfinding_RichFunnel2453525928.h"
#include "AssemblyU2DCSharp_Pathfinding_RichPath1926198167.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

// System.Void Pathfinding.RichPath::.ctor()
extern "C"  void RichPath__ctor_m935098544 (RichPath_t1926198167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichPath::Initialize(Seeker,Pathfinding.Path,System.Boolean,Pathfinding.RichFunnel/FunnelSimplification)
extern "C"  void RichPath_Initialize_m904147005 (RichPath_t1926198167 * __this, Seeker_t2472610117 * ___s0, Path_t1974241691 * ___p1, bool ___mergePartEndpoints2, int32_t ___simplificationMode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RichPath::PartsLeft()
extern "C"  bool RichPath_PartsLeft_m3255205985 (RichPath_t1926198167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichPath::NextPart()
extern "C"  void RichPath_NextPart_m949798170 (RichPath_t1926198167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RichPathPart Pathfinding.RichPath::GetCurrentPart()
extern "C"  RichPathPart_t2520985130 * RichPath_GetCurrentPart_m1114835849 (RichPath_t1926198167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RichFunnel Pathfinding.RichPath::ilo_Initialize1(Pathfinding.RichFunnel,Pathfinding.RichPath,Pathfinding.IFunnelGraph)
extern "C"  RichFunnel_t2453525928 * RichPath_ilo_Initialize1_m3700966001 (Il2CppObject * __this /* static, unused */, RichFunnel_t2453525928 * ____this0, RichPath_t1926198167 * ___path1, Il2CppObject * ___graph2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.RichPath::ilo_get_GraphIndex2(Pathfinding.GraphNode)
extern "C"  uint32_t RichPath_ilo_get_GraphIndex2_m2923553255 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RichPath::ilo_op_Explicit3(Pathfinding.Int3)
extern "C"  Vector3_t4282066566  RichPath_ilo_op_Explicit3_m1736686356 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NodeLink2 Pathfinding.RichPath::ilo_GetNodeLink4(Pathfinding.GraphNode)
extern "C"  NodeLink2_t1645404664 * RichPath_ilo_GetNodeLink4_m1279389404 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
