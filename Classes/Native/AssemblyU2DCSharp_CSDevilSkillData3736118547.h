﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSDevilSkillData
struct  CSDevilSkillData_t3736118547  : public Il2CppObject
{
public:
	// System.String CSDevilSkillData::skills
	String_t* ___skills_0;
	// System.String CSDevilSkillData::skillLvs
	String_t* ___skillLvs_1;
	// System.String CSDevilSkillData::ais
	String_t* ___ais_2;
	// System.Single CSDevilSkillData::atkPower
	float ___atkPower_3;
	// System.Single CSDevilSkillData::hp
	float ___hp_4;
	// System.Single CSDevilSkillData::PD
	float ___PD_5;
	// System.Single CSDevilSkillData::MD
	float ___MD_6;

public:
	inline static int32_t get_offset_of_skills_0() { return static_cast<int32_t>(offsetof(CSDevilSkillData_t3736118547, ___skills_0)); }
	inline String_t* get_skills_0() const { return ___skills_0; }
	inline String_t** get_address_of_skills_0() { return &___skills_0; }
	inline void set_skills_0(String_t* value)
	{
		___skills_0 = value;
		Il2CppCodeGenWriteBarrier(&___skills_0, value);
	}

	inline static int32_t get_offset_of_skillLvs_1() { return static_cast<int32_t>(offsetof(CSDevilSkillData_t3736118547, ___skillLvs_1)); }
	inline String_t* get_skillLvs_1() const { return ___skillLvs_1; }
	inline String_t** get_address_of_skillLvs_1() { return &___skillLvs_1; }
	inline void set_skillLvs_1(String_t* value)
	{
		___skillLvs_1 = value;
		Il2CppCodeGenWriteBarrier(&___skillLvs_1, value);
	}

	inline static int32_t get_offset_of_ais_2() { return static_cast<int32_t>(offsetof(CSDevilSkillData_t3736118547, ___ais_2)); }
	inline String_t* get_ais_2() const { return ___ais_2; }
	inline String_t** get_address_of_ais_2() { return &___ais_2; }
	inline void set_ais_2(String_t* value)
	{
		___ais_2 = value;
		Il2CppCodeGenWriteBarrier(&___ais_2, value);
	}

	inline static int32_t get_offset_of_atkPower_3() { return static_cast<int32_t>(offsetof(CSDevilSkillData_t3736118547, ___atkPower_3)); }
	inline float get_atkPower_3() const { return ___atkPower_3; }
	inline float* get_address_of_atkPower_3() { return &___atkPower_3; }
	inline void set_atkPower_3(float value)
	{
		___atkPower_3 = value;
	}

	inline static int32_t get_offset_of_hp_4() { return static_cast<int32_t>(offsetof(CSDevilSkillData_t3736118547, ___hp_4)); }
	inline float get_hp_4() const { return ___hp_4; }
	inline float* get_address_of_hp_4() { return &___hp_4; }
	inline void set_hp_4(float value)
	{
		___hp_4 = value;
	}

	inline static int32_t get_offset_of_PD_5() { return static_cast<int32_t>(offsetof(CSDevilSkillData_t3736118547, ___PD_5)); }
	inline float get_PD_5() const { return ___PD_5; }
	inline float* get_address_of_PD_5() { return &___PD_5; }
	inline void set_PD_5(float value)
	{
		___PD_5 = value;
	}

	inline static int32_t get_offset_of_MD_6() { return static_cast<int32_t>(offsetof(CSDevilSkillData_t3736118547, ___MD_6)); }
	inline float get_MD_6() const { return ___MD_6; }
	inline float* get_address_of_MD_6() { return &___MD_6; }
	inline void set_MD_6(float value)
	{
		___MD_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
