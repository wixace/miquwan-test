﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Poly2Tri.FixedArray3`1/<Enumerate>c__Iterator0<System.Object>
struct U3CEnumerateU3Ec__Iterator0_t181911054;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Poly2Tri.FixedArray3`1/<Enumerate>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CEnumerateU3Ec__Iterator0__ctor_m71689204_gshared (U3CEnumerateU3Ec__Iterator0_t181911054 * __this, const MethodInfo* method);
#define U3CEnumerateU3Ec__Iterator0__ctor_m71689204(__this, method) ((  void (*) (U3CEnumerateU3Ec__Iterator0_t181911054 *, const MethodInfo*))U3CEnumerateU3Ec__Iterator0__ctor_m71689204_gshared)(__this, method)
// T Pathfinding.Poly2Tri.FixedArray3`1/<Enumerate>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CEnumerateU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3596731643_gshared (U3CEnumerateU3Ec__Iterator0_t181911054 * __this, const MethodInfo* method);
#define U3CEnumerateU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3596731643(__this, method) ((  Il2CppObject * (*) (U3CEnumerateU3Ec__Iterator0_t181911054 *, const MethodInfo*))U3CEnumerateU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3596731643_gshared)(__this, method)
// System.Object Pathfinding.Poly2Tri.FixedArray3`1/<Enumerate>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEnumerateU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1485434226_gshared (U3CEnumerateU3Ec__Iterator0_t181911054 * __this, const MethodInfo* method);
#define U3CEnumerateU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1485434226(__this, method) ((  Il2CppObject * (*) (U3CEnumerateU3Ec__Iterator0_t181911054 *, const MethodInfo*))U3CEnumerateU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1485434226_gshared)(__this, method)
// System.Collections.IEnumerator Pathfinding.Poly2Tri.FixedArray3`1/<Enumerate>c__Iterator0<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CEnumerateU3Ec__Iterator0_System_Collections_IEnumerable_GetEnumerator_m1705069515_gshared (U3CEnumerateU3Ec__Iterator0_t181911054 * __this, const MethodInfo* method);
#define U3CEnumerateU3Ec__Iterator0_System_Collections_IEnumerable_GetEnumerator_m1705069515(__this, method) ((  Il2CppObject * (*) (U3CEnumerateU3Ec__Iterator0_t181911054 *, const MethodInfo*))U3CEnumerateU3Ec__Iterator0_System_Collections_IEnumerable_GetEnumerator_m1705069515_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> Pathfinding.Poly2Tri.FixedArray3`1/<Enumerate>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CEnumerateU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m410823166_gshared (U3CEnumerateU3Ec__Iterator0_t181911054 * __this, const MethodInfo* method);
#define U3CEnumerateU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m410823166(__this, method) ((  Il2CppObject* (*) (U3CEnumerateU3Ec__Iterator0_t181911054 *, const MethodInfo*))U3CEnumerateU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m410823166_gshared)(__this, method)
// System.Boolean Pathfinding.Poly2Tri.FixedArray3`1/<Enumerate>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CEnumerateU3Ec__Iterator0_MoveNext_m2412220510_gshared (U3CEnumerateU3Ec__Iterator0_t181911054 * __this, const MethodInfo* method);
#define U3CEnumerateU3Ec__Iterator0_MoveNext_m2412220510(__this, method) ((  bool (*) (U3CEnumerateU3Ec__Iterator0_t181911054 *, const MethodInfo*))U3CEnumerateU3Ec__Iterator0_MoveNext_m2412220510_gshared)(__this, method)
// System.Void Pathfinding.Poly2Tri.FixedArray3`1/<Enumerate>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CEnumerateU3Ec__Iterator0_Dispose_m72131377_gshared (U3CEnumerateU3Ec__Iterator0_t181911054 * __this, const MethodInfo* method);
#define U3CEnumerateU3Ec__Iterator0_Dispose_m72131377(__this, method) ((  void (*) (U3CEnumerateU3Ec__Iterator0_t181911054 *, const MethodInfo*))U3CEnumerateU3Ec__Iterator0_Dispose_m72131377_gshared)(__this, method)
// System.Void Pathfinding.Poly2Tri.FixedArray3`1/<Enumerate>c__Iterator0<System.Object>::Reset()
extern "C"  void U3CEnumerateU3Ec__Iterator0_Reset_m2013089441_gshared (U3CEnumerateU3Ec__Iterator0_t181911054 * __this, const MethodInfo* method);
#define U3CEnumerateU3Ec__Iterator0_Reset_m2013089441(__this, method) ((  void (*) (U3CEnumerateU3Ec__Iterator0_t181911054 *, const MethodInfo*))U3CEnumerateU3Ec__Iterator0_Reset_m2013089441_gshared)(__this, method)
