﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.UInt16>
struct Dictionary_2_t845086293;
// System.Collections.Generic.Dictionary`2<System.UInt16,System.String>
struct Dictionary_2_t1870263745;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.Collections.Generic.Dictionary`2<System.UInt32,System.String>
struct Dictionary_2_t4381535;
// Pomelo.Protobuf.Protobuf
struct Protobuf_t1750100383;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pomelo.DotNetClient.MessageProtocol
struct  MessageProtocol_t562499845  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.UInt16> Pomelo.DotNetClient.MessageProtocol::dict
	Dictionary_2_t845086293 * ___dict_3;
	// System.Collections.Generic.Dictionary`2<System.UInt16,System.String> Pomelo.DotNetClient.MessageProtocol::abbrs
	Dictionary_2_t1870263745 * ___abbrs_4;
	// Newtonsoft.Json.Linq.JObject Pomelo.DotNetClient.MessageProtocol::encodeProtos
	JObject_t1798544199 * ___encodeProtos_5;
	// Newtonsoft.Json.Linq.JObject Pomelo.DotNetClient.MessageProtocol::decodeProtos
	JObject_t1798544199 * ___decodeProtos_6;
	// System.Collections.Generic.Dictionary`2<System.UInt32,System.String> Pomelo.DotNetClient.MessageProtocol::reqMap
	Dictionary_2_t4381535 * ___reqMap_7;
	// Pomelo.Protobuf.Protobuf Pomelo.DotNetClient.MessageProtocol::protobuf
	Protobuf_t1750100383 * ___protobuf_8;

public:
	inline static int32_t get_offset_of_dict_3() { return static_cast<int32_t>(offsetof(MessageProtocol_t562499845, ___dict_3)); }
	inline Dictionary_2_t845086293 * get_dict_3() const { return ___dict_3; }
	inline Dictionary_2_t845086293 ** get_address_of_dict_3() { return &___dict_3; }
	inline void set_dict_3(Dictionary_2_t845086293 * value)
	{
		___dict_3 = value;
		Il2CppCodeGenWriteBarrier(&___dict_3, value);
	}

	inline static int32_t get_offset_of_abbrs_4() { return static_cast<int32_t>(offsetof(MessageProtocol_t562499845, ___abbrs_4)); }
	inline Dictionary_2_t1870263745 * get_abbrs_4() const { return ___abbrs_4; }
	inline Dictionary_2_t1870263745 ** get_address_of_abbrs_4() { return &___abbrs_4; }
	inline void set_abbrs_4(Dictionary_2_t1870263745 * value)
	{
		___abbrs_4 = value;
		Il2CppCodeGenWriteBarrier(&___abbrs_4, value);
	}

	inline static int32_t get_offset_of_encodeProtos_5() { return static_cast<int32_t>(offsetof(MessageProtocol_t562499845, ___encodeProtos_5)); }
	inline JObject_t1798544199 * get_encodeProtos_5() const { return ___encodeProtos_5; }
	inline JObject_t1798544199 ** get_address_of_encodeProtos_5() { return &___encodeProtos_5; }
	inline void set_encodeProtos_5(JObject_t1798544199 * value)
	{
		___encodeProtos_5 = value;
		Il2CppCodeGenWriteBarrier(&___encodeProtos_5, value);
	}

	inline static int32_t get_offset_of_decodeProtos_6() { return static_cast<int32_t>(offsetof(MessageProtocol_t562499845, ___decodeProtos_6)); }
	inline JObject_t1798544199 * get_decodeProtos_6() const { return ___decodeProtos_6; }
	inline JObject_t1798544199 ** get_address_of_decodeProtos_6() { return &___decodeProtos_6; }
	inline void set_decodeProtos_6(JObject_t1798544199 * value)
	{
		___decodeProtos_6 = value;
		Il2CppCodeGenWriteBarrier(&___decodeProtos_6, value);
	}

	inline static int32_t get_offset_of_reqMap_7() { return static_cast<int32_t>(offsetof(MessageProtocol_t562499845, ___reqMap_7)); }
	inline Dictionary_2_t4381535 * get_reqMap_7() const { return ___reqMap_7; }
	inline Dictionary_2_t4381535 ** get_address_of_reqMap_7() { return &___reqMap_7; }
	inline void set_reqMap_7(Dictionary_2_t4381535 * value)
	{
		___reqMap_7 = value;
		Il2CppCodeGenWriteBarrier(&___reqMap_7, value);
	}

	inline static int32_t get_offset_of_protobuf_8() { return static_cast<int32_t>(offsetof(MessageProtocol_t562499845, ___protobuf_8)); }
	inline Protobuf_t1750100383 * get_protobuf_8() const { return ___protobuf_8; }
	inline Protobuf_t1750100383 ** get_address_of_protobuf_8() { return &___protobuf_8; }
	inline void set_protobuf_8(Protobuf_t1750100383 * value)
	{
		___protobuf_8 = value;
		Il2CppCodeGenWriteBarrier(&___protobuf_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
