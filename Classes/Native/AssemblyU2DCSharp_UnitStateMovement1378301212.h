﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_UnitStateBase1040218558.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitStateMovement
struct  UnitStateMovement_t1378301212  : public UnitStateBase_t1040218558
{
public:
	// System.Boolean UnitStateMovement::isInitPath
	bool ___isInitPath_5;
	// System.Single UnitStateMovement::speed
	float ___speed_6;
	// System.Single UnitStateMovement::distance
	float ___distance_7;
	// UnityEngine.Vector3 UnitStateMovement::targetPos
	Vector3_t4282066566  ___targetPos_8;
	// UnityEngine.Vector3 UnitStateMovement::direction
	Vector3_t4282066566  ___direction_9;
	// System.Single UnitStateMovement::avgtime
	float ___avgtime_10;
	// System.Single UnitStateMovement::mTime
	float ___mTime_11;
	// System.Single UnitStateMovement::maxtime
	float ___maxtime_12;

public:
	inline static int32_t get_offset_of_isInitPath_5() { return static_cast<int32_t>(offsetof(UnitStateMovement_t1378301212, ___isInitPath_5)); }
	inline bool get_isInitPath_5() const { return ___isInitPath_5; }
	inline bool* get_address_of_isInitPath_5() { return &___isInitPath_5; }
	inline void set_isInitPath_5(bool value)
	{
		___isInitPath_5 = value;
	}

	inline static int32_t get_offset_of_speed_6() { return static_cast<int32_t>(offsetof(UnitStateMovement_t1378301212, ___speed_6)); }
	inline float get_speed_6() const { return ___speed_6; }
	inline float* get_address_of_speed_6() { return &___speed_6; }
	inline void set_speed_6(float value)
	{
		___speed_6 = value;
	}

	inline static int32_t get_offset_of_distance_7() { return static_cast<int32_t>(offsetof(UnitStateMovement_t1378301212, ___distance_7)); }
	inline float get_distance_7() const { return ___distance_7; }
	inline float* get_address_of_distance_7() { return &___distance_7; }
	inline void set_distance_7(float value)
	{
		___distance_7 = value;
	}

	inline static int32_t get_offset_of_targetPos_8() { return static_cast<int32_t>(offsetof(UnitStateMovement_t1378301212, ___targetPos_8)); }
	inline Vector3_t4282066566  get_targetPos_8() const { return ___targetPos_8; }
	inline Vector3_t4282066566 * get_address_of_targetPos_8() { return &___targetPos_8; }
	inline void set_targetPos_8(Vector3_t4282066566  value)
	{
		___targetPos_8 = value;
	}

	inline static int32_t get_offset_of_direction_9() { return static_cast<int32_t>(offsetof(UnitStateMovement_t1378301212, ___direction_9)); }
	inline Vector3_t4282066566  get_direction_9() const { return ___direction_9; }
	inline Vector3_t4282066566 * get_address_of_direction_9() { return &___direction_9; }
	inline void set_direction_9(Vector3_t4282066566  value)
	{
		___direction_9 = value;
	}

	inline static int32_t get_offset_of_avgtime_10() { return static_cast<int32_t>(offsetof(UnitStateMovement_t1378301212, ___avgtime_10)); }
	inline float get_avgtime_10() const { return ___avgtime_10; }
	inline float* get_address_of_avgtime_10() { return &___avgtime_10; }
	inline void set_avgtime_10(float value)
	{
		___avgtime_10 = value;
	}

	inline static int32_t get_offset_of_mTime_11() { return static_cast<int32_t>(offsetof(UnitStateMovement_t1378301212, ___mTime_11)); }
	inline float get_mTime_11() const { return ___mTime_11; }
	inline float* get_address_of_mTime_11() { return &___mTime_11; }
	inline void set_mTime_11(float value)
	{
		___mTime_11 = value;
	}

	inline static int32_t get_offset_of_maxtime_12() { return static_cast<int32_t>(offsetof(UnitStateMovement_t1378301212, ___maxtime_12)); }
	inline float get_maxtime_12() const { return ___maxtime_12; }
	inline float* get_address_of_maxtime_12() { return &___maxtime_12; }
	inline void set_maxtime_12(float value)
	{
		___maxtime_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
