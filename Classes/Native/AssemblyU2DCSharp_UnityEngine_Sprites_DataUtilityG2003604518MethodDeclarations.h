﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Sprites_DataUtilityGenerated
struct UnityEngine_Sprites_DataUtilityGenerated_t2003604518;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_Sprites_DataUtilityGenerated::.ctor()
extern "C"  void UnityEngine_Sprites_DataUtilityGenerated__ctor_m1489179957 (UnityEngine_Sprites_DataUtilityGenerated_t2003604518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Sprites_DataUtilityGenerated::DataUtility_DataUtility1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Sprites_DataUtilityGenerated_DataUtility_DataUtility1_m2246152735 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Sprites_DataUtilityGenerated::DataUtility_GetInnerUV__Sprite(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Sprites_DataUtilityGenerated_DataUtility_GetInnerUV__Sprite_m4068603542 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Sprites_DataUtilityGenerated::DataUtility_GetMinSize__Sprite(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Sprites_DataUtilityGenerated_DataUtility_GetMinSize__Sprite_m733002642 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Sprites_DataUtilityGenerated::DataUtility_GetOuterUV__Sprite(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Sprites_DataUtilityGenerated_DataUtility_GetOuterUV__Sprite_m4282381499 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Sprites_DataUtilityGenerated::DataUtility_GetPadding__Sprite(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Sprites_DataUtilityGenerated_DataUtility_GetPadding__Sprite_m2531364272 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Sprites_DataUtilityGenerated::__Register()
extern "C"  void UnityEngine_Sprites_DataUtilityGenerated___Register_m1056202930 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Sprites_DataUtilityGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_Sprites_DataUtilityGenerated_ilo_attachFinalizerObject1_m798785866 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_Sprites_DataUtilityGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_Sprites_DataUtilityGenerated_ilo_getObject2_m2278325275 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
