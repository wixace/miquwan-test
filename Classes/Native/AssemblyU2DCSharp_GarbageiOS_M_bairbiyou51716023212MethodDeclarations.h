﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_bairbiyou51
struct M_bairbiyou51_t716023212;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_bairbiyou51::.ctor()
extern "C"  void M_bairbiyou51__ctor_m368998199 (M_bairbiyou51_t716023212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_bairbiyou51::M_paciRaceroo0(System.String[],System.Int32)
extern "C"  void M_bairbiyou51_M_paciRaceroo0_m2536297628 (M_bairbiyou51_t716023212 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
