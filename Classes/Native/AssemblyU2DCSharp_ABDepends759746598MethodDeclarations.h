﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ABDepends
struct ABDepends_t759746598;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ABDepends::.ctor()
extern "C"  void ABDepends__ctor_m2946699269 (ABDepends_t759746598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABDepends::AddDepend(System.String,System.String[])
extern "C"  void ABDepends_AddDepend_m145359372 (ABDepends_t759746598 * __this, String_t* ___key0, StringU5BU5D_t4054002952* ___depends1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] ABDepends::GetDepend(System.String)
extern "C"  StringU5BU5D_t4054002952* ABDepends_GetDepend_m2247145020 (ABDepends_t759746598 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABDepends::Init()
extern "C"  void ABDepends_Init_m3233180079 (ABDepends_t759746598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ABDepends::GetDepend(System.String,System.String[]&)
extern "C"  bool ABDepends_GetDepend_m3808453353 (Il2CppObject * __this /* static, unused */, String_t* ___key0, StringU5BU5D_t4054002952** ___depends1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
