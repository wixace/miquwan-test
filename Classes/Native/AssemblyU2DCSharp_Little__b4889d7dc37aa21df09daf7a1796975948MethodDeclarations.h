﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._b4889d7dc37aa21df09daf7a1219da89
struct _b4889d7dc37aa21df09daf7a1219da89_t796975948;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._b4889d7dc37aa21df09daf7a1219da89::.ctor()
extern "C"  void _b4889d7dc37aa21df09daf7a1219da89__ctor_m2707372129 (_b4889d7dc37aa21df09daf7a1219da89_t796975948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b4889d7dc37aa21df09daf7a1219da89::_b4889d7dc37aa21df09daf7a1219da89m2(System.Int32)
extern "C"  int32_t _b4889d7dc37aa21df09daf7a1219da89__b4889d7dc37aa21df09daf7a1219da89m2_m3086478873 (_b4889d7dc37aa21df09daf7a1219da89_t796975948 * __this, int32_t ____b4889d7dc37aa21df09daf7a1219da89a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b4889d7dc37aa21df09daf7a1219da89::_b4889d7dc37aa21df09daf7a1219da89m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _b4889d7dc37aa21df09daf7a1219da89__b4889d7dc37aa21df09daf7a1219da89m_m3647326397 (_b4889d7dc37aa21df09daf7a1219da89_t796975948 * __this, int32_t ____b4889d7dc37aa21df09daf7a1219da89a0, int32_t ____b4889d7dc37aa21df09daf7a1219da89191, int32_t ____b4889d7dc37aa21df09daf7a1219da89c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
