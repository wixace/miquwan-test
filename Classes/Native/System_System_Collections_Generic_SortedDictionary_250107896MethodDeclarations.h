﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>
struct SortedDictionary_2_t186228230;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary_250107896.h"

// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m374370775_gshared (Enumerator_t50107896 * __this, SortedDictionary_2_t186228230 * ___dic0, const MethodInfo* method);
#define Enumerator__ctor_m374370775(__this, ___dic0, method) ((  void (*) (Enumerator_t50107896 *, SortedDictionary_2_t186228230 *, const MethodInfo*))Enumerator__ctor_m374370775_gshared)(__this, ___dic0, method)
// System.Object System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m803363879_gshared (Enumerator_t50107896 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m803363879(__this, method) ((  Il2CppObject * (*) (Enumerator_t50107896 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m803363879_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2331646843_gshared (Enumerator_t50107896 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2331646843(__this, method) ((  void (*) (Enumerator_t50107896 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2331646843_gshared)(__this, method)
// TValue System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m978680143_gshared (Enumerator_t50107896 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m978680143(__this, method) ((  Il2CppObject * (*) (Enumerator_t50107896 *, const MethodInfo*))Enumerator_get_Current_m978680143_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2486277459_gshared (Enumerator_t50107896 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2486277459(__this, method) ((  bool (*) (Enumerator_t50107896 *, const MethodInfo*))Enumerator_MoveNext_m2486277459_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3273221660_gshared (Enumerator_t50107896 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3273221660(__this, method) ((  void (*) (Enumerator_t50107896 *, const MethodInfo*))Enumerator_Dispose_m3273221660_gshared)(__this, method)
