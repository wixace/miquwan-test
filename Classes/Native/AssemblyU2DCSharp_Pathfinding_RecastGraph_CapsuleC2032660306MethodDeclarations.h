﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RecastGraph/CapsuleCache
struct CapsuleCache_t2032660306;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.RecastGraph/CapsuleCache::.ctor()
extern "C"  void CapsuleCache__ctor_m2550945673 (CapsuleCache_t2032660306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
