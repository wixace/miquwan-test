﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BMGlyphGenerated
struct BMGlyphGenerated_t4291550958;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// BMGlyph
struct BMGlyph_t719052705;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_BMGlyph719052705.h"

// System.Void BMGlyphGenerated::.ctor()
extern "C"  void BMGlyphGenerated__ctor_m1229599597 (BMGlyphGenerated_t4291550958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BMGlyphGenerated::BMGlyph_BMGlyph1(JSVCall,System.Int32)
extern "C"  bool BMGlyphGenerated_BMGlyph_BMGlyph1_m4192278505 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMGlyphGenerated::BMGlyph_index(JSVCall)
extern "C"  void BMGlyphGenerated_BMGlyph_index_m1561527400 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMGlyphGenerated::BMGlyph_x(JSVCall)
extern "C"  void BMGlyphGenerated_BMGlyph_x_m547045762 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMGlyphGenerated::BMGlyph_y(JSVCall)
extern "C"  void BMGlyphGenerated_BMGlyph_y_m350532257 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMGlyphGenerated::BMGlyph_width(JSVCall)
extern "C"  void BMGlyphGenerated_BMGlyph_width_m2205438452 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMGlyphGenerated::BMGlyph_height(JSVCall)
extern "C"  void BMGlyphGenerated_BMGlyph_height_m304558923 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMGlyphGenerated::BMGlyph_offsetX(JSVCall)
extern "C"  void BMGlyphGenerated_BMGlyph_offsetX_m517947253 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMGlyphGenerated::BMGlyph_offsetY(JSVCall)
extern "C"  void BMGlyphGenerated_BMGlyph_offsetY_m321433748 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMGlyphGenerated::BMGlyph_advance(JSVCall)
extern "C"  void BMGlyphGenerated_BMGlyph_advance_m933109176 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMGlyphGenerated::BMGlyph_channel(JSVCall)
extern "C"  void BMGlyphGenerated_BMGlyph_channel_m3480275543 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMGlyphGenerated::BMGlyph_kerning(JSVCall)
extern "C"  void BMGlyphGenerated_BMGlyph_kerning_m508212846 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BMGlyphGenerated::BMGlyph_GetKerning__Int32(JSVCall,System.Int32)
extern "C"  bool BMGlyphGenerated_BMGlyph_GetKerning__Int32_m3930460353 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BMGlyphGenerated::BMGlyph_SetKerning__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool BMGlyphGenerated_BMGlyph_SetKerning__Int32__Int32_m152571619 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BMGlyphGenerated::BMGlyph_Trim__Int32__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool BMGlyphGenerated_BMGlyph_Trim__Int32__Int32__Int32__Int32_m1008253819 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMGlyphGenerated::__Register()
extern "C"  void BMGlyphGenerated___Register_m465528186 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BMGlyphGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t BMGlyphGenerated_ilo_getObject1_m2609332677 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMGlyphGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void BMGlyphGenerated_ilo_addJSCSRel2_m817970218 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BMGlyphGenerated::ilo_getInt323(System.Int32)
extern "C"  int32_t BMGlyphGenerated_ilo_getInt323_m2114966454 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMGlyphGenerated::ilo_setInt324(System.Int32,System.Int32)
extern "C"  void BMGlyphGenerated_ilo_setInt324_m4038154278 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BMGlyphGenerated::ilo_getObject5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * BMGlyphGenerated_ilo_getObject5_m1365840358 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BMGlyphGenerated::ilo_GetKerning6(BMGlyph,System.Int32)
extern "C"  int32_t BMGlyphGenerated_ilo_GetKerning6_m423448208 (Il2CppObject * __this /* static, unused */, BMGlyph_t719052705 * ____this0, int32_t ___previousChar1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BMGlyphGenerated::ilo_SetKerning7(BMGlyph,System.Int32,System.Int32)
extern "C"  void BMGlyphGenerated_ilo_SetKerning7_m822597900 (Il2CppObject * __this /* static, unused */, BMGlyph_t719052705 * ____this0, int32_t ___previousChar1, int32_t ___amount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
