﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pomelo.Protobuf.Decoder
struct Decoder_t2687281370;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void Pomelo.Protobuf.Decoder::.ctor()
extern "C"  void Decoder__ctor_m4150572662 (Decoder_t2687281370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pomelo.Protobuf.Decoder::decodeUInt32(System.Int32,System.Byte[],System.Int32&)
extern "C"  uint32_t Decoder_decodeUInt32_m1871305923 (Il2CppObject * __this /* static, unused */, int32_t ___offset0, ByteU5BU5D_t4260760469* ___bytes1, int32_t* ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pomelo.Protobuf.Decoder::decodeUInt32(System.Byte[])
extern "C"  uint32_t Decoder_decodeUInt32_m3969551023 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pomelo.Protobuf.Decoder::decodeSInt32(System.Byte[])
extern "C"  int32_t Decoder_decodeSInt32_m2028220960 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
