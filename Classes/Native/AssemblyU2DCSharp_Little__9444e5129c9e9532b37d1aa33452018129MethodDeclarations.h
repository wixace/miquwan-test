﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._9444e5129c9e9532b37d1aa35f7acb12
struct _9444e5129c9e9532b37d1aa35f7acb12_t3452018129;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__9444e5129c9e9532b37d1aa33452018129.h"

// System.Void Little._9444e5129c9e9532b37d1aa35f7acb12::.ctor()
extern "C"  void _9444e5129c9e9532b37d1aa35f7acb12__ctor_m1366786940 (_9444e5129c9e9532b37d1aa35f7acb12_t3452018129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._9444e5129c9e9532b37d1aa35f7acb12::_9444e5129c9e9532b37d1aa35f7acb12m2(System.Int32)
extern "C"  int32_t _9444e5129c9e9532b37d1aa35f7acb12__9444e5129c9e9532b37d1aa35f7acb12m2_m2503358457 (_9444e5129c9e9532b37d1aa35f7acb12_t3452018129 * __this, int32_t ____9444e5129c9e9532b37d1aa35f7acb12a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._9444e5129c9e9532b37d1aa35f7acb12::_9444e5129c9e9532b37d1aa35f7acb12m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _9444e5129c9e9532b37d1aa35f7acb12__9444e5129c9e9532b37d1aa35f7acb12m_m1422154461 (_9444e5129c9e9532b37d1aa35f7acb12_t3452018129 * __this, int32_t ____9444e5129c9e9532b37d1aa35f7acb12a0, int32_t ____9444e5129c9e9532b37d1aa35f7acb12611, int32_t ____9444e5129c9e9532b37d1aa35f7acb12c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._9444e5129c9e9532b37d1aa35f7acb12::ilo__9444e5129c9e9532b37d1aa35f7acb12m21(Little._9444e5129c9e9532b37d1aa35f7acb12,System.Int32)
extern "C"  int32_t _9444e5129c9e9532b37d1aa35f7acb12_ilo__9444e5129c9e9532b37d1aa35f7acb12m21_m2313154744 (Il2CppObject * __this /* static, unused */, _9444e5129c9e9532b37d1aa35f7acb12_t3452018129 * ____this0, int32_t ____9444e5129c9e9532b37d1aa35f7acb12a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
