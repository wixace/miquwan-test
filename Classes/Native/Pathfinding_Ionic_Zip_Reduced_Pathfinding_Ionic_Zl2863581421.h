﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Ionic.Zlib.InternalInflateConstants
struct  InternalInflateConstants_t2863581421  : public Il2CppObject
{
public:

public:
};

struct InternalInflateConstants_t2863581421_StaticFields
{
public:
	// System.Int32[] Pathfinding.Ionic.Zlib.InternalInflateConstants::InflateMask
	Int32U5BU5D_t3230847821* ___InflateMask_0;

public:
	inline static int32_t get_offset_of_InflateMask_0() { return static_cast<int32_t>(offsetof(InternalInflateConstants_t2863581421_StaticFields, ___InflateMask_0)); }
	inline Int32U5BU5D_t3230847821* get_InflateMask_0() const { return ___InflateMask_0; }
	inline Int32U5BU5D_t3230847821** get_address_of_InflateMask_0() { return &___InflateMask_0; }
	inline void set_InflateMask_0(Int32U5BU5D_t3230847821* value)
	{
		___InflateMask_0 = value;
		Il2CppCodeGenWriteBarrier(&___InflateMask_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
