﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<MHIAPMgr.PayInfo>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2116406230(__this, ___l0, method) ((  void (*) (Enumerator_t1832288558 *, List_1_t1812615788 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<MHIAPMgr.PayInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2701310652(__this, method) ((  void (*) (Enumerator_t1832288558 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<MHIAPMgr.PayInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m869648562(__this, method) ((  Il2CppObject * (*) (Enumerator_t1832288558 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<MHIAPMgr.PayInfo>::Dispose()
#define Enumerator_Dispose_m3099209531(__this, method) ((  void (*) (Enumerator_t1832288558 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<MHIAPMgr.PayInfo>::VerifyState()
#define Enumerator_VerifyState_m3464354036(__this, method) ((  void (*) (Enumerator_t1832288558 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<MHIAPMgr.PayInfo>::MoveNext()
#define Enumerator_MoveNext_m768394860(__this, method) ((  bool (*) (Enumerator_t1832288558 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<MHIAPMgr.PayInfo>::get_Current()
#define Enumerator_get_Current_m767003789(__this, method) ((  PayInfo_t444430236 * (*) (Enumerator_t1832288558 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
