﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationPointJSC
struct  AnimationPointJSC_t487595022  : public Il2CppObject
{
public:
	// System.String AnimationPointJSC::_aniType
	String_t* ____aniType_0;
	// System.Single AnimationPointJSC::_aniTime
	float ____aniTime_1;
	// System.Single AnimationPointJSC::_px
	float ____px_2;
	// System.Single AnimationPointJSC::_py
	float ____py_3;
	// System.Single AnimationPointJSC::_pz
	float ____pz_4;
	// System.Single AnimationPointJSC::_rx
	float ____rx_5;
	// System.Single AnimationPointJSC::_ry
	float ____ry_6;
	// System.Single AnimationPointJSC::_rz
	float ____rz_7;
	// System.Single AnimationPointJSC::_delay
	float ____delay_8;
	// System.Single AnimationPointJSC::_time
	float ____time_9;
	// System.String AnimationPointJSC::_fun
	String_t* ____fun_10;
	// System.Single AnimationPointJSC::_vx
	float ____vx_11;
	// System.Single AnimationPointJSC::_vy
	float ____vy_12;
	// System.Single AnimationPointJSC::_vz
	float ____vz_13;
	// System.Single AnimationPointJSC::_vibrationTime
	float ____vibrationTime_14;
	// System.Single AnimationPointJSC::_vibrationDelay
	float ____vibrationDelay_15;
	// ProtoBuf.IExtension AnimationPointJSC::extensionObject
	Il2CppObject * ___extensionObject_16;

public:
	inline static int32_t get_offset_of__aniType_0() { return static_cast<int32_t>(offsetof(AnimationPointJSC_t487595022, ____aniType_0)); }
	inline String_t* get__aniType_0() const { return ____aniType_0; }
	inline String_t** get_address_of__aniType_0() { return &____aniType_0; }
	inline void set__aniType_0(String_t* value)
	{
		____aniType_0 = value;
		Il2CppCodeGenWriteBarrier(&____aniType_0, value);
	}

	inline static int32_t get_offset_of__aniTime_1() { return static_cast<int32_t>(offsetof(AnimationPointJSC_t487595022, ____aniTime_1)); }
	inline float get__aniTime_1() const { return ____aniTime_1; }
	inline float* get_address_of__aniTime_1() { return &____aniTime_1; }
	inline void set__aniTime_1(float value)
	{
		____aniTime_1 = value;
	}

	inline static int32_t get_offset_of__px_2() { return static_cast<int32_t>(offsetof(AnimationPointJSC_t487595022, ____px_2)); }
	inline float get__px_2() const { return ____px_2; }
	inline float* get_address_of__px_2() { return &____px_2; }
	inline void set__px_2(float value)
	{
		____px_2 = value;
	}

	inline static int32_t get_offset_of__py_3() { return static_cast<int32_t>(offsetof(AnimationPointJSC_t487595022, ____py_3)); }
	inline float get__py_3() const { return ____py_3; }
	inline float* get_address_of__py_3() { return &____py_3; }
	inline void set__py_3(float value)
	{
		____py_3 = value;
	}

	inline static int32_t get_offset_of__pz_4() { return static_cast<int32_t>(offsetof(AnimationPointJSC_t487595022, ____pz_4)); }
	inline float get__pz_4() const { return ____pz_4; }
	inline float* get_address_of__pz_4() { return &____pz_4; }
	inline void set__pz_4(float value)
	{
		____pz_4 = value;
	}

	inline static int32_t get_offset_of__rx_5() { return static_cast<int32_t>(offsetof(AnimationPointJSC_t487595022, ____rx_5)); }
	inline float get__rx_5() const { return ____rx_5; }
	inline float* get_address_of__rx_5() { return &____rx_5; }
	inline void set__rx_5(float value)
	{
		____rx_5 = value;
	}

	inline static int32_t get_offset_of__ry_6() { return static_cast<int32_t>(offsetof(AnimationPointJSC_t487595022, ____ry_6)); }
	inline float get__ry_6() const { return ____ry_6; }
	inline float* get_address_of__ry_6() { return &____ry_6; }
	inline void set__ry_6(float value)
	{
		____ry_6 = value;
	}

	inline static int32_t get_offset_of__rz_7() { return static_cast<int32_t>(offsetof(AnimationPointJSC_t487595022, ____rz_7)); }
	inline float get__rz_7() const { return ____rz_7; }
	inline float* get_address_of__rz_7() { return &____rz_7; }
	inline void set__rz_7(float value)
	{
		____rz_7 = value;
	}

	inline static int32_t get_offset_of__delay_8() { return static_cast<int32_t>(offsetof(AnimationPointJSC_t487595022, ____delay_8)); }
	inline float get__delay_8() const { return ____delay_8; }
	inline float* get_address_of__delay_8() { return &____delay_8; }
	inline void set__delay_8(float value)
	{
		____delay_8 = value;
	}

	inline static int32_t get_offset_of__time_9() { return static_cast<int32_t>(offsetof(AnimationPointJSC_t487595022, ____time_9)); }
	inline float get__time_9() const { return ____time_9; }
	inline float* get_address_of__time_9() { return &____time_9; }
	inline void set__time_9(float value)
	{
		____time_9 = value;
	}

	inline static int32_t get_offset_of__fun_10() { return static_cast<int32_t>(offsetof(AnimationPointJSC_t487595022, ____fun_10)); }
	inline String_t* get__fun_10() const { return ____fun_10; }
	inline String_t** get_address_of__fun_10() { return &____fun_10; }
	inline void set__fun_10(String_t* value)
	{
		____fun_10 = value;
		Il2CppCodeGenWriteBarrier(&____fun_10, value);
	}

	inline static int32_t get_offset_of__vx_11() { return static_cast<int32_t>(offsetof(AnimationPointJSC_t487595022, ____vx_11)); }
	inline float get__vx_11() const { return ____vx_11; }
	inline float* get_address_of__vx_11() { return &____vx_11; }
	inline void set__vx_11(float value)
	{
		____vx_11 = value;
	}

	inline static int32_t get_offset_of__vy_12() { return static_cast<int32_t>(offsetof(AnimationPointJSC_t487595022, ____vy_12)); }
	inline float get__vy_12() const { return ____vy_12; }
	inline float* get_address_of__vy_12() { return &____vy_12; }
	inline void set__vy_12(float value)
	{
		____vy_12 = value;
	}

	inline static int32_t get_offset_of__vz_13() { return static_cast<int32_t>(offsetof(AnimationPointJSC_t487595022, ____vz_13)); }
	inline float get__vz_13() const { return ____vz_13; }
	inline float* get_address_of__vz_13() { return &____vz_13; }
	inline void set__vz_13(float value)
	{
		____vz_13 = value;
	}

	inline static int32_t get_offset_of__vibrationTime_14() { return static_cast<int32_t>(offsetof(AnimationPointJSC_t487595022, ____vibrationTime_14)); }
	inline float get__vibrationTime_14() const { return ____vibrationTime_14; }
	inline float* get_address_of__vibrationTime_14() { return &____vibrationTime_14; }
	inline void set__vibrationTime_14(float value)
	{
		____vibrationTime_14 = value;
	}

	inline static int32_t get_offset_of__vibrationDelay_15() { return static_cast<int32_t>(offsetof(AnimationPointJSC_t487595022, ____vibrationDelay_15)); }
	inline float get__vibrationDelay_15() const { return ____vibrationDelay_15; }
	inline float* get_address_of__vibrationDelay_15() { return &____vibrationDelay_15; }
	inline void set__vibrationDelay_15(float value)
	{
		____vibrationDelay_15 = value;
	}

	inline static int32_t get_offset_of_extensionObject_16() { return static_cast<int32_t>(offsetof(AnimationPointJSC_t487595022, ___extensionObject_16)); }
	inline Il2CppObject * get_extensionObject_16() const { return ___extensionObject_16; }
	inline Il2CppObject ** get_address_of_extensionObject_16() { return &___extensionObject_16; }
	inline void set_extensionObject_16(Il2CppObject * value)
	{
		___extensionObject_16 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObject_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
