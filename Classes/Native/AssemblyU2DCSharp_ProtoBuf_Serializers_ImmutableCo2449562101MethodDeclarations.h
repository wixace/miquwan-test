﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.ImmutableCollectionDecorator
struct ImmutableCollectionDecorator_t2449562101;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// ProtoBuf.Serializers.IProtoSerializer
struct IProtoSerializer_t3033312651;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// System.String
struct String_t;
// System.Reflection.Assembly
struct Assembly_t1418687608;
// System.Type[]
struct TypeU5BU5D_t3339007067;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_ProtoBuf_WireType2355646059.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Reflection_Assembly1418687608.h"
#include "AssemblyU2DCSharp_ProtoBuf_SubItemToken3365128146.h"

// System.Void ProtoBuf.Serializers.ImmutableCollectionDecorator::.ctor(ProtoBuf.Meta.TypeModel,System.Type,System.Type,ProtoBuf.Serializers.IProtoSerializer,System.Int32,System.Boolean,ProtoBuf.WireType,System.Boolean,System.Boolean,System.Boolean,System.Reflection.MethodInfo,System.Reflection.MethodInfo,System.Reflection.MethodInfo,System.Reflection.MethodInfo)
extern "C"  void ImmutableCollectionDecorator__ctor_m1716865011 (ImmutableCollectionDecorator_t2449562101 * __this, TypeModel_t2730011105 * ___model0, Type_t * ___declaredType1, Type_t * ___concreteType2, Il2CppObject * ___tail3, int32_t ___fieldNumber4, bool ___writePacked5, int32_t ___packedWireType6, bool ___returnList7, bool ___overwriteList8, bool ___supportNull9, MethodInfo_t * ___builderFactory10, MethodInfo_t * ___add11, MethodInfo_t * ___addRange12, MethodInfo_t * ___finish13, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ImmutableCollectionDecorator::get_RequireAdd()
extern "C"  bool ImmutableCollectionDecorator_get_RequireAdd_m155526165 (ImmutableCollectionDecorator_t2449562101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.ImmutableCollectionDecorator::ResolveIReadOnlyCollection(System.Type,System.Type)
extern "C"  Type_t * ImmutableCollectionDecorator_ResolveIReadOnlyCollection_m3897862917 (Il2CppObject * __this /* static, unused */, Type_t * ___declaredType0, Type_t * ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ImmutableCollectionDecorator::IdentifyImmutable(ProtoBuf.Meta.TypeModel,System.Type,System.Reflection.MethodInfo&,System.Reflection.MethodInfo&,System.Reflection.MethodInfo&,System.Reflection.MethodInfo&)
extern "C"  bool ImmutableCollectionDecorator_IdentifyImmutable_m535290968 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Type_t * ___declaredType1, MethodInfo_t ** ___builderFactory2, MethodInfo_t ** ___add3, MethodInfo_t ** ___addRange4, MethodInfo_t ** ___finish5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.ImmutableCollectionDecorator::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * ImmutableCollectionDecorator_Read_m731872175 (ImmutableCollectionDecorator_t2449562101 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.ImmutableCollectionDecorator::ilo_MapType1(ProtoBuf.Meta.TypeModel,System.Type)
extern "C"  Type_t * ImmutableCollectionDecorator_ilo_MapType1_m3320458820 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.ImmutableCollectionDecorator::ilo_GetType2(ProtoBuf.Meta.TypeModel,System.String,System.Reflection.Assembly)
extern "C"  Type_t * ImmutableCollectionDecorator_ilo_GetType2_m564531894 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, String_t* ___fullName1, Assembly_t1418687608 * ___context2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Serializers.ImmutableCollectionDecorator::ilo_GetInstanceMethod3(System.Type,System.String,System.Type[])
extern "C"  MethodInfo_t * ImmutableCollectionDecorator_ilo_GetInstanceMethod3_m4167234258 (Il2CppObject * __this /* static, unused */, Type_t * ___declaringType0, String_t* ___name1, TypeU5BU5D_t3339007067* ___types2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.ImmutableCollectionDecorator::ilo_MapType4(ProtoBuf.Meta.TypeModel,System.Type,System.Boolean)
extern "C"  Type_t * ImmutableCollectionDecorator_ilo_MapType4_m1013147964 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, Type_t * ___type1, bool ___demand2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Serializers.ImmutableCollectionDecorator::ilo_get_FieldNumber5(ProtoBuf.ProtoReader)
extern "C"  int32_t ImmutableCollectionDecorator_ilo_get_FieldNumber5_m2654941148 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.SubItemToken ProtoBuf.Serializers.ImmutableCollectionDecorator::ilo_StartSubItem6(ProtoBuf.ProtoReader)
extern "C"  SubItemToken_t3365128146  ImmutableCollectionDecorator_ilo_StartSubItem6_m2951470386 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.ImmutableCollectionDecorator::ilo_EndSubItem7(ProtoBuf.SubItemToken,ProtoBuf.ProtoReader)
extern "C"  void ImmutableCollectionDecorator_ilo_EndSubItem7_m1300878559 (Il2CppObject * __this /* static, unused */, SubItemToken_t3365128146  ___token0, ProtoReader_t3962509489 * ___reader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.ImmutableCollectionDecorator::ilo_Read8(ProtoBuf.Serializers.IProtoSerializer,System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * ImmutableCollectionDecorator_ilo_Read8_m2202305281 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___value1, ProtoReader_t3962509489 * ___source2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.ImmutableCollectionDecorator::ilo_TryReadFieldHeader9(ProtoBuf.ProtoReader,System.Int32)
extern "C"  bool ImmutableCollectionDecorator_ilo_TryReadFieldHeader9_m328869853 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, int32_t ___field1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
