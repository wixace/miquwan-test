﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>
struct ReadOnlyCollection_1_t1480139955;
// System.Collections.Generic.IList`1<Pathfinding.Voxels.ExtraMesh>
struct IList_1_t2617709622;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.Voxels.ExtraMesh[]
struct ExtraMeshU5BU5D_t2875893186;
// System.Collections.Generic.IEnumerator`1<Pathfinding.Voxels.ExtraMesh>
struct IEnumerator_1_t1834927468;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_ExtraMesh4218029715.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2375660874_gshared (ReadOnlyCollection_1_t1480139955 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2375660874(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1480139955 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2375660874_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m162411060_gshared (ReadOnlyCollection_1_t1480139955 * __this, ExtraMesh_t4218029715  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m162411060(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1480139955 *, ExtraMesh_t4218029715 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m162411060_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2746466582_gshared (ReadOnlyCollection_1_t1480139955 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2746466582(__this, method) ((  void (*) (ReadOnlyCollection_1_t1480139955 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2746466582_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3540550683_gshared (ReadOnlyCollection_1_t1480139955 * __this, int32_t ___index0, ExtraMesh_t4218029715  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3540550683(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1480139955 *, int32_t, ExtraMesh_t4218029715 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3540550683_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1067409923_gshared (ReadOnlyCollection_1_t1480139955 * __this, ExtraMesh_t4218029715  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1067409923(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1480139955 *, ExtraMesh_t4218029715 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1067409923_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1414403553_gshared (ReadOnlyCollection_1_t1480139955 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1414403553(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1480139955 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1414403553_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  ExtraMesh_t4218029715  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2130706247_gshared (ReadOnlyCollection_1_t1480139955 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2130706247(__this, ___index0, method) ((  ExtraMesh_t4218029715  (*) (ReadOnlyCollection_1_t1480139955 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2130706247_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2664704562_gshared (ReadOnlyCollection_1_t1480139955 * __this, int32_t ___index0, ExtraMesh_t4218029715  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2664704562(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1480139955 *, int32_t, ExtraMesh_t4218029715 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2664704562_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2538185260_gshared (ReadOnlyCollection_1_t1480139955 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2538185260(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1480139955 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2538185260_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2904948473_gshared (ReadOnlyCollection_1_t1480139955 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2904948473(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1480139955 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2904948473_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2681309640_gshared (ReadOnlyCollection_1_t1480139955 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2681309640(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1480139955 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2681309640_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2058800885_gshared (ReadOnlyCollection_1_t1480139955 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m2058800885(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1480139955 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m2058800885_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m935670279_gshared (ReadOnlyCollection_1_t1480139955 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m935670279(__this, method) ((  void (*) (ReadOnlyCollection_1_t1480139955 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m935670279_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m3185794027_gshared (ReadOnlyCollection_1_t1480139955 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3185794027(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1480139955 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m3185794027_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1130136269_gshared (ReadOnlyCollection_1_t1480139955 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1130136269(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1480139955 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1130136269_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m4192849344_gshared (ReadOnlyCollection_1_t1480139955 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m4192849344(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1480139955 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m4192849344_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1481540456_gshared (ReadOnlyCollection_1_t1480139955 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1481540456(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1480139955 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1481540456_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m268469584_gshared (ReadOnlyCollection_1_t1480139955 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m268469584(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1480139955 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m268469584_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2092494737_gshared (ReadOnlyCollection_1_t1480139955 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2092494737(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1480139955 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2092494737_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m101649283_gshared (ReadOnlyCollection_1_t1480139955 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m101649283(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1480139955 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m101649283_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1357715354_gshared (ReadOnlyCollection_1_t1480139955 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1357715354(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1480139955 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1357715354_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m774900767_gshared (ReadOnlyCollection_1_t1480139955 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m774900767(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1480139955 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m774900767_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m4261292106_gshared (ReadOnlyCollection_1_t1480139955 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m4261292106(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1480139955 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m4261292106_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1262848407_gshared (ReadOnlyCollection_1_t1480139955 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1262848407(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1480139955 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1262848407_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3283316296_gshared (ReadOnlyCollection_1_t1480139955 * __this, ExtraMesh_t4218029715  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3283316296(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1480139955 *, ExtraMesh_t4218029715 , const MethodInfo*))ReadOnlyCollection_1_Contains_m3283316296_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2116018532_gshared (ReadOnlyCollection_1_t1480139955 * __this, ExtraMeshU5BU5D_t2875893186* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m2116018532(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1480139955 *, ExtraMeshU5BU5D_t2875893186*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m2116018532_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3015188255_gshared (ReadOnlyCollection_1_t1480139955 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3015188255(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1480139955 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3015188255_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1264078128_gshared (ReadOnlyCollection_1_t1480139955 * __this, ExtraMesh_t4218029715  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1264078128(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1480139955 *, ExtraMesh_t4218029715 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1264078128_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m349023819_gshared (ReadOnlyCollection_1_t1480139955 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m349023819(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1480139955 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m349023819_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.ExtraMesh>::get_Item(System.Int32)
extern "C"  ExtraMesh_t4218029715  ReadOnlyCollection_1_get_Item_m1778923655_gshared (ReadOnlyCollection_1_t1480139955 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1778923655(__this, ___index0, method) ((  ExtraMesh_t4218029715  (*) (ReadOnlyCollection_1_t1480139955 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1778923655_gshared)(__this, ___index0, method)
