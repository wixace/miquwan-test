﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_vemjewai192
struct M_vemjewai192_t1694127012;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_vemjewai1921694127012.h"

// System.Void GarbageiOS.M_vemjewai192::.ctor()
extern "C"  void M_vemjewai192__ctor_m4190765119 (M_vemjewai192_t1694127012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vemjewai192::M_jichaiMiscel0(System.String[],System.Int32)
extern "C"  void M_vemjewai192_M_jichaiMiscel0_m2530251987 (M_vemjewai192_t1694127012 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vemjewai192::M_telhaHissas1(System.String[],System.Int32)
extern "C"  void M_vemjewai192_M_telhaHissas1_m1351641592 (M_vemjewai192_t1694127012 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vemjewai192::M_tranaPodrasoo2(System.String[],System.Int32)
extern "C"  void M_vemjewai192_M_tranaPodrasoo2_m451515115 (M_vemjewai192_t1694127012 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vemjewai192::M_purmalgor3(System.String[],System.Int32)
extern "C"  void M_vemjewai192_M_purmalgor3_m4271220226 (M_vemjewai192_t1694127012 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vemjewai192::M_nejirTeltoukaw4(System.String[],System.Int32)
extern "C"  void M_vemjewai192_M_nejirTeltoukaw4_m3570713008 (M_vemjewai192_t1694127012 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vemjewai192::ilo_M_tranaPodrasoo21(GarbageiOS.M_vemjewai192,System.String[],System.Int32)
extern "C"  void M_vemjewai192_ilo_M_tranaPodrasoo21_m1108952205 (Il2CppObject * __this /* static, unused */, M_vemjewai192_t1694127012 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vemjewai192::ilo_M_purmalgor32(GarbageiOS.M_vemjewai192,System.String[],System.Int32)
extern "C"  void M_vemjewai192_ilo_M_purmalgor32_m3353410883 (Il2CppObject * __this /* static, unused */, M_vemjewai192_t1694127012 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
