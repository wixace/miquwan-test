﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.LODGroup
struct LODGroup_t1930694210;
// UnityEngine.LOD[]
struct LODU5BU5D_t1411784526;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_LODFadeMode102325078.h"

// System.Void UnityEngine.LODGroup::.ctor()
extern "C"  void LODGroup__ctor_m2683402095 (LODGroup_t1930694210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.LODGroup::get_localReferencePoint()
extern "C"  Vector3_t4282066566  LODGroup_get_localReferencePoint_m1717276954 (LODGroup_t1930694210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LODGroup::set_localReferencePoint(UnityEngine.Vector3)
extern "C"  void LODGroup_set_localReferencePoint_m2391108857 (LODGroup_t1930694210 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LODGroup::INTERNAL_get_localReferencePoint(UnityEngine.Vector3&)
extern "C"  void LODGroup_INTERNAL_get_localReferencePoint_m1243978253 (LODGroup_t1930694210 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LODGroup::INTERNAL_set_localReferencePoint(UnityEngine.Vector3&)
extern "C"  void LODGroup_INTERNAL_set_localReferencePoint_m2309928985 (LODGroup_t1930694210 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.LODGroup::get_size()
extern "C"  float LODGroup_get_size_m1285674411 (LODGroup_t1930694210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LODGroup::set_size(System.Single)
extern "C"  void LODGroup_set_size_m1430818936 (LODGroup_t1930694210 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.LODGroup::get_lodCount()
extern "C"  int32_t LODGroup_get_lodCount_m3925980076 (LODGroup_t1930694210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LODFadeMode UnityEngine.LODGroup::get_fadeMode()
extern "C"  int32_t LODGroup_get_fadeMode_m689922183 (LODGroup_t1930694210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LODGroup::set_fadeMode(UnityEngine.LODFadeMode)
extern "C"  void LODGroup_set_fadeMode_m1814345496 (LODGroup_t1930694210 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.LODGroup::get_animateCrossFading()
extern "C"  bool LODGroup_get_animateCrossFading_m4060990844 (LODGroup_t1930694210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LODGroup::set_animateCrossFading(System.Boolean)
extern "C"  void LODGroup_set_animateCrossFading_m4238468993 (LODGroup_t1930694210 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.LODGroup::get_enabled()
extern "C"  bool LODGroup_get_enabled_m1172166751 (LODGroup_t1930694210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LODGroup::set_enabled(System.Boolean)
extern "C"  void LODGroup_set_enabled_m1860909768 (LODGroup_t1930694210 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LODGroup::RecalculateBounds()
extern "C"  void LODGroup_RecalculateBounds_m1990420405 (LODGroup_t1930694210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LOD[] UnityEngine.LODGroup::GetLODs()
extern "C"  LODU5BU5D_t1411784526* LODGroup_GetLODs_m1939673198 (LODGroup_t1930694210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LODGroup::SetLODs(UnityEngine.LOD[])
extern "C"  void LODGroup_SetLODs_m204360677 (LODGroup_t1930694210 * __this, LODU5BU5D_t1411784526* ___lods0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LODGroup::ForceLOD(System.Int32)
extern "C"  void LODGroup_ForceLOD_m123781532 (LODGroup_t1930694210 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.LODGroup::get_crossFadeAnimationDuration()
extern "C"  float LODGroup_get_crossFadeAnimationDuration_m982714342 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LODGroup::set_crossFadeAnimationDuration(System.Single)
extern "C"  void LODGroup_set_crossFadeAnimationDuration_m2054460445 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
