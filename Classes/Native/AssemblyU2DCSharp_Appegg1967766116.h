﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// Appegg/ViewConfig
struct ViewConfig_t3906840306;
// Appegg/SceneConfig
struct SceneConfig_t3474339747;
// Appegg/GeneralConfig
struct GeneralConfig_t3043367359;
// Appegg/TestConfig
struct TestConfig_t3201747743;
// Appegg/ServerConfig
struct ServerConfig_t2996416080;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Appegg
struct  Appegg_t1967766116  : public MonoBehaviour_t667441552
{
public:
	// System.String Appegg::_appId
	String_t* ____appId_2;
	// System.String Appegg::_restApiKey
	String_t* ____restApiKey_3;
	// System.String[] Appegg::_configUrl
	StringU5BU5D_t4054002952* ____configUrl_4;
	// Appegg/ViewConfig Appegg::_view
	ViewConfig_t3906840306 * ____view_5;
	// Appegg/SceneConfig Appegg::_scene
	SceneConfig_t3474339747 * ____scene_6;
	// Appegg/GeneralConfig Appegg::_general
	GeneralConfig_t3043367359 * ____general_7;
	// Appegg/TestConfig Appegg::_test
	TestConfig_t3201747743 * ____test_8;
	// Appegg/ServerConfig Appegg::_serverConfig
	ServerConfig_t2996416080 * ____serverConfig_9;
	// System.String Appegg::_idfa
	String_t* ____idfa_10;
	// System.String Appegg::_idfv
	String_t* ____idfv_11;
	// System.Int32 Appegg::_currentVersion
	int32_t ____currentVersion_12;
	// System.Int32 Appegg::_currentHybridIndex
	int32_t ____currentHybridIndex_13;
	// System.Boolean Appegg::_isDataCollected
	bool ____isDataCollected_14;
	// System.Boolean Appegg::_isDownloaded
	bool ____isDownloaded_15;

public:
	inline static int32_t get_offset_of__appId_2() { return static_cast<int32_t>(offsetof(Appegg_t1967766116, ____appId_2)); }
	inline String_t* get__appId_2() const { return ____appId_2; }
	inline String_t** get_address_of__appId_2() { return &____appId_2; }
	inline void set__appId_2(String_t* value)
	{
		____appId_2 = value;
		Il2CppCodeGenWriteBarrier(&____appId_2, value);
	}

	inline static int32_t get_offset_of__restApiKey_3() { return static_cast<int32_t>(offsetof(Appegg_t1967766116, ____restApiKey_3)); }
	inline String_t* get__restApiKey_3() const { return ____restApiKey_3; }
	inline String_t** get_address_of__restApiKey_3() { return &____restApiKey_3; }
	inline void set__restApiKey_3(String_t* value)
	{
		____restApiKey_3 = value;
		Il2CppCodeGenWriteBarrier(&____restApiKey_3, value);
	}

	inline static int32_t get_offset_of__configUrl_4() { return static_cast<int32_t>(offsetof(Appegg_t1967766116, ____configUrl_4)); }
	inline StringU5BU5D_t4054002952* get__configUrl_4() const { return ____configUrl_4; }
	inline StringU5BU5D_t4054002952** get_address_of__configUrl_4() { return &____configUrl_4; }
	inline void set__configUrl_4(StringU5BU5D_t4054002952* value)
	{
		____configUrl_4 = value;
		Il2CppCodeGenWriteBarrier(&____configUrl_4, value);
	}

	inline static int32_t get_offset_of__view_5() { return static_cast<int32_t>(offsetof(Appegg_t1967766116, ____view_5)); }
	inline ViewConfig_t3906840306 * get__view_5() const { return ____view_5; }
	inline ViewConfig_t3906840306 ** get_address_of__view_5() { return &____view_5; }
	inline void set__view_5(ViewConfig_t3906840306 * value)
	{
		____view_5 = value;
		Il2CppCodeGenWriteBarrier(&____view_5, value);
	}

	inline static int32_t get_offset_of__scene_6() { return static_cast<int32_t>(offsetof(Appegg_t1967766116, ____scene_6)); }
	inline SceneConfig_t3474339747 * get__scene_6() const { return ____scene_6; }
	inline SceneConfig_t3474339747 ** get_address_of__scene_6() { return &____scene_6; }
	inline void set__scene_6(SceneConfig_t3474339747 * value)
	{
		____scene_6 = value;
		Il2CppCodeGenWriteBarrier(&____scene_6, value);
	}

	inline static int32_t get_offset_of__general_7() { return static_cast<int32_t>(offsetof(Appegg_t1967766116, ____general_7)); }
	inline GeneralConfig_t3043367359 * get__general_7() const { return ____general_7; }
	inline GeneralConfig_t3043367359 ** get_address_of__general_7() { return &____general_7; }
	inline void set__general_7(GeneralConfig_t3043367359 * value)
	{
		____general_7 = value;
		Il2CppCodeGenWriteBarrier(&____general_7, value);
	}

	inline static int32_t get_offset_of__test_8() { return static_cast<int32_t>(offsetof(Appegg_t1967766116, ____test_8)); }
	inline TestConfig_t3201747743 * get__test_8() const { return ____test_8; }
	inline TestConfig_t3201747743 ** get_address_of__test_8() { return &____test_8; }
	inline void set__test_8(TestConfig_t3201747743 * value)
	{
		____test_8 = value;
		Il2CppCodeGenWriteBarrier(&____test_8, value);
	}

	inline static int32_t get_offset_of__serverConfig_9() { return static_cast<int32_t>(offsetof(Appegg_t1967766116, ____serverConfig_9)); }
	inline ServerConfig_t2996416080 * get__serverConfig_9() const { return ____serverConfig_9; }
	inline ServerConfig_t2996416080 ** get_address_of__serverConfig_9() { return &____serverConfig_9; }
	inline void set__serverConfig_9(ServerConfig_t2996416080 * value)
	{
		____serverConfig_9 = value;
		Il2CppCodeGenWriteBarrier(&____serverConfig_9, value);
	}

	inline static int32_t get_offset_of__idfa_10() { return static_cast<int32_t>(offsetof(Appegg_t1967766116, ____idfa_10)); }
	inline String_t* get__idfa_10() const { return ____idfa_10; }
	inline String_t** get_address_of__idfa_10() { return &____idfa_10; }
	inline void set__idfa_10(String_t* value)
	{
		____idfa_10 = value;
		Il2CppCodeGenWriteBarrier(&____idfa_10, value);
	}

	inline static int32_t get_offset_of__idfv_11() { return static_cast<int32_t>(offsetof(Appegg_t1967766116, ____idfv_11)); }
	inline String_t* get__idfv_11() const { return ____idfv_11; }
	inline String_t** get_address_of__idfv_11() { return &____idfv_11; }
	inline void set__idfv_11(String_t* value)
	{
		____idfv_11 = value;
		Il2CppCodeGenWriteBarrier(&____idfv_11, value);
	}

	inline static int32_t get_offset_of__currentVersion_12() { return static_cast<int32_t>(offsetof(Appegg_t1967766116, ____currentVersion_12)); }
	inline int32_t get__currentVersion_12() const { return ____currentVersion_12; }
	inline int32_t* get_address_of__currentVersion_12() { return &____currentVersion_12; }
	inline void set__currentVersion_12(int32_t value)
	{
		____currentVersion_12 = value;
	}

	inline static int32_t get_offset_of__currentHybridIndex_13() { return static_cast<int32_t>(offsetof(Appegg_t1967766116, ____currentHybridIndex_13)); }
	inline int32_t get__currentHybridIndex_13() const { return ____currentHybridIndex_13; }
	inline int32_t* get_address_of__currentHybridIndex_13() { return &____currentHybridIndex_13; }
	inline void set__currentHybridIndex_13(int32_t value)
	{
		____currentHybridIndex_13 = value;
	}

	inline static int32_t get_offset_of__isDataCollected_14() { return static_cast<int32_t>(offsetof(Appegg_t1967766116, ____isDataCollected_14)); }
	inline bool get__isDataCollected_14() const { return ____isDataCollected_14; }
	inline bool* get_address_of__isDataCollected_14() { return &____isDataCollected_14; }
	inline void set__isDataCollected_14(bool value)
	{
		____isDataCollected_14 = value;
	}

	inline static int32_t get_offset_of__isDownloaded_15() { return static_cast<int32_t>(offsetof(Appegg_t1967766116, ____isDownloaded_15)); }
	inline bool get__isDownloaded_15() const { return ____isDownloaded_15; }
	inline bool* get_address_of__isDownloaded_15() { return &____isDownloaded_15; }
	inline void set__isDownloaded_15(bool value)
	{
		____isDownloaded_15 = value;
	}
};

struct Appegg_t1967766116_StaticFields
{
public:
	// System.String Appegg::<SavePath>k__BackingField
	String_t* ___U3CSavePathU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_U3CSavePathU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Appegg_t1967766116_StaticFields, ___U3CSavePathU3Ek__BackingField_16)); }
	inline String_t* get_U3CSavePathU3Ek__BackingField_16() const { return ___U3CSavePathU3Ek__BackingField_16; }
	inline String_t** get_address_of_U3CSavePathU3Ek__BackingField_16() { return &___U3CSavePathU3Ek__BackingField_16; }
	inline void set_U3CSavePathU3Ek__BackingField_16(String_t* value)
	{
		___U3CSavePathU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSavePathU3Ek__BackingField_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
