﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.PointNode
struct PointNode_t2761813780;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.GraphNodeDelegate
struct GraphNodeDelegate_t1466738551;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.PathNode
struct PathNode_t417131581;
// Pathfinding.PathHandler
struct PathHandler_t918952263;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// Pathfinding.Serialization.GraphSerializationContext
struct GraphSerializationContext_t3256954663;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNodeDelegate1466738551.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"
#include "AssemblyU2DCSharp_Pathfinding_PathHandler918952263.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_Serialization_GraphS3256954663.h"
#include "AssemblyU2DCSharp_Pathfinding_PointNode2761813780.h"

// System.Void Pathfinding.PointNode::.ctor(AstarPath)
extern "C"  void PointNode__ctor_m3988895511 (PointNode_t2761813780 * __this, AstarPath_t4090270936 * ___astar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointNode::SetPosition(Pathfinding.Int3)
extern "C"  void PointNode_SetPosition_m2612694046 (PointNode_t2761813780 * __this, Int3_t1974045594  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointNode::GetConnections(Pathfinding.GraphNodeDelegate)
extern "C"  void PointNode_GetConnections_m1636972103 (PointNode_t2761813780 * __this, GraphNodeDelegate_t1466738551 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointNode::ClearConnections(System.Boolean)
extern "C"  void PointNode_ClearConnections_m1325785280 (PointNode_t2761813780 * __this, bool ___alsoReverse0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointNode::UpdateRecursiveG(Pathfinding.Path,Pathfinding.PathNode,Pathfinding.PathHandler)
extern "C"  void PointNode_UpdateRecursiveG_m336789110 (PointNode_t2761813780 * __this, Path_t1974241691 * ___path0, PathNode_t417131581 * ___pathNode1, PathHandler_t918952263 * ___handler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.PointNode::ContainsConnection(Pathfinding.GraphNode)
extern "C"  bool PointNode_ContainsConnection_m4283501826 (PointNode_t2761813780 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointNode::AddConnection(Pathfinding.GraphNode,System.UInt32)
extern "C"  void PointNode_AddConnection_m3418021824 (PointNode_t2761813780 * __this, GraphNode_t23612370 * ___node0, uint32_t ___cost1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointNode::RemoveConnection(Pathfinding.GraphNode)
extern "C"  void PointNode_RemoveConnection_m3331656841 (PointNode_t2761813780 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointNode::Open(Pathfinding.Path,Pathfinding.PathNode,Pathfinding.PathHandler)
extern "C"  void PointNode_Open_m739045034 (PointNode_t2761813780 * __this, Path_t1974241691 * ___path0, PathNode_t417131581 * ___pathNode1, PathHandler_t918952263 * ___handler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointNode::SerializeNode(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void PointNode_SerializeNode_m833291114 (PointNode_t2761813780 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointNode::DeserializeNode(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void PointNode_DeserializeNode_m1928895403 (PointNode_t2761813780 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointNode::SerializeReferences(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void PointNode_SerializeReferences_m4191133348 (PointNode_t2761813780 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointNode::DeserializeReferences(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void PointNode_DeserializeReferences_m1862287909 (PointNode_t2761813780 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointNode::ilo_Invoke1(Pathfinding.GraphNodeDelegate,Pathfinding.GraphNode)
extern "C"  void PointNode_ilo_Invoke1_m2875894022 (Il2CppObject * __this /* static, unused */, GraphNodeDelegate_t1466738551 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointNode::ilo_PushNode2(Pathfinding.PathHandler,Pathfinding.PathNode)
extern "C"  void PointNode_ilo_PushNode2_m2388296546 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, PathNode_t417131581 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointNode::ilo_UpdateRecursiveG3(Pathfinding.GraphNode,Pathfinding.Path,Pathfinding.PathNode,Pathfinding.PathHandler)
extern "C"  void PointNode_ilo_UpdateRecursiveG3_m2336689592 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, Path_t1974241691 * ___path1, PathNode_t417131581 * ___pathNode2, PathHandler_t918952263 * ___handler3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 Pathfinding.PointNode::ilo_get_PathID4(Pathfinding.PathHandler)
extern "C"  uint16_t PointNode_ilo_get_PathID4_m1478507017 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.PointNode::ilo_get_G5(Pathfinding.PathNode)
extern "C"  uint32_t PointNode_ilo_get_G5_m1189954463 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointNode::ilo_set_cost6(Pathfinding.PathNode,System.UInt32)
extern "C"  void PointNode_ilo_set_cost6_m1059701213 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointNode::ilo_UpdateRecursiveG7(Pathfinding.PointNode,Pathfinding.Path,Pathfinding.PathNode,Pathfinding.PathHandler)
extern "C"  void PointNode_ilo_UpdateRecursiveG7_m3329299634 (Il2CppObject * __this /* static, unused */, PointNode_t2761813780 * ____this0, Path_t1974241691 * ___path1, PathNode_t417131581 * ___pathNode2, PathHandler_t918952263 * ___handler3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PointNode::ilo_SerializeNode8(Pathfinding.GraphNode,Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void PointNode_ilo_SerializeNode8_m2946920969 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, GraphSerializationContext_t3256954663 * ___ctx1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
