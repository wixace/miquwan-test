﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.CommandLineParser.CommandSubCharsSet
struct CommandSubCharsSet_t4216237968;

#include "codegen/il2cpp-codegen.h"

// System.Void SevenZip.CommandLineParser.CommandSubCharsSet::.ctor()
extern "C"  void CommandSubCharsSet__ctor_m1256650119 (CommandSubCharsSet_t4216237968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
