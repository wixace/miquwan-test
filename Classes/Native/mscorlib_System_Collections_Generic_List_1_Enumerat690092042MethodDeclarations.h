﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>
struct List_1_t670419272;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat690092042.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_VoxelContour3597201016.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.VoxelContour>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2835219186_gshared (Enumerator_t690092042 * __this, List_1_t670419272 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m2835219186(__this, ___l0, method) ((  void (*) (Enumerator_t690092042 *, List_1_t670419272 *, const MethodInfo*))Enumerator__ctor_m2835219186_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.VoxelContour>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2469626400_gshared (Enumerator_t690092042 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2469626400(__this, method) ((  void (*) (Enumerator_t690092042 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2469626400_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.VoxelContour>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4123448972_gshared (Enumerator_t690092042 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m4123448972(__this, method) ((  Il2CppObject * (*) (Enumerator_t690092042 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4123448972_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.VoxelContour>::Dispose()
extern "C"  void Enumerator_Dispose_m2584538455_gshared (Enumerator_t690092042 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2584538455(__this, method) ((  void (*) (Enumerator_t690092042 *, const MethodInfo*))Enumerator_Dispose_m2584538455_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.VoxelContour>::VerifyState()
extern "C"  void Enumerator_VerifyState_m768354576_gshared (Enumerator_t690092042 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m768354576(__this, method) ((  void (*) (Enumerator_t690092042 *, const MethodInfo*))Enumerator_VerifyState_m768354576_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.VoxelContour>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3624013452_gshared (Enumerator_t690092042 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3624013452(__this, method) ((  bool (*) (Enumerator_t690092042 *, const MethodInfo*))Enumerator_MoveNext_m3624013452_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.Voxels.VoxelContour>::get_Current()
extern "C"  VoxelContour_t3597201016  Enumerator_get_Current_m1550212935_gshared (Enumerator_t690092042 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1550212935(__this, method) ((  VoxelContour_t3597201016  (*) (Enumerator_t690092042 *, const MethodInfo*))Enumerator_get_Current_m1550212935_gshared)(__this, method)
