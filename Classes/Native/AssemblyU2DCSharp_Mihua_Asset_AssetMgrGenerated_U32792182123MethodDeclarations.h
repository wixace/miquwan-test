﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua_Asset_AssetMgrGenerated/<AssetMgr_LoadAssetAsync_GetDelegate_member10_arg1>c__AnonStorey70
struct U3CAssetMgr_LoadAssetAsync_GetDelegate_member10_arg1U3Ec__AnonStorey70_t2792182123;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"

// System.Void Mihua_Asset_AssetMgrGenerated/<AssetMgr_LoadAssetAsync_GetDelegate_member10_arg1>c__AnonStorey70::.ctor()
extern "C"  void U3CAssetMgr_LoadAssetAsync_GetDelegate_member10_arg1U3Ec__AnonStorey70__ctor_m242830736 (U3CAssetMgr_LoadAssetAsync_GetDelegate_member10_arg1U3Ec__AnonStorey70_t2792182123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_AssetMgrGenerated/<AssetMgr_LoadAssetAsync_GetDelegate_member10_arg1>c__AnonStorey70::<>m__7A(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  void U3CAssetMgr_LoadAssetAsync_GetDelegate_member10_arg1U3Ec__AnonStorey70_U3CU3Em__7A_m3941079658 (U3CAssetMgr_LoadAssetAsync_GetDelegate_member10_arg1U3Ec__AnonStorey70_t2792182123 * __this, AssetOperation_t778728221 * ___assetOperation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
