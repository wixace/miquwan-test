﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t3683564144;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSDataExchangeMgr
struct  JSDataExchangeMgr_t3744712290  : public Il2CppObject
{
public:
	// System.Object JSDataExchangeMgr::mTempObj
	Il2CppObject * ___mTempObj_1;

public:
	inline static int32_t get_offset_of_mTempObj_1() { return static_cast<int32_t>(offsetof(JSDataExchangeMgr_t3744712290, ___mTempObj_1)); }
	inline Il2CppObject * get_mTempObj_1() const { return ___mTempObj_1; }
	inline Il2CppObject ** get_address_of_mTempObj_1() { return &___mTempObj_1; }
	inline void set_mTempObj_1(Il2CppObject * value)
	{
		___mTempObj_1 = value;
		Il2CppCodeGenWriteBarrier(&___mTempObj_1, value);
	}
};

struct JSDataExchangeMgr_t3744712290_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Type> JSDataExchangeMgr::typeCache
	Dictionary_2_t3683564144 * ___typeCache_2;

public:
	inline static int32_t get_offset_of_typeCache_2() { return static_cast<int32_t>(offsetof(JSDataExchangeMgr_t3744712290_StaticFields, ___typeCache_2)); }
	inline Dictionary_2_t3683564144 * get_typeCache_2() const { return ___typeCache_2; }
	inline Dictionary_2_t3683564144 ** get_address_of_typeCache_2() { return &___typeCache_2; }
	inline void set_typeCache_2(Dictionary_2_t3683564144 * value)
	{
		___typeCache_2 = value;
		Il2CppCodeGenWriteBarrier(&___typeCache_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
