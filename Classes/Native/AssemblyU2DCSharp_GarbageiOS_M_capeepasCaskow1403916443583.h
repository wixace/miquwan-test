﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_capeepasCaskow140
struct  M_capeepasCaskow140_t3916443583  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_capeepasCaskow140::_lawzoo
	float ____lawzoo_0;
	// System.Int32 GarbageiOS.M_capeepasCaskow140::_fourecear
	int32_t ____fourecear_1;
	// System.String GarbageiOS.M_capeepasCaskow140::_tageediWegee
	String_t* ____tageediWegee_2;
	// System.String GarbageiOS.M_capeepasCaskow140::_tertinu
	String_t* ____tertinu_3;
	// System.Int32 GarbageiOS.M_capeepasCaskow140::_vuselGusaimea
	int32_t ____vuselGusaimea_4;
	// System.Int32 GarbageiOS.M_capeepasCaskow140::_bejurreeChilawxea
	int32_t ____bejurreeChilawxea_5;
	// System.String GarbageiOS.M_capeepasCaskow140::_dewa
	String_t* ____dewa_6;
	// System.String GarbageiOS.M_capeepasCaskow140::_parall
	String_t* ____parall_7;
	// System.String GarbageiOS.M_capeepasCaskow140::_poosayqouPadismere
	String_t* ____poosayqouPadismere_8;
	// System.UInt32 GarbageiOS.M_capeepasCaskow140::_qaircar
	uint32_t ____qaircar_9;
	// System.UInt32 GarbageiOS.M_capeepasCaskow140::_ripearwhur
	uint32_t ____ripearwhur_10;
	// System.Int32 GarbageiOS.M_capeepasCaskow140::_ripemSutre
	int32_t ____ripemSutre_11;
	// System.Single GarbageiOS.M_capeepasCaskow140::_cedrudel
	float ____cedrudel_12;
	// System.UInt32 GarbageiOS.M_capeepasCaskow140::_cowwuGejutu
	uint32_t ____cowwuGejutu_13;
	// System.Boolean GarbageiOS.M_capeepasCaskow140::_nesutroTeaceye
	bool ____nesutroTeaceye_14;
	// System.UInt32 GarbageiOS.M_capeepasCaskow140::_hawcurba
	uint32_t ____hawcurba_15;
	// System.String GarbageiOS.M_capeepasCaskow140::_qasairsawKallnecair
	String_t* ____qasairsawKallnecair_16;
	// System.UInt32 GarbageiOS.M_capeepasCaskow140::_druwallzearNupaw
	uint32_t ____druwallzearNupaw_17;
	// System.UInt32 GarbageiOS.M_capeepasCaskow140::_qose
	uint32_t ____qose_18;
	// System.Single GarbageiOS.M_capeepasCaskow140::_rirdruGearmanel
	float ____rirdruGearmanel_19;
	// System.String GarbageiOS.M_capeepasCaskow140::_derawrorDurti
	String_t* ____derawrorDurti_20;
	// System.UInt32 GarbageiOS.M_capeepasCaskow140::_kuselairCusu
	uint32_t ____kuselairCusu_21;
	// System.UInt32 GarbageiOS.M_capeepasCaskow140::_forallNoujer
	uint32_t ____forallNoujer_22;
	// System.Boolean GarbageiOS.M_capeepasCaskow140::_kedestuRerejem
	bool ____kedestuRerejem_23;
	// System.String GarbageiOS.M_capeepasCaskow140::_ralcohow
	String_t* ____ralcohow_24;
	// System.Int32 GarbageiOS.M_capeepasCaskow140::_cehucerePerepor
	int32_t ____cehucerePerepor_25;
	// System.Boolean GarbageiOS.M_capeepasCaskow140::_dimetallPermou
	bool ____dimetallPermou_26;
	// System.Int32 GarbageiOS.M_capeepasCaskow140::_joumeesis
	int32_t ____joumeesis_27;
	// System.UInt32 GarbageiOS.M_capeepasCaskow140::_gawsoRoomeecow
	uint32_t ____gawsoRoomeecow_28;
	// System.String GarbageiOS.M_capeepasCaskow140::_reanar
	String_t* ____reanar_29;
	// System.Boolean GarbageiOS.M_capeepasCaskow140::_qeajeajis
	bool ____qeajeajis_30;
	// System.UInt32 GarbageiOS.M_capeepasCaskow140::_boti
	uint32_t ____boti_31;
	// System.String GarbageiOS.M_capeepasCaskow140::_jufowZasall
	String_t* ____jufowZasall_32;
	// System.Single GarbageiOS.M_capeepasCaskow140::_hawsehal
	float ____hawsehal_33;
	// System.UInt32 GarbageiOS.M_capeepasCaskow140::_peyergiSichurmir
	uint32_t ____peyergiSichurmir_34;
	// System.UInt32 GarbageiOS.M_capeepasCaskow140::_daijairBearweredre
	uint32_t ____daijairBearweredre_35;
	// System.Int32 GarbageiOS.M_capeepasCaskow140::_neregekaReemerchel
	int32_t ____neregekaReemerchel_36;
	// System.UInt32 GarbageiOS.M_capeepasCaskow140::_kallpuray
	uint32_t ____kallpuray_37;
	// System.Boolean GarbageiOS.M_capeepasCaskow140::_jemlal
	bool ____jemlal_38;
	// System.Int32 GarbageiOS.M_capeepasCaskow140::_karjiKootrow
	int32_t ____karjiKootrow_39;
	// System.Single GarbageiOS.M_capeepasCaskow140::_keraibowHaibe
	float ____keraibowHaibe_40;
	// System.Single GarbageiOS.M_capeepasCaskow140::_jumuCistrigea
	float ____jumuCistrigea_41;
	// System.Int32 GarbageiOS.M_capeepasCaskow140::_weata
	int32_t ____weata_42;
	// System.Int32 GarbageiOS.M_capeepasCaskow140::_kujay
	int32_t ____kujay_43;
	// System.String GarbageiOS.M_capeepasCaskow140::_dastaNirsouchis
	String_t* ____dastaNirsouchis_44;
	// System.UInt32 GarbageiOS.M_capeepasCaskow140::_menikayYerema
	uint32_t ____menikayYerema_45;
	// System.Int32 GarbageiOS.M_capeepasCaskow140::_fasvirnor
	int32_t ____fasvirnor_46;
	// System.Int32 GarbageiOS.M_capeepasCaskow140::_pelsersowSursawfu
	int32_t ____pelsersowSursawfu_47;
	// System.UInt32 GarbageiOS.M_capeepasCaskow140::_mirallJuceltu
	uint32_t ____mirallJuceltu_48;
	// System.UInt32 GarbageiOS.M_capeepasCaskow140::_qallwaChallvayrai
	uint32_t ____qallwaChallvayrai_49;
	// System.UInt32 GarbageiOS.M_capeepasCaskow140::_lerqilereHasawlar
	uint32_t ____lerqilereHasawlar_50;

public:
	inline static int32_t get_offset_of__lawzoo_0() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____lawzoo_0)); }
	inline float get__lawzoo_0() const { return ____lawzoo_0; }
	inline float* get_address_of__lawzoo_0() { return &____lawzoo_0; }
	inline void set__lawzoo_0(float value)
	{
		____lawzoo_0 = value;
	}

	inline static int32_t get_offset_of__fourecear_1() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____fourecear_1)); }
	inline int32_t get__fourecear_1() const { return ____fourecear_1; }
	inline int32_t* get_address_of__fourecear_1() { return &____fourecear_1; }
	inline void set__fourecear_1(int32_t value)
	{
		____fourecear_1 = value;
	}

	inline static int32_t get_offset_of__tageediWegee_2() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____tageediWegee_2)); }
	inline String_t* get__tageediWegee_2() const { return ____tageediWegee_2; }
	inline String_t** get_address_of__tageediWegee_2() { return &____tageediWegee_2; }
	inline void set__tageediWegee_2(String_t* value)
	{
		____tageediWegee_2 = value;
		Il2CppCodeGenWriteBarrier(&____tageediWegee_2, value);
	}

	inline static int32_t get_offset_of__tertinu_3() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____tertinu_3)); }
	inline String_t* get__tertinu_3() const { return ____tertinu_3; }
	inline String_t** get_address_of__tertinu_3() { return &____tertinu_3; }
	inline void set__tertinu_3(String_t* value)
	{
		____tertinu_3 = value;
		Il2CppCodeGenWriteBarrier(&____tertinu_3, value);
	}

	inline static int32_t get_offset_of__vuselGusaimea_4() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____vuselGusaimea_4)); }
	inline int32_t get__vuselGusaimea_4() const { return ____vuselGusaimea_4; }
	inline int32_t* get_address_of__vuselGusaimea_4() { return &____vuselGusaimea_4; }
	inline void set__vuselGusaimea_4(int32_t value)
	{
		____vuselGusaimea_4 = value;
	}

	inline static int32_t get_offset_of__bejurreeChilawxea_5() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____bejurreeChilawxea_5)); }
	inline int32_t get__bejurreeChilawxea_5() const { return ____bejurreeChilawxea_5; }
	inline int32_t* get_address_of__bejurreeChilawxea_5() { return &____bejurreeChilawxea_5; }
	inline void set__bejurreeChilawxea_5(int32_t value)
	{
		____bejurreeChilawxea_5 = value;
	}

	inline static int32_t get_offset_of__dewa_6() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____dewa_6)); }
	inline String_t* get__dewa_6() const { return ____dewa_6; }
	inline String_t** get_address_of__dewa_6() { return &____dewa_6; }
	inline void set__dewa_6(String_t* value)
	{
		____dewa_6 = value;
		Il2CppCodeGenWriteBarrier(&____dewa_6, value);
	}

	inline static int32_t get_offset_of__parall_7() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____parall_7)); }
	inline String_t* get__parall_7() const { return ____parall_7; }
	inline String_t** get_address_of__parall_7() { return &____parall_7; }
	inline void set__parall_7(String_t* value)
	{
		____parall_7 = value;
		Il2CppCodeGenWriteBarrier(&____parall_7, value);
	}

	inline static int32_t get_offset_of__poosayqouPadismere_8() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____poosayqouPadismere_8)); }
	inline String_t* get__poosayqouPadismere_8() const { return ____poosayqouPadismere_8; }
	inline String_t** get_address_of__poosayqouPadismere_8() { return &____poosayqouPadismere_8; }
	inline void set__poosayqouPadismere_8(String_t* value)
	{
		____poosayqouPadismere_8 = value;
		Il2CppCodeGenWriteBarrier(&____poosayqouPadismere_8, value);
	}

	inline static int32_t get_offset_of__qaircar_9() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____qaircar_9)); }
	inline uint32_t get__qaircar_9() const { return ____qaircar_9; }
	inline uint32_t* get_address_of__qaircar_9() { return &____qaircar_9; }
	inline void set__qaircar_9(uint32_t value)
	{
		____qaircar_9 = value;
	}

	inline static int32_t get_offset_of__ripearwhur_10() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____ripearwhur_10)); }
	inline uint32_t get__ripearwhur_10() const { return ____ripearwhur_10; }
	inline uint32_t* get_address_of__ripearwhur_10() { return &____ripearwhur_10; }
	inline void set__ripearwhur_10(uint32_t value)
	{
		____ripearwhur_10 = value;
	}

	inline static int32_t get_offset_of__ripemSutre_11() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____ripemSutre_11)); }
	inline int32_t get__ripemSutre_11() const { return ____ripemSutre_11; }
	inline int32_t* get_address_of__ripemSutre_11() { return &____ripemSutre_11; }
	inline void set__ripemSutre_11(int32_t value)
	{
		____ripemSutre_11 = value;
	}

	inline static int32_t get_offset_of__cedrudel_12() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____cedrudel_12)); }
	inline float get__cedrudel_12() const { return ____cedrudel_12; }
	inline float* get_address_of__cedrudel_12() { return &____cedrudel_12; }
	inline void set__cedrudel_12(float value)
	{
		____cedrudel_12 = value;
	}

	inline static int32_t get_offset_of__cowwuGejutu_13() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____cowwuGejutu_13)); }
	inline uint32_t get__cowwuGejutu_13() const { return ____cowwuGejutu_13; }
	inline uint32_t* get_address_of__cowwuGejutu_13() { return &____cowwuGejutu_13; }
	inline void set__cowwuGejutu_13(uint32_t value)
	{
		____cowwuGejutu_13 = value;
	}

	inline static int32_t get_offset_of__nesutroTeaceye_14() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____nesutroTeaceye_14)); }
	inline bool get__nesutroTeaceye_14() const { return ____nesutroTeaceye_14; }
	inline bool* get_address_of__nesutroTeaceye_14() { return &____nesutroTeaceye_14; }
	inline void set__nesutroTeaceye_14(bool value)
	{
		____nesutroTeaceye_14 = value;
	}

	inline static int32_t get_offset_of__hawcurba_15() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____hawcurba_15)); }
	inline uint32_t get__hawcurba_15() const { return ____hawcurba_15; }
	inline uint32_t* get_address_of__hawcurba_15() { return &____hawcurba_15; }
	inline void set__hawcurba_15(uint32_t value)
	{
		____hawcurba_15 = value;
	}

	inline static int32_t get_offset_of__qasairsawKallnecair_16() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____qasairsawKallnecair_16)); }
	inline String_t* get__qasairsawKallnecair_16() const { return ____qasairsawKallnecair_16; }
	inline String_t** get_address_of__qasairsawKallnecair_16() { return &____qasairsawKallnecair_16; }
	inline void set__qasairsawKallnecair_16(String_t* value)
	{
		____qasairsawKallnecair_16 = value;
		Il2CppCodeGenWriteBarrier(&____qasairsawKallnecair_16, value);
	}

	inline static int32_t get_offset_of__druwallzearNupaw_17() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____druwallzearNupaw_17)); }
	inline uint32_t get__druwallzearNupaw_17() const { return ____druwallzearNupaw_17; }
	inline uint32_t* get_address_of__druwallzearNupaw_17() { return &____druwallzearNupaw_17; }
	inline void set__druwallzearNupaw_17(uint32_t value)
	{
		____druwallzearNupaw_17 = value;
	}

	inline static int32_t get_offset_of__qose_18() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____qose_18)); }
	inline uint32_t get__qose_18() const { return ____qose_18; }
	inline uint32_t* get_address_of__qose_18() { return &____qose_18; }
	inline void set__qose_18(uint32_t value)
	{
		____qose_18 = value;
	}

	inline static int32_t get_offset_of__rirdruGearmanel_19() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____rirdruGearmanel_19)); }
	inline float get__rirdruGearmanel_19() const { return ____rirdruGearmanel_19; }
	inline float* get_address_of__rirdruGearmanel_19() { return &____rirdruGearmanel_19; }
	inline void set__rirdruGearmanel_19(float value)
	{
		____rirdruGearmanel_19 = value;
	}

	inline static int32_t get_offset_of__derawrorDurti_20() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____derawrorDurti_20)); }
	inline String_t* get__derawrorDurti_20() const { return ____derawrorDurti_20; }
	inline String_t** get_address_of__derawrorDurti_20() { return &____derawrorDurti_20; }
	inline void set__derawrorDurti_20(String_t* value)
	{
		____derawrorDurti_20 = value;
		Il2CppCodeGenWriteBarrier(&____derawrorDurti_20, value);
	}

	inline static int32_t get_offset_of__kuselairCusu_21() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____kuselairCusu_21)); }
	inline uint32_t get__kuselairCusu_21() const { return ____kuselairCusu_21; }
	inline uint32_t* get_address_of__kuselairCusu_21() { return &____kuselairCusu_21; }
	inline void set__kuselairCusu_21(uint32_t value)
	{
		____kuselairCusu_21 = value;
	}

	inline static int32_t get_offset_of__forallNoujer_22() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____forallNoujer_22)); }
	inline uint32_t get__forallNoujer_22() const { return ____forallNoujer_22; }
	inline uint32_t* get_address_of__forallNoujer_22() { return &____forallNoujer_22; }
	inline void set__forallNoujer_22(uint32_t value)
	{
		____forallNoujer_22 = value;
	}

	inline static int32_t get_offset_of__kedestuRerejem_23() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____kedestuRerejem_23)); }
	inline bool get__kedestuRerejem_23() const { return ____kedestuRerejem_23; }
	inline bool* get_address_of__kedestuRerejem_23() { return &____kedestuRerejem_23; }
	inline void set__kedestuRerejem_23(bool value)
	{
		____kedestuRerejem_23 = value;
	}

	inline static int32_t get_offset_of__ralcohow_24() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____ralcohow_24)); }
	inline String_t* get__ralcohow_24() const { return ____ralcohow_24; }
	inline String_t** get_address_of__ralcohow_24() { return &____ralcohow_24; }
	inline void set__ralcohow_24(String_t* value)
	{
		____ralcohow_24 = value;
		Il2CppCodeGenWriteBarrier(&____ralcohow_24, value);
	}

	inline static int32_t get_offset_of__cehucerePerepor_25() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____cehucerePerepor_25)); }
	inline int32_t get__cehucerePerepor_25() const { return ____cehucerePerepor_25; }
	inline int32_t* get_address_of__cehucerePerepor_25() { return &____cehucerePerepor_25; }
	inline void set__cehucerePerepor_25(int32_t value)
	{
		____cehucerePerepor_25 = value;
	}

	inline static int32_t get_offset_of__dimetallPermou_26() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____dimetallPermou_26)); }
	inline bool get__dimetallPermou_26() const { return ____dimetallPermou_26; }
	inline bool* get_address_of__dimetallPermou_26() { return &____dimetallPermou_26; }
	inline void set__dimetallPermou_26(bool value)
	{
		____dimetallPermou_26 = value;
	}

	inline static int32_t get_offset_of__joumeesis_27() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____joumeesis_27)); }
	inline int32_t get__joumeesis_27() const { return ____joumeesis_27; }
	inline int32_t* get_address_of__joumeesis_27() { return &____joumeesis_27; }
	inline void set__joumeesis_27(int32_t value)
	{
		____joumeesis_27 = value;
	}

	inline static int32_t get_offset_of__gawsoRoomeecow_28() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____gawsoRoomeecow_28)); }
	inline uint32_t get__gawsoRoomeecow_28() const { return ____gawsoRoomeecow_28; }
	inline uint32_t* get_address_of__gawsoRoomeecow_28() { return &____gawsoRoomeecow_28; }
	inline void set__gawsoRoomeecow_28(uint32_t value)
	{
		____gawsoRoomeecow_28 = value;
	}

	inline static int32_t get_offset_of__reanar_29() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____reanar_29)); }
	inline String_t* get__reanar_29() const { return ____reanar_29; }
	inline String_t** get_address_of__reanar_29() { return &____reanar_29; }
	inline void set__reanar_29(String_t* value)
	{
		____reanar_29 = value;
		Il2CppCodeGenWriteBarrier(&____reanar_29, value);
	}

	inline static int32_t get_offset_of__qeajeajis_30() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____qeajeajis_30)); }
	inline bool get__qeajeajis_30() const { return ____qeajeajis_30; }
	inline bool* get_address_of__qeajeajis_30() { return &____qeajeajis_30; }
	inline void set__qeajeajis_30(bool value)
	{
		____qeajeajis_30 = value;
	}

	inline static int32_t get_offset_of__boti_31() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____boti_31)); }
	inline uint32_t get__boti_31() const { return ____boti_31; }
	inline uint32_t* get_address_of__boti_31() { return &____boti_31; }
	inline void set__boti_31(uint32_t value)
	{
		____boti_31 = value;
	}

	inline static int32_t get_offset_of__jufowZasall_32() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____jufowZasall_32)); }
	inline String_t* get__jufowZasall_32() const { return ____jufowZasall_32; }
	inline String_t** get_address_of__jufowZasall_32() { return &____jufowZasall_32; }
	inline void set__jufowZasall_32(String_t* value)
	{
		____jufowZasall_32 = value;
		Il2CppCodeGenWriteBarrier(&____jufowZasall_32, value);
	}

	inline static int32_t get_offset_of__hawsehal_33() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____hawsehal_33)); }
	inline float get__hawsehal_33() const { return ____hawsehal_33; }
	inline float* get_address_of__hawsehal_33() { return &____hawsehal_33; }
	inline void set__hawsehal_33(float value)
	{
		____hawsehal_33 = value;
	}

	inline static int32_t get_offset_of__peyergiSichurmir_34() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____peyergiSichurmir_34)); }
	inline uint32_t get__peyergiSichurmir_34() const { return ____peyergiSichurmir_34; }
	inline uint32_t* get_address_of__peyergiSichurmir_34() { return &____peyergiSichurmir_34; }
	inline void set__peyergiSichurmir_34(uint32_t value)
	{
		____peyergiSichurmir_34 = value;
	}

	inline static int32_t get_offset_of__daijairBearweredre_35() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____daijairBearweredre_35)); }
	inline uint32_t get__daijairBearweredre_35() const { return ____daijairBearweredre_35; }
	inline uint32_t* get_address_of__daijairBearweredre_35() { return &____daijairBearweredre_35; }
	inline void set__daijairBearweredre_35(uint32_t value)
	{
		____daijairBearweredre_35 = value;
	}

	inline static int32_t get_offset_of__neregekaReemerchel_36() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____neregekaReemerchel_36)); }
	inline int32_t get__neregekaReemerchel_36() const { return ____neregekaReemerchel_36; }
	inline int32_t* get_address_of__neregekaReemerchel_36() { return &____neregekaReemerchel_36; }
	inline void set__neregekaReemerchel_36(int32_t value)
	{
		____neregekaReemerchel_36 = value;
	}

	inline static int32_t get_offset_of__kallpuray_37() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____kallpuray_37)); }
	inline uint32_t get__kallpuray_37() const { return ____kallpuray_37; }
	inline uint32_t* get_address_of__kallpuray_37() { return &____kallpuray_37; }
	inline void set__kallpuray_37(uint32_t value)
	{
		____kallpuray_37 = value;
	}

	inline static int32_t get_offset_of__jemlal_38() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____jemlal_38)); }
	inline bool get__jemlal_38() const { return ____jemlal_38; }
	inline bool* get_address_of__jemlal_38() { return &____jemlal_38; }
	inline void set__jemlal_38(bool value)
	{
		____jemlal_38 = value;
	}

	inline static int32_t get_offset_of__karjiKootrow_39() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____karjiKootrow_39)); }
	inline int32_t get__karjiKootrow_39() const { return ____karjiKootrow_39; }
	inline int32_t* get_address_of__karjiKootrow_39() { return &____karjiKootrow_39; }
	inline void set__karjiKootrow_39(int32_t value)
	{
		____karjiKootrow_39 = value;
	}

	inline static int32_t get_offset_of__keraibowHaibe_40() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____keraibowHaibe_40)); }
	inline float get__keraibowHaibe_40() const { return ____keraibowHaibe_40; }
	inline float* get_address_of__keraibowHaibe_40() { return &____keraibowHaibe_40; }
	inline void set__keraibowHaibe_40(float value)
	{
		____keraibowHaibe_40 = value;
	}

	inline static int32_t get_offset_of__jumuCistrigea_41() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____jumuCistrigea_41)); }
	inline float get__jumuCistrigea_41() const { return ____jumuCistrigea_41; }
	inline float* get_address_of__jumuCistrigea_41() { return &____jumuCistrigea_41; }
	inline void set__jumuCistrigea_41(float value)
	{
		____jumuCistrigea_41 = value;
	}

	inline static int32_t get_offset_of__weata_42() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____weata_42)); }
	inline int32_t get__weata_42() const { return ____weata_42; }
	inline int32_t* get_address_of__weata_42() { return &____weata_42; }
	inline void set__weata_42(int32_t value)
	{
		____weata_42 = value;
	}

	inline static int32_t get_offset_of__kujay_43() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____kujay_43)); }
	inline int32_t get__kujay_43() const { return ____kujay_43; }
	inline int32_t* get_address_of__kujay_43() { return &____kujay_43; }
	inline void set__kujay_43(int32_t value)
	{
		____kujay_43 = value;
	}

	inline static int32_t get_offset_of__dastaNirsouchis_44() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____dastaNirsouchis_44)); }
	inline String_t* get__dastaNirsouchis_44() const { return ____dastaNirsouchis_44; }
	inline String_t** get_address_of__dastaNirsouchis_44() { return &____dastaNirsouchis_44; }
	inline void set__dastaNirsouchis_44(String_t* value)
	{
		____dastaNirsouchis_44 = value;
		Il2CppCodeGenWriteBarrier(&____dastaNirsouchis_44, value);
	}

	inline static int32_t get_offset_of__menikayYerema_45() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____menikayYerema_45)); }
	inline uint32_t get__menikayYerema_45() const { return ____menikayYerema_45; }
	inline uint32_t* get_address_of__menikayYerema_45() { return &____menikayYerema_45; }
	inline void set__menikayYerema_45(uint32_t value)
	{
		____menikayYerema_45 = value;
	}

	inline static int32_t get_offset_of__fasvirnor_46() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____fasvirnor_46)); }
	inline int32_t get__fasvirnor_46() const { return ____fasvirnor_46; }
	inline int32_t* get_address_of__fasvirnor_46() { return &____fasvirnor_46; }
	inline void set__fasvirnor_46(int32_t value)
	{
		____fasvirnor_46 = value;
	}

	inline static int32_t get_offset_of__pelsersowSursawfu_47() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____pelsersowSursawfu_47)); }
	inline int32_t get__pelsersowSursawfu_47() const { return ____pelsersowSursawfu_47; }
	inline int32_t* get_address_of__pelsersowSursawfu_47() { return &____pelsersowSursawfu_47; }
	inline void set__pelsersowSursawfu_47(int32_t value)
	{
		____pelsersowSursawfu_47 = value;
	}

	inline static int32_t get_offset_of__mirallJuceltu_48() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____mirallJuceltu_48)); }
	inline uint32_t get__mirallJuceltu_48() const { return ____mirallJuceltu_48; }
	inline uint32_t* get_address_of__mirallJuceltu_48() { return &____mirallJuceltu_48; }
	inline void set__mirallJuceltu_48(uint32_t value)
	{
		____mirallJuceltu_48 = value;
	}

	inline static int32_t get_offset_of__qallwaChallvayrai_49() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____qallwaChallvayrai_49)); }
	inline uint32_t get__qallwaChallvayrai_49() const { return ____qallwaChallvayrai_49; }
	inline uint32_t* get_address_of__qallwaChallvayrai_49() { return &____qallwaChallvayrai_49; }
	inline void set__qallwaChallvayrai_49(uint32_t value)
	{
		____qallwaChallvayrai_49 = value;
	}

	inline static int32_t get_offset_of__lerqilereHasawlar_50() { return static_cast<int32_t>(offsetof(M_capeepasCaskow140_t3916443583, ____lerqilereHasawlar_50)); }
	inline uint32_t get__lerqilereHasawlar_50() const { return ____lerqilereHasawlar_50; }
	inline uint32_t* get_address_of__lerqilereHasawlar_50() { return &____lerqilereHasawlar_50; }
	inline void set__lerqilereHasawlar_50(uint32_t value)
	{
		____lerqilereHasawlar_50 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
