﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._acecfd0f3ec1ba076651d0444a69d4f0
struct _acecfd0f3ec1ba076651d0444a69d4f0_t281836037;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._acecfd0f3ec1ba076651d0444a69d4f0::.ctor()
extern "C"  void _acecfd0f3ec1ba076651d0444a69d4f0__ctor_m2513379784 (_acecfd0f3ec1ba076651d0444a69d4f0_t281836037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._acecfd0f3ec1ba076651d0444a69d4f0::_acecfd0f3ec1ba076651d0444a69d4f0m2(System.Int32)
extern "C"  int32_t _acecfd0f3ec1ba076651d0444a69d4f0__acecfd0f3ec1ba076651d0444a69d4f0m2_m988359545 (_acecfd0f3ec1ba076651d0444a69d4f0_t281836037 * __this, int32_t ____acecfd0f3ec1ba076651d0444a69d4f0a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._acecfd0f3ec1ba076651d0444a69d4f0::_acecfd0f3ec1ba076651d0444a69d4f0m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _acecfd0f3ec1ba076651d0444a69d4f0__acecfd0f3ec1ba076651d0444a69d4f0m_m1373787997 (_acecfd0f3ec1ba076651d0444a69d4f0_t281836037 * __this, int32_t ____acecfd0f3ec1ba076651d0444a69d4f0a0, int32_t ____acecfd0f3ec1ba076651d0444a69d4f0931, int32_t ____acecfd0f3ec1ba076651d0444a69d4f0c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
