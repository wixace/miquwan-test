﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<HitTarget>
struct List_1_t761115060;

#include "AssemblyU2DCSharp_ReplayBase779703160.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayUnitState
struct  ReplayUnitState_t3104690470  : public ReplayBase_t779703160
{
public:
	// System.Int32 ReplayUnitState::skillId
	int32_t ___skillId_8;
	// System.Single ReplayUnitState::rotX
	float ___rotX_9;
	// System.Single ReplayUnitState::rotY
	float ___rotY_10;
	// System.Single ReplayUnitState::rotZ
	float ___rotZ_11;
	// System.Collections.Generic.List`1<HitTarget> ReplayUnitState::hitTargets
	List_1_t761115060 * ___hitTargets_12;

public:
	inline static int32_t get_offset_of_skillId_8() { return static_cast<int32_t>(offsetof(ReplayUnitState_t3104690470, ___skillId_8)); }
	inline int32_t get_skillId_8() const { return ___skillId_8; }
	inline int32_t* get_address_of_skillId_8() { return &___skillId_8; }
	inline void set_skillId_8(int32_t value)
	{
		___skillId_8 = value;
	}

	inline static int32_t get_offset_of_rotX_9() { return static_cast<int32_t>(offsetof(ReplayUnitState_t3104690470, ___rotX_9)); }
	inline float get_rotX_9() const { return ___rotX_9; }
	inline float* get_address_of_rotX_9() { return &___rotX_9; }
	inline void set_rotX_9(float value)
	{
		___rotX_9 = value;
	}

	inline static int32_t get_offset_of_rotY_10() { return static_cast<int32_t>(offsetof(ReplayUnitState_t3104690470, ___rotY_10)); }
	inline float get_rotY_10() const { return ___rotY_10; }
	inline float* get_address_of_rotY_10() { return &___rotY_10; }
	inline void set_rotY_10(float value)
	{
		___rotY_10 = value;
	}

	inline static int32_t get_offset_of_rotZ_11() { return static_cast<int32_t>(offsetof(ReplayUnitState_t3104690470, ___rotZ_11)); }
	inline float get_rotZ_11() const { return ___rotZ_11; }
	inline float* get_address_of_rotZ_11() { return &___rotZ_11; }
	inline void set_rotZ_11(float value)
	{
		___rotZ_11 = value;
	}

	inline static int32_t get_offset_of_hitTargets_12() { return static_cast<int32_t>(offsetof(ReplayUnitState_t3104690470, ___hitTargets_12)); }
	inline List_1_t761115060 * get_hitTargets_12() const { return ___hitTargets_12; }
	inline List_1_t761115060 ** get_address_of_hitTargets_12() { return &___hitTargets_12; }
	inline void set_hitTargets_12(List_1_t761115060 * value)
	{
		___hitTargets_12 = value;
		Il2CppCodeGenWriteBarrier(&___hitTargets_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
