﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_WaitForSecondsGenerated
struct UnityEngine_WaitForSecondsGenerated_t888495772;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_WaitForSecondsGenerated::.ctor()
extern "C"  void UnityEngine_WaitForSecondsGenerated__ctor_m3937440847 (UnityEngine_WaitForSecondsGenerated_t888495772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WaitForSecondsGenerated::WaitForSeconds_WaitForSeconds1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WaitForSecondsGenerated_WaitForSeconds_WaitForSeconds1_m500311971 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WaitForSecondsGenerated::__Register()
extern "C"  void UnityEngine_WaitForSecondsGenerated___Register_m3170648536 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_WaitForSecondsGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_WaitForSecondsGenerated_ilo_getObject1_m3161487431 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
