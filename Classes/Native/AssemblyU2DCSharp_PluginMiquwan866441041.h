﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginMiquwan
struct  PluginMiquwan_t866441041  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginMiquwan::userId
	String_t* ___userId_2;
	// System.String PluginMiquwan::gid
	String_t* ___gid_3;
	// System.String PluginMiquwan::sessiond
	String_t* ___sessiond_4;
	// System.String[] PluginMiquwan::sdkInfos
	StringU5BU5D_t4054002952* ___sdkInfos_5;
	// System.String PluginMiquwan::configId
	String_t* ___configId_6;
	// System.Boolean PluginMiquwan::isOut
	bool ___isOut_7;
	// System.String PluginMiquwan::accountName
	String_t* ___accountName_8;
	// System.String PluginMiquwan::phoneNumber
	String_t* ___phoneNumber_9;

public:
	inline static int32_t get_offset_of_userId_2() { return static_cast<int32_t>(offsetof(PluginMiquwan_t866441041, ___userId_2)); }
	inline String_t* get_userId_2() const { return ___userId_2; }
	inline String_t** get_address_of_userId_2() { return &___userId_2; }
	inline void set_userId_2(String_t* value)
	{
		___userId_2 = value;
		Il2CppCodeGenWriteBarrier(&___userId_2, value);
	}

	inline static int32_t get_offset_of_gid_3() { return static_cast<int32_t>(offsetof(PluginMiquwan_t866441041, ___gid_3)); }
	inline String_t* get_gid_3() const { return ___gid_3; }
	inline String_t** get_address_of_gid_3() { return &___gid_3; }
	inline void set_gid_3(String_t* value)
	{
		___gid_3 = value;
		Il2CppCodeGenWriteBarrier(&___gid_3, value);
	}

	inline static int32_t get_offset_of_sessiond_4() { return static_cast<int32_t>(offsetof(PluginMiquwan_t866441041, ___sessiond_4)); }
	inline String_t* get_sessiond_4() const { return ___sessiond_4; }
	inline String_t** get_address_of_sessiond_4() { return &___sessiond_4; }
	inline void set_sessiond_4(String_t* value)
	{
		___sessiond_4 = value;
		Il2CppCodeGenWriteBarrier(&___sessiond_4, value);
	}

	inline static int32_t get_offset_of_sdkInfos_5() { return static_cast<int32_t>(offsetof(PluginMiquwan_t866441041, ___sdkInfos_5)); }
	inline StringU5BU5D_t4054002952* get_sdkInfos_5() const { return ___sdkInfos_5; }
	inline StringU5BU5D_t4054002952** get_address_of_sdkInfos_5() { return &___sdkInfos_5; }
	inline void set_sdkInfos_5(StringU5BU5D_t4054002952* value)
	{
		___sdkInfos_5 = value;
		Il2CppCodeGenWriteBarrier(&___sdkInfos_5, value);
	}

	inline static int32_t get_offset_of_configId_6() { return static_cast<int32_t>(offsetof(PluginMiquwan_t866441041, ___configId_6)); }
	inline String_t* get_configId_6() const { return ___configId_6; }
	inline String_t** get_address_of_configId_6() { return &___configId_6; }
	inline void set_configId_6(String_t* value)
	{
		___configId_6 = value;
		Il2CppCodeGenWriteBarrier(&___configId_6, value);
	}

	inline static int32_t get_offset_of_isOut_7() { return static_cast<int32_t>(offsetof(PluginMiquwan_t866441041, ___isOut_7)); }
	inline bool get_isOut_7() const { return ___isOut_7; }
	inline bool* get_address_of_isOut_7() { return &___isOut_7; }
	inline void set_isOut_7(bool value)
	{
		___isOut_7 = value;
	}

	inline static int32_t get_offset_of_accountName_8() { return static_cast<int32_t>(offsetof(PluginMiquwan_t866441041, ___accountName_8)); }
	inline String_t* get_accountName_8() const { return ___accountName_8; }
	inline String_t** get_address_of_accountName_8() { return &___accountName_8; }
	inline void set_accountName_8(String_t* value)
	{
		___accountName_8 = value;
		Il2CppCodeGenWriteBarrier(&___accountName_8, value);
	}

	inline static int32_t get_offset_of_phoneNumber_9() { return static_cast<int32_t>(offsetof(PluginMiquwan_t866441041, ___phoneNumber_9)); }
	inline String_t* get_phoneNumber_9() const { return ___phoneNumber_9; }
	inline String_t** get_address_of_phoneNumber_9() { return &___phoneNumber_9; }
	inline void set_phoneNumber_9(String_t* value)
	{
		___phoneNumber_9 = value;
		Il2CppCodeGenWriteBarrier(&___phoneNumber_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
