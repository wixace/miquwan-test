﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.GraphNode[]
struct GraphNodeU5BU5D_t927449255;
// System.Object
struct Il2CppObject;
// System.Random
struct Random_t4255898871;

#include "AssemblyU2DCSharp_Pathfinding_MonoModifier200043088.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.AlternativePath
struct  AlternativePath_t823703636  : public MonoModifier_t200043088
{
public:
	// System.Int32 Pathfinding.AlternativePath::penalty
	int32_t ___penalty_4;
	// System.Int32 Pathfinding.AlternativePath::randomStep
	int32_t ___randomStep_5;
	// Pathfinding.GraphNode[] Pathfinding.AlternativePath::prevNodes
	GraphNodeU5BU5D_t927449255* ___prevNodes_6;
	// System.Int32 Pathfinding.AlternativePath::prevSeed
	int32_t ___prevSeed_7;
	// System.Int32 Pathfinding.AlternativePath::prevPenalty
	int32_t ___prevPenalty_8;
	// System.Boolean Pathfinding.AlternativePath::waitingForApply
	bool ___waitingForApply_9;
	// System.Object Pathfinding.AlternativePath::lockObject
	Il2CppObject * ___lockObject_10;
	// System.Random Pathfinding.AlternativePath::rnd
	Random_t4255898871 * ___rnd_11;
	// System.Random Pathfinding.AlternativePath::seedGenerator
	Random_t4255898871 * ___seedGenerator_12;
	// System.Boolean Pathfinding.AlternativePath::destroyed
	bool ___destroyed_13;
	// Pathfinding.GraphNode[] Pathfinding.AlternativePath::toBeApplied
	GraphNodeU5BU5D_t927449255* ___toBeApplied_14;

public:
	inline static int32_t get_offset_of_penalty_4() { return static_cast<int32_t>(offsetof(AlternativePath_t823703636, ___penalty_4)); }
	inline int32_t get_penalty_4() const { return ___penalty_4; }
	inline int32_t* get_address_of_penalty_4() { return &___penalty_4; }
	inline void set_penalty_4(int32_t value)
	{
		___penalty_4 = value;
	}

	inline static int32_t get_offset_of_randomStep_5() { return static_cast<int32_t>(offsetof(AlternativePath_t823703636, ___randomStep_5)); }
	inline int32_t get_randomStep_5() const { return ___randomStep_5; }
	inline int32_t* get_address_of_randomStep_5() { return &___randomStep_5; }
	inline void set_randomStep_5(int32_t value)
	{
		___randomStep_5 = value;
	}

	inline static int32_t get_offset_of_prevNodes_6() { return static_cast<int32_t>(offsetof(AlternativePath_t823703636, ___prevNodes_6)); }
	inline GraphNodeU5BU5D_t927449255* get_prevNodes_6() const { return ___prevNodes_6; }
	inline GraphNodeU5BU5D_t927449255** get_address_of_prevNodes_6() { return &___prevNodes_6; }
	inline void set_prevNodes_6(GraphNodeU5BU5D_t927449255* value)
	{
		___prevNodes_6 = value;
		Il2CppCodeGenWriteBarrier(&___prevNodes_6, value);
	}

	inline static int32_t get_offset_of_prevSeed_7() { return static_cast<int32_t>(offsetof(AlternativePath_t823703636, ___prevSeed_7)); }
	inline int32_t get_prevSeed_7() const { return ___prevSeed_7; }
	inline int32_t* get_address_of_prevSeed_7() { return &___prevSeed_7; }
	inline void set_prevSeed_7(int32_t value)
	{
		___prevSeed_7 = value;
	}

	inline static int32_t get_offset_of_prevPenalty_8() { return static_cast<int32_t>(offsetof(AlternativePath_t823703636, ___prevPenalty_8)); }
	inline int32_t get_prevPenalty_8() const { return ___prevPenalty_8; }
	inline int32_t* get_address_of_prevPenalty_8() { return &___prevPenalty_8; }
	inline void set_prevPenalty_8(int32_t value)
	{
		___prevPenalty_8 = value;
	}

	inline static int32_t get_offset_of_waitingForApply_9() { return static_cast<int32_t>(offsetof(AlternativePath_t823703636, ___waitingForApply_9)); }
	inline bool get_waitingForApply_9() const { return ___waitingForApply_9; }
	inline bool* get_address_of_waitingForApply_9() { return &___waitingForApply_9; }
	inline void set_waitingForApply_9(bool value)
	{
		___waitingForApply_9 = value;
	}

	inline static int32_t get_offset_of_lockObject_10() { return static_cast<int32_t>(offsetof(AlternativePath_t823703636, ___lockObject_10)); }
	inline Il2CppObject * get_lockObject_10() const { return ___lockObject_10; }
	inline Il2CppObject ** get_address_of_lockObject_10() { return &___lockObject_10; }
	inline void set_lockObject_10(Il2CppObject * value)
	{
		___lockObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___lockObject_10, value);
	}

	inline static int32_t get_offset_of_rnd_11() { return static_cast<int32_t>(offsetof(AlternativePath_t823703636, ___rnd_11)); }
	inline Random_t4255898871 * get_rnd_11() const { return ___rnd_11; }
	inline Random_t4255898871 ** get_address_of_rnd_11() { return &___rnd_11; }
	inline void set_rnd_11(Random_t4255898871 * value)
	{
		___rnd_11 = value;
		Il2CppCodeGenWriteBarrier(&___rnd_11, value);
	}

	inline static int32_t get_offset_of_seedGenerator_12() { return static_cast<int32_t>(offsetof(AlternativePath_t823703636, ___seedGenerator_12)); }
	inline Random_t4255898871 * get_seedGenerator_12() const { return ___seedGenerator_12; }
	inline Random_t4255898871 ** get_address_of_seedGenerator_12() { return &___seedGenerator_12; }
	inline void set_seedGenerator_12(Random_t4255898871 * value)
	{
		___seedGenerator_12 = value;
		Il2CppCodeGenWriteBarrier(&___seedGenerator_12, value);
	}

	inline static int32_t get_offset_of_destroyed_13() { return static_cast<int32_t>(offsetof(AlternativePath_t823703636, ___destroyed_13)); }
	inline bool get_destroyed_13() const { return ___destroyed_13; }
	inline bool* get_address_of_destroyed_13() { return &___destroyed_13; }
	inline void set_destroyed_13(bool value)
	{
		___destroyed_13 = value;
	}

	inline static int32_t get_offset_of_toBeApplied_14() { return static_cast<int32_t>(offsetof(AlternativePath_t823703636, ___toBeApplied_14)); }
	inline GraphNodeU5BU5D_t927449255* get_toBeApplied_14() const { return ___toBeApplied_14; }
	inline GraphNodeU5BU5D_t927449255** get_address_of_toBeApplied_14() { return &___toBeApplied_14; }
	inline void set_toBeApplied_14(GraphNodeU5BU5D_t927449255* value)
	{
		___toBeApplied_14 = value;
		Il2CppCodeGenWriteBarrier(&___toBeApplied_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
