﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UISceneCamera
struct UISceneCamera_t2586519453;

#include "codegen/il2cpp-codegen.h"

// System.Void UISceneCamera::.ctor()
extern "C"  void UISceneCamera__ctor_m2385869422 (UISceneCamera_t2586519453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISceneCamera::Awake()
extern "C"  void UISceneCamera_Awake_m2623474641 (UISceneCamera_t2586519453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISceneCamera::Update()
extern "C"  void UISceneCamera_Update_m2674370143 (UISceneCamera_t2586519453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISceneCamera::RefreshCamera()
extern "C"  void UISceneCamera_RefreshCamera_m3350371948 (UISceneCamera_t2586519453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
