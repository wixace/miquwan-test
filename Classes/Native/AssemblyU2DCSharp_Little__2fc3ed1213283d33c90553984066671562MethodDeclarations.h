﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._2fc3ed1213283d33c9055398ba78b69a
struct _2fc3ed1213283d33c9055398ba78b69a_t4066671562;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__2fc3ed1213283d33c90553984066671562.h"

// System.Void Little._2fc3ed1213283d33c9055398ba78b69a::.ctor()
extern "C"  void _2fc3ed1213283d33c9055398ba78b69a__ctor_m1160531363 (_2fc3ed1213283d33c9055398ba78b69a_t4066671562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._2fc3ed1213283d33c9055398ba78b69a::_2fc3ed1213283d33c9055398ba78b69am2(System.Int32)
extern "C"  int32_t _2fc3ed1213283d33c9055398ba78b69a__2fc3ed1213283d33c9055398ba78b69am2_m4039650649 (_2fc3ed1213283d33c9055398ba78b69a_t4066671562 * __this, int32_t ____2fc3ed1213283d33c9055398ba78b69aa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._2fc3ed1213283d33c9055398ba78b69a::_2fc3ed1213283d33c9055398ba78b69am(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _2fc3ed1213283d33c9055398ba78b69a__2fc3ed1213283d33c9055398ba78b69am_m476000637 (_2fc3ed1213283d33c9055398ba78b69a_t4066671562 * __this, int32_t ____2fc3ed1213283d33c9055398ba78b69aa0, int32_t ____2fc3ed1213283d33c9055398ba78b69a21, int32_t ____2fc3ed1213283d33c9055398ba78b69ac2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._2fc3ed1213283d33c9055398ba78b69a::ilo__2fc3ed1213283d33c9055398ba78b69am21(Little._2fc3ed1213283d33c9055398ba78b69a,System.Int32)
extern "C"  int32_t _2fc3ed1213283d33c9055398ba78b69a_ilo__2fc3ed1213283d33c9055398ba78b69am21_m3191142609 (Il2CppObject * __this /* static, unused */, _2fc3ed1213283d33c9055398ba78b69a_t4066671562 * ____this0, int32_t ____2fc3ed1213283d33c9055398ba78b69aa1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
