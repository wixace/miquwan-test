﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ProceduralMaterialGenerated
struct UnityEngine_ProceduralMaterialGenerated_t3446159299;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_ProceduralMaterialGenerated::.ctor()
extern "C"  void UnityEngine_ProceduralMaterialGenerated__ctor_m3429524232 (UnityEngine_ProceduralMaterialGenerated_t3446159299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_cacheSize(JSVCall)
extern "C"  void UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_cacheSize_m2995071747 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_animationUpdateRate(JSVCall)
extern "C"  void UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_animationUpdateRate_m2544775417 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_isProcessing(JSVCall)
extern "C"  void UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_isProcessing_m1827547529 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_isCachedDataAvailable(JSVCall)
extern "C"  void UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_isCachedDataAvailable_m2132490483 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_isLoadTimeGenerated(JSVCall)
extern "C"  void UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_isLoadTimeGenerated_m1731476628 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_loadingBehavior(JSVCall)
extern "C"  void UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_loadingBehavior_m3897757272 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_isSupported(JSVCall)
extern "C"  void UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_isSupported_m3222358050 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_substanceProcessorUsage(JSVCall)
extern "C"  void UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_substanceProcessorUsage_m1123808103 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_preset(JSVCall)
extern "C"  void UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_preset_m2056758471 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_isReadable(JSVCall)
extern "C"  void UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_isReadable_m498942668 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_isFrozen(JSVCall)
extern "C"  void UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_isFrozen_m3827646364 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_CacheProceduralProperty__String__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_CacheProceduralProperty__String__Boolean_m2505430408 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_ClearCache(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_ClearCache_m2425248282 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_FreezeAndReleaseSourceData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_FreezeAndReleaseSourceData_m1101891473 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_GetGeneratedTexture__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_GetGeneratedTexture__String_m2995845072 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_GetGeneratedTextures(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_GetGeneratedTextures_m532795286 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_GetProceduralBoolean__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_GetProceduralBoolean__String_m2554872171 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_GetProceduralColor__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_GetProceduralColor__String_m1770303398 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_GetProceduralEnum__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_GetProceduralEnum__String_m2411369058 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_GetProceduralFloat__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_GetProceduralFloat__String_m392489183 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_GetProceduralPropertyDescriptions(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_GetProceduralPropertyDescriptions_m73524796 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_GetProceduralTexture__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_GetProceduralTexture__String_m582663966 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_GetProceduralVector__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_GetProceduralVector__String_m143051620 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_HasProceduralProperty__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_HasProceduralProperty__String_m612377594 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_IsProceduralPropertyCached__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_IsProceduralPropertyCached__String_m254349044 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_IsProceduralPropertyVisible__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_IsProceduralPropertyVisible__String_m1055879780 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_RebuildTextures(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_RebuildTextures_m3035881392 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_RebuildTexturesImmediately(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_RebuildTexturesImmediately_m2012246896 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_SetProceduralBoolean__String__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_SetProceduralBoolean__String__Boolean_m466219 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_SetProceduralColor__String__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_SetProceduralColor__String__Color_m3519447819 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_SetProceduralEnum__String__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_SetProceduralEnum__String__Int32_m1829564962 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_SetProceduralFloat__String__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_SetProceduralFloat__String__Single_m973884315 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_SetProceduralTexture__String__Texture2D(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_SetProceduralTexture__String__Texture2D_m2243469277 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_SetProceduralVector__String__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_SetProceduralVector__String__Vector4_m4176174627 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ProceduralMaterial_StopRebuilds(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ProceduralMaterial_StopRebuilds_m3216820223 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralMaterialGenerated::__Register()
extern "C"  void UnityEngine_ProceduralMaterialGenerated___Register_m3370368703 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralMaterialGenerated::ilo_setEnum1(System.Int32,System.Int32)
extern "C"  void UnityEngine_ProceduralMaterialGenerated_ilo_setEnum1_m3410120873 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ProceduralMaterialGenerated::ilo_getEnum2(System.Int32)
extern "C"  int32_t UnityEngine_ProceduralMaterialGenerated_ilo_getEnum2_m2434471149 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralMaterialGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void UnityEngine_ProceduralMaterialGenerated_ilo_setInt323_m939376780 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralMaterialGenerated::ilo_setBooleanS4(System.Int32,System.Boolean)
extern "C"  void UnityEngine_ProceduralMaterialGenerated_ilo_setBooleanS4_m652558934 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ProceduralMaterialGenerated::ilo_getBooleanS5(System.Int32)
extern "C"  bool UnityEngine_ProceduralMaterialGenerated_ilo_getBooleanS5_m1499028960 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_ProceduralMaterialGenerated::ilo_getStringS6(System.Int32)
extern "C"  String_t* UnityEngine_ProceduralMaterialGenerated_ilo_getStringS6_m1792873841 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ProceduralMaterialGenerated::ilo_setArrayS7(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_ProceduralMaterialGenerated_ilo_setArrayS7_m13080449 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ProceduralMaterialGenerated::ilo_setObject8(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_ProceduralMaterialGenerated_ilo_setObject8_m1967426925 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_ProceduralMaterialGenerated::ilo_getObject9(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_ProceduralMaterialGenerated_ilo_getObject9_m3693864165 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
