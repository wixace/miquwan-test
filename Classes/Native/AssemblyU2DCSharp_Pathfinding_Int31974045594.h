﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Int3
struct  Int3_t1974045594 
{
public:
	// System.Int32 Pathfinding.Int3::x
	int32_t ___x_3;
	// System.Int32 Pathfinding.Int3::y
	int32_t ___y_4;
	// System.Int32 Pathfinding.Int3::z
	int32_t ___z_5;

public:
	inline static int32_t get_offset_of_x_3() { return static_cast<int32_t>(offsetof(Int3_t1974045594, ___x_3)); }
	inline int32_t get_x_3() const { return ___x_3; }
	inline int32_t* get_address_of_x_3() { return &___x_3; }
	inline void set_x_3(int32_t value)
	{
		___x_3 = value;
	}

	inline static int32_t get_offset_of_y_4() { return static_cast<int32_t>(offsetof(Int3_t1974045594, ___y_4)); }
	inline int32_t get_y_4() const { return ___y_4; }
	inline int32_t* get_address_of_y_4() { return &___y_4; }
	inline void set_y_4(int32_t value)
	{
		___y_4 = value;
	}

	inline static int32_t get_offset_of_z_5() { return static_cast<int32_t>(offsetof(Int3_t1974045594, ___z_5)); }
	inline int32_t get_z_5() const { return ___z_5; }
	inline int32_t* get_address_of_z_5() { return &___z_5; }
	inline void set_z_5(int32_t value)
	{
		___z_5 = value;
	}
};

struct Int3_t1974045594_StaticFields
{
public:
	// Pathfinding.Int3 Pathfinding.Int3::_zero
	Int3_t1974045594  ____zero_6;

public:
	inline static int32_t get_offset_of__zero_6() { return static_cast<int32_t>(offsetof(Int3_t1974045594_StaticFields, ____zero_6)); }
	inline Int3_t1974045594  get__zero_6() const { return ____zero_6; }
	inline Int3_t1974045594 * get_address_of__zero_6() { return &____zero_6; }
	inline void set__zero_6(Int3_t1974045594  value)
	{
		____zero_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Pathfinding.Int3
struct Int3_t1974045594_marshaled_pinvoke
{
	int32_t ___x_3;
	int32_t ___y_4;
	int32_t ___z_5;
};
// Native definition for marshalling of: Pathfinding.Int3
struct Int3_t1974045594_marshaled_com
{
	int32_t ___x_3;
	int32_t ___y_4;
	int32_t ___z_5;
};
