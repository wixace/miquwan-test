//
//  ToUnityUtil.m
//  AnquSDKDemo
//
//  Created by XYG on 2017/8/18.
//  Copyright © 2017年 anqu. All rights reserved.
//
#import "ToUnityUtil.h"
#import <CoreGraphics/CoreGraphics.h>
#if defined(UNITY_VERSION)
#import "UnityAppController.h"
#import "UI/UnityView.h"
#endif


@implementation ToUnityUtil

static ToUnityUtil* _instance=nil;
warningCallBack boxCallBack = nil;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

+ (ToUnityUtil*) instance
{
	if (_instance==nil)
	{
		_instance=[[ToUnityUtil alloc]init];
	}
	
	return _instance;
}


//*************************弹窗**************************
-(void)showWarningBox:(NSString*) strTitle message:(NSString*) strText leftBtnTxt:(NSString*) leftTxt rightBtnTxt:(NSString*) rightTxt callBack:(warningCallBack)callBack
{
    boxCallBack = callBack;
	UIAlertView* alertview=[ [UIAlertView alloc] initWithTitle:strTitle message:strText delegate:self cancelButtonTitle:leftTxt otherButtonTitles:rightTxt,nil];
	[alertview show];
}

-(void)showWarningBox:(NSString*) strTitle message:(NSString*) strText leftBtnTxt:(NSString*) leftTxt callBack:(warningCallBack)callBack
{
    boxCallBack = callBack;
    UIAlertView* alertview=[ [UIAlertView alloc] initWithTitle:strTitle message:strText delegate:self cancelButtonTitle:leftTxt otherButtonTitles:nil];
    [alertview show];
}

-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(boxCallBack != nil){
        boxCallBack(alertView.cancelButtonIndex == buttonIndex);
        boxCallBack = nil;
    }else{
        if(alertView.cancelButtonIndex == buttonIndex){
            //取消按钮
            SendMsg2Unity("OnDialogOK","");
        }else{
            //确定按钮
            SendMsg2Unity("OnDialogCancel","");
        }
    }
}
//*************************弹窗end**************************
@end

CGRect getSafeArea(UIView* view)
{
    UIScreen *mainScreen = [UIScreen mainScreen];
    
    CGSize screenSize = mainScreen.bounds.size;
    CGRect screenRect = CGRectMake(0, 0, screenSize.width, screenSize.height);
    UIEdgeInsets insets = UIEdgeInsetsMake(0,0 ,0 ,0 );
    if([view respondsToSelector:@selector(safeAreaInsets)]){
        if (@available(iOS 11.0, *)) {
            insets = [view safeAreaInsets];
        }
    }
    screenRect.origin.x += insets.left * 0.5f;
    screenRect.origin.y += insets.bottom * 0.5f;
    screenRect.size.width -= insets.left * 0.5f + insets.right * 0.5f;
    //screenRect.size.height -= insets.bottom * 0.5f;
    float scale = mainScreen.scale;
    
    screenRect.origin.x *= scale;
    screenRect.origin.y *= scale;
    screenRect.size.width *= scale;
    screenRect.size.height *= scale;
    return screenRect;
}

#if defined(__cplusplus)
extern "C"{    
#endif

#if defined(UNITY_VERSION)
    extern void UnitySendMessage(const char *,const char *,const char *);
#endif

    NSString *_CreateNSString(const char* string){
        if(string)
            return [NSString stringWithUTF8String:string];
        else
            return [NSString stringWithUTF8String:""];
    }


    void SendMsg2Unity(const char *funName,const char *arg){
        
#if defined(UNITY_VERSION)
        UnitySendMessage("sdks",funName,arg);
#endif
    
    }
    
    void GetSafeAreaImpl(float* x,float* y,float* w,float* h){
#if defined(UNITY_VERSION)
        UIView* view = ((UnityAppController*)[UIApplication sharedApplication].delegate).unityView;
        CGRect area = getSafeArea(view);
        *x = area.origin.x;
        *y = area.origin.y;
        *w = area.size.width;
        *h = area.size.height;
#else
        *x = 0;
        *y = 0;
        *w = 0;
        *h = 0;
#endif
    }
	//**************************获取设备的语言及地区格式 语言-地区 zh-Hans-GB*****************************
    const char* mh_get_user_language_locale()
    {
		NSString *language = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] firstObject];
        char* pc = (char *)malloc(80);
        strcpy(pc, [language UTF8String]);
        return pc;
    }

	//*************************弹窗**************************
	void _showWarningBox1(char* strTitle ,char* strText,char* leftBtnTxt)
	{	
        [[ToUnityUtil instance] showWarningBox:_CreateNSString(strTitle) message:_CreateNSString(strText) leftBtnTxt:_CreateNSString(leftBtnTxt) callBack:nil];
	}
		
	void _showWarningBox2(char* strTitle ,char* strText,char* leftBtnTxt,char* rightBtnTxt)
	{
		[[ToUnityUtil instance] showWarningBox:_CreateNSString(strTitle) message:_CreateNSString(strText) leftBtnTxt:_CreateNSString(leftBtnTxt) rightBtnTxt:_CreateNSString(rightBtnTxt) callBack:nil];
	}
	//*************************弹窗end**************************

#if defined(__cplusplus)
}
#endif
