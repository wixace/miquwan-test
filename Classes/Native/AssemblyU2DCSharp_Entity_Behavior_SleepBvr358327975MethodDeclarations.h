﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.SleepBvr
struct SleepBvr_t358327975;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;
// CombatEntity
struct CombatEntity_t684137495;
// BuffCtrl
struct BuffCtrl_t2836564350;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"

// System.Void Entity.Behavior.SleepBvr::.ctor()
extern "C"  void SleepBvr__ctor_m3407012451 (SleepBvr_t358327975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.SleepBvr::get_id()
extern "C"  uint8_t SleepBvr_get_id_m780647575 (SleepBvr_t358327975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Entity.Behavior.SleepBvr::CanTran2OtherBehavior(Entity.Behavior.IBehavior)
extern "C"  bool SleepBvr_CanTran2OtherBehavior_m700556432 (SleepBvr_t358327975 * __this, IBehavior_t770859129 * ___behavior0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.SleepBvr::Reason()
extern "C"  void SleepBvr_Reason_m2089558981 (SleepBvr_t358327975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.SleepBvr::Action()
extern "C"  void SleepBvr_Action_m1286265655 (SleepBvr_t358327975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.SleepBvr::DoEntering()
extern "C"  void SleepBvr_DoEntering_m3284267254 (SleepBvr_t358327975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.SleepBvr::DoLeaving()
extern "C"  void SleepBvr_DoLeaving_m1936327658 (SleepBvr_t358327975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.SleepBvr::ilo_get_entity1(Entity.Behavior.IBehavior)
extern "C"  CombatEntity_t684137495 * SleepBvr_ilo_get_entity1_m3102369275 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BuffCtrl Entity.Behavior.SleepBvr::ilo_get_buffCtrl2(CombatEntity)
extern "C"  BuffCtrl_t2836564350 * SleepBvr_ilo_get_buffCtrl2_m1361373155 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl Entity.Behavior.SleepBvr::ilo_get_bvrCtrl3(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * SleepBvr_ilo_get_bvrCtrl3_m3836501806 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Entity.Behavior.SleepBvr::ilo_get_atkRange4(CombatEntity)
extern "C"  float SleepBvr_ilo_get_atkRange4_m1325063217 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
