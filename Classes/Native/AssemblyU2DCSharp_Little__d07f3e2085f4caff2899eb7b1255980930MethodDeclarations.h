﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._d07f3e2085f4caff2899eb7b0a3c0a1f
struct _d07f3e2085f4caff2899eb7b0a3c0a1f_t1255980930;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__d07f3e2085f4caff2899eb7b1255980930.h"

// System.Void Little._d07f3e2085f4caff2899eb7b0a3c0a1f::.ctor()
extern "C"  void _d07f3e2085f4caff2899eb7b0a3c0a1f__ctor_m3239622891 (_d07f3e2085f4caff2899eb7b0a3c0a1f_t1255980930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._d07f3e2085f4caff2899eb7b0a3c0a1f::_d07f3e2085f4caff2899eb7b0a3c0a1fm2(System.Int32)
extern "C"  int32_t _d07f3e2085f4caff2899eb7b0a3c0a1f__d07f3e2085f4caff2899eb7b0a3c0a1fm2_m1896862297 (_d07f3e2085f4caff2899eb7b0a3c0a1f_t1255980930 * __this, int32_t ____d07f3e2085f4caff2899eb7b0a3c0a1fa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._d07f3e2085f4caff2899eb7b0a3c0a1f::_d07f3e2085f4caff2899eb7b0a3c0a1fm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _d07f3e2085f4caff2899eb7b0a3c0a1f__d07f3e2085f4caff2899eb7b0a3c0a1fm_m3163237501 (_d07f3e2085f4caff2899eb7b0a3c0a1f_t1255980930 * __this, int32_t ____d07f3e2085f4caff2899eb7b0a3c0a1fa0, int32_t ____d07f3e2085f4caff2899eb7b0a3c0a1f831, int32_t ____d07f3e2085f4caff2899eb7b0a3c0a1fc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._d07f3e2085f4caff2899eb7b0a3c0a1f::ilo__d07f3e2085f4caff2899eb7b0a3c0a1fm21(Little._d07f3e2085f4caff2899eb7b0a3c0a1f,System.Int32)
extern "C"  int32_t _d07f3e2085f4caff2899eb7b0a3c0a1f_ilo__d07f3e2085f4caff2899eb7b0a3c0a1fm21_m2992429961 (Il2CppObject * __this /* static, unused */, _d07f3e2085f4caff2899eb7b0a3c0a1f_t1255980930 * ____this0, int32_t ____d07f3e2085f4caff2899eb7b0a3c0a1fa1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
