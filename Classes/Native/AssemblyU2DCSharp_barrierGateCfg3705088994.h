﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// barrierGateCfg
struct  barrierGateCfg_t3705088994  : public CsCfgBase_t69924517
{
public:
	// System.Int32 barrierGateCfg::id
	int32_t ___id_0;
	// System.Int32 barrierGateCfg::initState
	int32_t ___initState_1;
	// System.Int32 barrierGateCfg::openWay
	int32_t ___openWay_2;
	// System.Int32 barrierGateCfg::openParameter
	int32_t ___openParameter_3;
	// System.Int32 barrierGateCfg::closeWay
	int32_t ___closeWay_4;
	// System.Int32 barrierGateCfg::closeParameter
	int32_t ___closeParameter_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(barrierGateCfg_t3705088994, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_initState_1() { return static_cast<int32_t>(offsetof(barrierGateCfg_t3705088994, ___initState_1)); }
	inline int32_t get_initState_1() const { return ___initState_1; }
	inline int32_t* get_address_of_initState_1() { return &___initState_1; }
	inline void set_initState_1(int32_t value)
	{
		___initState_1 = value;
	}

	inline static int32_t get_offset_of_openWay_2() { return static_cast<int32_t>(offsetof(barrierGateCfg_t3705088994, ___openWay_2)); }
	inline int32_t get_openWay_2() const { return ___openWay_2; }
	inline int32_t* get_address_of_openWay_2() { return &___openWay_2; }
	inline void set_openWay_2(int32_t value)
	{
		___openWay_2 = value;
	}

	inline static int32_t get_offset_of_openParameter_3() { return static_cast<int32_t>(offsetof(barrierGateCfg_t3705088994, ___openParameter_3)); }
	inline int32_t get_openParameter_3() const { return ___openParameter_3; }
	inline int32_t* get_address_of_openParameter_3() { return &___openParameter_3; }
	inline void set_openParameter_3(int32_t value)
	{
		___openParameter_3 = value;
	}

	inline static int32_t get_offset_of_closeWay_4() { return static_cast<int32_t>(offsetof(barrierGateCfg_t3705088994, ___closeWay_4)); }
	inline int32_t get_closeWay_4() const { return ___closeWay_4; }
	inline int32_t* get_address_of_closeWay_4() { return &___closeWay_4; }
	inline void set_closeWay_4(int32_t value)
	{
		___closeWay_4 = value;
	}

	inline static int32_t get_offset_of_closeParameter_5() { return static_cast<int32_t>(offsetof(barrierGateCfg_t3705088994, ___closeParameter_5)); }
	inline int32_t get_closeParameter_5() const { return ___closeParameter_5; }
	inline int32_t* get_address_of_closeParameter_5() { return &___closeParameter_5; }
	inline void set_closeParameter_5(int32_t value)
	{
		___closeParameter_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
