﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_LocationInfoGenerated
struct UnityEngine_LocationInfoGenerated_t240295524;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_LocationInfoGenerated::.ctor()
extern "C"  void UnityEngine_LocationInfoGenerated__ctor_m1366044551 (UnityEngine_LocationInfoGenerated_t240295524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LocationInfoGenerated::.cctor()
extern "C"  void UnityEngine_LocationInfoGenerated__cctor_m3210579206 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LocationInfoGenerated::LocationInfo_LocationInfo1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LocationInfoGenerated_LocationInfo_LocationInfo1_m2845396587 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LocationInfoGenerated::LocationInfo_latitude(JSVCall)
extern "C"  void UnityEngine_LocationInfoGenerated_LocationInfo_latitude_m1612117818 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LocationInfoGenerated::LocationInfo_longitude(JSVCall)
extern "C"  void UnityEngine_LocationInfoGenerated_LocationInfo_longitude_m3076059159 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LocationInfoGenerated::LocationInfo_altitude(JSVCall)
extern "C"  void UnityEngine_LocationInfoGenerated_LocationInfo_altitude_m2584671684 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LocationInfoGenerated::LocationInfo_horizontalAccuracy(JSVCall)
extern "C"  void UnityEngine_LocationInfoGenerated_LocationInfo_horizontalAccuracy_m3877859721 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LocationInfoGenerated::LocationInfo_verticalAccuracy(JSVCall)
extern "C"  void UnityEngine_LocationInfoGenerated_LocationInfo_verticalAccuracy_m4265413751 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LocationInfoGenerated::LocationInfo_timestamp(JSVCall)
extern "C"  void UnityEngine_LocationInfoGenerated_LocationInfo_timestamp_m1453274256 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LocationInfoGenerated::__Register()
extern "C"  void UnityEngine_LocationInfoGenerated___Register_m836299168 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LocationInfoGenerated::ilo_setSingle1(System.Int32,System.Single)
extern "C"  void UnityEngine_LocationInfoGenerated_ilo_setSingle1_m7029645 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LocationInfoGenerated::ilo_setDouble2(System.Int32,System.Double)
extern "C"  void UnityEngine_LocationInfoGenerated_ilo_setDouble2_m1255891644 (Il2CppObject * __this /* static, unused */, int32_t ___e0, double ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
