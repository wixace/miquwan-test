﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_MHIAPMgr_PayInfo_SdkType483686632.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MHIAPMgr.PayInfo
struct  PayInfo_t444430236  : public Il2CppObject
{
public:
	// System.Single MHIAPMgr.PayInfo::requestTime
	float ___requestTime_0;
	// System.UInt32 MHIAPMgr.PayInfo::requestCount
	uint32_t ___requestCount_1;
	// MHIAPMgr.PayInfo/SdkType MHIAPMgr.PayInfo::<sdkType>k__BackingField
	int32_t ___U3CsdkTypeU3Ek__BackingField_2;
	// System.String MHIAPMgr.PayInfo::<mainInfo>k__BackingField
	String_t* ___U3CmainInfoU3Ek__BackingField_3;
	// System.String MHIAPMgr.PayInfo::<roleid>k__BackingField
	String_t* ___U3CroleidU3Ek__BackingField_4;
	// System.String MHIAPMgr.PayInfo::<serverId>k__BackingField
	String_t* ___U3CserverIdU3Ek__BackingField_5;
	// System.String MHIAPMgr.PayInfo::<type>k__BackingField
	String_t* ___U3CtypeU3Ek__BackingField_6;
	// System.String MHIAPMgr.PayInfo::<amount>k__BackingField
	String_t* ___U3CamountU3Ek__BackingField_7;
	// System.String MHIAPMgr.PayInfo::<orderId>k__BackingField
	String_t* ___U3CorderIdU3Ek__BackingField_8;
	// System.String MHIAPMgr.PayInfo::<receiptData>k__BackingField
	String_t* ___U3CreceiptDataU3Ek__BackingField_9;
	// System.String MHIAPMgr.PayInfo::<pcbUrl>k__BackingField
	String_t* ___U3CpcbUrlU3Ek__BackingField_10;
	// System.String MHIAPMgr.PayInfo::<environment>k__BackingField
	String_t* ___U3CenvironmentU3Ek__BackingField_11;
	// System.String MHIAPMgr.PayInfo::<appKey>k__BackingField
	String_t* ___U3CappKeyU3Ek__BackingField_12;
	// System.String MHIAPMgr.PayInfo::<eargs>k__BackingField
	String_t* ___U3CeargsU3Ek__BackingField_13;
	// System.String MHIAPMgr.PayInfo::<transactionId>k__BackingField
	String_t* ___U3CtransactionIdU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_requestTime_0() { return static_cast<int32_t>(offsetof(PayInfo_t444430236, ___requestTime_0)); }
	inline float get_requestTime_0() const { return ___requestTime_0; }
	inline float* get_address_of_requestTime_0() { return &___requestTime_0; }
	inline void set_requestTime_0(float value)
	{
		___requestTime_0 = value;
	}

	inline static int32_t get_offset_of_requestCount_1() { return static_cast<int32_t>(offsetof(PayInfo_t444430236, ___requestCount_1)); }
	inline uint32_t get_requestCount_1() const { return ___requestCount_1; }
	inline uint32_t* get_address_of_requestCount_1() { return &___requestCount_1; }
	inline void set_requestCount_1(uint32_t value)
	{
		___requestCount_1 = value;
	}

	inline static int32_t get_offset_of_U3CsdkTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PayInfo_t444430236, ___U3CsdkTypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CsdkTypeU3Ek__BackingField_2() const { return ___U3CsdkTypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CsdkTypeU3Ek__BackingField_2() { return &___U3CsdkTypeU3Ek__BackingField_2; }
	inline void set_U3CsdkTypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CsdkTypeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CmainInfoU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PayInfo_t444430236, ___U3CmainInfoU3Ek__BackingField_3)); }
	inline String_t* get_U3CmainInfoU3Ek__BackingField_3() const { return ___U3CmainInfoU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CmainInfoU3Ek__BackingField_3() { return &___U3CmainInfoU3Ek__BackingField_3; }
	inline void set_U3CmainInfoU3Ek__BackingField_3(String_t* value)
	{
		___U3CmainInfoU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmainInfoU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CroleidU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PayInfo_t444430236, ___U3CroleidU3Ek__BackingField_4)); }
	inline String_t* get_U3CroleidU3Ek__BackingField_4() const { return ___U3CroleidU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CroleidU3Ek__BackingField_4() { return &___U3CroleidU3Ek__BackingField_4; }
	inline void set_U3CroleidU3Ek__BackingField_4(String_t* value)
	{
		___U3CroleidU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CroleidU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CserverIdU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PayInfo_t444430236, ___U3CserverIdU3Ek__BackingField_5)); }
	inline String_t* get_U3CserverIdU3Ek__BackingField_5() const { return ___U3CserverIdU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CserverIdU3Ek__BackingField_5() { return &___U3CserverIdU3Ek__BackingField_5; }
	inline void set_U3CserverIdU3Ek__BackingField_5(String_t* value)
	{
		___U3CserverIdU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CserverIdU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CtypeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PayInfo_t444430236, ___U3CtypeU3Ek__BackingField_6)); }
	inline String_t* get_U3CtypeU3Ek__BackingField_6() const { return ___U3CtypeU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CtypeU3Ek__BackingField_6() { return &___U3CtypeU3Ek__BackingField_6; }
	inline void set_U3CtypeU3Ek__BackingField_6(String_t* value)
	{
		___U3CtypeU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtypeU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CamountU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PayInfo_t444430236, ___U3CamountU3Ek__BackingField_7)); }
	inline String_t* get_U3CamountU3Ek__BackingField_7() const { return ___U3CamountU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CamountU3Ek__BackingField_7() { return &___U3CamountU3Ek__BackingField_7; }
	inline void set_U3CamountU3Ek__BackingField_7(String_t* value)
	{
		___U3CamountU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CamountU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CorderIdU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PayInfo_t444430236, ___U3CorderIdU3Ek__BackingField_8)); }
	inline String_t* get_U3CorderIdU3Ek__BackingField_8() const { return ___U3CorderIdU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CorderIdU3Ek__BackingField_8() { return &___U3CorderIdU3Ek__BackingField_8; }
	inline void set_U3CorderIdU3Ek__BackingField_8(String_t* value)
	{
		___U3CorderIdU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CorderIdU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CreceiptDataU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PayInfo_t444430236, ___U3CreceiptDataU3Ek__BackingField_9)); }
	inline String_t* get_U3CreceiptDataU3Ek__BackingField_9() const { return ___U3CreceiptDataU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CreceiptDataU3Ek__BackingField_9() { return &___U3CreceiptDataU3Ek__BackingField_9; }
	inline void set_U3CreceiptDataU3Ek__BackingField_9(String_t* value)
	{
		___U3CreceiptDataU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CreceiptDataU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CpcbUrlU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PayInfo_t444430236, ___U3CpcbUrlU3Ek__BackingField_10)); }
	inline String_t* get_U3CpcbUrlU3Ek__BackingField_10() const { return ___U3CpcbUrlU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CpcbUrlU3Ek__BackingField_10() { return &___U3CpcbUrlU3Ek__BackingField_10; }
	inline void set_U3CpcbUrlU3Ek__BackingField_10(String_t* value)
	{
		___U3CpcbUrlU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpcbUrlU3Ek__BackingField_10, value);
	}

	inline static int32_t get_offset_of_U3CenvironmentU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PayInfo_t444430236, ___U3CenvironmentU3Ek__BackingField_11)); }
	inline String_t* get_U3CenvironmentU3Ek__BackingField_11() const { return ___U3CenvironmentU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CenvironmentU3Ek__BackingField_11() { return &___U3CenvironmentU3Ek__BackingField_11; }
	inline void set_U3CenvironmentU3Ek__BackingField_11(String_t* value)
	{
		___U3CenvironmentU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CenvironmentU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CappKeyU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PayInfo_t444430236, ___U3CappKeyU3Ek__BackingField_12)); }
	inline String_t* get_U3CappKeyU3Ek__BackingField_12() const { return ___U3CappKeyU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CappKeyU3Ek__BackingField_12() { return &___U3CappKeyU3Ek__BackingField_12; }
	inline void set_U3CappKeyU3Ek__BackingField_12(String_t* value)
	{
		___U3CappKeyU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CappKeyU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of_U3CeargsU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PayInfo_t444430236, ___U3CeargsU3Ek__BackingField_13)); }
	inline String_t* get_U3CeargsU3Ek__BackingField_13() const { return ___U3CeargsU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CeargsU3Ek__BackingField_13() { return &___U3CeargsU3Ek__BackingField_13; }
	inline void set_U3CeargsU3Ek__BackingField_13(String_t* value)
	{
		___U3CeargsU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CeargsU3Ek__BackingField_13, value);
	}

	inline static int32_t get_offset_of_U3CtransactionIdU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PayInfo_t444430236, ___U3CtransactionIdU3Ek__BackingField_14)); }
	inline String_t* get_U3CtransactionIdU3Ek__BackingField_14() const { return ___U3CtransactionIdU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CtransactionIdU3Ek__BackingField_14() { return &___U3CtransactionIdU3Ek__BackingField_14; }
	inline void set_U3CtransactionIdU3Ek__BackingField_14(String_t* value)
	{
		___U3CtransactionIdU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtransactionIdU3Ek__BackingField_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
