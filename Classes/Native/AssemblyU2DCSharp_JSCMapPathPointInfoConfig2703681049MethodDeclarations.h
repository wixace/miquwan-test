﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSCMapPathPointInfoConfig
struct JSCMapPathPointInfoConfig_t2703681049;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "codegen/il2cpp-codegen.h"

// System.Void JSCMapPathPointInfoConfig::.ctor()
extern "C"  void JSCMapPathPointInfoConfig__ctor_m1021062002 (JSCMapPathPointInfoConfig_t2703681049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension JSCMapPathPointInfoConfig::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * JSCMapPathPointInfoConfig_ProtoBuf_IExtensible_GetExtensionObject_m2171635830 (JSCMapPathPointInfoConfig_t2703681049 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSCMapPathPointInfoConfig::get_id()
extern "C"  int32_t JSCMapPathPointInfoConfig_get_id_m4072848392 (JSCMapPathPointInfoConfig_t2703681049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCMapPathPointInfoConfig::set_id(System.Int32)
extern "C"  void JSCMapPathPointInfoConfig_set_id_m3868408219 (JSCMapPathPointInfoConfig_t2703681049 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSCMapPathPointInfoConfig::get_x()
extern "C"  float JSCMapPathPointInfoConfig_get_x_m1514850443 (JSCMapPathPointInfoConfig_t2703681049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCMapPathPointInfoConfig::set_x(System.Single)
extern "C"  void JSCMapPathPointInfoConfig_set_x_m1785290176 (JSCMapPathPointInfoConfig_t2703681049 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSCMapPathPointInfoConfig::get_y()
extern "C"  float JSCMapPathPointInfoConfig_get_y_m1514851404 (JSCMapPathPointInfoConfig_t2703681049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCMapPathPointInfoConfig::set_y(System.Single)
extern "C"  void JSCMapPathPointInfoConfig_set_y_m1274755999 (JSCMapPathPointInfoConfig_t2703681049 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSCMapPathPointInfoConfig::get_z()
extern "C"  float JSCMapPathPointInfoConfig_get_z_m1514852365 (JSCMapPathPointInfoConfig_t2703681049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCMapPathPointInfoConfig::set_z(System.Single)
extern "C"  void JSCMapPathPointInfoConfig_set_z_m764221822 (JSCMapPathPointInfoConfig_t2703681049 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSCMapPathPointInfoConfig::get_angle()
extern "C"  float JSCMapPathPointInfoConfig_get_angle_m3920477478 (JSCMapPathPointInfoConfig_t2703681049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCMapPathPointInfoConfig::set_angle(System.Single)
extern "C"  void JSCMapPathPointInfoConfig_set_angle_m3462962437 (JSCMapPathPointInfoConfig_t2703681049 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
