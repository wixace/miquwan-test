﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BGSound
struct BGSound_t558445514;
// UnityEngine.AudioSource
struct AudioSource_t1740077639;
// SoundCfg
struct SoundCfg_t1807275253;
// UnityEngine.AudioClip
struct AudioClip_t794140988;
// ISound
struct ISound_t2170003014;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SoundCfg1807275253.h"
#include "UnityEngine_UnityEngine_AudioClip794140988.h"
#include "AssemblyU2DCSharp_ISound2170003014.h"

// System.Void BGSound::.ctor()
extern "C"  void BGSound__ctor_m1451007713 (BGSound_t558445514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BGSound::Pause()
extern "C"  void BGSound_Pause_m1505133685 (BGSound_t558445514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioSource BGSound::Play(SoundCfg,UnityEngine.AudioClip)
extern "C"  AudioSource_t1740077639 * BGSound_Play_m2892343238 (BGSound_t558445514 * __this, SoundCfg_t1807275253 * ___soundCfg0, AudioClip_t794140988 * ___clip1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BGSound::RePlay()
extern "C"  void BGSound_RePlay_m1089387530 (BGSound_t558445514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BGSound::SetPitch(System.Single,System.Int32)
extern "C"  void BGSound_SetPitch_m4254663565 (BGSound_t558445514 * __this, float ___pitch0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BGSound::Stop()
extern "C"  void BGSound_Stop_m2091466181 (BGSound_t558445514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BGSound::StopByID(System.Int32)
extern "C"  void BGSound_StopByID_m2285840872 (BGSound_t558445514 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BGSound::Update()
extern "C"  void BGSound_Update_m3758428236 (BGSound_t558445514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BGSound::get_mute()
extern "C"  bool BGSound_get_mute_m3931815633 (BGSound_t558445514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BGSound::set_mute(System.Boolean)
extern "C"  void BGSound_set_mute_m3677859120 (BGSound_t558445514 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BGSound::get_volume()
extern "C"  float BGSound_get_volume_m93554362 (BGSound_t558445514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BGSound::set_volume(System.Single)
extern "C"  void BGSound_set_volume_m3067636593 (BGSound_t558445514 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BGSound::StopLoopSound()
extern "C"  void BGSound_StopLoopSound_m2433786536 (BGSound_t558445514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BGSound::ilo_set_mute1(ISound,System.Boolean)
extern "C"  void BGSound_ilo_set_mute1_m2847505148 (Il2CppObject * __this /* static, unused */, ISound_t2170003014 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
