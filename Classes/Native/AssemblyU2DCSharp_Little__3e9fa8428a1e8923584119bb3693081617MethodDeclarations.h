﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._3e9fa8428a1e8923584119bbd22d6135
struct _3e9fa8428a1e8923584119bbd22d6135_t3693081617;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._3e9fa8428a1e8923584119bbd22d6135::.ctor()
extern "C"  void _3e9fa8428a1e8923584119bbd22d6135__ctor_m1261843772 (_3e9fa8428a1e8923584119bbd22d6135_t3693081617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._3e9fa8428a1e8923584119bbd22d6135::_3e9fa8428a1e8923584119bbd22d6135m2(System.Int32)
extern "C"  int32_t _3e9fa8428a1e8923584119bbd22d6135__3e9fa8428a1e8923584119bbd22d6135m2_m2105839609 (_3e9fa8428a1e8923584119bbd22d6135_t3693081617 * __this, int32_t ____3e9fa8428a1e8923584119bbd22d6135a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._3e9fa8428a1e8923584119bbd22d6135::_3e9fa8428a1e8923584119bbd22d6135m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _3e9fa8428a1e8923584119bbd22d6135__3e9fa8428a1e8923584119bbd22d6135m_m1959330525 (_3e9fa8428a1e8923584119bbd22d6135_t3693081617 * __this, int32_t ____3e9fa8428a1e8923584119bbd22d6135a0, int32_t ____3e9fa8428a1e8923584119bbd22d6135351, int32_t ____3e9fa8428a1e8923584119bbd22d6135c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
