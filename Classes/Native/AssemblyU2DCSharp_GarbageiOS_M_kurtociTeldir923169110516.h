﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_kurtociTeldir92
struct  M_kurtociTeldir92_t3169110516  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_kurtociTeldir92::_pawbaqe
	int32_t ____pawbaqe_0;
	// System.Boolean GarbageiOS.M_kurtociTeldir92::_kebow
	bool ____kebow_1;
	// System.Single GarbageiOS.M_kurtociTeldir92::_pirmairTesurmi
	float ____pirmairTesurmi_2;
	// System.Single GarbageiOS.M_kurtociTeldir92::_couyemsiDestedras
	float ____couyemsiDestedras_3;
	// System.Single GarbageiOS.M_kurtociTeldir92::_padou
	float ____padou_4;
	// System.Single GarbageiOS.M_kurtociTeldir92::_borhayFiswo
	float ____borhayFiswo_5;
	// System.Single GarbageiOS.M_kurtociTeldir92::_gestaiMuje
	float ____gestaiMuje_6;
	// System.String GarbageiOS.M_kurtociTeldir92::_nairju
	String_t* ____nairju_7;
	// System.UInt32 GarbageiOS.M_kurtociTeldir92::_fereqayjeNemicher
	uint32_t ____fereqayjeNemicher_8;
	// System.String GarbageiOS.M_kurtociTeldir92::_tairsispear
	String_t* ____tairsispear_9;
	// System.Single GarbageiOS.M_kurtociTeldir92::_dasayoo
	float ____dasayoo_10;
	// System.String GarbageiOS.M_kurtociTeldir92::_rulooCocaw
	String_t* ____rulooCocaw_11;
	// System.UInt32 GarbageiOS.M_kurtociTeldir92::_guseku
	uint32_t ____guseku_12;
	// System.Single GarbageiOS.M_kurtociTeldir92::_sousi
	float ____sousi_13;
	// System.String GarbageiOS.M_kurtociTeldir92::_seedoomeDairwhaircu
	String_t* ____seedoomeDairwhaircu_14;
	// System.UInt32 GarbageiOS.M_kurtociTeldir92::_hooxesereDrive
	uint32_t ____hooxesereDrive_15;
	// System.String GarbageiOS.M_kurtociTeldir92::_drirwaRatodrou
	String_t* ____drirwaRatodrou_16;
	// System.Int32 GarbageiOS.M_kurtociTeldir92::_feyetraw
	int32_t ____feyetraw_17;
	// System.Boolean GarbageiOS.M_kurtociTeldir92::_noneeteSounallqe
	bool ____noneeteSounallqe_18;

public:
	inline static int32_t get_offset_of__pawbaqe_0() { return static_cast<int32_t>(offsetof(M_kurtociTeldir92_t3169110516, ____pawbaqe_0)); }
	inline int32_t get__pawbaqe_0() const { return ____pawbaqe_0; }
	inline int32_t* get_address_of__pawbaqe_0() { return &____pawbaqe_0; }
	inline void set__pawbaqe_0(int32_t value)
	{
		____pawbaqe_0 = value;
	}

	inline static int32_t get_offset_of__kebow_1() { return static_cast<int32_t>(offsetof(M_kurtociTeldir92_t3169110516, ____kebow_1)); }
	inline bool get__kebow_1() const { return ____kebow_1; }
	inline bool* get_address_of__kebow_1() { return &____kebow_1; }
	inline void set__kebow_1(bool value)
	{
		____kebow_1 = value;
	}

	inline static int32_t get_offset_of__pirmairTesurmi_2() { return static_cast<int32_t>(offsetof(M_kurtociTeldir92_t3169110516, ____pirmairTesurmi_2)); }
	inline float get__pirmairTesurmi_2() const { return ____pirmairTesurmi_2; }
	inline float* get_address_of__pirmairTesurmi_2() { return &____pirmairTesurmi_2; }
	inline void set__pirmairTesurmi_2(float value)
	{
		____pirmairTesurmi_2 = value;
	}

	inline static int32_t get_offset_of__couyemsiDestedras_3() { return static_cast<int32_t>(offsetof(M_kurtociTeldir92_t3169110516, ____couyemsiDestedras_3)); }
	inline float get__couyemsiDestedras_3() const { return ____couyemsiDestedras_3; }
	inline float* get_address_of__couyemsiDestedras_3() { return &____couyemsiDestedras_3; }
	inline void set__couyemsiDestedras_3(float value)
	{
		____couyemsiDestedras_3 = value;
	}

	inline static int32_t get_offset_of__padou_4() { return static_cast<int32_t>(offsetof(M_kurtociTeldir92_t3169110516, ____padou_4)); }
	inline float get__padou_4() const { return ____padou_4; }
	inline float* get_address_of__padou_4() { return &____padou_4; }
	inline void set__padou_4(float value)
	{
		____padou_4 = value;
	}

	inline static int32_t get_offset_of__borhayFiswo_5() { return static_cast<int32_t>(offsetof(M_kurtociTeldir92_t3169110516, ____borhayFiswo_5)); }
	inline float get__borhayFiswo_5() const { return ____borhayFiswo_5; }
	inline float* get_address_of__borhayFiswo_5() { return &____borhayFiswo_5; }
	inline void set__borhayFiswo_5(float value)
	{
		____borhayFiswo_5 = value;
	}

	inline static int32_t get_offset_of__gestaiMuje_6() { return static_cast<int32_t>(offsetof(M_kurtociTeldir92_t3169110516, ____gestaiMuje_6)); }
	inline float get__gestaiMuje_6() const { return ____gestaiMuje_6; }
	inline float* get_address_of__gestaiMuje_6() { return &____gestaiMuje_6; }
	inline void set__gestaiMuje_6(float value)
	{
		____gestaiMuje_6 = value;
	}

	inline static int32_t get_offset_of__nairju_7() { return static_cast<int32_t>(offsetof(M_kurtociTeldir92_t3169110516, ____nairju_7)); }
	inline String_t* get__nairju_7() const { return ____nairju_7; }
	inline String_t** get_address_of__nairju_7() { return &____nairju_7; }
	inline void set__nairju_7(String_t* value)
	{
		____nairju_7 = value;
		Il2CppCodeGenWriteBarrier(&____nairju_7, value);
	}

	inline static int32_t get_offset_of__fereqayjeNemicher_8() { return static_cast<int32_t>(offsetof(M_kurtociTeldir92_t3169110516, ____fereqayjeNemicher_8)); }
	inline uint32_t get__fereqayjeNemicher_8() const { return ____fereqayjeNemicher_8; }
	inline uint32_t* get_address_of__fereqayjeNemicher_8() { return &____fereqayjeNemicher_8; }
	inline void set__fereqayjeNemicher_8(uint32_t value)
	{
		____fereqayjeNemicher_8 = value;
	}

	inline static int32_t get_offset_of__tairsispear_9() { return static_cast<int32_t>(offsetof(M_kurtociTeldir92_t3169110516, ____tairsispear_9)); }
	inline String_t* get__tairsispear_9() const { return ____tairsispear_9; }
	inline String_t** get_address_of__tairsispear_9() { return &____tairsispear_9; }
	inline void set__tairsispear_9(String_t* value)
	{
		____tairsispear_9 = value;
		Il2CppCodeGenWriteBarrier(&____tairsispear_9, value);
	}

	inline static int32_t get_offset_of__dasayoo_10() { return static_cast<int32_t>(offsetof(M_kurtociTeldir92_t3169110516, ____dasayoo_10)); }
	inline float get__dasayoo_10() const { return ____dasayoo_10; }
	inline float* get_address_of__dasayoo_10() { return &____dasayoo_10; }
	inline void set__dasayoo_10(float value)
	{
		____dasayoo_10 = value;
	}

	inline static int32_t get_offset_of__rulooCocaw_11() { return static_cast<int32_t>(offsetof(M_kurtociTeldir92_t3169110516, ____rulooCocaw_11)); }
	inline String_t* get__rulooCocaw_11() const { return ____rulooCocaw_11; }
	inline String_t** get_address_of__rulooCocaw_11() { return &____rulooCocaw_11; }
	inline void set__rulooCocaw_11(String_t* value)
	{
		____rulooCocaw_11 = value;
		Il2CppCodeGenWriteBarrier(&____rulooCocaw_11, value);
	}

	inline static int32_t get_offset_of__guseku_12() { return static_cast<int32_t>(offsetof(M_kurtociTeldir92_t3169110516, ____guseku_12)); }
	inline uint32_t get__guseku_12() const { return ____guseku_12; }
	inline uint32_t* get_address_of__guseku_12() { return &____guseku_12; }
	inline void set__guseku_12(uint32_t value)
	{
		____guseku_12 = value;
	}

	inline static int32_t get_offset_of__sousi_13() { return static_cast<int32_t>(offsetof(M_kurtociTeldir92_t3169110516, ____sousi_13)); }
	inline float get__sousi_13() const { return ____sousi_13; }
	inline float* get_address_of__sousi_13() { return &____sousi_13; }
	inline void set__sousi_13(float value)
	{
		____sousi_13 = value;
	}

	inline static int32_t get_offset_of__seedoomeDairwhaircu_14() { return static_cast<int32_t>(offsetof(M_kurtociTeldir92_t3169110516, ____seedoomeDairwhaircu_14)); }
	inline String_t* get__seedoomeDairwhaircu_14() const { return ____seedoomeDairwhaircu_14; }
	inline String_t** get_address_of__seedoomeDairwhaircu_14() { return &____seedoomeDairwhaircu_14; }
	inline void set__seedoomeDairwhaircu_14(String_t* value)
	{
		____seedoomeDairwhaircu_14 = value;
		Il2CppCodeGenWriteBarrier(&____seedoomeDairwhaircu_14, value);
	}

	inline static int32_t get_offset_of__hooxesereDrive_15() { return static_cast<int32_t>(offsetof(M_kurtociTeldir92_t3169110516, ____hooxesereDrive_15)); }
	inline uint32_t get__hooxesereDrive_15() const { return ____hooxesereDrive_15; }
	inline uint32_t* get_address_of__hooxesereDrive_15() { return &____hooxesereDrive_15; }
	inline void set__hooxesereDrive_15(uint32_t value)
	{
		____hooxesereDrive_15 = value;
	}

	inline static int32_t get_offset_of__drirwaRatodrou_16() { return static_cast<int32_t>(offsetof(M_kurtociTeldir92_t3169110516, ____drirwaRatodrou_16)); }
	inline String_t* get__drirwaRatodrou_16() const { return ____drirwaRatodrou_16; }
	inline String_t** get_address_of__drirwaRatodrou_16() { return &____drirwaRatodrou_16; }
	inline void set__drirwaRatodrou_16(String_t* value)
	{
		____drirwaRatodrou_16 = value;
		Il2CppCodeGenWriteBarrier(&____drirwaRatodrou_16, value);
	}

	inline static int32_t get_offset_of__feyetraw_17() { return static_cast<int32_t>(offsetof(M_kurtociTeldir92_t3169110516, ____feyetraw_17)); }
	inline int32_t get__feyetraw_17() const { return ____feyetraw_17; }
	inline int32_t* get_address_of__feyetraw_17() { return &____feyetraw_17; }
	inline void set__feyetraw_17(int32_t value)
	{
		____feyetraw_17 = value;
	}

	inline static int32_t get_offset_of__noneeteSounallqe_18() { return static_cast<int32_t>(offsetof(M_kurtociTeldir92_t3169110516, ____noneeteSounallqe_18)); }
	inline bool get__noneeteSounallqe_18() const { return ____noneeteSounallqe_18; }
	inline bool* get_address_of__noneeteSounallqe_18() { return &____noneeteSounallqe_18; }
	inline void set__noneeteSounallqe_18(bool value)
	{
		____noneeteSounallqe_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
