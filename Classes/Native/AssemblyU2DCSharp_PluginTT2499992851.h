﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginTT
struct  PluginTT_t2499992851  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginTT::userId
	String_t* ___userId_2;
	// System.String PluginTT::token
	String_t* ___token_3;
	// System.String PluginTT::configId
	String_t* ___configId_4;
	// System.String PluginTT::gameId
	String_t* ___gameId_5;
	// System.String PluginTT::ts
	String_t* ___ts_6;
	// System.String PluginTT::extra
	String_t* ___extra_7;
	// Mihua.SDK.PayInfo PluginTT::iapInfo
	PayInfo_t1775308120 * ___iapInfo_8;

public:
	inline static int32_t get_offset_of_userId_2() { return static_cast<int32_t>(offsetof(PluginTT_t2499992851, ___userId_2)); }
	inline String_t* get_userId_2() const { return ___userId_2; }
	inline String_t** get_address_of_userId_2() { return &___userId_2; }
	inline void set_userId_2(String_t* value)
	{
		___userId_2 = value;
		Il2CppCodeGenWriteBarrier(&___userId_2, value);
	}

	inline static int32_t get_offset_of_token_3() { return static_cast<int32_t>(offsetof(PluginTT_t2499992851, ___token_3)); }
	inline String_t* get_token_3() const { return ___token_3; }
	inline String_t** get_address_of_token_3() { return &___token_3; }
	inline void set_token_3(String_t* value)
	{
		___token_3 = value;
		Il2CppCodeGenWriteBarrier(&___token_3, value);
	}

	inline static int32_t get_offset_of_configId_4() { return static_cast<int32_t>(offsetof(PluginTT_t2499992851, ___configId_4)); }
	inline String_t* get_configId_4() const { return ___configId_4; }
	inline String_t** get_address_of_configId_4() { return &___configId_4; }
	inline void set_configId_4(String_t* value)
	{
		___configId_4 = value;
		Il2CppCodeGenWriteBarrier(&___configId_4, value);
	}

	inline static int32_t get_offset_of_gameId_5() { return static_cast<int32_t>(offsetof(PluginTT_t2499992851, ___gameId_5)); }
	inline String_t* get_gameId_5() const { return ___gameId_5; }
	inline String_t** get_address_of_gameId_5() { return &___gameId_5; }
	inline void set_gameId_5(String_t* value)
	{
		___gameId_5 = value;
		Il2CppCodeGenWriteBarrier(&___gameId_5, value);
	}

	inline static int32_t get_offset_of_ts_6() { return static_cast<int32_t>(offsetof(PluginTT_t2499992851, ___ts_6)); }
	inline String_t* get_ts_6() const { return ___ts_6; }
	inline String_t** get_address_of_ts_6() { return &___ts_6; }
	inline void set_ts_6(String_t* value)
	{
		___ts_6 = value;
		Il2CppCodeGenWriteBarrier(&___ts_6, value);
	}

	inline static int32_t get_offset_of_extra_7() { return static_cast<int32_t>(offsetof(PluginTT_t2499992851, ___extra_7)); }
	inline String_t* get_extra_7() const { return ___extra_7; }
	inline String_t** get_address_of_extra_7() { return &___extra_7; }
	inline void set_extra_7(String_t* value)
	{
		___extra_7 = value;
		Il2CppCodeGenWriteBarrier(&___extra_7, value);
	}

	inline static int32_t get_offset_of_iapInfo_8() { return static_cast<int32_t>(offsetof(PluginTT_t2499992851, ___iapInfo_8)); }
	inline PayInfo_t1775308120 * get_iapInfo_8() const { return ___iapInfo_8; }
	inline PayInfo_t1775308120 ** get_address_of_iapInfo_8() { return &___iapInfo_8; }
	inline void set_iapInfo_8(PayInfo_t1775308120 * value)
	{
		___iapInfo_8 = value;
		Il2CppCodeGenWriteBarrier(&___iapInfo_8, value);
	}
};

struct PluginTT_t2499992851_StaticFields
{
public:
	// System.Action PluginTT::<>f__am$cache7
	Action_t3771233898 * ___U3CU3Ef__amU24cache7_9;
	// System.Action PluginTT::<>f__am$cache8
	Action_t3771233898 * ___U3CU3Ef__amU24cache8_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_9() { return static_cast<int32_t>(offsetof(PluginTT_t2499992851_StaticFields, ___U3CU3Ef__amU24cache7_9)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache7_9() const { return ___U3CU3Ef__amU24cache7_9; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache7_9() { return &___U3CU3Ef__amU24cache7_9; }
	inline void set_U3CU3Ef__amU24cache7_9(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache7_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_10() { return static_cast<int32_t>(offsetof(PluginTT_t2499992851_StaticFields, ___U3CU3Ef__amU24cache8_10)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache8_10() const { return ___U3CU3Ef__amU24cache8_10; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache8_10() { return &___U3CU3Ef__amU24cache8_10; }
	inline void set_U3CU3Ef__amU24cache8_10(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache8_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
