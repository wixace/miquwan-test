﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,TargetMgr/FightPoint>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4108116206(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1571140176 *, Dictionary_2_t253816784 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,TargetMgr/FightPoint>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3102238515(__this, method) ((  Il2CppObject * (*) (Enumerator_t1571140176 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,TargetMgr/FightPoint>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3889051399(__this, method) ((  void (*) (Enumerator_t1571140176 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,TargetMgr/FightPoint>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m857697936(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1571140176 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,TargetMgr/FightPoint>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1680617935(__this, method) ((  Il2CppObject * (*) (Enumerator_t1571140176 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,TargetMgr/FightPoint>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1214568609(__this, method) ((  Il2CppObject * (*) (Enumerator_t1571140176 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,TargetMgr/FightPoint>::MoveNext()
#define Enumerator_MoveNext_m3888985587(__this, method) ((  bool (*) (Enumerator_t1571140176 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,TargetMgr/FightPoint>::get_Current()
#define Enumerator_get_Current_m3733288925(__this, method) ((  KeyValuePair_2_t152597490  (*) (Enumerator_t1571140176 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,TargetMgr/FightPoint>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1618363904(__this, method) ((  CombatEntity_t684137495 * (*) (Enumerator_t1571140176 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,TargetMgr/FightPoint>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m721327396(__this, method) ((  FightPoint_t4216057832 * (*) (Enumerator_t1571140176 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,TargetMgr/FightPoint>::Reset()
#define Enumerator_Reset_m3829921344(__this, method) ((  void (*) (Enumerator_t1571140176 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,TargetMgr/FightPoint>::VerifyState()
#define Enumerator_VerifyState_m4221146697(__this, method) ((  void (*) (Enumerator_t1571140176 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,TargetMgr/FightPoint>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m522752881(__this, method) ((  void (*) (Enumerator_t1571140176 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,TargetMgr/FightPoint>::Dispose()
#define Enumerator_Dispose_m2290867984(__this, method) ((  void (*) (Enumerator_t1571140176 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
