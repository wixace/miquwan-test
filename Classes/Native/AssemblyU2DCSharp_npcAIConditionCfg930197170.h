﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// npcAIConditionCfg
struct  npcAIConditionCfg_t930197170  : public CsCfgBase_t69924517
{
public:
	// System.Int32 npcAIConditionCfg::id
	int32_t ___id_0;
	// System.Int32 npcAIConditionCfg::target
	int32_t ___target_1;
	// System.Int32 npcAIConditionCfg::eventtype
	int32_t ___eventtype_2;
	// System.Int32 npcAIConditionCfg::type
	int32_t ___type_3;
	// System.Int32 npcAIConditionCfg::param1
	int32_t ___param1_4;
	// System.Int32 npcAIConditionCfg::param2
	int32_t ___param2_5;
	// System.Int32 npcAIConditionCfg::param3
	int32_t ___param3_6;
	// System.Int32 npcAIConditionCfg::param4
	int32_t ___param4_7;
	// System.Int32 npcAIConditionCfg::param5
	int32_t ___param5_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(npcAIConditionCfg_t930197170, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(npcAIConditionCfg_t930197170, ___target_1)); }
	inline int32_t get_target_1() const { return ___target_1; }
	inline int32_t* get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(int32_t value)
	{
		___target_1 = value;
	}

	inline static int32_t get_offset_of_eventtype_2() { return static_cast<int32_t>(offsetof(npcAIConditionCfg_t930197170, ___eventtype_2)); }
	inline int32_t get_eventtype_2() const { return ___eventtype_2; }
	inline int32_t* get_address_of_eventtype_2() { return &___eventtype_2; }
	inline void set_eventtype_2(int32_t value)
	{
		___eventtype_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(npcAIConditionCfg_t930197170, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_param1_4() { return static_cast<int32_t>(offsetof(npcAIConditionCfg_t930197170, ___param1_4)); }
	inline int32_t get_param1_4() const { return ___param1_4; }
	inline int32_t* get_address_of_param1_4() { return &___param1_4; }
	inline void set_param1_4(int32_t value)
	{
		___param1_4 = value;
	}

	inline static int32_t get_offset_of_param2_5() { return static_cast<int32_t>(offsetof(npcAIConditionCfg_t930197170, ___param2_5)); }
	inline int32_t get_param2_5() const { return ___param2_5; }
	inline int32_t* get_address_of_param2_5() { return &___param2_5; }
	inline void set_param2_5(int32_t value)
	{
		___param2_5 = value;
	}

	inline static int32_t get_offset_of_param3_6() { return static_cast<int32_t>(offsetof(npcAIConditionCfg_t930197170, ___param3_6)); }
	inline int32_t get_param3_6() const { return ___param3_6; }
	inline int32_t* get_address_of_param3_6() { return &___param3_6; }
	inline void set_param3_6(int32_t value)
	{
		___param3_6 = value;
	}

	inline static int32_t get_offset_of_param4_7() { return static_cast<int32_t>(offsetof(npcAIConditionCfg_t930197170, ___param4_7)); }
	inline int32_t get_param4_7() const { return ___param4_7; }
	inline int32_t* get_address_of_param4_7() { return &___param4_7; }
	inline void set_param4_7(int32_t value)
	{
		___param4_7 = value;
	}

	inline static int32_t get_offset_of_param5_8() { return static_cast<int32_t>(offsetof(npcAIConditionCfg_t930197170, ___param5_8)); }
	inline int32_t get_param5_8() const { return ___param5_8; }
	inline int32_t* get_address_of_param5_8() { return &___param5_8; }
	inline void set_param5_8(int32_t value)
	{
		___param5_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
