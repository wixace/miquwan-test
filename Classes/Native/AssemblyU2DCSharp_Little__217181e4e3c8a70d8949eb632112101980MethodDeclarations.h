﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._217181e4e3c8a70d8949eb63f65948eb
struct _217181e4e3c8a70d8949eb63f65948eb_t2112101980;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._217181e4e3c8a70d8949eb63f65948eb::.ctor()
extern "C"  void _217181e4e3c8a70d8949eb63f65948eb__ctor_m67465041 (_217181e4e3c8a70d8949eb63f65948eb_t2112101980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._217181e4e3c8a70d8949eb63f65948eb::_217181e4e3c8a70d8949eb63f65948ebm2(System.Int32)
extern "C"  int32_t _217181e4e3c8a70d8949eb63f65948eb__217181e4e3c8a70d8949eb63f65948ebm2_m2130547737 (_217181e4e3c8a70d8949eb63f65948eb_t2112101980 * __this, int32_t ____217181e4e3c8a70d8949eb63f65948eba0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._217181e4e3c8a70d8949eb63f65948eb::_217181e4e3c8a70d8949eb63f65948ebm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _217181e4e3c8a70d8949eb63f65948eb__217181e4e3c8a70d8949eb63f65948ebm_m2112283325 (_217181e4e3c8a70d8949eb63f65948eb_t2112101980 * __this, int32_t ____217181e4e3c8a70d8949eb63f65948eba0, int32_t ____217181e4e3c8a70d8949eb63f65948eb71, int32_t ____217181e4e3c8a70d8949eb63f65948ebc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
