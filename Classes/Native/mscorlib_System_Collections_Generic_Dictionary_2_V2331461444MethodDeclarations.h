﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Object>
struct ValueCollection_t2331461444;
// System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>
struct Dictionary_2_t3630855731;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1562689139.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1378915496_gshared (ValueCollection_t2331461444 * __this, Dictionary_2_t3630855731 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1378915496(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2331461444 *, Dictionary_2_t3630855731 *, const MethodInfo*))ValueCollection__ctor_m1378915496_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3516166794_gshared (ValueCollection_t2331461444 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3516166794(__this, ___item0, method) ((  void (*) (ValueCollection_t2331461444 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3516166794_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3513594963_gshared (ValueCollection_t2331461444 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3513594963(__this, method) ((  void (*) (ValueCollection_t2331461444 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3513594963_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1298725212_gshared (ValueCollection_t2331461444 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1298725212(__this, ___item0, method) ((  bool (*) (ValueCollection_t2331461444 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1298725212_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1302903041_gshared (ValueCollection_t2331461444 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1302903041(__this, ___item0, method) ((  bool (*) (ValueCollection_t2331461444 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1302903041_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m506325281_gshared (ValueCollection_t2331461444 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m506325281(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2331461444 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m506325281_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3017329047_gshared (ValueCollection_t2331461444 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3017329047(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2331461444 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3017329047_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m157092562_gshared (ValueCollection_t2331461444 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m157092562(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2331461444 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m157092562_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3868868367_gshared (ValueCollection_t2331461444 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3868868367(__this, method) ((  bool (*) (ValueCollection_t2331461444 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3868868367_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3081986927_gshared (ValueCollection_t2331461444 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3081986927(__this, method) ((  bool (*) (ValueCollection_t2331461444 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3081986927_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2148847707_gshared (ValueCollection_t2331461444 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2148847707(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2331461444 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2148847707_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m2863754991_gshared (ValueCollection_t2331461444 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m2863754991(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2331461444 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2863754991_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1562689139  ValueCollection_GetEnumerator_m1092043858_gshared (ValueCollection_t2331461444 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1092043858(__this, method) ((  Enumerator_t1562689139  (*) (ValueCollection_t2331461444 *, const MethodInfo*))ValueCollection_GetEnumerator_m1092043858_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1269644853_gshared (ValueCollection_t2331461444 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1269644853(__this, method) ((  int32_t (*) (ValueCollection_t2331461444 *, const MethodInfo*))ValueCollection_get_Count_m1269644853_gshared)(__this, method)
