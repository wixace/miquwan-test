﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BlurEffect
struct BlurEffect_t197757176;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void BlurEffect::.ctor()
extern "C"  void BlurEffect__ctor_m3377079383 (BlurEffect_t197757176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlurEffect::.cctor()
extern "C"  void BlurEffect__cctor_m1128149558 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material BlurEffect::get_material()
extern "C"  Material_t3870600107 * BlurEffect_get_material_m2440803492 (BlurEffect_t197757176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlurEffect::OnDisable()
extern "C"  void BlurEffect_OnDisable_m577336638 (BlurEffect_t197757176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlurEffect::Start()
extern "C"  void BlurEffect_Start_m2324217175 (BlurEffect_t197757176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlurEffect::FourTapCone(UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Int32)
extern "C"  void BlurEffect_FourTapCone_m3282173034 (BlurEffect_t197757176 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___dest1, int32_t ___iteration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlurEffect::DownSample4x(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void BlurEffect_DownSample4x_m2331705765 (BlurEffect_t197757176 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlurEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void BlurEffect_OnRenderImage_m1598977447 (BlurEffect_t197757176 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
