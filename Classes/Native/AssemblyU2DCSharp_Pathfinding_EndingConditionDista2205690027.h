﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Pathfinding_PathEndingCondition3410840753.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.EndingConditionDistance
struct  EndingConditionDistance_t2205690027  : public PathEndingCondition_t3410840753
{
public:
	// System.Int32 Pathfinding.EndingConditionDistance::maxGScore
	int32_t ___maxGScore_1;

public:
	inline static int32_t get_offset_of_maxGScore_1() { return static_cast<int32_t>(offsetof(EndingConditionDistance_t2205690027, ___maxGScore_1)); }
	inline int32_t get_maxGScore_1() const { return ___maxGScore_1; }
	inline int32_t* get_address_of_maxGScore_1() { return &___maxGScore_1; }
	inline void set_maxGScore_1(int32_t value)
	{
		___maxGScore_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
