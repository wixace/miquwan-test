﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSCLevelMonsterConfig
struct JSCLevelMonsterConfig_t1924079698;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "codegen/il2cpp-codegen.h"

// System.Void JSCLevelMonsterConfig::.ctor()
extern "C"  void JSCLevelMonsterConfig__ctor_m3544323417 (JSCLevelMonsterConfig_t1924079698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension JSCLevelMonsterConfig::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * JSCLevelMonsterConfig_ProtoBuf_IExtensible_GetExtensionObject_m1028388189 (JSCLevelMonsterConfig_t1924079698 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSCLevelMonsterConfig::get_id()
extern "C"  int32_t JSCLevelMonsterConfig_get_id_m1506244609 (JSCLevelMonsterConfig_t1924079698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCLevelMonsterConfig::set_id(System.Int32)
extern "C"  void JSCLevelMonsterConfig_set_id_m1206968084 (JSCLevelMonsterConfig_t1924079698 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSCLevelMonsterConfig::get_x()
extern "C"  float JSCLevelMonsterConfig_get_x_m2313517170 (JSCLevelMonsterConfig_t1924079698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCLevelMonsterConfig::set_x(System.Single)
extern "C"  void JSCLevelMonsterConfig_set_x_m3418817337 (JSCLevelMonsterConfig_t1924079698 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSCLevelMonsterConfig::get_y()
extern "C"  float JSCLevelMonsterConfig_get_y_m2313518131 (JSCLevelMonsterConfig_t1924079698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCLevelMonsterConfig::set_y(System.Single)
extern "C"  void JSCLevelMonsterConfig_set_y_m2908283160 (JSCLevelMonsterConfig_t1924079698 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSCLevelMonsterConfig::get_z()
extern "C"  float JSCLevelMonsterConfig_get_z_m2313519092 (JSCLevelMonsterConfig_t1924079698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCLevelMonsterConfig::set_z(System.Single)
extern "C"  void JSCLevelMonsterConfig_set_z_m2397748983 (JSCLevelMonsterConfig_t1924079698 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSCLevelMonsterConfig::get_rot()
extern "C"  float JSCLevelMonsterConfig_get_rot_m2788555729 (JSCLevelMonsterConfig_t1924079698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCLevelMonsterConfig::set_rot(System.Single)
extern "C"  void JSCLevelMonsterConfig_set_rot_m1393033786 (JSCLevelMonsterConfig_t1924079698 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSCLevelMonsterConfig::get_lv()
extern "C"  int32_t JSCLevelMonsterConfig_get_lv_m1506351280 (JSCLevelMonsterConfig_t1924079698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCLevelMonsterConfig::set_lv(System.Int32)
extern "C"  void JSCLevelMonsterConfig_set_lv_m1041494339 (JSCLevelMonsterConfig_t1924079698 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
