﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.ProtoAfterDeserializationAttribute
struct ProtoAfterDeserializationAttribute_t2275714701;

#include "codegen/il2cpp-codegen.h"

// System.Void ProtoBuf.ProtoAfterDeserializationAttribute::.ctor()
extern "C"  void ProtoAfterDeserializationAttribute__ctor_m1780311879 (ProtoAfterDeserializationAttribute_t2275714701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
