﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSComponentUtil
struct JSComponentUtil_t3383427030;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void JSComponentUtil::.ctor()
extern "C"  void JSComponentUtil__ctor_m2765109333 (JSComponentUtil_t3383427030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponentUtil::initMemberFunction()
extern "C"  void JSComponentUtil_initMemberFunction_m1877006065 (JSComponentUtil_t3383427030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSComponentUtil::IsInheritanceRel(System.String,System.String)
extern "C"  bool JSComponentUtil_IsInheritanceRel_m2597991466 (JSComponentUtil_t3383427030 * __this, String_t* ___baseClassName0, String_t* ___subClassName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSComponentUtil::ilo_getObjFunction1(System.Int32,System.String)
extern "C"  int32_t JSComponentUtil_ilo_getObjFunction1_m1827670195 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___fname1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
