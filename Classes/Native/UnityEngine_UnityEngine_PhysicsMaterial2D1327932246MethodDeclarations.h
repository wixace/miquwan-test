﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.PhysicsMaterial2D
struct PhysicsMaterial2D_t1327932246;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_PhysicsMaterial2D1327932246.h"

// System.Void UnityEngine.PhysicsMaterial2D::.ctor()
extern "C"  void PhysicsMaterial2D__ctor_m1464354681 (PhysicsMaterial2D_t1327932246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PhysicsMaterial2D::.ctor(System.String)
extern "C"  void PhysicsMaterial2D__ctor_m3638101033 (PhysicsMaterial2D_t1327932246 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PhysicsMaterial2D::Internal_Create(UnityEngine.PhysicsMaterial2D,System.String)
extern "C"  void PhysicsMaterial2D_Internal_Create_m2465813680 (Il2CppObject * __this /* static, unused */, PhysicsMaterial2D_t1327932246 * ___mat0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.PhysicsMaterial2D::get_bounciness()
extern "C"  float PhysicsMaterial2D_get_bounciness_m706982379 (PhysicsMaterial2D_t1327932246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PhysicsMaterial2D::set_bounciness(System.Single)
extern "C"  void PhysicsMaterial2D_set_bounciness_m3099314848 (PhysicsMaterial2D_t1327932246 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.PhysicsMaterial2D::get_friction()
extern "C"  float PhysicsMaterial2D_get_friction_m3231791618 (PhysicsMaterial2D_t1327932246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PhysicsMaterial2D::set_friction(System.Single)
extern "C"  void PhysicsMaterial2D_set_friction_m3453329385 (PhysicsMaterial2D_t1327932246 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
