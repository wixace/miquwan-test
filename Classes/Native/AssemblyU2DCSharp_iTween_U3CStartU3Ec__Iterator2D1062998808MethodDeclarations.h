﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iTween/<Start>c__Iterator2D
struct U3CStartU3Ec__Iterator2D_t1062998808;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void iTween/<Start>c__Iterator2D::.ctor()
extern "C"  void U3CStartU3Ec__Iterator2D__ctor_m593793875 (U3CStartU3Ec__Iterator2D_t1062998808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<Start>c__Iterator2D::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator2D_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m350731103 (U3CStartU3Ec__Iterator2D_t1062998808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<Start>c__Iterator2D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator2D_System_Collections_IEnumerator_get_Current_m3571604723 (U3CStartU3Ec__Iterator2D_t1062998808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTween/<Start>c__Iterator2D::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator2D_MoveNext_m2016455617 (U3CStartU3Ec__Iterator2D_t1062998808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<Start>c__Iterator2D::Dispose()
extern "C"  void U3CStartU3Ec__Iterator2D_Dispose_m3598513872 (U3CStartU3Ec__Iterator2D_t1062998808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<Start>c__Iterator2D::Reset()
extern "C"  void U3CStartU3Ec__Iterator2D_Reset_m2535194112 (U3CStartU3Ec__Iterator2D_t1062998808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
