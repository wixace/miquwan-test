﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Util.TileHandler/<ClearTile>c__AnonStorey121
struct U3CClearTileU3Ec__AnonStorey121_t3965712627;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Util.TileHandler/<ClearTile>c__AnonStorey121::.ctor()
extern "C"  void U3CClearTileU3Ec__AnonStorey121__ctor_m3585120008 (U3CClearTileU3Ec__AnonStorey121_t3965712627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Util.TileHandler/<ClearTile>c__AnonStorey121::<>m__357(System.Boolean)
extern "C"  bool U3CClearTileU3Ec__AnonStorey121_U3CU3Em__357_m535041457 (U3CClearTileU3Ec__AnonStorey121_t3965712627 * __this, bool ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
