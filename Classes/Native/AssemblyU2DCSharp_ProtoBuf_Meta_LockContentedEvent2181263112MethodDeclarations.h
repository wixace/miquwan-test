﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.LockContentedEventArgs
struct LockContentedEventArgs_t2181263112;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ProtoBuf.Meta.LockContentedEventArgs::.ctor(System.String)
extern "C"  void LockContentedEventArgs__ctor_m3165138327 (LockContentedEventArgs_t2181263112 * __this, String_t* ___ownerStackTrace0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.Meta.LockContentedEventArgs::get_OwnerStackTrace()
extern "C"  String_t* LockContentedEventArgs_get_OwnerStackTrace_m3138713709 (LockContentedEventArgs_t2181263112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
