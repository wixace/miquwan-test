﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.NavGraph
struct NavGraph_t1254319713;
// Pathfinding.NNConstraint
struct NNConstraint_t758567699;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// Pathfinding.PathHandler
struct PathHandler_t918952263;
// Pathfinding.Serialization.GraphSerializationContext
struct GraphSerializationContext_t3256954663;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.GraphNodeDelegateCancelable
struct GraphNodeDelegateCancelable_t3591372971;
// AstarPath
struct AstarPath_t4090270936;
// OnGraphDelegate
struct OnGraphDelegate_t381382964;
// Pathfinding.PathNode
struct PathNode_t417131581;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_Guid3584625871.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "AssemblyU2DCSharp_Pathfinding_NNInfo1570625892.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_NNConstraint758567699.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_Pathfinding_PathHandler918952263.h"
#include "AssemblyU2DCSharp_Pathfinding_Serialization_GraphS3256954663.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Pathfinding_NavGraph1254319713.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNodeDelegateCan3591372971.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_OnGraphDelegate381382964.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"

// System.Void Pathfinding.NavGraph::.ctor()
extern "C"  void NavGraph__ctor_m3490449702 (NavGraph_t1254319713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Util.Guid Pathfinding.NavGraph::get_guid()
extern "C"  Guid_t3584625871  NavGraph_get_guid_m521667096 (NavGraph_t1254319713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavGraph::set_guid(Pathfinding.Util.Guid)
extern "C"  void NavGraph_set_guid_m3323292131 (NavGraph_t1254319713 * __this, Guid_t3584625871  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.NavGraph::CountNodes()
extern "C"  int32_t NavGraph_CountNodes_m534904462 (NavGraph_t1254319713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavGraph::SetMatrix(UnityEngine.Matrix4x4)
extern "C"  void NavGraph_SetMatrix_m3348298771 (NavGraph_t1254319713 * __this, Matrix4x4_t1651859333  ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavGraph::CreateNodes(System.Int32)
extern "C"  void NavGraph_CreateNodes_m2213033386 (NavGraph_t1254319713 * __this, int32_t ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavGraph::RelocateNodes(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C"  void NavGraph_RelocateNodes_m3621416958 (NavGraph_t1254319713 * __this, Matrix4x4_t1651859333  ___oldMatrix0, Matrix4x4_t1651859333  ___newMatrix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.NavGraph::GetNearest(UnityEngine.Vector3)
extern "C"  NNInfo_t1570625892  NavGraph_GetNearest_m3100844558 (NavGraph_t1254319713 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.NavGraph::GetNearest(UnityEngine.Vector3,Pathfinding.NNConstraint)
extern "C"  NNInfo_t1570625892  NavGraph_GetNearest_m3442751057 (NavGraph_t1254319713 * __this, Vector3_t4282066566  ___position0, NNConstraint_t758567699 * ___constraint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.NavGraph::GetNearest(UnityEngine.Vector3,Pathfinding.NNConstraint,Pathfinding.GraphNode)
extern "C"  NNInfo_t1570625892  NavGraph_GetNearest_m3036295607 (NavGraph_t1254319713 * __this, Vector3_t4282066566  ___position0, NNConstraint_t758567699 * ___constraint1, GraphNode_t23612370 * ___hint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.NavGraph::GetNearestForce(UnityEngine.Vector3,Pathfinding.NNConstraint)
extern "C"  NNInfo_t1570625892  NavGraph_GetNearestForce_m2355761224 (NavGraph_t1254319713 * __this, Vector3_t4282066566  ___position0, NNConstraint_t758567699 * ___constraint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavGraph::Awake()
extern "C"  void NavGraph_Awake_m3728054921 (NavGraph_t1254319713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavGraph::SafeOnDestroy()
extern "C"  void NavGraph_SafeOnDestroy_m4004765650 (NavGraph_t1254319713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavGraph::OnDestroy()
extern "C"  void NavGraph_OnDestroy_m4207467295 (NavGraph_t1254319713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavGraph::ScanGraph()
extern "C"  void NavGraph_ScanGraph_m636728661 (NavGraph_t1254319713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavGraph::Scan()
extern "C"  void NavGraph_Scan_m1864041115 (NavGraph_t1254319713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavGraph::ScanInternal()
extern "C"  void NavGraph_ScanInternal_m2683547704 (NavGraph_t1254319713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Pathfinding.NavGraph::NodeColor(Pathfinding.GraphNode,Pathfinding.PathHandler)
extern "C"  Color_t4194546905  NavGraph_NodeColor_m1429109407 (NavGraph_t1254319713 * __this, GraphNode_t23612370 * ___node0, PathHandler_t918952263 * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavGraph::SerializeExtraInfo(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void NavGraph_SerializeExtraInfo_m3631511057 (NavGraph_t1254319713 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavGraph::DeserializeExtraInfo(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void NavGraph_DeserializeExtraInfo_m3806644656 (NavGraph_t1254319713 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavGraph::PostDeserialization()
extern "C"  void NavGraph_PostDeserialization_m3237833107 (NavGraph_t1254319713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavGraph::InSearchTree(Pathfinding.GraphNode,Pathfinding.Path)
extern "C"  bool NavGraph_InSearchTree_m2369252776 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ___node0, Path_t1974241691 * ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavGraph::OnDrawGizmos(System.Boolean)
extern "C"  void NavGraph_OnDrawGizmos_m2034866481 (NavGraph_t1254319713 * __this, bool ___drawNodes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavGraph::<OnDestroy>m__349(Pathfinding.GraphNode)
extern "C"  bool NavGraph_U3COnDestroyU3Em__349_m2580026770 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Util.Guid Pathfinding.NavGraph::ilo_NewGuid1()
extern "C"  Guid_t3584625871  NavGraph_ilo_NewGuid1_m726906849 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavGraph::ilo_GetNodes2(Pathfinding.NavGraph,Pathfinding.GraphNodeDelegateCancelable)
extern "C"  void NavGraph_ilo_GetNodes2_m3273648832 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ____this0, GraphNodeDelegateCancelable_t3591372971 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.NavGraph::ilo_GetNearest3(Pathfinding.NavGraph,UnityEngine.Vector3,Pathfinding.NNConstraint)
extern "C"  NNInfo_t1570625892  NavGraph_ilo_GetNearest3_m2262698666 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ____this0, Vector3_t4282066566  ___position1, NNConstraint_t758567699 * ___constraint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.NavGraph::ilo_get_maxNearestNodeDistanceSqr4(AstarPath)
extern "C"  float NavGraph_ilo_get_maxNearestNodeDistanceSqr4_m3729305507 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavGraph::ilo_Invoke5(OnGraphDelegate,Pathfinding.NavGraph)
extern "C"  void NavGraph_ilo_Invoke5_m1113180993 (Il2CppObject * __this /* static, unused */, OnGraphDelegate_t381382964 * ____this0, NavGraph_t1254319713 * ___graph1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.NavGraph::ilo_get_Area6(Pathfinding.GraphNode)
extern "C"  uint32_t NavGraph_ilo_get_Area6_m3292528022 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Pathfinding.NavGraph::ilo_GetAreaColor7(System.UInt32)
extern "C"  Color_t4194546905  NavGraph_ilo_GetAreaColor7_m2153270649 (Il2CppObject * __this /* static, unused */, uint32_t ___area0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.NavGraph::ilo_get_H8(Pathfinding.PathNode)
extern "C"  uint32_t NavGraph_ilo_get_H8_m3201052532 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathNode Pathfinding.NavGraph::ilo_GetPathNode9(Pathfinding.PathHandler,Pathfinding.GraphNode)
extern "C"  PathNode_t417131581 * NavGraph_ilo_GetPathNode9_m359349380 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
