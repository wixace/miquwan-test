﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlowControl.FlowContainerLine
struct FlowContainerLine_t4203559837;
// FlowControl.FlowContainerBase
struct FlowContainerBase_t4203254394;
// FlowControl.IFlowBase
struct IFlowBase_t1464761278;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FlowControl_FlowContainerBase4203254394.h"

// System.Void FlowControl.FlowContainerLine::.ctor()
extern "C"  void FlowContainerLine__ctor_m3765628163 (FlowContainerLine_t4203559837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowContainerLine::.cctor()
extern "C"  void FlowContainerLine__cctor_m288259850 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowContainerLine::Activate()
extern "C"  void FlowContainerLine_Activate_m1278445588 (FlowContainerLine_t4203559837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowContainerLine::FlowUpdate(System.Single)
extern "C"  void FlowContainerLine_FlowUpdate_m2930614003 (FlowContainerLine_t4203559837 * __this, float ___duringTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FlowControl.FlowContainerLine::ilo_get_NumChildren1(FlowControl.FlowContainerBase)
extern "C"  int32_t FlowContainerLine_ilo_get_NumChildren1_m618355122 (Il2CppObject * __this /* static, unused */, FlowContainerBase_t4203254394 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowContainerLine::ilo_Activate2(FlowControl.IFlowBase)
extern "C"  void FlowContainerLine_ilo_Activate2_m613885564 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FlowControl.FlowContainerLine::ilo_get_isComplete3(FlowControl.IFlowBase)
extern "C"  bool FlowContainerLine_ilo_get_isComplete3_m2391042016 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
