﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_xoubowcow174
struct M_xoubowcow174_t2902347359;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_xoubowcow1742902347359.h"

// System.Void GarbageiOS.M_xoubowcow174::.ctor()
extern "C"  void M_xoubowcow174__ctor_m3396837044 (M_xoubowcow174_t2902347359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xoubowcow174::M_mozallpa0(System.String[],System.Int32)
extern "C"  void M_xoubowcow174_M_mozallpa0_m3593646893 (M_xoubowcow174_t2902347359 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xoubowcow174::M_chanemay1(System.String[],System.Int32)
extern "C"  void M_xoubowcow174_M_chanemay1_m901135990 (M_xoubowcow174_t2902347359 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xoubowcow174::M_nearlee2(System.String[],System.Int32)
extern "C"  void M_xoubowcow174_M_nearlee2_m1767695207 (M_xoubowcow174_t2902347359 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xoubowcow174::M_zabayCusowra3(System.String[],System.Int32)
extern "C"  void M_xoubowcow174_M_zabayCusowra3_m343793029 (M_xoubowcow174_t2902347359 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xoubowcow174::M_suhas4(System.String[],System.Int32)
extern "C"  void M_xoubowcow174_M_suhas4_m1028705685 (M_xoubowcow174_t2902347359 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xoubowcow174::ilo_M_mozallpa01(GarbageiOS.M_xoubowcow174,System.String[],System.Int32)
extern "C"  void M_xoubowcow174_ilo_M_mozallpa01_m226149170 (Il2CppObject * __this /* static, unused */, M_xoubowcow174_t2902347359 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_xoubowcow174::ilo_M_nearlee22(GarbageiOS.M_xoubowcow174,System.String[],System.Int32)
extern "C"  void M_xoubowcow174_ilo_M_nearlee22_m2653506291 (Il2CppObject * __this /* static, unused */, M_xoubowcow174_t2902347359 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
