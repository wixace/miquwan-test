﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssetGOListGenerated
struct AssetGOListGenerated_t1021691321;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void AssetGOListGenerated::.ctor()
extern "C"  void AssetGOListGenerated__ctor_m2567107842 (AssetGOListGenerated_t1021691321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssetGOListGenerated::AssetGOList_AssetGOList1(JSVCall,System.Int32)
extern "C"  bool AssetGOListGenerated_AssetGOList_AssetGOList1_m1690860618 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetGOListGenerated::AssetGOList_goList(JSVCall)
extern "C"  void AssetGOListGenerated_AssetGOList_goList_m2124420246 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetGOListGenerated::__Register()
extern "C"  void AssetGOListGenerated___Register_m882465029 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject[] AssetGOListGenerated::<AssetGOList_goList>m__1()
extern "C"  GameObjectU5BU5D_t2662109048* AssetGOListGenerated_U3CAssetGOList_goListU3Em__1_m2653747212 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetGOListGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void AssetGOListGenerated_ilo_addJSCSRel1_m3528176318 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AssetGOListGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t AssetGOListGenerated_ilo_setObject2_m2910177801 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetGOListGenerated::ilo_moveSaveID2Arr3(System.Int32)
extern "C"  void AssetGOListGenerated_ilo_moveSaveID2Arr3_m1998002457 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetGOListGenerated::ilo_setArrayS4(System.Int32,System.Int32,System.Boolean)
extern "C"  void AssetGOListGenerated_ilo_setArrayS4_m3190756484 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AssetGOListGenerated::ilo_getObject5(System.Int32)
extern "C"  int32_t AssetGOListGenerated_ilo_getObject5_m3222335828 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AssetGOListGenerated::ilo_getElement6(System.Int32,System.Int32)
extern "C"  int32_t AssetGOListGenerated_ilo_getElement6_m1754839415 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
