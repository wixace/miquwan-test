﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSFightforData
struct CSFightforData_t215334547;
// System.String
struct String_t;
// System.Collections.Generic.List`1<CSPlusAtt>
struct List_1_t341533415;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"

// System.Void CSFightforData::.ctor()
extern "C"  void CSFightforData__ctor_m3238118248 (CSFightforData_t215334547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSFightforData::LoadData(System.String)
extern "C"  void CSFightforData_LoadData_m3690635638 (CSFightforData_t215334547 * __this, String_t* ___jsonStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSFightforData::Clear()
extern "C"  void CSFightforData_Clear_m644251539 (CSFightforData_t215334547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSFightforData::UnLoadData()
extern "C"  void CSFightforData_UnLoadData_m1143219653 (CSFightforData_t215334547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSFightforData::GetCountryName()
extern "C"  String_t* CSFightforData_GetCountryName_m3535466634 (CSFightforData_t215334547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPlusAtt> CSFightforData::GetFightForPlusAtt()
extern "C"  List_1_t341533415 * CSFightforData_GetFightForPlusAtt_m4248249932 (CSFightforData_t215334547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPlusAtt> CSFightforData::GetFightForOtherPlusAtt()
extern "C"  List_1_t341533415 * CSFightforData_GetFightForOtherPlusAtt_m627746516 (CSFightforData_t215334547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSFightforData::ilo_Log1(System.Object,System.Boolean)
extern "C"  void CSFightforData_ilo_Log1_m2162570981 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken CSFightforData::ilo_get_Item2(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * CSFightforData_ilo_get_Item2_m2608510474 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSFightforData::ilo_FormatUIString3(System.Int32,System.Object[])
extern "C"  String_t* CSFightforData_ilo_FormatUIString3_m2770481188 (Il2CppObject * __this /* static, unused */, int32_t ___key0, ObjectU5BU5D_t1108656482* ___datas1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
