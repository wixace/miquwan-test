﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// elementCfg
struct  elementCfg_t575911880  : public CsCfgBase_t69924517
{
public:
	// System.Int32 elementCfg::id
	int32_t ___id_0;
	// System.String elementCfg::icon
	String_t* ___icon_1;
	// System.Single elementCfg::modulus1
	float ___modulus1_2;
	// System.Single elementCfg::modulus2
	float ___modulus2_3;
	// System.Single elementCfg::modulus3
	float ___modulus3_4;
	// System.Single elementCfg::modulus4
	float ___modulus4_5;
	// System.Single elementCfg::modulus5
	float ___modulus5_6;
	// System.Single elementCfg::modulus6
	float ___modulus6_7;
	// System.Single elementCfg::modulus7
	float ___modulus7_8;
	// System.Single elementCfg::modulus8
	float ___modulus8_9;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(elementCfg_t575911880, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_icon_1() { return static_cast<int32_t>(offsetof(elementCfg_t575911880, ___icon_1)); }
	inline String_t* get_icon_1() const { return ___icon_1; }
	inline String_t** get_address_of_icon_1() { return &___icon_1; }
	inline void set_icon_1(String_t* value)
	{
		___icon_1 = value;
		Il2CppCodeGenWriteBarrier(&___icon_1, value);
	}

	inline static int32_t get_offset_of_modulus1_2() { return static_cast<int32_t>(offsetof(elementCfg_t575911880, ___modulus1_2)); }
	inline float get_modulus1_2() const { return ___modulus1_2; }
	inline float* get_address_of_modulus1_2() { return &___modulus1_2; }
	inline void set_modulus1_2(float value)
	{
		___modulus1_2 = value;
	}

	inline static int32_t get_offset_of_modulus2_3() { return static_cast<int32_t>(offsetof(elementCfg_t575911880, ___modulus2_3)); }
	inline float get_modulus2_3() const { return ___modulus2_3; }
	inline float* get_address_of_modulus2_3() { return &___modulus2_3; }
	inline void set_modulus2_3(float value)
	{
		___modulus2_3 = value;
	}

	inline static int32_t get_offset_of_modulus3_4() { return static_cast<int32_t>(offsetof(elementCfg_t575911880, ___modulus3_4)); }
	inline float get_modulus3_4() const { return ___modulus3_4; }
	inline float* get_address_of_modulus3_4() { return &___modulus3_4; }
	inline void set_modulus3_4(float value)
	{
		___modulus3_4 = value;
	}

	inline static int32_t get_offset_of_modulus4_5() { return static_cast<int32_t>(offsetof(elementCfg_t575911880, ___modulus4_5)); }
	inline float get_modulus4_5() const { return ___modulus4_5; }
	inline float* get_address_of_modulus4_5() { return &___modulus4_5; }
	inline void set_modulus4_5(float value)
	{
		___modulus4_5 = value;
	}

	inline static int32_t get_offset_of_modulus5_6() { return static_cast<int32_t>(offsetof(elementCfg_t575911880, ___modulus5_6)); }
	inline float get_modulus5_6() const { return ___modulus5_6; }
	inline float* get_address_of_modulus5_6() { return &___modulus5_6; }
	inline void set_modulus5_6(float value)
	{
		___modulus5_6 = value;
	}

	inline static int32_t get_offset_of_modulus6_7() { return static_cast<int32_t>(offsetof(elementCfg_t575911880, ___modulus6_7)); }
	inline float get_modulus6_7() const { return ___modulus6_7; }
	inline float* get_address_of_modulus6_7() { return &___modulus6_7; }
	inline void set_modulus6_7(float value)
	{
		___modulus6_7 = value;
	}

	inline static int32_t get_offset_of_modulus7_8() { return static_cast<int32_t>(offsetof(elementCfg_t575911880, ___modulus7_8)); }
	inline float get_modulus7_8() const { return ___modulus7_8; }
	inline float* get_address_of_modulus7_8() { return &___modulus7_8; }
	inline void set_modulus7_8(float value)
	{
		___modulus7_8 = value;
	}

	inline static int32_t get_offset_of_modulus8_9() { return static_cast<int32_t>(offsetof(elementCfg_t575911880, ___modulus8_9)); }
	inline float get_modulus8_9() const { return ___modulus8_9; }
	inline float* get_address_of_modulus8_9() { return &___modulus8_9; }
	inline void set_modulus8_9(float value)
	{
		___modulus8_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
