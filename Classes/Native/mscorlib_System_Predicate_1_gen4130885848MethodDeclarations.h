﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen3781873254MethodDeclarations.h"

// System.Void System.Predicate`1<CurCameraShotCfg>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m2452080822(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t4130885848 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m231416842_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<CurCameraShotCfg>::Invoke(T)
#define Predicate_1_Invoke_m2217324428(__this, ___obj0, method) ((  bool (*) (Predicate_1_t4130885848 *, CurCameraShotCfg_t224861669 *, const MethodInfo*))Predicate_1_Invoke_m1328901537_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<CurCameraShotCfg>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m432507035(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t4130885848 *, CurCameraShotCfg_t224861669 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2038073176_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<CurCameraShotCfg>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m436208584(__this, ___result0, method) ((  bool (*) (Predicate_1_t4130885848 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3970497007_gshared)(__this, ___result0, method)
