﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VOLine>
struct List_1_t3398117353;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3417790123.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_VOLin2029931801.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/VOLine>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3001993024_gshared (Enumerator_t3417790123 * __this, List_1_t3398117353 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3001993024(__this, ___l0, method) ((  void (*) (Enumerator_t3417790123 *, List_1_t3398117353 *, const MethodInfo*))Enumerator__ctor_m3001993024_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3694183698_gshared (Enumerator_t3417790123 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3694183698(__this, method) ((  void (*) (Enumerator_t3417790123 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3694183698_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1699818814_gshared (Enumerator_t3417790123 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1699818814(__this, method) ((  Il2CppObject * (*) (Enumerator_t3417790123 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1699818814_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/VOLine>::Dispose()
extern "C"  void Enumerator_Dispose_m627123493_gshared (Enumerator_t3417790123 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m627123493(__this, method) ((  void (*) (Enumerator_t3417790123 *, const MethodInfo*))Enumerator_Dispose_m627123493_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/VOLine>::VerifyState()
extern "C"  void Enumerator_VerifyState_m25414110_gshared (Enumerator_t3417790123 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m25414110(__this, method) ((  void (*) (Enumerator_t3417790123 *, const MethodInfo*))Enumerator_VerifyState_m25414110_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/VOLine>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m209975550_gshared (Enumerator_t3417790123 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m209975550(__this, method) ((  bool (*) (Enumerator_t3417790123 *, const MethodInfo*))Enumerator_MoveNext_m209975550_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.LocalAvoidance/VOLine>::get_Current()
extern "C"  VOLine_t2029931801  Enumerator_get_Current_m2648796245_gshared (Enumerator_t3417790123 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2648796245(__this, method) ((  VOLine_t2029931801  (*) (Enumerator_t3417790123 *, const MethodInfo*))Enumerator_get_Current_m2648796245_gshared)(__this, method)
