﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// UnityEngine.Vector4
struct Vector4_t4282066567;
struct Vector4_t4282066567_marshaled_pinvoke;
struct Vector4_t4282066567_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m2441427762 (Vector4_t4282066567 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m4059400973 (Vector4_t4282066567 * __this, float ___x0, float ___y1, float ___z2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single)
extern "C"  void Vector4__ctor_m2176640552 (Vector4_t4282066567 * __this, float ___x0, float ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::get_Item(System.Int32)
extern "C"  float Vector4_get_Item_m2326091453 (Vector4_t4282066567 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::set_Item(System.Int32,System.Single)
extern "C"  void Vector4_set_Item_m922152162 (Vector4_t4282066567 * __this, int32_t ___index0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::Set(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4_Set_m1965151474 (Vector4_t4282066567 * __this, float ___new_x0, float ___new_y1, float ___new_z2, float ___new_w3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::Lerp(UnityEngine.Vector4,UnityEngine.Vector4,System.Single)
extern "C"  Vector4_t4282066567  Vector4_Lerp_m1065767861 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___a0, Vector4_t4282066567  ___b1, float ___t2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::LerpUnclamped(UnityEngine.Vector4,UnityEngine.Vector4,System.Single)
extern "C"  Vector4_t4282066567  Vector4_LerpUnclamped_m2453906086 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___a0, Vector4_t4282066567  ___b1, float ___t2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::MoveTowards(UnityEngine.Vector4,UnityEngine.Vector4,System.Single)
extern "C"  Vector4_t4282066567  Vector4_MoveTowards_m3213736293 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___current0, Vector4_t4282066567  ___target1, float ___maxDistanceDelta2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::Scale(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  Vector4_t4282066567  Vector4_Scale_m1454273633 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___a0, Vector4_t4282066567  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::Scale(UnityEngine.Vector4)
extern "C"  void Vector4_Scale_m1487064040 (Vector4_t4282066567 * __this, Vector4_t4282066567  ___scale0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector4::GetHashCode()
extern "C"  int32_t Vector4_GetHashCode_m3402333527 (Vector4_t4282066567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector4::Equals(System.Object)
extern "C"  bool Vector4_Equals_m3270185343 (Vector4_t4282066567 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::Normalize(UnityEngine.Vector4)
extern "C"  Vector4_t4282066567  Vector4_Normalize_m3233754444 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::Normalize()
extern "C"  void Vector4_Normalize_m3479425171 (Vector4_t4282066567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::get_normalized()
extern "C"  Vector4_t4282066567  Vector4_get_normalized_m883784899 (Vector4_t4282066567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Vector4::ToString()
extern "C"  String_t* Vector4_ToString_m3272970053 (Vector4_t4282066567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Vector4::ToString(System.String)
extern "C"  String_t* Vector4_ToString_m1104398301 (Vector4_t4282066567 * __this, String_t* ___format0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::Dot(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  float Vector4_Dot_m2303368623 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___a0, Vector4_t4282066567  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::Project(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  Vector4_t4282066567  Vector4_Project_m739546866 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___a0, Vector4_t4282066567  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::Distance(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  float Vector4_Distance_m2097804101 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___a0, Vector4_t4282066567  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::Magnitude(UnityEngine.Vector4)
extern "C"  float Vector4_Magnitude_m3106604758 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::get_magnitude()
extern "C"  float Vector4_get_magnitude_m4287880729 (Vector4_t4282066567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::SqrMagnitude(UnityEngine.Vector4)
extern "C"  float Vector4_SqrMagnitude_m3613076556 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::SqrMagnitude()
extern "C"  float Vector4_SqrMagnitude_m4185349164 (Vector4_t4282066567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::get_sqrMagnitude()
extern "C"  float Vector4_get_sqrMagnitude_m1418774677 (Vector4_t4282066567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::Min(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  Vector4_t4282066567  Vector4_Min_m3630166233 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___lhs0, Vector4_t4282066567  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::Max(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  Vector4_t4282066567  Vector4_Max_m4165504519 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___lhs0, Vector4_t4282066567  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::get_zero()
extern "C"  Vector4_t4282066567  Vector4_get_zero_m3835647092 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::get_one()
extern "C"  Vector4_t4282066567  Vector4_get_one_m3300413884 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Addition(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  Vector4_t4282066567  Vector4_op_Addition_m217826897 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___a0, Vector4_t4282066567  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Subtraction(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  Vector4_t4282066567  Vector4_op_Subtraction_m3588766929 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___a0, Vector4_t4282066567  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_UnaryNegation(UnityEngine.Vector4)
extern "C"  Vector4_t4282066567  Vector4_op_UnaryNegation_m560722015 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Multiply(UnityEngine.Vector4,System.Single)
extern "C"  Vector4_t4282066567  Vector4_op_Multiply_m209031836 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___a0, float ___d1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Multiply(System.Single,UnityEngine.Vector4)
extern "C"  Vector4_t4282066567  Vector4_op_Multiply_m3555003804 (Il2CppObject * __this /* static, unused */, float ___d0, Vector4_t4282066567  ___a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Division(UnityEngine.Vector4,System.Single)
extern "C"  Vector4_t4282066567  Vector4_op_Division_m3513381747 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___a0, float ___d1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector4::op_Equality(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  bool Vector4_op_Equality_m3533121638 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___lhs0, Vector4_t4282066567  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector4::op_Inequality(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  bool Vector4_op_Inequality_m3118756897 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___lhs0, Vector4_t4282066567  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector4_t4282066567  Vector4_op_Implicit_m331673271 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector4)
extern "C"  Vector3_t4282066566  Vector4_op_Implicit_m3933247893 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector4_t4282066567  Vector4_op_Implicit_m331673240 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector4)
extern "C"  Vector2_t4282066565  Vector4_op_Implicit_m3239855188 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Vector4_t4282066567;
struct Vector4_t4282066567_marshaled_pinvoke;

extern "C" void Vector4_t4282066567_marshal_pinvoke(const Vector4_t4282066567& unmarshaled, Vector4_t4282066567_marshaled_pinvoke& marshaled);
extern "C" void Vector4_t4282066567_marshal_pinvoke_back(const Vector4_t4282066567_marshaled_pinvoke& marshaled, Vector4_t4282066567& unmarshaled);
extern "C" void Vector4_t4282066567_marshal_pinvoke_cleanup(Vector4_t4282066567_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Vector4_t4282066567;
struct Vector4_t4282066567_marshaled_com;

extern "C" void Vector4_t4282066567_marshal_com(const Vector4_t4282066567& unmarshaled, Vector4_t4282066567_marshaled_com& marshaled);
extern "C" void Vector4_t4282066567_marshal_com_back(const Vector4_t4282066567_marshaled_com& marshaled, Vector4_t4282066567& unmarshaled);
extern "C" void Vector4_t4282066567_marshal_com_cleanup(Vector4_t4282066567_marshaled_com& marshaled);
