﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIScrollViewGenerated/<UIScrollView_onMomentumMove_GetDelegate_member18_arg0>c__AnonStoreyCD
struct U3CUIScrollView_onMomentumMove_GetDelegate_member18_arg0U3Ec__AnonStoreyCD_t3879399978;

#include "codegen/il2cpp-codegen.h"

// System.Void UIScrollViewGenerated/<UIScrollView_onMomentumMove_GetDelegate_member18_arg0>c__AnonStoreyCD::.ctor()
extern "C"  void U3CUIScrollView_onMomentumMove_GetDelegate_member18_arg0U3Ec__AnonStoreyCD__ctor_m700833713 (U3CUIScrollView_onMomentumMove_GetDelegate_member18_arg0U3Ec__AnonStoreyCD_t3879399978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated/<UIScrollView_onMomentumMove_GetDelegate_member18_arg0>c__AnonStoreyCD::<>m__15F()
extern "C"  void U3CUIScrollView_onMomentumMove_GetDelegate_member18_arg0U3Ec__AnonStoreyCD_U3CU3Em__15F_m3531058442 (U3CUIScrollView_onMomentumMove_GetDelegate_member18_arg0U3Ec__AnonStoreyCD_t3879399978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
