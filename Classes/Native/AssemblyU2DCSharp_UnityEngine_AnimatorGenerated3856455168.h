﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MethodID
struct MethodID_t3916401116;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine_AnimatorGenerated
struct  UnityEngine_AnimatorGenerated_t3856455168  : public Il2CppObject
{
public:

public:
};

struct UnityEngine_AnimatorGenerated_t3856455168_StaticFields
{
public:
	// MethodID UnityEngine_AnimatorGenerated::methodID14
	MethodID_t3916401116 * ___methodID14_0;
	// MethodID UnityEngine_AnimatorGenerated::methodID15
	MethodID_t3916401116 * ___methodID15_1;

public:
	inline static int32_t get_offset_of_methodID14_0() { return static_cast<int32_t>(offsetof(UnityEngine_AnimatorGenerated_t3856455168_StaticFields, ___methodID14_0)); }
	inline MethodID_t3916401116 * get_methodID14_0() const { return ___methodID14_0; }
	inline MethodID_t3916401116 ** get_address_of_methodID14_0() { return &___methodID14_0; }
	inline void set_methodID14_0(MethodID_t3916401116 * value)
	{
		___methodID14_0 = value;
		Il2CppCodeGenWriteBarrier(&___methodID14_0, value);
	}

	inline static int32_t get_offset_of_methodID15_1() { return static_cast<int32_t>(offsetof(UnityEngine_AnimatorGenerated_t3856455168_StaticFields, ___methodID15_1)); }
	inline MethodID_t3916401116 * get_methodID15_1() const { return ___methodID15_1; }
	inline MethodID_t3916401116 ** get_address_of_methodID15_1() { return &___methodID15_1; }
	inline void set_methodID15_1(MethodID_t3916401116 * value)
	{
		___methodID15_1 = value;
		Il2CppCodeGenWriteBarrier(&___methodID15_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
