﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.UnitStateBvr
struct UnitStateBvr_t181606353;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"

// System.Void Entity.Behavior.UnitStateBvr::.ctor()
extern "C"  void UnitStateBvr__ctor_m483808377 (UnitStateBvr_t181606353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.UnitStateBvr::get_id()
extern "C"  uint8_t UnitStateBvr_get_id_m9827393 (UnitStateBvr_t181606353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.UnitStateBvr::Reason()
extern "C"  void UnitStateBvr_Reason_m1664545903 (UnitStateBvr_t181606353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.UnitStateBvr::Action()
extern "C"  void UnitStateBvr_Action_m861252577 (UnitStateBvr_t181606353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.UnitStateBvr::DoEntering()
extern "C"  void UnitStateBvr_DoEntering_m3252706464 (UnitStateBvr_t181606353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.UnitStateBvr::SetParams(System.Object[])
extern "C"  void UnitStateBvr_SetParams_m4021609651 (UnitStateBvr_t181606353 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.UnitStateBvr::DoLeaving()
extern "C"  void UnitStateBvr_DoLeaving_m1935309568 (UnitStateBvr_t181606353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Entity.Behavior.UnitStateBvr::IsLockingBehavior()
extern "C"  bool UnitStateBvr_IsLockingBehavior_m836973666 (UnitStateBvr_t181606353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.UnitStateBvr::<DoLeaving>m__3C8()
extern "C"  void UnitStateBvr_U3CDoLeavingU3Em__3C8_m2592908451 (UnitStateBvr_t181606353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
