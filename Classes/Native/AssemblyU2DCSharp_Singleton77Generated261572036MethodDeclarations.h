﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Singleton77Generated
struct Singleton77Generated_t261572036;
// JSVCall
struct JSVCall_t3708497963;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t4136801618;
// System.Type
struct Type_t;
// ConstructorID
struct ConstructorID_t3348888181;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_ConstructorID3348888181.h"

// System.Void Singleton77Generated::.ctor()
extern "C"  void Singleton77Generated__ctor_m1965642071 (Singleton77Generated_t261572036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Singleton77Generated::.cctor()
extern "C"  void Singleton77Generated__cctor_m323265846 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Singleton77Generated::SingletonA1_SingletonA11(JSVCall,System.Int32)
extern "C"  bool Singleton77Generated_SingletonA1_SingletonA11_m119307531 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Singleton77Generated::SingletonA1_Instance(JSVCall)
extern "C"  void Singleton77Generated_SingletonA1_Instance_m578004225 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Singleton77Generated::__Register()
extern "C"  void Singleton77Generated___Register_m2400741840 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Singleton77Generated::ilo_getObject1(System.Int32)
extern "C"  int32_t Singleton77Generated_ilo_getObject1_m1773495067 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Singleton77Generated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool Singleton77Generated_ilo_attachFinalizerObject2_m3403161001 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo Singleton77Generated::ilo_makeGenericConstructor3(System.Type,ConstructorID)
extern "C"  ConstructorInfo_t4136801618 * Singleton77Generated_ilo_makeGenericConstructor3_m2478135611 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, ConstructorID_t3348888181 * ___constructorID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
