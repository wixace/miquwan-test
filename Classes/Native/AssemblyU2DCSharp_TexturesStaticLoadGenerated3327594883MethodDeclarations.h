﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TexturesStaticLoadGenerated
struct TexturesStaticLoadGenerated_t3327594883;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Texture
struct Texture_t2526458961;
// TexturesStaticLoad
struct TexturesStaticLoad_t2473754988;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_TexturesStaticLoad2473754988.h"
#include "mscorlib_System_String7231557.h"

// System.Void TexturesStaticLoadGenerated::.ctor()
extern "C"  void TexturesStaticLoadGenerated__ctor_m3466173768 (TexturesStaticLoadGenerated_t3327594883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TexturesStaticLoadGenerated::TexturesStaticLoad_TexturesStaticLoad1(JSVCall,System.Int32)
extern "C"  bool TexturesStaticLoadGenerated_TexturesStaticLoad_TexturesStaticLoad1_m570862570 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TexturesStaticLoadGenerated::TexturesStaticLoad_Length(JSVCall)
extern "C"  void TexturesStaticLoadGenerated_TexturesStaticLoad_Length_m3461766088 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TexturesStaticLoadGenerated::TexturesStaticLoad_AddTextures__TexturesStaticLoad(JSVCall,System.Int32)
extern "C"  bool TexturesStaticLoadGenerated_TexturesStaticLoad_AddTextures__TexturesStaticLoad_m2320370378 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TexturesStaticLoadGenerated::TexturesStaticLoad_GetRandomTexture(JSVCall,System.Int32)
extern "C"  bool TexturesStaticLoadGenerated_TexturesStaticLoad_GetRandomTexture_m3778083583 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TexturesStaticLoadGenerated::TexturesStaticLoad_GetTexture__String(JSVCall,System.Int32)
extern "C"  bool TexturesStaticLoadGenerated_TexturesStaticLoad_GetTexture__String_m316299731 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TexturesStaticLoadGenerated::TexturesStaticLoad_GetTexture__Int32(JSVCall,System.Int32)
extern "C"  bool TexturesStaticLoadGenerated_TexturesStaticLoad_GetTexture__Int32_m2732888974 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TexturesStaticLoadGenerated::__Register()
extern "C"  void TexturesStaticLoadGenerated___Register_m845081727 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture TexturesStaticLoadGenerated::ilo_GetTexture1(TexturesStaticLoad,System.String)
extern "C"  Texture_t2526458961 * TexturesStaticLoadGenerated_ilo_GetTexture1_m3117839902 (Il2CppObject * __this /* static, unused */, TexturesStaticLoad_t2473754988 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
