﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._1fb56dd813fe334b6404430513f138bc
struct _1fb56dd813fe334b6404430513f138bc_t4163233611;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._1fb56dd813fe334b6404430513f138bc::.ctor()
extern "C"  void _1fb56dd813fe334b6404430513f138bc__ctor_m4090839874 (_1fb56dd813fe334b6404430513f138bc_t4163233611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1fb56dd813fe334b6404430513f138bc::_1fb56dd813fe334b6404430513f138bcm2(System.Int32)
extern "C"  int32_t _1fb56dd813fe334b6404430513f138bc__1fb56dd813fe334b6404430513f138bcm2_m3644257721 (_1fb56dd813fe334b6404430513f138bc_t4163233611 * __this, int32_t ____1fb56dd813fe334b6404430513f138bca0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1fb56dd813fe334b6404430513f138bc::_1fb56dd813fe334b6404430513f138bcm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _1fb56dd813fe334b6404430513f138bc__1fb56dd813fe334b6404430513f138bcm_m1713411357 (_1fb56dd813fe334b6404430513f138bc_t4163233611 * __this, int32_t ____1fb56dd813fe334b6404430513f138bca0, int32_t ____1fb56dd813fe334b6404430513f138bc901, int32_t ____1fb56dd813fe334b6404430513f138bcc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
