﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.BinaryReader
struct BinaryReader_t3990958868;
// System.IO.MemoryStream
struct MemoryStream_t418716369;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ByteReadArray
struct  ByteReadArray_t751193627  : public Il2CppObject
{
public:
	// System.IO.BinaryReader ByteReadArray::m_Reader
	BinaryReader_t3990958868 * ___m_Reader_0;
	// System.IO.MemoryStream ByteReadArray::m_Stream
	MemoryStream_t418716369 * ___m_Stream_1;

public:
	inline static int32_t get_offset_of_m_Reader_0() { return static_cast<int32_t>(offsetof(ByteReadArray_t751193627, ___m_Reader_0)); }
	inline BinaryReader_t3990958868 * get_m_Reader_0() const { return ___m_Reader_0; }
	inline BinaryReader_t3990958868 ** get_address_of_m_Reader_0() { return &___m_Reader_0; }
	inline void set_m_Reader_0(BinaryReader_t3990958868 * value)
	{
		___m_Reader_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Reader_0, value);
	}

	inline static int32_t get_offset_of_m_Stream_1() { return static_cast<int32_t>(offsetof(ByteReadArray_t751193627, ___m_Stream_1)); }
	inline MemoryStream_t418716369 * get_m_Stream_1() const { return ___m_Stream_1; }
	inline MemoryStream_t418716369 ** get_address_of_m_Stream_1() { return &___m_Stream_1; }
	inline void set_m_Stream_1(MemoryStream_t418716369 * value)
	{
		___m_Stream_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Stream_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
