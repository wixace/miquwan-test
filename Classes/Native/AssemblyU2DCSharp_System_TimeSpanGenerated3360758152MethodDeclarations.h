﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_TimeSpanGenerated
struct System_TimeSpanGenerated_t3360758152;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void System_TimeSpanGenerated::.ctor()
extern "C"  void System_TimeSpanGenerated__ctor_m1998630419 (System_TimeSpanGenerated_t3360758152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::.cctor()
extern "C"  void System_TimeSpanGenerated__cctor_m1345904634 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_TimeSpan1(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_TimeSpan1_m2537040327 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_TimeSpan2(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_TimeSpan2_m3781804808 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_TimeSpan3(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_TimeSpan3_m731601993 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_TimeSpan4(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_TimeSpan4_m1976366474 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_TimeSpan5(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_TimeSpan5_m3221130955 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::TimeSpan_TicksPerDay(JSVCall)
extern "C"  void System_TimeSpanGenerated_TimeSpan_TicksPerDay_m4238469033 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::TimeSpan_TicksPerHour(JSVCall)
extern "C"  void System_TimeSpanGenerated_TimeSpan_TicksPerHour_m789737923 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::TimeSpan_TicksPerMillisecond(JSVCall)
extern "C"  void System_TimeSpanGenerated_TimeSpan_TicksPerMillisecond_m745822052 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::TimeSpan_TicksPerMinute(JSVCall)
extern "C"  void System_TimeSpanGenerated_TimeSpan_TicksPerMinute_m650318675 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::TimeSpan_TicksPerSecond(JSVCall)
extern "C"  void System_TimeSpanGenerated_TimeSpan_TicksPerSecond_m3674185971 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::TimeSpan_MaxValue(JSVCall)
extern "C"  void System_TimeSpanGenerated_TimeSpan_MaxValue_m2926516961 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::TimeSpan_MinValue(JSVCall)
extern "C"  void System_TimeSpanGenerated_TimeSpan_MinValue_m946594639 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::TimeSpan_Zero(JSVCall)
extern "C"  void System_TimeSpanGenerated_TimeSpan_Zero_m3763879174 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::TimeSpan_Days(JSVCall)
extern "C"  void System_TimeSpanGenerated_TimeSpan_Days_m632668215 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::TimeSpan_Hours(JSVCall)
extern "C"  void System_TimeSpanGenerated_TimeSpan_Hours_m696843055 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::TimeSpan_Milliseconds(JSVCall)
extern "C"  void System_TimeSpanGenerated_TimeSpan_Milliseconds_m2854277340 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::TimeSpan_Minutes(JSVCall)
extern "C"  void System_TimeSpanGenerated_TimeSpan_Minutes_m3953367391 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::TimeSpan_Seconds(JSVCall)
extern "C"  void System_TimeSpanGenerated_TimeSpan_Seconds_m3203973055 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::TimeSpan_Ticks(JSVCall)
extern "C"  void System_TimeSpanGenerated_TimeSpan_Ticks_m2350760872 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::TimeSpan_TotalDays(JSVCall)
extern "C"  void System_TimeSpanGenerated_TimeSpan_TotalDays_m1887296003 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::TimeSpan_TotalHours(JSVCall)
extern "C"  void System_TimeSpanGenerated_TimeSpan_TotalHours_m935598819 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::TimeSpan_TotalMilliseconds(JSVCall)
extern "C"  void System_TimeSpanGenerated_TimeSpan_TotalMilliseconds_m2185780904 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::TimeSpan_TotalMinutes(JSVCall)
extern "C"  void System_TimeSpanGenerated_TimeSpan_TotalMinutes_m1469422611 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::TimeSpan_TotalSeconds(JSVCall)
extern "C"  void System_TimeSpanGenerated_TimeSpan_TotalSeconds_m720028275 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_Add__TimeSpan(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_Add__TimeSpan_m2209576197 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_CompareTo__TimeSpan(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_CompareTo__TimeSpan_m3225341348 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_CompareTo__Object(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_CompareTo__Object_m4114244140 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_Duration(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_Duration_m2481630025 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_Equals__Object(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_Equals__Object_m2066910003 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_Equals__TimeSpan(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_Equals__TimeSpan_m2832257259 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_GetHashCode(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_GetHashCode_m3185004990 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_Negate(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_Negate_m3653285687 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_Subtract__TimeSpan(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_Subtract__TimeSpan_m2471329600 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_ToString(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_ToString_m3078290977 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_Compare__TimeSpan__TimeSpan(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_Compare__TimeSpan__TimeSpan_m3698614720 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_Equals__TimeSpan__TimeSpan(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_Equals__TimeSpan__TimeSpan_m367637730 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_FromDays__Double(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_FromDays__Double_m2583015719 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_FromHours__Double(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_FromHours__Double_m1041347011 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_FromMilliseconds__Double(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_FromMilliseconds__Double_m2459857826 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_FromMinutes__Double(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_FromMinutes__Double_m1408569107 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_FromSeconds__Double(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_FromSeconds__Double_m2744755379 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_FromTicks__Int64(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_FromTicks__Int64_m3546706550 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_op_Addition__TimeSpan__TimeSpan(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_op_Addition__TimeSpan__TimeSpan_m4013685877 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_op_Equality__TimeSpan__TimeSpan(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_op_Equality__TimeSpan__TimeSpan_m3073661171 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_op_GreaterThan__TimeSpan__TimeSpan(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_op_GreaterThan__TimeSpan__TimeSpan_m4111424192 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_op_GreaterThanOrEqual__TimeSpan__TimeSpan(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_op_GreaterThanOrEqual__TimeSpan__TimeSpan_m579612239 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_op_Inequality__TimeSpan__TimeSpan(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_op_Inequality__TimeSpan__TimeSpan_m1884458072 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_op_LessThan__TimeSpan__TimeSpan(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_op_LessThan__TimeSpan__TimeSpan_m3627317971 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_op_LessThanOrEqual__TimeSpan__TimeSpan(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_op_LessThanOrEqual__TimeSpan__TimeSpan_m924410588 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_op_Subtraction__TimeSpan__TimeSpan(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_op_Subtraction__TimeSpan__TimeSpan_m3966351161 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_op_UnaryNegation__TimeSpan(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_op_UnaryNegation__TimeSpan_m3868487618 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_Parse__String(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_Parse__String_m2274824369 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::TimeSpan_TryParse__String__TimeSpan(JSVCall,System.Int32)
extern "C"  bool System_TimeSpanGenerated_TimeSpan_TryParse__String__TimeSpan_m1071016437 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::__Register()
extern "C"  void System_TimeSpanGenerated___Register_m1848222356 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_TimeSpanGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t System_TimeSpanGenerated_ilo_getObject1_m3796147423 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_TimeSpanGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool System_TimeSpanGenerated_ilo_attachFinalizerObject2_m3360378477 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void System_TimeSpanGenerated_ilo_addJSCSRel3_m2391130193 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_TimeSpanGenerated::ilo_getInt324(System.Int32)
extern "C"  int32_t System_TimeSpanGenerated_ilo_getInt324_m2968065949 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::ilo_setInt645(System.Int32,System.Int64)
extern "C"  void System_TimeSpanGenerated_ilo_setInt645_m1629628671 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int64_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_TimeSpanGenerated::ilo_setObject6(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_TimeSpanGenerated_ilo_setObject6_m1580317596 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::ilo_setInt327(System.Int32,System.Int32)
extern "C"  void System_TimeSpanGenerated_ilo_setInt327_m3994968157 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::ilo_setDouble8(System.Int32,System.Double)
extern "C"  void System_TimeSpanGenerated_ilo_setDouble8_m1356354486 (Il2CppObject * __this /* static, unused */, int32_t ___e0, double ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_TimeSpanGenerated::ilo_getObject9(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_TimeSpanGenerated_ilo_getObject9_m3637834628 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::ilo_changeJSObj10(System.Int32,System.Object)
extern "C"  void System_TimeSpanGenerated_ilo_changeJSObj10_m4251438074 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::ilo_setBooleanS11(System.Int32,System.Boolean)
extern "C"  void System_TimeSpanGenerated_ilo_setBooleanS11_m1320538913 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System_TimeSpanGenerated::ilo_getStringS12(System.Int32)
extern "C"  String_t* System_TimeSpanGenerated_ilo_getStringS12_m3320387749 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_TimeSpanGenerated::ilo_setArgIndex13(System.Int32)
extern "C"  void System_TimeSpanGenerated_ilo_setArgIndex13_m3160812111 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
