﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<Entity.Behavior.EBehaviorID>
struct EqualityComparer_1_t346370017;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.EqualityComparer`1<Entity.Behavior.EBehaviorID>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m123014912_gshared (EqualityComparer_1_t346370017 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m123014912(__this, method) ((  void (*) (EqualityComparer_1_t346370017 *, const MethodInfo*))EqualityComparer_1__ctor_m123014912_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<Entity.Behavior.EBehaviorID>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3331366061_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m3331366061(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m3331366061_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<Entity.Behavior.EBehaviorID>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3912958169_gshared (EqualityComparer_1_t346370017 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3912958169(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t346370017 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3912958169_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Entity.Behavior.EBehaviorID>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3555145169_gshared (EqualityComparer_1_t346370017 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3555145169(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t346370017 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3555145169_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Entity.Behavior.EBehaviorID>::get_Default()
extern "C"  EqualityComparer_1_t346370017 * EqualityComparer_1_get_Default_m2290072746_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m2290072746(__this /* static, unused */, method) ((  EqualityComparer_1_t346370017 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m2290072746_gshared)(__this /* static, unused */, method)
