﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEventTriggerGenerated
struct UIEventTriggerGenerated_t444858173;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UIEventTriggerGenerated::.ctor()
extern "C"  void UIEventTriggerGenerated__ctor_m471413966 (UIEventTriggerGenerated_t444858173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIEventTriggerGenerated::UIEventTrigger_UIEventTrigger1(JSVCall,System.Int32)
extern "C"  bool UIEventTriggerGenerated_UIEventTrigger_UIEventTrigger1_m3142678116 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTriggerGenerated::UIEventTrigger_current(JSVCall)
extern "C"  void UIEventTriggerGenerated_UIEventTrigger_current_m3268988517 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTriggerGenerated::UIEventTrigger_onHoverOver(JSVCall)
extern "C"  void UIEventTriggerGenerated_UIEventTrigger_onHoverOver_m2639102413 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTriggerGenerated::UIEventTrigger_onHoverOut(JSVCall)
extern "C"  void UIEventTriggerGenerated_UIEventTrigger_onHoverOut_m3953865725 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTriggerGenerated::UIEventTrigger_onPress(JSVCall)
extern "C"  void UIEventTriggerGenerated_UIEventTrigger_onPress_m3580400282 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTriggerGenerated::UIEventTrigger_onRelease(JSVCall)
extern "C"  void UIEventTriggerGenerated_UIEventTrigger_onRelease_m3187763606 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTriggerGenerated::UIEventTrigger_onSelect(JSVCall)
extern "C"  void UIEventTriggerGenerated_UIEventTrigger_onSelect_m3865421043 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTriggerGenerated::UIEventTrigger_onDeselect(JSVCall)
extern "C"  void UIEventTriggerGenerated_UIEventTrigger_onDeselect_m1708937234 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTriggerGenerated::UIEventTrigger_onClick(JSVCall)
extern "C"  void UIEventTriggerGenerated_UIEventTrigger_onClick_m4098035445 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTriggerGenerated::UIEventTrigger_onDoubleClick(JSVCall)
extern "C"  void UIEventTriggerGenerated_UIEventTrigger_onDoubleClick_m485310438 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTriggerGenerated::UIEventTrigger_onDragStart(JSVCall)
extern "C"  void UIEventTriggerGenerated_UIEventTrigger_onDragStart_m2775916591 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTriggerGenerated::UIEventTrigger_onDragEnd(JSVCall)
extern "C"  void UIEventTriggerGenerated_UIEventTrigger_onDragEnd_m1820002966 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTriggerGenerated::UIEventTrigger_onDragOver(JSVCall)
extern "C"  void UIEventTriggerGenerated_UIEventTrigger_onDragOver_m196964615 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTriggerGenerated::UIEventTrigger_onDragOut(JSVCall)
extern "C"  void UIEventTriggerGenerated_UIEventTrigger_onDragOut_m272856451 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTriggerGenerated::UIEventTrigger_onDrag(JSVCall)
extern "C"  void UIEventTriggerGenerated_UIEventTrigger_onDrag_m2413728251 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventTriggerGenerated::__Register()
extern "C"  void UIEventTriggerGenerated___Register_m212801721 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIEventTriggerGenerated::ilo_getObject1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIEventTriggerGenerated_ilo_getObject1_m3842053719 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIEventTriggerGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIEventTriggerGenerated_ilo_setObject2_m2529495521 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
