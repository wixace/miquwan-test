﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen4293064463MethodDeclarations.h"

// System.Void System.Action`2<System.Byte[],System.String>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m2545950317(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t1442129143 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m4171654682_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<System.Byte[],System.String>::Invoke(T1,T2)
#define Action_2_Invoke_m2958047454(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t1442129143 *, ByteU5BU5D_t4260760469*, String_t*, const MethodInfo*))Action_2_Invoke_m1325815329_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<System.Byte[],System.String>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m2818042285(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t1442129143 *, ByteU5BU5D_t4260760469*, String_t*, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m3413657584_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<System.Byte[],System.String>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m221201421(__this, ___result0, method) ((  void (*) (Action_2_t1442129143 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m3926193450_gshared)(__this, ___result0, method)
