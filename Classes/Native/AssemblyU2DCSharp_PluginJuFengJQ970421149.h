﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginJuFengJQ
struct  PluginJuFengJQ_t970421149  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginJuFengJQ::token
	String_t* ___token_2;
	// System.String PluginJuFengJQ::userId
	String_t* ___userId_3;
	// System.String PluginJuFengJQ::configId
	String_t* ___configId_4;
	// System.String PluginJuFengJQ::sign
	String_t* ___sign_5;
	// System.String PluginJuFengJQ::extra
	String_t* ___extra_6;
	// Mihua.SDK.PayInfo PluginJuFengJQ::iapInfo
	PayInfo_t1775308120 * ___iapInfo_7;

public:
	inline static int32_t get_offset_of_token_2() { return static_cast<int32_t>(offsetof(PluginJuFengJQ_t970421149, ___token_2)); }
	inline String_t* get_token_2() const { return ___token_2; }
	inline String_t** get_address_of_token_2() { return &___token_2; }
	inline void set_token_2(String_t* value)
	{
		___token_2 = value;
		Il2CppCodeGenWriteBarrier(&___token_2, value);
	}

	inline static int32_t get_offset_of_userId_3() { return static_cast<int32_t>(offsetof(PluginJuFengJQ_t970421149, ___userId_3)); }
	inline String_t* get_userId_3() const { return ___userId_3; }
	inline String_t** get_address_of_userId_3() { return &___userId_3; }
	inline void set_userId_3(String_t* value)
	{
		___userId_3 = value;
		Il2CppCodeGenWriteBarrier(&___userId_3, value);
	}

	inline static int32_t get_offset_of_configId_4() { return static_cast<int32_t>(offsetof(PluginJuFengJQ_t970421149, ___configId_4)); }
	inline String_t* get_configId_4() const { return ___configId_4; }
	inline String_t** get_address_of_configId_4() { return &___configId_4; }
	inline void set_configId_4(String_t* value)
	{
		___configId_4 = value;
		Il2CppCodeGenWriteBarrier(&___configId_4, value);
	}

	inline static int32_t get_offset_of_sign_5() { return static_cast<int32_t>(offsetof(PluginJuFengJQ_t970421149, ___sign_5)); }
	inline String_t* get_sign_5() const { return ___sign_5; }
	inline String_t** get_address_of_sign_5() { return &___sign_5; }
	inline void set_sign_5(String_t* value)
	{
		___sign_5 = value;
		Il2CppCodeGenWriteBarrier(&___sign_5, value);
	}

	inline static int32_t get_offset_of_extra_6() { return static_cast<int32_t>(offsetof(PluginJuFengJQ_t970421149, ___extra_6)); }
	inline String_t* get_extra_6() const { return ___extra_6; }
	inline String_t** get_address_of_extra_6() { return &___extra_6; }
	inline void set_extra_6(String_t* value)
	{
		___extra_6 = value;
		Il2CppCodeGenWriteBarrier(&___extra_6, value);
	}

	inline static int32_t get_offset_of_iapInfo_7() { return static_cast<int32_t>(offsetof(PluginJuFengJQ_t970421149, ___iapInfo_7)); }
	inline PayInfo_t1775308120 * get_iapInfo_7() const { return ___iapInfo_7; }
	inline PayInfo_t1775308120 ** get_address_of_iapInfo_7() { return &___iapInfo_7; }
	inline void set_iapInfo_7(PayInfo_t1775308120 * value)
	{
		___iapInfo_7 = value;
		Il2CppCodeGenWriteBarrier(&___iapInfo_7, value);
	}
};

struct PluginJuFengJQ_t970421149_StaticFields
{
public:
	// System.Action PluginJuFengJQ::<>f__am$cache6
	Action_t3771233898 * ___U3CU3Ef__amU24cache6_8;
	// System.Action PluginJuFengJQ::<>f__am$cache7
	Action_t3771233898 * ___U3CU3Ef__amU24cache7_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_8() { return static_cast<int32_t>(offsetof(PluginJuFengJQ_t970421149_StaticFields, ___U3CU3Ef__amU24cache6_8)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache6_8() const { return ___U3CU3Ef__amU24cache6_8; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache6_8() { return &___U3CU3Ef__amU24cache6_8; }
	inline void set_U3CU3Ef__amU24cache6_8(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache6_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_9() { return static_cast<int32_t>(offsetof(PluginJuFengJQ_t970421149_StaticFields, ___U3CU3Ef__amU24cache7_9)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache7_9() const { return ___U3CU3Ef__amU24cache7_9; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache7_9() { return &___U3CU3Ef__amU24cache7_9; }
	inline void set_U3CU3Ef__amU24cache7_9(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache7_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
