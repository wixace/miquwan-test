﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.GraphCollision
struct GraphCollision_t2160440954;
// Pathfinding.GridGraph/TextureData
struct TextureData_t1394081396;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.UInt32[]
struct UInt32U5BU5D_t3230734560;
// Pathfinding.GridNode[]
struct GridNodeU5BU5D_t2925197291;

#include "AssemblyU2DCSharp_Pathfinding_NavGraph1254319713.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_Pathfinding_NumNeighbours3171482750.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.GridGraph
struct  GridGraph_t2455707914  : public NavGraph_t1254319713
{
public:
	// System.Int32 Pathfinding.GridGraph::width
	int32_t ___width_12;
	// System.Int32 Pathfinding.GridGraph::depth
	int32_t ___depth_13;
	// System.Single Pathfinding.GridGraph::aspectRatio
	float ___aspectRatio_14;
	// System.Single Pathfinding.GridGraph::isometricAngle
	float ___isometricAngle_15;
	// UnityEngine.Vector3 Pathfinding.GridGraph::rotation
	Vector3_t4282066566  ___rotation_16;
	// UnityEngine.Bounds Pathfinding.GridGraph::bounds
	Bounds_t2711641849  ___bounds_17;
	// UnityEngine.Vector3 Pathfinding.GridGraph::center
	Vector3_t4282066566  ___center_18;
	// UnityEngine.Vector2 Pathfinding.GridGraph::unclampedSize
	Vector2_t4282066565  ___unclampedSize_19;
	// System.Single Pathfinding.GridGraph::nodeSize
	float ___nodeSize_20;
	// Pathfinding.GraphCollision Pathfinding.GridGraph::collision
	GraphCollision_t2160440954 * ___collision_21;
	// System.Single Pathfinding.GridGraph::maxClimb
	float ___maxClimb_22;
	// System.Int32 Pathfinding.GridGraph::maxClimbAxis
	int32_t ___maxClimbAxis_23;
	// System.Single Pathfinding.GridGraph::maxSlope
	float ___maxSlope_24;
	// System.Int32 Pathfinding.GridGraph::erodeIterations
	int32_t ___erodeIterations_25;
	// System.Boolean Pathfinding.GridGraph::erosionUseTags
	bool ___erosionUseTags_26;
	// System.Int32 Pathfinding.GridGraph::erosionFirstTag
	int32_t ___erosionFirstTag_27;
	// System.Boolean Pathfinding.GridGraph::autoLinkGrids
	bool ___autoLinkGrids_28;
	// System.Single Pathfinding.GridGraph::autoLinkDistLimit
	float ___autoLinkDistLimit_29;
	// Pathfinding.NumNeighbours Pathfinding.GridGraph::neighbours
	int32_t ___neighbours_30;
	// System.Boolean Pathfinding.GridGraph::cutCorners
	bool ___cutCorners_31;
	// System.Single Pathfinding.GridGraph::penaltyPositionOffset
	float ___penaltyPositionOffset_32;
	// System.Boolean Pathfinding.GridGraph::penaltyPosition
	bool ___penaltyPosition_33;
	// System.Single Pathfinding.GridGraph::penaltyPositionFactor
	float ___penaltyPositionFactor_34;
	// System.Boolean Pathfinding.GridGraph::penaltyAngle
	bool ___penaltyAngle_35;
	// System.Single Pathfinding.GridGraph::penaltyAngleFactor
	float ___penaltyAngleFactor_36;
	// System.Single Pathfinding.GridGraph::penaltyAnglePower
	float ___penaltyAnglePower_37;
	// System.Boolean Pathfinding.GridGraph::useJumpPointSearch
	bool ___useJumpPointSearch_38;
	// Pathfinding.GridGraph/TextureData Pathfinding.GridGraph::textureData
	TextureData_t1394081396 * ___textureData_39;
	// UnityEngine.Vector2 Pathfinding.GridGraph::size
	Vector2_t4282066565  ___size_40;
	// System.Int32[] Pathfinding.GridGraph::neighbourOffsets
	Int32U5BU5D_t3230847821* ___neighbourOffsets_41;
	// System.UInt32[] Pathfinding.GridGraph::neighbourCosts
	UInt32U5BU5D_t3230734560* ___neighbourCosts_42;
	// System.Int32[] Pathfinding.GridGraph::neighbourXOffsets
	Int32U5BU5D_t3230847821* ___neighbourXOffsets_43;
	// System.Int32[] Pathfinding.GridGraph::neighbourZOffsets
	Int32U5BU5D_t3230847821* ___neighbourZOffsets_44;
	// UnityEngine.Matrix4x4 Pathfinding.GridGraph::boundsMatrix
	Matrix4x4_t1651859333  ___boundsMatrix_45;
	// UnityEngine.Matrix4x4 Pathfinding.GridGraph::boundsMatrix2
	Matrix4x4_t1651859333  ___boundsMatrix2_46;
	// System.Int32 Pathfinding.GridGraph::scans
	int32_t ___scans_47;
	// Pathfinding.GridNode[] Pathfinding.GridGraph::nodes
	GridNodeU5BU5D_t2925197291* ___nodes_48;
	// System.Int32[] Pathfinding.GridGraph::corners
	Int32U5BU5D_t3230847821* ___corners_49;

public:
	inline static int32_t get_offset_of_width_12() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___width_12)); }
	inline int32_t get_width_12() const { return ___width_12; }
	inline int32_t* get_address_of_width_12() { return &___width_12; }
	inline void set_width_12(int32_t value)
	{
		___width_12 = value;
	}

	inline static int32_t get_offset_of_depth_13() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___depth_13)); }
	inline int32_t get_depth_13() const { return ___depth_13; }
	inline int32_t* get_address_of_depth_13() { return &___depth_13; }
	inline void set_depth_13(int32_t value)
	{
		___depth_13 = value;
	}

	inline static int32_t get_offset_of_aspectRatio_14() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___aspectRatio_14)); }
	inline float get_aspectRatio_14() const { return ___aspectRatio_14; }
	inline float* get_address_of_aspectRatio_14() { return &___aspectRatio_14; }
	inline void set_aspectRatio_14(float value)
	{
		___aspectRatio_14 = value;
	}

	inline static int32_t get_offset_of_isometricAngle_15() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___isometricAngle_15)); }
	inline float get_isometricAngle_15() const { return ___isometricAngle_15; }
	inline float* get_address_of_isometricAngle_15() { return &___isometricAngle_15; }
	inline void set_isometricAngle_15(float value)
	{
		___isometricAngle_15 = value;
	}

	inline static int32_t get_offset_of_rotation_16() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___rotation_16)); }
	inline Vector3_t4282066566  get_rotation_16() const { return ___rotation_16; }
	inline Vector3_t4282066566 * get_address_of_rotation_16() { return &___rotation_16; }
	inline void set_rotation_16(Vector3_t4282066566  value)
	{
		___rotation_16 = value;
	}

	inline static int32_t get_offset_of_bounds_17() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___bounds_17)); }
	inline Bounds_t2711641849  get_bounds_17() const { return ___bounds_17; }
	inline Bounds_t2711641849 * get_address_of_bounds_17() { return &___bounds_17; }
	inline void set_bounds_17(Bounds_t2711641849  value)
	{
		___bounds_17 = value;
	}

	inline static int32_t get_offset_of_center_18() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___center_18)); }
	inline Vector3_t4282066566  get_center_18() const { return ___center_18; }
	inline Vector3_t4282066566 * get_address_of_center_18() { return &___center_18; }
	inline void set_center_18(Vector3_t4282066566  value)
	{
		___center_18 = value;
	}

	inline static int32_t get_offset_of_unclampedSize_19() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___unclampedSize_19)); }
	inline Vector2_t4282066565  get_unclampedSize_19() const { return ___unclampedSize_19; }
	inline Vector2_t4282066565 * get_address_of_unclampedSize_19() { return &___unclampedSize_19; }
	inline void set_unclampedSize_19(Vector2_t4282066565  value)
	{
		___unclampedSize_19 = value;
	}

	inline static int32_t get_offset_of_nodeSize_20() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___nodeSize_20)); }
	inline float get_nodeSize_20() const { return ___nodeSize_20; }
	inline float* get_address_of_nodeSize_20() { return &___nodeSize_20; }
	inline void set_nodeSize_20(float value)
	{
		___nodeSize_20 = value;
	}

	inline static int32_t get_offset_of_collision_21() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___collision_21)); }
	inline GraphCollision_t2160440954 * get_collision_21() const { return ___collision_21; }
	inline GraphCollision_t2160440954 ** get_address_of_collision_21() { return &___collision_21; }
	inline void set_collision_21(GraphCollision_t2160440954 * value)
	{
		___collision_21 = value;
		Il2CppCodeGenWriteBarrier(&___collision_21, value);
	}

	inline static int32_t get_offset_of_maxClimb_22() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___maxClimb_22)); }
	inline float get_maxClimb_22() const { return ___maxClimb_22; }
	inline float* get_address_of_maxClimb_22() { return &___maxClimb_22; }
	inline void set_maxClimb_22(float value)
	{
		___maxClimb_22 = value;
	}

	inline static int32_t get_offset_of_maxClimbAxis_23() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___maxClimbAxis_23)); }
	inline int32_t get_maxClimbAxis_23() const { return ___maxClimbAxis_23; }
	inline int32_t* get_address_of_maxClimbAxis_23() { return &___maxClimbAxis_23; }
	inline void set_maxClimbAxis_23(int32_t value)
	{
		___maxClimbAxis_23 = value;
	}

	inline static int32_t get_offset_of_maxSlope_24() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___maxSlope_24)); }
	inline float get_maxSlope_24() const { return ___maxSlope_24; }
	inline float* get_address_of_maxSlope_24() { return &___maxSlope_24; }
	inline void set_maxSlope_24(float value)
	{
		___maxSlope_24 = value;
	}

	inline static int32_t get_offset_of_erodeIterations_25() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___erodeIterations_25)); }
	inline int32_t get_erodeIterations_25() const { return ___erodeIterations_25; }
	inline int32_t* get_address_of_erodeIterations_25() { return &___erodeIterations_25; }
	inline void set_erodeIterations_25(int32_t value)
	{
		___erodeIterations_25 = value;
	}

	inline static int32_t get_offset_of_erosionUseTags_26() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___erosionUseTags_26)); }
	inline bool get_erosionUseTags_26() const { return ___erosionUseTags_26; }
	inline bool* get_address_of_erosionUseTags_26() { return &___erosionUseTags_26; }
	inline void set_erosionUseTags_26(bool value)
	{
		___erosionUseTags_26 = value;
	}

	inline static int32_t get_offset_of_erosionFirstTag_27() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___erosionFirstTag_27)); }
	inline int32_t get_erosionFirstTag_27() const { return ___erosionFirstTag_27; }
	inline int32_t* get_address_of_erosionFirstTag_27() { return &___erosionFirstTag_27; }
	inline void set_erosionFirstTag_27(int32_t value)
	{
		___erosionFirstTag_27 = value;
	}

	inline static int32_t get_offset_of_autoLinkGrids_28() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___autoLinkGrids_28)); }
	inline bool get_autoLinkGrids_28() const { return ___autoLinkGrids_28; }
	inline bool* get_address_of_autoLinkGrids_28() { return &___autoLinkGrids_28; }
	inline void set_autoLinkGrids_28(bool value)
	{
		___autoLinkGrids_28 = value;
	}

	inline static int32_t get_offset_of_autoLinkDistLimit_29() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___autoLinkDistLimit_29)); }
	inline float get_autoLinkDistLimit_29() const { return ___autoLinkDistLimit_29; }
	inline float* get_address_of_autoLinkDistLimit_29() { return &___autoLinkDistLimit_29; }
	inline void set_autoLinkDistLimit_29(float value)
	{
		___autoLinkDistLimit_29 = value;
	}

	inline static int32_t get_offset_of_neighbours_30() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___neighbours_30)); }
	inline int32_t get_neighbours_30() const { return ___neighbours_30; }
	inline int32_t* get_address_of_neighbours_30() { return &___neighbours_30; }
	inline void set_neighbours_30(int32_t value)
	{
		___neighbours_30 = value;
	}

	inline static int32_t get_offset_of_cutCorners_31() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___cutCorners_31)); }
	inline bool get_cutCorners_31() const { return ___cutCorners_31; }
	inline bool* get_address_of_cutCorners_31() { return &___cutCorners_31; }
	inline void set_cutCorners_31(bool value)
	{
		___cutCorners_31 = value;
	}

	inline static int32_t get_offset_of_penaltyPositionOffset_32() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___penaltyPositionOffset_32)); }
	inline float get_penaltyPositionOffset_32() const { return ___penaltyPositionOffset_32; }
	inline float* get_address_of_penaltyPositionOffset_32() { return &___penaltyPositionOffset_32; }
	inline void set_penaltyPositionOffset_32(float value)
	{
		___penaltyPositionOffset_32 = value;
	}

	inline static int32_t get_offset_of_penaltyPosition_33() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___penaltyPosition_33)); }
	inline bool get_penaltyPosition_33() const { return ___penaltyPosition_33; }
	inline bool* get_address_of_penaltyPosition_33() { return &___penaltyPosition_33; }
	inline void set_penaltyPosition_33(bool value)
	{
		___penaltyPosition_33 = value;
	}

	inline static int32_t get_offset_of_penaltyPositionFactor_34() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___penaltyPositionFactor_34)); }
	inline float get_penaltyPositionFactor_34() const { return ___penaltyPositionFactor_34; }
	inline float* get_address_of_penaltyPositionFactor_34() { return &___penaltyPositionFactor_34; }
	inline void set_penaltyPositionFactor_34(float value)
	{
		___penaltyPositionFactor_34 = value;
	}

	inline static int32_t get_offset_of_penaltyAngle_35() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___penaltyAngle_35)); }
	inline bool get_penaltyAngle_35() const { return ___penaltyAngle_35; }
	inline bool* get_address_of_penaltyAngle_35() { return &___penaltyAngle_35; }
	inline void set_penaltyAngle_35(bool value)
	{
		___penaltyAngle_35 = value;
	}

	inline static int32_t get_offset_of_penaltyAngleFactor_36() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___penaltyAngleFactor_36)); }
	inline float get_penaltyAngleFactor_36() const { return ___penaltyAngleFactor_36; }
	inline float* get_address_of_penaltyAngleFactor_36() { return &___penaltyAngleFactor_36; }
	inline void set_penaltyAngleFactor_36(float value)
	{
		___penaltyAngleFactor_36 = value;
	}

	inline static int32_t get_offset_of_penaltyAnglePower_37() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___penaltyAnglePower_37)); }
	inline float get_penaltyAnglePower_37() const { return ___penaltyAnglePower_37; }
	inline float* get_address_of_penaltyAnglePower_37() { return &___penaltyAnglePower_37; }
	inline void set_penaltyAnglePower_37(float value)
	{
		___penaltyAnglePower_37 = value;
	}

	inline static int32_t get_offset_of_useJumpPointSearch_38() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___useJumpPointSearch_38)); }
	inline bool get_useJumpPointSearch_38() const { return ___useJumpPointSearch_38; }
	inline bool* get_address_of_useJumpPointSearch_38() { return &___useJumpPointSearch_38; }
	inline void set_useJumpPointSearch_38(bool value)
	{
		___useJumpPointSearch_38 = value;
	}

	inline static int32_t get_offset_of_textureData_39() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___textureData_39)); }
	inline TextureData_t1394081396 * get_textureData_39() const { return ___textureData_39; }
	inline TextureData_t1394081396 ** get_address_of_textureData_39() { return &___textureData_39; }
	inline void set_textureData_39(TextureData_t1394081396 * value)
	{
		___textureData_39 = value;
		Il2CppCodeGenWriteBarrier(&___textureData_39, value);
	}

	inline static int32_t get_offset_of_size_40() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___size_40)); }
	inline Vector2_t4282066565  get_size_40() const { return ___size_40; }
	inline Vector2_t4282066565 * get_address_of_size_40() { return &___size_40; }
	inline void set_size_40(Vector2_t4282066565  value)
	{
		___size_40 = value;
	}

	inline static int32_t get_offset_of_neighbourOffsets_41() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___neighbourOffsets_41)); }
	inline Int32U5BU5D_t3230847821* get_neighbourOffsets_41() const { return ___neighbourOffsets_41; }
	inline Int32U5BU5D_t3230847821** get_address_of_neighbourOffsets_41() { return &___neighbourOffsets_41; }
	inline void set_neighbourOffsets_41(Int32U5BU5D_t3230847821* value)
	{
		___neighbourOffsets_41 = value;
		Il2CppCodeGenWriteBarrier(&___neighbourOffsets_41, value);
	}

	inline static int32_t get_offset_of_neighbourCosts_42() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___neighbourCosts_42)); }
	inline UInt32U5BU5D_t3230734560* get_neighbourCosts_42() const { return ___neighbourCosts_42; }
	inline UInt32U5BU5D_t3230734560** get_address_of_neighbourCosts_42() { return &___neighbourCosts_42; }
	inline void set_neighbourCosts_42(UInt32U5BU5D_t3230734560* value)
	{
		___neighbourCosts_42 = value;
		Il2CppCodeGenWriteBarrier(&___neighbourCosts_42, value);
	}

	inline static int32_t get_offset_of_neighbourXOffsets_43() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___neighbourXOffsets_43)); }
	inline Int32U5BU5D_t3230847821* get_neighbourXOffsets_43() const { return ___neighbourXOffsets_43; }
	inline Int32U5BU5D_t3230847821** get_address_of_neighbourXOffsets_43() { return &___neighbourXOffsets_43; }
	inline void set_neighbourXOffsets_43(Int32U5BU5D_t3230847821* value)
	{
		___neighbourXOffsets_43 = value;
		Il2CppCodeGenWriteBarrier(&___neighbourXOffsets_43, value);
	}

	inline static int32_t get_offset_of_neighbourZOffsets_44() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___neighbourZOffsets_44)); }
	inline Int32U5BU5D_t3230847821* get_neighbourZOffsets_44() const { return ___neighbourZOffsets_44; }
	inline Int32U5BU5D_t3230847821** get_address_of_neighbourZOffsets_44() { return &___neighbourZOffsets_44; }
	inline void set_neighbourZOffsets_44(Int32U5BU5D_t3230847821* value)
	{
		___neighbourZOffsets_44 = value;
		Il2CppCodeGenWriteBarrier(&___neighbourZOffsets_44, value);
	}

	inline static int32_t get_offset_of_boundsMatrix_45() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___boundsMatrix_45)); }
	inline Matrix4x4_t1651859333  get_boundsMatrix_45() const { return ___boundsMatrix_45; }
	inline Matrix4x4_t1651859333 * get_address_of_boundsMatrix_45() { return &___boundsMatrix_45; }
	inline void set_boundsMatrix_45(Matrix4x4_t1651859333  value)
	{
		___boundsMatrix_45 = value;
	}

	inline static int32_t get_offset_of_boundsMatrix2_46() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___boundsMatrix2_46)); }
	inline Matrix4x4_t1651859333  get_boundsMatrix2_46() const { return ___boundsMatrix2_46; }
	inline Matrix4x4_t1651859333 * get_address_of_boundsMatrix2_46() { return &___boundsMatrix2_46; }
	inline void set_boundsMatrix2_46(Matrix4x4_t1651859333  value)
	{
		___boundsMatrix2_46 = value;
	}

	inline static int32_t get_offset_of_scans_47() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___scans_47)); }
	inline int32_t get_scans_47() const { return ___scans_47; }
	inline int32_t* get_address_of_scans_47() { return &___scans_47; }
	inline void set_scans_47(int32_t value)
	{
		___scans_47 = value;
	}

	inline static int32_t get_offset_of_nodes_48() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___nodes_48)); }
	inline GridNodeU5BU5D_t2925197291* get_nodes_48() const { return ___nodes_48; }
	inline GridNodeU5BU5D_t2925197291** get_address_of_nodes_48() { return &___nodes_48; }
	inline void set_nodes_48(GridNodeU5BU5D_t2925197291* value)
	{
		___nodes_48 = value;
		Il2CppCodeGenWriteBarrier(&___nodes_48, value);
	}

	inline static int32_t get_offset_of_corners_49() { return static_cast<int32_t>(offsetof(GridGraph_t2455707914, ___corners_49)); }
	inline Int32U5BU5D_t3230847821* get_corners_49() const { return ___corners_49; }
	inline Int32U5BU5D_t3230847821** get_address_of_corners_49() { return &___corners_49; }
	inline void set_corners_49(Int32U5BU5D_t3230847821* value)
	{
		___corners_49 = value;
		Il2CppCodeGenWriteBarrier(&___corners_49, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
