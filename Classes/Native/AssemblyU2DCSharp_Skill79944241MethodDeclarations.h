﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Skill
struct Skill_t79944241;
// CombatEntity
struct CombatEntity_t684137495;
// skillCfg
struct skillCfg_t2142425171;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// FightEnum.ESkillBuffEvent[]
struct ESkillBuffEventU5BU5D_t2768361140;
// FightEnum.ESkillBuffTarget[]
struct ESkillBuffTargetU5BU5D_t2691263849;
// System.Boolean[]
struct BooleanU5BU5D_t3456302923;
// System.String
struct String_t;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// GlobalGOMgr
struct GlobalGOMgr_t803081773;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Skill79944241.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Skill::.ctor(System.Int32,System.Int32,System.UInt32,CombatEntity)
extern "C"  void Skill__ctor_m2121015439 (Skill_t79944241 * __this, int32_t ___skillID0, int32_t ___index1, uint32_t ___level2, CombatEntity_t684137495 * ___entity3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::.ctor(System.Int32,System.Int32,CombatEntity)
extern "C"  void Skill__ctor_m2194440515 (Skill_t79944241 * __this, int32_t ___skillID0, int32_t ___index1, CombatEntity_t684137495 * ___entity2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_index(System.Int32)
extern "C"  void Skill_set_index_m1278371678 (Skill_t79944241 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Skill::get_index()
extern "C"  int32_t Skill_get_index_m4208277263 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_mTplID(System.Int32)
extern "C"  void Skill_set_mTplID_m2005565558 (Skill_t79944241 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Skill::get_mTplID()
extern "C"  int32_t Skill_get_mTplID_m3032232291 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_isActive(System.Boolean)
extern "C"  void Skill_set_isActive_m2904396942 (Skill_t79944241 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Skill::get_isActive()
extern "C"  bool Skill_get_isActive_m1329043887 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_isAddedAnger(System.Boolean)
extern "C"  void Skill_set_isAddedAnger_m1273467503 (Skill_t79944241 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Skill::get_isAddedAnger()
extern "C"  bool Skill_get_isAddedAnger_m225974608 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_mLastUseTime(System.Single)
extern "C"  void Skill_set_mLastUseTime_m1087582867 (Skill_t79944241 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Skill::get_mLastUseTime()
extern "C"  float Skill_get_mLastUseTime_m1392672088 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// skillCfg Skill::get_mpJson()
extern "C"  skillCfg_t2142425171 * Skill_get_mpJson_m309147380 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_action(System.String[])
extern "C"  void Skill_set_action_m4170023747 (Skill_t79944241 * __this, StringU5BU5D_t4054002952* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Skill::get_action()
extern "C"  StringU5BU5D_t4054002952* Skill_get_action_m1639750248 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_actionTime(System.Single[])
extern "C"  void Skill_set_actionTime_m1594371679 (Skill_t79944241 * __this, SingleU5BU5D_t2316563989* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] Skill::get_actionTime()
extern "C"  SingleU5BU5D_t2316563989* Skill_get_actionTime_m1657801740 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_castingEffect(System.Int32[])
extern "C"  void Skill_set_castingEffect_m119492574 (Skill_t79944241 * __this, Int32U5BU5D_t3230847821* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Skill::get_castingEffect()
extern "C"  Int32U5BU5D_t3230847821* Skill_get_castingEffect_m4277031759 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_castingEffectTime(System.Single[])
extern "C"  void Skill_set_castingEffectTime_m2247619565 (Skill_t79944241 * __this, SingleU5BU5D_t2316563989* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] Skill::get_castingEffectTime()
extern "C"  SingleU5BU5D_t2316563989* Skill_get_castingEffectTime_m2201861818 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_castingEffectPlaySpeed(System.Single[])
extern "C"  void Skill_set_castingEffectPlaySpeed_m2961682659 (Skill_t79944241 * __this, SingleU5BU5D_t2316563989* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] Skill::get_castingEffectPlaySpeed()
extern "C"  SingleU5BU5D_t2316563989* Skill_get_castingEffectPlaySpeed_m2253188872 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_effect(System.Int32[])
extern "C"  void Skill_set_effect_m3627023239 (Skill_t79944241 * __this, Int32U5BU5D_t3230847821* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Skill::get_effect()
extern "C"  Int32U5BU5D_t3230847821* Skill_get_effect_m849196024 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_effectTime(System.Single[])
extern "C"  void Skill_set_effectTime_m3422366180 (Skill_t79944241 * __this, SingleU5BU5D_t2316563989* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] Skill::get_effectTime()
extern "C"  SingleU5BU5D_t2316563989* Skill_get_effectTime_m4060485415 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_effcetEvent(System.Int32[])
extern "C"  void Skill_set_effcetEvent_m2461603063 (Skill_t79944241 * __this, Int32U5BU5D_t3230847821* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Skill::get_effcetEvent()
extern "C"  Int32U5BU5D_t3230847821* Skill_get_effcetEvent_m3378438184 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_effectPlaySpeed(System.Single[])
extern "C"  void Skill_set_effectPlaySpeed_m2079890764 (Skill_t79944241 * __this, SingleU5BU5D_t2316563989* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] Skill::get_effectPlaySpeed()
extern "C"  SingleU5BU5D_t2316563989* Skill_get_effectPlaySpeed_m4177253051 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_hitEffect(System.Int32)
extern "C"  void Skill_set_hitEffect_m258560272 (Skill_t79944241 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Skill::get_hitEffect()
extern "C"  int32_t Skill_get_hitEffect_m857410241 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_hitEffectTime(System.Single)
extern "C"  void Skill_set_hitEffectTime_m3958189631 (Skill_t79944241 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Skill::get_hitEffectTime()
extern "C"  float Skill_get_hitEffectTime_m3582841772 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_zoominoffset(UnityEngine.Vector3)
extern "C"  void Skill_set_zoominoffset_m1209230087 (Skill_t79944241 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Skill::get_zoominoffset()
extern "C"  Vector3_t4282066566  Skill_get_zoominoffset_m2803785188 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_sound(System.Int32[])
extern "C"  void Skill_set_sound_m3003012409 (Skill_t79944241 * __this, Int32U5BU5D_t3230847821* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Skill::get_sound()
extern "C"  Int32U5BU5D_t3230847821* Skill_get_sound_m2194629610 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_soundTime(System.Single[])
extern "C"  void Skill_set_soundTime_m2432503666 (Skill_t79944241 * __this, SingleU5BU5D_t2316563989* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] Skill::get_soundTime()
extern "C"  SingleU5BU5D_t2316563989* Skill_get_soundTime_m3472140245 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_skillLevel(System.UInt32)
extern "C"  void Skill_set_skillLevel_m2059398976 (Skill_t79944241 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Skill::get_skillLevel()
extern "C"  uint32_t Skill_get_skillLevel_m2866046731 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_damage_add_per(System.Single)
extern "C"  void Skill_set_damage_add_per_m3712865141 (Skill_t79944241 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Skill::get_damage_add_per()
extern "C"  float Skill_get_damage_add_per_m3225765174 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_damage_add_value(System.Int32)
extern "C"  void Skill_set_damage_add_value_m2074663323 (Skill_t79944241 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Skill::get_damage_add_value()
extern "C"  int32_t Skill_get_damage_add_value_m1687979144 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_buffHitPer(System.Int32)
extern "C"  void Skill_set_buffHitPer_m4006517717 (Skill_t79944241 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Skill::get_buffHitPer()
extern "C"  int32_t Skill_get_buffHitPer_m1215927362 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_buffid(System.Int32[])
extern "C"  void Skill_set_buffid_m1611463844 (Skill_t79944241 * __this, Int32U5BU5D_t3230847821* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Skill::get_buffid()
extern "C"  Int32U5BU5D_t3230847821* Skill_get_buffid_m344472533 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_buffprob(System.Int32[])
extern "C"  void Skill_set_buffprob_m3869228190 (Skill_t79944241 * __this, Int32U5BU5D_t3230847821* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Skill::get_buffprob()
extern "C"  Int32U5BU5D_t3230847821* Skill_get_buffprob_m541126991 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_buffevent(FightEnum.ESkillBuffEvent[])
extern "C"  void Skill_set_buffevent_m3341122736 (Skill_t79944241 * __this, ESkillBuffEventU5BU5D_t2768361140* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightEnum.ESkillBuffEvent[] Skill::get_buffevent()
extern "C"  ESkillBuffEventU5BU5D_t2768361140* Skill_get_buffevent_m3338189705 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_bufftarget(FightEnum.ESkillBuffTarget[])
extern "C"  void Skill_set_bufftarget_m1196899644 (Skill_t79944241 * __this, ESkillBuffTargetU5BU5D_t2691263849* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightEnum.ESkillBuffTarget[] Skill::get_bufftarget()
extern "C"  ESkillBuffTargetU5BU5D_t2691263849* Skill_get_bufftarget_m612621481 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_buffbreak(System.Boolean[])
extern "C"  void Skill_set_buffbreak_m3300889244 (Skill_t79944241 * __this, BooleanU5BU5D_t3456302923* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean[] Skill::get_buffbreak()
extern "C"  BooleanU5BU5D_t3456302923* Skill_get_buffbreak_m3105247693 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_isProgressBar(System.Boolean)
extern "C"  void Skill_set_isProgressBar_m3229799566 (Skill_t79944241 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Skill::get_isProgressBar()
extern "C"  bool Skill_get_isProgressBar_m667149311 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_breakskill(System.Boolean)
extern "C"  void Skill_set_breakskill_m565489392 (Skill_t79944241 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Skill::get_breakskill()
extern "C"  bool Skill_get_breakskill_m510226833 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_summonid(System.Int32[])
extern "C"  void Skill_set_summonid_m2418940978 (Skill_t79944241 * __this, Int32U5BU5D_t3230847821* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Skill::get_summonid()
extern "C"  Int32U5BU5D_t3230847821* Skill_get_summonid_m2695193571 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_summonTime(System.Single[])
extern "C"  void Skill_set_summonTime_m3669447828 (Skill_t79944241 * __this, SingleU5BU5D_t2316563989* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] Skill::get_summonTime()
extern "C"  SingleU5BU5D_t2316563989* Skill_get_summonTime_m141681271 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_summonAttackper(System.Int32[])
extern "C"  void Skill_set_summonAttackper_m22776734 (Skill_t79944241 * __this, Int32U5BU5D_t3230847821* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Skill::get_summonAttackper()
extern "C"  Int32U5BU5D_t3230847821* Skill_get_summonAttackper_m1831290639 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_callAttributeKey1(System.String)
extern "C"  void Skill_set_callAttributeKey1_m3624659447 (Skill_t79944241 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Skill::get_callAttributeKey1()
extern "C"  String_t* Skill_get_callAttributeKey1_m2275571394 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_callAttributeValue1(System.Int32)
extern "C"  void Skill_set_callAttributeValue1_m2238623850 (Skill_t79944241 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Skill::get_callAttributeValue1()
extern "C"  int32_t Skill_get_callAttributeValue1_m1165616795 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_callAttributeKey2(System.String)
extern "C"  void Skill_set_callAttributeKey2_m3114125270 (Skill_t79944241 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Skill::get_callAttributeKey2()
extern "C"  String_t* Skill_get_callAttributeKey2_m2275572355 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_callAttributeValue2(System.Int32)
extern "C"  void Skill_set_callAttributeValue2_m3746175659 (Skill_t79944241 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Skill::get_callAttributeValue2()
extern "C"  int32_t Skill_get_callAttributeValue2_m1165617756 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_isSputter(System.Boolean)
extern "C"  void Skill_set_isSputter_m4088902861 (Skill_t79944241 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Skill::get_isSputter()
extern "C"  bool Skill_get_isSputter_m2458591870 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_sputter_radius(System.Single)
extern "C"  void Skill_set_sputter_radius_m133860440 (Skill_t79944241 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Skill::get_sputter_radius()
extern "C"  float Skill_get_sputter_radius_m1930352499 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_sputter_num(System.Int32)
extern "C"  void Skill_set_sputter_num_m1320796856 (Skill_t79944241 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Skill::get_sputter_num()
extern "C"  int32_t Skill_get_sputter_num_m418029545 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_sputter_hurt(System.Single)
extern "C"  void Skill_set_sputter_hurt_m738990907 (Skill_t79944241 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Skill::get_sputter_hurt()
extern "C"  float Skill_get_sputter_hurt_m422880176 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_effectCount(System.Int32)
extern "C"  void Skill_set_effectCount_m3273222154 (Skill_t79944241 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Skill::get_effectCount()
extern "C"  int32_t Skill_get_effectCount_m1512714299 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_CD(System.Single)
extern "C"  void Skill_set_CD_m2858142595 (Skill_t79944241 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Skill::get_CD()
extern "C"  float Skill_get_CD_m4203228776 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_isFingdPos(System.Boolean)
extern "C"  void Skill_set_isFingdPos_m2481110164 (Skill_t79944241 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Skill::get_isFingdPos()
extern "C"  bool Skill_get_isFingdPos_m2783997493 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_blackShowIDs(System.Int32[])
extern "C"  void Skill_set_blackShowIDs_m2453187858 (Skill_t79944241 * __this, Int32U5BU5D_t3230847821* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Skill::get_blackShowIDs()
extern "C"  Int32U5BU5D_t3230847821* Skill_get_blackShowIDs_m2826611907 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_activeSpacing(System.Single[])
extern "C"  void Skill_set_activeSpacing_m1976500401 (Skill_t79944241 * __this, SingleU5BU5D_t2316563989* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] Skill::get_activeSpacing()
extern "C"  SingleU5BU5D_t2316563989* Skill_get_activeSpacing_m884605302 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_isThump(System.Boolean)
extern "C"  void Skill_set_isThump_m3043653580 (Skill_t79944241 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Skill::get_isThump()
extern "C"  bool Skill_get_isThump_m4026173885 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::set_thumpAddValue(System.Single)
extern "C"  void Skill_set_thumpAddValue_m1786374268 (Skill_t79944241 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Skill::get_thumpAddValue()
extern "C"  float Skill_get_thumpAddValue_m2240690767 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::RudeceCD(System.Single)
extern "C"  void Skill_RudeceCD_m3669516186 (Skill_t79944241 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Skill::get_isInCD()
extern "C"  bool Skill_get_isInCD_m234607855 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Skill::isInBlackByEffectID(System.Int32)
extern "C"  bool Skill_isInBlackByEffectID_m1811327856 (Skill_t79944241 * __this, int32_t ___effectID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::InitActiveSpacing()
extern "C"  void Skill_InitActiveSpacing_m3982219493 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::InitAttribute(System.Int32,System.Int32,System.UInt32,System.Boolean,System.Single,System.Int32,System.Int32,System.Single)
extern "C"  void Skill_InitAttribute_m3391864369 (Skill_t79944241 * __this, int32_t ___id0, int32_t ___index1, uint32_t ___lv2, bool ___isActive3, float ___damageAddPer4, int32_t ___damageAddValue5, int32_t ___buffHitPer6, float ___cd7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::InintSputter(System.Boolean,System.Single,System.Int32,System.Single)
extern "C"  void Skill_InintSputter_m1791438715 (Skill_t79944241 * __this, bool ___isSputter0, float ___radius1, int32_t ___num2, float ___hurt3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::InitAction(System.String,System.String,System.Boolean,System.Boolean)
extern "C"  void Skill_InitAction_m2989816014 (Skill_t79944241 * __this, String_t* ____action0, String_t* ____actiontime1, bool ____isprogressBar2, bool ____breakskill3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::InitSound(System.String,System.String)
extern "C"  void Skill_InitSound_m4089740103 (Skill_t79944241 * __this, String_t* ____sound0, String_t* ____soundtime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::InitEffectSing(System.String,System.String,System.String)
extern "C"  void Skill_InitEffectSing_m2894347104 (Skill_t79944241 * __this, String_t* ____effectsing0, String_t* ____singshowtime1, String_t* ____singpalyspeed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::InitEffectBlack(System.String)
extern "C"  void Skill_InitEffectBlack_m550752716 (Skill_t79944241 * __this, String_t* ____blackeffect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::InitEffectSkill(System.String,System.String,System.String,System.String)
extern "C"  void Skill_InitEffectSkill_m3798085230 (Skill_t79944241 * __this, String_t* ____effectskill0, String_t* ____skillshowtime1, String_t* ____effectskillevent2, String_t* ____skillpalyspeed3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::InitSummonid(System.String,System.String,System.String,System.String,System.Int32,System.String,System.Int32)
extern "C"  void Skill_InitSummonid_m1695887812 (Skill_t79944241 * __this, String_t* ____summonid0, String_t* ____summontime1, String_t* ____summon_attackper2, String_t* ___key13, int32_t ___value14, String_t* ___key25, int32_t ___value26, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::InitZoomintime(System.Single,System.String)
extern "C"  void Skill_InitZoomintime_m4122585064 (Skill_t79944241 * __this, float ____zoomintime0, String_t* ____zoominoffset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::InitBuff(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void Skill_InitBuff_m2588843937 (Skill_t79944241 * __this, String_t* ____buffid0, String_t* ____buffprob1, String_t* ____bufftarget2, String_t* ____buffevent3, String_t* ____buffbrek4, String_t* ____awakenbuffprob5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Skill::CheckUseDistance(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  bool Skill_CheckUseDistance_m903694759 (Skill_t79944241 * __this, Vector3_t4282066566  ___selfPos0, Vector3_t4282066566  ___targetPos1, float ___halfwidth2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::OnUseing()
extern "C"  void Skill_OnUseing_m1872627812 (Skill_t79944241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Skill::ilo_get_mTplID1(Skill)
extern "C"  int32_t Skill_ilo_get_mTplID1_m2073010628 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// skillCfg Skill::ilo_GetskillCfg2(CSDatacfgManager,System.Int32)
extern "C"  skillCfg_t2142425171 * Skill_ilo_GetskillCfg2_m783947902 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] Skill::ilo_get_actionTime3(Skill)
extern "C"  SingleU5BU5D_t2316563989* Skill_ilo_get_actionTime3_m2003120107 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Skill::ilo_get_mLastUseTime4(Skill)
extern "C"  float Skill_ilo_get_mLastUseTime4_m286971222 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Skill::ilo_get_CD5(Skill)
extern "C"  float Skill_ilo_get_CD5_m960682757 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Skill::ilo_get_blackShowIDs6(Skill)
extern "C"  Int32U5BU5D_t3230847821* Skill_ilo_get_blackShowIDs6_m4291834431 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] Skill::ilo_GetArticle_Float7(System.String)
extern "C"  SingleU5BU5D_t2316563989* Skill_ilo_GetArticle_Float7_m96396985 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::ilo_set_activeSpacing8(Skill,System.Single[])
extern "C"  void Skill_ilo_set_activeSpacing8_m2030775695 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, SingleU5BU5D_t2316563989* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// skillCfg Skill::ilo_get_mpJson9(Skill)
extern "C"  skillCfg_t2142425171 * Skill_ilo_get_mpJson9_m1645271949 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] Skill::ilo_get_activeSpacing10(Skill)
extern "C"  SingleU5BU5D_t2316563989* Skill_ilo_get_activeSpacing10_m155789855 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::ilo_set_mTplID11(Skill,System.Int32)
extern "C"  void Skill_ilo_set_mTplID11_m1322036750 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::ilo_set_isActive12(Skill,System.Boolean)
extern "C"  void Skill_ilo_set_isActive12_m2700841639 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::ilo_set_damage_add_value13(Skill,System.Int32)
extern "C"  void Skill_ilo_set_damage_add_value13_m3472800117 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::ilo_set_buffHitPer14(Skill,System.Int32)
extern "C"  void Skill_ilo_set_buffHitPer14_m1464191344 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::ilo_set_sputter_radius15(Skill,System.Single)
extern "C"  void Skill_ilo_set_sputter_radius15_m4118850876 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::ilo_set_sputter_num16(Skill,System.Int32)
extern "C"  void Skill_ilo_set_sputter_num16_m2993808975 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::ilo_set_action17(Skill,System.String[])
extern "C"  void Skill_ilo_set_action17_m3326427109 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, StringU5BU5D_t4054002952* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GlobalGOMgr Skill::ilo_get_GlobalGOMgr18()
extern "C"  GlobalGOMgr_t803081773 * Skill_ilo_get_GlobalGOMgr18_m2247561782 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::ilo_set_soundTime19(Skill,System.Single[])
extern "C"  void Skill_ilo_set_soundTime19_m4278926808 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, SingleU5BU5D_t2316563989* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] Skill::ilo_get_soundTime20(Skill)
extern "C"  SingleU5BU5D_t2316563989* Skill_ilo_get_soundTime20_m1479953249 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::ilo_set_castingEffect21(Skill,System.Int32[])
extern "C"  void Skill_ilo_set_castingEffect21_m4092783375 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, Int32U5BU5D_t3230847821* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::ilo_set_castingEffectTime22(Skill,System.Single[])
extern "C"  void Skill_ilo_set_castingEffectTime22_m2403580219 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, SingleU5BU5D_t2316563989* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] Skill::ilo_get_castingEffectTime23(Skill)
extern "C"  SingleU5BU5D_t2316563989* Skill_ilo_get_castingEffectTime23_m1180236921 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] Skill::ilo_get_castingEffectPlaySpeed24(Skill)
extern "C"  SingleU5BU5D_t2316563989* Skill_ilo_get_castingEffectPlaySpeed24_m1430036036 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::ilo_set_effect25(Skill,System.Int32[])
extern "C"  void Skill_ilo_set_effect25_m3575247874 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, Int32U5BU5D_t3230847821* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Skill::ilo_get_effect26(Skill)
extern "C"  Int32U5BU5D_t3230847821* Skill_ilo_get_effect26_m2229751314 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::ilo_set_effectTime27(Skill,System.Single[])
extern "C"  void Skill_ilo_set_effectTime27_m3165584647 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, SingleU5BU5D_t2316563989* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] Skill::ilo_get_effectTime28(Skill)
extern "C"  SingleU5BU5D_t2316563989* Skill_ilo_get_effectTime28_m4248900673 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::ilo_set_effcetEvent29(Skill,System.Int32[])
extern "C"  void Skill_ilo_set_effcetEvent29_m116807792 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, Int32U5BU5D_t3230847821* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Skill::ilo_get_effcetEvent30(Skill)
extern "C"  Int32U5BU5D_t3230847821* Skill_ilo_get_effcetEvent30_m4087582447 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::ilo_set_summonid31(Skill,System.Int32[])
extern "C"  void Skill_ilo_set_summonid31_m4213437128 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, Int32U5BU5D_t3230847821* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::ilo_set_summonAttackper32(Skill,System.Int32[])
extern "C"  void Skill_ilo_set_summonAttackper32_m455890863 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, Int32U5BU5D_t3230847821* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] Skill::ilo_get_summonTime33(Skill)
extern "C"  SingleU5BU5D_t2316563989* Skill_ilo_get_summonTime33_m2051505559 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::ilo_Log34(System.Object,System.Boolean)
extern "C"  void Skill_ilo_Log34_m283492915 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::ilo_set_callAttributeValue135(Skill,System.Int32)
extern "C"  void Skill_ilo_set_callAttributeValue135_m2907112638 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::ilo_set_buffid36(Skill,System.Int32[])
extern "C"  void Skill_ilo_set_buffid36_m2254024767 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, Int32U5BU5D_t3230847821* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skill::ilo_set_buffprob37(Skill,System.Int32[])
extern "C"  void Skill_ilo_set_buffprob37_m1610481338 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, Int32U5BU5D_t3230847821* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Skill::ilo_get_buffprob38(Skill)
extern "C"  Int32U5BU5D_t3230847821* Skill_ilo_get_buffprob38_m7393882 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightEnum.ESkillBuffEvent[] Skill::ilo_get_buffevent39(Skill)
extern "C"  ESkillBuffEventU5BU5D_t2768361140* Skill_ilo_get_buffevent39_m118097029 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
