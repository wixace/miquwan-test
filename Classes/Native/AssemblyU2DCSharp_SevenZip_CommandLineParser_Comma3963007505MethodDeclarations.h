﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.CommandLineParser.CommandForm
struct CommandForm_t3963007505;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void SevenZip.CommandLineParser.CommandForm::.ctor(System.String,System.Boolean)
extern "C"  void CommandForm__ctor_m173427345 (CommandForm_t3963007505 * __this, String_t* ___idString0, bool ___postStringMode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
