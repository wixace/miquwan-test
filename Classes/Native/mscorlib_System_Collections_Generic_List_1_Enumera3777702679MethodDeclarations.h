﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Mihua.Asset.ABLoadOperation.AssetOperationPreload1>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m619700404(__this, ___l0, method) ((  void (*) (Enumerator_t3777702679 *, List_1_t3758029909 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Mihua.Asset.ABLoadOperation.AssetOperationPreload1>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m513446686(__this, method) ((  void (*) (Enumerator_t3777702679 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Mihua.Asset.ABLoadOperation.AssetOperationPreload1>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2051494484(__this, method) ((  Il2CppObject * (*) (Enumerator_t3777702679 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Mihua.Asset.ABLoadOperation.AssetOperationPreload1>::Dispose()
#define Enumerator_Dispose_m4070103961(__this, method) ((  void (*) (Enumerator_t3777702679 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Mihua.Asset.ABLoadOperation.AssetOperationPreload1>::VerifyState()
#define Enumerator_VerifyState_m1716725330(__this, method) ((  void (*) (Enumerator_t3777702679 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Mihua.Asset.ABLoadOperation.AssetOperationPreload1>::MoveNext()
#define Enumerator_MoveNext_m693634382(__this, method) ((  bool (*) (Enumerator_t3777702679 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Mihua.Asset.ABLoadOperation.AssetOperationPreload1>::get_Current()
#define Enumerator_get_Current_m1722331563(__this, method) ((  AssetOperationPreload1_t2389844357 * (*) (Enumerator_t3777702679 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
