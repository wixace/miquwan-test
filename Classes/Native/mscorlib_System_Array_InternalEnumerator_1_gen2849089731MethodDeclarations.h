﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2849089731.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066747055.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3703280468_gshared (InternalEnumerator_1_t2849089731 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3703280468(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2849089731 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3703280468_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1457920460_gshared (InternalEnumerator_1_t2849089731 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1457920460(__this, method) ((  void (*) (InternalEnumerator_1_t2849089731 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1457920460_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1418624696_gshared (InternalEnumerator_1_t2849089731 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1418624696(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2849089731 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1418624696_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m864384555_gshared (InternalEnumerator_1_t2849089731 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m864384555(__this, method) ((  void (*) (InternalEnumerator_1_t2849089731 *, const MethodInfo*))InternalEnumerator_1_Dispose_m864384555_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3624630072_gshared (InternalEnumerator_1_t2849089731 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3624630072(__this, method) ((  bool (*) (InternalEnumerator_1_t2849089731 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3624630072_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt32,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t4066747055  InternalEnumerator_1_get_Current_m2456983963_gshared (InternalEnumerator_1_t2849089731 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2456983963(__this, method) ((  KeyValuePair_2_t4066747055  (*) (InternalEnumerator_1_t2849089731 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2456983963_gshared)(__this, method)
