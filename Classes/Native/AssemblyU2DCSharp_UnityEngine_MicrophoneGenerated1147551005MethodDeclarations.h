﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_MicrophoneGenerated
struct UnityEngine_MicrophoneGenerated_t1147551005;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_MicrophoneGenerated::.ctor()
extern "C"  void UnityEngine_MicrophoneGenerated__ctor_m1887251182 (UnityEngine_MicrophoneGenerated_t1147551005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MicrophoneGenerated::Microphone_Microphone1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MicrophoneGenerated_Microphone_Microphone1_m3817079236 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MicrophoneGenerated::Microphone_devices(JSVCall)
extern "C"  void UnityEngine_MicrophoneGenerated_Microphone_devices_m2270315337 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MicrophoneGenerated::Microphone_End__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MicrophoneGenerated_Microphone_End__String_m1812916649 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MicrophoneGenerated::Microphone_GetDeviceCaps__String__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MicrophoneGenerated_Microphone_GetDeviceCaps__String__Int32__Int32_m1599294299 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MicrophoneGenerated::Microphone_GetPosition__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MicrophoneGenerated_Microphone_GetPosition__String_m404642925 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MicrophoneGenerated::Microphone_IsRecording__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MicrophoneGenerated_Microphone_IsRecording__String_m853661621 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MicrophoneGenerated::Microphone_Start__String__Boolean__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MicrophoneGenerated_Microphone_Start__String__Boolean__Int32__Int32_m1767735226 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MicrophoneGenerated::__Register()
extern "C"  void UnityEngine_MicrophoneGenerated___Register_m4243584665 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MicrophoneGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_MicrophoneGenerated_ilo_getObject1_m4103877640 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MicrophoneGenerated::ilo_moveSaveID2Arr2(System.Int32)
extern "C"  void UnityEngine_MicrophoneGenerated_ilo_moveSaveID2Arr2_m3479770564 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MicrophoneGenerated::ilo_setArrayS3(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_MicrophoneGenerated_ilo_setArrayS3_m4101846359 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MicrophoneGenerated::ilo_setInt324(System.Int32,System.Int32)
extern "C"  void UnityEngine_MicrophoneGenerated_ilo_setInt324_m1794839749 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MicrophoneGenerated::ilo_setArgIndex5(System.Int32)
extern "C"  void UnityEngine_MicrophoneGenerated_ilo_setArgIndex5_m1226206929 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MicrophoneGenerated::ilo_setBooleanS6(System.Int32,System.Boolean)
extern "C"  void UnityEngine_MicrophoneGenerated_ilo_setBooleanS6_m2246198522 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MicrophoneGenerated::ilo_getInt327(System.Int32)
extern "C"  int32_t UnityEngine_MicrophoneGenerated_ilo_getInt327_m850376215 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
