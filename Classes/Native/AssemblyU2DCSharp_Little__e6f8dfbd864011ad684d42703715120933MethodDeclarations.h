﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._e6f8dfbd864011ad684d427047751f09
struct _e6f8dfbd864011ad684d427047751f09_t3715120933;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__e6f8dfbd864011ad684d42703715120933.h"

// System.Void Little._e6f8dfbd864011ad684d427047751f09::.ctor()
extern "C"  void _e6f8dfbd864011ad684d427047751f09__ctor_m1573331112 (_e6f8dfbd864011ad684d427047751f09_t3715120933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e6f8dfbd864011ad684d427047751f09::_e6f8dfbd864011ad684d427047751f09m2(System.Int32)
extern "C"  int32_t _e6f8dfbd864011ad684d427047751f09__e6f8dfbd864011ad684d427047751f09m2_m2532972409 (_e6f8dfbd864011ad684d427047751f09_t3715120933 * __this, int32_t ____e6f8dfbd864011ad684d427047751f09a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e6f8dfbd864011ad684d427047751f09::_e6f8dfbd864011ad684d427047751f09m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _e6f8dfbd864011ad684d427047751f09__e6f8dfbd864011ad684d427047751f09m_m7531357 (_e6f8dfbd864011ad684d427047751f09_t3715120933 * __this, int32_t ____e6f8dfbd864011ad684d427047751f09a0, int32_t ____e6f8dfbd864011ad684d427047751f09981, int32_t ____e6f8dfbd864011ad684d427047751f09c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e6f8dfbd864011ad684d427047751f09::ilo__e6f8dfbd864011ad684d427047751f09m21(Little._e6f8dfbd864011ad684d427047751f09,System.Int32)
extern "C"  int32_t _e6f8dfbd864011ad684d427047751f09_ilo__e6f8dfbd864011ad684d427047751f09m21_m4277188748 (Il2CppObject * __this /* static, unused */, _e6f8dfbd864011ad684d427047751f09_t3715120933 * ____this0, int32_t ____e6f8dfbd864011ad684d427047751f09a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
