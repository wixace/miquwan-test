﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UITweener>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m517750795(__this, ___l0, method) ((  void (*) (Enumerator_t1493347510 *, List_1_t1473674740 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UITweener>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3314006375(__this, method) ((  void (*) (Enumerator_t1493347510 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UITweener>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2877906643(__this, method) ((  Il2CppObject * (*) (Enumerator_t1493347510 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UITweener>::Dispose()
#define Enumerator_Dispose_m509952176(__this, method) ((  void (*) (Enumerator_t1493347510 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UITweener>::VerifyState()
#define Enumerator_VerifyState_m1554589673(__this, method) ((  void (*) (Enumerator_t1493347510 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UITweener>::MoveNext()
#define Enumerator_MoveNext_m1141188819(__this, method) ((  bool (*) (Enumerator_t1493347510 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UITweener>::get_Current()
#define Enumerator_get_Current_m3524106080(__this, method) ((  UITweener_t105489188 * (*) (Enumerator_t1493347510 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
