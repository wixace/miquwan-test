﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>
struct DefaultComparer_t822616353;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_MeshSubsetCombineUtility_S2374839686.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::.ctor()
extern "C"  void DefaultComparer__ctor_m2380955250_gshared (DefaultComparer_t822616353 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2380955250(__this, method) ((  void (*) (DefaultComparer_t822616353 *, const MethodInfo*))DefaultComparer__ctor_m2380955250_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3110785601_gshared (DefaultComparer_t822616353 * __this, SubMeshInstance_t2374839686  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3110785601(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t822616353 *, SubMeshInstance_t2374839686 , const MethodInfo*))DefaultComparer_GetHashCode_m3110785601_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1084704903_gshared (DefaultComparer_t822616353 * __this, SubMeshInstance_t2374839686  ___x0, SubMeshInstance_t2374839686  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1084704903(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t822616353 *, SubMeshInstance_t2374839686 , SubMeshInstance_t2374839686 , const MethodInfo*))DefaultComparer_Equals_m1084704903_gshared)(__this, ___x0, ___y1, method)
