﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<GestureRecognizer>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1993451220(__this, ___l0, method) ((  void (*) (Enumerator_t605766975 *, List_1_t586094205 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GestureRecognizer>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1576892670(__this, method) ((  void (*) (Enumerator_t605766975 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<GestureRecognizer>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3183015466(__this, method) ((  Il2CppObject * (*) (Enumerator_t605766975 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GestureRecognizer>::Dispose()
#define Enumerator_Dispose_m2251696569(__this, method) ((  void (*) (Enumerator_t605766975 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GestureRecognizer>::VerifyState()
#define Enumerator_VerifyState_m811361394(__this, method) ((  void (*) (Enumerator_t605766975 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<GestureRecognizer>::MoveNext()
#define Enumerator_MoveNext_m3595170593(__this, method) ((  bool (*) (Enumerator_t605766975 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<GestureRecognizer>::get_Current()
#define Enumerator_get_Current_m4039184975(__this, method) ((  GestureRecognizer_t3512875949 * (*) (Enumerator_t605766975 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
