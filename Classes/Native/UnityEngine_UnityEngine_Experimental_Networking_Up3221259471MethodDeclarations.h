﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Experimental.Networking.UploadHandler
struct UploadHandler_t3221259471;
struct UploadHandler_t3221259471_marshaled_pinvoke;
struct UploadHandler_t3221259471_marshaled_com;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Experimental.Networking.UploadHandler::InternalDestroy()
extern "C"  void UploadHandler_InternalDestroy_m3441844159 (UploadHandler_t3221259471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.UploadHandler::Finalize()
extern "C"  void UploadHandler_Finalize_m2567806270 (UploadHandler_t3221259471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.UploadHandler::Dispose()
extern "C"  void UploadHandler_Dispose_m4272776161 (UploadHandler_t3221259471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct UploadHandler_t3221259471;
struct UploadHandler_t3221259471_marshaled_pinvoke;

extern "C" void UploadHandler_t3221259471_marshal_pinvoke(const UploadHandler_t3221259471& unmarshaled, UploadHandler_t3221259471_marshaled_pinvoke& marshaled);
extern "C" void UploadHandler_t3221259471_marshal_pinvoke_back(const UploadHandler_t3221259471_marshaled_pinvoke& marshaled, UploadHandler_t3221259471& unmarshaled);
extern "C" void UploadHandler_t3221259471_marshal_pinvoke_cleanup(UploadHandler_t3221259471_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct UploadHandler_t3221259471;
struct UploadHandler_t3221259471_marshaled_com;

extern "C" void UploadHandler_t3221259471_marshal_com(const UploadHandler_t3221259471& unmarshaled, UploadHandler_t3221259471_marshaled_com& marshaled);
extern "C" void UploadHandler_t3221259471_marshal_com_back(const UploadHandler_t3221259471_marshaled_com& marshaled, UploadHandler_t3221259471& unmarshaled);
extern "C" void UploadHandler_t3221259471_marshal_com_cleanup(UploadHandler_t3221259471_marshaled_com& marshaled);
