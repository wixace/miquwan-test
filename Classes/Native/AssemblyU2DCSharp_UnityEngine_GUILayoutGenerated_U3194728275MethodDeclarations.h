﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUILayoutGenerated/<GUILayout_Window_GetDelegate_member102_arg2>c__AnonStoreyF4
struct U3CGUILayout_Window_GetDelegate_member102_arg2U3Ec__AnonStoreyF4_t194728275;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_GUILayoutGenerated/<GUILayout_Window_GetDelegate_member102_arg2>c__AnonStoreyF4::.ctor()
extern "C"  void U3CGUILayout_Window_GetDelegate_member102_arg2U3Ec__AnonStoreyF4__ctor_m936175480 (U3CGUILayout_Window_GetDelegate_member102_arg2U3Ec__AnonStoreyF4_t194728275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUILayoutGenerated/<GUILayout_Window_GetDelegate_member102_arg2>c__AnonStoreyF4::<>m__239(System.Int32)
extern "C"  void U3CGUILayout_Window_GetDelegate_member102_arg2U3Ec__AnonStoreyF4_U3CU3Em__239_m1626829226 (U3CGUILayout_Window_GetDelegate_member102_arg2U3Ec__AnonStoreyF4_t194728275 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
