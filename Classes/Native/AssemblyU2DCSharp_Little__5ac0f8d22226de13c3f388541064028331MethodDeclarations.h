﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._5ac0f8d22226de13c3f38854ca5b51a0
struct _5ac0f8d22226de13c3f38854ca5b51a0_t1064028331;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._5ac0f8d22226de13c3f38854ca5b51a0::.ctor()
extern "C"  void _5ac0f8d22226de13c3f38854ca5b51a0__ctor_m841412066 (_5ac0f8d22226de13c3f38854ca5b51a0_t1064028331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._5ac0f8d22226de13c3f38854ca5b51a0::_5ac0f8d22226de13c3f38854ca5b51a0m2(System.Int32)
extern "C"  int32_t _5ac0f8d22226de13c3f38854ca5b51a0__5ac0f8d22226de13c3f38854ca5b51a0m2_m131103161 (_5ac0f8d22226de13c3f38854ca5b51a0_t1064028331 * __this, int32_t ____5ac0f8d22226de13c3f38854ca5b51a0a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._5ac0f8d22226de13c3f38854ca5b51a0::_5ac0f8d22226de13c3f38854ca5b51a0m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _5ac0f8d22226de13c3f38854ca5b51a0__5ac0f8d22226de13c3f38854ca5b51a0m_m3028521245 (_5ac0f8d22226de13c3f38854ca5b51a0_t1064028331 * __this, int32_t ____5ac0f8d22226de13c3f38854ca5b51a0a0, int32_t ____5ac0f8d22226de13c3f38854ca5b51a0531, int32_t ____5ac0f8d22226de13c3f38854ca5b51a0c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
