﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_kelkouPirboca98
struct M_kelkouPirboca98_t2581829796;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_kelkouPirboca98::.ctor()
extern "C"  void M_kelkouPirboca98__ctor_m2564656959 (M_kelkouPirboca98_t2581829796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kelkouPirboca98::M_foosojear0(System.String[],System.Int32)
extern "C"  void M_kelkouPirboca98_M_foosojear0_m2799898256 (M_kelkouPirboca98_t2581829796 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
