﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.DefaultContractResolver/<CreateISerializableContract>c__AnonStorey131
struct U3CCreateISerializableContractU3Ec__AnonStorey131_t2843904219;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<CreateISerializableContract>c__AnonStorey131::.ctor()
extern "C"  void U3CCreateISerializableContractU3Ec__AnonStorey131__ctor_m2182758640 (U3CCreateISerializableContractU3Ec__AnonStorey131_t2843904219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.DefaultContractResolver/<CreateISerializableContract>c__AnonStorey131::<>m__385(System.Object[])
extern "C"  Il2CppObject * U3CCreateISerializableContractU3Ec__AnonStorey131_U3CU3Em__385_m3903325390 (U3CCreateISerializableContractU3Ec__AnonStorey131_t2843904219 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
