﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.RaycastHit>
struct List_1_t1076393982;
// System.Collections.Generic.IEnumerable`1<UnityEngine.RaycastHit>
struct IEnumerable_1_t3009121387;
// System.Collections.Generic.IEnumerator`1<UnityEngine.RaycastHit>
struct IEnumerator_1_t1620073479;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UnityEngine.RaycastHit>
struct ICollection_1_t602798417;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RaycastHit>
struct ReadOnlyCollection_1_t1265285966;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t528650843;
// System.Predicate`1<UnityEngine.RaycastHit>
struct Predicate_1_t3614232609;
// System.Collections.Generic.IComparer`1<UnityEngine.RaycastHit>
struct IComparer_1_t2283222472;
// System.Comparison`1<UnityEngine.RaycastHit>
struct Comparison_1_t2719536913;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1096066752.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::.ctor()
extern "C"  void List_1__ctor_m993226956_gshared (List_1_t1076393982 * __this, const MethodInfo* method);
#define List_1__ctor_m993226956(__this, method) ((  void (*) (List_1_t1076393982 *, const MethodInfo*))List_1__ctor_m993226956_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1471146826_gshared (List_1_t1076393982 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1471146826(__this, ___collection0, method) ((  void (*) (List_1_t1076393982 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1471146826_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m2030773222_gshared (List_1_t1076393982 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m2030773222(__this, ___capacity0, method) ((  void (*) (List_1_t1076393982 *, int32_t, const MethodInfo*))List_1__ctor_m2030773222_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::.cctor()
extern "C"  void List_1__cctor_m4248414264_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m4248414264(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m4248414264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m331452895_gshared (List_1_t1076393982 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m331452895(__this, method) ((  Il2CppObject* (*) (List_1_t1076393982 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m331452895_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m559930575_gshared (List_1_t1076393982 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m559930575(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1076393982 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m559930575_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1228255006_gshared (List_1_t1076393982 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1228255006(__this, method) ((  Il2CppObject * (*) (List_1_t1076393982 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1228255006_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2068433759_gshared (List_1_t1076393982 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2068433759(__this, ___item0, method) ((  int32_t (*) (List_1_t1076393982 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2068433759_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1265598401_gshared (List_1_t1076393982 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1265598401(__this, ___item0, method) ((  bool (*) (List_1_t1076393982 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1265598401_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2414295607_gshared (List_1_t1076393982 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m2414295607(__this, ___item0, method) ((  int32_t (*) (List_1_t1076393982 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m2414295607_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m1998987818_gshared (List_1_t1076393982 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1998987818(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1076393982 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1998987818_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m55528126_gshared (List_1_t1076393982 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m55528126(__this, ___item0, method) ((  void (*) (List_1_t1076393982 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m55528126_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1634955906_gshared (List_1_t1076393982 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1634955906(__this, method) ((  bool (*) (List_1_t1076393982 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1634955906_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m2214015995_gshared (List_1_t1076393982 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2214015995(__this, method) ((  bool (*) (List_1_t1076393982 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m2214015995_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m612421741_gshared (List_1_t1076393982 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m612421741(__this, method) ((  Il2CppObject * (*) (List_1_t1076393982 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m612421741_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1946047472_gshared (List_1_t1076393982 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1946047472(__this, method) ((  bool (*) (List_1_t1076393982 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1946047472_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3564825865_gshared (List_1_t1076393982 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m3564825865(__this, method) ((  bool (*) (List_1_t1076393982 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3564825865_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1192385204_gshared (List_1_t1076393982 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1192385204(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1076393982 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1192385204_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m1790864257_gshared (List_1_t1076393982 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1790864257(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1076393982 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1790864257_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Add(T)
extern "C"  void List_1_Add_m687587772_gshared (List_1_t1076393982 * __this, RaycastHit_t4003175726  ___item0, const MethodInfo* method);
#define List_1_Add_m687587772(__this, ___item0, method) ((  void (*) (List_1_t1076393982 *, RaycastHit_t4003175726 , const MethodInfo*))List_1_Add_m687587772_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2909137157_gshared (List_1_t1076393982 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2909137157(__this, ___newCount0, method) ((  void (*) (List_1_t1076393982 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2909137157_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m400610754_gshared (List_1_t1076393982 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m400610754(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t1076393982 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m400610754_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3584617475_gshared (List_1_t1076393982 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3584617475(__this, ___collection0, method) ((  void (*) (List_1_t1076393982 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3584617475_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2845589699_gshared (List_1_t1076393982 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2845589699(__this, ___enumerable0, method) ((  void (*) (List_1_t1076393982 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2845589699_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m844759028_gshared (List_1_t1076393982 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m844759028(__this, ___collection0, method) ((  void (*) (List_1_t1076393982 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m844759028_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.RaycastHit>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1265285966 * List_1_AsReadOnly_m3812640785_gshared (List_1_t1076393982 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3812640785(__this, method) ((  ReadOnlyCollection_1_t1265285966 * (*) (List_1_t1076393982 *, const MethodInfo*))List_1_AsReadOnly_m3812640785_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.RaycastHit>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m727170082_gshared (List_1_t1076393982 * __this, RaycastHit_t4003175726  ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m727170082(__this, ___item0, method) ((  int32_t (*) (List_1_t1076393982 *, RaycastHit_t4003175726 , const MethodInfo*))List_1_BinarySearch_m727170082_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Clear()
extern "C"  void List_1_Clear_m2823529024_gshared (List_1_t1076393982 * __this, const MethodInfo* method);
#define List_1_Clear_m2823529024(__this, method) ((  void (*) (List_1_t1076393982 *, const MethodInfo*))List_1_Clear_m2823529024_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Contains(T)
extern "C"  bool List_1_Contains_m1590551730_gshared (List_1_t1076393982 * __this, RaycastHit_t4003175726  ___item0, const MethodInfo* method);
#define List_1_Contains_m1590551730(__this, ___item0, method) ((  bool (*) (List_1_t1076393982 *, RaycastHit_t4003175726 , const MethodInfo*))List_1_Contains_m1590551730_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1761398074_gshared (List_1_t1076393982 * __this, RaycastHitU5BU5D_t528650843* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m1761398074(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1076393982 *, RaycastHitU5BU5D_t528650843*, int32_t, const MethodInfo*))List_1_CopyTo_m1761398074_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Find(System.Predicate`1<T>)
extern "C"  RaycastHit_t4003175726  List_1_Find_m347702860_gshared (List_1_t1076393982 * __this, Predicate_1_t3614232609 * ___match0, const MethodInfo* method);
#define List_1_Find_m347702860(__this, ___match0, method) ((  RaycastHit_t4003175726  (*) (List_1_t1076393982 *, Predicate_1_t3614232609 *, const MethodInfo*))List_1_Find_m347702860_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m2298450665_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3614232609 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m2298450665(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3614232609 *, const MethodInfo*))List_1_CheckMatch_m2298450665_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.RaycastHit>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3340538566_gshared (List_1_t1076393982 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3614232609 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3340538566(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1076393982 *, int32_t, int32_t, Predicate_1_t3614232609 *, const MethodInfo*))List_1_GetIndex_m3340538566_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.RaycastHit>::GetEnumerator()
extern "C"  Enumerator_t1096066752  List_1_GetEnumerator_m3405348079_gshared (List_1_t1076393982 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3405348079(__this, method) ((  Enumerator_t1096066752  (*) (List_1_t1076393982 *, const MethodInfo*))List_1_GetEnumerator_m3405348079_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.RaycastHit>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m4248695686_gshared (List_1_t1076393982 * __this, RaycastHit_t4003175726  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m4248695686(__this, ___item0, method) ((  int32_t (*) (List_1_t1076393982 *, RaycastHit_t4003175726 , const MethodInfo*))List_1_IndexOf_m4248695686_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2109262225_gshared (List_1_t1076393982 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2109262225(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1076393982 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2109262225_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1507765258_gshared (List_1_t1076393982 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m1507765258(__this, ___index0, method) ((  void (*) (List_1_t1076393982 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1507765258_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m1275128369_gshared (List_1_t1076393982 * __this, int32_t ___index0, RaycastHit_t4003175726  ___item1, const MethodInfo* method);
#define List_1_Insert_m1275128369(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1076393982 *, int32_t, RaycastHit_t4003175726 , const MethodInfo*))List_1_Insert_m1275128369_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m780290534_gshared (List_1_t1076393982 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m780290534(__this, ___collection0, method) ((  void (*) (List_1_t1076393982 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m780290534_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Remove(T)
extern "C"  bool List_1_Remove_m725100845_gshared (List_1_t1076393982 * __this, RaycastHit_t4003175726  ___item0, const MethodInfo* method);
#define List_1_Remove_m725100845(__this, ___item0, method) ((  bool (*) (List_1_t1076393982 *, RaycastHit_t4003175726 , const MethodInfo*))List_1_Remove_m725100845_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.RaycastHit>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m3909530313_gshared (List_1_t1076393982 * __this, Predicate_1_t3614232609 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m3909530313(__this, ___match0, method) ((  int32_t (*) (List_1_t1076393982 *, Predicate_1_t3614232609 *, const MethodInfo*))List_1_RemoveAll_m3909530313_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3443948535_gshared (List_1_t1076393982 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3443948535(__this, ___index0, method) ((  void (*) (List_1_t1076393982 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3443948535_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m3564210714_gshared (List_1_t1076393982 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m3564210714(__this, ___index0, ___count1, method) ((  void (*) (List_1_t1076393982 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3564210714_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Reverse()
extern "C"  void List_1_Reverse_m654747925_gshared (List_1_t1076393982 * __this, const MethodInfo* method);
#define List_1_Reverse_m654747925(__this, method) ((  void (*) (List_1_t1076393982 *, const MethodInfo*))List_1_Reverse_m654747925_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Sort()
extern "C"  void List_1_Sort_m1522153133_gshared (List_1_t1076393982 * __this, const MethodInfo* method);
#define List_1_Sort_m1522153133(__this, method) ((  void (*) (List_1_t1076393982 *, const MethodInfo*))List_1_Sort_m1522153133_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1724167639_gshared (List_1_t1076393982 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m1724167639(__this, ___comparer0, method) ((  void (*) (List_1_t1076393982 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1724167639_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1989694272_gshared (List_1_t1076393982 * __this, Comparison_1_t2719536913 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1989694272(__this, ___comparison0, method) ((  void (*) (List_1_t1076393982 *, Comparison_1_t2719536913 *, const MethodInfo*))List_1_Sort_m1989694272_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UnityEngine.RaycastHit>::ToArray()
extern "C"  RaycastHitU5BU5D_t528650843* List_1_ToArray_m1312310356_gshared (List_1_t1076393982 * __this, const MethodInfo* method);
#define List_1_ToArray_m1312310356(__this, method) ((  RaycastHitU5BU5D_t528650843* (*) (List_1_t1076393982 *, const MethodInfo*))List_1_ToArray_m1312310356_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::TrimExcess()
extern "C"  void List_1_TrimExcess_m528408710_gshared (List_1_t1076393982 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m528408710(__this, method) ((  void (*) (List_1_t1076393982 *, const MethodInfo*))List_1_TrimExcess_m528408710_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.RaycastHit>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1586280630_gshared (List_1_t1076393982 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1586280630(__this, method) ((  int32_t (*) (List_1_t1076393982 *, const MethodInfo*))List_1_get_Capacity_m1586280630_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m119325975_gshared (List_1_t1076393982 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m119325975(__this, ___value0, method) ((  void (*) (List_1_t1076393982 *, int32_t, const MethodInfo*))List_1_set_Capacity_m119325975_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.RaycastHit>::get_Count()
extern "C"  int32_t List_1_get_Count_m2883412574_gshared (List_1_t1076393982 * __this, const MethodInfo* method);
#define List_1_get_Count_m2883412574(__this, method) ((  int32_t (*) (List_1_t1076393982 *, const MethodInfo*))List_1_get_Count_m2883412574_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.RaycastHit>::get_Item(System.Int32)
extern "C"  RaycastHit_t4003175726  List_1_get_Item_m576795869_gshared (List_1_t1076393982 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m576795869(__this, ___index0, method) ((  RaycastHit_t4003175726  (*) (List_1_t1076393982 *, int32_t, const MethodInfo*))List_1_get_Item_m576795869_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.RaycastHit>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3142279880_gshared (List_1_t1076393982 * __this, int32_t ___index0, RaycastHit_t4003175726  ___value1, const MethodInfo* method);
#define List_1_set_Item_m3142279880(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1076393982 *, int32_t, RaycastHit_t4003175726 , const MethodInfo*))List_1_set_Item_m3142279880_gshared)(__this, ___index0, ___value1, method)
