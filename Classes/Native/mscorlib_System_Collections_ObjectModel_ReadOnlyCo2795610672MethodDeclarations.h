﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>
struct ReadOnlyCollection_1_t2795610672;
// System.Collections.Generic.IList`1<Entity.Behavior.EBehaviorID>
struct IList_1_t3933180339;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Entity.Behavior.EBehaviorID[]
struct EBehaviorIDU5BU5D_t373889457;
// System.Collections.Generic.IEnumerator`1<Entity.Behavior.EBehaviorID>
struct IEnumerator_1_t3150398185;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2121058735_gshared (ReadOnlyCollection_1_t2795610672 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2121058735(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2795610672 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2121058735_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2019986457_gshared (ReadOnlyCollection_1_t2795610672 * __this, uint8_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2019986457(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2795610672 *, uint8_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2019986457_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m201761745_gshared (ReadOnlyCollection_1_t2795610672 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m201761745(__this, method) ((  void (*) (ReadOnlyCollection_1_t2795610672 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m201761745_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4000940992_gshared (ReadOnlyCollection_1_t2795610672 * __this, int32_t ___index0, uint8_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4000940992(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2795610672 *, int32_t, uint8_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4000940992_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2222448314_gshared (ReadOnlyCollection_1_t2795610672 * __this, uint8_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2222448314(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2795610672 *, uint8_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2222448314_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1874793862_gshared (ReadOnlyCollection_1_t2795610672 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1874793862(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2795610672 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1874793862_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  uint8_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1696067466_gshared (ReadOnlyCollection_1_t2795610672 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1696067466(__this, ___index0, method) ((  uint8_t (*) (ReadOnlyCollection_1_t2795610672 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1696067466_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2718160023_gshared (ReadOnlyCollection_1_t2795610672 * __this, int32_t ___index0, uint8_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2718160023(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2795610672 *, int32_t, uint8_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2718160023_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m52738453_gshared (ReadOnlyCollection_1_t2795610672 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m52738453(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2795610672 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m52738453_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3791646430_gshared (ReadOnlyCollection_1_t2795610672 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3791646430(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2795610672 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3791646430_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2272724825_gshared (ReadOnlyCollection_1_t2795610672 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2272724825(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2795610672 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2272724825_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1357766712_gshared (ReadOnlyCollection_1_t2795610672 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1357766712(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2795610672 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1357766712_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2926573036_gshared (ReadOnlyCollection_1_t2795610672 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2926573036(__this, method) ((  void (*) (ReadOnlyCollection_1_t2795610672 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2926573036_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m4211602068_gshared (ReadOnlyCollection_1_t2795610672 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m4211602068(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2795610672 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m4211602068_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m424884880_gshared (ReadOnlyCollection_1_t2795610672 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m424884880(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2795610672 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m424884880_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1285079739_gshared (ReadOnlyCollection_1_t2795610672 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1285079739(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2795610672 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1285079739_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1622020749_gshared (ReadOnlyCollection_1_t2795610672 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1622020749(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2795610672 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1622020749_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m328391371_gshared (ReadOnlyCollection_1_t2795610672 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m328391371(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2795610672 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m328391371_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3247533128_gshared (ReadOnlyCollection_1_t2795610672 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3247533128(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2795610672 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3247533128_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4057252916_gshared (ReadOnlyCollection_1_t2795610672 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4057252916(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2795610672 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4057252916_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2421039363_gshared (ReadOnlyCollection_1_t2795610672 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2421039363(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2795610672 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2421039363_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1363390870_gshared (ReadOnlyCollection_1_t2795610672 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1363390870(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2795610672 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1363390870_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m255359163_gshared (ReadOnlyCollection_1_t2795610672 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m255359163(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2795610672 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m255359163_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2919967698_gshared (ReadOnlyCollection_1_t2795610672 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2919967698(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2795610672 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2919967698_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m2516069887_gshared (ReadOnlyCollection_1_t2795610672 * __this, uint8_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m2516069887(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2795610672 *, uint8_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m2516069887_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3666565705_gshared (ReadOnlyCollection_1_t2795610672 * __this, EBehaviorIDU5BU5D_t373889457* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m3666565705(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2795610672 *, EBehaviorIDU5BU5D_t373889457*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m3666565705_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m1243995362_gshared (ReadOnlyCollection_1_t2795610672 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1243995362(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2795610672 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1243995362_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1191263117_gshared (ReadOnlyCollection_1_t2795610672 * __this, uint8_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1191263117(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2795610672 *, uint8_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1191263117_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2386725774_gshared (ReadOnlyCollection_1_t2795610672 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m2386725774(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2795610672 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2386725774_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>::get_Item(System.Int32)
extern "C"  uint8_t ReadOnlyCollection_1_get_Item_m34639434_gshared (ReadOnlyCollection_1_t2795610672 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m34639434(__this, ___index0, method) ((  uint8_t (*) (ReadOnlyCollection_1_t2795610672 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m34639434_gshared)(__this, ___index0, method)
