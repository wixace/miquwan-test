﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Schema.XmlSchemaValidator
struct XmlSchemaValidator_t1781729182;
// System.Xml.XmlNameTable
struct XmlNameTable_t1216706026;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_t4125473774;
// System.Xml.IXmlNamespaceResolver
struct IXmlNamespaceResolver_t3774973253;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t4231404781;
// System.Object
struct Il2CppObject;
// System.Xml.IXmlLineInfo
struct IXmlLineInfo_t2849465358;
// System.Xml.XmlResolver
struct XmlResolver_t3822670287;
// System.Uri
struct Uri_t1116831938;
// System.String
struct String_t;
// Mono.Xml.Schema.XsdValidationContext
struct XsdValidationContext_t4179462825;
// System.Xml.Schema.XmlSchemaAttribute[]
struct XmlSchemaAttributeU5BU5D_t589142137;
// System.Collections.ArrayList
struct ArrayList_t3948406897;
// System.Xml.Schema.XmlValueGetter
struct XmlValueGetter_t2234841801;
// System.Xml.Schema.XmlSchemaInfo
struct XmlSchemaInfo_t4089849692;
// System.Exception
struct Exception_t3991598821;
// System.Xml.Schema.XmlSchemaValidationException
struct XmlSchemaValidationException_t2573697378;
// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_t3664762632;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_t4090188264;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2133315502;
// System.Xml.Schema.XmlSchemaComplexType
struct XmlSchemaComplexType_t2974990486;
// System.Xml.Schema.XmlSchemaAttribute
struct XmlSchemaAttribute_t2904598248;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t3060492794;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t196391954;
// Mono.Xml.Schema.XsdKeyTable
struct XsdKeyTable_t539606558;
// System.Xml.Schema.XmlSchemaIdentityConstraint
struct XmlSchemaIdentityConstraint_t3091667497;
// System.Xml.Schema.XmlSchema
struct XmlSchema_t1010706190;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlNameTable1216706026.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSet4125473774.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidationFlag87720602.h"
#include "System_Xml_System_Xml_Schema_ValidationEventHandle4231404781.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Xml_System_Xml_XmlResolver3822670287.h"
#include "System_System_Uri1116831938.h"
#include "mscorlib_System_Collections_ArrayList3948406897.h"
#include "mscorlib_System_String7231557.h"
#include "System_Xml_System_Xml_Schema_XmlValueGetter2234841801.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaInfo4089849692.h"
#include "mscorlib_System_Exception3991598821.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidationEx2573697378.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidator_Tr1435741651.h"
#include "System_Xml_System_Xml_XmlQualifiedName2133315502.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaComplexType2974990486.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttribute2904598248.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleType3060492794.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDatatype196391954.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaIdentityCons3091667497.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyTable539606558.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDerivationMet998439334.h"

// System.Void System.Xml.Schema.XmlSchemaValidator::.ctor(System.Xml.XmlNameTable,System.Xml.Schema.XmlSchemaSet,System.Xml.IXmlNamespaceResolver,System.Xml.Schema.XmlSchemaValidationFlags)
extern "C"  void XmlSchemaValidator__ctor_m101237977 (XmlSchemaValidator_t1781729182 * __this, XmlNameTable_t1216706026 * ___nameTable0, XmlSchemaSet_t4125473774 * ___schemas1, Il2CppObject * ___nsResolver2, int32_t ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::.cctor()
extern "C"  void XmlSchemaValidator__cctor_m1308663193 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::add_ValidationEventHandler(System.Xml.Schema.ValidationEventHandler)
extern "C"  void XmlSchemaValidator_add_ValidationEventHandler_m3583812407 (XmlSchemaValidator_t1781729182 * __this, ValidationEventHandler_t4231404781 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::remove_ValidationEventHandler(System.Xml.Schema.ValidationEventHandler)
extern "C"  void XmlSchemaValidator_remove_ValidationEventHandler_m1669832630 (XmlSchemaValidator_t1781729182 * __this, ValidationEventHandler_t4231404781 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::set_ValidationEventSender(System.Object)
extern "C"  void XmlSchemaValidator_set_ValidationEventSender_m1489152873 (XmlSchemaValidator_t1781729182 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::set_LineInfoProvider(System.Xml.IXmlLineInfo)
extern "C"  void XmlSchemaValidator_set_LineInfoProvider_m2893169580 (XmlSchemaValidator_t1781729182 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::set_XmlResolver(System.Xml.XmlResolver)
extern "C"  void XmlSchemaValidator_set_XmlResolver_m2247302507 (XmlSchemaValidator_t1781729182 * __this, XmlResolver_t3822670287 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Xml.Schema.XmlSchemaValidator::get_SourceUri()
extern "C"  Uri_t1116831938 * XmlSchemaValidator_get_SourceUri_m1910066838 (XmlSchemaValidator_t1781729182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::set_SourceUri(System.Uri)
extern "C"  void XmlSchemaValidator_set_SourceUri_m1542729625 (XmlSchemaValidator_t1781729182 * __this, Uri_t1116831938 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Schema.XmlSchemaValidator::get_BaseUri()
extern "C"  String_t* XmlSchemaValidator_get_BaseUri_m2276030433 (XmlSchemaValidator_t1781729182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Xml.Schema.XsdValidationContext System.Xml.Schema.XmlSchemaValidator::get_Context()
extern "C"  XsdValidationContext_t4179462825 * XmlSchemaValidator_get_Context_m2909743487 (XmlSchemaValidator_t1781729182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Schema.XmlSchemaValidator::get_IgnoreWarnings()
extern "C"  bool XmlSchemaValidator_get_IgnoreWarnings_m3958234058 (XmlSchemaValidator_t1781729182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Schema.XmlSchemaValidator::get_IgnoreIdentity()
extern "C"  bool XmlSchemaValidator_get_IgnoreIdentity_m416057969 (XmlSchemaValidator_t1781729182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaAttribute[] System.Xml.Schema.XmlSchemaValidator::GetExpectedAttributes()
extern "C"  XmlSchemaAttributeU5BU5D_t589142137* XmlSchemaValidator_GetExpectedAttributes_m185644163 (XmlSchemaValidator_t1781729182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::GetUnspecifiedDefaultAttributes(System.Collections.ArrayList)
extern "C"  void XmlSchemaValidator_GetUnspecifiedDefaultAttributes_m3305667626 (XmlSchemaValidator_t1781729182 * __this, ArrayList_t3948406897 * ___defaultAttributeList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::Initialize()
extern "C"  void XmlSchemaValidator_Initialize_m1867928192 (XmlSchemaValidator_t1781729182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::EndValidation()
extern "C"  void XmlSchemaValidator_EndValidation_m2117984102 (XmlSchemaValidator_t1781729182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Schema.XmlSchemaValidator::ValidateAttribute(System.String,System.String,System.Xml.Schema.XmlValueGetter,System.Xml.Schema.XmlSchemaInfo)
extern "C"  Il2CppObject * XmlSchemaValidator_ValidateAttribute_m1507030796 (XmlSchemaValidator_t1781729182 * __this, String_t* ___localName0, String_t* ___ns1, XmlValueGetter_t2234841801 * ___attributeValue2, XmlSchemaInfo_t4089849692 * ___info3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::ValidateElement(System.String,System.String,System.Xml.Schema.XmlSchemaInfo,System.String,System.String,System.String,System.String)
extern "C"  void XmlSchemaValidator_ValidateElement_m2014711955 (XmlSchemaValidator_t1781729182 * __this, String_t* ___localName0, String_t* ___ns1, XmlSchemaInfo_t4089849692 * ___info2, String_t* ___xsiType3, String_t* ___xsiNil4, String_t* ___schemaLocation5, String_t* ___noNsSchemaLocation6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Schema.XmlSchemaValidator::ValidateEndElement(System.Xml.Schema.XmlSchemaInfo)
extern "C"  Il2CppObject * XmlSchemaValidator_ValidateEndElement_m1338769191 (XmlSchemaValidator_t1781729182 * __this, XmlSchemaInfo_t4089849692 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Schema.XmlSchemaValidator::ValidateEndElement(System.Xml.Schema.XmlSchemaInfo,System.Object)
extern "C"  Il2CppObject * XmlSchemaValidator_ValidateEndElement_m2911189429 (XmlSchemaValidator_t1781729182 * __this, XmlSchemaInfo_t4089849692 * ___info0, Il2CppObject * ___var1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::ValidateEndOfAttributes(System.Xml.Schema.XmlSchemaInfo)
extern "C"  void XmlSchemaValidator_ValidateEndOfAttributes_m2843415742 (XmlSchemaValidator_t1781729182 * __this, XmlSchemaInfo_t4089849692 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::ValidateText(System.Xml.Schema.XmlValueGetter)
extern "C"  void XmlSchemaValidator_ValidateText_m3849019831 (XmlSchemaValidator_t1781729182 * __this, XmlValueGetter_t2234841801 * ___getter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::ValidateWhitespace(System.Xml.Schema.XmlValueGetter)
extern "C"  void XmlSchemaValidator_ValidateWhitespace_m1417060167 (XmlSchemaValidator_t1781729182 * __this, XmlValueGetter_t2234841801 * ___getter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::HandleError(System.String)
extern "C"  void XmlSchemaValidator_HandleError_m1036580496 (XmlSchemaValidator_t1781729182 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::HandleError(System.String,System.Exception)
extern "C"  void XmlSchemaValidator_HandleError_m3745824934 (XmlSchemaValidator_t1781729182 * __this, String_t* ___message0, Exception_t3991598821 * ___innerException1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::HandleError(System.String,System.Exception,System.Boolean)
extern "C"  void XmlSchemaValidator_HandleError_m3866718743 (XmlSchemaValidator_t1781729182 * __this, String_t* ___message0, Exception_t3991598821 * ___innerException1, bool ___isWarning2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::HandleError(System.Xml.Schema.XmlSchemaValidationException)
extern "C"  void XmlSchemaValidator_HandleError_m2823918173 (XmlSchemaValidator_t1781729182 * __this, XmlSchemaValidationException_t2573697378 * ___exception0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::HandleError(System.Xml.Schema.XmlSchemaValidationException,System.Boolean)
extern "C"  void XmlSchemaValidator_HandleError_m3109428096 (XmlSchemaValidator_t1781729182 * __this, XmlSchemaValidationException_t2573697378 * ___exception0, bool ___isWarning1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::CheckState(System.Xml.Schema.XmlSchemaValidator/Transition)
extern "C"  void XmlSchemaValidator_CheckState_m3904587206 (XmlSchemaValidator_t1781729182 * __this, int32_t ___expected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaElement System.Xml.Schema.XmlSchemaValidator::FindElement(System.String,System.String)
extern "C"  XmlSchemaElement_t3664762632 * XmlSchemaValidator_FindElement_m1516494655 (XmlSchemaValidator_t1781729182 * __this, String_t* ___name0, String_t* ___ns1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaValidator::FindType(System.Xml.XmlQualifiedName)
extern "C"  XmlSchemaType_t4090188264 * XmlSchemaValidator_FindType_m1146941361 (XmlSchemaValidator_t1781729182 * __this, XmlQualifiedName_t2133315502 * ___qname0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::ValidateStartElementParticle(System.String,System.String)
extern "C"  void XmlSchemaValidator_ValidateStartElementParticle_m2153910584 (XmlSchemaValidator_t1781729182 * __this, String_t* ___localName0, String_t* ___ns1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::AssessOpenStartElementSchemaValidity(System.String,System.String)
extern "C"  void XmlSchemaValidator_AssessOpenStartElementSchemaValidity_m1657957427 (XmlSchemaValidator_t1781729182 * __this, String_t* ___localName0, String_t* ___ns1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::AssessCloseStartElementSchemaValidity(System.Xml.Schema.XmlSchemaInfo)
extern "C"  void XmlSchemaValidator_AssessCloseStartElementSchemaValidity_m1620040816 (XmlSchemaValidator_t1781729182 * __this, XmlSchemaInfo_t4089849692 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::AssessElementLocallyValidElement()
extern "C"  void XmlSchemaValidator_AssessElementLocallyValidElement_m1895092560 (XmlSchemaValidator_t1781729182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::AssessCloseStartElementLocallyValidType(System.Xml.Schema.XmlSchemaInfo)
extern "C"  void XmlSchemaValidator_AssessCloseStartElementLocallyValidType_m272013189 (XmlSchemaValidator_t1781729182 * __this, XmlSchemaInfo_t4089849692 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::AssessCloseStartElementLocallyValidComplexType(System.Xml.Schema.XmlSchemaComplexType,System.Xml.Schema.XmlSchemaInfo)
extern "C"  void XmlSchemaValidator_AssessCloseStartElementLocallyValidComplexType_m1194981724 (XmlSchemaValidator_t1781729182 * __this, XmlSchemaComplexType_t2974990486 * ___cType0, XmlSchemaInfo_t4089849692 * ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Schema.XmlSchemaValidator::AssessAttributeElementLocallyValidType(System.String,System.String,System.Xml.Schema.XmlValueGetter,System.Xml.Schema.XmlSchemaInfo)
extern "C"  Il2CppObject * XmlSchemaValidator_AssessAttributeElementLocallyValidType_m1942871184 (XmlSchemaValidator_t1781729182 * __this, String_t* ___localName0, String_t* ___ns1, XmlValueGetter_t2234841801 * ___getter2, XmlSchemaInfo_t4089849692 * ___info3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Schema.XmlSchemaValidator::AssessAttributeLocallyValid(System.Xml.Schema.XmlSchemaAttribute,System.Xml.Schema.XmlSchemaInfo,System.Xml.Schema.XmlValueGetter)
extern "C"  Il2CppObject * XmlSchemaValidator_AssessAttributeLocallyValid_m1120569679 (XmlSchemaValidator_t1781729182 * __this, XmlSchemaAttribute_t2904598248 * ___attr0, XmlSchemaInfo_t4089849692 * ___info1, XmlValueGetter_t2234841801 * ___getter2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::AssessAttributeLocallyValidUse(System.Xml.Schema.XmlSchemaAttribute)
extern "C"  void XmlSchemaValidator_AssessAttributeLocallyValidUse_m2754999520 (XmlSchemaValidator_t1781729182 * __this, XmlSchemaAttribute_t2904598248 * ___attr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Schema.XmlSchemaValidator::AssessEndElementSchemaValidity(System.Xml.Schema.XmlSchemaInfo)
extern "C"  Il2CppObject * XmlSchemaValidator_AssessEndElementSchemaValidity_m1020044582 (XmlSchemaValidator_t1781729182 * __this, XmlSchemaInfo_t4089849692 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::ValidateEndElementParticle()
extern "C"  void XmlSchemaValidator_ValidateEndElementParticle_m3420115117 (XmlSchemaValidator_t1781729182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::ValidateCharacters(System.Xml.Schema.XmlValueGetter)
extern "C"  void XmlSchemaValidator_ValidateCharacters_m478525428 (XmlSchemaValidator_t1781729182 * __this, XmlValueGetter_t2234841801 * ___getter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Schema.XmlSchemaValidator::ValidateEndSimpleContent(System.Xml.Schema.XmlSchemaInfo)
extern "C"  Il2CppObject * XmlSchemaValidator_ValidateEndSimpleContent_m2089323612 (XmlSchemaValidator_t1781729182 * __this, XmlSchemaInfo_t4089849692 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Schema.XmlSchemaValidator::ValidateEndSimpleContentCore(System.Xml.Schema.XmlSchemaInfo)
extern "C"  Il2CppObject * XmlSchemaValidator_ValidateEndSimpleContentCore_m2142723965 (XmlSchemaValidator_t1781729182 * __this, XmlSchemaInfo_t4089849692 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Schema.XmlSchemaValidator::AssessStringValid(System.Xml.Schema.XmlSchemaSimpleType,System.Xml.Schema.XmlSchemaDatatype,System.String)
extern "C"  Il2CppObject * XmlSchemaValidator_AssessStringValid_m1750645476 (XmlSchemaValidator_t1781729182 * __this, XmlSchemaSimpleType_t3060492794 * ___st0, XmlSchemaDatatype_t196391954 * ___dt1, String_t* ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::ValidateRestrictedSimpleTypeValue(System.Xml.Schema.XmlSchemaSimpleType,System.Xml.Schema.XmlSchemaDatatype&,System.String)
extern "C"  void XmlSchemaValidator_ValidateRestrictedSimpleTypeValue_m3639845314 (XmlSchemaValidator_t1781729182 * __this, XmlSchemaSimpleType_t3060492794 * ___st0, XmlSchemaDatatype_t196391954 ** ___dt1, String_t* ___normalized2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Xml.Schema.XsdKeyTable System.Xml.Schema.XmlSchemaValidator::CreateNewKeyTable(System.Xml.Schema.XmlSchemaIdentityConstraint)
extern "C"  XsdKeyTable_t539606558 * XmlSchemaValidator_CreateNewKeyTable_m3606436695 (XmlSchemaValidator_t1781729182 * __this, XmlSchemaIdentityConstraint_t3091667497 * ___ident0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::ValidateKeySelectors()
extern "C"  void XmlSchemaValidator_ValidateKeySelectors_m3746625147 (XmlSchemaValidator_t1781729182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::ValidateKeyFieldsAttribute(System.Xml.Schema.XmlSchemaAttribute,System.Object)
extern "C"  void XmlSchemaValidator_ValidateKeyFieldsAttribute_m1301098845 (XmlSchemaValidator_t1781729182 * __this, XmlSchemaAttribute_t2904598248 * ___attr0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::ValidateKeyFields(System.Boolean,System.Boolean,System.Object,System.String,System.String,System.Object)
extern "C"  void XmlSchemaValidator_ValidateKeyFields_m911501318 (XmlSchemaValidator_t1781729182 * __this, bool ___isAttr0, bool ___isNil1, Il2CppObject * ___schemaType2, String_t* ___attrName3, String_t* ___attrNs4, Il2CppObject * ___value5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::ValidateEndElementKeyConstraints()
extern "C"  void XmlSchemaValidator_ValidateEndElementKeyConstraints_m2592926654 (XmlSchemaValidator_t1781729182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::ValidateEndKeyConstraint(Mono.Xml.Schema.XsdKeyTable)
extern "C"  void XmlSchemaValidator_ValidateEndKeyConstraint_m3195928606 (XmlSchemaValidator_t1781729182 * __this, XsdKeyTable_t539606558 * ___seq0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::ValidateSimpleContentIdentity(System.Xml.Schema.XmlSchemaDatatype,System.String)
extern "C"  void XmlSchemaValidator_ValidateSimpleContentIdentity_m1304533128 (XmlSchemaValidator_t1781729182 * __this, XmlSchemaDatatype_t196391954 * ___dt0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Schema.XmlSchemaValidator::GetXsiType(System.String)
extern "C"  Il2CppObject * XmlSchemaValidator_GetXsiType_m3100235019 (XmlSchemaValidator_t1781729182 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::HandleXsiType(System.String)
extern "C"  void XmlSchemaValidator_HandleXsiType_m1492108720 (XmlSchemaValidator_t1781729182 * __this, String_t* ___typename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::AssessLocalTypeDerivationOK(System.Object,System.Object,System.Xml.Schema.XmlSchemaDerivationMethod)
extern "C"  void XmlSchemaValidator_AssessLocalTypeDerivationOK_m313414735 (XmlSchemaValidator_t1781729182 * __this, Il2CppObject * ___xsiType0, Il2CppObject * ___baseType1, int32_t ___flag2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::HandleXsiNil(System.String,System.Xml.Schema.XmlSchemaInfo)
extern "C"  void XmlSchemaValidator_HandleXsiNil_m2932704676 (XmlSchemaValidator_t1781729182 * __this, String_t* ___value0, XmlSchemaInfo_t4089849692 * ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchema System.Xml.Schema.XmlSchemaValidator::ReadExternalSchema(System.String)
extern "C"  XmlSchema_t1010706190 * XmlSchemaValidator_ReadExternalSchema_m2133594686 (XmlSchemaValidator_t1781729182 * __this, String_t* ___uri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::HandleSchemaLocation(System.String)
extern "C"  void XmlSchemaValidator_HandleSchemaLocation_m2974871636 (XmlSchemaValidator_t1781729182 * __this, String_t* ___schemaLocation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaValidator::HandleNoNSSchemaLocation(System.String)
extern "C"  void XmlSchemaValidator_HandleNoNSSchemaLocation_m1845454382 (XmlSchemaValidator_t1781729182 * __this, String_t* ___noNsSchemaLocation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
