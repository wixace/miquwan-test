﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AnimatorGenerated
struct UnityEngine_AnimatorGenerated_t3856455168;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type
struct Type_t;
// MethodID
struct MethodID_t3916401116;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_MethodID3916401116.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine_AnimatorGenerated::.ctor()
extern "C"  void UnityEngine_AnimatorGenerated__ctor_m741309291 (UnityEngine_AnimatorGenerated_t3856455168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::.cctor()
extern "C"  void UnityEngine_AnimatorGenerated__cctor_m1023655330 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_Animator1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_Animator1_m2714377863 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_isOptimizable(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_isOptimizable_m3291973646 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_isHuman(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_isHuman_m4246659747 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_hasRootMotion(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_hasRootMotion_m3257589812 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_humanScale(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_humanScale_m1116568105 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_isInitialized(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_isInitialized_m923869532 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_deltaPosition(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_deltaPosition_m4104479109 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_deltaRotation(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_deltaRotation_m3773184720 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_velocity(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_velocity_m2652899689 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_angularVelocity(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_angularVelocity_m1566930791 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_rootPosition(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_rootPosition_m2819363291 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_rootRotation(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_rootRotation_m2488068902 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_applyRootMotion(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_applyRootMotion_m1229389536 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_linearVelocityBlending(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_linearVelocityBlending_m3611666387 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_updateMode(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_updateMode_m1231931674 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_hasTransformHierarchy(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_hasTransformHierarchy_m2875176035 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_gravityWeight(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_gravityWeight_m477951424 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_bodyPosition(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_bodyPosition_m3741577499 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_bodyRotation(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_bodyRotation_m3410283110 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_stabilizeFeet(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_stabilizeFeet_m3007016809 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_layerCount(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_layerCount_m4156327208 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_parameters(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_parameters_m1046698748 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_parameterCount(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_parameterCount_m2088393088 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_feetPivotActive(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_feetPivotActive_m311686988 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_pivotWeight(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_pivotWeight_m693857612 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_pivotPosition(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_pivotPosition_m337655355 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_isMatchingTarget(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_isMatchingTarget_m1314933998 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_speed(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_speed_m2990838015 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_targetPosition(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_targetPosition_m2856529164 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_targetRotation(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_targetRotation_m2525234775 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_cullingMode(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_cullingMode_m318066643 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_playbackTime(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_playbackTime_m1522134814 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_recorderStartTime(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_recorderStartTime_m3513971029 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_recorderStopTime(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_recorderStopTime_m1369679737 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_recorderMode(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_recorderMode_m147203909 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_runtimeAnimatorController(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_runtimeAnimatorController_m2868325355 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_avatar(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_avatar_m1342724205 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_layersAffectMassCenter(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_layersAffectMassCenter_m1676890854 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_leftFeetBottomHeight(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_leftFeetBottomHeight_m3537781599 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_rightFeetBottomHeight(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_rightFeetBottomHeight_m3690837674 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_logWarnings(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_logWarnings_m2720236555 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::Animator_fireEvents(JSVCall)
extern "C"  void UnityEngine_AnimatorGenerated_Animator_fireEvents_m1264496535 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_ApplyBuiltinRootMotion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_ApplyBuiltinRootMotion_m3091378162 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_CrossFade__Int32__Single__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_CrossFade__Int32__Single__Int32__Single_m4105056505 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_CrossFade__String__Single__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_CrossFade__String__Single__Int32__Single_m3158958086 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_CrossFade__String__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_CrossFade__String__Single__Int32_m673623742 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_CrossFade__Int32__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_CrossFade__Int32__Single__Int32_m1329111729 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_CrossFade__String__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_CrossFade__String__Single_m3694745682 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_CrossFade__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_CrossFade__Int32__Single_m3145310143 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_CrossFadeInFixedTime__Int32__Single__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_CrossFadeInFixedTime__Int32__Single__Int32__Single_m2233699653 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_CrossFadeInFixedTime__String__Single__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_CrossFadeInFixedTime__String__Single__Int32__Single_m981470522 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_CrossFadeInFixedTime__String__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_CrossFadeInFixedTime__String__Single__Int32_m4272377842 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_CrossFadeInFixedTime__Int32__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_CrossFadeInFixedTime__Int32__Single__Int32_m3661957885 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_CrossFadeInFixedTime__String__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_CrossFadeInFixedTime__String__Single_m563931550 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_CrossFadeInFixedTime__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_CrossFadeInFixedTime__Int32__Single_m3321410803 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetAnimatorTransitionInfo__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetAnimatorTransitionInfo__Int32_m853864307 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetBehaviourT1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetBehaviourT1_m3874858345 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetBehavioursT1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetBehavioursT1_m1905871046 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetBoneTransform__HumanBodyBones(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetBoneTransform__HumanBodyBones_m3115654039 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetBool__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetBool__Int32_m4222497395 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetBool__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetBool__String_m3544487822 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetCurrentAnimatorClipInfo__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetCurrentAnimatorClipInfo__Int32_m3789135203 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetCurrentAnimatorStateInfo__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetCurrentAnimatorStateInfo__Int32_m1291650366 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetFloat__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetFloat__Int32_m1436667909 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetFloat__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetFloat__String_m3083119676 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetIKHintPosition__AvatarIKHint(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetIKHintPosition__AvatarIKHint_m859718311 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetIKHintPositionWeight__AvatarIKHint(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetIKHintPositionWeight__AvatarIKHint_m2443570751 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetIKPosition__AvatarIKGoal(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetIKPosition__AvatarIKGoal_m1403682828 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetIKPositionWeight__AvatarIKGoal(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetIKPositionWeight__AvatarIKGoal_m3282871524 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetIKRotation__AvatarIKGoal(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetIKRotation__AvatarIKGoal_m2078748641 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetIKRotationWeight__AvatarIKGoal(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetIKRotationWeight__AvatarIKGoal_m888758521 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetInteger__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetInteger__Int32_m1958267875 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetInteger__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetInteger__String_m2072849438 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetLayerIndex__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetLayerIndex__String_m3950235365 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetLayerName__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetLayerName__Int32_m268280293 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetLayerWeight__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetLayerWeight__Int32_m2309031128 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetNextAnimatorClipInfo__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetNextAnimatorClipInfo__Int32_m2505549541 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetNextAnimatorStateInfo__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetNextAnimatorStateInfo__Int32_m155200508 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_GetParameter__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_GetParameter__Int32_m1711328472 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_HasState__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_HasState__Int32__Int32_m803118236 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_InterruptMatchTarget__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_InterruptMatchTarget__Boolean_m3865579602 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_InterruptMatchTarget(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_InterruptMatchTarget_m1764794328 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_IsInTransition__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_IsInTransition__Int32_m3959396807 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_IsParameterControlledByCurve__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_IsParameterControlledByCurve__String_m1173281377 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_IsParameterControlledByCurve__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_IsParameterControlledByCurve__Int32_m3591817536 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_MatchTarget__Vector3__Quaternion__AvatarTarget__MatchTargetWeightMask__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_MatchTarget__Vector3__Quaternion__AvatarTarget__MatchTargetWeightMask__Single__Single_m1519456581 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_MatchTarget__Vector3__Quaternion__AvatarTarget__MatchTargetWeightMask__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_MatchTarget__Vector3__Quaternion__AvatarTarget__MatchTargetWeightMask__Single_m662081277 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_Play__String__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_Play__String__Int32__Single_m1715916046 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_Play__Int32__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_Play__Int32__Int32__Single_m107137921 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_Play__String__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_Play__String__Int32_m3880068038 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_Play__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_Play__Int32__Int32_m2572436793 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_Play__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_Play__String_m1928395850 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_Play__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_Play__Int32_m3200534071 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_PlayInFixedTime__String__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_PlayInFixedTime__String__Int32__Single_m4034238658 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_PlayInFixedTime__Int32__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_PlayInFixedTime__Int32__Int32__Single_m4061247821 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_PlayInFixedTime__String__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_PlayInFixedTime__String__Int32_m3515147130 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_PlayInFixedTime__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_PlayInFixedTime__Int32__Int32_m621002501 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_PlayInFixedTime__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_PlayInFixedTime__String_m1862450966 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_PlayInFixedTime__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_PlayInFixedTime__Int32_m1397291499 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_Rebind(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_Rebind_m1062128309 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_ResetTrigger__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_ResetTrigger__String_m1227258815 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_ResetTrigger__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_ResetTrigger__Int32_m1792443426 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetBoneLocalRotation__HumanBodyBones__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetBoneLocalRotation__HumanBodyBones__Quaternion_m2198620422 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetBool__String__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetBool__String__Boolean_m1537935568 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetBool__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetBool__Int32__Boolean_m3675216419 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetFloat__String__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetFloat__String__Single__Single__Single_m4072475016 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetFloat__Int32__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetFloat__Int32__Single__Single__Single_m950267113 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetFloat__String__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetFloat__String__Single_m2883855096 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetFloat__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetFloat__Int32__Single_m1872226393 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetIKHintPosition__AvatarIKHint__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetIKHintPosition__AvatarIKHint__Vector3_m209776383 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetIKHintPositionWeight__AvatarIKHint__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetIKHintPositionWeight__AvatarIKHint__Single_m296213395 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetIKPosition__AvatarIKGoal__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetIKPosition__AvatarIKGoal__Vector3_m783469882 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetIKPositionWeight__AvatarIKGoal__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetIKPositionWeight__AvatarIKGoal__Single_m844923704 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetIKRotation__AvatarIKGoal__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetIKRotation__AvatarIKGoal__Quaternion_m168775307 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetIKRotationWeight__AvatarIKGoal__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetIKRotationWeight__AvatarIKGoal__Single_m46815821 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetInteger__String__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetInteger__String__Int32_m379127934 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetInteger__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetInteger__Int32__Int32_m2182408577 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetLayerWeight__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetLayerWeight__Int32__Single_m981917996 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetLookAtPosition__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetLookAtPosition__Vector3_m1277002744 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetLookAtWeight__Single__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetLookAtWeight__Single__Single__Single__Single__Single_m3495538449 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetLookAtWeight__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetLookAtWeight__Single__Single__Single__Single_m166705353 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetLookAtWeight__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetLookAtWeight__Single__Single__Single_m2032597633 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetLookAtWeight__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetLookAtWeight__Single__Single_m2394387513 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetLookAtWeight__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetLookAtWeight__Single_m4082705905 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetTarget__AvatarTarget__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetTarget__AvatarTarget__Single_m1690630146 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetTrigger__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetTrigger__Int32_m3593525429 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_SetTrigger__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_SetTrigger__String_m1226226060 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_StartPlayback(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_StartPlayback_m3598482106 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_StartRecording__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_StartRecording__Int32_m1287639196 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_StopPlayback(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_StopPlayback_m1041533762 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_StopRecording(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_StopRecording_m3859772588 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_Update__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_Update__Single_m180874006 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::Animator_StringToHash__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_Animator_StringToHash__String_m2578026544 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::__Register()
extern "C"  void UnityEngine_AnimatorGenerated___Register_m427650108 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimatorGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_AnimatorGenerated_ilo_getObject1_m3624044587 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_ilo_attachFinalizerObject2_m1397332269 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void UnityEngine_AnimatorGenerated_ilo_setBooleanS3_m1556827354 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::ilo_setVector3S4(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_AnimatorGenerated_ilo_setVector3S4_m2591024447 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_AnimatorGenerated::ilo_getVector3S5(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_AnimatorGenerated_ilo_getVector3S5_m159444799 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorGenerated::ilo_getBooleanS6(System.Int32)
extern "C"  bool UnityEngine_AnimatorGenerated_ilo_getBooleanS6_m2509029214 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::ilo_setSingle7(System.Int32,System.Single)
extern "C"  void UnityEngine_AnimatorGenerated_ilo_setSingle7_m3698960431 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimatorGenerated::ilo_setObject8(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_AnimatorGenerated_ilo_setObject8_m1887683434 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_AnimatorGenerated::ilo_getObject9(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_AnimatorGenerated_ilo_getObject9_m3614735586 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::ilo_setInt3210(System.Int32,System.Int32)
extern "C"  void UnityEngine_AnimatorGenerated_ilo_setInt3210_m2672264015 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_AnimatorGenerated::ilo_getSingle11(System.Int32)
extern "C"  float UnityEngine_AnimatorGenerated_ilo_getSingle11_m590676945 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_AnimatorGenerated::ilo_getStringS12(System.Int32)
extern "C"  String_t* UnityEngine_AnimatorGenerated_ilo_getStringS12_m2958924775 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimatorGenerated::ilo_getInt3213(System.Int32)
extern "C"  int32_t UnityEngine_AnimatorGenerated_ilo_getInt3213_m1177731081 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine_AnimatorGenerated::ilo_makeGenericMethod14(System.Type,MethodID,System.Int32)
extern "C"  MethodInfo_t * UnityEngine_AnimatorGenerated_ilo_makeGenericMethod14_m3047285529 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MethodID_t3916401116 * ___methodID1, int32_t ___TCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::ilo_setArrayS15(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_AnimatorGenerated_ilo_setArrayS15_m1498149933 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimatorGenerated::ilo_getEnum16(System.Int32)
extern "C"  int32_t UnityEngine_AnimatorGenerated_ilo_getEnum16_m134635089 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorGenerated::ilo_setStringS17(System.Int32,System.String)
extern "C"  void UnityEngine_AnimatorGenerated_ilo_setStringS17_m1563552479 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
