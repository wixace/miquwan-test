﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICenterOnChildGenerated
struct UICenterOnChildGenerated_t1545208251;
// JSVCall
struct JSVCall_t3708497963;
// SpringPanel/OnFinished
struct OnFinished_t3316389065;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// UICenterOnChild/OnCenterCallback
struct OnCenterCallback_t2672694804;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UICenterOnChild
struct UICenterOnChild_t854454836;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_UICenterOnChild854454836.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"

// System.Void UICenterOnChildGenerated::.ctor()
extern "C"  void UICenterOnChildGenerated__ctor_m3716042560 (UICenterOnChildGenerated_t1545208251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICenterOnChildGenerated::UICenterOnChild_UICenterOnChild1(JSVCall,System.Int32)
extern "C"  bool UICenterOnChildGenerated_UICenterOnChild_UICenterOnChild1_m2160113616 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICenterOnChildGenerated::UICenterOnChild_springStrength(JSVCall)
extern "C"  void UICenterOnChildGenerated_UICenterOnChild_springStrength_m1489636938 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICenterOnChildGenerated::UICenterOnChild_nextPageThreshold(JSVCall)
extern "C"  void UICenterOnChildGenerated_UICenterOnChild_nextPageThreshold_m6688139 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SpringPanel/OnFinished UICenterOnChildGenerated::UICenterOnChild_onFinished_GetDelegate_member2_arg0(CSRepresentedObject)
extern "C"  OnFinished_t3316389065 * UICenterOnChildGenerated_UICenterOnChild_onFinished_GetDelegate_member2_arg0_m362572586 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICenterOnChildGenerated::UICenterOnChild_onFinished(JSVCall)
extern "C"  void UICenterOnChildGenerated_UICenterOnChild_onFinished_m56107175 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICenterOnChild/OnCenterCallback UICenterOnChildGenerated::UICenterOnChild_onCenter_GetDelegate_member3_arg0(CSRepresentedObject)
extern "C"  OnCenterCallback_t2672694804 * UICenterOnChildGenerated_UICenterOnChild_onCenter_GetDelegate_member3_arg0_m945303715 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICenterOnChildGenerated::UICenterOnChild_onCenter(JSVCall)
extern "C"  void UICenterOnChildGenerated_UICenterOnChild_onCenter_m3846995172 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICenterOnChildGenerated::UICenterOnChild_centeredObject(JSVCall)
extern "C"  void UICenterOnChildGenerated_UICenterOnChild_centeredObject_m1372979525 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICenterOnChildGenerated::UICenterOnChild_CenterOn__Transform(JSVCall,System.Int32)
extern "C"  bool UICenterOnChildGenerated_UICenterOnChild_CenterOn__Transform_m2329316231 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICenterOnChildGenerated::UICenterOnChild_Recenter(JSVCall,System.Int32)
extern "C"  bool UICenterOnChildGenerated_UICenterOnChild_Recenter_m1998356347 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICenterOnChildGenerated::__Register()
extern "C"  void UICenterOnChildGenerated___Register_m2196944263 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SpringPanel/OnFinished UICenterOnChildGenerated::<UICenterOnChild_onFinished>m__129()
extern "C"  OnFinished_t3316389065 * UICenterOnChildGenerated_U3CUICenterOnChild_onFinishedU3Em__129_m3833729257 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICenterOnChild/OnCenterCallback UICenterOnChildGenerated::<UICenterOnChild_onCenter>m__12B()
extern "C"  OnCenterCallback_t2672694804 * UICenterOnChildGenerated_U3CUICenterOnChild_onCenterU3Em__12B_m3973416644 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICenterOnChildGenerated::ilo_setSingle1(System.Int32,System.Single)
extern "C"  void UICenterOnChildGenerated_ilo_setSingle1_m2493284660 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UICenterOnChildGenerated::ilo_get_centeredObject2(UICenterOnChild)
extern "C"  GameObject_t3674682005 * UICenterOnChildGenerated_ilo_get_centeredObject2_m2343181842 (Il2CppObject * __this /* static, unused */, UICenterOnChild_t854454836 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICenterOnChildGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UICenterOnChildGenerated_ilo_setObject3_m956139980 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICenterOnChildGenerated::ilo_CenterOn4(UICenterOnChild,UnityEngine.Transform)
extern "C"  void UICenterOnChildGenerated_ilo_CenterOn4_m4018052522 (Il2CppObject * __this /* static, unused */, UICenterOnChild_t854454836 * ____this0, Transform_t1659122786 * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject UICenterOnChildGenerated::ilo_getFunctionS5(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * UICenterOnChildGenerated_ilo_getFunctionS5_m1273262063 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UICenterOnChildGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UICenterOnChildGenerated_ilo_getObject6_m3742389684 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICenterOnChild/OnCenterCallback UICenterOnChildGenerated::ilo_UICenterOnChild_onCenter_GetDelegate_member3_arg07(CSRepresentedObject)
extern "C"  OnCenterCallback_t2672694804 * UICenterOnChildGenerated_ilo_UICenterOnChild_onCenter_GetDelegate_member3_arg07_m1734140009 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
