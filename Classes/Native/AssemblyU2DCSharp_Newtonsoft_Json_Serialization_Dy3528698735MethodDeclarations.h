﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.DynamicValueProvider
struct DynamicValueProvider_t3528698735;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"

// System.Void Newtonsoft.Json.Serialization.DynamicValueProvider::.ctor(System.Reflection.MemberInfo)
extern "C"  void DynamicValueProvider__ctor_m2699604729 (DynamicValueProvider_t3528698735 * __this, MemberInfo_t * ___memberInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DynamicValueProvider::SetValue(System.Object,System.Object)
extern "C"  void DynamicValueProvider_SetValue_m4225055156 (DynamicValueProvider_t3528698735 * __this, Il2CppObject * ___target0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.DynamicValueProvider::GetValue(System.Object)
extern "C"  Il2CppObject * DynamicValueProvider_GetValue_m553509509 (DynamicValueProvider_t3528698735 * __this, Il2CppObject * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.DynamicValueProvider::ilo_FormatWith1(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* DynamicValueProvider_ilo_FormatWith1_m4056270143 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
