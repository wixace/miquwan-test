﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SevenZip.CRC
struct CRC_t1616380534;

#include "mscorlib_System_IO_Stream1561764144.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SevenZip.LzmaBench/CrcOutStream
struct  CrcOutStream_t4130964789  : public Stream_t1561764144
{
public:
	// SevenZip.CRC SevenZip.LzmaBench/CrcOutStream::CRC
	CRC_t1616380534 * ___CRC_1;

public:
	inline static int32_t get_offset_of_CRC_1() { return static_cast<int32_t>(offsetof(CrcOutStream_t4130964789, ___CRC_1)); }
	inline CRC_t1616380534 * get_CRC_1() const { return ___CRC_1; }
	inline CRC_t1616380534 ** get_address_of_CRC_1() { return &___CRC_1; }
	inline void set_CRC_1(CRC_t1616380534 * value)
	{
		___CRC_1 = value;
		Il2CppCodeGenWriteBarrier(&___CRC_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
