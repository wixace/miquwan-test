﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OnVoidDelegate
struct OnVoidDelegate_t2787120856;
// System.Func`2<System.Boolean,System.Boolean>
struct Func_2_t3133160109;

#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AstarPath/AstarWorkItem
struct  AstarWorkItem_t2566693888 
{
public:
	// OnVoidDelegate AstarPath/AstarWorkItem::init
	OnVoidDelegate_t2787120856 * ___init_0;
	// System.Func`2<System.Boolean,System.Boolean> AstarPath/AstarWorkItem::update
	Func_2_t3133160109 * ___update_1;

public:
	inline static int32_t get_offset_of_init_0() { return static_cast<int32_t>(offsetof(AstarWorkItem_t2566693888, ___init_0)); }
	inline OnVoidDelegate_t2787120856 * get_init_0() const { return ___init_0; }
	inline OnVoidDelegate_t2787120856 ** get_address_of_init_0() { return &___init_0; }
	inline void set_init_0(OnVoidDelegate_t2787120856 * value)
	{
		___init_0 = value;
		Il2CppCodeGenWriteBarrier(&___init_0, value);
	}

	inline static int32_t get_offset_of_update_1() { return static_cast<int32_t>(offsetof(AstarWorkItem_t2566693888, ___update_1)); }
	inline Func_2_t3133160109 * get_update_1() const { return ___update_1; }
	inline Func_2_t3133160109 ** get_address_of_update_1() { return &___update_1; }
	inline void set_update_1(Func_2_t3133160109 * value)
	{
		___update_1 = value;
		Il2CppCodeGenWriteBarrier(&___update_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: AstarPath/AstarWorkItem
struct AstarWorkItem_t2566693888_marshaled_pinvoke
{
	Il2CppMethodPointer ___init_0;
	Il2CppMethodPointer ___update_1;
};
// Native definition for marshalling of: AstarPath/AstarWorkItem
struct AstarWorkItem_t2566693888_marshaled_com
{
	Il2CppMethodPointer ___init_0;
	Il2CppMethodPointer ___update_1;
};
