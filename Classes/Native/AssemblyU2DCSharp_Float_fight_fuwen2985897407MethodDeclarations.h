﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_fight_fuwen
struct Float_fight_fuwen_t2985897407;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void Float_fight_fuwen::.ctor()
extern "C"  void Float_fight_fuwen__ctor_m967345804 (Float_fight_fuwen_t2985897407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_fight_fuwen::FloatID()
extern "C"  int32_t Float_fight_fuwen_FloatID_m927777866 (Float_fight_fuwen_t2985897407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_fight_fuwen::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_fight_fuwen_OnAwake_m3933374024 (Float_fight_fuwen_t2985897407 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_fight_fuwen::OnDestroy()
extern "C"  void Float_fight_fuwen_OnDestroy_m494679429 (Float_fight_fuwen_t2985897407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_fight_fuwen::Init()
extern "C"  void Float_fight_fuwen_Init_m4000613960 (Float_fight_fuwen_t2985897407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_fight_fuwen::SetFile(System.Object[])
extern "C"  void Float_fight_fuwen_SetFile_m3828909002 (Float_fight_fuwen_t2985897407 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
