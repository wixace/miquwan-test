﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VersionInfo
struct  VersionInfo_t2356638086  : public Il2CppObject
{
public:
	// System.String VersionInfo::name
	String_t* ___name_0;
	// System.String VersionInfo::pkgvs
	String_t* ___pkgvs_1;
	// System.Int64 VersionInfo::pkgsize
	int64_t ___pkgsize_2;
	// System.String VersionInfo::pkgmd5
	String_t* ___pkgmd5_3;
	// System.String VersionInfo::pkgurl
	String_t* ___pkgurl_4;
	// System.String VersionInfo::assetsvs
	String_t* ___assetsvs_5;
	// System.String VersionInfo::assetsurl
	String_t* ___assetsurl_6;
	// System.String VersionInfo::servervs
	String_t* ___servervs_7;
	// System.String[] VersionInfo::serverurl
	StringU5BU5D_t4054002952* ___serverurl_8;
	// System.String VersionInfo::bulletinurl
	String_t* ___bulletinurl_9;
	// System.String VersionInfo::appkey
	String_t* ___appkey_10;
	// System.String VersionInfo::trackappkey
	String_t* ___trackappkey_11;
	// System.String VersionInfo::trackingIOappkey
	String_t* ___trackingIOappkey_12;
	// System.UInt32 VersionInfo::usertype
	uint32_t ___usertype_13;
	// System.Int32 VersionInfo::hide2000
	int32_t ___hide2000_14;
	// System.String VersionInfo::sdkinfo
	String_t* ___sdkinfo_15;
	// System.String VersionInfo::paycburl
	String_t* ___paycburl_16;
	// System.String VersionInfo::orderurl
	String_t* ___orderurl_17;
	// System.String VersionInfo::pidurl
	String_t* ___pidurl_18;
	// System.Int32 VersionInfo::hideGuide
	int32_t ___hideGuide_19;
	// System.Int32 VersionInfo::sdk
	int32_t ___sdk_20;
	// System.String VersionInfo::deviceurl
	String_t* ___deviceurl_21;
	// System.Boolean VersionInfo::isNoUpdate
	bool ___isNoUpdate_22;
	// System.String VersionInfo::vscfg
	String_t* ___vscfg_23;
	// System.String VersionInfo::noRestartAssetsvs
	String_t* ___noRestartAssetsvs_24;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_pkgvs_1() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___pkgvs_1)); }
	inline String_t* get_pkgvs_1() const { return ___pkgvs_1; }
	inline String_t** get_address_of_pkgvs_1() { return &___pkgvs_1; }
	inline void set_pkgvs_1(String_t* value)
	{
		___pkgvs_1 = value;
		Il2CppCodeGenWriteBarrier(&___pkgvs_1, value);
	}

	inline static int32_t get_offset_of_pkgsize_2() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___pkgsize_2)); }
	inline int64_t get_pkgsize_2() const { return ___pkgsize_2; }
	inline int64_t* get_address_of_pkgsize_2() { return &___pkgsize_2; }
	inline void set_pkgsize_2(int64_t value)
	{
		___pkgsize_2 = value;
	}

	inline static int32_t get_offset_of_pkgmd5_3() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___pkgmd5_3)); }
	inline String_t* get_pkgmd5_3() const { return ___pkgmd5_3; }
	inline String_t** get_address_of_pkgmd5_3() { return &___pkgmd5_3; }
	inline void set_pkgmd5_3(String_t* value)
	{
		___pkgmd5_3 = value;
		Il2CppCodeGenWriteBarrier(&___pkgmd5_3, value);
	}

	inline static int32_t get_offset_of_pkgurl_4() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___pkgurl_4)); }
	inline String_t* get_pkgurl_4() const { return ___pkgurl_4; }
	inline String_t** get_address_of_pkgurl_4() { return &___pkgurl_4; }
	inline void set_pkgurl_4(String_t* value)
	{
		___pkgurl_4 = value;
		Il2CppCodeGenWriteBarrier(&___pkgurl_4, value);
	}

	inline static int32_t get_offset_of_assetsvs_5() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___assetsvs_5)); }
	inline String_t* get_assetsvs_5() const { return ___assetsvs_5; }
	inline String_t** get_address_of_assetsvs_5() { return &___assetsvs_5; }
	inline void set_assetsvs_5(String_t* value)
	{
		___assetsvs_5 = value;
		Il2CppCodeGenWriteBarrier(&___assetsvs_5, value);
	}

	inline static int32_t get_offset_of_assetsurl_6() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___assetsurl_6)); }
	inline String_t* get_assetsurl_6() const { return ___assetsurl_6; }
	inline String_t** get_address_of_assetsurl_6() { return &___assetsurl_6; }
	inline void set_assetsurl_6(String_t* value)
	{
		___assetsurl_6 = value;
		Il2CppCodeGenWriteBarrier(&___assetsurl_6, value);
	}

	inline static int32_t get_offset_of_servervs_7() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___servervs_7)); }
	inline String_t* get_servervs_7() const { return ___servervs_7; }
	inline String_t** get_address_of_servervs_7() { return &___servervs_7; }
	inline void set_servervs_7(String_t* value)
	{
		___servervs_7 = value;
		Il2CppCodeGenWriteBarrier(&___servervs_7, value);
	}

	inline static int32_t get_offset_of_serverurl_8() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___serverurl_8)); }
	inline StringU5BU5D_t4054002952* get_serverurl_8() const { return ___serverurl_8; }
	inline StringU5BU5D_t4054002952** get_address_of_serverurl_8() { return &___serverurl_8; }
	inline void set_serverurl_8(StringU5BU5D_t4054002952* value)
	{
		___serverurl_8 = value;
		Il2CppCodeGenWriteBarrier(&___serverurl_8, value);
	}

	inline static int32_t get_offset_of_bulletinurl_9() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___bulletinurl_9)); }
	inline String_t* get_bulletinurl_9() const { return ___bulletinurl_9; }
	inline String_t** get_address_of_bulletinurl_9() { return &___bulletinurl_9; }
	inline void set_bulletinurl_9(String_t* value)
	{
		___bulletinurl_9 = value;
		Il2CppCodeGenWriteBarrier(&___bulletinurl_9, value);
	}

	inline static int32_t get_offset_of_appkey_10() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___appkey_10)); }
	inline String_t* get_appkey_10() const { return ___appkey_10; }
	inline String_t** get_address_of_appkey_10() { return &___appkey_10; }
	inline void set_appkey_10(String_t* value)
	{
		___appkey_10 = value;
		Il2CppCodeGenWriteBarrier(&___appkey_10, value);
	}

	inline static int32_t get_offset_of_trackappkey_11() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___trackappkey_11)); }
	inline String_t* get_trackappkey_11() const { return ___trackappkey_11; }
	inline String_t** get_address_of_trackappkey_11() { return &___trackappkey_11; }
	inline void set_trackappkey_11(String_t* value)
	{
		___trackappkey_11 = value;
		Il2CppCodeGenWriteBarrier(&___trackappkey_11, value);
	}

	inline static int32_t get_offset_of_trackingIOappkey_12() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___trackingIOappkey_12)); }
	inline String_t* get_trackingIOappkey_12() const { return ___trackingIOappkey_12; }
	inline String_t** get_address_of_trackingIOappkey_12() { return &___trackingIOappkey_12; }
	inline void set_trackingIOappkey_12(String_t* value)
	{
		___trackingIOappkey_12 = value;
		Il2CppCodeGenWriteBarrier(&___trackingIOappkey_12, value);
	}

	inline static int32_t get_offset_of_usertype_13() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___usertype_13)); }
	inline uint32_t get_usertype_13() const { return ___usertype_13; }
	inline uint32_t* get_address_of_usertype_13() { return &___usertype_13; }
	inline void set_usertype_13(uint32_t value)
	{
		___usertype_13 = value;
	}

	inline static int32_t get_offset_of_hide2000_14() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___hide2000_14)); }
	inline int32_t get_hide2000_14() const { return ___hide2000_14; }
	inline int32_t* get_address_of_hide2000_14() { return &___hide2000_14; }
	inline void set_hide2000_14(int32_t value)
	{
		___hide2000_14 = value;
	}

	inline static int32_t get_offset_of_sdkinfo_15() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___sdkinfo_15)); }
	inline String_t* get_sdkinfo_15() const { return ___sdkinfo_15; }
	inline String_t** get_address_of_sdkinfo_15() { return &___sdkinfo_15; }
	inline void set_sdkinfo_15(String_t* value)
	{
		___sdkinfo_15 = value;
		Il2CppCodeGenWriteBarrier(&___sdkinfo_15, value);
	}

	inline static int32_t get_offset_of_paycburl_16() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___paycburl_16)); }
	inline String_t* get_paycburl_16() const { return ___paycburl_16; }
	inline String_t** get_address_of_paycburl_16() { return &___paycburl_16; }
	inline void set_paycburl_16(String_t* value)
	{
		___paycburl_16 = value;
		Il2CppCodeGenWriteBarrier(&___paycburl_16, value);
	}

	inline static int32_t get_offset_of_orderurl_17() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___orderurl_17)); }
	inline String_t* get_orderurl_17() const { return ___orderurl_17; }
	inline String_t** get_address_of_orderurl_17() { return &___orderurl_17; }
	inline void set_orderurl_17(String_t* value)
	{
		___orderurl_17 = value;
		Il2CppCodeGenWriteBarrier(&___orderurl_17, value);
	}

	inline static int32_t get_offset_of_pidurl_18() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___pidurl_18)); }
	inline String_t* get_pidurl_18() const { return ___pidurl_18; }
	inline String_t** get_address_of_pidurl_18() { return &___pidurl_18; }
	inline void set_pidurl_18(String_t* value)
	{
		___pidurl_18 = value;
		Il2CppCodeGenWriteBarrier(&___pidurl_18, value);
	}

	inline static int32_t get_offset_of_hideGuide_19() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___hideGuide_19)); }
	inline int32_t get_hideGuide_19() const { return ___hideGuide_19; }
	inline int32_t* get_address_of_hideGuide_19() { return &___hideGuide_19; }
	inline void set_hideGuide_19(int32_t value)
	{
		___hideGuide_19 = value;
	}

	inline static int32_t get_offset_of_sdk_20() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___sdk_20)); }
	inline int32_t get_sdk_20() const { return ___sdk_20; }
	inline int32_t* get_address_of_sdk_20() { return &___sdk_20; }
	inline void set_sdk_20(int32_t value)
	{
		___sdk_20 = value;
	}

	inline static int32_t get_offset_of_deviceurl_21() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___deviceurl_21)); }
	inline String_t* get_deviceurl_21() const { return ___deviceurl_21; }
	inline String_t** get_address_of_deviceurl_21() { return &___deviceurl_21; }
	inline void set_deviceurl_21(String_t* value)
	{
		___deviceurl_21 = value;
		Il2CppCodeGenWriteBarrier(&___deviceurl_21, value);
	}

	inline static int32_t get_offset_of_isNoUpdate_22() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___isNoUpdate_22)); }
	inline bool get_isNoUpdate_22() const { return ___isNoUpdate_22; }
	inline bool* get_address_of_isNoUpdate_22() { return &___isNoUpdate_22; }
	inline void set_isNoUpdate_22(bool value)
	{
		___isNoUpdate_22 = value;
	}

	inline static int32_t get_offset_of_vscfg_23() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___vscfg_23)); }
	inline String_t* get_vscfg_23() const { return ___vscfg_23; }
	inline String_t** get_address_of_vscfg_23() { return &___vscfg_23; }
	inline void set_vscfg_23(String_t* value)
	{
		___vscfg_23 = value;
		Il2CppCodeGenWriteBarrier(&___vscfg_23, value);
	}

	inline static int32_t get_offset_of_noRestartAssetsvs_24() { return static_cast<int32_t>(offsetof(VersionInfo_t2356638086, ___noRestartAssetsvs_24)); }
	inline String_t* get_noRestartAssetsvs_24() const { return ___noRestartAssetsvs_24; }
	inline String_t** get_address_of_noRestartAssetsvs_24() { return &___noRestartAssetsvs_24; }
	inline void set_noRestartAssetsvs_24(String_t* value)
	{
		___noRestartAssetsvs_24 = value;
		Il2CppCodeGenWriteBarrier(&___noRestartAssetsvs_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
