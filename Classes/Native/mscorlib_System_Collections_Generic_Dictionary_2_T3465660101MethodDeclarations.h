﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,Mihua.Assets.SubAssetMgr/ErrorInfo>
struct Transform_1_t3465660101;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr_ErrorIn2633981210.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m845981799_gshared (Transform_1_t3465660101 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m845981799(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3465660101 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m845981799_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,Mihua.Assets.SubAssetMgr/ErrorInfo>::Invoke(TKey,TValue)
extern "C"  ErrorInfo_t2633981210  Transform_1_Invoke_m2387463989_gshared (Transform_1_t3465660101 * __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m2387463989(__this, ___key0, ___value1, method) ((  ErrorInfo_t2633981210  (*) (Transform_1_t3465660101 *, Il2CppObject *, ErrorInfo_t2633981210 , const MethodInfo*))Transform_1_Invoke_m2387463989_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,Mihua.Assets.SubAssetMgr/ErrorInfo>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3619760660_gshared (Transform_1_t3465660101 * __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m3619760660(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3465660101 *, Il2CppObject *, ErrorInfo_t2633981210 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m3619760660_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo,Mihua.Assets.SubAssetMgr/ErrorInfo>::EndInvoke(System.IAsyncResult)
extern "C"  ErrorInfo_t2633981210  Transform_1_EndInvoke_m3065818677_gshared (Transform_1_t3465660101 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m3065818677(__this, ___result0, method) ((  ErrorInfo_t2633981210  (*) (Transform_1_t3465660101 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3065818677_gshared)(__this, ___result0, method)
