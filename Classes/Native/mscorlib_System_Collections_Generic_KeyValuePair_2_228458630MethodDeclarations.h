﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22065771578MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<CombatEntity,System.Single>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1710232253(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t228458630 *, CombatEntity_t684137495 *, float, const MethodInfo*))KeyValuePair_2__ctor_m2908028374_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<CombatEntity,System.Single>::get_Key()
#define KeyValuePair_2_get_Key_m3449753739(__this, method) ((  CombatEntity_t684137495 * (*) (KeyValuePair_2_t228458630 *, const MethodInfo*))KeyValuePair_2_get_Key_m975404242_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<CombatEntity,System.Single>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1889781068(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t228458630 *, CombatEntity_t684137495 *, const MethodInfo*))KeyValuePair_2_set_Key_m662474387_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<CombatEntity,System.Single>::get_Value()
#define KeyValuePair_2_get_Value_m2628895755(__this, method) ((  float (*) (KeyValuePair_2_t228458630 *, const MethodInfo*))KeyValuePair_2_get_Value_m2222463222_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<CombatEntity,System.Single>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m935564108(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t228458630 *, float, const MethodInfo*))KeyValuePair_2_set_Value_m3606602003_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<CombatEntity,System.Single>::ToString()
#define KeyValuePair_2_ToString_m1911740054(__this, method) ((  String_t* (*) (KeyValuePair_2_t228458630 *, const MethodInfo*))KeyValuePair_2_ToString_m3615079765_gshared)(__this, method)
