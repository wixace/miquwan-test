﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ClientGenerated/<Client_Run_GetDelegate_member7_arg3>c__AnonStorey56
struct U3CClient_Run_GetDelegate_member7_arg3U3Ec__AnonStorey56_t287378116;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ClientGenerated/<Client_Run_GetDelegate_member7_arg3>c__AnonStorey56::.ctor()
extern "C"  void U3CClient_Run_GetDelegate_member7_arg3U3Ec__AnonStorey56__ctor_m688364631 (U3CClient_Run_GetDelegate_member7_arg3U3Ec__AnonStorey56_t287378116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClientGenerated/<Client_Run_GetDelegate_member7_arg3>c__AnonStorey56::<>m__2E(System.String,System.String)
extern "C"  void U3CClient_Run_GetDelegate_member7_arg3U3Ec__AnonStorey56_U3CU3Em__2E_m42897643 (U3CClient_Run_GetDelegate_member7_arg3U3Ec__AnonStorey56_t287378116 * __this, String_t* ___route0, String_t* ___jsonString1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
