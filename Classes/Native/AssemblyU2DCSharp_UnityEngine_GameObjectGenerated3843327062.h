﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MethodID
struct MethodID_t3916401116;
// JSDataExchangeMgr/DGetV`1<System.Type[]>
struct DGetV_1_t3216658408;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine_GameObjectGenerated
struct  UnityEngine_GameObjectGenerated_t3843327062  : public Il2CppObject
{
public:

public:
};

struct UnityEngine_GameObjectGenerated_t3843327062_StaticFields
{
public:
	// MethodID UnityEngine_GameObjectGenerated::methodID1
	MethodID_t3916401116 * ___methodID1_0;
	// MethodID UnityEngine_GameObjectGenerated::methodID9
	MethodID_t3916401116 * ___methodID9_1;
	// MethodID UnityEngine_GameObjectGenerated::methodID11
	MethodID_t3916401116 * ___methodID11_2;
	// MethodID UnityEngine_GameObjectGenerated::methodID13
	MethodID_t3916401116 * ___methodID13_3;
	// MethodID UnityEngine_GameObjectGenerated::methodID15
	MethodID_t3916401116 * ___methodID15_4;
	// MethodID UnityEngine_GameObjectGenerated::methodID17
	MethodID_t3916401116 * ___methodID17_5;
	// MethodID UnityEngine_GameObjectGenerated::methodID19
	MethodID_t3916401116 * ___methodID19_6;
	// MethodID UnityEngine_GameObjectGenerated::methodID21
	MethodID_t3916401116 * ___methodID21_7;
	// MethodID UnityEngine_GameObjectGenerated::methodID22
	MethodID_t3916401116 * ___methodID22_8;
	// MethodID UnityEngine_GameObjectGenerated::methodID24
	MethodID_t3916401116 * ___methodID24_9;
	// MethodID UnityEngine_GameObjectGenerated::methodID25
	MethodID_t3916401116 * ___methodID25_10;
	// MethodID UnityEngine_GameObjectGenerated::methodID26
	MethodID_t3916401116 * ___methodID26_11;
	// MethodID UnityEngine_GameObjectGenerated::methodID28
	MethodID_t3916401116 * ___methodID28_12;
	// MethodID UnityEngine_GameObjectGenerated::methodID30
	MethodID_t3916401116 * ___methodID30_13;
	// JSDataExchangeMgr/DGetV`1<System.Type[]> UnityEngine_GameObjectGenerated::<>f__am$cacheE
	DGetV_1_t3216658408 * ___U3CU3Ef__amU24cacheE_14;

public:
	inline static int32_t get_offset_of_methodID1_0() { return static_cast<int32_t>(offsetof(UnityEngine_GameObjectGenerated_t3843327062_StaticFields, ___methodID1_0)); }
	inline MethodID_t3916401116 * get_methodID1_0() const { return ___methodID1_0; }
	inline MethodID_t3916401116 ** get_address_of_methodID1_0() { return &___methodID1_0; }
	inline void set_methodID1_0(MethodID_t3916401116 * value)
	{
		___methodID1_0 = value;
		Il2CppCodeGenWriteBarrier(&___methodID1_0, value);
	}

	inline static int32_t get_offset_of_methodID9_1() { return static_cast<int32_t>(offsetof(UnityEngine_GameObjectGenerated_t3843327062_StaticFields, ___methodID9_1)); }
	inline MethodID_t3916401116 * get_methodID9_1() const { return ___methodID9_1; }
	inline MethodID_t3916401116 ** get_address_of_methodID9_1() { return &___methodID9_1; }
	inline void set_methodID9_1(MethodID_t3916401116 * value)
	{
		___methodID9_1 = value;
		Il2CppCodeGenWriteBarrier(&___methodID9_1, value);
	}

	inline static int32_t get_offset_of_methodID11_2() { return static_cast<int32_t>(offsetof(UnityEngine_GameObjectGenerated_t3843327062_StaticFields, ___methodID11_2)); }
	inline MethodID_t3916401116 * get_methodID11_2() const { return ___methodID11_2; }
	inline MethodID_t3916401116 ** get_address_of_methodID11_2() { return &___methodID11_2; }
	inline void set_methodID11_2(MethodID_t3916401116 * value)
	{
		___methodID11_2 = value;
		Il2CppCodeGenWriteBarrier(&___methodID11_2, value);
	}

	inline static int32_t get_offset_of_methodID13_3() { return static_cast<int32_t>(offsetof(UnityEngine_GameObjectGenerated_t3843327062_StaticFields, ___methodID13_3)); }
	inline MethodID_t3916401116 * get_methodID13_3() const { return ___methodID13_3; }
	inline MethodID_t3916401116 ** get_address_of_methodID13_3() { return &___methodID13_3; }
	inline void set_methodID13_3(MethodID_t3916401116 * value)
	{
		___methodID13_3 = value;
		Il2CppCodeGenWriteBarrier(&___methodID13_3, value);
	}

	inline static int32_t get_offset_of_methodID15_4() { return static_cast<int32_t>(offsetof(UnityEngine_GameObjectGenerated_t3843327062_StaticFields, ___methodID15_4)); }
	inline MethodID_t3916401116 * get_methodID15_4() const { return ___methodID15_4; }
	inline MethodID_t3916401116 ** get_address_of_methodID15_4() { return &___methodID15_4; }
	inline void set_methodID15_4(MethodID_t3916401116 * value)
	{
		___methodID15_4 = value;
		Il2CppCodeGenWriteBarrier(&___methodID15_4, value);
	}

	inline static int32_t get_offset_of_methodID17_5() { return static_cast<int32_t>(offsetof(UnityEngine_GameObjectGenerated_t3843327062_StaticFields, ___methodID17_5)); }
	inline MethodID_t3916401116 * get_methodID17_5() const { return ___methodID17_5; }
	inline MethodID_t3916401116 ** get_address_of_methodID17_5() { return &___methodID17_5; }
	inline void set_methodID17_5(MethodID_t3916401116 * value)
	{
		___methodID17_5 = value;
		Il2CppCodeGenWriteBarrier(&___methodID17_5, value);
	}

	inline static int32_t get_offset_of_methodID19_6() { return static_cast<int32_t>(offsetof(UnityEngine_GameObjectGenerated_t3843327062_StaticFields, ___methodID19_6)); }
	inline MethodID_t3916401116 * get_methodID19_6() const { return ___methodID19_6; }
	inline MethodID_t3916401116 ** get_address_of_methodID19_6() { return &___methodID19_6; }
	inline void set_methodID19_6(MethodID_t3916401116 * value)
	{
		___methodID19_6 = value;
		Il2CppCodeGenWriteBarrier(&___methodID19_6, value);
	}

	inline static int32_t get_offset_of_methodID21_7() { return static_cast<int32_t>(offsetof(UnityEngine_GameObjectGenerated_t3843327062_StaticFields, ___methodID21_7)); }
	inline MethodID_t3916401116 * get_methodID21_7() const { return ___methodID21_7; }
	inline MethodID_t3916401116 ** get_address_of_methodID21_7() { return &___methodID21_7; }
	inline void set_methodID21_7(MethodID_t3916401116 * value)
	{
		___methodID21_7 = value;
		Il2CppCodeGenWriteBarrier(&___methodID21_7, value);
	}

	inline static int32_t get_offset_of_methodID22_8() { return static_cast<int32_t>(offsetof(UnityEngine_GameObjectGenerated_t3843327062_StaticFields, ___methodID22_8)); }
	inline MethodID_t3916401116 * get_methodID22_8() const { return ___methodID22_8; }
	inline MethodID_t3916401116 ** get_address_of_methodID22_8() { return &___methodID22_8; }
	inline void set_methodID22_8(MethodID_t3916401116 * value)
	{
		___methodID22_8 = value;
		Il2CppCodeGenWriteBarrier(&___methodID22_8, value);
	}

	inline static int32_t get_offset_of_methodID24_9() { return static_cast<int32_t>(offsetof(UnityEngine_GameObjectGenerated_t3843327062_StaticFields, ___methodID24_9)); }
	inline MethodID_t3916401116 * get_methodID24_9() const { return ___methodID24_9; }
	inline MethodID_t3916401116 ** get_address_of_methodID24_9() { return &___methodID24_9; }
	inline void set_methodID24_9(MethodID_t3916401116 * value)
	{
		___methodID24_9 = value;
		Il2CppCodeGenWriteBarrier(&___methodID24_9, value);
	}

	inline static int32_t get_offset_of_methodID25_10() { return static_cast<int32_t>(offsetof(UnityEngine_GameObjectGenerated_t3843327062_StaticFields, ___methodID25_10)); }
	inline MethodID_t3916401116 * get_methodID25_10() const { return ___methodID25_10; }
	inline MethodID_t3916401116 ** get_address_of_methodID25_10() { return &___methodID25_10; }
	inline void set_methodID25_10(MethodID_t3916401116 * value)
	{
		___methodID25_10 = value;
		Il2CppCodeGenWriteBarrier(&___methodID25_10, value);
	}

	inline static int32_t get_offset_of_methodID26_11() { return static_cast<int32_t>(offsetof(UnityEngine_GameObjectGenerated_t3843327062_StaticFields, ___methodID26_11)); }
	inline MethodID_t3916401116 * get_methodID26_11() const { return ___methodID26_11; }
	inline MethodID_t3916401116 ** get_address_of_methodID26_11() { return &___methodID26_11; }
	inline void set_methodID26_11(MethodID_t3916401116 * value)
	{
		___methodID26_11 = value;
		Il2CppCodeGenWriteBarrier(&___methodID26_11, value);
	}

	inline static int32_t get_offset_of_methodID28_12() { return static_cast<int32_t>(offsetof(UnityEngine_GameObjectGenerated_t3843327062_StaticFields, ___methodID28_12)); }
	inline MethodID_t3916401116 * get_methodID28_12() const { return ___methodID28_12; }
	inline MethodID_t3916401116 ** get_address_of_methodID28_12() { return &___methodID28_12; }
	inline void set_methodID28_12(MethodID_t3916401116 * value)
	{
		___methodID28_12 = value;
		Il2CppCodeGenWriteBarrier(&___methodID28_12, value);
	}

	inline static int32_t get_offset_of_methodID30_13() { return static_cast<int32_t>(offsetof(UnityEngine_GameObjectGenerated_t3843327062_StaticFields, ___methodID30_13)); }
	inline MethodID_t3916401116 * get_methodID30_13() const { return ___methodID30_13; }
	inline MethodID_t3916401116 ** get_address_of_methodID30_13() { return &___methodID30_13; }
	inline void set_methodID30_13(MethodID_t3916401116 * value)
	{
		___methodID30_13 = value;
		Il2CppCodeGenWriteBarrier(&___methodID30_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_14() { return static_cast<int32_t>(offsetof(UnityEngine_GameObjectGenerated_t3843327062_StaticFields, ___U3CU3Ef__amU24cacheE_14)); }
	inline DGetV_1_t3216658408 * get_U3CU3Ef__amU24cacheE_14() const { return ___U3CU3Ef__amU24cacheE_14; }
	inline DGetV_1_t3216658408 ** get_address_of_U3CU3Ef__amU24cacheE_14() { return &___U3CU3Ef__amU24cacheE_14; }
	inline void set_U3CU3Ef__amU24cacheE_14(DGetV_1_t3216658408 * value)
	{
		___U3CU3Ef__amU24cacheE_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
