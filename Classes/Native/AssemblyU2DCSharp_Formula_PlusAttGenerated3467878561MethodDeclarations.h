﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Formula_PlusAttGenerated
struct Formula_PlusAttGenerated_t3467878561;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Formula_PlusAttGenerated::.ctor()
extern "C"  void Formula_PlusAttGenerated__ctor_m349311770 (Formula_PlusAttGenerated_t3467878561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Formula_PlusAttGenerated::PlusAtt_PlusAtt1(JSVCall,System.Int32)
extern "C"  bool Formula_PlusAttGenerated_PlusAtt_PlusAtt1_m2826088784 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Formula_PlusAttGenerated::PlusAtt_hp(JSVCall)
extern "C"  void Formula_PlusAttGenerated_PlusAtt_hp_m1334678685 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Formula_PlusAttGenerated::PlusAtt_attack(JSVCall)
extern "C"  void Formula_PlusAttGenerated_PlusAtt_attack_m2467263453 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Formula_PlusAttGenerated::PlusAtt_phy_def(JSVCall)
extern "C"  void Formula_PlusAttGenerated_PlusAtt_phy_def_m949756256 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Formula_PlusAttGenerated::PlusAtt_mag_def(JSVCall)
extern "C"  void Formula_PlusAttGenerated_PlusAtt_mag_def_m1028551374 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Formula_PlusAttGenerated::__Register()
extern "C"  void Formula_PlusAttGenerated___Register_m478235117 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Formula_PlusAttGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void Formula_PlusAttGenerated_ilo_addJSCSRel1_m1428137174 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Formula_PlusAttGenerated::ilo_getUInt322(System.Int32)
extern "C"  uint32_t Formula_PlusAttGenerated_ilo_getUInt322_m587194284 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Formula_PlusAttGenerated::ilo_setUInt323(System.Int32,System.UInt32)
extern "C"  void Formula_PlusAttGenerated_ilo_setUInt323_m972524090 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
