﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._798919d13467fd36a9d43d7c7e0d3f07
struct _798919d13467fd36a9d43d7c7e0d3f07_t1965012085;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._798919d13467fd36a9d43d7c7e0d3f07::.ctor()
extern "C"  void _798919d13467fd36a9d43d7c7e0d3f07__ctor_m3802047832 (_798919d13467fd36a9d43d7c7e0d3f07_t1965012085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._798919d13467fd36a9d43d7c7e0d3f07::_798919d13467fd36a9d43d7c7e0d3f07m2(System.Int32)
extern "C"  int32_t _798919d13467fd36a9d43d7c7e0d3f07__798919d13467fd36a9d43d7c7e0d3f07m2_m2858653049 (_798919d13467fd36a9d43d7c7e0d3f07_t1965012085 * __this, int32_t ____798919d13467fd36a9d43d7c7e0d3f07a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._798919d13467fd36a9d43d7c7e0d3f07::_798919d13467fd36a9d43d7c7e0d3f07m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _798919d13467fd36a9d43d7c7e0d3f07__798919d13467fd36a9d43d7c7e0d3f07m_m1544760669 (_798919d13467fd36a9d43d7c7e0d3f07_t1965012085 * __this, int32_t ____798919d13467fd36a9d43d7c7e0d3f07a0, int32_t ____798919d13467fd36a9d43d7c7e0d3f0771, int32_t ____798919d13467fd36a9d43d7c7e0d3f07c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
