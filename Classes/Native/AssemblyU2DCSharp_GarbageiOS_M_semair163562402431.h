﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_semair163
struct  M_semair163_t562402431  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_semair163::_stapeXarvo
	bool ____stapeXarvo_0;
	// System.String GarbageiOS.M_semair163::_qekawSoupaixal
	String_t* ____qekawSoupaixal_1;
	// System.UInt32 GarbageiOS.M_semair163::_gurpitoRallmea
	uint32_t ____gurpitoRallmea_2;
	// System.UInt32 GarbageiOS.M_semair163::_drasrebou
	uint32_t ____drasrebou_3;
	// System.Boolean GarbageiOS.M_semair163::_zarmoSelrairle
	bool ____zarmoSelrairle_4;
	// System.String GarbageiOS.M_semair163::_cairvonal
	String_t* ____cairvonal_5;
	// System.String GarbageiOS.M_semair163::_ceestefouStouda
	String_t* ____ceestefouStouda_6;
	// System.String GarbageiOS.M_semair163::_cepetereBaremci
	String_t* ____cepetereBaremci_7;
	// System.String GarbageiOS.M_semair163::_marereteaTairsarsa
	String_t* ____marereteaTairsarsa_8;
	// System.Boolean GarbageiOS.M_semair163::_sena
	bool ____sena_9;
	// System.Boolean GarbageiOS.M_semair163::_surtajuWhahor
	bool ____surtajuWhahor_10;
	// System.UInt32 GarbageiOS.M_semair163::_saytouBasfelsta
	uint32_t ____saytouBasfelsta_11;
	// System.UInt32 GarbageiOS.M_semair163::_wainor
	uint32_t ____wainor_12;
	// System.UInt32 GarbageiOS.M_semair163::_bemiGileatrear
	uint32_t ____bemiGileatrear_13;
	// System.Boolean GarbageiOS.M_semair163::_tastreajai
	bool ____tastreajai_14;
	// System.Int32 GarbageiOS.M_semair163::_reechoomirSerzi
	int32_t ____reechoomirSerzi_15;
	// System.Int32 GarbageiOS.M_semair163::_sunalkeRalljear
	int32_t ____sunalkeRalljear_16;
	// System.UInt32 GarbageiOS.M_semair163::_dedearSterawso
	uint32_t ____dedearSterawso_17;
	// System.UInt32 GarbageiOS.M_semair163::_jabou
	uint32_t ____jabou_18;
	// System.UInt32 GarbageiOS.M_semair163::_voupa
	uint32_t ____voupa_19;
	// System.UInt32 GarbageiOS.M_semair163::_sowcer
	uint32_t ____sowcer_20;
	// System.UInt32 GarbageiOS.M_semair163::_roujowni
	uint32_t ____roujowni_21;
	// System.Int32 GarbageiOS.M_semair163::_moteKornal
	int32_t ____moteKornal_22;
	// System.String GarbageiOS.M_semair163::_jachaiNotal
	String_t* ____jachaiNotal_23;
	// System.Int32 GarbageiOS.M_semair163::_wowsa
	int32_t ____wowsa_24;
	// System.Single GarbageiOS.M_semair163::_hemwo
	float ____hemwo_25;
	// System.Boolean GarbageiOS.M_semair163::_kamow
	bool ____kamow_26;
	// System.Boolean GarbageiOS.M_semair163::_lirbissall
	bool ____lirbissall_27;
	// System.Boolean GarbageiOS.M_semair163::_healake
	bool ____healake_28;
	// System.Int32 GarbageiOS.M_semair163::_kemdrerFopal
	int32_t ____kemdrerFopal_29;

public:
	inline static int32_t get_offset_of__stapeXarvo_0() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____stapeXarvo_0)); }
	inline bool get__stapeXarvo_0() const { return ____stapeXarvo_0; }
	inline bool* get_address_of__stapeXarvo_0() { return &____stapeXarvo_0; }
	inline void set__stapeXarvo_0(bool value)
	{
		____stapeXarvo_0 = value;
	}

	inline static int32_t get_offset_of__qekawSoupaixal_1() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____qekawSoupaixal_1)); }
	inline String_t* get__qekawSoupaixal_1() const { return ____qekawSoupaixal_1; }
	inline String_t** get_address_of__qekawSoupaixal_1() { return &____qekawSoupaixal_1; }
	inline void set__qekawSoupaixal_1(String_t* value)
	{
		____qekawSoupaixal_1 = value;
		Il2CppCodeGenWriteBarrier(&____qekawSoupaixal_1, value);
	}

	inline static int32_t get_offset_of__gurpitoRallmea_2() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____gurpitoRallmea_2)); }
	inline uint32_t get__gurpitoRallmea_2() const { return ____gurpitoRallmea_2; }
	inline uint32_t* get_address_of__gurpitoRallmea_2() { return &____gurpitoRallmea_2; }
	inline void set__gurpitoRallmea_2(uint32_t value)
	{
		____gurpitoRallmea_2 = value;
	}

	inline static int32_t get_offset_of__drasrebou_3() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____drasrebou_3)); }
	inline uint32_t get__drasrebou_3() const { return ____drasrebou_3; }
	inline uint32_t* get_address_of__drasrebou_3() { return &____drasrebou_3; }
	inline void set__drasrebou_3(uint32_t value)
	{
		____drasrebou_3 = value;
	}

	inline static int32_t get_offset_of__zarmoSelrairle_4() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____zarmoSelrairle_4)); }
	inline bool get__zarmoSelrairle_4() const { return ____zarmoSelrairle_4; }
	inline bool* get_address_of__zarmoSelrairle_4() { return &____zarmoSelrairle_4; }
	inline void set__zarmoSelrairle_4(bool value)
	{
		____zarmoSelrairle_4 = value;
	}

	inline static int32_t get_offset_of__cairvonal_5() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____cairvonal_5)); }
	inline String_t* get__cairvonal_5() const { return ____cairvonal_5; }
	inline String_t** get_address_of__cairvonal_5() { return &____cairvonal_5; }
	inline void set__cairvonal_5(String_t* value)
	{
		____cairvonal_5 = value;
		Il2CppCodeGenWriteBarrier(&____cairvonal_5, value);
	}

	inline static int32_t get_offset_of__ceestefouStouda_6() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____ceestefouStouda_6)); }
	inline String_t* get__ceestefouStouda_6() const { return ____ceestefouStouda_6; }
	inline String_t** get_address_of__ceestefouStouda_6() { return &____ceestefouStouda_6; }
	inline void set__ceestefouStouda_6(String_t* value)
	{
		____ceestefouStouda_6 = value;
		Il2CppCodeGenWriteBarrier(&____ceestefouStouda_6, value);
	}

	inline static int32_t get_offset_of__cepetereBaremci_7() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____cepetereBaremci_7)); }
	inline String_t* get__cepetereBaremci_7() const { return ____cepetereBaremci_7; }
	inline String_t** get_address_of__cepetereBaremci_7() { return &____cepetereBaremci_7; }
	inline void set__cepetereBaremci_7(String_t* value)
	{
		____cepetereBaremci_7 = value;
		Il2CppCodeGenWriteBarrier(&____cepetereBaremci_7, value);
	}

	inline static int32_t get_offset_of__marereteaTairsarsa_8() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____marereteaTairsarsa_8)); }
	inline String_t* get__marereteaTairsarsa_8() const { return ____marereteaTairsarsa_8; }
	inline String_t** get_address_of__marereteaTairsarsa_8() { return &____marereteaTairsarsa_8; }
	inline void set__marereteaTairsarsa_8(String_t* value)
	{
		____marereteaTairsarsa_8 = value;
		Il2CppCodeGenWriteBarrier(&____marereteaTairsarsa_8, value);
	}

	inline static int32_t get_offset_of__sena_9() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____sena_9)); }
	inline bool get__sena_9() const { return ____sena_9; }
	inline bool* get_address_of__sena_9() { return &____sena_9; }
	inline void set__sena_9(bool value)
	{
		____sena_9 = value;
	}

	inline static int32_t get_offset_of__surtajuWhahor_10() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____surtajuWhahor_10)); }
	inline bool get__surtajuWhahor_10() const { return ____surtajuWhahor_10; }
	inline bool* get_address_of__surtajuWhahor_10() { return &____surtajuWhahor_10; }
	inline void set__surtajuWhahor_10(bool value)
	{
		____surtajuWhahor_10 = value;
	}

	inline static int32_t get_offset_of__saytouBasfelsta_11() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____saytouBasfelsta_11)); }
	inline uint32_t get__saytouBasfelsta_11() const { return ____saytouBasfelsta_11; }
	inline uint32_t* get_address_of__saytouBasfelsta_11() { return &____saytouBasfelsta_11; }
	inline void set__saytouBasfelsta_11(uint32_t value)
	{
		____saytouBasfelsta_11 = value;
	}

	inline static int32_t get_offset_of__wainor_12() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____wainor_12)); }
	inline uint32_t get__wainor_12() const { return ____wainor_12; }
	inline uint32_t* get_address_of__wainor_12() { return &____wainor_12; }
	inline void set__wainor_12(uint32_t value)
	{
		____wainor_12 = value;
	}

	inline static int32_t get_offset_of__bemiGileatrear_13() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____bemiGileatrear_13)); }
	inline uint32_t get__bemiGileatrear_13() const { return ____bemiGileatrear_13; }
	inline uint32_t* get_address_of__bemiGileatrear_13() { return &____bemiGileatrear_13; }
	inline void set__bemiGileatrear_13(uint32_t value)
	{
		____bemiGileatrear_13 = value;
	}

	inline static int32_t get_offset_of__tastreajai_14() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____tastreajai_14)); }
	inline bool get__tastreajai_14() const { return ____tastreajai_14; }
	inline bool* get_address_of__tastreajai_14() { return &____tastreajai_14; }
	inline void set__tastreajai_14(bool value)
	{
		____tastreajai_14 = value;
	}

	inline static int32_t get_offset_of__reechoomirSerzi_15() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____reechoomirSerzi_15)); }
	inline int32_t get__reechoomirSerzi_15() const { return ____reechoomirSerzi_15; }
	inline int32_t* get_address_of__reechoomirSerzi_15() { return &____reechoomirSerzi_15; }
	inline void set__reechoomirSerzi_15(int32_t value)
	{
		____reechoomirSerzi_15 = value;
	}

	inline static int32_t get_offset_of__sunalkeRalljear_16() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____sunalkeRalljear_16)); }
	inline int32_t get__sunalkeRalljear_16() const { return ____sunalkeRalljear_16; }
	inline int32_t* get_address_of__sunalkeRalljear_16() { return &____sunalkeRalljear_16; }
	inline void set__sunalkeRalljear_16(int32_t value)
	{
		____sunalkeRalljear_16 = value;
	}

	inline static int32_t get_offset_of__dedearSterawso_17() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____dedearSterawso_17)); }
	inline uint32_t get__dedearSterawso_17() const { return ____dedearSterawso_17; }
	inline uint32_t* get_address_of__dedearSterawso_17() { return &____dedearSterawso_17; }
	inline void set__dedearSterawso_17(uint32_t value)
	{
		____dedearSterawso_17 = value;
	}

	inline static int32_t get_offset_of__jabou_18() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____jabou_18)); }
	inline uint32_t get__jabou_18() const { return ____jabou_18; }
	inline uint32_t* get_address_of__jabou_18() { return &____jabou_18; }
	inline void set__jabou_18(uint32_t value)
	{
		____jabou_18 = value;
	}

	inline static int32_t get_offset_of__voupa_19() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____voupa_19)); }
	inline uint32_t get__voupa_19() const { return ____voupa_19; }
	inline uint32_t* get_address_of__voupa_19() { return &____voupa_19; }
	inline void set__voupa_19(uint32_t value)
	{
		____voupa_19 = value;
	}

	inline static int32_t get_offset_of__sowcer_20() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____sowcer_20)); }
	inline uint32_t get__sowcer_20() const { return ____sowcer_20; }
	inline uint32_t* get_address_of__sowcer_20() { return &____sowcer_20; }
	inline void set__sowcer_20(uint32_t value)
	{
		____sowcer_20 = value;
	}

	inline static int32_t get_offset_of__roujowni_21() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____roujowni_21)); }
	inline uint32_t get__roujowni_21() const { return ____roujowni_21; }
	inline uint32_t* get_address_of__roujowni_21() { return &____roujowni_21; }
	inline void set__roujowni_21(uint32_t value)
	{
		____roujowni_21 = value;
	}

	inline static int32_t get_offset_of__moteKornal_22() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____moteKornal_22)); }
	inline int32_t get__moteKornal_22() const { return ____moteKornal_22; }
	inline int32_t* get_address_of__moteKornal_22() { return &____moteKornal_22; }
	inline void set__moteKornal_22(int32_t value)
	{
		____moteKornal_22 = value;
	}

	inline static int32_t get_offset_of__jachaiNotal_23() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____jachaiNotal_23)); }
	inline String_t* get__jachaiNotal_23() const { return ____jachaiNotal_23; }
	inline String_t** get_address_of__jachaiNotal_23() { return &____jachaiNotal_23; }
	inline void set__jachaiNotal_23(String_t* value)
	{
		____jachaiNotal_23 = value;
		Il2CppCodeGenWriteBarrier(&____jachaiNotal_23, value);
	}

	inline static int32_t get_offset_of__wowsa_24() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____wowsa_24)); }
	inline int32_t get__wowsa_24() const { return ____wowsa_24; }
	inline int32_t* get_address_of__wowsa_24() { return &____wowsa_24; }
	inline void set__wowsa_24(int32_t value)
	{
		____wowsa_24 = value;
	}

	inline static int32_t get_offset_of__hemwo_25() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____hemwo_25)); }
	inline float get__hemwo_25() const { return ____hemwo_25; }
	inline float* get_address_of__hemwo_25() { return &____hemwo_25; }
	inline void set__hemwo_25(float value)
	{
		____hemwo_25 = value;
	}

	inline static int32_t get_offset_of__kamow_26() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____kamow_26)); }
	inline bool get__kamow_26() const { return ____kamow_26; }
	inline bool* get_address_of__kamow_26() { return &____kamow_26; }
	inline void set__kamow_26(bool value)
	{
		____kamow_26 = value;
	}

	inline static int32_t get_offset_of__lirbissall_27() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____lirbissall_27)); }
	inline bool get__lirbissall_27() const { return ____lirbissall_27; }
	inline bool* get_address_of__lirbissall_27() { return &____lirbissall_27; }
	inline void set__lirbissall_27(bool value)
	{
		____lirbissall_27 = value;
	}

	inline static int32_t get_offset_of__healake_28() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____healake_28)); }
	inline bool get__healake_28() const { return ____healake_28; }
	inline bool* get_address_of__healake_28() { return &____healake_28; }
	inline void set__healake_28(bool value)
	{
		____healake_28 = value;
	}

	inline static int32_t get_offset_of__kemdrerFopal_29() { return static_cast<int32_t>(offsetof(M_semair163_t562402431, ____kemdrerFopal_29)); }
	inline int32_t get__kemdrerFopal_29() const { return ____kemdrerFopal_29; }
	inline int32_t* get_address_of__kemdrerFopal_29() { return &____kemdrerFopal_29; }
	inline void set__kemdrerFopal_29(int32_t value)
	{
		____kemdrerFopal_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
