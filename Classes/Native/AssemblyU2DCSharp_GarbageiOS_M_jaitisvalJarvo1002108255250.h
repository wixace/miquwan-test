﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_jaitisvalJarvo100
struct  M_jaitisvalJarvo100_t2108255250  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_jaitisvalJarvo100::_cedreFarje
	bool ____cedreFarje_0;
	// System.Boolean GarbageiOS.M_jaitisvalJarvo100::_houremcairHermis
	bool ____houremcairHermis_1;
	// System.Single GarbageiOS.M_jaitisvalJarvo100::_sixooKene
	float ____sixooKene_2;
	// System.Int32 GarbageiOS.M_jaitisvalJarvo100::_whorqirjouBesearbur
	int32_t ____whorqirjouBesearbur_3;
	// System.UInt32 GarbageiOS.M_jaitisvalJarvo100::_sallapiKofaw
	uint32_t ____sallapiKofaw_4;
	// System.Int32 GarbageiOS.M_jaitisvalJarvo100::_jehel
	int32_t ____jehel_5;
	// System.Single GarbageiOS.M_jaitisvalJarvo100::_gaturGaise
	float ____gaturGaise_6;
	// System.Boolean GarbageiOS.M_jaitisvalJarvo100::_nouvirNealaqel
	bool ____nouvirNealaqel_7;
	// System.Boolean GarbageiOS.M_jaitisvalJarvo100::_surcaci
	bool ____surcaci_8;
	// System.UInt32 GarbageiOS.M_jaitisvalJarvo100::_pasdis
	uint32_t ____pasdis_9;
	// System.UInt32 GarbageiOS.M_jaitisvalJarvo100::_noreGourear
	uint32_t ____noreGourear_10;
	// System.Boolean GarbageiOS.M_jaitisvalJarvo100::_dusurbi
	bool ____dusurbi_11;
	// System.Boolean GarbageiOS.M_jaitisvalJarvo100::_drupoo
	bool ____drupoo_12;
	// System.String GarbageiOS.M_jaitisvalJarvo100::_puliKemhukow
	String_t* ____puliKemhukow_13;
	// System.Int32 GarbageiOS.M_jaitisvalJarvo100::_haihas
	int32_t ____haihas_14;
	// System.Single GarbageiOS.M_jaitisvalJarvo100::_drismou
	float ____drismou_15;
	// System.UInt32 GarbageiOS.M_jaitisvalJarvo100::_kouma
	uint32_t ____kouma_16;
	// System.UInt32 GarbageiOS.M_jaitisvalJarvo100::_jijaStayqai
	uint32_t ____jijaStayqai_17;
	// System.Single GarbageiOS.M_jaitisvalJarvo100::_risowCerchuchi
	float ____risowCerchuchi_18;
	// System.Single GarbageiOS.M_jaitisvalJarvo100::_mewhuSurrear
	float ____mewhuSurrear_19;
	// System.UInt32 GarbageiOS.M_jaitisvalJarvo100::_guliDelpe
	uint32_t ____guliDelpe_20;
	// System.UInt32 GarbageiOS.M_jaitisvalJarvo100::_guparstemXaswhe
	uint32_t ____guparstemXaswhe_21;
	// System.Int32 GarbageiOS.M_jaitisvalJarvo100::_temhouGallwur
	int32_t ____temhouGallwur_22;
	// System.Int32 GarbageiOS.M_jaitisvalJarvo100::_neawhasee
	int32_t ____neawhasee_23;
	// System.String GarbageiOS.M_jaitisvalJarvo100::_busosiPemceye
	String_t* ____busosiPemceye_24;
	// System.Int32 GarbageiOS.M_jaitisvalJarvo100::_celorteaBearmem
	int32_t ____celorteaBearmem_25;
	// System.Int32 GarbageiOS.M_jaitisvalJarvo100::_cehiskee
	int32_t ____cehiskee_26;
	// System.Int32 GarbageiOS.M_jaitisvalJarvo100::_poustaJoho
	int32_t ____poustaJoho_27;
	// System.UInt32 GarbageiOS.M_jaitisvalJarvo100::_pusaStiswa
	uint32_t ____pusaStiswa_28;
	// System.Single GarbageiOS.M_jaitisvalJarvo100::_pearzisnaiHeesemi
	float ____pearzisnaiHeesemi_29;
	// System.String GarbageiOS.M_jaitisvalJarvo100::_deedrur
	String_t* ____deedrur_30;
	// System.Int32 GarbageiOS.M_jaitisvalJarvo100::_togowsaMowzou
	int32_t ____togowsaMowzou_31;
	// System.Int32 GarbageiOS.M_jaitisvalJarvo100::_korkou
	int32_t ____korkou_32;
	// System.Single GarbageiOS.M_jaitisvalJarvo100::_wamormea
	float ____wamormea_33;
	// System.Int32 GarbageiOS.M_jaitisvalJarvo100::_poofastearSownou
	int32_t ____poofastearSownou_34;
	// System.UInt32 GarbageiOS.M_jaitisvalJarvo100::_mairfober
	uint32_t ____mairfober_35;
	// System.UInt32 GarbageiOS.M_jaitisvalJarvo100::_nojoukawReti
	uint32_t ____nojoukawReti_36;
	// System.UInt32 GarbageiOS.M_jaitisvalJarvo100::_tookelpiWheargir
	uint32_t ____tookelpiWheargir_37;
	// System.Int32 GarbageiOS.M_jaitisvalJarvo100::_staipoumo
	int32_t ____staipoumo_38;
	// System.Single GarbageiOS.M_jaitisvalJarvo100::_rearyelqi
	float ____rearyelqi_39;
	// System.Single GarbageiOS.M_jaitisvalJarvo100::_miburfis
	float ____miburfis_40;
	// System.UInt32 GarbageiOS.M_jaitisvalJarvo100::_hoopa
	uint32_t ____hoopa_41;
	// System.String GarbageiOS.M_jaitisvalJarvo100::_doriJorhefai
	String_t* ____doriJorhefai_42;
	// System.Boolean GarbageiOS.M_jaitisvalJarvo100::_huce
	bool ____huce_43;
	// System.Int32 GarbageiOS.M_jaitisvalJarvo100::_feawersi
	int32_t ____feawersi_44;
	// System.Boolean GarbageiOS.M_jaitisvalJarvo100::_trayrea
	bool ____trayrea_45;
	// System.String GarbageiOS.M_jaitisvalJarvo100::_sowdisdrisTroute
	String_t* ____sowdisdrisTroute_46;

public:
	inline static int32_t get_offset_of__cedreFarje_0() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____cedreFarje_0)); }
	inline bool get__cedreFarje_0() const { return ____cedreFarje_0; }
	inline bool* get_address_of__cedreFarje_0() { return &____cedreFarje_0; }
	inline void set__cedreFarje_0(bool value)
	{
		____cedreFarje_0 = value;
	}

	inline static int32_t get_offset_of__houremcairHermis_1() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____houremcairHermis_1)); }
	inline bool get__houremcairHermis_1() const { return ____houremcairHermis_1; }
	inline bool* get_address_of__houremcairHermis_1() { return &____houremcairHermis_1; }
	inline void set__houremcairHermis_1(bool value)
	{
		____houremcairHermis_1 = value;
	}

	inline static int32_t get_offset_of__sixooKene_2() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____sixooKene_2)); }
	inline float get__sixooKene_2() const { return ____sixooKene_2; }
	inline float* get_address_of__sixooKene_2() { return &____sixooKene_2; }
	inline void set__sixooKene_2(float value)
	{
		____sixooKene_2 = value;
	}

	inline static int32_t get_offset_of__whorqirjouBesearbur_3() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____whorqirjouBesearbur_3)); }
	inline int32_t get__whorqirjouBesearbur_3() const { return ____whorqirjouBesearbur_3; }
	inline int32_t* get_address_of__whorqirjouBesearbur_3() { return &____whorqirjouBesearbur_3; }
	inline void set__whorqirjouBesearbur_3(int32_t value)
	{
		____whorqirjouBesearbur_3 = value;
	}

	inline static int32_t get_offset_of__sallapiKofaw_4() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____sallapiKofaw_4)); }
	inline uint32_t get__sallapiKofaw_4() const { return ____sallapiKofaw_4; }
	inline uint32_t* get_address_of__sallapiKofaw_4() { return &____sallapiKofaw_4; }
	inline void set__sallapiKofaw_4(uint32_t value)
	{
		____sallapiKofaw_4 = value;
	}

	inline static int32_t get_offset_of__jehel_5() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____jehel_5)); }
	inline int32_t get__jehel_5() const { return ____jehel_5; }
	inline int32_t* get_address_of__jehel_5() { return &____jehel_5; }
	inline void set__jehel_5(int32_t value)
	{
		____jehel_5 = value;
	}

	inline static int32_t get_offset_of__gaturGaise_6() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____gaturGaise_6)); }
	inline float get__gaturGaise_6() const { return ____gaturGaise_6; }
	inline float* get_address_of__gaturGaise_6() { return &____gaturGaise_6; }
	inline void set__gaturGaise_6(float value)
	{
		____gaturGaise_6 = value;
	}

	inline static int32_t get_offset_of__nouvirNealaqel_7() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____nouvirNealaqel_7)); }
	inline bool get__nouvirNealaqel_7() const { return ____nouvirNealaqel_7; }
	inline bool* get_address_of__nouvirNealaqel_7() { return &____nouvirNealaqel_7; }
	inline void set__nouvirNealaqel_7(bool value)
	{
		____nouvirNealaqel_7 = value;
	}

	inline static int32_t get_offset_of__surcaci_8() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____surcaci_8)); }
	inline bool get__surcaci_8() const { return ____surcaci_8; }
	inline bool* get_address_of__surcaci_8() { return &____surcaci_8; }
	inline void set__surcaci_8(bool value)
	{
		____surcaci_8 = value;
	}

	inline static int32_t get_offset_of__pasdis_9() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____pasdis_9)); }
	inline uint32_t get__pasdis_9() const { return ____pasdis_9; }
	inline uint32_t* get_address_of__pasdis_9() { return &____pasdis_9; }
	inline void set__pasdis_9(uint32_t value)
	{
		____pasdis_9 = value;
	}

	inline static int32_t get_offset_of__noreGourear_10() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____noreGourear_10)); }
	inline uint32_t get__noreGourear_10() const { return ____noreGourear_10; }
	inline uint32_t* get_address_of__noreGourear_10() { return &____noreGourear_10; }
	inline void set__noreGourear_10(uint32_t value)
	{
		____noreGourear_10 = value;
	}

	inline static int32_t get_offset_of__dusurbi_11() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____dusurbi_11)); }
	inline bool get__dusurbi_11() const { return ____dusurbi_11; }
	inline bool* get_address_of__dusurbi_11() { return &____dusurbi_11; }
	inline void set__dusurbi_11(bool value)
	{
		____dusurbi_11 = value;
	}

	inline static int32_t get_offset_of__drupoo_12() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____drupoo_12)); }
	inline bool get__drupoo_12() const { return ____drupoo_12; }
	inline bool* get_address_of__drupoo_12() { return &____drupoo_12; }
	inline void set__drupoo_12(bool value)
	{
		____drupoo_12 = value;
	}

	inline static int32_t get_offset_of__puliKemhukow_13() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____puliKemhukow_13)); }
	inline String_t* get__puliKemhukow_13() const { return ____puliKemhukow_13; }
	inline String_t** get_address_of__puliKemhukow_13() { return &____puliKemhukow_13; }
	inline void set__puliKemhukow_13(String_t* value)
	{
		____puliKemhukow_13 = value;
		Il2CppCodeGenWriteBarrier(&____puliKemhukow_13, value);
	}

	inline static int32_t get_offset_of__haihas_14() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____haihas_14)); }
	inline int32_t get__haihas_14() const { return ____haihas_14; }
	inline int32_t* get_address_of__haihas_14() { return &____haihas_14; }
	inline void set__haihas_14(int32_t value)
	{
		____haihas_14 = value;
	}

	inline static int32_t get_offset_of__drismou_15() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____drismou_15)); }
	inline float get__drismou_15() const { return ____drismou_15; }
	inline float* get_address_of__drismou_15() { return &____drismou_15; }
	inline void set__drismou_15(float value)
	{
		____drismou_15 = value;
	}

	inline static int32_t get_offset_of__kouma_16() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____kouma_16)); }
	inline uint32_t get__kouma_16() const { return ____kouma_16; }
	inline uint32_t* get_address_of__kouma_16() { return &____kouma_16; }
	inline void set__kouma_16(uint32_t value)
	{
		____kouma_16 = value;
	}

	inline static int32_t get_offset_of__jijaStayqai_17() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____jijaStayqai_17)); }
	inline uint32_t get__jijaStayqai_17() const { return ____jijaStayqai_17; }
	inline uint32_t* get_address_of__jijaStayqai_17() { return &____jijaStayqai_17; }
	inline void set__jijaStayqai_17(uint32_t value)
	{
		____jijaStayqai_17 = value;
	}

	inline static int32_t get_offset_of__risowCerchuchi_18() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____risowCerchuchi_18)); }
	inline float get__risowCerchuchi_18() const { return ____risowCerchuchi_18; }
	inline float* get_address_of__risowCerchuchi_18() { return &____risowCerchuchi_18; }
	inline void set__risowCerchuchi_18(float value)
	{
		____risowCerchuchi_18 = value;
	}

	inline static int32_t get_offset_of__mewhuSurrear_19() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____mewhuSurrear_19)); }
	inline float get__mewhuSurrear_19() const { return ____mewhuSurrear_19; }
	inline float* get_address_of__mewhuSurrear_19() { return &____mewhuSurrear_19; }
	inline void set__mewhuSurrear_19(float value)
	{
		____mewhuSurrear_19 = value;
	}

	inline static int32_t get_offset_of__guliDelpe_20() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____guliDelpe_20)); }
	inline uint32_t get__guliDelpe_20() const { return ____guliDelpe_20; }
	inline uint32_t* get_address_of__guliDelpe_20() { return &____guliDelpe_20; }
	inline void set__guliDelpe_20(uint32_t value)
	{
		____guliDelpe_20 = value;
	}

	inline static int32_t get_offset_of__guparstemXaswhe_21() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____guparstemXaswhe_21)); }
	inline uint32_t get__guparstemXaswhe_21() const { return ____guparstemXaswhe_21; }
	inline uint32_t* get_address_of__guparstemXaswhe_21() { return &____guparstemXaswhe_21; }
	inline void set__guparstemXaswhe_21(uint32_t value)
	{
		____guparstemXaswhe_21 = value;
	}

	inline static int32_t get_offset_of__temhouGallwur_22() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____temhouGallwur_22)); }
	inline int32_t get__temhouGallwur_22() const { return ____temhouGallwur_22; }
	inline int32_t* get_address_of__temhouGallwur_22() { return &____temhouGallwur_22; }
	inline void set__temhouGallwur_22(int32_t value)
	{
		____temhouGallwur_22 = value;
	}

	inline static int32_t get_offset_of__neawhasee_23() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____neawhasee_23)); }
	inline int32_t get__neawhasee_23() const { return ____neawhasee_23; }
	inline int32_t* get_address_of__neawhasee_23() { return &____neawhasee_23; }
	inline void set__neawhasee_23(int32_t value)
	{
		____neawhasee_23 = value;
	}

	inline static int32_t get_offset_of__busosiPemceye_24() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____busosiPemceye_24)); }
	inline String_t* get__busosiPemceye_24() const { return ____busosiPemceye_24; }
	inline String_t** get_address_of__busosiPemceye_24() { return &____busosiPemceye_24; }
	inline void set__busosiPemceye_24(String_t* value)
	{
		____busosiPemceye_24 = value;
		Il2CppCodeGenWriteBarrier(&____busosiPemceye_24, value);
	}

	inline static int32_t get_offset_of__celorteaBearmem_25() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____celorteaBearmem_25)); }
	inline int32_t get__celorteaBearmem_25() const { return ____celorteaBearmem_25; }
	inline int32_t* get_address_of__celorteaBearmem_25() { return &____celorteaBearmem_25; }
	inline void set__celorteaBearmem_25(int32_t value)
	{
		____celorteaBearmem_25 = value;
	}

	inline static int32_t get_offset_of__cehiskee_26() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____cehiskee_26)); }
	inline int32_t get__cehiskee_26() const { return ____cehiskee_26; }
	inline int32_t* get_address_of__cehiskee_26() { return &____cehiskee_26; }
	inline void set__cehiskee_26(int32_t value)
	{
		____cehiskee_26 = value;
	}

	inline static int32_t get_offset_of__poustaJoho_27() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____poustaJoho_27)); }
	inline int32_t get__poustaJoho_27() const { return ____poustaJoho_27; }
	inline int32_t* get_address_of__poustaJoho_27() { return &____poustaJoho_27; }
	inline void set__poustaJoho_27(int32_t value)
	{
		____poustaJoho_27 = value;
	}

	inline static int32_t get_offset_of__pusaStiswa_28() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____pusaStiswa_28)); }
	inline uint32_t get__pusaStiswa_28() const { return ____pusaStiswa_28; }
	inline uint32_t* get_address_of__pusaStiswa_28() { return &____pusaStiswa_28; }
	inline void set__pusaStiswa_28(uint32_t value)
	{
		____pusaStiswa_28 = value;
	}

	inline static int32_t get_offset_of__pearzisnaiHeesemi_29() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____pearzisnaiHeesemi_29)); }
	inline float get__pearzisnaiHeesemi_29() const { return ____pearzisnaiHeesemi_29; }
	inline float* get_address_of__pearzisnaiHeesemi_29() { return &____pearzisnaiHeesemi_29; }
	inline void set__pearzisnaiHeesemi_29(float value)
	{
		____pearzisnaiHeesemi_29 = value;
	}

	inline static int32_t get_offset_of__deedrur_30() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____deedrur_30)); }
	inline String_t* get__deedrur_30() const { return ____deedrur_30; }
	inline String_t** get_address_of__deedrur_30() { return &____deedrur_30; }
	inline void set__deedrur_30(String_t* value)
	{
		____deedrur_30 = value;
		Il2CppCodeGenWriteBarrier(&____deedrur_30, value);
	}

	inline static int32_t get_offset_of__togowsaMowzou_31() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____togowsaMowzou_31)); }
	inline int32_t get__togowsaMowzou_31() const { return ____togowsaMowzou_31; }
	inline int32_t* get_address_of__togowsaMowzou_31() { return &____togowsaMowzou_31; }
	inline void set__togowsaMowzou_31(int32_t value)
	{
		____togowsaMowzou_31 = value;
	}

	inline static int32_t get_offset_of__korkou_32() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____korkou_32)); }
	inline int32_t get__korkou_32() const { return ____korkou_32; }
	inline int32_t* get_address_of__korkou_32() { return &____korkou_32; }
	inline void set__korkou_32(int32_t value)
	{
		____korkou_32 = value;
	}

	inline static int32_t get_offset_of__wamormea_33() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____wamormea_33)); }
	inline float get__wamormea_33() const { return ____wamormea_33; }
	inline float* get_address_of__wamormea_33() { return &____wamormea_33; }
	inline void set__wamormea_33(float value)
	{
		____wamormea_33 = value;
	}

	inline static int32_t get_offset_of__poofastearSownou_34() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____poofastearSownou_34)); }
	inline int32_t get__poofastearSownou_34() const { return ____poofastearSownou_34; }
	inline int32_t* get_address_of__poofastearSownou_34() { return &____poofastearSownou_34; }
	inline void set__poofastearSownou_34(int32_t value)
	{
		____poofastearSownou_34 = value;
	}

	inline static int32_t get_offset_of__mairfober_35() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____mairfober_35)); }
	inline uint32_t get__mairfober_35() const { return ____mairfober_35; }
	inline uint32_t* get_address_of__mairfober_35() { return &____mairfober_35; }
	inline void set__mairfober_35(uint32_t value)
	{
		____mairfober_35 = value;
	}

	inline static int32_t get_offset_of__nojoukawReti_36() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____nojoukawReti_36)); }
	inline uint32_t get__nojoukawReti_36() const { return ____nojoukawReti_36; }
	inline uint32_t* get_address_of__nojoukawReti_36() { return &____nojoukawReti_36; }
	inline void set__nojoukawReti_36(uint32_t value)
	{
		____nojoukawReti_36 = value;
	}

	inline static int32_t get_offset_of__tookelpiWheargir_37() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____tookelpiWheargir_37)); }
	inline uint32_t get__tookelpiWheargir_37() const { return ____tookelpiWheargir_37; }
	inline uint32_t* get_address_of__tookelpiWheargir_37() { return &____tookelpiWheargir_37; }
	inline void set__tookelpiWheargir_37(uint32_t value)
	{
		____tookelpiWheargir_37 = value;
	}

	inline static int32_t get_offset_of__staipoumo_38() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____staipoumo_38)); }
	inline int32_t get__staipoumo_38() const { return ____staipoumo_38; }
	inline int32_t* get_address_of__staipoumo_38() { return &____staipoumo_38; }
	inline void set__staipoumo_38(int32_t value)
	{
		____staipoumo_38 = value;
	}

	inline static int32_t get_offset_of__rearyelqi_39() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____rearyelqi_39)); }
	inline float get__rearyelqi_39() const { return ____rearyelqi_39; }
	inline float* get_address_of__rearyelqi_39() { return &____rearyelqi_39; }
	inline void set__rearyelqi_39(float value)
	{
		____rearyelqi_39 = value;
	}

	inline static int32_t get_offset_of__miburfis_40() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____miburfis_40)); }
	inline float get__miburfis_40() const { return ____miburfis_40; }
	inline float* get_address_of__miburfis_40() { return &____miburfis_40; }
	inline void set__miburfis_40(float value)
	{
		____miburfis_40 = value;
	}

	inline static int32_t get_offset_of__hoopa_41() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____hoopa_41)); }
	inline uint32_t get__hoopa_41() const { return ____hoopa_41; }
	inline uint32_t* get_address_of__hoopa_41() { return &____hoopa_41; }
	inline void set__hoopa_41(uint32_t value)
	{
		____hoopa_41 = value;
	}

	inline static int32_t get_offset_of__doriJorhefai_42() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____doriJorhefai_42)); }
	inline String_t* get__doriJorhefai_42() const { return ____doriJorhefai_42; }
	inline String_t** get_address_of__doriJorhefai_42() { return &____doriJorhefai_42; }
	inline void set__doriJorhefai_42(String_t* value)
	{
		____doriJorhefai_42 = value;
		Il2CppCodeGenWriteBarrier(&____doriJorhefai_42, value);
	}

	inline static int32_t get_offset_of__huce_43() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____huce_43)); }
	inline bool get__huce_43() const { return ____huce_43; }
	inline bool* get_address_of__huce_43() { return &____huce_43; }
	inline void set__huce_43(bool value)
	{
		____huce_43 = value;
	}

	inline static int32_t get_offset_of__feawersi_44() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____feawersi_44)); }
	inline int32_t get__feawersi_44() const { return ____feawersi_44; }
	inline int32_t* get_address_of__feawersi_44() { return &____feawersi_44; }
	inline void set__feawersi_44(int32_t value)
	{
		____feawersi_44 = value;
	}

	inline static int32_t get_offset_of__trayrea_45() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____trayrea_45)); }
	inline bool get__trayrea_45() const { return ____trayrea_45; }
	inline bool* get_address_of__trayrea_45() { return &____trayrea_45; }
	inline void set__trayrea_45(bool value)
	{
		____trayrea_45 = value;
	}

	inline static int32_t get_offset_of__sowdisdrisTroute_46() { return static_cast<int32_t>(offsetof(M_jaitisvalJarvo100_t2108255250, ____sowdisdrisTroute_46)); }
	inline String_t* get__sowdisdrisTroute_46() const { return ____sowdisdrisTroute_46; }
	inline String_t** get_address_of__sowdisdrisTroute_46() { return &____sowdisdrisTroute_46; }
	inline void set__sowdisdrisTroute_46(String_t* value)
	{
		____sowdisdrisTroute_46 = value;
		Il2CppCodeGenWriteBarrier(&____sowdisdrisTroute_46, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
