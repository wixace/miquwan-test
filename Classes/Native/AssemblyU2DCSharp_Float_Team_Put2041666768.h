﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UITexture
struct UITexture_t3903132647;
// UISprite
struct UISprite_t661437049;
// UILabel
struct UILabel_t291504320;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Float_Team_Put
struct  Float_Team_Put_t2041666768  : public FloatTextUnit_t2362298029
{
public:
	// UITexture Float_Team_Put::BG_tex
	UITexture_t3903132647 * ___BG_tex_3;
	// UISprite Float_Team_Put::Label_FateTile_sp
	UISprite_t661437049 * ___Label_FateTile_sp_4;
	// UILabel Float_Team_Put::Label_FateName_txt
	UILabel_t291504320 * ___Label_FateName_txt_5;
	// UILabel Float_Team_Put::Label_FateInfo_txt
	UILabel_t291504320 * ___Label_FateInfo_txt_6;
	// UnityEngine.GameObject Float_Team_Put::Obj_Suit_go
	GameObject_t3674682005 * ___Obj_Suit_go_7;
	// UnityEngine.GameObject Float_Team_Put::Sprite_fengge02_go
	GameObject_t3674682005 * ___Sprite_fengge02_go_8;
	// UILabel Float_Team_Put::Label_SuitTile01_txt
	UILabel_t291504320 * ___Label_SuitTile01_txt_9;
	// UILabel Float_Team_Put::Label_Suit01A_txt
	UILabel_t291504320 * ___Label_Suit01A_txt_10;
	// UILabel Float_Team_Put::Label_Suit01B_txt
	UILabel_t291504320 * ___Label_Suit01B_txt_11;
	// UILabel Float_Team_Put::Label_SuitTile02_txt
	UILabel_t291504320 * ___Label_SuitTile02_txt_12;
	// UILabel Float_Team_Put::Label_Suit02A_txt
	UILabel_t291504320 * ___Label_Suit02A_txt_13;
	// UILabel Float_Team_Put::Label_Suit02B_txt
	UILabel_t291504320 * ___Label_Suit02B_txt_14;
	// UnityEngine.GameObject Float_Team_Put::Obj_master_go
	GameObject_t3674682005 * ___Obj_master_go_15;
	// UILabel Float_Team_Put::Label_Strengtile_txt
	UILabel_t291504320 * ___Label_Strengtile_txt_16;
	// UILabel Float_Team_Put::Label_Refinetile_txt
	UILabel_t291504320 * ___Label_Refinetile_txt_17;
	// UnityEngine.GameObject Float_Team_Put::Sprite_fengge03_go
	GameObject_t3674682005 * ___Sprite_fengge03_go_18;
	// UILabel Float_Team_Put::Label_Att_txt
	UILabel_t291504320 * ___Label_Att_txt_19;
	// System.Int32 Float_Team_Put::YPos
	int32_t ___YPos_20;

public:
	inline static int32_t get_offset_of_BG_tex_3() { return static_cast<int32_t>(offsetof(Float_Team_Put_t2041666768, ___BG_tex_3)); }
	inline UITexture_t3903132647 * get_BG_tex_3() const { return ___BG_tex_3; }
	inline UITexture_t3903132647 ** get_address_of_BG_tex_3() { return &___BG_tex_3; }
	inline void set_BG_tex_3(UITexture_t3903132647 * value)
	{
		___BG_tex_3 = value;
		Il2CppCodeGenWriteBarrier(&___BG_tex_3, value);
	}

	inline static int32_t get_offset_of_Label_FateTile_sp_4() { return static_cast<int32_t>(offsetof(Float_Team_Put_t2041666768, ___Label_FateTile_sp_4)); }
	inline UISprite_t661437049 * get_Label_FateTile_sp_4() const { return ___Label_FateTile_sp_4; }
	inline UISprite_t661437049 ** get_address_of_Label_FateTile_sp_4() { return &___Label_FateTile_sp_4; }
	inline void set_Label_FateTile_sp_4(UISprite_t661437049 * value)
	{
		___Label_FateTile_sp_4 = value;
		Il2CppCodeGenWriteBarrier(&___Label_FateTile_sp_4, value);
	}

	inline static int32_t get_offset_of_Label_FateName_txt_5() { return static_cast<int32_t>(offsetof(Float_Team_Put_t2041666768, ___Label_FateName_txt_5)); }
	inline UILabel_t291504320 * get_Label_FateName_txt_5() const { return ___Label_FateName_txt_5; }
	inline UILabel_t291504320 ** get_address_of_Label_FateName_txt_5() { return &___Label_FateName_txt_5; }
	inline void set_Label_FateName_txt_5(UILabel_t291504320 * value)
	{
		___Label_FateName_txt_5 = value;
		Il2CppCodeGenWriteBarrier(&___Label_FateName_txt_5, value);
	}

	inline static int32_t get_offset_of_Label_FateInfo_txt_6() { return static_cast<int32_t>(offsetof(Float_Team_Put_t2041666768, ___Label_FateInfo_txt_6)); }
	inline UILabel_t291504320 * get_Label_FateInfo_txt_6() const { return ___Label_FateInfo_txt_6; }
	inline UILabel_t291504320 ** get_address_of_Label_FateInfo_txt_6() { return &___Label_FateInfo_txt_6; }
	inline void set_Label_FateInfo_txt_6(UILabel_t291504320 * value)
	{
		___Label_FateInfo_txt_6 = value;
		Il2CppCodeGenWriteBarrier(&___Label_FateInfo_txt_6, value);
	}

	inline static int32_t get_offset_of_Obj_Suit_go_7() { return static_cast<int32_t>(offsetof(Float_Team_Put_t2041666768, ___Obj_Suit_go_7)); }
	inline GameObject_t3674682005 * get_Obj_Suit_go_7() const { return ___Obj_Suit_go_7; }
	inline GameObject_t3674682005 ** get_address_of_Obj_Suit_go_7() { return &___Obj_Suit_go_7; }
	inline void set_Obj_Suit_go_7(GameObject_t3674682005 * value)
	{
		___Obj_Suit_go_7 = value;
		Il2CppCodeGenWriteBarrier(&___Obj_Suit_go_7, value);
	}

	inline static int32_t get_offset_of_Sprite_fengge02_go_8() { return static_cast<int32_t>(offsetof(Float_Team_Put_t2041666768, ___Sprite_fengge02_go_8)); }
	inline GameObject_t3674682005 * get_Sprite_fengge02_go_8() const { return ___Sprite_fengge02_go_8; }
	inline GameObject_t3674682005 ** get_address_of_Sprite_fengge02_go_8() { return &___Sprite_fengge02_go_8; }
	inline void set_Sprite_fengge02_go_8(GameObject_t3674682005 * value)
	{
		___Sprite_fengge02_go_8 = value;
		Il2CppCodeGenWriteBarrier(&___Sprite_fengge02_go_8, value);
	}

	inline static int32_t get_offset_of_Label_SuitTile01_txt_9() { return static_cast<int32_t>(offsetof(Float_Team_Put_t2041666768, ___Label_SuitTile01_txt_9)); }
	inline UILabel_t291504320 * get_Label_SuitTile01_txt_9() const { return ___Label_SuitTile01_txt_9; }
	inline UILabel_t291504320 ** get_address_of_Label_SuitTile01_txt_9() { return &___Label_SuitTile01_txt_9; }
	inline void set_Label_SuitTile01_txt_9(UILabel_t291504320 * value)
	{
		___Label_SuitTile01_txt_9 = value;
		Il2CppCodeGenWriteBarrier(&___Label_SuitTile01_txt_9, value);
	}

	inline static int32_t get_offset_of_Label_Suit01A_txt_10() { return static_cast<int32_t>(offsetof(Float_Team_Put_t2041666768, ___Label_Suit01A_txt_10)); }
	inline UILabel_t291504320 * get_Label_Suit01A_txt_10() const { return ___Label_Suit01A_txt_10; }
	inline UILabel_t291504320 ** get_address_of_Label_Suit01A_txt_10() { return &___Label_Suit01A_txt_10; }
	inline void set_Label_Suit01A_txt_10(UILabel_t291504320 * value)
	{
		___Label_Suit01A_txt_10 = value;
		Il2CppCodeGenWriteBarrier(&___Label_Suit01A_txt_10, value);
	}

	inline static int32_t get_offset_of_Label_Suit01B_txt_11() { return static_cast<int32_t>(offsetof(Float_Team_Put_t2041666768, ___Label_Suit01B_txt_11)); }
	inline UILabel_t291504320 * get_Label_Suit01B_txt_11() const { return ___Label_Suit01B_txt_11; }
	inline UILabel_t291504320 ** get_address_of_Label_Suit01B_txt_11() { return &___Label_Suit01B_txt_11; }
	inline void set_Label_Suit01B_txt_11(UILabel_t291504320 * value)
	{
		___Label_Suit01B_txt_11 = value;
		Il2CppCodeGenWriteBarrier(&___Label_Suit01B_txt_11, value);
	}

	inline static int32_t get_offset_of_Label_SuitTile02_txt_12() { return static_cast<int32_t>(offsetof(Float_Team_Put_t2041666768, ___Label_SuitTile02_txt_12)); }
	inline UILabel_t291504320 * get_Label_SuitTile02_txt_12() const { return ___Label_SuitTile02_txt_12; }
	inline UILabel_t291504320 ** get_address_of_Label_SuitTile02_txt_12() { return &___Label_SuitTile02_txt_12; }
	inline void set_Label_SuitTile02_txt_12(UILabel_t291504320 * value)
	{
		___Label_SuitTile02_txt_12 = value;
		Il2CppCodeGenWriteBarrier(&___Label_SuitTile02_txt_12, value);
	}

	inline static int32_t get_offset_of_Label_Suit02A_txt_13() { return static_cast<int32_t>(offsetof(Float_Team_Put_t2041666768, ___Label_Suit02A_txt_13)); }
	inline UILabel_t291504320 * get_Label_Suit02A_txt_13() const { return ___Label_Suit02A_txt_13; }
	inline UILabel_t291504320 ** get_address_of_Label_Suit02A_txt_13() { return &___Label_Suit02A_txt_13; }
	inline void set_Label_Suit02A_txt_13(UILabel_t291504320 * value)
	{
		___Label_Suit02A_txt_13 = value;
		Il2CppCodeGenWriteBarrier(&___Label_Suit02A_txt_13, value);
	}

	inline static int32_t get_offset_of_Label_Suit02B_txt_14() { return static_cast<int32_t>(offsetof(Float_Team_Put_t2041666768, ___Label_Suit02B_txt_14)); }
	inline UILabel_t291504320 * get_Label_Suit02B_txt_14() const { return ___Label_Suit02B_txt_14; }
	inline UILabel_t291504320 ** get_address_of_Label_Suit02B_txt_14() { return &___Label_Suit02B_txt_14; }
	inline void set_Label_Suit02B_txt_14(UILabel_t291504320 * value)
	{
		___Label_Suit02B_txt_14 = value;
		Il2CppCodeGenWriteBarrier(&___Label_Suit02B_txt_14, value);
	}

	inline static int32_t get_offset_of_Obj_master_go_15() { return static_cast<int32_t>(offsetof(Float_Team_Put_t2041666768, ___Obj_master_go_15)); }
	inline GameObject_t3674682005 * get_Obj_master_go_15() const { return ___Obj_master_go_15; }
	inline GameObject_t3674682005 ** get_address_of_Obj_master_go_15() { return &___Obj_master_go_15; }
	inline void set_Obj_master_go_15(GameObject_t3674682005 * value)
	{
		___Obj_master_go_15 = value;
		Il2CppCodeGenWriteBarrier(&___Obj_master_go_15, value);
	}

	inline static int32_t get_offset_of_Label_Strengtile_txt_16() { return static_cast<int32_t>(offsetof(Float_Team_Put_t2041666768, ___Label_Strengtile_txt_16)); }
	inline UILabel_t291504320 * get_Label_Strengtile_txt_16() const { return ___Label_Strengtile_txt_16; }
	inline UILabel_t291504320 ** get_address_of_Label_Strengtile_txt_16() { return &___Label_Strengtile_txt_16; }
	inline void set_Label_Strengtile_txt_16(UILabel_t291504320 * value)
	{
		___Label_Strengtile_txt_16 = value;
		Il2CppCodeGenWriteBarrier(&___Label_Strengtile_txt_16, value);
	}

	inline static int32_t get_offset_of_Label_Refinetile_txt_17() { return static_cast<int32_t>(offsetof(Float_Team_Put_t2041666768, ___Label_Refinetile_txt_17)); }
	inline UILabel_t291504320 * get_Label_Refinetile_txt_17() const { return ___Label_Refinetile_txt_17; }
	inline UILabel_t291504320 ** get_address_of_Label_Refinetile_txt_17() { return &___Label_Refinetile_txt_17; }
	inline void set_Label_Refinetile_txt_17(UILabel_t291504320 * value)
	{
		___Label_Refinetile_txt_17 = value;
		Il2CppCodeGenWriteBarrier(&___Label_Refinetile_txt_17, value);
	}

	inline static int32_t get_offset_of_Sprite_fengge03_go_18() { return static_cast<int32_t>(offsetof(Float_Team_Put_t2041666768, ___Sprite_fengge03_go_18)); }
	inline GameObject_t3674682005 * get_Sprite_fengge03_go_18() const { return ___Sprite_fengge03_go_18; }
	inline GameObject_t3674682005 ** get_address_of_Sprite_fengge03_go_18() { return &___Sprite_fengge03_go_18; }
	inline void set_Sprite_fengge03_go_18(GameObject_t3674682005 * value)
	{
		___Sprite_fengge03_go_18 = value;
		Il2CppCodeGenWriteBarrier(&___Sprite_fengge03_go_18, value);
	}

	inline static int32_t get_offset_of_Label_Att_txt_19() { return static_cast<int32_t>(offsetof(Float_Team_Put_t2041666768, ___Label_Att_txt_19)); }
	inline UILabel_t291504320 * get_Label_Att_txt_19() const { return ___Label_Att_txt_19; }
	inline UILabel_t291504320 ** get_address_of_Label_Att_txt_19() { return &___Label_Att_txt_19; }
	inline void set_Label_Att_txt_19(UILabel_t291504320 * value)
	{
		___Label_Att_txt_19 = value;
		Il2CppCodeGenWriteBarrier(&___Label_Att_txt_19, value);
	}

	inline static int32_t get_offset_of_YPos_20() { return static_cast<int32_t>(offsetof(Float_Team_Put_t2041666768, ___YPos_20)); }
	inline int32_t get_YPos_20() const { return ___YPos_20; }
	inline int32_t* get_address_of_YPos_20() { return &___YPos_20; }
	inline void set_YPos_20(int32_t value)
	{
		___YPos_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
