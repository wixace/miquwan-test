﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_jutrortee210
struct M_jutrortee210_t579802681;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_jutrortee210::.ctor()
extern "C"  void M_jutrortee210__ctor_m3026258970 (M_jutrortee210_t579802681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jutrortee210::M_canocou0(System.String[],System.Int32)
extern "C"  void M_jutrortee210_M_canocou0_m1421783001 (M_jutrortee210_t579802681 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
