﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlowControl.FlowAssetMgrInit
struct FlowAssetMgrInit_t2352630970;
// LoadingBoardMgr
struct LoadingBoardMgr_t303632014;
// FlowControl.FlowBase
struct FlowBase_t3680091731;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FlowControl_FlowBase3680091731.h"

// System.Void FlowControl.FlowAssetMgrInit::.ctor()
extern "C"  void FlowAssetMgrInit__ctor_m2883583414 (FlowAssetMgrInit_t2352630970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowAssetMgrInit::Activate()
extern "C"  void FlowAssetMgrInit_Activate_m893245057 (FlowAssetMgrInit_t2352630970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowAssetMgrInit::OnComplete()
extern "C"  void FlowAssetMgrInit_OnComplete_m3410524486 (FlowAssetMgrInit_t2352630970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LoadingBoardMgr FlowControl.FlowAssetMgrInit::ilo_get_Instance1()
extern "C"  LoadingBoardMgr_t303632014 * FlowAssetMgrInit_ilo_get_Instance1_m590478575 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowAssetMgrInit::ilo_set_isComplete2(FlowControl.FlowBase,System.Boolean)
extern "C"  void FlowAssetMgrInit_ilo_set_isComplete2_m3632325928 (Il2CppObject * __this /* static, unused */, FlowBase_t3680091731 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
