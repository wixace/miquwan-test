﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._030a4b71f6ffd93d0726ec82a198d624
struct _030a4b71f6ffd93d0726ec82a198d624_t2271997329;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._030a4b71f6ffd93d0726ec82a198d624::.ctor()
extern "C"  void _030a4b71f6ffd93d0726ec82a198d624__ctor_m3087723452 (_030a4b71f6ffd93d0726ec82a198d624_t2271997329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._030a4b71f6ffd93d0726ec82a198d624::_030a4b71f6ffd93d0726ec82a198d624m2(System.Int32)
extern "C"  int32_t _030a4b71f6ffd93d0726ec82a198d624__030a4b71f6ffd93d0726ec82a198d624m2_m167124985 (_030a4b71f6ffd93d0726ec82a198d624_t2271997329 * __this, int32_t ____030a4b71f6ffd93d0726ec82a198d624a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._030a4b71f6ffd93d0726ec82a198d624::_030a4b71f6ffd93d0726ec82a198d624m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _030a4b71f6ffd93d0726ec82a198d624__030a4b71f6ffd93d0726ec82a198d624m_m1480159965 (_030a4b71f6ffd93d0726ec82a198d624_t2271997329 * __this, int32_t ____030a4b71f6ffd93d0726ec82a198d624a0, int32_t ____030a4b71f6ffd93d0726ec82a198d624311, int32_t ____030a4b71f6ffd93d0726ec82a198d624c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
