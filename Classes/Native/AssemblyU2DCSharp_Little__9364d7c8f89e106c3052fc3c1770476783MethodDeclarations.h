﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._9364d7c8f89e106c3052fc3c7e0970d9
struct _9364d7c8f89e106c3052fc3c7e0970d9_t1770476783;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__9364d7c8f89e106c3052fc3c1770476783.h"

// System.Void Little._9364d7c8f89e106c3052fc3c7e0970d9::.ctor()
extern "C"  void _9364d7c8f89e106c3052fc3c7e0970d9__ctor_m1137372702 (_9364d7c8f89e106c3052fc3c7e0970d9_t1770476783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._9364d7c8f89e106c3052fc3c7e0970d9::_9364d7c8f89e106c3052fc3c7e0970d9m2(System.Int32)
extern "C"  int32_t _9364d7c8f89e106c3052fc3c7e0970d9__9364d7c8f89e106c3052fc3c7e0970d9m2_m1852633913 (_9364d7c8f89e106c3052fc3c7e0970d9_t1770476783 * __this, int32_t ____9364d7c8f89e106c3052fc3c7e0970d9a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._9364d7c8f89e106c3052fc3c7e0970d9::_9364d7c8f89e106c3052fc3c7e0970d9m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _9364d7c8f89e106c3052fc3c7e0970d9__9364d7c8f89e106c3052fc3c7e0970d9m_m1515898781 (_9364d7c8f89e106c3052fc3c7e0970d9_t1770476783 * __this, int32_t ____9364d7c8f89e106c3052fc3c7e0970d9a0, int32_t ____9364d7c8f89e106c3052fc3c7e0970d9161, int32_t ____9364d7c8f89e106c3052fc3c7e0970d9c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._9364d7c8f89e106c3052fc3c7e0970d9::ilo__9364d7c8f89e106c3052fc3c7e0970d9m21(Little._9364d7c8f89e106c3052fc3c7e0970d9,System.Int32)
extern "C"  int32_t _9364d7c8f89e106c3052fc3c7e0970d9_ilo__9364d7c8f89e106c3052fc3c7e0970d9m21_m3750815126 (Il2CppObject * __this /* static, unused */, _9364d7c8f89e106c3052fc3c7e0970d9_t1770476783 * ____this0, int32_t ____9364d7c8f89e106c3052fc3c7e0970d9a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
