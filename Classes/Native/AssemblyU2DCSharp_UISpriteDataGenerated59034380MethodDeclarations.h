﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UISpriteDataGenerated
struct UISpriteDataGenerated_t59034380;
// JSVCall
struct JSVCall_t3708497963;
// UISpriteData
struct UISpriteData_t3578345923;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_UISpriteData3578345923.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UISpriteDataGenerated::.ctor()
extern "C"  void UISpriteDataGenerated__ctor_m2760367071 (UISpriteDataGenerated_t59034380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteDataGenerated::UISpriteData_UISpriteData1(JSVCall,System.Int32)
extern "C"  bool UISpriteDataGenerated_UISpriteData_UISpriteData1_m1458983699 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteDataGenerated::UISpriteData_name(JSVCall)
extern "C"  void UISpriteDataGenerated_UISpriteData_name_m4090095683 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteDataGenerated::UISpriteData_x(JSVCall)
extern "C"  void UISpriteDataGenerated_UISpriteData_x_m3825243174 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteDataGenerated::UISpriteData_y(JSVCall)
extern "C"  void UISpriteDataGenerated_UISpriteData_y_m3628729669 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteDataGenerated::UISpriteData_width(JSVCall)
extern "C"  void UISpriteDataGenerated_UISpriteData_width_m2565321368 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteDataGenerated::UISpriteData_height(JSVCall)
extern "C"  void UISpriteDataGenerated_UISpriteData_height_m2870994727 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteDataGenerated::UISpriteData_borderLeft(JSVCall)
extern "C"  void UISpriteDataGenerated_UISpriteData_borderLeft_m3112485755 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteDataGenerated::UISpriteData_borderRight(JSVCall)
extern "C"  void UISpriteDataGenerated_UISpriteData_borderRight_m925216846 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteDataGenerated::UISpriteData_borderTop(JSVCall)
extern "C"  void UISpriteDataGenerated_UISpriteData_borderTop_m2302069301 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteDataGenerated::UISpriteData_borderBottom(JSVCall)
extern "C"  void UISpriteDataGenerated_UISpriteData_borderBottom_m3949451991 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteDataGenerated::UISpriteData_paddingLeft(JSVCall)
extern "C"  void UISpriteDataGenerated_UISpriteData_paddingLeft_m1253744358 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteDataGenerated::UISpriteData_paddingRight(JSVCall)
extern "C"  void UISpriteDataGenerated_UISpriteData_paddingRight_m3433775683 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteDataGenerated::UISpriteData_paddingTop(JSVCall)
extern "C"  void UISpriteDataGenerated_UISpriteData_paddingTop_m3211941226 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteDataGenerated::UISpriteData_paddingBottom(JSVCall)
extern "C"  void UISpriteDataGenerated_UISpriteData_paddingBottom_m110397314 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteDataGenerated::UISpriteData_hasBorder(JSVCall)
extern "C"  void UISpriteDataGenerated_UISpriteData_hasBorder_m321769720 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteDataGenerated::UISpriteData_hasPadding(JSVCall)
extern "C"  void UISpriteDataGenerated_UISpriteData_hasPadding_m1975030327 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteDataGenerated::UISpriteData_CopyBorderFrom__UISpriteData(JSVCall,System.Int32)
extern "C"  bool UISpriteDataGenerated_UISpriteData_CopyBorderFrom__UISpriteData_m319427627 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteDataGenerated::UISpriteData_CopyFrom__UISpriteData(JSVCall,System.Int32)
extern "C"  bool UISpriteDataGenerated_UISpriteData_CopyFrom__UISpriteData_m1451998367 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteDataGenerated::UISpriteData_SetBorder__Int32__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UISpriteDataGenerated_UISpriteData_SetBorder__Int32__Int32__Int32__Int32_m126806739 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteDataGenerated::UISpriteData_SetPadding__Int32__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UISpriteDataGenerated_UISpriteData_SetPadding__Int32__Int32__Int32__Int32_m2481521164 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteDataGenerated::UISpriteData_SetRect__Int32__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UISpriteDataGenerated_UISpriteData_SetRect__Int32__Int32__Int32__Int32_m4241555467 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteDataGenerated::__Register()
extern "C"  void UISpriteDataGenerated___Register_m2941465672 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UISpriteDataGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UISpriteDataGenerated_ilo_getObject1_m2137000503 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteDataGenerated::ilo_setInt322(System.Int32,System.Int32)
extern "C"  void UISpriteDataGenerated_ilo_setInt322_m3763074870 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UISpriteDataGenerated::ilo_getInt323(System.Int32)
extern "C"  int32_t UISpriteDataGenerated_ilo_getInt323_m1822635268 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteDataGenerated::ilo_get_hasBorder4(UISpriteData)
extern "C"  bool UISpriteDataGenerated_ilo_get_hasBorder4_m18203480 (Il2CppObject * __this /* static, unused */, UISpriteData_t3578345923 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteDataGenerated::ilo_CopyBorderFrom5(UISpriteData,UISpriteData)
extern "C"  void UISpriteDataGenerated_ilo_CopyBorderFrom5_m2236721970 (Il2CppObject * __this /* static, unused */, UISpriteData_t3578345923 * ____this0, UISpriteData_t3578345923 * ___sd1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UISpriteDataGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UISpriteDataGenerated_ilo_getObject6_m2920524011 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteDataGenerated::ilo_CopyFrom7(UISpriteData,UISpriteData)
extern "C"  void UISpriteDataGenerated_ilo_CopyFrom7_m3381263012 (Il2CppObject * __this /* static, unused */, UISpriteData_t3578345923 * ____this0, UISpriteData_t3578345923 * ___sd1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteDataGenerated::ilo_SetRect8(UISpriteData,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void UISpriteDataGenerated_ilo_SetRect8_m1487706471 (Il2CppObject * __this /* static, unused */, UISpriteData_t3578345923 * ____this0, int32_t ___x1, int32_t ___y2, int32_t ___width3, int32_t ___height4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
