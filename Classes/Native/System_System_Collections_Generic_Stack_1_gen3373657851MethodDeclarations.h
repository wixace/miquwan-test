﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen2974409999MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<Pathfinding.ConstantPath>::.ctor()
#define Stack_1__ctor_m2162671747(__this, method) ((  void (*) (Stack_1_t3373657851 *, const MethodInfo*))Stack_1__ctor_m2725689112_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Pathfinding.ConstantPath>::.ctor(System.Int32)
#define Stack_1__ctor_m3336685908(__this, ___count0, method) ((  void (*) (Stack_1_t3373657851 *, int32_t, const MethodInfo*))Stack_1__ctor_m3186788457_gshared)(__this, ___count0, method)
// System.Boolean System.Collections.Generic.Stack`1<Pathfinding.ConstantPath>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m2000315409(__this, method) ((  bool (*) (Stack_1_t3373657851 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m1582336274_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<Pathfinding.ConstantPath>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m3794610897(__this, method) ((  Il2CppObject * (*) (Stack_1_t3373657851 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m2938343088_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Pathfinding.ConstantPath>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m384840225(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t3373657851 *, Il2CppArray *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m3277353260_gshared)(__this, ___dest0, ___idx1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<Pathfinding.ConstantPath>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1734833429(__this, method) ((  Il2CppObject* (*) (Stack_1_t3373657851 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m625377314_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<Pathfinding.ConstantPath>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m812585968(__this, method) ((  Il2CppObject * (*) (Stack_1_t3373657851 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m4095051687_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Pathfinding.ConstantPath>::Clear()
#define Stack_1_Clear_m3863772334(__this, method) ((  void (*) (Stack_1_t3373657851 *, const MethodInfo*))Stack_1_Clear_m131822403_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<Pathfinding.ConstantPath>::Contains(T)
#define Stack_1_Contains_m539189960(__this, ___t0, method) ((  bool (*) (Stack_1_t3373657851 *, ConstantPath_t275096927 *, const MethodInfo*))Stack_1_Contains_m328948937_gshared)(__this, ___t0, method)
// T System.Collections.Generic.Stack`1<Pathfinding.ConstantPath>::Peek()
#define Stack_1_Peek_m2352844219(__this, method) ((  ConstantPath_t275096927 * (*) (Stack_1_t3373657851 *, const MethodInfo*))Stack_1_Peek_m3418768488_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<Pathfinding.ConstantPath>::Pop()
#define Stack_1_Pop_m2985698579(__this, method) ((  ConstantPath_t275096927 * (*) (Stack_1_t3373657851 *, const MethodInfo*))Stack_1_Pop_m4267009222_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Pathfinding.ConstantPath>::Push(T)
#define Stack_1_Push_m2787148739(__this, ___t0, method) ((  void (*) (Stack_1_t3373657851 *, ConstantPath_t275096927 *, const MethodInfo*))Stack_1_Push_m3350166104_gshared)(__this, ___t0, method)
// System.Int32 System.Collections.Generic.Stack`1<Pathfinding.ConstantPath>::get_Count()
#define Stack_1_get_Count_m1185869015(__this, method) ((  int32_t (*) (Stack_1_t3373657851 *, const MethodInfo*))Stack_1_get_Count_m3631765324_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<Pathfinding.ConstantPath>::GetEnumerator()
#define Stack_1_GetEnumerator_m896983673(__this, method) ((  Enumerator_t2931443877  (*) (Stack_1_t3373657851 *, const MethodInfo*))Stack_1_GetEnumerator_m202302354_gshared)(__this, method)
