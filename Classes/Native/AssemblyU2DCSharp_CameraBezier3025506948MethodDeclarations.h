﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraBezier
struct CameraBezier_t3025506948;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_SetType3649656476.h"
#include "AssemblyU2DCSharp_CameraBezier3025506948.h"

// System.Void CameraBezier::.ctor()
extern "C"  void CameraBezier__ctor_m3145004183 (CameraBezier_t3025506948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraBezier::Start()
extern "C"  void CameraBezier_Start_m2092141975 (CameraBezier_t3025506948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraBezier::FixedUpdate()
extern "C"  void CameraBezier_FixedUpdate_m3469001938 (CameraBezier_t3025506948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraBezier::Move()
extern "C"  void CameraBezier_Move_m445896030 (CameraBezier_t3025506948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraBezier::CheckProcess()
extern "C"  void CameraBezier_CheckProcess_m2595184724 (CameraBezier_t3025506948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CameraBezier::Plot(System.Single)
extern "C"  Vector3_t4282066566  CameraBezier_Plot_m1971446723 (CameraBezier_t3025506948 * __this, float ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CameraBezier::CheckDefault(System.Object,SetType)
extern "C"  Il2CppObject * CameraBezier_CheckDefault_m3017820073 (CameraBezier_t3025506948 * __this, Il2CppObject * ___obj0, int32_t ___str1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CameraBezier::ilo_CheckDefault1(CameraBezier,System.Object,SetType)
extern "C"  Il2CppObject * CameraBezier_ilo_CheckDefault1_m742901855 (Il2CppObject * __this /* static, unused */, CameraBezier_t3025506948 * ____this0, Il2CppObject * ___obj1, int32_t ___str2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
