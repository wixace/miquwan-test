﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameObjectVisitorGenerated/<GameObjectVisitor_VisitChildren_GetDelegate_member6_arg1>c__AnonStorey65
struct U3CGameObjectVisitor_VisitChildren_GetDelegate_member6_arg1U3Ec__AnonStorey65_t1478053984;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void GameObjectVisitorGenerated/<GameObjectVisitor_VisitChildren_GetDelegate_member6_arg1>c__AnonStorey65::.ctor()
extern "C"  void U3CGameObjectVisitor_VisitChildren_GetDelegate_member6_arg1U3Ec__AnonStorey65__ctor_m1811908155 (U3CGameObjectVisitor_VisitChildren_GetDelegate_member6_arg1U3Ec__AnonStorey65_t1478053984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameObjectVisitorGenerated/<GameObjectVisitor_VisitChildren_GetDelegate_member6_arg1>c__AnonStorey65::<>m__5C(UnityEngine.GameObject)
extern "C"  void U3CGameObjectVisitor_VisitChildren_GetDelegate_member6_arg1U3Ec__AnonStorey65_U3CU3Em__5C_m3893940074 (U3CGameObjectVisitor_VisitChildren_GetDelegate_member6_arg1U3Ec__AnonStorey65_t1478053984 * __this, GameObject_t3674682005 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
