﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayGameSpeed
struct ReplayGameSpeed_t283045678;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;
// ReplayBase
struct ReplayBase_t779703160;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ReplayBase779703160.h"

// System.Void ReplayGameSpeed::.ctor()
extern "C"  void ReplayGameSpeed__ctor_m1356336125 (ReplayGameSpeed_t283045678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayGameSpeed::.ctor(System.Object[])
extern "C"  void ReplayGameSpeed__ctor_m2362330645 (ReplayGameSpeed_t283045678 * __this, ObjectU5BU5D_t1108656482* ___datas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] ReplayGameSpeed::ParserJsonStr(System.String)
extern "C"  StringU5BU5D_t4054002952* ReplayGameSpeed_ParserJsonStr_m2135303388 (ReplayGameSpeed_t283045678 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayGameSpeed::GetJsonStr()
extern "C"  String_t* ReplayGameSpeed_GetJsonStr_m3158929687 (ReplayGameSpeed_t283045678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayGameSpeed::Execute()
extern "C"  void ReplayGameSpeed_Execute_m620347600 (ReplayGameSpeed_t283045678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayGameSpeed::ilo_Execute1(ReplayBase)
extern "C"  void ReplayGameSpeed_ilo_Execute1_m2177245630 (Il2CppObject * __this /* static, unused */, ReplayBase_t779703160 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
