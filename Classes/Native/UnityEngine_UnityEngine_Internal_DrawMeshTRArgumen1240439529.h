﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Internal_DrawMeshTRArguments
struct  Internal_DrawMeshTRArguments_t1240439529 
{
public:
	// System.Int32 UnityEngine.Internal_DrawMeshTRArguments::layer
	int32_t ___layer_0;
	// System.Int32 UnityEngine.Internal_DrawMeshTRArguments::submeshIndex
	int32_t ___submeshIndex_1;
	// UnityEngine.Quaternion UnityEngine.Internal_DrawMeshTRArguments::rotation
	Quaternion_t1553702882  ___rotation_2;
	// UnityEngine.Vector3 UnityEngine.Internal_DrawMeshTRArguments::position
	Vector3_t4282066566  ___position_3;
	// System.Int32 UnityEngine.Internal_DrawMeshTRArguments::castShadows
	int32_t ___castShadows_4;
	// System.Int32 UnityEngine.Internal_DrawMeshTRArguments::receiveShadows
	int32_t ___receiveShadows_5;
	// System.Int32 UnityEngine.Internal_DrawMeshTRArguments::reflectionProbeAnchorInstanceID
	int32_t ___reflectionProbeAnchorInstanceID_6;

public:
	inline static int32_t get_offset_of_layer_0() { return static_cast<int32_t>(offsetof(Internal_DrawMeshTRArguments_t1240439529, ___layer_0)); }
	inline int32_t get_layer_0() const { return ___layer_0; }
	inline int32_t* get_address_of_layer_0() { return &___layer_0; }
	inline void set_layer_0(int32_t value)
	{
		___layer_0 = value;
	}

	inline static int32_t get_offset_of_submeshIndex_1() { return static_cast<int32_t>(offsetof(Internal_DrawMeshTRArguments_t1240439529, ___submeshIndex_1)); }
	inline int32_t get_submeshIndex_1() const { return ___submeshIndex_1; }
	inline int32_t* get_address_of_submeshIndex_1() { return &___submeshIndex_1; }
	inline void set_submeshIndex_1(int32_t value)
	{
		___submeshIndex_1 = value;
	}

	inline static int32_t get_offset_of_rotation_2() { return static_cast<int32_t>(offsetof(Internal_DrawMeshTRArguments_t1240439529, ___rotation_2)); }
	inline Quaternion_t1553702882  get_rotation_2() const { return ___rotation_2; }
	inline Quaternion_t1553702882 * get_address_of_rotation_2() { return &___rotation_2; }
	inline void set_rotation_2(Quaternion_t1553702882  value)
	{
		___rotation_2 = value;
	}

	inline static int32_t get_offset_of_position_3() { return static_cast<int32_t>(offsetof(Internal_DrawMeshTRArguments_t1240439529, ___position_3)); }
	inline Vector3_t4282066566  get_position_3() const { return ___position_3; }
	inline Vector3_t4282066566 * get_address_of_position_3() { return &___position_3; }
	inline void set_position_3(Vector3_t4282066566  value)
	{
		___position_3 = value;
	}

	inline static int32_t get_offset_of_castShadows_4() { return static_cast<int32_t>(offsetof(Internal_DrawMeshTRArguments_t1240439529, ___castShadows_4)); }
	inline int32_t get_castShadows_4() const { return ___castShadows_4; }
	inline int32_t* get_address_of_castShadows_4() { return &___castShadows_4; }
	inline void set_castShadows_4(int32_t value)
	{
		___castShadows_4 = value;
	}

	inline static int32_t get_offset_of_receiveShadows_5() { return static_cast<int32_t>(offsetof(Internal_DrawMeshTRArguments_t1240439529, ___receiveShadows_5)); }
	inline int32_t get_receiveShadows_5() const { return ___receiveShadows_5; }
	inline int32_t* get_address_of_receiveShadows_5() { return &___receiveShadows_5; }
	inline void set_receiveShadows_5(int32_t value)
	{
		___receiveShadows_5 = value;
	}

	inline static int32_t get_offset_of_reflectionProbeAnchorInstanceID_6() { return static_cast<int32_t>(offsetof(Internal_DrawMeshTRArguments_t1240439529, ___reflectionProbeAnchorInstanceID_6)); }
	inline int32_t get_reflectionProbeAnchorInstanceID_6() const { return ___reflectionProbeAnchorInstanceID_6; }
	inline int32_t* get_address_of_reflectionProbeAnchorInstanceID_6() { return &___reflectionProbeAnchorInstanceID_6; }
	inline void set_reflectionProbeAnchorInstanceID_6(int32_t value)
	{
		___reflectionProbeAnchorInstanceID_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: UnityEngine.Internal_DrawMeshTRArguments
struct Internal_DrawMeshTRArguments_t1240439529_marshaled_pinvoke
{
	int32_t ___layer_0;
	int32_t ___submeshIndex_1;
	Quaternion_t1553702882_marshaled_pinvoke ___rotation_2;
	Vector3_t4282066566_marshaled_pinvoke ___position_3;
	int32_t ___castShadows_4;
	int32_t ___receiveShadows_5;
	int32_t ___reflectionProbeAnchorInstanceID_6;
};
// Native definition for marshalling of: UnityEngine.Internal_DrawMeshTRArguments
struct Internal_DrawMeshTRArguments_t1240439529_marshaled_com
{
	int32_t ___layer_0;
	int32_t ___submeshIndex_1;
	Quaternion_t1553702882_marshaled_com ___rotation_2;
	Vector3_t4282066566_marshaled_com ___position_3;
	int32_t ___castShadows_4;
	int32_t ___receiveShadows_5;
	int32_t ___reflectionProbeAnchorInstanceID_6;
};
