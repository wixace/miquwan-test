﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>
struct Dictionary_2_t3476030434;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t666883479;
// System.Collections.Generic.IDictionary`2<System.Object,ProductsCfgMgr/ProductInfo>
struct IDictionary_2_t3053903779;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.Generic.ICollection`1<ProductsCfgMgr/ProductInfo>
struct ICollection_1_t2200581225;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>[]
struct KeyValuePair_2U5BU5D_t1556865901;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>>
struct IEnumerator_1_t991708893;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,ProductsCfgMgr/ProductInfo>
struct KeyCollection_t807822589;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ProductsCfgMgr/ProductInfo>
struct ValueCollection_t2176636147;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23374811140.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr_ProductInfo1305991238.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En498386530.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::.ctor()
extern "C"  void Dictionary_2__ctor_m1163564655_gshared (Dictionary_2_t3476030434 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1163564655(__this, method) ((  void (*) (Dictionary_2_t3476030434 *, const MethodInfo*))Dictionary_2__ctor_m1163564655_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2718016358_gshared (Dictionary_2_t3476030434 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m2718016358(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3476030434 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2718016358_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m2669854345_gshared (Dictionary_2_t3476030434 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m2669854345(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t3476030434 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2669854345_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m1434918208_gshared (Dictionary_2_t3476030434 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m1434918208(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3476030434 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1434918208_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3032139028_gshared (Dictionary_2_t3476030434 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m3032139028(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t3476030434 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3032139028_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m769815344_gshared (Dictionary_2_t3476030434 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m769815344(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3476030434 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m769815344_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m443499593_gshared (Dictionary_2_t3476030434 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m443499593(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3476030434 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m443499593_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3407032805_gshared (Dictionary_2_t3476030434 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3407032805(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3476030434 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3407032805_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m4073818605_gshared (Dictionary_2_t3476030434 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m4073818605(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3476030434 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m4073818605_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m297060251_gshared (Dictionary_2_t3476030434 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m297060251(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3476030434 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m297060251_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m610592526_gshared (Dictionary_2_t3476030434 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m610592526(__this, method) ((  bool (*) (Dictionary_2_t3476030434 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m610592526_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1166442027_gshared (Dictionary_2_t3476030434 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1166442027(__this, method) ((  bool (*) (Dictionary_2_t3476030434 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1166442027_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3605586031_gshared (Dictionary_2_t3476030434 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3605586031(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3476030434 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m3605586031_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m640641310_gshared (Dictionary_2_t3476030434 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m640641310(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3476030434 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m640641310_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m847583859_gshared (Dictionary_2_t3476030434 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m847583859(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3476030434 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m847583859_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m4267099231_gshared (Dictionary_2_t3476030434 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m4267099231(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3476030434 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m4267099231_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2892226396_gshared (Dictionary_2_t3476030434 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m2892226396(__this, ___key0, method) ((  void (*) (Dictionary_2_t3476030434 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2892226396_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m487415637_gshared (Dictionary_2_t3476030434 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m487415637(__this, method) ((  bool (*) (Dictionary_2_t3476030434 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m487415637_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3679690311_gshared (Dictionary_2_t3476030434 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3679690311(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3476030434 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3679690311_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m218731353_gshared (Dictionary_2_t3476030434 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m218731353(__this, method) ((  bool (*) (Dictionary_2_t3476030434 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m218731353_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3921882610_gshared (Dictionary_2_t3476030434 * __this, KeyValuePair_2_t3374811140  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3921882610(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3476030434 *, KeyValuePair_2_t3374811140 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3921882610_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m350821844_gshared (Dictionary_2_t3476030434 * __this, KeyValuePair_2_t3374811140  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m350821844(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3476030434 *, KeyValuePair_2_t3374811140 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m350821844_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1247757654_gshared (Dictionary_2_t3476030434 * __this, KeyValuePair_2U5BU5D_t1556865901* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1247757654(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3476030434 *, KeyValuePair_2U5BU5D_t1556865901*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1247757654_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2920065529_gshared (Dictionary_2_t3476030434 * __this, KeyValuePair_2_t3374811140  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2920065529(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3476030434 *, KeyValuePair_2_t3374811140 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2920065529_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m1301040565_gshared (Dictionary_2_t3476030434 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1301040565(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3476030434 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1301040565_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1931431812_gshared (Dictionary_2_t3476030434 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1931431812(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3476030434 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1931431812_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2500755835_gshared (Dictionary_2_t3476030434 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2500755835(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3476030434 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2500755835_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2782676424_gshared (Dictionary_2_t3476030434 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2782676424(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3476030434 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2782676424_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m780153359_gshared (Dictionary_2_t3476030434 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m780153359(__this, method) ((  int32_t (*) (Dictionary_2_t3476030434 *, const MethodInfo*))Dictionary_2_get_Count_m780153359_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::get_Item(TKey)
extern "C"  ProductInfo_t1305991238  Dictionary_2_get_Item_m2796012696_gshared (Dictionary_2_t3476030434 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m2796012696(__this, ___key0, method) ((  ProductInfo_t1305991238  (*) (Dictionary_2_t3476030434 *, Il2CppObject *, const MethodInfo*))Dictionary_2_get_Item_m2796012696_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1177771503_gshared (Dictionary_2_t3476030434 * __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1177771503(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3476030434 *, Il2CppObject *, ProductInfo_t1305991238 , const MethodInfo*))Dictionary_2_set_Item_m1177771503_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m3219420647_gshared (Dictionary_2_t3476030434 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m3219420647(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3476030434 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3219420647_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m371151024_gshared (Dictionary_2_t3476030434 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m371151024(__this, ___size0, method) ((  void (*) (Dictionary_2_t3476030434 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m371151024_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m2540648620_gshared (Dictionary_2_t3476030434 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m2540648620(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3476030434 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2540648620_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t3374811140  Dictionary_2_make_pair_m97395264_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m97395264(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3374811140  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, ProductInfo_t1305991238 , const MethodInfo*))Dictionary_2_make_pair_m97395264_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::pick_key(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_key_m3933797758_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m3933797758(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, ProductInfo_t1305991238 , const MethodInfo*))Dictionary_2_pick_key_m3933797758_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::pick_value(TKey,TValue)
extern "C"  ProductInfo_t1305991238  Dictionary_2_pick_value_m1128648410_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m1128648410(__this /* static, unused */, ___key0, ___value1, method) ((  ProductInfo_t1305991238  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, ProductInfo_t1305991238 , const MethodInfo*))Dictionary_2_pick_value_m1128648410_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m140079523_gshared (Dictionary_2_t3476030434 * __this, KeyValuePair_2U5BU5D_t1556865901* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m140079523(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3476030434 *, KeyValuePair_2U5BU5D_t1556865901*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m140079523_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::Resize()
extern "C"  void Dictionary_2_Resize_m1768562601_gshared (Dictionary_2_t3476030434 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1768562601(__this, method) ((  void (*) (Dictionary_2_t3476030434 *, const MethodInfo*))Dictionary_2_Resize_m1768562601_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m2148363046_gshared (Dictionary_2_t3476030434 * __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m2148363046(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3476030434 *, Il2CppObject *, ProductInfo_t1305991238 , const MethodInfo*))Dictionary_2_Add_m2148363046_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::Clear()
extern "C"  void Dictionary_2_Clear_m2864665242_gshared (Dictionary_2_t3476030434 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2864665242(__this, method) ((  void (*) (Dictionary_2_t3476030434 *, const MethodInfo*))Dictionary_2_Clear_m2864665242_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2562332228_gshared (Dictionary_2_t3476030434 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2562332228(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3476030434 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsKey_m2562332228_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m3654854724_gshared (Dictionary_2_t3476030434 * __this, ProductInfo_t1305991238  ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m3654854724(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3476030434 *, ProductInfo_t1305991238 , const MethodInfo*))Dictionary_2_ContainsValue_m3654854724_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m919964301_gshared (Dictionary_2_t3476030434 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m919964301(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3476030434 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m919964301_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m1406735863_gshared (Dictionary_2_t3476030434 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1406735863(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3476030434 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1406735863_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m3506063404_gshared (Dictionary_2_t3476030434 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m3506063404(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3476030434 *, Il2CppObject *, const MethodInfo*))Dictionary_2_Remove_m3506063404_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2449336989_gshared (Dictionary_2_t3476030434 * __this, Il2CppObject * ___key0, ProductInfo_t1305991238 * ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m2449336989(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3476030434 *, Il2CppObject *, ProductInfo_t1305991238 *, const MethodInfo*))Dictionary_2_TryGetValue_m2449336989_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::get_Keys()
extern "C"  KeyCollection_t807822589 * Dictionary_2_get_Keys_m3385795102_gshared (Dictionary_2_t3476030434 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m3385795102(__this, method) ((  KeyCollection_t807822589 * (*) (Dictionary_2_t3476030434 *, const MethodInfo*))Dictionary_2_get_Keys_m3385795102_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::get_Values()
extern "C"  ValueCollection_t2176636147 * Dictionary_2_get_Values_m3853824314_gshared (Dictionary_2_t3476030434 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3853824314(__this, method) ((  ValueCollection_t2176636147 * (*) (Dictionary_2_t3476030434 *, const MethodInfo*))Dictionary_2_get_Values_m3853824314_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::ToTKey(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTKey_m3383656665_gshared (Dictionary_2_t3476030434 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m3383656665(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3476030434 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3383656665_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::ToTValue(System.Object)
extern "C"  ProductInfo_t1305991238  Dictionary_2_ToTValue_m3735929845_gshared (Dictionary_2_t3476030434 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m3735929845(__this, ___value0, method) ((  ProductInfo_t1305991238  (*) (Dictionary_2_t3476030434 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m3735929845_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m2042893839_gshared (Dictionary_2_t3476030434 * __this, KeyValuePair_2_t3374811140  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m2042893839(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3476030434 *, KeyValuePair_2_t3374811140 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m2042893839_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::GetEnumerator()
extern "C"  Enumerator_t498386530  Dictionary_2_GetEnumerator_m4021125946_gshared (Dictionary_2_t3476030434 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m4021125946(__this, method) ((  Enumerator_t498386530  (*) (Dictionary_2_t3476030434 *, const MethodInfo*))Dictionary_2_GetEnumerator_m4021125946_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m986403121_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m986403121(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, ProductInfo_t1305991238 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m986403121_gshared)(__this /* static, unused */, ___key0, ___value1, method)
