﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3499168201.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_SkeletonBone421858229.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SkeletonBone>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1154634873_gshared (InternalEnumerator_1_t3499168201 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1154634873(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3499168201 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1154634873_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SkeletonBone>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3625038983_gshared (InternalEnumerator_1_t3499168201 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3625038983(__this, method) ((  void (*) (InternalEnumerator_1_t3499168201 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3625038983_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SkeletonBone>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3267179635_gshared (InternalEnumerator_1_t3499168201 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3267179635(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3499168201 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3267179635_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SkeletonBone>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1566536080_gshared (InternalEnumerator_1_t3499168201 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1566536080(__this, method) ((  void (*) (InternalEnumerator_1_t3499168201 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1566536080_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SkeletonBone>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2323776755_gshared (InternalEnumerator_1_t3499168201 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2323776755(__this, method) ((  bool (*) (InternalEnumerator_1_t3499168201 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2323776755_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SkeletonBone>::get_Current()
extern "C"  SkeletonBone_t421858229  InternalEnumerator_1_get_Current_m3739386048_gshared (InternalEnumerator_1_t3499168201 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3739386048(__this, method) ((  SkeletonBone_t421858229  (*) (InternalEnumerator_1_t3499168201 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3739386048_gshared)(__this, method)
