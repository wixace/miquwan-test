﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Xml_XmlDocumentGenerated/<XmlDocument_Validate_GetDelegate_member35_arg0>c__AnonStorey99
struct U3CXmlDocument_Validate_GetDelegate_member35_arg0U3Ec__AnonStorey99_t3563555400;
// System.Object
struct Il2CppObject;
// System.Xml.Schema.ValidationEventArgs
struct ValidationEventArgs_t3260433748;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Xml_System_Xml_Schema_ValidationEventArgs3260433748.h"

// System.Void System_Xml_XmlDocumentGenerated/<XmlDocument_Validate_GetDelegate_member35_arg0>c__AnonStorey99::.ctor()
extern "C"  void U3CXmlDocument_Validate_GetDelegate_member35_arg0U3Ec__AnonStorey99__ctor_m2618113571 (U3CXmlDocument_Validate_GetDelegate_member35_arg0U3Ec__AnonStorey99_t3563555400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlDocumentGenerated/<XmlDocument_Validate_GetDelegate_member35_arg0>c__AnonStorey99::<>m__F5(System.Object,System.Xml.Schema.ValidationEventArgs)
extern "C"  void U3CXmlDocument_Validate_GetDelegate_member35_arg0U3Ec__AnonStorey99_U3CU3Em__F5_m2245482046 (U3CXmlDocument_Validate_GetDelegate_member35_arg0U3Ec__AnonStorey99_t3563555400 * __this, Il2CppObject * ___sender0, ValidationEventArgs_t3260433748 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
