﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Compression.LZ.OutWindow
struct OutWindow_t503839888;
// System.IO.Stream
struct Stream_t1561764144;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZ_OutWindow503839888.h"

// System.Void SevenZip.Compression.LZ.OutWindow::.ctor()
extern "C"  void OutWindow__ctor_m3075153067 (OutWindow_t503839888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.OutWindow::Create(System.UInt32)
extern "C"  void OutWindow_Create_m2510592997 (OutWindow_t503839888 * __this, uint32_t ___windowSize0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.OutWindow::Init(System.IO.Stream,System.Boolean)
extern "C"  void OutWindow_Init_m2613719709 (OutWindow_t503839888 * __this, Stream_t1561764144 * ___stream0, bool ___solid1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.Compression.LZ.OutWindow::Train(System.IO.Stream)
extern "C"  bool OutWindow_Train_m1643052060 (OutWindow_t503839888 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.OutWindow::ReleaseStream()
extern "C"  void OutWindow_ReleaseStream_m1152073328 (OutWindow_t503839888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.OutWindow::Flush()
extern "C"  void OutWindow_Flush_m3159100365 (OutWindow_t503839888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.OutWindow::CopyBlock(System.UInt32,System.UInt32)
extern "C"  void OutWindow_CopyBlock_m4171844397 (OutWindow_t503839888 * __this, uint32_t ___distance0, uint32_t ___len1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.OutWindow::PutByte(System.Byte)
extern "C"  void OutWindow_PutByte_m2464360619 (OutWindow_t503839888 * __this, uint8_t ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte SevenZip.Compression.LZ.OutWindow::GetByte(System.UInt32)
extern "C"  uint8_t OutWindow_GetByte_m2213548295 (OutWindow_t503839888 * __this, uint32_t ___distance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZ.OutWindow::ilo_Flush1(SevenZip.Compression.LZ.OutWindow)
extern "C"  void OutWindow_ilo_Flush1_m1050072761 (Il2CppObject * __this /* static, unused */, OutWindow_t503839888 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
