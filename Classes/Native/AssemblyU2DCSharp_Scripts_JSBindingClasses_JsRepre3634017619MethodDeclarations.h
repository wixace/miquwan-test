﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Scripts.JSBindingClasses.JsRepresentClass/<GeneratedJSFuntion4>c__AnonStorey100
struct U3CGeneratedJSFuntion4U3Ec__AnonStorey100_t3634017619;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Scripts.JSBindingClasses.JsRepresentClass/<GeneratedJSFuntion4>c__AnonStorey100::.ctor()
extern "C"  void U3CGeneratedJSFuntion4U3Ec__AnonStorey100__ctor_m2485985144 (U3CGeneratedJSFuntion4U3Ec__AnonStorey100_t3634017619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Scripts.JSBindingClasses.JsRepresentClass/<GeneratedJSFuntion4>c__AnonStorey100::<>m__324(System.Object,System.Object,System.Object,System.Object)
extern "C"  Il2CppObject * U3CGeneratedJSFuntion4U3Ec__AnonStorey100_U3CU3Em__324_m315628029 (U3CGeneratedJSFuntion4U3Ec__AnonStorey100_t3634017619 * __this, Il2CppObject * ___object10, Il2CppObject * ___object21, Il2CppObject * ___object32, Il2CppObject * ___object43, const MethodInfo* method) IL2CPP_METHOD_ATTR;
