﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VersionMgr
struct VersionMgr_t1322950208;
// System.String
struct String_t;
// VersionInfo[]
struct VersionInfoU5BU5D_t1155590563;
// System.Collections.Generic.Dictionary`2<System.String,VersionInfo>
struct Dictionary_2_t3177056456;
// VersionInfo
struct VersionInfo_t2356638086;
// System.Action
struct Action_t3771233898;
// ABCheckUpdate
struct ABCheckUpdate_t527832336;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionInfo2356638086.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"

// System.Void VersionMgr::.ctor()
extern "C"  void VersionMgr__ctor_m3361669723 (VersionMgr_t1322950208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgr::.cctor()
extern "C"  void VersionMgr__cctor_m650450098 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr VersionMgr::get_Instance()
extern "C"  VersionMgr_t1322950208 * VersionMgr_get_Instance_m1140390500 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionMgr::get_isSdkLogin()
extern "C"  bool VersionMgr_get_isSdkLogin_m915697855 (VersionMgr_t1322950208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgr::set_isSdkLogin(System.Boolean)
extern "C"  void VersionMgr_set_isSdkLogin_m3520670134 (VersionMgr_t1322950208 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionMgr::get_isWaiwang()
extern "C"  bool VersionMgr_get_isWaiwang_m2947554260 (VersionMgr_t1322950208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgr::set_isWaiwang(System.Boolean)
extern "C"  void VersionMgr_set_isWaiwang_m566433931 (VersionMgr_t1322950208 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionMgr::get_isHide2000()
extern "C"  bool VersionMgr_get_isHide2000_m2229064240 (VersionMgr_t1322950208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgr::set_isHide2000(System.Boolean)
extern "C"  void VersionMgr_set_isHide2000_m1329443175 (VersionMgr_t1322950208 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VersionMgr::get_loadingbgName()
extern "C"  String_t* VersionMgr_get_loadingbgName_m3150626777 (VersionMgr_t1322950208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgr::set_loadingbgName(System.String)
extern "C"  void VersionMgr_set_loadingbgName_m3957773466 (VersionMgr_t1322950208 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo[] VersionMgr::get_vsArr()
extern "C"  VersionInfoU5BU5D_t1155590563* VersionMgr_get_vsArr_m866109001 (VersionMgr_t1322950208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgr::set_vsArr(VersionInfo[])
extern "C"  void VersionMgr_set_vsArr_m3525749034 (VersionMgr_t1322950208 * __this, VersionInfoU5BU5D_t1155590563* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,VersionInfo> VersionMgr::get_vsDic()
extern "C"  Dictionary_2_t3177056456 * VersionMgr_get_vsDic_m1325923629 (VersionMgr_t1322950208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgr::set_vsDic(System.Collections.Generic.Dictionary`2<System.String,VersionInfo>)
extern "C"  void VersionMgr_set_vsDic_m2150393284 (VersionMgr_t1322950208 * __this, Dictionary_2_t3177056456 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo VersionMgr::get_currentVS()
extern "C"  VersionInfo_t2356638086 * VersionMgr_get_currentVS_m4125223353 (VersionMgr_t1322950208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgr::set_currentVS(VersionInfo)
extern "C"  void VersionMgr_set_currentVS_m3987925050 (VersionMgr_t1322950208 * __this, VersionInfo_t2356638086 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionMgr::get_isSame()
extern "C"  bool VersionMgr_get_isSame_m67624854 (VersionMgr_t1322950208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgr::set_isSame(System.Boolean)
extern "C"  void VersionMgr_set_isSame_m1392456781 (VersionMgr_t1322950208 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionMgr::get_isReview()
extern "C"  bool VersionMgr_get_isReview_m2633429576 (VersionMgr_t1322950208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgr::set_isReview(System.Boolean)
extern "C"  void VersionMgr_set_isReview_m2743482239 (VersionMgr_t1322950208 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionMgr::get_isHideGuide()
extern "C"  bool VersionMgr_get_isHideGuide_m3868882700 (VersionMgr_t1322950208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgr::set_isHideGuide(System.Boolean)
extern "C"  void VersionMgr_set_isHideGuide_m993518019 (VersionMgr_t1322950208 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionMgr::isReviewGame()
extern "C"  bool VersionMgr_isReviewGame_m2650071121 (VersionMgr_t1322950208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VersionMgr::mh_get_user_language_locale()
extern "C"  String_t* VersionMgr_mh_get_user_language_locale_m2868273264 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionMgr::get_isTs()
extern "C"  bool VersionMgr_get_isTs_m603466543 (VersionMgr_t1322950208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VersionMgr::checkCurVersionDiff_I()
extern "C"  int32_t VersionMgr_checkCurVersionDiff_I_m3262978906 (VersionMgr_t1322950208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VersionMgr::checkCurVersionDiff(System.Int32,System.String,System.String)
extern "C"  String_t* VersionMgr_checkCurVersionDiff_m3813167780 (VersionMgr_t1322950208 * __this, int32_t ___index0, String_t* ___name1, String_t* ___str2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgr::Init(System.String)
extern "C"  void VersionMgr_Init_m2686438153 (VersionMgr_t1322950208 * __this, String_t* ___allVsStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo VersionMgr::GetvsInfoByName(System.String)
extern "C"  VersionInfo_t2356638086 * VersionMgr_GetvsInfoByName_m3588347795 (VersionMgr_t1322950208 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 VersionMgr::NeedDownloadSize(VersionInfo)
extern "C"  int64_t VersionMgr_NeedDownloadSize_m355576149 (VersionMgr_t1322950208 * __this, VersionInfo_t2356638086 * ___vsInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgr::LoadAllVSCfg(System.Int32,System.Action)
extern "C"  void VersionMgr_LoadAllVSCfg_m464541949 (VersionMgr_t1322950208 * __this, int32_t ___type0, Action_t3771233898 * ___call1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgr::CheckVersion()
extern "C"  void VersionMgr_CheckVersion_m4153742905 (VersionMgr_t1322950208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgr::ShowName()
extern "C"  void VersionMgr_ShowName_m31967409 (VersionMgr_t1322950208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgr::UpdateDeviceInfo()
extern "C"  void VersionMgr_UpdateDeviceInfo_m3690852886 (VersionMgr_t1322950208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgr::InitAppBean()
extern "C"  void VersionMgr_InitAppBean_m871271290 (VersionMgr_t1322950208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr VersionMgr::ilo_get_Instance1()
extern "C"  VersionMgr_t1322950208 * VersionMgr_ilo_get_Instance1_m2965271516 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo VersionMgr::ilo_get_currentVS2(VersionMgr)
extern "C"  VersionInfo_t2356638086 * VersionMgr_ilo_get_currentVS2_m2346987566 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,VersionInfo> VersionMgr::ilo_get_vsDic3(VersionMgr)
extern "C"  Dictionary_2_t3177056456 * VersionMgr_ilo_get_vsDic3_m3029299387 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo[] VersionMgr::ilo_get_vsArr4(VersionMgr)
extern "C"  VersionInfoU5BU5D_t1155590563* VersionMgr_ilo_get_vsArr4_m136288544 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ABCheckUpdate VersionMgr::ilo_get_Instance5()
extern "C"  ABCheckUpdate_t527832336 * VersionMgr_ilo_get_Instance5_m2024349082 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo VersionMgr::ilo_get_currentVS6(VersionMgr)
extern "C"  VersionInfo_t2356638086 * VersionMgr_ilo_get_currentVS6_m1173375538 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgr::ilo_set_currentVS7(VersionMgr,VersionInfo)
extern "C"  void VersionMgr_ilo_set_currentVS7_m4270229376 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, VersionInfo_t2356638086 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgr::ilo_set_isSame8(VersionMgr,System.Boolean)
extern "C"  void VersionMgr_ilo_set_isSame8_m233437548 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionMgr::ilo_get_isReview9(VersionMgr)
extern "C"  bool VersionMgr_ilo_get_isReview9_m2981978880 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
