﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ColorChangerVertex
struct ColorChangerVertex_t3646782595;

#include "codegen/il2cpp-codegen.h"

// System.Void ColorChangerVertex::.ctor()
extern "C"  void ColorChangerVertex__ctor_m3418941304 (ColorChangerVertex_t3646782595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorChangerVertex::Start()
extern "C"  void ColorChangerVertex_Start_m2366079096 (ColorChangerVertex_t3646782595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorChangerVertex::Update()
extern "C"  void ColorChangerVertex_Update_m339860117 (ColorChangerVertex_t3646782595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
