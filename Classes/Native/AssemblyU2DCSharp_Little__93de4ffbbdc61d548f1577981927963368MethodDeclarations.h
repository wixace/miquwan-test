﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._93de4ffbbdc61d548f157798159ab3d5
struct _93de4ffbbdc61d548f157798159ab3d5_t1927963368;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__93de4ffbbdc61d548f1577981927963368.h"

// System.Void Little._93de4ffbbdc61d548f157798159ab3d5::.ctor()
extern "C"  void _93de4ffbbdc61d548f157798159ab3d5__ctor_m1878362181 (_93de4ffbbdc61d548f157798159ab3d5_t1927963368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._93de4ffbbdc61d548f157798159ab3d5::_93de4ffbbdc61d548f157798159ab3d5m2(System.Int32)
extern "C"  int32_t _93de4ffbbdc61d548f157798159ab3d5__93de4ffbbdc61d548f157798159ab3d5m2_m143714457 (_93de4ffbbdc61d548f157798159ab3d5_t1927963368 * __this, int32_t ____93de4ffbbdc61d548f157798159ab3d5a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._93de4ffbbdc61d548f157798159ab3d5::_93de4ffbbdc61d548f157798159ab3d5m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _93de4ffbbdc61d548f157798159ab3d5__93de4ffbbdc61d548f157798159ab3d5m_m195272253 (_93de4ffbbdc61d548f157798159ab3d5_t1927963368 * __this, int32_t ____93de4ffbbdc61d548f157798159ab3d5a0, int32_t ____93de4ffbbdc61d548f157798159ab3d5721, int32_t ____93de4ffbbdc61d548f157798159ab3d5c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._93de4ffbbdc61d548f157798159ab3d5::ilo__93de4ffbbdc61d548f157798159ab3d5m21(Little._93de4ffbbdc61d548f157798159ab3d5,System.Int32)
extern "C"  int32_t _93de4ffbbdc61d548f157798159ab3d5_ilo__93de4ffbbdc61d548f157798159ab3d5m21_m3913031087 (Il2CppObject * __this /* static, unused */, _93de4ffbbdc61d548f157798159ab3d5_t1927963368 * ____this0, int32_t ____93de4ffbbdc61d548f157798159ab3d5a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
