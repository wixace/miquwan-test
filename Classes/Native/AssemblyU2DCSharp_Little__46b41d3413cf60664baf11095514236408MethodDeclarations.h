﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._46b41d3413cf60664baf11095b9f8ed9
struct _46b41d3413cf60664baf11095b9f8ed9_t514236408;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__46b41d3413cf60664baf11095514236408.h"

// System.Void Little._46b41d3413cf60664baf11095b9f8ed9::.ctor()
extern "C"  void _46b41d3413cf60664baf11095b9f8ed9__ctor_m4232379701 (_46b41d3413cf60664baf11095b9f8ed9_t514236408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._46b41d3413cf60664baf11095b9f8ed9::_46b41d3413cf60664baf11095b9f8ed9m2(System.Int32)
extern "C"  int32_t _46b41d3413cf60664baf11095b9f8ed9__46b41d3413cf60664baf11095b9f8ed9m2_m2380746393 (_46b41d3413cf60664baf11095b9f8ed9_t514236408 * __this, int32_t ____46b41d3413cf60664baf11095b9f8ed9a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._46b41d3413cf60664baf11095b9f8ed9::_46b41d3413cf60664baf11095b9f8ed9m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _46b41d3413cf60664baf11095b9f8ed9__46b41d3413cf60664baf11095b9f8ed9m_m2855008317 (_46b41d3413cf60664baf11095b9f8ed9_t514236408 * __this, int32_t ____46b41d3413cf60664baf11095b9f8ed9a0, int32_t ____46b41d3413cf60664baf11095b9f8ed9391, int32_t ____46b41d3413cf60664baf11095b9f8ed9c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._46b41d3413cf60664baf11095b9f8ed9::ilo__46b41d3413cf60664baf11095b9f8ed9m21(Little._46b41d3413cf60664baf11095b9f8ed9,System.Int32)
extern "C"  int32_t _46b41d3413cf60664baf11095b9f8ed9_ilo__46b41d3413cf60664baf11095b9f8ed9m21_m1002384575 (Il2CppObject * __this /* static, unused */, _46b41d3413cf60664baf11095b9f8ed9_t514236408 * ____this0, int32_t ____46b41d3413cf60664baf11095b9f8ed9a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
