﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<AnimationRunner/AniType,System.Object>
struct ShimEnumerator_t4021342866;
// System.Collections.Generic.Dictionary`2<AnimationRunner/AniType,System.Object>
struct Dictionary_2_t10597543;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<AnimationRunner/AniType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2355281879_gshared (ShimEnumerator_t4021342866 * __this, Dictionary_2_t10597543 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m2355281879(__this, ___host0, method) ((  void (*) (ShimEnumerator_t4021342866 *, Dictionary_2_t10597543 *, const MethodInfo*))ShimEnumerator__ctor_m2355281879_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<AnimationRunner/AniType,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2219287018_gshared (ShimEnumerator_t4021342866 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2219287018(__this, method) ((  bool (*) (ShimEnumerator_t4021342866 *, const MethodInfo*))ShimEnumerator_MoveNext_m2219287018_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<AnimationRunner/AniType,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m861689066_gshared (ShimEnumerator_t4021342866 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m861689066(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t4021342866 *, const MethodInfo*))ShimEnumerator_get_Entry_m861689066_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<AnimationRunner/AniType,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2900969925_gshared (ShimEnumerator_t4021342866 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m2900969925(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4021342866 *, const MethodInfo*))ShimEnumerator_get_Key_m2900969925_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<AnimationRunner/AniType,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1446759191_gshared (ShimEnumerator_t4021342866 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1446759191(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4021342866 *, const MethodInfo*))ShimEnumerator_get_Value_m1446759191_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<AnimationRunner/AniType,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m190869471_gshared (ShimEnumerator_t4021342866 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m190869471(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4021342866 *, const MethodInfo*))ShimEnumerator_get_Current_m190869471_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<AnimationRunner/AniType,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m2192672681_gshared (ShimEnumerator_t4021342866 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m2192672681(__this, method) ((  void (*) (ShimEnumerator_t4021342866 *, const MethodInfo*))ShimEnumerator_Reset_m2192672681_gshared)(__this, method)
