﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TeamSkillMgrGenerated
struct TeamSkillMgrGenerated_t3779808843;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// TeamSkillMgr
struct TeamSkillMgr_t2030650276;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_TeamSkillMgr2030650276.h"

// System.Void TeamSkillMgrGenerated::.ctor()
extern "C"  void TeamSkillMgrGenerated__ctor_m2557972352 (TeamSkillMgrGenerated_t3779808843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TeamSkillMgrGenerated::TeamSkillMgr_TeamSkillMgr1(JSVCall,System.Int32)
extern "C"  bool TeamSkillMgrGenerated_TeamSkillMgr_TeamSkillMgr1_m1728528114 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_CAN_USE(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_CAN_USE_m2643985254 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_RRFRESH(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_RRFRESH_m48393648 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_SELECT_SKILL(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_SELECT_SKILL_m3361414112 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_TEAM_SKILL_END(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_TEAM_SKILL_END_m144882019 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_TEAM_SKILL_CANCEL(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_TEAM_SKILL_CANCEL_m1969124116 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_TEAM_SKILL_LEIBAR_CLOSE(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_TEAM_SKILL_LEIBAR_CLOSE_m1702041394 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_TEAM_SKILL_LEIBAR_OPEN(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_TEAM_SKILL_LEIBAR_OPEN_m1755719032 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_TEAM_SKILL_CYCLONE_CREATE(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_TEAM_SKILL_CYCLONE_CREATE_m3159722266 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_TEAM_SKILL_ADDLEVEL(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_TEAM_SKILL_ADDLEVEL_m2413907499 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_TEAM_SKILL_ADDEXP(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_TEAM_SKILL_ADDEXP_m61148658 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_skillList(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_skillList_m168847631 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_rage(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_rage_m439073953 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_maxRage(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_maxRage_m1410288909 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_gradePoint(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_gradePoint_m4013301621 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_level(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_level_m421284794 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_accExp(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_accExp_m2546100466 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_curRatio(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_curRatio_m729507843 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_maxRatio(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_maxRatio_m3453828135 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_effectid(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_effectid_m2152616034 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_residueDegree(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_residueDegree_m140204135 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_allTimes(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_allTimes_m7183561 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_rageRate(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_rageRate_m2083198497 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_selectid(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_selectid_m590025751 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_curSelectSkill(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_curSelectSkill_m2830988953 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_isUseingSkill(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_isUseingSkill_m1760153010 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_isCanUse(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_isCanUse_m2101218573 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_damage(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_damage_m2815275199 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_progresslei(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_progresslei_m3759931227 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::TeamSkillMgr_progress(JSVCall)
extern "C"  void TeamSkillMgrGenerated_TeamSkillMgr_progress_m509926849 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TeamSkillMgrGenerated::TeamSkillMgr_AddExp(JSVCall,System.Int32)
extern "C"  bool TeamSkillMgrGenerated_TeamSkillMgr_AddExp_m1897796569 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TeamSkillMgrGenerated::TeamSkillMgr_AddRage__Int32(JSVCall,System.Int32)
extern "C"  bool TeamSkillMgrGenerated_TeamSkillMgr_AddRage__Int32_m581146205 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TeamSkillMgrGenerated::TeamSkillMgr_AddRage(JSVCall,System.Int32)
extern "C"  bool TeamSkillMgrGenerated_TeamSkillMgr_AddRage_m606483859 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TeamSkillMgrGenerated::TeamSkillMgr_GetCurSkill__Int32(JSVCall,System.Int32)
extern "C"  bool TeamSkillMgrGenerated_TeamSkillMgr_GetCurSkill__Int32_m2902412580 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TeamSkillMgrGenerated::TeamSkillMgr_Init(JSVCall,System.Int32)
extern "C"  bool TeamSkillMgrGenerated_TeamSkillMgr_Init_m79940397 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TeamSkillMgrGenerated::TeamSkillMgr_isTeamSkillAndType__Int32(JSVCall,System.Int32)
extern "C"  bool TeamSkillMgrGenerated_TeamSkillMgr_isTeamSkillAndType__Int32_m3451992748 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TeamSkillMgrGenerated::TeamSkillMgr_OnGameWin(JSVCall,System.Int32)
extern "C"  bool TeamSkillMgrGenerated_TeamSkillMgr_OnGameWin_m101101808 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TeamSkillMgrGenerated::TeamSkillMgr_OnUseSkillEnd_Break(JSVCall,System.Int32)
extern "C"  bool TeamSkillMgrGenerated_TeamSkillMgr_OnUseSkillEnd_Break_m202944375 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TeamSkillMgrGenerated::TeamSkillMgr_SelectSkill__Int32(JSVCall,System.Int32)
extern "C"  bool TeamSkillMgrGenerated_TeamSkillMgr_SelectSkill__Int32_m4116402838 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TeamSkillMgrGenerated::TeamSkillMgr_UseSkill(JSVCall,System.Int32)
extern "C"  bool TeamSkillMgrGenerated_TeamSkillMgr_UseSkill_m537909671 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TeamSkillMgrGenerated::TeamSkillMgr_ZUpdate(JSVCall,System.Int32)
extern "C"  bool TeamSkillMgrGenerated_TeamSkillMgr_ZUpdate_m2916940328 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::__Register()
extern "C"  void TeamSkillMgrGenerated___Register_m1593285959 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::ilo_setStringS1(System.Int32,System.String)
extern "C"  void TeamSkillMgrGenerated_ilo_setStringS1_m3657310793 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TeamSkillMgrGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * TeamSkillMgrGenerated_ilo_getObject2_m4247134758 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void TeamSkillMgrGenerated_ilo_setInt323_m4264166164 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TeamSkillMgrGenerated::ilo_getInt324(System.Int32)
extern "C"  int32_t TeamSkillMgrGenerated_ilo_getInt324_m1288255590 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void TeamSkillMgrGenerated_ilo_setSingle5_m4062767864 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TeamSkillMgrGenerated::ilo_setObject6(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t TeamSkillMgrGenerated_ilo_setObject6_m3032913907 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TeamSkillMgrGenerated::ilo_get_isUseingSkill7(TeamSkillMgr)
extern "C"  bool TeamSkillMgrGenerated_ilo_get_isUseingSkill7_m1306195571 (Il2CppObject * __this /* static, unused */, TeamSkillMgr_t2030650276 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TeamSkillMgrGenerated::ilo_get_damage8(TeamSkillMgr)
extern "C"  float TeamSkillMgrGenerated_ilo_get_damage8_m1539375251 (Il2CppObject * __this /* static, unused */, TeamSkillMgr_t2030650276 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::ilo_AddExp9(TeamSkillMgr)
extern "C"  void TeamSkillMgrGenerated_ilo_AddExp9_m950993700 (Il2CppObject * __this /* static, unused */, TeamSkillMgr_t2030650276 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TeamSkillMgrGenerated::ilo_GetCurSkill10(TeamSkillMgr,System.Int32)
extern "C"  int32_t TeamSkillMgrGenerated_ilo_GetCurSkill10_m2628407676 (Il2CppObject * __this /* static, unused */, TeamSkillMgr_t2030650276 * ____this0, int32_t ___checkPointID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TeamSkillMgrGenerated::ilo_isTeamSkillAndType11(TeamSkillMgr,System.Int32)
extern "C"  int32_t TeamSkillMgrGenerated_ilo_isTeamSkillAndType11_m2079583531 (Il2CppObject * __this /* static, unused */, TeamSkillMgr_t2030650276 * ____this0, int32_t ___skillID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgrGenerated::ilo_setBooleanS12(System.Int32,System.Boolean)
extern "C"  void TeamSkillMgrGenerated_ilo_setBooleanS12_m1639976115 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
