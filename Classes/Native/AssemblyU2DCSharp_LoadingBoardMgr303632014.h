﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LoadingBoardMgr
struct LoadingBoardMgr_t303632014;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UISlider
struct UISlider_t657469589;
// UISprite
struct UISprite_t661437049;
// UILabel
struct UILabel_t291504320;
// UnityEngine.Transform
struct Transform_t1659122786;
// UIPanel
struct UIPanel_t295209936;
// TweenAlpha
struct TweenAlpha_t2920325587;
// System.Action
struct Action_t3771233898;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadingBoardMgr
struct  LoadingBoardMgr_t303632014  : public Il2CppObject
{
public:
	// System.Boolean LoadingBoardMgr::isCleared
	bool ___isCleared_1;
	// System.String LoadingBoardMgr::_des
	String_t* ____des_2;
	// System.String LoadingBoardMgr::_per
	String_t* ____per_3;
	// UnityEngine.GameObject LoadingBoardMgr::go
	GameObject_t3674682005 * ___go_4;
	// UnityEngine.GameObject LoadingBoardMgr::AASgo
	GameObject_t3674682005 * ___AASgo_18;
	// UnityEngine.GameObject LoadingBoardMgr::AVgo
	GameObject_t3674682005 * ___AVgo_19;
	// UISlider LoadingBoardMgr::ProgressBar_sdr
	UISlider_t657469589 * ___ProgressBar_sdr_20;
	// UISprite LoadingBoardMgr::ForeGround_sp
	UISprite_t661437049 * ___ForeGround_sp_21;
	// UISprite LoadingBoardMgr::Thumb_sp
	UISprite_t661437049 * ___Thumb_sp_22;
	// UILabel LoadingBoardMgr::Percent_txt
	UILabel_t291504320 * ___Percent_txt_23;
	// UILabel LoadingBoardMgr::ClientVer_txt
	UILabel_t291504320 * ___ClientVer_txt_24;
	// UnityEngine.Transform LoadingBoardMgr::mTransform
	Transform_t1659122786 * ___mTransform_25;
	// UIPanel LoadingBoardMgr::dialogPnl
	UIPanel_t295209936 * ___dialogPnl_26;
	// UILabel LoadingBoardMgr::dialogText
	UILabel_t291504320 * ___dialogText_27;
	// UnityEngine.GameObject LoadingBoardMgr::dialogBtn
	GameObject_t3674682005 * ___dialogBtn_28;
	// UnityEngine.Transform LoadingBoardMgr::anitTransform
	Transform_t1659122786 * ___anitTransform_29;
	// TweenAlpha LoadingBoardMgr::BGTweenAlpha
	TweenAlpha_t2920325587 * ___BGTweenAlpha_30;
	// UnityEngine.Transform LoadingBoardMgr::loadingPic
	Transform_t1659122786 * ___loadingPic_31;
	// System.Action LoadingBoardMgr::dialgCallBack
	Action_t3771233898 * ___dialgCallBack_32;

public:
	inline static int32_t get_offset_of_isCleared_1() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014, ___isCleared_1)); }
	inline bool get_isCleared_1() const { return ___isCleared_1; }
	inline bool* get_address_of_isCleared_1() { return &___isCleared_1; }
	inline void set_isCleared_1(bool value)
	{
		___isCleared_1 = value;
	}

	inline static int32_t get_offset_of__des_2() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014, ____des_2)); }
	inline String_t* get__des_2() const { return ____des_2; }
	inline String_t** get_address_of__des_2() { return &____des_2; }
	inline void set__des_2(String_t* value)
	{
		____des_2 = value;
		Il2CppCodeGenWriteBarrier(&____des_2, value);
	}

	inline static int32_t get_offset_of__per_3() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014, ____per_3)); }
	inline String_t* get__per_3() const { return ____per_3; }
	inline String_t** get_address_of__per_3() { return &____per_3; }
	inline void set__per_3(String_t* value)
	{
		____per_3 = value;
		Il2CppCodeGenWriteBarrier(&____per_3, value);
	}

	inline static int32_t get_offset_of_go_4() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014, ___go_4)); }
	inline GameObject_t3674682005 * get_go_4() const { return ___go_4; }
	inline GameObject_t3674682005 ** get_address_of_go_4() { return &___go_4; }
	inline void set_go_4(GameObject_t3674682005 * value)
	{
		___go_4 = value;
		Il2CppCodeGenWriteBarrier(&___go_4, value);
	}

	inline static int32_t get_offset_of_AASgo_18() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014, ___AASgo_18)); }
	inline GameObject_t3674682005 * get_AASgo_18() const { return ___AASgo_18; }
	inline GameObject_t3674682005 ** get_address_of_AASgo_18() { return &___AASgo_18; }
	inline void set_AASgo_18(GameObject_t3674682005 * value)
	{
		___AASgo_18 = value;
		Il2CppCodeGenWriteBarrier(&___AASgo_18, value);
	}

	inline static int32_t get_offset_of_AVgo_19() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014, ___AVgo_19)); }
	inline GameObject_t3674682005 * get_AVgo_19() const { return ___AVgo_19; }
	inline GameObject_t3674682005 ** get_address_of_AVgo_19() { return &___AVgo_19; }
	inline void set_AVgo_19(GameObject_t3674682005 * value)
	{
		___AVgo_19 = value;
		Il2CppCodeGenWriteBarrier(&___AVgo_19, value);
	}

	inline static int32_t get_offset_of_ProgressBar_sdr_20() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014, ___ProgressBar_sdr_20)); }
	inline UISlider_t657469589 * get_ProgressBar_sdr_20() const { return ___ProgressBar_sdr_20; }
	inline UISlider_t657469589 ** get_address_of_ProgressBar_sdr_20() { return &___ProgressBar_sdr_20; }
	inline void set_ProgressBar_sdr_20(UISlider_t657469589 * value)
	{
		___ProgressBar_sdr_20 = value;
		Il2CppCodeGenWriteBarrier(&___ProgressBar_sdr_20, value);
	}

	inline static int32_t get_offset_of_ForeGround_sp_21() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014, ___ForeGround_sp_21)); }
	inline UISprite_t661437049 * get_ForeGround_sp_21() const { return ___ForeGround_sp_21; }
	inline UISprite_t661437049 ** get_address_of_ForeGround_sp_21() { return &___ForeGround_sp_21; }
	inline void set_ForeGround_sp_21(UISprite_t661437049 * value)
	{
		___ForeGround_sp_21 = value;
		Il2CppCodeGenWriteBarrier(&___ForeGround_sp_21, value);
	}

	inline static int32_t get_offset_of_Thumb_sp_22() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014, ___Thumb_sp_22)); }
	inline UISprite_t661437049 * get_Thumb_sp_22() const { return ___Thumb_sp_22; }
	inline UISprite_t661437049 ** get_address_of_Thumb_sp_22() { return &___Thumb_sp_22; }
	inline void set_Thumb_sp_22(UISprite_t661437049 * value)
	{
		___Thumb_sp_22 = value;
		Il2CppCodeGenWriteBarrier(&___Thumb_sp_22, value);
	}

	inline static int32_t get_offset_of_Percent_txt_23() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014, ___Percent_txt_23)); }
	inline UILabel_t291504320 * get_Percent_txt_23() const { return ___Percent_txt_23; }
	inline UILabel_t291504320 ** get_address_of_Percent_txt_23() { return &___Percent_txt_23; }
	inline void set_Percent_txt_23(UILabel_t291504320 * value)
	{
		___Percent_txt_23 = value;
		Il2CppCodeGenWriteBarrier(&___Percent_txt_23, value);
	}

	inline static int32_t get_offset_of_ClientVer_txt_24() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014, ___ClientVer_txt_24)); }
	inline UILabel_t291504320 * get_ClientVer_txt_24() const { return ___ClientVer_txt_24; }
	inline UILabel_t291504320 ** get_address_of_ClientVer_txt_24() { return &___ClientVer_txt_24; }
	inline void set_ClientVer_txt_24(UILabel_t291504320 * value)
	{
		___ClientVer_txt_24 = value;
		Il2CppCodeGenWriteBarrier(&___ClientVer_txt_24, value);
	}

	inline static int32_t get_offset_of_mTransform_25() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014, ___mTransform_25)); }
	inline Transform_t1659122786 * get_mTransform_25() const { return ___mTransform_25; }
	inline Transform_t1659122786 ** get_address_of_mTransform_25() { return &___mTransform_25; }
	inline void set_mTransform_25(Transform_t1659122786 * value)
	{
		___mTransform_25 = value;
		Il2CppCodeGenWriteBarrier(&___mTransform_25, value);
	}

	inline static int32_t get_offset_of_dialogPnl_26() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014, ___dialogPnl_26)); }
	inline UIPanel_t295209936 * get_dialogPnl_26() const { return ___dialogPnl_26; }
	inline UIPanel_t295209936 ** get_address_of_dialogPnl_26() { return &___dialogPnl_26; }
	inline void set_dialogPnl_26(UIPanel_t295209936 * value)
	{
		___dialogPnl_26 = value;
		Il2CppCodeGenWriteBarrier(&___dialogPnl_26, value);
	}

	inline static int32_t get_offset_of_dialogText_27() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014, ___dialogText_27)); }
	inline UILabel_t291504320 * get_dialogText_27() const { return ___dialogText_27; }
	inline UILabel_t291504320 ** get_address_of_dialogText_27() { return &___dialogText_27; }
	inline void set_dialogText_27(UILabel_t291504320 * value)
	{
		___dialogText_27 = value;
		Il2CppCodeGenWriteBarrier(&___dialogText_27, value);
	}

	inline static int32_t get_offset_of_dialogBtn_28() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014, ___dialogBtn_28)); }
	inline GameObject_t3674682005 * get_dialogBtn_28() const { return ___dialogBtn_28; }
	inline GameObject_t3674682005 ** get_address_of_dialogBtn_28() { return &___dialogBtn_28; }
	inline void set_dialogBtn_28(GameObject_t3674682005 * value)
	{
		___dialogBtn_28 = value;
		Il2CppCodeGenWriteBarrier(&___dialogBtn_28, value);
	}

	inline static int32_t get_offset_of_anitTransform_29() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014, ___anitTransform_29)); }
	inline Transform_t1659122786 * get_anitTransform_29() const { return ___anitTransform_29; }
	inline Transform_t1659122786 ** get_address_of_anitTransform_29() { return &___anitTransform_29; }
	inline void set_anitTransform_29(Transform_t1659122786 * value)
	{
		___anitTransform_29 = value;
		Il2CppCodeGenWriteBarrier(&___anitTransform_29, value);
	}

	inline static int32_t get_offset_of_BGTweenAlpha_30() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014, ___BGTweenAlpha_30)); }
	inline TweenAlpha_t2920325587 * get_BGTweenAlpha_30() const { return ___BGTweenAlpha_30; }
	inline TweenAlpha_t2920325587 ** get_address_of_BGTweenAlpha_30() { return &___BGTweenAlpha_30; }
	inline void set_BGTweenAlpha_30(TweenAlpha_t2920325587 * value)
	{
		___BGTweenAlpha_30 = value;
		Il2CppCodeGenWriteBarrier(&___BGTweenAlpha_30, value);
	}

	inline static int32_t get_offset_of_loadingPic_31() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014, ___loadingPic_31)); }
	inline Transform_t1659122786 * get_loadingPic_31() const { return ___loadingPic_31; }
	inline Transform_t1659122786 ** get_address_of_loadingPic_31() { return &___loadingPic_31; }
	inline void set_loadingPic_31(Transform_t1659122786 * value)
	{
		___loadingPic_31 = value;
		Il2CppCodeGenWriteBarrier(&___loadingPic_31, value);
	}

	inline static int32_t get_offset_of_dialgCallBack_32() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014, ___dialgCallBack_32)); }
	inline Action_t3771233898 * get_dialgCallBack_32() const { return ___dialgCallBack_32; }
	inline Action_t3771233898 ** get_address_of_dialgCallBack_32() { return &___dialgCallBack_32; }
	inline void set_dialgCallBack_32(Action_t3771233898 * value)
	{
		___dialgCallBack_32 = value;
		Il2CppCodeGenWriteBarrier(&___dialgCallBack_32, value);
	}
};

struct LoadingBoardMgr_t303632014_StaticFields
{
public:
	// LoadingBoardMgr LoadingBoardMgr::_instance
	LoadingBoardMgr_t303632014 * ____instance_0;
	// System.String LoadingBoardMgr::checkUpdate
	String_t* ___checkUpdate_5;
	// System.String LoadingBoardMgr::update
	String_t* ___update_6;
	// System.String LoadingBoardMgr::unzipAss
	String_t* ___unzipAss_7;
	// System.String LoadingBoardMgr::moveAss
	String_t* ___moveAss_8;
	// System.String LoadingBoardMgr::loadJs
	String_t* ___loadJs_9;
	// System.String LoadingBoardMgr::initAss
	String_t* ___initAss_10;
	// System.String LoadingBoardMgr::loadCfg
	String_t* ___loadCfg_11;
	// System.String LoadingBoardMgr::initCfg
	String_t* ___initCfg_12;
	// System.String LoadingBoardMgr::AssetMgrInit
	String_t* ___AssetMgrInit_13;
	// System.String LoadingBoardMgr::versionMsg
	String_t* ___versionMsg_14;
	// System.String LoadingBoardMgr::readCfg
	String_t* ___readCfg_15;
	// System.String LoadingBoardMgr::downError
	String_t* ___downError_16;
	// System.String LoadingBoardMgr::downing
	String_t* ___downing_17;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014_StaticFields, ____instance_0)); }
	inline LoadingBoardMgr_t303632014 * get__instance_0() const { return ____instance_0; }
	inline LoadingBoardMgr_t303632014 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(LoadingBoardMgr_t303632014 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}

	inline static int32_t get_offset_of_checkUpdate_5() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014_StaticFields, ___checkUpdate_5)); }
	inline String_t* get_checkUpdate_5() const { return ___checkUpdate_5; }
	inline String_t** get_address_of_checkUpdate_5() { return &___checkUpdate_5; }
	inline void set_checkUpdate_5(String_t* value)
	{
		___checkUpdate_5 = value;
		Il2CppCodeGenWriteBarrier(&___checkUpdate_5, value);
	}

	inline static int32_t get_offset_of_update_6() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014_StaticFields, ___update_6)); }
	inline String_t* get_update_6() const { return ___update_6; }
	inline String_t** get_address_of_update_6() { return &___update_6; }
	inline void set_update_6(String_t* value)
	{
		___update_6 = value;
		Il2CppCodeGenWriteBarrier(&___update_6, value);
	}

	inline static int32_t get_offset_of_unzipAss_7() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014_StaticFields, ___unzipAss_7)); }
	inline String_t* get_unzipAss_7() const { return ___unzipAss_7; }
	inline String_t** get_address_of_unzipAss_7() { return &___unzipAss_7; }
	inline void set_unzipAss_7(String_t* value)
	{
		___unzipAss_7 = value;
		Il2CppCodeGenWriteBarrier(&___unzipAss_7, value);
	}

	inline static int32_t get_offset_of_moveAss_8() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014_StaticFields, ___moveAss_8)); }
	inline String_t* get_moveAss_8() const { return ___moveAss_8; }
	inline String_t** get_address_of_moveAss_8() { return &___moveAss_8; }
	inline void set_moveAss_8(String_t* value)
	{
		___moveAss_8 = value;
		Il2CppCodeGenWriteBarrier(&___moveAss_8, value);
	}

	inline static int32_t get_offset_of_loadJs_9() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014_StaticFields, ___loadJs_9)); }
	inline String_t* get_loadJs_9() const { return ___loadJs_9; }
	inline String_t** get_address_of_loadJs_9() { return &___loadJs_9; }
	inline void set_loadJs_9(String_t* value)
	{
		___loadJs_9 = value;
		Il2CppCodeGenWriteBarrier(&___loadJs_9, value);
	}

	inline static int32_t get_offset_of_initAss_10() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014_StaticFields, ___initAss_10)); }
	inline String_t* get_initAss_10() const { return ___initAss_10; }
	inline String_t** get_address_of_initAss_10() { return &___initAss_10; }
	inline void set_initAss_10(String_t* value)
	{
		___initAss_10 = value;
		Il2CppCodeGenWriteBarrier(&___initAss_10, value);
	}

	inline static int32_t get_offset_of_loadCfg_11() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014_StaticFields, ___loadCfg_11)); }
	inline String_t* get_loadCfg_11() const { return ___loadCfg_11; }
	inline String_t** get_address_of_loadCfg_11() { return &___loadCfg_11; }
	inline void set_loadCfg_11(String_t* value)
	{
		___loadCfg_11 = value;
		Il2CppCodeGenWriteBarrier(&___loadCfg_11, value);
	}

	inline static int32_t get_offset_of_initCfg_12() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014_StaticFields, ___initCfg_12)); }
	inline String_t* get_initCfg_12() const { return ___initCfg_12; }
	inline String_t** get_address_of_initCfg_12() { return &___initCfg_12; }
	inline void set_initCfg_12(String_t* value)
	{
		___initCfg_12 = value;
		Il2CppCodeGenWriteBarrier(&___initCfg_12, value);
	}

	inline static int32_t get_offset_of_AssetMgrInit_13() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014_StaticFields, ___AssetMgrInit_13)); }
	inline String_t* get_AssetMgrInit_13() const { return ___AssetMgrInit_13; }
	inline String_t** get_address_of_AssetMgrInit_13() { return &___AssetMgrInit_13; }
	inline void set_AssetMgrInit_13(String_t* value)
	{
		___AssetMgrInit_13 = value;
		Il2CppCodeGenWriteBarrier(&___AssetMgrInit_13, value);
	}

	inline static int32_t get_offset_of_versionMsg_14() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014_StaticFields, ___versionMsg_14)); }
	inline String_t* get_versionMsg_14() const { return ___versionMsg_14; }
	inline String_t** get_address_of_versionMsg_14() { return &___versionMsg_14; }
	inline void set_versionMsg_14(String_t* value)
	{
		___versionMsg_14 = value;
		Il2CppCodeGenWriteBarrier(&___versionMsg_14, value);
	}

	inline static int32_t get_offset_of_readCfg_15() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014_StaticFields, ___readCfg_15)); }
	inline String_t* get_readCfg_15() const { return ___readCfg_15; }
	inline String_t** get_address_of_readCfg_15() { return &___readCfg_15; }
	inline void set_readCfg_15(String_t* value)
	{
		___readCfg_15 = value;
		Il2CppCodeGenWriteBarrier(&___readCfg_15, value);
	}

	inline static int32_t get_offset_of_downError_16() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014_StaticFields, ___downError_16)); }
	inline String_t* get_downError_16() const { return ___downError_16; }
	inline String_t** get_address_of_downError_16() { return &___downError_16; }
	inline void set_downError_16(String_t* value)
	{
		___downError_16 = value;
		Il2CppCodeGenWriteBarrier(&___downError_16, value);
	}

	inline static int32_t get_offset_of_downing_17() { return static_cast<int32_t>(offsetof(LoadingBoardMgr_t303632014_StaticFields, ___downing_17)); }
	inline String_t* get_downing_17() const { return ___downing_17; }
	inline String_t** get_address_of_downing_17() { return &___downing_17; }
	inline void set_downing_17(String_t* value)
	{
		___downing_17 = value;
		Il2CppCodeGenWriteBarrier(&___downing_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
