﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_ICancelHandlerGenerated
struct UnityEngine_EventSystems_ICancelHandlerGenerated_t3386730491;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_EventSystems_ICancelHandlerGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_ICancelHandlerGenerated__ctor_m3252627712 (UnityEngine_EventSystems_ICancelHandlerGenerated_t3386730491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_ICancelHandlerGenerated::ICancelHandler_OnCancel__BaseEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_ICancelHandlerGenerated_ICancelHandler_OnCancel__BaseEventData_m2745664170 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_ICancelHandlerGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_ICancelHandlerGenerated___Register_m3990090695 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
