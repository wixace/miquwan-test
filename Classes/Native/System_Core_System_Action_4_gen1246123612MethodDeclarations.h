﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_4_gen1491902808MethodDeclarations.h"

// System.Void System.Action`4<System.Int32,CombatEntity,FightEnum.EDamageType,System.Int32>::.ctor(System.Object,System.IntPtr)
#define Action_4__ctor_m3235490016(__this, ___object0, ___method1, method) ((  void (*) (Action_4_t1246123612 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_4__ctor_m1038436083_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`4<System.Int32,CombatEntity,FightEnum.EDamageType,System.Int32>::Invoke(T1,T2,T3,T4)
#define Action_4_Invoke_m2694039258(__this, ___arg10, ___arg21, ___arg32, ___arg43, method) ((  void (*) (Action_4_t1246123612 *, int32_t, CombatEntity_t684137495 *, int32_t, int32_t, const MethodInfo*))Action_4_Invoke_m3680689175_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, method)
// System.IAsyncResult System.Action`4<System.Int32,CombatEntity,FightEnum.EDamageType,System.Int32>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
#define Action_4_BeginInvoke_m2331050211(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method) ((  Il2CppObject * (*) (Action_4_t1246123612 *, int32_t, CombatEntity_t684137495 *, int32_t, int32_t, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_4_BeginInvoke_m995807384_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method)
// System.Void System.Action`4<System.Int32,CombatEntity,FightEnum.EDamageType,System.Int32>::EndInvoke(System.IAsyncResult)
#define Action_4_EndInvoke_m2403989472(__this, ___result0, method) ((  void (*) (Action_4_t1246123612 *, Il2CppObject *, const MethodInfo*))Action_4_EndInvoke_m889203331_gshared)(__this, ___result0, method)
