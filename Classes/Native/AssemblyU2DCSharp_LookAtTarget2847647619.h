﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LookAtTarget
struct  LookAtTarget_t2847647619  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Transform LookAtTarget::_target
	Transform_t1659122786 * ____target_2;
	// System.Single LookAtTarget::_speed
	float ____speed_3;
	// UnityEngine.Vector3 LookAtTarget::_lookAtTarget
	Vector3_t4282066566  ____lookAtTarget_4;

public:
	inline static int32_t get_offset_of__target_2() { return static_cast<int32_t>(offsetof(LookAtTarget_t2847647619, ____target_2)); }
	inline Transform_t1659122786 * get__target_2() const { return ____target_2; }
	inline Transform_t1659122786 ** get_address_of__target_2() { return &____target_2; }
	inline void set__target_2(Transform_t1659122786 * value)
	{
		____target_2 = value;
		Il2CppCodeGenWriteBarrier(&____target_2, value);
	}

	inline static int32_t get_offset_of__speed_3() { return static_cast<int32_t>(offsetof(LookAtTarget_t2847647619, ____speed_3)); }
	inline float get__speed_3() const { return ____speed_3; }
	inline float* get_address_of__speed_3() { return &____speed_3; }
	inline void set__speed_3(float value)
	{
		____speed_3 = value;
	}

	inline static int32_t get_offset_of__lookAtTarget_4() { return static_cast<int32_t>(offsetof(LookAtTarget_t2847647619, ____lookAtTarget_4)); }
	inline Vector3_t4282066566  get__lookAtTarget_4() const { return ____lookAtTarget_4; }
	inline Vector3_t4282066566 * get_address_of__lookAtTarget_4() { return &____lookAtTarget_4; }
	inline void set__lookAtTarget_4(Vector3_t4282066566  value)
	{
		____lookAtTarget_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
