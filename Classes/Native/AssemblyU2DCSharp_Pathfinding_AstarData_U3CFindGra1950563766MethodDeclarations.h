﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.AstarData/<FindGraphsOfType>c__Iterator8
struct U3CFindGraphsOfTypeU3Ec__Iterator8_t1950563766;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.AstarData/<FindGraphsOfType>c__Iterator8::.ctor()
extern "C"  void U3CFindGraphsOfTypeU3Ec__Iterator8__ctor_m2061617573 (U3CFindGraphsOfTypeU3Ec__Iterator8_t1950563766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.AstarData/<FindGraphsOfType>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFindGraphsOfTypeU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3298885847 (U3CFindGraphsOfTypeU3Ec__Iterator8_t1950563766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.AstarData/<FindGraphsOfType>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFindGraphsOfTypeU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m805671531 (U3CFindGraphsOfTypeU3Ec__Iterator8_t1950563766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Pathfinding.AstarData/<FindGraphsOfType>c__Iterator8::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CFindGraphsOfTypeU3Ec__Iterator8_System_Collections_IEnumerable_GetEnumerator_m2413507500 (U3CFindGraphsOfTypeU3Ec__Iterator8_t1950563766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Object> Pathfinding.AstarData/<FindGraphsOfType>c__Iterator8::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern "C"  Il2CppObject* U3CFindGraphsOfTypeU3Ec__Iterator8_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1479306998 (U3CFindGraphsOfTypeU3Ec__Iterator8_t1950563766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.AstarData/<FindGraphsOfType>c__Iterator8::MoveNext()
extern "C"  bool U3CFindGraphsOfTypeU3Ec__Iterator8_MoveNext_m637246487 (U3CFindGraphsOfTypeU3Ec__Iterator8_t1950563766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData/<FindGraphsOfType>c__Iterator8::Dispose()
extern "C"  void U3CFindGraphsOfTypeU3Ec__Iterator8_Dispose_m1132847266 (U3CFindGraphsOfTypeU3Ec__Iterator8_t1950563766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData/<FindGraphsOfType>c__Iterator8::Reset()
extern "C"  void U3CFindGraphsOfTypeU3Ec__Iterator8_Reset_m4003017810 (U3CFindGraphsOfTypeU3Ec__Iterator8_t1950563766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
