﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>
struct ReadOnlyCollection_1_t1384076478;
// System.Collections.Generic.IList`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>
struct IList_1_t2521646145;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// UnityEngine.MeshSubsetCombineUtility/MeshInstance[]
struct MeshInstanceU5BU5D_t1318761771;
// System.Collections.Generic.IEnumerator`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>
struct IEnumerator_1_t1738863991;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_MeshSubsetCombineUtility_M4121966238.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3455772178_gshared (ReadOnlyCollection_1_t1384076478 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3455772178(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1384076478 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3455772178_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3774463740_gshared (ReadOnlyCollection_1_t1384076478 * __this, MeshInstance_t4121966238  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3774463740(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1384076478 *, MeshInstance_t4121966238 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3774463740_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3050949966_gshared (ReadOnlyCollection_1_t1384076478 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3050949966(__this, method) ((  void (*) (ReadOnlyCollection_1_t1384076478 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3050949966_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2902505187_gshared (ReadOnlyCollection_1_t1384076478 * __this, int32_t ___index0, MeshInstance_t4121966238  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2902505187(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1384076478 *, int32_t, MeshInstance_t4121966238 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2902505187_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1432369783_gshared (ReadOnlyCollection_1_t1384076478 * __this, MeshInstance_t4121966238  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1432369783(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1384076478 *, MeshInstance_t4121966238 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1432369783_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m776358057_gshared (ReadOnlyCollection_1_t1384076478 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m776358057(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1384076478 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m776358057_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  MeshInstance_t4121966238  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1719589229_gshared (ReadOnlyCollection_1_t1384076478 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1719589229(__this, ___index0, method) ((  MeshInstance_t4121966238  (*) (ReadOnlyCollection_1_t1384076478 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1719589229_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3683306234_gshared (ReadOnlyCollection_1_t1384076478 * __this, int32_t ___index0, MeshInstance_t4121966238  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3683306234(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1384076478 *, int32_t, MeshInstance_t4121966238 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3683306234_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2574131000_gshared (ReadOnlyCollection_1_t1384076478 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2574131000(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1384076478 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2574131000_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2022636481_gshared (ReadOnlyCollection_1_t1384076478 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2022636481(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1384076478 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2022636481_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4106183292_gshared (ReadOnlyCollection_1_t1384076478 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4106183292(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1384076478 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4106183292_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3304605813_gshared (ReadOnlyCollection_1_t1384076478 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3304605813(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1384076478 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3304605813_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m3524480719_gshared (ReadOnlyCollection_1_t1384076478 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3524480719(__this, method) ((  void (*) (ReadOnlyCollection_1_t1384076478 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3524480719_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1015183351_gshared (ReadOnlyCollection_1_t1384076478 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1015183351(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1384076478 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1015183351_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m893729869_gshared (ReadOnlyCollection_1_t1384076478 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m893729869(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1384076478 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m893729869_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1593308152_gshared (ReadOnlyCollection_1_t1384076478 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1593308152(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1384076478 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1593308152_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m86417968_gshared (ReadOnlyCollection_1_t1384076478 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m86417968(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1384076478 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m86417968_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m4264312712_gshared (ReadOnlyCollection_1_t1384076478 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m4264312712(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1384076478 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m4264312712_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2457454597_gshared (ReadOnlyCollection_1_t1384076478 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2457454597(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1384076478 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2457454597_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2566087153_gshared (ReadOnlyCollection_1_t1384076478 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2566087153(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1384076478 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2566087153_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2347052198_gshared (ReadOnlyCollection_1_t1384076478 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2347052198(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1384076478 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2347052198_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1083909523_gshared (ReadOnlyCollection_1_t1384076478 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1083909523(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1384076478 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1083909523_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1764137784_gshared (ReadOnlyCollection_1_t1384076478 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1764137784(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1384076478 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1764137784_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2774729167_gshared (ReadOnlyCollection_1_t1384076478 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2774729167(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1384076478 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2774729167_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m2121059004_gshared (ReadOnlyCollection_1_t1384076478 * __this, MeshInstance_t4121966238  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m2121059004(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1384076478 *, MeshInstance_t4121966238 , const MethodInfo*))ReadOnlyCollection_1_Contains_m2121059004_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m849036844_gshared (ReadOnlyCollection_1_t1384076478 * __this, MeshInstanceU5BU5D_t1318761771* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m849036844(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1384076478 *, MeshInstanceU5BU5D_t1318761771*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m849036844_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m564548255_gshared (ReadOnlyCollection_1_t1384076478 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m564548255(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1384076478 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m564548255_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1161536432_gshared (ReadOnlyCollection_1_t1384076478 * __this, MeshInstance_t4121966238  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1161536432(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1384076478 *, MeshInstance_t4121966238 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1161536432_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1465198539_gshared (ReadOnlyCollection_1_t1384076478 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1465198539(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1384076478 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1465198539_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::get_Item(System.Int32)
extern "C"  MeshInstance_t4121966238  ReadOnlyCollection_1_get_Item_m2192846253_gshared (ReadOnlyCollection_1_t1384076478 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m2192846253(__this, ___index0, method) ((  MeshInstance_t4121966238  (*) (ReadOnlyCollection_1_t1384076478 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2192846253_gshared)(__this, ___index0, method)
