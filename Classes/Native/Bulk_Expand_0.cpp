﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Char[]
struct CharU5BU5D_t3324145743;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "Expand_U3CModuleU3E86524790.h"
#include "Expand_U3CModuleU3E86524790MethodDeclarations.h"
#include "Expand_EArray2038949140.h"
#include "Expand_EArray2038949140MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_Array1146569071MethodDeclarations.h"
#include "Expand_EList66091651.h"
#include "Expand_EList66091651MethodDeclarations.h"
#include "Expand_EString3595060406.h"
#include "Expand_EString3595060406MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Char2862622538.h"
#include "mscorlib_System_Int321153838500MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 EArray::IndexOf(System.Array,System.Object)
extern "C"  int32_t EArray_IndexOf_m926102760 (Il2CppObject * __this /* static, unused */, Il2CppArray * ___array0, Il2CppObject * ___value1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppArray * L_0 = ___array0;
		Il2CppObject * L_1 = ___value1;
		int32_t L_2 = Array_IndexOf_m480670679(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000b;
	}

IL_000b:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Int32 EArray::IndexOf(System.Array,System.Object,System.Int32)
extern "C"  int32_t EArray_IndexOf_m12787951 (Il2CppObject * __this /* static, unused */, Il2CppArray * ___array0, Il2CppObject * ___value1, int32_t ___startIndex2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppArray * L_0 = ___array0;
		Il2CppObject * L_1 = ___value1;
		int32_t L_2 = ___startIndex2;
		int32_t L_3 = Array_IndexOf_m1662246496(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Int32 EArray::IndexOf(System.Array,System.Object,System.Int32,System.Int32)
extern "C"  int32_t EArray_IndexOf_m444807240 (Il2CppObject * __this /* static, unused */, Il2CppArray * ___array0, Il2CppObject * ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppArray * L_0 = ___array0;
		Il2CppObject * L_1 = ___value1;
		int32_t L_2 = ___startIndex2;
		int32_t L_3 = ___count3;
		int32_t L_4 = Array_IndexOf_m1794349687(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Int32 EArray::LastIndexOf(System.Array,System.Object)
extern "C"  int32_t EArray_LastIndexOf_m3176648050 (Il2CppObject * __this /* static, unused */, Il2CppArray * ___array0, Il2CppObject * ___value1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppArray * L_0 = ___array0;
		Il2CppObject * L_1 = ___value1;
		int32_t L_2 = Array_LastIndexOf_m673447137(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000b;
	}

IL_000b:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Int32 EArray::LastIndexOf(System.Array,System.Object,System.Int32)
extern "C"  int32_t EArray_LastIndexOf_m107231397 (Il2CppObject * __this /* static, unused */, Il2CppArray * ___array0, Il2CppObject * ___value1, int32_t ___startIndex2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppArray * L_0 = ___array0;
		Il2CppObject * L_1 = ___value1;
		int32_t L_2 = ___startIndex2;
		int32_t L_3 = Array_LastIndexOf_m776394134(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Int32 EArray::LastIndexOf(System.Array,System.Object,System.Int32,System.Int32)
extern "C"  int32_t EArray_LastIndexOf_m2202851410 (Il2CppObject * __this /* static, unused */, Il2CppArray * ___array0, Il2CppObject * ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppArray * L_0 = ___array0;
		Il2CppObject * L_1 = ___value1;
		int32_t L_2 = ___startIndex2;
		int32_t L_3 = ___count3;
		int32_t L_4 = Array_LastIndexOf_m2203224833(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.String EString::EFormat(System.String,System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t EString_EFormat_m1422808458_MetadataUsageId;
extern "C"  String_t* EString_EFormat_m1422808458 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___arg01, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EString_EFormat_m1422808458_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___format0;
		Il2CppObject * L_1 = ___arg01;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m2471250780(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000b;
	}

IL_000b:
	{
		String_t* L_3 = V_0;
		return L_3;
	}
}
// System.String EString::EFormat(System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t EString_EFormat_m1519378984_MetadataUsageId;
extern "C"  String_t* EString_EFormat_m1519378984 (Il2CppObject * __this /* static, unused */, String_t* ___format0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EString_EFormat_m1519378984_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___format0;
		ObjectU5BU5D_t1108656482* L_1 = ___args1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m4050103162(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000b;
	}

IL_000b:
	{
		String_t* L_3 = V_0;
		return L_3;
	}
}
// System.String EString::EFormat(System.IFormatProvider,System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t EString_EFormat_m785528092_MetadataUsageId;
extern "C"  String_t* EString_EFormat_m785528092 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___provider0, String_t* ___format1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EString_EFormat_m785528092_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		Il2CppObject * L_0 = ___provider0;
		String_t* L_1 = ___format1;
		ObjectU5BU5D_t1108656482* L_2 = ___args2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m3351777162(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// System.String EString::EFormat(System.String,System.Object,System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t EString_EFormat_m1016276696_MetadataUsageId;
extern "C"  String_t* EString_EFormat_m1016276696 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___arg01, Il2CppObject * ___arg12, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EString_EFormat_m1016276696_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___format0;
		Il2CppObject * L_1 = ___arg01;
		Il2CppObject * L_2 = ___arg12;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m2398979370(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// System.String EString::EFormat(System.String,System.Object,System.Object,System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t EString_EFormat_m3220215718_MetadataUsageId;
extern "C"  String_t* EString_EFormat_m3220215718 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___arg01, Il2CppObject * ___arg12, Il2CppObject * ___arg23, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EString_EFormat_m3220215718_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___format0;
		Il2CppObject * L_1 = ___arg01;
		Il2CppObject * L_2 = ___arg12;
		Il2CppObject * L_3 = ___arg23;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m3928391288(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_5 = V_0;
		return L_5;
	}
}
// System.String EString::EReplace(System.String,System.String,System.String)
extern "C"  String_t* EString_EReplace_m3544730803 (Il2CppObject * __this /* static, unused */, String_t* ___t0, String_t* ___oldString1, String_t* ___newString2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	bool V_2 = false;
	{
		String_t* L_0 = ___t0;
		String_t* L_1 = ___oldString1;
		NullCheck(L_0);
		int32_t L_2 = String_IndexOf_m1476794331(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		V_2 = (bool)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0);
		bool L_4 = V_2;
		if (L_4)
		{
			goto IL_002c;
		}
	}
	{
		String_t* L_5 = ___t0;
		int32_t L_6 = V_0;
		String_t* L_7 = ___oldString1;
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m2979997331(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_9 = String_Remove_m242090629(L_5, L_6, L_8, /*hidden argument*/NULL);
		___t0 = L_9;
		String_t* L_10 = ___t0;
		int32_t L_11 = V_0;
		String_t* L_12 = ___newString2;
		NullCheck(L_10);
		String_t* L_13 = String_Insert_m3926397187(L_10, L_11, L_12, /*hidden argument*/NULL);
		___t0 = L_13;
	}

IL_002c:
	{
		String_t* L_14 = ___t0;
		V_1 = L_14;
		goto IL_0030;
	}

IL_0030:
	{
		String_t* L_15 = V_1;
		return L_15;
	}
}
// System.String EString::EReplace(System.String,System.String,System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t EString_EReplace_m2109194159_MetadataUsageId;
extern "C"  String_t* EString_EReplace_m2109194159 (Il2CppObject * __this /* static, unused */, String_t* ___mainStr0, String_t* ___startStr1, String_t* ___endStr2, String_t* ___newStr3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EString_EReplace_m2109194159_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	bool V_3 = false;
	{
		String_t* L_0 = ___mainStr0;
		String_t* L_1 = ___startStr1;
		NullCheck(L_0);
		int32_t L_2 = String_IndexOf_m1476794331(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		V_3 = (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_4 = V_3;
		if (L_4)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_5 = ___mainStr0;
		String_t* L_6 = ___newStr3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m138640077(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		___mainStr0 = L_7;
		String_t* L_8 = ___mainStr0;
		V_2 = L_8;
		goto IL_0072;
	}

IL_0022:
	{
		String_t* L_9 = ___mainStr0;
		String_t* L_10 = ___endStr2;
		int32_t L_11 = V_0;
		String_t* L_12 = ___startStr1;
		NullCheck(L_12);
		int32_t L_13 = String_get_Length_m2979997331(L_12, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_14 = String_IndexOf_m1991631068(L_9, L_10, ((int32_t)((int32_t)L_11+(int32_t)L_13)), /*hidden argument*/NULL);
		V_1 = L_14;
		int32_t L_15 = V_1;
		V_3 = (bool)((((int32_t)((((int32_t)L_15) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_16 = V_3;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		String_t* L_17 = ___mainStr0;
		int32_t L_18 = V_0;
		NullCheck(L_17);
		String_t* L_19 = String_Remove_m2345367442(L_17, L_18, /*hidden argument*/NULL);
		___mainStr0 = L_19;
		String_t* L_20 = ___mainStr0;
		String_t* L_21 = ___newStr3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m138640077(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		___mainStr0 = L_22;
		String_t* L_23 = ___mainStr0;
		V_2 = L_23;
		goto IL_0072;
	}

IL_0054:
	{
		String_t* L_24 = ___mainStr0;
		int32_t L_25 = V_0;
		int32_t L_26 = V_1;
		int32_t L_27 = V_0;
		NullCheck(L_24);
		String_t* L_28 = String_Remove_m242090629(L_24, L_25, ((int32_t)((int32_t)((int32_t)((int32_t)L_26-(int32_t)L_27))+(int32_t)1)), /*hidden argument*/NULL);
		___mainStr0 = L_28;
		String_t* L_29 = ___mainStr0;
		int32_t L_30 = V_0;
		String_t* L_31 = ___newStr3;
		NullCheck(L_29);
		String_t* L_32 = String_Insert_m3926397187(L_29, L_30, L_31, /*hidden argument*/NULL);
		___mainStr0 = L_32;
		String_t* L_33 = ___mainStr0;
		V_2 = L_33;
		goto IL_0072;
	}

IL_0072:
	{
		String_t* L_34 = V_2;
		return L_34;
	}
}
// System.Int32 EString::ECompareOrdinal(System.String,System.String,System.Char[])
extern "C"  int32_t EString_ECompareOrdinal_m131616597 (Il2CppObject * __this /* static, unused */, String_t* ___strA0, String_t* ___strB1, CharU5BU5D_t3324145743* ___separator2, const MethodInfo* method)
{
	StringU5BU5D_t4054002952* V_0 = NULL;
	StringU5BU5D_t4054002952* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	bool V_5 = false;
	int32_t G_B7_0 = 0;
	{
		String_t* L_0 = ___strA0;
		CharU5BU5D_t3324145743* L_1 = ___separator2;
		NullCheck(L_0);
		StringU5BU5D_t4054002952* L_2 = String_Split_m290179486(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = ___strB1;
		CharU5BU5D_t3324145743* L_4 = ___separator2;
		NullCheck(L_3);
		StringU5BU5D_t4054002952* L_5 = String_Split_m290179486(L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		V_2 = 0;
		goto IL_003d;
	}

IL_0015:
	{
		StringU5BU5D_t4054002952* L_6 = V_0;
		int32_t L_7 = V_2;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		String_t* L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		int32_t L_10 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		StringU5BU5D_t4054002952* L_11 = V_1;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		String_t* L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		int32_t L_15 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		V_3 = ((int32_t)((int32_t)L_10-(int32_t)L_15));
		int32_t L_16 = V_3;
		V_5 = (bool)((((int32_t)L_16) == ((int32_t)0))? 1 : 0);
		bool L_17 = V_5;
		if (L_17)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_18 = V_3;
		V_4 = L_18;
		goto IL_005e;
	}

IL_0038:
	{
		int32_t L_19 = V_2;
		V_2 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_003d:
	{
		int32_t L_20 = V_2;
		StringU5BU5D_t4054002952* L_21 = V_0;
		NullCheck(L_21);
		if ((((int32_t)L_20) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_004b;
		}
	}
	{
		int32_t L_22 = V_2;
		StringU5BU5D_t4054002952* L_23 = V_1;
		NullCheck(L_23);
		G_B7_0 = ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length))))))? 1 : 0);
		goto IL_004c;
	}

IL_004b:
	{
		G_B7_0 = 0;
	}

IL_004c:
	{
		V_5 = (bool)G_B7_0;
		bool L_24 = V_5;
		if (L_24)
		{
			goto IL_0015;
		}
	}
	{
		StringU5BU5D_t4054002952* L_25 = V_0;
		NullCheck(L_25);
		StringU5BU5D_t4054002952* L_26 = V_1;
		NullCheck(L_26);
		V_4 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_25)->max_length))))-(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length))))));
		goto IL_005e;
	}

IL_005e:
	{
		int32_t L_27 = V_4;
		return L_27;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
