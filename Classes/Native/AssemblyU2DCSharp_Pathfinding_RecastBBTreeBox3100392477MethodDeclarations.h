﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RecastBBTreeBox
struct RecastBBTreeBox_t3100392477;
// Pathfinding.RecastBBTree
struct RecastBBTree_t4266206694;
// Pathfinding.RecastMeshObj
struct RecastMeshObj_t2069195738;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_RecastBBTree4266206694.h"
#include "AssemblyU2DCSharp_Pathfinding_RecastMeshObj2069195738.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_RecastBBTreeBox3100392477.h"

// System.Void Pathfinding.RecastBBTreeBox::.ctor(Pathfinding.RecastBBTree,Pathfinding.RecastMeshObj)
extern "C"  void RecastBBTreeBox__ctor_m1190083838 (RecastBBTreeBox_t3100392477 * __this, RecastBBTree_t4266206694 * ___tree0, RecastMeshObj_t2069195738 * ___mesh1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastBBTreeBox::Contains(UnityEngine.Vector3)
extern "C"  bool RecastBBTreeBox_Contains_m3027580612 (RecastBBTreeBox_t3100392477 * __this, Vector3_t4282066566  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastBBTreeBox::WriteChildren(System.Int32)
extern "C"  void RecastBBTreeBox_WriteChildren_m2324839847 (RecastBBTreeBox_t3100392477 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastBBTreeBox::ilo_WriteChildren1(Pathfinding.RecastBBTreeBox,System.Int32)
extern "C"  void RecastBBTreeBox_ilo_WriteChildren1_m4038299686 (Il2CppObject * __this /* static, unused */, RecastBBTreeBox_t3100392477 * ____this0, int32_t ___level1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
