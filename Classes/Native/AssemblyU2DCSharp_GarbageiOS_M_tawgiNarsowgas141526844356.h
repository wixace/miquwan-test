﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_tawgiNarsowgas14
struct  M_tawgiNarsowgas14_t1526844356  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_tawgiNarsowgas14::_beaseaJaimo
	bool ____beaseaJaimo_0;
	// System.UInt32 GarbageiOS.M_tawgiNarsowgas14::_visresaXearsaljou
	uint32_t ____visresaXearsaljou_1;
	// System.Single GarbageiOS.M_tawgiNarsowgas14::_nomiHarwe
	float ____nomiHarwe_2;
	// System.Single GarbageiOS.M_tawgiNarsowgas14::_deporcea
	float ____deporcea_3;
	// System.Boolean GarbageiOS.M_tawgiNarsowgas14::_rixaidemKuxafai
	bool ____rixaidemKuxafai_4;
	// System.Single GarbageiOS.M_tawgiNarsowgas14::_beepaljasMerejar
	float ____beepaljasMerejar_5;
	// System.Int32 GarbageiOS.M_tawgiNarsowgas14::_mairfirHozea
	int32_t ____mairfirHozea_6;
	// System.UInt32 GarbageiOS.M_tawgiNarsowgas14::_wasdazawLirlirmai
	uint32_t ____wasdazawLirlirmai_7;
	// System.String GarbageiOS.M_tawgiNarsowgas14::_jurpiStaberi
	String_t* ____jurpiStaberi_8;
	// System.UInt32 GarbageiOS.M_tawgiNarsowgas14::_wearletorXaywow
	uint32_t ____wearletorXaywow_9;
	// System.Single GarbageiOS.M_tawgiNarsowgas14::_dasjaceBawwhearrer
	float ____dasjaceBawwhearrer_10;
	// System.String GarbageiOS.M_tawgiNarsowgas14::_cajisChashorkou
	String_t* ____cajisChashorkou_11;
	// System.Int32 GarbageiOS.M_tawgiNarsowgas14::_falpetiCisme
	int32_t ____falpetiCisme_12;
	// System.Boolean GarbageiOS.M_tawgiNarsowgas14::_seardeera
	bool ____seardeera_13;
	// System.Int32 GarbageiOS.M_tawgiNarsowgas14::_ballpoDile
	int32_t ____ballpoDile_14;
	// System.String GarbageiOS.M_tawgiNarsowgas14::_qousirnou
	String_t* ____qousirnou_15;
	// System.Single GarbageiOS.M_tawgiNarsowgas14::_lerbetrisCercou
	float ____lerbetrisCercou_16;
	// System.UInt32 GarbageiOS.M_tawgiNarsowgas14::_nawlair
	uint32_t ____nawlair_17;
	// System.Int32 GarbageiOS.M_tawgiNarsowgas14::_cemdiWhubalzu
	int32_t ____cemdiWhubalzu_18;
	// System.Int32 GarbageiOS.M_tawgiNarsowgas14::_kasoSoraytra
	int32_t ____kasoSoraytra_19;
	// System.UInt32 GarbageiOS.M_tawgiNarsowgas14::_choojagee
	uint32_t ____choojagee_20;
	// System.String GarbageiOS.M_tawgiNarsowgas14::_bemraDraydabi
	String_t* ____bemraDraydabi_21;
	// System.Single GarbageiOS.M_tawgiNarsowgas14::_miwemta
	float ____miwemta_22;
	// System.Int32 GarbageiOS.M_tawgiNarsowgas14::_mairrelPegere
	int32_t ____mairrelPegere_23;
	// System.Single GarbageiOS.M_tawgiNarsowgas14::_payqistay
	float ____payqistay_24;
	// System.UInt32 GarbageiOS.M_tawgiNarsowgas14::_korjeDearcou
	uint32_t ____korjeDearcou_25;
	// System.String GarbageiOS.M_tawgiNarsowgas14::_cirusayPaqe
	String_t* ____cirusayPaqe_26;
	// System.UInt32 GarbageiOS.M_tawgiNarsowgas14::_surcumairMowniger
	uint32_t ____surcumairMowniger_27;
	// System.UInt32 GarbageiOS.M_tawgiNarsowgas14::_gasherelaMalbear
	uint32_t ____gasherelaMalbear_28;
	// System.String GarbageiOS.M_tawgiNarsowgas14::_qooseedru
	String_t* ____qooseedru_29;

public:
	inline static int32_t get_offset_of__beaseaJaimo_0() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____beaseaJaimo_0)); }
	inline bool get__beaseaJaimo_0() const { return ____beaseaJaimo_0; }
	inline bool* get_address_of__beaseaJaimo_0() { return &____beaseaJaimo_0; }
	inline void set__beaseaJaimo_0(bool value)
	{
		____beaseaJaimo_0 = value;
	}

	inline static int32_t get_offset_of__visresaXearsaljou_1() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____visresaXearsaljou_1)); }
	inline uint32_t get__visresaXearsaljou_1() const { return ____visresaXearsaljou_1; }
	inline uint32_t* get_address_of__visresaXearsaljou_1() { return &____visresaXearsaljou_1; }
	inline void set__visresaXearsaljou_1(uint32_t value)
	{
		____visresaXearsaljou_1 = value;
	}

	inline static int32_t get_offset_of__nomiHarwe_2() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____nomiHarwe_2)); }
	inline float get__nomiHarwe_2() const { return ____nomiHarwe_2; }
	inline float* get_address_of__nomiHarwe_2() { return &____nomiHarwe_2; }
	inline void set__nomiHarwe_2(float value)
	{
		____nomiHarwe_2 = value;
	}

	inline static int32_t get_offset_of__deporcea_3() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____deporcea_3)); }
	inline float get__deporcea_3() const { return ____deporcea_3; }
	inline float* get_address_of__deporcea_3() { return &____deporcea_3; }
	inline void set__deporcea_3(float value)
	{
		____deporcea_3 = value;
	}

	inline static int32_t get_offset_of__rixaidemKuxafai_4() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____rixaidemKuxafai_4)); }
	inline bool get__rixaidemKuxafai_4() const { return ____rixaidemKuxafai_4; }
	inline bool* get_address_of__rixaidemKuxafai_4() { return &____rixaidemKuxafai_4; }
	inline void set__rixaidemKuxafai_4(bool value)
	{
		____rixaidemKuxafai_4 = value;
	}

	inline static int32_t get_offset_of__beepaljasMerejar_5() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____beepaljasMerejar_5)); }
	inline float get__beepaljasMerejar_5() const { return ____beepaljasMerejar_5; }
	inline float* get_address_of__beepaljasMerejar_5() { return &____beepaljasMerejar_5; }
	inline void set__beepaljasMerejar_5(float value)
	{
		____beepaljasMerejar_5 = value;
	}

	inline static int32_t get_offset_of__mairfirHozea_6() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____mairfirHozea_6)); }
	inline int32_t get__mairfirHozea_6() const { return ____mairfirHozea_6; }
	inline int32_t* get_address_of__mairfirHozea_6() { return &____mairfirHozea_6; }
	inline void set__mairfirHozea_6(int32_t value)
	{
		____mairfirHozea_6 = value;
	}

	inline static int32_t get_offset_of__wasdazawLirlirmai_7() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____wasdazawLirlirmai_7)); }
	inline uint32_t get__wasdazawLirlirmai_7() const { return ____wasdazawLirlirmai_7; }
	inline uint32_t* get_address_of__wasdazawLirlirmai_7() { return &____wasdazawLirlirmai_7; }
	inline void set__wasdazawLirlirmai_7(uint32_t value)
	{
		____wasdazawLirlirmai_7 = value;
	}

	inline static int32_t get_offset_of__jurpiStaberi_8() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____jurpiStaberi_8)); }
	inline String_t* get__jurpiStaberi_8() const { return ____jurpiStaberi_8; }
	inline String_t** get_address_of__jurpiStaberi_8() { return &____jurpiStaberi_8; }
	inline void set__jurpiStaberi_8(String_t* value)
	{
		____jurpiStaberi_8 = value;
		Il2CppCodeGenWriteBarrier(&____jurpiStaberi_8, value);
	}

	inline static int32_t get_offset_of__wearletorXaywow_9() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____wearletorXaywow_9)); }
	inline uint32_t get__wearletorXaywow_9() const { return ____wearletorXaywow_9; }
	inline uint32_t* get_address_of__wearletorXaywow_9() { return &____wearletorXaywow_9; }
	inline void set__wearletorXaywow_9(uint32_t value)
	{
		____wearletorXaywow_9 = value;
	}

	inline static int32_t get_offset_of__dasjaceBawwhearrer_10() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____dasjaceBawwhearrer_10)); }
	inline float get__dasjaceBawwhearrer_10() const { return ____dasjaceBawwhearrer_10; }
	inline float* get_address_of__dasjaceBawwhearrer_10() { return &____dasjaceBawwhearrer_10; }
	inline void set__dasjaceBawwhearrer_10(float value)
	{
		____dasjaceBawwhearrer_10 = value;
	}

	inline static int32_t get_offset_of__cajisChashorkou_11() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____cajisChashorkou_11)); }
	inline String_t* get__cajisChashorkou_11() const { return ____cajisChashorkou_11; }
	inline String_t** get_address_of__cajisChashorkou_11() { return &____cajisChashorkou_11; }
	inline void set__cajisChashorkou_11(String_t* value)
	{
		____cajisChashorkou_11 = value;
		Il2CppCodeGenWriteBarrier(&____cajisChashorkou_11, value);
	}

	inline static int32_t get_offset_of__falpetiCisme_12() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____falpetiCisme_12)); }
	inline int32_t get__falpetiCisme_12() const { return ____falpetiCisme_12; }
	inline int32_t* get_address_of__falpetiCisme_12() { return &____falpetiCisme_12; }
	inline void set__falpetiCisme_12(int32_t value)
	{
		____falpetiCisme_12 = value;
	}

	inline static int32_t get_offset_of__seardeera_13() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____seardeera_13)); }
	inline bool get__seardeera_13() const { return ____seardeera_13; }
	inline bool* get_address_of__seardeera_13() { return &____seardeera_13; }
	inline void set__seardeera_13(bool value)
	{
		____seardeera_13 = value;
	}

	inline static int32_t get_offset_of__ballpoDile_14() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____ballpoDile_14)); }
	inline int32_t get__ballpoDile_14() const { return ____ballpoDile_14; }
	inline int32_t* get_address_of__ballpoDile_14() { return &____ballpoDile_14; }
	inline void set__ballpoDile_14(int32_t value)
	{
		____ballpoDile_14 = value;
	}

	inline static int32_t get_offset_of__qousirnou_15() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____qousirnou_15)); }
	inline String_t* get__qousirnou_15() const { return ____qousirnou_15; }
	inline String_t** get_address_of__qousirnou_15() { return &____qousirnou_15; }
	inline void set__qousirnou_15(String_t* value)
	{
		____qousirnou_15 = value;
		Il2CppCodeGenWriteBarrier(&____qousirnou_15, value);
	}

	inline static int32_t get_offset_of__lerbetrisCercou_16() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____lerbetrisCercou_16)); }
	inline float get__lerbetrisCercou_16() const { return ____lerbetrisCercou_16; }
	inline float* get_address_of__lerbetrisCercou_16() { return &____lerbetrisCercou_16; }
	inline void set__lerbetrisCercou_16(float value)
	{
		____lerbetrisCercou_16 = value;
	}

	inline static int32_t get_offset_of__nawlair_17() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____nawlair_17)); }
	inline uint32_t get__nawlair_17() const { return ____nawlair_17; }
	inline uint32_t* get_address_of__nawlair_17() { return &____nawlair_17; }
	inline void set__nawlair_17(uint32_t value)
	{
		____nawlair_17 = value;
	}

	inline static int32_t get_offset_of__cemdiWhubalzu_18() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____cemdiWhubalzu_18)); }
	inline int32_t get__cemdiWhubalzu_18() const { return ____cemdiWhubalzu_18; }
	inline int32_t* get_address_of__cemdiWhubalzu_18() { return &____cemdiWhubalzu_18; }
	inline void set__cemdiWhubalzu_18(int32_t value)
	{
		____cemdiWhubalzu_18 = value;
	}

	inline static int32_t get_offset_of__kasoSoraytra_19() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____kasoSoraytra_19)); }
	inline int32_t get__kasoSoraytra_19() const { return ____kasoSoraytra_19; }
	inline int32_t* get_address_of__kasoSoraytra_19() { return &____kasoSoraytra_19; }
	inline void set__kasoSoraytra_19(int32_t value)
	{
		____kasoSoraytra_19 = value;
	}

	inline static int32_t get_offset_of__choojagee_20() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____choojagee_20)); }
	inline uint32_t get__choojagee_20() const { return ____choojagee_20; }
	inline uint32_t* get_address_of__choojagee_20() { return &____choojagee_20; }
	inline void set__choojagee_20(uint32_t value)
	{
		____choojagee_20 = value;
	}

	inline static int32_t get_offset_of__bemraDraydabi_21() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____bemraDraydabi_21)); }
	inline String_t* get__bemraDraydabi_21() const { return ____bemraDraydabi_21; }
	inline String_t** get_address_of__bemraDraydabi_21() { return &____bemraDraydabi_21; }
	inline void set__bemraDraydabi_21(String_t* value)
	{
		____bemraDraydabi_21 = value;
		Il2CppCodeGenWriteBarrier(&____bemraDraydabi_21, value);
	}

	inline static int32_t get_offset_of__miwemta_22() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____miwemta_22)); }
	inline float get__miwemta_22() const { return ____miwemta_22; }
	inline float* get_address_of__miwemta_22() { return &____miwemta_22; }
	inline void set__miwemta_22(float value)
	{
		____miwemta_22 = value;
	}

	inline static int32_t get_offset_of__mairrelPegere_23() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____mairrelPegere_23)); }
	inline int32_t get__mairrelPegere_23() const { return ____mairrelPegere_23; }
	inline int32_t* get_address_of__mairrelPegere_23() { return &____mairrelPegere_23; }
	inline void set__mairrelPegere_23(int32_t value)
	{
		____mairrelPegere_23 = value;
	}

	inline static int32_t get_offset_of__payqistay_24() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____payqistay_24)); }
	inline float get__payqistay_24() const { return ____payqistay_24; }
	inline float* get_address_of__payqistay_24() { return &____payqistay_24; }
	inline void set__payqistay_24(float value)
	{
		____payqistay_24 = value;
	}

	inline static int32_t get_offset_of__korjeDearcou_25() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____korjeDearcou_25)); }
	inline uint32_t get__korjeDearcou_25() const { return ____korjeDearcou_25; }
	inline uint32_t* get_address_of__korjeDearcou_25() { return &____korjeDearcou_25; }
	inline void set__korjeDearcou_25(uint32_t value)
	{
		____korjeDearcou_25 = value;
	}

	inline static int32_t get_offset_of__cirusayPaqe_26() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____cirusayPaqe_26)); }
	inline String_t* get__cirusayPaqe_26() const { return ____cirusayPaqe_26; }
	inline String_t** get_address_of__cirusayPaqe_26() { return &____cirusayPaqe_26; }
	inline void set__cirusayPaqe_26(String_t* value)
	{
		____cirusayPaqe_26 = value;
		Il2CppCodeGenWriteBarrier(&____cirusayPaqe_26, value);
	}

	inline static int32_t get_offset_of__surcumairMowniger_27() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____surcumairMowniger_27)); }
	inline uint32_t get__surcumairMowniger_27() const { return ____surcumairMowniger_27; }
	inline uint32_t* get_address_of__surcumairMowniger_27() { return &____surcumairMowniger_27; }
	inline void set__surcumairMowniger_27(uint32_t value)
	{
		____surcumairMowniger_27 = value;
	}

	inline static int32_t get_offset_of__gasherelaMalbear_28() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____gasherelaMalbear_28)); }
	inline uint32_t get__gasherelaMalbear_28() const { return ____gasherelaMalbear_28; }
	inline uint32_t* get_address_of__gasherelaMalbear_28() { return &____gasherelaMalbear_28; }
	inline void set__gasherelaMalbear_28(uint32_t value)
	{
		____gasherelaMalbear_28 = value;
	}

	inline static int32_t get_offset_of__qooseedru_29() { return static_cast<int32_t>(offsetof(M_tawgiNarsowgas14_t1526844356, ____qooseedru_29)); }
	inline String_t* get__qooseedru_29() const { return ____qooseedru_29; }
	inline String_t** get_address_of__qooseedru_29() { return &____qooseedru_29; }
	inline void set__qooseedru_29(String_t* value)
	{
		____qooseedru_29 = value;
		Il2CppCodeGenWriteBarrier(&____qooseedru_29, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
