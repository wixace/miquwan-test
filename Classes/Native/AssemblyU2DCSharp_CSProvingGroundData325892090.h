﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CSProvingGroundsHeroData[]
struct CSProvingGroundsHeroDataU5BU5D_t1350178142;
// CSPVPRobotNPC[]
struct CSPVPRobotNPCU5BU5D_t1447782092;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSProvingGroundData
struct  CSProvingGroundData_t325892090  : public Il2CppObject
{
public:
	// CSProvingGroundsHeroData[] CSProvingGroundData::heroRealInfos
	CSProvingGroundsHeroDataU5BU5D_t1350178142* ___heroRealInfos_0;
	// CSPVPRobotNPC[] CSProvingGroundData::robotNpcs
	CSPVPRobotNPCU5BU5D_t1447782092* ___robotNpcs_1;

public:
	inline static int32_t get_offset_of_heroRealInfos_0() { return static_cast<int32_t>(offsetof(CSProvingGroundData_t325892090, ___heroRealInfos_0)); }
	inline CSProvingGroundsHeroDataU5BU5D_t1350178142* get_heroRealInfos_0() const { return ___heroRealInfos_0; }
	inline CSProvingGroundsHeroDataU5BU5D_t1350178142** get_address_of_heroRealInfos_0() { return &___heroRealInfos_0; }
	inline void set_heroRealInfos_0(CSProvingGroundsHeroDataU5BU5D_t1350178142* value)
	{
		___heroRealInfos_0 = value;
		Il2CppCodeGenWriteBarrier(&___heroRealInfos_0, value);
	}

	inline static int32_t get_offset_of_robotNpcs_1() { return static_cast<int32_t>(offsetof(CSProvingGroundData_t325892090, ___robotNpcs_1)); }
	inline CSPVPRobotNPCU5BU5D_t1447782092* get_robotNpcs_1() const { return ___robotNpcs_1; }
	inline CSPVPRobotNPCU5BU5D_t1447782092** get_address_of_robotNpcs_1() { return &___robotNpcs_1; }
	inline void set_robotNpcs_1(CSPVPRobotNPCU5BU5D_t1447782092* value)
	{
		___robotNpcs_1 = value;
		Il2CppCodeGenWriteBarrier(&___robotNpcs_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
