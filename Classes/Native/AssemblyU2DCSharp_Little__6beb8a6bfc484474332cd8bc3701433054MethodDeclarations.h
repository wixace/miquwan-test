﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._6beb8a6bfc484474332cd8bc8af6668b
struct _6beb8a6bfc484474332cd8bc8af6668b_t3701433054;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._6beb8a6bfc484474332cd8bc8af6668b::.ctor()
extern "C"  void _6beb8a6bfc484474332cd8bc8af6668b__ctor_m2533498127 (_6beb8a6bfc484474332cd8bc8af6668b_t3701433054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6beb8a6bfc484474332cd8bc8af6668b::_6beb8a6bfc484474332cd8bc8af6668bm2(System.Int32)
extern "C"  int32_t _6beb8a6bfc484474332cd8bc8af6668b__6beb8a6bfc484474332cd8bc8af6668bm2_m2472031449 (_6beb8a6bfc484474332cd8bc8af6668b_t3701433054 * __this, int32_t ____6beb8a6bfc484474332cd8bc8af6668ba0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6beb8a6bfc484474332cd8bc8af6668b::_6beb8a6bfc484474332cd8bc8af6668bm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _6beb8a6bfc484474332cd8bc8af6668b__6beb8a6bfc484474332cd8bc8af6668bm_m1505663485 (_6beb8a6bfc484474332cd8bc8af6668b_t3701433054 * __this, int32_t ____6beb8a6bfc484474332cd8bc8af6668ba0, int32_t ____6beb8a6bfc484474332cd8bc8af6668b981, int32_t ____6beb8a6bfc484474332cd8bc8af6668bc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
