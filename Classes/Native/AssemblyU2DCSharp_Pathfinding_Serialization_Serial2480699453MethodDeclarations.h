﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Serialization.SerializeSettings
struct SerializeSettings_t2480699453;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Serialization.SerializeSettings::.ctor()
extern "C"  void SerializeSettings__ctor_m1127773104 (SerializeSettings_t2480699453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Serialization.SerializeSettings Pathfinding.Serialization.SerializeSettings::get_Settings()
extern "C"  SerializeSettings_t2480699453 * SerializeSettings_get_Settings_m1821024424 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Serialization.SerializeSettings Pathfinding.Serialization.SerializeSettings::get_All()
extern "C"  SerializeSettings_t2480699453 * SerializeSettings_get_All_m922985374 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
