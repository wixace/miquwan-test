﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EventDelegateGenerated
struct EventDelegateGenerated_t2543977008;
// JSVCall
struct JSVCall_t3708497963;
// EventDelegate/Callback
struct Callback_t1094463061;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t667441552;
// EventDelegate
struct EventDelegate_t4004424223;
// EventDelegate/Parameter[]
struct ParameterU5BU5D_t206664164;
// System.String
struct String_t;
// System.Delegate
struct Delegate_t3310234105;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t1077642479;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_EventDelegate4004424223.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"

// System.Void EventDelegateGenerated::.ctor()
extern "C"  void EventDelegateGenerated__ctor_m3728844907 (EventDelegateGenerated_t2543977008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::EventDelegate_EventDelegate1(JSVCall,System.Int32)
extern "C"  bool EventDelegateGenerated_EventDelegate_EventDelegate1_m1973784495 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Callback EventDelegateGenerated::EventDelegate__ctor_GetDelegate_member1_arg0(CSRepresentedObject)
extern "C"  Callback_t1094463061 * EventDelegateGenerated_EventDelegate__ctor_GetDelegate_member1_arg0_m1619082377 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::EventDelegate_EventDelegate2(JSVCall,System.Int32)
extern "C"  bool EventDelegateGenerated_EventDelegate_EventDelegate2_m3218548976 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::EventDelegate_EventDelegate3(JSVCall,System.Int32)
extern "C"  bool EventDelegateGenerated_EventDelegate_EventDelegate3_m168346161 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventDelegateGenerated::EventDelegate_oneShot(JSVCall)
extern "C"  void EventDelegateGenerated_EventDelegate_oneShot_m1707364222 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventDelegateGenerated::EventDelegate_target(JSVCall)
extern "C"  void EventDelegateGenerated_EventDelegate_target_m2006504349 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventDelegateGenerated::EventDelegate_methodName(JSVCall)
extern "C"  void EventDelegateGenerated_EventDelegate_methodName_m753489026 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventDelegateGenerated::EventDelegate_parameters(JSVCall)
extern "C"  void EventDelegateGenerated_EventDelegate_parameters_m2955694948 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventDelegateGenerated::EventDelegate_isValid(JSVCall)
extern "C"  void EventDelegateGenerated_EventDelegate_isValid_m161549644 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventDelegateGenerated::EventDelegate_isEnabled(JSVCall)
extern "C"  void EventDelegateGenerated_EventDelegate_isEnabled_m2075649831 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::EventDelegate_Clear(JSVCall,System.Int32)
extern "C"  bool EventDelegateGenerated_EventDelegate_Clear_m2656153554 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::EventDelegate_Equals__Object(JSVCall,System.Int32)
extern "C"  bool EventDelegateGenerated_EventDelegate_Equals__Object_m1305287899 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::EventDelegate_Execute(JSVCall,System.Int32)
extern "C"  bool EventDelegateGenerated_EventDelegate_Execute_m4246529530 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::EventDelegate_Set__MonoBehaviour__String(JSVCall,System.Int32)
extern "C"  bool EventDelegateGenerated_EventDelegate_Set__MonoBehaviour__String_m3968460070 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::EventDelegate_ToString(JSVCall,System.Int32)
extern "C"  bool EventDelegateGenerated_EventDelegate_ToString_m4012764617 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Callback EventDelegateGenerated::EventDelegate_Add_GetDelegate_member5_arg1(CSRepresentedObject)
extern "C"  Callback_t1094463061 * EventDelegateGenerated_EventDelegate_Add_GetDelegate_member5_arg1_m2336982842 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::EventDelegate_Add__ListT1_EventDelegate__Callback__Boolean(JSVCall,System.Int32)
extern "C"  bool EventDelegateGenerated_EventDelegate_Add__ListT1_EventDelegate__Callback__Boolean_m1687324900 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::EventDelegate_Add__ListT1_EventDelegate__EventDelegate__Boolean(JSVCall,System.Int32)
extern "C"  bool EventDelegateGenerated_EventDelegate_Add__ListT1_EventDelegate__EventDelegate__Boolean_m2377544842 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Callback EventDelegateGenerated::EventDelegate_Add_GetDelegate_member7_arg1(CSRepresentedObject)
extern "C"  Callback_t1094463061 * EventDelegateGenerated_EventDelegate_Add_GetDelegate_member7_arg1_m4260210876 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::EventDelegate_Add__ListT1_EventDelegate__Callback(JSVCall,System.Int32)
extern "C"  bool EventDelegateGenerated_EventDelegate_Add__ListT1_EventDelegate__Callback_m2727009030 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::EventDelegate_Add__ListT1_EventDelegate__EventDelegate(JSVCall,System.Int32)
extern "C"  bool EventDelegateGenerated_EventDelegate_Add__ListT1_EventDelegate__EventDelegate_m2326666400 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::EventDelegate_Execute__ListT1_EventDelegate(JSVCall,System.Int32)
extern "C"  bool EventDelegateGenerated_EventDelegate_Execute__ListT1_EventDelegate_m189382485 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::EventDelegate_IsValid__ListT1_EventDelegate(JSVCall,System.Int32)
extern "C"  bool EventDelegateGenerated_EventDelegate_IsValid__ListT1_EventDelegate_m1081179474 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::EventDelegate_Remove__ListT1_EventDelegate__EventDelegate(JSVCall,System.Int32)
extern "C"  bool EventDelegateGenerated_EventDelegate_Remove__ListT1_EventDelegate__EventDelegate_m985393349 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Callback EventDelegateGenerated::EventDelegate_Remove_GetDelegate_member12_arg1(CSRepresentedObject)
extern "C"  Callback_t1094463061 * EventDelegateGenerated_EventDelegate_Remove_GetDelegate_member12_arg1_m1629450523 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::EventDelegate_Remove__ListT1_EventDelegate__Callback(JSVCall,System.Int32)
extern "C"  bool EventDelegateGenerated_EventDelegate_Remove__ListT1_EventDelegate__Callback_m731728833 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Callback EventDelegateGenerated::EventDelegate_Set_GetDelegate_member13_arg1(CSRepresentedObject)
extern "C"  Callback_t1094463061 * EventDelegateGenerated_EventDelegate_Set_GetDelegate_member13_arg1_m1639286782 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::EventDelegate_Set__ListT1_EventDelegate__Callback(JSVCall,System.Int32)
extern "C"  bool EventDelegateGenerated_EventDelegate_Set__ListT1_EventDelegate__Callback_m3715048455 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::EventDelegate_Set__ListT1_EventDelegate__EventDelegate(JSVCall,System.Int32)
extern "C"  bool EventDelegateGenerated_EventDelegate_Set__ListT1_EventDelegate__EventDelegate_m298077247 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventDelegateGenerated::__Register()
extern "C"  void EventDelegateGenerated___Register_m2898222908 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Callback EventDelegateGenerated::<EventDelegate_EventDelegate2>m__45()
extern "C"  Callback_t1094463061 * EventDelegateGenerated_U3CEventDelegate_EventDelegate2U3Em__45_m3211260544 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Callback EventDelegateGenerated::<EventDelegate_Add__ListT1_EventDelegate__Callback__Boolean>m__47()
extern "C"  Callback_t1094463061 * EventDelegateGenerated_U3CEventDelegate_Add__ListT1_EventDelegate__Callback__BooleanU3Em__47_m820984054 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Callback EventDelegateGenerated::<EventDelegate_Add__ListT1_EventDelegate__Callback>m__49()
extern "C"  Callback_t1094463061 * EventDelegateGenerated_U3CEventDelegate_Add__ListT1_EventDelegate__CallbackU3Em__49_m1246475002 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Callback EventDelegateGenerated::<EventDelegate_Remove__ListT1_EventDelegate__Callback>m__4B()
extern "C"  Callback_t1094463061 * EventDelegateGenerated_U3CEventDelegate_Remove__ListT1_EventDelegate__CallbackU3Em__4B_m2532927454 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Callback EventDelegateGenerated::<EventDelegate_Set__ListT1_EventDelegate__Callback>m__4D()
extern "C"  Callback_t1094463061 * EventDelegateGenerated_U3CEventDelegate_Set__ListT1_EventDelegate__CallbackU3Em__4D_m978160390 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool EventDelegateGenerated_ilo_attachFinalizerObject1_m3810161108 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EventDelegateGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * EventDelegateGenerated_ilo_getObject2_m3201682277 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventDelegateGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void EventDelegateGenerated_ilo_setBooleanS3_m483637722 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MonoBehaviour EventDelegateGenerated::ilo_get_target4(EventDelegate)
extern "C"  MonoBehaviour_t667441552 * EventDelegateGenerated_ilo_get_target4_m445016627 (Il2CppObject * __this /* static, unused */, EventDelegate_t4004424223 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventDelegateGenerated::ilo_set_target5(EventDelegate,UnityEngine.MonoBehaviour)
extern "C"  void EventDelegateGenerated_ilo_set_target5_m2010317855 (Il2CppObject * __this /* static, unused */, EventDelegate_t4004424223 * ____this0, MonoBehaviour_t667441552 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Parameter[] EventDelegateGenerated::ilo_get_parameters6(EventDelegate)
extern "C"  ParameterU5BU5D_t206664164* EventDelegateGenerated_ilo_get_parameters6_m590994386 (Il2CppObject * __this /* static, unused */, EventDelegate_t4004424223 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventDelegateGenerated::ilo_Clear7(EventDelegate)
extern "C"  void EventDelegateGenerated_ilo_Clear7_m2814562685 (Il2CppObject * __this /* static, unused */, EventDelegate_t4004424223 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::ilo_Equals8(EventDelegate,System.Object)
extern "C"  bool EventDelegateGenerated_ilo_Equals8_m1107964294 (Il2CppObject * __this /* static, unused */, EventDelegate_t4004424223 * ____this0, Il2CppObject * ___obj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventDelegateGenerated::ilo_setStringS9(System.Int32,System.String)
extern "C"  void EventDelegateGenerated_ilo_setStringS9_m2485814716 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::ilo_getBooleanS10(System.Int32)
extern "C"  bool EventDelegateGenerated_ilo_getBooleanS10_m466191027 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventDelegateGenerated::ilo_addJSFunCSDelegateRel11(System.Int32,System.Delegate)
extern "C"  void EventDelegateGenerated_ilo_addJSFunCSDelegateRel11_m4100637310 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate EventDelegateGenerated::ilo_Add12(System.Collections.Generic.List`1<EventDelegate>,EventDelegate/Callback)
extern "C"  EventDelegate_t4004424223 * EventDelegateGenerated_ilo_Add12_m428918890 (Il2CppObject * __this /* static, unused */, List_1_t1077642479 * ___list0, Callback_t1094463061 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventDelegateGenerated::ilo_Execute13(System.Collections.Generic.List`1<EventDelegate>)
extern "C"  void EventDelegateGenerated_ilo_Execute13_m3899900912 (Il2CppObject * __this /* static, unused */, List_1_t1077642479 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventDelegateGenerated::ilo_Set14(System.Collections.Generic.List`1<EventDelegate>,EventDelegate)
extern "C"  void EventDelegateGenerated_ilo_Set14_m2513889483 (Il2CppObject * __this /* static, unused */, List_1_t1077642479 * ___list0, EventDelegate_t4004424223 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject EventDelegateGenerated::ilo_getFunctionS15(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * EventDelegateGenerated_ilo_getFunctionS15_m420545987 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EventDelegateGenerated::ilo_isFunctionS16(System.Int32)
extern "C"  bool EventDelegateGenerated_ilo_isFunctionS16_m3778958089 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Callback EventDelegateGenerated::ilo_EventDelegate_Remove_GetDelegate_member12_arg117(CSRepresentedObject)
extern "C"  Callback_t1094463061 * EventDelegateGenerated_ilo_EventDelegate_Remove_GetDelegate_member12_arg117_m748124258 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
