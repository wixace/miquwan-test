﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_surbo237
struct  M_surbo237_t992819113  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_surbo237::_jalo
	uint32_t ____jalo_0;
	// System.Int32 GarbageiOS.M_surbo237::_romeni
	int32_t ____romeni_1;
	// System.String GarbageiOS.M_surbo237::_sapasjaSohea
	String_t* ____sapasjaSohea_2;
	// System.UInt32 GarbageiOS.M_surbo237::_durtelir
	uint32_t ____durtelir_3;
	// System.Int32 GarbageiOS.M_surbo237::_simiwawToopu
	int32_t ____simiwawToopu_4;
	// System.String GarbageiOS.M_surbo237::_cirkeQezur
	String_t* ____cirkeQezur_5;

public:
	inline static int32_t get_offset_of__jalo_0() { return static_cast<int32_t>(offsetof(M_surbo237_t992819113, ____jalo_0)); }
	inline uint32_t get__jalo_0() const { return ____jalo_0; }
	inline uint32_t* get_address_of__jalo_0() { return &____jalo_0; }
	inline void set__jalo_0(uint32_t value)
	{
		____jalo_0 = value;
	}

	inline static int32_t get_offset_of__romeni_1() { return static_cast<int32_t>(offsetof(M_surbo237_t992819113, ____romeni_1)); }
	inline int32_t get__romeni_1() const { return ____romeni_1; }
	inline int32_t* get_address_of__romeni_1() { return &____romeni_1; }
	inline void set__romeni_1(int32_t value)
	{
		____romeni_1 = value;
	}

	inline static int32_t get_offset_of__sapasjaSohea_2() { return static_cast<int32_t>(offsetof(M_surbo237_t992819113, ____sapasjaSohea_2)); }
	inline String_t* get__sapasjaSohea_2() const { return ____sapasjaSohea_2; }
	inline String_t** get_address_of__sapasjaSohea_2() { return &____sapasjaSohea_2; }
	inline void set__sapasjaSohea_2(String_t* value)
	{
		____sapasjaSohea_2 = value;
		Il2CppCodeGenWriteBarrier(&____sapasjaSohea_2, value);
	}

	inline static int32_t get_offset_of__durtelir_3() { return static_cast<int32_t>(offsetof(M_surbo237_t992819113, ____durtelir_3)); }
	inline uint32_t get__durtelir_3() const { return ____durtelir_3; }
	inline uint32_t* get_address_of__durtelir_3() { return &____durtelir_3; }
	inline void set__durtelir_3(uint32_t value)
	{
		____durtelir_3 = value;
	}

	inline static int32_t get_offset_of__simiwawToopu_4() { return static_cast<int32_t>(offsetof(M_surbo237_t992819113, ____simiwawToopu_4)); }
	inline int32_t get__simiwawToopu_4() const { return ____simiwawToopu_4; }
	inline int32_t* get_address_of__simiwawToopu_4() { return &____simiwawToopu_4; }
	inline void set__simiwawToopu_4(int32_t value)
	{
		____simiwawToopu_4 = value;
	}

	inline static int32_t get_offset_of__cirkeQezur_5() { return static_cast<int32_t>(offsetof(M_surbo237_t992819113, ____cirkeQezur_5)); }
	inline String_t* get__cirkeQezur_5() const { return ____cirkeQezur_5; }
	inline String_t** get_address_of__cirkeQezur_5() { return &____cirkeQezur_5; }
	inline void set__cirkeQezur_5(String_t* value)
	{
		____cirkeQezur_5 = value;
		Il2CppCodeGenWriteBarrier(&____cirkeQezur_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
