﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t153068654;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Security.Cryptography.HMACSHA1
struct HMACSHA1_t4024365272;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Encryption.ZipAESTransform
struct  ZipAESTransform_t496812608  : public Il2CppObject
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_blockSize
	int32_t ____blockSize_0;
	// System.Security.Cryptography.ICryptoTransform ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_encryptor
	Il2CppObject * ____encryptor_1;
	// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_counterNonce
	ByteU5BU5D_t4260760469* ____counterNonce_2;
	// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_encryptBuffer
	ByteU5BU5D_t4260760469* ____encryptBuffer_3;
	// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_encrPos
	int32_t ____encrPos_4;
	// System.Security.Cryptography.HMACSHA1 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_hmacsha1
	HMACSHA1_t4024365272 * ____hmacsha1_5;
	// System.Boolean ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_finalised
	bool ____finalised_6;
	// System.Boolean ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::_writeMode
	bool ____writeMode_7;

public:
	inline static int32_t get_offset_of__blockSize_0() { return static_cast<int32_t>(offsetof(ZipAESTransform_t496812608, ____blockSize_0)); }
	inline int32_t get__blockSize_0() const { return ____blockSize_0; }
	inline int32_t* get_address_of__blockSize_0() { return &____blockSize_0; }
	inline void set__blockSize_0(int32_t value)
	{
		____blockSize_0 = value;
	}

	inline static int32_t get_offset_of__encryptor_1() { return static_cast<int32_t>(offsetof(ZipAESTransform_t496812608, ____encryptor_1)); }
	inline Il2CppObject * get__encryptor_1() const { return ____encryptor_1; }
	inline Il2CppObject ** get_address_of__encryptor_1() { return &____encryptor_1; }
	inline void set__encryptor_1(Il2CppObject * value)
	{
		____encryptor_1 = value;
		Il2CppCodeGenWriteBarrier(&____encryptor_1, value);
	}

	inline static int32_t get_offset_of__counterNonce_2() { return static_cast<int32_t>(offsetof(ZipAESTransform_t496812608, ____counterNonce_2)); }
	inline ByteU5BU5D_t4260760469* get__counterNonce_2() const { return ____counterNonce_2; }
	inline ByteU5BU5D_t4260760469** get_address_of__counterNonce_2() { return &____counterNonce_2; }
	inline void set__counterNonce_2(ByteU5BU5D_t4260760469* value)
	{
		____counterNonce_2 = value;
		Il2CppCodeGenWriteBarrier(&____counterNonce_2, value);
	}

	inline static int32_t get_offset_of__encryptBuffer_3() { return static_cast<int32_t>(offsetof(ZipAESTransform_t496812608, ____encryptBuffer_3)); }
	inline ByteU5BU5D_t4260760469* get__encryptBuffer_3() const { return ____encryptBuffer_3; }
	inline ByteU5BU5D_t4260760469** get_address_of__encryptBuffer_3() { return &____encryptBuffer_3; }
	inline void set__encryptBuffer_3(ByteU5BU5D_t4260760469* value)
	{
		____encryptBuffer_3 = value;
		Il2CppCodeGenWriteBarrier(&____encryptBuffer_3, value);
	}

	inline static int32_t get_offset_of__encrPos_4() { return static_cast<int32_t>(offsetof(ZipAESTransform_t496812608, ____encrPos_4)); }
	inline int32_t get__encrPos_4() const { return ____encrPos_4; }
	inline int32_t* get_address_of__encrPos_4() { return &____encrPos_4; }
	inline void set__encrPos_4(int32_t value)
	{
		____encrPos_4 = value;
	}

	inline static int32_t get_offset_of__hmacsha1_5() { return static_cast<int32_t>(offsetof(ZipAESTransform_t496812608, ____hmacsha1_5)); }
	inline HMACSHA1_t4024365272 * get__hmacsha1_5() const { return ____hmacsha1_5; }
	inline HMACSHA1_t4024365272 ** get_address_of__hmacsha1_5() { return &____hmacsha1_5; }
	inline void set__hmacsha1_5(HMACSHA1_t4024365272 * value)
	{
		____hmacsha1_5 = value;
		Il2CppCodeGenWriteBarrier(&____hmacsha1_5, value);
	}

	inline static int32_t get_offset_of__finalised_6() { return static_cast<int32_t>(offsetof(ZipAESTransform_t496812608, ____finalised_6)); }
	inline bool get__finalised_6() const { return ____finalised_6; }
	inline bool* get_address_of__finalised_6() { return &____finalised_6; }
	inline void set__finalised_6(bool value)
	{
		____finalised_6 = value;
	}

	inline static int32_t get_offset_of__writeMode_7() { return static_cast<int32_t>(offsetof(ZipAESTransform_t496812608, ____writeMode_7)); }
	inline bool get__writeMode_7() const { return ____writeMode_7; }
	inline bool* get_address_of__writeMode_7() { return &____writeMode_7; }
	inline void set__writeMode_7(bool value)
	{
		____writeMode_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
