﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3672647722MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,asset_sharedCfg>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m3162443436(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t53524365 *, Dictionary_2_t2721732210 *, const MethodInfo*))KeyCollection__ctor_m3432069128_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,asset_sharedCfg>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1816842026(__this, ___item0, method) ((  void (*) (KeyCollection_t53524365 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3101899854_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,asset_sharedCfg>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3429807905(__this, method) ((  void (*) (KeyCollection_t53524365 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m164109637_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,asset_sharedCfg>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m984044192(__this, ___item0, method) ((  bool (*) (KeyCollection_t53524365 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2402136956_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,asset_sharedCfg>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m904391685(__this, ___item0, method) ((  bool (*) (KeyCollection_t53524365 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1325978593_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,asset_sharedCfg>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3183784605(__this, method) ((  Il2CppObject* (*) (KeyCollection_t53524365 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3150060033_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,asset_sharedCfg>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m3227120147(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t53524365 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2134327863_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,asset_sharedCfg>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m150840014(__this, method) ((  Il2CppObject * (*) (KeyCollection_t53524365 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3067601266_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,asset_sharedCfg>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3654722049(__this, method) ((  bool (*) (KeyCollection_t53524365 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m642268125_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,asset_sharedCfg>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m901384179(__this, method) ((  bool (*) (KeyCollection_t53524365 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1412236495_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,asset_sharedCfg>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1925799327(__this, method) ((  Il2CppObject * (*) (KeyCollection_t53524365 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3575527099_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,asset_sharedCfg>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m2280793441(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t53524365 *, StringU5BU5D_t4054002952*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2803941053_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,asset_sharedCfg>::GetEnumerator()
#define KeyCollection_GetEnumerator_m2902983172(__this, method) ((  Enumerator_t3336668264  (*) (KeyCollection_t53524365 *, const MethodInfo*))KeyCollection_GetEnumerator_m2980864032_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,asset_sharedCfg>::get_Count()
#define KeyCollection_get_Count_m1821862713(__this, method) ((  int32_t (*) (KeyCollection_t53524365 *, const MethodInfo*))KeyCollection_get_Count_m1374340501_gshared)(__this, method)
