﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Int3[]
struct Int3U5BU5D_t516284607;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
struct Int3_t1974045594_marshaled_pinvoke;
struct Int3_t1974045594_marshaled_com;

#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Voxels.VoxelMesh
struct  VoxelMesh_t3943678281 
{
public:
	// Pathfinding.Int3[] Pathfinding.Voxels.VoxelMesh::verts
	Int3U5BU5D_t516284607* ___verts_0;
	// System.Int32[] Pathfinding.Voxels.VoxelMesh::tris
	Int32U5BU5D_t3230847821* ___tris_1;

public:
	inline static int32_t get_offset_of_verts_0() { return static_cast<int32_t>(offsetof(VoxelMesh_t3943678281, ___verts_0)); }
	inline Int3U5BU5D_t516284607* get_verts_0() const { return ___verts_0; }
	inline Int3U5BU5D_t516284607** get_address_of_verts_0() { return &___verts_0; }
	inline void set_verts_0(Int3U5BU5D_t516284607* value)
	{
		___verts_0 = value;
		Il2CppCodeGenWriteBarrier(&___verts_0, value);
	}

	inline static int32_t get_offset_of_tris_1() { return static_cast<int32_t>(offsetof(VoxelMesh_t3943678281, ___tris_1)); }
	inline Int32U5BU5D_t3230847821* get_tris_1() const { return ___tris_1; }
	inline Int32U5BU5D_t3230847821** get_address_of_tris_1() { return &___tris_1; }
	inline void set_tris_1(Int32U5BU5D_t3230847821* value)
	{
		___tris_1 = value;
		Il2CppCodeGenWriteBarrier(&___tris_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Pathfinding.Voxels.VoxelMesh
struct VoxelMesh_t3943678281_marshaled_pinvoke
{
	Int3_t1974045594_marshaled_pinvoke* ___verts_0;
	int32_t* ___tris_1;
};
// Native definition for marshalling of: Pathfinding.Voxels.VoxelMesh
struct VoxelMesh_t3943678281_marshaled_com
{
	Int3_t1974045594_marshaled_com* ___verts_0;
	int32_t* ___tris_1;
};
