﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Vector4>
struct U3CGetEnumeratorU3Ec__Iterator29_t1734063028;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Vector4>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29__ctor_m1230447619_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1734063028 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29__ctor_m1230447619(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t1734063028 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29__ctor_m1230447619_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Vector4>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Vector4_t4282066567  U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2519245356_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1734063028 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2519245356(__this, method) ((  Vector4_t4282066567  (*) (U3CGetEnumeratorU3Ec__Iterator29_t1734063028 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2519245356_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Vector4>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m3454987405_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1734063028 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m3454987405(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator29_t1734063028 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m3454987405_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Vector4>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m1564404729_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1734063028 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m1564404729(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator29_t1734063028 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m1564404729_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Vector4>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29_Dispose_m1242438528_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1734063028 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_Dispose_m1242438528(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t1734063028 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_Dispose_m1242438528_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Vector4>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29_Reset_m3171847856_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1734063028 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_Reset_m3171847856(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t1734063028 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_Reset_m3171847856_gshared)(__this, method)
