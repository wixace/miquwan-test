﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Checksums.Crc32
struct Crc32_t3523361801;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Int64 ICSharpCode.SharpZipLib.Checksums.Crc32::get_Value()
extern "C"  int64_t Crc32_get_Value_m2535696875 (Crc32_t3523361801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::Update(System.Int32)
extern "C"  void Crc32_Update_m3645794406 (Crc32_t3523361801 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::Update(System.Byte[],System.Int32,System.Int32)
extern "C"  void Crc32_Update_m3381543860 (Crc32_t3523361801 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::.ctor()
extern "C"  void Crc32__ctor_m1138481016 (Crc32_t3523361801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::.cctor()
extern "C"  void Crc32__cctor_m451076917 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
