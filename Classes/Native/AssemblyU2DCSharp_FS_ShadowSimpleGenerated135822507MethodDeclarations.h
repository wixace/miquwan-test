﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FS_ShadowSimpleGenerated
struct FS_ShadowSimpleGenerated_t135822507;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void FS_ShadowSimpleGenerated::.ctor()
extern "C"  void FS_ShadowSimpleGenerated__ctor_m3556975184 (FS_ShadowSimpleGenerated_t135822507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FS_ShadowSimpleGenerated::FS_ShadowSimple_FS_ShadowSimple1(JSVCall,System.Int32)
extern "C"  bool FS_ShadowSimpleGenerated_FS_ShadowSimple_FS_ShadowSimple1_m751759520 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimpleGenerated::FS_ShadowSimple_maxProjectionDistance(JSVCall)
extern "C"  void FS_ShadowSimpleGenerated_FS_ShadowSimple_maxProjectionDistance_m460064204 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimpleGenerated::FS_ShadowSimple_girth(JSVCall)
extern "C"  void FS_ShadowSimpleGenerated_FS_ShadowSimple_girth_m3021925104 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimpleGenerated::FS_ShadowSimple_shadowHoverHeight(JSVCall)
extern "C"  void FS_ShadowSimpleGenerated_FS_ShadowSimple_shadowHoverHeight_m2465442513 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimpleGenerated::FS_ShadowSimple_layerMask(JSVCall)
extern "C"  void FS_ShadowSimpleGenerated_FS_ShadowSimple_layerMask_m4174088823 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimpleGenerated::FS_ShadowSimple_shadowMaterial(JSVCall)
extern "C"  void FS_ShadowSimpleGenerated_FS_ShadowSimple_shadowMaterial_m659171217 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimpleGenerated::FS_ShadowSimple_isStatic(JSVCall)
extern "C"  void FS_ShadowSimpleGenerated_FS_ShadowSimple_isStatic_m2674317920 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimpleGenerated::FS_ShadowSimple_useLightSource(JSVCall)
extern "C"  void FS_ShadowSimpleGenerated_FS_ShadowSimple_useLightSource_m3979735182 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimpleGenerated::FS_ShadowSimple_lightSource(JSVCall)
extern "C"  void FS_ShadowSimpleGenerated_FS_ShadowSimple_lightSource_m1412196483 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimpleGenerated::FS_ShadowSimple_lightDirection(JSVCall)
extern "C"  void FS_ShadowSimpleGenerated_FS_ShadowSimple_lightDirection_m463312175 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimpleGenerated::FS_ShadowSimple_isPerspectiveProjection(JSVCall)
extern "C"  void FS_ShadowSimpleGenerated_FS_ShadowSimple_isPerspectiveProjection_m4038916019 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimpleGenerated::FS_ShadowSimple_doVisibilityCulling(JSVCall)
extern "C"  void FS_ShadowSimpleGenerated_FS_ShadowSimple_doVisibilityCulling_m3814962465 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimpleGenerated::FS_ShadowSimple_uvs(JSVCall)
extern "C"  void FS_ShadowSimpleGenerated_FS_ShadowSimple_uvs_m2328118914 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimpleGenerated::FS_ShadowSimple_corners(JSVCall)
extern "C"  void FS_ShadowSimpleGenerated_FS_ShadowSimple_corners_m2884670006 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimpleGenerated::FS_ShadowSimple_color(JSVCall)
extern "C"  void FS_ShadowSimpleGenerated_FS_ShadowSimple_color_m938932401 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimpleGenerated::FS_ShadowSimple_normal(JSVCall)
extern "C"  void FS_ShadowSimpleGenerated_FS_ShadowSimple_normal_m1399000849 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimpleGenerated::__Register()
extern "C"  void FS_ShadowSimpleGenerated___Register_m684149879 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FS_ShadowSimpleGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool FS_ShadowSimpleGenerated_ilo_attachFinalizerObject1_m2872945551 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FS_ShadowSimpleGenerated::ilo_getSingle2(System.Int32)
extern "C"  float FS_ShadowSimpleGenerated_ilo_getSingle2_m929175320 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimpleGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void FS_ShadowSimpleGenerated_ilo_setSingle3_m2711756070 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FS_ShadowSimpleGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t FS_ShadowSimpleGenerated_ilo_setObject4_m227059453 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FS_ShadowSimpleGenerated::ilo_getObject5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * FS_ShadowSimpleGenerated_ilo_getObject5_m4254823587 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FS_ShadowSimpleGenerated::ilo_getBooleanS6(System.Int32)
extern "C"  bool FS_ShadowSimpleGenerated_ilo_getBooleanS6_m1206787009 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimpleGenerated::ilo_setVector3S7(System.Int32,UnityEngine.Vector3)
extern "C"  void FS_ShadowSimpleGenerated_ilo_setVector3S7_m1259667581 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowSimpleGenerated::ilo_setBooleanS8(System.Int32,System.Boolean)
extern "C"  void FS_ShadowSimpleGenerated_ilo_setBooleanS8_m4293851674 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
