﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_TouchGenerated
struct UnityEngine_TouchGenerated_t2901673080;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine_TouchGenerated::.ctor()
extern "C"  void UnityEngine_TouchGenerated__ctor_m894823203 (UnityEngine_TouchGenerated_t2901673080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchGenerated::.cctor()
extern "C"  void UnityEngine_TouchGenerated__cctor_m1487619306 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TouchGenerated::Touch_Touch1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TouchGenerated_Touch_Touch1_m2545618423 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchGenerated::Touch_fingerId(JSVCall)
extern "C"  void UnityEngine_TouchGenerated_Touch_fingerId_m675260898 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchGenerated::Touch_position(JSVCall)
extern "C"  void UnityEngine_TouchGenerated_Touch_position_m1989260477 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchGenerated::Touch_rawPosition(JSVCall)
extern "C"  void UnityEngine_TouchGenerated_Touch_rawPosition_m2444141813 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchGenerated::Touch_deltaPosition(JSVCall)
extern "C"  void UnityEngine_TouchGenerated_Touch_deltaPosition_m1932444517 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchGenerated::Touch_deltaTime(JSVCall)
extern "C"  void UnityEngine_TouchGenerated_Touch_deltaTime_m4125382209 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchGenerated::Touch_tapCount(JSVCall)
extern "C"  void UnityEngine_TouchGenerated_Touch_tapCount_m2807656986 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchGenerated::Touch_phase(JSVCall)
extern "C"  void UnityEngine_TouchGenerated_Touch_phase_m2115036203 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchGenerated::Touch_pressure(JSVCall)
extern "C"  void UnityEngine_TouchGenerated_Touch_pressure_m2581280545 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchGenerated::Touch_maximumPossiblePressure(JSVCall)
extern "C"  void UnityEngine_TouchGenerated_Touch_maximumPossiblePressure_m659439344 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchGenerated::Touch_type(JSVCall)
extern "C"  void UnityEngine_TouchGenerated_Touch_type_m942547404 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchGenerated::Touch_altitudeAngle(JSVCall)
extern "C"  void UnityEngine_TouchGenerated_Touch_altitudeAngle_m4232399637 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchGenerated::Touch_azimuthAngle(JSVCall)
extern "C"  void UnityEngine_TouchGenerated_Touch_azimuthAngle_m2101885151 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchGenerated::Touch_radius(JSVCall)
extern "C"  void UnityEngine_TouchGenerated_Touch_radius_m1361459060 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchGenerated::Touch_radiusVariance(JSVCall)
extern "C"  void UnityEngine_TouchGenerated_Touch_radiusVariance_m3004455427 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchGenerated::__Register()
extern "C"  void UnityEngine_TouchGenerated___Register_m2080378756 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchGenerated::ilo_setInt321(System.Int32,System.Int32)
extern "C"  void UnityEngine_TouchGenerated_ilo_setInt321_m1230690579 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchGenerated::ilo_setVector2S2(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_TouchGenerated_ilo_setVector2S2_m1338802021 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TouchGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_TouchGenerated_ilo_setSingle3_m3811953523 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
