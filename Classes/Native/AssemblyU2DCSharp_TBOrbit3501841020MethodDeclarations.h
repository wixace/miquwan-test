﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TBOrbit
struct TBOrbit_t3501841020;
// DragGesture
struct DragGesture_t2914643285;
// PinchGesture
struct PinchGesture_t1502590799;
// GestureRecognizer
struct GestureRecognizer_t3512875949;
// Gesture
struct Gesture_t1589572905;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_DragGesture2914643285.h"
#include "AssemblyU2DCSharp_PinchGesture1502590799.h"
#include "AssemblyU2DCSharp_GestureRecognizer3512875949.h"
#include "AssemblyU2DCSharp_TBOrbit3501841020.h"
#include "AssemblyU2DCSharp_Gesture1589572905.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void TBOrbit::.ctor()
extern "C"  void TBOrbit__ctor_m2138529903 (TBOrbit_t3501841020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TBOrbit::get_Distance()
extern "C"  float TBOrbit_get_Distance_m1488810983 (TBOrbit_t3501841020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TBOrbit::get_IdealDistance()
extern "C"  float TBOrbit_get_IdealDistance_m2874419290 (TBOrbit_t3501841020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBOrbit::set_IdealDistance(System.Single)
extern "C"  void TBOrbit_set_IdealDistance_m1556939089 (TBOrbit_t3501841020 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TBOrbit::get_Yaw()
extern "C"  float TBOrbit_get_Yaw_m1931170015 (TBOrbit_t3501841020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TBOrbit::get_IdealYaw()
extern "C"  float TBOrbit_get_IdealYaw_m2936823948 (TBOrbit_t3501841020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBOrbit::set_IdealYaw(System.Single)
extern "C"  void TBOrbit_set_IdealYaw_m2220099295 (TBOrbit_t3501841020 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TBOrbit::get_Pitch()
extern "C"  float TBOrbit_get_Pitch_m1258996144 (TBOrbit_t3501841020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TBOrbit::get_IdealPitch()
extern "C"  float TBOrbit_get_IdealPitch_m1324784157 (TBOrbit_t3501841020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBOrbit::set_IdealPitch(System.Single)
extern "C"  void TBOrbit_set_IdealPitch_m1475219694 (TBOrbit_t3501841020 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TBOrbit::get_IdealPanOffset()
extern "C"  Vector3_t4282066566  TBOrbit_get_IdealPanOffset_m959699583 (TBOrbit_t3501841020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBOrbit::set_IdealPanOffset(UnityEngine.Vector3)
extern "C"  void TBOrbit_set_IdealPanOffset_m2351606924 (TBOrbit_t3501841020 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TBOrbit::get_PanOffset()
extern "C"  Vector3_t4282066566  TBOrbit_get_PanOffset_m1211261230 (TBOrbit_t3501841020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBOrbit::InstallGestureRecognizers()
extern "C"  void TBOrbit_InstallGestureRecognizers_m3369568910 (TBOrbit_t3501841020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBOrbit::Start()
extern "C"  void TBOrbit_Start_m1085667695 (TBOrbit_t3501841020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBOrbit::OnDrag(DragGesture)
extern "C"  void TBOrbit_OnDrag_m3310469973 (TBOrbit_t3501841020 * __this, DragGesture_t2914643285 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBOrbit::OnPinch(PinchGesture)
extern "C"  void TBOrbit_OnPinch_m3884041721 (TBOrbit_t3501841020 * __this, PinchGesture_t1502590799 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBOrbit::OnTwoFingerDrag(DragGesture)
extern "C"  void TBOrbit_OnTwoFingerDrag_m1163269318 (TBOrbit_t3501841020 * __this, DragGesture_t2914643285 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBOrbit::Apply()
extern "C"  void TBOrbit_Apply_m2189632891 (TBOrbit_t3501841020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBOrbit::LateUpdate()
extern "C"  void TBOrbit_LateUpdate_m2590234436 (TBOrbit_t3501841020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TBOrbit::ClampAngle(System.Single,System.Single,System.Single)
extern "C"  float TBOrbit_ClampAngle_m2904755156 (Il2CppObject * __this /* static, unused */, float ___angle0, float ___min1, float ___max2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBOrbit::ResetPanning()
extern "C"  void TBOrbit_ResetPanning_m148413687 (TBOrbit_t3501841020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TBOrbit::<InstallGestureRecognizers>m__364(GestureRecognizer)
extern "C"  bool TBOrbit_U3CInstallGestureRecognizersU3Em__364_m3983959047 (Il2CppObject * __this /* static, unused */, GestureRecognizer_t3512875949 * ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TBOrbit::<InstallGestureRecognizers>m__365(GestureRecognizer)
extern "C"  bool TBOrbit_U3CInstallGestureRecognizersU3Em__365_m3575134822 (Il2CppObject * __this /* static, unused */, GestureRecognizer_t3512875949 * ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TBOrbit::<InstallGestureRecognizers>m__366(GestureRecognizer)
extern "C"  bool TBOrbit_U3CInstallGestureRecognizersU3Em__366_m3166310597 (Il2CppObject * __this /* static, unused */, GestureRecognizer_t3512875949 * ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBOrbit::ilo_InstallGestureRecognizers1(TBOrbit)
extern "C"  void TBOrbit_ilo_InstallGestureRecognizers1_m2178941886 (Il2CppObject * __this /* static, unused */, TBOrbit_t3501841020 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBOrbit::ilo_set_IdealDistance2(TBOrbit,System.Single)
extern "C"  void TBOrbit_ilo_set_IdealDistance2_m462054670 (Il2CppObject * __this /* static, unused */, TBOrbit_t3501841020 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GestureRecognizer TBOrbit::ilo_get_Recognizer3(Gesture)
extern "C"  GestureRecognizer_t3512875949 * TBOrbit_ilo_get_Recognizer3_m1551093251 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TBOrbit::ilo_get_IdealYaw4(TBOrbit)
extern "C"  float TBOrbit_ilo_get_IdealYaw4_m3632974367 (Il2CppObject * __this /* static, unused */, TBOrbit_t3501841020 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TBOrbit::ilo_Centimeters5(System.Single)
extern "C"  float TBOrbit_ilo_Centimeters5_m2064040901 (Il2CppObject * __this /* static, unused */, float ___valueInPixels0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TBOrbit::ilo_get_IdealPitch6(TBOrbit)
extern "C"  float TBOrbit_ilo_get_IdealPitch6_m3945862190 (Il2CppObject * __this /* static, unused */, TBOrbit_t3501841020 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TBOrbit::ilo_get_DeltaMove7(DragGesture)
extern "C"  Vector2_t4282066565  TBOrbit_ilo_get_DeltaMove7_m769166745 (Il2CppObject * __this /* static, unused */, DragGesture_t2914643285 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBOrbit::ilo_set_IdealPitch8(TBOrbit,System.Single)
extern "C"  void TBOrbit_ilo_set_IdealPitch8_m3477193745 (Il2CppObject * __this /* static, unused */, TBOrbit_t3501841020 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBOrbit::ilo_Apply9(TBOrbit)
extern "C"  void TBOrbit_ilo_Apply9_m595387171 (Il2CppObject * __this /* static, unused */, TBOrbit_t3501841020 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
