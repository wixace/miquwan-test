﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_407833816.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr_ErrorIn2633981210.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m333815527_gshared (KeyValuePair_2_t407833816 * __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m333815527(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t407833816 *, Il2CppObject *, ErrorInfo_t2633981210 , const MethodInfo*))KeyValuePair_2__ctor_m333815527_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3798965537_gshared (KeyValuePair_2_t407833816 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m3798965537(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t407833816 *, const MethodInfo*))KeyValuePair_2_get_Key_m3798965537_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3619538658_gshared (KeyValuePair_2_t407833816 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3619538658(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t407833816 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m3619538658_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Value()
extern "C"  ErrorInfo_t2633981210  KeyValuePair_2_get_Value_m2907532449_gshared (KeyValuePair_2_t407833816 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2907532449(__this, method) ((  ErrorInfo_t2633981210  (*) (KeyValuePair_2_t407833816 *, const MethodInfo*))KeyValuePair_2_get_Value_m2907532449_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2553731554_gshared (KeyValuePair_2_t407833816 * __this, ErrorInfo_t2633981210  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2553731554(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t407833816 *, ErrorInfo_t2633981210 , const MethodInfo*))KeyValuePair_2_set_Value_m2553731554_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2605285632_gshared (KeyValuePair_2_t407833816 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2605285632(__this, method) ((  String_t* (*) (KeyValuePair_2_t407833816 *, const MethodInfo*))KeyValuePair_2_ToString_m2605285632_gshared)(__this, method)
