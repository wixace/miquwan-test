﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ReplayOtherPlayer
struct ReplayOtherPlayer_t1774842762;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<ReplayBase>>
struct Dictionary_2_t2968307082;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayData
struct  ReplayData_t779762769  : public Il2CppObject
{
public:
	// System.Int32 ReplayData::id
	int32_t ___id_0;
	// System.Int32 ReplayData::checkPointId
	int32_t ___checkPointId_1;
	// System.Boolean ReplayData::isWin
	bool ___isWin_2;
	// System.String ReplayData::mineJson
	String_t* ___mineJson_3;
	// System.String ReplayData::enemyJson
	String_t* ___enemyJson_4;
	// System.String ReplayData::replayDataJson
	String_t* ___replayDataJson_5;
	// ReplayOtherPlayer ReplayData::mine
	ReplayOtherPlayer_t1774842762 * ___mine_6;
	// ReplayOtherPlayer ReplayData::enemy
	ReplayOtherPlayer_t1774842762 * ___enemy_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<ReplayBase>> ReplayData::replayDataDics
	Dictionary_2_t2968307082 * ___replayDataDics_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ReplayData_t779762769, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_checkPointId_1() { return static_cast<int32_t>(offsetof(ReplayData_t779762769, ___checkPointId_1)); }
	inline int32_t get_checkPointId_1() const { return ___checkPointId_1; }
	inline int32_t* get_address_of_checkPointId_1() { return &___checkPointId_1; }
	inline void set_checkPointId_1(int32_t value)
	{
		___checkPointId_1 = value;
	}

	inline static int32_t get_offset_of_isWin_2() { return static_cast<int32_t>(offsetof(ReplayData_t779762769, ___isWin_2)); }
	inline bool get_isWin_2() const { return ___isWin_2; }
	inline bool* get_address_of_isWin_2() { return &___isWin_2; }
	inline void set_isWin_2(bool value)
	{
		___isWin_2 = value;
	}

	inline static int32_t get_offset_of_mineJson_3() { return static_cast<int32_t>(offsetof(ReplayData_t779762769, ___mineJson_3)); }
	inline String_t* get_mineJson_3() const { return ___mineJson_3; }
	inline String_t** get_address_of_mineJson_3() { return &___mineJson_3; }
	inline void set_mineJson_3(String_t* value)
	{
		___mineJson_3 = value;
		Il2CppCodeGenWriteBarrier(&___mineJson_3, value);
	}

	inline static int32_t get_offset_of_enemyJson_4() { return static_cast<int32_t>(offsetof(ReplayData_t779762769, ___enemyJson_4)); }
	inline String_t* get_enemyJson_4() const { return ___enemyJson_4; }
	inline String_t** get_address_of_enemyJson_4() { return &___enemyJson_4; }
	inline void set_enemyJson_4(String_t* value)
	{
		___enemyJson_4 = value;
		Il2CppCodeGenWriteBarrier(&___enemyJson_4, value);
	}

	inline static int32_t get_offset_of_replayDataJson_5() { return static_cast<int32_t>(offsetof(ReplayData_t779762769, ___replayDataJson_5)); }
	inline String_t* get_replayDataJson_5() const { return ___replayDataJson_5; }
	inline String_t** get_address_of_replayDataJson_5() { return &___replayDataJson_5; }
	inline void set_replayDataJson_5(String_t* value)
	{
		___replayDataJson_5 = value;
		Il2CppCodeGenWriteBarrier(&___replayDataJson_5, value);
	}

	inline static int32_t get_offset_of_mine_6() { return static_cast<int32_t>(offsetof(ReplayData_t779762769, ___mine_6)); }
	inline ReplayOtherPlayer_t1774842762 * get_mine_6() const { return ___mine_6; }
	inline ReplayOtherPlayer_t1774842762 ** get_address_of_mine_6() { return &___mine_6; }
	inline void set_mine_6(ReplayOtherPlayer_t1774842762 * value)
	{
		___mine_6 = value;
		Il2CppCodeGenWriteBarrier(&___mine_6, value);
	}

	inline static int32_t get_offset_of_enemy_7() { return static_cast<int32_t>(offsetof(ReplayData_t779762769, ___enemy_7)); }
	inline ReplayOtherPlayer_t1774842762 * get_enemy_7() const { return ___enemy_7; }
	inline ReplayOtherPlayer_t1774842762 ** get_address_of_enemy_7() { return &___enemy_7; }
	inline void set_enemy_7(ReplayOtherPlayer_t1774842762 * value)
	{
		___enemy_7 = value;
		Il2CppCodeGenWriteBarrier(&___enemy_7, value);
	}

	inline static int32_t get_offset_of_replayDataDics_8() { return static_cast<int32_t>(offsetof(ReplayData_t779762769, ___replayDataDics_8)); }
	inline Dictionary_2_t2968307082 * get_replayDataDics_8() const { return ___replayDataDics_8; }
	inline Dictionary_2_t2968307082 ** get_address_of_replayDataDics_8() { return &___replayDataDics_8; }
	inline void set_replayDataDics_8(Dictionary_2_t2968307082 * value)
	{
		___replayDataDics_8 = value;
		Il2CppCodeGenWriteBarrier(&___replayDataDics_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
