﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// SceneMgr
struct SceneMgr_t3584105996;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneMgr/<LoadScene>c__AnonStorey159
struct  U3CLoadSceneU3Ec__AnonStorey159_t1284017471  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.String> SceneMgr/<LoadScene>c__AnonStorey159::list
	List_1_t1375417109 * ___list_0;
	// SceneMgr SceneMgr/<LoadScene>c__AnonStorey159::<>f__this
	SceneMgr_t3584105996 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__AnonStorey159_t1284017471, ___list_0)); }
	inline List_1_t1375417109 * get_list_0() const { return ___list_0; }
	inline List_1_t1375417109 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t1375417109 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier(&___list_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ec__AnonStorey159_t1284017471, ___U3CU3Ef__this_1)); }
	inline SceneMgr_t3584105996 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline SceneMgr_t3584105996 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(SceneMgr_t3584105996 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
