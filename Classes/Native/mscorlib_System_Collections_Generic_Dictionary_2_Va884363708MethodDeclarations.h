﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Int32>
struct Dictionary_2_t2952530300;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va884363708.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UIModelDisplayType,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3643404933_gshared (Enumerator_t884363708 * __this, Dictionary_2_t2952530300 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3643404933(__this, ___host0, method) ((  void (*) (Enumerator_t884363708 *, Dictionary_2_t2952530300 *, const MethodInfo*))Enumerator__ctor_m3643404933_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UIModelDisplayType,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m156951356_gshared (Enumerator_t884363708 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m156951356(__this, method) ((  Il2CppObject * (*) (Enumerator_t884363708 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m156951356_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UIModelDisplayType,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m252146960_gshared (Enumerator_t884363708 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m252146960(__this, method) ((  void (*) (Enumerator_t884363708 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m252146960_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UIModelDisplayType,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m1286149223_gshared (Enumerator_t884363708 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1286149223(__this, method) ((  void (*) (Enumerator_t884363708 *, const MethodInfo*))Enumerator_Dispose_m1286149223_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UIModelDisplayType,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3663374076_gshared (Enumerator_t884363708 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3663374076(__this, method) ((  bool (*) (Enumerator_t884363708 *, const MethodInfo*))Enumerator_MoveNext_m3663374076_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UIModelDisplayType,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2584001542_gshared (Enumerator_t884363708 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2584001542(__this, method) ((  int32_t (*) (Enumerator_t884363708 *, const MethodInfo*))Enumerator_get_Current_m2584001542_gshared)(__this, method)
