﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__Iterator1B
struct U3CBeforeSelfU3Ec__Iterator1B_t742088068;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerator_1_t1029143704;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__Iterator1B::.ctor()
extern "C"  void U3CBeforeSelfU3Ec__Iterator1B__ctor_m3410292839 (U3CBeforeSelfU3Ec__Iterator1B_t742088068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__Iterator1B::System.Collections.Generic.IEnumerator<Newtonsoft.Json.Linq.JToken>.get_Current()
extern "C"  JToken_t3412245951 * U3CBeforeSelfU3Ec__Iterator1B_System_Collections_Generic_IEnumeratorU3CNewtonsoft_Json_Linq_JTokenU3E_get_Current_m2979525190 (U3CBeforeSelfU3Ec__Iterator1B_t742088068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__Iterator1B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBeforeSelfU3Ec__Iterator1B_System_Collections_IEnumerator_get_Current_m2713402527 (U3CBeforeSelfU3Ec__Iterator1B_t742088068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__Iterator1B::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CBeforeSelfU3Ec__Iterator1B_System_Collections_IEnumerable_GetEnumerator_m1142663962 (U3CBeforeSelfU3Ec__Iterator1B_t742088068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__Iterator1B::System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>.GetEnumerator()
extern "C"  Il2CppObject* U3CBeforeSelfU3Ec__Iterator1B_System_Collections_Generic_IEnumerableU3CNewtonsoft_Json_Linq_JTokenU3E_GetEnumerator_m1153211441 (U3CBeforeSelfU3Ec__Iterator1B_t742088068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__Iterator1B::MoveNext()
extern "C"  bool U3CBeforeSelfU3Ec__Iterator1B_MoveNext_m2229018413 (U3CBeforeSelfU3Ec__Iterator1B_t742088068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__Iterator1B::Dispose()
extern "C"  void U3CBeforeSelfU3Ec__Iterator1B_Dispose_m129654500 (U3CBeforeSelfU3Ec__Iterator1B_t742088068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__Iterator1B::Reset()
extern "C"  void U3CBeforeSelfU3Ec__Iterator1B_Reset_m1056725780 (U3CBeforeSelfU3Ec__Iterator1B_t742088068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
