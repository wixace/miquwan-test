﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnQuIOSSDKMsg
struct AnQuIOSSDKMsg_t2914021123;
// System.String
struct String_t;
// VersionMgr
struct VersionMgr_t1322950208;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_AnQuIOSSDKMsg2914021123.h"

// System.Void AnQuIOSSDKMsg::.ctor()
extern "C"  void AnQuIOSSDKMsg__ctor_m1408443848 (AnQuIOSSDKMsg_t2914021123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnQuIOSSDKMsg AnQuIOSSDKMsg::Instance()
extern "C"  AnQuIOSSDKMsg_t2914021123 * AnQuIOSSDKMsg_Instance_m3036467233 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnQuIOSSDKMsg::chuShiHua()
extern "C"  void AnQuIOSSDKMsg_chuShiHua_m1730078966 (AnQuIOSSDKMsg_t2914021123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnQuIOSSDKMsg::openDengLu()
extern "C"  void AnQuIOSSDKMsg_openDengLu_m2946459369 (AnQuIOSSDKMsg_t2914021123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnQuIOSSDKMsg::zhuXiaoDengLu()
extern "C"  void AnQuIOSSDKMsg_zhuXiaoDengLu_m2846548751 (AnQuIOSSDKMsg_t2914021123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnQuIOSSDKMsg::zhiFu(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void AnQuIOSSDKMsg_zhiFu_m1093028146 (AnQuIOSSDKMsg_t2914021123 * __this, String_t* ___amount0, String_t* ___name1, String_t* ___Level2, String_t* ___ID3, String_t* ___s_id4, String_t* ___s_name5, String_t* ___subject6, String_t* ___order_no7, String_t* ___ext8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnQuIOSSDKMsg::chuangJieJueSe(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void AnQuIOSSDKMsg_chuangJieJueSe_m1754147190 (AnQuIOSSDKMsg_t2914021123 * __this, String_t* ___roleID0, String_t* ___roleName1, String_t* ___roleLevel2, String_t* ___serverID3, String_t* ___serverName4, String_t* ___balance5, String_t* ___vip6, String_t* ___partyName7, String_t* ___extra8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnQuIOSSDKMsg::jinRuFuWuQiEvent(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void AnQuIOSSDKMsg_jinRuFuWuQiEvent_m2863918755 (AnQuIOSSDKMsg_t2914021123 * __this, String_t* ___roleID0, String_t* ___roleName1, String_t* ___roleLevel2, String_t* ___serverID3, String_t* ___serverName4, String_t* ___balance5, String_t* ___vip6, String_t* ___partyName7, String_t* ___extra8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnQuIOSSDKMsg::jueSeShengJi(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void AnQuIOSSDKMsg_jueSeShengJi_m312526858 (AnQuIOSSDKMsg_t2914021123 * __this, String_t* ___roleID0, String_t* ___roleName1, String_t* ___roleLevel2, String_t* ___serverID3, String_t* ___serverName4, String_t* ___balance5, String_t* ___vip6, String_t* ___partyName7, String_t* ___extra8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnQuIOSSDKMsg::registerSDKWithAppKey(System.String,System.String,System.String,System.String)
extern "C"  void AnQuIOSSDKMsg_registerSDKWithAppKey_m2950899701 (AnQuIOSSDKMsg_t2914021123 * __this, String_t* ___anAppKey0, String_t* ___appID1, String_t* ___cchID2, String_t* ___mdID3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnQuIOSSDKMsg::showLoginView()
extern "C"  void AnQuIOSSDKMsg_showLoginView_m518727927 (AnQuIOSSDKMsg_t2914021123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnQuIOSSDKMsg::loginViewOut()
extern "C"  void AnQuIOSSDKMsg_loginViewOut_m309687836 (AnQuIOSSDKMsg_t2914021123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnQuIOSSDKMsg::payWithAmount(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void AnQuIOSSDKMsg_payWithAmount_m365753430 (AnQuIOSSDKMsg_t2914021123 * __this, String_t* ___amount0, String_t* ___name1, String_t* ___Level2, String_t* ___ID3, String_t* ___s_id4, String_t* ___s_name5, String_t* ___subject6, String_t* ___order_no7, String_t* ___ext8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnQuIOSSDKMsg::uploadCreateInfoRoleID(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void AnQuIOSSDKMsg_uploadCreateInfoRoleID_m3108293802 (AnQuIOSSDKMsg_t2914021123 * __this, String_t* ___roleID0, String_t* ___roleName1, String_t* ___roleLevel2, String_t* ___serverID3, String_t* ___serverName4, String_t* ___balance5, String_t* ___vip6, String_t* ___partyName7, String_t* ___extra8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnQuIOSSDKMsg::uploadEnterInfoRoleID(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void AnQuIOSSDKMsg_uploadEnterInfoRoleID_m3169237958 (AnQuIOSSDKMsg_t2914021123 * __this, String_t* ___roleID0, String_t* ___roleName1, String_t* ___roleLevel2, String_t* ___serverID3, String_t* ___serverName4, String_t* ___balance5, String_t* ___vip6, String_t* ___partyName7, String_t* ___extra8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnQuIOSSDKMsg::uploadUp_levelInfoRoleID(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void AnQuIOSSDKMsg_uploadUp_levelInfoRoleID_m1514287174 (AnQuIOSSDKMsg_t2914021123 * __this, String_t* ___roleID0, String_t* ___roleName1, String_t* ___roleLevel2, String_t* ___serverID3, String_t* ___serverName4, String_t* ___balance5, String_t* ___vip6, String_t* ___partyName7, String_t* ___extra8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr AnQuIOSSDKMsg::ilo_get_Instance1()
extern "C"  VersionMgr_t1322950208 * AnQuIOSSDKMsg_ilo_get_Instance1_m3315812003 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnQuIOSSDKMsg::ilo_uploadCreateInfoRoleID2(AnQuIOSSDKMsg,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void AnQuIOSSDKMsg_ilo_uploadCreateInfoRoleID2_m685795650 (Il2CppObject * __this /* static, unused */, AnQuIOSSDKMsg_t2914021123 * ____this0, String_t* ___roleID1, String_t* ___roleName2, String_t* ___roleLevel3, String_t* ___serverID4, String_t* ___serverName5, String_t* ___balance6, String_t* ___vip7, String_t* ___partyName8, String_t* ___extra9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
