﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocalizationGenerated/<Localization_loadFunction_GetDelegate_member0_arg0>c__AnonStorey6C
struct U3CLocalization_loadFunction_GetDelegate_member0_arg0U3Ec__AnonStorey6C_t745960347;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void LocalizationGenerated/<Localization_loadFunction_GetDelegate_member0_arg0>c__AnonStorey6C::.ctor()
extern "C"  void U3CLocalization_loadFunction_GetDelegate_member0_arg0U3Ec__AnonStorey6C__ctor_m3299569712 (U3CLocalization_loadFunction_GetDelegate_member0_arg0U3Ec__AnonStorey6C_t745960347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] LocalizationGenerated/<Localization_loadFunction_GetDelegate_member0_arg0>c__AnonStorey6C::<>m__6E(System.String)
extern "C"  ByteU5BU5D_t4260760469* U3CLocalization_loadFunction_GetDelegate_member0_arg0U3Ec__AnonStorey6C_U3CU3Em__6E_m453074000 (U3CLocalization_loadFunction_GetDelegate_member0_arg0U3Ec__AnonStorey6C_t745960347 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
