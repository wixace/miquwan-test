﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animation
struct Animation_t1724966010;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AniSpeed
struct  AniSpeed_t3540829163  : public MonoBehaviour_t667441552
{
public:
	// System.Single AniSpeed::speed
	float ___speed_2;
	// UnityEngine.Animation AniSpeed::ani
	Animation_t1724966010 * ___ani_3;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(AniSpeed_t3540829163, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_ani_3() { return static_cast<int32_t>(offsetof(AniSpeed_t3540829163, ___ani_3)); }
	inline Animation_t1724966010 * get_ani_3() const { return ___ani_3; }
	inline Animation_t1724966010 ** get_address_of_ani_3() { return &___ani_3; }
	inline void set_ani_3(Animation_t1724966010 * value)
	{
		___ani_3 = value;
		Il2CppCodeGenWriteBarrier(&___ani_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
