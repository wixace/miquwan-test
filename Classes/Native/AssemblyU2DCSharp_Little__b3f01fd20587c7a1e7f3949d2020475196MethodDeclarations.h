﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._b3f01fd20587c7a1e7f3949dfc72de43
struct _b3f01fd20587c7a1e7f3949dfc72de43_t2020475196;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._b3f01fd20587c7a1e7f3949dfc72de43::.ctor()
extern "C"  void _b3f01fd20587c7a1e7f3949dfc72de43__ctor_m1773979761 (_b3f01fd20587c7a1e7f3949dfc72de43_t2020475196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b3f01fd20587c7a1e7f3949dfc72de43::_b3f01fd20587c7a1e7f3949dfc72de43m2(System.Int32)
extern "C"  int32_t _b3f01fd20587c7a1e7f3949dfc72de43__b3f01fd20587c7a1e7f3949dfc72de43m2_m2328602649 (_b3f01fd20587c7a1e7f3949dfc72de43_t2020475196 * __this, int32_t ____b3f01fd20587c7a1e7f3949dfc72de43a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b3f01fd20587c7a1e7f3949dfc72de43::_b3f01fd20587c7a1e7f3949dfc72de43m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _b3f01fd20587c7a1e7f3949dfc72de43__b3f01fd20587c7a1e7f3949dfc72de43m_m2958511805 (_b3f01fd20587c7a1e7f3949dfc72de43_t2020475196 * __this, int32_t ____b3f01fd20587c7a1e7f3949dfc72de43a0, int32_t ____b3f01fd20587c7a1e7f3949dfc72de43211, int32_t ____b3f01fd20587c7a1e7f3949dfc72de43c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
