﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vignetting
struct Vignetting_t439778199;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void Vignetting::.ctor()
extern "C"  void Vignetting__ctor_m3033148941 (Vignetting_t439778199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vignetting::CheckResources()
extern "C"  bool Vignetting_CheckResources_m2415816762 (Vignetting_t439778199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vignetting::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void Vignetting_OnRenderImage_m2896650801 (Vignetting_t439778199 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vignetting::Main()
extern "C"  void Vignetting_Main_m13337872 (Vignetting_t439778199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
