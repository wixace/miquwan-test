﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;
// UILabel
struct UILabel_t291504320;
// UnityEngine.ParticleSystem
struct ParticleSystem_t381473177;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Float_equip_streng
struct  Float_equip_streng_t2474838303  : public FloatTextUnit_t2362298029
{
public:
	// UnityEngine.Transform Float_equip_streng::_strength_master_panel
	Transform_t1659122786 * ____strength_master_panel_5;
	// UnityEngine.Transform Float_equip_streng::_tanzi
	Transform_t1659122786 * ____tanzi_6;
	// UILabel Float_equip_streng::_label_master_tile
	UILabel_t291504320 * ____label_master_tile_7;
	// UILabel Float_equip_streng::_label_master_value
	UILabel_t291504320 * ____label_master_value_8;
	// UILabel Float_equip_streng::_label_baoji
	UILabel_t291504320 * ____label_baoji_9;
	// UILabel Float_equip_streng::_label_type
	UILabel_t291504320 * ____label_type_10;
	// UILabel Float_equip_streng::_label_num
	UILabel_t291504320 * ____label_num_11;
	// UILabel Float_equip_streng::_label_value
	UILabel_t291504320 * ____label_value_12;
	// UILabel Float_equip_streng::Label_Awaken_txt
	UILabel_t291504320 * ___Label_Awaken_txt_13;
	// UnityEngine.ParticleSystem Float_equip_streng::_particle
	ParticleSystem_t381473177 * ____particle_14;
	// System.String[] Float_equip_streng::floatMasterArr
	StringU5BU5D_t4054002952* ___floatMasterArr_15;
	// System.Int32 Float_equip_streng::masterlv
	int32_t ___masterlv_16;
	// System.Int32 Float_equip_streng::StrKey
	int32_t ___StrKey_17;
	// System.Boolean Float_equip_streng::isMaster
	bool ___isMaster_18;
	// UnityEngine.Vector3 Float_equip_streng::_lab_type_init_pos
	Vector3_t4282066566  ____lab_type_init_pos_19;

public:
	inline static int32_t get_offset_of__strength_master_panel_5() { return static_cast<int32_t>(offsetof(Float_equip_streng_t2474838303, ____strength_master_panel_5)); }
	inline Transform_t1659122786 * get__strength_master_panel_5() const { return ____strength_master_panel_5; }
	inline Transform_t1659122786 ** get_address_of__strength_master_panel_5() { return &____strength_master_panel_5; }
	inline void set__strength_master_panel_5(Transform_t1659122786 * value)
	{
		____strength_master_panel_5 = value;
		Il2CppCodeGenWriteBarrier(&____strength_master_panel_5, value);
	}

	inline static int32_t get_offset_of__tanzi_6() { return static_cast<int32_t>(offsetof(Float_equip_streng_t2474838303, ____tanzi_6)); }
	inline Transform_t1659122786 * get__tanzi_6() const { return ____tanzi_6; }
	inline Transform_t1659122786 ** get_address_of__tanzi_6() { return &____tanzi_6; }
	inline void set__tanzi_6(Transform_t1659122786 * value)
	{
		____tanzi_6 = value;
		Il2CppCodeGenWriteBarrier(&____tanzi_6, value);
	}

	inline static int32_t get_offset_of__label_master_tile_7() { return static_cast<int32_t>(offsetof(Float_equip_streng_t2474838303, ____label_master_tile_7)); }
	inline UILabel_t291504320 * get__label_master_tile_7() const { return ____label_master_tile_7; }
	inline UILabel_t291504320 ** get_address_of__label_master_tile_7() { return &____label_master_tile_7; }
	inline void set__label_master_tile_7(UILabel_t291504320 * value)
	{
		____label_master_tile_7 = value;
		Il2CppCodeGenWriteBarrier(&____label_master_tile_7, value);
	}

	inline static int32_t get_offset_of__label_master_value_8() { return static_cast<int32_t>(offsetof(Float_equip_streng_t2474838303, ____label_master_value_8)); }
	inline UILabel_t291504320 * get__label_master_value_8() const { return ____label_master_value_8; }
	inline UILabel_t291504320 ** get_address_of__label_master_value_8() { return &____label_master_value_8; }
	inline void set__label_master_value_8(UILabel_t291504320 * value)
	{
		____label_master_value_8 = value;
		Il2CppCodeGenWriteBarrier(&____label_master_value_8, value);
	}

	inline static int32_t get_offset_of__label_baoji_9() { return static_cast<int32_t>(offsetof(Float_equip_streng_t2474838303, ____label_baoji_9)); }
	inline UILabel_t291504320 * get__label_baoji_9() const { return ____label_baoji_9; }
	inline UILabel_t291504320 ** get_address_of__label_baoji_9() { return &____label_baoji_9; }
	inline void set__label_baoji_9(UILabel_t291504320 * value)
	{
		____label_baoji_9 = value;
		Il2CppCodeGenWriteBarrier(&____label_baoji_9, value);
	}

	inline static int32_t get_offset_of__label_type_10() { return static_cast<int32_t>(offsetof(Float_equip_streng_t2474838303, ____label_type_10)); }
	inline UILabel_t291504320 * get__label_type_10() const { return ____label_type_10; }
	inline UILabel_t291504320 ** get_address_of__label_type_10() { return &____label_type_10; }
	inline void set__label_type_10(UILabel_t291504320 * value)
	{
		____label_type_10 = value;
		Il2CppCodeGenWriteBarrier(&____label_type_10, value);
	}

	inline static int32_t get_offset_of__label_num_11() { return static_cast<int32_t>(offsetof(Float_equip_streng_t2474838303, ____label_num_11)); }
	inline UILabel_t291504320 * get__label_num_11() const { return ____label_num_11; }
	inline UILabel_t291504320 ** get_address_of__label_num_11() { return &____label_num_11; }
	inline void set__label_num_11(UILabel_t291504320 * value)
	{
		____label_num_11 = value;
		Il2CppCodeGenWriteBarrier(&____label_num_11, value);
	}

	inline static int32_t get_offset_of__label_value_12() { return static_cast<int32_t>(offsetof(Float_equip_streng_t2474838303, ____label_value_12)); }
	inline UILabel_t291504320 * get__label_value_12() const { return ____label_value_12; }
	inline UILabel_t291504320 ** get_address_of__label_value_12() { return &____label_value_12; }
	inline void set__label_value_12(UILabel_t291504320 * value)
	{
		____label_value_12 = value;
		Il2CppCodeGenWriteBarrier(&____label_value_12, value);
	}

	inline static int32_t get_offset_of_Label_Awaken_txt_13() { return static_cast<int32_t>(offsetof(Float_equip_streng_t2474838303, ___Label_Awaken_txt_13)); }
	inline UILabel_t291504320 * get_Label_Awaken_txt_13() const { return ___Label_Awaken_txt_13; }
	inline UILabel_t291504320 ** get_address_of_Label_Awaken_txt_13() { return &___Label_Awaken_txt_13; }
	inline void set_Label_Awaken_txt_13(UILabel_t291504320 * value)
	{
		___Label_Awaken_txt_13 = value;
		Il2CppCodeGenWriteBarrier(&___Label_Awaken_txt_13, value);
	}

	inline static int32_t get_offset_of__particle_14() { return static_cast<int32_t>(offsetof(Float_equip_streng_t2474838303, ____particle_14)); }
	inline ParticleSystem_t381473177 * get__particle_14() const { return ____particle_14; }
	inline ParticleSystem_t381473177 ** get_address_of__particle_14() { return &____particle_14; }
	inline void set__particle_14(ParticleSystem_t381473177 * value)
	{
		____particle_14 = value;
		Il2CppCodeGenWriteBarrier(&____particle_14, value);
	}

	inline static int32_t get_offset_of_floatMasterArr_15() { return static_cast<int32_t>(offsetof(Float_equip_streng_t2474838303, ___floatMasterArr_15)); }
	inline StringU5BU5D_t4054002952* get_floatMasterArr_15() const { return ___floatMasterArr_15; }
	inline StringU5BU5D_t4054002952** get_address_of_floatMasterArr_15() { return &___floatMasterArr_15; }
	inline void set_floatMasterArr_15(StringU5BU5D_t4054002952* value)
	{
		___floatMasterArr_15 = value;
		Il2CppCodeGenWriteBarrier(&___floatMasterArr_15, value);
	}

	inline static int32_t get_offset_of_masterlv_16() { return static_cast<int32_t>(offsetof(Float_equip_streng_t2474838303, ___masterlv_16)); }
	inline int32_t get_masterlv_16() const { return ___masterlv_16; }
	inline int32_t* get_address_of_masterlv_16() { return &___masterlv_16; }
	inline void set_masterlv_16(int32_t value)
	{
		___masterlv_16 = value;
	}

	inline static int32_t get_offset_of_StrKey_17() { return static_cast<int32_t>(offsetof(Float_equip_streng_t2474838303, ___StrKey_17)); }
	inline int32_t get_StrKey_17() const { return ___StrKey_17; }
	inline int32_t* get_address_of_StrKey_17() { return &___StrKey_17; }
	inline void set_StrKey_17(int32_t value)
	{
		___StrKey_17 = value;
	}

	inline static int32_t get_offset_of_isMaster_18() { return static_cast<int32_t>(offsetof(Float_equip_streng_t2474838303, ___isMaster_18)); }
	inline bool get_isMaster_18() const { return ___isMaster_18; }
	inline bool* get_address_of_isMaster_18() { return &___isMaster_18; }
	inline void set_isMaster_18(bool value)
	{
		___isMaster_18 = value;
	}

	inline static int32_t get_offset_of__lab_type_init_pos_19() { return static_cast<int32_t>(offsetof(Float_equip_streng_t2474838303, ____lab_type_init_pos_19)); }
	inline Vector3_t4282066566  get__lab_type_init_pos_19() const { return ____lab_type_init_pos_19; }
	inline Vector3_t4282066566 * get_address_of__lab_type_init_pos_19() { return &____lab_type_init_pos_19; }
	inline void set__lab_type_init_pos_19(Vector3_t4282066566  value)
	{
		____lab_type_init_pos_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
