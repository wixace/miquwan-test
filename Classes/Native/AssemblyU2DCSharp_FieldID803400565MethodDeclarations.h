﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FieldID
struct FieldID_t803400565;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void FieldID::.ctor(System.String)
extern "C"  void FieldID__ctor_m2035010348 (FieldID_t803400565 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
