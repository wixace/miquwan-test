﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_JSComponent1642894772.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSComponent_EventTrigger
struct  JSComponent_EventTrigger_t2269136521  : public JSComponent_t1642894772
{
public:
	// System.Int32 JSComponent_EventTrigger::idUpdate
	int32_t ___idUpdate_16;
	// System.Int32 JSComponent_EventTrigger::idLateUpdate
	int32_t ___idLateUpdate_17;
	// System.Int32 JSComponent_EventTrigger::idOnBeginDrag
	int32_t ___idOnBeginDrag_18;
	// System.Int32 JSComponent_EventTrigger::idOnCancel
	int32_t ___idOnCancel_19;
	// System.Int32 JSComponent_EventTrigger::idOnDeselect
	int32_t ___idOnDeselect_20;
	// System.Int32 JSComponent_EventTrigger::idOnDrag
	int32_t ___idOnDrag_21;
	// System.Int32 JSComponent_EventTrigger::idOnDrop
	int32_t ___idOnDrop_22;
	// System.Int32 JSComponent_EventTrigger::idOnEndDrag
	int32_t ___idOnEndDrag_23;
	// System.Int32 JSComponent_EventTrigger::idOnInitializePotentialDrag
	int32_t ___idOnInitializePotentialDrag_24;
	// System.Int32 JSComponent_EventTrigger::idOnMove
	int32_t ___idOnMove_25;
	// System.Int32 JSComponent_EventTrigger::idOnPointerClick
	int32_t ___idOnPointerClick_26;
	// System.Int32 JSComponent_EventTrigger::idOnPointerDown
	int32_t ___idOnPointerDown_27;
	// System.Int32 JSComponent_EventTrigger::idOnPointerEnter
	int32_t ___idOnPointerEnter_28;
	// System.Int32 JSComponent_EventTrigger::idOnPointerExit
	int32_t ___idOnPointerExit_29;
	// System.Int32 JSComponent_EventTrigger::idOnPointerUp
	int32_t ___idOnPointerUp_30;
	// System.Int32 JSComponent_EventTrigger::idOnScroll
	int32_t ___idOnScroll_31;
	// System.Int32 JSComponent_EventTrigger::idOnSelect
	int32_t ___idOnSelect_32;
	// System.Int32 JSComponent_EventTrigger::idOnSubmit
	int32_t ___idOnSubmit_33;
	// System.Int32 JSComponent_EventTrigger::idOnUpdateSelected
	int32_t ___idOnUpdateSelected_34;

public:
	inline static int32_t get_offset_of_idUpdate_16() { return static_cast<int32_t>(offsetof(JSComponent_EventTrigger_t2269136521, ___idUpdate_16)); }
	inline int32_t get_idUpdate_16() const { return ___idUpdate_16; }
	inline int32_t* get_address_of_idUpdate_16() { return &___idUpdate_16; }
	inline void set_idUpdate_16(int32_t value)
	{
		___idUpdate_16 = value;
	}

	inline static int32_t get_offset_of_idLateUpdate_17() { return static_cast<int32_t>(offsetof(JSComponent_EventTrigger_t2269136521, ___idLateUpdate_17)); }
	inline int32_t get_idLateUpdate_17() const { return ___idLateUpdate_17; }
	inline int32_t* get_address_of_idLateUpdate_17() { return &___idLateUpdate_17; }
	inline void set_idLateUpdate_17(int32_t value)
	{
		___idLateUpdate_17 = value;
	}

	inline static int32_t get_offset_of_idOnBeginDrag_18() { return static_cast<int32_t>(offsetof(JSComponent_EventTrigger_t2269136521, ___idOnBeginDrag_18)); }
	inline int32_t get_idOnBeginDrag_18() const { return ___idOnBeginDrag_18; }
	inline int32_t* get_address_of_idOnBeginDrag_18() { return &___idOnBeginDrag_18; }
	inline void set_idOnBeginDrag_18(int32_t value)
	{
		___idOnBeginDrag_18 = value;
	}

	inline static int32_t get_offset_of_idOnCancel_19() { return static_cast<int32_t>(offsetof(JSComponent_EventTrigger_t2269136521, ___idOnCancel_19)); }
	inline int32_t get_idOnCancel_19() const { return ___idOnCancel_19; }
	inline int32_t* get_address_of_idOnCancel_19() { return &___idOnCancel_19; }
	inline void set_idOnCancel_19(int32_t value)
	{
		___idOnCancel_19 = value;
	}

	inline static int32_t get_offset_of_idOnDeselect_20() { return static_cast<int32_t>(offsetof(JSComponent_EventTrigger_t2269136521, ___idOnDeselect_20)); }
	inline int32_t get_idOnDeselect_20() const { return ___idOnDeselect_20; }
	inline int32_t* get_address_of_idOnDeselect_20() { return &___idOnDeselect_20; }
	inline void set_idOnDeselect_20(int32_t value)
	{
		___idOnDeselect_20 = value;
	}

	inline static int32_t get_offset_of_idOnDrag_21() { return static_cast<int32_t>(offsetof(JSComponent_EventTrigger_t2269136521, ___idOnDrag_21)); }
	inline int32_t get_idOnDrag_21() const { return ___idOnDrag_21; }
	inline int32_t* get_address_of_idOnDrag_21() { return &___idOnDrag_21; }
	inline void set_idOnDrag_21(int32_t value)
	{
		___idOnDrag_21 = value;
	}

	inline static int32_t get_offset_of_idOnDrop_22() { return static_cast<int32_t>(offsetof(JSComponent_EventTrigger_t2269136521, ___idOnDrop_22)); }
	inline int32_t get_idOnDrop_22() const { return ___idOnDrop_22; }
	inline int32_t* get_address_of_idOnDrop_22() { return &___idOnDrop_22; }
	inline void set_idOnDrop_22(int32_t value)
	{
		___idOnDrop_22 = value;
	}

	inline static int32_t get_offset_of_idOnEndDrag_23() { return static_cast<int32_t>(offsetof(JSComponent_EventTrigger_t2269136521, ___idOnEndDrag_23)); }
	inline int32_t get_idOnEndDrag_23() const { return ___idOnEndDrag_23; }
	inline int32_t* get_address_of_idOnEndDrag_23() { return &___idOnEndDrag_23; }
	inline void set_idOnEndDrag_23(int32_t value)
	{
		___idOnEndDrag_23 = value;
	}

	inline static int32_t get_offset_of_idOnInitializePotentialDrag_24() { return static_cast<int32_t>(offsetof(JSComponent_EventTrigger_t2269136521, ___idOnInitializePotentialDrag_24)); }
	inline int32_t get_idOnInitializePotentialDrag_24() const { return ___idOnInitializePotentialDrag_24; }
	inline int32_t* get_address_of_idOnInitializePotentialDrag_24() { return &___idOnInitializePotentialDrag_24; }
	inline void set_idOnInitializePotentialDrag_24(int32_t value)
	{
		___idOnInitializePotentialDrag_24 = value;
	}

	inline static int32_t get_offset_of_idOnMove_25() { return static_cast<int32_t>(offsetof(JSComponent_EventTrigger_t2269136521, ___idOnMove_25)); }
	inline int32_t get_idOnMove_25() const { return ___idOnMove_25; }
	inline int32_t* get_address_of_idOnMove_25() { return &___idOnMove_25; }
	inline void set_idOnMove_25(int32_t value)
	{
		___idOnMove_25 = value;
	}

	inline static int32_t get_offset_of_idOnPointerClick_26() { return static_cast<int32_t>(offsetof(JSComponent_EventTrigger_t2269136521, ___idOnPointerClick_26)); }
	inline int32_t get_idOnPointerClick_26() const { return ___idOnPointerClick_26; }
	inline int32_t* get_address_of_idOnPointerClick_26() { return &___idOnPointerClick_26; }
	inline void set_idOnPointerClick_26(int32_t value)
	{
		___idOnPointerClick_26 = value;
	}

	inline static int32_t get_offset_of_idOnPointerDown_27() { return static_cast<int32_t>(offsetof(JSComponent_EventTrigger_t2269136521, ___idOnPointerDown_27)); }
	inline int32_t get_idOnPointerDown_27() const { return ___idOnPointerDown_27; }
	inline int32_t* get_address_of_idOnPointerDown_27() { return &___idOnPointerDown_27; }
	inline void set_idOnPointerDown_27(int32_t value)
	{
		___idOnPointerDown_27 = value;
	}

	inline static int32_t get_offset_of_idOnPointerEnter_28() { return static_cast<int32_t>(offsetof(JSComponent_EventTrigger_t2269136521, ___idOnPointerEnter_28)); }
	inline int32_t get_idOnPointerEnter_28() const { return ___idOnPointerEnter_28; }
	inline int32_t* get_address_of_idOnPointerEnter_28() { return &___idOnPointerEnter_28; }
	inline void set_idOnPointerEnter_28(int32_t value)
	{
		___idOnPointerEnter_28 = value;
	}

	inline static int32_t get_offset_of_idOnPointerExit_29() { return static_cast<int32_t>(offsetof(JSComponent_EventTrigger_t2269136521, ___idOnPointerExit_29)); }
	inline int32_t get_idOnPointerExit_29() const { return ___idOnPointerExit_29; }
	inline int32_t* get_address_of_idOnPointerExit_29() { return &___idOnPointerExit_29; }
	inline void set_idOnPointerExit_29(int32_t value)
	{
		___idOnPointerExit_29 = value;
	}

	inline static int32_t get_offset_of_idOnPointerUp_30() { return static_cast<int32_t>(offsetof(JSComponent_EventTrigger_t2269136521, ___idOnPointerUp_30)); }
	inline int32_t get_idOnPointerUp_30() const { return ___idOnPointerUp_30; }
	inline int32_t* get_address_of_idOnPointerUp_30() { return &___idOnPointerUp_30; }
	inline void set_idOnPointerUp_30(int32_t value)
	{
		___idOnPointerUp_30 = value;
	}

	inline static int32_t get_offset_of_idOnScroll_31() { return static_cast<int32_t>(offsetof(JSComponent_EventTrigger_t2269136521, ___idOnScroll_31)); }
	inline int32_t get_idOnScroll_31() const { return ___idOnScroll_31; }
	inline int32_t* get_address_of_idOnScroll_31() { return &___idOnScroll_31; }
	inline void set_idOnScroll_31(int32_t value)
	{
		___idOnScroll_31 = value;
	}

	inline static int32_t get_offset_of_idOnSelect_32() { return static_cast<int32_t>(offsetof(JSComponent_EventTrigger_t2269136521, ___idOnSelect_32)); }
	inline int32_t get_idOnSelect_32() const { return ___idOnSelect_32; }
	inline int32_t* get_address_of_idOnSelect_32() { return &___idOnSelect_32; }
	inline void set_idOnSelect_32(int32_t value)
	{
		___idOnSelect_32 = value;
	}

	inline static int32_t get_offset_of_idOnSubmit_33() { return static_cast<int32_t>(offsetof(JSComponent_EventTrigger_t2269136521, ___idOnSubmit_33)); }
	inline int32_t get_idOnSubmit_33() const { return ___idOnSubmit_33; }
	inline int32_t* get_address_of_idOnSubmit_33() { return &___idOnSubmit_33; }
	inline void set_idOnSubmit_33(int32_t value)
	{
		___idOnSubmit_33 = value;
	}

	inline static int32_t get_offset_of_idOnUpdateSelected_34() { return static_cast<int32_t>(offsetof(JSComponent_EventTrigger_t2269136521, ___idOnUpdateSelected_34)); }
	inline int32_t get_idOnUpdateSelected_34() const { return ___idOnUpdateSelected_34; }
	inline int32_t* get_address_of_idOnUpdateSelected_34() { return &___idOnUpdateSelected_34; }
	inline void set_idOnUpdateSelected_34(int32_t value)
	{
		___idOnUpdateSelected_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
