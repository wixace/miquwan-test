﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;
// UILabel
struct UILabel_t291504320;
// UISprite
struct UISprite_t661437049;
// UIPanel
struct UIPanel_t295209936;
// TweenScale[]
struct TweenScaleU5BU5D_t3591996518;
// AssetGOList
struct AssetGOList_t2543589494;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_FightEnum_ESkillFunType2084611099.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SkillHurtShow
struct  SkillHurtShow_t1728264445  : public Il2CppObject
{
public:
	// UnityEngine.Transform SkillHurtShow::mTransform
	Transform_t1659122786 * ___mTransform_0;
	// UILabel SkillHurtShow::_labelHurt
	UILabel_t291504320 * ____labelHurt_1;
	// UISprite SkillHurtShow::_sprite_font
	UISprite_t661437049 * ____sprite_font_2;
	// UIPanel SkillHurtShow::_panel
	UIPanel_t295209936 * ____panel_3;
	// UISprite SkillHurtShow::_spriteBg
	UISprite_t661437049 * ____spriteBg_4;
	// TweenScale[] SkillHurtShow::spriteBgTs
	TweenScaleU5BU5D_t3591996518* ___spriteBgTs_5;
	// System.Int32 SkillHurtShow::_heroId
	int32_t ____heroId_6;
	// System.Int32 SkillHurtShow::_skillId
	int32_t ____skillId_7;
	// FightEnum.ESkillFunType SkillHurtShow::_skillType
	int32_t ____skillType_8;
	// System.Single SkillHurtShow::_showHurt
	float ____showHurt_9;
	// System.Single SkillHurtShow::_showSumHurt
	float ____showSumHurt_10;
	// System.Int32 SkillHurtShow::_sumHurt
	int32_t ____sumHurt_11;
	// System.Boolean SkillHurtShow::_isPlay
	bool ____isPlay_12;
	// System.Int32 SkillHurtShow::_shootSkillChildNum
	int32_t ____shootSkillChildNum_13;
	// System.Single SkillHurtShow::_lastTime
	float ____lastTime_14;
	// AssetGOList SkillHurtShow::fontList
	AssetGOList_t2543589494 * ___fontList_15;
	// System.Int32 SkillHurtShow::funType
	int32_t ___funType_16;
	// System.Int32 SkillHurtShow::teamSkillType
	int32_t ___teamSkillType_17;
	// System.Int32 SkillHurtShow::palyTimes
	int32_t ___palyTimes_18;
	// System.Boolean SkillHurtShow::<isUsing>k__BackingField
	bool ___U3CisUsingU3Ek__BackingField_19;

public:
	inline static int32_t get_offset_of_mTransform_0() { return static_cast<int32_t>(offsetof(SkillHurtShow_t1728264445, ___mTransform_0)); }
	inline Transform_t1659122786 * get_mTransform_0() const { return ___mTransform_0; }
	inline Transform_t1659122786 ** get_address_of_mTransform_0() { return &___mTransform_0; }
	inline void set_mTransform_0(Transform_t1659122786 * value)
	{
		___mTransform_0 = value;
		Il2CppCodeGenWriteBarrier(&___mTransform_0, value);
	}

	inline static int32_t get_offset_of__labelHurt_1() { return static_cast<int32_t>(offsetof(SkillHurtShow_t1728264445, ____labelHurt_1)); }
	inline UILabel_t291504320 * get__labelHurt_1() const { return ____labelHurt_1; }
	inline UILabel_t291504320 ** get_address_of__labelHurt_1() { return &____labelHurt_1; }
	inline void set__labelHurt_1(UILabel_t291504320 * value)
	{
		____labelHurt_1 = value;
		Il2CppCodeGenWriteBarrier(&____labelHurt_1, value);
	}

	inline static int32_t get_offset_of__sprite_font_2() { return static_cast<int32_t>(offsetof(SkillHurtShow_t1728264445, ____sprite_font_2)); }
	inline UISprite_t661437049 * get__sprite_font_2() const { return ____sprite_font_2; }
	inline UISprite_t661437049 ** get_address_of__sprite_font_2() { return &____sprite_font_2; }
	inline void set__sprite_font_2(UISprite_t661437049 * value)
	{
		____sprite_font_2 = value;
		Il2CppCodeGenWriteBarrier(&____sprite_font_2, value);
	}

	inline static int32_t get_offset_of__panel_3() { return static_cast<int32_t>(offsetof(SkillHurtShow_t1728264445, ____panel_3)); }
	inline UIPanel_t295209936 * get__panel_3() const { return ____panel_3; }
	inline UIPanel_t295209936 ** get_address_of__panel_3() { return &____panel_3; }
	inline void set__panel_3(UIPanel_t295209936 * value)
	{
		____panel_3 = value;
		Il2CppCodeGenWriteBarrier(&____panel_3, value);
	}

	inline static int32_t get_offset_of__spriteBg_4() { return static_cast<int32_t>(offsetof(SkillHurtShow_t1728264445, ____spriteBg_4)); }
	inline UISprite_t661437049 * get__spriteBg_4() const { return ____spriteBg_4; }
	inline UISprite_t661437049 ** get_address_of__spriteBg_4() { return &____spriteBg_4; }
	inline void set__spriteBg_4(UISprite_t661437049 * value)
	{
		____spriteBg_4 = value;
		Il2CppCodeGenWriteBarrier(&____spriteBg_4, value);
	}

	inline static int32_t get_offset_of_spriteBgTs_5() { return static_cast<int32_t>(offsetof(SkillHurtShow_t1728264445, ___spriteBgTs_5)); }
	inline TweenScaleU5BU5D_t3591996518* get_spriteBgTs_5() const { return ___spriteBgTs_5; }
	inline TweenScaleU5BU5D_t3591996518** get_address_of_spriteBgTs_5() { return &___spriteBgTs_5; }
	inline void set_spriteBgTs_5(TweenScaleU5BU5D_t3591996518* value)
	{
		___spriteBgTs_5 = value;
		Il2CppCodeGenWriteBarrier(&___spriteBgTs_5, value);
	}

	inline static int32_t get_offset_of__heroId_6() { return static_cast<int32_t>(offsetof(SkillHurtShow_t1728264445, ____heroId_6)); }
	inline int32_t get__heroId_6() const { return ____heroId_6; }
	inline int32_t* get_address_of__heroId_6() { return &____heroId_6; }
	inline void set__heroId_6(int32_t value)
	{
		____heroId_6 = value;
	}

	inline static int32_t get_offset_of__skillId_7() { return static_cast<int32_t>(offsetof(SkillHurtShow_t1728264445, ____skillId_7)); }
	inline int32_t get__skillId_7() const { return ____skillId_7; }
	inline int32_t* get_address_of__skillId_7() { return &____skillId_7; }
	inline void set__skillId_7(int32_t value)
	{
		____skillId_7 = value;
	}

	inline static int32_t get_offset_of__skillType_8() { return static_cast<int32_t>(offsetof(SkillHurtShow_t1728264445, ____skillType_8)); }
	inline int32_t get__skillType_8() const { return ____skillType_8; }
	inline int32_t* get_address_of__skillType_8() { return &____skillType_8; }
	inline void set__skillType_8(int32_t value)
	{
		____skillType_8 = value;
	}

	inline static int32_t get_offset_of__showHurt_9() { return static_cast<int32_t>(offsetof(SkillHurtShow_t1728264445, ____showHurt_9)); }
	inline float get__showHurt_9() const { return ____showHurt_9; }
	inline float* get_address_of__showHurt_9() { return &____showHurt_9; }
	inline void set__showHurt_9(float value)
	{
		____showHurt_9 = value;
	}

	inline static int32_t get_offset_of__showSumHurt_10() { return static_cast<int32_t>(offsetof(SkillHurtShow_t1728264445, ____showSumHurt_10)); }
	inline float get__showSumHurt_10() const { return ____showSumHurt_10; }
	inline float* get_address_of__showSumHurt_10() { return &____showSumHurt_10; }
	inline void set__showSumHurt_10(float value)
	{
		____showSumHurt_10 = value;
	}

	inline static int32_t get_offset_of__sumHurt_11() { return static_cast<int32_t>(offsetof(SkillHurtShow_t1728264445, ____sumHurt_11)); }
	inline int32_t get__sumHurt_11() const { return ____sumHurt_11; }
	inline int32_t* get_address_of__sumHurt_11() { return &____sumHurt_11; }
	inline void set__sumHurt_11(int32_t value)
	{
		____sumHurt_11 = value;
	}

	inline static int32_t get_offset_of__isPlay_12() { return static_cast<int32_t>(offsetof(SkillHurtShow_t1728264445, ____isPlay_12)); }
	inline bool get__isPlay_12() const { return ____isPlay_12; }
	inline bool* get_address_of__isPlay_12() { return &____isPlay_12; }
	inline void set__isPlay_12(bool value)
	{
		____isPlay_12 = value;
	}

	inline static int32_t get_offset_of__shootSkillChildNum_13() { return static_cast<int32_t>(offsetof(SkillHurtShow_t1728264445, ____shootSkillChildNum_13)); }
	inline int32_t get__shootSkillChildNum_13() const { return ____shootSkillChildNum_13; }
	inline int32_t* get_address_of__shootSkillChildNum_13() { return &____shootSkillChildNum_13; }
	inline void set__shootSkillChildNum_13(int32_t value)
	{
		____shootSkillChildNum_13 = value;
	}

	inline static int32_t get_offset_of__lastTime_14() { return static_cast<int32_t>(offsetof(SkillHurtShow_t1728264445, ____lastTime_14)); }
	inline float get__lastTime_14() const { return ____lastTime_14; }
	inline float* get_address_of__lastTime_14() { return &____lastTime_14; }
	inline void set__lastTime_14(float value)
	{
		____lastTime_14 = value;
	}

	inline static int32_t get_offset_of_fontList_15() { return static_cast<int32_t>(offsetof(SkillHurtShow_t1728264445, ___fontList_15)); }
	inline AssetGOList_t2543589494 * get_fontList_15() const { return ___fontList_15; }
	inline AssetGOList_t2543589494 ** get_address_of_fontList_15() { return &___fontList_15; }
	inline void set_fontList_15(AssetGOList_t2543589494 * value)
	{
		___fontList_15 = value;
		Il2CppCodeGenWriteBarrier(&___fontList_15, value);
	}

	inline static int32_t get_offset_of_funType_16() { return static_cast<int32_t>(offsetof(SkillHurtShow_t1728264445, ___funType_16)); }
	inline int32_t get_funType_16() const { return ___funType_16; }
	inline int32_t* get_address_of_funType_16() { return &___funType_16; }
	inline void set_funType_16(int32_t value)
	{
		___funType_16 = value;
	}

	inline static int32_t get_offset_of_teamSkillType_17() { return static_cast<int32_t>(offsetof(SkillHurtShow_t1728264445, ___teamSkillType_17)); }
	inline int32_t get_teamSkillType_17() const { return ___teamSkillType_17; }
	inline int32_t* get_address_of_teamSkillType_17() { return &___teamSkillType_17; }
	inline void set_teamSkillType_17(int32_t value)
	{
		___teamSkillType_17 = value;
	}

	inline static int32_t get_offset_of_palyTimes_18() { return static_cast<int32_t>(offsetof(SkillHurtShow_t1728264445, ___palyTimes_18)); }
	inline int32_t get_palyTimes_18() const { return ___palyTimes_18; }
	inline int32_t* get_address_of_palyTimes_18() { return &___palyTimes_18; }
	inline void set_palyTimes_18(int32_t value)
	{
		___palyTimes_18 = value;
	}

	inline static int32_t get_offset_of_U3CisUsingU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(SkillHurtShow_t1728264445, ___U3CisUsingU3Ek__BackingField_19)); }
	inline bool get_U3CisUsingU3Ek__BackingField_19() const { return ___U3CisUsingU3Ek__BackingField_19; }
	inline bool* get_address_of_U3CisUsingU3Ek__BackingField_19() { return &___U3CisUsingU3Ek__BackingField_19; }
	inline void set_U3CisUsingU3Ek__BackingField_19(bool value)
	{
		___U3CisUsingU3Ek__BackingField_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
