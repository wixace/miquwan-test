﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlowControl.FlowInitCFG
struct FlowInitCFG_t4184666876;
// FlowControl.FlowBase
struct FlowBase_t3680091731;
// LoadingBoardMgr
struct LoadingBoardMgr_t303632014;
// LevelConfigManager
struct LevelConfigManager_t657947911;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FlowControl_FlowBase3680091731.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void FlowControl.FlowInitCFG::.ctor()
extern "C"  void FlowInitCFG__ctor_m866442884 (FlowInitCFG_t4184666876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowInitCFG::Activate()
extern "C"  void FlowInitCFG_Activate_m3442121459 (FlowInitCFG_t4184666876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowInitCFG::FlowUpdate(System.Single)
extern "C"  void FlowInitCFG_FlowUpdate_m8426036 (FlowInitCFG_t4184666876 * __this, float ___duringTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowInitCFG::OnComplete()
extern "C"  void FlowInitCFG_OnComplete_m454420792 (FlowInitCFG_t4184666876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowInitCFG::ilo_Activate1(FlowControl.FlowBase)
extern "C"  void FlowInitCFG_ilo_Activate1_m848072271 (Il2CppObject * __this /* static, unused */, FlowBase_t3680091731 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LoadingBoardMgr FlowControl.FlowInitCFG::ilo_get_Instance2()
extern "C"  LoadingBoardMgr_t303632014 * FlowInitCFG_ilo_get_Instance2_m3288225000 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FlowControl.FlowInitCFG::ilo_get_inited3()
extern "C"  bool FlowInitCFG_ilo_get_inited3_m3892600478 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LevelConfigManager FlowControl.FlowInitCFG::ilo_get_instance4()
extern "C"  LevelConfigManager_t657947911 * FlowInitCFG_ilo_get_instance4_m2247881755 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowInitCFG::ilo_Log5(System.Object,System.Boolean)
extern "C"  void FlowInitCFG_ilo_Log5_m3381254477 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FlowControl.FlowInitCFG::ilo_get_inited6()
extern "C"  bool FlowInitCFG_ilo_get_inited6_m3892603361 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowInitCFG::ilo_set_isComplete7(FlowControl.FlowBase,System.Boolean)
extern "C"  void FlowInitCFG_ilo_set_isComplete7_m800637237 (Il2CppObject * __this /* static, unused */, FlowBase_t3680091731 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
