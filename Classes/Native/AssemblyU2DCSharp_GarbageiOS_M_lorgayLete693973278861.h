﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_lorgayLete69
struct  M_lorgayLete69_t3973278861  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_lorgayLete69::_refer
	String_t* ____refer_0;
	// System.String GarbageiOS.M_lorgayLete69::_stallpugaRestou
	String_t* ____stallpugaRestou_1;
	// System.String GarbageiOS.M_lorgayLete69::_challjur
	String_t* ____challjur_2;
	// System.Single GarbageiOS.M_lorgayLete69::_jouqamir
	float ____jouqamir_3;
	// System.String GarbageiOS.M_lorgayLete69::_leartawtrem
	String_t* ____leartawtrem_4;
	// System.Boolean GarbageiOS.M_lorgayLete69::_naxere
	bool ____naxere_5;
	// System.String GarbageiOS.M_lorgayLete69::_vijeeti
	String_t* ____vijeeti_6;
	// System.UInt32 GarbageiOS.M_lorgayLete69::_vairmaDahurpem
	uint32_t ____vairmaDahurpem_7;
	// System.Boolean GarbageiOS.M_lorgayLete69::_zedeBeror
	bool ____zedeBeror_8;
	// System.Single GarbageiOS.M_lorgayLete69::_tibu
	float ____tibu_9;
	// System.UInt32 GarbageiOS.M_lorgayLete69::_siyeKachoulel
	uint32_t ____siyeKachoulel_10;
	// System.Boolean GarbageiOS.M_lorgayLete69::_cawtal
	bool ____cawtal_11;
	// System.UInt32 GarbageiOS.M_lorgayLete69::_jelferefearVowsear
	uint32_t ____jelferefearVowsear_12;
	// System.Boolean GarbageiOS.M_lorgayLete69::_juchekoo
	bool ____juchekoo_13;
	// System.Int32 GarbageiOS.M_lorgayLete69::_hurmirbou
	int32_t ____hurmirbou_14;
	// System.Single GarbageiOS.M_lorgayLete69::_numasWeyomu
	float ____numasWeyomu_15;
	// System.String GarbageiOS.M_lorgayLete69::_zovecaJereno
	String_t* ____zovecaJereno_16;
	// System.UInt32 GarbageiOS.M_lorgayLete69::_boumormoo
	uint32_t ____boumormoo_17;
	// System.String GarbageiOS.M_lorgayLete69::_joolorSouje
	String_t* ____joolorSouje_18;
	// System.UInt32 GarbageiOS.M_lorgayLete69::_cirxu
	uint32_t ____cirxu_19;
	// System.Int32 GarbageiOS.M_lorgayLete69::_lardrapal
	int32_t ____lardrapal_20;
	// System.Int32 GarbageiOS.M_lorgayLete69::_sisisjir
	int32_t ____sisisjir_21;
	// System.Int32 GarbageiOS.M_lorgayLete69::_ficernal
	int32_t ____ficernal_22;
	// System.Int32 GarbageiOS.M_lorgayLete69::_pirmemla
	int32_t ____pirmemla_23;
	// System.Boolean GarbageiOS.M_lorgayLete69::_remmirxe
	bool ____remmirxe_24;
	// System.String GarbageiOS.M_lorgayLete69::_hearmarHerkouwhem
	String_t* ____hearmarHerkouwhem_25;
	// System.String GarbageiOS.M_lorgayLete69::_hayhairyall
	String_t* ____hayhairyall_26;
	// System.Boolean GarbageiOS.M_lorgayLete69::_nepaw
	bool ____nepaw_27;
	// System.String GarbageiOS.M_lorgayLete69::_sayjallseeDerawou
	String_t* ____sayjallseeDerawou_28;
	// System.Int32 GarbageiOS.M_lorgayLete69::_qoucassirHifurse
	int32_t ____qoucassirHifurse_29;
	// System.Int32 GarbageiOS.M_lorgayLete69::_sikearSewhay
	int32_t ____sikearSewhay_30;
	// System.Boolean GarbageiOS.M_lorgayLete69::_stouxoubeSalgem
	bool ____stouxoubeSalgem_31;
	// System.Boolean GarbageiOS.M_lorgayLete69::_tremfaitasDane
	bool ____tremfaitasDane_32;
	// System.Boolean GarbageiOS.M_lorgayLete69::_neartur
	bool ____neartur_33;
	// System.UInt32 GarbageiOS.M_lorgayLete69::_harto
	uint32_t ____harto_34;
	// System.UInt32 GarbageiOS.M_lorgayLete69::_cete
	uint32_t ____cete_35;
	// System.String GarbageiOS.M_lorgayLete69::_jearpeyall
	String_t* ____jearpeyall_36;
	// System.Single GarbageiOS.M_lorgayLete69::_dorpanoVarwis
	float ____dorpanoVarwis_37;
	// System.Single GarbageiOS.M_lorgayLete69::_benorri
	float ____benorri_38;
	// System.Boolean GarbageiOS.M_lorgayLete69::_herfi
	bool ____herfi_39;
	// System.Int32 GarbageiOS.M_lorgayLete69::_rexi
	int32_t ____rexi_40;
	// System.String GarbageiOS.M_lorgayLete69::_pormee
	String_t* ____pormee_41;
	// System.Single GarbageiOS.M_lorgayLete69::_gempoNanairnel
	float ____gempoNanairnel_42;
	// System.UInt32 GarbageiOS.M_lorgayLete69::_hilepayMayaysel
	uint32_t ____hilepayMayaysel_43;
	// System.String GarbageiOS.M_lorgayLete69::_jikair
	String_t* ____jikair_44;
	// System.Boolean GarbageiOS.M_lorgayLete69::_lallfoubereRearsoumi
	bool ____lallfoubereRearsoumi_45;
	// System.Int32 GarbageiOS.M_lorgayLete69::_zicaday
	int32_t ____zicaday_46;
	// System.Int32 GarbageiOS.M_lorgayLete69::_seke
	int32_t ____seke_47;
	// System.String GarbageiOS.M_lorgayLete69::_roverSealoo
	String_t* ____roverSealoo_48;
	// System.Int32 GarbageiOS.M_lorgayLete69::_sepeji
	int32_t ____sepeji_49;
	// System.UInt32 GarbageiOS.M_lorgayLete69::_yormeredrallHilai
	uint32_t ____yormeredrallHilai_50;
	// System.Int32 GarbageiOS.M_lorgayLete69::_xairrawPijico
	int32_t ____xairrawPijico_51;
	// System.Int32 GarbageiOS.M_lorgayLete69::_guwur
	int32_t ____guwur_52;
	// System.String GarbageiOS.M_lorgayLete69::_rafo
	String_t* ____rafo_53;
	// System.Single GarbageiOS.M_lorgayLete69::_peekuwiKairsor
	float ____peekuwiKairsor_54;
	// System.Boolean GarbageiOS.M_lorgayLete69::_virhorRedu
	bool ____virhorRedu_55;
	// System.Single GarbageiOS.M_lorgayLete69::_duhou
	float ____duhou_56;
	// System.String GarbageiOS.M_lorgayLete69::_yispisooBalltras
	String_t* ____yispisooBalltras_57;

public:
	inline static int32_t get_offset_of__refer_0() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____refer_0)); }
	inline String_t* get__refer_0() const { return ____refer_0; }
	inline String_t** get_address_of__refer_0() { return &____refer_0; }
	inline void set__refer_0(String_t* value)
	{
		____refer_0 = value;
		Il2CppCodeGenWriteBarrier(&____refer_0, value);
	}

	inline static int32_t get_offset_of__stallpugaRestou_1() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____stallpugaRestou_1)); }
	inline String_t* get__stallpugaRestou_1() const { return ____stallpugaRestou_1; }
	inline String_t** get_address_of__stallpugaRestou_1() { return &____stallpugaRestou_1; }
	inline void set__stallpugaRestou_1(String_t* value)
	{
		____stallpugaRestou_1 = value;
		Il2CppCodeGenWriteBarrier(&____stallpugaRestou_1, value);
	}

	inline static int32_t get_offset_of__challjur_2() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____challjur_2)); }
	inline String_t* get__challjur_2() const { return ____challjur_2; }
	inline String_t** get_address_of__challjur_2() { return &____challjur_2; }
	inline void set__challjur_2(String_t* value)
	{
		____challjur_2 = value;
		Il2CppCodeGenWriteBarrier(&____challjur_2, value);
	}

	inline static int32_t get_offset_of__jouqamir_3() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____jouqamir_3)); }
	inline float get__jouqamir_3() const { return ____jouqamir_3; }
	inline float* get_address_of__jouqamir_3() { return &____jouqamir_3; }
	inline void set__jouqamir_3(float value)
	{
		____jouqamir_3 = value;
	}

	inline static int32_t get_offset_of__leartawtrem_4() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____leartawtrem_4)); }
	inline String_t* get__leartawtrem_4() const { return ____leartawtrem_4; }
	inline String_t** get_address_of__leartawtrem_4() { return &____leartawtrem_4; }
	inline void set__leartawtrem_4(String_t* value)
	{
		____leartawtrem_4 = value;
		Il2CppCodeGenWriteBarrier(&____leartawtrem_4, value);
	}

	inline static int32_t get_offset_of__naxere_5() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____naxere_5)); }
	inline bool get__naxere_5() const { return ____naxere_5; }
	inline bool* get_address_of__naxere_5() { return &____naxere_5; }
	inline void set__naxere_5(bool value)
	{
		____naxere_5 = value;
	}

	inline static int32_t get_offset_of__vijeeti_6() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____vijeeti_6)); }
	inline String_t* get__vijeeti_6() const { return ____vijeeti_6; }
	inline String_t** get_address_of__vijeeti_6() { return &____vijeeti_6; }
	inline void set__vijeeti_6(String_t* value)
	{
		____vijeeti_6 = value;
		Il2CppCodeGenWriteBarrier(&____vijeeti_6, value);
	}

	inline static int32_t get_offset_of__vairmaDahurpem_7() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____vairmaDahurpem_7)); }
	inline uint32_t get__vairmaDahurpem_7() const { return ____vairmaDahurpem_7; }
	inline uint32_t* get_address_of__vairmaDahurpem_7() { return &____vairmaDahurpem_7; }
	inline void set__vairmaDahurpem_7(uint32_t value)
	{
		____vairmaDahurpem_7 = value;
	}

	inline static int32_t get_offset_of__zedeBeror_8() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____zedeBeror_8)); }
	inline bool get__zedeBeror_8() const { return ____zedeBeror_8; }
	inline bool* get_address_of__zedeBeror_8() { return &____zedeBeror_8; }
	inline void set__zedeBeror_8(bool value)
	{
		____zedeBeror_8 = value;
	}

	inline static int32_t get_offset_of__tibu_9() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____tibu_9)); }
	inline float get__tibu_9() const { return ____tibu_9; }
	inline float* get_address_of__tibu_9() { return &____tibu_9; }
	inline void set__tibu_9(float value)
	{
		____tibu_9 = value;
	}

	inline static int32_t get_offset_of__siyeKachoulel_10() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____siyeKachoulel_10)); }
	inline uint32_t get__siyeKachoulel_10() const { return ____siyeKachoulel_10; }
	inline uint32_t* get_address_of__siyeKachoulel_10() { return &____siyeKachoulel_10; }
	inline void set__siyeKachoulel_10(uint32_t value)
	{
		____siyeKachoulel_10 = value;
	}

	inline static int32_t get_offset_of__cawtal_11() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____cawtal_11)); }
	inline bool get__cawtal_11() const { return ____cawtal_11; }
	inline bool* get_address_of__cawtal_11() { return &____cawtal_11; }
	inline void set__cawtal_11(bool value)
	{
		____cawtal_11 = value;
	}

	inline static int32_t get_offset_of__jelferefearVowsear_12() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____jelferefearVowsear_12)); }
	inline uint32_t get__jelferefearVowsear_12() const { return ____jelferefearVowsear_12; }
	inline uint32_t* get_address_of__jelferefearVowsear_12() { return &____jelferefearVowsear_12; }
	inline void set__jelferefearVowsear_12(uint32_t value)
	{
		____jelferefearVowsear_12 = value;
	}

	inline static int32_t get_offset_of__juchekoo_13() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____juchekoo_13)); }
	inline bool get__juchekoo_13() const { return ____juchekoo_13; }
	inline bool* get_address_of__juchekoo_13() { return &____juchekoo_13; }
	inline void set__juchekoo_13(bool value)
	{
		____juchekoo_13 = value;
	}

	inline static int32_t get_offset_of__hurmirbou_14() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____hurmirbou_14)); }
	inline int32_t get__hurmirbou_14() const { return ____hurmirbou_14; }
	inline int32_t* get_address_of__hurmirbou_14() { return &____hurmirbou_14; }
	inline void set__hurmirbou_14(int32_t value)
	{
		____hurmirbou_14 = value;
	}

	inline static int32_t get_offset_of__numasWeyomu_15() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____numasWeyomu_15)); }
	inline float get__numasWeyomu_15() const { return ____numasWeyomu_15; }
	inline float* get_address_of__numasWeyomu_15() { return &____numasWeyomu_15; }
	inline void set__numasWeyomu_15(float value)
	{
		____numasWeyomu_15 = value;
	}

	inline static int32_t get_offset_of__zovecaJereno_16() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____zovecaJereno_16)); }
	inline String_t* get__zovecaJereno_16() const { return ____zovecaJereno_16; }
	inline String_t** get_address_of__zovecaJereno_16() { return &____zovecaJereno_16; }
	inline void set__zovecaJereno_16(String_t* value)
	{
		____zovecaJereno_16 = value;
		Il2CppCodeGenWriteBarrier(&____zovecaJereno_16, value);
	}

	inline static int32_t get_offset_of__boumormoo_17() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____boumormoo_17)); }
	inline uint32_t get__boumormoo_17() const { return ____boumormoo_17; }
	inline uint32_t* get_address_of__boumormoo_17() { return &____boumormoo_17; }
	inline void set__boumormoo_17(uint32_t value)
	{
		____boumormoo_17 = value;
	}

	inline static int32_t get_offset_of__joolorSouje_18() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____joolorSouje_18)); }
	inline String_t* get__joolorSouje_18() const { return ____joolorSouje_18; }
	inline String_t** get_address_of__joolorSouje_18() { return &____joolorSouje_18; }
	inline void set__joolorSouje_18(String_t* value)
	{
		____joolorSouje_18 = value;
		Il2CppCodeGenWriteBarrier(&____joolorSouje_18, value);
	}

	inline static int32_t get_offset_of__cirxu_19() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____cirxu_19)); }
	inline uint32_t get__cirxu_19() const { return ____cirxu_19; }
	inline uint32_t* get_address_of__cirxu_19() { return &____cirxu_19; }
	inline void set__cirxu_19(uint32_t value)
	{
		____cirxu_19 = value;
	}

	inline static int32_t get_offset_of__lardrapal_20() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____lardrapal_20)); }
	inline int32_t get__lardrapal_20() const { return ____lardrapal_20; }
	inline int32_t* get_address_of__lardrapal_20() { return &____lardrapal_20; }
	inline void set__lardrapal_20(int32_t value)
	{
		____lardrapal_20 = value;
	}

	inline static int32_t get_offset_of__sisisjir_21() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____sisisjir_21)); }
	inline int32_t get__sisisjir_21() const { return ____sisisjir_21; }
	inline int32_t* get_address_of__sisisjir_21() { return &____sisisjir_21; }
	inline void set__sisisjir_21(int32_t value)
	{
		____sisisjir_21 = value;
	}

	inline static int32_t get_offset_of__ficernal_22() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____ficernal_22)); }
	inline int32_t get__ficernal_22() const { return ____ficernal_22; }
	inline int32_t* get_address_of__ficernal_22() { return &____ficernal_22; }
	inline void set__ficernal_22(int32_t value)
	{
		____ficernal_22 = value;
	}

	inline static int32_t get_offset_of__pirmemla_23() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____pirmemla_23)); }
	inline int32_t get__pirmemla_23() const { return ____pirmemla_23; }
	inline int32_t* get_address_of__pirmemla_23() { return &____pirmemla_23; }
	inline void set__pirmemla_23(int32_t value)
	{
		____pirmemla_23 = value;
	}

	inline static int32_t get_offset_of__remmirxe_24() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____remmirxe_24)); }
	inline bool get__remmirxe_24() const { return ____remmirxe_24; }
	inline bool* get_address_of__remmirxe_24() { return &____remmirxe_24; }
	inline void set__remmirxe_24(bool value)
	{
		____remmirxe_24 = value;
	}

	inline static int32_t get_offset_of__hearmarHerkouwhem_25() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____hearmarHerkouwhem_25)); }
	inline String_t* get__hearmarHerkouwhem_25() const { return ____hearmarHerkouwhem_25; }
	inline String_t** get_address_of__hearmarHerkouwhem_25() { return &____hearmarHerkouwhem_25; }
	inline void set__hearmarHerkouwhem_25(String_t* value)
	{
		____hearmarHerkouwhem_25 = value;
		Il2CppCodeGenWriteBarrier(&____hearmarHerkouwhem_25, value);
	}

	inline static int32_t get_offset_of__hayhairyall_26() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____hayhairyall_26)); }
	inline String_t* get__hayhairyall_26() const { return ____hayhairyall_26; }
	inline String_t** get_address_of__hayhairyall_26() { return &____hayhairyall_26; }
	inline void set__hayhairyall_26(String_t* value)
	{
		____hayhairyall_26 = value;
		Il2CppCodeGenWriteBarrier(&____hayhairyall_26, value);
	}

	inline static int32_t get_offset_of__nepaw_27() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____nepaw_27)); }
	inline bool get__nepaw_27() const { return ____nepaw_27; }
	inline bool* get_address_of__nepaw_27() { return &____nepaw_27; }
	inline void set__nepaw_27(bool value)
	{
		____nepaw_27 = value;
	}

	inline static int32_t get_offset_of__sayjallseeDerawou_28() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____sayjallseeDerawou_28)); }
	inline String_t* get__sayjallseeDerawou_28() const { return ____sayjallseeDerawou_28; }
	inline String_t** get_address_of__sayjallseeDerawou_28() { return &____sayjallseeDerawou_28; }
	inline void set__sayjallseeDerawou_28(String_t* value)
	{
		____sayjallseeDerawou_28 = value;
		Il2CppCodeGenWriteBarrier(&____sayjallseeDerawou_28, value);
	}

	inline static int32_t get_offset_of__qoucassirHifurse_29() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____qoucassirHifurse_29)); }
	inline int32_t get__qoucassirHifurse_29() const { return ____qoucassirHifurse_29; }
	inline int32_t* get_address_of__qoucassirHifurse_29() { return &____qoucassirHifurse_29; }
	inline void set__qoucassirHifurse_29(int32_t value)
	{
		____qoucassirHifurse_29 = value;
	}

	inline static int32_t get_offset_of__sikearSewhay_30() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____sikearSewhay_30)); }
	inline int32_t get__sikearSewhay_30() const { return ____sikearSewhay_30; }
	inline int32_t* get_address_of__sikearSewhay_30() { return &____sikearSewhay_30; }
	inline void set__sikearSewhay_30(int32_t value)
	{
		____sikearSewhay_30 = value;
	}

	inline static int32_t get_offset_of__stouxoubeSalgem_31() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____stouxoubeSalgem_31)); }
	inline bool get__stouxoubeSalgem_31() const { return ____stouxoubeSalgem_31; }
	inline bool* get_address_of__stouxoubeSalgem_31() { return &____stouxoubeSalgem_31; }
	inline void set__stouxoubeSalgem_31(bool value)
	{
		____stouxoubeSalgem_31 = value;
	}

	inline static int32_t get_offset_of__tremfaitasDane_32() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____tremfaitasDane_32)); }
	inline bool get__tremfaitasDane_32() const { return ____tremfaitasDane_32; }
	inline bool* get_address_of__tremfaitasDane_32() { return &____tremfaitasDane_32; }
	inline void set__tremfaitasDane_32(bool value)
	{
		____tremfaitasDane_32 = value;
	}

	inline static int32_t get_offset_of__neartur_33() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____neartur_33)); }
	inline bool get__neartur_33() const { return ____neartur_33; }
	inline bool* get_address_of__neartur_33() { return &____neartur_33; }
	inline void set__neartur_33(bool value)
	{
		____neartur_33 = value;
	}

	inline static int32_t get_offset_of__harto_34() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____harto_34)); }
	inline uint32_t get__harto_34() const { return ____harto_34; }
	inline uint32_t* get_address_of__harto_34() { return &____harto_34; }
	inline void set__harto_34(uint32_t value)
	{
		____harto_34 = value;
	}

	inline static int32_t get_offset_of__cete_35() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____cete_35)); }
	inline uint32_t get__cete_35() const { return ____cete_35; }
	inline uint32_t* get_address_of__cete_35() { return &____cete_35; }
	inline void set__cete_35(uint32_t value)
	{
		____cete_35 = value;
	}

	inline static int32_t get_offset_of__jearpeyall_36() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____jearpeyall_36)); }
	inline String_t* get__jearpeyall_36() const { return ____jearpeyall_36; }
	inline String_t** get_address_of__jearpeyall_36() { return &____jearpeyall_36; }
	inline void set__jearpeyall_36(String_t* value)
	{
		____jearpeyall_36 = value;
		Il2CppCodeGenWriteBarrier(&____jearpeyall_36, value);
	}

	inline static int32_t get_offset_of__dorpanoVarwis_37() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____dorpanoVarwis_37)); }
	inline float get__dorpanoVarwis_37() const { return ____dorpanoVarwis_37; }
	inline float* get_address_of__dorpanoVarwis_37() { return &____dorpanoVarwis_37; }
	inline void set__dorpanoVarwis_37(float value)
	{
		____dorpanoVarwis_37 = value;
	}

	inline static int32_t get_offset_of__benorri_38() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____benorri_38)); }
	inline float get__benorri_38() const { return ____benorri_38; }
	inline float* get_address_of__benorri_38() { return &____benorri_38; }
	inline void set__benorri_38(float value)
	{
		____benorri_38 = value;
	}

	inline static int32_t get_offset_of__herfi_39() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____herfi_39)); }
	inline bool get__herfi_39() const { return ____herfi_39; }
	inline bool* get_address_of__herfi_39() { return &____herfi_39; }
	inline void set__herfi_39(bool value)
	{
		____herfi_39 = value;
	}

	inline static int32_t get_offset_of__rexi_40() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____rexi_40)); }
	inline int32_t get__rexi_40() const { return ____rexi_40; }
	inline int32_t* get_address_of__rexi_40() { return &____rexi_40; }
	inline void set__rexi_40(int32_t value)
	{
		____rexi_40 = value;
	}

	inline static int32_t get_offset_of__pormee_41() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____pormee_41)); }
	inline String_t* get__pormee_41() const { return ____pormee_41; }
	inline String_t** get_address_of__pormee_41() { return &____pormee_41; }
	inline void set__pormee_41(String_t* value)
	{
		____pormee_41 = value;
		Il2CppCodeGenWriteBarrier(&____pormee_41, value);
	}

	inline static int32_t get_offset_of__gempoNanairnel_42() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____gempoNanairnel_42)); }
	inline float get__gempoNanairnel_42() const { return ____gempoNanairnel_42; }
	inline float* get_address_of__gempoNanairnel_42() { return &____gempoNanairnel_42; }
	inline void set__gempoNanairnel_42(float value)
	{
		____gempoNanairnel_42 = value;
	}

	inline static int32_t get_offset_of__hilepayMayaysel_43() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____hilepayMayaysel_43)); }
	inline uint32_t get__hilepayMayaysel_43() const { return ____hilepayMayaysel_43; }
	inline uint32_t* get_address_of__hilepayMayaysel_43() { return &____hilepayMayaysel_43; }
	inline void set__hilepayMayaysel_43(uint32_t value)
	{
		____hilepayMayaysel_43 = value;
	}

	inline static int32_t get_offset_of__jikair_44() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____jikair_44)); }
	inline String_t* get__jikair_44() const { return ____jikair_44; }
	inline String_t** get_address_of__jikair_44() { return &____jikair_44; }
	inline void set__jikair_44(String_t* value)
	{
		____jikair_44 = value;
		Il2CppCodeGenWriteBarrier(&____jikair_44, value);
	}

	inline static int32_t get_offset_of__lallfoubereRearsoumi_45() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____lallfoubereRearsoumi_45)); }
	inline bool get__lallfoubereRearsoumi_45() const { return ____lallfoubereRearsoumi_45; }
	inline bool* get_address_of__lallfoubereRearsoumi_45() { return &____lallfoubereRearsoumi_45; }
	inline void set__lallfoubereRearsoumi_45(bool value)
	{
		____lallfoubereRearsoumi_45 = value;
	}

	inline static int32_t get_offset_of__zicaday_46() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____zicaday_46)); }
	inline int32_t get__zicaday_46() const { return ____zicaday_46; }
	inline int32_t* get_address_of__zicaday_46() { return &____zicaday_46; }
	inline void set__zicaday_46(int32_t value)
	{
		____zicaday_46 = value;
	}

	inline static int32_t get_offset_of__seke_47() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____seke_47)); }
	inline int32_t get__seke_47() const { return ____seke_47; }
	inline int32_t* get_address_of__seke_47() { return &____seke_47; }
	inline void set__seke_47(int32_t value)
	{
		____seke_47 = value;
	}

	inline static int32_t get_offset_of__roverSealoo_48() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____roverSealoo_48)); }
	inline String_t* get__roverSealoo_48() const { return ____roverSealoo_48; }
	inline String_t** get_address_of__roverSealoo_48() { return &____roverSealoo_48; }
	inline void set__roverSealoo_48(String_t* value)
	{
		____roverSealoo_48 = value;
		Il2CppCodeGenWriteBarrier(&____roverSealoo_48, value);
	}

	inline static int32_t get_offset_of__sepeji_49() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____sepeji_49)); }
	inline int32_t get__sepeji_49() const { return ____sepeji_49; }
	inline int32_t* get_address_of__sepeji_49() { return &____sepeji_49; }
	inline void set__sepeji_49(int32_t value)
	{
		____sepeji_49 = value;
	}

	inline static int32_t get_offset_of__yormeredrallHilai_50() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____yormeredrallHilai_50)); }
	inline uint32_t get__yormeredrallHilai_50() const { return ____yormeredrallHilai_50; }
	inline uint32_t* get_address_of__yormeredrallHilai_50() { return &____yormeredrallHilai_50; }
	inline void set__yormeredrallHilai_50(uint32_t value)
	{
		____yormeredrallHilai_50 = value;
	}

	inline static int32_t get_offset_of__xairrawPijico_51() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____xairrawPijico_51)); }
	inline int32_t get__xairrawPijico_51() const { return ____xairrawPijico_51; }
	inline int32_t* get_address_of__xairrawPijico_51() { return &____xairrawPijico_51; }
	inline void set__xairrawPijico_51(int32_t value)
	{
		____xairrawPijico_51 = value;
	}

	inline static int32_t get_offset_of__guwur_52() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____guwur_52)); }
	inline int32_t get__guwur_52() const { return ____guwur_52; }
	inline int32_t* get_address_of__guwur_52() { return &____guwur_52; }
	inline void set__guwur_52(int32_t value)
	{
		____guwur_52 = value;
	}

	inline static int32_t get_offset_of__rafo_53() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____rafo_53)); }
	inline String_t* get__rafo_53() const { return ____rafo_53; }
	inline String_t** get_address_of__rafo_53() { return &____rafo_53; }
	inline void set__rafo_53(String_t* value)
	{
		____rafo_53 = value;
		Il2CppCodeGenWriteBarrier(&____rafo_53, value);
	}

	inline static int32_t get_offset_of__peekuwiKairsor_54() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____peekuwiKairsor_54)); }
	inline float get__peekuwiKairsor_54() const { return ____peekuwiKairsor_54; }
	inline float* get_address_of__peekuwiKairsor_54() { return &____peekuwiKairsor_54; }
	inline void set__peekuwiKairsor_54(float value)
	{
		____peekuwiKairsor_54 = value;
	}

	inline static int32_t get_offset_of__virhorRedu_55() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____virhorRedu_55)); }
	inline bool get__virhorRedu_55() const { return ____virhorRedu_55; }
	inline bool* get_address_of__virhorRedu_55() { return &____virhorRedu_55; }
	inline void set__virhorRedu_55(bool value)
	{
		____virhorRedu_55 = value;
	}

	inline static int32_t get_offset_of__duhou_56() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____duhou_56)); }
	inline float get__duhou_56() const { return ____duhou_56; }
	inline float* get_address_of__duhou_56() { return &____duhou_56; }
	inline void set__duhou_56(float value)
	{
		____duhou_56 = value;
	}

	inline static int32_t get_offset_of__yispisooBalltras_57() { return static_cast<int32_t>(offsetof(M_lorgayLete69_t3973278861, ____yispisooBalltras_57)); }
	inline String_t* get__yispisooBalltras_57() const { return ____yispisooBalltras_57; }
	inline String_t** get_address_of__yispisooBalltras_57() { return &____yispisooBalltras_57; }
	inline void set__yispisooBalltras_57(String_t* value)
	{
		____yispisooBalltras_57 = value;
		Il2CppCodeGenWriteBarrier(&____yispisooBalltras_57, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
