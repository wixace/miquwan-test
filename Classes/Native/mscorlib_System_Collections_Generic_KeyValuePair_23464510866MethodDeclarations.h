﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2519471770(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3464510866 *, String_t*, Dictionary_2_t2745311790 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>::get_Key()
#define KeyValuePair_2_get_Key_m2412132238(__this, method) ((  String_t* (*) (KeyValuePair_2_t3464510866 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3548515151(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3464510866 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>::get_Value()
#define KeyValuePair_2_get_Value_m138930354(__this, method) ((  Dictionary_2_t2745311790 * (*) (KeyValuePair_2_t3464510866 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3594067919(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3464510866 *, Dictionary_2_t2745311790 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>::ToString()
#define KeyValuePair_2_ToString_m592570393(__this, method) ((  String_t* (*) (KeyValuePair_2_t3464510866 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
