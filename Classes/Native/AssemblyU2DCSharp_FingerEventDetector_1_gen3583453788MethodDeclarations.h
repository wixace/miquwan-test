﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FingerEventDetector_1_gen3103176768MethodDeclarations.h"

// System.Void FingerEventDetector`1<FingerDownEvent>::.ctor()
#define FingerEventDetector_1__ctor_m2416834488(__this, method) ((  void (*) (FingerEventDetector_1_t3583453788 *, const MethodInfo*))FingerEventDetector_1__ctor_m3763465287_gshared)(__this, method)
// T FingerEventDetector`1<FingerDownEvent>::CreateFingerEvent()
#define FingerEventDetector_1_CreateFingerEvent_m1514829644(__this, method) ((  FingerDownEvent_t356126095 * (*) (FingerEventDetector_1_t3583453788 *, const MethodInfo*))FingerEventDetector_1_CreateFingerEvent_m1420222875_gshared)(__this, method)
// System.Type FingerEventDetector`1<FingerDownEvent>::GetEventType()
#define FingerEventDetector_1_GetEventType_m1030125028(__this, method) ((  Type_t * (*) (FingerEventDetector_1_t3583453788 *, const MethodInfo*))FingerEventDetector_1_GetEventType_m3851890933_gshared)(__this, method)
// System.Void FingerEventDetector`1<FingerDownEvent>::Start()
#define FingerEventDetector_1_Start_m1363972280(__this, method) ((  void (*) (FingerEventDetector_1_t3583453788 *, const MethodInfo*))FingerEventDetector_1_Start_m2710603079_gshared)(__this, method)
// System.Void FingerEventDetector`1<FingerDownEvent>::OnDestroy()
#define FingerEventDetector_1_OnDestroy_m4096602289(__this, method) ((  void (*) (FingerEventDetector_1_t3583453788 *, const MethodInfo*))FingerEventDetector_1_OnDestroy_m3778430400_gshared)(__this, method)
// System.Void FingerEventDetector`1<FingerDownEvent>::FingerGestures_OnInputProviderChanged()
#define FingerEventDetector_1_FingerGestures_OnInputProviderChanged_m3005543866(__this, method) ((  void (*) (FingerEventDetector_1_t3583453788 *, const MethodInfo*))FingerEventDetector_1_FingerGestures_OnInputProviderChanged_m572190281_gshared)(__this, method)
// System.Void FingerEventDetector`1<FingerDownEvent>::Init()
#define FingerEventDetector_1_Init_m306593692(__this, method) ((  void (*) (FingerEventDetector_1_t3583453788 *, const MethodInfo*))FingerEventDetector_1_Init_m2289696045_gshared)(__this, method)
// System.Void FingerEventDetector`1<FingerDownEvent>::Init(System.Int32)
#define FingerEventDetector_1_Init_m2252696301(__this, ___fingersCount0, method) ((  void (*) (FingerEventDetector_1_t3583453788 *, int32_t, const MethodInfo*))FingerEventDetector_1_Init_m469522174_gshared)(__this, ___fingersCount0, method)
// T FingerEventDetector`1<FingerDownEvent>::GetEvent(FingerGestures/Finger)
#define FingerEventDetector_1_GetEvent_m176509918(__this, ___finger0, method) ((  FingerDownEvent_t356126095 * (*) (FingerEventDetector_1_t3583453788 *, Finger_t182428197 *, const MethodInfo*))FingerEventDetector_1_GetEvent_m906084269_gshared)(__this, ___finger0, method)
// T FingerEventDetector`1<FingerDownEvent>::GetEvent(System.Int32)
#define FingerEventDetector_1_GetEvent_m4179879296(__this, ___fingerIndex0, method) ((  FingerDownEvent_t356126095 * (*) (FingerEventDetector_1_t3583453788 *, int32_t, const MethodInfo*))FingerEventDetector_1_GetEvent_m3248170193_gshared)(__this, ___fingerIndex0, method)
// FingerGestures FingerEventDetector`1<FingerDownEvent>::ilo_get_Instance1()
#define FingerEventDetector_1_ilo_get_Instance1_m156327916(__this /* static, unused */, method) ((  FingerGestures_t2907604723 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))FingerEventDetector_1_ilo_get_Instance1_m1581142907_gshared)(__this /* static, unused */, method)
// FingerGestures/Finger FingerEventDetector`1<FingerDownEvent>::ilo_GetFinger2(System.Int32)
#define FingerEventDetector_1_ilo_GetFinger2_m1671587601(__this /* static, unused */, ___index0, method) ((  Finger_t182428197 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))FingerEventDetector_1_ilo_GetFinger2_m3652595042_gshared)(__this /* static, unused */, ___index0, method)
