﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UITableGenerated/<UITable_onReposition_GetDelegate_member8_arg0>c__AnonStoreyCF
struct U3CUITable_onReposition_GetDelegate_member8_arg0U3Ec__AnonStoreyCF_t3597620520;

#include "codegen/il2cpp-codegen.h"

// System.Void UITableGenerated/<UITable_onReposition_GetDelegate_member8_arg0>c__AnonStoreyCF::.ctor()
extern "C"  void U3CUITable_onReposition_GetDelegate_member8_arg0U3Ec__AnonStoreyCF__ctor_m3275128643 (U3CUITable_onReposition_GetDelegate_member8_arg0U3Ec__AnonStoreyCF_t3597620520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITableGenerated/<UITable_onReposition_GetDelegate_member8_arg0>c__AnonStoreyCF::<>m__163()
extern "C"  void U3CUITable_onReposition_GetDelegate_member8_arg0U3Ec__AnonStoreyCF_U3CU3Em__163_m3415292228 (U3CUITable_onReposition_GetDelegate_member8_arg0U3Ec__AnonStoreyCF_t3597620520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
