﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// AllUpdate
struct AllUpdate_t1625637290;
// System.Collections.Generic.Dictionary`2<System.Int32,UpdateForJs>
struct Dictionary_2_t1068456464;
// System.Collections.Generic.List`1<IZUpdate>
struct List_1_t555261994;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllUpdate
struct  AllUpdate_t1625637290  : public MonoBehaviour_t667441552
{
public:
	// System.Collections.Generic.List`1<IZUpdate> AllUpdate::updateList
	List_1_t555261994 * ___updateList_6;
	// System.Collections.Generic.List`1<IZUpdate> AllUpdate::removeList
	List_1_t555261994 * ___removeList_7;

public:
	inline static int32_t get_offset_of_updateList_6() { return static_cast<int32_t>(offsetof(AllUpdate_t1625637290, ___updateList_6)); }
	inline List_1_t555261994 * get_updateList_6() const { return ___updateList_6; }
	inline List_1_t555261994 ** get_address_of_updateList_6() { return &___updateList_6; }
	inline void set_updateList_6(List_1_t555261994 * value)
	{
		___updateList_6 = value;
		Il2CppCodeGenWriteBarrier(&___updateList_6, value);
	}

	inline static int32_t get_offset_of_removeList_7() { return static_cast<int32_t>(offsetof(AllUpdate_t1625637290, ___removeList_7)); }
	inline List_1_t555261994 * get_removeList_7() const { return ___removeList_7; }
	inline List_1_t555261994 ** get_address_of_removeList_7() { return &___removeList_7; }
	inline void set_removeList_7(List_1_t555261994 * value)
	{
		___removeList_7 = value;
		Il2CppCodeGenWriteBarrier(&___removeList_7, value);
	}
};

struct AllUpdate_t1625637290_StaticFields
{
public:
	// AllUpdate AllUpdate::_allUpdate
	AllUpdate_t1625637290 * ____allUpdate_4;
	// System.Collections.Generic.Dictionary`2<System.Int32,UpdateForJs> AllUpdate::_jsUpdateDic
	Dictionary_2_t1068456464 * ____jsUpdateDic_5;

public:
	inline static int32_t get_offset_of__allUpdate_4() { return static_cast<int32_t>(offsetof(AllUpdate_t1625637290_StaticFields, ____allUpdate_4)); }
	inline AllUpdate_t1625637290 * get__allUpdate_4() const { return ____allUpdate_4; }
	inline AllUpdate_t1625637290 ** get_address_of__allUpdate_4() { return &____allUpdate_4; }
	inline void set__allUpdate_4(AllUpdate_t1625637290 * value)
	{
		____allUpdate_4 = value;
		Il2CppCodeGenWriteBarrier(&____allUpdate_4, value);
	}

	inline static int32_t get_offset_of__jsUpdateDic_5() { return static_cast<int32_t>(offsetof(AllUpdate_t1625637290_StaticFields, ____jsUpdateDic_5)); }
	inline Dictionary_2_t1068456464 * get__jsUpdateDic_5() const { return ____jsUpdateDic_5; }
	inline Dictionary_2_t1068456464 ** get_address_of__jsUpdateDic_5() { return &____jsUpdateDic_5; }
	inline void set__jsUpdateDic_5(Dictionary_2_t1068456464 * value)
	{
		____jsUpdateDic_5 = value;
		Il2CppCodeGenWriteBarrier(&____jsUpdateDic_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
