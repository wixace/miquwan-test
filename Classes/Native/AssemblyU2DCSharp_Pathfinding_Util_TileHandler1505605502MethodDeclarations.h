﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Util.TileHandler
struct TileHandler_t1505605502;
// Pathfinding.RecastGraph
struct RecastGraph_t2197443166;
// Pathfinding.Util.TileHandler/TileType
struct TileType_t2364590013;
// UnityEngine.Mesh
struct Mesh_t4241756145;
// Pathfinding.Int3[]
struct Int3U5BU5D_t516284607;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// Pathfinding.Poly2Tri.TriangulationPoint
struct TriangulationPoint_t3810082933;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.NavmeshCut
struct NavmeshCut_t300992648;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>
struct List_1_t1767529987;
// System.Collections.Generic.List`1<Pathfinding.NavmeshAdd>
struct List_1_t1669175735;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_RecastGraph2197443166.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_TileHandler_CutM440622000.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3810082933.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_TileHandler_Til2364590013.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_TileHandler1505605502.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_AstarPath_AstarWorkItem2566693888.h"
#include "AssemblyU2DCSharp_Pathfinding_IntRect3015058261.h"
#include "AssemblyU2DCSharp_Pathfinding_NavmeshCut300992648.h"

// System.Void Pathfinding.Util.TileHandler::.ctor(Pathfinding.RecastGraph)
extern "C"  void TileHandler__ctor_m1277850751 (TileHandler_t1505605502 * __this, RecastGraph_t2197443166 * ___graph0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RecastGraph Pathfinding.Util.TileHandler::get_graph()
extern "C"  RecastGraph_t2197443166 * TileHandler_get_graph_m815156995 (TileHandler_t1505605502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Util.TileHandler::GetActiveRotation(Pathfinding.Int2)
extern "C"  int32_t TileHandler_GetActiveRotation_m3190445118 (TileHandler_t1505605502 * __this, Int2_t1974045593  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Util.TileHandler/TileType Pathfinding.Util.TileHandler::GetTileType(System.Int32)
extern "C"  TileType_t2364590013 * TileHandler_GetTileType_m913592056 (TileHandler_t1505605502 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Util.TileHandler::GetTileTypeCount()
extern "C"  int32_t TileHandler_GetTileTypeCount_m4043786530 (TileHandler_t1505605502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Util.TileHandler/TileType Pathfinding.Util.TileHandler::RegisterTileType(UnityEngine.Mesh,Pathfinding.Int3,System.Int32,System.Int32)
extern "C"  TileType_t2364590013 * TileHandler_RegisterTileType_m3105866392 (TileHandler_t1505605502 * __this, Mesh_t4241756145 * ___source0, Int3_t1974045594  ___centerOffset1, int32_t ___width2, int32_t ___depth3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Util.TileHandler::CreateTileTypesFromGraph()
extern "C"  void TileHandler_CreateTileTypesFromGraph_m1433343128 (TileHandler_t1505605502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Util.TileHandler::StartBatchLoad()
extern "C"  bool TileHandler_StartBatchLoad_m4291185109 (TileHandler_t1505605502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Util.TileHandler::EndBatchLoad()
extern "C"  void TileHandler_EndBatchLoad_m3246021768 (TileHandler_t1505605502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Util.TileHandler::CutPoly(Pathfinding.Int3[],System.Int32[],Pathfinding.Int3[]&,System.Int32[]&,System.Int32&,System.Int32&,Pathfinding.Int3[],Pathfinding.Int3,UnityEngine.Bounds,Pathfinding.Util.TileHandler/CutMode,System.Int32)
extern "C"  void TileHandler_CutPoly_m1953827530 (TileHandler_t1505605502 * __this, Int3U5BU5D_t516284607* ___verts0, Int32U5BU5D_t3230847821* ___tris1, Int3U5BU5D_t516284607** ___outVertsArr2, Int32U5BU5D_t3230847821** ___outTrisArr3, int32_t* ___outVCount4, int32_t* ___outTCount5, Int3U5BU5D_t516284607* ___extraShape6, Int3_t1974045594  ___cuttingOffset7, Bounds_t2711641849  ___realBounds8, int32_t ___mode9, int32_t ___perturbate10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Util.TileHandler::DelaunayRefinement(Pathfinding.Int3[],System.Int32[],System.Int32&,System.Int32&,System.Boolean,System.Boolean,Pathfinding.Int3)
extern "C"  void TileHandler_DelaunayRefinement_m1050320318 (TileHandler_t1505605502 * __this, Int3U5BU5D_t516284607* ___verts0, Int32U5BU5D_t3230847821* ___tris1, int32_t* ___vCount2, int32_t* ___tCount3, bool ___delaunay4, bool ___colinear5, Int3_t1974045594  ___worldOffset6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.Util.TileHandler::Point2D2V3(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  Vector3_t4282066566  TileHandler_Point2D2V3_m2923893768 (TileHandler_t1505605502 * __this, TriangulationPoint_t3810082933 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.Util.TileHandler::IntPoint2Int3(Pathfinding.ClipperLib.IntPoint)
extern "C"  Int3_t1974045594  TileHandler_IntPoint2Int3_m2483852000 (TileHandler_t1505605502 * __this, IntPoint_t3326126179  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Util.TileHandler::ClearTile(System.Int32,System.Int32)
extern "C"  void TileHandler_ClearTile_m1638101100 (TileHandler_t1505605502 * __this, int32_t ___x0, int32_t ___z1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Util.TileHandler::ReloadInBounds(UnityEngine.Bounds)
extern "C"  void TileHandler_ReloadInBounds_m1225205354 (TileHandler_t1505605502 * __this, Bounds_t2711641849  ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Util.TileHandler::ReloadTile(System.Int32,System.Int32)
extern "C"  void TileHandler_ReloadTile_m1114833564 (TileHandler_t1505605502 * __this, int32_t ___x0, int32_t ___z1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Util.TileHandler::CutShapeWithTile(System.Int32,System.Int32,Pathfinding.Int3[],Pathfinding.Int3[]&,System.Int32[]&,System.Int32&,System.Int32&)
extern "C"  void TileHandler_CutShapeWithTile_m3045918173 (TileHandler_t1505605502 * __this, int32_t ___x0, int32_t ___z1, Int3U5BU5D_t516284607* ___shape2, Int3U5BU5D_t516284607** ___verts3, Int32U5BU5D_t3230847821** ___tris4, int32_t* ___vCount5, int32_t* ___tCount6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Util.TileHandler::LoadTile(Pathfinding.Util.TileHandler/TileType,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void TileHandler_LoadTile_m642410206 (TileHandler_t1505605502 * __this, TileType_t2364590013 * ___tile0, int32_t ___x1, int32_t ___z2, int32_t ___rotation3, int32_t ___yoffset4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Util.TileHandler::<StartBatchLoad>m__355(System.Boolean)
extern "C"  bool TileHandler_U3CStartBatchLoadU3Em__355_m421046648 (TileHandler_t1505605502 * __this, bool ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Util.TileHandler::<EndBatchLoad>m__356(System.Boolean)
extern "C"  bool TileHandler_U3CEndBatchLoadU3Em__356_m910710386 (TileHandler_t1505605502 * __this, bool ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RecastGraph Pathfinding.Util.TileHandler::ilo_get_graph1(Pathfinding.Util.TileHandler)
extern "C"  RecastGraph_t2197443166 * TileHandler_ilo_get_graph1_m14262729 (Il2CppObject * __this /* static, unused */, TileHandler_t1505605502 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.Util.TileHandler::ilo_op_Multiply2(Pathfinding.Int3,System.Single)
extern "C"  Int3_t1974045594  TileHandler_ilo_op_Multiply2_m2653254092 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, float ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds Pathfinding.Util.TileHandler::ilo_GetTileBounds3(Pathfinding.RecastGraph,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  Bounds_t2711641849  TileHandler_ilo_GetTileBounds3_m3412291327 (Il2CppObject * __this /* static, unused */, RecastGraph_t2197443166 * ____this0, int32_t ___x1, int32_t ___z2, int32_t ___width3, int32_t ___depth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.Util.TileHandler::ilo_op_Addition4(Pathfinding.Int3,Pathfinding.Int3)
extern "C"  Int3_t1974045594  TileHandler_ilo_op_Addition4_m4208912199 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, Int3_t1974045594  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Util.TileHandler::ilo_AddWorkItem5(AstarPath,AstarPath/AstarWorkItem)
extern "C"  void TileHandler_ilo_AddWorkItem5_m1099779552 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, AstarWorkItem_t2566693888  ___itm1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.IntRect Pathfinding.Util.TileHandler::ilo_ExpandToContain6(Pathfinding.IntRect&,System.Int32,System.Int32)
extern "C"  IntRect_t3015058261  TileHandler_ilo_ExpandToContain6_m2420691322 (Il2CppObject * __this /* static, unused */, IntRect_t3015058261 * ____this0, int32_t ___x1, int32_t ___y2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds Pathfinding.Util.TileHandler::ilo_GetBounds7(Pathfinding.NavmeshCut)
extern "C"  Bounds_t2711641849  TileHandler_ilo_GetBounds7_m2185677719 (Il2CppObject * __this /* static, unused */, NavmeshCut_t300992648 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Util.TileHandler::ilo_GetContour8(Pathfinding.NavmeshCut,System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>)
extern "C"  void TileHandler_ilo_GetContour8_m1326858015 (Il2CppObject * __this /* static, unused */, NavmeshCut_t300992648 * ____this0, List_1_t1767529987 * ___buffer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Pathfinding.NavmeshAdd> Pathfinding.Util.TileHandler::ilo_GetAllInRange9(UnityEngine.Bounds)
extern "C"  List_1_t1669175735 * TileHandler_ilo_GetAllInRange9_m1535888132 (Il2CppObject * __this /* static, unused */, Bounds_t2711641849  ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.Util.TileHandler::ilo_get_zero10()
extern "C"  Int3_t1974045594  TileHandler_ilo_get_zero10_m4051994357 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Util.TileHandler::ilo_UsedForCut11(Pathfinding.NavmeshCut)
extern "C"  void TileHandler_ilo_UsedForCut11_m1398206224 (Il2CppObject * __this /* static, unused */, NavmeshCut_t300992648 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Util.TileHandler::ilo_LeftNotColinear12(Pathfinding.Int3,Pathfinding.Int3,Pathfinding.Int3)
extern "C"  bool TileHandler_ilo_LeftNotColinear12_m3840861608 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___a0, Int3_t1974045594  ___b1, Int3_t1974045594  ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.Util.TileHandler::ilo_op_Subtraction13(Pathfinding.Int3,Pathfinding.Int3)
extern "C"  Int3_t1974045594  TileHandler_ilo_op_Subtraction13_m2627986601 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, Int3_t1974045594  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Util.TileHandler::ilo_IsValid14(Pathfinding.IntRect&)
extern "C"  bool TileHandler_ilo_IsValid14_m1554347664 (Il2CppObject * __this /* static, unused */, IntRect_t3015058261 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Util.TileHandler::ilo_Load15(Pathfinding.Util.TileHandler/TileType,Pathfinding.Int3[]&,System.Int32[]&,System.Int32,System.Int32)
extern "C"  void TileHandler_ilo_Load15_m388618946 (Il2CppObject * __this /* static, unused */, TileType_t2364590013 * ____this0, Int3U5BU5D_t516284607** ___verts1, Int32U5BU5D_t3230847821** ___tris2, int32_t ___rotation3, int32_t ___yoffset4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.Util.TileHandler::ilo_op_UnaryNegation16(Pathfinding.Int3)
extern "C"  Int3_t1974045594  TileHandler_ilo_op_UnaryNegation16_m2459179570 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Util.TileHandler::ilo_CutPoly17(Pathfinding.Util.TileHandler,Pathfinding.Int3[],System.Int32[],Pathfinding.Int3[]&,System.Int32[]&,System.Int32&,System.Int32&,Pathfinding.Int3[],Pathfinding.Int3,UnityEngine.Bounds,Pathfinding.Util.TileHandler/CutMode,System.Int32)
extern "C"  void TileHandler_ilo_CutPoly17_m1983841853 (Il2CppObject * __this /* static, unused */, TileHandler_t1505605502 * ____this0, Int3U5BU5D_t516284607* ___verts1, Int32U5BU5D_t3230847821* ___tris2, Int3U5BU5D_t516284607** ___outVertsArr3, Int32U5BU5D_t3230847821** ___outTrisArr4, int32_t* ___outVCount5, int32_t* ___outTCount6, Int3U5BU5D_t516284607* ___extraShape7, Int3_t1974045594  ___cuttingOffset8, Bounds_t2711641849  ___realBounds9, int32_t ___mode10, int32_t ___perturbate11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
