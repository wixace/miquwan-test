﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.NavGraph/<CountNodes>c__AnonStorey113
struct U3CCountNodesU3Ec__AnonStorey113_t3953095078;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.NavGraph/<CountNodes>c__AnonStorey113::.ctor()
extern "C"  void U3CCountNodesU3Ec__AnonStorey113__ctor_m2677347973 (U3CCountNodesU3Ec__AnonStorey113_t3953095078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavGraph/<CountNodes>c__AnonStorey113::<>m__346(Pathfinding.GraphNode)
extern "C"  bool U3CCountNodesU3Ec__AnonStorey113_U3CU3Em__346_m1550777335 (U3CCountNodesU3Ec__AnonStorey113_t3953095078 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
