﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameObjectVisitorGenerated/<GameObjectVisitor_FindChildInCondition_GetDelegate_member1_arg1>c__AnonStorey63
struct U3CGameObjectVisitor_FindChildInCondition_GetDelegate_member1_arg1U3Ec__AnonStorey63_t2873939672;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void GameObjectVisitorGenerated/<GameObjectVisitor_FindChildInCondition_GetDelegate_member1_arg1>c__AnonStorey63::.ctor()
extern "C"  void U3CGameObjectVisitor_FindChildInCondition_GetDelegate_member1_arg1U3Ec__AnonStorey63__ctor_m2367027091 (U3CGameObjectVisitor_FindChildInCondition_GetDelegate_member1_arg1U3Ec__AnonStorey63_t2873939672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameObjectVisitorGenerated/<GameObjectVisitor_FindChildInCondition_GetDelegate_member1_arg1>c__AnonStorey63::<>m__58(UnityEngine.GameObject)
extern "C"  bool U3CGameObjectVisitor_FindChildInCondition_GetDelegate_member1_arg1U3Ec__AnonStorey63_U3CU3Em__58_m2488380011 (U3CGameObjectVisitor_FindChildInCondition_GetDelegate_member1_arg1U3Ec__AnonStorey63_t2873939672 * __this, GameObject_t3674682005 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
