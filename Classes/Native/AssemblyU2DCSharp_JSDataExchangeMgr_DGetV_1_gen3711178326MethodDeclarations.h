﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr_DGetV_1_gen4048467712MethodDeclarations.h"

// System.Void JSDataExchangeMgr/DGetV`1<GameObjectVisitor/condition>::.ctor(System.Object,System.IntPtr)
#define DGetV_1__ctor_m4015242672(__this, ___object0, ___method1, method) ((  void (*) (DGetV_1_t3711178326 *, Il2CppObject *, IntPtr_t, const MethodInfo*))DGetV_1__ctor_m3356914363_gshared)(__this, ___object0, ___method1, method)
// T JSDataExchangeMgr/DGetV`1<GameObjectVisitor/condition>::Invoke()
#define DGetV_1_Invoke_m3148601737(__this, method) ((  condition_t3833526985 * (*) (DGetV_1_t3711178326 *, const MethodInfo*))DGetV_1_Invoke_m1509146772_gshared)(__this, method)
// System.IAsyncResult JSDataExchangeMgr/DGetV`1<GameObjectVisitor/condition>::BeginInvoke(System.AsyncCallback,System.Object)
#define DGetV_1_BeginInvoke_m125051577(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (DGetV_1_t3711178326 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))DGetV_1_BeginInvoke_m2681184398_gshared)(__this, ___callback0, ___object1, method)
// T JSDataExchangeMgr/DGetV`1<GameObjectVisitor/condition>::EndInvoke(System.IAsyncResult)
#define DGetV_1_EndInvoke_m3648127487(__this, ___result0, method) ((  condition_t3833526985 * (*) (DGetV_1_t3711178326 *, Il2CppObject *, const MethodInfo*))DGetV_1_EndInvoke_m1776565962_gshared)(__this, ___result0, method)
