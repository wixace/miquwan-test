﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_rallcaTanirzear68
struct M_rallcaTanirzear68_t1718997467;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_rallcaTanirzear681718997467.h"

// System.Void GarbageiOS.M_rallcaTanirzear68::.ctor()
extern "C"  void M_rallcaTanirzear68__ctor_m4057767400 (M_rallcaTanirzear68_t1718997467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rallcaTanirzear68::M_dorrasBanelha0(System.String[],System.Int32)
extern "C"  void M_rallcaTanirzear68_M_dorrasBanelha0_m4187226979 (M_rallcaTanirzear68_t1718997467 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rallcaTanirzear68::M_nalearlere1(System.String[],System.Int32)
extern "C"  void M_rallcaTanirzear68_M_nalearlere1_m3084425139 (M_rallcaTanirzear68_t1718997467 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rallcaTanirzear68::ilo_M_dorrasBanelha01(GarbageiOS.M_rallcaTanirzear68,System.String[],System.Int32)
extern "C"  void M_rallcaTanirzear68_ilo_M_dorrasBanelha01_m2972308412 (Il2CppObject * __this /* static, unused */, M_rallcaTanirzear68_t1718997467 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
