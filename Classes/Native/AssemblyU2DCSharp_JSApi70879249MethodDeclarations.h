﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSApi
struct JSApi_t70879249;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;
// JSApi/JSErrorReporter
struct JSErrorReporter_t2324290114;
// JSApi/CSEntry
struct CSEntry_t1794636260;
// JSApi/JSNative
struct JSNative_t3654534910;
// JSApi/OnObjCollected
struct OnObjCollected_t2008609135;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.Object
struct Il2CppObject;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSApi_JSErrorReporter2324290114.h"
#include "AssemblyU2DCSharp_JSApi_CSEntry1794636260.h"
#include "AssemblyU2DCSharp_JSApi_JSNative3654534910.h"
#include "AssemblyU2DCSharp_JSApi_OnObjCollected2008609135.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void JSApi::.ctor()
extern "C"  void JSApi__ctor_m282444922 (JSApi_t70879249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::.cctor()
extern "C"  void JSApi__cctor_m3978729075 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::updateDebugger()
extern "C"  void JSApi_updateDebugger_m514044852 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::cleanupDebugger()
extern "C"  void JSApi_cleanupDebugger_m3340369565 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::enableDebugger(System.String[],System.UInt32,System.Int32)
extern "C"  void JSApi_enableDebugger_m184605681 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t4054002952* ___paths0, uint32_t ___nums1, int32_t ___port2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSApi::attachFinalizerObject(System.Int32)
extern "C"  bool JSApi_attachFinalizerObject_m1661831595 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::createJSClassObject(System.String)
extern "C"  int32_t JSApi_createJSClassObject_m2448807338 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::newJSClassObject(System.String)
extern "C"  int32_t JSApi_newJSClassObject_m1017561336 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::InitJSEngine(JSApi/JSErrorReporter,JSApi/CSEntry,JSApi/JSNative,JSApi/OnObjCollected,JSApi/JSNative)
extern "C"  int32_t JSApi_InitJSEngine_m2517478328 (Il2CppObject * __this /* static, unused */, JSErrorReporter_t2324290114 * ___er0, CSEntry_t1794636260 * ___csEntry1, JSNative_t3654534910 * ___req2, OnObjCollected_t2008609135 * ___onObjCollected3, JSNative_t3654534910 * ___print4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::initErrorHandler()
extern "C"  int32_t JSApi_initErrorHandler_m3711480430 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::ShutdownJSEngine(System.Int32)
extern "C"  void JSApi_ShutdownJSEngine_m818184668 (Il2CppObject * __this /* static, unused */, int32_t ___bCleanup0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::reportError(System.String)
extern "C"  void JSApi_reportError_m3659430358 (Il2CppObject * __this /* static, unused */, String_t* ___err0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::getErroReportLineNo(System.IntPtr)
extern "C"  int32_t JSApi_getErroReportLineNo_m433660165 (Il2CppObject * __this /* static, unused */, IntPtr_t ___report0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr JSApi::getErroReportFileName(System.IntPtr)
extern "C"  IntPtr_t JSApi_getErroReportFileName_m455326700 (Il2CppObject * __this /* static, unused */, IntPtr_t ___report0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSApi::getErroReportFileNameS(System.IntPtr)
extern "C"  String_t* JSApi_getErroReportFileNameS_m1227313685 (Il2CppObject * __this /* static, unused */, IntPtr_t ___report0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::getArgIndex()
extern "C"  int32_t JSApi_getArgIndex_m1302028440 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setArgIndex(System.Int32)
extern "C"  void JSApi_setArgIndex_m2841715687 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::incArgIndex()
extern "C"  int32_t JSApi_incArgIndex_m2360368928 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 JSApi::getTag(System.Int32)
extern "C"  uint32_t JSApi_getTag_m3719299556 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 JSApi::getChar(System.Int32)
extern "C"  int16_t JSApi_getChar_m670361373 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char JSApi::getSByte(System.Int32)
extern "C"  Il2CppChar JSApi_getSByte_m4145907490 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte JSApi::getByte(System.Int32)
extern "C"  uint8_t JSApi_getByte_m2331192147 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 JSApi::getInt16(System.Int32)
extern "C"  int16_t JSApi_getInt16_m2435221841 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 JSApi::getUInt16(System.Int32)
extern "C"  uint16_t JSApi_getUInt16_m479914739 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::getInt32(System.Int32)
extern "C"  int32_t JSApi_getInt32_m2845440709 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 JSApi::getUInt32(System.Int32)
extern "C"  uint32_t JSApi_getUInt32_m1396667955 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 JSApi::getInt64(System.Int32)
extern "C"  int64_t JSApi_getInt64_m3295197443 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 JSApi::getUInt64(System.Int32)
extern "C"  uint64_t JSApi_getUInt64_m898865171 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::getEnum(System.Int32)
extern "C"  int32_t JSApi_getEnum_m2588869518 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSApi::getSingle(System.Int32)
extern "C"  float JSApi_getSingle_m2910343699 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double JSApi::getDouble(System.Int32)
extern "C"  double JSApi_getDouble_m3936042291 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 JSApi::getIntPtr(System.Int32)
extern "C"  int64_t JSApi_getIntPtr_m72754061 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::getBoolean(System.Int32)
extern "C"  int32_t JSApi_getBoolean_m1750191519 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSApi::getBooleanS(System.Int32)
extern "C"  bool JSApi_getBooleanS_m3718804830 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr JSApi::getString(System.Int32)
extern "C"  IntPtr_t JSApi_getString_m4082708229 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSApi::getStringS(System.Int32)
extern "C"  String_t* JSApi_getStringS_m3405527972 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::getVector2(System.Int32)
extern "C"  void JSApi_getVector2_m2947447892 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::getVector3(System.Int32)
extern "C"  void JSApi_getVector3_m160032405 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSApi::getObjX()
extern "C"  float JSApi_getObjX_m982764987 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSApi::getObjY()
extern "C"  float JSApi_getObjY_m982765948 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSApi::getObjZ()
extern "C"  float JSApi_getObjZ_m982766909 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 JSApi::getVector2S(System.Int32)
extern "C"  Vector2_t4282066565  JSApi_getVector2S_m3055596030 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 JSApi::getVector3S(System.Int32)
extern "C"  Vector3_t4282066566  JSApi_getVector3S_m3570553852 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::getObject(System.Int32)
extern "C"  int32_t JSApi_getObject_m3984611980 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::isFunction(System.Int32)
extern "C"  int32_t JSApi_isFunction_m4144950479 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSApi::isFunctionS(System.Int32)
extern "C"  bool JSApi_isFunctionS_m646921262 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::getFunction(System.Int32)
extern "C"  int32_t JSApi_getFunction_m249850021 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject JSApi::getFunctionS(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * JSApi_getFunctionS_m3318384739 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setUndefined(System.Int32)
extern "C"  void JSApi_setUndefined_m2718995785 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setChar(System.Int32,System.Char)
extern "C"  void JSApi_setChar_m1068601144 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Il2CppChar ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setSByte(System.Int32,System.SByte)
extern "C"  void JSApi_setSByte_m4213992726 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int8_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setByte(System.Int32,System.Byte)
extern "C"  void JSApi_setByte_m4088572664 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint8_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setInt16(System.Int32,System.Int16)
extern "C"  void JSApi_setInt16_m687612964 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int16_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setUInt16(System.Int32,System.UInt16)
extern "C"  void JSApi_setUInt16_m2537826648 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint16_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setInt32(System.Int32,System.Int32)
extern "C"  void JSApi_setInt32_m3092764336 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setUInt32(System.Int32,System.UInt32)
extern "C"  void JSApi_setUInt32_m4083021208 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setInt64(System.Int32,System.Int64)
extern "C"  void JSApi_setInt64_m367631986 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int64_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setUInt64(System.Int32,System.UInt64)
extern "C"  void JSApi_setUInt64_m1208208632 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint64_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setEnum(System.Int32,System.Int32)
extern "C"  void JSApi_setEnum_m1965030347 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setSingle(System.Int32,System.Single)
extern "C"  void JSApi_setSingle_m1382682360 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setDouble(System.Int32,System.Double)
extern "C"  void JSApi_setDouble_m4068591768 (Il2CppObject * __this /* static, unused */, int32_t ___e0, double ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setIntPtr(System.Int32,System.Int64)
extern "C"  void JSApi_setIntPtr_m109774670 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int64_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setIntPtr(System.Int32,System.IntPtr)
extern "C"  void JSApi_setIntPtr_m3403853144 (Il2CppObject * __this /* static, unused */, int32_t ___e0, IntPtr_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setBoolean(System.Int32,System.Int32)
extern "C"  void JSApi_setBoolean_m2875313814 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setBooleanS(System.Int32,System.Boolean)
extern "C"  void JSApi_setBooleanS_m3067076103 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setString(System.Int32,System.String)
extern "C"  void JSApi_setString_m1815703704 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setStringS(System.Int32,System.Object)
extern "C"  void JSApi_setStringS_m890620969 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setStringS(System.Int32,System.String)
extern "C"  void JSApi_setStringS_m668510615 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setVector2(System.Int32,System.Single,System.Single)
extern "C"  void JSApi_setVector2_m1807504338 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___x1, float ___y2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setVector2S(System.Int32,UnityEngine.Vector2)
extern "C"  void JSApi_setVector2S_m3539319971 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setVector3(System.Int32,System.Single,System.Single,System.Single)
extern "C"  void JSApi_setVector3_m792383544 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___x1, float ___y2, float ___z3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setVector3S(System.Int32,UnityEngine.Vector3)
extern "C"  void JSApi_setVector3S_m1194652961 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setObject(System.Int32,System.Int32)
extern "C"  void JSApi_setObject_m4156328205 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setFunction(System.Int32,System.Int32)
extern "C"  void JSApi_setFunction_m574890772 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___funID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setArray(System.Int32,System.Int32,System.Int32)
extern "C"  void JSApi_setArray_m737530098 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, int32_t ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setArrayS(System.Int32,System.Int32,System.Boolean)
extern "C"  void JSApi_setArrayS_m1792123627 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::isVector2(System.Int32)
extern "C"  int32_t JSApi_isVector2_m4197050140 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSApi::isVector2S(System.Int32)
extern "C"  bool JSApi_isVector2S_m2956064117 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::isVector3(System.Int32)
extern "C"  int32_t JSApi_isVector3_m1409634653 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSApi::isVector3S(System.Int32)
extern "C"  bool JSApi_isVector3S_m2445529940 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::callFunctionValue(System.Int32,System.Int32,System.Int32)
extern "C"  void JSApi_callFunctionValue_m1431822436 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, int32_t ___funID1, int32_t ___argCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::incRefCount(System.Int32)
extern "C"  int32_t JSApi_incRefCount_m3272733841 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::decRefCount(System.Int32)
extern "C"  int32_t JSApi_decRefCount_m1376939445 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::isTraced(System.Int32)
extern "C"  int32_t JSApi_isTraced_m2813957558 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSApi::isTracedS(System.Int32)
extern "C"  bool JSApi_isTracedS_m696951207 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setTrace(System.Int32,System.Int32)
extern "C"  void JSApi_setTrace_m2447133081 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___trace1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setTraceS(System.Int32,System.Boolean)
extern "C"  void JSApi_setTraceS_m3187728420 (Il2CppObject * __this /* static, unused */, int32_t ___id0, bool ___trace1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::moveSaveID2Arr(System.Int32)
extern "C"  void JSApi_moveSaveID2Arr_m2319759699 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::getSaveID()
extern "C"  int32_t JSApi_getSaveID_m2360901524 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::removeByID(System.Int32)
extern "C"  void JSApi_removeByID_m22081745 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSApi::moveID2Arr(System.Int32,System.Int32)
extern "C"  bool JSApi_moveID2Arr_m3144561685 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___arrIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setCallFunctionValueRemoveArr(System.Int32)
extern "C"  void JSApi_setCallFunctionValueRemoveArr_m950031309 (Il2CppObject * __this /* static, unused */, int32_t ___bRemove0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setCallFunctionValueRemoveArrS(System.Boolean)
extern "C"  void JSApi_setCallFunctionValueRemoveArrS_m641345072 (Il2CppObject * __this /* static, unused */, bool ___bRemove0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::getProperty(System.Int32,System.String)
extern "C"  void JSApi_getProperty_m1469648656 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::setProperty(System.Int32,System.String,System.Int32)
extern "C"  int32_t JSApi_setProperty_m2454913389 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___name1, int32_t ___valueID2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::getElement(System.Int32,System.Int32)
extern "C"  int32_t JSApi_getElement_m4101624388 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::getArrayLength(System.Int32)
extern "C"  int32_t JSApi_getArrayLength_m34676054 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::gc()
extern "C"  void JSApi_gc_m3887518086 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::evaluate(System.Byte[],System.UInt32,System.String)
extern "C"  int32_t JSApi_evaluate_m68102244 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___ascii0, uint32_t ___length1, String_t* ___filename2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::evaluate_jsc(System.Byte[],System.UInt32,System.String)
extern "C"  int32_t JSApi_evaluate_jsc_m3277912073 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___ascii0, uint32_t ___length1, String_t* ___filename2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr JSApi::getArgString(System.IntPtr,System.Int32)
extern "C"  IntPtr_t JSApi_getArgString_m2444158121 (Il2CppObject * __this /* static, unused */, IntPtr_t ___vp0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSApi::getArgStringS(System.IntPtr,System.Int32)
extern "C"  String_t* JSApi_getArgStringS_m2333999616 (Il2CppObject * __this /* static, unused */, IntPtr_t ___vp0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::getObjFunction(System.Int32,System.String)
extern "C"  int32_t JSApi_getObjFunction_m2333870690 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___fname1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setRvalBool(System.IntPtr,System.Int32)
extern "C"  void JSApi_setRvalBool_m4098050166 (Il2CppObject * __this /* static, unused */, IntPtr_t ___vp0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::setRvalBoolS(System.IntPtr,System.Boolean)
extern "C"  void JSApi_setRvalBoolS_m4110044235 (Il2CppObject * __this /* static, unused */, IntPtr_t ___vp0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::getValueMapSize()
extern "C"  int32_t JSApi_getValueMapSize_m3845921736 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::getValueMapIndex()
extern "C"  int32_t JSApi_getValueMapIndex_m3097259789 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::getValueMapStartIndex()
extern "C"  int32_t JSApi_getValueMapStartIndex_m3951171863 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr JSApi::ilo_getErroReportFileName1(System.IntPtr)
extern "C"  IntPtr_t JSApi_ilo_getErroReportFileName1_m2162535542 (Il2CppObject * __this /* static, unused */, IntPtr_t ___report0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr JSApi::ilo_getString2(System.Int32)
extern "C"  IntPtr_t JSApi_ilo_getString2_m2100126948 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSApi::ilo_getObjX3()
extern "C"  float JSApi_ilo_getObjX3_m2651977453 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSApi::ilo_getObjY4()
extern "C"  float JSApi_ilo_getObjY4_m2652008205 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::ilo_setVector25(System.Int32,System.Single,System.Single)
extern "C"  void JSApi_ilo_setVector25_m594208456 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___x1, float ___y2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi::ilo_isVector36(System.Int32)
extern "C"  int32_t JSApi_ilo_isVector36_m3645291280 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi::ilo_setRvalBool7(System.IntPtr,System.Int32)
extern "C"  void JSApi_ilo_setRvalBool7_m2313594300 (Il2CppObject * __this /* static, unused */, IntPtr_t ___vp0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
