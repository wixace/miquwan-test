﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Voxels.VoxelContour>
struct DefaultComparer_t2044977683;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_VoxelContour3597201016.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Voxels.VoxelContour>::.ctor()
extern "C"  void DefaultComparer__ctor_m1915993417_gshared (DefaultComparer_t2044977683 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1915993417(__this, method) ((  void (*) (DefaultComparer_t2044977683 *, const MethodInfo*))DefaultComparer__ctor_m1915993417_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Voxels.VoxelContour>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3624734786_gshared (DefaultComparer_t2044977683 * __this, VoxelContour_t3597201016  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3624734786(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2044977683 *, VoxelContour_t3597201016 , const MethodInfo*))DefaultComparer_GetHashCode_m3624734786_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Voxels.VoxelContour>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3147245722_gshared (DefaultComparer_t2044977683 * __this, VoxelContour_t3597201016  ___x0, VoxelContour_t3597201016  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3147245722(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2044977683 *, VoxelContour_t3597201016 , VoxelContour_t3597201016 , const MethodInfo*))DefaultComparer_Equals_m3147245722_gshared)(__this, ___x0, ___y1, method)
