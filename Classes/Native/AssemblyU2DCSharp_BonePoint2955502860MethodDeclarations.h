﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BonePoint
struct BonePoint_t2955502860;

#include "codegen/il2cpp-codegen.h"

// System.Void BonePoint::.ctor()
extern "C"  void BonePoint__ctor_m1478794719 (BonePoint_t2955502860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
