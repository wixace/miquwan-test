﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_kelkouPirboca98
struct  M_kelkouPirboca98_t2581829796  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_kelkouPirboca98::_risteZaiyoja
	int32_t ____risteZaiyoja_0;
	// System.Int32 GarbageiOS.M_kelkouPirboca98::_wemruzi
	int32_t ____wemruzi_1;
	// System.Int32 GarbageiOS.M_kelkouPirboca98::_laterpor
	int32_t ____laterpor_2;
	// System.Single GarbageiOS.M_kelkouPirboca98::_mesasbe
	float ____mesasbe_3;
	// System.Boolean GarbageiOS.M_kelkouPirboca98::_kisxosas
	bool ____kisxosas_4;
	// System.Boolean GarbageiOS.M_kelkouPirboca98::_tezere
	bool ____tezere_5;
	// System.Int32 GarbageiOS.M_kelkouPirboca98::_meregow
	int32_t ____meregow_6;
	// System.Int32 GarbageiOS.M_kelkouPirboca98::_lico
	int32_t ____lico_7;
	// System.Boolean GarbageiOS.M_kelkouPirboca98::_yissowPohere
	bool ____yissowPohere_8;

public:
	inline static int32_t get_offset_of__risteZaiyoja_0() { return static_cast<int32_t>(offsetof(M_kelkouPirboca98_t2581829796, ____risteZaiyoja_0)); }
	inline int32_t get__risteZaiyoja_0() const { return ____risteZaiyoja_0; }
	inline int32_t* get_address_of__risteZaiyoja_0() { return &____risteZaiyoja_0; }
	inline void set__risteZaiyoja_0(int32_t value)
	{
		____risteZaiyoja_0 = value;
	}

	inline static int32_t get_offset_of__wemruzi_1() { return static_cast<int32_t>(offsetof(M_kelkouPirboca98_t2581829796, ____wemruzi_1)); }
	inline int32_t get__wemruzi_1() const { return ____wemruzi_1; }
	inline int32_t* get_address_of__wemruzi_1() { return &____wemruzi_1; }
	inline void set__wemruzi_1(int32_t value)
	{
		____wemruzi_1 = value;
	}

	inline static int32_t get_offset_of__laterpor_2() { return static_cast<int32_t>(offsetof(M_kelkouPirboca98_t2581829796, ____laterpor_2)); }
	inline int32_t get__laterpor_2() const { return ____laterpor_2; }
	inline int32_t* get_address_of__laterpor_2() { return &____laterpor_2; }
	inline void set__laterpor_2(int32_t value)
	{
		____laterpor_2 = value;
	}

	inline static int32_t get_offset_of__mesasbe_3() { return static_cast<int32_t>(offsetof(M_kelkouPirboca98_t2581829796, ____mesasbe_3)); }
	inline float get__mesasbe_3() const { return ____mesasbe_3; }
	inline float* get_address_of__mesasbe_3() { return &____mesasbe_3; }
	inline void set__mesasbe_3(float value)
	{
		____mesasbe_3 = value;
	}

	inline static int32_t get_offset_of__kisxosas_4() { return static_cast<int32_t>(offsetof(M_kelkouPirboca98_t2581829796, ____kisxosas_4)); }
	inline bool get__kisxosas_4() const { return ____kisxosas_4; }
	inline bool* get_address_of__kisxosas_4() { return &____kisxosas_4; }
	inline void set__kisxosas_4(bool value)
	{
		____kisxosas_4 = value;
	}

	inline static int32_t get_offset_of__tezere_5() { return static_cast<int32_t>(offsetof(M_kelkouPirboca98_t2581829796, ____tezere_5)); }
	inline bool get__tezere_5() const { return ____tezere_5; }
	inline bool* get_address_of__tezere_5() { return &____tezere_5; }
	inline void set__tezere_5(bool value)
	{
		____tezere_5 = value;
	}

	inline static int32_t get_offset_of__meregow_6() { return static_cast<int32_t>(offsetof(M_kelkouPirboca98_t2581829796, ____meregow_6)); }
	inline int32_t get__meregow_6() const { return ____meregow_6; }
	inline int32_t* get_address_of__meregow_6() { return &____meregow_6; }
	inline void set__meregow_6(int32_t value)
	{
		____meregow_6 = value;
	}

	inline static int32_t get_offset_of__lico_7() { return static_cast<int32_t>(offsetof(M_kelkouPirboca98_t2581829796, ____lico_7)); }
	inline int32_t get__lico_7() const { return ____lico_7; }
	inline int32_t* get_address_of__lico_7() { return &____lico_7; }
	inline void set__lico_7(int32_t value)
	{
		____lico_7 = value;
	}

	inline static int32_t get_offset_of__yissowPohere_8() { return static_cast<int32_t>(offsetof(M_kelkouPirboca98_t2581829796, ____yissowPohere_8)); }
	inline bool get__yissowPohere_8() const { return ____yissowPohere_8; }
	inline bool* get_address_of__yissowPohere_8() { return &____yissowPohere_8; }
	inline void set__yissowPohere_8(bool value)
	{
		____yissowPohere_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
