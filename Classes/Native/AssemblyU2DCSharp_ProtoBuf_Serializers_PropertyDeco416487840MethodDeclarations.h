﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.PropertyDecorator
struct PropertyDecorator_t416487840;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// ProtoBuf.Serializers.IProtoSerializer
struct IProtoSerializer_t3033312651;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// System.Reflection.MemberInfo
struct MemberInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_PropertyInfo924268725.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"

// System.Void ProtoBuf.Serializers.PropertyDecorator::.ctor(ProtoBuf.Meta.TypeModel,System.Type,System.Reflection.PropertyInfo,ProtoBuf.Serializers.IProtoSerializer)
extern "C"  void PropertyDecorator__ctor_m587594739 (PropertyDecorator_t416487840 * __this, TypeModel_t2730011105 * ___model0, Type_t * ___forType1, PropertyInfo_t * ___property2, Il2CppObject * ___tail3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.PropertyDecorator::get_ExpectedType()
extern "C"  Type_t * PropertyDecorator_get_ExpectedType_m3041779060 (PropertyDecorator_t416487840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.PropertyDecorator::get_RequiresOldValue()
extern "C"  bool PropertyDecorator_get_RequiresOldValue_m2586974420 (PropertyDecorator_t416487840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.PropertyDecorator::get_ReturnsValue()
extern "C"  bool PropertyDecorator_get_ReturnsValue_m3135477098 (PropertyDecorator_t416487840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.PropertyDecorator::SanityCheck(ProtoBuf.Meta.TypeModel,System.Reflection.PropertyInfo,ProtoBuf.Serializers.IProtoSerializer,System.Boolean&,System.Boolean,System.Boolean)
extern "C"  void PropertyDecorator_SanityCheck_m2990030029 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, PropertyInfo_t * ___property1, Il2CppObject * ___tail2, bool* ___writeValue3, bool ___nonPublic4, bool ___allowInternal5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Serializers.PropertyDecorator::GetShadowSetter(ProtoBuf.Meta.TypeModel,System.Reflection.PropertyInfo)
extern "C"  MethodInfo_t * PropertyDecorator_GetShadowSetter_m1791321156 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, PropertyInfo_t * ___property1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.PropertyDecorator::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void PropertyDecorator_Write_m2693004956 (PropertyDecorator_t416487840 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.PropertyDecorator::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * PropertyDecorator_Read_m2924072180 (PropertyDecorator_t416487840 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.PropertyDecorator::CanWrite(ProtoBuf.Meta.TypeModel,System.Reflection.MemberInfo)
extern "C"  bool PropertyDecorator_CanWrite_m316169779 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, MemberInfo_t * ___member1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.PropertyDecorator::ilo_get_ReturnsValue1(ProtoBuf.Serializers.IProtoSerializer)
extern "C"  bool PropertyDecorator_ilo_get_ReturnsValue1_m3120022555 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.PropertyDecorator::ilo_MapType2(ProtoBuf.Meta.TypeModel,System.Type)
extern "C"  Type_t * PropertyDecorator_ilo_MapType2_m534105236 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.PropertyDecorator::ilo_Write3(ProtoBuf.Serializers.IProtoSerializer,System.Object,ProtoBuf.ProtoWriter)
extern "C"  void PropertyDecorator_ilo_Write3_m861762837 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___value1, ProtoWriter_t4117914721 * ___dest2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.PropertyDecorator::ilo_get_RequiresOldValue4(ProtoBuf.Serializers.IProtoSerializer)
extern "C"  bool PropertyDecorator_ilo_get_RequiresOldValue4_m3254970658 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.PropertyDecorator::ilo_Read5(ProtoBuf.Serializers.IProtoSerializer,System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * PropertyDecorator_ilo_Read5_m1985968217 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___value1, ProtoReader_t3962509489 * ___source2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Serializers.PropertyDecorator::ilo_GetShadowSetter6(ProtoBuf.Meta.TypeModel,System.Reflection.PropertyInfo)
extern "C"  MethodInfo_t * PropertyDecorator_ilo_GetShadowSetter6_m1279277747 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, PropertyInfo_t * ___property1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
