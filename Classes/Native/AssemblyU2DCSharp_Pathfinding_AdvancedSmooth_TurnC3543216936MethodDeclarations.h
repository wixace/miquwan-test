﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.AdvancedSmooth/TurnConstructor
struct TurnConstructor_t3543216936;
// System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>
struct List_1_t1833380930;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Void Pathfinding.AdvancedSmooth/TurnConstructor::.ctor()
extern "C"  void TurnConstructor__ctor_m548868723 (TurnConstructor_t3543216936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth/TurnConstructor::.cctor()
extern "C"  void TurnConstructor__cctor_m3647932314 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth/TurnConstructor::OnTangentUpdate()
extern "C"  void TurnConstructor_OnTangentUpdate_m3932598176 (TurnConstructor_t3543216936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth/TurnConstructor::PointToTangent(System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>)
extern "C"  void TurnConstructor_PointToTangent_m2997081857 (TurnConstructor_t3543216936 * __this, List_1_t1833380930 * ___turnList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth/TurnConstructor::TangentToPoint(System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>)
extern "C"  void TurnConstructor_TangentToPoint_m4035939895 (TurnConstructor_t3543216936 * __this, List_1_t1833380930 * ___turnList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth/TurnConstructor::TangentToTangent(System.Collections.Generic.List`1<Pathfinding.AdvancedSmooth/Turn>)
extern "C"  void TurnConstructor_TangentToTangent_m1101390956 (TurnConstructor_t3543216936 * __this, List_1_t1833380930 * ___turnList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth/TurnConstructor::Setup(System.Int32,UnityEngine.Vector3[])
extern "C"  void TurnConstructor_Setup_m2448906704 (Il2CppObject * __this /* static, unused */, int32_t ___i0, Vector3U5BU5D_t215400611* ___vectorPath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth/TurnConstructor::PostPrepare()
extern "C"  void TurnConstructor_PostPrepare_m703884824 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth/TurnConstructor::AddCircleSegment(System.Double,System.Double,System.Boolean,UnityEngine.Vector3,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Single)
extern "C"  void TurnConstructor_AddCircleSegment_m1224736803 (TurnConstructor_t3543216936 * __this, double ___startAngle0, double ___endAngle1, bool ___clockwise2, Vector3_t4282066566  ___center3, List_1_t1355284822 * ___output4, float ___radius5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth/TurnConstructor::DebugCircleSegment(UnityEngine.Vector3,System.Double,System.Double,System.Double,UnityEngine.Color)
extern "C"  void TurnConstructor_DebugCircleSegment_m1709779180 (TurnConstructor_t3543216936 * __this, Vector3_t4282066566  ___center0, double ___startAngle1, double ___endAngle2, double ___radius3, Color_t4194546905  ___color4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth/TurnConstructor::DebugCircle(UnityEngine.Vector3,System.Double,UnityEngine.Color)
extern "C"  void TurnConstructor_DebugCircle_m2414558049 (TurnConstructor_t3543216936 * __this, Vector3_t4282066566  ___center0, double ___radius1, Color_t4194546905  ___color2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Pathfinding.AdvancedSmooth/TurnConstructor::GetLengthFromAngle(System.Double,System.Double)
extern "C"  double TurnConstructor_GetLengthFromAngle_m4161957693 (TurnConstructor_t3543216936 * __this, double ___angle0, double ___radius1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Pathfinding.AdvancedSmooth/TurnConstructor::ClockwiseAngle(System.Double,System.Double)
extern "C"  double TurnConstructor_ClockwiseAngle_m3496105161 (TurnConstructor_t3543216936 * __this, double ___from0, double ___to1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Pathfinding.AdvancedSmooth/TurnConstructor::CounterClockwiseAngle(System.Double,System.Double)
extern "C"  double TurnConstructor_CounterClockwiseAngle_m2273072883 (TurnConstructor_t3543216936 * __this, double ___from0, double ___to1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.AdvancedSmooth/TurnConstructor::AngleToVector(System.Double)
extern "C"  Vector3_t4282066566  TurnConstructor_AngleToVector_m2888798490 (TurnConstructor_t3543216936 * __this, double ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Pathfinding.AdvancedSmooth/TurnConstructor::ToDegrees(System.Double)
extern "C"  double TurnConstructor_ToDegrees_m1019826824 (TurnConstructor_t3543216936 * __this, double ___rad0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Pathfinding.AdvancedSmooth/TurnConstructor::ClampAngle(System.Double)
extern "C"  double TurnConstructor_ClampAngle_m1390964246 (TurnConstructor_t3543216936 * __this, double ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Pathfinding.AdvancedSmooth/TurnConstructor::Atan2(UnityEngine.Vector3)
extern "C"  double TurnConstructor_Atan2_m111378297 (TurnConstructor_t3543216936 * __this, Vector3_t4282066566  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
