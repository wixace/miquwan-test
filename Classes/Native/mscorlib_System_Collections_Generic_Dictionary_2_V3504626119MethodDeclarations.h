﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>
struct ValueCollection_t3504626119;
// System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>
struct Dictionary_2_t509053110;
// System.Collections.Generic.IEnumerator`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct IEnumerator_1_t250878963;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Mihua.Assets.SubAssetMgr/ErrorInfo[]
struct ErrorInfoU5BU5D_t2864912703;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr_ErrorIn2633981210.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2735853814.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1593514206_gshared (ValueCollection_t3504626119 * __this, Dictionary_2_t509053110 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1593514206(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3504626119 *, Dictionary_2_t509053110 *, const MethodInfo*))ValueCollection__ctor_m1593514206_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2414710676_gshared (ValueCollection_t3504626119 * __this, ErrorInfo_t2633981210  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2414710676(__this, ___item0, method) ((  void (*) (ValueCollection_t3504626119 *, ErrorInfo_t2633981210 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2414710676_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2971821661_gshared (ValueCollection_t3504626119 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2971821661(__this, method) ((  void (*) (ValueCollection_t3504626119 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2971821661_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3636485974_gshared (ValueCollection_t3504626119 * __this, ErrorInfo_t2633981210  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3636485974(__this, ___item0, method) ((  bool (*) (ValueCollection_t3504626119 *, ErrorInfo_t2633981210 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3636485974_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2847233403_gshared (ValueCollection_t3504626119 * __this, ErrorInfo_t2633981210  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2847233403(__this, ___item0, method) ((  bool (*) (ValueCollection_t3504626119 *, ErrorInfo_t2633981210 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2847233403_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m171930461_gshared (ValueCollection_t3504626119 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m171930461(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3504626119 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m171930461_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2972088097_gshared (ValueCollection_t3504626119 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m2972088097(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3504626119 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2972088097_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1070718448_gshared (ValueCollection_t3504626119 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1070718448(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3504626119 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1070718448_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1911661833_gshared (ValueCollection_t3504626119 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1911661833(__this, method) ((  bool (*) (ValueCollection_t3504626119 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1911661833_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1783132265_gshared (ValueCollection_t3504626119 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1783132265(__this, method) ((  bool (*) (ValueCollection_t3504626119 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1783132265_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2782635355_gshared (ValueCollection_t3504626119 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2782635355(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3504626119 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2782635355_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m2611221925_gshared (ValueCollection_t3504626119 * __this, ErrorInfoU5BU5D_t2864912703* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m2611221925(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3504626119 *, ErrorInfoU5BU5D_t2864912703*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2611221925_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::GetEnumerator()
extern "C"  Enumerator_t2735853814  ValueCollection_GetEnumerator_m1194861198_gshared (ValueCollection_t3504626119 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1194861198(__this, method) ((  Enumerator_t2735853814  (*) (ValueCollection_t3504626119 *, const MethodInfo*))ValueCollection_GetEnumerator_m1194861198_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3905304867_gshared (ValueCollection_t3504626119 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3905304867(__this, method) ((  int32_t (*) (ValueCollection_t3504626119 *, const MethodInfo*))ValueCollection_get_Count_m3905304867_gshared)(__this, method)
