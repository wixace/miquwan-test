﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>
struct ReadOnlyCollection_1_t688055650;
// System.Collections.Generic.IList`1<HatredCtrl/stValue>
struct IList_1_t1825625317;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// HatredCtrl/stValue[]
struct stValueU5BU5D_t1121401207;
// System.Collections.Generic.IEnumerator`1<HatredCtrl/stValue>
struct IEnumerator_1_t1042843163;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3553802350_gshared (ReadOnlyCollection_1_t688055650 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3553802350(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t688055650 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3553802350_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3081306968_gshared (ReadOnlyCollection_1_t688055650 * __this, stValue_t3425945410  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3081306968(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t688055650 *, stValue_t3425945410 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3081306968_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3037926514_gshared (ReadOnlyCollection_1_t688055650 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3037926514(__this, method) ((  void (*) (ReadOnlyCollection_1_t688055650 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3037926514_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1989883967_gshared (ReadOnlyCollection_1_t688055650 * __this, int32_t ___index0, stValue_t3425945410  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1989883967(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t688055650 *, int32_t, stValue_t3425945410 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1989883967_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2399267039_gshared (ReadOnlyCollection_1_t688055650 * __this, stValue_t3425945410  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2399267039(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t688055650 *, stValue_t3425945410 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2399267039_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m4158704133_gshared (ReadOnlyCollection_1_t688055650 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m4158704133(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t688055650 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m4158704133_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  stValue_t3425945410  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1506646187_gshared (ReadOnlyCollection_1_t688055650 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1506646187(__this, ___index0, method) ((  stValue_t3425945410  (*) (ReadOnlyCollection_1_t688055650 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1506646187_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2827642198_gshared (ReadOnlyCollection_1_t688055650 * __this, int32_t ___index0, stValue_t3425945410  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2827642198(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t688055650 *, int32_t, stValue_t3425945410 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2827642198_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3097947088_gshared (ReadOnlyCollection_1_t688055650 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3097947088(__this, method) ((  bool (*) (ReadOnlyCollection_1_t688055650 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3097947088_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m759302173_gshared (ReadOnlyCollection_1_t688055650 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m759302173(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t688055650 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m759302173_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1387602028_gshared (ReadOnlyCollection_1_t688055650 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1387602028(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t688055650 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1387602028_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2377304657_gshared (ReadOnlyCollection_1_t688055650 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m2377304657(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t688055650 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m2377304657_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m4197370667_gshared (ReadOnlyCollection_1_t688055650 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m4197370667(__this, method) ((  void (*) (ReadOnlyCollection_1_t688055650 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m4197370667_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1146987151_gshared (ReadOnlyCollection_1_t688055650 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1146987151(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t688055650 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1146987151_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m921923625_gshared (ReadOnlyCollection_1_t688055650 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m921923625(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t688055650 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m921923625_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3366821404_gshared (ReadOnlyCollection_1_t688055650 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3366821404(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t688055650 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m3366821404_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m4099100044_gshared (ReadOnlyCollection_1_t688055650 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m4099100044(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t688055650 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m4099100044_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m4103405484_gshared (ReadOnlyCollection_1_t688055650 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m4103405484(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t688055650 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m4103405484_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3424351853_gshared (ReadOnlyCollection_1_t688055650 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3424351853(__this, method) ((  bool (*) (ReadOnlyCollection_1_t688055650 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3424351853_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3394964255_gshared (ReadOnlyCollection_1_t688055650 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3394964255(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t688055650 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3394964255_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3082430782_gshared (ReadOnlyCollection_1_t688055650 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3082430782(__this, method) ((  bool (*) (ReadOnlyCollection_1_t688055650 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3082430782_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3462936059_gshared (ReadOnlyCollection_1_t688055650 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3462936059(__this, method) ((  bool (*) (ReadOnlyCollection_1_t688055650 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3462936059_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3746083046_gshared (ReadOnlyCollection_1_t688055650 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3746083046(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t688055650 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3746083046_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2018947827_gshared (ReadOnlyCollection_1_t688055650 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2018947827(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t688055650 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2018947827_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m2750087460_gshared (ReadOnlyCollection_1_t688055650 * __this, stValue_t3425945410  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m2750087460(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t688055650 *, stValue_t3425945410 , const MethodInfo*))ReadOnlyCollection_1_Contains_m2750087460_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m993451656_gshared (ReadOnlyCollection_1_t688055650 * __this, stValueU5BU5D_t1121401207* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m993451656(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t688055650 *, stValueU5BU5D_t1121401207*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m993451656_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3847175291_gshared (ReadOnlyCollection_1_t688055650 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3847175291(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t688055650 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3847175291_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1011811924_gshared (ReadOnlyCollection_1_t688055650 * __this, stValue_t3425945410  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1011811924(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t688055650 *, stValue_t3425945410 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1011811924_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1118706087_gshared (ReadOnlyCollection_1_t688055650 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1118706087(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t688055650 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1118706087_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>::get_Item(System.Int32)
extern "C"  stValue_t3425945410  ReadOnlyCollection_1_get_Item_m3299390443_gshared (ReadOnlyCollection_1_t688055650 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3299390443(__this, ___index0, method) ((  stValue_t3425945410  (*) (ReadOnlyCollection_1_t688055650 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3299390443_gshared)(__this, ___index0, method)
