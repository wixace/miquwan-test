﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_mene129
struct M_mene129_t3458920393;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_mene129::.ctor()
extern "C"  void M_mene129__ctor_m2349664698 (M_mene129_t3458920393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_mene129::M_tairyawgir0(System.String[],System.Int32)
extern "C"  void M_mene129_M_tairyawgir0_m2400766006 (M_mene129_t3458920393 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
