﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.FollowHeroBvr
struct FollowHeroBvr_t249631345;
// CombatEntity
struct CombatEntity_t684137495;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;
// HeroMgr
struct HeroMgr_t2475965342;
// AnimationRunner
struct AnimationRunner_t1015409588;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "System_Core_System_Action3771233898.h"

// System.Void Entity.Behavior.FollowHeroBvr::.ctor()
extern "C"  void FollowHeroBvr__ctor_m2149784745 (FollowHeroBvr_t249631345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.FollowHeroBvr::get_id()
extern "C"  uint8_t FollowHeroBvr_get_id_m680705325 (FollowHeroBvr_t249631345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FollowHeroBvr::Reason()
extern "C"  void FollowHeroBvr_Reason_m1770205759 (FollowHeroBvr_t249631345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FollowHeroBvr::Action()
extern "C"  void FollowHeroBvr_Action_m966912433 (FollowHeroBvr_t249631345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FollowHeroBvr::DoEntering()
extern "C"  void FollowHeroBvr_DoEntering_m691614320 (FollowHeroBvr_t249631345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FollowHeroBvr::DoLeaving()
extern "C"  void FollowHeroBvr_DoLeaving_m1437051696 (FollowHeroBvr_t249631345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FollowHeroBvr::Pause()
extern "C"  void FollowHeroBvr_Pause_m2203910717 (FollowHeroBvr_t249631345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FollowHeroBvr::Play()
extern "C"  void FollowHeroBvr_Play_m3544343983 (FollowHeroBvr_t249631345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Entity.Behavior.FollowHeroBvr::ilo_get_canMove1(CombatEntity)
extern "C"  bool FollowHeroBvr_ilo_get_canMove1_m2056242684 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.FollowHeroBvr::ilo_get_atkTarget2(CombatEntity)
extern "C"  CombatEntity_t684137495 * FollowHeroBvr_ilo_get_atkTarget2_m2676675491 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl Entity.Behavior.FollowHeroBvr::ilo_get_bvrCtrl3(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * FollowHeroBvr_ilo_get_bvrCtrl3_m714925868 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.FollowHeroBvr::ilo_get_entity4(Entity.Behavior.IBehavior)
extern "C"  CombatEntity_t684137495 * FollowHeroBvr_ilo_get_entity4_m637268934 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Entity.Behavior.FollowHeroBvr::ilo_get_startPos5(CombatEntity)
extern "C"  Vector3_t4282066566  FollowHeroBvr_ilo_get_startPos5_m3453929809 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr Entity.Behavior.FollowHeroBvr::ilo_get_instance6()
extern "C"  HeroMgr_t2475965342 * FollowHeroBvr_ilo_get_instance6_m4202587873 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnimationRunner Entity.Behavior.FollowHeroBvr::ilo_get_characterAnim7(CombatEntity)
extern "C"  AnimationRunner_t1015409588 * FollowHeroBvr_ilo_get_characterAnim7_m577753534 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FollowHeroBvr::ilo_Pause8(Entity.Behavior.IBehavior)
extern "C"  void FollowHeroBvr_ilo_Pause8_m4030223032 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.FollowHeroBvr::ilo_UpdateTargetPos9(CombatEntity,UnityEngine.Vector3,System.Action)
extern "C"  void FollowHeroBvr_ilo_UpdateTargetPos9_m36453568 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, Vector3_t4282066566  ___targetPos1, Action_t3771233898 * ___OnTargetCall2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
