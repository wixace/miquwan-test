﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t4054002952;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UITexture
struct UITexture_t3903132647;

#include "AssemblyU2DCSharp_FlowControl_FlowBase3680091731.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlowControl.FlowSplashScreen
struct  FlowSplashScreen_t1652246581  : public FlowBase_t3680091731
{
public:
	// System.String[] FlowControl.FlowSplashScreen::splashScreenPaths
	StringU5BU5D_t4054002952* ___splashScreenPaths_7;
	// UnityEngine.GameObject FlowControl.FlowSplashScreen::mainGO
	GameObject_t3674682005 * ___mainGO_8;
	// UITexture FlowControl.FlowSplashScreen::uitex
	UITexture_t3903132647 * ___uitex_9;
	// System.Int32 FlowControl.FlowSplashScreen::index
	int32_t ___index_10;
	// System.Single FlowControl.FlowSplashScreen::time
	float ___time_11;
	// System.Single FlowControl.FlowSplashScreen::showtime
	float ___showtime_12;

public:
	inline static int32_t get_offset_of_splashScreenPaths_7() { return static_cast<int32_t>(offsetof(FlowSplashScreen_t1652246581, ___splashScreenPaths_7)); }
	inline StringU5BU5D_t4054002952* get_splashScreenPaths_7() const { return ___splashScreenPaths_7; }
	inline StringU5BU5D_t4054002952** get_address_of_splashScreenPaths_7() { return &___splashScreenPaths_7; }
	inline void set_splashScreenPaths_7(StringU5BU5D_t4054002952* value)
	{
		___splashScreenPaths_7 = value;
		Il2CppCodeGenWriteBarrier(&___splashScreenPaths_7, value);
	}

	inline static int32_t get_offset_of_mainGO_8() { return static_cast<int32_t>(offsetof(FlowSplashScreen_t1652246581, ___mainGO_8)); }
	inline GameObject_t3674682005 * get_mainGO_8() const { return ___mainGO_8; }
	inline GameObject_t3674682005 ** get_address_of_mainGO_8() { return &___mainGO_8; }
	inline void set_mainGO_8(GameObject_t3674682005 * value)
	{
		___mainGO_8 = value;
		Il2CppCodeGenWriteBarrier(&___mainGO_8, value);
	}

	inline static int32_t get_offset_of_uitex_9() { return static_cast<int32_t>(offsetof(FlowSplashScreen_t1652246581, ___uitex_9)); }
	inline UITexture_t3903132647 * get_uitex_9() const { return ___uitex_9; }
	inline UITexture_t3903132647 ** get_address_of_uitex_9() { return &___uitex_9; }
	inline void set_uitex_9(UITexture_t3903132647 * value)
	{
		___uitex_9 = value;
		Il2CppCodeGenWriteBarrier(&___uitex_9, value);
	}

	inline static int32_t get_offset_of_index_10() { return static_cast<int32_t>(offsetof(FlowSplashScreen_t1652246581, ___index_10)); }
	inline int32_t get_index_10() const { return ___index_10; }
	inline int32_t* get_address_of_index_10() { return &___index_10; }
	inline void set_index_10(int32_t value)
	{
		___index_10 = value;
	}

	inline static int32_t get_offset_of_time_11() { return static_cast<int32_t>(offsetof(FlowSplashScreen_t1652246581, ___time_11)); }
	inline float get_time_11() const { return ___time_11; }
	inline float* get_address_of_time_11() { return &___time_11; }
	inline void set_time_11(float value)
	{
		___time_11 = value;
	}

	inline static int32_t get_offset_of_showtime_12() { return static_cast<int32_t>(offsetof(FlowSplashScreen_t1652246581, ___showtime_12)); }
	inline float get_showtime_12() const { return ___showtime_12; }
	inline float* get_address_of_showtime_12() { return &___showtime_12; }
	inline void set_showtime_12(float value)
	{
		___showtime_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
