﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PropertyBinding
struct PropertyBinding_t2650920656;
// PropertyReference
struct PropertyReference_t614957270;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PropertyReference614957270.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void PropertyBinding::.ctor()
extern "C"  void PropertyBinding__ctor_m1166187163 (PropertyBinding_t2650920656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyBinding::Start()
extern "C"  void PropertyBinding_Start_m113324955 (PropertyBinding_t2650920656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyBinding::Update()
extern "C"  void PropertyBinding_Update_m3518925778 (PropertyBinding_t2650920656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyBinding::LateUpdate()
extern "C"  void PropertyBinding_LateUpdate_m660740248 (PropertyBinding_t2650920656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyBinding::FixedUpdate()
extern "C"  void PropertyBinding_FixedUpdate_m4179023830 (PropertyBinding_t2650920656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyBinding::OnValidate()
extern "C"  void PropertyBinding_OnValidate_m3210119198 (PropertyBinding_t2650920656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyBinding::UpdateTarget()
extern "C"  void PropertyBinding_UpdateTarget_m2814058627 (PropertyBinding_t2650920656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PropertyBinding::ilo_Reset1(PropertyReference)
extern "C"  void PropertyBinding_ilo_Reset1_m2222115358 (Il2CppObject * __this /* static, unused */, PropertyReference_t614957270 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PropertyBinding::ilo_Get2(PropertyReference)
extern "C"  Il2CppObject * PropertyBinding_ilo_Get2_m97869689 (Il2CppObject * __this /* static, unused */, PropertyReference_t614957270 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PropertyBinding::ilo_Set3(PropertyReference,System.Object)
extern "C"  bool PropertyBinding_ilo_Set3_m142284273 (Il2CppObject * __this /* static, unused */, PropertyReference_t614957270 * ____this0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
