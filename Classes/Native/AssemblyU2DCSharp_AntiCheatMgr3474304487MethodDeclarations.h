﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AntiCheatMgr
struct AntiCheatMgr_t3474304487;
// System.String
struct String_t;
// ACData
struct ACData_t1924893420;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ACData1924893420.h"
#include "AssemblyU2DCSharp_AntiCheatMgr3474304487.h"

// System.Void AntiCheatMgr::.ctor()
extern "C"  void AntiCheatMgr__ctor_m4027185300 (AntiCheatMgr_t3474304487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntiCheatMgr::SetHeroAntiCheatCS(System.String,System.String,System.UInt32,System.UInt32)
extern "C"  void AntiCheatMgr_SetHeroAntiCheatCS_m1306834993 (AntiCheatMgr_t3474304487 * __this, String_t* ___uuid0, String_t* ___attrName1, uint32_t ___value2, uint32_t ___acValue3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 AntiCheatMgr::GetHeroAntiCheatCS(System.String,System.String)
extern "C"  uint32_t AntiCheatMgr_GetHeroAntiCheatCS_m776303874 (AntiCheatMgr_t3474304487 * __this, String_t* ___uuid0, String_t* ___attrName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AntiCheatMgr::GetHeroAntiCheatCS(System.String,System.String,System.Int32)
extern "C"  int32_t AntiCheatMgr_GetHeroAntiCheatCS_m516506952 (AntiCheatMgr_t3474304487 * __this, String_t* ___uuid0, String_t* ___attrName1, int32_t ___defaultValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntiCheatMgr::RemoveHeroAntiCheatCS(System.String)
extern "C"  void AntiCheatMgr_RemoveHeroAntiCheatCS_m3084353773 (AntiCheatMgr_t3474304487 * __this, String_t* ___uuid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntiCheatMgr::ClearHeroAntiCheat()
extern "C"  void AntiCheatMgr_ClearHeroAntiCheat_m3133908954 (AntiCheatMgr_t3474304487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntiCheatMgr::ClearHeroAntiCheatCS()
extern "C"  void AntiCheatMgr_ClearHeroAntiCheatCS_m915276298 (AntiCheatMgr_t3474304487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntiCheatMgr::Clear()
extern "C"  void AntiCheatMgr_Clear_m1433318591 (AntiCheatMgr_t3474304487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntiCheatMgr::SetHeroAntiCheat(System.String,System.String,System.UInt32,System.UInt32)
extern "C"  void AntiCheatMgr_SetHeroAntiCheat_m2444171681 (AntiCheatMgr_t3474304487 * __this, String_t* ___uuid0, String_t* ___attrName1, uint32_t ___value2, uint32_t ___acValue3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 AntiCheatMgr::GetHeroAntiCheat(System.String,System.String)
extern "C"  uint32_t AntiCheatMgr_GetHeroAntiCheat_m2123479218 (AntiCheatMgr_t3474304487 * __this, String_t* ___uuid0, String_t* ___attrName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AntiCheatMgr::GetHeroAntiCheat(System.String,System.String,System.Int32)
extern "C"  int32_t AntiCheatMgr_GetHeroAntiCheat_m3624366168 (AntiCheatMgr_t3474304487 * __this, String_t* ___uuid0, String_t* ___attrName1, int32_t ___defaultValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntiCheatMgr::RemoveHeroAntiCheat(System.String)
extern "C"  void AntiCheatMgr_RemoveHeroAntiCheat_m884553309 (AntiCheatMgr_t3474304487 * __this, String_t* ___uuid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 AntiCheatMgr::ilo_GetValue1(ACData)
extern "C"  uint32_t AntiCheatMgr_ilo_GetValue1_m16413326 (Il2CppObject * __this /* static, unused */, ACData_t1924893420 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntiCheatMgr::ilo_ClearHeroAntiCheatCS2(AntiCheatMgr)
extern "C"  void AntiCheatMgr_ilo_ClearHeroAntiCheatCS2_m2126812848 (Il2CppObject * __this /* static, unused */, AntiCheatMgr_t3474304487 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
