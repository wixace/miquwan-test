﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSVCall
struct  JSVCall_t3708497963  : public Il2CppObject
{
public:
	// System.Boolean JSVCall::bGet
	bool ___bGet_0;
	// System.Int32 JSVCall::jsObjID
	int32_t ___jsObjID_1;
	// System.Object JSVCall::csObj
	Il2CppObject * ___csObj_2;
	// System.Int32 JSVCall::jsCallCount
	int32_t ___jsCallCount_3;

public:
	inline static int32_t get_offset_of_bGet_0() { return static_cast<int32_t>(offsetof(JSVCall_t3708497963, ___bGet_0)); }
	inline bool get_bGet_0() const { return ___bGet_0; }
	inline bool* get_address_of_bGet_0() { return &___bGet_0; }
	inline void set_bGet_0(bool value)
	{
		___bGet_0 = value;
	}

	inline static int32_t get_offset_of_jsObjID_1() { return static_cast<int32_t>(offsetof(JSVCall_t3708497963, ___jsObjID_1)); }
	inline int32_t get_jsObjID_1() const { return ___jsObjID_1; }
	inline int32_t* get_address_of_jsObjID_1() { return &___jsObjID_1; }
	inline void set_jsObjID_1(int32_t value)
	{
		___jsObjID_1 = value;
	}

	inline static int32_t get_offset_of_csObj_2() { return static_cast<int32_t>(offsetof(JSVCall_t3708497963, ___csObj_2)); }
	inline Il2CppObject * get_csObj_2() const { return ___csObj_2; }
	inline Il2CppObject ** get_address_of_csObj_2() { return &___csObj_2; }
	inline void set_csObj_2(Il2CppObject * value)
	{
		___csObj_2 = value;
		Il2CppCodeGenWriteBarrier(&___csObj_2, value);
	}

	inline static int32_t get_offset_of_jsCallCount_3() { return static_cast<int32_t>(offsetof(JSVCall_t3708497963, ___jsCallCount_3)); }
	inline int32_t get_jsCallCount_3() const { return ___jsCallCount_3; }
	inline int32_t* get_address_of_jsCallCount_3() { return &___jsCallCount_3; }
	inline void set_jsCallCount_3(int32_t value)
	{
		___jsCallCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
