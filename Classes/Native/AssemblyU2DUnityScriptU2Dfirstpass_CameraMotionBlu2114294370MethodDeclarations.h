﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraMotionBlur
struct CameraMotionBlur_t2114294370;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void CameraMotionBlur::.ctor()
extern "C"  void CameraMotionBlur__ctor_m2724484834 (CameraMotionBlur_t2114294370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraMotionBlur::.cctor()
extern "C"  void CameraMotionBlur__cctor_m2372555019 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraMotionBlur::CalculateViewProjection()
extern "C"  void CameraMotionBlur_CalculateViewProjection_m3450963866 (CameraMotionBlur_t2114294370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraMotionBlur::Start()
extern "C"  void CameraMotionBlur_Start_m1671622626 (CameraMotionBlur_t2114294370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraMotionBlur::OnEnable()
extern "C"  void CameraMotionBlur_OnEnable_m907308388 (CameraMotionBlur_t2114294370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraMotionBlur::OnDisable()
extern "C"  void CameraMotionBlur_OnDisable_m2797693513 (CameraMotionBlur_t2114294370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraMotionBlur::CheckResources()
extern "C"  bool CameraMotionBlur_CheckResources_m2324546181 (CameraMotionBlur_t2114294370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraMotionBlur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraMotionBlur_OnRenderImage_m2411491388 (CameraMotionBlur_t2114294370 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraMotionBlur::Remember()
extern "C"  void CameraMotionBlur_Remember_m1343680751 (CameraMotionBlur_t2114294370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera CameraMotionBlur::GetTmpCam()
extern "C"  Camera_t2727095145 * CameraMotionBlur_GetTmpCam_m4157442043 (CameraMotionBlur_t2114294370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraMotionBlur::StartFrame()
extern "C"  void CameraMotionBlur_StartFrame_m2660784333 (CameraMotionBlur_t2114294370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraMotionBlur::divRoundUp(System.Int32,System.Int32)
extern "C"  int32_t CameraMotionBlur_divRoundUp_m3584197676 (CameraMotionBlur_t2114294370 * __this, int32_t ___x0, int32_t ___d1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraMotionBlur::Main()
extern "C"  void CameraMotionBlur_Main_m1804496283 (CameraMotionBlur_t2114294370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
