﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_gikaljisYeesa101
struct  M_gikaljisYeesa101_t1656339451  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_gikaljisYeesa101::_duwal
	String_t* ____duwal_0;
	// System.String GarbageiOS.M_gikaljisYeesa101::_wairinou
	String_t* ____wairinou_1;
	// System.UInt32 GarbageiOS.M_gikaljisYeesa101::_jornaBemha
	uint32_t ____jornaBemha_2;
	// System.Single GarbageiOS.M_gikaljisYeesa101::_linemo
	float ____linemo_3;
	// System.String GarbageiOS.M_gikaljisYeesa101::_jomurcaSisowci
	String_t* ____jomurcaSisowci_4;
	// System.Single GarbageiOS.M_gikaljisYeesa101::_salcaiStearrichem
	float ____salcaiStearrichem_5;

public:
	inline static int32_t get_offset_of__duwal_0() { return static_cast<int32_t>(offsetof(M_gikaljisYeesa101_t1656339451, ____duwal_0)); }
	inline String_t* get__duwal_0() const { return ____duwal_0; }
	inline String_t** get_address_of__duwal_0() { return &____duwal_0; }
	inline void set__duwal_0(String_t* value)
	{
		____duwal_0 = value;
		Il2CppCodeGenWriteBarrier(&____duwal_0, value);
	}

	inline static int32_t get_offset_of__wairinou_1() { return static_cast<int32_t>(offsetof(M_gikaljisYeesa101_t1656339451, ____wairinou_1)); }
	inline String_t* get__wairinou_1() const { return ____wairinou_1; }
	inline String_t** get_address_of__wairinou_1() { return &____wairinou_1; }
	inline void set__wairinou_1(String_t* value)
	{
		____wairinou_1 = value;
		Il2CppCodeGenWriteBarrier(&____wairinou_1, value);
	}

	inline static int32_t get_offset_of__jornaBemha_2() { return static_cast<int32_t>(offsetof(M_gikaljisYeesa101_t1656339451, ____jornaBemha_2)); }
	inline uint32_t get__jornaBemha_2() const { return ____jornaBemha_2; }
	inline uint32_t* get_address_of__jornaBemha_2() { return &____jornaBemha_2; }
	inline void set__jornaBemha_2(uint32_t value)
	{
		____jornaBemha_2 = value;
	}

	inline static int32_t get_offset_of__linemo_3() { return static_cast<int32_t>(offsetof(M_gikaljisYeesa101_t1656339451, ____linemo_3)); }
	inline float get__linemo_3() const { return ____linemo_3; }
	inline float* get_address_of__linemo_3() { return &____linemo_3; }
	inline void set__linemo_3(float value)
	{
		____linemo_3 = value;
	}

	inline static int32_t get_offset_of__jomurcaSisowci_4() { return static_cast<int32_t>(offsetof(M_gikaljisYeesa101_t1656339451, ____jomurcaSisowci_4)); }
	inline String_t* get__jomurcaSisowci_4() const { return ____jomurcaSisowci_4; }
	inline String_t** get_address_of__jomurcaSisowci_4() { return &____jomurcaSisowci_4; }
	inline void set__jomurcaSisowci_4(String_t* value)
	{
		____jomurcaSisowci_4 = value;
		Il2CppCodeGenWriteBarrier(&____jomurcaSisowci_4, value);
	}

	inline static int32_t get_offset_of__salcaiStearrichem_5() { return static_cast<int32_t>(offsetof(M_gikaljisYeesa101_t1656339451, ____salcaiStearrichem_5)); }
	inline float get__salcaiStearrichem_5() const { return ____salcaiStearrichem_5; }
	inline float* get_address_of__salcaiStearrichem_5() { return &____salcaiStearrichem_5; }
	inline void set__salcaiStearrichem_5(float value)
	{
		____salcaiStearrichem_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
