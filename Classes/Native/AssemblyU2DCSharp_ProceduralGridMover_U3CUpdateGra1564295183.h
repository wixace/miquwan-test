﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProceduralGridMover/<UpdateGraph>c__AnonStorey112
struct  U3CUpdateGraphU3Ec__AnonStorey112_t1564295183  : public Il2CppObject
{
public:
	// System.Collections.IEnumerator ProceduralGridMover/<UpdateGraph>c__AnonStorey112::ie
	Il2CppObject * ___ie_0;

public:
	inline static int32_t get_offset_of_ie_0() { return static_cast<int32_t>(offsetof(U3CUpdateGraphU3Ec__AnonStorey112_t1564295183, ___ie_0)); }
	inline Il2CppObject * get_ie_0() const { return ___ie_0; }
	inline Il2CppObject ** get_address_of_ie_0() { return &___ie_0; }
	inline void set_ie_0(Il2CppObject * value)
	{
		___ie_0 = value;
		Il2CppCodeGenWriteBarrier(&___ie_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
