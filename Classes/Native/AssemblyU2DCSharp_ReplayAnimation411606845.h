﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_ReplayBase779703160.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayAnimation
struct  ReplayAnimation_t411606845  : public ReplayBase_t779703160
{
public:
	// System.String ReplayAnimation::aniType
	String_t* ___aniType_8;
	// System.Single ReplayAnimation::playTime
	float ___playTime_9;

public:
	inline static int32_t get_offset_of_aniType_8() { return static_cast<int32_t>(offsetof(ReplayAnimation_t411606845, ___aniType_8)); }
	inline String_t* get_aniType_8() const { return ___aniType_8; }
	inline String_t** get_address_of_aniType_8() { return &___aniType_8; }
	inline void set_aniType_8(String_t* value)
	{
		___aniType_8 = value;
		Il2CppCodeGenWriteBarrier(&___aniType_8, value);
	}

	inline static int32_t get_offset_of_playTime_9() { return static_cast<int32_t>(offsetof(ReplayAnimation_t411606845, ___playTime_9)); }
	inline float get_playTime_9() const { return ___playTime_9; }
	inline float* get_address_of_playTime_9() { return &___playTime_9; }
	inline void set_playTime_9(float value)
	{
		___playTime_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
