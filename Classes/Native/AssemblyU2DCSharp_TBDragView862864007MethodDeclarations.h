﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TBDragView
struct TBDragView_t862864007;
// DragGesture
struct DragGesture_t2914643285;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DragGesture2914643285.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_TBDragView862864007.h"

// System.Void TBDragView::.ctor()
extern "C"  void TBDragView__ctor_m637681140 (TBDragView_t862864007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBDragView::Awake()
extern "C"  void TBDragView_Awake_m875286359 (TBDragView_t862864007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBDragView::Start()
extern "C"  void TBDragView_Start_m3879786228 (TBDragView_t862864007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TBDragView::get_Dragging()
extern "C"  bool TBDragView_get_Dragging_m2959516060 (TBDragView_t862864007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBDragView::OnDrag(DragGesture)
extern "C"  void TBDragView_OnDrag_m4086006106 (TBDragView_t862864007 * __this, DragGesture_t2914643285 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBDragView::Update()
extern "C"  void TBDragView_Update_m20140953 (TBDragView_t862864007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TBDragView::NormalizePitch(System.Single)
extern "C"  float TBDragView_NormalizePitch_m4238461372 (Il2CppObject * __this /* static, unused */, float ___angle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion TBDragView::get_IdealRotation()
extern "C"  Quaternion_t1553702882  TBDragView_get_IdealRotation_m3097739476 (TBDragView_t862864007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBDragView::set_IdealRotation(UnityEngine.Quaternion)
extern "C"  void TBDragView_set_IdealRotation_m1071307699 (TBDragView_t862864007 * __this, Quaternion_t1553702882  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBDragView::LookAt(UnityEngine.Vector3)
extern "C"  void TBDragView_LookAt_m1742896343 (TBDragView_t862864007 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TBDragView::ilo_NormalizePitch1(System.Single)
extern "C"  float TBDragView_ilo_NormalizePitch1_m2280519226 (Il2CppObject * __this /* static, unused */, float ___angle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion TBDragView::ilo_get_IdealRotation2(TBDragView)
extern "C"  Quaternion_t1553702882  TBDragView_ilo_get_IdealRotation2_m2326522956 (Il2CppObject * __this /* static, unused */, TBDragView_t862864007 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
