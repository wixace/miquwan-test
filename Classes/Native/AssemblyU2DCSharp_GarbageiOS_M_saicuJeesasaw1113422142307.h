﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_saicuJeesasaw111
struct  M_saicuJeesasaw111_t3422142307  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_saicuJeesasaw111::_saworPitawku
	bool ____saworPitawku_0;
	// System.Single GarbageiOS.M_saicuJeesasaw111::_sestalldouSaiwu
	float ____sestalldouSaiwu_1;
	// System.UInt32 GarbageiOS.M_saicuJeesasaw111::_dobee
	uint32_t ____dobee_2;
	// System.String GarbageiOS.M_saicuJeesasaw111::_luchaiVeasti
	String_t* ____luchaiVeasti_3;
	// System.String GarbageiOS.M_saicuJeesasaw111::_somir
	String_t* ____somir_4;
	// System.Int32 GarbageiOS.M_saicuJeesasaw111::_cheresallrem
	int32_t ____cheresallrem_5;

public:
	inline static int32_t get_offset_of__saworPitawku_0() { return static_cast<int32_t>(offsetof(M_saicuJeesasaw111_t3422142307, ____saworPitawku_0)); }
	inline bool get__saworPitawku_0() const { return ____saworPitawku_0; }
	inline bool* get_address_of__saworPitawku_0() { return &____saworPitawku_0; }
	inline void set__saworPitawku_0(bool value)
	{
		____saworPitawku_0 = value;
	}

	inline static int32_t get_offset_of__sestalldouSaiwu_1() { return static_cast<int32_t>(offsetof(M_saicuJeesasaw111_t3422142307, ____sestalldouSaiwu_1)); }
	inline float get__sestalldouSaiwu_1() const { return ____sestalldouSaiwu_1; }
	inline float* get_address_of__sestalldouSaiwu_1() { return &____sestalldouSaiwu_1; }
	inline void set__sestalldouSaiwu_1(float value)
	{
		____sestalldouSaiwu_1 = value;
	}

	inline static int32_t get_offset_of__dobee_2() { return static_cast<int32_t>(offsetof(M_saicuJeesasaw111_t3422142307, ____dobee_2)); }
	inline uint32_t get__dobee_2() const { return ____dobee_2; }
	inline uint32_t* get_address_of__dobee_2() { return &____dobee_2; }
	inline void set__dobee_2(uint32_t value)
	{
		____dobee_2 = value;
	}

	inline static int32_t get_offset_of__luchaiVeasti_3() { return static_cast<int32_t>(offsetof(M_saicuJeesasaw111_t3422142307, ____luchaiVeasti_3)); }
	inline String_t* get__luchaiVeasti_3() const { return ____luchaiVeasti_3; }
	inline String_t** get_address_of__luchaiVeasti_3() { return &____luchaiVeasti_3; }
	inline void set__luchaiVeasti_3(String_t* value)
	{
		____luchaiVeasti_3 = value;
		Il2CppCodeGenWriteBarrier(&____luchaiVeasti_3, value);
	}

	inline static int32_t get_offset_of__somir_4() { return static_cast<int32_t>(offsetof(M_saicuJeesasaw111_t3422142307, ____somir_4)); }
	inline String_t* get__somir_4() const { return ____somir_4; }
	inline String_t** get_address_of__somir_4() { return &____somir_4; }
	inline void set__somir_4(String_t* value)
	{
		____somir_4 = value;
		Il2CppCodeGenWriteBarrier(&____somir_4, value);
	}

	inline static int32_t get_offset_of__cheresallrem_5() { return static_cast<int32_t>(offsetof(M_saicuJeesasaw111_t3422142307, ____cheresallrem_5)); }
	inline int32_t get__cheresallrem_5() const { return ____cheresallrem_5; }
	inline int32_t* get_address_of__cheresallrem_5() { return &____cheresallrem_5; }
	inline void set__cheresallrem_5(int32_t value)
	{
		____cheresallrem_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
