﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<SoundCfg>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1865153224(__this, ___l0, method) ((  void (*) (Enumerator_t3195133575 *, List_1_t3175460805 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SoundCfg>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3369303178(__this, method) ((  void (*) (Enumerator_t3195133575 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<SoundCfg>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2166766848(__this, method) ((  Il2CppObject * (*) (Enumerator_t3195133575 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SoundCfg>::Dispose()
#define Enumerator_Dispose_m903723693(__this, method) ((  void (*) (Enumerator_t3195133575 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SoundCfg>::VerifyState()
#define Enumerator_VerifyState_m2938788710(__this, method) ((  void (*) (Enumerator_t3195133575 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<SoundCfg>::MoveNext()
#define Enumerator_MoveNext_m877677370(__this, method) ((  bool (*) (Enumerator_t3195133575 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<SoundCfg>::get_Current()
#define Enumerator_get_Current_m1167998975(__this, method) ((  SoundCfg_t1807275253 * (*) (Enumerator_t3195133575 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
