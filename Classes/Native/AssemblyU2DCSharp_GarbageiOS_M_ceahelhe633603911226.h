﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_ceahelhe63
struct  M_ceahelhe63_t3603911226  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_ceahelhe63::_wirbar
	uint32_t ____wirbar_0;
	// System.Boolean GarbageiOS.M_ceahelhe63::_corkouMallpaytear
	bool ____corkouMallpaytear_1;
	// System.Int32 GarbageiOS.M_ceahelhe63::_dralsou
	int32_t ____dralsou_2;
	// System.Boolean GarbageiOS.M_ceahelhe63::_jarpedra
	bool ____jarpedra_3;
	// System.Boolean GarbageiOS.M_ceahelhe63::_keferexeeSinarrai
	bool ____keferexeeSinarrai_4;
	// System.String GarbageiOS.M_ceahelhe63::_jemroovelFownastraw
	String_t* ____jemroovelFownastraw_5;
	// System.UInt32 GarbageiOS.M_ceahelhe63::_tokeeStawwemnir
	uint32_t ____tokeeStawwemnir_6;
	// System.Boolean GarbageiOS.M_ceahelhe63::_chedarheHouwa
	bool ____chedarheHouwa_7;
	// System.UInt32 GarbageiOS.M_ceahelhe63::_sirturtuJearwea
	uint32_t ____sirturtuJearwea_8;
	// System.String GarbageiOS.M_ceahelhe63::_hoolovou
	String_t* ____hoolovou_9;
	// System.String GarbageiOS.M_ceahelhe63::_quste
	String_t* ____quste_10;
	// System.Int32 GarbageiOS.M_ceahelhe63::_douballJalache
	int32_t ____douballJalache_11;
	// System.Single GarbageiOS.M_ceahelhe63::_troteboo
	float ____troteboo_12;
	// System.Boolean GarbageiOS.M_ceahelhe63::_licayxe
	bool ____licayxe_13;
	// System.String GarbageiOS.M_ceahelhe63::_wersisMemzouhay
	String_t* ____wersisMemzouhay_14;
	// System.UInt32 GarbageiOS.M_ceahelhe63::_sistou
	uint32_t ____sistou_15;
	// System.String GarbageiOS.M_ceahelhe63::_kusaynall
	String_t* ____kusaynall_16;
	// System.Single GarbageiOS.M_ceahelhe63::_luyeneaDallxu
	float ____luyeneaDallxu_17;
	// System.Boolean GarbageiOS.M_ceahelhe63::_karcisbur
	bool ____karcisbur_18;
	// System.Boolean GarbageiOS.M_ceahelhe63::_trairzearsair
	bool ____trairzearsair_19;
	// System.Single GarbageiOS.M_ceahelhe63::_deefouBerezalsa
	float ____deefouBerezalsa_20;
	// System.String GarbageiOS.M_ceahelhe63::_geme
	String_t* ____geme_21;
	// System.Single GarbageiOS.M_ceahelhe63::_sigaiChasnear
	float ____sigaiChasnear_22;
	// System.Single GarbageiOS.M_ceahelhe63::_xalqogow
	float ____xalqogow_23;
	// System.Single GarbageiOS.M_ceahelhe63::_sertouYellemto
	float ____sertouYellemto_24;
	// System.Int32 GarbageiOS.M_ceahelhe63::_chimor
	int32_t ____chimor_25;
	// System.Boolean GarbageiOS.M_ceahelhe63::_hokaBaycisbear
	bool ____hokaBaycisbear_26;
	// System.UInt32 GarbageiOS.M_ceahelhe63::_nujaynu
	uint32_t ____nujaynu_27;
	// System.Single GarbageiOS.M_ceahelhe63::_dehou
	float ____dehou_28;
	// System.Boolean GarbageiOS.M_ceahelhe63::_gesorfair
	bool ____gesorfair_29;

public:
	inline static int32_t get_offset_of__wirbar_0() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____wirbar_0)); }
	inline uint32_t get__wirbar_0() const { return ____wirbar_0; }
	inline uint32_t* get_address_of__wirbar_0() { return &____wirbar_0; }
	inline void set__wirbar_0(uint32_t value)
	{
		____wirbar_0 = value;
	}

	inline static int32_t get_offset_of__corkouMallpaytear_1() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____corkouMallpaytear_1)); }
	inline bool get__corkouMallpaytear_1() const { return ____corkouMallpaytear_1; }
	inline bool* get_address_of__corkouMallpaytear_1() { return &____corkouMallpaytear_1; }
	inline void set__corkouMallpaytear_1(bool value)
	{
		____corkouMallpaytear_1 = value;
	}

	inline static int32_t get_offset_of__dralsou_2() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____dralsou_2)); }
	inline int32_t get__dralsou_2() const { return ____dralsou_2; }
	inline int32_t* get_address_of__dralsou_2() { return &____dralsou_2; }
	inline void set__dralsou_2(int32_t value)
	{
		____dralsou_2 = value;
	}

	inline static int32_t get_offset_of__jarpedra_3() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____jarpedra_3)); }
	inline bool get__jarpedra_3() const { return ____jarpedra_3; }
	inline bool* get_address_of__jarpedra_3() { return &____jarpedra_3; }
	inline void set__jarpedra_3(bool value)
	{
		____jarpedra_3 = value;
	}

	inline static int32_t get_offset_of__keferexeeSinarrai_4() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____keferexeeSinarrai_4)); }
	inline bool get__keferexeeSinarrai_4() const { return ____keferexeeSinarrai_4; }
	inline bool* get_address_of__keferexeeSinarrai_4() { return &____keferexeeSinarrai_4; }
	inline void set__keferexeeSinarrai_4(bool value)
	{
		____keferexeeSinarrai_4 = value;
	}

	inline static int32_t get_offset_of__jemroovelFownastraw_5() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____jemroovelFownastraw_5)); }
	inline String_t* get__jemroovelFownastraw_5() const { return ____jemroovelFownastraw_5; }
	inline String_t** get_address_of__jemroovelFownastraw_5() { return &____jemroovelFownastraw_5; }
	inline void set__jemroovelFownastraw_5(String_t* value)
	{
		____jemroovelFownastraw_5 = value;
		Il2CppCodeGenWriteBarrier(&____jemroovelFownastraw_5, value);
	}

	inline static int32_t get_offset_of__tokeeStawwemnir_6() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____tokeeStawwemnir_6)); }
	inline uint32_t get__tokeeStawwemnir_6() const { return ____tokeeStawwemnir_6; }
	inline uint32_t* get_address_of__tokeeStawwemnir_6() { return &____tokeeStawwemnir_6; }
	inline void set__tokeeStawwemnir_6(uint32_t value)
	{
		____tokeeStawwemnir_6 = value;
	}

	inline static int32_t get_offset_of__chedarheHouwa_7() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____chedarheHouwa_7)); }
	inline bool get__chedarheHouwa_7() const { return ____chedarheHouwa_7; }
	inline bool* get_address_of__chedarheHouwa_7() { return &____chedarheHouwa_7; }
	inline void set__chedarheHouwa_7(bool value)
	{
		____chedarheHouwa_7 = value;
	}

	inline static int32_t get_offset_of__sirturtuJearwea_8() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____sirturtuJearwea_8)); }
	inline uint32_t get__sirturtuJearwea_8() const { return ____sirturtuJearwea_8; }
	inline uint32_t* get_address_of__sirturtuJearwea_8() { return &____sirturtuJearwea_8; }
	inline void set__sirturtuJearwea_8(uint32_t value)
	{
		____sirturtuJearwea_8 = value;
	}

	inline static int32_t get_offset_of__hoolovou_9() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____hoolovou_9)); }
	inline String_t* get__hoolovou_9() const { return ____hoolovou_9; }
	inline String_t** get_address_of__hoolovou_9() { return &____hoolovou_9; }
	inline void set__hoolovou_9(String_t* value)
	{
		____hoolovou_9 = value;
		Il2CppCodeGenWriteBarrier(&____hoolovou_9, value);
	}

	inline static int32_t get_offset_of__quste_10() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____quste_10)); }
	inline String_t* get__quste_10() const { return ____quste_10; }
	inline String_t** get_address_of__quste_10() { return &____quste_10; }
	inline void set__quste_10(String_t* value)
	{
		____quste_10 = value;
		Il2CppCodeGenWriteBarrier(&____quste_10, value);
	}

	inline static int32_t get_offset_of__douballJalache_11() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____douballJalache_11)); }
	inline int32_t get__douballJalache_11() const { return ____douballJalache_11; }
	inline int32_t* get_address_of__douballJalache_11() { return &____douballJalache_11; }
	inline void set__douballJalache_11(int32_t value)
	{
		____douballJalache_11 = value;
	}

	inline static int32_t get_offset_of__troteboo_12() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____troteboo_12)); }
	inline float get__troteboo_12() const { return ____troteboo_12; }
	inline float* get_address_of__troteboo_12() { return &____troteboo_12; }
	inline void set__troteboo_12(float value)
	{
		____troteboo_12 = value;
	}

	inline static int32_t get_offset_of__licayxe_13() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____licayxe_13)); }
	inline bool get__licayxe_13() const { return ____licayxe_13; }
	inline bool* get_address_of__licayxe_13() { return &____licayxe_13; }
	inline void set__licayxe_13(bool value)
	{
		____licayxe_13 = value;
	}

	inline static int32_t get_offset_of__wersisMemzouhay_14() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____wersisMemzouhay_14)); }
	inline String_t* get__wersisMemzouhay_14() const { return ____wersisMemzouhay_14; }
	inline String_t** get_address_of__wersisMemzouhay_14() { return &____wersisMemzouhay_14; }
	inline void set__wersisMemzouhay_14(String_t* value)
	{
		____wersisMemzouhay_14 = value;
		Il2CppCodeGenWriteBarrier(&____wersisMemzouhay_14, value);
	}

	inline static int32_t get_offset_of__sistou_15() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____sistou_15)); }
	inline uint32_t get__sistou_15() const { return ____sistou_15; }
	inline uint32_t* get_address_of__sistou_15() { return &____sistou_15; }
	inline void set__sistou_15(uint32_t value)
	{
		____sistou_15 = value;
	}

	inline static int32_t get_offset_of__kusaynall_16() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____kusaynall_16)); }
	inline String_t* get__kusaynall_16() const { return ____kusaynall_16; }
	inline String_t** get_address_of__kusaynall_16() { return &____kusaynall_16; }
	inline void set__kusaynall_16(String_t* value)
	{
		____kusaynall_16 = value;
		Il2CppCodeGenWriteBarrier(&____kusaynall_16, value);
	}

	inline static int32_t get_offset_of__luyeneaDallxu_17() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____luyeneaDallxu_17)); }
	inline float get__luyeneaDallxu_17() const { return ____luyeneaDallxu_17; }
	inline float* get_address_of__luyeneaDallxu_17() { return &____luyeneaDallxu_17; }
	inline void set__luyeneaDallxu_17(float value)
	{
		____luyeneaDallxu_17 = value;
	}

	inline static int32_t get_offset_of__karcisbur_18() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____karcisbur_18)); }
	inline bool get__karcisbur_18() const { return ____karcisbur_18; }
	inline bool* get_address_of__karcisbur_18() { return &____karcisbur_18; }
	inline void set__karcisbur_18(bool value)
	{
		____karcisbur_18 = value;
	}

	inline static int32_t get_offset_of__trairzearsair_19() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____trairzearsair_19)); }
	inline bool get__trairzearsair_19() const { return ____trairzearsair_19; }
	inline bool* get_address_of__trairzearsair_19() { return &____trairzearsair_19; }
	inline void set__trairzearsair_19(bool value)
	{
		____trairzearsair_19 = value;
	}

	inline static int32_t get_offset_of__deefouBerezalsa_20() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____deefouBerezalsa_20)); }
	inline float get__deefouBerezalsa_20() const { return ____deefouBerezalsa_20; }
	inline float* get_address_of__deefouBerezalsa_20() { return &____deefouBerezalsa_20; }
	inline void set__deefouBerezalsa_20(float value)
	{
		____deefouBerezalsa_20 = value;
	}

	inline static int32_t get_offset_of__geme_21() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____geme_21)); }
	inline String_t* get__geme_21() const { return ____geme_21; }
	inline String_t** get_address_of__geme_21() { return &____geme_21; }
	inline void set__geme_21(String_t* value)
	{
		____geme_21 = value;
		Il2CppCodeGenWriteBarrier(&____geme_21, value);
	}

	inline static int32_t get_offset_of__sigaiChasnear_22() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____sigaiChasnear_22)); }
	inline float get__sigaiChasnear_22() const { return ____sigaiChasnear_22; }
	inline float* get_address_of__sigaiChasnear_22() { return &____sigaiChasnear_22; }
	inline void set__sigaiChasnear_22(float value)
	{
		____sigaiChasnear_22 = value;
	}

	inline static int32_t get_offset_of__xalqogow_23() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____xalqogow_23)); }
	inline float get__xalqogow_23() const { return ____xalqogow_23; }
	inline float* get_address_of__xalqogow_23() { return &____xalqogow_23; }
	inline void set__xalqogow_23(float value)
	{
		____xalqogow_23 = value;
	}

	inline static int32_t get_offset_of__sertouYellemto_24() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____sertouYellemto_24)); }
	inline float get__sertouYellemto_24() const { return ____sertouYellemto_24; }
	inline float* get_address_of__sertouYellemto_24() { return &____sertouYellemto_24; }
	inline void set__sertouYellemto_24(float value)
	{
		____sertouYellemto_24 = value;
	}

	inline static int32_t get_offset_of__chimor_25() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____chimor_25)); }
	inline int32_t get__chimor_25() const { return ____chimor_25; }
	inline int32_t* get_address_of__chimor_25() { return &____chimor_25; }
	inline void set__chimor_25(int32_t value)
	{
		____chimor_25 = value;
	}

	inline static int32_t get_offset_of__hokaBaycisbear_26() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____hokaBaycisbear_26)); }
	inline bool get__hokaBaycisbear_26() const { return ____hokaBaycisbear_26; }
	inline bool* get_address_of__hokaBaycisbear_26() { return &____hokaBaycisbear_26; }
	inline void set__hokaBaycisbear_26(bool value)
	{
		____hokaBaycisbear_26 = value;
	}

	inline static int32_t get_offset_of__nujaynu_27() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____nujaynu_27)); }
	inline uint32_t get__nujaynu_27() const { return ____nujaynu_27; }
	inline uint32_t* get_address_of__nujaynu_27() { return &____nujaynu_27; }
	inline void set__nujaynu_27(uint32_t value)
	{
		____nujaynu_27 = value;
	}

	inline static int32_t get_offset_of__dehou_28() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____dehou_28)); }
	inline float get__dehou_28() const { return ____dehou_28; }
	inline float* get_address_of__dehou_28() { return &____dehou_28; }
	inline void set__dehou_28(float value)
	{
		____dehou_28 = value;
	}

	inline static int32_t get_offset_of__gesorfair_29() { return static_cast<int32_t>(offsetof(M_ceahelhe63_t3603911226, ____gesorfair_29)); }
	inline bool get__gesorfair_29() const { return ____gesorfair_29; }
	inline bool* get_address_of__gesorfair_29() { return &____gesorfair_29; }
	inline void set__gesorfair_29(bool value)
	{
		____gesorfair_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
