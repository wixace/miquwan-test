﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RichAI/<TraverseSpecial>c__Iterator6
struct U3CTraverseSpecialU3Ec__Iterator6_t3936386231;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.RichAI/<TraverseSpecial>c__Iterator6::.ctor()
extern "C"  void U3CTraverseSpecialU3Ec__Iterator6__ctor_m4014057412 (U3CTraverseSpecialU3Ec__Iterator6_t3936386231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.RichAI/<TraverseSpecial>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTraverseSpecialU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m129972888 (U3CTraverseSpecialU3Ec__Iterator6_t3936386231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.RichAI/<TraverseSpecial>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTraverseSpecialU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m4163464748 (U3CTraverseSpecialU3Ec__Iterator6_t3936386231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RichAI/<TraverseSpecial>c__Iterator6::MoveNext()
extern "C"  bool U3CTraverseSpecialU3Ec__Iterator6_MoveNext_m275812248 (U3CTraverseSpecialU3Ec__Iterator6_t3936386231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichAI/<TraverseSpecial>c__Iterator6::Dispose()
extern "C"  void U3CTraverseSpecialU3Ec__Iterator6_Dispose_m526824193 (U3CTraverseSpecialU3Ec__Iterator6_t3936386231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichAI/<TraverseSpecial>c__Iterator6::Reset()
extern "C"  void U3CTraverseSpecialU3Ec__Iterator6_Reset_m1660490353 (U3CTraverseSpecialU3Ec__Iterator6_t3936386231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
