﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlowControl.FlowUnzipAsset
struct FlowUnzipAsset_t2928582570;
// UnzipABAsset
struct UnzipABAsset_t414492807;

#include "codegen/il2cpp-codegen.h"

// System.Void FlowControl.FlowUnzipAsset::.ctor()
extern "C"  void FlowUnzipAsset__ctor_m1192861382 (FlowUnzipAsset_t2928582570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowUnzipAsset::Activate()
extern "C"  void FlowUnzipAsset_Activate_m3969637233 (FlowUnzipAsset_t2928582570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnzipABAsset FlowControl.FlowUnzipAsset::ilo_get_Instance1()
extern "C"  UnzipABAsset_t414492807 * FlowUnzipAsset_ilo_get_Instance1_m1841268558 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
