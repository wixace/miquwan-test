﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._3f3cbe422021c894980a2c8a553bb1bd
struct _3f3cbe422021c894980a2c8a553bb1bd_t780502516;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__3f3cbe422021c894980a2c8a5780502516.h"

// System.Void Little._3f3cbe422021c894980a2c8a553bb1bd::.ctor()
extern "C"  void _3f3cbe422021c894980a2c8a553bb1bd__ctor_m279122617 (_3f3cbe422021c894980a2c8a553bb1bd_t780502516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._3f3cbe422021c894980a2c8a553bb1bd::_3f3cbe422021c894980a2c8a553bb1bdm2(System.Int32)
extern "C"  int32_t _3f3cbe422021c894980a2c8a553bb1bd__3f3cbe422021c894980a2c8a553bb1bdm2_m3477318937 (_3f3cbe422021c894980a2c8a553bb1bd_t780502516 * __this, int32_t ____3f3cbe422021c894980a2c8a553bb1bda0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._3f3cbe422021c894980a2c8a553bb1bd::_3f3cbe422021c894980a2c8a553bb1bdm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _3f3cbe422021c894980a2c8a553bb1bd__3f3cbe422021c894980a2c8a553bb1bdm_m561965501 (_3f3cbe422021c894980a2c8a553bb1bd_t780502516 * __this, int32_t ____3f3cbe422021c894980a2c8a553bb1bda0, int32_t ____3f3cbe422021c894980a2c8a553bb1bd811, int32_t ____3f3cbe422021c894980a2c8a553bb1bdc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._3f3cbe422021c894980a2c8a553bb1bd::ilo__3f3cbe422021c894980a2c8a553bb1bdm21(Little._3f3cbe422021c894980a2c8a553bb1bd,System.Int32)
extern "C"  int32_t _3f3cbe422021c894980a2c8a553bb1bd_ilo__3f3cbe422021c894980a2c8a553bb1bdm21_m271238715 (Il2CppObject * __this /* static, unused */, _3f3cbe422021c894980a2c8a553bb1bd_t780502516 * ____this0, int32_t ____3f3cbe422021c894980a2c8a553bb1bda1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
