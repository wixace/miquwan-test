﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.BinaryWriter
struct BinaryWriter_t4146364100;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Serialization.AstarSerializer/<SerializeExtraInfo>c__AnonStorey10D
struct  U3CSerializeExtraInfoU3Ec__AnonStorey10D_t889342228  : public Il2CppObject
{
public:
	// System.Int32 Pathfinding.Serialization.AstarSerializer/<SerializeExtraInfo>c__AnonStorey10D::c
	int32_t ___c_0;
	// System.IO.BinaryWriter Pathfinding.Serialization.AstarSerializer/<SerializeExtraInfo>c__AnonStorey10D::wr
	BinaryWriter_t4146364100 * ___wr_1;

public:
	inline static int32_t get_offset_of_c_0() { return static_cast<int32_t>(offsetof(U3CSerializeExtraInfoU3Ec__AnonStorey10D_t889342228, ___c_0)); }
	inline int32_t get_c_0() const { return ___c_0; }
	inline int32_t* get_address_of_c_0() { return &___c_0; }
	inline void set_c_0(int32_t value)
	{
		___c_0 = value;
	}

	inline static int32_t get_offset_of_wr_1() { return static_cast<int32_t>(offsetof(U3CSerializeExtraInfoU3Ec__AnonStorey10D_t889342228, ___wr_1)); }
	inline BinaryWriter_t4146364100 * get_wr_1() const { return ___wr_1; }
	inline BinaryWriter_t4146364100 ** get_address_of_wr_1() { return &___wr_1; }
	inline void set_wr_1(BinaryWriter_t4146364100 * value)
	{
		___wr_1 = value;
		Il2CppCodeGenWriteBarrier(&___wr_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
