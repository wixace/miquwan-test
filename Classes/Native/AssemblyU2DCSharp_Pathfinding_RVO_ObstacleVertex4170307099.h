﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.RVO.ObstacleVertex
struct ObstacleVertex_t4170307099;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_RVOLayer1386980398.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RVO.ObstacleVertex
struct  ObstacleVertex_t4170307099  : public Il2CppObject
{
public:
	// System.Boolean Pathfinding.RVO.ObstacleVertex::ignore
	bool ___ignore_0;
	// UnityEngine.Vector3 Pathfinding.RVO.ObstacleVertex::position
	Vector3_t4282066566  ___position_1;
	// UnityEngine.Vector2 Pathfinding.RVO.ObstacleVertex::dir
	Vector2_t4282066565  ___dir_2;
	// System.Single Pathfinding.RVO.ObstacleVertex::height
	float ___height_3;
	// Pathfinding.RVO.RVOLayer Pathfinding.RVO.ObstacleVertex::layer
	int32_t ___layer_4;
	// System.Boolean Pathfinding.RVO.ObstacleVertex::convex
	bool ___convex_5;
	// System.Boolean Pathfinding.RVO.ObstacleVertex::split
	bool ___split_6;
	// Pathfinding.RVO.ObstacleVertex Pathfinding.RVO.ObstacleVertex::next
	ObstacleVertex_t4170307099 * ___next_7;
	// Pathfinding.RVO.ObstacleVertex Pathfinding.RVO.ObstacleVertex::prev
	ObstacleVertex_t4170307099 * ___prev_8;

public:
	inline static int32_t get_offset_of_ignore_0() { return static_cast<int32_t>(offsetof(ObstacleVertex_t4170307099, ___ignore_0)); }
	inline bool get_ignore_0() const { return ___ignore_0; }
	inline bool* get_address_of_ignore_0() { return &___ignore_0; }
	inline void set_ignore_0(bool value)
	{
		___ignore_0 = value;
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(ObstacleVertex_t4170307099, ___position_1)); }
	inline Vector3_t4282066566  get_position_1() const { return ___position_1; }
	inline Vector3_t4282066566 * get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(Vector3_t4282066566  value)
	{
		___position_1 = value;
	}

	inline static int32_t get_offset_of_dir_2() { return static_cast<int32_t>(offsetof(ObstacleVertex_t4170307099, ___dir_2)); }
	inline Vector2_t4282066565  get_dir_2() const { return ___dir_2; }
	inline Vector2_t4282066565 * get_address_of_dir_2() { return &___dir_2; }
	inline void set_dir_2(Vector2_t4282066565  value)
	{
		___dir_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(ObstacleVertex_t4170307099, ___height_3)); }
	inline float get_height_3() const { return ___height_3; }
	inline float* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(float value)
	{
		___height_3 = value;
	}

	inline static int32_t get_offset_of_layer_4() { return static_cast<int32_t>(offsetof(ObstacleVertex_t4170307099, ___layer_4)); }
	inline int32_t get_layer_4() const { return ___layer_4; }
	inline int32_t* get_address_of_layer_4() { return &___layer_4; }
	inline void set_layer_4(int32_t value)
	{
		___layer_4 = value;
	}

	inline static int32_t get_offset_of_convex_5() { return static_cast<int32_t>(offsetof(ObstacleVertex_t4170307099, ___convex_5)); }
	inline bool get_convex_5() const { return ___convex_5; }
	inline bool* get_address_of_convex_5() { return &___convex_5; }
	inline void set_convex_5(bool value)
	{
		___convex_5 = value;
	}

	inline static int32_t get_offset_of_split_6() { return static_cast<int32_t>(offsetof(ObstacleVertex_t4170307099, ___split_6)); }
	inline bool get_split_6() const { return ___split_6; }
	inline bool* get_address_of_split_6() { return &___split_6; }
	inline void set_split_6(bool value)
	{
		___split_6 = value;
	}

	inline static int32_t get_offset_of_next_7() { return static_cast<int32_t>(offsetof(ObstacleVertex_t4170307099, ___next_7)); }
	inline ObstacleVertex_t4170307099 * get_next_7() const { return ___next_7; }
	inline ObstacleVertex_t4170307099 ** get_address_of_next_7() { return &___next_7; }
	inline void set_next_7(ObstacleVertex_t4170307099 * value)
	{
		___next_7 = value;
		Il2CppCodeGenWriteBarrier(&___next_7, value);
	}

	inline static int32_t get_offset_of_prev_8() { return static_cast<int32_t>(offsetof(ObstacleVertex_t4170307099, ___prev_8)); }
	inline ObstacleVertex_t4170307099 * get_prev_8() const { return ___prev_8; }
	inline ObstacleVertex_t4170307099 ** get_address_of_prev_8() { return &___prev_8; }
	inline void set_prev_8(ObstacleVertex_t4170307099 * value)
	{
		___prev_8 = value;
		Il2CppCodeGenWriteBarrier(&___prev_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
