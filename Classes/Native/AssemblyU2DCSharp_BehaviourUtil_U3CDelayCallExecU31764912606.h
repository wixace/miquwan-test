﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Int32>
struct Action_1_t1549654636;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Int32>
struct  U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606  : public Il2CppObject
{
public:
	// System.UInt32 BehaviourUtil/<DelayCallExec>c__Iterator3F`1::<id>__0
	uint32_t ___U3CidU3E__0_0;
	// System.Single BehaviourUtil/<DelayCallExec>c__Iterator3F`1::delaytime
	float ___delaytime_1;
	// System.Action`1<T1> BehaviourUtil/<DelayCallExec>c__Iterator3F`1::act
	Action_1_t1549654636 * ___act_2;
	// T1 BehaviourUtil/<DelayCallExec>c__Iterator3F`1::arg1
	int32_t ___arg1_3;
	// System.Int32 BehaviourUtil/<DelayCallExec>c__Iterator3F`1::$PC
	int32_t ___U24PC_4;
	// System.Object BehaviourUtil/<DelayCallExec>c__Iterator3F`1::$current
	Il2CppObject * ___U24current_5;
	// System.Single BehaviourUtil/<DelayCallExec>c__Iterator3F`1::<$>delaytime
	float ___U3CU24U3Edelaytime_6;
	// System.Action`1<T1> BehaviourUtil/<DelayCallExec>c__Iterator3F`1::<$>act
	Action_1_t1549654636 * ___U3CU24U3Eact_7;
	// T1 BehaviourUtil/<DelayCallExec>c__Iterator3F`1::<$>arg1
	int32_t ___U3CU24U3Earg1_8;

public:
	inline static int32_t get_offset_of_U3CidU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606, ___U3CidU3E__0_0)); }
	inline uint32_t get_U3CidU3E__0_0() const { return ___U3CidU3E__0_0; }
	inline uint32_t* get_address_of_U3CidU3E__0_0() { return &___U3CidU3E__0_0; }
	inline void set_U3CidU3E__0_0(uint32_t value)
	{
		___U3CidU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_delaytime_1() { return static_cast<int32_t>(offsetof(U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606, ___delaytime_1)); }
	inline float get_delaytime_1() const { return ___delaytime_1; }
	inline float* get_address_of_delaytime_1() { return &___delaytime_1; }
	inline void set_delaytime_1(float value)
	{
		___delaytime_1 = value;
	}

	inline static int32_t get_offset_of_act_2() { return static_cast<int32_t>(offsetof(U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606, ___act_2)); }
	inline Action_1_t1549654636 * get_act_2() const { return ___act_2; }
	inline Action_1_t1549654636 ** get_address_of_act_2() { return &___act_2; }
	inline void set_act_2(Action_1_t1549654636 * value)
	{
		___act_2 = value;
		Il2CppCodeGenWriteBarrier(&___act_2, value);
	}

	inline static int32_t get_offset_of_arg1_3() { return static_cast<int32_t>(offsetof(U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606, ___arg1_3)); }
	inline int32_t get_arg1_3() const { return ___arg1_3; }
	inline int32_t* get_address_of_arg1_3() { return &___arg1_3; }
	inline void set_arg1_3(int32_t value)
	{
		___arg1_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Edelaytime_6() { return static_cast<int32_t>(offsetof(U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606, ___U3CU24U3Edelaytime_6)); }
	inline float get_U3CU24U3Edelaytime_6() const { return ___U3CU24U3Edelaytime_6; }
	inline float* get_address_of_U3CU24U3Edelaytime_6() { return &___U3CU24U3Edelaytime_6; }
	inline void set_U3CU24U3Edelaytime_6(float value)
	{
		___U3CU24U3Edelaytime_6 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Eact_7() { return static_cast<int32_t>(offsetof(U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606, ___U3CU24U3Eact_7)); }
	inline Action_1_t1549654636 * get_U3CU24U3Eact_7() const { return ___U3CU24U3Eact_7; }
	inline Action_1_t1549654636 ** get_address_of_U3CU24U3Eact_7() { return &___U3CU24U3Eact_7; }
	inline void set_U3CU24U3Eact_7(Action_1_t1549654636 * value)
	{
		___U3CU24U3Eact_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eact_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Earg1_8() { return static_cast<int32_t>(offsetof(U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606, ___U3CU24U3Earg1_8)); }
	inline int32_t get_U3CU24U3Earg1_8() const { return ___U3CU24U3Earg1_8; }
	inline int32_t* get_address_of_U3CU24U3Earg1_8() { return &___U3CU24U3Earg1_8; }
	inline void set_U3CU24U3Earg1_8(int32_t value)
	{
		___U3CU24U3Earg1_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
