﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginWanYu
struct  PluginWanYu_t2557377933  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginWanYu::token
	String_t* ___token_2;
	// System.String PluginWanYu::uid
	String_t* ___uid_3;
	// System.String PluginWanYu::notifyId
	String_t* ___notifyId_4;
	// System.String PluginWanYu::appID
	String_t* ___appID_5;
	// System.Boolean PluginWanYu::isOut
	bool ___isOut_6;
	// System.String PluginWanYu::configId
	String_t* ___configId_7;

public:
	inline static int32_t get_offset_of_token_2() { return static_cast<int32_t>(offsetof(PluginWanYu_t2557377933, ___token_2)); }
	inline String_t* get_token_2() const { return ___token_2; }
	inline String_t** get_address_of_token_2() { return &___token_2; }
	inline void set_token_2(String_t* value)
	{
		___token_2 = value;
		Il2CppCodeGenWriteBarrier(&___token_2, value);
	}

	inline static int32_t get_offset_of_uid_3() { return static_cast<int32_t>(offsetof(PluginWanYu_t2557377933, ___uid_3)); }
	inline String_t* get_uid_3() const { return ___uid_3; }
	inline String_t** get_address_of_uid_3() { return &___uid_3; }
	inline void set_uid_3(String_t* value)
	{
		___uid_3 = value;
		Il2CppCodeGenWriteBarrier(&___uid_3, value);
	}

	inline static int32_t get_offset_of_notifyId_4() { return static_cast<int32_t>(offsetof(PluginWanYu_t2557377933, ___notifyId_4)); }
	inline String_t* get_notifyId_4() const { return ___notifyId_4; }
	inline String_t** get_address_of_notifyId_4() { return &___notifyId_4; }
	inline void set_notifyId_4(String_t* value)
	{
		___notifyId_4 = value;
		Il2CppCodeGenWriteBarrier(&___notifyId_4, value);
	}

	inline static int32_t get_offset_of_appID_5() { return static_cast<int32_t>(offsetof(PluginWanYu_t2557377933, ___appID_5)); }
	inline String_t* get_appID_5() const { return ___appID_5; }
	inline String_t** get_address_of_appID_5() { return &___appID_5; }
	inline void set_appID_5(String_t* value)
	{
		___appID_5 = value;
		Il2CppCodeGenWriteBarrier(&___appID_5, value);
	}

	inline static int32_t get_offset_of_isOut_6() { return static_cast<int32_t>(offsetof(PluginWanYu_t2557377933, ___isOut_6)); }
	inline bool get_isOut_6() const { return ___isOut_6; }
	inline bool* get_address_of_isOut_6() { return &___isOut_6; }
	inline void set_isOut_6(bool value)
	{
		___isOut_6 = value;
	}

	inline static int32_t get_offset_of_configId_7() { return static_cast<int32_t>(offsetof(PluginWanYu_t2557377933, ___configId_7)); }
	inline String_t* get_configId_7() const { return ___configId_7; }
	inline String_t** get_address_of_configId_7() { return &___configId_7; }
	inline void set_configId_7(String_t* value)
	{
		___configId_7 = value;
		Il2CppCodeGenWriteBarrier(&___configId_7, value);
	}
};

struct PluginWanYu_t2557377933_StaticFields
{
public:
	// System.Action PluginWanYu::<>f__am$cache6
	Action_t3771233898 * ___U3CU3Ef__amU24cache6_8;
	// System.Action PluginWanYu::<>f__am$cache7
	Action_t3771233898 * ___U3CU3Ef__amU24cache7_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_8() { return static_cast<int32_t>(offsetof(PluginWanYu_t2557377933_StaticFields, ___U3CU3Ef__amU24cache6_8)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache6_8() const { return ___U3CU3Ef__amU24cache6_8; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache6_8() { return &___U3CU3Ef__amU24cache6_8; }
	inline void set_U3CU3Ef__amU24cache6_8(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache6_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_9() { return static_cast<int32_t>(offsetof(PluginWanYu_t2557377933_StaticFields, ___U3CU3Ef__amU24cache7_9)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache7_9() const { return ___U3CU3Ef__amU24cache7_9; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache7_9() { return &___U3CU3Ef__amU24cache7_9; }
	inline void set_U3CU3Ef__amU24cache7_9(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache7_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
