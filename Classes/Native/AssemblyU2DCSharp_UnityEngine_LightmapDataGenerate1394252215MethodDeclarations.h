﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_LightmapDataGenerated
struct UnityEngine_LightmapDataGenerated_t1394252215;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_LightmapDataGenerated::.ctor()
extern "C"  void UnityEngine_LightmapDataGenerated__ctor_m2990424468 (UnityEngine_LightmapDataGenerated_t1394252215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LightmapDataGenerated::LightmapData_LightmapData1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LightmapDataGenerated_LightmapData_LightmapData1_m2006443486 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightmapDataGenerated::LightmapData_lightmapFar(JSVCall)
extern "C"  void UnityEngine_LightmapDataGenerated_LightmapData_lightmapFar_m3633265141 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightmapDataGenerated::LightmapData_lightmapNear(JSVCall)
extern "C"  void UnityEngine_LightmapDataGenerated_LightmapData_lightmapNear_m2924345720 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightmapDataGenerated::__Register()
extern "C"  void UnityEngine_LightmapDataGenerated___Register_m1370560435 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LightmapDataGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_LightmapDataGenerated_ilo_setObject1_m3979546650 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_LightmapDataGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_LightmapDataGenerated_ilo_getObject2_m1281866002 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
