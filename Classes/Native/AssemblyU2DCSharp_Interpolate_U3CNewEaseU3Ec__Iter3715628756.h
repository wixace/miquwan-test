﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEnumerable`1<System.Single>
struct IEnumerable_1_t3297864633;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t1908816725;
// Interpolate/Function
struct Function_t4102301030;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Interpolate/<NewEase>c__Iterator23
struct  U3CNewEaseU3Ec__Iterator23_t3715628756  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 Interpolate/<NewEase>c__Iterator23::end
	Vector3_t4282066566  ___end_0;
	// UnityEngine.Vector3 Interpolate/<NewEase>c__Iterator23::start
	Vector3_t4282066566  ___start_1;
	// UnityEngine.Vector3 Interpolate/<NewEase>c__Iterator23::<distance>__0
	Vector3_t4282066566  ___U3CdistanceU3E__0_2;
	// System.Collections.Generic.IEnumerable`1<System.Single> Interpolate/<NewEase>c__Iterator23::driver
	Il2CppObject* ___driver_3;
	// System.Collections.Generic.IEnumerator`1<System.Single> Interpolate/<NewEase>c__Iterator23::<$s_175>__1
	Il2CppObject* ___U3CU24s_175U3E__1_4;
	// System.Single Interpolate/<NewEase>c__Iterator23::<i>__2
	float ___U3CiU3E__2_5;
	// Interpolate/Function Interpolate/<NewEase>c__Iterator23::ease
	Function_t4102301030 * ___ease_6;
	// System.Single Interpolate/<NewEase>c__Iterator23::total
	float ___total_7;
	// System.Int32 Interpolate/<NewEase>c__Iterator23::$PC
	int32_t ___U24PC_8;
	// System.Object Interpolate/<NewEase>c__Iterator23::$current
	Il2CppObject * ___U24current_9;
	// UnityEngine.Vector3 Interpolate/<NewEase>c__Iterator23::<$>end
	Vector3_t4282066566  ___U3CU24U3Eend_10;
	// UnityEngine.Vector3 Interpolate/<NewEase>c__Iterator23::<$>start
	Vector3_t4282066566  ___U3CU24U3Estart_11;
	// System.Collections.Generic.IEnumerable`1<System.Single> Interpolate/<NewEase>c__Iterator23::<$>driver
	Il2CppObject* ___U3CU24U3Edriver_12;
	// Interpolate/Function Interpolate/<NewEase>c__Iterator23::<$>ease
	Function_t4102301030 * ___U3CU24U3Eease_13;
	// System.Single Interpolate/<NewEase>c__Iterator23::<$>total
	float ___U3CU24U3Etotal_14;

public:
	inline static int32_t get_offset_of_end_0() { return static_cast<int32_t>(offsetof(U3CNewEaseU3Ec__Iterator23_t3715628756, ___end_0)); }
	inline Vector3_t4282066566  get_end_0() const { return ___end_0; }
	inline Vector3_t4282066566 * get_address_of_end_0() { return &___end_0; }
	inline void set_end_0(Vector3_t4282066566  value)
	{
		___end_0 = value;
	}

	inline static int32_t get_offset_of_start_1() { return static_cast<int32_t>(offsetof(U3CNewEaseU3Ec__Iterator23_t3715628756, ___start_1)); }
	inline Vector3_t4282066566  get_start_1() const { return ___start_1; }
	inline Vector3_t4282066566 * get_address_of_start_1() { return &___start_1; }
	inline void set_start_1(Vector3_t4282066566  value)
	{
		___start_1 = value;
	}

	inline static int32_t get_offset_of_U3CdistanceU3E__0_2() { return static_cast<int32_t>(offsetof(U3CNewEaseU3Ec__Iterator23_t3715628756, ___U3CdistanceU3E__0_2)); }
	inline Vector3_t4282066566  get_U3CdistanceU3E__0_2() const { return ___U3CdistanceU3E__0_2; }
	inline Vector3_t4282066566 * get_address_of_U3CdistanceU3E__0_2() { return &___U3CdistanceU3E__0_2; }
	inline void set_U3CdistanceU3E__0_2(Vector3_t4282066566  value)
	{
		___U3CdistanceU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_driver_3() { return static_cast<int32_t>(offsetof(U3CNewEaseU3Ec__Iterator23_t3715628756, ___driver_3)); }
	inline Il2CppObject* get_driver_3() const { return ___driver_3; }
	inline Il2CppObject** get_address_of_driver_3() { return &___driver_3; }
	inline void set_driver_3(Il2CppObject* value)
	{
		___driver_3 = value;
		Il2CppCodeGenWriteBarrier(&___driver_3, value);
	}

	inline static int32_t get_offset_of_U3CU24s_175U3E__1_4() { return static_cast<int32_t>(offsetof(U3CNewEaseU3Ec__Iterator23_t3715628756, ___U3CU24s_175U3E__1_4)); }
	inline Il2CppObject* get_U3CU24s_175U3E__1_4() const { return ___U3CU24s_175U3E__1_4; }
	inline Il2CppObject** get_address_of_U3CU24s_175U3E__1_4() { return &___U3CU24s_175U3E__1_4; }
	inline void set_U3CU24s_175U3E__1_4(Il2CppObject* value)
	{
		___U3CU24s_175U3E__1_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_175U3E__1_4, value);
	}

	inline static int32_t get_offset_of_U3CiU3E__2_5() { return static_cast<int32_t>(offsetof(U3CNewEaseU3Ec__Iterator23_t3715628756, ___U3CiU3E__2_5)); }
	inline float get_U3CiU3E__2_5() const { return ___U3CiU3E__2_5; }
	inline float* get_address_of_U3CiU3E__2_5() { return &___U3CiU3E__2_5; }
	inline void set_U3CiU3E__2_5(float value)
	{
		___U3CiU3E__2_5 = value;
	}

	inline static int32_t get_offset_of_ease_6() { return static_cast<int32_t>(offsetof(U3CNewEaseU3Ec__Iterator23_t3715628756, ___ease_6)); }
	inline Function_t4102301030 * get_ease_6() const { return ___ease_6; }
	inline Function_t4102301030 ** get_address_of_ease_6() { return &___ease_6; }
	inline void set_ease_6(Function_t4102301030 * value)
	{
		___ease_6 = value;
		Il2CppCodeGenWriteBarrier(&___ease_6, value);
	}

	inline static int32_t get_offset_of_total_7() { return static_cast<int32_t>(offsetof(U3CNewEaseU3Ec__Iterator23_t3715628756, ___total_7)); }
	inline float get_total_7() const { return ___total_7; }
	inline float* get_address_of_total_7() { return &___total_7; }
	inline void set_total_7(float value)
	{
		___total_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CNewEaseU3Ec__Iterator23_t3715628756, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CNewEaseU3Ec__Iterator23_t3715628756, ___U24current_9)); }
	inline Il2CppObject * get_U24current_9() const { return ___U24current_9; }
	inline Il2CppObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(Il2CppObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_9, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eend_10() { return static_cast<int32_t>(offsetof(U3CNewEaseU3Ec__Iterator23_t3715628756, ___U3CU24U3Eend_10)); }
	inline Vector3_t4282066566  get_U3CU24U3Eend_10() const { return ___U3CU24U3Eend_10; }
	inline Vector3_t4282066566 * get_address_of_U3CU24U3Eend_10() { return &___U3CU24U3Eend_10; }
	inline void set_U3CU24U3Eend_10(Vector3_t4282066566  value)
	{
		___U3CU24U3Eend_10 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Estart_11() { return static_cast<int32_t>(offsetof(U3CNewEaseU3Ec__Iterator23_t3715628756, ___U3CU24U3Estart_11)); }
	inline Vector3_t4282066566  get_U3CU24U3Estart_11() const { return ___U3CU24U3Estart_11; }
	inline Vector3_t4282066566 * get_address_of_U3CU24U3Estart_11() { return &___U3CU24U3Estart_11; }
	inline void set_U3CU24U3Estart_11(Vector3_t4282066566  value)
	{
		___U3CU24U3Estart_11 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Edriver_12() { return static_cast<int32_t>(offsetof(U3CNewEaseU3Ec__Iterator23_t3715628756, ___U3CU24U3Edriver_12)); }
	inline Il2CppObject* get_U3CU24U3Edriver_12() const { return ___U3CU24U3Edriver_12; }
	inline Il2CppObject** get_address_of_U3CU24U3Edriver_12() { return &___U3CU24U3Edriver_12; }
	inline void set_U3CU24U3Edriver_12(Il2CppObject* value)
	{
		___U3CU24U3Edriver_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Edriver_12, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eease_13() { return static_cast<int32_t>(offsetof(U3CNewEaseU3Ec__Iterator23_t3715628756, ___U3CU24U3Eease_13)); }
	inline Function_t4102301030 * get_U3CU24U3Eease_13() const { return ___U3CU24U3Eease_13; }
	inline Function_t4102301030 ** get_address_of_U3CU24U3Eease_13() { return &___U3CU24U3Eease_13; }
	inline void set_U3CU24U3Eease_13(Function_t4102301030 * value)
	{
		___U3CU24U3Eease_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eease_13, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Etotal_14() { return static_cast<int32_t>(offsetof(U3CNewEaseU3Ec__Iterator23_t3715628756, ___U3CU24U3Etotal_14)); }
	inline float get_U3CU24U3Etotal_14() const { return ___U3CU24U3Etotal_14; }
	inline float* get_address_of_U3CU24U3Etotal_14() { return &___U3CU24U3Etotal_14; }
	inline void set_U3CU24U3Etotal_14(float value)
	{
		___U3CU24U3Etotal_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
