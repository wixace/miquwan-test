﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlowControl.FlowCheckUpdate
struct FlowCheckUpdate_t2442219577;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// FlowControl.FlowBase
struct FlowBase_t3680091731;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// LoadingBoardMgr
struct LoadingBoardMgr_t303632014;
// ABCheckUpdate
struct ABCheckUpdate_t527832336;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_FlowControl_FlowBase3680091731.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_LoadingBoardMgr303632014.h"
#include "AssemblyU2DCSharp_ABCheckUpdate527832336.h"

// System.Void FlowControl.FlowCheckUpdate::.ctor()
extern "C"  void FlowCheckUpdate__ctor_m847471079 (FlowCheckUpdate_t2442219577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowCheckUpdate::Activate()
extern "C"  void FlowCheckUpdate_Activate_m893794480 (FlowCheckUpdate_t2442219577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowCheckUpdate::CheckUpdateedHandler(CEvent.ZEvent)
extern "C"  void FlowCheckUpdate_CheckUpdateedHandler_m176963844 (FlowCheckUpdate_t2442219577 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowCheckUpdate::FlowUpdate(System.Single)
extern "C"  void FlowCheckUpdate_FlowUpdate_m4240974551 (FlowCheckUpdate_t2442219577 * __this, float ___duringTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowCheckUpdate::ilo_Activate1(FlowControl.FlowBase)
extern "C"  void FlowCheckUpdate_ilo_Activate1_m876474162 (Il2CppObject * __this /* static, unused */, FlowBase_t3680091731 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowCheckUpdate::ilo_AddEventListener2(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void FlowCheckUpdate_ilo_AddEventListener2_m35427404 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LoadingBoardMgr FlowControl.FlowCheckUpdate::ilo_get_Instance3()
extern "C"  LoadingBoardMgr_t303632014 * FlowCheckUpdate_ilo_get_Instance3_m2091480332 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowCheckUpdate::ilo_Show4(LoadingBoardMgr)
extern "C"  void FlowCheckUpdate_ilo_Show4_m2671518971 (Il2CppObject * __this /* static, unused */, LoadingBoardMgr_t303632014 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ABCheckUpdate FlowControl.FlowCheckUpdate::ilo_get_Instance5()
extern "C"  ABCheckUpdate_t527832336 * FlowCheckUpdate_ilo_get_Instance5_m3162672396 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowCheckUpdate::ilo_Start6(ABCheckUpdate)
extern "C"  void FlowCheckUpdate_ilo_Start6_m310115678 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowCheckUpdate::ilo_set_isComplete7(FlowControl.FlowBase,System.Boolean)
extern "C"  void FlowCheckUpdate_ilo_set_isComplete7_m1161288754 (Il2CppObject * __this /* static, unused */, FlowBase_t3680091731 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FlowControl.FlowCheckUpdate::ilo_get_Version8(ABCheckUpdate)
extern "C"  String_t* FlowCheckUpdate_ilo_get_Version8_m2923981452 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowCheckUpdate::ilo_FlowUpdate9(FlowControl.FlowBase,System.Single)
extern "C"  void FlowCheckUpdate_ilo_FlowUpdate9_m4226534971 (Il2CppObject * __this /* static, unused */, FlowBase_t3680091731 * ____this0, float ___duringTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowCheckUpdate::ilo_SetPercent10(LoadingBoardMgr,System.Single,System.String)
extern "C"  void FlowCheckUpdate_ilo_SetPercent10_m2592787443 (Il2CppObject * __this /* static, unused */, LoadingBoardMgr_t303632014 * ____this0, float ___value1, String_t* ___msg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FlowControl.FlowCheckUpdate::ilo_get_isStartDown11(ABCheckUpdate)
extern "C"  bool FlowCheckUpdate_ilo_get_isStartDown11_m2261454059 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowCheckUpdate::ilo_StartDown12(ABCheckUpdate)
extern "C"  void FlowCheckUpdate_ilo_StartDown12_m1315793227 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FlowControl.FlowCheckUpdate::ilo_get_IsDownSuccess13(ABCheckUpdate)
extern "C"  bool FlowCheckUpdate_ilo_get_IsDownSuccess13_m887663116 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
