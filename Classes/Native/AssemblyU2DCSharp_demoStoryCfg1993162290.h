﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// demoStoryCfg
struct  demoStoryCfg_t1993162290  : public CsCfgBase_t69924517
{
public:
	// System.Int32 demoStoryCfg::id
	int32_t ___id_0;
	// System.Int32 demoStoryCfg::storyID
	int32_t ___storyID_1;
	// System.Int32 demoStoryCfg::funType
	int32_t ___funType_2;
	// System.String demoStoryCfg::param1
	String_t* ___param1_3;
	// System.String demoStoryCfg::param2
	String_t* ___param2_4;
	// System.String demoStoryCfg::param3
	String_t* ___param3_5;
	// System.String demoStoryCfg::param4
	String_t* ___param4_6;
	// System.String demoStoryCfg::param5
	String_t* ___param5_7;
	// System.String demoStoryCfg::param6
	String_t* ___param6_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(demoStoryCfg_t1993162290, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_storyID_1() { return static_cast<int32_t>(offsetof(demoStoryCfg_t1993162290, ___storyID_1)); }
	inline int32_t get_storyID_1() const { return ___storyID_1; }
	inline int32_t* get_address_of_storyID_1() { return &___storyID_1; }
	inline void set_storyID_1(int32_t value)
	{
		___storyID_1 = value;
	}

	inline static int32_t get_offset_of_funType_2() { return static_cast<int32_t>(offsetof(demoStoryCfg_t1993162290, ___funType_2)); }
	inline int32_t get_funType_2() const { return ___funType_2; }
	inline int32_t* get_address_of_funType_2() { return &___funType_2; }
	inline void set_funType_2(int32_t value)
	{
		___funType_2 = value;
	}

	inline static int32_t get_offset_of_param1_3() { return static_cast<int32_t>(offsetof(demoStoryCfg_t1993162290, ___param1_3)); }
	inline String_t* get_param1_3() const { return ___param1_3; }
	inline String_t** get_address_of_param1_3() { return &___param1_3; }
	inline void set_param1_3(String_t* value)
	{
		___param1_3 = value;
		Il2CppCodeGenWriteBarrier(&___param1_3, value);
	}

	inline static int32_t get_offset_of_param2_4() { return static_cast<int32_t>(offsetof(demoStoryCfg_t1993162290, ___param2_4)); }
	inline String_t* get_param2_4() const { return ___param2_4; }
	inline String_t** get_address_of_param2_4() { return &___param2_4; }
	inline void set_param2_4(String_t* value)
	{
		___param2_4 = value;
		Il2CppCodeGenWriteBarrier(&___param2_4, value);
	}

	inline static int32_t get_offset_of_param3_5() { return static_cast<int32_t>(offsetof(demoStoryCfg_t1993162290, ___param3_5)); }
	inline String_t* get_param3_5() const { return ___param3_5; }
	inline String_t** get_address_of_param3_5() { return &___param3_5; }
	inline void set_param3_5(String_t* value)
	{
		___param3_5 = value;
		Il2CppCodeGenWriteBarrier(&___param3_5, value);
	}

	inline static int32_t get_offset_of_param4_6() { return static_cast<int32_t>(offsetof(demoStoryCfg_t1993162290, ___param4_6)); }
	inline String_t* get_param4_6() const { return ___param4_6; }
	inline String_t** get_address_of_param4_6() { return &___param4_6; }
	inline void set_param4_6(String_t* value)
	{
		___param4_6 = value;
		Il2CppCodeGenWriteBarrier(&___param4_6, value);
	}

	inline static int32_t get_offset_of_param5_7() { return static_cast<int32_t>(offsetof(demoStoryCfg_t1993162290, ___param5_7)); }
	inline String_t* get_param5_7() const { return ___param5_7; }
	inline String_t** get_address_of_param5_7() { return &___param5_7; }
	inline void set_param5_7(String_t* value)
	{
		___param5_7 = value;
		Il2CppCodeGenWriteBarrier(&___param5_7, value);
	}

	inline static int32_t get_offset_of_param6_8() { return static_cast<int32_t>(offsetof(demoStoryCfg_t1993162290, ___param6_8)); }
	inline String_t* get_param6_8() const { return ___param6_8; }
	inline String_t** get_address_of_param6_8() { return &___param6_8; }
	inline void set_param6_8(String_t* value)
	{
		___param6_8 = value;
		Il2CppCodeGenWriteBarrier(&___param6_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
