﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AnimationGenerated
struct UnityEngine_AnimationGenerated_t3757494739;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_AnimationGenerated::.ctor()
extern "C"  void UnityEngine_AnimationGenerated__ctor_m1638344744 (UnityEngine_AnimationGenerated_t3757494739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_Animation1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_Animation1_m4037505864 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationGenerated::Animation_clip(JSVCall)
extern "C"  void UnityEngine_AnimationGenerated_Animation_clip_m3193706752 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationGenerated::Animation_playAutomatically(JSVCall)
extern "C"  void UnityEngine_AnimationGenerated_Animation_playAutomatically_m1708200205 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationGenerated::Animation_wrapMode(JSVCall)
extern "C"  void UnityEngine_AnimationGenerated_Animation_wrapMode_m3669631715 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationGenerated::Animation_isPlaying(JSVCall)
extern "C"  void UnityEngine_AnimationGenerated_Animation_isPlaying_m1244661720 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationGenerated::Animation_Item_String(JSVCall)
extern "C"  void UnityEngine_AnimationGenerated_Animation_Item_String_m46570335 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationGenerated::Animation_animatePhysics(JSVCall)
extern "C"  void UnityEngine_AnimationGenerated_Animation_animatePhysics_m4292140138 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationGenerated::Animation_cullingType(JSVCall)
extern "C"  void UnityEngine_AnimationGenerated_Animation_cullingType_m12438578 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationGenerated::Animation_localBounds(JSVCall)
extern "C"  void UnityEngine_AnimationGenerated_Animation_localBounds_m8239996 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_AddClip__AnimationClip__String__Int32__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_AddClip__AnimationClip__String__Int32__Int32__Boolean_m3095138843 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_AddClip__AnimationClip__String__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_AddClip__AnimationClip__String__Int32__Int32_m2745354223 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_AddClip__AnimationClip__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_AddClip__AnimationClip__String_m2569936943 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_Blend__String__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_Blend__String__Single__Single_m1205114489 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_Blend__String__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_Blend__String__Single_m2614921265 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_Blend__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_Blend__String_m166572521 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_CrossFade__String__Single__PlayMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_CrossFade__String__Single__PlayMode_m950051187 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_CrossFade__String__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_CrossFade__String__Single_m1021050748 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_CrossFade__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_CrossFade__String_m3455231028 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_CrossFadeQueued__String__Single__QueueMode__PlayMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_CrossFadeQueued__String__Single__QueueMode__PlayMode_m4033363358 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_CrossFadeQueued__String__Single__QueueMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_CrossFadeQueued__String__Single__QueueMode_m1617282407 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_CrossFadeQueued__String__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_CrossFadeQueued__String__Single_m328698191 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_CrossFadeQueued__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_CrossFadeQueued__String_m1527951623 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_GetClip__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_GetClip__String_m233209118 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_GetClipCount(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_GetClipCount_m3736870788 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_GetEnumerator(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_GetEnumerator_m1230651457 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_IsPlaying__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_IsPlaying__String_m2999347964 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_Play__String__PlayMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_Play__String__PlayMode_m422784471 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_Play__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_Play__String_m3707500768 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_Play__PlayMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_Play__PlayMode_m2092539846 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_Play(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_Play_m1803303311 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_PlayQueued__String__QueueMode__PlayMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_PlayQueued__String__QueueMode__PlayMode_m4186749114 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_PlayQueued__String__QueueMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_PlayQueued__String__QueueMode_m2050503555 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_PlayQueued__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_PlayQueued__String_m1434244275 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_RemoveClip__AnimationClip(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_RemoveClip__AnimationClip_m2124178311 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_RemoveClip__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_RemoveClip__String_m1081535520 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_Rewind__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_Rewind__String_m2595178919 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_Rewind(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_Rewind_m16007510 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_Sample(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_Sample_m3917765893 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_Stop__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_Stop__String_m965581102 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_Stop(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_Stop_m3202484189 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::Animation_SyncLayer__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_Animation_SyncLayer__Int32_m3949708979 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationGenerated::__Register()
extern "C"  void UnityEngine_AnimationGenerated___Register_m2054499743 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimationGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_AnimationGenerated_ilo_getObject1_m3908521834 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_ilo_attachFinalizerObject2_m1960842232 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimationGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_AnimationGenerated_ilo_setObject3_m290525028 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationGenerated::ilo_setBooleanS4(System.Int32,System.Boolean)
extern "C"  void UnityEngine_AnimationGenerated_ilo_setBooleanS4_m3448057718 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_AnimationGenerated::ilo_getObject5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_AnimationGenerated_ilo_getObject5_m3713511435 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimationGenerated::ilo_getInt326(System.Int32)
extern "C"  int32_t UnityEngine_AnimationGenerated_ilo_getInt326_m1691827252 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationGenerated::ilo_getBooleanS7(System.Int32)
extern "C"  bool UnityEngine_AnimationGenerated_ilo_getBooleanS7_m1030264362 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_AnimationGenerated::ilo_getStringS8(System.Int32)
extern "C"  String_t* UnityEngine_AnimationGenerated_ilo_getStringS8_m3667141613 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_AnimationGenerated::ilo_getSingle9(System.Int32)
extern "C"  float UnityEngine_AnimationGenerated_ilo_getSingle9_m3896215303 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimationGenerated::ilo_getEnum10(System.Int32)
extern "C"  int32_t UnityEngine_AnimationGenerated_ilo_getEnum10_m3567760812 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
