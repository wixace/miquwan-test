﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24204345545MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.String>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3869745328(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t40760731 *, int32_t, String_t*, const MethodInfo*))KeyValuePair_2__ctor_m1933297218_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.String>::get_Key()
#define KeyValuePair_2_get_Key_m2868161720(__this, method) ((  int32_t (*) (KeyValuePair_2_t40760731 *, const MethodInfo*))KeyValuePair_2_get_Key_m3772685030_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.String>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2293369721(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t40760731 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3195876007_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.String>::get_Value()
#define KeyValuePair_2_get_Value_m1376057564(__this, method) ((  String_t* (*) (KeyValuePair_2_t40760731 *, const MethodInfo*))KeyValuePair_2_get_Value_m3039564682_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.String>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1975057145(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t40760731 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Value_m4129348391_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<AnimationRunner/AniType,System.String>::ToString()
#define KeyValuePair_2_ToString_m1052052079(__this, method) ((  String_t* (*) (KeyValuePair_2_t40760731 *, const MethodInfo*))KeyValuePair_2_ToString_m3322470913_gshared)(__this, method)
