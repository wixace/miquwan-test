﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AudioReverbFilter
struct AudioReverbFilter_t2131061462;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AudioReverbPreset2425455165.h"

// System.Void UnityEngine.AudioReverbFilter::.ctor()
extern "C"  void AudioReverbFilter__ctor_m1557262329 (AudioReverbFilter_t2131061462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioReverbPreset UnityEngine.AudioReverbFilter::get_reverbPreset()
extern "C"  int32_t AudioReverbFilter_get_reverbPreset_m1789457762 (AudioReverbFilter_t2131061462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbFilter::set_reverbPreset(UnityEngine.AudioReverbPreset)
extern "C"  void AudioReverbFilter_set_reverbPreset_m4252058313 (AudioReverbFilter_t2131061462 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbFilter::get_dryLevel()
extern "C"  float AudioReverbFilter_get_dryLevel_m2431354721 (AudioReverbFilter_t2131061462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbFilter::set_dryLevel(System.Single)
extern "C"  void AudioReverbFilter_set_dryLevel_m3435021546 (AudioReverbFilter_t2131061462 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbFilter::get_room()
extern "C"  float AudioReverbFilter_get_room_m308814499 (AudioReverbFilter_t2131061462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbFilter::set_room(System.Single)
extern "C"  void AudioReverbFilter_set_room_m3503162600 (AudioReverbFilter_t2131061462 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbFilter::get_roomHF()
extern "C"  float AudioReverbFilter_get_roomHF_m418972577 (AudioReverbFilter_t2131061462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbFilter::set_roomHF(System.Single)
extern "C"  void AudioReverbFilter_set_roomHF_m4222218410 (AudioReverbFilter_t2131061462 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbFilter::get_roomRolloff()
extern "C"  float AudioReverbFilter_get_roomRolloff_m3211927633 (AudioReverbFilter_t2131061462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbFilter::set_roomRolloff(System.Single)
extern "C"  void AudioReverbFilter_set_roomRolloff_m2978842618 (AudioReverbFilter_t2131061462 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbFilter::get_decayTime()
extern "C"  float AudioReverbFilter_get_decayTime_m3566427585 (AudioReverbFilter_t2131061462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbFilter::set_decayTime(System.Single)
extern "C"  void AudioReverbFilter_set_decayTime_m1465540746 (AudioReverbFilter_t2131061462 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbFilter::get_decayHFRatio()
extern "C"  float AudioReverbFilter_get_decayHFRatio_m3948871035 (AudioReverbFilter_t2131061462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbFilter::set_decayHFRatio(System.Single)
extern "C"  void AudioReverbFilter_set_decayHFRatio_m2719385872 (AudioReverbFilter_t2131061462 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbFilter::get_reflectionsLevel()
extern "C"  float AudioReverbFilter_get_reflectionsLevel_m2830136772 (AudioReverbFilter_t2131061462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbFilter::set_reflectionsLevel(System.Single)
extern "C"  void AudioReverbFilter_set_reflectionsLevel_m2849771367 (AudioReverbFilter_t2131061462 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbFilter::get_reflectionsDelay()
extern "C"  float AudioReverbFilter_get_reflectionsDelay_m15732739 (AudioReverbFilter_t2131061462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbFilter::set_reflectionsDelay(System.Single)
extern "C"  void AudioReverbFilter_set_reflectionsDelay_m4040905608 (AudioReverbFilter_t2131061462 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbFilter::get_reverbLevel()
extern "C"  float AudioReverbFilter_get_reverbLevel_m2084474860 (AudioReverbFilter_t2131061462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbFilter::set_reverbLevel(System.Single)
extern "C"  void AudioReverbFilter_set_reverbLevel_m200707135 (AudioReverbFilter_t2131061462 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbFilter::get_reverbDelay()
extern "C"  float AudioReverbFilter_get_reverbDelay_m3565038123 (AudioReverbFilter_t2131061462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbFilter::set_reverbDelay(System.Single)
extern "C"  void AudioReverbFilter_set_reverbDelay_m1391841376 (AudioReverbFilter_t2131061462 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbFilter::get_diffusion()
extern "C"  float AudioReverbFilter_get_diffusion_m3409619039 (AudioReverbFilter_t2131061462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbFilter::set_diffusion(System.Single)
extern "C"  void AudioReverbFilter_set_diffusion_m725468844 (AudioReverbFilter_t2131061462 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbFilter::get_density()
extern "C"  float AudioReverbFilter_get_density_m3000735490 (AudioReverbFilter_t2131061462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbFilter::set_density(System.Single)
extern "C"  void AudioReverbFilter_set_density_m2888939753 (AudioReverbFilter_t2131061462 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbFilter::get_hfReference()
extern "C"  float AudioReverbFilter_get_hfReference_m1624387655 (AudioReverbFilter_t2131061462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbFilter::set_hfReference(System.Single)
extern "C"  void AudioReverbFilter_set_hfReference_m1444779972 (AudioReverbFilter_t2131061462 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbFilter::get_roomLF()
extern "C"  float AudioReverbFilter_get_roomLF_m419091741 (AudioReverbFilter_t2131061462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbFilter::set_roomLF(System.Single)
extern "C"  void AudioReverbFilter_set_roomLF_m1045522606 (AudioReverbFilter_t2131061462 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbFilter::get_lfReference()
extern "C"  float AudioReverbFilter_get_lfReference_m450775627 (AudioReverbFilter_t2131061462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbFilter::set_lfReference(System.Single)
extern "C"  void AudioReverbFilter_set_lfReference_m1568859200 (AudioReverbFilter_t2131061462 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
