﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_FileInfoBase3368634971.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FileInfoRes
struct  FileInfoRes_t1217059798  : public FileInfoBase_t3368634971
{
public:
	// System.String FileInfoRes::mDataMD5
	String_t* ___mDataMD5_1;
	// System.String FileInfoRes::mPath
	String_t* ___mPath_2;
	// System.UInt32 FileInfoRes::mSize
	uint32_t ___mSize_3;
	// System.Boolean FileInfoRes::isCompress
	bool ___isCompress_4;
	// System.Int32 FileInfoRes::loadType
	int32_t ___loadType_5;
	// System.Boolean FileInfoRes::isWrite
	bool ___isWrite_6;

public:
	inline static int32_t get_offset_of_mDataMD5_1() { return static_cast<int32_t>(offsetof(FileInfoRes_t1217059798, ___mDataMD5_1)); }
	inline String_t* get_mDataMD5_1() const { return ___mDataMD5_1; }
	inline String_t** get_address_of_mDataMD5_1() { return &___mDataMD5_1; }
	inline void set_mDataMD5_1(String_t* value)
	{
		___mDataMD5_1 = value;
		Il2CppCodeGenWriteBarrier(&___mDataMD5_1, value);
	}

	inline static int32_t get_offset_of_mPath_2() { return static_cast<int32_t>(offsetof(FileInfoRes_t1217059798, ___mPath_2)); }
	inline String_t* get_mPath_2() const { return ___mPath_2; }
	inline String_t** get_address_of_mPath_2() { return &___mPath_2; }
	inline void set_mPath_2(String_t* value)
	{
		___mPath_2 = value;
		Il2CppCodeGenWriteBarrier(&___mPath_2, value);
	}

	inline static int32_t get_offset_of_mSize_3() { return static_cast<int32_t>(offsetof(FileInfoRes_t1217059798, ___mSize_3)); }
	inline uint32_t get_mSize_3() const { return ___mSize_3; }
	inline uint32_t* get_address_of_mSize_3() { return &___mSize_3; }
	inline void set_mSize_3(uint32_t value)
	{
		___mSize_3 = value;
	}

	inline static int32_t get_offset_of_isCompress_4() { return static_cast<int32_t>(offsetof(FileInfoRes_t1217059798, ___isCompress_4)); }
	inline bool get_isCompress_4() const { return ___isCompress_4; }
	inline bool* get_address_of_isCompress_4() { return &___isCompress_4; }
	inline void set_isCompress_4(bool value)
	{
		___isCompress_4 = value;
	}

	inline static int32_t get_offset_of_loadType_5() { return static_cast<int32_t>(offsetof(FileInfoRes_t1217059798, ___loadType_5)); }
	inline int32_t get_loadType_5() const { return ___loadType_5; }
	inline int32_t* get_address_of_loadType_5() { return &___loadType_5; }
	inline void set_loadType_5(int32_t value)
	{
		___loadType_5 = value;
	}

	inline static int32_t get_offset_of_isWrite_6() { return static_cast<int32_t>(offsetof(FileInfoRes_t1217059798, ___isWrite_6)); }
	inline bool get_isWrite_6() const { return ___isWrite_6; }
	inline bool* get_address_of_isWrite_6() { return &___isWrite_6; }
	inline void set_isWrite_6(bool value)
	{
		___isWrite_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
