﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._7587570eff0310a99e5da816815b3d66
struct _7587570eff0310a99e5da816815b3d66_t2699329461;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._7587570eff0310a99e5da816815b3d66::.ctor()
extern "C"  void _7587570eff0310a99e5da816815b3d66__ctor_m40534552 (_7587570eff0310a99e5da816815b3d66_t2699329461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7587570eff0310a99e5da816815b3d66::_7587570eff0310a99e5da816815b3d66m2(System.Int32)
extern "C"  int32_t _7587570eff0310a99e5da816815b3d66__7587570eff0310a99e5da816815b3d66m2_m167103865 (_7587570eff0310a99e5da816815b3d66_t2699329461 * __this, int32_t ____7587570eff0310a99e5da816815b3d66a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7587570eff0310a99e5da816815b3d66::_7587570eff0310a99e5da816815b3d66m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _7587570eff0310a99e5da816815b3d66__7587570eff0310a99e5da816815b3d66m_m3471832413 (_7587570eff0310a99e5da816815b3d66_t2699329461 * __this, int32_t ____7587570eff0310a99e5da816815b3d66a0, int32_t ____7587570eff0310a99e5da816815b3d66961, int32_t ____7587570eff0310a99e5da816815b3d66c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
