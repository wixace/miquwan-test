﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Interpolate/<NewCounter>c__Iterator22
struct U3CNewCounterU3Ec__Iterator22_t1689671549;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t1908816725;

#include "codegen/il2cpp-codegen.h"

// System.Void Interpolate/<NewCounter>c__Iterator22::.ctor()
extern "C"  void U3CNewCounterU3Ec__Iterator22__ctor_m4224708238 (U3CNewCounterU3Ec__Iterator22_t1689671549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate/<NewCounter>c__Iterator22::System.Collections.Generic.IEnumerator<float>.get_Current()
extern "C"  float U3CNewCounterU3Ec__Iterator22_System_Collections_Generic_IEnumeratorU3CfloatU3E_get_Current_m4134624090 (U3CNewCounterU3Ec__Iterator22_t1689671549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Interpolate/<NewCounter>c__Iterator22::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CNewCounterU3Ec__Iterator22_System_Collections_IEnumerator_get_Current_m2990444760 (U3CNewCounterU3Ec__Iterator22_t1689671549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Interpolate/<NewCounter>c__Iterator22::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CNewCounterU3Ec__Iterator22_System_Collections_IEnumerable_GetEnumerator_m2458062611 (U3CNewCounterU3Ec__Iterator22_t1689671549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Single> Interpolate/<NewCounter>c__Iterator22::System.Collections.Generic.IEnumerable<float>.GetEnumerator()
extern "C"  Il2CppObject* U3CNewCounterU3Ec__Iterator22_System_Collections_Generic_IEnumerableU3CfloatU3E_GetEnumerator_m2163165155 (U3CNewCounterU3Ec__Iterator22_t1689671549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Interpolate/<NewCounter>c__Iterator22::MoveNext()
extern "C"  bool U3CNewCounterU3Ec__Iterator22_MoveNext_m190526374 (U3CNewCounterU3Ec__Iterator22_t1689671549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Interpolate/<NewCounter>c__Iterator22::Dispose()
extern "C"  void U3CNewCounterU3Ec__Iterator22_Dispose_m1098805067 (U3CNewCounterU3Ec__Iterator22_t1689671549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Interpolate/<NewCounter>c__Iterator22::Reset()
extern "C"  void U3CNewCounterU3Ec__Iterator22_Reset_m1871141179 (U3CNewCounterU3Ec__Iterator22_t1689671549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
