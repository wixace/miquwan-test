﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ObjectGenerated
struct UnityEngine_ObjectGenerated_t1343506088;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type
struct Type_t;
// MethodID
struct MethodID_t3916401116;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_MethodID3916401116.h"

// System.Void UnityEngine_ObjectGenerated::.ctor()
extern "C"  void UnityEngine_ObjectGenerated__ctor_m1820680643 (UnityEngine_ObjectGenerated_t1343506088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ObjectGenerated::.cctor()
extern "C"  void UnityEngine_ObjectGenerated__cctor_m124428874 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_Object1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_Object1_m2481090863 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ObjectGenerated::Object_name(JSVCall)
extern "C"  void UnityEngine_ObjectGenerated_Object_name_m1496319099 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ObjectGenerated::Object_hideFlags(JSVCall)
extern "C"  void UnityEngine_ObjectGenerated_Object_hideFlags_m519144353 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_Equals__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_Equals__Object_m431280195 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_GetHashCode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_GetHashCode_m3216090798 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_GetInstanceID(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_GetInstanceID_m3122184387 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_ToString_m3793375025 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_Destroy__UEObject__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_Destroy__UEObject__Single_m2989649070 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_Destroy__UEObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_Destroy__UEObject_m1444569958 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_DestroyImmediate__UEObject__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_DestroyImmediate__UEObject__Boolean_m3433288543 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_DestroyImmediate__UEObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_DestroyImmediate__UEObject_m1186187819 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_DestroyObject__UEObject__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_DestroyObject__UEObject__Single_m3598419085 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_DestroyObject__UEObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_DestroyObject__UEObject_m2693581381 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_DontDestroyOnLoad__UEObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_DontDestroyOnLoad__UEObject_m3571690586 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_FindObjectOfType__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_FindObjectOfType__Type_m1986947368 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_FindObjectOfTypeT1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_FindObjectOfTypeT1_m2506210955 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_FindObjectsOfType__Type(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_FindObjectsOfType__Type_m4150192707 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_FindObjectsOfTypeT1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_FindObjectsOfTypeT1_m1759315238 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_Instantiate__UEObject__Vector3__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_Instantiate__UEObject__Vector3__Quaternion_m606360282 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_Instantiate__UEObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_Instantiate__UEObject_m951238742 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_InstantiateT1__T(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_InstantiateT1__T_m2388928082 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_op_Equality__UEObject__UEObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_op_Equality__UEObject__UEObject_m2862762003 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_op_Implicit__UEObject_to_Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_op_Implicit__UEObject_to_Boolean_m4068910806 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::Object_op_Inequality__UEObject__UEObject(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_Object_op_Inequality__UEObject__UEObject_m1779761016 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ObjectGenerated::__Register()
extern "C"  void UnityEngine_ObjectGenerated___Register_m3608263908 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ObjectGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_ObjectGenerated_ilo_attachFinalizerObject1_m3951146644 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ObjectGenerated::ilo_setBooleanS2(System.Int32,System.Boolean)
extern "C"  void UnityEngine_ObjectGenerated_ilo_setBooleanS2_m2871854483 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_ObjectGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_ObjectGenerated_ilo_getSingle3_m4250680494 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_ObjectGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_ObjectGenerated_ilo_getObject4_m2110346821 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ObjectGenerated::ilo_setObject5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_ObjectGenerated_ilo_setObject5_m1685476815 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine_ObjectGenerated::ilo_makeGenericMethod6(System.Type,MethodID,System.Int32)
extern "C"  MethodInfo_t * UnityEngine_ObjectGenerated_ilo_makeGenericMethod6_m19341168 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MethodID_t3916401116 * ___methodID1, int32_t ___TCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ObjectGenerated::ilo_moveSaveID2Arr7(System.Int32)
extern "C"  void UnityEngine_ObjectGenerated_ilo_moveSaveID2Arr7_m3702902686 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
