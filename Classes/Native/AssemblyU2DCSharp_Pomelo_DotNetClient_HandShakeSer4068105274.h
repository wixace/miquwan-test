﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Pomelo.DotNetClient.Protocol
struct Protocol_t400511732;
// System.Action`1<Newtonsoft.Json.Linq.JObject>
struct Action_1_t2194360335;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pomelo.DotNetClient.HandShakeService
struct  HandShakeService_t4068105274  : public Il2CppObject
{
public:
	// Pomelo.DotNetClient.Protocol Pomelo.DotNetClient.HandShakeService::protocol
	Protocol_t400511732 * ___protocol_2;
	// System.Action`1<Newtonsoft.Json.Linq.JObject> Pomelo.DotNetClient.HandShakeService::callback
	Action_1_t2194360335 * ___callback_3;

public:
	inline static int32_t get_offset_of_protocol_2() { return static_cast<int32_t>(offsetof(HandShakeService_t4068105274, ___protocol_2)); }
	inline Protocol_t400511732 * get_protocol_2() const { return ___protocol_2; }
	inline Protocol_t400511732 ** get_address_of_protocol_2() { return &___protocol_2; }
	inline void set_protocol_2(Protocol_t400511732 * value)
	{
		___protocol_2 = value;
		Il2CppCodeGenWriteBarrier(&___protocol_2, value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(HandShakeService_t4068105274, ___callback_3)); }
	inline Action_1_t2194360335 * get_callback_3() const { return ___callback_3; }
	inline Action_1_t2194360335 ** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(Action_1_t2194360335 * value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier(&___callback_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
