﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AnimatorControllerParameter
struct AnimatorControllerParameter_t3339705788;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.String UnityEngine.AnimatorControllerParameter::get_name()
extern "C"  String_t* AnimatorControllerParameter_get_name_m227538242 (AnimatorControllerParameter_t3339705788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnimatorControllerParameter::Equals(System.Object)
extern "C"  bool AnimatorControllerParameter_Equals_m3644357162 (AnimatorControllerParameter_t3339705788 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AnimatorControllerParameter::GetHashCode()
extern "C"  int32_t AnimatorControllerParameter_GetHashCode_m477573122 (AnimatorControllerParameter_t3339705788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
