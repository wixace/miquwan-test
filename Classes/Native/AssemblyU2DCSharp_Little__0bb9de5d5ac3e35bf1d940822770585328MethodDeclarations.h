﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._0bb9de5d5ac3e35bf1d940823e7dac4d
struct _0bb9de5d5ac3e35bf1d940823e7dac4d_t2770585328;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__0bb9de5d5ac3e35bf1d940822770585328.h"

// System.Void Little._0bb9de5d5ac3e35bf1d940823e7dac4d::.ctor()
extern "C"  void _0bb9de5d5ac3e35bf1d940823e7dac4d__ctor_m5778749 (_0bb9de5d5ac3e35bf1d940823e7dac4d_t2770585328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._0bb9de5d5ac3e35bf1d940823e7dac4d::_0bb9de5d5ac3e35bf1d940823e7dac4dm2(System.Int32)
extern "C"  int32_t _0bb9de5d5ac3e35bf1d940823e7dac4d__0bb9de5d5ac3e35bf1d940823e7dac4dm2_m2164927385 (_0bb9de5d5ac3e35bf1d940823e7dac4d_t2770585328 * __this, int32_t ____0bb9de5d5ac3e35bf1d940823e7dac4da0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._0bb9de5d5ac3e35bf1d940823e7dac4d::_0bb9de5d5ac3e35bf1d940823e7dac4dm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _0bb9de5d5ac3e35bf1d940823e7dac4d__0bb9de5d5ac3e35bf1d940823e7dac4dm_m3188439869 (_0bb9de5d5ac3e35bf1d940823e7dac4d_t2770585328 * __this, int32_t ____0bb9de5d5ac3e35bf1d940823e7dac4da0, int32_t ____0bb9de5d5ac3e35bf1d940823e7dac4d921, int32_t ____0bb9de5d5ac3e35bf1d940823e7dac4dc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._0bb9de5d5ac3e35bf1d940823e7dac4d::ilo__0bb9de5d5ac3e35bf1d940823e7dac4dm21(Little._0bb9de5d5ac3e35bf1d940823e7dac4d,System.Int32)
extern "C"  int32_t _0bb9de5d5ac3e35bf1d940823e7dac4d_ilo__0bb9de5d5ac3e35bf1d940823e7dac4dm21_m1322392247 (Il2CppObject * __this /* static, unused */, _0bb9de5d5ac3e35bf1d940823e7dac4d_t2770585328 * ____this0, int32_t ____0bb9de5d5ac3e35bf1d940823e7dac4da1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
