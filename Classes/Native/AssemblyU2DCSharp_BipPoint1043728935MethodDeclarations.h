﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BipPoint
struct BipPoint_t1043728935;

#include "codegen/il2cpp-codegen.h"

// System.Void BipPoint::.ctor()
extern "C"  void BipPoint__ctor_m2091266132 (BipPoint_t1043728935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BipPoint::IsShow()
extern "C"  bool BipPoint_IsShow_m2259660267 (BipPoint_t1043728935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BipPoint::Show(System.Boolean)
extern "C"  void BipPoint_Show_m2624460004 (BipPoint_t1043728935 * __this, bool ___isShow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
