﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._d6ae9611fd25390a52430f92ed2d41af
struct _d6ae9611fd25390a52430f92ed2d41af_t3788711490;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__d6ae9611fd25390a52430f923788711490.h"

// System.Void Little._d6ae9611fd25390a52430f92ed2d41af::.ctor()
extern "C"  void _d6ae9611fd25390a52430f92ed2d41af__ctor_m420080171 (_d6ae9611fd25390a52430f92ed2d41af_t3788711490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._d6ae9611fd25390a52430f92ed2d41af::_d6ae9611fd25390a52430f92ed2d41afm2(System.Int32)
extern "C"  int32_t _d6ae9611fd25390a52430f92ed2d41af__d6ae9611fd25390a52430f92ed2d41afm2_m991976025 (_d6ae9611fd25390a52430f92ed2d41af_t3788711490 * __this, int32_t ____d6ae9611fd25390a52430f92ed2d41afa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._d6ae9611fd25390a52430f92ed2d41af::_d6ae9611fd25390a52430f92ed2d41afm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _d6ae9611fd25390a52430f92ed2d41af__d6ae9611fd25390a52430f92ed2d41afm_m3634603133 (_d6ae9611fd25390a52430f92ed2d41af_t3788711490 * __this, int32_t ____d6ae9611fd25390a52430f92ed2d41afa0, int32_t ____d6ae9611fd25390a52430f92ed2d41af571, int32_t ____d6ae9611fd25390a52430f92ed2d41afc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._d6ae9611fd25390a52430f92ed2d41af::ilo__d6ae9611fd25390a52430f92ed2d41afm21(Little._d6ae9611fd25390a52430f92ed2d41af,System.Int32)
extern "C"  int32_t _d6ae9611fd25390a52430f92ed2d41af_ilo__d6ae9611fd25390a52430f92ed2d41afm21_m246700617 (Il2CppObject * __this /* static, unused */, _d6ae9611fd25390a52430f92ed2d41af_t3788711490 * ____this0, int32_t ____d6ae9611fd25390a52430f92ed2d41afa1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
