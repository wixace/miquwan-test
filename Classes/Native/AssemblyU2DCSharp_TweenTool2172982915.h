﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenPosition[]
struct TweenPositionU5BU5D_t2332880541;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenTool
struct  TweenTool_t2172982915  : public MonoBehaviour_t667441552
{
public:
	// TweenPosition[] TweenTool::animationTweens
	TweenPositionU5BU5D_t2332880541* ___animationTweens_2;
	// UnityEngine.GameObject TweenTool::targetObj
	GameObject_t3674682005 * ___targetObj_3;
	// UnityEngine.GameObject TweenTool::otherCameraObj
	GameObject_t3674682005 * ___otherCameraObj_4;
	// System.Boolean TweenTool::isStart
	bool ___isStart_5;

public:
	inline static int32_t get_offset_of_animationTweens_2() { return static_cast<int32_t>(offsetof(TweenTool_t2172982915, ___animationTweens_2)); }
	inline TweenPositionU5BU5D_t2332880541* get_animationTweens_2() const { return ___animationTweens_2; }
	inline TweenPositionU5BU5D_t2332880541** get_address_of_animationTweens_2() { return &___animationTweens_2; }
	inline void set_animationTweens_2(TweenPositionU5BU5D_t2332880541* value)
	{
		___animationTweens_2 = value;
		Il2CppCodeGenWriteBarrier(&___animationTweens_2, value);
	}

	inline static int32_t get_offset_of_targetObj_3() { return static_cast<int32_t>(offsetof(TweenTool_t2172982915, ___targetObj_3)); }
	inline GameObject_t3674682005 * get_targetObj_3() const { return ___targetObj_3; }
	inline GameObject_t3674682005 ** get_address_of_targetObj_3() { return &___targetObj_3; }
	inline void set_targetObj_3(GameObject_t3674682005 * value)
	{
		___targetObj_3 = value;
		Il2CppCodeGenWriteBarrier(&___targetObj_3, value);
	}

	inline static int32_t get_offset_of_otherCameraObj_4() { return static_cast<int32_t>(offsetof(TweenTool_t2172982915, ___otherCameraObj_4)); }
	inline GameObject_t3674682005 * get_otherCameraObj_4() const { return ___otherCameraObj_4; }
	inline GameObject_t3674682005 ** get_address_of_otherCameraObj_4() { return &___otherCameraObj_4; }
	inline void set_otherCameraObj_4(GameObject_t3674682005 * value)
	{
		___otherCameraObj_4 = value;
		Il2CppCodeGenWriteBarrier(&___otherCameraObj_4, value);
	}

	inline static int32_t get_offset_of_isStart_5() { return static_cast<int32_t>(offsetof(TweenTool_t2172982915, ___isStart_5)); }
	inline bool get_isStart_5() const { return ___isStart_5; }
	inline bool* get_address_of_isStart_5() { return &___isStart_5; }
	inline void set_isStart_5(bool value)
	{
		___isStart_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
