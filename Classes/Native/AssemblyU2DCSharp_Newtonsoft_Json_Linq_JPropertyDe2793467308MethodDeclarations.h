﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JPropertyDescriptor
struct JPropertyDescriptor_t2793467308;
// System.String
struct String_t;
// System.Type
struct Type_t;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"

// System.Void Newtonsoft.Json.Linq.JPropertyDescriptor::.ctor(System.String,System.Type)
extern "C"  void JPropertyDescriptor__ctor_m1055826031 (JPropertyDescriptor_t2793467308 * __this, String_t* ___name0, Type_t * ___propertyType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JPropertyDescriptor::CastInstance(System.Object)
extern "C"  JObject_t1798544199 * JPropertyDescriptor_CastInstance_m2534040853 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JPropertyDescriptor::CanResetValue(System.Object)
extern "C"  bool JPropertyDescriptor_CanResetValue_m770216082 (JPropertyDescriptor_t2793467308 * __this, Il2CppObject * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JPropertyDescriptor::GetValue(System.Object)
extern "C"  Il2CppObject * JPropertyDescriptor_GetValue_m4223826598 (JPropertyDescriptor_t2793467308 * __this, Il2CppObject * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JPropertyDescriptor::ResetValue(System.Object)
extern "C"  void JPropertyDescriptor_ResetValue_m928125812 (JPropertyDescriptor_t2793467308 * __this, Il2CppObject * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JPropertyDescriptor::SetValue(System.Object,System.Object)
extern "C"  void JPropertyDescriptor_SetValue_m4218382933 (JPropertyDescriptor_t2793467308 * __this, Il2CppObject * ___component0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JPropertyDescriptor::ShouldSerializeValue(System.Object)
extern "C"  bool JPropertyDescriptor_ShouldSerializeValue_m3731528606 (JPropertyDescriptor_t2793467308 * __this, Il2CppObject * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Linq.JPropertyDescriptor::get_ComponentType()
extern "C"  Type_t * JPropertyDescriptor_get_ComponentType_m1366109816 (JPropertyDescriptor_t2793467308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JPropertyDescriptor::get_IsReadOnly()
extern "C"  bool JPropertyDescriptor_get_IsReadOnly_m1571133799 (JPropertyDescriptor_t2793467308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Linq.JPropertyDescriptor::get_PropertyType()
extern "C"  Type_t * JPropertyDescriptor_get_PropertyType_m855600784 (JPropertyDescriptor_t2793467308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JPropertyDescriptor::get_NameHashCode()
extern "C"  int32_t JPropertyDescriptor_get_NameHashCode_m394857179 (JPropertyDescriptor_t2793467308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JPropertyDescriptor::ilo_set_Item1(Newtonsoft.Json.Linq.JObject,System.String,Newtonsoft.Json.Linq.JToken)
extern "C"  void JPropertyDescriptor_ilo_set_Item1_m2418442882 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, JToken_t3412245951 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
