﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_vakeka17
struct M_vakeka17_t1158370833;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_vakeka171158370833.h"

// System.Void GarbageiOS.M_vakeka17::.ctor()
extern "C"  void M_vakeka17__ctor_m4112896834 (M_vakeka17_t1158370833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vakeka17::M_keseTraiwhease0(System.String[],System.Int32)
extern "C"  void M_vakeka17_M_keseTraiwhease0_m2242920356 (M_vakeka17_t1158370833 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vakeka17::M_sagisgow1(System.String[],System.Int32)
extern "C"  void M_vakeka17_M_sagisgow1_m1645838986 (M_vakeka17_t1158370833 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vakeka17::M_hamalgeBodro2(System.String[],System.Int32)
extern "C"  void M_vakeka17_M_hamalgeBodro2_m1041228928 (M_vakeka17_t1158370833 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vakeka17::M_zairwelMoodree3(System.String[],System.Int32)
extern "C"  void M_vakeka17_M_zairwelMoodree3_m1443809419 (M_vakeka17_t1158370833 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vakeka17::M_nofabee4(System.String[],System.Int32)
extern "C"  void M_vakeka17_M_nofabee4_m601719033 (M_vakeka17_t1158370833 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vakeka17::M_rairrawni5(System.String[],System.Int32)
extern "C"  void M_vakeka17_M_rairrawni5_m2537366869 (M_vakeka17_t1158370833 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vakeka17::M_learmijal6(System.String[],System.Int32)
extern "C"  void M_vakeka17_M_learmijal6_m3328860818 (M_vakeka17_t1158370833 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vakeka17::ilo_M_sagisgow11(GarbageiOS.M_vakeka17,System.String[],System.Int32)
extern "C"  void M_vakeka17_ilo_M_sagisgow11_m759955943 (Il2CppObject * __this /* static, unused */, M_vakeka17_t1158370833 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vakeka17::ilo_M_zairwelMoodree32(GarbageiOS.M_vakeka17,System.String[],System.Int32)
extern "C"  void M_vakeka17_ilo_M_zairwelMoodree32_m1183443463 (Il2CppObject * __this /* static, unused */, M_vakeka17_t1158370833 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_vakeka17::ilo_M_nofabee43(GarbageiOS.M_vakeka17,System.String[],System.Int32)
extern "C"  void M_vakeka17_ilo_M_nofabee43_m583613396 (Il2CppObject * __this /* static, unused */, M_vakeka17_t1158370833 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
