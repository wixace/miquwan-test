﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr_DGetV_1_gen4048467712MethodDeclarations.h"

// System.Void JSDataExchangeMgr/DGetV`1<SpringPanel/OnFinished>::.ctor(System.Object,System.IntPtr)
#define DGetV_1__ctor_m3999404664(__this, ___object0, ___method1, method) ((  void (*) (DGetV_1_t3194040406 *, Il2CppObject *, IntPtr_t, const MethodInfo*))DGetV_1__ctor_m3356914363_gshared)(__this, ___object0, ___method1, method)
// T JSDataExchangeMgr/DGetV`1<SpringPanel/OnFinished>::Invoke()
#define DGetV_1_Invoke_m2621095411(__this, method) ((  OnFinished_t3316389065 * (*) (DGetV_1_t3194040406 *, const MethodInfo*))DGetV_1_Invoke_m1509146772_gshared)(__this, method)
// System.IAsyncResult JSDataExchangeMgr/DGetV`1<SpringPanel/OnFinished>::BeginInvoke(System.AsyncCallback,System.Object)
#define DGetV_1_BeginInvoke_m2677734777(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (DGetV_1_t3194040406 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))DGetV_1_BeginInvoke_m2681184398_gshared)(__this, ___callback0, ___object1, method)
// T JSDataExchangeMgr/DGetV`1<SpringPanel/OnFinished>::EndInvoke(System.IAsyncResult)
#define DGetV_1_EndInvoke_m2693225193(__this, ___result0, method) ((  OnFinished_t3316389065 * (*) (DGetV_1_t3194040406 *, Il2CppObject *, const MethodInfo*))DGetV_1_EndInvoke_m1776565962_gshared)(__this, ___result0, method)
