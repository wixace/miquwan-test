﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_noqall28
struct  M_noqall28_t4104028199  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_noqall28::_leretayTallnearjor
	uint32_t ____leretayTallnearjor_0;
	// System.Boolean GarbageiOS.M_noqall28::_ferejoDeway
	bool ____ferejoDeway_1;
	// System.Boolean GarbageiOS.M_noqall28::_patetowNetrere
	bool ____patetowNetrere_2;
	// System.String GarbageiOS.M_noqall28::_seltru
	String_t* ____seltru_3;
	// System.String GarbageiOS.M_noqall28::_hallriswhow
	String_t* ____hallriswhow_4;
	// System.String GarbageiOS.M_noqall28::_sigor
	String_t* ____sigor_5;
	// System.Int32 GarbageiOS.M_noqall28::_seredouMowchearlas
	int32_t ____seredouMowchearlas_6;
	// System.Boolean GarbageiOS.M_noqall28::_rerene
	bool ____rerene_7;
	// System.Int32 GarbageiOS.M_noqall28::_pesel
	int32_t ____pesel_8;
	// System.Boolean GarbageiOS.M_noqall28::_stemha
	bool ____stemha_9;
	// System.Boolean GarbageiOS.M_noqall28::_ladis
	bool ____ladis_10;
	// System.UInt32 GarbageiOS.M_noqall28::_mirjusas
	uint32_t ____mirjusas_11;
	// System.Single GarbageiOS.M_noqall28::_jinemti
	float ____jinemti_12;
	// System.Single GarbageiOS.M_noqall28::_birde
	float ____birde_13;
	// System.String GarbageiOS.M_noqall28::_palldradarZeanidem
	String_t* ____palldradarZeanidem_14;
	// System.Boolean GarbageiOS.M_noqall28::_sowpofel
	bool ____sowpofel_15;
	// System.Boolean GarbageiOS.M_noqall28::_borxooneSismihaw
	bool ____borxooneSismihaw_16;
	// System.String GarbageiOS.M_noqall28::_wijee
	String_t* ____wijee_17;
	// System.Int32 GarbageiOS.M_noqall28::_lezerarWelsallra
	int32_t ____lezerarWelsallra_18;
	// System.Int32 GarbageiOS.M_noqall28::_nerrouNorsel
	int32_t ____nerrouNorsel_19;
	// System.UInt32 GarbageiOS.M_noqall28::_jacair
	uint32_t ____jacair_20;
	// System.Boolean GarbageiOS.M_noqall28::_demousouBeboras
	bool ____demousouBeboras_21;
	// System.String GarbageiOS.M_noqall28::_coogerelairPaipou
	String_t* ____coogerelairPaipou_22;
	// System.Int32 GarbageiOS.M_noqall28::_cairstaw
	int32_t ____cairstaw_23;
	// System.String GarbageiOS.M_noqall28::_feris
	String_t* ____feris_24;
	// System.UInt32 GarbageiOS.M_noqall28::_rirma
	uint32_t ____rirma_25;
	// System.Boolean GarbageiOS.M_noqall28::_wassoutoRacem
	bool ____wassoutoRacem_26;
	// System.Single GarbageiOS.M_noqall28::_soucaltru
	float ____soucaltru_27;
	// System.UInt32 GarbageiOS.M_noqall28::_neyisgou
	uint32_t ____neyisgou_28;
	// System.UInt32 GarbageiOS.M_noqall28::_qeeraylouFereral
	uint32_t ____qeeraylouFereral_29;
	// System.Int32 GarbageiOS.M_noqall28::_heremallTacarsoo
	int32_t ____heremallTacarsoo_30;
	// System.Int32 GarbageiOS.M_noqall28::_dotu
	int32_t ____dotu_31;
	// System.String GarbageiOS.M_noqall28::_pechor
	String_t* ____pechor_32;
	// System.Int32 GarbageiOS.M_noqall28::_woutousis
	int32_t ____woutousis_33;
	// System.String GarbageiOS.M_noqall28::_xeradrouHopo
	String_t* ____xeradrouHopo_34;
	// System.Boolean GarbageiOS.M_noqall28::_mouster
	bool ____mouster_35;
	// System.Single GarbageiOS.M_noqall28::_wikerea
	float ____wikerea_36;
	// System.Boolean GarbageiOS.M_noqall28::_felmuDife
	bool ____felmuDife_37;
	// System.Boolean GarbageiOS.M_noqall28::_purlea
	bool ____purlea_38;
	// System.UInt32 GarbageiOS.M_noqall28::_kursalhere
	uint32_t ____kursalhere_39;
	// System.String GarbageiOS.M_noqall28::_fairputiQawvir
	String_t* ____fairputiQawvir_40;
	// System.UInt32 GarbageiOS.M_noqall28::_gigoujeBelsallkair
	uint32_t ____gigoujeBelsallkair_41;
	// System.Int32 GarbageiOS.M_noqall28::_dati
	int32_t ____dati_42;
	// System.UInt32 GarbageiOS.M_noqall28::_pimou
	uint32_t ____pimou_43;
	// System.Boolean GarbageiOS.M_noqall28::_wecaci
	bool ____wecaci_44;

public:
	inline static int32_t get_offset_of__leretayTallnearjor_0() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____leretayTallnearjor_0)); }
	inline uint32_t get__leretayTallnearjor_0() const { return ____leretayTallnearjor_0; }
	inline uint32_t* get_address_of__leretayTallnearjor_0() { return &____leretayTallnearjor_0; }
	inline void set__leretayTallnearjor_0(uint32_t value)
	{
		____leretayTallnearjor_0 = value;
	}

	inline static int32_t get_offset_of__ferejoDeway_1() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____ferejoDeway_1)); }
	inline bool get__ferejoDeway_1() const { return ____ferejoDeway_1; }
	inline bool* get_address_of__ferejoDeway_1() { return &____ferejoDeway_1; }
	inline void set__ferejoDeway_1(bool value)
	{
		____ferejoDeway_1 = value;
	}

	inline static int32_t get_offset_of__patetowNetrere_2() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____patetowNetrere_2)); }
	inline bool get__patetowNetrere_2() const { return ____patetowNetrere_2; }
	inline bool* get_address_of__patetowNetrere_2() { return &____patetowNetrere_2; }
	inline void set__patetowNetrere_2(bool value)
	{
		____patetowNetrere_2 = value;
	}

	inline static int32_t get_offset_of__seltru_3() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____seltru_3)); }
	inline String_t* get__seltru_3() const { return ____seltru_3; }
	inline String_t** get_address_of__seltru_3() { return &____seltru_3; }
	inline void set__seltru_3(String_t* value)
	{
		____seltru_3 = value;
		Il2CppCodeGenWriteBarrier(&____seltru_3, value);
	}

	inline static int32_t get_offset_of__hallriswhow_4() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____hallriswhow_4)); }
	inline String_t* get__hallriswhow_4() const { return ____hallriswhow_4; }
	inline String_t** get_address_of__hallriswhow_4() { return &____hallriswhow_4; }
	inline void set__hallriswhow_4(String_t* value)
	{
		____hallriswhow_4 = value;
		Il2CppCodeGenWriteBarrier(&____hallriswhow_4, value);
	}

	inline static int32_t get_offset_of__sigor_5() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____sigor_5)); }
	inline String_t* get__sigor_5() const { return ____sigor_5; }
	inline String_t** get_address_of__sigor_5() { return &____sigor_5; }
	inline void set__sigor_5(String_t* value)
	{
		____sigor_5 = value;
		Il2CppCodeGenWriteBarrier(&____sigor_5, value);
	}

	inline static int32_t get_offset_of__seredouMowchearlas_6() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____seredouMowchearlas_6)); }
	inline int32_t get__seredouMowchearlas_6() const { return ____seredouMowchearlas_6; }
	inline int32_t* get_address_of__seredouMowchearlas_6() { return &____seredouMowchearlas_6; }
	inline void set__seredouMowchearlas_6(int32_t value)
	{
		____seredouMowchearlas_6 = value;
	}

	inline static int32_t get_offset_of__rerene_7() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____rerene_7)); }
	inline bool get__rerene_7() const { return ____rerene_7; }
	inline bool* get_address_of__rerene_7() { return &____rerene_7; }
	inline void set__rerene_7(bool value)
	{
		____rerene_7 = value;
	}

	inline static int32_t get_offset_of__pesel_8() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____pesel_8)); }
	inline int32_t get__pesel_8() const { return ____pesel_8; }
	inline int32_t* get_address_of__pesel_8() { return &____pesel_8; }
	inline void set__pesel_8(int32_t value)
	{
		____pesel_8 = value;
	}

	inline static int32_t get_offset_of__stemha_9() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____stemha_9)); }
	inline bool get__stemha_9() const { return ____stemha_9; }
	inline bool* get_address_of__stemha_9() { return &____stemha_9; }
	inline void set__stemha_9(bool value)
	{
		____stemha_9 = value;
	}

	inline static int32_t get_offset_of__ladis_10() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____ladis_10)); }
	inline bool get__ladis_10() const { return ____ladis_10; }
	inline bool* get_address_of__ladis_10() { return &____ladis_10; }
	inline void set__ladis_10(bool value)
	{
		____ladis_10 = value;
	}

	inline static int32_t get_offset_of__mirjusas_11() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____mirjusas_11)); }
	inline uint32_t get__mirjusas_11() const { return ____mirjusas_11; }
	inline uint32_t* get_address_of__mirjusas_11() { return &____mirjusas_11; }
	inline void set__mirjusas_11(uint32_t value)
	{
		____mirjusas_11 = value;
	}

	inline static int32_t get_offset_of__jinemti_12() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____jinemti_12)); }
	inline float get__jinemti_12() const { return ____jinemti_12; }
	inline float* get_address_of__jinemti_12() { return &____jinemti_12; }
	inline void set__jinemti_12(float value)
	{
		____jinemti_12 = value;
	}

	inline static int32_t get_offset_of__birde_13() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____birde_13)); }
	inline float get__birde_13() const { return ____birde_13; }
	inline float* get_address_of__birde_13() { return &____birde_13; }
	inline void set__birde_13(float value)
	{
		____birde_13 = value;
	}

	inline static int32_t get_offset_of__palldradarZeanidem_14() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____palldradarZeanidem_14)); }
	inline String_t* get__palldradarZeanidem_14() const { return ____palldradarZeanidem_14; }
	inline String_t** get_address_of__palldradarZeanidem_14() { return &____palldradarZeanidem_14; }
	inline void set__palldradarZeanidem_14(String_t* value)
	{
		____palldradarZeanidem_14 = value;
		Il2CppCodeGenWriteBarrier(&____palldradarZeanidem_14, value);
	}

	inline static int32_t get_offset_of__sowpofel_15() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____sowpofel_15)); }
	inline bool get__sowpofel_15() const { return ____sowpofel_15; }
	inline bool* get_address_of__sowpofel_15() { return &____sowpofel_15; }
	inline void set__sowpofel_15(bool value)
	{
		____sowpofel_15 = value;
	}

	inline static int32_t get_offset_of__borxooneSismihaw_16() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____borxooneSismihaw_16)); }
	inline bool get__borxooneSismihaw_16() const { return ____borxooneSismihaw_16; }
	inline bool* get_address_of__borxooneSismihaw_16() { return &____borxooneSismihaw_16; }
	inline void set__borxooneSismihaw_16(bool value)
	{
		____borxooneSismihaw_16 = value;
	}

	inline static int32_t get_offset_of__wijee_17() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____wijee_17)); }
	inline String_t* get__wijee_17() const { return ____wijee_17; }
	inline String_t** get_address_of__wijee_17() { return &____wijee_17; }
	inline void set__wijee_17(String_t* value)
	{
		____wijee_17 = value;
		Il2CppCodeGenWriteBarrier(&____wijee_17, value);
	}

	inline static int32_t get_offset_of__lezerarWelsallra_18() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____lezerarWelsallra_18)); }
	inline int32_t get__lezerarWelsallra_18() const { return ____lezerarWelsallra_18; }
	inline int32_t* get_address_of__lezerarWelsallra_18() { return &____lezerarWelsallra_18; }
	inline void set__lezerarWelsallra_18(int32_t value)
	{
		____lezerarWelsallra_18 = value;
	}

	inline static int32_t get_offset_of__nerrouNorsel_19() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____nerrouNorsel_19)); }
	inline int32_t get__nerrouNorsel_19() const { return ____nerrouNorsel_19; }
	inline int32_t* get_address_of__nerrouNorsel_19() { return &____nerrouNorsel_19; }
	inline void set__nerrouNorsel_19(int32_t value)
	{
		____nerrouNorsel_19 = value;
	}

	inline static int32_t get_offset_of__jacair_20() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____jacair_20)); }
	inline uint32_t get__jacair_20() const { return ____jacair_20; }
	inline uint32_t* get_address_of__jacair_20() { return &____jacair_20; }
	inline void set__jacair_20(uint32_t value)
	{
		____jacair_20 = value;
	}

	inline static int32_t get_offset_of__demousouBeboras_21() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____demousouBeboras_21)); }
	inline bool get__demousouBeboras_21() const { return ____demousouBeboras_21; }
	inline bool* get_address_of__demousouBeboras_21() { return &____demousouBeboras_21; }
	inline void set__demousouBeboras_21(bool value)
	{
		____demousouBeboras_21 = value;
	}

	inline static int32_t get_offset_of__coogerelairPaipou_22() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____coogerelairPaipou_22)); }
	inline String_t* get__coogerelairPaipou_22() const { return ____coogerelairPaipou_22; }
	inline String_t** get_address_of__coogerelairPaipou_22() { return &____coogerelairPaipou_22; }
	inline void set__coogerelairPaipou_22(String_t* value)
	{
		____coogerelairPaipou_22 = value;
		Il2CppCodeGenWriteBarrier(&____coogerelairPaipou_22, value);
	}

	inline static int32_t get_offset_of__cairstaw_23() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____cairstaw_23)); }
	inline int32_t get__cairstaw_23() const { return ____cairstaw_23; }
	inline int32_t* get_address_of__cairstaw_23() { return &____cairstaw_23; }
	inline void set__cairstaw_23(int32_t value)
	{
		____cairstaw_23 = value;
	}

	inline static int32_t get_offset_of__feris_24() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____feris_24)); }
	inline String_t* get__feris_24() const { return ____feris_24; }
	inline String_t** get_address_of__feris_24() { return &____feris_24; }
	inline void set__feris_24(String_t* value)
	{
		____feris_24 = value;
		Il2CppCodeGenWriteBarrier(&____feris_24, value);
	}

	inline static int32_t get_offset_of__rirma_25() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____rirma_25)); }
	inline uint32_t get__rirma_25() const { return ____rirma_25; }
	inline uint32_t* get_address_of__rirma_25() { return &____rirma_25; }
	inline void set__rirma_25(uint32_t value)
	{
		____rirma_25 = value;
	}

	inline static int32_t get_offset_of__wassoutoRacem_26() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____wassoutoRacem_26)); }
	inline bool get__wassoutoRacem_26() const { return ____wassoutoRacem_26; }
	inline bool* get_address_of__wassoutoRacem_26() { return &____wassoutoRacem_26; }
	inline void set__wassoutoRacem_26(bool value)
	{
		____wassoutoRacem_26 = value;
	}

	inline static int32_t get_offset_of__soucaltru_27() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____soucaltru_27)); }
	inline float get__soucaltru_27() const { return ____soucaltru_27; }
	inline float* get_address_of__soucaltru_27() { return &____soucaltru_27; }
	inline void set__soucaltru_27(float value)
	{
		____soucaltru_27 = value;
	}

	inline static int32_t get_offset_of__neyisgou_28() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____neyisgou_28)); }
	inline uint32_t get__neyisgou_28() const { return ____neyisgou_28; }
	inline uint32_t* get_address_of__neyisgou_28() { return &____neyisgou_28; }
	inline void set__neyisgou_28(uint32_t value)
	{
		____neyisgou_28 = value;
	}

	inline static int32_t get_offset_of__qeeraylouFereral_29() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____qeeraylouFereral_29)); }
	inline uint32_t get__qeeraylouFereral_29() const { return ____qeeraylouFereral_29; }
	inline uint32_t* get_address_of__qeeraylouFereral_29() { return &____qeeraylouFereral_29; }
	inline void set__qeeraylouFereral_29(uint32_t value)
	{
		____qeeraylouFereral_29 = value;
	}

	inline static int32_t get_offset_of__heremallTacarsoo_30() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____heremallTacarsoo_30)); }
	inline int32_t get__heremallTacarsoo_30() const { return ____heremallTacarsoo_30; }
	inline int32_t* get_address_of__heremallTacarsoo_30() { return &____heremallTacarsoo_30; }
	inline void set__heremallTacarsoo_30(int32_t value)
	{
		____heremallTacarsoo_30 = value;
	}

	inline static int32_t get_offset_of__dotu_31() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____dotu_31)); }
	inline int32_t get__dotu_31() const { return ____dotu_31; }
	inline int32_t* get_address_of__dotu_31() { return &____dotu_31; }
	inline void set__dotu_31(int32_t value)
	{
		____dotu_31 = value;
	}

	inline static int32_t get_offset_of__pechor_32() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____pechor_32)); }
	inline String_t* get__pechor_32() const { return ____pechor_32; }
	inline String_t** get_address_of__pechor_32() { return &____pechor_32; }
	inline void set__pechor_32(String_t* value)
	{
		____pechor_32 = value;
		Il2CppCodeGenWriteBarrier(&____pechor_32, value);
	}

	inline static int32_t get_offset_of__woutousis_33() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____woutousis_33)); }
	inline int32_t get__woutousis_33() const { return ____woutousis_33; }
	inline int32_t* get_address_of__woutousis_33() { return &____woutousis_33; }
	inline void set__woutousis_33(int32_t value)
	{
		____woutousis_33 = value;
	}

	inline static int32_t get_offset_of__xeradrouHopo_34() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____xeradrouHopo_34)); }
	inline String_t* get__xeradrouHopo_34() const { return ____xeradrouHopo_34; }
	inline String_t** get_address_of__xeradrouHopo_34() { return &____xeradrouHopo_34; }
	inline void set__xeradrouHopo_34(String_t* value)
	{
		____xeradrouHopo_34 = value;
		Il2CppCodeGenWriteBarrier(&____xeradrouHopo_34, value);
	}

	inline static int32_t get_offset_of__mouster_35() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____mouster_35)); }
	inline bool get__mouster_35() const { return ____mouster_35; }
	inline bool* get_address_of__mouster_35() { return &____mouster_35; }
	inline void set__mouster_35(bool value)
	{
		____mouster_35 = value;
	}

	inline static int32_t get_offset_of__wikerea_36() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____wikerea_36)); }
	inline float get__wikerea_36() const { return ____wikerea_36; }
	inline float* get_address_of__wikerea_36() { return &____wikerea_36; }
	inline void set__wikerea_36(float value)
	{
		____wikerea_36 = value;
	}

	inline static int32_t get_offset_of__felmuDife_37() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____felmuDife_37)); }
	inline bool get__felmuDife_37() const { return ____felmuDife_37; }
	inline bool* get_address_of__felmuDife_37() { return &____felmuDife_37; }
	inline void set__felmuDife_37(bool value)
	{
		____felmuDife_37 = value;
	}

	inline static int32_t get_offset_of__purlea_38() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____purlea_38)); }
	inline bool get__purlea_38() const { return ____purlea_38; }
	inline bool* get_address_of__purlea_38() { return &____purlea_38; }
	inline void set__purlea_38(bool value)
	{
		____purlea_38 = value;
	}

	inline static int32_t get_offset_of__kursalhere_39() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____kursalhere_39)); }
	inline uint32_t get__kursalhere_39() const { return ____kursalhere_39; }
	inline uint32_t* get_address_of__kursalhere_39() { return &____kursalhere_39; }
	inline void set__kursalhere_39(uint32_t value)
	{
		____kursalhere_39 = value;
	}

	inline static int32_t get_offset_of__fairputiQawvir_40() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____fairputiQawvir_40)); }
	inline String_t* get__fairputiQawvir_40() const { return ____fairputiQawvir_40; }
	inline String_t** get_address_of__fairputiQawvir_40() { return &____fairputiQawvir_40; }
	inline void set__fairputiQawvir_40(String_t* value)
	{
		____fairputiQawvir_40 = value;
		Il2CppCodeGenWriteBarrier(&____fairputiQawvir_40, value);
	}

	inline static int32_t get_offset_of__gigoujeBelsallkair_41() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____gigoujeBelsallkair_41)); }
	inline uint32_t get__gigoujeBelsallkair_41() const { return ____gigoujeBelsallkair_41; }
	inline uint32_t* get_address_of__gigoujeBelsallkair_41() { return &____gigoujeBelsallkair_41; }
	inline void set__gigoujeBelsallkair_41(uint32_t value)
	{
		____gigoujeBelsallkair_41 = value;
	}

	inline static int32_t get_offset_of__dati_42() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____dati_42)); }
	inline int32_t get__dati_42() const { return ____dati_42; }
	inline int32_t* get_address_of__dati_42() { return &____dati_42; }
	inline void set__dati_42(int32_t value)
	{
		____dati_42 = value;
	}

	inline static int32_t get_offset_of__pimou_43() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____pimou_43)); }
	inline uint32_t get__pimou_43() const { return ____pimou_43; }
	inline uint32_t* get_address_of__pimou_43() { return &____pimou_43; }
	inline void set__pimou_43(uint32_t value)
	{
		____pimou_43 = value;
	}

	inline static int32_t get_offset_of__wecaci_44() { return static_cast<int32_t>(offsetof(M_noqall28_t4104028199, ____wecaci_44)); }
	inline bool get__wecaci_44() const { return ____wecaci_44; }
	inline bool* get_address_of__wecaci_44() { return &____wecaci_44; }
	inline void set__wecaci_44(bool value)
	{
		____wecaci_44 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
