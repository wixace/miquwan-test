﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LightweightRVO
struct LightweightRVO_t2013673693;
// Pathfinding.RVO.Simulator
struct Simulator_t2705969170;
// Pathfinding.RVO.IAgent
struct IAgent_t2802767396;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_LightweightRVO2013673693.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_Simulator2705969170.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void LightweightRVO::.ctor()
extern "C"  void LightweightRVO__ctor_m2297773150 (LightweightRVO_t2013673693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightweightRVO::Start()
extern "C"  void LightweightRVO_Start_m1244910942 (LightweightRVO_t2013673693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightweightRVO::OnGUI()
extern "C"  void LightweightRVO_OnGUI_m1793171800 (LightweightRVO_t2013673693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single LightweightRVO::uniformDistance(System.Single)
extern "C"  float LightweightRVO_uniformDistance_m2594585138 (LightweightRVO_t2013673693 * __this, float ___radius0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightweightRVO::CreateAgents(System.Int32)
extern "C"  void LightweightRVO_CreateAgents_m2450395425 (LightweightRVO_t2013673693 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightweightRVO::Update()
extern "C"  void LightweightRVO_Update_m4238353007 (LightweightRVO_t2013673693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color LightweightRVO::HSVToRGB(System.Single,System.Single,System.Single)
extern "C"  Color_t4194546905  LightweightRVO_HSVToRGB_m2184098305 (Il2CppObject * __this /* static, unused */, float ___h0, float ___s1, float ___v2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightweightRVO::ilo_CreateAgents1(LightweightRVO,System.Int32)
extern "C"  void LightweightRVO_ilo_CreateAgents1_m745069892 (Il2CppObject * __this /* static, unused */, LightweightRVO_t2013673693 * ____this0, int32_t ___num1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightweightRVO::ilo_ClearAgents2(Pathfinding.RVO.Simulator)
extern "C"  void LightweightRVO_ilo_ClearAgents2_m458627051 (Il2CppObject * __this /* static, unused */, Simulator_t2705969170 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RVO.IAgent LightweightRVO::ilo_AddAgent3(Pathfinding.RVO.Simulator,UnityEngine.Vector3)
extern "C"  Il2CppObject * LightweightRVO_ilo_AddAgent3_m55597514 (Il2CppObject * __this /* static, unused */, Simulator_t2705969170 * ____this0, Vector3_t4282066566  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color LightweightRVO::ilo_HSVToRGB4(System.Single,System.Single,System.Single)
extern "C"  Color_t4194546905  LightweightRVO_ilo_HSVToRGB4_m664037446 (Il2CppObject * __this /* static, unused */, float ___h0, float ___s1, float ___v2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single LightweightRVO::ilo_uniformDistance5(LightweightRVO,System.Single)
extern "C"  float LightweightRVO_ilo_uniformDistance5_m1777686393 (Il2CppObject * __this /* static, unused */, LightweightRVO_t2013673693 * ____this0, float ___radius1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightweightRVO::ilo_set_MaxSpeed6(Pathfinding.RVO.IAgent,System.Single)
extern "C"  void LightweightRVO_ilo_set_MaxSpeed6_m1329003547 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightweightRVO::ilo_set_AgentTimeHorizon7(Pathfinding.RVO.IAgent,System.Single)
extern "C"  void LightweightRVO_ilo_set_AgentTimeHorizon7_m992426262 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightweightRVO::ilo_set_NeighbourDist8(Pathfinding.RVO.IAgent,System.Single)
extern "C"  void LightweightRVO_ilo_set_NeighbourDist8_m3495223367 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightweightRVO::ilo_set_DebugDraw9(Pathfinding.RVO.IAgent,System.Boolean)
extern "C"  void LightweightRVO_ilo_set_DebugDraw9_m1973721998 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single LightweightRVO::ilo_get_Radius10(Pathfinding.RVO.IAgent)
extern "C"  float LightweightRVO_ilo_get_Radius10_m1538745846 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 LightweightRVO::ilo_get_InterpolatedPosition11(Pathfinding.RVO.IAgent)
extern "C"  Vector3_t4282066566  LightweightRVO_ilo_get_InterpolatedPosition11_m4154605343 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
