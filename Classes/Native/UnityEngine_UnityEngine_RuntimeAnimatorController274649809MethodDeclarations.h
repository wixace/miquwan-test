﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t274649809;
// UnityEngine.AnimationClip[]
struct AnimationClipU5BU5D_t4186127791;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.RuntimeAnimatorController::.ctor()
extern "C"  void RuntimeAnimatorController__ctor_m1877476190 (RuntimeAnimatorController_t274649809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationClip[] UnityEngine.RuntimeAnimatorController::get_animationClips()
extern "C"  AnimationClipU5BU5D_t4186127791* RuntimeAnimatorController_get_animationClips_m3376046746 (RuntimeAnimatorController_t274649809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
