﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// monstersCfg
struct monstersCfg_t1542396363;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSPVPRobotNPC
struct  CSPVPRobotNPC_t3152738001  : public Il2CppObject
{
public:
	// System.Int32 CSPVPRobotNPC::npcId
	int32_t ___npcId_0;
	// System.Int32 CSPVPRobotNPC::level
	int32_t ___level_1;
	// System.Int32 CSPVPRobotNPC::viewLevel
	int32_t ___viewLevel_2;
	// System.Int32 CSPVPRobotNPC::viewGrade
	int32_t ___viewGrade_3;
	// System.Int32 CSPVPRobotNPC::star
	int32_t ___star_4;
	// System.Int32 CSPVPRobotNPC::evolve
	int32_t ___evolve_5;
	// System.Int32 CSPVPRobotNPC::nuqi
	int32_t ___nuqi_6;
	// System.Int32 CSPVPRobotNPC::intelligence
	int32_t ___intelligence_7;
	// System.Int32 CSPVPRobotNPC::residualHp
	int32_t ___residualHp_8;
	// System.Int32 CSPVPRobotNPC::residualMp
	int32_t ___residualMp_9;
	// monstersCfg CSPVPRobotNPC::monsterCfg
	monstersCfg_t1542396363 * ___monsterCfg_10;

public:
	inline static int32_t get_offset_of_npcId_0() { return static_cast<int32_t>(offsetof(CSPVPRobotNPC_t3152738001, ___npcId_0)); }
	inline int32_t get_npcId_0() const { return ___npcId_0; }
	inline int32_t* get_address_of_npcId_0() { return &___npcId_0; }
	inline void set_npcId_0(int32_t value)
	{
		___npcId_0 = value;
	}

	inline static int32_t get_offset_of_level_1() { return static_cast<int32_t>(offsetof(CSPVPRobotNPC_t3152738001, ___level_1)); }
	inline int32_t get_level_1() const { return ___level_1; }
	inline int32_t* get_address_of_level_1() { return &___level_1; }
	inline void set_level_1(int32_t value)
	{
		___level_1 = value;
	}

	inline static int32_t get_offset_of_viewLevel_2() { return static_cast<int32_t>(offsetof(CSPVPRobotNPC_t3152738001, ___viewLevel_2)); }
	inline int32_t get_viewLevel_2() const { return ___viewLevel_2; }
	inline int32_t* get_address_of_viewLevel_2() { return &___viewLevel_2; }
	inline void set_viewLevel_2(int32_t value)
	{
		___viewLevel_2 = value;
	}

	inline static int32_t get_offset_of_viewGrade_3() { return static_cast<int32_t>(offsetof(CSPVPRobotNPC_t3152738001, ___viewGrade_3)); }
	inline int32_t get_viewGrade_3() const { return ___viewGrade_3; }
	inline int32_t* get_address_of_viewGrade_3() { return &___viewGrade_3; }
	inline void set_viewGrade_3(int32_t value)
	{
		___viewGrade_3 = value;
	}

	inline static int32_t get_offset_of_star_4() { return static_cast<int32_t>(offsetof(CSPVPRobotNPC_t3152738001, ___star_4)); }
	inline int32_t get_star_4() const { return ___star_4; }
	inline int32_t* get_address_of_star_4() { return &___star_4; }
	inline void set_star_4(int32_t value)
	{
		___star_4 = value;
	}

	inline static int32_t get_offset_of_evolve_5() { return static_cast<int32_t>(offsetof(CSPVPRobotNPC_t3152738001, ___evolve_5)); }
	inline int32_t get_evolve_5() const { return ___evolve_5; }
	inline int32_t* get_address_of_evolve_5() { return &___evolve_5; }
	inline void set_evolve_5(int32_t value)
	{
		___evolve_5 = value;
	}

	inline static int32_t get_offset_of_nuqi_6() { return static_cast<int32_t>(offsetof(CSPVPRobotNPC_t3152738001, ___nuqi_6)); }
	inline int32_t get_nuqi_6() const { return ___nuqi_6; }
	inline int32_t* get_address_of_nuqi_6() { return &___nuqi_6; }
	inline void set_nuqi_6(int32_t value)
	{
		___nuqi_6 = value;
	}

	inline static int32_t get_offset_of_intelligence_7() { return static_cast<int32_t>(offsetof(CSPVPRobotNPC_t3152738001, ___intelligence_7)); }
	inline int32_t get_intelligence_7() const { return ___intelligence_7; }
	inline int32_t* get_address_of_intelligence_7() { return &___intelligence_7; }
	inline void set_intelligence_7(int32_t value)
	{
		___intelligence_7 = value;
	}

	inline static int32_t get_offset_of_residualHp_8() { return static_cast<int32_t>(offsetof(CSPVPRobotNPC_t3152738001, ___residualHp_8)); }
	inline int32_t get_residualHp_8() const { return ___residualHp_8; }
	inline int32_t* get_address_of_residualHp_8() { return &___residualHp_8; }
	inline void set_residualHp_8(int32_t value)
	{
		___residualHp_8 = value;
	}

	inline static int32_t get_offset_of_residualMp_9() { return static_cast<int32_t>(offsetof(CSPVPRobotNPC_t3152738001, ___residualMp_9)); }
	inline int32_t get_residualMp_9() const { return ___residualMp_9; }
	inline int32_t* get_address_of_residualMp_9() { return &___residualMp_9; }
	inline void set_residualMp_9(int32_t value)
	{
		___residualMp_9 = value;
	}

	inline static int32_t get_offset_of_monsterCfg_10() { return static_cast<int32_t>(offsetof(CSPVPRobotNPC_t3152738001, ___monsterCfg_10)); }
	inline monstersCfg_t1542396363 * get_monsterCfg_10() const { return ___monsterCfg_10; }
	inline monstersCfg_t1542396363 ** get_address_of_monsterCfg_10() { return &___monsterCfg_10; }
	inline void set_monsterCfg_10(monstersCfg_t1542396363 * value)
	{
		___monsterCfg_10 = value;
		Il2CppCodeGenWriteBarrier(&___monsterCfg_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
