﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<PushType,PluginPush/NotificationData>
struct Dictionary_2_t4034500590;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginPush
struct  PluginPush_t1606327565  : public MonoBehaviour_t667441552
{
public:
	// System.Collections.Generic.Dictionary`2<PushType,PluginPush/NotificationData> PluginPush::notificationDataDic
	Dictionary_2_t4034500590 * ___notificationDataDic_2;

public:
	inline static int32_t get_offset_of_notificationDataDic_2() { return static_cast<int32_t>(offsetof(PluginPush_t1606327565, ___notificationDataDic_2)); }
	inline Dictionary_2_t4034500590 * get_notificationDataDic_2() const { return ___notificationDataDic_2; }
	inline Dictionary_2_t4034500590 ** get_address_of_notificationDataDic_2() { return &___notificationDataDic_2; }
	inline void set_notificationDataDic_2(Dictionary_2_t4034500590 * value)
	{
		___notificationDataDic_2 = value;
		Il2CppCodeGenWriteBarrier(&___notificationDataDic_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
