﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CombatEntity
struct CombatEntity_t684137495;
// Portal
struct Portal_t2396353676;
// AICtrl
struct AICtrl_t1930422963;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;

#include "AssemblyU2DCSharp_CombatEntity684137495.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Monster
struct  Monster_t2901270458  : public CombatEntity_t684137495
{
public:
	// CombatEntity Monster::SummonTarget
	CombatEntity_t684137495 * ___SummonTarget_321;
	// Portal Monster::_parMonster
	Portal_t2396353676 * ____parMonster_322;
	// System.Int32 Monster::summonEffectId
	int32_t ___summonEffectId_323;
	// System.Int32 Monster::drop_growup
	int32_t ___drop_growup_324;
	// System.Boolean Monster::isHideSummon
	bool ___isHideSummon_325;
	// System.Single Monster::baseAtk
	float ___baseAtk_326;
	// System.Single Monster::basePd
	float ___basePd_327;
	// System.Single Monster::baseMd
	float ___baseMd_328;
	// System.Single Monster::baseHp
	float ___baseHp_329;
	// System.Single Monster::<birth_time>k__BackingField
	float ___U3Cbirth_timeU3Ek__BackingField_330;
	// System.Single Monster::<cur_time>k__BackingField
	float ___U3Ccur_timeU3Ek__BackingField_331;
	// System.Int32 Monster::<mapEditorType>k__BackingField
	int32_t ___U3CmapEditorTypeU3Ek__BackingField_332;
	// AICtrl Monster::<aiCtrl>k__BackingField
	AICtrl_t1930422963 * ___U3CaiCtrlU3Ek__BackingField_333;
	// System.Boolean Monster::<isInFinish>k__BackingField
	bool ___U3CisInFinishU3Ek__BackingField_334;
	// System.Boolean Monster::<isInNum>k__BackingField
	bool ___U3CisInNumU3Ek__BackingField_335;

public:
	inline static int32_t get_offset_of_SummonTarget_321() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___SummonTarget_321)); }
	inline CombatEntity_t684137495 * get_SummonTarget_321() const { return ___SummonTarget_321; }
	inline CombatEntity_t684137495 ** get_address_of_SummonTarget_321() { return &___SummonTarget_321; }
	inline void set_SummonTarget_321(CombatEntity_t684137495 * value)
	{
		___SummonTarget_321 = value;
		Il2CppCodeGenWriteBarrier(&___SummonTarget_321, value);
	}

	inline static int32_t get_offset_of__parMonster_322() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ____parMonster_322)); }
	inline Portal_t2396353676 * get__parMonster_322() const { return ____parMonster_322; }
	inline Portal_t2396353676 ** get_address_of__parMonster_322() { return &____parMonster_322; }
	inline void set__parMonster_322(Portal_t2396353676 * value)
	{
		____parMonster_322 = value;
		Il2CppCodeGenWriteBarrier(&____parMonster_322, value);
	}

	inline static int32_t get_offset_of_summonEffectId_323() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___summonEffectId_323)); }
	inline int32_t get_summonEffectId_323() const { return ___summonEffectId_323; }
	inline int32_t* get_address_of_summonEffectId_323() { return &___summonEffectId_323; }
	inline void set_summonEffectId_323(int32_t value)
	{
		___summonEffectId_323 = value;
	}

	inline static int32_t get_offset_of_drop_growup_324() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___drop_growup_324)); }
	inline int32_t get_drop_growup_324() const { return ___drop_growup_324; }
	inline int32_t* get_address_of_drop_growup_324() { return &___drop_growup_324; }
	inline void set_drop_growup_324(int32_t value)
	{
		___drop_growup_324 = value;
	}

	inline static int32_t get_offset_of_isHideSummon_325() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___isHideSummon_325)); }
	inline bool get_isHideSummon_325() const { return ___isHideSummon_325; }
	inline bool* get_address_of_isHideSummon_325() { return &___isHideSummon_325; }
	inline void set_isHideSummon_325(bool value)
	{
		___isHideSummon_325 = value;
	}

	inline static int32_t get_offset_of_baseAtk_326() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___baseAtk_326)); }
	inline float get_baseAtk_326() const { return ___baseAtk_326; }
	inline float* get_address_of_baseAtk_326() { return &___baseAtk_326; }
	inline void set_baseAtk_326(float value)
	{
		___baseAtk_326 = value;
	}

	inline static int32_t get_offset_of_basePd_327() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___basePd_327)); }
	inline float get_basePd_327() const { return ___basePd_327; }
	inline float* get_address_of_basePd_327() { return &___basePd_327; }
	inline void set_basePd_327(float value)
	{
		___basePd_327 = value;
	}

	inline static int32_t get_offset_of_baseMd_328() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___baseMd_328)); }
	inline float get_baseMd_328() const { return ___baseMd_328; }
	inline float* get_address_of_baseMd_328() { return &___baseMd_328; }
	inline void set_baseMd_328(float value)
	{
		___baseMd_328 = value;
	}

	inline static int32_t get_offset_of_baseHp_329() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___baseHp_329)); }
	inline float get_baseHp_329() const { return ___baseHp_329; }
	inline float* get_address_of_baseHp_329() { return &___baseHp_329; }
	inline void set_baseHp_329(float value)
	{
		___baseHp_329 = value;
	}

	inline static int32_t get_offset_of_U3Cbirth_timeU3Ek__BackingField_330() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___U3Cbirth_timeU3Ek__BackingField_330)); }
	inline float get_U3Cbirth_timeU3Ek__BackingField_330() const { return ___U3Cbirth_timeU3Ek__BackingField_330; }
	inline float* get_address_of_U3Cbirth_timeU3Ek__BackingField_330() { return &___U3Cbirth_timeU3Ek__BackingField_330; }
	inline void set_U3Cbirth_timeU3Ek__BackingField_330(float value)
	{
		___U3Cbirth_timeU3Ek__BackingField_330 = value;
	}

	inline static int32_t get_offset_of_U3Ccur_timeU3Ek__BackingField_331() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___U3Ccur_timeU3Ek__BackingField_331)); }
	inline float get_U3Ccur_timeU3Ek__BackingField_331() const { return ___U3Ccur_timeU3Ek__BackingField_331; }
	inline float* get_address_of_U3Ccur_timeU3Ek__BackingField_331() { return &___U3Ccur_timeU3Ek__BackingField_331; }
	inline void set_U3Ccur_timeU3Ek__BackingField_331(float value)
	{
		___U3Ccur_timeU3Ek__BackingField_331 = value;
	}

	inline static int32_t get_offset_of_U3CmapEditorTypeU3Ek__BackingField_332() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___U3CmapEditorTypeU3Ek__BackingField_332)); }
	inline int32_t get_U3CmapEditorTypeU3Ek__BackingField_332() const { return ___U3CmapEditorTypeU3Ek__BackingField_332; }
	inline int32_t* get_address_of_U3CmapEditorTypeU3Ek__BackingField_332() { return &___U3CmapEditorTypeU3Ek__BackingField_332; }
	inline void set_U3CmapEditorTypeU3Ek__BackingField_332(int32_t value)
	{
		___U3CmapEditorTypeU3Ek__BackingField_332 = value;
	}

	inline static int32_t get_offset_of_U3CaiCtrlU3Ek__BackingField_333() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___U3CaiCtrlU3Ek__BackingField_333)); }
	inline AICtrl_t1930422963 * get_U3CaiCtrlU3Ek__BackingField_333() const { return ___U3CaiCtrlU3Ek__BackingField_333; }
	inline AICtrl_t1930422963 ** get_address_of_U3CaiCtrlU3Ek__BackingField_333() { return &___U3CaiCtrlU3Ek__BackingField_333; }
	inline void set_U3CaiCtrlU3Ek__BackingField_333(AICtrl_t1930422963 * value)
	{
		___U3CaiCtrlU3Ek__BackingField_333 = value;
		Il2CppCodeGenWriteBarrier(&___U3CaiCtrlU3Ek__BackingField_333, value);
	}

	inline static int32_t get_offset_of_U3CisInFinishU3Ek__BackingField_334() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___U3CisInFinishU3Ek__BackingField_334)); }
	inline bool get_U3CisInFinishU3Ek__BackingField_334() const { return ___U3CisInFinishU3Ek__BackingField_334; }
	inline bool* get_address_of_U3CisInFinishU3Ek__BackingField_334() { return &___U3CisInFinishU3Ek__BackingField_334; }
	inline void set_U3CisInFinishU3Ek__BackingField_334(bool value)
	{
		___U3CisInFinishU3Ek__BackingField_334 = value;
	}

	inline static int32_t get_offset_of_U3CisInNumU3Ek__BackingField_335() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___U3CisInNumU3Ek__BackingField_335)); }
	inline bool get_U3CisInNumU3Ek__BackingField_335() const { return ___U3CisInNumU3Ek__BackingField_335; }
	inline bool* get_address_of_U3CisInNumU3Ek__BackingField_335() { return &___U3CisInNumU3Ek__BackingField_335; }
	inline void set_U3CisInNumU3Ek__BackingField_335(bool value)
	{
		___U3CisInNumU3Ek__BackingField_335 = value;
	}
};

struct Monster_t2901270458_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Monster::<>f__switch$map1A
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map1A_336;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1A_336() { return static_cast<int32_t>(offsetof(Monster_t2901270458_StaticFields, ___U3CU3Ef__switchU24map1A_336)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map1A_336() const { return ___U3CU3Ef__switchU24map1A_336; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map1A_336() { return &___U3CU3Ef__switchU24map1A_336; }
	inline void set_U3CU3Ef__switchU24map1A_336(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map1A_336 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1A_336, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
