﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Vector3>
struct U3CGetEnumeratorU3Ec__Iterator29_t1734063027;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Vector3>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29__ctor_m3027398978_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1734063027 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29__ctor_m3027398978(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t1734063027 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29__ctor_m3027398978_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Vector3>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Vector3_t4282066566  U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2238895467_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1734063027 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2238895467(__this, method) ((  Vector3_t4282066566  (*) (U3CGetEnumeratorU3Ec__Iterator29_t1734063027 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2238895467_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m2485406382_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1734063027 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m2485406382(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator29_t1734063027 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m2485406382_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Vector3>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m2069963354_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1734063027 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m2069963354(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator29_t1734063027 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m2069963354_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Vector3>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29_Dispose_m1535841535_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1734063027 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_Dispose_m1535841535(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t1734063027 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_Dispose_m1535841535_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Vector3>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29_Reset_m673831919_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1734063027 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_Reset_m673831919(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t1734063027 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_Reset_m673831919_gshared)(__this, method)
