﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._e628ed36c34984f19707b26f0f605a44
struct _e628ed36c34984f19707b26f0f605a44_t715680303;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._e628ed36c34984f19707b26f0f605a44::.ctor()
extern "C"  void _e628ed36c34984f19707b26f0f605a44__ctor_m1349370078 (_e628ed36c34984f19707b26f0f605a44_t715680303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e628ed36c34984f19707b26f0f605a44::_e628ed36c34984f19707b26f0f605a44m2(System.Int32)
extern "C"  int32_t _e628ed36c34984f19707b26f0f605a44__e628ed36c34984f19707b26f0f605a44m2_m2806704953 (_e628ed36c34984f19707b26f0f605a44_t715680303 * __this, int32_t ____e628ed36c34984f19707b26f0f605a44a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e628ed36c34984f19707b26f0f605a44::_e628ed36c34984f19707b26f0f605a44m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _e628ed36c34984f19707b26f0f605a44__e628ed36c34984f19707b26f0f605a44m_m1822674845 (_e628ed36c34984f19707b26f0f605a44_t715680303 * __this, int32_t ____e628ed36c34984f19707b26f0f605a44a0, int32_t ____e628ed36c34984f19707b26f0f605a44351, int32_t ____e628ed36c34984f19707b26f0f605a44c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
