﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSSerializer/SerializeStruct
struct SerializeStruct_t2767317089;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSSerializer_SerializeStruct_STy1913504511.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSSerializer_SerializeStruct2767317089.h"

// System.Void JSSerializer/SerializeStruct::.ctor(JSSerializer/SerializeStruct/SType,System.String,JSSerializer/SerializeStruct)
extern "C"  void SerializeStruct__ctor_m2927943982 (SerializeStruct_t2767317089 * __this, int32_t ___t0, String_t* ___name1, SerializeStruct_t2767317089 * ___father2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSSerializer/SerializeStruct::get_id()
extern "C"  int32_t SerializeStruct_get_id_m3883999612 (SerializeStruct_t2767317089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSSerializer/SerializeStruct::set_id(System.Int32)
extern "C"  void SerializeStruct_set_id_m648038067 (SerializeStruct_t2767317089 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSSerializer/SerializeStruct::AddChild(JSSerializer/SerializeStruct)
extern "C"  void SerializeStruct_AddChild_m3716861476 (SerializeStruct_t2767317089 * __this, SerializeStruct_t2767317089 * ___ss0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSSerializer/SerializeStruct::removeID()
extern "C"  void SerializeStruct_removeID_m1089670889 (SerializeStruct_t2767317089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSSerializer/SerializeStruct::calcID()
extern "C"  int32_t SerializeStruct_calcID_m1726354952 (SerializeStruct_t2767317089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
