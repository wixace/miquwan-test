﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AssetBundle
struct AssetBundle_t2070959688;
// System.String[]
struct StringU5BU5D_t4054002952;
// ABDepends
struct ABDepends_t759746598;
// System.Object
struct Il2CppObject;
// ConfigAssetMgr
struct ConfigAssetMgr_t4036193930;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConfigAssetMgr/<RunLoadConfig>c__Iterator33
struct  U3CRunLoadConfigU3Ec__Iterator33_t1082580655  : public Il2CppObject
{
public:
	// UnityEngine.AssetBundle ConfigAssetMgr/<RunLoadConfig>c__Iterator33::<abDepends>__0
	AssetBundle_t2070959688 * ___U3CabDependsU3E__0_0;
	// System.String[] ConfigAssetMgr/<RunLoadConfig>c__Iterator33::<allName>__1
	StringU5BU5D_t4054002952* ___U3CallNameU3E__1_1;
	// ABDepends ConfigAssetMgr/<RunLoadConfig>c__Iterator33::<dpbs>__2
	ABDepends_t759746598 * ___U3CdpbsU3E__2_2;
	// UnityEngine.AssetBundle ConfigAssetMgr/<RunLoadConfig>c__Iterator33::<resname>__3
	AssetBundle_t2070959688 * ___U3CresnameU3E__3_3;
	// UnityEngine.AssetBundle ConfigAssetMgr/<RunLoadConfig>c__Iterator33::<config>__4
	AssetBundle_t2070959688 * ___U3CconfigU3E__4_4;
	// System.Int32 ConfigAssetMgr/<RunLoadConfig>c__Iterator33::$PC
	int32_t ___U24PC_5;
	// System.Object ConfigAssetMgr/<RunLoadConfig>c__Iterator33::$current
	Il2CppObject * ___U24current_6;
	// ConfigAssetMgr ConfigAssetMgr/<RunLoadConfig>c__Iterator33::<>f__this
	ConfigAssetMgr_t4036193930 * ___U3CU3Ef__this_7;

public:
	inline static int32_t get_offset_of_U3CabDependsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRunLoadConfigU3Ec__Iterator33_t1082580655, ___U3CabDependsU3E__0_0)); }
	inline AssetBundle_t2070959688 * get_U3CabDependsU3E__0_0() const { return ___U3CabDependsU3E__0_0; }
	inline AssetBundle_t2070959688 ** get_address_of_U3CabDependsU3E__0_0() { return &___U3CabDependsU3E__0_0; }
	inline void set_U3CabDependsU3E__0_0(AssetBundle_t2070959688 * value)
	{
		___U3CabDependsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CabDependsU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CallNameU3E__1_1() { return static_cast<int32_t>(offsetof(U3CRunLoadConfigU3Ec__Iterator33_t1082580655, ___U3CallNameU3E__1_1)); }
	inline StringU5BU5D_t4054002952* get_U3CallNameU3E__1_1() const { return ___U3CallNameU3E__1_1; }
	inline StringU5BU5D_t4054002952** get_address_of_U3CallNameU3E__1_1() { return &___U3CallNameU3E__1_1; }
	inline void set_U3CallNameU3E__1_1(StringU5BU5D_t4054002952* value)
	{
		___U3CallNameU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CallNameU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CdpbsU3E__2_2() { return static_cast<int32_t>(offsetof(U3CRunLoadConfigU3Ec__Iterator33_t1082580655, ___U3CdpbsU3E__2_2)); }
	inline ABDepends_t759746598 * get_U3CdpbsU3E__2_2() const { return ___U3CdpbsU3E__2_2; }
	inline ABDepends_t759746598 ** get_address_of_U3CdpbsU3E__2_2() { return &___U3CdpbsU3E__2_2; }
	inline void set_U3CdpbsU3E__2_2(ABDepends_t759746598 * value)
	{
		___U3CdpbsU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdpbsU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CresnameU3E__3_3() { return static_cast<int32_t>(offsetof(U3CRunLoadConfigU3Ec__Iterator33_t1082580655, ___U3CresnameU3E__3_3)); }
	inline AssetBundle_t2070959688 * get_U3CresnameU3E__3_3() const { return ___U3CresnameU3E__3_3; }
	inline AssetBundle_t2070959688 ** get_address_of_U3CresnameU3E__3_3() { return &___U3CresnameU3E__3_3; }
	inline void set_U3CresnameU3E__3_3(AssetBundle_t2070959688 * value)
	{
		___U3CresnameU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CresnameU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CconfigU3E__4_4() { return static_cast<int32_t>(offsetof(U3CRunLoadConfigU3Ec__Iterator33_t1082580655, ___U3CconfigU3E__4_4)); }
	inline AssetBundle_t2070959688 * get_U3CconfigU3E__4_4() const { return ___U3CconfigU3E__4_4; }
	inline AssetBundle_t2070959688 ** get_address_of_U3CconfigU3E__4_4() { return &___U3CconfigU3E__4_4; }
	inline void set_U3CconfigU3E__4_4(AssetBundle_t2070959688 * value)
	{
		___U3CconfigU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CconfigU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CRunLoadConfigU3Ec__Iterator33_t1082580655, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CRunLoadConfigU3Ec__Iterator33_t1082580655, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_7() { return static_cast<int32_t>(offsetof(U3CRunLoadConfigU3Ec__Iterator33_t1082580655, ___U3CU3Ef__this_7)); }
	inline ConfigAssetMgr_t4036193930 * get_U3CU3Ef__this_7() const { return ___U3CU3Ef__this_7; }
	inline ConfigAssetMgr_t4036193930 ** get_address_of_U3CU3Ef__this_7() { return &___U3CU3Ef__this_7; }
	inline void set_U3CU3Ef__this_7(ConfigAssetMgr_t4036193930 * value)
	{
		___U3CU3Ef__this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
