﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_NavMeshHitGenerated
struct UnityEngine_NavMeshHitGenerated_t2933202596;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_NavMeshHitGenerated::.ctor()
extern "C"  void UnityEngine_NavMeshHitGenerated__ctor_m1089868103 (UnityEngine_NavMeshHitGenerated_t2933202596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshHitGenerated::.cctor()
extern "C"  void UnityEngine_NavMeshHitGenerated__cctor_m3239043910 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshHitGenerated::NavMeshHit_NavMeshHit1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshHitGenerated_NavMeshHit_NavMeshHit1_m3799813291 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshHitGenerated::NavMeshHit_position(JSVCall)
extern "C"  void UnityEngine_NavMeshHitGenerated_NavMeshHit_position_m3896449565 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshHitGenerated::NavMeshHit_normal(JSVCall)
extern "C"  void UnityEngine_NavMeshHitGenerated_NavMeshHit_normal_m342995999 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshHitGenerated::NavMeshHit_distance(JSVCall)
extern "C"  void UnityEngine_NavMeshHitGenerated_NavMeshHit_distance_m1826000081 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshHitGenerated::NavMeshHit_mask(JSVCall)
extern "C"  void UnityEngine_NavMeshHitGenerated_NavMeshHit_mask_m2320451514 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshHitGenerated::NavMeshHit_hit(JSVCall)
extern "C"  void UnityEngine_NavMeshHitGenerated_NavMeshHit_hit_m3271517107 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshHitGenerated::__Register()
extern "C"  void UnityEngine_NavMeshHitGenerated___Register_m3388350432 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_NavMeshHitGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_NavMeshHitGenerated_ilo_getObject1_m2303520591 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshHitGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_NavMeshHitGenerated_ilo_attachFinalizerObject2_m4065438673 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshHitGenerated::ilo_setVector3S3(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_NavMeshHitGenerated_ilo_setVector3S3_m999292450 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_NavMeshHitGenerated::ilo_getSingle4(System.Int32)
extern "C"  float UnityEngine_NavMeshHitGenerated_ilo_getSingle4_m2119112683 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshHitGenerated::ilo_changeJSObj5(System.Int32,System.Object)
extern "C"  void UnityEngine_NavMeshHitGenerated_ilo_changeJSObj5_m4033132166 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
