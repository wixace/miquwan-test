﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.MEventDelegate
struct MEventDelegate_t1837131344;
// Mihua.MEventDelegate/RequestDelegate
struct RequestDelegate_t1822766829;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mihua_MEventDelegate_RequestDele1822766829.h"

// System.Void Mihua.MEventDelegate::.ctor(Mihua.MEventDelegate/RequestDelegate)
extern "C"  void MEventDelegate__ctor_m654330726 (MEventDelegate_t1837131344 * __this, RequestDelegate_t1822766829 * ___reqDelegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
