﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_LayerMaskGenerated
struct UnityEngine_LayerMaskGenerated_t1724613018;
// JSVCall
struct JSVCall_t3708497963;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine_LayerMaskGenerated::.ctor()
extern "C"  void UnityEngine_LayerMaskGenerated__ctor_m1656637505 (UnityEngine_LayerMaskGenerated_t1724613018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LayerMaskGenerated::.cctor()
extern "C"  void UnityEngine_LayerMaskGenerated__cctor_m3629026188 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LayerMaskGenerated::LayerMask_LayerMask1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LayerMaskGenerated_LayerMask_LayerMask1_m2373084893 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LayerMaskGenerated::LayerMask_value(JSVCall)
extern "C"  void UnityEngine_LayerMaskGenerated_LayerMask_value_m1705894713 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LayerMaskGenerated::LayerMask_GetMask__String_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LayerMaskGenerated_LayerMask_GetMask__String_Array_m575652326 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LayerMaskGenerated::LayerMask_LayerToName__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LayerMaskGenerated_LayerMask_LayerToName__Int32_m1287593216 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LayerMaskGenerated::LayerMask_NameToLayer__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LayerMaskGenerated_LayerMask_NameToLayer__String_m1523124693 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LayerMaskGenerated::LayerMask_op_Implicit__Int32_to_LayerMask(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LayerMaskGenerated_LayerMask_op_Implicit__Int32_to_LayerMask_m3345833926 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LayerMaskGenerated::LayerMask_op_Implicit__LayerMask_to_Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LayerMaskGenerated_LayerMask_op_Implicit__LayerMask_to_Int32_m2872534792 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LayerMaskGenerated::__Register()
extern "C"  void UnityEngine_LayerMaskGenerated___Register_m1434137894 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UnityEngine_LayerMaskGenerated::<LayerMask_GetMask__String_Array>m__25D()
extern "C"  StringU5BU5D_t4054002952* UnityEngine_LayerMaskGenerated_U3CLayerMask_GetMask__String_ArrayU3Em__25D_m3556514209 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LayerMaskGenerated::ilo_getInt321(System.Int32)
extern "C"  int32_t UnityEngine_LayerMaskGenerated_ilo_getInt321_m2361737864 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LayerMaskGenerated::ilo_setStringS2(System.Int32,System.String)
extern "C"  void UnityEngine_LayerMaskGenerated_ilo_setStringS2_m337589003 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LayerMaskGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void UnityEngine_LayerMaskGenerated_ilo_setInt323_m4270663283 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LayerMaskGenerated::ilo_getObject4(System.Int32)
extern "C"  int32_t UnityEngine_LayerMaskGenerated_ilo_getObject4_m875931572 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_LayerMaskGenerated::ilo_getStringS5(System.Int32)
extern "C"  String_t* UnityEngine_LayerMaskGenerated_ilo_getStringS5_m1155071107 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
