﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<Core.RpsResult>
struct Comparer_1_t2731348111;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.Comparer`1<Core.RpsResult>::.ctor()
extern "C"  void Comparer_1__ctor_m4233864872_gshared (Comparer_1_t2731348111 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m4233864872(__this, method) ((  void (*) (Comparer_1_t2731348111 *, const MethodInfo*))Comparer_1__ctor_m4233864872_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<Core.RpsResult>::.cctor()
extern "C"  void Comparer_1__cctor_m1918695941_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m1918695941(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m1918695941_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<Core.RpsResult>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m4279975885_gshared (Comparer_1_t2731348111 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m4279975885(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t2731348111 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m4279975885_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Core.RpsResult>::get_Default()
extern "C"  Comparer_1_t2731348111 * Comparer_1_get_Default_m557246032_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m557246032(__this /* static, unused */, method) ((  Comparer_1_t2731348111 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m557246032_gshared)(__this /* static, unused */, method)
