﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_IO_DirectoryInfoGenerated
struct System_IO_DirectoryInfoGenerated_t2527648893;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void System_IO_DirectoryInfoGenerated::.ctor()
extern "C"  void System_IO_DirectoryInfoGenerated__ctor_m112252606 (System_IO_DirectoryInfoGenerated_t2527648893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryInfoGenerated::DirectoryInfo_DirectoryInfo1(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryInfoGenerated_DirectoryInfo_DirectoryInfo1_m2135314308 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_DirectoryInfoGenerated::DirectoryInfo_Exists(JSVCall)
extern "C"  void System_IO_DirectoryInfoGenerated_DirectoryInfo_Exists_m1054892449 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_DirectoryInfoGenerated::DirectoryInfo_Name(JSVCall)
extern "C"  void System_IO_DirectoryInfoGenerated_DirectoryInfo_Name_m722416146 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_DirectoryInfoGenerated::DirectoryInfo_Parent(JSVCall)
extern "C"  void System_IO_DirectoryInfoGenerated_DirectoryInfo_Parent_m1043481875 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_DirectoryInfoGenerated::DirectoryInfo_Root(JSVCall)
extern "C"  void System_IO_DirectoryInfoGenerated_DirectoryInfo_Root_m3404291483 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryInfoGenerated::DirectoryInfo_Create__DirectorySecurity(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryInfoGenerated_DirectoryInfo_Create__DirectorySecurity_m1165682789 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryInfoGenerated::DirectoryInfo_Create(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryInfoGenerated_DirectoryInfo_Create_m1679326762 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryInfoGenerated::DirectoryInfo_CreateSubdirectory__String__DirectorySecurity(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryInfoGenerated_DirectoryInfo_CreateSubdirectory__String__DirectorySecurity_m927289671 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryInfoGenerated::DirectoryInfo_CreateSubdirectory__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryInfoGenerated_DirectoryInfo_CreateSubdirectory__String_m918817928 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryInfoGenerated::DirectoryInfo_Delete__Boolean(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryInfoGenerated_DirectoryInfo_Delete__Boolean_m534672305 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryInfoGenerated::DirectoryInfo_Delete(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryInfoGenerated_DirectoryInfo_Delete_m2306745753 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryInfoGenerated::DirectoryInfo_GetAccessControl__AccessControlSections(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryInfoGenerated_DirectoryInfo_GetAccessControl__AccessControlSections_m747870776 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryInfoGenerated::DirectoryInfo_GetAccessControl(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryInfoGenerated_DirectoryInfo_GetAccessControl_m1779122513 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryInfoGenerated::DirectoryInfo_GetDirectories__String__SearchOption(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryInfoGenerated_DirectoryInfo_GetDirectories__String__SearchOption_m2230747281 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryInfoGenerated::DirectoryInfo_GetDirectories__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryInfoGenerated_DirectoryInfo_GetDirectories__String_m2935857492 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryInfoGenerated::DirectoryInfo_GetDirectories(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryInfoGenerated_DirectoryInfo_GetDirectories_m3247937027 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryInfoGenerated::DirectoryInfo_GetFiles__String__SearchOption(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryInfoGenerated_DirectoryInfo_GetFiles__String__SearchOption_m3327511165 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryInfoGenerated::DirectoryInfo_GetFiles__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryInfoGenerated_DirectoryInfo_GetFiles__String_m2810542144 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryInfoGenerated::DirectoryInfo_GetFiles(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryInfoGenerated_DirectoryInfo_GetFiles_m229316847 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryInfoGenerated::DirectoryInfo_GetFileSystemInfos__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryInfoGenerated_DirectoryInfo_GetFileSystemInfos__String_m2057548611 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryInfoGenerated::DirectoryInfo_GetFileSystemInfos(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryInfoGenerated_DirectoryInfo_GetFileSystemInfos_m2194278130 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryInfoGenerated::DirectoryInfo_MoveTo__String(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryInfoGenerated_DirectoryInfo_MoveTo__String_m2482779051 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryInfoGenerated::DirectoryInfo_SetAccessControl__DirectorySecurity(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryInfoGenerated_DirectoryInfo_SetAccessControl__DirectorySecurity_m422271466 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryInfoGenerated::DirectoryInfo_ToString(JSVCall,System.Int32)
extern "C"  bool System_IO_DirectoryInfoGenerated_DirectoryInfo_ToString_m3673925722 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_DirectoryInfoGenerated::__Register()
extern "C"  void System_IO_DirectoryInfoGenerated___Register_m2118069449 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_DirectoryInfoGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool System_IO_DirectoryInfoGenerated_ilo_attachFinalizerObject1_m3084503265 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_DirectoryInfoGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void System_IO_DirectoryInfoGenerated_ilo_addJSCSRel2_m3337374459 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_DirectoryInfoGenerated::ilo_setStringS3(System.Int32,System.String)
extern "C"  void System_IO_DirectoryInfoGenerated_ilo_setStringS3_m10808201 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System_IO_DirectoryInfoGenerated::ilo_getStringS4(System.Int32)
extern "C"  String_t* System_IO_DirectoryInfoGenerated_ilo_getStringS4_m2790930879 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_IO_DirectoryInfoGenerated::ilo_getEnum5(System.Int32)
extern "C"  int32_t System_IO_DirectoryInfoGenerated_ilo_getEnum5_m2006967830 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_IO_DirectoryInfoGenerated::ilo_setObject6(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_IO_DirectoryInfoGenerated_ilo_setObject6_m1048230353 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_DirectoryInfoGenerated::ilo_setArrayS7(System.Int32,System.Int32,System.Boolean)
extern "C"  void System_IO_DirectoryInfoGenerated_ilo_setArrayS7_m2004721291 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_DirectoryInfoGenerated::ilo_moveSaveID2Arr8(System.Int32)
extern "C"  void System_IO_DirectoryInfoGenerated_ilo_moveSaveID2Arr8_m252648218 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
