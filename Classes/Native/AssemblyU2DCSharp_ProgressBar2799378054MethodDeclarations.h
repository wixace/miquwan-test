﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProgressBar
struct ProgressBar_t2799378054;
// UIBasicSprite
struct UIBasicSprite_t2501337439;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIBasicSprite2501337439.h"
#include "AssemblyU2DCSharp_ProgressBar2799378054.h"

// System.Void ProgressBar::.ctor()
extern "C"  void ProgressBar__ctor_m2892190629 (ProgressBar_t2799378054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar::.cctor()
extern "C"  void ProgressBar__cctor_m3276467368 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar::Awake()
extern "C"  void ProgressBar_Awake_m3129795848 (ProgressBar_t2799378054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar::Update()
extern "C"  void ProgressBar_Update_m1190458376 (ProgressBar_t2799378054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar::SetPb(System.Single)
extern "C"  void ProgressBar_SetPb_m1144684884 (ProgressBar_t2799378054 * __this, float ___ratio0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar::Clear()
extern "C"  void ProgressBar_Clear_m298323920 (ProgressBar_t2799378054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar::Init(System.Boolean)
extern "C"  void ProgressBar_Init_m2849087814 (ProgressBar_t2799378054 * __this, bool ___isbreak0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar::Hide()
extern "C"  void ProgressBar_Hide_m2366727617 (ProgressBar_t2799378054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar::ilo_set_fillAmount1(UIBasicSprite,System.Single)
extern "C"  void ProgressBar_ilo_set_fillAmount1_m1475482479 (Il2CppObject * __this /* static, unused */, UIBasicSprite_t2501337439 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar::ilo_SetPb2(ProgressBar,System.Single)
extern "C"  void ProgressBar_ilo_SetPb2_m673250081 (Il2CppObject * __this /* static, unused */, ProgressBar_t2799378054 * ____this0, float ___ratio1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
