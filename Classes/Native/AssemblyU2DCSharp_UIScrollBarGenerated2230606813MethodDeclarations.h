﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIScrollBarGenerated
struct UIScrollBarGenerated_t2230606813;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// UIScrollBar
struct UIScrollBar_t2839103954;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_UIScrollBar2839103954.h"

// System.Void UIScrollBarGenerated::.ctor()
extern "C"  void UIScrollBarGenerated__ctor_m3813738334 (UIScrollBarGenerated_t2230606813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollBarGenerated::UIScrollBar_UIScrollBar1(JSVCall,System.Int32)
extern "C"  bool UIScrollBarGenerated_UIScrollBar_UIScrollBar1_m3034395318 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollBarGenerated::UIScrollBar_barSize(JSVCall)
extern "C"  void UIScrollBarGenerated_UIScrollBar_barSize_m2005086052 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollBarGenerated::UIScrollBar_ForceUpdate(JSVCall,System.Int32)
extern "C"  bool UIScrollBarGenerated_UIScrollBar_ForceUpdate_m4192545407 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollBarGenerated::__Register()
extern "C"  void UIScrollBarGenerated___Register_m2135187497 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIScrollBarGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UIScrollBarGenerated_ilo_getObject1_m3930294132 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIScrollBarGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UIScrollBarGenerated_ilo_attachFinalizerObject2_m1054704770 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollBarGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UIScrollBarGenerated_ilo_addJSCSRel3_m3177746460 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIScrollBarGenerated::ilo_get_barSize4(UIScrollBar)
extern "C"  float UIScrollBarGenerated_ilo_get_barSize4_m378366674 (Il2CppObject * __this /* static, unused */, UIScrollBar_t2839103954 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollBarGenerated::ilo_ForceUpdate5(UIScrollBar)
extern "C"  void UIScrollBarGenerated_ilo_ForceUpdate5_m948182438 (Il2CppObject * __this /* static, unused */, UIScrollBar_t2839103954 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
