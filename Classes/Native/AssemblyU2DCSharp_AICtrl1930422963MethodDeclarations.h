﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AICtrl
struct AICtrl_t1930422963;
// CombatEntity
struct CombatEntity_t684137495;
// System.String[]
struct StringU5BU5D_t4054002952;
// AIObject
struct AIObject_t37280135;
// npcAIConditionCfg
struct npcAIConditionCfg_t930197170;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// FightCtrl
struct FightCtrl_t648967803;
// BuffCtrl
struct BuffCtrl_t2836564350;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// CameraHelper
struct CameraHelper_t3196871507;
// NpcMgr
struct NpcMgr_t2339534743;
// HeroMgr
struct HeroMgr_t2475965342;
// HatredCtrl
struct HatredCtrl_t891253697;
// Hero
struct Hero_t2245658;
// GameMgr
struct GameMgr_t1469029542;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_AIEnum_EAIEventtype3896289311.h"
#include "AssemblyU2DCSharp_AIObject37280135.h"
#include "AssemblyU2DCSharp_npcAIConditionCfg930197170.h"
#include "AssemblyU2DCSharp_AICtrl1930422963.h"
#include "AssemblyU2DCSharp_FightEnum_EFightRst3627658870.h"
#include "AssemblyU2DCSharp_FightCtrl648967803.h"
#include "AssemblyU2DCSharp_FloatTextMgr630384591.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "AssemblyU2DCSharp_CameraHelper3196871507.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitCamp642829979.h"
#include "AssemblyU2DCSharp_NpcMgr2339534743.h"
#include "AssemblyU2DCSharp_HatredCtrl891253697.h"
#include "AssemblyU2DCSharp_HeroMgr2475965342.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"

// System.Void AICtrl::.ctor(CombatEntity)
extern "C"  void AICtrl__ctor_m388924305 (AICtrl_t1930422963 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICtrl::Init(System.String[])
extern "C"  void AICtrl_Init_m1216946708 (AICtrl_t1930422963 * __this, StringU5BU5D_t4054002952* ___aiIDs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICtrl::OnObservableRequest(AIEnum.EAIEventtype,System.Int32,System.Int32)
extern "C"  void AICtrl_OnObservableRequest_m3778746483 (AICtrl_t1930422963 * __this, uint8_t ___eType0, int32_t ___tParam11, int32_t ___tParam22, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AICtrl::OnObserverHandle(AIObject,AIEnum.EAIEventtype,System.Int32,System.Int32)
extern "C"  bool AICtrl_OnObserverHandle_m928957190 (AICtrl_t1930422963 * __this, AIObject_t37280135 * ___aiObj0, uint8_t ___eType1, int32_t ___tParam12, int32_t ___tParam23, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AICtrl::ProcessAIAction(AIObject,System.Int32,System.Int32)
extern "C"  bool AICtrl_ProcessAIAction_m3420045144 (AICtrl_t1930422963 * __this, AIObject_t37280135 * ___aiObj0, int32_t ___tParam11, int32_t ___tParam22, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AICtrl::ChangeAtkTarget(System.Int32,System.Int32,System.Int32)
extern "C"  bool AICtrl_ChangeAtkTarget_m335863644 (AICtrl_t1930422963 * __this, int32_t ___tParam10, int32_t ___tParam21, int32_t ___tParam32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AICtrl::CheckCondition(npcAIConditionCfg,System.Int32,System.Int32)
extern "C"  bool AICtrl_CheckCondition_m3500324893 (AICtrl_t1930422963 * __this, npcAIConditionCfg_t930197170 * ___condition0, int32_t ___tParam11, int32_t ___tParam22, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AICtrl::CheckRelation(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  bool AICtrl_CheckRelation_m3413046512 (AICtrl_t1930422963 * __this, int32_t ___nRelation0, int32_t ___nParam11, int32_t ___nParam22, int32_t ___nParam33, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AICtrl::ilo_get_isDeath1(CombatEntity)
extern "C"  bool AICtrl_ilo_get_isDeath1_m3646826396 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AICtrl::ilo_ProcessAIAction2(AICtrl,AIObject,System.Int32,System.Int32)
extern "C"  bool AICtrl_ilo_ProcessAIAction2_m2199283754 (Il2CppObject * __this /* static, unused */, AICtrl_t1930422963 * ____this0, AIObject_t37280135 * ___aiObj1, int32_t ___tParam12, int32_t ___tParam23, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSDatacfgManager AICtrl::ilo_get_CfgDataMgr3()
extern "C"  CSDatacfgManager_t1565254243 * AICtrl_ilo_get_CfgDataMgr3_m681114457 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightEnum.EFightRst AICtrl::ilo_SetNextSkillIDByAI4(FightCtrl,System.Int32)
extern "C"  int32_t AICtrl_ilo_SetNextSkillIDByAI4_m3716267593 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, int32_t ___skillid1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BuffCtrl AICtrl::ilo_get_buffCtrl5(CombatEntity)
extern "C"  BuffCtrl_t2836564350 * AICtrl_ilo_get_buffCtrl5_m764042955 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICtrl::ilo_FloatText6(FloatTextMgr,FLOAT_TEXT_ID,System.Int32,System.Object[])
extern "C"  void AICtrl_ilo_FloatText6_m3845019060 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, int32_t ___textId1, int32_t ___offsetY2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr AICtrl::ilo_get_FloatTextMgr7()
extern "C"  FloatTextMgr_t630384591 * AICtrl_ilo_get_FloatTextMgr7_m3515295940 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AICtrl::ilo_get_id8(CombatEntity)
extern "C"  int32_t AICtrl_ilo_get_id8_m4006378114 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICtrl::ilo_MoveCamera9(CameraHelper,System.Boolean,System.Int32,System.Boolean,System.Single,System.Int32)
extern "C"  void AICtrl_ilo_MoveCamera9_m3127745650 (Il2CppObject * __this /* static, unused */, CameraHelper_t3196871507 * ____this0, bool ___isHero1, int32_t ___id2, bool ___isMove3, float ___time4, int32_t ___AIid5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitCamp AICtrl::ilo_get_unitCamp10(CombatEntity)
extern "C"  int32_t AICtrl_ilo_get_unitCamp10_m4084965769 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity AICtrl::ilo_GetHurtMaxNpc11(NpcMgr)
extern "C"  CombatEntity_t684137495 * AICtrl_ilo_GetHurtMaxNpc11_m175433272 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity AICtrl::ilo_GetHPMinNpc12(NpcMgr)
extern "C"  CombatEntity_t684137495 * AICtrl_ilo_GetHPMinNpc12_m3398848100 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity AICtrl::ilo_GetNearestNpc13(NpcMgr,CombatEntity)
extern "C"  CombatEntity_t684137495 * AICtrl_ilo_GetNearestNpc13_m3567100446 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, CombatEntity_t684137495 * ___entity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NpcMgr AICtrl::ilo_get_instance14()
extern "C"  NpcMgr_t2339534743 * AICtrl_ilo_get_instance14_m572990422 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity AICtrl::ilo_GetNeutralNpcById15(NpcMgr,System.Int32)
extern "C"  CombatEntity_t684137495 * AICtrl_ilo_GetNeutralNpcById15_m2440956507 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr AICtrl::ilo_get_instance16()
extern "C"  HeroMgr_t2475965342 * AICtrl_ilo_get_instance16_m1566790343 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICtrl::ilo_AddHatred17(HatredCtrl,CombatEntity,System.Single)
extern "C"  void AICtrl_ilo_AddHatred17_m3886441317 (Il2CppObject * __this /* static, unused */, HatredCtrl_t891253697 * ____this0, CombatEntity_t684137495 * ___entity1, float ___hatred2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICtrl::ilo_ChangeAttackTarge18(HatredCtrl)
extern "C"  void AICtrl_ilo_ChangeAttackTarge18_m3174990116 (Il2CppObject * __this /* static, unused */, HatredCtrl_t891253697 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero AICtrl::ilo_GetHurtMaxHero19(HeroMgr)
extern "C"  Hero_t2245658 * AICtrl_ilo_GetHurtMaxHero19_m2330887361 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero AICtrl::ilo_GetNearestHero20(HeroMgr,CombatEntity)
extern "C"  Hero_t2245658 * AICtrl_ilo_GetNearestHero20_m1242657357 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, CombatEntity_t684137495 * ___entity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity AICtrl::ilo_GetMaxHatredEntity21(HatredCtrl)
extern "C"  CombatEntity_t684137495 * AICtrl_ilo_GetMaxHatredEntity21_m3944452282 (Il2CppObject * __this /* static, unused */, HatredCtrl_t891253697 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero AICtrl::ilo_GetHeroByID22(HeroMgr,System.Int32)
extern "C"  Hero_t2245658 * AICtrl_ilo_GetHeroByID22_m3942313813 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity AICtrl::ilo_GetAniShowNpc23(NpcMgr,System.Int32)
extern "C"  CombatEntity_t684137495 * AICtrl_ilo_GetAniShowNpc23_m3572925250 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero AICtrl::ilo_get_captain24(HeroMgr)
extern "C"  Hero_t2245658 * AICtrl_ilo_get_captain24_m788733511 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AICtrl::ilo_get_maxTime25(GameMgr)
extern "C"  int32_t AICtrl_ilo_get_maxTime25_m1972790012 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AICtrl::ilo_CheckRelation26(AICtrl,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  bool AICtrl_ilo_CheckRelation26_m3734641652 (Il2CppObject * __this /* static, unused */, AICtrl_t1930422963 * ____this0, int32_t ___nRelation1, int32_t ___nParam12, int32_t ___nParam23, int32_t ___nParam34, const MethodInfo* method) IL2CPP_METHOD_ATTR;
