﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginReYun/strutDict
struct strutDict_t4055560227;
struct strutDict_t4055560227_marshaled_pinvoke;
struct strutDict_t4055560227_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct strutDict_t4055560227;
struct strutDict_t4055560227_marshaled_pinvoke;

extern "C" void strutDict_t4055560227_marshal_pinvoke(const strutDict_t4055560227& unmarshaled, strutDict_t4055560227_marshaled_pinvoke& marshaled);
extern "C" void strutDict_t4055560227_marshal_pinvoke_back(const strutDict_t4055560227_marshaled_pinvoke& marshaled, strutDict_t4055560227& unmarshaled);
extern "C" void strutDict_t4055560227_marshal_pinvoke_cleanup(strutDict_t4055560227_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct strutDict_t4055560227;
struct strutDict_t4055560227_marshaled_com;

extern "C" void strutDict_t4055560227_marshal_com(const strutDict_t4055560227& unmarshaled, strutDict_t4055560227_marshaled_com& marshaled);
extern "C" void strutDict_t4055560227_marshal_com_back(const strutDict_t4055560227_marshaled_com& marshaled, strutDict_t4055560227& unmarshaled);
extern "C" void strutDict_t4055560227_marshal_com_cleanup(strutDict_t4055560227_marshaled_com& marshaled);
