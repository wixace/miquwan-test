﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.TimeSpanSerializer
struct TimeSpanSerializer_t2582022211;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "mscorlib_System_TimeSpan413522987.h"

// System.Void ProtoBuf.Serializers.TimeSpanSerializer::.ctor(ProtoBuf.Meta.TypeModel)
extern "C"  void TimeSpanSerializer__ctor_m1633953079 (TimeSpanSerializer_t2582022211 * __this, TypeModel_t2730011105 * ___model0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.TimeSpanSerializer::.cctor()
extern "C"  void TimeSpanSerializer__cctor_m3762493435 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.TimeSpanSerializer::ProtoBuf.Serializers.IProtoSerializer.get_RequiresOldValue()
extern "C"  bool TimeSpanSerializer_ProtoBuf_Serializers_IProtoSerializer_get_RequiresOldValue_m3748532396 (TimeSpanSerializer_t2582022211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.TimeSpanSerializer::ProtoBuf.Serializers.IProtoSerializer.get_ReturnsValue()
extern "C"  bool TimeSpanSerializer_ProtoBuf_Serializers_IProtoSerializer_get_ReturnsValue_m1405434178 (TimeSpanSerializer_t2582022211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.TimeSpanSerializer::get_ExpectedType()
extern "C"  Type_t * TimeSpanSerializer_get_ExpectedType_m4243815891 (TimeSpanSerializer_t2582022211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.TimeSpanSerializer::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * TimeSpanSerializer_Read_m791629885 (TimeSpanSerializer_t2582022211 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.TimeSpanSerializer::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void TimeSpanSerializer_Write_m635092713 (TimeSpanSerializer_t2582022211 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan ProtoBuf.Serializers.TimeSpanSerializer::ilo_ReadTimeSpan1(ProtoBuf.ProtoReader)
extern "C"  TimeSpan_t413522987  TimeSpanSerializer_ilo_ReadTimeSpan1_m380973014 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.TimeSpanSerializer::ilo_WriteTimeSpan2(System.TimeSpan,ProtoBuf.ProtoWriter)
extern "C"  void TimeSpanSerializer_ilo_WriteTimeSpan2_m558968499 (Il2CppObject * __this /* static, unused */, TimeSpan_t413522987  ___timeSpan0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
