﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_WheelFrictionCurveGenerated
struct UnityEngine_WheelFrictionCurveGenerated_t2060968397;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_WheelFrictionCurveGenerated::.ctor()
extern "C"  void UnityEngine_WheelFrictionCurveGenerated__ctor_m467166270 (UnityEngine_WheelFrictionCurveGenerated_t2060968397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelFrictionCurveGenerated::.cctor()
extern "C"  void UnityEngine_WheelFrictionCurveGenerated__cctor_m1115156271 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WheelFrictionCurveGenerated::WheelFrictionCurve_WheelFrictionCurve1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WheelFrictionCurveGenerated_WheelFrictionCurve_WheelFrictionCurve1_m1884369780 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelFrictionCurveGenerated::WheelFrictionCurve_extremumSlip(JSVCall)
extern "C"  void UnityEngine_WheelFrictionCurveGenerated_WheelFrictionCurve_extremumSlip_m1901509525 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelFrictionCurveGenerated::WheelFrictionCurve_extremumValue(JSVCall)
extern "C"  void UnityEngine_WheelFrictionCurveGenerated_WheelFrictionCurve_extremumValue_m2089911750 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelFrictionCurveGenerated::WheelFrictionCurve_asymptoteSlip(JSVCall)
extern "C"  void UnityEngine_WheelFrictionCurveGenerated_WheelFrictionCurve_asymptoteSlip_m756385104 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelFrictionCurveGenerated::WheelFrictionCurve_asymptoteValue(JSVCall)
extern "C"  void UnityEngine_WheelFrictionCurveGenerated_WheelFrictionCurve_asymptoteValue_m950793067 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelFrictionCurveGenerated::WheelFrictionCurve_stiffness(JSVCall)
extern "C"  void UnityEngine_WheelFrictionCurveGenerated_WheelFrictionCurve_stiffness_m3452041767 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelFrictionCurveGenerated::__Register()
extern "C"  void UnityEngine_WheelFrictionCurveGenerated___Register_m4281601865 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelFrictionCurveGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_WheelFrictionCurveGenerated_ilo_addJSCSRel1_m2930455546 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelFrictionCurveGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void UnityEngine_WheelFrictionCurveGenerated_ilo_setSingle2_m3631464439 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_WheelFrictionCurveGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_WheelFrictionCurveGenerated_ilo_getSingle3_m973584147 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelFrictionCurveGenerated::ilo_changeJSObj4(System.Int32,System.Object)
extern "C"  void UnityEngine_WheelFrictionCurveGenerated_ilo_changeJSObj4_m3461537134 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
