﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4168079610MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::.ctor()
#define Dictionary_2__ctor_m951824177(__this, method) ((  void (*) (Dictionary_2_t2813371203 *, const MethodInfo*))Dictionary_2__ctor_m1050910858_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m701672113(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t2813371203 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3610002771_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m292724894(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t2813371203 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m256662076_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::.ctor(System.Int32)
#define Dictionary_2__ctor_m166824843(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t2813371203 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3273912365_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m4292183583(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t2813371203 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1930793793_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m1691161467(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2813371203 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m1549788189_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1724894686(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2813371203 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1428264956_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m253641402(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2813371203 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2955279704_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m3209168632(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2813371203 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m4198131226_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.IDictionary.get_Values()
#define Dictionary_2_System_Collections_IDictionary_get_Values_m2592091622(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2813371203 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m3797372040_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.IDictionary.get_IsFixedSize()
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2346468643(__this, method) ((  bool (*) (Dictionary_2_t2813371203 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3627613121_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.IDictionary.get_IsReadOnly()
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m391154038(__this, method) ((  bool (*) (Dictionary_2_t2813371203 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1263765272_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3185626500(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2813371203 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2843055522_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1830446643(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2813371203 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m2020057553_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m3499852862(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2813371203 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m2894265824_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m2436005556(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2813371203 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2093434578_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m1003333233(__this, ___key0, method) ((  void (*) (Dictionary_2_t2813371203 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m127000079_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2566573344(__this, method) ((  bool (*) (Dictionary_2_t2813371203 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m4062325186_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1137292690(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2813371203 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4065571764_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m4279115108(__this, method) ((  bool (*) (Dictionary_2_t2813371203 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m840305542_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m105555591(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t2813371203 *, KeyValuePair_2_t2712151909 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2185230117_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2530952671(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2813371203 *, KeyValuePair_2_t2712151909 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3713378305_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2233705131(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2813371203 *, KeyValuePair_2U5BU5D_t946445672*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4220340169_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2649708740(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2813371203 *, KeyValuePair_2_t2712151909 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3330268006_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1917001034(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2813371203 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m323672040_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m4177132505(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2813371203 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m464503287_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2647517648(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2813371203 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1289662318_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2605324445(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2813371203 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3187662523_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::get_Count()
#define Dictionary_2_get_Count_m650267843(__this, method) ((  int32_t (*) (Dictionary_2_t2813371203 *, const MethodInfo*))Dictionary_2_get_Count_m655926012_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::get_Item(TKey)
#define Dictionary_2_get_Item_m28214227(__this, ___key0, method) ((  checkpointCfg_t2816107964 * (*) (Dictionary_2_t2813371203 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m2361773736_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m2310759802(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2813371203 *, int32_t, checkpointCfg_t2816107964 *, const MethodInfo*))Dictionary_2_set_Item_m1170572283_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m2345002162(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t2813371203 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3161627732_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m3372554565(__this, ___size0, method) ((  void (*) (Dictionary_2_t2813371203 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3089254883_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m2370558081(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2813371203 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3741359263_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m4228967189(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2712151909  (*) (Il2CppObject * /* static, unused */, int32_t, checkpointCfg_t2816107964 *, const MethodInfo*))Dictionary_2_make_pair_m2338171699_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m485913865(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, checkpointCfg_t2816107964 *, const MethodInfo*))Dictionary_2_pick_key_m1394751787_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m4061132837(__this /* static, unused */, ___key0, ___value1, method) ((  checkpointCfg_t2816107964 * (*) (Il2CppObject * /* static, unused */, int32_t, checkpointCfg_t2816107964 *, const MethodInfo*))Dictionary_2_pick_value_m1281047495_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m2273401902(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2813371203 *, KeyValuePair_2U5BU5D_t946445672*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2503627344_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::Resize()
#define Dictionary_2_Resize_m2127934014(__this, method) ((  void (*) (Dictionary_2_t2813371203 *, const MethodInfo*))Dictionary_2_Resize_m1861476060_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::Add(TKey,TValue)
#define Dictionary_2_Add_m453828437(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2813371203 *, int32_t, checkpointCfg_t2816107964 *, const MethodInfo*))Dictionary_2_Add_m2232043353_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::Clear()
#define Dictionary_2_Clear_m3707541861(__this, method) ((  void (*) (Dictionary_2_t2813371203 *, const MethodInfo*))Dictionary_2_Clear_m3560399111_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m3545540212(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2813371203 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2612169713_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m3840457551(__this, ___value0, method) ((  bool (*) (Dictionary_2_t2813371203 *, checkpointCfg_t2816107964 *, const MethodInfo*))Dictionary_2_ContainsValue_m454328177_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m422359000(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2813371203 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m3426598522_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m340490636(__this, ___sender0, method) ((  void (*) (Dictionary_2_t2813371203 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3983879210_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::Remove(TKey)
#define Dictionary_2_Remove_m3843394753(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2813371203 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m183515743_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m2707914792(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t2813371203 *, int32_t, checkpointCfg_t2816107964 **, const MethodInfo*))Dictionary_2_TryGetValue_m2256091851_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::get_Keys()
#define Dictionary_2_get_Keys_m2488770675(__this, method) ((  KeyCollection_t145163358 * (*) (Dictionary_2_t2813371203 *, const MethodInfo*))Dictionary_2_get_Keys_m4120714641_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::get_Values()
#define Dictionary_2_get_Values_m1179997903(__this, method) ((  ValueCollection_t1513976916 * (*) (Dictionary_2_t2813371203 *, const MethodInfo*))Dictionary_2_get_Values_m1815086189_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m4230740068(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t2813371203 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m844610694_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m2373446976(__this, ___value0, method) ((  checkpointCfg_t2816107964 * (*) (Dictionary_2_t2813371203 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m3888328930_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m1378890404(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t2813371203 *, KeyValuePair_2_t2712151909 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m139391042_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m3136799813(__this, method) ((  Enumerator_t4130694595  (*) (Dictionary_2_t2813371203 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3720989159_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,checkpointCfg>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m3129459580(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, checkpointCfg_t2816107964 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2937104030_gshared)(__this /* static, unused */, ___key0, ___value1, method)
