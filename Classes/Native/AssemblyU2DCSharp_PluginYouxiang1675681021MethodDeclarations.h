﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginYouxiang
struct PluginYouxiang_t1675681021;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// VersionInfo
struct VersionInfo_t2356638086;
// VersionMgr
struct VersionMgr_t1322950208;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// System.Object
struct Il2CppObject;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"

// System.Void PluginYouxiang::.ctor()
extern "C"  void PluginYouxiang__ctor_m3892395070 (PluginYouxiang_t1675681021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::Init()
extern "C"  void PluginYouxiang_Init_m2016760406 (PluginYouxiang_t1675681021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginYouxiang::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginYouxiang_ReqSDKHttpLogin_m1704882835 (PluginYouxiang_t1675681021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYouxiang::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginYouxiang_IsLoginSuccess_m2992177237 (PluginYouxiang_t1675681021 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::OpenUserLogin()
extern "C"  void PluginYouxiang_OpenUserLogin_m2203140240 (PluginYouxiang_t1675681021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::RolelvUpinfo(CEvent.ZEvent)
extern "C"  void PluginYouxiang_RolelvUpinfo_m359503980 (PluginYouxiang_t1675681021 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::UserPay(CEvent.ZEvent)
extern "C"  void PluginYouxiang_UserPay_m3738407138 (PluginYouxiang_t1675681021 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::OnInitSuccess(System.String)
extern "C"  void PluginYouxiang_OnInitSuccess_m3003973170 (PluginYouxiang_t1675681021 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::OnInitFail(System.String)
extern "C"  void PluginYouxiang_OnInitFail_m1342337103 (PluginYouxiang_t1675681021 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::OnLoginSuccess(System.String)
extern "C"  void PluginYouxiang_OnLoginSuccess_m3318567107 (PluginYouxiang_t1675681021 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::OnLoginFail(System.String)
extern "C"  void PluginYouxiang_OnLoginFail_m1587580766 (PluginYouxiang_t1675681021 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::OnPaySuccess(System.String)
extern "C"  void PluginYouxiang_OnPaySuccess_m2185693122 (PluginYouxiang_t1675681021 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::OnPayFail(System.String)
extern "C"  void PluginYouxiang_OnPayFail_m2471448767 (PluginYouxiang_t1675681021 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::OnExitGameSuccess(System.String)
extern "C"  void PluginYouxiang_OnExitGameSuccess_m2845391186 (PluginYouxiang_t1675681021 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::LogoutSuccess()
extern "C"  void PluginYouxiang_LogoutSuccess_m2154432309 (PluginYouxiang_t1675681021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::OnLogoutSuccess(System.String)
extern "C"  void PluginYouxiang_OnLogoutSuccess_m4094875180 (PluginYouxiang_t1675681021 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::OnLogoutFail(System.String)
extern "C"  void PluginYouxiang_OnLogoutFail_m1783245461 (PluginYouxiang_t1675681021 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::Init(System.String,System.String)
extern "C"  void PluginYouxiang_Init_m2366587816 (PluginYouxiang_t1675681021 * __this, String_t* ___gameId0, String_t* ___gameKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::Login()
extern "C"  void PluginYouxiang_Login_m784095877 (PluginYouxiang_t1675681021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::ChangeAccount()
extern "C"  void PluginYouxiang_ChangeAccount_m1908669305 (PluginYouxiang_t1675681021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::Pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYouxiang_Pay_m397457498 (PluginYouxiang_t1675681021 * __this, String_t* ___OrderId0, String_t* ___ServerId1, String_t* ___Amount2, String_t* ___ServerName3, String_t* ___RoleId4, String_t* ___RoleName5, String_t* ___RoleLv6, String_t* ___Ratio7, String_t* ___ProductId8, String_t* ___extra9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::<OnLogoutSuccess>m__47B()
extern "C"  void PluginYouxiang_U3COnLogoutSuccessU3Em__47B_m3356417678 (PluginYouxiang_t1675681021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginYouxiang_ilo_AddEventListener1_m798631158 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.PayInfo PluginYouxiang::ilo_Parse2(System.Object[])
extern "C"  PayInfo_t1775308120 * PluginYouxiang_ilo_Parse2_m2241845491 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___payOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginYouxiang::ilo_get_currentVS3(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginYouxiang_ilo_get_currentVS3_m1769242348 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginYouxiang::ilo_GetProductID4(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginYouxiang_ilo_GetProductID4_m1158118058 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::ilo_Log5(System.Object,System.Boolean)
extern "C"  void PluginYouxiang_ilo_Log5_m66077779 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginYouxiang::ilo_get_PluginsSdkMgr6()
extern "C"  PluginsSdkMgr_t3884624670 * PluginYouxiang_ilo_get_PluginsSdkMgr6_m2847746479 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::ilo_ReqSDKHttpLogin7(PluginsSdkMgr)
extern "C"  void PluginYouxiang_ilo_ReqSDKHttpLogin7_m39983497 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYouxiang::ilo_DispatchEvent8(CEvent.ZEvent)
extern "C"  void PluginYouxiang_ilo_DispatchEvent8_m870682474 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
