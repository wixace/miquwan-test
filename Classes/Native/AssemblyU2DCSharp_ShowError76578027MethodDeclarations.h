﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShowError
struct ShowError_t76578027;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ShowError::.ctor()
extern "C"  void ShowError__ctor_m1824583904 (ShowError_t76578027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ShowError::get_isShow()
extern "C"  bool ShowError_get_isShow_m3621765408 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowError::set_isShow(System.Boolean)
extern "C"  void ShowError_set_isShow_m169669183 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowError::Awake()
extern "C"  void ShowError_Awake_m2062189123 (ShowError_t76578027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowError::OnGUI()
extern "C"  void ShowError_OnGUI_m1319982554 (ShowError_t76578027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowError::Show(System.String)
extern "C"  void ShowError_Show_m1795976321 (ShowError_t76578027 * __this, String_t* ___log0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ShowError ShowError::get_showError()
extern "C"  ShowError_t76578027 * ShowError_get_showError_m560971976 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowError::AddShow(System.String)
extern "C"  void ShowError_AddShow_m598984198 (Il2CppObject * __this /* static, unused */, String_t* ___log0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
