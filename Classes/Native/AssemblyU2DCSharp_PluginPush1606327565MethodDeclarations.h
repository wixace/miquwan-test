﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginPush
struct PluginPush_t1606327565;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PushType1840642452.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "AssemblyU2DCSharp_PluginPush1606327565.h"

// System.Void PluginPush::.ctor()
extern "C"  void PluginPush__ctor_m2213042734 (PluginPush_t1606327565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPush::Init()
extern "C"  void PluginPush_Init_m1962587750 (PluginPush_t1606327565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPush::OnDestory()
extern "C"  void PluginPush_OnDestory_m3596385985 (PluginPush_t1606327565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPush::PushLogin(System.String)
extern "C"  void PluginPush_PushLogin_m2296630375 (PluginPush_t1606327565 * __this, String_t* ___account0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginPush::GetPushState(System.String)
extern "C"  bool PluginPush_GetPushState_m408626231 (PluginPush_t1606327565 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginPush::GetPushState(System.String,PushType)
extern "C"  bool PluginPush_GetPushState_m328873499 (PluginPush_t1606327565 * __this, String_t* ___account0, int32_t ___pushType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginPush::SettingPushState(System.String,PushType,System.Boolean)
extern "C"  bool PluginPush_SettingPushState_m906986364 (PluginPush_t1606327565 * __this, String_t* ___account0, int32_t ___pushType1, bool ___isLoopTimer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPush::RequiredInit()
extern "C"  void PluginPush_RequiredInit_m1821128165 (PluginPush_t1606327565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPush::StopPush()
extern "C"  void PluginPush_StopPush_m1528487442 (PluginPush_t1606327565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPush::ResumePush()
extern "C"  void PluginPush_ResumePush_m813592765 (PluginPush_t1606327565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPush::SetAliasAndTags(System.String,System.String)
extern "C"  void PluginPush_SetAliasAndTags_m2140204624 (PluginPush_t1606327565 * __this, String_t* ___uid0, String_t* ___channels1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPush::SendNotification(System.String,PushType,System.Int32,System.Single,System.String,System.String,System.Boolean,System.String)
extern "C"  void PluginPush_SendNotification_m42547870 (PluginPush_t1606327565 * __this, String_t* ___account0, int32_t ___pushType1, int32_t ___id2, float ___delay3, String_t* ___title4, String_t* ___message5, bool ___isLoopTimer6, String_t* ___integralTimes7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPush::NotificationMessage(System.String,System.String,System.Boolean)
extern "C"  void PluginPush_NotificationMessage_m2110561191 (Il2CppObject * __this /* static, unused */, String_t* ___message0, String_t* ___time1, bool ___isRepeatDay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPush::NotificationMessage(System.String,System.DateTime,System.Boolean)
extern "C"  void PluginPush_NotificationMessage_m3754116785 (Il2CppObject * __this /* static, unused */, String_t* ___message0, DateTime_t4283661327  ___newDate1, bool ___isRepeatDay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPush::OnApplicationPause(System.Boolean)
extern "C"  void PluginPush_OnApplicationPause_m135897106 (PluginPush_t1606327565 * __this, bool ___paused0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPush::CleanNotification()
extern "C"  void PluginPush_CleanNotification_m2585456544 (PluginPush_t1606327565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPush::ilo_CleanNotification1(PluginPush)
extern "C"  void PluginPush_ilo_CleanNotification1_m1335936697 (Il2CppObject * __this /* static, unused */, PluginPush_t1606327565 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPush::ilo_SetAliasAndTags2(PluginPush,System.String,System.String)
extern "C"  void PluginPush_ilo_SetAliasAndTags2_m1756607908 (Il2CppObject * __this /* static, unused */, PluginPush_t1606327565 * ____this0, String_t* ___uid1, String_t* ___channels2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginPush::ilo_FormatUIString3(System.Int32,System.Object[])
extern "C"  String_t* PluginPush_ilo_FormatUIString3_m3250951658 (Il2CppObject * __this /* static, unused */, int32_t ___key0, ObjectU5BU5D_t1108656482* ___datas1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPush::ilo_SendNotification4(PluginPush,System.String,PushType,System.Int32,System.Single,System.String,System.String,System.Boolean,System.String)
extern "C"  void PluginPush_ilo_SendNotification4_m170240462 (Il2CppObject * __this /* static, unused */, PluginPush_t1606327565 * ____this0, String_t* ___account1, int32_t ___pushType2, int32_t ___id3, float ___delay4, String_t* ___title5, String_t* ___message6, bool ___isLoopTimer7, String_t* ___integralTimes8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginPush::ilo_GetPushState5(PluginPush,System.String)
extern "C"  bool PluginPush_ilo_GetPushState5_m2239071048 (Il2CppObject * __this /* static, unused */, PluginPush_t1606327565 * ____this0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginPush::ilo_NotificationMessage6(System.String,System.DateTime,System.Boolean)
extern "C"  void PluginPush_ilo_NotificationMessage6_m4130976748 (Il2CppObject * __this /* static, unused */, String_t* ___message0, DateTime_t4283661327  ___newDate1, bool ___isRepeatDay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
