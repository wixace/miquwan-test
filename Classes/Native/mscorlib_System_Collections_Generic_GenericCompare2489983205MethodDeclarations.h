﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<System.UInt32>
struct GenericComparer_1_t2489983205;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.UInt32>::.ctor()
extern "C"  void GenericComparer_1__ctor_m3701909944_gshared (GenericComparer_1_t2489983205 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m3701909944(__this, method) ((  void (*) (GenericComparer_1_t2489983205 *, const MethodInfo*))GenericComparer_1__ctor_m3701909944_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.UInt32>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m937987007_gshared (GenericComparer_1_t2489983205 * __this, uint32_t ___x0, uint32_t ___y1, const MethodInfo* method);
#define GenericComparer_1_Compare_m937987007(__this, ___x0, ___y1, method) ((  int32_t (*) (GenericComparer_1_t2489983205 *, uint32_t, uint32_t, const MethodInfo*))GenericComparer_1_Compare_m937987007_gshared)(__this, ___x0, ___y1, method)
