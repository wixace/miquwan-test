﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProgressMgr
struct ProgressMgr_t2799388811;
// ProgressBar
struct ProgressBar_t2799378054;
// CombatEntity
struct CombatEntity_t684137495;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_ProgressBar2799378054.h"

// System.Void ProgressMgr::.ctor()
extern "C"  void ProgressMgr__ctor_m2120326976 (ProgressMgr_t2799388811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressMgr::Init()
extern "C"  void ProgressMgr_Init_m2929428244 (ProgressMgr_t2799388811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProgressBar ProgressMgr::AddProgressBar(CombatEntity,System.Boolean)
extern "C"  ProgressBar_t2799378054 * ProgressMgr_AddProgressBar_m2790707448 (ProgressMgr_t2799388811 * __this, CombatEntity_t684137495 * ___unit0, bool ___breakskill1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ProgressMgr::ilo_Instantiate1(UnityEngine.GameObject)
extern "C"  GameObject_t3674682005 * ProgressMgr_ilo_Instantiate1_m111845537 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressMgr::ilo_ChangeParent2(UnityEngine.GameObject,UnityEngine.GameObject,System.Boolean)
extern "C"  void ProgressMgr_ilo_ChangeParent2_m458140282 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, GameObject_t3674682005 * ___parent1, bool ___resetMat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressMgr::ilo_Init3(ProgressBar,System.Boolean)
extern "C"  void ProgressMgr_ilo_Init3_m3455070975 (Il2CppObject * __this /* static, unused */, ProgressBar_t2799378054 * ____this0, bool ___isbreak1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
