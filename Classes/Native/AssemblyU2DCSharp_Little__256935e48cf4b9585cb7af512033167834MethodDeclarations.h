﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._256935e48cf4b9585cb7af51dca841aa
struct _256935e48cf4b9585cb7af51dca841aa_t2033167834;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__256935e48cf4b9585cb7af512033167834.h"

// System.Void Little._256935e48cf4b9585cb7af51dca841aa::.ctor()
extern "C"  void _256935e48cf4b9585cb7af51dca841aa__ctor_m3480251795 (_256935e48cf4b9585cb7af51dca841aa_t2033167834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._256935e48cf4b9585cb7af51dca841aa::_256935e48cf4b9585cb7af51dca841aam2(System.Int32)
extern "C"  int32_t _256935e48cf4b9585cb7af51dca841aa__256935e48cf4b9585cb7af51dca841aam2_m143815513 (_256935e48cf4b9585cb7af51dca841aa_t2033167834 * __this, int32_t ____256935e48cf4b9585cb7af51dca841aaa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._256935e48cf4b9585cb7af51dca841aa::_256935e48cf4b9585cb7af51dca841aam(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _256935e48cf4b9585cb7af51dca841aa__256935e48cf4b9585cb7af51dca841aam_m3901729661 (_256935e48cf4b9585cb7af51dca841aa_t2033167834 * __this, int32_t ____256935e48cf4b9585cb7af51dca841aaa0, int32_t ____256935e48cf4b9585cb7af51dca841aa341, int32_t ____256935e48cf4b9585cb7af51dca841aac2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._256935e48cf4b9585cb7af51dca841aa::ilo__256935e48cf4b9585cb7af51dca841aam21(Little._256935e48cf4b9585cb7af51dca841aa,System.Int32)
extern "C"  int32_t _256935e48cf4b9585cb7af51dca841aa_ilo__256935e48cf4b9585cb7af51dca841aam21_m3036978401 (Il2CppObject * __this /* static, unused */, _256935e48cf4b9585cb7af51dca841aa_t2033167834 * ____this0, int32_t ____256935e48cf4b9585cb7af51dca841aaa1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
