﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_dirvormoSowristo202
struct M_dirvormoSowristo202_t3043720426;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_dirvormoSowristo202::.ctor()
extern "C"  void M_dirvormoSowristo202__ctor_m3786427833 (M_dirvormoSowristo202_t3043720426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_dirvormoSowristo202::M_cowstejasJemorpa0(System.String[],System.Int32)
extern "C"  void M_dirvormoSowristo202_M_cowstejasJemorpa0_m1086984873 (M_dirvormoSowristo202_t3043720426 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_dirvormoSowristo202::M_jisstehe1(System.String[],System.Int32)
extern "C"  void M_dirvormoSowristo202_M_jisstehe1_m511149696 (M_dirvormoSowristo202_t3043720426 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
