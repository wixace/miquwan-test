﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__Iterator1B
struct  U3CBeforeSelfU3Ec__Iterator1B_t742088068  : public Il2CppObject
{
public:
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__Iterator1B::<o>__0
	JToken_t3412245951 * ___U3CoU3E__0_0;
	// System.Int32 Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__Iterator1B::$PC
	int32_t ___U24PC_1;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__Iterator1B::$current
	JToken_t3412245951 * ___U24current_2;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<BeforeSelf>c__Iterator1B::<>f__this
	JToken_t3412245951 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3CoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CBeforeSelfU3Ec__Iterator1B_t742088068, ___U3CoU3E__0_0)); }
	inline JToken_t3412245951 * get_U3CoU3E__0_0() const { return ___U3CoU3E__0_0; }
	inline JToken_t3412245951 ** get_address_of_U3CoU3E__0_0() { return &___U3CoU3E__0_0; }
	inline void set_U3CoU3E__0_0(JToken_t3412245951 * value)
	{
		___U3CoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CoU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CBeforeSelfU3Ec__Iterator1B_t742088068, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CBeforeSelfU3Ec__Iterator1B_t742088068, ___U24current_2)); }
	inline JToken_t3412245951 * get_U24current_2() const { return ___U24current_2; }
	inline JToken_t3412245951 ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(JToken_t3412245951 * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CBeforeSelfU3Ec__Iterator1B_t742088068, ___U3CU3Ef__this_3)); }
	inline JToken_t3412245951 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline JToken_t3412245951 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(JToken_t3412245951 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
