﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_gacharCearsesere186
struct  M_gacharCearsesere186_t4099204981  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_gacharCearsesere186::_zeastouSasle
	float ____zeastouSasle_0;
	// System.Int32 GarbageiOS.M_gacharCearsesere186::_hocooCairmis
	int32_t ____hocooCairmis_1;
	// System.Int32 GarbageiOS.M_gacharCearsesere186::_kouceaPoustor
	int32_t ____kouceaPoustor_2;
	// System.Single GarbageiOS.M_gacharCearsesere186::_serrasooMisrur
	float ____serrasooMisrur_3;
	// System.Single GarbageiOS.M_gacharCearsesere186::_hamaca
	float ____hamaca_4;
	// System.Boolean GarbageiOS.M_gacharCearsesere186::_surlou
	bool ____surlou_5;
	// System.Single GarbageiOS.M_gacharCearsesere186::_chawvocho
	float ____chawvocho_6;
	// System.Boolean GarbageiOS.M_gacharCearsesere186::_hucouSeali
	bool ____hucouSeali_7;
	// System.Boolean GarbageiOS.M_gacharCearsesere186::_seader
	bool ____seader_8;
	// System.Int32 GarbageiOS.M_gacharCearsesere186::_rawcair
	int32_t ____rawcair_9;
	// System.String GarbageiOS.M_gacharCearsesere186::_cowsemsir
	String_t* ____cowsemsir_10;

public:
	inline static int32_t get_offset_of__zeastouSasle_0() { return static_cast<int32_t>(offsetof(M_gacharCearsesere186_t4099204981, ____zeastouSasle_0)); }
	inline float get__zeastouSasle_0() const { return ____zeastouSasle_0; }
	inline float* get_address_of__zeastouSasle_0() { return &____zeastouSasle_0; }
	inline void set__zeastouSasle_0(float value)
	{
		____zeastouSasle_0 = value;
	}

	inline static int32_t get_offset_of__hocooCairmis_1() { return static_cast<int32_t>(offsetof(M_gacharCearsesere186_t4099204981, ____hocooCairmis_1)); }
	inline int32_t get__hocooCairmis_1() const { return ____hocooCairmis_1; }
	inline int32_t* get_address_of__hocooCairmis_1() { return &____hocooCairmis_1; }
	inline void set__hocooCairmis_1(int32_t value)
	{
		____hocooCairmis_1 = value;
	}

	inline static int32_t get_offset_of__kouceaPoustor_2() { return static_cast<int32_t>(offsetof(M_gacharCearsesere186_t4099204981, ____kouceaPoustor_2)); }
	inline int32_t get__kouceaPoustor_2() const { return ____kouceaPoustor_2; }
	inline int32_t* get_address_of__kouceaPoustor_2() { return &____kouceaPoustor_2; }
	inline void set__kouceaPoustor_2(int32_t value)
	{
		____kouceaPoustor_2 = value;
	}

	inline static int32_t get_offset_of__serrasooMisrur_3() { return static_cast<int32_t>(offsetof(M_gacharCearsesere186_t4099204981, ____serrasooMisrur_3)); }
	inline float get__serrasooMisrur_3() const { return ____serrasooMisrur_3; }
	inline float* get_address_of__serrasooMisrur_3() { return &____serrasooMisrur_3; }
	inline void set__serrasooMisrur_3(float value)
	{
		____serrasooMisrur_3 = value;
	}

	inline static int32_t get_offset_of__hamaca_4() { return static_cast<int32_t>(offsetof(M_gacharCearsesere186_t4099204981, ____hamaca_4)); }
	inline float get__hamaca_4() const { return ____hamaca_4; }
	inline float* get_address_of__hamaca_4() { return &____hamaca_4; }
	inline void set__hamaca_4(float value)
	{
		____hamaca_4 = value;
	}

	inline static int32_t get_offset_of__surlou_5() { return static_cast<int32_t>(offsetof(M_gacharCearsesere186_t4099204981, ____surlou_5)); }
	inline bool get__surlou_5() const { return ____surlou_5; }
	inline bool* get_address_of__surlou_5() { return &____surlou_5; }
	inline void set__surlou_5(bool value)
	{
		____surlou_5 = value;
	}

	inline static int32_t get_offset_of__chawvocho_6() { return static_cast<int32_t>(offsetof(M_gacharCearsesere186_t4099204981, ____chawvocho_6)); }
	inline float get__chawvocho_6() const { return ____chawvocho_6; }
	inline float* get_address_of__chawvocho_6() { return &____chawvocho_6; }
	inline void set__chawvocho_6(float value)
	{
		____chawvocho_6 = value;
	}

	inline static int32_t get_offset_of__hucouSeali_7() { return static_cast<int32_t>(offsetof(M_gacharCearsesere186_t4099204981, ____hucouSeali_7)); }
	inline bool get__hucouSeali_7() const { return ____hucouSeali_7; }
	inline bool* get_address_of__hucouSeali_7() { return &____hucouSeali_7; }
	inline void set__hucouSeali_7(bool value)
	{
		____hucouSeali_7 = value;
	}

	inline static int32_t get_offset_of__seader_8() { return static_cast<int32_t>(offsetof(M_gacharCearsesere186_t4099204981, ____seader_8)); }
	inline bool get__seader_8() const { return ____seader_8; }
	inline bool* get_address_of__seader_8() { return &____seader_8; }
	inline void set__seader_8(bool value)
	{
		____seader_8 = value;
	}

	inline static int32_t get_offset_of__rawcair_9() { return static_cast<int32_t>(offsetof(M_gacharCearsesere186_t4099204981, ____rawcair_9)); }
	inline int32_t get__rawcair_9() const { return ____rawcair_9; }
	inline int32_t* get_address_of__rawcair_9() { return &____rawcair_9; }
	inline void set__rawcair_9(int32_t value)
	{
		____rawcair_9 = value;
	}

	inline static int32_t get_offset_of__cowsemsir_10() { return static_cast<int32_t>(offsetof(M_gacharCearsesere186_t4099204981, ____cowsemsir_10)); }
	inline String_t* get__cowsemsir_10() const { return ____cowsemsir_10; }
	inline String_t** get_address_of__cowsemsir_10() { return &____cowsemsir_10; }
	inline void set__cowsemsir_10(String_t* value)
	{
		____cowsemsir_10 = value;
		Il2CppCodeGenWriteBarrier(&____cowsemsir_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
