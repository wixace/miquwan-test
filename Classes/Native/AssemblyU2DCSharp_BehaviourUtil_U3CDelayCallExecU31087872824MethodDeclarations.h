﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Boolean>
struct U3CDelayCallExecU3Ec__Iterator3F_1_t1087872824;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Boolean>::.ctor()
extern "C"  void U3CDelayCallExecU3Ec__Iterator3F_1__ctor_m796173157_gshared (U3CDelayCallExecU3Ec__Iterator3F_1_t1087872824 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator3F_1__ctor_m796173157(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator3F_1_t1087872824 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator3F_1__ctor_m796173157_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Boolean>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator3F_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2323413527_gshared (U3CDelayCallExecU3Ec__Iterator3F_1_t1087872824 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator3F_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2323413527(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator3F_1_t1087872824 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator3F_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2323413527_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator3F_1_System_Collections_IEnumerator_get_Current_m477858731_gshared (U3CDelayCallExecU3Ec__Iterator3F_1_t1087872824 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator3F_1_System_Collections_IEnumerator_get_Current_m477858731(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator3F_1_t1087872824 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator3F_1_System_Collections_IEnumerator_get_Current_m477858731_gshared)(__this, method)
// System.Boolean BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Boolean>::MoveNext()
extern "C"  bool U3CDelayCallExecU3Ec__Iterator3F_1_MoveNext_m3407181399_gshared (U3CDelayCallExecU3Ec__Iterator3F_1_t1087872824 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator3F_1_MoveNext_m3407181399(__this, method) ((  bool (*) (U3CDelayCallExecU3Ec__Iterator3F_1_t1087872824 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator3F_1_MoveNext_m3407181399_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Boolean>::Dispose()
extern "C"  void U3CDelayCallExecU3Ec__Iterator3F_1_Dispose_m516508258_gshared (U3CDelayCallExecU3Ec__Iterator3F_1_t1087872824 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator3F_1_Dispose_m516508258(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator3F_1_t1087872824 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator3F_1_Dispose_m516508258_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Boolean>::Reset()
extern "C"  void U3CDelayCallExecU3Ec__Iterator3F_1_Reset_m2737573394_gshared (U3CDelayCallExecU3Ec__Iterator3F_1_t1087872824 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator3F_1_Reset_m2737573394(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator3F_1_t1087872824 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator3F_1_Reset_m2737573394_gshared)(__this, method)
