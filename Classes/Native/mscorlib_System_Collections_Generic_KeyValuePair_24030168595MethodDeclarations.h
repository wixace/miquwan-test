﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,HeroEmbattleCfg>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3694193378(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4030168595 *, int32_t, HeroEmbattleCfg_t4134124650 *, const MethodInfo*))KeyValuePair_2__ctor_m11197230_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,HeroEmbattleCfg>::get_Key()
#define KeyValuePair_2_get_Key_m2274458310(__this, method) ((  int32_t (*) (KeyValuePair_2_t4030168595 *, const MethodInfo*))KeyValuePair_2_get_Key_m494458106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,HeroEmbattleCfg>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2328603143(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4030168595 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m4229413435_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,HeroEmbattleCfg>::get_Value()
#define KeyValuePair_2_get_Value_m4180563974(__this, method) ((  HeroEmbattleCfg_t4134124650 * (*) (KeyValuePair_2_t4030168595 *, const MethodInfo*))KeyValuePair_2_get_Value_m1563175098_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,HeroEmbattleCfg>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2107941511(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4030168595 *, HeroEmbattleCfg_t4134124650 *, const MethodInfo*))KeyValuePair_2_set_Value_m1296398523_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,HeroEmbattleCfg>::ToString()
#define KeyValuePair_2_ToString_m637991035(__this, method) ((  String_t* (*) (KeyValuePair_2_t4030168595 *, const MethodInfo*))KeyValuePair_2_ToString_m491888647_gshared)(__this, method)
