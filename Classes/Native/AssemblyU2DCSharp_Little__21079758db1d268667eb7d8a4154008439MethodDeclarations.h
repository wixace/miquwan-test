﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._21079758db1d268667eb7d8a2d153f29
struct _21079758db1d268667eb7d8a2d153f29_t4154008439;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__21079758db1d268667eb7d8a4154008439.h"

// System.Void Little._21079758db1d268667eb7d8a2d153f29::.ctor()
extern "C"  void _21079758db1d268667eb7d8a2d153f29__ctor_m3638884502 (_21079758db1d268667eb7d8a2d153f29_t4154008439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._21079758db1d268667eb7d8a2d153f29::_21079758db1d268667eb7d8a2d153f29m2(System.Int32)
extern "C"  int32_t _21079758db1d268667eb7d8a2d153f29__21079758db1d268667eb7d8a2d153f29m2_m3740935737 (_21079758db1d268667eb7d8a2d153f29_t4154008439 * __this, int32_t ____21079758db1d268667eb7d8a2d153f29a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._21079758db1d268667eb7d8a2d153f29::_21079758db1d268667eb7d8a2d153f29m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _21079758db1d268667eb7d8a2d153f29__21079758db1d268667eb7d8a2d153f29m_m4261213341 (_21079758db1d268667eb7d8a2d153f29_t4154008439 * __this, int32_t ____21079758db1d268667eb7d8a2d153f29a0, int32_t ____21079758db1d268667eb7d8a2d153f29121, int32_t ____21079758db1d268667eb7d8a2d153f29c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._21079758db1d268667eb7d8a2d153f29::ilo__21079758db1d268667eb7d8a2d153f29m21(Little._21079758db1d268667eb7d8a2d153f29,System.Int32)
extern "C"  int32_t _21079758db1d268667eb7d8a2d153f29_ilo__21079758db1d268667eb7d8a2d153f29m21_m982261022 (Il2CppObject * __this /* static, unused */, _21079758db1d268667eb7d8a2d153f29_t4154008439 * ____this0, int32_t ____21079758db1d268667eb7d8a2d153f29a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
