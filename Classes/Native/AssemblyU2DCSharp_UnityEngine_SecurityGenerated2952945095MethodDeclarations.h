﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_SecurityGenerated
struct UnityEngine_SecurityGenerated_t2952945095;
// JSVCall
struct JSVCall_t3708497963;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_SecurityGenerated::.ctor()
extern "C"  void UnityEngine_SecurityGenerated__ctor_m2607334276 (UnityEngine_SecurityGenerated_t2952945095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SecurityGenerated::Security_Security1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SecurityGenerated_Security_Security1_m750355822 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SecurityGenerated::Security_LoadAndVerifyAssembly__Byte_Array__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SecurityGenerated_Security_LoadAndVerifyAssembly__Byte_Array__String_m3655772224 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SecurityGenerated::Security_LoadAndVerifyAssembly__Byte_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SecurityGenerated_Security_LoadAndVerifyAssembly__Byte_Array_m2352761071 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SecurityGenerated::Security_PrefetchSocketPolicy__String__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SecurityGenerated_Security_PrefetchSocketPolicy__String__Int32__Int32_m3725670482 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SecurityGenerated::Security_PrefetchSocketPolicy__String__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SecurityGenerated_Security_PrefetchSocketPolicy__String__Int32_m3827150142 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SecurityGenerated::__Register()
extern "C"  void UnityEngine_SecurityGenerated___Register_m1299860419 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine_SecurityGenerated::<Security_LoadAndVerifyAssembly__Byte_Array__String>m__2D1()
extern "C"  ByteU5BU5D_t4260760469* UnityEngine_SecurityGenerated_U3CSecurity_LoadAndVerifyAssembly__Byte_Array__StringU3Em__2D1_m1258307622 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine_SecurityGenerated::<Security_LoadAndVerifyAssembly__Byte_Array>m__2D2()
extern "C"  ByteU5BU5D_t4260760469* UnityEngine_SecurityGenerated_U3CSecurity_LoadAndVerifyAssembly__Byte_ArrayU3Em__2D2_m2285860568 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SecurityGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_SecurityGenerated_ilo_getObject1_m368409010 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SecurityGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_SecurityGenerated_ilo_setObject2_m442102635 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SecurityGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void UnityEngine_SecurityGenerated_ilo_setBooleanS3_m2145827379 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SecurityGenerated::ilo_getArrayLength4(System.Int32)
extern "C"  int32_t UnityEngine_SecurityGenerated_ilo_getArrayLength4_m102854329 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
