﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// JSMgr/CSCallbackField[]
struct CSCallbackFieldU5BU5D_t1052203688;
// JSMgr/CSCallbackProperty[]
struct CSCallbackPropertyU5BU5D_t1496546767;
// JSMgr/MethodCallBackInfo[]
struct MethodCallBackInfoU5BU5D_t1108309981;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSMgr/CallbackInfo
struct  CallbackInfo_t3768479923  : public Il2CppObject
{
public:
	// System.Type JSMgr/CallbackInfo::type
	Type_t * ___type_0;
	// JSMgr/CSCallbackField[] JSMgr/CallbackInfo::fields
	CSCallbackFieldU5BU5D_t1052203688* ___fields_1;
	// JSMgr/CSCallbackProperty[] JSMgr/CallbackInfo::properties
	CSCallbackPropertyU5BU5D_t1496546767* ___properties_2;
	// JSMgr/MethodCallBackInfo[] JSMgr/CallbackInfo::constructors
	MethodCallBackInfoU5BU5D_t1108309981* ___constructors_3;
	// JSMgr/MethodCallBackInfo[] JSMgr/CallbackInfo::methods
	MethodCallBackInfoU5BU5D_t1108309981* ___methods_4;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(CallbackInfo_t3768479923, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier(&___type_0, value);
	}

	inline static int32_t get_offset_of_fields_1() { return static_cast<int32_t>(offsetof(CallbackInfo_t3768479923, ___fields_1)); }
	inline CSCallbackFieldU5BU5D_t1052203688* get_fields_1() const { return ___fields_1; }
	inline CSCallbackFieldU5BU5D_t1052203688** get_address_of_fields_1() { return &___fields_1; }
	inline void set_fields_1(CSCallbackFieldU5BU5D_t1052203688* value)
	{
		___fields_1 = value;
		Il2CppCodeGenWriteBarrier(&___fields_1, value);
	}

	inline static int32_t get_offset_of_properties_2() { return static_cast<int32_t>(offsetof(CallbackInfo_t3768479923, ___properties_2)); }
	inline CSCallbackPropertyU5BU5D_t1496546767* get_properties_2() const { return ___properties_2; }
	inline CSCallbackPropertyU5BU5D_t1496546767** get_address_of_properties_2() { return &___properties_2; }
	inline void set_properties_2(CSCallbackPropertyU5BU5D_t1496546767* value)
	{
		___properties_2 = value;
		Il2CppCodeGenWriteBarrier(&___properties_2, value);
	}

	inline static int32_t get_offset_of_constructors_3() { return static_cast<int32_t>(offsetof(CallbackInfo_t3768479923, ___constructors_3)); }
	inline MethodCallBackInfoU5BU5D_t1108309981* get_constructors_3() const { return ___constructors_3; }
	inline MethodCallBackInfoU5BU5D_t1108309981** get_address_of_constructors_3() { return &___constructors_3; }
	inline void set_constructors_3(MethodCallBackInfoU5BU5D_t1108309981* value)
	{
		___constructors_3 = value;
		Il2CppCodeGenWriteBarrier(&___constructors_3, value);
	}

	inline static int32_t get_offset_of_methods_4() { return static_cast<int32_t>(offsetof(CallbackInfo_t3768479923, ___methods_4)); }
	inline MethodCallBackInfoU5BU5D_t1108309981* get_methods_4() const { return ___methods_4; }
	inline MethodCallBackInfoU5BU5D_t1108309981** get_address_of_methods_4() { return &___methods_4; }
	inline void set_methods_4(MethodCallBackInfoU5BU5D_t1108309981* value)
	{
		___methods_4 = value;
		Il2CppCodeGenWriteBarrier(&___methods_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
