﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Pathfinding.AstarProfiler/ProfilePoint>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m411389167(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1659289992 *, String_t*, ProfilePoint_t940090916 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Pathfinding.AstarProfiler/ProfilePoint>::get_Key()
#define KeyValuePair_2_get_Key_m1483266622(__this, method) ((  String_t* (*) (KeyValuePair_2_t1659289992 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Pathfinding.AstarProfiler/ProfilePoint>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3657135066(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1659289992 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Pathfinding.AstarProfiler/ProfilePoint>::get_Value()
#define KeyValuePair_2_get_Value_m1620326127(__this, method) ((  ProfilePoint_t940090916 * (*) (KeyValuePair_2_t1659289992 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Pathfinding.AstarProfiler/ProfilePoint>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3110423258(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1659289992 *, ProfilePoint_t940090916 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Pathfinding.AstarProfiler/ProfilePoint>::ToString()
#define KeyValuePair_2_ToString_m3406434440(__this, method) ((  String_t* (*) (KeyValuePair_2_t1659289992 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
