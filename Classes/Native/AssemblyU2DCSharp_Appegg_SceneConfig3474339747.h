﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Appegg/SceneConfig
struct  SceneConfig_t3474339747  : public Il2CppObject
{
public:
	// System.String Appegg/SceneConfig::WebScene
	String_t* ___WebScene_0;
	// System.String Appegg/SceneConfig::BuiltInScene
	String_t* ___BuiltInScene_1;
	// System.String Appegg/SceneConfig::HotfixScene
	String_t* ___HotfixScene_2;

public:
	inline static int32_t get_offset_of_WebScene_0() { return static_cast<int32_t>(offsetof(SceneConfig_t3474339747, ___WebScene_0)); }
	inline String_t* get_WebScene_0() const { return ___WebScene_0; }
	inline String_t** get_address_of_WebScene_0() { return &___WebScene_0; }
	inline void set_WebScene_0(String_t* value)
	{
		___WebScene_0 = value;
		Il2CppCodeGenWriteBarrier(&___WebScene_0, value);
	}

	inline static int32_t get_offset_of_BuiltInScene_1() { return static_cast<int32_t>(offsetof(SceneConfig_t3474339747, ___BuiltInScene_1)); }
	inline String_t* get_BuiltInScene_1() const { return ___BuiltInScene_1; }
	inline String_t** get_address_of_BuiltInScene_1() { return &___BuiltInScene_1; }
	inline void set_BuiltInScene_1(String_t* value)
	{
		___BuiltInScene_1 = value;
		Il2CppCodeGenWriteBarrier(&___BuiltInScene_1, value);
	}

	inline static int32_t get_offset_of_HotfixScene_2() { return static_cast<int32_t>(offsetof(SceneConfig_t3474339747, ___HotfixScene_2)); }
	inline String_t* get_HotfixScene_2() const { return ___HotfixScene_2; }
	inline String_t** get_address_of_HotfixScene_2() { return &___HotfixScene_2; }
	inline void set_HotfixScene_2(String_t* value)
	{
		___HotfixScene_2 = value;
		Il2CppCodeGenWriteBarrier(&___HotfixScene_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
