﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CSLevelDate
struct CSLevelDate_t1079849666;
// CSPlayerData
struct CSPlayerData_t1029357243;
// CSHeroData
struct CSHeroData_t3763839828;
// CSRobData
struct CSRobData_t815326335;
// CSStoryData
struct CSStoryData_t151547823;
// CSKruunuData
struct CSKruunuData_t1258986408;
// CSGuideData
struct CSGuideData_t659641910;
// CSBossData
struct CSBossData_t2214337863;
// CSShiliBossData
struct CSShiliBossData_t2808712824;
// CSFightforData
struct CSFightforData_t215334547;
// CSDailyTurntableData
struct CSDailyTurntableData_t3656781746;
// CSCampfightData
struct CSCampfightData_t179152873;
// CSProvingGroundData
struct CSProvingGroundData_t325892090;
// CSArenaInterServiceData
struct CSArenaInterServiceData_t1260466202;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSGameDataMgr
struct  CSGameDataMgr_t2623305516  : public Il2CppObject
{
public:
	// CSLevelDate CSGameDataMgr::levelData
	CSLevelDate_t1079849666 * ___levelData_0;
	// CSPlayerData CSGameDataMgr::playerData
	CSPlayerData_t1029357243 * ___playerData_1;
	// CSHeroData CSGameDataMgr::heroData
	CSHeroData_t3763839828 * ___heroData_2;
	// CSRobData CSGameDataMgr::robData
	CSRobData_t815326335 * ___robData_3;
	// CSStoryData CSGameDataMgr::storyData
	CSStoryData_t151547823 * ___storyData_4;
	// CSKruunuData CSGameDataMgr::kruunuData
	CSKruunuData_t1258986408 * ___kruunuData_5;
	// CSGuideData CSGameDataMgr::guideData
	CSGuideData_t659641910 * ___guideData_6;
	// CSBossData CSGameDataMgr::bossData
	CSBossData_t2214337863 * ___bossData_7;
	// CSShiliBossData CSGameDataMgr::shiliBossData
	CSShiliBossData_t2808712824 * ___shiliBossData_8;
	// CSFightforData CSGameDataMgr::fightforData
	CSFightforData_t215334547 * ___fightforData_9;
	// CSDailyTurntableData CSGameDataMgr::dailyData
	CSDailyTurntableData_t3656781746 * ___dailyData_10;
	// CSCampfightData CSGameDataMgr::campfightData
	CSCampfightData_t179152873 * ___campfightData_11;
	// CSProvingGroundData CSGameDataMgr::provingGroundData
	CSProvingGroundData_t325892090 * ___provingGroundData_12;
	// CSArenaInterServiceData CSGameDataMgr::arenaInterServiceData
	CSArenaInterServiceData_t1260466202 * ___arenaInterServiceData_13;

public:
	inline static int32_t get_offset_of_levelData_0() { return static_cast<int32_t>(offsetof(CSGameDataMgr_t2623305516, ___levelData_0)); }
	inline CSLevelDate_t1079849666 * get_levelData_0() const { return ___levelData_0; }
	inline CSLevelDate_t1079849666 ** get_address_of_levelData_0() { return &___levelData_0; }
	inline void set_levelData_0(CSLevelDate_t1079849666 * value)
	{
		___levelData_0 = value;
		Il2CppCodeGenWriteBarrier(&___levelData_0, value);
	}

	inline static int32_t get_offset_of_playerData_1() { return static_cast<int32_t>(offsetof(CSGameDataMgr_t2623305516, ___playerData_1)); }
	inline CSPlayerData_t1029357243 * get_playerData_1() const { return ___playerData_1; }
	inline CSPlayerData_t1029357243 ** get_address_of_playerData_1() { return &___playerData_1; }
	inline void set_playerData_1(CSPlayerData_t1029357243 * value)
	{
		___playerData_1 = value;
		Il2CppCodeGenWriteBarrier(&___playerData_1, value);
	}

	inline static int32_t get_offset_of_heroData_2() { return static_cast<int32_t>(offsetof(CSGameDataMgr_t2623305516, ___heroData_2)); }
	inline CSHeroData_t3763839828 * get_heroData_2() const { return ___heroData_2; }
	inline CSHeroData_t3763839828 ** get_address_of_heroData_2() { return &___heroData_2; }
	inline void set_heroData_2(CSHeroData_t3763839828 * value)
	{
		___heroData_2 = value;
		Il2CppCodeGenWriteBarrier(&___heroData_2, value);
	}

	inline static int32_t get_offset_of_robData_3() { return static_cast<int32_t>(offsetof(CSGameDataMgr_t2623305516, ___robData_3)); }
	inline CSRobData_t815326335 * get_robData_3() const { return ___robData_3; }
	inline CSRobData_t815326335 ** get_address_of_robData_3() { return &___robData_3; }
	inline void set_robData_3(CSRobData_t815326335 * value)
	{
		___robData_3 = value;
		Il2CppCodeGenWriteBarrier(&___robData_3, value);
	}

	inline static int32_t get_offset_of_storyData_4() { return static_cast<int32_t>(offsetof(CSGameDataMgr_t2623305516, ___storyData_4)); }
	inline CSStoryData_t151547823 * get_storyData_4() const { return ___storyData_4; }
	inline CSStoryData_t151547823 ** get_address_of_storyData_4() { return &___storyData_4; }
	inline void set_storyData_4(CSStoryData_t151547823 * value)
	{
		___storyData_4 = value;
		Il2CppCodeGenWriteBarrier(&___storyData_4, value);
	}

	inline static int32_t get_offset_of_kruunuData_5() { return static_cast<int32_t>(offsetof(CSGameDataMgr_t2623305516, ___kruunuData_5)); }
	inline CSKruunuData_t1258986408 * get_kruunuData_5() const { return ___kruunuData_5; }
	inline CSKruunuData_t1258986408 ** get_address_of_kruunuData_5() { return &___kruunuData_5; }
	inline void set_kruunuData_5(CSKruunuData_t1258986408 * value)
	{
		___kruunuData_5 = value;
		Il2CppCodeGenWriteBarrier(&___kruunuData_5, value);
	}

	inline static int32_t get_offset_of_guideData_6() { return static_cast<int32_t>(offsetof(CSGameDataMgr_t2623305516, ___guideData_6)); }
	inline CSGuideData_t659641910 * get_guideData_6() const { return ___guideData_6; }
	inline CSGuideData_t659641910 ** get_address_of_guideData_6() { return &___guideData_6; }
	inline void set_guideData_6(CSGuideData_t659641910 * value)
	{
		___guideData_6 = value;
		Il2CppCodeGenWriteBarrier(&___guideData_6, value);
	}

	inline static int32_t get_offset_of_bossData_7() { return static_cast<int32_t>(offsetof(CSGameDataMgr_t2623305516, ___bossData_7)); }
	inline CSBossData_t2214337863 * get_bossData_7() const { return ___bossData_7; }
	inline CSBossData_t2214337863 ** get_address_of_bossData_7() { return &___bossData_7; }
	inline void set_bossData_7(CSBossData_t2214337863 * value)
	{
		___bossData_7 = value;
		Il2CppCodeGenWriteBarrier(&___bossData_7, value);
	}

	inline static int32_t get_offset_of_shiliBossData_8() { return static_cast<int32_t>(offsetof(CSGameDataMgr_t2623305516, ___shiliBossData_8)); }
	inline CSShiliBossData_t2808712824 * get_shiliBossData_8() const { return ___shiliBossData_8; }
	inline CSShiliBossData_t2808712824 ** get_address_of_shiliBossData_8() { return &___shiliBossData_8; }
	inline void set_shiliBossData_8(CSShiliBossData_t2808712824 * value)
	{
		___shiliBossData_8 = value;
		Il2CppCodeGenWriteBarrier(&___shiliBossData_8, value);
	}

	inline static int32_t get_offset_of_fightforData_9() { return static_cast<int32_t>(offsetof(CSGameDataMgr_t2623305516, ___fightforData_9)); }
	inline CSFightforData_t215334547 * get_fightforData_9() const { return ___fightforData_9; }
	inline CSFightforData_t215334547 ** get_address_of_fightforData_9() { return &___fightforData_9; }
	inline void set_fightforData_9(CSFightforData_t215334547 * value)
	{
		___fightforData_9 = value;
		Il2CppCodeGenWriteBarrier(&___fightforData_9, value);
	}

	inline static int32_t get_offset_of_dailyData_10() { return static_cast<int32_t>(offsetof(CSGameDataMgr_t2623305516, ___dailyData_10)); }
	inline CSDailyTurntableData_t3656781746 * get_dailyData_10() const { return ___dailyData_10; }
	inline CSDailyTurntableData_t3656781746 ** get_address_of_dailyData_10() { return &___dailyData_10; }
	inline void set_dailyData_10(CSDailyTurntableData_t3656781746 * value)
	{
		___dailyData_10 = value;
		Il2CppCodeGenWriteBarrier(&___dailyData_10, value);
	}

	inline static int32_t get_offset_of_campfightData_11() { return static_cast<int32_t>(offsetof(CSGameDataMgr_t2623305516, ___campfightData_11)); }
	inline CSCampfightData_t179152873 * get_campfightData_11() const { return ___campfightData_11; }
	inline CSCampfightData_t179152873 ** get_address_of_campfightData_11() { return &___campfightData_11; }
	inline void set_campfightData_11(CSCampfightData_t179152873 * value)
	{
		___campfightData_11 = value;
		Il2CppCodeGenWriteBarrier(&___campfightData_11, value);
	}

	inline static int32_t get_offset_of_provingGroundData_12() { return static_cast<int32_t>(offsetof(CSGameDataMgr_t2623305516, ___provingGroundData_12)); }
	inline CSProvingGroundData_t325892090 * get_provingGroundData_12() const { return ___provingGroundData_12; }
	inline CSProvingGroundData_t325892090 ** get_address_of_provingGroundData_12() { return &___provingGroundData_12; }
	inline void set_provingGroundData_12(CSProvingGroundData_t325892090 * value)
	{
		___provingGroundData_12 = value;
		Il2CppCodeGenWriteBarrier(&___provingGroundData_12, value);
	}

	inline static int32_t get_offset_of_arenaInterServiceData_13() { return static_cast<int32_t>(offsetof(CSGameDataMgr_t2623305516, ___arenaInterServiceData_13)); }
	inline CSArenaInterServiceData_t1260466202 * get_arenaInterServiceData_13() const { return ___arenaInterServiceData_13; }
	inline CSArenaInterServiceData_t1260466202 ** get_address_of_arenaInterServiceData_13() { return &___arenaInterServiceData_13; }
	inline void set_arenaInterServiceData_13(CSArenaInterServiceData_t1260466202 * value)
	{
		___arenaInterServiceData_13 = value;
		Il2CppCodeGenWriteBarrier(&___arenaInterServiceData_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
