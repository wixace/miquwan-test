﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_salcu233
struct M_salcu233_t251942034;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_salcu233251942034.h"

// System.Void GarbageiOS.M_salcu233::.ctor()
extern "C"  void M_salcu233__ctor_m3647772129 (M_salcu233_t251942034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_salcu233::M_calldemfi0(System.String[],System.Int32)
extern "C"  void M_salcu233_M_calldemfi0_m3521208683 (M_salcu233_t251942034 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_salcu233::M_drouwawxaRokoma1(System.String[],System.Int32)
extern "C"  void M_salcu233_M_drouwawxaRokoma1_m107339686 (M_salcu233_t251942034 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_salcu233::ilo_M_calldemfi01(GarbageiOS.M_salcu233,System.String[],System.Int32)
extern "C"  void M_salcu233_ilo_M_calldemfi01_m2085531681 (Il2CppObject * __this /* static, unused */, M_salcu233_t251942034 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
