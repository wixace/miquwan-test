﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEffectCfg
struct UIEffectCfg_t952653023;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void UIEffectCfg::.ctor()
extern "C"  void UIEffectCfg__ctor_m1618686316 (UIEffectCfg_t952653023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffectCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void UIEffectCfg_Init_m2639695513 (UIEffectCfg_t952653023 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
