﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReadFileTool
struct ReadFileTool_t2276708618;
// UnityEngine.AssetBundle
struct AssetBundle_t2070959688;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;
// ConfigAssetMgr
struct ConfigAssetMgr_t4036193930;
// System.Text.Encoding
struct Encoding_t2012439129;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AssetBundle2070959688.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ConfigAssetMgr4036193930.h"
#include "mscorlib_System_Text_Encoding2012439129.h"

// System.Void ReadFileTool::.ctor()
extern "C"  void ReadFileTool__ctor_m2041425105 (ReadFileTool_t2276708618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReadFileTool::.cctor()
extern "C"  void ReadFileTool__cctor_m2672539900 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReadFileTool::SetData(UnityEngine.AssetBundle)
extern "C"  void ReadFileTool_SetData_m1267800796 (Il2CppObject * __this /* static, unused */, AssetBundle_t2070959688 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ReadFileTool::GetBytesAsset(System.String)
extern "C"  ByteU5BU5D_t4260760469* ReadFileTool_GetBytesAsset_m1855411010 (Il2CppObject * __this /* static, unused */, String_t* ___txtAddress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReadFileTool::GetTextAsset(System.String)
extern "C"  String_t* ReadFileTool_GetTextAsset_m842604575 (Il2CppObject * __this /* static, unused */, String_t* ___txtAddress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReadFileTool::ilo_LoadPathToAssetName1(ConfigAssetMgr,System.String)
extern "C"  String_t* ReadFileTool_ilo_LoadPathToAssetName1_m4024982199 (Il2CppObject * __this /* static, unused */, ConfigAssetMgr_t4036193930 * ____this0, String_t* ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReadFileTool::ilo_BytesToString2(System.Byte[],System.Text.Encoding)
extern "C"  String_t* ReadFileTool_ilo_BytesToString2_m2476462764 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, Encoding_t2012439129 * ___encoding1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
