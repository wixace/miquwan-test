﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._3c16b623275514a9e2d4f5f99c22e0ee
struct _3c16b623275514a9e2d4f5f99c22e0ee_t2368083223;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._3c16b623275514a9e2d4f5f99c22e0ee::.ctor()
extern "C"  void _3c16b623275514a9e2d4f5f99c22e0ee__ctor_m2453527286 (_3c16b623275514a9e2d4f5f99c22e0ee_t2368083223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._3c16b623275514a9e2d4f5f99c22e0ee::_3c16b623275514a9e2d4f5f99c22e0eem2(System.Int32)
extern "C"  int32_t _3c16b623275514a9e2d4f5f99c22e0ee__3c16b623275514a9e2d4f5f99c22e0eem2_m2019017273 (_3c16b623275514a9e2d4f5f99c22e0ee_t2368083223 * __this, int32_t ____3c16b623275514a9e2d4f5f99c22e0eea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._3c16b623275514a9e2d4f5f99c22e0ee::_3c16b623275514a9e2d4f5f99c22e0eem(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _3c16b623275514a9e2d4f5f99c22e0ee__3c16b623275514a9e2d4f5f99c22e0eem_m635885725 (_3c16b623275514a9e2d4f5f99c22e0ee_t2368083223 * __this, int32_t ____3c16b623275514a9e2d4f5f99c22e0eea0, int32_t ____3c16b623275514a9e2d4f5f99c22e0ee301, int32_t ____3c16b623275514a9e2d4f5f99c22e0eec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
