﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FingerGestures/FingerList/FingerPropertyGetterDelegate`1<System.Object>
struct FingerPropertyGetterDelegate_1_t1483578891;
// System.Object
struct Il2CppObject;
// FingerGestures/Finger
struct Finger_t182428197;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_FingerGestures_Finger182428197.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void FingerGestures/FingerList/FingerPropertyGetterDelegate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void FingerPropertyGetterDelegate_1__ctor_m834827784_gshared (FingerPropertyGetterDelegate_1_t1483578891 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define FingerPropertyGetterDelegate_1__ctor_m834827784(__this, ___object0, ___method1, method) ((  void (*) (FingerPropertyGetterDelegate_1_t1483578891 *, Il2CppObject *, IntPtr_t, const MethodInfo*))FingerPropertyGetterDelegate_1__ctor_m834827784_gshared)(__this, ___object0, ___method1, method)
// T FingerGestures/FingerList/FingerPropertyGetterDelegate`1<System.Object>::Invoke(FingerGestures/Finger)
extern "C"  Il2CppObject * FingerPropertyGetterDelegate_1_Invoke_m305672266_gshared (FingerPropertyGetterDelegate_1_t1483578891 * __this, Finger_t182428197 * ___finger0, const MethodInfo* method);
#define FingerPropertyGetterDelegate_1_Invoke_m305672266(__this, ___finger0, method) ((  Il2CppObject * (*) (FingerPropertyGetterDelegate_1_t1483578891 *, Finger_t182428197 *, const MethodInfo*))FingerPropertyGetterDelegate_1_Invoke_m305672266_gshared)(__this, ___finger0, method)
// System.IAsyncResult FingerGestures/FingerList/FingerPropertyGetterDelegate`1<System.Object>::BeginInvoke(FingerGestures/Finger,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FingerPropertyGetterDelegate_1_BeginInvoke_m3452851682_gshared (FingerPropertyGetterDelegate_1_t1483578891 * __this, Finger_t182428197 * ___finger0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define FingerPropertyGetterDelegate_1_BeginInvoke_m3452851682(__this, ___finger0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (FingerPropertyGetterDelegate_1_t1483578891 *, Finger_t182428197 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))FingerPropertyGetterDelegate_1_BeginInvoke_m3452851682_gshared)(__this, ___finger0, ___callback1, ___object2, method)
// T FingerGestures/FingerList/FingerPropertyGetterDelegate`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * FingerPropertyGetterDelegate_1_EndInvoke_m2245236665_gshared (FingerPropertyGetterDelegate_1_t1483578891 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define FingerPropertyGetterDelegate_1_EndInvoke_m2245236665(__this, ___result0, method) ((  Il2CppObject * (*) (FingerPropertyGetterDelegate_1_t1483578891 *, Il2CppObject *, const MethodInfo*))FingerPropertyGetterDelegate_1_EndInvoke_m2245236665_gshared)(__this, ___result0, method)
