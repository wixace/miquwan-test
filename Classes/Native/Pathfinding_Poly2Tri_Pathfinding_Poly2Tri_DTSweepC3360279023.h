﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangula137695394.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Poly2Tri.DTSweepConstraint
struct  DTSweepConstraint_t3360279023  : public TriangulationConstraint_t137695394
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
