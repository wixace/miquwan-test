﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

// System.Void Il2cppLink::Link()
extern "C"  void Il2cppLink_Link_m2436019921 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Il2cppLink::Link(System.Boolean)
extern "C"  void Il2cppLink_Link_m3989088904 (Il2CppObject * __this /* static, unused */, bool ___b10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
