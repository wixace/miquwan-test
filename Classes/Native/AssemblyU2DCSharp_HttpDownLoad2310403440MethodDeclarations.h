﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HttpDownLoad
struct HttpDownLoad_t2310403440;
// System.String
struct String_t;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "System_Core_System_Action3771233898.h"

// System.Void HttpDownLoad::.ctor()
extern "C"  void HttpDownLoad__ctor_m1125587755 (HttpDownLoad_t2310403440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HttpDownLoad::get_progress()
extern "C"  float HttpDownLoad_get_progress_m2338983643 (HttpDownLoad_t2310403440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HttpDownLoad::set_progress(System.Single)
extern "C"  void HttpDownLoad_set_progress_m1749630728 (HttpDownLoad_t2310403440 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HttpDownLoad::get_isDone()
extern "C"  bool HttpDownLoad_get_isDone_m595720162 (HttpDownLoad_t2310403440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HttpDownLoad::set_isDone(System.Boolean)
extern "C"  void HttpDownLoad_set_isDone_m2118513817 (HttpDownLoad_t2310403440 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HttpDownLoad::DownLoad(System.String,System.String,System.Action)
extern "C"  void HttpDownLoad_DownLoad_m1113501620 (HttpDownLoad_t2310403440 * __this, String_t* ___url0, String_t* ___saveFullPath1, Action_t3771233898 * ___callBack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 HttpDownLoad::GetLength(System.String)
extern "C"  int64_t HttpDownLoad_GetLength_m1470446156 (HttpDownLoad_t2310403440 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HttpDownLoad::Close()
extern "C"  void HttpDownLoad_Close_m2836447297 (HttpDownLoad_t2310403440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
