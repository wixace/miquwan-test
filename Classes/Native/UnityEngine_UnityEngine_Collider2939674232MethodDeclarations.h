﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Collider
struct Collider_t2939674232;
// UnityEngine.Rigidbody
struct Rigidbody_t3346577219;
// UnityEngine.PhysicMaterial
struct PhysicMaterial_t211873335;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_PhysicMaterial211873335.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"

// System.Void UnityEngine.Collider::.ctor()
extern "C"  void Collider__ctor_m2392163321 (Collider_t2939674232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Collider::get_enabled()
extern "C"  bool Collider_get_enabled_m4167357801 (Collider_t2939674232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider::set_enabled(System.Boolean)
extern "C"  void Collider_set_enabled_m2575670866 (Collider_t2939674232 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
extern "C"  Rigidbody_t3346577219 * Collider_get_attachedRigidbody_m2821754842 (Collider_t2939674232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Collider::get_isTrigger()
extern "C"  bool Collider_get_isTrigger_m3877528150 (Collider_t2939674232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider::set_isTrigger(System.Boolean)
extern "C"  void Collider_set_isTrigger_m2057864191 (Collider_t2939674232 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Collider::get_contactOffset()
extern "C"  float Collider_get_contactOffset_m3898413013 (Collider_t2939674232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider::set_contactOffset(System.Single)
extern "C"  void Collider_set_contactOffset_m2688772190 (Collider_t2939674232 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.PhysicMaterial UnityEngine.Collider::get_material()
extern "C"  PhysicMaterial_t211873335 * Collider_get_material_m2158909696 (Collider_t2939674232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider::set_material(UnityEngine.PhysicMaterial)
extern "C"  void Collider_set_material_m564942661 (Collider_t2939674232 * __this, PhysicMaterial_t211873335 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Collider::ClosestPointOnBounds(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Collider_ClosestPointOnBounds_m2926847401 (Collider_t2939674232 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider::INTERNAL_CALL_ClosestPointOnBounds(UnityEngine.Collider,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Collider_INTERNAL_CALL_ClosestPointOnBounds_m861289994 (Il2CppObject * __this /* static, unused */, Collider_t2939674232 * ___self0, Vector3_t4282066566 * ___position1, Vector3_t4282066566 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.PhysicMaterial UnityEngine.Collider::get_sharedMaterial()
extern "C"  PhysicMaterial_t211873335 * Collider_get_sharedMaterial_m747017541 (Collider_t2939674232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider::set_sharedMaterial(UnityEngine.PhysicMaterial)
extern "C"  void Collider_set_sharedMaterial_m4263434250 (Collider_t2939674232 * __this, PhysicMaterial_t211873335 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UnityEngine.Collider::get_bounds()
extern "C"  Bounds_t2711641849  Collider_get_bounds_m1050008332 (Collider_t2939674232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider::INTERNAL_get_bounds(UnityEngine.Bounds&)
extern "C"  void Collider_INTERNAL_get_bounds_m1269265569 (Collider_t2939674232 * __this, Bounds_t2711641849 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Collider::Internal_Raycast(UnityEngine.Collider,UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Collider_Internal_Raycast_m2952058962 (Il2CppObject * __this /* static, unused */, Collider_t2939674232 * ___col0, Ray_t3134616544  ___ray1, RaycastHit_t4003175726 * ___hitInfo2, float ___maxDistance3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Collider::INTERNAL_CALL_Internal_Raycast(UnityEngine.Collider,UnityEngine.Ray&,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Collider_INTERNAL_CALL_Internal_Raycast_m2856018931 (Il2CppObject * __this /* static, unused */, Collider_t2939674232 * ___col0, Ray_t3134616544 * ___ray1, RaycastHit_t4003175726 * ___hitInfo2, float ___maxDistance3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Collider::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Collider_Raycast_m3774073393 (Collider_t2939674232 * __this, Ray_t3134616544  ___ray0, RaycastHit_t4003175726 * ___hitInfo1, float ___maxDistance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
