﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Int2>
struct DefaultComparer_t421822260;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Int2>::.ctor()
extern "C"  void DefaultComparer__ctor_m3960064505_gshared (DefaultComparer_t421822260 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3960064505(__this, method) ((  void (*) (DefaultComparer_t421822260 *, const MethodInfo*))DefaultComparer__ctor_m3960064505_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Int2>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3399561946_gshared (DefaultComparer_t421822260 * __this, Int2_t1974045593  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3399561946(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t421822260 *, Int2_t1974045593 , const MethodInfo*))DefaultComparer_GetHashCode_m3399561946_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Int2>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2712916366_gshared (DefaultComparer_t421822260 * __this, Int2_t1974045593  ___x0, Int2_t1974045593  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2712916366(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t421822260 *, Int2_t1974045593 , Int2_t1974045593 , const MethodInfo*))DefaultComparer_Equals_m2712916366_gshared)(__this, ___x0, ___y1, method)
