﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._8db4130f9c5a437125a1d1985dbc0940
struct _8db4130f9c5a437125a1d1985dbc0940_t1214163282;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._8db4130f9c5a437125a1d1985dbc0940::.ctor()
extern "C"  void _8db4130f9c5a437125a1d1985dbc0940__ctor_m3092830491 (_8db4130f9c5a437125a1d1985dbc0940_t1214163282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8db4130f9c5a437125a1d1985dbc0940::_8db4130f9c5a437125a1d1985dbc0940m2(System.Int32)
extern "C"  int32_t _8db4130f9c5a437125a1d1985dbc0940__8db4130f9c5a437125a1d1985dbc0940m2_m2197880921 (_8db4130f9c5a437125a1d1985dbc0940_t1214163282 * __this, int32_t ____8db4130f9c5a437125a1d1985dbc0940a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8db4130f9c5a437125a1d1985dbc0940::_8db4130f9c5a437125a1d1985dbc0940m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _8db4130f9c5a437125a1d1985dbc0940__8db4130f9c5a437125a1d1985dbc0940m_m413777533 (_8db4130f9c5a437125a1d1985dbc0940_t1214163282 * __this, int32_t ____8db4130f9c5a437125a1d1985dbc0940a0, int32_t ____8db4130f9c5a437125a1d1985dbc094031, int32_t ____8db4130f9c5a437125a1d1985dbc0940c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
