﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Events_UnityEvent77Generated/<UnityEventA1_RemoveListener__UnityActionT1_T0>c__AnonStoreyE4
struct U3CUnityEventA1_RemoveListener__UnityActionT1_T0U3Ec__AnonStoreyE4_t2110053844;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_Events_UnityEvent77Generated/<UnityEventA1_RemoveListener__UnityActionT1_T0>c__AnonStoreyE4::.ctor()
extern "C"  void U3CUnityEventA1_RemoveListener__UnityActionT1_T0U3Ec__AnonStoreyE4__ctor_m3323838487 (U3CUnityEventA1_RemoveListener__UnityActionT1_T0U3Ec__AnonStoreyE4_t2110053844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_Events_UnityEvent77Generated/<UnityEventA1_RemoveListener__UnityActionT1_T0>c__AnonStoreyE4::<>m__1AF()
extern "C"  Il2CppObject * U3CUnityEventA1_RemoveListener__UnityActionT1_T0U3Ec__AnonStoreyE4_U3CU3Em__1AF_m4154822787 (U3CUnityEventA1_RemoveListener__UnityActionT1_T0U3Ec__AnonStoreyE4_t2110053844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
