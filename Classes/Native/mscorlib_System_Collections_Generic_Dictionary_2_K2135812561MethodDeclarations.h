﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>
struct KeyCollection_t2135812561;
// System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>
struct Dictionary_2_t509053110;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1123989164.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m3187802608_gshared (KeyCollection_t2135812561 * __this, Dictionary_2_t509053110 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m3187802608(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2135812561 *, Dictionary_2_t509053110 *, const MethodInfo*))KeyCollection__ctor_m3187802608_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3980779878_gshared (KeyCollection_t2135812561 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3980779878(__this, ___item0, method) ((  void (*) (KeyCollection_t2135812561 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3980779878_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4165019741_gshared (KeyCollection_t2135812561 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4165019741(__this, method) ((  void (*) (KeyCollection_t2135812561 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4165019741_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1323386536_gshared (KeyCollection_t2135812561 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1323386536(__this, ___item0, method) ((  bool (*) (KeyCollection_t2135812561 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1323386536_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2594128397_gshared (KeyCollection_t2135812561 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2594128397(__this, ___item0, method) ((  bool (*) (KeyCollection_t2135812561 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2594128397_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m819301999_gshared (KeyCollection_t2135812561 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m819301999(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2135812561 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m819301999_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m2109761871_gshared (KeyCollection_t2135812561 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m2109761871(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2135812561 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2109761871_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1711186462_gshared (KeyCollection_t2135812561 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1711186462(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2135812561 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1711186462_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3345200137_gshared (KeyCollection_t2135812561 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3345200137(__this, method) ((  bool (*) (KeyCollection_t2135812561 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3345200137_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4094460923_gshared (KeyCollection_t2135812561 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4094460923(__this, method) ((  bool (*) (KeyCollection_t2135812561 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4094460923_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1472155437_gshared (KeyCollection_t2135812561 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1472155437(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2135812561 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1472155437_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3639814309_gshared (KeyCollection_t2135812561 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3639814309(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2135812561 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3639814309_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::GetEnumerator()
extern "C"  Enumerator_t1123989164  KeyCollection_GetEnumerator_m504829106_gshared (KeyCollection_t2135812561 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m504829106(__this, method) ((  Enumerator_t1123989164  (*) (KeyCollection_t2135812561 *, const MethodInfo*))KeyCollection_GetEnumerator_m504829106_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2393859381_gshared (KeyCollection_t2135812561 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m2393859381(__this, method) ((  int32_t (*) (KeyCollection_t2135812561 *, const MethodInfo*))KeyCollection_get_Count_m2393859381_gshared)(__this, method)
