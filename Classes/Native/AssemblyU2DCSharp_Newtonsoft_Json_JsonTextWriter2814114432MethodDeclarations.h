﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonTextWriter
struct JsonTextWriter_t2814114432;
// System.IO.TextWriter
struct TextWriter_t2304124208;
// Newtonsoft.Json.Utilities.Base64Encoder
struct Base64Encoder_t2146525835;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// SerializableGuid
struct SerializableGuid_t3998617672;
// System.Uri
struct Uri_t1116831938;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_TextWriter2304124208.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken4173078175.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_DateTimeOffset3884714306.h"
#include "mscorlib_System_Guid2862754429.h"
#include "AssemblyU2DCSharp_SerializableGuid3998617672.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "System_System_Uri1116831938.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonTextWriter2814114432.h"

// System.Void Newtonsoft.Json.JsonTextWriter::.ctor(System.IO.TextWriter)
extern "C"  void JsonTextWriter__ctor_m355031413 (JsonTextWriter_t2814114432 * __this, TextWriter_t2304124208 * ___textWriter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.Base64Encoder Newtonsoft.Json.JsonTextWriter::get_Base64Encoder()
extern "C"  Base64Encoder_t2146525835 * JsonTextWriter_get_Base64Encoder_m2833371713 (JsonTextWriter_t2814114432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.JsonTextWriter::get_Indentation()
extern "C"  int32_t JsonTextWriter_get_Indentation_m1657715406 (JsonTextWriter_t2814114432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::set_Indentation(System.Int32)
extern "C"  void JsonTextWriter_set_Indentation_m1864968313 (JsonTextWriter_t2814114432 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char Newtonsoft.Json.JsonTextWriter::get_QuoteChar()
extern "C"  Il2CppChar JsonTextWriter_get_QuoteChar_m2378229063 (JsonTextWriter_t2814114432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::set_QuoteChar(System.Char)
extern "C"  void JsonTextWriter_set_QuoteChar_m2146566060 (JsonTextWriter_t2814114432 * __this, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char Newtonsoft.Json.JsonTextWriter::get_IndentChar()
extern "C"  Il2CppChar JsonTextWriter_get_IndentChar_m865596495 (JsonTextWriter_t2814114432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::set_IndentChar(System.Char)
extern "C"  void JsonTextWriter_set_IndentChar_m3060169080 (JsonTextWriter_t2814114432 * __this, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonTextWriter::get_QuoteName()
extern "C"  bool JsonTextWriter_get_QuoteName_m2029453382 (JsonTextWriter_t2814114432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::set_QuoteName(System.Boolean)
extern "C"  void JsonTextWriter_set_QuoteName_m3074435965 (JsonTextWriter_t2814114432 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::Flush()
extern "C"  void JsonTextWriter_Flush_m2019959936 (JsonTextWriter_t2814114432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::Close()
extern "C"  void JsonTextWriter_Close_m3646872180 (JsonTextWriter_t2814114432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteStartObject()
extern "C"  void JsonTextWriter_WriteStartObject_m263197960 (JsonTextWriter_t2814114432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteStartArray()
extern "C"  void JsonTextWriter_WriteStartArray_m2596262802 (JsonTextWriter_t2814114432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteStartConstructor(System.String)
extern "C"  void JsonTextWriter_WriteStartConstructor_m803579631 (JsonTextWriter_t2814114432 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteEnd(Newtonsoft.Json.JsonToken)
extern "C"  void JsonTextWriter_WriteEnd_m2716451540 (JsonTextWriter_t2814114432 * __this, int32_t ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WritePropertyName(System.String)
extern "C"  void JsonTextWriter_WritePropertyName_m4002480423 (JsonTextWriter_t2814114432 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteIndent()
extern "C"  void JsonTextWriter_WriteIndent_m2138453223 (JsonTextWriter_t2814114432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValueDelimiter()
extern "C"  void JsonTextWriter_WriteValueDelimiter_m3817823057 (JsonTextWriter_t2814114432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteIndentSpace()
extern "C"  void JsonTextWriter_WriteIndentSpace_m1641231425 (JsonTextWriter_t2814114432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValueInternal(System.String,Newtonsoft.Json.JsonToken)
extern "C"  void JsonTextWriter_WriteValueInternal_m2134942589 (JsonTextWriter_t2814114432 * __this, String_t* ___value0, int32_t ___token1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteNull()
extern "C"  void JsonTextWriter_WriteNull_m2413528450 (JsonTextWriter_t2814114432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteUndefined()
extern "C"  void JsonTextWriter_WriteUndefined_m563000791 (JsonTextWriter_t2814114432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteRaw(System.String)
extern "C"  void JsonTextWriter_WriteRaw_m3985059891 (JsonTextWriter_t2814114432 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.String)
extern "C"  void JsonTextWriter_WriteValue_m1052767562 (JsonTextWriter_t2814114432 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Int32)
extern "C"  void JsonTextWriter_WriteValue_m157775369 (JsonTextWriter_t2814114432 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.UInt32)
extern "C"  void JsonTextWriter_WriteValue_m1593296706 (JsonTextWriter_t2814114432 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Int64)
extern "C"  void JsonTextWriter_WriteValue_m157778314 (JsonTextWriter_t2814114432 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.UInt64)
extern "C"  void JsonTextWriter_WriteValue_m1593299651 (JsonTextWriter_t2814114432 * __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Single)
extern "C"  void JsonTextWriter_WriteValue_m734091251 (JsonTextWriter_t2814114432 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Double)
extern "C"  void JsonTextWriter_WriteValue_m484528522 (JsonTextWriter_t2814114432 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Boolean)
extern "C"  void JsonTextWriter_WriteValue_m2782079023 (JsonTextWriter_t2814114432 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Int16)
extern "C"  void JsonTextWriter_WriteValue_m157773571 (JsonTextWriter_t2814114432 * __this, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.UInt16)
extern "C"  void JsonTextWriter_WriteValue_m1593294908 (JsonTextWriter_t2814114432 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Char)
extern "C"  void JsonTextWriter_WriteValue_m1384826661 (JsonTextWriter_t2814114432 * __this, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Byte)
extern "C"  void JsonTextWriter_WriteValue_m1384427443 (JsonTextWriter_t2814114432 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.SByte)
extern "C"  void JsonTextWriter_WriteValue_m403644956 (JsonTextWriter_t2814114432 * __this, int8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Decimal)
extern "C"  void JsonTextWriter_WriteValue_m1341548070 (JsonTextWriter_t2814114432 * __this, Decimal_t1954350631  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.DateTime)
extern "C"  void JsonTextWriter_WriteValue_m2322307712 (JsonTextWriter_t2814114432 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Byte[])
extern "C"  void JsonTextWriter_WriteValue_m3289929233 (JsonTextWriter_t2814114432 * __this, ByteU5BU5D_t4260760469* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.DateTimeOffset)
extern "C"  void JsonTextWriter_WriteValue_m1325138669 (JsonTextWriter_t2814114432 * __this, DateTimeOffset_t3884714306  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Guid)
extern "C"  void JsonTextWriter_WriteValue_m1388915282 (JsonTextWriter_t2814114432 * __this, Guid_t2862754429  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(SerializableGuid)
extern "C"  void JsonTextWriter_WriteValue_m1800540016 (JsonTextWriter_t2814114432 * __this, SerializableGuid_t3998617672 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.TimeSpan)
extern "C"  void JsonTextWriter_WriteValue_m2607103460 (JsonTextWriter_t2814114432 * __this, TimeSpan_t413522987  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Uri)
extern "C"  void JsonTextWriter_WriteValue_m1707785835 (JsonTextWriter_t2814114432 * __this, Uri_t1116831938 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteComment(System.String)
extern "C"  void JsonTextWriter_WriteComment_m708802588 (JsonTextWriter_t2814114432 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::WriteWhitespace(System.String)
extern "C"  void JsonTextWriter_WriteWhitespace_m8619146 (JsonTextWriter_t2814114432 * __this, String_t* ___ws0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::ilo_Close1(Newtonsoft.Json.JsonWriter)
extern "C"  void JsonTextWriter_ilo_Close1_m2881737858 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::ilo_WriteValueInternal2(Newtonsoft.Json.JsonTextWriter,System.String,Newtonsoft.Json.JsonToken)
extern "C"  void JsonTextWriter_ilo_WriteValueInternal2_m1427749307 (Il2CppObject * __this /* static, unused */, JsonTextWriter_t2814114432 * ____this0, String_t* ___value1, int32_t ___token2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::ilo_WriteValue3(Newtonsoft.Json.JsonWriter,System.String)
extern "C"  void JsonTextWriter_ilo_WriteValue3_m345359734 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonTextWriter::ilo_ToString4(System.Int32)
extern "C"  String_t* JsonTextWriter_ilo_ToString4_m492307583 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::ilo_WriteValue5(Newtonsoft.Json.JsonWriter,System.UInt32)
extern "C"  void JsonTextWriter_ilo_WriteValue5_m4283462896 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::ilo_WriteValue6(Newtonsoft.Json.JsonWriter,System.Double)
extern "C"  void JsonTextWriter_ilo_WriteValue6_m2725998073 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, double ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::ilo_WriteValue7(Newtonsoft.Json.JsonWriter,System.Boolean)
extern "C"  void JsonTextWriter_ilo_WriteValue7_m2523464447 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonTextWriter::ilo_ToString8(System.Boolean)
extern "C"  String_t* JsonTextWriter_ilo_ToString8_m3263260969 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::ilo_WriteValue9(Newtonsoft.Json.JsonWriter,System.UInt16)
extern "C"  void JsonTextWriter_ilo_WriteValue9_m2488674542 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, uint16_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonTextWriter::ilo_ToString10(System.Char)
extern "C"  String_t* JsonTextWriter_ilo_ToString10_m3096596982 (Il2CppObject * __this /* static, unused */, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonTextWriter::ilo_ToString11(System.SByte)
extern "C"  String_t* JsonTextWriter_ilo_ToString11_m3436469164 (Il2CppObject * __this /* static, unused */, int8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::ilo_WriteValue12(Newtonsoft.Json.JsonWriter,System.DateTimeOffset)
extern "C"  void JsonTextWriter_ilo_WriteValue12_m1647479707 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, DateTimeOffset_t3884714306  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonTextWriter::ilo_ToString13(System.DateTimeOffset)
extern "C"  String_t* JsonTextWriter_ilo_ToString13_m3814304027 (Il2CppObject * __this /* static, unused */, DateTimeOffset_t3884714306  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::ilo_WriteValue14(Newtonsoft.Json.JsonWriter,SerializableGuid)
extern "C"  void JsonTextWriter_ilo_WriteValue14_m1741122880 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, SerializableGuid_t3998617672 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonTextWriter::ilo_WriteValue15(Newtonsoft.Json.JsonWriter,System.TimeSpan)
extern "C"  void JsonTextWriter_ilo_WriteValue15_m2844108949 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, TimeSpan_t413522987  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
