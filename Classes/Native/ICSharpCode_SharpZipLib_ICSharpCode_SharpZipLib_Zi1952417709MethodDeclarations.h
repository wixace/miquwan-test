﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader
struct InflaterDynHeader_t1952417709;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator
struct StreamManipulator_t2348681196;
// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree
struct InflaterHuffmanTree_t1141403250;

#include "codegen/il2cpp-codegen.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi2348681196.h"

// System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::.ctor()
extern "C"  void InflaterDynHeader__ctor_m648908231 (InflaterDynHeader_t1952417709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::Decode(ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator)
extern "C"  bool InflaterDynHeader_Decode_m3398615020 (InflaterDynHeader_t1952417709 * __this, StreamManipulator_t2348681196 * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::BuildLitLenTree()
extern "C"  InflaterHuffmanTree_t1141403250 * InflaterDynHeader_BuildLitLenTree_m1654794856 (InflaterDynHeader_t1952417709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::BuildDistTree()
extern "C"  InflaterHuffmanTree_t1141403250 * InflaterDynHeader_BuildDistTree_m3505675472 (InflaterDynHeader_t1952417709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::.cctor()
extern "C"  void InflaterDynHeader__cctor_m2454189766 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
