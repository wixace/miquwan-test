﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraShotMgrGenerated
struct CameraShotMgrGenerated_t135577558;
// JSVCall
struct JSVCall_t3708497963;
// cameraShotCfg[]
struct cameraShotCfgU5BU5D_t3196548520;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// CameraShotMgr
struct CameraShotMgr_t1580128697;
// CEvent.ZEvent
struct ZEvent_t3638018500;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_CameraShotMgr1580128697.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"

// System.Void CameraShotMgrGenerated::.ctor()
extern "C"  void CameraShotMgrGenerated__ctor_m1848829317 (CameraShotMgrGenerated_t135577558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraShotMgrGenerated::CameraShotMgr_CameraShotMgr1(JSVCall,System.Int32)
extern "C"  bool CameraShotMgrGenerated_CameraShotMgr_CameraShotMgr1_m3553702433 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgrGenerated::CameraShotMgr_WAITTIME_COMPLETE(JSVCall)
extern "C"  void CameraShotMgrGenerated_CameraShotMgr_WAITTIME_COMPLETE_m3773222900 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgrGenerated::CameraShotMgr_ACTION_DONE(JSVCall)
extern "C"  void CameraShotMgrGenerated_CameraShotMgr_ACTION_DONE_m2507053503 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgrGenerated::CameraShotMgr_SKILL_DONE(JSVCall)
extern "C"  void CameraShotMgrGenerated_CameraShotMgr_SKILL_DONE_m201248370 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgrGenerated::CameraShotMgr_START_ANI_COMPLETE(JSVCall)
extern "C"  void CameraShotMgrGenerated_CameraShotMgr_START_ANI_COMPLETE_m1705070633 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgrGenerated::CameraShotMgr_isEditorRun(JSVCall)
extern "C"  void CameraShotMgrGenerated_CameraShotMgr_isEditorRun_m1921119222 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgrGenerated::CameraShotMgr_isCameraShotRunning(JSVCall)
extern "C"  void CameraShotMgrGenerated_CameraShotMgr_isCameraShotRunning_m3428690196 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgrGenerated::CameraShotMgr_effectDic(JSVCall)
extern "C"  void CameraShotMgrGenerated_CameraShotMgr_effectDic_m3903241149 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraShotMgrGenerated::CameraShotMgr_Close(JSVCall,System.Int32)
extern "C"  bool CameraShotMgrGenerated_CameraShotMgr_Close_m3686043761 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraShotMgrGenerated::CameraShotMgr_ClosePeak(JSVCall,System.Int32)
extern "C"  bool CameraShotMgrGenerated_CameraShotMgr_ClosePeak_m3388238896 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraShotMgrGenerated::CameraShotMgr_Execute(JSVCall,System.Int32)
extern "C"  bool CameraShotMgrGenerated_CameraShotMgr_Execute_m3310060590 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraShotMgrGenerated::CameraShotMgr_isCameraShotSkillFit__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool CameraShotMgrGenerated_CameraShotMgr_isCameraShotSkillFit__Int32__Int32_m1885100530 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraShotMgrGenerated::CameraShotMgr_JumpAllStory__ZEvent(JSVCall,System.Int32)
extern "C"  bool CameraShotMgrGenerated_CameraShotMgr_JumpAllStory__ZEvent_m804971915 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraShotMgrGenerated::CameraShotMgr_OnEnd(JSVCall,System.Int32)
extern "C"  bool CameraShotMgrGenerated_CameraShotMgr_OnEnd_m2836265461 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraShotMgrGenerated::CameraShotMgr_Open(JSVCall,System.Int32)
extern "C"  bool CameraShotMgrGenerated_CameraShotMgr_Open_m3380943731 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraShotMgrGenerated::CameraShotMgr_PreviewCameraShot__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool CameraShotMgrGenerated_CameraShotMgr_PreviewCameraShot__Int32__Int32_m1046320352 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraShotMgrGenerated::CameraShotMgr_PreviewCameraShot__Int32(JSVCall,System.Int32)
extern "C"  bool CameraShotMgrGenerated_CameraShotMgr_PreviewCameraShot__Int32_m1500918896 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraShotMgrGenerated::CameraShotMgr_PreviewCameraShot(JSVCall,System.Int32)
extern "C"  bool CameraShotMgrGenerated_CameraShotMgr_PreviewCameraShot_m2472736480 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraShotMgrGenerated::CameraShotMgr_previewInEditor__cameraShotCfg_Array__CameraAniMap(JSVCall,System.Int32)
extern "C"  bool CameraShotMgrGenerated_CameraShotMgr_previewInEditor__cameraShotCfg_Array__CameraAniMap_m1462733523 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgrGenerated::__Register()
extern "C"  void CameraShotMgrGenerated___Register_m2942976610 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// cameraShotCfg[] CameraShotMgrGenerated::<CameraShotMgr_previewInEditor__cameraShotCfg_Array__CameraAniMap>m__2C()
extern "C"  cameraShotCfgU5BU5D_t3196548520* CameraShotMgrGenerated_U3CCameraShotMgr_previewInEditor__cameraShotCfg_Array__CameraAniMapU3Em__2C_m318316035 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgrGenerated::ilo_setStringS1(System.Int32,System.String)
extern "C"  void CameraShotMgrGenerated_ilo_setStringS1_m1905105358 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgrGenerated::ilo_setBooleanS2(System.Int32,System.Boolean)
extern "C"  void CameraShotMgrGenerated_ilo_setBooleanS2_m3562776533 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraShotMgrGenerated::ilo_getBooleanS3(System.Int32)
extern "C"  bool CameraShotMgrGenerated_ilo_getBooleanS3_m3654052649 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotMgrGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t CameraShotMgrGenerated_ilo_setObject4_m3664348776 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CameraShotMgrGenerated::ilo_getObject5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * CameraShotMgrGenerated_ilo_getObject5_m2260441358 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgrGenerated::ilo_ClosePeak6(CameraShotMgr)
extern "C"  void CameraShotMgrGenerated_ilo_ClosePeak6_m2725863176 (Il2CppObject * __this /* static, unused */, CameraShotMgr_t1580128697 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgrGenerated::ilo_Execute7(CameraShotMgr)
extern "C"  void CameraShotMgrGenerated_ilo_Execute7_m72972581 (Il2CppObject * __this /* static, unused */, CameraShotMgr_t1580128697 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotMgrGenerated::ilo_getInt328(System.Int32)
extern "C"  int32_t CameraShotMgrGenerated_ilo_getInt328_m3498823571 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgrGenerated::ilo_JumpAllStory9(CameraShotMgr,CEvent.ZEvent)
extern "C"  void CameraShotMgrGenerated_ilo_JumpAllStory9_m1711980295 (Il2CppObject * __this /* static, unused */, CameraShotMgr_t1580128697 * ____this0, ZEvent_t3638018500 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgrGenerated::ilo_PreviewCameraShot10(CameraShotMgr,System.Int32,System.Int32)
extern "C"  void CameraShotMgrGenerated_ilo_PreviewCameraShot10_m1118413859 (Il2CppObject * __this /* static, unused */, CameraShotMgr_t1580128697 * ____this0, int32_t ___mapId1, int32_t ___cameraShotId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMgrGenerated::ilo_PreviewCameraShot11(CameraShotMgr)
extern "C"  void CameraShotMgrGenerated_ilo_PreviewCameraShot11_m3764460130 (Il2CppObject * __this /* static, unused */, CameraShotMgr_t1580128697 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotMgrGenerated::ilo_getObject12(System.Int32)
extern "C"  int32_t CameraShotMgrGenerated_ilo_getObject12_m345063817 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
