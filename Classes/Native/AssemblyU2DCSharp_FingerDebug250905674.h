﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUITexture
struct GUITexture_t4020448292;
// UnityEngine.GUITexture[]
struct GUITextureU5BU5D_t1303218893;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FingerDebug
struct  FingerDebug_t250905674  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GUITexture FingerDebug::FingerIcon
	GUITexture_t4020448292 * ___FingerIcon_2;
	// System.Boolean FingerDebug::ShowGUI
	bool ___ShowGUI_3;
	// UnityEngine.Rect FingerDebug::GuiRect
	Rect_t4241904616  ___GuiRect_4;
	// UnityEngine.GUITexture[] FingerDebug::icons
	GUITextureU5BU5D_t1303218893* ___icons_5;
	// System.Single FingerDebug::distance
	float ___distance_6;

public:
	inline static int32_t get_offset_of_FingerIcon_2() { return static_cast<int32_t>(offsetof(FingerDebug_t250905674, ___FingerIcon_2)); }
	inline GUITexture_t4020448292 * get_FingerIcon_2() const { return ___FingerIcon_2; }
	inline GUITexture_t4020448292 ** get_address_of_FingerIcon_2() { return &___FingerIcon_2; }
	inline void set_FingerIcon_2(GUITexture_t4020448292 * value)
	{
		___FingerIcon_2 = value;
		Il2CppCodeGenWriteBarrier(&___FingerIcon_2, value);
	}

	inline static int32_t get_offset_of_ShowGUI_3() { return static_cast<int32_t>(offsetof(FingerDebug_t250905674, ___ShowGUI_3)); }
	inline bool get_ShowGUI_3() const { return ___ShowGUI_3; }
	inline bool* get_address_of_ShowGUI_3() { return &___ShowGUI_3; }
	inline void set_ShowGUI_3(bool value)
	{
		___ShowGUI_3 = value;
	}

	inline static int32_t get_offset_of_GuiRect_4() { return static_cast<int32_t>(offsetof(FingerDebug_t250905674, ___GuiRect_4)); }
	inline Rect_t4241904616  get_GuiRect_4() const { return ___GuiRect_4; }
	inline Rect_t4241904616 * get_address_of_GuiRect_4() { return &___GuiRect_4; }
	inline void set_GuiRect_4(Rect_t4241904616  value)
	{
		___GuiRect_4 = value;
	}

	inline static int32_t get_offset_of_icons_5() { return static_cast<int32_t>(offsetof(FingerDebug_t250905674, ___icons_5)); }
	inline GUITextureU5BU5D_t1303218893* get_icons_5() const { return ___icons_5; }
	inline GUITextureU5BU5D_t1303218893** get_address_of_icons_5() { return &___icons_5; }
	inline void set_icons_5(GUITextureU5BU5D_t1303218893* value)
	{
		___icons_5 = value;
		Il2CppCodeGenWriteBarrier(&___icons_5, value);
	}

	inline static int32_t get_offset_of_distance_6() { return static_cast<int32_t>(offsetof(FingerDebug_t250905674, ___distance_6)); }
	inline float get_distance_6() const { return ___distance_6; }
	inline float* get_address_of_distance_6() { return &___distance_6; }
	inline void set_distance_6(float value)
	{
		___distance_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
