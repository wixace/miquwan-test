﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>
struct List_1_t2606718688;
// System.Collections.Generic.IEnumerable`1<Entity.Behavior.EBehaviorID>
struct IEnumerable_1_t244478797;
// System.Collections.Generic.IEnumerator`1<Entity.Behavior.EBehaviorID>
struct IEnumerator_1_t3150398185;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Entity.Behavior.EBehaviorID>
struct ICollection_1_t2133123123;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Entity.Behavior.EBehaviorID>
struct ReadOnlyCollection_1_t2795610672;
// Entity.Behavior.EBehaviorID[]
struct EBehaviorIDU5BU5D_t373889457;
// System.Predicate`1<Entity.Behavior.EBehaviorID>
struct Predicate_1_t849590019;
// System.Collections.Generic.IComparer`1<Entity.Behavior.EBehaviorID>
struct IComparer_1_t3813547178;
// System.Comparison`1<Entity.Behavior.EBehaviorID>
struct Comparison_1_t4249861619;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2626391458.h"

// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::.ctor()
extern "C"  void List_1__ctor_m1718639104_gshared (List_1_t2606718688 * __this, const MethodInfo* method);
#define List_1__ctor_m1718639104(__this, method) ((  void (*) (List_1_t2606718688 *, const MethodInfo*))List_1__ctor_m1718639104_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1379119560_gshared (List_1_t2606718688 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1379119560(__this, ___collection0, method) ((  void (*) (List_1_t2606718688 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1379119560_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m333821224_gshared (List_1_t2606718688 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m333821224(__this, ___capacity0, method) ((  void (*) (List_1_t2606718688 *, int32_t, const MethodInfo*))List_1__ctor_m333821224_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::.cctor()
extern "C"  void List_1__cctor_m3473755190_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3473755190(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3473755190_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3676258537_gshared (List_1_t2606718688 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3676258537(__this, method) ((  Il2CppObject* (*) (List_1_t2606718688 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3676258537_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3905908429_gshared (List_1_t2606718688 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3905908429(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2606718688 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3905908429_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3585937032_gshared (List_1_t2606718688 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3585937032(__this, method) ((  Il2CppObject * (*) (List_1_t2606718688 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3585937032_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m4257855977_gshared (List_1_t2606718688 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m4257855977(__this, ___item0, method) ((  int32_t (*) (List_1_t2606718688 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m4257855977_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m486616323_gshared (List_1_t2606718688 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m486616323(__this, ___item0, method) ((  bool (*) (List_1_t2606718688 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m486616323_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3696808897_gshared (List_1_t2606718688 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m3696808897(__this, ___item0, method) ((  int32_t (*) (List_1_t2606718688 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3696808897_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2846280044_gshared (List_1_t2606718688 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2846280044(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2606718688 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2846280044_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3947233340_gshared (List_1_t2606718688 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3947233340(__this, ___item0, method) ((  void (*) (List_1_t2606718688 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3947233340_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m562492740_gshared (List_1_t2606718688 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m562492740(__this, method) ((  bool (*) (List_1_t2606718688 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m562492740_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m1267230585_gshared (List_1_t2606718688 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1267230585(__this, method) ((  bool (*) (List_1_t2606718688 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1267230585_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2007783781_gshared (List_1_t2606718688 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2007783781(__this, method) ((  Il2CppObject * (*) (List_1_t2606718688 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2007783781_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m2325977778_gshared (List_1_t2606718688 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m2325977778(__this, method) ((  bool (*) (List_1_t2606718688 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2325977778_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1083229703_gshared (List_1_t2606718688 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1083229703(__this, method) ((  bool (*) (List_1_t2606718688 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1083229703_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2105509292_gshared (List_1_t2606718688 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2105509292(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2606718688 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2105509292_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m4289874499_gshared (List_1_t2606718688 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m4289874499(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2606718688 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m4289874499_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::Add(T)
extern "C"  void List_1_Add_m1412999920_gshared (List_1_t2606718688 * __this, uint8_t ___item0, const MethodInfo* method);
#define List_1_Add_m1412999920(__this, ___item0, method) ((  void (*) (List_1_t2606718688 *, uint8_t, const MethodInfo*))List_1_Add_m1412999920_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1539557763_gshared (List_1_t2606718688 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1539557763(__this, ___newCount0, method) ((  void (*) (List_1_t2606718688 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1539557763_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m4152777348_gshared (List_1_t2606718688 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m4152777348(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t2606718688 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m4152777348_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2226476161_gshared (List_1_t2606718688 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2226476161(__this, ___collection0, method) ((  void (*) (List_1_t2606718688 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2226476161_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m1487448385_gshared (List_1_t2606718688 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m1487448385(__this, ___enumerable0, method) ((  void (*) (List_1_t2606718688 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1487448385_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3744579766_gshared (List_1_t2606718688 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3744579766(__this, ___collection0, method) ((  void (*) (List_1_t2606718688 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3744579766_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2795610672 * List_1_AsReadOnly_m3843060019_gshared (List_1_t2606718688 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3843060019(__this, method) ((  ReadOnlyCollection_1_t2795610672 * (*) (List_1_t2606718688 *, const MethodInfo*))List_1_AsReadOnly_m3843060019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m46267820_gshared (List_1_t2606718688 * __this, uint8_t ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m46267820(__this, ___item0, method) ((  int32_t (*) (List_1_t2606718688 *, uint8_t, const MethodInfo*))List_1_BinarySearch_m46267820_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::Clear()
extern "C"  void List_1_Clear_m3214182018_gshared (List_1_t2606718688 * __this, const MethodInfo* method);
#define List_1_Clear_m3214182018(__this, method) ((  void (*) (List_1_t2606718688 *, const MethodInfo*))List_1_Clear_m3214182018_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::Contains(T)
extern "C"  bool List_1_Contains_m2370105352_gshared (List_1_t2606718688 * __this, uint8_t ___item0, const MethodInfo* method);
#define List_1_Contains_m2370105352(__this, ___item0, method) ((  bool (*) (List_1_t2606718688 *, uint8_t, const MethodInfo*))List_1_Contains_m2370105352_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1706341688_gshared (List_1_t2606718688 * __this, EBehaviorIDU5BU5D_t373889457* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m1706341688(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2606718688 *, EBehaviorIDU5BU5D_t373889457*, int32_t, const MethodInfo*))List_1_CopyTo_m1706341688_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::Find(System.Predicate`1<T>)
extern "C"  uint8_t List_1_Find_m3581307376_gshared (List_1_t2606718688 * __this, Predicate_1_t849590019 * ___match0, const MethodInfo* method);
#define List_1_Find_m3581307376(__this, ___match0, method) ((  uint8_t (*) (List_1_t2606718688 *, Predicate_1_t849590019 *, const MethodInfo*))List_1_Find_m3581307376_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m2417608875_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t849590019 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m2417608875(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t849590019 *, const MethodInfo*))List_1_CheckMatch_m2417608875_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3966453712_gshared (List_1_t2606718688 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t849590019 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3966453712(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2606718688 *, int32_t, int32_t, Predicate_1_t849590019 *, const MethodInfo*))List_1_GetIndex_m3966453712_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::GetEnumerator()
extern "C"  Enumerator_t2626391458  List_1_GetEnumerator_m1508723565_gshared (List_1_t2606718688 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1508723565(__this, method) ((  Enumerator_t2626391458  (*) (List_1_t2606718688 *, const MethodInfo*))List_1_GetEnumerator_m1508723565_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2371838908_gshared (List_1_t2606718688 * __this, uint8_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2371838908(__this, ___item0, method) ((  int32_t (*) (List_1_t2606718688 *, uint8_t, const MethodInfo*))List_1_IndexOf_m2371838908_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2944390031_gshared (List_1_t2606718688 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2944390031(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2606718688 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2944390031_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1452708872_gshared (List_1_t2606718688 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m1452708872(__this, ___index0, method) ((  void (*) (List_1_t2606718688 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1452708872_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3438197167_gshared (List_1_t2606718688 * __this, int32_t ___index0, uint8_t ___item1, const MethodInfo* method);
#define List_1_Insert_m3438197167(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2606718688 *, int32_t, uint8_t, const MethodInfo*))List_1_Insert_m3438197167_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m1276545764_gshared (List_1_t2606718688 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m1276545764(__this, ___collection0, method) ((  void (*) (List_1_t2606718688 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m1276545764_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::Remove(T)
extern "C"  bool List_1_Remove_m3028384299_gshared (List_1_t2606718688 * __this, uint8_t ___item0, const MethodInfo* method);
#define List_1_Remove_m3028384299(__this, ___item0, method) ((  bool (*) (List_1_t2606718688 *, uint8_t, const MethodInfo*))List_1_Remove_m3028384299_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2481616511_gshared (List_1_t2606718688 * __this, Predicate_1_t849590019 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m2481616511(__this, ___match0, method) ((  int32_t (*) (List_1_t2606718688 *, Predicate_1_t849590019 *, const MethodInfo*))List_1_RemoveAll_m2481616511_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m1312050037_gshared (List_1_t2606718688 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m1312050037(__this, ___index0, method) ((  void (*) (List_1_t2606718688 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1312050037_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m3917258136_gshared (List_1_t2606718688 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m3917258136(__this, ___index0, ___count1, method) ((  void (*) (List_1_t2606718688 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3917258136_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::Reverse()
extern "C"  void List_1_Reverse_m2410120407_gshared (List_1_t2606718688 * __this, const MethodInfo* method);
#define List_1_Reverse_m2410120407(__this, method) ((  void (*) (List_1_t2606718688 *, const MethodInfo*))List_1_Reverse_m2410120407_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::Sort()
extern "C"  void List_1_Sort_m2088944171_gshared (List_1_t2606718688 * __this, const MethodInfo* method);
#define List_1_Sort_m2088944171(__this, method) ((  void (*) (List_1_t2606718688 *, const MethodInfo*))List_1_Sort_m2088944171_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m2107944985_gshared (List_1_t2606718688 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m2107944985(__this, ___comparer0, method) ((  void (*) (List_1_t2606718688 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m2107944985_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m83889214_gshared (List_1_t2606718688 * __this, Comparison_1_t4249861619 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m83889214(__this, ___comparison0, method) ((  void (*) (List_1_t2606718688 *, Comparison_1_t4249861619 *, const MethodInfo*))List_1_Sort_m83889214_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::ToArray()
extern "C"  EBehaviorIDU5BU5D_t373889457* List_1_ToArray_m949417110_gshared (List_1_t2606718688 * __this, const MethodInfo* method);
#define List_1_ToArray_m949417110(__this, method) ((  EBehaviorIDU5BU5D_t373889457* (*) (List_1_t2606718688 *, const MethodInfo*))List_1_ToArray_m949417110_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::TrimExcess()
extern "C"  void List_1_TrimExcess_m3603191172_gshared (List_1_t2606718688 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m3603191172(__this, method) ((  void (*) (List_1_t2606718688 *, const MethodInfo*))List_1_TrimExcess_m3603191172_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m594484716_gshared (List_1_t2606718688 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m594484716(__this, method) ((  int32_t (*) (List_1_t2606718688 *, const MethodInfo*))List_1_get_Capacity_m594484716_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3044713877_gshared (List_1_t2606718688 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3044713877(__this, ___value0, method) ((  void (*) (List_1_t2606718688 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3044713877_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::get_Count()
extern "C"  int32_t List_1_get_Count_m329869631_gshared (List_1_t2606718688 * __this, const MethodInfo* method);
#define List_1_get_Count_m329869631(__this, method) ((  int32_t (*) (List_1_t2606718688 *, const MethodInfo*))List_1_get_Count_m329869631_gshared)(__this, method)
// T System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::get_Item(System.Int32)
extern "C"  uint8_t List_1_get_Item_m3104152249_gshared (List_1_t2606718688 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3104152249(__this, ___index0, method) ((  uint8_t (*) (List_1_t2606718688 *, int32_t, const MethodInfo*))List_1_get_Item_m3104152249_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Entity.Behavior.EBehaviorID>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3087223494_gshared (List_1_t2606718688 * __this, int32_t ___index0, uint8_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m3087223494(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2606718688 *, int32_t, uint8_t, const MethodInfo*))List_1_set_Item_m3087223494_gshared)(__this, ___index0, ___value1, method)
