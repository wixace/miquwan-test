﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RuntimePlatform[]
struct RuntimePlatformU5BU5D_t2169002428;
// FGInputProvider
struct FGInputProvider_t1238597786;
// FingerClusterManager
struct FingerClusterManager_t3376029756;
// System.Collections.Generic.List`1<GestureRecognizer>
struct List_1_t586094205;
// FingerGestures
struct FingerGestures_t2907604723;
// FingerGestures/Finger[]
struct FingerU5BU5D_t906829736;
// FingerGestures/FingerList
struct FingerList_t1886137443;
// FingerGestures/GlobalTouchFilterDelegate
struct GlobalTouchFilterDelegate_t316184925;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3792884695;
// FingerGestures/SwipeDirection[]
struct SwipeDirectionU5BU5D_t196035132;
// Gesture/EventHandler
struct EventHandler_t2814654102;
// FingerEventDetector`1/FingerEventHandler<FingerEvent>
struct FingerEventHandler_t2860637046;
// FingerGestures/EventHandler
struct EventHandler_t2445892876;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FingerGestures
struct  FingerGestures_t2907604723  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean FingerGestures::makePersistent
	bool ___makePersistent_6;
	// System.Boolean FingerGestures::detectUnityRemote
	bool ___detectUnityRemote_7;
	// FGInputProvider FingerGestures::mouseInputProviderPrefab
	FGInputProvider_t1238597786 * ___mouseInputProviderPrefab_8;
	// FGInputProvider FingerGestures::touchInputProviderPrefab
	FGInputProvider_t1238597786 * ___touchInputProviderPrefab_9;
	// FingerClusterManager FingerGestures::fingerClusterManager
	FingerClusterManager_t3376029756 * ___fingerClusterManager_10;
	// FGInputProvider FingerGestures::inputProvider
	FGInputProvider_t1238597786 * ___inputProvider_11;
	// FingerGestures/Finger[] FingerGestures::fingers
	FingerU5BU5D_t906829736* ___fingers_14;
	// FingerGestures/FingerList FingerGestures::touches
	FingerList_t1886137443 * ___touches_15;
	// FingerGestures/GlobalTouchFilterDelegate FingerGestures::globalTouchFilterFunc
	GlobalTouchFilterDelegate_t316184925 * ___globalTouchFilterFunc_16;
	// UnityEngine.Transform[] FingerGestures::fingerNodes
	TransformU5BU5D_t3792884695* ___fingerNodes_17;

public:
	inline static int32_t get_offset_of_makePersistent_6() { return static_cast<int32_t>(offsetof(FingerGestures_t2907604723, ___makePersistent_6)); }
	inline bool get_makePersistent_6() const { return ___makePersistent_6; }
	inline bool* get_address_of_makePersistent_6() { return &___makePersistent_6; }
	inline void set_makePersistent_6(bool value)
	{
		___makePersistent_6 = value;
	}

	inline static int32_t get_offset_of_detectUnityRemote_7() { return static_cast<int32_t>(offsetof(FingerGestures_t2907604723, ___detectUnityRemote_7)); }
	inline bool get_detectUnityRemote_7() const { return ___detectUnityRemote_7; }
	inline bool* get_address_of_detectUnityRemote_7() { return &___detectUnityRemote_7; }
	inline void set_detectUnityRemote_7(bool value)
	{
		___detectUnityRemote_7 = value;
	}

	inline static int32_t get_offset_of_mouseInputProviderPrefab_8() { return static_cast<int32_t>(offsetof(FingerGestures_t2907604723, ___mouseInputProviderPrefab_8)); }
	inline FGInputProvider_t1238597786 * get_mouseInputProviderPrefab_8() const { return ___mouseInputProviderPrefab_8; }
	inline FGInputProvider_t1238597786 ** get_address_of_mouseInputProviderPrefab_8() { return &___mouseInputProviderPrefab_8; }
	inline void set_mouseInputProviderPrefab_8(FGInputProvider_t1238597786 * value)
	{
		___mouseInputProviderPrefab_8 = value;
		Il2CppCodeGenWriteBarrier(&___mouseInputProviderPrefab_8, value);
	}

	inline static int32_t get_offset_of_touchInputProviderPrefab_9() { return static_cast<int32_t>(offsetof(FingerGestures_t2907604723, ___touchInputProviderPrefab_9)); }
	inline FGInputProvider_t1238597786 * get_touchInputProviderPrefab_9() const { return ___touchInputProviderPrefab_9; }
	inline FGInputProvider_t1238597786 ** get_address_of_touchInputProviderPrefab_9() { return &___touchInputProviderPrefab_9; }
	inline void set_touchInputProviderPrefab_9(FGInputProvider_t1238597786 * value)
	{
		___touchInputProviderPrefab_9 = value;
		Il2CppCodeGenWriteBarrier(&___touchInputProviderPrefab_9, value);
	}

	inline static int32_t get_offset_of_fingerClusterManager_10() { return static_cast<int32_t>(offsetof(FingerGestures_t2907604723, ___fingerClusterManager_10)); }
	inline FingerClusterManager_t3376029756 * get_fingerClusterManager_10() const { return ___fingerClusterManager_10; }
	inline FingerClusterManager_t3376029756 ** get_address_of_fingerClusterManager_10() { return &___fingerClusterManager_10; }
	inline void set_fingerClusterManager_10(FingerClusterManager_t3376029756 * value)
	{
		___fingerClusterManager_10 = value;
		Il2CppCodeGenWriteBarrier(&___fingerClusterManager_10, value);
	}

	inline static int32_t get_offset_of_inputProvider_11() { return static_cast<int32_t>(offsetof(FingerGestures_t2907604723, ___inputProvider_11)); }
	inline FGInputProvider_t1238597786 * get_inputProvider_11() const { return ___inputProvider_11; }
	inline FGInputProvider_t1238597786 ** get_address_of_inputProvider_11() { return &___inputProvider_11; }
	inline void set_inputProvider_11(FGInputProvider_t1238597786 * value)
	{
		___inputProvider_11 = value;
		Il2CppCodeGenWriteBarrier(&___inputProvider_11, value);
	}

	inline static int32_t get_offset_of_fingers_14() { return static_cast<int32_t>(offsetof(FingerGestures_t2907604723, ___fingers_14)); }
	inline FingerU5BU5D_t906829736* get_fingers_14() const { return ___fingers_14; }
	inline FingerU5BU5D_t906829736** get_address_of_fingers_14() { return &___fingers_14; }
	inline void set_fingers_14(FingerU5BU5D_t906829736* value)
	{
		___fingers_14 = value;
		Il2CppCodeGenWriteBarrier(&___fingers_14, value);
	}

	inline static int32_t get_offset_of_touches_15() { return static_cast<int32_t>(offsetof(FingerGestures_t2907604723, ___touches_15)); }
	inline FingerList_t1886137443 * get_touches_15() const { return ___touches_15; }
	inline FingerList_t1886137443 ** get_address_of_touches_15() { return &___touches_15; }
	inline void set_touches_15(FingerList_t1886137443 * value)
	{
		___touches_15 = value;
		Il2CppCodeGenWriteBarrier(&___touches_15, value);
	}

	inline static int32_t get_offset_of_globalTouchFilterFunc_16() { return static_cast<int32_t>(offsetof(FingerGestures_t2907604723, ___globalTouchFilterFunc_16)); }
	inline GlobalTouchFilterDelegate_t316184925 * get_globalTouchFilterFunc_16() const { return ___globalTouchFilterFunc_16; }
	inline GlobalTouchFilterDelegate_t316184925 ** get_address_of_globalTouchFilterFunc_16() { return &___globalTouchFilterFunc_16; }
	inline void set_globalTouchFilterFunc_16(GlobalTouchFilterDelegate_t316184925 * value)
	{
		___globalTouchFilterFunc_16 = value;
		Il2CppCodeGenWriteBarrier(&___globalTouchFilterFunc_16, value);
	}

	inline static int32_t get_offset_of_fingerNodes_17() { return static_cast<int32_t>(offsetof(FingerGestures_t2907604723, ___fingerNodes_17)); }
	inline TransformU5BU5D_t3792884695* get_fingerNodes_17() const { return ___fingerNodes_17; }
	inline TransformU5BU5D_t3792884695** get_address_of_fingerNodes_17() { return &___fingerNodes_17; }
	inline void set_fingerNodes_17(TransformU5BU5D_t3792884695* value)
	{
		___fingerNodes_17 = value;
		Il2CppCodeGenWriteBarrier(&___fingerNodes_17, value);
	}
};

struct FingerGestures_t2907604723_StaticFields
{
public:
	// UnityEngine.RuntimePlatform[] FingerGestures::TouchScreenPlatforms
	RuntimePlatformU5BU5D_t2169002428* ___TouchScreenPlatforms_5;
	// System.Collections.Generic.List`1<GestureRecognizer> FingerGestures::gestureRecognizers
	List_1_t586094205 * ___gestureRecognizers_12;
	// FingerGestures FingerGestures::instance
	FingerGestures_t2907604723 * ___instance_13;
	// FingerGestures/SwipeDirection[] FingerGestures::AngleToDirectionMap
	SwipeDirectionU5BU5D_t196035132* ___AngleToDirectionMap_18;
	// System.Single FingerGestures::screenDPI
	float ___screenDPI_19;
	// Gesture/EventHandler FingerGestures::OnGestureEvent
	EventHandler_t2814654102 * ___OnGestureEvent_20;
	// FingerEventDetector`1/FingerEventHandler<FingerEvent> FingerGestures::OnFingerEvent
	FingerEventHandler_t2860637046 * ___OnFingerEvent_21;
	// FingerGestures/EventHandler FingerGestures::OnInputProviderChanged
	EventHandler_t2445892876 * ___OnInputProviderChanged_22;

public:
	inline static int32_t get_offset_of_TouchScreenPlatforms_5() { return static_cast<int32_t>(offsetof(FingerGestures_t2907604723_StaticFields, ___TouchScreenPlatforms_5)); }
	inline RuntimePlatformU5BU5D_t2169002428* get_TouchScreenPlatforms_5() const { return ___TouchScreenPlatforms_5; }
	inline RuntimePlatformU5BU5D_t2169002428** get_address_of_TouchScreenPlatforms_5() { return &___TouchScreenPlatforms_5; }
	inline void set_TouchScreenPlatforms_5(RuntimePlatformU5BU5D_t2169002428* value)
	{
		___TouchScreenPlatforms_5 = value;
		Il2CppCodeGenWriteBarrier(&___TouchScreenPlatforms_5, value);
	}

	inline static int32_t get_offset_of_gestureRecognizers_12() { return static_cast<int32_t>(offsetof(FingerGestures_t2907604723_StaticFields, ___gestureRecognizers_12)); }
	inline List_1_t586094205 * get_gestureRecognizers_12() const { return ___gestureRecognizers_12; }
	inline List_1_t586094205 ** get_address_of_gestureRecognizers_12() { return &___gestureRecognizers_12; }
	inline void set_gestureRecognizers_12(List_1_t586094205 * value)
	{
		___gestureRecognizers_12 = value;
		Il2CppCodeGenWriteBarrier(&___gestureRecognizers_12, value);
	}

	inline static int32_t get_offset_of_instance_13() { return static_cast<int32_t>(offsetof(FingerGestures_t2907604723_StaticFields, ___instance_13)); }
	inline FingerGestures_t2907604723 * get_instance_13() const { return ___instance_13; }
	inline FingerGestures_t2907604723 ** get_address_of_instance_13() { return &___instance_13; }
	inline void set_instance_13(FingerGestures_t2907604723 * value)
	{
		___instance_13 = value;
		Il2CppCodeGenWriteBarrier(&___instance_13, value);
	}

	inline static int32_t get_offset_of_AngleToDirectionMap_18() { return static_cast<int32_t>(offsetof(FingerGestures_t2907604723_StaticFields, ___AngleToDirectionMap_18)); }
	inline SwipeDirectionU5BU5D_t196035132* get_AngleToDirectionMap_18() const { return ___AngleToDirectionMap_18; }
	inline SwipeDirectionU5BU5D_t196035132** get_address_of_AngleToDirectionMap_18() { return &___AngleToDirectionMap_18; }
	inline void set_AngleToDirectionMap_18(SwipeDirectionU5BU5D_t196035132* value)
	{
		___AngleToDirectionMap_18 = value;
		Il2CppCodeGenWriteBarrier(&___AngleToDirectionMap_18, value);
	}

	inline static int32_t get_offset_of_screenDPI_19() { return static_cast<int32_t>(offsetof(FingerGestures_t2907604723_StaticFields, ___screenDPI_19)); }
	inline float get_screenDPI_19() const { return ___screenDPI_19; }
	inline float* get_address_of_screenDPI_19() { return &___screenDPI_19; }
	inline void set_screenDPI_19(float value)
	{
		___screenDPI_19 = value;
	}

	inline static int32_t get_offset_of_OnGestureEvent_20() { return static_cast<int32_t>(offsetof(FingerGestures_t2907604723_StaticFields, ___OnGestureEvent_20)); }
	inline EventHandler_t2814654102 * get_OnGestureEvent_20() const { return ___OnGestureEvent_20; }
	inline EventHandler_t2814654102 ** get_address_of_OnGestureEvent_20() { return &___OnGestureEvent_20; }
	inline void set_OnGestureEvent_20(EventHandler_t2814654102 * value)
	{
		___OnGestureEvent_20 = value;
		Il2CppCodeGenWriteBarrier(&___OnGestureEvent_20, value);
	}

	inline static int32_t get_offset_of_OnFingerEvent_21() { return static_cast<int32_t>(offsetof(FingerGestures_t2907604723_StaticFields, ___OnFingerEvent_21)); }
	inline FingerEventHandler_t2860637046 * get_OnFingerEvent_21() const { return ___OnFingerEvent_21; }
	inline FingerEventHandler_t2860637046 ** get_address_of_OnFingerEvent_21() { return &___OnFingerEvent_21; }
	inline void set_OnFingerEvent_21(FingerEventHandler_t2860637046 * value)
	{
		___OnFingerEvent_21 = value;
		Il2CppCodeGenWriteBarrier(&___OnFingerEvent_21, value);
	}

	inline static int32_t get_offset_of_OnInputProviderChanged_22() { return static_cast<int32_t>(offsetof(FingerGestures_t2907604723_StaticFields, ___OnInputProviderChanged_22)); }
	inline EventHandler_t2445892876 * get_OnInputProviderChanged_22() const { return ___OnInputProviderChanged_22; }
	inline EventHandler_t2445892876 ** get_address_of_OnInputProviderChanged_22() { return &___OnInputProviderChanged_22; }
	inline void set_OnInputProviderChanged_22(EventHandler_t2445892876 * value)
	{
		___OnInputProviderChanged_22 = value;
		Il2CppCodeGenWriteBarrier(&___OnInputProviderChanged_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
