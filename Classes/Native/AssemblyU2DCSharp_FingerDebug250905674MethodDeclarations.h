﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FingerDebug
struct FingerDebug_t250905674;
// FingerGestures
struct FingerGestures_t2907604723;
// FingerGestures/IFingerList
struct IFingerList_t1026994100;
// FingerGestures/Finger
struct Finger_t182428197;
// FingerClusterManager
struct FingerClusterManager_t3376029756;
// FingerGestures/FingerList
struct FingerList_t1886137443;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_FingerGestures_Finger182428197.h"
#include "AssemblyU2DCSharp_FingerGestures_FingerList1886137443.h"

// System.Void FingerDebug::.ctor()
extern "C"  void FingerDebug__ctor_m1380330081 (FingerDebug_t250905674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerDebug::Start()
extern "C"  void FingerDebug_Start_m327467873 (FingerDebug_t250905674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerDebug::Update()
extern "C"  void FingerDebug_Update_m1567421644 (FingerDebug_t250905674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerDebug::OnGUI()
extern "C"  void FingerDebug_OnGUI_m875728731 (FingerDebug_t250905674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures FingerDebug::ilo_get_Instance1()
extern "C"  FingerGestures_t2907604723 * FingerDebug_ilo_get_Instance1_m2241468457 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/IFingerList FingerDebug::ilo_get_Touches2()
extern "C"  Il2CppObject * FingerDebug_ilo_get_Touches2_m361926715 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/Finger FingerDebug::ilo_get_Item3(FingerGestures/IFingerList,System.Int32)
extern "C"  Finger_t182428197 * FingerDebug_ilo_get_Item3_m2089417434 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 FingerDebug::ilo_get_Position4(FingerGestures/Finger)
extern "C"  Vector2_t4282066565  FingerDebug_ilo_get_Position4_m2807139204 (Il2CppObject * __this /* static, unused */, Finger_t182428197 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FingerDebug::ilo_get_Count5(FingerGestures/IFingerList)
extern "C"  int32_t FingerDebug_ilo_get_Count5_m4017659459 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerClusterManager FingerDebug::ilo_get_DefaultClusterManager6()
extern "C"  FingerClusterManager_t3376029756 * FingerDebug_ilo_get_DefaultClusterManager6_m3149378336 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FingerDebug::ilo_get_Count7(FingerGestures/FingerList)
extern "C"  int32_t FingerDebug_ilo_get_Count7_m2614293974 (Il2CppObject * __this /* static, unused */, FingerList_t1886137443 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
