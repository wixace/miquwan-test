﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Nova.PersistPathMgr
struct PersistPathMgr_t636044869;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Nova.PersistPathMgr::.ctor()
extern "C"  void PersistPathMgr__ctor_m2564585494 (PersistPathMgr_t636044869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Nova.PersistPathMgr::.cctor()
extern "C"  void PersistPathMgr__cctor_m1710642775 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Nova.PersistPathMgr::GetPersistentDataPath()
extern "C"  String_t* PersistPathMgr_GetPersistentDataPath_m4140785523 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Nova.PersistPathMgr::InternalGetPersistentDataPath()
extern "C"  String_t* PersistPathMgr_InternalGetPersistentDataPath_m3594057206 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Nova.PersistPathMgr::get_persistentDataPath_fromC()
extern "C"  String_t* PersistPathMgr_get_persistentDataPath_fromC_m798234836 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
