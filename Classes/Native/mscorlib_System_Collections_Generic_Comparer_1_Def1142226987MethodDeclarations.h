﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct DefaultComparer_t1142226987;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr_ErrorIn2633981210.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m2992504096_gshared (DefaultComparer_t1142226987 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2992504096(__this, method) ((  void (*) (DefaultComparer_t1142226987 *, const MethodInfo*))DefaultComparer__ctor_m2992504096_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Mihua.Assets.SubAssetMgr/ErrorInfo>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m2585006679_gshared (DefaultComparer_t1142226987 * __this, ErrorInfo_t2633981210  ___x0, ErrorInfo_t2633981210  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m2585006679(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t1142226987 *, ErrorInfo_t2633981210 , ErrorInfo_t2633981210 , const MethodInfo*))DefaultComparer_Compare_m2585006679_gshared)(__this, ___x0, ___y1, method)
