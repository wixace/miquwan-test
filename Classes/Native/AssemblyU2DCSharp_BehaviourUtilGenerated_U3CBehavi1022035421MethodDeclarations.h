﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtilGenerated/<BehaviourUtil_JSDelayCall_GetDelegate_member7_arg1>c__AnonStorey4C
struct U3CBehaviourUtil_JSDelayCall_GetDelegate_member7_arg1U3Ec__AnonStorey4C_t1022035421;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void BehaviourUtilGenerated/<BehaviourUtil_JSDelayCall_GetDelegate_member7_arg1>c__AnonStorey4C::.ctor()
extern "C"  void U3CBehaviourUtil_JSDelayCall_GetDelegate_member7_arg1U3Ec__AnonStorey4C__ctor_m1084025694 (U3CBehaviourUtil_JSDelayCall_GetDelegate_member7_arg1U3Ec__AnonStorey4C_t1022035421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtilGenerated/<BehaviourUtil_JSDelayCall_GetDelegate_member7_arg1>c__AnonStorey4C::<>m__C(System.Object,System.Object,System.Object,System.Object)
extern "C"  void U3CBehaviourUtil_JSDelayCall_GetDelegate_member7_arg1U3Ec__AnonStorey4C_U3CU3Em__C_m133036960 (U3CBehaviourUtil_JSDelayCall_GetDelegate_member7_arg1U3Ec__AnonStorey4C_t1022035421 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, const MethodInfo* method) IL2CPP_METHOD_ATTR;
