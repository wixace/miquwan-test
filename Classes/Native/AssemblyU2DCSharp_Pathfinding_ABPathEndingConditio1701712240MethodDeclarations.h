﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.ABPathEndingCondition
struct ABPathEndingCondition_t1701712240;
// Pathfinding.ABPath
struct ABPath_t1187561148;
// Pathfinding.PathNode
struct PathNode_t417131581;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_ABPath1187561148.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"

// System.Void Pathfinding.ABPathEndingCondition::.ctor(Pathfinding.ABPath)
extern "C"  void ABPathEndingCondition__ctor_m3196064471 (ABPathEndingCondition_t1701712240 * __this, ABPath_t1187561148 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ABPathEndingCondition::TargetFound(Pathfinding.PathNode)
extern "C"  bool ABPathEndingCondition_TargetFound_m2691413881 (ABPathEndingCondition_t1701712240 * __this, PathNode_t417131581 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
