﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginDolphin
struct PluginDolphin_t1635836683;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// VersionInfo
struct VersionInfo_t2356638086;
// System.Object
struct Il2CppObject;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// Mihua.SDK.EnterGameInfo
struct EnterGameInfo_t1897532954;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Action
struct Action_t3771233898;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"

// System.Void PluginDolphin::.ctor()
extern "C"  void PluginDolphin__ctor_m3551574720 (PluginDolphin_t1635836683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::Init()
extern "C"  void PluginDolphin_Init_m758840212 (PluginDolphin_t1635836683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginDolphin::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginDolphin_ReqSDKHttpLogin_m3445925041 (PluginDolphin_t1635836683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginDolphin::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginDolphin_IsLoginSuccess_m3039453611 (PluginDolphin_t1635836683 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::OpenUserLogin()
extern "C"  void PluginDolphin_OpenUserLogin_m1231232274 (PluginDolphin_t1635836683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::UserPay(CEvent.ZEvent)
extern "C"  void PluginDolphin_UserPay_m2525691424 (PluginDolphin_t1635836683 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginDolphin_SignCallBack_m132244665 (PluginDolphin_t1635836683 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginDolphin::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginDolphin_BuildOrderParam2WWWForm_m1874106147 (PluginDolphin_t1635836683 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::CreateRole(CEvent.ZEvent)
extern "C"  void PluginDolphin_CreateRole_m1476290789 (PluginDolphin_t1635836683 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::EnterGame(CEvent.ZEvent)
extern "C"  void PluginDolphin_EnterGame_m2749482931 (PluginDolphin_t1635836683 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginDolphin_RoleUpgrade_m1183672727 (PluginDolphin_t1635836683 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::OnLoginSuccess(System.String)
extern "C"  void PluginDolphin_OnLoginSuccess_m1255809477 (PluginDolphin_t1635836683 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::OnLogoutSuccess(System.String)
extern "C"  void PluginDolphin_OnLogoutSuccess_m278930794 (PluginDolphin_t1635836683 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::OnLogout(System.String)
extern "C"  void PluginDolphin_OnLogout_m2725911829 (PluginDolphin_t1635836683 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::initSdk(System.String,System.String)
extern "C"  void PluginDolphin_initSdk_m1245603702 (PluginDolphin_t1635836683 * __this, String_t* ___gameid0, String_t* ___appkey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::login()
extern "C"  void PluginDolphin_login_m3073589543 (PluginDolphin_t1635836683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::logout()
extern "C"  void PluginDolphin_logout_m797818030 (PluginDolphin_t1635836683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::creatRole(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginDolphin_creatRole_m632343981 (PluginDolphin_t1635836683 * __this, String_t* ___role_id0, String_t* ___role_name1, String_t* ___role_level2, String_t* ___gold3, String_t* ___vipLv4, String_t* ___server_id5, String_t* ___server_name6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::roleUpgrade(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginDolphin_roleUpgrade_m39155558 (PluginDolphin_t1635836683 * __this, String_t* ___role_id0, String_t* ___role_name1, String_t* ___role_level2, String_t* ___gold3, String_t* ___vipLv4, String_t* ___server_id5, String_t* ___server_name6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginDolphin_pay_m2620964780 (PluginDolphin_t1635836683 * __this, String_t* ___productId0, String_t* ___price1, String_t* ___serverId2, String_t* ___orderId3, String_t* ___extra4, String_t* ___servername5, String_t* ___roleid6, String_t* ___rolename7, String_t* ___rolelv8, String_t* ___rate9, String_t* ___goodsname10, String_t* ___productname11, String_t* ___gamename12, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::<OnLogoutSuccess>m__418()
extern "C"  void PluginDolphin_U3COnLogoutSuccessU3Em__418_m312320204 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::<OnLogout>m__419()
extern "C"  void PluginDolphin_U3COnLogoutU3Em__419_m83232396 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginDolphin_ilo_AddEventListener1_m2000084532 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginDolphin::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginDolphin_ilo_get_Instance2_m1620126556 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginDolphin::ilo_get_currentVS3(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginDolphin_ilo_get_currentVS3_m2232934512 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginDolphin::ilo_get_isSdkLogin4(VersionMgr)
extern "C"  bool PluginDolphin_ilo_get_isSdkLogin4_m2979778737 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::ilo_Log5(System.Object,System.Boolean)
extern "C"  void PluginDolphin_ilo_Log5_m1246852497 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginDolphin::ilo_get_ProductsCfgMgr6()
extern "C"  ProductsCfgMgr_t2493714872 * PluginDolphin_ilo_get_ProductsCfgMgr6_m2921492741 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr PluginDolphin::ilo_get_FloatTextMgr7()
extern "C"  FloatTextMgr_t630384591 * PluginDolphin_ilo_get_FloatTextMgr7_m447178776 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginDolphin::ilo_GetProductID8(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginDolphin_ilo_GetProductID8_m2315694486 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.EnterGameInfo PluginDolphin::ilo_Parse9(System.Object[])
extern "C"  EnterGameInfo_t1897532954 * PluginDolphin_ilo_Parse9_m1329252794 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::ilo_Logout10(System.Action)
extern "C"  void PluginDolphin_ilo_Logout10_m1861289117 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginDolphin::ilo_get_PluginsSdkMgr11()
extern "C"  PluginsSdkMgr_t3884624670 * PluginDolphin_ilo_get_PluginsSdkMgr11_m1783749749 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDolphin::ilo_ReqSDKHttpLogin12(PluginsSdkMgr)
extern "C"  void PluginDolphin_ilo_ReqSDKHttpLogin12_m3036146019 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
