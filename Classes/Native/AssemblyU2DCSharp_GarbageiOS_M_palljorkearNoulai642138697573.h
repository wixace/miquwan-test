﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_palljorkearNoulai64
struct  M_palljorkearNoulai64_t2138697573  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_palljorkearNoulai64::_vutuGineevay
	int32_t ____vutuGineevay_0;
	// System.String GarbageiOS.M_palljorkearNoulai64::_caldai
	String_t* ____caldai_1;
	// System.Single GarbageiOS.M_palljorkearNoulai64::_faidairkair
	float ____faidairkair_2;
	// System.Single GarbageiOS.M_palljorkearNoulai64::_waleFoopa
	float ____waleFoopa_3;
	// System.Int32 GarbageiOS.M_palljorkearNoulai64::_gastrukee
	int32_t ____gastrukee_4;
	// System.Single GarbageiOS.M_palljorkearNoulai64::_navaSaydai
	float ____navaSaydai_5;
	// System.String GarbageiOS.M_palljorkearNoulai64::_surce
	String_t* ____surce_6;
	// System.Boolean GarbageiOS.M_palljorkearNoulai64::_rerela
	bool ____rerela_7;
	// System.Single GarbageiOS.M_palljorkearNoulai64::_kurkeZarchooso
	float ____kurkeZarchooso_8;
	// System.UInt32 GarbageiOS.M_palljorkearNoulai64::_neredeaBorbestur
	uint32_t ____neredeaBorbestur_9;
	// System.Boolean GarbageiOS.M_palljorkearNoulai64::_voba
	bool ____voba_10;
	// System.UInt32 GarbageiOS.M_palljorkearNoulai64::_lougoRowjas
	uint32_t ____lougoRowjas_11;
	// System.String GarbageiOS.M_palljorkearNoulai64::_yagestea
	String_t* ____yagestea_12;
	// System.String GarbageiOS.M_palljorkearNoulai64::_rallselbowTisirvay
	String_t* ____rallselbowTisirvay_13;
	// System.UInt32 GarbageiOS.M_palljorkearNoulai64::_nacouDrargoutra
	uint32_t ____nacouDrargoutra_14;
	// System.String GarbageiOS.M_palljorkearNoulai64::_sawjopeaSairsearde
	String_t* ____sawjopeaSairsearde_15;
	// System.String GarbageiOS.M_palljorkearNoulai64::_sinu
	String_t* ____sinu_16;
	// System.Boolean GarbageiOS.M_palljorkearNoulai64::_nojelne
	bool ____nojelne_17;
	// System.Boolean GarbageiOS.M_palljorkearNoulai64::_sifemsow
	bool ____sifemsow_18;
	// System.Int32 GarbageiOS.M_palljorkearNoulai64::_pifiVearwheargall
	int32_t ____pifiVearwheargall_19;
	// System.Int32 GarbageiOS.M_palljorkearNoulai64::_traynerejal
	int32_t ____traynerejal_20;
	// System.Single GarbageiOS.M_palljorkearNoulai64::_malltecaiStapurwe
	float ____malltecaiStapurwe_21;
	// System.String GarbageiOS.M_palljorkearNoulai64::_hurstem
	String_t* ____hurstem_22;
	// System.Int32 GarbageiOS.M_palljorkearNoulai64::_sairceatem
	int32_t ____sairceatem_23;
	// System.String GarbageiOS.M_palljorkearNoulai64::_jashe
	String_t* ____jashe_24;
	// System.Boolean GarbageiOS.M_palljorkearNoulai64::_saheesuSucouwair
	bool ____saheesuSucouwair_25;
	// System.UInt32 GarbageiOS.M_palljorkearNoulai64::_selporstere
	uint32_t ____selporstere_26;
	// System.Single GarbageiOS.M_palljorkearNoulai64::_tisseatar
	float ____tisseatar_27;

public:
	inline static int32_t get_offset_of__vutuGineevay_0() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____vutuGineevay_0)); }
	inline int32_t get__vutuGineevay_0() const { return ____vutuGineevay_0; }
	inline int32_t* get_address_of__vutuGineevay_0() { return &____vutuGineevay_0; }
	inline void set__vutuGineevay_0(int32_t value)
	{
		____vutuGineevay_0 = value;
	}

	inline static int32_t get_offset_of__caldai_1() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____caldai_1)); }
	inline String_t* get__caldai_1() const { return ____caldai_1; }
	inline String_t** get_address_of__caldai_1() { return &____caldai_1; }
	inline void set__caldai_1(String_t* value)
	{
		____caldai_1 = value;
		Il2CppCodeGenWriteBarrier(&____caldai_1, value);
	}

	inline static int32_t get_offset_of__faidairkair_2() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____faidairkair_2)); }
	inline float get__faidairkair_2() const { return ____faidairkair_2; }
	inline float* get_address_of__faidairkair_2() { return &____faidairkair_2; }
	inline void set__faidairkair_2(float value)
	{
		____faidairkair_2 = value;
	}

	inline static int32_t get_offset_of__waleFoopa_3() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____waleFoopa_3)); }
	inline float get__waleFoopa_3() const { return ____waleFoopa_3; }
	inline float* get_address_of__waleFoopa_3() { return &____waleFoopa_3; }
	inline void set__waleFoopa_3(float value)
	{
		____waleFoopa_3 = value;
	}

	inline static int32_t get_offset_of__gastrukee_4() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____gastrukee_4)); }
	inline int32_t get__gastrukee_4() const { return ____gastrukee_4; }
	inline int32_t* get_address_of__gastrukee_4() { return &____gastrukee_4; }
	inline void set__gastrukee_4(int32_t value)
	{
		____gastrukee_4 = value;
	}

	inline static int32_t get_offset_of__navaSaydai_5() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____navaSaydai_5)); }
	inline float get__navaSaydai_5() const { return ____navaSaydai_5; }
	inline float* get_address_of__navaSaydai_5() { return &____navaSaydai_5; }
	inline void set__navaSaydai_5(float value)
	{
		____navaSaydai_5 = value;
	}

	inline static int32_t get_offset_of__surce_6() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____surce_6)); }
	inline String_t* get__surce_6() const { return ____surce_6; }
	inline String_t** get_address_of__surce_6() { return &____surce_6; }
	inline void set__surce_6(String_t* value)
	{
		____surce_6 = value;
		Il2CppCodeGenWriteBarrier(&____surce_6, value);
	}

	inline static int32_t get_offset_of__rerela_7() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____rerela_7)); }
	inline bool get__rerela_7() const { return ____rerela_7; }
	inline bool* get_address_of__rerela_7() { return &____rerela_7; }
	inline void set__rerela_7(bool value)
	{
		____rerela_7 = value;
	}

	inline static int32_t get_offset_of__kurkeZarchooso_8() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____kurkeZarchooso_8)); }
	inline float get__kurkeZarchooso_8() const { return ____kurkeZarchooso_8; }
	inline float* get_address_of__kurkeZarchooso_8() { return &____kurkeZarchooso_8; }
	inline void set__kurkeZarchooso_8(float value)
	{
		____kurkeZarchooso_8 = value;
	}

	inline static int32_t get_offset_of__neredeaBorbestur_9() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____neredeaBorbestur_9)); }
	inline uint32_t get__neredeaBorbestur_9() const { return ____neredeaBorbestur_9; }
	inline uint32_t* get_address_of__neredeaBorbestur_9() { return &____neredeaBorbestur_9; }
	inline void set__neredeaBorbestur_9(uint32_t value)
	{
		____neredeaBorbestur_9 = value;
	}

	inline static int32_t get_offset_of__voba_10() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____voba_10)); }
	inline bool get__voba_10() const { return ____voba_10; }
	inline bool* get_address_of__voba_10() { return &____voba_10; }
	inline void set__voba_10(bool value)
	{
		____voba_10 = value;
	}

	inline static int32_t get_offset_of__lougoRowjas_11() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____lougoRowjas_11)); }
	inline uint32_t get__lougoRowjas_11() const { return ____lougoRowjas_11; }
	inline uint32_t* get_address_of__lougoRowjas_11() { return &____lougoRowjas_11; }
	inline void set__lougoRowjas_11(uint32_t value)
	{
		____lougoRowjas_11 = value;
	}

	inline static int32_t get_offset_of__yagestea_12() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____yagestea_12)); }
	inline String_t* get__yagestea_12() const { return ____yagestea_12; }
	inline String_t** get_address_of__yagestea_12() { return &____yagestea_12; }
	inline void set__yagestea_12(String_t* value)
	{
		____yagestea_12 = value;
		Il2CppCodeGenWriteBarrier(&____yagestea_12, value);
	}

	inline static int32_t get_offset_of__rallselbowTisirvay_13() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____rallselbowTisirvay_13)); }
	inline String_t* get__rallselbowTisirvay_13() const { return ____rallselbowTisirvay_13; }
	inline String_t** get_address_of__rallselbowTisirvay_13() { return &____rallselbowTisirvay_13; }
	inline void set__rallselbowTisirvay_13(String_t* value)
	{
		____rallselbowTisirvay_13 = value;
		Il2CppCodeGenWriteBarrier(&____rallselbowTisirvay_13, value);
	}

	inline static int32_t get_offset_of__nacouDrargoutra_14() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____nacouDrargoutra_14)); }
	inline uint32_t get__nacouDrargoutra_14() const { return ____nacouDrargoutra_14; }
	inline uint32_t* get_address_of__nacouDrargoutra_14() { return &____nacouDrargoutra_14; }
	inline void set__nacouDrargoutra_14(uint32_t value)
	{
		____nacouDrargoutra_14 = value;
	}

	inline static int32_t get_offset_of__sawjopeaSairsearde_15() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____sawjopeaSairsearde_15)); }
	inline String_t* get__sawjopeaSairsearde_15() const { return ____sawjopeaSairsearde_15; }
	inline String_t** get_address_of__sawjopeaSairsearde_15() { return &____sawjopeaSairsearde_15; }
	inline void set__sawjopeaSairsearde_15(String_t* value)
	{
		____sawjopeaSairsearde_15 = value;
		Il2CppCodeGenWriteBarrier(&____sawjopeaSairsearde_15, value);
	}

	inline static int32_t get_offset_of__sinu_16() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____sinu_16)); }
	inline String_t* get__sinu_16() const { return ____sinu_16; }
	inline String_t** get_address_of__sinu_16() { return &____sinu_16; }
	inline void set__sinu_16(String_t* value)
	{
		____sinu_16 = value;
		Il2CppCodeGenWriteBarrier(&____sinu_16, value);
	}

	inline static int32_t get_offset_of__nojelne_17() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____nojelne_17)); }
	inline bool get__nojelne_17() const { return ____nojelne_17; }
	inline bool* get_address_of__nojelne_17() { return &____nojelne_17; }
	inline void set__nojelne_17(bool value)
	{
		____nojelne_17 = value;
	}

	inline static int32_t get_offset_of__sifemsow_18() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____sifemsow_18)); }
	inline bool get__sifemsow_18() const { return ____sifemsow_18; }
	inline bool* get_address_of__sifemsow_18() { return &____sifemsow_18; }
	inline void set__sifemsow_18(bool value)
	{
		____sifemsow_18 = value;
	}

	inline static int32_t get_offset_of__pifiVearwheargall_19() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____pifiVearwheargall_19)); }
	inline int32_t get__pifiVearwheargall_19() const { return ____pifiVearwheargall_19; }
	inline int32_t* get_address_of__pifiVearwheargall_19() { return &____pifiVearwheargall_19; }
	inline void set__pifiVearwheargall_19(int32_t value)
	{
		____pifiVearwheargall_19 = value;
	}

	inline static int32_t get_offset_of__traynerejal_20() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____traynerejal_20)); }
	inline int32_t get__traynerejal_20() const { return ____traynerejal_20; }
	inline int32_t* get_address_of__traynerejal_20() { return &____traynerejal_20; }
	inline void set__traynerejal_20(int32_t value)
	{
		____traynerejal_20 = value;
	}

	inline static int32_t get_offset_of__malltecaiStapurwe_21() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____malltecaiStapurwe_21)); }
	inline float get__malltecaiStapurwe_21() const { return ____malltecaiStapurwe_21; }
	inline float* get_address_of__malltecaiStapurwe_21() { return &____malltecaiStapurwe_21; }
	inline void set__malltecaiStapurwe_21(float value)
	{
		____malltecaiStapurwe_21 = value;
	}

	inline static int32_t get_offset_of__hurstem_22() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____hurstem_22)); }
	inline String_t* get__hurstem_22() const { return ____hurstem_22; }
	inline String_t** get_address_of__hurstem_22() { return &____hurstem_22; }
	inline void set__hurstem_22(String_t* value)
	{
		____hurstem_22 = value;
		Il2CppCodeGenWriteBarrier(&____hurstem_22, value);
	}

	inline static int32_t get_offset_of__sairceatem_23() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____sairceatem_23)); }
	inline int32_t get__sairceatem_23() const { return ____sairceatem_23; }
	inline int32_t* get_address_of__sairceatem_23() { return &____sairceatem_23; }
	inline void set__sairceatem_23(int32_t value)
	{
		____sairceatem_23 = value;
	}

	inline static int32_t get_offset_of__jashe_24() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____jashe_24)); }
	inline String_t* get__jashe_24() const { return ____jashe_24; }
	inline String_t** get_address_of__jashe_24() { return &____jashe_24; }
	inline void set__jashe_24(String_t* value)
	{
		____jashe_24 = value;
		Il2CppCodeGenWriteBarrier(&____jashe_24, value);
	}

	inline static int32_t get_offset_of__saheesuSucouwair_25() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____saheesuSucouwair_25)); }
	inline bool get__saheesuSucouwair_25() const { return ____saheesuSucouwair_25; }
	inline bool* get_address_of__saheesuSucouwair_25() { return &____saheesuSucouwair_25; }
	inline void set__saheesuSucouwair_25(bool value)
	{
		____saheesuSucouwair_25 = value;
	}

	inline static int32_t get_offset_of__selporstere_26() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____selporstere_26)); }
	inline uint32_t get__selporstere_26() const { return ____selporstere_26; }
	inline uint32_t* get_address_of__selporstere_26() { return &____selporstere_26; }
	inline void set__selporstere_26(uint32_t value)
	{
		____selporstere_26 = value;
	}

	inline static int32_t get_offset_of__tisseatar_27() { return static_cast<int32_t>(offsetof(M_palljorkearNoulai64_t2138697573, ____tisseatar_27)); }
	inline float get__tisseatar_27() const { return ____tisseatar_27; }
	inline float* get_address_of__tisseatar_27() { return &____tisseatar_27; }
	inline void set__tisseatar_27(float value)
	{
		____tisseatar_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
