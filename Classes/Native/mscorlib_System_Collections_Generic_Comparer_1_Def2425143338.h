﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Collections_Generic_Comparer_1_gen1873683552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Comparer`1/DefaultComparer<Newtonsoft.Json.Linq.JTokenType>
struct  DefaultComparer_t2425143338  : public Comparer_1_t1873683552
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
