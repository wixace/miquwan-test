﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.AttributeMap
struct AttributeMap_t924393598;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.Meta.AttributeMap[]
struct AttributeMapU5BU5D_t2350548939;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.Assembly
struct Assembly_t1418687608;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "mscorlib_System_Reflection_Assembly1418687608.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_AttributeMap924393598.h"

// System.Void ProtoBuf.Meta.AttributeMap::.ctor()
extern "C"  void AttributeMap__ctor_m2284795029 (AttributeMap_t924393598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.AttributeMap::TryGet(System.String,System.Object&)
extern "C"  bool AttributeMap_TryGet_m3384343194 (AttributeMap_t924393598 * __this, String_t* ___key0, Il2CppObject ** ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.AttributeMap[] ProtoBuf.Meta.AttributeMap::Create(ProtoBuf.Meta.TypeModel,System.Type,System.Boolean)
extern "C"  AttributeMapU5BU5D_t2350548939* AttributeMap_Create_m1202063745 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Type_t * ___type1, bool ___inherit2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.AttributeMap[] ProtoBuf.Meta.AttributeMap::Create(ProtoBuf.Meta.TypeModel,System.Reflection.MemberInfo,System.Boolean)
extern "C"  AttributeMapU5BU5D_t2350548939* AttributeMap_Create_m1135261868 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, MemberInfo_t * ___member1, bool ___inherit2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.AttributeMap[] ProtoBuf.Meta.AttributeMap::Create(ProtoBuf.Meta.TypeModel,System.Reflection.Assembly)
extern "C"  AttributeMapU5BU5D_t2350548939* AttributeMap_Create_m2818914035 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Assembly_t1418687608 * ___assembly1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.AttributeMap::ilo_TryGet1(ProtoBuf.Meta.AttributeMap,System.String,System.Boolean,System.Object&)
extern "C"  bool AttributeMap_ilo_TryGet1_m3579029817 (Il2CppObject * __this /* static, unused */, AttributeMap_t924393598 * ____this0, String_t* ___key1, bool ___publicOnly2, Il2CppObject ** ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
