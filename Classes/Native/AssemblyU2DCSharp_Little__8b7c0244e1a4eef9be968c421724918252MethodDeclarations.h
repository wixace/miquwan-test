﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._8b7c0244e1a4eef9be968c4282df2963
struct _8b7c0244e1a4eef9be968c4282df2963_t1724918252;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._8b7c0244e1a4eef9be968c4282df2963::.ctor()
extern "C"  void _8b7c0244e1a4eef9be968c4282df2963__ctor_m4123354561 (_8b7c0244e1a4eef9be968c4282df2963_t1724918252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8b7c0244e1a4eef9be968c4282df2963::_8b7c0244e1a4eef9be968c4282df2963m2(System.Int32)
extern "C"  int32_t _8b7c0244e1a4eef9be968c4282df2963__8b7c0244e1a4eef9be968c4282df2963m2_m1007512089 (_8b7c0244e1a4eef9be968c4282df2963_t1724918252 * __this, int32_t ____8b7c0244e1a4eef9be968c4282df2963a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8b7c0244e1a4eef9be968c4282df2963::_8b7c0244e1a4eef9be968c4282df2963m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _8b7c0244e1a4eef9be968c4282df2963__8b7c0244e1a4eef9be968c4282df2963m_m3946040509 (_8b7c0244e1a4eef9be968c4282df2963_t1724918252 * __this, int32_t ____8b7c0244e1a4eef9be968c4282df2963a0, int32_t ____8b7c0244e1a4eef9be968c4282df296301, int32_t ____8b7c0244e1a4eef9be968c4282df2963c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
