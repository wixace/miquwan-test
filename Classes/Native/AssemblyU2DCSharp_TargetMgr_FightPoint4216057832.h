﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<TargetMgr/AngleEntity>>
struct Dictionary_2_t264609637;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TargetMgr/FightPoint
struct  FightPoint_t4216057832  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 TargetMgr/FightPoint::mNormalize
	Vector3_t4282066566  ___mNormalize_0;
	// UnityEngine.Vector3 TargetMgr/FightPoint::targerPos
	Vector3_t4282066566  ___targerPos_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<TargetMgr/AngleEntity>> TargetMgr/FightPoint::nearAngerDic
	Dictionary_2_t264609637 * ___nearAngerDic_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<TargetMgr/AngleEntity>> TargetMgr/FightPoint::farAngerDic
	Dictionary_2_t264609637 * ___farAngerDic_3;

public:
	inline static int32_t get_offset_of_mNormalize_0() { return static_cast<int32_t>(offsetof(FightPoint_t4216057832, ___mNormalize_0)); }
	inline Vector3_t4282066566  get_mNormalize_0() const { return ___mNormalize_0; }
	inline Vector3_t4282066566 * get_address_of_mNormalize_0() { return &___mNormalize_0; }
	inline void set_mNormalize_0(Vector3_t4282066566  value)
	{
		___mNormalize_0 = value;
	}

	inline static int32_t get_offset_of_targerPos_1() { return static_cast<int32_t>(offsetof(FightPoint_t4216057832, ___targerPos_1)); }
	inline Vector3_t4282066566  get_targerPos_1() const { return ___targerPos_1; }
	inline Vector3_t4282066566 * get_address_of_targerPos_1() { return &___targerPos_1; }
	inline void set_targerPos_1(Vector3_t4282066566  value)
	{
		___targerPos_1 = value;
	}

	inline static int32_t get_offset_of_nearAngerDic_2() { return static_cast<int32_t>(offsetof(FightPoint_t4216057832, ___nearAngerDic_2)); }
	inline Dictionary_2_t264609637 * get_nearAngerDic_2() const { return ___nearAngerDic_2; }
	inline Dictionary_2_t264609637 ** get_address_of_nearAngerDic_2() { return &___nearAngerDic_2; }
	inline void set_nearAngerDic_2(Dictionary_2_t264609637 * value)
	{
		___nearAngerDic_2 = value;
		Il2CppCodeGenWriteBarrier(&___nearAngerDic_2, value);
	}

	inline static int32_t get_offset_of_farAngerDic_3() { return static_cast<int32_t>(offsetof(FightPoint_t4216057832, ___farAngerDic_3)); }
	inline Dictionary_2_t264609637 * get_farAngerDic_3() const { return ___farAngerDic_3; }
	inline Dictionary_2_t264609637 ** get_address_of_farAngerDic_3() { return &___farAngerDic_3; }
	inline void set_farAngerDic_3(Dictionary_2_t264609637 * value)
	{
		___farAngerDic_3 = value;
		Il2CppCodeGenWriteBarrier(&___farAngerDic_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
