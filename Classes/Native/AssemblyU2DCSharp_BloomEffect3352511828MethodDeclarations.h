﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BloomEffect
struct BloomEffect_t3352511828;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;
// PostEffectBase
struct PostEffectBase_t3382443234;
// UnityEngine.Shader
struct Shader_t3191267369;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"
#include "AssemblyU2DCSharp_PostEffectBase3382443234.h"
#include "UnityEngine_UnityEngine_Shader3191267369.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"

// System.Void BloomEffect::.ctor()
extern "C"  void BloomEffect__ctor_m2622420119 (BloomEffect_t3352511828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material BloomEffect::get_material()
extern "C"  Material_t3870600107 * BloomEffect_get_material_m2887409822 (BloomEffect_t3352511828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BloomEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void BloomEffect_OnRenderImage_m1878687079 (BloomEffect_t3352511828 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material BloomEffect::ilo_CheckShaderAndCreateMaterial1(PostEffectBase,UnityEngine.Shader,UnityEngine.Material)
extern "C"  Material_t3870600107 * BloomEffect_ilo_CheckShaderAndCreateMaterial1_m1774805569 (Il2CppObject * __this /* static, unused */, PostEffectBase_t3382443234 * ____this0, Shader_t3191267369 * ___shader1, Material_t3870600107 * ___material2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
