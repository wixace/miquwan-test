﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_PathPool_1_gen4134265632MethodDeclarations.h"

// System.Void Pathfinding.PathPool`1<Pathfinding.FloodPath>::.cctor()
#define PathPool_1__cctor_m3386566407(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PathPool_1__cctor_m4111071586_gshared)(__this /* static, unused */, method)
// System.Void Pathfinding.PathPool`1<Pathfinding.FloodPath>::Recycle(T)
#define PathPool_1_Recycle_m895892647(__this /* static, unused */, ___path0, method) ((  void (*) (Il2CppObject * /* static, unused */, FloodPath_t3766979749 *, const MethodInfo*))PathPool_1_Recycle_m1360667714_gshared)(__this /* static, unused */, ___path0, method)
// System.Void Pathfinding.PathPool`1<Pathfinding.FloodPath>::Warmup(System.Int32,System.Int32)
#define PathPool_1_Warmup_m2501739816(__this /* static, unused */, ___count0, ___length1, method) ((  void (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))PathPool_1_Warmup_m4243013421_gshared)(__this /* static, unused */, ___count0, ___length1, method)
// System.Int32 Pathfinding.PathPool`1<Pathfinding.FloodPath>::GetTotalCreated()
#define PathPool_1_GetTotalCreated_m891175884(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PathPool_1_GetTotalCreated_m3938489809_gshared)(__this /* static, unused */, method)
// System.Int32 Pathfinding.PathPool`1<Pathfinding.FloodPath>::GetSize()
#define PathPool_1_GetSize_m954910857(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PathPool_1_GetSize_m2941120398_gshared)(__this /* static, unused */, method)
// T Pathfinding.PathPool`1<Pathfinding.FloodPath>::GetPath()
#define PathPool_1_GetPath_m1312806430(__this /* static, unused */, method) ((  FloodPath_t3766979749 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PathPool_1_GetPath_m3122724387_gshared)(__this /* static, unused */, method)
// System.Void Pathfinding.PathPool`1<Pathfinding.FloodPath>::ilo_Claim1(Pathfinding.Path,System.Object)
#define PathPool_1_ilo_Claim1_m3476138213(__this /* static, unused */, ____this0, ___o1, method) ((  void (*) (Il2CppObject * /* static, unused */, Path_t1974241691 *, Il2CppObject *, const MethodInfo*))PathPool_1_ilo_Claim1_m992502912_gshared)(__this /* static, unused */, ____this0, ___o1, method)
// System.Void Pathfinding.PathPool`1<Pathfinding.FloodPath>::ilo_Release2(Pathfinding.Path,System.Object)
#define PathPool_1_ilo_Release2_m1344972411(__this /* static, unused */, ____this0, ___o1, method) ((  void (*) (Il2CppObject * /* static, unused */, Path_t1974241691 *, Il2CppObject *, const MethodInfo*))PathPool_1_ilo_Release2_m2573264726_gshared)(__this /* static, unused */, ____this0, ___o1, method)
