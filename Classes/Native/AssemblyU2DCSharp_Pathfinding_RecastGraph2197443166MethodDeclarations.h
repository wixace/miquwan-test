﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RecastGraph
struct RecastGraph_t2197443166;
// Pathfinding.GraphNodeDelegateCancelable
struct GraphNodeDelegateCancelable_t3591372971;
// Pathfinding.BBTree
struct BBTree_t1216325332;
// Pathfinding.Int3[]
struct Int3U5BU5D_t516284607;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// Pathfinding.RecastGraph/NavmeshTile[]
struct NavmeshTileU5BU5D_t3225070876;
// Pathfinding.RecastGraph/NavmeshTile
struct NavmeshTile_t4022309569;
// Pathfinding.TriangleMeshNode
struct TriangleMeshNode_t1626248749;
// System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>
struct List_1_t1291247971;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// Pathfinding.GraphUpdateObject
struct GraphUpdateObject_t430843704;
// Pathfinding.NNConstraint
struct NNConstraint_t758567699;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// System.Collections.Generic.List`1<Pathfinding.GraphNode>
struct List_1_t1391797922;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// System.String
struct String_t;
// OnScanStatus
struct OnScanStatus_t2412749870;
// Pathfinding.Voxels.Voxelize
struct Voxelize_t3998270866;
// Pathfinding.TriangleMeshNode[]
struct TriangleMeshNodeU5BU5D_t2064970368;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// UnityEngine.Terrain
struct Terrain_t2520838763;
// UnityEngine.Collider
struct Collider_t2939674232;
// Pathfinding.Serialization.GraphSerializationContext
struct GraphSerializationContext_t3256954663;
// Pathfinding.NavGraph
struct NavGraph_t1254319713;
// Pathfinding.AstarData
struct AstarData_t3283402719;
// Pathfinding.INavmeshHolder
struct INavmeshHolder_t1019551337;
// System.Collections.Generic.List`1<Pathfinding.RecastMeshObj>
struct List_1_t3437381290;
// Pathfinding.INavmesh
struct INavmesh_t889550173;
// Pathfinding.MeshNode
struct MeshNode_t3005053445;
// Pathfinding.Voxels.VoxelContourSet
struct VoxelContourSet_t2502455108;
// Pathfinding.PathHandler
struct PathHandler_t918952263;
// AstarPath
struct AstarPath_t4090270936;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNodeDelegateCan3591372971.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "AssemblyU2DCSharp_Pathfinding_BBTree1216325332.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "AssemblyU2DCSharp_Pathfinding_IntRect3015058261.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_TriangleMeshNode1626248749.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"
#include "AssemblyU2DCSharp_GraphUpdateThreading3958092737.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphUpdateObject430843704.h"
#include "AssemblyU2DCSharp_Pathfinding_RecastGraph_NavmeshT4022309569.h"
#include "AssemblyU2DCSharp_Pathfinding_NNInfo1570625892.h"
#include "AssemblyU2DCSharp_Pathfinding_NNConstraint758567699.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_OnScanStatus2412749870.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_Voxelize3998270866.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_VoxelMesh3943678281.h"
#include "UnityEngine_UnityEngine_Terrain2520838763.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_ExtraMesh4218029715.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphHitInfo3082627369.h"
#include "AssemblyU2DCSharp_Pathfinding_Serialization_GraphS3256954663.h"
#include "AssemblyU2DCSharp_Pathfinding_NavGraph1254319713.h"
#include "AssemblyU2DCSharp_Pathfinding_AstarData3283402719.h"
#include "AssemblyU2DCSharp_Pathfinding_RecastGraph2197443166.h"
#include "AssemblyU2DCSharp_Pathfinding_Progress2476786339.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_VoxelContourS2502455108.h"
#include "AssemblyU2DCSharp_Pathfinding_MeshNode3005053445.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"

// System.Void Pathfinding.RecastGraph::.ctor()
extern "C"  void RecastGraph__ctor_m3941093017 (RecastGraph_t2197443166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::Pathfinding.INavmesh.GetNodes(Pathfinding.GraphNodeDelegateCancelable)
extern "C"  void RecastGraph_Pathfinding_INavmesh_GetNodes_m2837442532 (RecastGraph_t2197443166 * __this, GraphNodeDelegateCancelable_t3591372971 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::CreateNodes(System.Int32)
extern "C"  void RecastGraph_CreateNodes_m1873224797 (RecastGraph_t2197443166 * __this, int32_t ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds Pathfinding.RecastGraph::get_forcedBounds()
extern "C"  Bounds_t2711641849  RecastGraph_get_forcedBounds_m1364619761 (RecastGraph_t2197443166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.BBTree Pathfinding.RecastGraph::get_bbTree()
extern "C"  BBTree_t1216325332 * RecastGraph_get_bbTree_m2405799517 (RecastGraph_t2197443166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::set_bbTree(Pathfinding.BBTree)
extern "C"  void RecastGraph_set_bbTree_m381814238 (RecastGraph_t2197443166 * __this, BBTree_t1216325332 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3[] Pathfinding.RecastGraph::get_vertices()
extern "C"  Int3U5BU5D_t516284607* RecastGraph_get_vertices_m2773165248 (RecastGraph_t2197443166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::set_vertices(Pathfinding.Int3[])
extern "C"  void RecastGraph_set_vertices_m741265649 (RecastGraph_t2197443166 * __this, Int3U5BU5D_t516284607* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] Pathfinding.RecastGraph::get_vectorVertices()
extern "C"  Vector3U5BU5D_t215400611* RecastGraph_get_vectorVertices_m2746206264 (RecastGraph_t2197443166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.RecastGraph::GetVertex(System.Int32)
extern "C"  Int3_t1974045594  RecastGraph_GetVertex_m3092712401 (RecastGraph_t2197443166 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.RecastGraph::GetTileIndex(System.Int32)
extern "C"  int32_t RecastGraph_GetTileIndex_m738848028 (RecastGraph_t2197443166 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.RecastGraph::GetVertexArrayIndex(System.Int32)
extern "C"  int32_t RecastGraph_GetVertexArrayIndex_m732128649 (RecastGraph_t2197443166 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::GetTileCoordinates(System.Int32,System.Int32&,System.Int32&)
extern "C"  void RecastGraph_GetTileCoordinates_m2815541053 (RecastGraph_t2197443166 * __this, int32_t ___tileIndex0, int32_t* ___x1, int32_t* ___z2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RecastGraph/NavmeshTile[] Pathfinding.RecastGraph::GetTiles()
extern "C"  NavmeshTileU5BU5D_t3225070876* RecastGraph_GetTiles_m4132792394 (RecastGraph_t2197443166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds Pathfinding.RecastGraph::GetTileBounds(Pathfinding.IntRect)
extern "C"  Bounds_t2711641849  RecastGraph_GetTileBounds_m2014944680 (RecastGraph_t2197443166 * __this, IntRect_t3015058261  ___rect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds Pathfinding.RecastGraph::GetTileBounds(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  Bounds_t2711641849  RecastGraph_GetTileBounds_m2837860517 (RecastGraph_t2197443166 * __this, int32_t ___x0, int32_t ___z1, int32_t ___width2, int32_t ___depth3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int2 Pathfinding.RecastGraph::GetTileCoordinates(UnityEngine.Vector3)
extern "C"  Int2_t1974045593  RecastGraph_GetTileCoordinates_m818580263 (RecastGraph_t2197443166 * __this, Vector3_t4282066566  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::OnDestroy()
extern "C"  void RecastGraph_OnDestroy_m441397010 (RecastGraph_t2197443166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RecastGraph/NavmeshTile Pathfinding.RecastGraph::NewEmptyTile(System.Int32,System.Int32)
extern "C"  NavmeshTile_t4022309569 * RecastGraph_NewEmptyTile_m3098341170 (Il2CppObject * __this /* static, unused */, int32_t ___x0, int32_t ___z1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::GetNodes(Pathfinding.GraphNodeDelegateCancelable)
extern "C"  void RecastGraph_GetNodes_m2603817325 (RecastGraph_t2197443166 * __this, GraphNodeDelegateCancelable_t3591372971 * ___del0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RecastGraph::ClosestPointOnNode(Pathfinding.TriangleMeshNode,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  RecastGraph_ClosestPointOnNode_m1302092731 (RecastGraph_t2197443166 * __this, TriangleMeshNode_t1626248749 * ___node0, Vector3_t4282066566  ___pos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastGraph::ContainsPoint(Pathfinding.TriangleMeshNode,UnityEngine.Vector3)
extern "C"  bool RecastGraph_ContainsPoint_m2564836782 (RecastGraph_t2197443166 * __this, TriangleMeshNode_t1626248749 * ___node0, Vector3_t4282066566  ___pos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::SnapForceBoundsToScene()
extern "C"  void RecastGraph_SnapForceBoundsToScene_m741421286 (RecastGraph_t2197443166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::GetRecastMeshObjs(UnityEngine.Bounds,System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>)
extern "C"  void RecastGraph_GetRecastMeshObjs_m3395477070 (RecastGraph_t2197443166 * __this, Bounds_t2711641849  ___bounds0, List_1_t1291247971 * ___buffer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::GetSceneMeshes(UnityEngine.Bounds,System.Collections.Generic.List`1<System.String>,UnityEngine.LayerMask,System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>)
extern "C"  void RecastGraph_GetSceneMeshes_m674568322 (Il2CppObject * __this /* static, unused */, Bounds_t2711641849  ___bounds0, List_1_t1375417109 * ___tagMask1, LayerMask_t3236759763  ___layerMask2, List_1_t1291247971 * ___meshes3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.IntRect Pathfinding.RecastGraph::GetTouchingTiles(UnityEngine.Bounds)
extern "C"  IntRect_t3015058261  RecastGraph_GetTouchingTiles_m1284414145 (RecastGraph_t2197443166 * __this, Bounds_t2711641849  ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.IntRect Pathfinding.RecastGraph::GetTouchingTilesRound(UnityEngine.Bounds)
extern "C"  IntRect_t3015058261  RecastGraph_GetTouchingTilesRound_m3930294999 (RecastGraph_t2197443166 * __this, Bounds_t2711641849  ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GraphUpdateThreading Pathfinding.RecastGraph::CanUpdateAsync(Pathfinding.GraphUpdateObject)
extern "C"  int32_t RecastGraph_CanUpdateAsync_m2093863700 (RecastGraph_t2197443166 * __this, GraphUpdateObject_t430843704 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::UpdateAreaInit(Pathfinding.GraphUpdateObject)
extern "C"  void RecastGraph_UpdateAreaInit_m1374568501 (RecastGraph_t2197443166 * __this, GraphUpdateObject_t430843704 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::UpdateArea(Pathfinding.GraphUpdateObject)
extern "C"  void RecastGraph_UpdateArea_m2373255013 (RecastGraph_t2197443166 * __this, GraphUpdateObject_t430843704 * ___guo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ConnectTileWithNeighbours(Pathfinding.RecastGraph/NavmeshTile)
extern "C"  void RecastGraph_ConnectTileWithNeighbours_m4080159878 (RecastGraph_t2197443166 * __this, NavmeshTile_t4022309569 * ___tile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::RemoveConnectionsFromTile(Pathfinding.RecastGraph/NavmeshTile)
extern "C"  void RecastGraph_RemoveConnectionsFromTile_m2735937809 (RecastGraph_t2197443166 * __this, NavmeshTile_t4022309569 * ___tile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::RemoveConnectionsFromTo(Pathfinding.RecastGraph/NavmeshTile,Pathfinding.RecastGraph/NavmeshTile)
extern "C"  void RecastGraph_RemoveConnectionsFromTo_m1031810863 (RecastGraph_t2197443166 * __this, NavmeshTile_t4022309569 * ___a0, NavmeshTile_t4022309569 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.RecastGraph::GetNearest(UnityEngine.Vector3,Pathfinding.NNConstraint,Pathfinding.GraphNode)
extern "C"  NNInfo_t1570625892  RecastGraph_GetNearest_m2788200698 (RecastGraph_t2197443166 * __this, Vector3_t4282066566  ___position0, NNConstraint_t758567699 * ___constraint1, GraphNode_t23612370 * ___hint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.RecastGraph::GetNearestForce(UnityEngine.Vector3,Pathfinding.NNConstraint)
extern "C"  NNInfo_t1570625892  RecastGraph_GetNearestForce_m590321317 (RecastGraph_t2197443166 * __this, Vector3_t4282066566  ___position0, NNConstraint_t758567699 * ___constraint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.GraphNode Pathfinding.RecastGraph::PointOnNavmesh(UnityEngine.Vector3,Pathfinding.NNConstraint)
extern "C"  GraphNode_t23612370 * RecastGraph_PointOnNavmesh_m2479334597 (RecastGraph_t2197443166 * __this, Vector3_t4282066566  ___position0, NNConstraint_t758567699 * ___constraint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::BuildFunnelCorridor(System.Collections.Generic.List`1<Pathfinding.GraphNode>,System.Int32,System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void RecastGraph_BuildFunnelCorridor_m669631247 (RecastGraph_t2197443166 * __this, List_1_t1391797922 * ___path0, int32_t ___startIndex1, int32_t ___endIndex2, List_1_t1355284822 * ___left3, List_1_t1355284822 * ___right4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::AddPortal(Pathfinding.GraphNode,Pathfinding.GraphNode,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void RecastGraph_AddPortal_m3442379726 (RecastGraph_t2197443166 * __this, GraphNode_t23612370 * ___n10, GraphNode_t23612370 * ___n21, List_1_t1355284822 * ___left2, List_1_t1355284822 * ___right3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.RecastGraph::GetRecastPath()
extern "C"  String_t* RecastGraph_GetRecastPath_m2474025159 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ScanInternal(OnScanStatus)
extern "C"  void RecastGraph_ScanInternal_m1339874039 (RecastGraph_t2197443166 * __this, OnScanStatus_t2412749870 * ___statusCallback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ScanTiledNavmesh(OnScanStatus)
extern "C"  void RecastGraph_ScanTiledNavmesh_m4240393012 (RecastGraph_t2197443166 * __this, OnScanStatus_t2412749870 * ___statusCallback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ScanAllTiles(OnScanStatus)
extern "C"  void RecastGraph_ScanAllTiles_m2381355614 (RecastGraph_t2197443166 * __this, OnScanStatus_t2412749870 * ___statusCallback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::BuildTileMesh(Pathfinding.Voxels.Voxelize,System.Int32,System.Int32)
extern "C"  void RecastGraph_BuildTileMesh_m2863021837 (RecastGraph_t2197443166 * __this, Voxelize_t3998270866 * ___vox0, int32_t ___x1, int32_t ___z2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RecastGraph/NavmeshTile Pathfinding.RecastGraph::CreateTile(Pathfinding.Voxels.Voxelize,Pathfinding.Voxels.VoxelMesh,System.Int32,System.Int32)
extern "C"  NavmeshTile_t4022309569 * RecastGraph_CreateTile_m1444036192 (RecastGraph_t2197443166 * __this, Voxelize_t3998270866 * ___vox0, VoxelMesh_t3943678281  ___mesh1, int32_t ___x2, int32_t ___z3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::CreateNodeConnections(Pathfinding.TriangleMeshNode[])
extern "C"  void RecastGraph_CreateNodeConnections_m1401067 (RecastGraph_t2197443166 * __this, TriangleMeshNodeU5BU5D_t2064970368* ___nodes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ConnectTiles(Pathfinding.RecastGraph/NavmeshTile,Pathfinding.RecastGraph/NavmeshTile)
extern "C"  void RecastGraph_ConnectTiles_m1618403222 (RecastGraph_t2197443166 * __this, NavmeshTile_t4022309569 * ___tile10, NavmeshTile_t4022309569 * ___tile21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::StartBatchTileUpdate()
extern "C"  void RecastGraph_StartBatchTileUpdate_m701716218 (RecastGraph_t2197443166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::EndBatchTileUpdate()
extern "C"  void RecastGraph_EndBatchTileUpdate_m178720289 (RecastGraph_t2197443166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ReplaceTile(System.Int32,System.Int32,Pathfinding.Int3[],System.Int32[],System.Boolean)
extern "C"  void RecastGraph_ReplaceTile_m932568529 (RecastGraph_t2197443166 * __this, int32_t ___x0, int32_t ___z1, Int3U5BU5D_t516284607* ___verts2, Int32U5BU5D_t3230847821* ___tris3, bool ___worldSpace4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ReplaceTile(System.Int32,System.Int32,System.Int32,System.Int32,Pathfinding.Int3[],System.Int32[],System.Boolean)
extern "C"  void RecastGraph_ReplaceTile_m93229745 (RecastGraph_t2197443166 * __this, int32_t ___x0, int32_t ___z1, int32_t ___w2, int32_t ___d3, Int3U5BU5D_t516284607* ___verts4, Int32U5BU5D_t3230847821* ___tris5, bool ___worldSpace6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ScanCRecast()
extern "C"  void RecastGraph_ScanCRecast_m1058012719 (RecastGraph_t2197443166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::CollectTreeMeshes(UnityEngine.Terrain,System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>)
extern "C"  void RecastGraph_CollectTreeMeshes_m1961283788 (RecastGraph_t2197443166 * __this, Terrain_t2520838763 * ___terrain0, List_1_t1291247971 * ___extraMeshes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::CollectTerrainMeshes(UnityEngine.Bounds,System.Boolean,System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>)
extern "C"  void RecastGraph_CollectTerrainMeshes_m3306662378 (RecastGraph_t2197443166 * __this, Bounds_t2711641849  ___bounds0, bool ___rasterizeTrees1, List_1_t1291247971 * ___extraMeshes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::CollectColliderMeshes(UnityEngine.Bounds,System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>)
extern "C"  void RecastGraph_CollectColliderMeshes_m3726573206 (RecastGraph_t2197443166 * __this, Bounds_t2711641849  ___bounds0, List_1_t1291247971 * ___extraMeshes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastGraph::CollectMeshes(System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>&,UnityEngine.Bounds)
extern "C"  bool RecastGraph_CollectMeshes_m275201942 (RecastGraph_t2197443166 * __this, List_1_t1291247971 ** ___extraMeshes0, Bounds_t2711641849  ___bounds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Voxels.ExtraMesh Pathfinding.RecastGraph::RasterizeCollider(UnityEngine.Collider)
extern "C"  ExtraMesh_t4218029715  RecastGraph_RasterizeCollider_m1663499150 (RecastGraph_t2197443166 * __this, Collider_t2939674232 * ___col0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Voxels.ExtraMesh Pathfinding.RecastGraph::RasterizeCollider(UnityEngine.Collider,UnityEngine.Matrix4x4)
extern "C"  ExtraMesh_t4218029715  RecastGraph_RasterizeCollider_m2018505922 (RecastGraph_t2197443166 * __this, Collider_t2939674232 * ___col0, Matrix4x4_t1651859333  ___localToWorldMatrix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastGraph::Linecast(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool RecastGraph_Linecast_m472877026 (RecastGraph_t2197443166 * __this, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___end1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastGraph::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode,Pathfinding.GraphHitInfo&)
extern "C"  bool RecastGraph_Linecast_m2401740331 (RecastGraph_t2197443166 * __this, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___end1, GraphNode_t23612370 * ___hint2, GraphHitInfo_t3082627369 * ___hit3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastGraph::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode)
extern "C"  bool RecastGraph_Linecast_m2102038664 (RecastGraph_t2197443166 * __this, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___end1, GraphNode_t23612370 * ___hint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastGraph::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode,Pathfinding.GraphHitInfo&,System.Collections.Generic.List`1<Pathfinding.GraphNode>)
extern "C"  bool RecastGraph_Linecast_m3497406021 (RecastGraph_t2197443166 * __this, Vector3_t4282066566  ___tmp_origin0, Vector3_t4282066566  ___tmp_end1, GraphNode_t23612370 * ___hint2, GraphHitInfo_t3082627369 * ___hit3, List_1_t1391797922 * ___trace4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::OnDrawGizmos(System.Boolean)
extern "C"  void RecastGraph_OnDrawGizmos_m2035108254 (RecastGraph_t2197443166 * __this, bool ___drawNodes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::SerializeExtraInfo(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void RecastGraph_SerializeExtraInfo_m2611716996 (RecastGraph_t2197443166 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::DeserializeExtraInfo(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void RecastGraph_DeserializeExtraInfo_m3037095523 (RecastGraph_t2197443166 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.RecastGraph::ilo_get_Width1(Pathfinding.IntRect&)
extern "C"  int32_t RecastGraph_ilo_get_Width1_m1829556871 (Il2CppObject * __this /* static, unused */, IntRect_t3015058261 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_OnDestroy2(Pathfinding.NavGraph)
extern "C"  void RecastGraph_ilo_OnDestroy2_m911526144 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.RecastGraph::ilo_GetGraphIndex3(Pathfinding.AstarData,Pathfinding.NavGraph)
extern "C"  int32_t RecastGraph_ilo_GetGraphIndex3_m1668082491 (Il2CppObject * __this /* static, unused */, AstarData_t3283402719 * ____this0, NavGraph_t1254319713 * ___graph1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_SetNavmeshHolder4(System.Int32,Pathfinding.INavmeshHolder)
extern "C"  void RecastGraph_ilo_SetNavmeshHolder4_m1447508378 (Il2CppObject * __this /* static, unused */, int32_t ___graphIndex0, Il2CppObject * ___graph1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.RecastGraph::ilo_GetVertex5(Pathfinding.RecastGraph,System.Int32)
extern "C"  Int3_t1974045594  RecastGraph_ilo_GetVertex5_m3289609921 (Il2CppObject * __this /* static, unused */, RecastGraph_t2197443166 * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RecastGraph::ilo_ClosestPointOnTriangle6(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  RecastGraph_ilo_ClosestPointOnTriangle6_m1575976604 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___tr00, Vector3_t4282066566  ___tr11, Vector3_t4282066566  ___tr22, Vector3_t4282066566  ___point3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastGraph::ilo_IsClockwise7(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool RecastGraph_ilo_IsClockwise7_m1046110918 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, Vector3_t4282066566  ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RecastGraph::ilo_op_Explicit8(Pathfinding.Int3)
extern "C"  Vector3_t4282066566  RecastGraph_ilo_op_Explicit8_m726334236 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_GetSceneMeshes9(UnityEngine.Bounds,System.Collections.Generic.List`1<System.String>,UnityEngine.LayerMask,System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>)
extern "C"  void RecastGraph_ilo_GetSceneMeshes9_m3266530610 (Il2CppObject * __this /* static, unused */, Bounds_t2711641849  ___bounds0, List_1_t1375417109 * ___tagMask1, LayerMask_t3236759763  ___layerMask2, List_1_t1291247971 * ___meshes3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_GetAllInBounds10(System.Collections.Generic.List`1<Pathfinding.RecastMeshObj>,UnityEngine.Bounds)
extern "C"  void RecastGraph_ilo_GetAllInBounds10_m898950374 (Il2CppObject * __this /* static, unused */, List_1_t3437381290 * ___buffer0, Bounds_t2711641849  ___bounds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Voxels.ExtraMesh Pathfinding.RecastGraph::ilo_RasterizeCollider11(Pathfinding.RecastGraph,UnityEngine.Collider)
extern "C"  ExtraMesh_t4218029715  RecastGraph_ilo_RasterizeCollider11_m1174332481 (Il2CppObject * __this /* static, unused */, RecastGraph_t2197443166 * ____this0, Collider_t2939674232 * ___col1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds Pathfinding.RecastGraph::ilo_GetTileBounds12(Pathfinding.RecastGraph,Pathfinding.IntRect)
extern "C"  Bounds_t2711641849  RecastGraph_ilo_GetTileBounds12_m3666063540 (Il2CppObject * __this /* static, unused */, RecastGraph_t2197443166 * ____this0, IntRect_t3015058261  ___rect1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastGraph::ilo_CollectMeshes13(Pathfinding.RecastGraph,System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>&,UnityEngine.Bounds)
extern "C"  bool RecastGraph_ilo_CollectMeshes13_m2814123649 (Il2CppObject * __this /* static, unused */, RecastGraph_t2197443166 * ____this0, List_1_t1291247971 ** ___extraMeshes1, Bounds_t2711641849  ___bounds2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_UpdateArea14(Pathfinding.GraphUpdateObject,Pathfinding.INavmesh)
extern "C"  void RecastGraph_ilo_UpdateArea14_m933451686 (Il2CppObject * __this /* static, unused */, GraphUpdateObject_t430843704 * ___o0, Il2CppObject * ___graph1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_BuildTileMesh15(Pathfinding.RecastGraph,Pathfinding.Voxels.Voxelize,System.Int32,System.Int32)
extern "C"  void RecastGraph_ilo_BuildTileMesh15_m2818898166 (Il2CppObject * __this /* static, unused */, RecastGraph_t2197443166 * ____this0, Voxelize_t3998270866 * ___vox1, int32_t ___x2, int32_t ___z3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.IntRect Pathfinding.RecastGraph::ilo_Intersection16(Pathfinding.IntRect,Pathfinding.IntRect)
extern "C"  IntRect_t3015058261  RecastGraph_ilo_Intersection16_m3054540618 (Il2CppObject * __this /* static, unused */, IntRect_t3015058261  ___a0, IntRect_t3015058261  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastGraph::ilo_Contains17(Pathfinding.IntRect&,System.Int32,System.Int32)
extern "C"  bool RecastGraph_ilo_Contains17_m2457039890 (Il2CppObject * __this /* static, unused */, IntRect_t3015058261 * ____this0, int32_t ___x1, int32_t ___y2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_ConnectTiles18(Pathfinding.RecastGraph,Pathfinding.RecastGraph/NavmeshTile,Pathfinding.RecastGraph/NavmeshTile)
extern "C"  void RecastGraph_ilo_ConnectTiles18_m1710841942 (Il2CppObject * __this /* static, unused */, RecastGraph_t2197443166 * ____this0, NavmeshTile_t4022309569 * ___tile11, NavmeshTile_t4022309569 * ___tile22, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_RemoveConnectionsFromTo19(Pathfinding.RecastGraph,Pathfinding.RecastGraph/NavmeshTile,Pathfinding.RecastGraph/NavmeshTile)
extern "C"  void RecastGraph_ilo_RemoveConnectionsFromTo19_m2570005460 (Il2CppObject * __this /* static, unused */, RecastGraph_t2197443166 * ____this0, NavmeshTile_t4022309569 * ___a1, NavmeshTile_t4022309569 * ___b2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds Pathfinding.RecastGraph::ilo_get_forcedBounds20(Pathfinding.RecastGraph)
extern "C"  Bounds_t2711641849  RecastGraph_ilo_get_forcedBounds20_m291968638 (Il2CppObject * __this /* static, unused */, RecastGraph_t2197443166 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.RecastGraph::ilo_QueryClosest21(Pathfinding.BBTree,UnityEngine.Vector3,Pathfinding.NNConstraint,System.Single&,Pathfinding.NNInfo)
extern "C"  NNInfo_t1570625892  RecastGraph_ilo_QueryClosest21_m1326226354 (Il2CppObject * __this /* static, unused */, BBTree_t1216325332 * ____this0, Vector3_t4282066566  ___p1, NNConstraint_t758567699 * ___constraint2, float* ___distance3, NNInfo_t1570625892  ___previous4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.MeshNode Pathfinding.RecastGraph::ilo_QueryInside22(Pathfinding.BBTree,UnityEngine.Vector3,Pathfinding.NNConstraint)
extern "C"  MeshNode_t3005053445 * RecastGraph_ilo_QueryInside22_m721085256 (Il2CppObject * __this /* static, unused */, BBTree_t1216325332 * ____this0, Vector3_t4282066566  ___p1, NNConstraint_t758567699 * ___constraint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RecastGraph::ilo_MapToRange23(System.Single,System.Single,System.Single)
extern "C"  float RecastGraph_ilo_MapToRange23_m2185757084 (Il2CppObject * __this /* static, unused */, float ___targetMin0, float ___targetMax1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_Invoke24(OnScanStatus,Pathfinding.Progress)
extern "C"  void RecastGraph_ilo_Invoke24_m1353139845 (Il2CppObject * __this /* static, unused */, OnScanStatus_t2412749870 * ____this0, Progress_t2476786339  ___progress1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_GetNodes25(Pathfinding.RecastGraph,Pathfinding.GraphNodeDelegateCancelable)
extern "C"  void RecastGraph_ilo_GetNodes25_m1462553617 (Il2CppObject * __this /* static, unused */, RecastGraph_t2197443166 * ____this0, GraphNodeDelegateCancelable_t3591372971 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_Init26(Pathfinding.Voxels.Voxelize)
extern "C"  void RecastGraph_ilo_Init26_m2348560603 (Il2CppObject * __this /* static, unused */, Voxelize_t3998270866 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_CollectMeshes27(Pathfinding.Voxels.Voxelize)
extern "C"  void RecastGraph_ilo_CollectMeshes27_m2552692191 (Il2CppObject * __this /* static, unused */, Voxelize_t3998270866 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_VoxelizeInput28(Pathfinding.Voxels.Voxelize)
extern "C"  void RecastGraph_ilo_VoxelizeInput28_m1573119271 (Il2CppObject * __this /* static, unused */, Voxelize_t3998270866 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_FilterLowHeightSpans29(Pathfinding.Voxels.Voxelize,System.UInt32,System.Single,System.Single,UnityEngine.Vector3)
extern "C"  void RecastGraph_ilo_FilterLowHeightSpans29_m4048269619 (Il2CppObject * __this /* static, unused */, Voxelize_t3998270866 * ____this0, uint32_t ___voxelWalkableHeight1, float ___cs2, float ___ch3, Vector3_t4282066566  ___min4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_BuildDistanceField30(Pathfinding.Voxels.Voxelize)
extern "C"  void RecastGraph_ilo_BuildDistanceField30_m2542385179 (Il2CppObject * __this /* static, unused */, Voxelize_t3998270866 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_BuildPolyMesh31(Pathfinding.Voxels.Voxelize,Pathfinding.Voxels.VoxelContourSet,System.Int32,Pathfinding.Voxels.VoxelMesh&)
extern "C"  void RecastGraph_ilo_BuildPolyMesh31_m2249700014 (Il2CppObject * __this /* static, unused */, Voxelize_t3998270866 * ____this0, VoxelContourSet_t2502455108 * ___cset1, int32_t ___nvp2, VoxelMesh_t3943678281 * ___mesh3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.RecastGraph::ilo_op_Multiply32(Pathfinding.Int3,UnityEngine.Vector3)
extern "C"  Int3_t1974045594  RecastGraph_ilo_op_Multiply32_m295823737 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, Vector3_t4282066566  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.RecastGraph::ilo_op_Explicit33(UnityEngine.Vector3)
extern "C"  Int3_t1974045594  RecastGraph_ilo_op_Explicit33_m1739333236 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_set_GraphIndex34(Pathfinding.GraphNode,System.UInt32)
extern "C"  void RecastGraph_ilo_set_GraphIndex34_m1620328288 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.RecastGraph::ilo_GetVertex35(Pathfinding.TriangleMeshNode,System.Int32)
extern "C"  Int3_t1974045594  RecastGraph_ilo_GetVertex35_m512481929 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ____this0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.RecastGraph::ilo_GetVertexCount36(Pathfinding.TriangleMeshNode)
extern "C"  int32_t RecastGraph_ilo_GetVertexCount36_m1888544167 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.RecastGraph::ilo_GetVertexIndex37(Pathfinding.TriangleMeshNode,System.Int32)
extern "C"  int32_t RecastGraph_ilo_GetVertexIndex37_m18329868 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ____this0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.RecastGraph::ilo_op_Subtraction38(Pathfinding.Int3,Pathfinding.Int3)
extern "C"  Int3_t1974045594  RecastGraph_ilo_op_Subtraction38_m1569202972 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, Int3_t1974045594  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.RecastGraph::ilo_get_costMagnitude39(Pathfinding.Int3&)
extern "C"  int32_t RecastGraph_ilo_get_costMagnitude39_m2412863176 (Il2CppObject * __this /* static, unused */, Int3_t1974045594 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.RecastGraph::ilo_get_Item40(Pathfinding.Int3&,System.Int32)
extern "C"  int32_t RecastGraph_ilo_get_Item40_m870108869 (Il2CppObject * __this /* static, unused */, Int3_t1974045594 * ____this0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastGraph::ilo_op_Equality41(Pathfinding.Int3,Pathfinding.Int3)
extern "C"  bool RecastGraph_ilo_op_Equality41_m2763660921 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, Int3_t1974045594  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RecastGraph::ilo_DistanceSegmentSegment3D42(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float RecastGraph_ilo_DistanceSegmentSegment3D42_m1464424284 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___s10, Vector3_t4282066566  ___e11, Vector3_t4282066566  ___s22, Vector3_t4282066566  ___e23, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_AddConnection43(Pathfinding.MeshNode,Pathfinding.GraphNode,System.UInt32)
extern "C"  void RecastGraph_ilo_AddConnection43_m863639107 (Il2CppObject * __this /* static, unused */, MeshNode_t3005053445 * ____this0, GraphNode_t23612370 * ___node1, uint32_t ___cost2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_Destroy44(Pathfinding.GraphNode)
extern "C"  void RecastGraph_ilo_Destroy44_m4170527374 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_set_Penalty45(Pathfinding.GraphNode,System.UInt32)
extern "C"  void RecastGraph_ilo_set_Penalty45_m1991681135 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_UpdatePositionFromVertices46(Pathfinding.TriangleMeshNode)
extern "C"  void RecastGraph_ilo_UpdatePositionFromVertices46_m3991313460 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_ConnectTileWithNeighbours47(Pathfinding.RecastGraph,Pathfinding.RecastGraph/NavmeshTile)
extern "C"  void RecastGraph_ilo_ConnectTileWithNeighbours47_m1840848368 (Il2CppObject * __this /* static, unused */, RecastGraph_t2197443166 * ____this0, NavmeshTile_t4022309569 * ___tile1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Voxels.ExtraMesh Pathfinding.RecastGraph::ilo_RasterizeCollider48(Pathfinding.RecastGraph,UnityEngine.Collider,UnityEngine.Matrix4x4)
extern "C"  ExtraMesh_t4218029715  RecastGraph_ilo_RasterizeCollider48_m2807305113 (Il2CppObject * __this /* static, unused */, RecastGraph_t2197443166 * ____this0, Collider_t2939674232 * ___col1, Matrix4x4_t1651859333  ___localToWorldMatrix2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_RecalculateBounds49(Pathfinding.Voxels.ExtraMesh&)
extern "C"  void RecastGraph_ilo_RecalculateBounds49_m1474708481 (Il2CppObject * __this /* static, unused */, ExtraMesh_t4218029715 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_GetRecastMeshObjs50(Pathfinding.RecastGraph,UnityEngine.Bounds,System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>)
extern "C"  void RecastGraph_ilo_GetRecastMeshObjs50_m2859870844 (Il2CppObject * __this /* static, unused */, RecastGraph_t2197443166 * ____this0, Bounds_t2711641849  ___bounds1, List_1_t1291247971 * ___buffer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_CollectColliderMeshes51(Pathfinding.RecastGraph,UnityEngine.Bounds,System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>)
extern "C"  void RecastGraph_ilo_CollectColliderMeshes51_m3981524165 (Il2CppObject * __this /* static, unused */, RecastGraph_t2197443166 * ____this0, Bounds_t2711641849  ___bounds1, List_1_t1291247971 * ___extraMeshes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNConstraint Pathfinding.RecastGraph::ilo_get_None52()
extern "C"  NNConstraint_t758567699 * RecastGraph_ilo_get_None52_m3804391686 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.RecastGraph::ilo_GetNearest53(Pathfinding.NavGraph,UnityEngine.Vector3,Pathfinding.NNConstraint)
extern "C"  NNInfo_t1570625892  RecastGraph_ilo_GetNearest53_m1170095586 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ____this0, Vector3_t4282066566  ___position1, NNConstraint_t758567699 * ___constraint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastGraph::ilo_Linecast54(Pathfinding.RecastGraph,UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode)
extern "C"  bool RecastGraph_ilo_Linecast54_m1002867536 (Il2CppObject * __this /* static, unused */, RecastGraph_t2197443166 * ____this0, Vector3_t4282066566  ___origin1, Vector3_t4282066566  ___end2, GraphNode_t23612370 * ___hint3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastGraph::ilo_Linecast55(Pathfinding.INavmesh,UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode,Pathfinding.GraphHitInfo&,System.Collections.Generic.List`1<Pathfinding.GraphNode>)
extern "C"  bool RecastGraph_ilo_Linecast55_m2812294549 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___graph0, Vector3_t4282066566  ___tmp_origin1, Vector3_t4282066566  ___tmp_end2, GraphNode_t23612370 * ___hint3, GraphHitInfo_t3082627369 * ___hit4, List_1_t1391797922 * ___trace5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_OnDrawGizmos56(Pathfinding.BBTree)
extern "C"  void RecastGraph_ilo_OnDrawGizmos56_m2198835059 (Il2CppObject * __this /* static, unused */, BBTree_t1216325332 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathHandler Pathfinding.RecastGraph::ilo_get_debugPathData57(AstarPath)
extern "C"  PathHandler_t918952263 * RecastGraph_ilo_get_debugPathData57_m3264429667 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RecastGraph::ilo_DeserializeNode58(Pathfinding.TriangleMeshNode,Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void RecastGraph_ilo_DeserializeNode58_m2877392814 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ____this0, GraphSerializationContext_t3256954663 * ___ctx1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
