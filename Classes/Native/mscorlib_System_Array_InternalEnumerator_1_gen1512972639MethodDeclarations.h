﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1512972639.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_2730629963.h"

// System.Void System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitTreeDecoder>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m205794030_gshared (InternalEnumerator_1_t1512972639 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m205794030(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1512972639 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m205794030_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitTreeDecoder>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1999312882_gshared (InternalEnumerator_1_t1512972639 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1999312882(__this, method) ((  void (*) (InternalEnumerator_1_t1512972639 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1999312882_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitTreeDecoder>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1379735902_gshared (InternalEnumerator_1_t1512972639 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1379735902(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1512972639 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1379735902_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitTreeDecoder>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m773842501_gshared (InternalEnumerator_1_t1512972639 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m773842501(__this, method) ((  void (*) (InternalEnumerator_1_t1512972639 *, const MethodInfo*))InternalEnumerator_1_Dispose_m773842501_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitTreeDecoder>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1506042974_gshared (InternalEnumerator_1_t1512972639 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1506042974(__this, method) ((  bool (*) (InternalEnumerator_1_t1512972639 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1506042974_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitTreeDecoder>::get_Current()
extern "C"  BitTreeDecoder_t2730629963  InternalEnumerator_1_get_Current_m3283351989_gshared (InternalEnumerator_1_t1512972639 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3283351989(__this, method) ((  BitTreeDecoder_t2730629963  (*) (InternalEnumerator_1_t1512972639 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3283351989_gshared)(__this, method)
