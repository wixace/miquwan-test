﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.FightBvr
struct  FightBvr_t869234382  : public IBehavior_t770859129
{
public:
	// System.Single Entity.Behavior.FightBvr::distance
	float ___distance_2;
	// System.Single Entity.Behavior.FightBvr::historySkillDistance
	float ___historySkillDistance_3;

public:
	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(FightBvr_t869234382, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_historySkillDistance_3() { return static_cast<int32_t>(offsetof(FightBvr_t869234382, ___historySkillDistance_3)); }
	inline float get_historySkillDistance_3() const { return ___historySkillDistance_3; }
	inline float* get_address_of_historySkillDistance_3() { return &___historySkillDistance_3; }
	inline void set_historySkillDistance_3(float value)
	{
		___historySkillDistance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
