﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LzmaAlone.Properties.Resources
struct Resources_t1211893751;
// System.Resources.ResourceManager
struct ResourceManager_t1323731545;
// System.Globalization.CultureInfo
struct CultureInfo_t1065375142;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142.h"

// System.Void LzmaAlone.Properties.Resources::.ctor()
extern "C"  void Resources__ctor_m548236604 (Resources_t1211893751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Resources.ResourceManager LzmaAlone.Properties.Resources::get_ResourceManager()
extern "C"  ResourceManager_t1323731545 * Resources_get_ResourceManager_m2718566378 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo LzmaAlone.Properties.Resources::get_Culture()
extern "C"  CultureInfo_t1065375142 * Resources_get_Culture_m3438383826 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LzmaAlone.Properties.Resources::set_Culture(System.Globalization.CultureInfo)
extern "C"  void Resources_set_Culture_m632601307 (Il2CppObject * __this /* static, unused */, CultureInfo_t1065375142 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
