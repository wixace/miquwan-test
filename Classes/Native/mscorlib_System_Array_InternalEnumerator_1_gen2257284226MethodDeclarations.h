﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2257284226.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_MScrollView_MoveWay3474941550.h"

// System.Void System.Array/InternalEnumerator`1<MScrollView/MoveWay>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m284719091_gshared (InternalEnumerator_1_t2257284226 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m284719091(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2257284226 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m284719091_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<MScrollView/MoveWay>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3785022157_gshared (InternalEnumerator_1_t2257284226 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3785022157(__this, method) ((  void (*) (InternalEnumerator_1_t2257284226 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3785022157_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<MScrollView/MoveWay>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m179346691_gshared (InternalEnumerator_1_t2257284226 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m179346691(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2257284226 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m179346691_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<MScrollView/MoveWay>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1643824522_gshared (InternalEnumerator_1_t2257284226 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1643824522(__this, method) ((  void (*) (InternalEnumerator_1_t2257284226 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1643824522_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<MScrollView/MoveWay>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2345966589_gshared (InternalEnumerator_1_t2257284226 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2345966589(__this, method) ((  bool (*) (InternalEnumerator_1_t2257284226 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2345966589_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<MScrollView/MoveWay>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3025179740_gshared (InternalEnumerator_1_t2257284226 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3025179740(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2257284226 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3025179740_gshared)(__this, method)
