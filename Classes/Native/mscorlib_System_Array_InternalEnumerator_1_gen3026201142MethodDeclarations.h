﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3026201142.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_Rendering_RenderBufferLoad4243858466.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rendering.RenderBufferLoadAction>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1669719452_gshared (InternalEnumerator_1_t3026201142 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1669719452(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3026201142 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1669719452_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rendering.RenderBufferLoadAction>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m873124484_gshared (InternalEnumerator_1_t3026201142 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m873124484(__this, method) ((  void (*) (InternalEnumerator_1_t3026201142 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m873124484_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Rendering.RenderBufferLoadAction>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m825482544_gshared (InternalEnumerator_1_t3026201142 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m825482544(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3026201142 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m825482544_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rendering.RenderBufferLoadAction>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2187100787_gshared (InternalEnumerator_1_t3026201142 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2187100787(__this, method) ((  void (*) (InternalEnumerator_1_t3026201142 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2187100787_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Rendering.RenderBufferLoadAction>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1324631408_gshared (InternalEnumerator_1_t3026201142 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1324631408(__this, method) ((  bool (*) (InternalEnumerator_1_t3026201142 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1324631408_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Rendering.RenderBufferLoadAction>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m674329251_gshared (InternalEnumerator_1_t3026201142 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m674329251(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3026201142 *, const MethodInfo*))InternalEnumerator_1_get_Current_m674329251_gshared)(__this, method)
