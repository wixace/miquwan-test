﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEffectMgr/<PlayUIEffect>c__AnonStorey15B
struct U3CPlayUIEffectU3Ec__AnonStorey15B_t2198330556;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"

// System.Void UIEffectMgr/<PlayUIEffect>c__AnonStorey15B::.ctor()
extern "C"  void U3CPlayUIEffectU3Ec__AnonStorey15B__ctor_m2546200415 (U3CPlayUIEffectU3Ec__AnonStorey15B_t2198330556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffectMgr/<PlayUIEffect>c__AnonStorey15B::<>m__3E9(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  void U3CPlayUIEffectU3Ec__AnonStorey15B_U3CU3Em__3E9_m2258325224 (U3CPlayUIEffectU3Ec__AnonStorey15B_t2198330556 * __this, AssetOperation_t778728221 * ___ao0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
