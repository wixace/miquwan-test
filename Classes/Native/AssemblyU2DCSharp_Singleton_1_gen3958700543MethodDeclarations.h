﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Singleton_1_gen128664468MethodDeclarations.h"

// System.Void Singleton`1<BloodMgr>::.ctor()
#define Singleton_1__ctor_m1410567691(__this, method) ((  void (*) (Singleton_1_t3958700543 *, const MethodInfo*))Singleton_1__ctor_m3958676923_gshared)(__this, method)
// T Singleton`1<BloodMgr>::get_Instance()
#define Singleton_1_get_Instance_m2013497752(__this /* static, unused */, method) ((  BloodMgr_t3705885150 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_Instance_m1020946630_gshared)(__this /* static, unused */, method)
