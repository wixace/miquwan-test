﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AnimatorStateInfoGenerated
struct UnityEngine_AnimatorStateInfoGenerated_t2958363167;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_AnimatorStateInfoGenerated::.ctor()
extern "C"  void UnityEngine_AnimatorStateInfoGenerated__ctor_m1621880156 (UnityEngine_AnimatorStateInfoGenerated_t2958363167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorStateInfoGenerated::.cctor()
extern "C"  void UnityEngine_AnimatorStateInfoGenerated__cctor_m2551548369 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorStateInfoGenerated::AnimatorStateInfo_AnimatorStateInfo1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorStateInfoGenerated_AnimatorStateInfo_AnimatorStateInfo1_m1061185580 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorStateInfoGenerated::AnimatorStateInfo_fullPathHash(JSVCall)
extern "C"  void UnityEngine_AnimatorStateInfoGenerated_AnimatorStateInfo_fullPathHash_m3033849718 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorStateInfoGenerated::AnimatorStateInfo_shortNameHash(JSVCall)
extern "C"  void UnityEngine_AnimatorStateInfoGenerated_AnimatorStateInfo_shortNameHash_m2887888479 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorStateInfoGenerated::AnimatorStateInfo_normalizedTime(JSVCall)
extern "C"  void UnityEngine_AnimatorStateInfoGenerated_AnimatorStateInfo_normalizedTime_m37644628 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorStateInfoGenerated::AnimatorStateInfo_length(JSVCall)
extern "C"  void UnityEngine_AnimatorStateInfoGenerated_AnimatorStateInfo_length_m2159470866 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorStateInfoGenerated::AnimatorStateInfo_speed(JSVCall)
extern "C"  void UnityEngine_AnimatorStateInfoGenerated_AnimatorStateInfo_speed_m3420142381 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorStateInfoGenerated::AnimatorStateInfo_speedMultiplier(JSVCall)
extern "C"  void UnityEngine_AnimatorStateInfoGenerated_AnimatorStateInfo_speedMultiplier_m4263954860 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorStateInfoGenerated::AnimatorStateInfo_tagHash(JSVCall)
extern "C"  void UnityEngine_AnimatorStateInfoGenerated_AnimatorStateInfo_tagHash_m979777868 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorStateInfoGenerated::AnimatorStateInfo_loop(JSVCall)
extern "C"  void UnityEngine_AnimatorStateInfoGenerated_AnimatorStateInfo_loop_m2767583124 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorStateInfoGenerated::AnimatorStateInfo_IsName__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorStateInfoGenerated_AnimatorStateInfo_IsName__String_m2597341657 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorStateInfoGenerated::AnimatorStateInfo_IsTag__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorStateInfoGenerated_AnimatorStateInfo_IsTag__String_m1381980368 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorStateInfoGenerated::__Register()
extern "C"  void UnityEngine_AnimatorStateInfoGenerated___Register_m3244263659 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorStateInfoGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_AnimatorStateInfoGenerated_ilo_attachFinalizerObject1_m1174693635 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorStateInfoGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_AnimatorStateInfoGenerated_ilo_addJSCSRel2_m2368268953 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorStateInfoGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_AnimatorStateInfoGenerated_ilo_setSingle3_m681842330 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorStateInfoGenerated::ilo_changeJSObj4(System.Int32,System.Object)
extern "C"  void UnityEngine_AnimatorStateInfoGenerated_ilo_changeJSObj4_m3202305808 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
