﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_daqole83
struct  M_daqole83_t33599839  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_daqole83::_keculereHerewhaw
	String_t* ____keculereHerewhaw_0;
	// System.Single GarbageiOS.M_daqole83::_qorir
	float ____qorir_1;
	// System.String GarbageiOS.M_daqole83::_petelwhir
	String_t* ____petelwhir_2;
	// System.UInt32 GarbageiOS.M_daqole83::_neesiSemjakee
	uint32_t ____neesiSemjakee_3;
	// System.Int32 GarbageiOS.M_daqole83::_keldese
	int32_t ____keldese_4;
	// System.Single GarbageiOS.M_daqole83::_sameLoyegi
	float ____sameLoyegi_5;
	// System.UInt32 GarbageiOS.M_daqole83::_keaxisbasFurrel
	uint32_t ____keaxisbasFurrel_6;
	// System.UInt32 GarbageiOS.M_daqole83::_tearwurche
	uint32_t ____tearwurche_7;
	// System.Single GarbageiOS.M_daqole83::_sojerePalpilar
	float ____sojerePalpilar_8;
	// System.Boolean GarbageiOS.M_daqole83::_tousemDejelis
	bool ____tousemDejelis_9;
	// System.Single GarbageiOS.M_daqole83::_kuwaynow
	float ____kuwaynow_10;
	// System.Single GarbageiOS.M_daqole83::_zenahu
	float ____zenahu_11;
	// System.Single GarbageiOS.M_daqole83::_qilaKerlurnoo
	float ____qilaKerlurnoo_12;
	// System.UInt32 GarbageiOS.M_daqole83::_mijeejelWharlouri
	uint32_t ____mijeejelWharlouri_13;
	// System.String GarbageiOS.M_daqole83::_nalcouwhowJookirja
	String_t* ____nalcouwhowJookirja_14;
	// System.Int32 GarbageiOS.M_daqole83::_tearkojeeNahea
	int32_t ____tearkojeeNahea_15;
	// System.String GarbageiOS.M_daqole83::_nurero
	String_t* ____nurero_16;
	// System.Single GarbageiOS.M_daqole83::_caysouVormall
	float ____caysouVormall_17;
	// System.UInt32 GarbageiOS.M_daqole83::_petra
	uint32_t ____petra_18;
	// System.String GarbageiOS.M_daqole83::_trerqouSejel
	String_t* ____trerqouSejel_19;
	// System.Single GarbageiOS.M_daqole83::_wumapouCemjair
	float ____wumapouCemjair_20;
	// System.String GarbageiOS.M_daqole83::_morjiLaigi
	String_t* ____morjiLaigi_21;
	// System.UInt32 GarbageiOS.M_daqole83::_jeatar
	uint32_t ____jeatar_22;
	// System.String GarbageiOS.M_daqole83::_booloujaJeldem
	String_t* ____booloujaJeldem_23;
	// System.UInt32 GarbageiOS.M_daqole83::_sorneparKairpere
	uint32_t ____sorneparKairpere_24;
	// System.UInt32 GarbageiOS.M_daqole83::_kuhebe
	uint32_t ____kuhebe_25;
	// System.Single GarbageiOS.M_daqole83::_yernure
	float ____yernure_26;
	// System.Boolean GarbageiOS.M_daqole83::_soomeLisepi
	bool ____soomeLisepi_27;
	// System.String GarbageiOS.M_daqole83::_zistasmeaGaqorsta
	String_t* ____zistasmeaGaqorsta_28;
	// System.Boolean GarbageiOS.M_daqole83::_fosarjur
	bool ____fosarjur_29;
	// System.Single GarbageiOS.M_daqole83::_vivarmaChemdamaw
	float ____vivarmaChemdamaw_30;
	// System.UInt32 GarbageiOS.M_daqole83::_troojuqallLujoo
	uint32_t ____troojuqallLujoo_31;
	// System.Single GarbageiOS.M_daqole83::_huxeManarur
	float ____huxeManarur_32;
	// System.Int32 GarbageiOS.M_daqole83::_fechawnuQaras
	int32_t ____fechawnuQaras_33;
	// System.Int32 GarbageiOS.M_daqole83::_jarpooJezowchir
	int32_t ____jarpooJezowchir_34;
	// System.String GarbageiOS.M_daqole83::_ruja
	String_t* ____ruja_35;
	// System.UInt32 GarbageiOS.M_daqole83::_sojaKalhall
	uint32_t ____sojaKalhall_36;
	// System.Boolean GarbageiOS.M_daqole83::_wecaka
	bool ____wecaka_37;
	// System.String GarbageiOS.M_daqole83::_sicelToure
	String_t* ____sicelToure_38;

public:
	inline static int32_t get_offset_of__keculereHerewhaw_0() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____keculereHerewhaw_0)); }
	inline String_t* get__keculereHerewhaw_0() const { return ____keculereHerewhaw_0; }
	inline String_t** get_address_of__keculereHerewhaw_0() { return &____keculereHerewhaw_0; }
	inline void set__keculereHerewhaw_0(String_t* value)
	{
		____keculereHerewhaw_0 = value;
		Il2CppCodeGenWriteBarrier(&____keculereHerewhaw_0, value);
	}

	inline static int32_t get_offset_of__qorir_1() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____qorir_1)); }
	inline float get__qorir_1() const { return ____qorir_1; }
	inline float* get_address_of__qorir_1() { return &____qorir_1; }
	inline void set__qorir_1(float value)
	{
		____qorir_1 = value;
	}

	inline static int32_t get_offset_of__petelwhir_2() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____petelwhir_2)); }
	inline String_t* get__petelwhir_2() const { return ____petelwhir_2; }
	inline String_t** get_address_of__petelwhir_2() { return &____petelwhir_2; }
	inline void set__petelwhir_2(String_t* value)
	{
		____petelwhir_2 = value;
		Il2CppCodeGenWriteBarrier(&____petelwhir_2, value);
	}

	inline static int32_t get_offset_of__neesiSemjakee_3() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____neesiSemjakee_3)); }
	inline uint32_t get__neesiSemjakee_3() const { return ____neesiSemjakee_3; }
	inline uint32_t* get_address_of__neesiSemjakee_3() { return &____neesiSemjakee_3; }
	inline void set__neesiSemjakee_3(uint32_t value)
	{
		____neesiSemjakee_3 = value;
	}

	inline static int32_t get_offset_of__keldese_4() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____keldese_4)); }
	inline int32_t get__keldese_4() const { return ____keldese_4; }
	inline int32_t* get_address_of__keldese_4() { return &____keldese_4; }
	inline void set__keldese_4(int32_t value)
	{
		____keldese_4 = value;
	}

	inline static int32_t get_offset_of__sameLoyegi_5() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____sameLoyegi_5)); }
	inline float get__sameLoyegi_5() const { return ____sameLoyegi_5; }
	inline float* get_address_of__sameLoyegi_5() { return &____sameLoyegi_5; }
	inline void set__sameLoyegi_5(float value)
	{
		____sameLoyegi_5 = value;
	}

	inline static int32_t get_offset_of__keaxisbasFurrel_6() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____keaxisbasFurrel_6)); }
	inline uint32_t get__keaxisbasFurrel_6() const { return ____keaxisbasFurrel_6; }
	inline uint32_t* get_address_of__keaxisbasFurrel_6() { return &____keaxisbasFurrel_6; }
	inline void set__keaxisbasFurrel_6(uint32_t value)
	{
		____keaxisbasFurrel_6 = value;
	}

	inline static int32_t get_offset_of__tearwurche_7() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____tearwurche_7)); }
	inline uint32_t get__tearwurche_7() const { return ____tearwurche_7; }
	inline uint32_t* get_address_of__tearwurche_7() { return &____tearwurche_7; }
	inline void set__tearwurche_7(uint32_t value)
	{
		____tearwurche_7 = value;
	}

	inline static int32_t get_offset_of__sojerePalpilar_8() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____sojerePalpilar_8)); }
	inline float get__sojerePalpilar_8() const { return ____sojerePalpilar_8; }
	inline float* get_address_of__sojerePalpilar_8() { return &____sojerePalpilar_8; }
	inline void set__sojerePalpilar_8(float value)
	{
		____sojerePalpilar_8 = value;
	}

	inline static int32_t get_offset_of__tousemDejelis_9() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____tousemDejelis_9)); }
	inline bool get__tousemDejelis_9() const { return ____tousemDejelis_9; }
	inline bool* get_address_of__tousemDejelis_9() { return &____tousemDejelis_9; }
	inline void set__tousemDejelis_9(bool value)
	{
		____tousemDejelis_9 = value;
	}

	inline static int32_t get_offset_of__kuwaynow_10() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____kuwaynow_10)); }
	inline float get__kuwaynow_10() const { return ____kuwaynow_10; }
	inline float* get_address_of__kuwaynow_10() { return &____kuwaynow_10; }
	inline void set__kuwaynow_10(float value)
	{
		____kuwaynow_10 = value;
	}

	inline static int32_t get_offset_of__zenahu_11() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____zenahu_11)); }
	inline float get__zenahu_11() const { return ____zenahu_11; }
	inline float* get_address_of__zenahu_11() { return &____zenahu_11; }
	inline void set__zenahu_11(float value)
	{
		____zenahu_11 = value;
	}

	inline static int32_t get_offset_of__qilaKerlurnoo_12() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____qilaKerlurnoo_12)); }
	inline float get__qilaKerlurnoo_12() const { return ____qilaKerlurnoo_12; }
	inline float* get_address_of__qilaKerlurnoo_12() { return &____qilaKerlurnoo_12; }
	inline void set__qilaKerlurnoo_12(float value)
	{
		____qilaKerlurnoo_12 = value;
	}

	inline static int32_t get_offset_of__mijeejelWharlouri_13() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____mijeejelWharlouri_13)); }
	inline uint32_t get__mijeejelWharlouri_13() const { return ____mijeejelWharlouri_13; }
	inline uint32_t* get_address_of__mijeejelWharlouri_13() { return &____mijeejelWharlouri_13; }
	inline void set__mijeejelWharlouri_13(uint32_t value)
	{
		____mijeejelWharlouri_13 = value;
	}

	inline static int32_t get_offset_of__nalcouwhowJookirja_14() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____nalcouwhowJookirja_14)); }
	inline String_t* get__nalcouwhowJookirja_14() const { return ____nalcouwhowJookirja_14; }
	inline String_t** get_address_of__nalcouwhowJookirja_14() { return &____nalcouwhowJookirja_14; }
	inline void set__nalcouwhowJookirja_14(String_t* value)
	{
		____nalcouwhowJookirja_14 = value;
		Il2CppCodeGenWriteBarrier(&____nalcouwhowJookirja_14, value);
	}

	inline static int32_t get_offset_of__tearkojeeNahea_15() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____tearkojeeNahea_15)); }
	inline int32_t get__tearkojeeNahea_15() const { return ____tearkojeeNahea_15; }
	inline int32_t* get_address_of__tearkojeeNahea_15() { return &____tearkojeeNahea_15; }
	inline void set__tearkojeeNahea_15(int32_t value)
	{
		____tearkojeeNahea_15 = value;
	}

	inline static int32_t get_offset_of__nurero_16() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____nurero_16)); }
	inline String_t* get__nurero_16() const { return ____nurero_16; }
	inline String_t** get_address_of__nurero_16() { return &____nurero_16; }
	inline void set__nurero_16(String_t* value)
	{
		____nurero_16 = value;
		Il2CppCodeGenWriteBarrier(&____nurero_16, value);
	}

	inline static int32_t get_offset_of__caysouVormall_17() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____caysouVormall_17)); }
	inline float get__caysouVormall_17() const { return ____caysouVormall_17; }
	inline float* get_address_of__caysouVormall_17() { return &____caysouVormall_17; }
	inline void set__caysouVormall_17(float value)
	{
		____caysouVormall_17 = value;
	}

	inline static int32_t get_offset_of__petra_18() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____petra_18)); }
	inline uint32_t get__petra_18() const { return ____petra_18; }
	inline uint32_t* get_address_of__petra_18() { return &____petra_18; }
	inline void set__petra_18(uint32_t value)
	{
		____petra_18 = value;
	}

	inline static int32_t get_offset_of__trerqouSejel_19() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____trerqouSejel_19)); }
	inline String_t* get__trerqouSejel_19() const { return ____trerqouSejel_19; }
	inline String_t** get_address_of__trerqouSejel_19() { return &____trerqouSejel_19; }
	inline void set__trerqouSejel_19(String_t* value)
	{
		____trerqouSejel_19 = value;
		Il2CppCodeGenWriteBarrier(&____trerqouSejel_19, value);
	}

	inline static int32_t get_offset_of__wumapouCemjair_20() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____wumapouCemjair_20)); }
	inline float get__wumapouCemjair_20() const { return ____wumapouCemjair_20; }
	inline float* get_address_of__wumapouCemjair_20() { return &____wumapouCemjair_20; }
	inline void set__wumapouCemjair_20(float value)
	{
		____wumapouCemjair_20 = value;
	}

	inline static int32_t get_offset_of__morjiLaigi_21() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____morjiLaigi_21)); }
	inline String_t* get__morjiLaigi_21() const { return ____morjiLaigi_21; }
	inline String_t** get_address_of__morjiLaigi_21() { return &____morjiLaigi_21; }
	inline void set__morjiLaigi_21(String_t* value)
	{
		____morjiLaigi_21 = value;
		Il2CppCodeGenWriteBarrier(&____morjiLaigi_21, value);
	}

	inline static int32_t get_offset_of__jeatar_22() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____jeatar_22)); }
	inline uint32_t get__jeatar_22() const { return ____jeatar_22; }
	inline uint32_t* get_address_of__jeatar_22() { return &____jeatar_22; }
	inline void set__jeatar_22(uint32_t value)
	{
		____jeatar_22 = value;
	}

	inline static int32_t get_offset_of__booloujaJeldem_23() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____booloujaJeldem_23)); }
	inline String_t* get__booloujaJeldem_23() const { return ____booloujaJeldem_23; }
	inline String_t** get_address_of__booloujaJeldem_23() { return &____booloujaJeldem_23; }
	inline void set__booloujaJeldem_23(String_t* value)
	{
		____booloujaJeldem_23 = value;
		Il2CppCodeGenWriteBarrier(&____booloujaJeldem_23, value);
	}

	inline static int32_t get_offset_of__sorneparKairpere_24() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____sorneparKairpere_24)); }
	inline uint32_t get__sorneparKairpere_24() const { return ____sorneparKairpere_24; }
	inline uint32_t* get_address_of__sorneparKairpere_24() { return &____sorneparKairpere_24; }
	inline void set__sorneparKairpere_24(uint32_t value)
	{
		____sorneparKairpere_24 = value;
	}

	inline static int32_t get_offset_of__kuhebe_25() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____kuhebe_25)); }
	inline uint32_t get__kuhebe_25() const { return ____kuhebe_25; }
	inline uint32_t* get_address_of__kuhebe_25() { return &____kuhebe_25; }
	inline void set__kuhebe_25(uint32_t value)
	{
		____kuhebe_25 = value;
	}

	inline static int32_t get_offset_of__yernure_26() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____yernure_26)); }
	inline float get__yernure_26() const { return ____yernure_26; }
	inline float* get_address_of__yernure_26() { return &____yernure_26; }
	inline void set__yernure_26(float value)
	{
		____yernure_26 = value;
	}

	inline static int32_t get_offset_of__soomeLisepi_27() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____soomeLisepi_27)); }
	inline bool get__soomeLisepi_27() const { return ____soomeLisepi_27; }
	inline bool* get_address_of__soomeLisepi_27() { return &____soomeLisepi_27; }
	inline void set__soomeLisepi_27(bool value)
	{
		____soomeLisepi_27 = value;
	}

	inline static int32_t get_offset_of__zistasmeaGaqorsta_28() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____zistasmeaGaqorsta_28)); }
	inline String_t* get__zistasmeaGaqorsta_28() const { return ____zistasmeaGaqorsta_28; }
	inline String_t** get_address_of__zistasmeaGaqorsta_28() { return &____zistasmeaGaqorsta_28; }
	inline void set__zistasmeaGaqorsta_28(String_t* value)
	{
		____zistasmeaGaqorsta_28 = value;
		Il2CppCodeGenWriteBarrier(&____zistasmeaGaqorsta_28, value);
	}

	inline static int32_t get_offset_of__fosarjur_29() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____fosarjur_29)); }
	inline bool get__fosarjur_29() const { return ____fosarjur_29; }
	inline bool* get_address_of__fosarjur_29() { return &____fosarjur_29; }
	inline void set__fosarjur_29(bool value)
	{
		____fosarjur_29 = value;
	}

	inline static int32_t get_offset_of__vivarmaChemdamaw_30() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____vivarmaChemdamaw_30)); }
	inline float get__vivarmaChemdamaw_30() const { return ____vivarmaChemdamaw_30; }
	inline float* get_address_of__vivarmaChemdamaw_30() { return &____vivarmaChemdamaw_30; }
	inline void set__vivarmaChemdamaw_30(float value)
	{
		____vivarmaChemdamaw_30 = value;
	}

	inline static int32_t get_offset_of__troojuqallLujoo_31() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____troojuqallLujoo_31)); }
	inline uint32_t get__troojuqallLujoo_31() const { return ____troojuqallLujoo_31; }
	inline uint32_t* get_address_of__troojuqallLujoo_31() { return &____troojuqallLujoo_31; }
	inline void set__troojuqallLujoo_31(uint32_t value)
	{
		____troojuqallLujoo_31 = value;
	}

	inline static int32_t get_offset_of__huxeManarur_32() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____huxeManarur_32)); }
	inline float get__huxeManarur_32() const { return ____huxeManarur_32; }
	inline float* get_address_of__huxeManarur_32() { return &____huxeManarur_32; }
	inline void set__huxeManarur_32(float value)
	{
		____huxeManarur_32 = value;
	}

	inline static int32_t get_offset_of__fechawnuQaras_33() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____fechawnuQaras_33)); }
	inline int32_t get__fechawnuQaras_33() const { return ____fechawnuQaras_33; }
	inline int32_t* get_address_of__fechawnuQaras_33() { return &____fechawnuQaras_33; }
	inline void set__fechawnuQaras_33(int32_t value)
	{
		____fechawnuQaras_33 = value;
	}

	inline static int32_t get_offset_of__jarpooJezowchir_34() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____jarpooJezowchir_34)); }
	inline int32_t get__jarpooJezowchir_34() const { return ____jarpooJezowchir_34; }
	inline int32_t* get_address_of__jarpooJezowchir_34() { return &____jarpooJezowchir_34; }
	inline void set__jarpooJezowchir_34(int32_t value)
	{
		____jarpooJezowchir_34 = value;
	}

	inline static int32_t get_offset_of__ruja_35() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____ruja_35)); }
	inline String_t* get__ruja_35() const { return ____ruja_35; }
	inline String_t** get_address_of__ruja_35() { return &____ruja_35; }
	inline void set__ruja_35(String_t* value)
	{
		____ruja_35 = value;
		Il2CppCodeGenWriteBarrier(&____ruja_35, value);
	}

	inline static int32_t get_offset_of__sojaKalhall_36() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____sojaKalhall_36)); }
	inline uint32_t get__sojaKalhall_36() const { return ____sojaKalhall_36; }
	inline uint32_t* get_address_of__sojaKalhall_36() { return &____sojaKalhall_36; }
	inline void set__sojaKalhall_36(uint32_t value)
	{
		____sojaKalhall_36 = value;
	}

	inline static int32_t get_offset_of__wecaka_37() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____wecaka_37)); }
	inline bool get__wecaka_37() const { return ____wecaka_37; }
	inline bool* get_address_of__wecaka_37() { return &____wecaka_37; }
	inline void set__wecaka_37(bool value)
	{
		____wecaka_37 = value;
	}

	inline static int32_t get_offset_of__sicelToure_38() { return static_cast<int32_t>(offsetof(M_daqole83_t33599839, ____sicelToure_38)); }
	inline String_t* get__sicelToure_38() const { return ____sicelToure_38; }
	inline String_t** get_address_of__sicelToure_38() { return &____sicelToure_38; }
	inline void set__sicelToure_38(String_t* value)
	{
		____sicelToure_38 = value;
		Il2CppCodeGenWriteBarrier(&____sicelToure_38, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
