﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_UnityExceptionGenerated
struct UnityEngine_UnityExceptionGenerated_t3325473773;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_UnityExceptionGenerated::.ctor()
extern "C"  void UnityEngine_UnityExceptionGenerated__ctor_m3764103710 (UnityEngine_UnityExceptionGenerated_t3325473773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_UnityExceptionGenerated::UnityException_UnityException1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_UnityExceptionGenerated_UnityException_UnityException1_m3934454292 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_UnityExceptionGenerated::UnityException_UnityException2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_UnityExceptionGenerated_UnityException_UnityException2_m884251477 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_UnityExceptionGenerated::UnityException_UnityException3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_UnityExceptionGenerated_UnityException_UnityException3_m2129015958 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UnityExceptionGenerated::__Register()
extern "C"  void UnityEngine_UnityExceptionGenerated___Register_m3509679465 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_UnityExceptionGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_UnityExceptionGenerated_ilo_getObject1_m1310091736 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UnityExceptionGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_UnityExceptionGenerated_ilo_addJSCSRel2_m1171844699 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_UnityExceptionGenerated::ilo_getStringS3(System.Int32)
extern "C"  String_t* UnityEngine_UnityExceptionGenerated_ilo_getStringS3_m3135730372 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
