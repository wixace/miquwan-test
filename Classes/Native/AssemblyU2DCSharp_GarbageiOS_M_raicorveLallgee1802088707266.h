﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_raicorveLallgee180
struct  M_raicorveLallgee180_t2088707266  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_raicorveLallgee180::_sterchas
	float ____sterchas_0;
	// System.UInt32 GarbageiOS.M_raicorveLallgee180::_curminow
	uint32_t ____curminow_1;
	// System.UInt32 GarbageiOS.M_raicorveLallgee180::_helsudror
	uint32_t ____helsudror_2;
	// System.String GarbageiOS.M_raicorveLallgee180::_selxowje
	String_t* ____selxowje_3;
	// System.Single GarbageiOS.M_raicorveLallgee180::_wallsirouFasraw
	float ____wallsirouFasraw_4;
	// System.String GarbageiOS.M_raicorveLallgee180::_stemooCurhatar
	String_t* ____stemooCurhatar_5;
	// System.Boolean GarbageiOS.M_raicorveLallgee180::_cugiDrereku
	bool ____cugiDrereku_6;
	// System.Int32 GarbageiOS.M_raicorveLallgee180::_zariTearsir
	int32_t ____zariTearsir_7;
	// System.UInt32 GarbageiOS.M_raicorveLallgee180::_pairsir
	uint32_t ____pairsir_8;
	// System.Int32 GarbageiOS.M_raicorveLallgee180::_selsagis
	int32_t ____selsagis_9;
	// System.String GarbageiOS.M_raicorveLallgee180::_palair
	String_t* ____palair_10;
	// System.Int32 GarbageiOS.M_raicorveLallgee180::_drisowmairJeerel
	int32_t ____drisowmairJeerel_11;
	// System.Single GarbageiOS.M_raicorveLallgee180::_kecouNerchai
	float ____kecouNerchai_12;
	// System.UInt32 GarbageiOS.M_raicorveLallgee180::_tarcisme
	uint32_t ____tarcisme_13;
	// System.String GarbageiOS.M_raicorveLallgee180::_sosata
	String_t* ____sosata_14;
	// System.String GarbageiOS.M_raicorveLallgee180::_yeacheSeayirbo
	String_t* ____yeacheSeayirbo_15;
	// System.String GarbageiOS.M_raicorveLallgee180::_wallburSibou
	String_t* ____wallburSibou_16;
	// System.Single GarbageiOS.M_raicorveLallgee180::_rallveedear
	float ____rallveedear_17;
	// System.Int32 GarbageiOS.M_raicorveLallgee180::_whoomoCoryeeta
	int32_t ____whoomoCoryeeta_18;
	// System.Int32 GarbageiOS.M_raicorveLallgee180::_rashi
	int32_t ____rashi_19;
	// System.UInt32 GarbageiOS.M_raicorveLallgee180::_miqemPurgokar
	uint32_t ____miqemPurgokar_20;
	// System.Single GarbageiOS.M_raicorveLallgee180::_lorpaltorHetur
	float ____lorpaltorHetur_21;
	// System.Int32 GarbageiOS.M_raicorveLallgee180::_laljoucowDrorsesa
	int32_t ____laljoucowDrorsesa_22;
	// System.String GarbageiOS.M_raicorveLallgee180::_ceseresa
	String_t* ____ceseresa_23;
	// System.Single GarbageiOS.M_raicorveLallgee180::_sterpeatoLorsalldroo
	float ____sterpeatoLorsalldroo_24;
	// System.Boolean GarbageiOS.M_raicorveLallgee180::_derecerpurPame
	bool ____derecerpurPame_25;
	// System.String GarbageiOS.M_raicorveLallgee180::_latow
	String_t* ____latow_26;
	// System.Single GarbageiOS.M_raicorveLallgee180::_jatenar
	float ____jatenar_27;

public:
	inline static int32_t get_offset_of__sterchas_0() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____sterchas_0)); }
	inline float get__sterchas_0() const { return ____sterchas_0; }
	inline float* get_address_of__sterchas_0() { return &____sterchas_0; }
	inline void set__sterchas_0(float value)
	{
		____sterchas_0 = value;
	}

	inline static int32_t get_offset_of__curminow_1() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____curminow_1)); }
	inline uint32_t get__curminow_1() const { return ____curminow_1; }
	inline uint32_t* get_address_of__curminow_1() { return &____curminow_1; }
	inline void set__curminow_1(uint32_t value)
	{
		____curminow_1 = value;
	}

	inline static int32_t get_offset_of__helsudror_2() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____helsudror_2)); }
	inline uint32_t get__helsudror_2() const { return ____helsudror_2; }
	inline uint32_t* get_address_of__helsudror_2() { return &____helsudror_2; }
	inline void set__helsudror_2(uint32_t value)
	{
		____helsudror_2 = value;
	}

	inline static int32_t get_offset_of__selxowje_3() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____selxowje_3)); }
	inline String_t* get__selxowje_3() const { return ____selxowje_3; }
	inline String_t** get_address_of__selxowje_3() { return &____selxowje_3; }
	inline void set__selxowje_3(String_t* value)
	{
		____selxowje_3 = value;
		Il2CppCodeGenWriteBarrier(&____selxowje_3, value);
	}

	inline static int32_t get_offset_of__wallsirouFasraw_4() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____wallsirouFasraw_4)); }
	inline float get__wallsirouFasraw_4() const { return ____wallsirouFasraw_4; }
	inline float* get_address_of__wallsirouFasraw_4() { return &____wallsirouFasraw_4; }
	inline void set__wallsirouFasraw_4(float value)
	{
		____wallsirouFasraw_4 = value;
	}

	inline static int32_t get_offset_of__stemooCurhatar_5() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____stemooCurhatar_5)); }
	inline String_t* get__stemooCurhatar_5() const { return ____stemooCurhatar_5; }
	inline String_t** get_address_of__stemooCurhatar_5() { return &____stemooCurhatar_5; }
	inline void set__stemooCurhatar_5(String_t* value)
	{
		____stemooCurhatar_5 = value;
		Il2CppCodeGenWriteBarrier(&____stemooCurhatar_5, value);
	}

	inline static int32_t get_offset_of__cugiDrereku_6() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____cugiDrereku_6)); }
	inline bool get__cugiDrereku_6() const { return ____cugiDrereku_6; }
	inline bool* get_address_of__cugiDrereku_6() { return &____cugiDrereku_6; }
	inline void set__cugiDrereku_6(bool value)
	{
		____cugiDrereku_6 = value;
	}

	inline static int32_t get_offset_of__zariTearsir_7() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____zariTearsir_7)); }
	inline int32_t get__zariTearsir_7() const { return ____zariTearsir_7; }
	inline int32_t* get_address_of__zariTearsir_7() { return &____zariTearsir_7; }
	inline void set__zariTearsir_7(int32_t value)
	{
		____zariTearsir_7 = value;
	}

	inline static int32_t get_offset_of__pairsir_8() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____pairsir_8)); }
	inline uint32_t get__pairsir_8() const { return ____pairsir_8; }
	inline uint32_t* get_address_of__pairsir_8() { return &____pairsir_8; }
	inline void set__pairsir_8(uint32_t value)
	{
		____pairsir_8 = value;
	}

	inline static int32_t get_offset_of__selsagis_9() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____selsagis_9)); }
	inline int32_t get__selsagis_9() const { return ____selsagis_9; }
	inline int32_t* get_address_of__selsagis_9() { return &____selsagis_9; }
	inline void set__selsagis_9(int32_t value)
	{
		____selsagis_9 = value;
	}

	inline static int32_t get_offset_of__palair_10() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____palair_10)); }
	inline String_t* get__palair_10() const { return ____palair_10; }
	inline String_t** get_address_of__palair_10() { return &____palair_10; }
	inline void set__palair_10(String_t* value)
	{
		____palair_10 = value;
		Il2CppCodeGenWriteBarrier(&____palair_10, value);
	}

	inline static int32_t get_offset_of__drisowmairJeerel_11() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____drisowmairJeerel_11)); }
	inline int32_t get__drisowmairJeerel_11() const { return ____drisowmairJeerel_11; }
	inline int32_t* get_address_of__drisowmairJeerel_11() { return &____drisowmairJeerel_11; }
	inline void set__drisowmairJeerel_11(int32_t value)
	{
		____drisowmairJeerel_11 = value;
	}

	inline static int32_t get_offset_of__kecouNerchai_12() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____kecouNerchai_12)); }
	inline float get__kecouNerchai_12() const { return ____kecouNerchai_12; }
	inline float* get_address_of__kecouNerchai_12() { return &____kecouNerchai_12; }
	inline void set__kecouNerchai_12(float value)
	{
		____kecouNerchai_12 = value;
	}

	inline static int32_t get_offset_of__tarcisme_13() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____tarcisme_13)); }
	inline uint32_t get__tarcisme_13() const { return ____tarcisme_13; }
	inline uint32_t* get_address_of__tarcisme_13() { return &____tarcisme_13; }
	inline void set__tarcisme_13(uint32_t value)
	{
		____tarcisme_13 = value;
	}

	inline static int32_t get_offset_of__sosata_14() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____sosata_14)); }
	inline String_t* get__sosata_14() const { return ____sosata_14; }
	inline String_t** get_address_of__sosata_14() { return &____sosata_14; }
	inline void set__sosata_14(String_t* value)
	{
		____sosata_14 = value;
		Il2CppCodeGenWriteBarrier(&____sosata_14, value);
	}

	inline static int32_t get_offset_of__yeacheSeayirbo_15() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____yeacheSeayirbo_15)); }
	inline String_t* get__yeacheSeayirbo_15() const { return ____yeacheSeayirbo_15; }
	inline String_t** get_address_of__yeacheSeayirbo_15() { return &____yeacheSeayirbo_15; }
	inline void set__yeacheSeayirbo_15(String_t* value)
	{
		____yeacheSeayirbo_15 = value;
		Il2CppCodeGenWriteBarrier(&____yeacheSeayirbo_15, value);
	}

	inline static int32_t get_offset_of__wallburSibou_16() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____wallburSibou_16)); }
	inline String_t* get__wallburSibou_16() const { return ____wallburSibou_16; }
	inline String_t** get_address_of__wallburSibou_16() { return &____wallburSibou_16; }
	inline void set__wallburSibou_16(String_t* value)
	{
		____wallburSibou_16 = value;
		Il2CppCodeGenWriteBarrier(&____wallburSibou_16, value);
	}

	inline static int32_t get_offset_of__rallveedear_17() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____rallveedear_17)); }
	inline float get__rallveedear_17() const { return ____rallveedear_17; }
	inline float* get_address_of__rallveedear_17() { return &____rallveedear_17; }
	inline void set__rallveedear_17(float value)
	{
		____rallveedear_17 = value;
	}

	inline static int32_t get_offset_of__whoomoCoryeeta_18() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____whoomoCoryeeta_18)); }
	inline int32_t get__whoomoCoryeeta_18() const { return ____whoomoCoryeeta_18; }
	inline int32_t* get_address_of__whoomoCoryeeta_18() { return &____whoomoCoryeeta_18; }
	inline void set__whoomoCoryeeta_18(int32_t value)
	{
		____whoomoCoryeeta_18 = value;
	}

	inline static int32_t get_offset_of__rashi_19() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____rashi_19)); }
	inline int32_t get__rashi_19() const { return ____rashi_19; }
	inline int32_t* get_address_of__rashi_19() { return &____rashi_19; }
	inline void set__rashi_19(int32_t value)
	{
		____rashi_19 = value;
	}

	inline static int32_t get_offset_of__miqemPurgokar_20() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____miqemPurgokar_20)); }
	inline uint32_t get__miqemPurgokar_20() const { return ____miqemPurgokar_20; }
	inline uint32_t* get_address_of__miqemPurgokar_20() { return &____miqemPurgokar_20; }
	inline void set__miqemPurgokar_20(uint32_t value)
	{
		____miqemPurgokar_20 = value;
	}

	inline static int32_t get_offset_of__lorpaltorHetur_21() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____lorpaltorHetur_21)); }
	inline float get__lorpaltorHetur_21() const { return ____lorpaltorHetur_21; }
	inline float* get_address_of__lorpaltorHetur_21() { return &____lorpaltorHetur_21; }
	inline void set__lorpaltorHetur_21(float value)
	{
		____lorpaltorHetur_21 = value;
	}

	inline static int32_t get_offset_of__laljoucowDrorsesa_22() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____laljoucowDrorsesa_22)); }
	inline int32_t get__laljoucowDrorsesa_22() const { return ____laljoucowDrorsesa_22; }
	inline int32_t* get_address_of__laljoucowDrorsesa_22() { return &____laljoucowDrorsesa_22; }
	inline void set__laljoucowDrorsesa_22(int32_t value)
	{
		____laljoucowDrorsesa_22 = value;
	}

	inline static int32_t get_offset_of__ceseresa_23() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____ceseresa_23)); }
	inline String_t* get__ceseresa_23() const { return ____ceseresa_23; }
	inline String_t** get_address_of__ceseresa_23() { return &____ceseresa_23; }
	inline void set__ceseresa_23(String_t* value)
	{
		____ceseresa_23 = value;
		Il2CppCodeGenWriteBarrier(&____ceseresa_23, value);
	}

	inline static int32_t get_offset_of__sterpeatoLorsalldroo_24() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____sterpeatoLorsalldroo_24)); }
	inline float get__sterpeatoLorsalldroo_24() const { return ____sterpeatoLorsalldroo_24; }
	inline float* get_address_of__sterpeatoLorsalldroo_24() { return &____sterpeatoLorsalldroo_24; }
	inline void set__sterpeatoLorsalldroo_24(float value)
	{
		____sterpeatoLorsalldroo_24 = value;
	}

	inline static int32_t get_offset_of__derecerpurPame_25() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____derecerpurPame_25)); }
	inline bool get__derecerpurPame_25() const { return ____derecerpurPame_25; }
	inline bool* get_address_of__derecerpurPame_25() { return &____derecerpurPame_25; }
	inline void set__derecerpurPame_25(bool value)
	{
		____derecerpurPame_25 = value;
	}

	inline static int32_t get_offset_of__latow_26() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____latow_26)); }
	inline String_t* get__latow_26() const { return ____latow_26; }
	inline String_t** get_address_of__latow_26() { return &____latow_26; }
	inline void set__latow_26(String_t* value)
	{
		____latow_26 = value;
		Il2CppCodeGenWriteBarrier(&____latow_26, value);
	}

	inline static int32_t get_offset_of__jatenar_27() { return static_cast<int32_t>(offsetof(M_raicorveLallgee180_t2088707266, ____jatenar_27)); }
	inline float get__jatenar_27() const { return ____jatenar_27; }
	inline float* get_address_of__jatenar_27() { return &____jatenar_27; }
	inline void set__jatenar_27(float value)
	{
		____jatenar_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
