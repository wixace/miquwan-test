﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._2b3eb8c74147c8dab99bdc70f4605c6e
struct _2b3eb8c74147c8dab99bdc70f4605c6e_t2138106516;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._2b3eb8c74147c8dab99bdc70f4605c6e::.ctor()
extern "C"  void _2b3eb8c74147c8dab99bdc70f4605c6e__ctor_m4130267673 (_2b3eb8c74147c8dab99bdc70f4605c6e_t2138106516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._2b3eb8c74147c8dab99bdc70f4605c6e::_2b3eb8c74147c8dab99bdc70f4605c6em2(System.Int32)
extern "C"  int32_t _2b3eb8c74147c8dab99bdc70f4605c6e__2b3eb8c74147c8dab99bdc70f4605c6em2_m1475635481 (_2b3eb8c74147c8dab99bdc70f4605c6e_t2138106516 * __this, int32_t ____2b3eb8c74147c8dab99bdc70f4605c6ea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._2b3eb8c74147c8dab99bdc70f4605c6e::_2b3eb8c74147c8dab99bdc70f4605c6em(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _2b3eb8c74147c8dab99bdc70f4605c6e__2b3eb8c74147c8dab99bdc70f4605c6em_m1473883581 (_2b3eb8c74147c8dab99bdc70f4605c6e_t2138106516 * __this, int32_t ____2b3eb8c74147c8dab99bdc70f4605c6ea0, int32_t ____2b3eb8c74147c8dab99bdc70f4605c6e281, int32_t ____2b3eb8c74147c8dab99bdc70f4605c6ec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
