﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._358b2ceeb02da25bfb7d5a5003b54f3d
struct _358b2ceeb02da25bfb7d5a5003b54f3d_t2503056765;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__358b2ceeb02da25bfb7d5a502503056765.h"

// System.Void Little._358b2ceeb02da25bfb7d5a5003b54f3d::.ctor()
extern "C"  void _358b2ceeb02da25bfb7d5a5003b54f3d__ctor_m3220071760 (_358b2ceeb02da25bfb7d5a5003b54f3d_t2503056765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._358b2ceeb02da25bfb7d5a5003b54f3d::_358b2ceeb02da25bfb7d5a5003b54f3dm2(System.Int32)
extern "C"  int32_t _358b2ceeb02da25bfb7d5a5003b54f3d__358b2ceeb02da25bfb7d5a5003b54f3dm2_m1550235769 (_358b2ceeb02da25bfb7d5a5003b54f3d_t2503056765 * __this, int32_t ____358b2ceeb02da25bfb7d5a5003b54f3da0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._358b2ceeb02da25bfb7d5a5003b54f3d::_358b2ceeb02da25bfb7d5a5003b54f3dm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _358b2ceeb02da25bfb7d5a5003b54f3d__358b2ceeb02da25bfb7d5a5003b54f3dm_m2580376157 (_358b2ceeb02da25bfb7d5a5003b54f3d_t2503056765 * __this, int32_t ____358b2ceeb02da25bfb7d5a5003b54f3da0, int32_t ____358b2ceeb02da25bfb7d5a5003b54f3d741, int32_t ____358b2ceeb02da25bfb7d5a5003b54f3dc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._358b2ceeb02da25bfb7d5a5003b54f3d::ilo__358b2ceeb02da25bfb7d5a5003b54f3dm21(Little._358b2ceeb02da25bfb7d5a5003b54f3d,System.Int32)
extern "C"  int32_t _358b2ceeb02da25bfb7d5a5003b54f3d_ilo__358b2ceeb02da25bfb7d5a5003b54f3dm21_m1009052132 (Il2CppObject * __this /* static, unused */, _358b2ceeb02da25bfb7d5a5003b54f3d_t2503056765 * ____this0, int32_t ____358b2ceeb02da25bfb7d5a5003b54f3da1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
