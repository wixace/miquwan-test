﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke655669851MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.GraphNode,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m3771532541(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t474462624 *, Dictionary_2_t3142670469 *, const MethodInfo*))KeyCollection__ctor_m2092569765_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.GraphNode,System.Int32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m480143929(__this, ___item0, method) ((  void (*) (KeyCollection_t474462624 *, GraphNode_t23612370 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m199242129_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.GraphNode,System.Int32>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m465291760(__this, method) ((  void (*) (KeyCollection_t474462624 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3942090568_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.GraphNode,System.Int32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1172968757(__this, ___item0, method) ((  bool (*) (KeyCollection_t474462624 *, GraphNode_t23612370 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m496617181_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.GraphNode,System.Int32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1119113178(__this, ___item0, method) ((  bool (*) (KeyCollection_t474462624 *, GraphNode_t23612370 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m175393666_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.GraphNode,System.Int32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1509941314(__this, method) ((  Il2CppObject* (*) (KeyCollection_t474462624 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2466934938_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.GraphNode,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m2525906530(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t474462624 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m505462714_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.GraphNode,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1359560561(__this, method) ((  Il2CppObject * (*) (KeyCollection_t474462624 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3955992777_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.GraphNode,System.Int32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m527635286(__this, method) ((  bool (*) (KeyCollection_t474462624 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3388799742_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.GraphNode,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3711905672(__this, method) ((  bool (*) (KeyCollection_t474462624 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2230257968_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.GraphNode,System.Int32>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2317692346(__this, method) ((  Il2CppObject * (*) (KeyCollection_t474462624 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m281315426_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.GraphNode,System.Int32>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m2540019634(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t474462624 *, GraphNodeU5BU5D_t927449255*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3090894682_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.GraphNode,System.Int32>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1992754175(__this, method) ((  Enumerator_t3757606523  (*) (KeyCollection_t474462624 *, const MethodInfo*))KeyCollection_GetEnumerator_m363545767_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.GraphNode,System.Int32>::get_Count()
#define KeyCollection_get_Count_m2561423170(__this, method) ((  int32_t (*) (KeyCollection_t474462624 *, const MethodInfo*))KeyCollection_get_Count_m264049386_gshared)(__this, method)
