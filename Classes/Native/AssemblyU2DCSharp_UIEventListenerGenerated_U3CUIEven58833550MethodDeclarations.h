﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEventListenerGenerated/<UIEventListener_onDrag_GetDelegate_member9_arg0>c__AnonStoreyBD
struct U3CUIEventListener_onDrag_GetDelegate_member9_arg0U3Ec__AnonStoreyBD_t58833550;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UIEventListenerGenerated/<UIEventListener_onDrag_GetDelegate_member9_arg0>c__AnonStoreyBD::.ctor()
extern "C"  void U3CUIEventListener_onDrag_GetDelegate_member9_arg0U3Ec__AnonStoreyBD__ctor_m2695187101 (U3CUIEventListener_onDrag_GetDelegate_member9_arg0U3Ec__AnonStoreyBD_t58833550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated/<UIEventListener_onDrag_GetDelegate_member9_arg0>c__AnonStoreyBD::<>m__13E(UnityEngine.GameObject,UnityEngine.Vector2)
extern "C"  void U3CUIEventListener_onDrag_GetDelegate_member9_arg0U3Ec__AnonStoreyBD_U3CU3Em__13E_m3223498827 (U3CUIEventListener_onDrag_GetDelegate_member9_arg0U3Ec__AnonStoreyBD_t58833550 * __this, GameObject_t3674682005 * ___go0, Vector2_t4282066565  ___delta1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
