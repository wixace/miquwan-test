﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_daircur34
struct M_daircur34_t454400315;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_daircur34454400315.h"

// System.Void GarbageiOS.M_daircur34::.ctor()
extern "C"  void M_daircur34__ctor_m4070095496 (M_daircur34_t454400315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_daircur34::M_qamelCeedrall0(System.String[],System.Int32)
extern "C"  void M_daircur34_M_qamelCeedrall0_m878413025 (M_daircur34_t454400315 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_daircur34::M_calzirbiNanai1(System.String[],System.Int32)
extern "C"  void M_daircur34_M_calzirbiNanai1_m1988326479 (M_daircur34_t454400315 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_daircur34::M_dexairNerbai2(System.String[],System.Int32)
extern "C"  void M_daircur34_M_dexairNerbai2_m549609211 (M_daircur34_t454400315 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_daircur34::M_heakurke3(System.String[],System.Int32)
extern "C"  void M_daircur34_M_heakurke3_m2571335264 (M_daircur34_t454400315 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_daircur34::ilo_M_dexairNerbai21(GarbageiOS.M_daircur34,System.String[],System.Int32)
extern "C"  void M_daircur34_ilo_M_dexairNerbai21_m1458021486 (Il2CppObject * __this /* static, unused */, M_daircur34_t454400315 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
