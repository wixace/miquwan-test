﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VersionInfo
struct VersionInfo_t2356638086;

#include "codegen/il2cpp-codegen.h"

// System.Void VersionInfo::.ctor()
extern "C"  void VersionInfo__ctor_m636772517 (VersionInfo_t2356638086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VersionInfo::get_isFirst()
extern "C"  bool VersionInfo_get_isFirst_m786151060 (VersionInfo_t2356638086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
