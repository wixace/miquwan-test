﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.LocalAvoidance/IntersectionPair
struct IntersectionPair_t2821319919;
struct IntersectionPair_t2821319919_marshaled_pinvoke;
struct IntersectionPair_t2821319919_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_Inter2821319919.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_Inter1564900636.h"

// System.Void Pathfinding.LocalAvoidance/IntersectionPair::.ctor(System.Single,System.Boolean)
extern "C"  void IntersectionPair__ctor_m2005405646 (IntersectionPair_t2821319919 * __this, float ___factor0, bool ___inside1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.LocalAvoidance/IntersectionPair::SetState(Pathfinding.LocalAvoidance/IntersectionState)
extern "C"  void IntersectionPair_SetState_m3385102811 (IntersectionPair_t2821319919 * __this, int32_t ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.LocalAvoidance/IntersectionPair::CompareTo(Pathfinding.LocalAvoidance/IntersectionPair)
extern "C"  int32_t IntersectionPair_CompareTo_m1072362907 (IntersectionPair_t2821319919 * __this, IntersectionPair_t2821319919  ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct IntersectionPair_t2821319919;
struct IntersectionPair_t2821319919_marshaled_pinvoke;

extern "C" void IntersectionPair_t2821319919_marshal_pinvoke(const IntersectionPair_t2821319919& unmarshaled, IntersectionPair_t2821319919_marshaled_pinvoke& marshaled);
extern "C" void IntersectionPair_t2821319919_marshal_pinvoke_back(const IntersectionPair_t2821319919_marshaled_pinvoke& marshaled, IntersectionPair_t2821319919& unmarshaled);
extern "C" void IntersectionPair_t2821319919_marshal_pinvoke_cleanup(IntersectionPair_t2821319919_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct IntersectionPair_t2821319919;
struct IntersectionPair_t2821319919_marshaled_com;

extern "C" void IntersectionPair_t2821319919_marshal_com(const IntersectionPair_t2821319919& unmarshaled, IntersectionPair_t2821319919_marshaled_com& marshaled);
extern "C" void IntersectionPair_t2821319919_marshal_com_back(const IntersectionPair_t2821319919_marshaled_com& marshaled, IntersectionPair_t2821319919& unmarshaled);
extern "C" void IntersectionPair_t2821319919_marshal_com_cleanup(IntersectionPair_t2821319919_marshaled_com& marshaled);
