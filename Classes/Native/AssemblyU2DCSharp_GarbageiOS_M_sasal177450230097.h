﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_sasal177
struct  M_sasal177_t450230097  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_sasal177::_rasripi
	bool ____rasripi_0;
	// System.Int32 GarbageiOS.M_sasal177::_teetisteHerine
	int32_t ____teetisteHerine_1;
	// System.UInt32 GarbageiOS.M_sasal177::_trinileChallnur
	uint32_t ____trinileChallnur_2;
	// System.Boolean GarbageiOS.M_sasal177::_tepurmel
	bool ____tepurmel_3;
	// System.Boolean GarbageiOS.M_sasal177::_katodirPairlidas
	bool ____katodirPairlidas_4;
	// System.Boolean GarbageiOS.M_sasal177::_kajoo
	bool ____kajoo_5;
	// System.Single GarbageiOS.M_sasal177::_cawmasrerResunar
	float ____cawmasrerResunar_6;
	// System.Int32 GarbageiOS.M_sasal177::_sorcalair
	int32_t ____sorcalair_7;
	// System.Int32 GarbageiOS.M_sasal177::_qegarwar
	int32_t ____qegarwar_8;
	// System.Single GarbageiOS.M_sasal177::_keazerekas
	float ____keazerekas_9;
	// System.Boolean GarbageiOS.M_sasal177::_lerxu
	bool ____lerxu_10;
	// System.Single GarbageiOS.M_sasal177::_faifair
	float ____faifair_11;
	// System.UInt32 GarbageiOS.M_sasal177::_whowkairBoulispoo
	uint32_t ____whowkairBoulispoo_12;
	// System.UInt32 GarbageiOS.M_sasal177::_cirjerxi
	uint32_t ____cirjerxi_13;
	// System.String GarbageiOS.M_sasal177::_casjerbeaPoojall
	String_t* ____casjerbeaPoojall_14;
	// System.UInt32 GarbageiOS.M_sasal177::_nirjee
	uint32_t ____nirjee_15;
	// System.Boolean GarbageiOS.M_sasal177::_lobe
	bool ____lobe_16;
	// System.Single GarbageiOS.M_sasal177::_sawloowaHide
	float ____sawloowaHide_17;
	// System.Single GarbageiOS.M_sasal177::_firtrur
	float ____firtrur_18;
	// System.Boolean GarbageiOS.M_sasal177::_nutallvaFireno
	bool ____nutallvaFireno_19;
	// System.UInt32 GarbageiOS.M_sasal177::_sitay
	uint32_t ____sitay_20;
	// System.Boolean GarbageiOS.M_sasal177::_yazeregir
	bool ____yazeregir_21;
	// System.Int32 GarbageiOS.M_sasal177::_rutebear
	int32_t ____rutebear_22;
	// System.Single GarbageiOS.M_sasal177::_stertePesawdir
	float ____stertePesawdir_23;
	// System.Single GarbageiOS.M_sasal177::_zascowSaganar
	float ____zascowSaganar_24;
	// System.String GarbageiOS.M_sasal177::_xowloseFeseremay
	String_t* ____xowloseFeseremay_25;
	// System.Int32 GarbageiOS.M_sasal177::_wocalLearmer
	int32_t ____wocalLearmer_26;
	// System.Single GarbageiOS.M_sasal177::_cijease
	float ____cijease_27;
	// System.UInt32 GarbageiOS.M_sasal177::_jokelgouRanule
	uint32_t ____jokelgouRanule_28;
	// System.UInt32 GarbageiOS.M_sasal177::_dirurnereJutoo
	uint32_t ____dirurnereJutoo_29;
	// System.UInt32 GarbageiOS.M_sasal177::_sererpeMeeca
	uint32_t ____sererpeMeeca_30;
	// System.String GarbageiOS.M_sasal177::_qomezaw
	String_t* ____qomezaw_31;
	// System.Int32 GarbageiOS.M_sasal177::_selbairnea
	int32_t ____selbairnea_32;
	// System.Boolean GarbageiOS.M_sasal177::_garrearNojeto
	bool ____garrearNojeto_33;
	// System.Single GarbageiOS.M_sasal177::_dipowGallcefu
	float ____dipowGallcefu_34;
	// System.Int32 GarbageiOS.M_sasal177::_toupeKaihocow
	int32_t ____toupeKaihocow_35;
	// System.UInt32 GarbageiOS.M_sasal177::_rooris
	uint32_t ____rooris_36;
	// System.Single GarbageiOS.M_sasal177::_micur
	float ____micur_37;
	// System.Boolean GarbageiOS.M_sasal177::_cirkeze
	bool ____cirkeze_38;
	// System.String GarbageiOS.M_sasal177::_souqallneDrupime
	String_t* ____souqallneDrupime_39;
	// System.Int32 GarbageiOS.M_sasal177::_cisem
	int32_t ____cisem_40;
	// System.Boolean GarbageiOS.M_sasal177::_dealibeaLatir
	bool ____dealibeaLatir_41;

public:
	inline static int32_t get_offset_of__rasripi_0() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____rasripi_0)); }
	inline bool get__rasripi_0() const { return ____rasripi_0; }
	inline bool* get_address_of__rasripi_0() { return &____rasripi_0; }
	inline void set__rasripi_0(bool value)
	{
		____rasripi_0 = value;
	}

	inline static int32_t get_offset_of__teetisteHerine_1() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____teetisteHerine_1)); }
	inline int32_t get__teetisteHerine_1() const { return ____teetisteHerine_1; }
	inline int32_t* get_address_of__teetisteHerine_1() { return &____teetisteHerine_1; }
	inline void set__teetisteHerine_1(int32_t value)
	{
		____teetisteHerine_1 = value;
	}

	inline static int32_t get_offset_of__trinileChallnur_2() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____trinileChallnur_2)); }
	inline uint32_t get__trinileChallnur_2() const { return ____trinileChallnur_2; }
	inline uint32_t* get_address_of__trinileChallnur_2() { return &____trinileChallnur_2; }
	inline void set__trinileChallnur_2(uint32_t value)
	{
		____trinileChallnur_2 = value;
	}

	inline static int32_t get_offset_of__tepurmel_3() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____tepurmel_3)); }
	inline bool get__tepurmel_3() const { return ____tepurmel_3; }
	inline bool* get_address_of__tepurmel_3() { return &____tepurmel_3; }
	inline void set__tepurmel_3(bool value)
	{
		____tepurmel_3 = value;
	}

	inline static int32_t get_offset_of__katodirPairlidas_4() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____katodirPairlidas_4)); }
	inline bool get__katodirPairlidas_4() const { return ____katodirPairlidas_4; }
	inline bool* get_address_of__katodirPairlidas_4() { return &____katodirPairlidas_4; }
	inline void set__katodirPairlidas_4(bool value)
	{
		____katodirPairlidas_4 = value;
	}

	inline static int32_t get_offset_of__kajoo_5() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____kajoo_5)); }
	inline bool get__kajoo_5() const { return ____kajoo_5; }
	inline bool* get_address_of__kajoo_5() { return &____kajoo_5; }
	inline void set__kajoo_5(bool value)
	{
		____kajoo_5 = value;
	}

	inline static int32_t get_offset_of__cawmasrerResunar_6() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____cawmasrerResunar_6)); }
	inline float get__cawmasrerResunar_6() const { return ____cawmasrerResunar_6; }
	inline float* get_address_of__cawmasrerResunar_6() { return &____cawmasrerResunar_6; }
	inline void set__cawmasrerResunar_6(float value)
	{
		____cawmasrerResunar_6 = value;
	}

	inline static int32_t get_offset_of__sorcalair_7() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____sorcalair_7)); }
	inline int32_t get__sorcalair_7() const { return ____sorcalair_7; }
	inline int32_t* get_address_of__sorcalair_7() { return &____sorcalair_7; }
	inline void set__sorcalair_7(int32_t value)
	{
		____sorcalair_7 = value;
	}

	inline static int32_t get_offset_of__qegarwar_8() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____qegarwar_8)); }
	inline int32_t get__qegarwar_8() const { return ____qegarwar_8; }
	inline int32_t* get_address_of__qegarwar_8() { return &____qegarwar_8; }
	inline void set__qegarwar_8(int32_t value)
	{
		____qegarwar_8 = value;
	}

	inline static int32_t get_offset_of__keazerekas_9() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____keazerekas_9)); }
	inline float get__keazerekas_9() const { return ____keazerekas_9; }
	inline float* get_address_of__keazerekas_9() { return &____keazerekas_9; }
	inline void set__keazerekas_9(float value)
	{
		____keazerekas_9 = value;
	}

	inline static int32_t get_offset_of__lerxu_10() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____lerxu_10)); }
	inline bool get__lerxu_10() const { return ____lerxu_10; }
	inline bool* get_address_of__lerxu_10() { return &____lerxu_10; }
	inline void set__lerxu_10(bool value)
	{
		____lerxu_10 = value;
	}

	inline static int32_t get_offset_of__faifair_11() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____faifair_11)); }
	inline float get__faifair_11() const { return ____faifair_11; }
	inline float* get_address_of__faifair_11() { return &____faifair_11; }
	inline void set__faifair_11(float value)
	{
		____faifair_11 = value;
	}

	inline static int32_t get_offset_of__whowkairBoulispoo_12() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____whowkairBoulispoo_12)); }
	inline uint32_t get__whowkairBoulispoo_12() const { return ____whowkairBoulispoo_12; }
	inline uint32_t* get_address_of__whowkairBoulispoo_12() { return &____whowkairBoulispoo_12; }
	inline void set__whowkairBoulispoo_12(uint32_t value)
	{
		____whowkairBoulispoo_12 = value;
	}

	inline static int32_t get_offset_of__cirjerxi_13() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____cirjerxi_13)); }
	inline uint32_t get__cirjerxi_13() const { return ____cirjerxi_13; }
	inline uint32_t* get_address_of__cirjerxi_13() { return &____cirjerxi_13; }
	inline void set__cirjerxi_13(uint32_t value)
	{
		____cirjerxi_13 = value;
	}

	inline static int32_t get_offset_of__casjerbeaPoojall_14() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____casjerbeaPoojall_14)); }
	inline String_t* get__casjerbeaPoojall_14() const { return ____casjerbeaPoojall_14; }
	inline String_t** get_address_of__casjerbeaPoojall_14() { return &____casjerbeaPoojall_14; }
	inline void set__casjerbeaPoojall_14(String_t* value)
	{
		____casjerbeaPoojall_14 = value;
		Il2CppCodeGenWriteBarrier(&____casjerbeaPoojall_14, value);
	}

	inline static int32_t get_offset_of__nirjee_15() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____nirjee_15)); }
	inline uint32_t get__nirjee_15() const { return ____nirjee_15; }
	inline uint32_t* get_address_of__nirjee_15() { return &____nirjee_15; }
	inline void set__nirjee_15(uint32_t value)
	{
		____nirjee_15 = value;
	}

	inline static int32_t get_offset_of__lobe_16() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____lobe_16)); }
	inline bool get__lobe_16() const { return ____lobe_16; }
	inline bool* get_address_of__lobe_16() { return &____lobe_16; }
	inline void set__lobe_16(bool value)
	{
		____lobe_16 = value;
	}

	inline static int32_t get_offset_of__sawloowaHide_17() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____sawloowaHide_17)); }
	inline float get__sawloowaHide_17() const { return ____sawloowaHide_17; }
	inline float* get_address_of__sawloowaHide_17() { return &____sawloowaHide_17; }
	inline void set__sawloowaHide_17(float value)
	{
		____sawloowaHide_17 = value;
	}

	inline static int32_t get_offset_of__firtrur_18() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____firtrur_18)); }
	inline float get__firtrur_18() const { return ____firtrur_18; }
	inline float* get_address_of__firtrur_18() { return &____firtrur_18; }
	inline void set__firtrur_18(float value)
	{
		____firtrur_18 = value;
	}

	inline static int32_t get_offset_of__nutallvaFireno_19() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____nutallvaFireno_19)); }
	inline bool get__nutallvaFireno_19() const { return ____nutallvaFireno_19; }
	inline bool* get_address_of__nutallvaFireno_19() { return &____nutallvaFireno_19; }
	inline void set__nutallvaFireno_19(bool value)
	{
		____nutallvaFireno_19 = value;
	}

	inline static int32_t get_offset_of__sitay_20() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____sitay_20)); }
	inline uint32_t get__sitay_20() const { return ____sitay_20; }
	inline uint32_t* get_address_of__sitay_20() { return &____sitay_20; }
	inline void set__sitay_20(uint32_t value)
	{
		____sitay_20 = value;
	}

	inline static int32_t get_offset_of__yazeregir_21() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____yazeregir_21)); }
	inline bool get__yazeregir_21() const { return ____yazeregir_21; }
	inline bool* get_address_of__yazeregir_21() { return &____yazeregir_21; }
	inline void set__yazeregir_21(bool value)
	{
		____yazeregir_21 = value;
	}

	inline static int32_t get_offset_of__rutebear_22() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____rutebear_22)); }
	inline int32_t get__rutebear_22() const { return ____rutebear_22; }
	inline int32_t* get_address_of__rutebear_22() { return &____rutebear_22; }
	inline void set__rutebear_22(int32_t value)
	{
		____rutebear_22 = value;
	}

	inline static int32_t get_offset_of__stertePesawdir_23() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____stertePesawdir_23)); }
	inline float get__stertePesawdir_23() const { return ____stertePesawdir_23; }
	inline float* get_address_of__stertePesawdir_23() { return &____stertePesawdir_23; }
	inline void set__stertePesawdir_23(float value)
	{
		____stertePesawdir_23 = value;
	}

	inline static int32_t get_offset_of__zascowSaganar_24() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____zascowSaganar_24)); }
	inline float get__zascowSaganar_24() const { return ____zascowSaganar_24; }
	inline float* get_address_of__zascowSaganar_24() { return &____zascowSaganar_24; }
	inline void set__zascowSaganar_24(float value)
	{
		____zascowSaganar_24 = value;
	}

	inline static int32_t get_offset_of__xowloseFeseremay_25() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____xowloseFeseremay_25)); }
	inline String_t* get__xowloseFeseremay_25() const { return ____xowloseFeseremay_25; }
	inline String_t** get_address_of__xowloseFeseremay_25() { return &____xowloseFeseremay_25; }
	inline void set__xowloseFeseremay_25(String_t* value)
	{
		____xowloseFeseremay_25 = value;
		Il2CppCodeGenWriteBarrier(&____xowloseFeseremay_25, value);
	}

	inline static int32_t get_offset_of__wocalLearmer_26() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____wocalLearmer_26)); }
	inline int32_t get__wocalLearmer_26() const { return ____wocalLearmer_26; }
	inline int32_t* get_address_of__wocalLearmer_26() { return &____wocalLearmer_26; }
	inline void set__wocalLearmer_26(int32_t value)
	{
		____wocalLearmer_26 = value;
	}

	inline static int32_t get_offset_of__cijease_27() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____cijease_27)); }
	inline float get__cijease_27() const { return ____cijease_27; }
	inline float* get_address_of__cijease_27() { return &____cijease_27; }
	inline void set__cijease_27(float value)
	{
		____cijease_27 = value;
	}

	inline static int32_t get_offset_of__jokelgouRanule_28() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____jokelgouRanule_28)); }
	inline uint32_t get__jokelgouRanule_28() const { return ____jokelgouRanule_28; }
	inline uint32_t* get_address_of__jokelgouRanule_28() { return &____jokelgouRanule_28; }
	inline void set__jokelgouRanule_28(uint32_t value)
	{
		____jokelgouRanule_28 = value;
	}

	inline static int32_t get_offset_of__dirurnereJutoo_29() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____dirurnereJutoo_29)); }
	inline uint32_t get__dirurnereJutoo_29() const { return ____dirurnereJutoo_29; }
	inline uint32_t* get_address_of__dirurnereJutoo_29() { return &____dirurnereJutoo_29; }
	inline void set__dirurnereJutoo_29(uint32_t value)
	{
		____dirurnereJutoo_29 = value;
	}

	inline static int32_t get_offset_of__sererpeMeeca_30() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____sererpeMeeca_30)); }
	inline uint32_t get__sererpeMeeca_30() const { return ____sererpeMeeca_30; }
	inline uint32_t* get_address_of__sererpeMeeca_30() { return &____sererpeMeeca_30; }
	inline void set__sererpeMeeca_30(uint32_t value)
	{
		____sererpeMeeca_30 = value;
	}

	inline static int32_t get_offset_of__qomezaw_31() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____qomezaw_31)); }
	inline String_t* get__qomezaw_31() const { return ____qomezaw_31; }
	inline String_t** get_address_of__qomezaw_31() { return &____qomezaw_31; }
	inline void set__qomezaw_31(String_t* value)
	{
		____qomezaw_31 = value;
		Il2CppCodeGenWriteBarrier(&____qomezaw_31, value);
	}

	inline static int32_t get_offset_of__selbairnea_32() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____selbairnea_32)); }
	inline int32_t get__selbairnea_32() const { return ____selbairnea_32; }
	inline int32_t* get_address_of__selbairnea_32() { return &____selbairnea_32; }
	inline void set__selbairnea_32(int32_t value)
	{
		____selbairnea_32 = value;
	}

	inline static int32_t get_offset_of__garrearNojeto_33() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____garrearNojeto_33)); }
	inline bool get__garrearNojeto_33() const { return ____garrearNojeto_33; }
	inline bool* get_address_of__garrearNojeto_33() { return &____garrearNojeto_33; }
	inline void set__garrearNojeto_33(bool value)
	{
		____garrearNojeto_33 = value;
	}

	inline static int32_t get_offset_of__dipowGallcefu_34() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____dipowGallcefu_34)); }
	inline float get__dipowGallcefu_34() const { return ____dipowGallcefu_34; }
	inline float* get_address_of__dipowGallcefu_34() { return &____dipowGallcefu_34; }
	inline void set__dipowGallcefu_34(float value)
	{
		____dipowGallcefu_34 = value;
	}

	inline static int32_t get_offset_of__toupeKaihocow_35() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____toupeKaihocow_35)); }
	inline int32_t get__toupeKaihocow_35() const { return ____toupeKaihocow_35; }
	inline int32_t* get_address_of__toupeKaihocow_35() { return &____toupeKaihocow_35; }
	inline void set__toupeKaihocow_35(int32_t value)
	{
		____toupeKaihocow_35 = value;
	}

	inline static int32_t get_offset_of__rooris_36() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____rooris_36)); }
	inline uint32_t get__rooris_36() const { return ____rooris_36; }
	inline uint32_t* get_address_of__rooris_36() { return &____rooris_36; }
	inline void set__rooris_36(uint32_t value)
	{
		____rooris_36 = value;
	}

	inline static int32_t get_offset_of__micur_37() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____micur_37)); }
	inline float get__micur_37() const { return ____micur_37; }
	inline float* get_address_of__micur_37() { return &____micur_37; }
	inline void set__micur_37(float value)
	{
		____micur_37 = value;
	}

	inline static int32_t get_offset_of__cirkeze_38() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____cirkeze_38)); }
	inline bool get__cirkeze_38() const { return ____cirkeze_38; }
	inline bool* get_address_of__cirkeze_38() { return &____cirkeze_38; }
	inline void set__cirkeze_38(bool value)
	{
		____cirkeze_38 = value;
	}

	inline static int32_t get_offset_of__souqallneDrupime_39() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____souqallneDrupime_39)); }
	inline String_t* get__souqallneDrupime_39() const { return ____souqallneDrupime_39; }
	inline String_t** get_address_of__souqallneDrupime_39() { return &____souqallneDrupime_39; }
	inline void set__souqallneDrupime_39(String_t* value)
	{
		____souqallneDrupime_39 = value;
		Il2CppCodeGenWriteBarrier(&____souqallneDrupime_39, value);
	}

	inline static int32_t get_offset_of__cisem_40() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____cisem_40)); }
	inline int32_t get__cisem_40() const { return ____cisem_40; }
	inline int32_t* get_address_of__cisem_40() { return &____cisem_40; }
	inline void set__cisem_40(int32_t value)
	{
		____cisem_40 = value;
	}

	inline static int32_t get_offset_of__dealibeaLatir_41() { return static_cast<int32_t>(offsetof(M_sasal177_t450230097, ____dealibeaLatir_41)); }
	inline bool get__dealibeaLatir_41() const { return ____dealibeaLatir_41; }
	inline bool* get_address_of__dealibeaLatir_41() { return &____dealibeaLatir_41; }
	inline void set__dealibeaLatir_41(bool value)
	{
		____dealibeaLatir_41 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
