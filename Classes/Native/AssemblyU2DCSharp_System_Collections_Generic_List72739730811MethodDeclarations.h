﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_FindLastIndex__PredicateT1_T>c__AnonStorey90
struct U3CListA1_FindLastIndex__PredicateT1_TU3Ec__AnonStorey90_t2739730811;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_FindLastIndex__PredicateT1_T>c__AnonStorey90::.ctor()
extern "C"  void U3CListA1_FindLastIndex__PredicateT1_TU3Ec__AnonStorey90__ctor_m1098266704 (U3CListA1_FindLastIndex__PredicateT1_TU3Ec__AnonStorey90_t2739730811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Collections_Generic_List77Generated/<ListA1_FindLastIndex__PredicateT1_T>c__AnonStorey90::<>m__B5()
extern "C"  Il2CppObject * U3CListA1_FindLastIndex__PredicateT1_TU3Ec__AnonStorey90_U3CU3Em__B5_m999823617 (U3CListA1_FindLastIndex__PredicateT1_TU3Ec__AnonStorey90_t2739730811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
