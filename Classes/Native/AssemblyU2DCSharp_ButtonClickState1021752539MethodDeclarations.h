﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonClickState
struct ButtonClickState_t1021752539;
// System.Object
struct Il2CppObject;
// ButtonClickState/VoidDelegate
struct VoidDelegate_t3553201165;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ButtonClickState_VoidDelegate3553201165.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_ButtonClickState1021752539.h"

// System.Void ButtonClickState::.ctor()
extern "C"  void ButtonClickState__ctor_m1821161504 (ButtonClickState_t1021752539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ButtonClickState::get_value()
extern "C"  bool ButtonClickState_get_value_m843646450 (ButtonClickState_t1021752539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickState::set_value(System.Boolean)
extern "C"  void ButtonClickState_set_value_m634494761 (ButtonClickState_t1021752539 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickState::OnClick()
extern "C"  void ButtonClickState_OnClick_m3928721063 (ButtonClickState_t1021752539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickState::OnPress(System.Boolean)
extern "C"  void ButtonClickState_OnPress_m1717900441 (ButtonClickState_t1021752539 * __this, bool ___ispress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickState::OnHover(System.Boolean)
extern "C"  void ButtonClickState_OnHover_m3798214546 (ButtonClickState_t1021752539 * __this, bool ___ishover0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickState::Update()
extern "C"  void ButtonClickState_Update_m2348293869 (ButtonClickState_t1021752539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickState::OnDestroy()
extern "C"  void ButtonClickState_OnDestroy_m4177886489 (ButtonClickState_t1021752539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickState::SetFunctionState(System.Object)
extern "C"  void ButtonClickState_SetFunctionState_m610997305 (ButtonClickState_t1021752539 * __this, Il2CppObject * ___isactive0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickState::SetFunctionState(System.Boolean)
extern "C"  void ButtonClickState_SetFunctionState_m3676616562 (ButtonClickState_t1021752539 * __this, bool ___isactive0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickState::ilo_Invoke1(ButtonClickState/VoidDelegate,UnityEngine.GameObject,System.Boolean)
extern "C"  void ButtonClickState_ilo_Invoke1_m3652903968 (Il2CppObject * __this /* static, unused */, VoidDelegate_t3553201165 * ____this0, GameObject_t3674682005 * ___go1, bool ___isPress2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickState::ilo_set_value2(ButtonClickState,System.Boolean)
extern "C"  void ButtonClickState_ilo_set_value2_m2770186979 (Il2CppObject * __this /* static, unused */, ButtonClickState_t1021752539 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ButtonClickState::ilo_get_value3(ButtonClickState)
extern "C"  bool ButtonClickState_ilo_get_value3_m3523339355 (Il2CppObject * __this /* static, unused */, ButtonClickState_t1021752539 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
