﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginTianYing
struct  PluginTianYing_t4234189246  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginTianYing::userId
	String_t* ___userId_2;
	// System.String PluginTianYing::channelID
	String_t* ___channelID_3;
	// System.String PluginTianYing::sign
	String_t* ___sign_4;
	// System.String PluginTianYing::userName
	String_t* ___userName_5;
	// System.Boolean PluginTianYing::isOut
	bool ___isOut_6;
	// System.String PluginTianYing::configId
	String_t* ___configId_7;
	// Mihua.SDK.PayInfo PluginTianYing::payInfo
	PayInfo_t1775308120 * ___payInfo_8;

public:
	inline static int32_t get_offset_of_userId_2() { return static_cast<int32_t>(offsetof(PluginTianYing_t4234189246, ___userId_2)); }
	inline String_t* get_userId_2() const { return ___userId_2; }
	inline String_t** get_address_of_userId_2() { return &___userId_2; }
	inline void set_userId_2(String_t* value)
	{
		___userId_2 = value;
		Il2CppCodeGenWriteBarrier(&___userId_2, value);
	}

	inline static int32_t get_offset_of_channelID_3() { return static_cast<int32_t>(offsetof(PluginTianYing_t4234189246, ___channelID_3)); }
	inline String_t* get_channelID_3() const { return ___channelID_3; }
	inline String_t** get_address_of_channelID_3() { return &___channelID_3; }
	inline void set_channelID_3(String_t* value)
	{
		___channelID_3 = value;
		Il2CppCodeGenWriteBarrier(&___channelID_3, value);
	}

	inline static int32_t get_offset_of_sign_4() { return static_cast<int32_t>(offsetof(PluginTianYing_t4234189246, ___sign_4)); }
	inline String_t* get_sign_4() const { return ___sign_4; }
	inline String_t** get_address_of_sign_4() { return &___sign_4; }
	inline void set_sign_4(String_t* value)
	{
		___sign_4 = value;
		Il2CppCodeGenWriteBarrier(&___sign_4, value);
	}

	inline static int32_t get_offset_of_userName_5() { return static_cast<int32_t>(offsetof(PluginTianYing_t4234189246, ___userName_5)); }
	inline String_t* get_userName_5() const { return ___userName_5; }
	inline String_t** get_address_of_userName_5() { return &___userName_5; }
	inline void set_userName_5(String_t* value)
	{
		___userName_5 = value;
		Il2CppCodeGenWriteBarrier(&___userName_5, value);
	}

	inline static int32_t get_offset_of_isOut_6() { return static_cast<int32_t>(offsetof(PluginTianYing_t4234189246, ___isOut_6)); }
	inline bool get_isOut_6() const { return ___isOut_6; }
	inline bool* get_address_of_isOut_6() { return &___isOut_6; }
	inline void set_isOut_6(bool value)
	{
		___isOut_6 = value;
	}

	inline static int32_t get_offset_of_configId_7() { return static_cast<int32_t>(offsetof(PluginTianYing_t4234189246, ___configId_7)); }
	inline String_t* get_configId_7() const { return ___configId_7; }
	inline String_t** get_address_of_configId_7() { return &___configId_7; }
	inline void set_configId_7(String_t* value)
	{
		___configId_7 = value;
		Il2CppCodeGenWriteBarrier(&___configId_7, value);
	}

	inline static int32_t get_offset_of_payInfo_8() { return static_cast<int32_t>(offsetof(PluginTianYing_t4234189246, ___payInfo_8)); }
	inline PayInfo_t1775308120 * get_payInfo_8() const { return ___payInfo_8; }
	inline PayInfo_t1775308120 ** get_address_of_payInfo_8() { return &___payInfo_8; }
	inline void set_payInfo_8(PayInfo_t1775308120 * value)
	{
		___payInfo_8 = value;
		Il2CppCodeGenWriteBarrier(&___payInfo_8, value);
	}
};

struct PluginTianYing_t4234189246_StaticFields
{
public:
	// System.Action PluginTianYing::<>f__am$cache7
	Action_t3771233898 * ___U3CU3Ef__amU24cache7_9;
	// System.Action PluginTianYing::<>f__am$cache8
	Action_t3771233898 * ___U3CU3Ef__amU24cache8_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_9() { return static_cast<int32_t>(offsetof(PluginTianYing_t4234189246_StaticFields, ___U3CU3Ef__amU24cache7_9)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache7_9() const { return ___U3CU3Ef__amU24cache7_9; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache7_9() { return &___U3CU3Ef__amU24cache7_9; }
	inline void set_U3CU3Ef__amU24cache7_9(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache7_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_10() { return static_cast<int32_t>(offsetof(PluginTianYing_t4234189246_StaticFields, ___U3CU3Ef__amU24cache8_10)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache8_10() const { return ___U3CU3Ef__amU24cache8_10; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache8_10() { return &___U3CU3Ef__amU24cache8_10; }
	inline void set_U3CU3Ef__amU24cache8_10(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache8_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
