﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_hoviwer24
struct M_hoviwer24_t3152002604;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_hoviwer243152002604.h"

// System.Void GarbageiOS.M_hoviwer24::.ctor()
extern "C"  void M_hoviwer24__ctor_m3793392823 (M_hoviwer24_t3152002604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hoviwer24::M_turferLeawi0(System.String[],System.Int32)
extern "C"  void M_hoviwer24_M_turferLeawi0_m260867214 (M_hoviwer24_t3152002604 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hoviwer24::M_securoMoukulee1(System.String[],System.Int32)
extern "C"  void M_hoviwer24_M_securoMoukulee1_m3946946523 (M_hoviwer24_t3152002604 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hoviwer24::M_saihelka2(System.String[],System.Int32)
extern "C"  void M_hoviwer24_M_saihelka2_m702990466 (M_hoviwer24_t3152002604 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hoviwer24::M_xirsawdir3(System.String[],System.Int32)
extern "C"  void M_hoviwer24_M_xirsawdir3_m1110731876 (M_hoviwer24_t3152002604 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hoviwer24::ilo_M_turferLeawi01(GarbageiOS.M_hoviwer24,System.String[],System.Int32)
extern "C"  void M_hoviwer24_ilo_M_turferLeawi01_m3299008952 (Il2CppObject * __this /* static, unused */, M_hoviwer24_t3152002604 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
