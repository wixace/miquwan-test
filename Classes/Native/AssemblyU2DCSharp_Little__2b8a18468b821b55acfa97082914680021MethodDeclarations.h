﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._2b8a18468b821b55acfa97081ccdfbc6
struct _2b8a18468b821b55acfa97081ccdfbc6_t2914680021;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._2b8a18468b821b55acfa97081ccdfbc6::.ctor()
extern "C"  void _2b8a18468b821b55acfa97081ccdfbc6__ctor_m3006220536 (_2b8a18468b821b55acfa97081ccdfbc6_t2914680021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._2b8a18468b821b55acfa97081ccdfbc6::_2b8a18468b821b55acfa97081ccdfbc6m2(System.Int32)
extern "C"  int32_t _2b8a18468b821b55acfa97081ccdfbc6__2b8a18468b821b55acfa97081ccdfbc6m2_m1931196793 (_2b8a18468b821b55acfa97081ccdfbc6_t2914680021 * __this, int32_t ____2b8a18468b821b55acfa97081ccdfbc6a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._2b8a18468b821b55acfa97081ccdfbc6::_2b8a18468b821b55acfa97081ccdfbc6m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _2b8a18468b821b55acfa97081ccdfbc6__2b8a18468b821b55acfa97081ccdfbc6m_m3792406877 (_2b8a18468b821b55acfa97081ccdfbc6_t2914680021 * __this, int32_t ____2b8a18468b821b55acfa97081ccdfbc6a0, int32_t ____2b8a18468b821b55acfa97081ccdfbc6831, int32_t ____2b8a18468b821b55acfa97081ccdfbc6c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
