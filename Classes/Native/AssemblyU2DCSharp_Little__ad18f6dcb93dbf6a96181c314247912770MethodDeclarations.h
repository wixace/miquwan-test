﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._ad18f6dcb93dbf6a96181c31bf01ff44
struct _ad18f6dcb93dbf6a96181c31bf01ff44_t4247912770;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._ad18f6dcb93dbf6a96181c31bf01ff44::.ctor()
extern "C"  void _ad18f6dcb93dbf6a96181c31bf01ff44__ctor_m3140579115 (_ad18f6dcb93dbf6a96181c31bf01ff44_t4247912770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._ad18f6dcb93dbf6a96181c31bf01ff44::_ad18f6dcb93dbf6a96181c31bf01ff44m2(System.Int32)
extern "C"  int32_t _ad18f6dcb93dbf6a96181c31bf01ff44__ad18f6dcb93dbf6a96181c31bf01ff44m2_m727349849 (_ad18f6dcb93dbf6a96181c31bf01ff44_t4247912770 * __this, int32_t ____ad18f6dcb93dbf6a96181c31bf01ff44a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._ad18f6dcb93dbf6a96181c31bf01ff44::_ad18f6dcb93dbf6a96181c31bf01ff44m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _ad18f6dcb93dbf6a96181c31bf01ff44__ad18f6dcb93dbf6a96181c31bf01ff44m_m3893199997 (_ad18f6dcb93dbf6a96181c31bf01ff44_t4247912770 * __this, int32_t ____ad18f6dcb93dbf6a96181c31bf01ff44a0, int32_t ____ad18f6dcb93dbf6a96181c31bf01ff44641, int32_t ____ad18f6dcb93dbf6a96181c31bf01ff44c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
