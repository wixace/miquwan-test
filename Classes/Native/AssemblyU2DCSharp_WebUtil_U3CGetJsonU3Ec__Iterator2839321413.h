﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action
struct Action_t3771233898;
// System.Action`1<System.Object>
struct Action_1_t271665211;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebUtil/<GetJson>c__Iterator2`1<System.Object>
struct  U3CGetJsonU3Ec__Iterator2_1_t2839321413  : public Il2CppObject
{
public:
	// System.String WebUtil/<GetJson>c__Iterator2`1::url
	String_t* ___url_0;
	// System.Action WebUtil/<GetJson>c__Iterator2`1::failure
	Action_t3771233898 * ___failure_1;
	// System.Action`1<T> WebUtil/<GetJson>c__Iterator2`1::success
	Action_1_t271665211 * ___success_2;
	// System.Int32 WebUtil/<GetJson>c__Iterator2`1::$PC
	int32_t ___U24PC_3;
	// System.Object WebUtil/<GetJson>c__Iterator2`1::$current
	Il2CppObject * ___U24current_4;
	// System.String WebUtil/<GetJson>c__Iterator2`1::<$>url
	String_t* ___U3CU24U3Eurl_5;
	// System.Action WebUtil/<GetJson>c__Iterator2`1::<$>failure
	Action_t3771233898 * ___U3CU24U3Efailure_6;
	// System.Action`1<T> WebUtil/<GetJson>c__Iterator2`1::<$>success
	Action_1_t271665211 * ___U3CU24U3Esuccess_7;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CGetJsonU3Ec__Iterator2_1_t2839321413, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_failure_1() { return static_cast<int32_t>(offsetof(U3CGetJsonU3Ec__Iterator2_1_t2839321413, ___failure_1)); }
	inline Action_t3771233898 * get_failure_1() const { return ___failure_1; }
	inline Action_t3771233898 ** get_address_of_failure_1() { return &___failure_1; }
	inline void set_failure_1(Action_t3771233898 * value)
	{
		___failure_1 = value;
		Il2CppCodeGenWriteBarrier(&___failure_1, value);
	}

	inline static int32_t get_offset_of_success_2() { return static_cast<int32_t>(offsetof(U3CGetJsonU3Ec__Iterator2_1_t2839321413, ___success_2)); }
	inline Action_1_t271665211 * get_success_2() const { return ___success_2; }
	inline Action_1_t271665211 ** get_address_of_success_2() { return &___success_2; }
	inline void set_success_2(Action_1_t271665211 * value)
	{
		___success_2 = value;
		Il2CppCodeGenWriteBarrier(&___success_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CGetJsonU3Ec__Iterator2_1_t2839321413, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetJsonU3Ec__Iterator2_1_t2839321413, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eurl_5() { return static_cast<int32_t>(offsetof(U3CGetJsonU3Ec__Iterator2_1_t2839321413, ___U3CU24U3Eurl_5)); }
	inline String_t* get_U3CU24U3Eurl_5() const { return ___U3CU24U3Eurl_5; }
	inline String_t** get_address_of_U3CU24U3Eurl_5() { return &___U3CU24U3Eurl_5; }
	inline void set_U3CU24U3Eurl_5(String_t* value)
	{
		___U3CU24U3Eurl_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eurl_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Efailure_6() { return static_cast<int32_t>(offsetof(U3CGetJsonU3Ec__Iterator2_1_t2839321413, ___U3CU24U3Efailure_6)); }
	inline Action_t3771233898 * get_U3CU24U3Efailure_6() const { return ___U3CU24U3Efailure_6; }
	inline Action_t3771233898 ** get_address_of_U3CU24U3Efailure_6() { return &___U3CU24U3Efailure_6; }
	inline void set_U3CU24U3Efailure_6(Action_t3771233898 * value)
	{
		___U3CU24U3Efailure_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Efailure_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Esuccess_7() { return static_cast<int32_t>(offsetof(U3CGetJsonU3Ec__Iterator2_1_t2839321413, ___U3CU24U3Esuccess_7)); }
	inline Action_1_t271665211 * get_U3CU24U3Esuccess_7() const { return ___U3CU24U3Esuccess_7; }
	inline Action_1_t271665211 ** get_address_of_U3CU24U3Esuccess_7() { return &___U3CU24U3Esuccess_7; }
	inline void set_U3CU24U3Esuccess_7(Action_1_t271665211 * value)
	{
		___U3CU24U3Esuccess_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Esuccess_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
