﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.TreeInstance
struct TreeInstance_t2456306007;
struct TreeInstance_t2456306007_marshaled_pinvoke;
struct TreeInstance_t2456306007_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct TreeInstance_t2456306007;
struct TreeInstance_t2456306007_marshaled_pinvoke;

extern "C" void TreeInstance_t2456306007_marshal_pinvoke(const TreeInstance_t2456306007& unmarshaled, TreeInstance_t2456306007_marshaled_pinvoke& marshaled);
extern "C" void TreeInstance_t2456306007_marshal_pinvoke_back(const TreeInstance_t2456306007_marshaled_pinvoke& marshaled, TreeInstance_t2456306007& unmarshaled);
extern "C" void TreeInstance_t2456306007_marshal_pinvoke_cleanup(TreeInstance_t2456306007_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct TreeInstance_t2456306007;
struct TreeInstance_t2456306007_marshaled_com;

extern "C" void TreeInstance_t2456306007_marshal_com(const TreeInstance_t2456306007& unmarshaled, TreeInstance_t2456306007_marshaled_com& marshaled);
extern "C" void TreeInstance_t2456306007_marshal_com_back(const TreeInstance_t2456306007_marshaled_com& marshaled, TreeInstance_t2456306007& unmarshaled);
extern "C" void TreeInstance_t2456306007_marshal_com_cleanup(TreeInstance_t2456306007_marshaled_com& marshaled);
