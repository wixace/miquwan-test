﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen2112091504MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<Pathfinding.GraphUpdateObject>::.ctor()
#define Queue_1__ctor_m1686653136(__this, method) ((  void (*) (Queue_1_t2667086133 *, const MethodInfo*))Queue_1__ctor_m3042804833_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<Pathfinding.GraphUpdateObject>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m583932725(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t2667086133 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m3260144643_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<Pathfinding.GraphUpdateObject>::System.Collections.ICollection.get_IsSynchronized()
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m1720408169(__this, method) ((  bool (*) (Queue_1_t2667086133 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m63917275_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<Pathfinding.GraphUpdateObject>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m3802647879(__this, method) ((  Il2CppObject * (*) (Queue_1_t2667086133 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m2093948217_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<Pathfinding.GraphUpdateObject>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m311977529(__this, method) ((  Il2CppObject* (*) (Queue_1_t2667086133 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m472615211_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<Pathfinding.GraphUpdateObject>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m572450288(__this, method) ((  Il2CppObject * (*) (Queue_1_t2667086133 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m3688614462_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<Pathfinding.GraphUpdateObject>::Clear()
#define Queue_1_Clear_m1027036442(__this, method) ((  void (*) (Queue_1_t2667086133 *, const MethodInfo*))Queue_1_Clear_m448938124_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<Pathfinding.GraphUpdateObject>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m3135629728(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t2667086133 *, GraphUpdateObjectU5BU5D_t2575105257*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3592753262_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<Pathfinding.GraphUpdateObject>::Dequeue()
#define Queue_1_Dequeue_m1027440908(__this, method) ((  GraphUpdateObject_t430843704 * (*) (Queue_1_t2667086133 *, const MethodInfo*))Queue_1_Dequeue_m102813934_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<Pathfinding.GraphUpdateObject>::Peek()
#define Queue_1_Peek_m1116465585(__this, method) ((  GraphUpdateObject_t430843704 * (*) (Queue_1_t2667086133 *, const MethodInfo*))Queue_1_Peek_m3013356031_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<Pathfinding.GraphUpdateObject>::Enqueue(T)
#define Queue_1_Enqueue_m1272538407(__this, ___item0, method) ((  void (*) (Queue_1_t2667086133 *, GraphUpdateObject_t430843704 *, const MethodInfo*))Queue_1_Enqueue_m4079343671_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<Pathfinding.GraphUpdateObject>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m287762714(__this, ___new_size0, method) ((  void (*) (Queue_1_t2667086133 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m1573690380_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<Pathfinding.GraphUpdateObject>::get_Count()
#define Queue_1_get_Count_m341125730(__this, method) ((  int32_t (*) (Queue_1_t2667086133 *, const MethodInfo*))Queue_1_get_Count_m1429559317_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<Pathfinding.GraphUpdateObject>::GetEnumerator()
#define Queue_1_GetEnumerator_m2762692416(__this, method) ((  Enumerator_t3956171645  (*) (Queue_1_t2667086133 *, const MethodInfo*))Queue_1_GetEnumerator_m3965043378_gshared)(__this, method)
