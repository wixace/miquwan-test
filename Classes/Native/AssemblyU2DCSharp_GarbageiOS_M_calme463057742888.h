﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_calme46
struct  M_calme46_t3057742888  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_calme46::_trersevall
	int32_t ____trersevall_0;
	// System.UInt32 GarbageiOS.M_calme46::_nasnarWeahem
	uint32_t ____nasnarWeahem_1;
	// System.UInt32 GarbageiOS.M_calme46::_getemtrir
	uint32_t ____getemtrir_2;
	// System.Single GarbageiOS.M_calme46::_trusu
	float ____trusu_3;
	// System.Boolean GarbageiOS.M_calme46::_ridousere
	bool ____ridousere_4;
	// System.Single GarbageiOS.M_calme46::_bouchupal
	float ____bouchupal_5;
	// System.Int32 GarbageiOS.M_calme46::_ribearcho
	int32_t ____ribearcho_6;
	// System.Single GarbageiOS.M_calme46::_sascal
	float ____sascal_7;
	// System.Int32 GarbageiOS.M_calme46::_berezisTikoo
	int32_t ____berezisTikoo_8;
	// System.String GarbageiOS.M_calme46::_howelDujow
	String_t* ____howelDujow_9;
	// System.UInt32 GarbageiOS.M_calme46::_fini
	uint32_t ____fini_10;
	// System.Single GarbageiOS.M_calme46::_xanir
	float ____xanir_11;
	// System.UInt32 GarbageiOS.M_calme46::_nesePucor
	uint32_t ____nesePucor_12;
	// System.Int32 GarbageiOS.M_calme46::_staipaiLerejow
	int32_t ____staipaiLerejow_13;
	// System.Int32 GarbageiOS.M_calme46::_sorre
	int32_t ____sorre_14;
	// System.Single GarbageiOS.M_calme46::_loumasor
	float ____loumasor_15;
	// System.String GarbageiOS.M_calme46::_reedor
	String_t* ____reedor_16;
	// System.Int32 GarbageiOS.M_calme46::_sarfowjarSejeadras
	int32_t ____sarfowjarSejeadras_17;

public:
	inline static int32_t get_offset_of__trersevall_0() { return static_cast<int32_t>(offsetof(M_calme46_t3057742888, ____trersevall_0)); }
	inline int32_t get__trersevall_0() const { return ____trersevall_0; }
	inline int32_t* get_address_of__trersevall_0() { return &____trersevall_0; }
	inline void set__trersevall_0(int32_t value)
	{
		____trersevall_0 = value;
	}

	inline static int32_t get_offset_of__nasnarWeahem_1() { return static_cast<int32_t>(offsetof(M_calme46_t3057742888, ____nasnarWeahem_1)); }
	inline uint32_t get__nasnarWeahem_1() const { return ____nasnarWeahem_1; }
	inline uint32_t* get_address_of__nasnarWeahem_1() { return &____nasnarWeahem_1; }
	inline void set__nasnarWeahem_1(uint32_t value)
	{
		____nasnarWeahem_1 = value;
	}

	inline static int32_t get_offset_of__getemtrir_2() { return static_cast<int32_t>(offsetof(M_calme46_t3057742888, ____getemtrir_2)); }
	inline uint32_t get__getemtrir_2() const { return ____getemtrir_2; }
	inline uint32_t* get_address_of__getemtrir_2() { return &____getemtrir_2; }
	inline void set__getemtrir_2(uint32_t value)
	{
		____getemtrir_2 = value;
	}

	inline static int32_t get_offset_of__trusu_3() { return static_cast<int32_t>(offsetof(M_calme46_t3057742888, ____trusu_3)); }
	inline float get__trusu_3() const { return ____trusu_3; }
	inline float* get_address_of__trusu_3() { return &____trusu_3; }
	inline void set__trusu_3(float value)
	{
		____trusu_3 = value;
	}

	inline static int32_t get_offset_of__ridousere_4() { return static_cast<int32_t>(offsetof(M_calme46_t3057742888, ____ridousere_4)); }
	inline bool get__ridousere_4() const { return ____ridousere_4; }
	inline bool* get_address_of__ridousere_4() { return &____ridousere_4; }
	inline void set__ridousere_4(bool value)
	{
		____ridousere_4 = value;
	}

	inline static int32_t get_offset_of__bouchupal_5() { return static_cast<int32_t>(offsetof(M_calme46_t3057742888, ____bouchupal_5)); }
	inline float get__bouchupal_5() const { return ____bouchupal_5; }
	inline float* get_address_of__bouchupal_5() { return &____bouchupal_5; }
	inline void set__bouchupal_5(float value)
	{
		____bouchupal_5 = value;
	}

	inline static int32_t get_offset_of__ribearcho_6() { return static_cast<int32_t>(offsetof(M_calme46_t3057742888, ____ribearcho_6)); }
	inline int32_t get__ribearcho_6() const { return ____ribearcho_6; }
	inline int32_t* get_address_of__ribearcho_6() { return &____ribearcho_6; }
	inline void set__ribearcho_6(int32_t value)
	{
		____ribearcho_6 = value;
	}

	inline static int32_t get_offset_of__sascal_7() { return static_cast<int32_t>(offsetof(M_calme46_t3057742888, ____sascal_7)); }
	inline float get__sascal_7() const { return ____sascal_7; }
	inline float* get_address_of__sascal_7() { return &____sascal_7; }
	inline void set__sascal_7(float value)
	{
		____sascal_7 = value;
	}

	inline static int32_t get_offset_of__berezisTikoo_8() { return static_cast<int32_t>(offsetof(M_calme46_t3057742888, ____berezisTikoo_8)); }
	inline int32_t get__berezisTikoo_8() const { return ____berezisTikoo_8; }
	inline int32_t* get_address_of__berezisTikoo_8() { return &____berezisTikoo_8; }
	inline void set__berezisTikoo_8(int32_t value)
	{
		____berezisTikoo_8 = value;
	}

	inline static int32_t get_offset_of__howelDujow_9() { return static_cast<int32_t>(offsetof(M_calme46_t3057742888, ____howelDujow_9)); }
	inline String_t* get__howelDujow_9() const { return ____howelDujow_9; }
	inline String_t** get_address_of__howelDujow_9() { return &____howelDujow_9; }
	inline void set__howelDujow_9(String_t* value)
	{
		____howelDujow_9 = value;
		Il2CppCodeGenWriteBarrier(&____howelDujow_9, value);
	}

	inline static int32_t get_offset_of__fini_10() { return static_cast<int32_t>(offsetof(M_calme46_t3057742888, ____fini_10)); }
	inline uint32_t get__fini_10() const { return ____fini_10; }
	inline uint32_t* get_address_of__fini_10() { return &____fini_10; }
	inline void set__fini_10(uint32_t value)
	{
		____fini_10 = value;
	}

	inline static int32_t get_offset_of__xanir_11() { return static_cast<int32_t>(offsetof(M_calme46_t3057742888, ____xanir_11)); }
	inline float get__xanir_11() const { return ____xanir_11; }
	inline float* get_address_of__xanir_11() { return &____xanir_11; }
	inline void set__xanir_11(float value)
	{
		____xanir_11 = value;
	}

	inline static int32_t get_offset_of__nesePucor_12() { return static_cast<int32_t>(offsetof(M_calme46_t3057742888, ____nesePucor_12)); }
	inline uint32_t get__nesePucor_12() const { return ____nesePucor_12; }
	inline uint32_t* get_address_of__nesePucor_12() { return &____nesePucor_12; }
	inline void set__nesePucor_12(uint32_t value)
	{
		____nesePucor_12 = value;
	}

	inline static int32_t get_offset_of__staipaiLerejow_13() { return static_cast<int32_t>(offsetof(M_calme46_t3057742888, ____staipaiLerejow_13)); }
	inline int32_t get__staipaiLerejow_13() const { return ____staipaiLerejow_13; }
	inline int32_t* get_address_of__staipaiLerejow_13() { return &____staipaiLerejow_13; }
	inline void set__staipaiLerejow_13(int32_t value)
	{
		____staipaiLerejow_13 = value;
	}

	inline static int32_t get_offset_of__sorre_14() { return static_cast<int32_t>(offsetof(M_calme46_t3057742888, ____sorre_14)); }
	inline int32_t get__sorre_14() const { return ____sorre_14; }
	inline int32_t* get_address_of__sorre_14() { return &____sorre_14; }
	inline void set__sorre_14(int32_t value)
	{
		____sorre_14 = value;
	}

	inline static int32_t get_offset_of__loumasor_15() { return static_cast<int32_t>(offsetof(M_calme46_t3057742888, ____loumasor_15)); }
	inline float get__loumasor_15() const { return ____loumasor_15; }
	inline float* get_address_of__loumasor_15() { return &____loumasor_15; }
	inline void set__loumasor_15(float value)
	{
		____loumasor_15 = value;
	}

	inline static int32_t get_offset_of__reedor_16() { return static_cast<int32_t>(offsetof(M_calme46_t3057742888, ____reedor_16)); }
	inline String_t* get__reedor_16() const { return ____reedor_16; }
	inline String_t** get_address_of__reedor_16() { return &____reedor_16; }
	inline void set__reedor_16(String_t* value)
	{
		____reedor_16 = value;
		Il2CppCodeGenWriteBarrier(&____reedor_16, value);
	}

	inline static int32_t get_offset_of__sarfowjarSejeadras_17() { return static_cast<int32_t>(offsetof(M_calme46_t3057742888, ____sarfowjarSejeadras_17)); }
	inline int32_t get__sarfowjarSejeadras_17() const { return ____sarfowjarSejeadras_17; }
	inline int32_t* get_address_of__sarfowjarSejeadras_17() { return &____sarfowjarSejeadras_17; }
	inline void set__sarfowjarSejeadras_17(int32_t value)
	{
		____sarfowjarSejeadras_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
