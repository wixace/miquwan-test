﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iTween/<TweenRestart>c__Iterator2C
struct U3CTweenRestartU3Ec__Iterator2C_t3643232791;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void iTween/<TweenRestart>c__Iterator2C::.ctor()
extern "C"  void U3CTweenRestartU3Ec__Iterator2C__ctor_m766568548 (U3CTweenRestartU3Ec__Iterator2C_t3643232791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<TweenRestart>c__Iterator2C::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTweenRestartU3Ec__Iterator2C_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1081933496 (U3CTweenRestartU3Ec__Iterator2C_t3643232791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<TweenRestart>c__Iterator2C::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTweenRestartU3Ec__Iterator2C_System_Collections_IEnumerator_get_Current_m3358814796 (U3CTweenRestartU3Ec__Iterator2C_t3643232791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTween/<TweenRestart>c__Iterator2C::MoveNext()
extern "C"  bool U3CTweenRestartU3Ec__Iterator2C_MoveNext_m306746872 (U3CTweenRestartU3Ec__Iterator2C_t3643232791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<TweenRestart>c__Iterator2C::Dispose()
extern "C"  void U3CTweenRestartU3Ec__Iterator2C_Dispose_m2131250081 (U3CTweenRestartU3Ec__Iterator2C_t3643232791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<TweenRestart>c__Iterator2C::Reset()
extern "C"  void U3CTweenRestartU3Ec__Iterator2C_Reset_m2707968785 (U3CTweenRestartU3Ec__Iterator2C_t3643232791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
