﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssetDownMgr/SetBytes
struct SetBytes_t2856136722;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void AssetDownMgr/SetBytes::.ctor(System.Object,System.IntPtr)
extern "C"  void SetBytes__ctor_m938911465 (SetBytes_t2856136722 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetDownMgr/SetBytes::Invoke(System.String,System.Byte[])
extern "C"  void SetBytes_Invoke_m2719899170 (SetBytes_t2856136722 * __this, String_t* ___key0, ByteU5BU5D_t4260760469* ___ab1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult AssetDownMgr/SetBytes::BeginInvoke(System.String,System.Byte[],System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SetBytes_BeginInvoke_m1692037385 (SetBytes_t2856136722 * __this, String_t* ___key0, ByteU5BU5D_t4260760469* ___ab1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssetDownMgr/SetBytes::EndInvoke(System.IAsyncResult)
extern "C"  void SetBytes_EndInvoke_m947647865 (SetBytes_t2856136722 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
