﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._5cf29bf747e229506fb03a94dfd980d4
struct _5cf29bf747e229506fb03a94dfd980d4_t644498021;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__5cf29bf747e229506fb03a94d644498021.h"

// System.Void Little._5cf29bf747e229506fb03a94dfd980d4::.ctor()
extern "C"  void _5cf29bf747e229506fb03a94dfd980d4__ctor_m2092746088 (_5cf29bf747e229506fb03a94dfd980d4_t644498021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._5cf29bf747e229506fb03a94dfd980d4::_5cf29bf747e229506fb03a94dfd980d4m2(System.Int32)
extern "C"  int32_t _5cf29bf747e229506fb03a94dfd980d4__5cf29bf747e229506fb03a94dfd980d4m2_m1287737209 (_5cf29bf747e229506fb03a94dfd980d4_t644498021 * __this, int32_t ____5cf29bf747e229506fb03a94dfd980d4a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._5cf29bf747e229506fb03a94dfd980d4::_5cf29bf747e229506fb03a94dfd980d4m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _5cf29bf747e229506fb03a94dfd980d4__5cf29bf747e229506fb03a94dfd980d4m_m314225501 (_5cf29bf747e229506fb03a94dfd980d4_t644498021 * __this, int32_t ____5cf29bf747e229506fb03a94dfd980d4a0, int32_t ____5cf29bf747e229506fb03a94dfd980d4491, int32_t ____5cf29bf747e229506fb03a94dfd980d4c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._5cf29bf747e229506fb03a94dfd980d4::ilo__5cf29bf747e229506fb03a94dfd980d4m21(Little._5cf29bf747e229506fb03a94dfd980d4,System.Int32)
extern "C"  int32_t _5cf29bf747e229506fb03a94dfd980d4_ilo__5cf29bf747e229506fb03a94dfd980d4m21_m114615244 (Il2CppObject * __this /* static, unused */, _5cf29bf747e229506fb03a94dfd980d4_t644498021 * ____this0, int32_t ____5cf29bf747e229506fb03a94dfd980d4a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
