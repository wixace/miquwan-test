﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Poly2Tri.DTSweepPointComparator
struct DTSweepPointComparator_t3174805566;
// Pathfinding.Poly2Tri.TriangulationPoint
struct TriangulationPoint_t3810082933;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3810082933.h"

// System.Void Pathfinding.Poly2Tri.DTSweepPointComparator::.ctor()
extern "C"  void DTSweepPointComparator__ctor_m444435905 (DTSweepPointComparator_t3174805566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.Poly2Tri.DTSweepPointComparator::Compare(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  int32_t DTSweepPointComparator_Compare_m3867896618 (DTSweepPointComparator_t3174805566 * __this, TriangulationPoint_t3810082933 * ___p10, TriangulationPoint_t3810082933 * ___p21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
