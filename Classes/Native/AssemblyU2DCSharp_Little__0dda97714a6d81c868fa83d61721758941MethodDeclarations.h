﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._0dda97714a6d81c868fa83d60ff9ecab
struct _0dda97714a6d81c868fa83d60ff9ecab_t1721758941;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._0dda97714a6d81c868fa83d60ff9ecab::.ctor()
extern "C"  void _0dda97714a6d81c868fa83d60ff9ecab__ctor_m993810928 (_0dda97714a6d81c868fa83d60ff9ecab_t1721758941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._0dda97714a6d81c868fa83d60ff9ecab::_0dda97714a6d81c868fa83d60ff9ecabm2(System.Int32)
extern "C"  int32_t _0dda97714a6d81c868fa83d60ff9ecab__0dda97714a6d81c868fa83d60ff9ecabm2_m1031568505 (_0dda97714a6d81c868fa83d60ff9ecab_t1721758941 * __this, int32_t ____0dda97714a6d81c868fa83d60ff9ecaba0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._0dda97714a6d81c868fa83d60ff9ecab::_0dda97714a6d81c868fa83d60ff9ecabm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _0dda97714a6d81c868fa83d60ff9ecab__0dda97714a6d81c868fa83d60ff9ecabm_m234628701 (_0dda97714a6d81c868fa83d60ff9ecab_t1721758941 * __this, int32_t ____0dda97714a6d81c868fa83d60ff9ecaba0, int32_t ____0dda97714a6d81c868fa83d60ff9ecab611, int32_t ____0dda97714a6d81c868fa83d60ff9ecabc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
