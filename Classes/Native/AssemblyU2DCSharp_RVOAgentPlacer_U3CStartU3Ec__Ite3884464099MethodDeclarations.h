﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RVOAgentPlacer/<Start>c__IteratorF
struct U3CStartU3Ec__IteratorF_t3884464099;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void RVOAgentPlacer/<Start>c__IteratorF::.ctor()
extern "C"  void U3CStartU3Ec__IteratorF__ctor_m2819082264 (U3CStartU3Ec__IteratorF_t3884464099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object RVOAgentPlacer/<Start>c__IteratorF::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3733489540 (U3CStartU3Ec__IteratorF_t3884464099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object RVOAgentPlacer/<Start>c__IteratorF::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m2418152216 (U3CStartU3Ec__IteratorF_t3884464099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RVOAgentPlacer/<Start>c__IteratorF::MoveNext()
extern "C"  bool U3CStartU3Ec__IteratorF_MoveNext_m3588434372 (U3CStartU3Ec__IteratorF_t3884464099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RVOAgentPlacer/<Start>c__IteratorF::Dispose()
extern "C"  void U3CStartU3Ec__IteratorF_Dispose_m3206942293 (U3CStartU3Ec__IteratorF_t3884464099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RVOAgentPlacer/<Start>c__IteratorF::Reset()
extern "C"  void U3CStartU3Ec__IteratorF_Reset_m465515205 (U3CStartU3Ec__IteratorF_t3884464099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
