﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.XmlNodeConverter/<DeserializeNode>c__AnonStorey12C
struct U3CDeserializeNodeU3Ec__AnonStorey12C_t4228089970;
// Newtonsoft.Json.Converters.IXmlElement
struct IXmlElement_t2583566720;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Converters.XmlNodeConverter/<DeserializeNode>c__AnonStorey12C::.ctor()
extern "C"  void U3CDeserializeNodeU3Ec__AnonStorey12C__ctor_m2510879033 (U3CDeserializeNodeU3Ec__AnonStorey12C_t4228089970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter/<DeserializeNode>c__AnonStorey12C::<>m__36D(Newtonsoft.Json.Converters.IXmlElement)
extern "C"  bool U3CDeserializeNodeU3Ec__AnonStorey12C_U3CU3Em__36D_m2443560677 (U3CDeserializeNodeU3Ec__AnonStorey12C_t4228089970 * __this, Il2CppObject * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
