﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginSjoy
struct  PluginSjoy_t1606406260  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginSjoy::tokenId
	String_t* ___tokenId_2;
	// System.Boolean PluginSjoy::isOpenUserSwitch
	bool ___isOpenUserSwitch_3;
	// System.Single PluginSjoy::lastSdkLoginOkTime
	float ___lastSdkLoginOkTime_4;

public:
	inline static int32_t get_offset_of_tokenId_2() { return static_cast<int32_t>(offsetof(PluginSjoy_t1606406260, ___tokenId_2)); }
	inline String_t* get_tokenId_2() const { return ___tokenId_2; }
	inline String_t** get_address_of_tokenId_2() { return &___tokenId_2; }
	inline void set_tokenId_2(String_t* value)
	{
		___tokenId_2 = value;
		Il2CppCodeGenWriteBarrier(&___tokenId_2, value);
	}

	inline static int32_t get_offset_of_isOpenUserSwitch_3() { return static_cast<int32_t>(offsetof(PluginSjoy_t1606406260, ___isOpenUserSwitch_3)); }
	inline bool get_isOpenUserSwitch_3() const { return ___isOpenUserSwitch_3; }
	inline bool* get_address_of_isOpenUserSwitch_3() { return &___isOpenUserSwitch_3; }
	inline void set_isOpenUserSwitch_3(bool value)
	{
		___isOpenUserSwitch_3 = value;
	}

	inline static int32_t get_offset_of_lastSdkLoginOkTime_4() { return static_cast<int32_t>(offsetof(PluginSjoy_t1606406260, ___lastSdkLoginOkTime_4)); }
	inline float get_lastSdkLoginOkTime_4() const { return ___lastSdkLoginOkTime_4; }
	inline float* get_address_of_lastSdkLoginOkTime_4() { return &___lastSdkLoginOkTime_4; }
	inline void set_lastSdkLoginOkTime_4(float value)
	{
		___lastSdkLoginOkTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
