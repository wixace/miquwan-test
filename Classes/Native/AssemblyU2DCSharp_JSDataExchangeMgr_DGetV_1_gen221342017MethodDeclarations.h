﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr_DGetV_1_gen4048467712MethodDeclarations.h"

// System.Void JSDataExchangeMgr/DGetV`1<UnityEngine.ParticleSystem/Particle[]>::.ctor(System.Object,System.IntPtr)
#define DGetV_1__ctor_m2493391246(__this, ___object0, ___method1, method) ((  void (*) (DGetV_1_t221342017 *, Il2CppObject *, IntPtr_t, const MethodInfo*))DGetV_1__ctor_m3356914363_gshared)(__this, ___object0, ___method1, method)
// T JSDataExchangeMgr/DGetV`1<UnityEngine.ParticleSystem/Particle[]>::Invoke()
#define DGetV_1_Invoke_m2318746663(__this, method) ((  ParticleU5BU5D_t343690676* (*) (DGetV_1_t221342017 *, const MethodInfo*))DGetV_1_Invoke_m1509146772_gshared)(__this, method)
// System.IAsyncResult JSDataExchangeMgr/DGetV`1<UnityEngine.ParticleSystem/Particle[]>::BeginInvoke(System.AsyncCallback,System.Object)
#define DGetV_1_BeginInvoke_m2343021723(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (DGetV_1_t221342017 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))DGetV_1_BeginInvoke_m2681184398_gshared)(__this, ___callback0, ___object1, method)
// T JSDataExchangeMgr/DGetV`1<UnityEngine.ParticleSystem/Particle[]>::EndInvoke(System.IAsyncResult)
#define DGetV_1_EndInvoke_m2135720477(__this, ___result0, method) ((  ParticleU5BU5D_t343690676* (*) (DGetV_1_t221342017 *, Il2CppObject *, const MethodInfo*))DGetV_1_EndInvoke_m1776565962_gshared)(__this, ___result0, method)
