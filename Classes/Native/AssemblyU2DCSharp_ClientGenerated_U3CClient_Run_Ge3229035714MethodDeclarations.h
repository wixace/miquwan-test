﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ClientGenerated/<Client_Run_GetDelegate_member7_arg2>c__AnonStorey55
struct U3CClient_Run_GetDelegate_member7_arg2U3Ec__AnonStorey55_t3229035714;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ClientGenerated/<Client_Run_GetDelegate_member7_arg2>c__AnonStorey55::.ctor()
extern "C"  void U3CClient_Run_GetDelegate_member7_arg2U3Ec__AnonStorey55__ctor_m853858329 (U3CClient_Run_GetDelegate_member7_arg2U3Ec__AnonStorey55_t3229035714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClientGenerated/<Client_Run_GetDelegate_member7_arg2>c__AnonStorey55::<>m__2D(System.UInt32,System.UInt32,System.String)
extern "C"  void U3CClient_Run_GetDelegate_member7_arg2U3Ec__AnonStorey55_U3CU3Em__2D_m3257681270 (U3CClient_Run_GetDelegate_member7_arg2U3Ec__AnonStorey55_t3229035714 * __this, uint32_t ___id0, uint32_t ___code1, String_t* ___msgString2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
