﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._3dc9a0dcc88854a911fe711684a68680
struct _3dc9a0dcc88854a911fe711684a68680_t1597363231;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__3dc9a0dcc88854a911fe71161597363231.h"

// System.Void Little._3dc9a0dcc88854a911fe711684a68680::.ctor()
extern "C"  void _3dc9a0dcc88854a911fe711684a68680__ctor_m247497966 (_3dc9a0dcc88854a911fe711684a68680_t1597363231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._3dc9a0dcc88854a911fe711684a68680::_3dc9a0dcc88854a911fe711684a68680m2(System.Int32)
extern "C"  int32_t _3dc9a0dcc88854a911fe711684a68680__3dc9a0dcc88854a911fe711684a68680m2_m2447058233 (_3dc9a0dcc88854a911fe711684a68680_t1597363231 * __this, int32_t ____3dc9a0dcc88854a911fe711684a68680a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._3dc9a0dcc88854a911fe711684a68680::_3dc9a0dcc88854a911fe711684a68680m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _3dc9a0dcc88854a911fe711684a68680__3dc9a0dcc88854a911fe711684a68680m_m3236484509 (_3dc9a0dcc88854a911fe711684a68680_t1597363231 * __this, int32_t ____3dc9a0dcc88854a911fe711684a68680a0, int32_t ____3dc9a0dcc88854a911fe711684a68680481, int32_t ____3dc9a0dcc88854a911fe711684a68680c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._3dc9a0dcc88854a911fe711684a68680::ilo__3dc9a0dcc88854a911fe711684a68680m21(Little._3dc9a0dcc88854a911fe711684a68680,System.Int32)
extern "C"  int32_t _3dc9a0dcc88854a911fe711684a68680_ilo__3dc9a0dcc88854a911fe711684a68680m21_m1780800198 (Il2CppObject * __this /* static, unused */, _3dc9a0dcc88854a911fe711684a68680_t1597363231 * ____this0, int32_t ____3dc9a0dcc88854a911fe711684a68680a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
