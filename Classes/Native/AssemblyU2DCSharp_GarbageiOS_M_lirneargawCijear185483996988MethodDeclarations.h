﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_lirneargawCijear185
struct M_lirneargawCijear185_t483996988;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_lirneargawCijear185::.ctor()
extern "C"  void M_lirneargawCijear185__ctor_m2377634215 (M_lirneargawCijear185_t483996988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lirneargawCijear185::M_mulorTemoosow0(System.String[],System.Int32)
extern "C"  void M_lirneargawCijear185_M_mulorTemoosow0_m1626196368 (M_lirneargawCijear185_t483996988 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lirneargawCijear185::M_pinaibur1(System.String[],System.Int32)
extern "C"  void M_lirneargawCijear185_M_pinaibur1_m3815080697 (M_lirneargawCijear185_t483996988 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lirneargawCijear185::M_kooyuCistor2(System.String[],System.Int32)
extern "C"  void M_lirneargawCijear185_M_kooyuCistor2_m371168999 (M_lirneargawCijear185_t483996988 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lirneargawCijear185::M_nemberelay3(System.String[],System.Int32)
extern "C"  void M_lirneargawCijear185_M_nemberelay3_m2044820293 (M_lirneargawCijear185_t483996988 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lirneargawCijear185::M_rootellay4(System.String[],System.Int32)
extern "C"  void M_lirneargawCijear185_M_rootellay4_m3743160799 (M_lirneargawCijear185_t483996988 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lirneargawCijear185::M_puneWhomis5(System.String[],System.Int32)
extern "C"  void M_lirneargawCijear185_M_puneWhomis5_m2102991498 (M_lirneargawCijear185_t483996988 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
