﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_PathPool_1_gen4134265632MethodDeclarations.h"

// System.Void Pathfinding.PathPool`1<Pathfinding.FleePath>::.cctor()
#define PathPool_1__cctor_m2766315035(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PathPool_1__cctor_m4111071586_gshared)(__this /* static, unused */, method)
// System.Void Pathfinding.PathPool`1<Pathfinding.FleePath>::Recycle(T)
#define PathPool_1_Recycle_m1834778299(__this /* static, unused */, ___path0, method) ((  void (*) (Il2CppObject * /* static, unused */, FleePath_t909342113 *, const MethodInfo*))PathPool_1_Recycle_m1360667714_gshared)(__this /* static, unused */, ___path0, method)
// System.Void Pathfinding.PathPool`1<Pathfinding.FleePath>::Warmup(System.Int32,System.Int32)
#define PathPool_1_Warmup_m3747359892(__this /* static, unused */, ___count0, ___length1, method) ((  void (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))PathPool_1_Warmup_m4243013421_gshared)(__this /* static, unused */, ___count0, ___length1, method)
// System.Int32 Pathfinding.PathPool`1<Pathfinding.FleePath>::GetTotalCreated()
#define PathPool_1_GetTotalCreated_m1754555740(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PathPool_1_GetTotalCreated_m3938489809_gshared)(__this /* static, unused */, method)
// System.Int32 Pathfinding.PathPool`1<Pathfinding.FleePath>::GetSize()
#define PathPool_1_GetSize_m1831041561(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PathPool_1_GetSize_m2941120398_gshared)(__this /* static, unused */, method)
// T Pathfinding.PathPool`1<Pathfinding.FleePath>::GetPath()
#define PathPool_1_GetPath_m2306338988(__this /* static, unused */, method) ((  FleePath_t909342113 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PathPool_1_GetPath_m3122724387_gshared)(__this /* static, unused */, method)
// System.Void Pathfinding.PathPool`1<Pathfinding.FleePath>::ilo_Claim1(Pathfinding.Path,System.Object)
#define PathPool_1_ilo_Claim1_m578460409(__this /* static, unused */, ____this0, ___o1, method) ((  void (*) (Il2CppObject * /* static, unused */, Path_t1974241691 *, Il2CppObject *, const MethodInfo*))PathPool_1_ilo_Claim1_m992502912_gshared)(__this /* static, unused */, ____this0, ___o1, method)
// System.Void Pathfinding.PathPool`1<Pathfinding.FleePath>::ilo_Release2(Pathfinding.Path,System.Object)
#define PathPool_1_ilo_Release2_m4110377871(__this /* static, unused */, ____this0, ___o1, method) ((  void (*) (Il2CppObject * /* static, unused */, Path_t1974241691 *, Il2CppObject *, const MethodInfo*))PathPool_1_ilo_Release2_m2573264726_gshared)(__this /* static, unused */, ____this0, ___o1, method)
