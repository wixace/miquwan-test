﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Val11726959MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1782985929(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t906737013 *, Dictionary_2_t2206131300 *, const MethodInfo*))ValueCollection__ctor_m3411917597_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m204065353(__this, ___item0, method) ((  void (*) (ValueCollection_t906737013 *, IBehavior_t770859129 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1503537525_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2611626898(__this, method) ((  void (*) (ValueCollection_t906737013 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4196735166_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2407028221(__this, ___item0, method) ((  bool (*) (ValueCollection_t906737013 *, IBehavior_t770859129 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3169795921_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3279473122(__this, ___item0, method) ((  bool (*) (ValueCollection_t906737013 *, IBehavior_t770859129 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2390882358_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3501549536(__this, method) ((  Il2CppObject* (*) (ValueCollection_t906737013 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1763429260_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2694256150(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t906737013 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2392482882_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4198851089(__this, method) ((  Il2CppObject * (*) (ValueCollection_t906737013 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2470319165_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m682204080(__this, method) ((  bool (*) (ValueCollection_t906737013 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1444971780_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m729278864(__this, method) ((  bool (*) (ValueCollection_t906737013 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2812489956_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2961017532(__this, method) ((  Il2CppObject * (*) (ValueCollection_t906737013 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2820594832_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m8343248(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t906737013 *, IBehaviorU5BU5D_t563026308*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1636360228_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3795745139(__this, method) ((  Enumerator_t137964708  (*) (ValueCollection_t906737013 *, const MethodInfo*))ValueCollection_GetEnumerator_m3349137735_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::get_Count()
#define ValueCollection_get_Count_m1409542742(__this, method) ((  int32_t (*) (ValueCollection_t906737013 *, const MethodInfo*))ValueCollection_get_Count_m2699976362_gshared)(__this, method)
