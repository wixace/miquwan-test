﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Pathfinding_AdvancedSmooth_TurnC3543216936.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.AdvancedSmooth/MaxTurn
struct  MaxTurn_t585750252  : public TurnConstructor_t3543216936
{
public:
	// UnityEngine.Vector3 Pathfinding.AdvancedSmooth/MaxTurn::preRightCircleCenter
	Vector3_t4282066566  ___preRightCircleCenter_12;
	// UnityEngine.Vector3 Pathfinding.AdvancedSmooth/MaxTurn::preLeftCircleCenter
	Vector3_t4282066566  ___preLeftCircleCenter_13;
	// UnityEngine.Vector3 Pathfinding.AdvancedSmooth/MaxTurn::rightCircleCenter
	Vector3_t4282066566  ___rightCircleCenter_14;
	// UnityEngine.Vector3 Pathfinding.AdvancedSmooth/MaxTurn::leftCircleCenter
	Vector3_t4282066566  ___leftCircleCenter_15;
	// System.Double Pathfinding.AdvancedSmooth/MaxTurn::vaRight
	double ___vaRight_16;
	// System.Double Pathfinding.AdvancedSmooth/MaxTurn::vaLeft
	double ___vaLeft_17;
	// System.Double Pathfinding.AdvancedSmooth/MaxTurn::preVaLeft
	double ___preVaLeft_18;
	// System.Double Pathfinding.AdvancedSmooth/MaxTurn::preVaRight
	double ___preVaRight_19;
	// System.Double Pathfinding.AdvancedSmooth/MaxTurn::gammaLeft
	double ___gammaLeft_20;
	// System.Double Pathfinding.AdvancedSmooth/MaxTurn::gammaRight
	double ___gammaRight_21;
	// System.Double Pathfinding.AdvancedSmooth/MaxTurn::betaRightRight
	double ___betaRightRight_22;
	// System.Double Pathfinding.AdvancedSmooth/MaxTurn::betaRightLeft
	double ___betaRightLeft_23;
	// System.Double Pathfinding.AdvancedSmooth/MaxTurn::betaLeftRight
	double ___betaLeftRight_24;
	// System.Double Pathfinding.AdvancedSmooth/MaxTurn::betaLeftLeft
	double ___betaLeftLeft_25;
	// System.Double Pathfinding.AdvancedSmooth/MaxTurn::deltaRightLeft
	double ___deltaRightLeft_26;
	// System.Double Pathfinding.AdvancedSmooth/MaxTurn::deltaLeftRight
	double ___deltaLeftRight_27;
	// System.Double Pathfinding.AdvancedSmooth/MaxTurn::alfaRightRight
	double ___alfaRightRight_28;
	// System.Double Pathfinding.AdvancedSmooth/MaxTurn::alfaLeftLeft
	double ___alfaLeftLeft_29;
	// System.Double Pathfinding.AdvancedSmooth/MaxTurn::alfaRightLeft
	double ___alfaRightLeft_30;
	// System.Double Pathfinding.AdvancedSmooth/MaxTurn::alfaLeftRight
	double ___alfaLeftRight_31;

public:
	inline static int32_t get_offset_of_preRightCircleCenter_12() { return static_cast<int32_t>(offsetof(MaxTurn_t585750252, ___preRightCircleCenter_12)); }
	inline Vector3_t4282066566  get_preRightCircleCenter_12() const { return ___preRightCircleCenter_12; }
	inline Vector3_t4282066566 * get_address_of_preRightCircleCenter_12() { return &___preRightCircleCenter_12; }
	inline void set_preRightCircleCenter_12(Vector3_t4282066566  value)
	{
		___preRightCircleCenter_12 = value;
	}

	inline static int32_t get_offset_of_preLeftCircleCenter_13() { return static_cast<int32_t>(offsetof(MaxTurn_t585750252, ___preLeftCircleCenter_13)); }
	inline Vector3_t4282066566  get_preLeftCircleCenter_13() const { return ___preLeftCircleCenter_13; }
	inline Vector3_t4282066566 * get_address_of_preLeftCircleCenter_13() { return &___preLeftCircleCenter_13; }
	inline void set_preLeftCircleCenter_13(Vector3_t4282066566  value)
	{
		___preLeftCircleCenter_13 = value;
	}

	inline static int32_t get_offset_of_rightCircleCenter_14() { return static_cast<int32_t>(offsetof(MaxTurn_t585750252, ___rightCircleCenter_14)); }
	inline Vector3_t4282066566  get_rightCircleCenter_14() const { return ___rightCircleCenter_14; }
	inline Vector3_t4282066566 * get_address_of_rightCircleCenter_14() { return &___rightCircleCenter_14; }
	inline void set_rightCircleCenter_14(Vector3_t4282066566  value)
	{
		___rightCircleCenter_14 = value;
	}

	inline static int32_t get_offset_of_leftCircleCenter_15() { return static_cast<int32_t>(offsetof(MaxTurn_t585750252, ___leftCircleCenter_15)); }
	inline Vector3_t4282066566  get_leftCircleCenter_15() const { return ___leftCircleCenter_15; }
	inline Vector3_t4282066566 * get_address_of_leftCircleCenter_15() { return &___leftCircleCenter_15; }
	inline void set_leftCircleCenter_15(Vector3_t4282066566  value)
	{
		___leftCircleCenter_15 = value;
	}

	inline static int32_t get_offset_of_vaRight_16() { return static_cast<int32_t>(offsetof(MaxTurn_t585750252, ___vaRight_16)); }
	inline double get_vaRight_16() const { return ___vaRight_16; }
	inline double* get_address_of_vaRight_16() { return &___vaRight_16; }
	inline void set_vaRight_16(double value)
	{
		___vaRight_16 = value;
	}

	inline static int32_t get_offset_of_vaLeft_17() { return static_cast<int32_t>(offsetof(MaxTurn_t585750252, ___vaLeft_17)); }
	inline double get_vaLeft_17() const { return ___vaLeft_17; }
	inline double* get_address_of_vaLeft_17() { return &___vaLeft_17; }
	inline void set_vaLeft_17(double value)
	{
		___vaLeft_17 = value;
	}

	inline static int32_t get_offset_of_preVaLeft_18() { return static_cast<int32_t>(offsetof(MaxTurn_t585750252, ___preVaLeft_18)); }
	inline double get_preVaLeft_18() const { return ___preVaLeft_18; }
	inline double* get_address_of_preVaLeft_18() { return &___preVaLeft_18; }
	inline void set_preVaLeft_18(double value)
	{
		___preVaLeft_18 = value;
	}

	inline static int32_t get_offset_of_preVaRight_19() { return static_cast<int32_t>(offsetof(MaxTurn_t585750252, ___preVaRight_19)); }
	inline double get_preVaRight_19() const { return ___preVaRight_19; }
	inline double* get_address_of_preVaRight_19() { return &___preVaRight_19; }
	inline void set_preVaRight_19(double value)
	{
		___preVaRight_19 = value;
	}

	inline static int32_t get_offset_of_gammaLeft_20() { return static_cast<int32_t>(offsetof(MaxTurn_t585750252, ___gammaLeft_20)); }
	inline double get_gammaLeft_20() const { return ___gammaLeft_20; }
	inline double* get_address_of_gammaLeft_20() { return &___gammaLeft_20; }
	inline void set_gammaLeft_20(double value)
	{
		___gammaLeft_20 = value;
	}

	inline static int32_t get_offset_of_gammaRight_21() { return static_cast<int32_t>(offsetof(MaxTurn_t585750252, ___gammaRight_21)); }
	inline double get_gammaRight_21() const { return ___gammaRight_21; }
	inline double* get_address_of_gammaRight_21() { return &___gammaRight_21; }
	inline void set_gammaRight_21(double value)
	{
		___gammaRight_21 = value;
	}

	inline static int32_t get_offset_of_betaRightRight_22() { return static_cast<int32_t>(offsetof(MaxTurn_t585750252, ___betaRightRight_22)); }
	inline double get_betaRightRight_22() const { return ___betaRightRight_22; }
	inline double* get_address_of_betaRightRight_22() { return &___betaRightRight_22; }
	inline void set_betaRightRight_22(double value)
	{
		___betaRightRight_22 = value;
	}

	inline static int32_t get_offset_of_betaRightLeft_23() { return static_cast<int32_t>(offsetof(MaxTurn_t585750252, ___betaRightLeft_23)); }
	inline double get_betaRightLeft_23() const { return ___betaRightLeft_23; }
	inline double* get_address_of_betaRightLeft_23() { return &___betaRightLeft_23; }
	inline void set_betaRightLeft_23(double value)
	{
		___betaRightLeft_23 = value;
	}

	inline static int32_t get_offset_of_betaLeftRight_24() { return static_cast<int32_t>(offsetof(MaxTurn_t585750252, ___betaLeftRight_24)); }
	inline double get_betaLeftRight_24() const { return ___betaLeftRight_24; }
	inline double* get_address_of_betaLeftRight_24() { return &___betaLeftRight_24; }
	inline void set_betaLeftRight_24(double value)
	{
		___betaLeftRight_24 = value;
	}

	inline static int32_t get_offset_of_betaLeftLeft_25() { return static_cast<int32_t>(offsetof(MaxTurn_t585750252, ___betaLeftLeft_25)); }
	inline double get_betaLeftLeft_25() const { return ___betaLeftLeft_25; }
	inline double* get_address_of_betaLeftLeft_25() { return &___betaLeftLeft_25; }
	inline void set_betaLeftLeft_25(double value)
	{
		___betaLeftLeft_25 = value;
	}

	inline static int32_t get_offset_of_deltaRightLeft_26() { return static_cast<int32_t>(offsetof(MaxTurn_t585750252, ___deltaRightLeft_26)); }
	inline double get_deltaRightLeft_26() const { return ___deltaRightLeft_26; }
	inline double* get_address_of_deltaRightLeft_26() { return &___deltaRightLeft_26; }
	inline void set_deltaRightLeft_26(double value)
	{
		___deltaRightLeft_26 = value;
	}

	inline static int32_t get_offset_of_deltaLeftRight_27() { return static_cast<int32_t>(offsetof(MaxTurn_t585750252, ___deltaLeftRight_27)); }
	inline double get_deltaLeftRight_27() const { return ___deltaLeftRight_27; }
	inline double* get_address_of_deltaLeftRight_27() { return &___deltaLeftRight_27; }
	inline void set_deltaLeftRight_27(double value)
	{
		___deltaLeftRight_27 = value;
	}

	inline static int32_t get_offset_of_alfaRightRight_28() { return static_cast<int32_t>(offsetof(MaxTurn_t585750252, ___alfaRightRight_28)); }
	inline double get_alfaRightRight_28() const { return ___alfaRightRight_28; }
	inline double* get_address_of_alfaRightRight_28() { return &___alfaRightRight_28; }
	inline void set_alfaRightRight_28(double value)
	{
		___alfaRightRight_28 = value;
	}

	inline static int32_t get_offset_of_alfaLeftLeft_29() { return static_cast<int32_t>(offsetof(MaxTurn_t585750252, ___alfaLeftLeft_29)); }
	inline double get_alfaLeftLeft_29() const { return ___alfaLeftLeft_29; }
	inline double* get_address_of_alfaLeftLeft_29() { return &___alfaLeftLeft_29; }
	inline void set_alfaLeftLeft_29(double value)
	{
		___alfaLeftLeft_29 = value;
	}

	inline static int32_t get_offset_of_alfaRightLeft_30() { return static_cast<int32_t>(offsetof(MaxTurn_t585750252, ___alfaRightLeft_30)); }
	inline double get_alfaRightLeft_30() const { return ___alfaRightLeft_30; }
	inline double* get_address_of_alfaRightLeft_30() { return &___alfaRightLeft_30; }
	inline void set_alfaRightLeft_30(double value)
	{
		___alfaRightLeft_30 = value;
	}

	inline static int32_t get_offset_of_alfaLeftRight_31() { return static_cast<int32_t>(offsetof(MaxTurn_t585750252, ___alfaLeftRight_31)); }
	inline double get_alfaLeftRight_31() const { return ___alfaLeftRight_31; }
	inline double* get_address_of_alfaLeftRight_31() { return &___alfaLeftRight_31; }
	inline void set_alfaLeftRight_31(double value)
	{
		___alfaLeftRight_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
