﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_whoodooce226
struct  M_whoodooce226_t1454824529  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_whoodooce226::_ceekemreCefow
	int32_t ____ceekemreCefow_0;
	// System.Single GarbageiOS.M_whoodooce226::_kairsearHeater
	float ____kairsearHeater_1;
	// System.Boolean GarbageiOS.M_whoodooce226::_kehiLawo
	bool ____kehiLawo_2;
	// System.Int32 GarbageiOS.M_whoodooce226::_zisqertrear
	int32_t ____zisqertrear_3;
	// System.Boolean GarbageiOS.M_whoodooce226::_zalwoostawTobe
	bool ____zalwoostawTobe_4;
	// System.Boolean GarbageiOS.M_whoodooce226::_zawzarzow
	bool ____zawzarzow_5;
	// System.Int32 GarbageiOS.M_whoodooce226::_drawfembear
	int32_t ____drawfembear_6;

public:
	inline static int32_t get_offset_of__ceekemreCefow_0() { return static_cast<int32_t>(offsetof(M_whoodooce226_t1454824529, ____ceekemreCefow_0)); }
	inline int32_t get__ceekemreCefow_0() const { return ____ceekemreCefow_0; }
	inline int32_t* get_address_of__ceekemreCefow_0() { return &____ceekemreCefow_0; }
	inline void set__ceekemreCefow_0(int32_t value)
	{
		____ceekemreCefow_0 = value;
	}

	inline static int32_t get_offset_of__kairsearHeater_1() { return static_cast<int32_t>(offsetof(M_whoodooce226_t1454824529, ____kairsearHeater_1)); }
	inline float get__kairsearHeater_1() const { return ____kairsearHeater_1; }
	inline float* get_address_of__kairsearHeater_1() { return &____kairsearHeater_1; }
	inline void set__kairsearHeater_1(float value)
	{
		____kairsearHeater_1 = value;
	}

	inline static int32_t get_offset_of__kehiLawo_2() { return static_cast<int32_t>(offsetof(M_whoodooce226_t1454824529, ____kehiLawo_2)); }
	inline bool get__kehiLawo_2() const { return ____kehiLawo_2; }
	inline bool* get_address_of__kehiLawo_2() { return &____kehiLawo_2; }
	inline void set__kehiLawo_2(bool value)
	{
		____kehiLawo_2 = value;
	}

	inline static int32_t get_offset_of__zisqertrear_3() { return static_cast<int32_t>(offsetof(M_whoodooce226_t1454824529, ____zisqertrear_3)); }
	inline int32_t get__zisqertrear_3() const { return ____zisqertrear_3; }
	inline int32_t* get_address_of__zisqertrear_3() { return &____zisqertrear_3; }
	inline void set__zisqertrear_3(int32_t value)
	{
		____zisqertrear_3 = value;
	}

	inline static int32_t get_offset_of__zalwoostawTobe_4() { return static_cast<int32_t>(offsetof(M_whoodooce226_t1454824529, ____zalwoostawTobe_4)); }
	inline bool get__zalwoostawTobe_4() const { return ____zalwoostawTobe_4; }
	inline bool* get_address_of__zalwoostawTobe_4() { return &____zalwoostawTobe_4; }
	inline void set__zalwoostawTobe_4(bool value)
	{
		____zalwoostawTobe_4 = value;
	}

	inline static int32_t get_offset_of__zawzarzow_5() { return static_cast<int32_t>(offsetof(M_whoodooce226_t1454824529, ____zawzarzow_5)); }
	inline bool get__zawzarzow_5() const { return ____zawzarzow_5; }
	inline bool* get_address_of__zawzarzow_5() { return &____zawzarzow_5; }
	inline void set__zawzarzow_5(bool value)
	{
		____zawzarzow_5 = value;
	}

	inline static int32_t get_offset_of__drawfembear_6() { return static_cast<int32_t>(offsetof(M_whoodooce226_t1454824529, ____drawfembear_6)); }
	inline int32_t get__drawfembear_6() const { return ____drawfembear_6; }
	inline int32_t* get_address_of__drawfembear_6() { return &____drawfembear_6; }
	inline void set__drawfembear_6(int32_t value)
	{
		____drawfembear_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
