﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.RVO.RVOQuadtree/Node[]
struct NodeU5BU5D_t3209507899;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RVO.RVOQuadtree
struct  RVOQuadtree_t4291712126  : public Il2CppObject
{
public:
	// System.Single Pathfinding.RVO.RVOQuadtree::maxRadius
	float ___maxRadius_1;
	// Pathfinding.RVO.RVOQuadtree/Node[] Pathfinding.RVO.RVOQuadtree::nodes
	NodeU5BU5D_t3209507899* ___nodes_2;
	// System.Int32 Pathfinding.RVO.RVOQuadtree::filledNodes
	int32_t ___filledNodes_3;
	// UnityEngine.Rect Pathfinding.RVO.RVOQuadtree::bounds
	Rect_t4241904616  ___bounds_4;

public:
	inline static int32_t get_offset_of_maxRadius_1() { return static_cast<int32_t>(offsetof(RVOQuadtree_t4291712126, ___maxRadius_1)); }
	inline float get_maxRadius_1() const { return ___maxRadius_1; }
	inline float* get_address_of_maxRadius_1() { return &___maxRadius_1; }
	inline void set_maxRadius_1(float value)
	{
		___maxRadius_1 = value;
	}

	inline static int32_t get_offset_of_nodes_2() { return static_cast<int32_t>(offsetof(RVOQuadtree_t4291712126, ___nodes_2)); }
	inline NodeU5BU5D_t3209507899* get_nodes_2() const { return ___nodes_2; }
	inline NodeU5BU5D_t3209507899** get_address_of_nodes_2() { return &___nodes_2; }
	inline void set_nodes_2(NodeU5BU5D_t3209507899* value)
	{
		___nodes_2 = value;
		Il2CppCodeGenWriteBarrier(&___nodes_2, value);
	}

	inline static int32_t get_offset_of_filledNodes_3() { return static_cast<int32_t>(offsetof(RVOQuadtree_t4291712126, ___filledNodes_3)); }
	inline int32_t get_filledNodes_3() const { return ___filledNodes_3; }
	inline int32_t* get_address_of_filledNodes_3() { return &___filledNodes_3; }
	inline void set_filledNodes_3(int32_t value)
	{
		___filledNodes_3 = value;
	}

	inline static int32_t get_offset_of_bounds_4() { return static_cast<int32_t>(offsetof(RVOQuadtree_t4291712126, ___bounds_4)); }
	inline Rect_t4241904616  get_bounds_4() const { return ___bounds_4; }
	inline Rect_t4241904616 * get_address_of_bounds_4() { return &___bounds_4; }
	inline void set_bounds_4(Rect_t4241904616  value)
	{
		___bounds_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
