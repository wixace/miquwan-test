﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Action`1<System.String>
struct Action_1_t403047693;
// System.IO.TextReader
struct TextReader_t2148718976;
// System.IO.TextWriter
struct TextWriter_t2304124208;
// Newtonsoft.Json.Utilities.StringUtils/ActionLine
struct ActionLine_t460070610;
// System.IO.StringWriter
struct StringWriter_t4216882900;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_IO_TextReader2148718976.h"
#include "mscorlib_System_IO_TextWriter2304124208.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_StringU460070610.h"
#include "mscorlib_System_Nullable_1_gen1237965023.h"

// System.String Newtonsoft.Json.Utilities.StringUtils::FormatWith(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* StringUtils_FormatWith_m3260740632 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.StringUtils::ContainsWhiteSpace(System.String)
extern "C"  bool StringUtils_ContainsWhiteSpace_m2122709882 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.StringUtils::IsWhiteSpace(System.String)
extern "C"  bool StringUtils_IsWhiteSpace_m2491032207 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.StringUtils::EnsureEndsWith(System.String,System.String)
extern "C"  String_t* StringUtils_EnsureEndsWith_m2768656901 (Il2CppObject * __this /* static, unused */, String_t* ___target0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.StringUtils::IsNullOrEmptyOrWhiteSpace(System.String)
extern "C"  bool StringUtils_IsNullOrEmptyOrWhiteSpace_m259533395 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.StringUtils::IfNotNullOrEmpty(System.String,System.Action`1<System.String>)
extern "C"  void StringUtils_IfNotNullOrEmpty_m4284518285 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Action_1_t403047693 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.StringUtils::IfNotNullOrEmpty(System.String,System.Action`1<System.String>,System.Action`1<System.String>)
extern "C"  void StringUtils_IfNotNullOrEmpty_m2348658805 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Action_1_t403047693 * ___trueAction1, Action_1_t403047693 * ___falseAction2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.StringUtils::Indent(System.String,System.Int32)
extern "C"  String_t* StringUtils_Indent_m3889443934 (Il2CppObject * __this /* static, unused */, String_t* ___s0, int32_t ___indentation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.StringUtils::Indent(System.String,System.Int32,System.Char)
extern "C"  String_t* StringUtils_Indent_m1706730997 (Il2CppObject * __this /* static, unused */, String_t* ___s0, int32_t ___indentation1, Il2CppChar ___indentChar2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.StringUtils::ActionTextReaderLine(System.IO.TextReader,System.IO.TextWriter,Newtonsoft.Json.Utilities.StringUtils/ActionLine)
extern "C"  void StringUtils_ActionTextReaderLine_m1955858648 (Il2CppObject * __this /* static, unused */, TextReader_t2148718976 * ___textReader0, TextWriter_t2304124208 * ___textWriter1, ActionLine_t460070610 * ___lineAction2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.StringUtils::NumberLines(System.String)
extern "C"  String_t* StringUtils_NumberLines_m2143165063 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.StringUtils::NullEmptyString(System.String)
extern "C"  String_t* StringUtils_NullEmptyString_m1623599078 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.StringUtils::ReplaceNewLines(System.String,System.String)
extern "C"  String_t* StringUtils_ReplaceNewLines_m2363437766 (Il2CppObject * __this /* static, unused */, String_t* ___s0, String_t* ___replacement1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.StringUtils::Truncate(System.String,System.Int32)
extern "C"  String_t* StringUtils_Truncate_m3670298392 (Il2CppObject * __this /* static, unused */, String_t* ___s0, int32_t ___maximumLength1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.StringUtils::Truncate(System.String,System.Int32,System.String)
extern "C"  String_t* StringUtils_Truncate_m3868798292 (Il2CppObject * __this /* static, unused */, String_t* ___s0, int32_t ___maximumLength1, String_t* ___suffix2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.StringWriter Newtonsoft.Json.Utilities.StringUtils::CreateStringWriter(System.Int32)
extern "C"  StringWriter_t4216882900 * StringUtils_CreateStringWriter_m4015224769 (Il2CppObject * __this /* static, unused */, int32_t ___capacity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.Utilities.StringUtils::GetLength(System.String)
extern "C"  Nullable_1_t1237965023  StringUtils_GetLength_m3269432277 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.StringUtils::ToCharAsUnicode(System.Char)
extern "C"  String_t* StringUtils_ToCharAsUnicode_m4026369150 (Il2CppObject * __this /* static, unused */, Il2CppChar ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.StringUtils::WriteCharAsUnicode(System.IO.TextWriter,System.Char)
extern "C"  void StringUtils_WriteCharAsUnicode_m368627844 (Il2CppObject * __this /* static, unused */, TextWriter_t2304124208 * ___writer0, Il2CppChar ___c1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.StringUtils::ToCamelCase(System.String)
extern "C"  String_t* StringUtils_ToCamelCase_m3833750098 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.StringUtils::ilo_IfNotNullOrEmpty1(System.String,System.Action`1<System.String>,System.Action`1<System.String>)
extern "C"  void StringUtils_ilo_IfNotNullOrEmpty1_m1210192943 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Action_1_t403047693 * ___trueAction1, Action_1_t403047693 * ___falseAction2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.StringUtils::ilo_Indent2(System.String,System.Int32,System.Char)
extern "C"  String_t* StringUtils_ilo_Indent2_m3770390916 (Il2CppObject * __this /* static, unused */, String_t* ___s0, int32_t ___indentation1, Il2CppChar ___indentChar2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.StringUtils::ilo_Invoke3(Newtonsoft.Json.Utilities.StringUtils/ActionLine,System.IO.TextWriter,System.String)
extern "C"  void StringUtils_ilo_Invoke3_m4075213953 (Il2CppObject * __this /* static, unused */, ActionLine_t460070610 * ____this0, TextWriter_t2304124208 * ___textWriter1, String_t* ___line2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.StringUtils::ilo_ActionTextReaderLine4(System.IO.TextReader,System.IO.TextWriter,Newtonsoft.Json.Utilities.StringUtils/ActionLine)
extern "C"  void StringUtils_ilo_ActionTextReaderLine4_m1082736295 (Il2CppObject * __this /* static, unused */, TextReader_t2148718976 * ___textReader0, TextWriter_t2304124208 * ___textWriter1, ActionLine_t460070610 * ___lineAction2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.StringUtils::ilo_Truncate5(System.String,System.Int32,System.String)
extern "C"  String_t* StringUtils_ilo_Truncate5_m3066067698 (Il2CppObject * __this /* static, unused */, String_t* ___s0, int32_t ___maximumLength1, String_t* ___suffix2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char Newtonsoft.Json.Utilities.StringUtils::ilo_IntToHex6(System.Int32)
extern "C"  Il2CppChar StringUtils_ilo_IntToHex6_m612084003 (Il2CppObject * __this /* static, unused */, int32_t ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
