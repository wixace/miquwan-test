﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Collections_ObjectModel_Collection_222222051.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Mail.AlternateViewCollection
struct  AlternateViewCollection_t943595443  : public Collection_1_t222222051
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
