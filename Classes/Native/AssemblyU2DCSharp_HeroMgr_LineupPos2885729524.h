﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroMgr/LineupPos
struct  LineupPos_t2885729524  : public Il2CppObject
{
public:
	// System.Boolean HeroMgr/LineupPos::_empty
	bool ____empty_0;
	// UnityEngine.Vector3 HeroMgr/LineupPos::_pos
	Vector3_t4282066566  ____pos_1;

public:
	inline static int32_t get_offset_of__empty_0() { return static_cast<int32_t>(offsetof(LineupPos_t2885729524, ____empty_0)); }
	inline bool get__empty_0() const { return ____empty_0; }
	inline bool* get_address_of__empty_0() { return &____empty_0; }
	inline void set__empty_0(bool value)
	{
		____empty_0 = value;
	}

	inline static int32_t get_offset_of__pos_1() { return static_cast<int32_t>(offsetof(LineupPos_t2885729524, ____pos_1)); }
	inline Vector3_t4282066566  get__pos_1() const { return ____pos_1; }
	inline Vector3_t4282066566 * get_address_of__pos_1() { return &____pos_1; }
	inline void set__pos_1(Vector3_t4282066566  value)
	{
		____pos_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
