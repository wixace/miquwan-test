﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.ForceMoveBvr
struct ForceMoveBvr_t1092279682;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// CombatEntity
struct CombatEntity_t684137495;
// System.Action
struct Action_t3771233898;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"

// System.Void Entity.Behavior.ForceMoveBvr::.ctor()
extern "C"  void ForceMoveBvr__ctor_m1900242408 (ForceMoveBvr_t1092279682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.ForceMoveBvr::get_id()
extern "C"  uint8_t ForceMoveBvr_get_id_m969609394 (ForceMoveBvr_t1092279682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.ForceMoveBvr::Reason()
extern "C"  void ForceMoveBvr_Reason_m2624327904 (ForceMoveBvr_t1092279682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.ForceMoveBvr::Action()
extern "C"  void ForceMoveBvr_Action_m1821034578 (ForceMoveBvr_t1092279682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.ForceMoveBvr::SetParams(System.Object[])
extern "C"  void ForceMoveBvr_SetParams_m2425366244 (ForceMoveBvr_t1092279682 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.ForceMoveBvr::DoEntering()
extern "C"  void ForceMoveBvr_DoEntering_m3915372689 (ForceMoveBvr_t1092279682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.ForceMoveBvr::DoLeaving()
extern "C"  void ForceMoveBvr_DoLeaving_m3203611887 (ForceMoveBvr_t1092279682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.ForceMoveBvr::Pause()
extern "C"  void ForceMoveBvr_Pause_m1954368380 (ForceMoveBvr_t1092279682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.ForceMoveBvr::Play()
extern "C"  void ForceMoveBvr_Play_m1180989584 (ForceMoveBvr_t1092279682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.ForceMoveBvr::ilo_UpdateTargetPos1(CombatEntity,UnityEngine.Vector3,System.Action)
extern "C"  void ForceMoveBvr_ilo_UpdateTargetPos1_m4059118553 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, Vector3_t4282066566  ___targetPos1, Action_t3771233898 * ___OnTargetCall2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.ForceMoveBvr::ilo_get_entity2(Entity.Behavior.IBehavior)
extern "C"  CombatEntity_t684137495 * ForceMoveBvr_ilo_get_entity2_m3536707765 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
