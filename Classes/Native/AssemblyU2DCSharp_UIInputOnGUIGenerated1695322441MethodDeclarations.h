﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIInputOnGUIGenerated
struct UIInputOnGUIGenerated_t1695322441;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UIInputOnGUIGenerated::.ctor()
extern "C"  void UIInputOnGUIGenerated__ctor_m232704578 (UIInputOnGUIGenerated_t1695322441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIInputOnGUIGenerated::UIInputOnGUI_UIInputOnGUI1(JSVCall,System.Int32)
extern "C"  bool UIInputOnGUIGenerated_UIInputOnGUI_UIInputOnGUI1_m2420081456 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputOnGUIGenerated::__Register()
extern "C"  void UIInputOnGUIGenerated___Register_m1980812229 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputOnGUIGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UIInputOnGUIGenerated_ilo_addJSCSRel1_m3264604158 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
