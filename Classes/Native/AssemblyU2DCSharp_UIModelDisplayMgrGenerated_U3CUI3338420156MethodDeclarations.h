﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIModelDisplayMgrGenerated/<UIModelDisplayMgr_AttachObject_GetDelegate_member0_arg2>c__AnonStoreyC7
struct U3CUIModelDisplayMgr_AttachObject_GetDelegate_member0_arg2U3Ec__AnonStoreyC7_t3338420156;

#include "codegen/il2cpp-codegen.h"

// System.Void UIModelDisplayMgrGenerated/<UIModelDisplayMgr_AttachObject_GetDelegate_member0_arg2>c__AnonStoreyC7::.ctor()
extern "C"  void U3CUIModelDisplayMgr_AttachObject_GetDelegate_member0_arg2U3Ec__AnonStoreyC7__ctor_m1740224303 (U3CUIModelDisplayMgr_AttachObject_GetDelegate_member0_arg2U3Ec__AnonStoreyC7_t3338420156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgrGenerated/<UIModelDisplayMgr_AttachObject_GetDelegate_member0_arg2>c__AnonStoreyC7::<>m__152()
extern "C"  void U3CUIModelDisplayMgr_AttachObject_GetDelegate_member0_arg2U3Ec__AnonStoreyC7_U3CU3Em__152_m1301901752 (U3CUIModelDisplayMgr_AttachObject_GetDelegate_member0_arg2U3Ec__AnonStoreyC7_t3338420156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
