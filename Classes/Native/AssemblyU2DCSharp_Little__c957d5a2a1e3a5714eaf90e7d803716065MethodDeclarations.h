﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._c957d5a2a1e3a5714eaf90e7dc4af1e1
struct _c957d5a2a1e3a5714eaf90e7dc4af1e1_t803716065;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._c957d5a2a1e3a5714eaf90e7dc4af1e1::.ctor()
extern "C"  void _c957d5a2a1e3a5714eaf90e7dc4af1e1__ctor_m3656005484 (_c957d5a2a1e3a5714eaf90e7dc4af1e1_t803716065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c957d5a2a1e3a5714eaf90e7dc4af1e1::_c957d5a2a1e3a5714eaf90e7dc4af1e1m2(System.Int32)
extern "C"  int32_t _c957d5a2a1e3a5714eaf90e7dc4af1e1__c957d5a2a1e3a5714eaf90e7dc4af1e1m2_m1706622457 (_c957d5a2a1e3a5714eaf90e7dc4af1e1_t803716065 * __this, int32_t ____c957d5a2a1e3a5714eaf90e7dc4af1e1a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c957d5a2a1e3a5714eaf90e7dc4af1e1::_c957d5a2a1e3a5714eaf90e7dc4af1e1m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _c957d5a2a1e3a5714eaf90e7dc4af1e1__c957d5a2a1e3a5714eaf90e7dc4af1e1m_m3503051997 (_c957d5a2a1e3a5714eaf90e7dc4af1e1_t803716065 * __this, int32_t ____c957d5a2a1e3a5714eaf90e7dc4af1e1a0, int32_t ____c957d5a2a1e3a5714eaf90e7dc4af1e1601, int32_t ____c957d5a2a1e3a5714eaf90e7dc4af1e1c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
