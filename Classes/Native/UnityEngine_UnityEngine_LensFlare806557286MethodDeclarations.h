﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.LensFlare
struct LensFlare_t806557286;
// UnityEngine.Flare
struct Flare_t4197217604;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Flare4197217604.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Void UnityEngine.LensFlare::.ctor()
extern "C"  void LensFlare__ctor_m2302252393 (LensFlare_t806557286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Flare UnityEngine.LensFlare::get_flare()
extern "C"  Flare_t4197217604 * LensFlare_get_flare_m237775112 (LensFlare_t806557286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LensFlare::set_flare(UnityEngine.Flare)
extern "C"  void LensFlare_set_flare_m2151642371 (LensFlare_t806557286 * __this, Flare_t4197217604 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.LensFlare::get_brightness()
extern "C"  float LensFlare_get_brightness_m1330115561 (LensFlare_t806557286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LensFlare::set_brightness(System.Single)
extern "C"  void LensFlare_set_brightness_m3900664162 (LensFlare_t806557286 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.LensFlare::get_fadeSpeed()
extern "C"  float LensFlare_get_fadeSpeed_m4184802229 (LensFlare_t806557286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LensFlare::set_fadeSpeed(System.Single)
extern "C"  void LensFlare_set_fadeSpeed_m4172829462 (LensFlare_t806557286 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.LensFlare::get_color()
extern "C"  Color_t4194546905  LensFlare_get_color_m2779121576 (LensFlare_t806557286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LensFlare::set_color(UnityEngine.Color)
extern "C"  void LensFlare_set_color_m3406581337 (LensFlare_t806557286 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LensFlare::INTERNAL_get_color(UnityEngine.Color&)
extern "C"  void LensFlare_INTERNAL_get_color_m3899716857 (LensFlare_t806557286 * __this, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LensFlare::INTERNAL_set_color(UnityEngine.Color&)
extern "C"  void LensFlare_INTERNAL_set_color_m3627002629 (LensFlare_t806557286 * __this, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
