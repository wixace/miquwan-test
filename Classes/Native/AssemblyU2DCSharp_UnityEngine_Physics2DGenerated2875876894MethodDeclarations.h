﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Physics2DGenerated
struct UnityEngine_Physics2DGenerated_t2875876894;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t889400257;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t1758559887;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_Physics2DGenerated::.ctor()
extern "C"  void UnityEngine_Physics2DGenerated__ctor_m3464378429 (UnityEngine_Physics2DGenerated_t2875876894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_Physics2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_Physics2D1_m1429003113 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::Physics2D_IgnoreRaycastLayer(JSVCall)
extern "C"  void UnityEngine_Physics2DGenerated_Physics2D_IgnoreRaycastLayer_m2259944896 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::Physics2D_DefaultRaycastLayers(JSVCall)
extern "C"  void UnityEngine_Physics2DGenerated_Physics2D_DefaultRaycastLayers_m1055227344 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::Physics2D_AllLayers(JSVCall)
extern "C"  void UnityEngine_Physics2DGenerated_Physics2D_AllLayers_m3652866511 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::Physics2D_velocityIterations(JSVCall)
extern "C"  void UnityEngine_Physics2DGenerated_Physics2D_velocityIterations_m2285484903 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::Physics2D_positionIterations(JSVCall)
extern "C"  void UnityEngine_Physics2DGenerated_Physics2D_positionIterations_m2993914779 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::Physics2D_gravity(JSVCall)
extern "C"  void UnityEngine_Physics2DGenerated_Physics2D_gravity_m944607492 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::Physics2D_queriesHitTriggers(JSVCall)
extern "C"  void UnityEngine_Physics2DGenerated_Physics2D_queriesHitTriggers_m4171542994 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::Physics2D_queriesStartInColliders(JSVCall)
extern "C"  void UnityEngine_Physics2DGenerated_Physics2D_queriesStartInColliders_m358473780 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::Physics2D_changeStopsCallbacks(JSVCall)
extern "C"  void UnityEngine_Physics2DGenerated_Physics2D_changeStopsCallbacks_m2118142637 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::Physics2D_velocityThreshold(JSVCall)
extern "C"  void UnityEngine_Physics2DGenerated_Physics2D_velocityThreshold_m1591372868 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::Physics2D_maxLinearCorrection(JSVCall)
extern "C"  void UnityEngine_Physics2DGenerated_Physics2D_maxLinearCorrection_m1746045835 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::Physics2D_maxAngularCorrection(JSVCall)
extern "C"  void UnityEngine_Physics2DGenerated_Physics2D_maxAngularCorrection_m1847145630 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::Physics2D_maxTranslationSpeed(JSVCall)
extern "C"  void UnityEngine_Physics2DGenerated_Physics2D_maxTranslationSpeed_m488931640 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::Physics2D_maxRotationSpeed(JSVCall)
extern "C"  void UnityEngine_Physics2DGenerated_Physics2D_maxRotationSpeed_m2627981877 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::Physics2D_minPenetrationForPenalty(JSVCall)
extern "C"  void UnityEngine_Physics2DGenerated_Physics2D_minPenetrationForPenalty_m2648514337 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::Physics2D_baumgarteScale(JSVCall)
extern "C"  void UnityEngine_Physics2DGenerated_Physics2D_baumgarteScale_m1802972962 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::Physics2D_baumgarteTOIScale(JSVCall)
extern "C"  void UnityEngine_Physics2DGenerated_Physics2D_baumgarteTOIScale_m205848324 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::Physics2D_timeToSleep(JSVCall)
extern "C"  void UnityEngine_Physics2DGenerated_Physics2D_timeToSleep_m187284707 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::Physics2D_linearSleepTolerance(JSVCall)
extern "C"  void UnityEngine_Physics2DGenerated_Physics2D_linearSleepTolerance_m3498124255 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::Physics2D_angularSleepTolerance(JSVCall)
extern "C"  void UnityEngine_Physics2DGenerated_Physics2D_angularSleepTolerance_m232596506 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_BoxCast__Vector2__Vector2__Single__Vector2__Single__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_BoxCast__Vector2__Vector2__Single__Vector2__Single__Int32__Single__Single_m1859150698 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_BoxCast__Vector2__Vector2__Single__Vector2__Single__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_BoxCast__Vector2__Vector2__Single__Vector2__Single__Int32__Single_m2231948834 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_BoxCast__Vector2__Vector2__Single__Vector2__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_BoxCast__Vector2__Vector2__Single__Vector2__Single__Int32_m173768922 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_BoxCast__Vector2__Vector2__Single__Vector2__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_BoxCast__Vector2__Vector2__Single__Vector2__Single_m3083365814 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_BoxCast__Vector2__Vector2__Single__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_BoxCast__Vector2__Vector2__Single__Vector2_m1428548718 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_BoxCastAll__Vector2__Vector2__Single__Vector2__Single__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_BoxCastAll__Vector2__Vector2__Single__Vector2__Single__Int32__Single__Single_m2334368279 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_BoxCastAll__Vector2__Vector2__Single__Vector2__Single__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_BoxCastAll__Vector2__Vector2__Single__Vector2__Single__Int32__Single_m2559054287 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_BoxCastAll__Vector2__Vector2__Single__Vector2__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_BoxCastAll__Vector2__Vector2__Single__Vector2__Single__Int32_m1800649095 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_BoxCastAll__Vector2__Vector2__Single__Vector2__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_BoxCastAll__Vector2__Vector2__Single__Vector2__Single_m1305537961 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_BoxCastAll__Vector2__Vector2__Single__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_BoxCastAll__Vector2__Vector2__Single__Vector2_m1014697825 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_BoxCastNonAlloc__Vector2__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_BoxCastNonAlloc__Vector2__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32__Single__Single_m2331016124 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_BoxCastNonAlloc__Vector2__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_BoxCastNonAlloc__Vector2__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32__Single_m2103926900 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_BoxCastNonAlloc__Vector2__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_BoxCastNonAlloc__Vector2__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32_m898268460 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_BoxCastNonAlloc__Vector2__Vector2__Single__Vector2__RaycastHit2D_Array__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_BoxCastNonAlloc__Vector2__Vector2__Single__Vector2__RaycastHit2D_Array__Single_m593807780 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_BoxCastNonAlloc__Vector2__Vector2__Single__Vector2__RaycastHit2D_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_BoxCastNonAlloc__Vector2__Vector2__Single__Vector2__RaycastHit2D_Array_m2304513628 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_CircleCast__Vector2__Single__Vector2__Single__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_CircleCast__Vector2__Single__Vector2__Single__Int32__Single__Single_m3522834656 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_CircleCast__Vector2__Single__Vector2__Single__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_CircleCast__Vector2__Single__Vector2__Single__Int32__Single_m2130344344 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_CircleCast__Vector2__Single__Vector2__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_CircleCast__Vector2__Single__Vector2__Single__Int32_m3008035408 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_CircleCast__Vector2__Single__Vector2__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_CircleCast__Vector2__Single__Vector2__Single_m1980697856 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_CircleCast__Vector2__Single__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_CircleCast__Vector2__Single__Vector2_m2671605176 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_CircleCastAll__Vector2__Single__Vector2__Single__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_CircleCastAll__Vector2__Single__Vector2__Single__Int32__Single__Single_m2964035453 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_CircleCastAll__Vector2__Single__Vector2__Single__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_CircleCastAll__Vector2__Single__Vector2__Single__Int32__Single_m3149319477 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_CircleCastAll__Vector2__Single__Vector2__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_CircleCastAll__Vector2__Single__Vector2__Single__Int32_m709835501 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_CircleCastAll__Vector2__Single__Vector2__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_CircleCastAll__Vector2__Single__Vector2__Single_m2896925443 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_CircleCastAll__Vector2__Single__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_CircleCastAll__Vector2__Single__Vector2_m1951772347 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_CircleCastNonAlloc__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_CircleCastNonAlloc__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32__Single__Single_m1330483490 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_CircleCastNonAlloc__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_CircleCastNonAlloc__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32__Single_m1988836314 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_CircleCastNonAlloc__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_CircleCastNonAlloc__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32_m563814034 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_CircleCastNonAlloc__Vector2__Single__Vector2__RaycastHit2D_Array__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_CircleCastNonAlloc__Vector2__Single__Vector2__RaycastHit2D_Array__Single_m1689145086 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_CircleCastNonAlloc__Vector2__Single__Vector2__RaycastHit2D_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_CircleCastNonAlloc__Vector2__Single__Vector2__RaycastHit2D_Array_m4035286454 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_GetIgnoreCollision__Collider2D__Collider2D(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_GetIgnoreCollision__Collider2D__Collider2D_m3151687495 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_GetIgnoreLayerCollision__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_GetIgnoreLayerCollision__Int32__Int32_m171480250 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_GetRayIntersection__Ray__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_GetRayIntersection__Ray__Single__Int32_m991387946 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_GetRayIntersection__Ray__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_GetRayIntersection__Ray__Single_m157632358 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_GetRayIntersection__Ray(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_GetRayIntersection__Ray_m1364162078 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_GetRayIntersectionAll__Ray__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_GetRayIntersectionAll__Ray__Single__Int32_m275780049 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_GetRayIntersectionAll__Ray__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_GetRayIntersectionAll__Ray__Single_m1776481439 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_GetRayIntersectionAll__Ray(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_GetRayIntersectionAll__Ray_m3789974615 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_GetRayIntersectionNonAlloc__Ray__RaycastHit2D_Array__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_GetRayIntersectionNonAlloc__Ray__RaycastHit2D_Array__Single__Int32_m1298239612 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_GetRayIntersectionNonAlloc__Ray__RaycastHit2D_Array__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_GetRayIntersectionNonAlloc__Ray__RaycastHit2D_Array__Single_m2982793300 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_GetRayIntersectionNonAlloc__Ray__RaycastHit2D_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_GetRayIntersectionNonAlloc__Ray__RaycastHit2D_Array_m3486174476 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_IgnoreCollision__Collider2D__Collider2D__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_IgnoreCollision__Collider2D__Collider2D__Boolean_m3661039245 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_IgnoreCollision__Collider2D__Collider2D(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_IgnoreCollision__Collider2D__Collider2D_m303541053 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_IgnoreLayerCollision__Int32__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_IgnoreLayerCollision__Int32__Int32__Boolean_m1457146534 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_IgnoreLayerCollision__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_IgnoreLayerCollision__Int32__Int32_m1013430020 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_IsTouching__Collider2D__Collider2D(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_IsTouching__Collider2D__Collider2D_m3470249546 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_IsTouchingLayers__Collider2D__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_IsTouchingLayers__Collider2D__Int32_m2808406890 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_IsTouchingLayers__Collider2D(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_IsTouchingLayers__Collider2D_m164220198 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_Linecast__Vector2__Vector2__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_Linecast__Vector2__Vector2__Int32__Single__Single_m1674327388 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_Linecast__Vector2__Vector2__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_Linecast__Vector2__Vector2__Int32__Single_m3488004628 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_Linecast__Vector2__Vector2__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_Linecast__Vector2__Vector2__Int32_m793605836 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_Linecast__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_Linecast__Vector2__Vector2_m2308006916 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_LinecastAll__Vector2__Vector2__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_LinecastAll__Vector2__Vector2__Int32__Single__Single_m2120433537 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_LinecastAll__Vector2__Vector2__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_LinecastAll__Vector2__Vector2__Int32__Single_m50903353 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_LinecastAll__Vector2__Vector2__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_LinecastAll__Vector2__Vector2__Int32_m3149884145 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_LinecastAll__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_LinecastAll__Vector2__Vector2_m1055367039 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_LinecastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_LinecastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Int32__Single__Single_m2359529886 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_LinecastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_LinecastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Int32__Single_m3300358230 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_LinecastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_LinecastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Int32_m1754896142 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_LinecastNonAlloc__Vector2__Vector2__RaycastHit2D_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_LinecastNonAlloc__Vector2__Vector2__RaycastHit2D_Array_m4202179074 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapArea__Vector2__Vector2__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapArea__Vector2__Vector2__Int32__Single__Single_m94898363 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapArea__Vector2__Vector2__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapArea__Vector2__Vector2__Int32__Single_m3592379507 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapArea__Vector2__Vector2__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapArea__Vector2__Vector2__Int32_m253880363 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapArea__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapArea__Vector2__Vector2_m1640640389 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapAreaAll__Vector2__Vector2__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapAreaAll__Vector2__Vector2__Int32__Single__Single_m717077442 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapAreaAll__Vector2__Vector2__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapAreaAll__Vector2__Vector2__Int32__Single_m4221568634 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapAreaAll__Vector2__Vector2__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapAreaAll__Vector2__Vector2__Int32_m250906930 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapAreaAll__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapAreaAll__Vector2__Vector2_m942774366 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapAreaNonAlloc__Vector2__Vector2__Collider2D_Array__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapAreaNonAlloc__Vector2__Vector2__Collider2D_Array__Int32__Single__Single_m1222169651 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapAreaNonAlloc__Vector2__Vector2__Collider2D_Array__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapAreaNonAlloc__Vector2__Vector2__Collider2D_Array__Int32__Single_m2007965675 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapAreaNonAlloc__Vector2__Vector2__Collider2D_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapAreaNonAlloc__Vector2__Vector2__Collider2D_Array__Int32_m4186481571 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapAreaNonAlloc__Vector2__Vector2__Collider2D_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapAreaNonAlloc__Vector2__Vector2__Collider2D_Array_m46852877 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapCircle__Vector2__Single__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapCircle__Vector2__Single__Int32__Single__Single_m682565711 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapCircle__Vector2__Single__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapCircle__Vector2__Single__Int32__Single_m2427537415 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapCircle__Vector2__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapCircle__Vector2__Single__Int32_m861054911 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapCircle__Vector2__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapCircle__Vector2__Single_m710479473 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapCircleAll__Vector2__Single__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapCircleAll__Vector2__Single__Int32__Single__Single_m63119106 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapCircleAll__Vector2__Single__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapCircleAll__Vector2__Single__Int32__Single_m752658874 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapCircleAll__Vector2__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapCircleAll__Vector2__Single__Int32_m3819465842 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapCircleAll__Vector2__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapCircleAll__Vector2__Single_m2720718110 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapCircleNonAlloc__Vector2__Single__Collider2D_Array__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapCircleNonAlloc__Vector2__Single__Collider2D_Array__Int32__Single__Single_m2096458711 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapCircleNonAlloc__Vector2__Single__Collider2D_Array__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapCircleNonAlloc__Vector2__Single__Collider2D_Array__Int32__Single_m3198131087 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapCircleNonAlloc__Vector2__Single__Collider2D_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapCircleNonAlloc__Vector2__Single__Collider2D_Array__Int32_m1198588743 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapCircleNonAlloc__Vector2__Single__Collider2D_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapCircleNonAlloc__Vector2__Single__Collider2D_Array_m4132379625 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapPoint__Vector2__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapPoint__Vector2__Int32__Single__Single_m353543017 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapPoint__Vector2__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapPoint__Vector2__Int32__Single_m1823376673 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapPoint__Vector2__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapPoint__Vector2__Int32_m3913522905 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapPoint__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapPoint__Vector2_m1039171735 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapPointAll__Vector2__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapPointAll__Vector2__Int32__Single__Single_m720299064 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapPointAll__Vector2__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapPointAll__Vector2__Int32__Single_m4045669616 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapPointAll__Vector2__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapPointAll__Vector2__Int32_m3439418792 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapPointAll__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapPointAll__Vector2_m1104886952 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapPointNonAlloc__Vector2__Collider2D_Array__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapPointNonAlloc__Vector2__Collider2D_Array__Int32__Single__Single_m3961959025 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapPointNonAlloc__Vector2__Collider2D_Array__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapPointNonAlloc__Vector2__Collider2D_Array__Int32__Single_m2871401513 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapPointNonAlloc__Vector2__Collider2D_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapPointNonAlloc__Vector2__Collider2D_Array__Int32_m3475422689 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_OverlapPointNonAlloc__Vector2__Collider2D_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_OverlapPointNonAlloc__Vector2__Collider2D_Array_m2473507471 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_Raycast__Vector2__Vector2__Single__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_Raycast__Vector2__Vector2__Single__Int32__Single__Single_m4172383966 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_Raycast__Vector2__Vector2__Single__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_Raycast__Vector2__Vector2__Single__Int32__Single_m3685658006 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_Raycast__Vector2__Vector2__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_Raycast__Vector2__Vector2__Single__Int32_m1203375182 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_Raycast__Vector2__Vector2__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_Raycast__Vector2__Vector2__Single_m2267356354 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_Raycast__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_Raycast__Vector2__Vector2_m1106798458 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_RaycastAll__Vector2__Vector2__Single__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_RaycastAll__Vector2__Vector2__Single__Int32__Single__Single_m630928431 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_RaycastAll__Vector2__Vector2__Single__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_RaycastAll__Vector2__Vector2__Single__Int32__Single_m3646897127 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_RaycastAll__Vector2__Vector2__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_RaycastAll__Vector2__Vector2__Single__Int32_m396524447 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_RaycastAll__Vector2__Vector2__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_RaycastAll__Vector2__Vector2__Single_m1958598289 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_RaycastAll__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_RaycastAll__Vector2__Vector2_m1521705033 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_RaycastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Single__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_RaycastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Single__Int32__Single__Single_m3333836320 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_RaycastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Single__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_RaycastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Single__Int32__Single_m591910616 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_RaycastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Single__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_RaycastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Single__Int32_m1281988496 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_RaycastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_RaycastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Single_m3369785280 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::Physics2D_RaycastNonAlloc__Vector2__Vector2__RaycastHit2D_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_Physics2D_RaycastNonAlloc__Vector2__Vector2__RaycastHit2D_Array_m2631925880 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::__Register()
extern "C"  void UnityEngine_Physics2DGenerated___Register_m3982353066 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_BoxCastNonAlloc__Vector2__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32__Single__Single>m__287()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_BoxCastNonAlloc__Vector2__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32__Single__SingleU3Em__287_m1901876076 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_BoxCastNonAlloc__Vector2__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32__Single>m__288()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_BoxCastNonAlloc__Vector2__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32__SingleU3Em__288_m191404917 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_BoxCastNonAlloc__Vector2__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32>m__289()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_BoxCastNonAlloc__Vector2__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32U3Em__289_m1711385470 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_BoxCastNonAlloc__Vector2__Vector2__Single__Vector2__RaycastHit2D_Array__Single>m__28A()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_BoxCastNonAlloc__Vector2__Vector2__Single__Vector2__RaycastHit2D_Array__SingleU3Em__28A_m2898941478 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_BoxCastNonAlloc__Vector2__Vector2__Single__Vector2__RaycastHit2D_Array>m__28B()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_BoxCastNonAlloc__Vector2__Vector2__Single__Vector2__RaycastHit2D_ArrayU3Em__28B_m1449053743 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_CircleCastNonAlloc__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32__Single__Single>m__28C()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_CircleCastNonAlloc__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32__Single__SingleU3Em__28C_m2606634066 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_CircleCastNonAlloc__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32__Single>m__28D()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_CircleCastNonAlloc__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32__SingleU3Em__28D_m293656155 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_CircleCastNonAlloc__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32>m__28E()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_CircleCastNonAlloc__Vector2__Single__Vector2__RaycastHit2D_Array__Single__Int32U3Em__28E_m3679084644 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_CircleCastNonAlloc__Vector2__Single__Vector2__RaycastHit2D_Array__Single>m__28F()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_CircleCastNonAlloc__Vector2__Single__Vector2__RaycastHit2D_Array__SingleU3Em__28F_m2912395857 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_CircleCastNonAlloc__Vector2__Single__Vector2__RaycastHit2D_Array>m__290()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_CircleCastNonAlloc__Vector2__Single__Vector2__RaycastHit2D_ArrayU3Em__290_m5701218 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_GetRayIntersectionNonAlloc__Ray__RaycastHit2D_Array__Single__Int32>m__291()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_GetRayIntersectionNonAlloc__Ray__RaycastHit2D_Array__Single__Int32U3Em__291_m636164765 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_GetRayIntersectionNonAlloc__Ray__RaycastHit2D_Array__Single>m__292()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_GetRayIntersectionNonAlloc__Ray__RaycastHit2D_Array__SingleU3Em__292_m3561245294 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_GetRayIntersectionNonAlloc__Ray__RaycastHit2D_Array>m__293()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_GetRayIntersectionNonAlloc__Ray__RaycastHit2D_ArrayU3Em__293_m2672851575 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_LinecastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Int32__Single__Single>m__294()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_LinecastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Int32__Single__SingleU3Em__294_m4028570470 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_LinecastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Int32__Single>m__295()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_LinecastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Int32__SingleU3Em__295_m548348271 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_LinecastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Int32>m__296()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_LinecastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Int32U3Em__296_m4167692152 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_LinecastNonAlloc__Vector2__Vector2__RaycastHit2D_Array>m__297()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_LinecastNonAlloc__Vector2__Vector2__RaycastHit2D_ArrayU3Em__297_m3180734429 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine_Physics2DGenerated::<Physics2D_OverlapAreaNonAlloc__Vector2__Vector2__Collider2D_Array__Int32__Single__Single>m__298()
extern "C"  Collider2DU5BU5D_t1758559887* UnityEngine_Physics2DGenerated_U3CPhysics2D_OverlapAreaNonAlloc__Vector2__Vector2__Collider2D_Array__Int32__Single__SingleU3Em__298_m3785741955 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine_Physics2DGenerated::<Physics2D_OverlapAreaNonAlloc__Vector2__Vector2__Collider2D_Array__Int32__Single>m__299()
extern "C"  Collider2DU5BU5D_t1758559887* UnityEngine_Physics2DGenerated_U3CPhysics2D_OverlapAreaNonAlloc__Vector2__Vector2__Collider2D_Array__Int32__SingleU3Em__299_m3811313036 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine_Physics2DGenerated::<Physics2D_OverlapAreaNonAlloc__Vector2__Vector2__Collider2D_Array__Int32>m__29A()
extern "C"  Collider2DU5BU5D_t1758559887* UnityEngine_Physics2DGenerated_U3CPhysics2D_OverlapAreaNonAlloc__Vector2__Vector2__Collider2D_Array__Int32U3Em__29A_m890640092 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine_Physics2DGenerated::<Physics2D_OverlapAreaNonAlloc__Vector2__Vector2__Collider2D_Array>m__29B()
extern "C"  Collider2DU5BU5D_t1758559887* UnityEngine_Physics2DGenerated_U3CPhysics2D_OverlapAreaNonAlloc__Vector2__Vector2__Collider2D_ArrayU3Em__29B_m923209007 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine_Physics2DGenerated::<Physics2D_OverlapCircleNonAlloc__Vector2__Single__Collider2D_Array__Int32__Single__Single>m__29C()
extern "C"  Collider2DU5BU5D_t1758559887* UnityEngine_Physics2DGenerated_U3CPhysics2D_OverlapCircleNonAlloc__Vector2__Single__Collider2D_Array__Int32__Single__SingleU3Em__29C_m1485944294 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine_Physics2DGenerated::<Physics2D_OverlapCircleNonAlloc__Vector2__Single__Collider2D_Array__Int32__Single>m__29D()
extern "C"  Collider2DU5BU5D_t1758559887* UnityEngine_Physics2DGenerated_U3CPhysics2D_OverlapCircleNonAlloc__Vector2__Single__Collider2D_Array__Int32__SingleU3Em__29D_m3101392111 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine_Physics2DGenerated::<Physics2D_OverlapCircleNonAlloc__Vector2__Single__Collider2D_Array__Int32>m__29E()
extern "C"  Collider2DU5BU5D_t1758559887* UnityEngine_Physics2DGenerated_U3CPhysics2D_OverlapCircleNonAlloc__Vector2__Single__Collider2D_Array__Int32U3Em__29E_m3533769720 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine_Physics2DGenerated::<Physics2D_OverlapCircleNonAlloc__Vector2__Single__Collider2D_Array>m__29F()
extern "C"  Collider2DU5BU5D_t1758559887* UnityEngine_Physics2DGenerated_U3CPhysics2D_OverlapCircleNonAlloc__Vector2__Single__Collider2D_ArrayU3Em__29F_m2690146587 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine_Physics2DGenerated::<Physics2D_OverlapPointNonAlloc__Vector2__Collider2D_Array__Int32__Single__Single>m__2A0()
extern "C"  Collider2DU5BU5D_t1758559887* UnityEngine_Physics2DGenerated_U3CPhysics2D_OverlapPointNonAlloc__Vector2__Collider2D_Array__Int32__Single__SingleU3Em__2A0_m1467896437 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine_Physics2DGenerated::<Physics2D_OverlapPointNonAlloc__Vector2__Collider2D_Array__Int32__Single>m__2A1()
extern "C"  Collider2DU5BU5D_t1758559887* UnityEngine_Physics2DGenerated_U3CPhysics2D_OverlapPointNonAlloc__Vector2__Collider2D_Array__Int32__SingleU3Em__2A1_m3332678526 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine_Physics2DGenerated::<Physics2D_OverlapPointNonAlloc__Vector2__Collider2D_Array__Int32>m__2A2()
extern "C"  Collider2DU5BU5D_t1758559887* UnityEngine_Physics2DGenerated_U3CPhysics2D_OverlapPointNonAlloc__Vector2__Collider2D_Array__Int32U3Em__2A2_m1903213703 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine_Physics2DGenerated::<Physics2D_OverlapPointNonAlloc__Vector2__Collider2D_Array>m__2A3()
extern "C"  Collider2DU5BU5D_t1758559887* UnityEngine_Physics2DGenerated_U3CPhysics2D_OverlapPointNonAlloc__Vector2__Collider2D_ArrayU3Em__2A3_m2751118102 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_RaycastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Single__Int32__Single__Single>m__2A4()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_RaycastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Single__Int32__Single__SingleU3Em__2A4_m2071950772 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_RaycastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Single__Int32__Single>m__2A5()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_RaycastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Single__Int32__SingleU3Em__2A5_m2513383869 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_RaycastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Single__Int32>m__2A6()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_RaycastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Single__Int32U3Em__2A6_m1432335814 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_RaycastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__Single>m__2A7()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_RaycastNonAlloc__Vector2__Vector2__RaycastHit2D_Array__SingleU3Em__2A7_m544676927 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine_Physics2DGenerated::<Physics2D_RaycastNonAlloc__Vector2__Vector2__RaycastHit2D_Array>m__2A8()
extern "C"  RaycastHit2DU5BU5D_t889400257* UnityEngine_Physics2DGenerated_U3CPhysics2D_RaycastNonAlloc__Vector2__Vector2__RaycastHit2D_ArrayU3Em__2A8_m194682952 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_Physics2DGenerated_ilo_addJSCSRel1_m2588882041 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Physics2DGenerated::ilo_getInt322(System.Int32)
extern "C"  int32_t UnityEngine_Physics2DGenerated_ilo_getInt322_m816105157 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void UnityEngine_Physics2DGenerated_ilo_setInt323_m437003767 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::ilo_setVector2S4(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_Physics2DGenerated_ilo_setVector2S4_m1124746637 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_Physics2DGenerated::ilo_getVector2S5(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_Physics2DGenerated_ilo_getVector2S5_m3844596609 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::ilo_setBooleanS6(System.Int32,System.Boolean)
extern "C"  void UnityEngine_Physics2DGenerated_ilo_setBooleanS6_m1862131465 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Physics2DGenerated::ilo_getBooleanS7(System.Int32)
extern "C"  bool UnityEngine_Physics2DGenerated_ilo_getBooleanS7_m331366005 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_Physics2DGenerated::ilo_getSingle8(System.Int32)
extern "C"  float UnityEngine_Physics2DGenerated_ilo_getSingle8_m3263912913 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::ilo_setSingle9(System.Int32,System.Single)
extern "C"  void UnityEngine_Physics2DGenerated_ilo_setSingle9_m1129149215 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::ilo_setArrayS10(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_Physics2DGenerated_ilo_setArrayS10_m2747985594 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Physics2DGenerated::ilo_setObject11(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_Physics2DGenerated_ilo_setObject11_m2825069328 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Physics2DGenerated::ilo_moveSaveID2Arr12(System.Int32)
extern "C"  void UnityEngine_Physics2DGenerated_ilo_moveSaveID2Arr12_m1115489860 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_Physics2DGenerated::ilo_getObject13(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_Physics2DGenerated_ilo_getObject13_m2770228529 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Physics2DGenerated::ilo_getElement14(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_Physics2DGenerated_ilo_getElement14_m3446661973 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Physics2DGenerated::ilo_getArrayLength15(System.Int32)
extern "C"  int32_t UnityEngine_Physics2DGenerated_ilo_getArrayLength15_m3479923750 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Physics2DGenerated::ilo_getObject16(System.Int32)
extern "C"  int32_t UnityEngine_Physics2DGenerated_ilo_getObject16_m498209093 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
