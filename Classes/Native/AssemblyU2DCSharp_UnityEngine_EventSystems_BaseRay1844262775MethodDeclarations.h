﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_BaseRaycasterGenerated
struct UnityEngine_EventSystems_BaseRaycasterGenerated_t1844262775;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_EventSystems_BaseRaycasterGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_BaseRaycasterGenerated__ctor_m727082452 (UnityEngine_EventSystems_BaseRaycasterGenerated_t1844262775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_BaseRaycasterGenerated::BaseRaycaster_eventCamera(JSVCall)
extern "C"  void UnityEngine_EventSystems_BaseRaycasterGenerated_BaseRaycaster_eventCamera_m2265363568 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_BaseRaycasterGenerated::BaseRaycaster_sortOrderPriority(JSVCall)
extern "C"  void UnityEngine_EventSystems_BaseRaycasterGenerated_BaseRaycaster_sortOrderPriority_m2237473179 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_BaseRaycasterGenerated::BaseRaycaster_renderOrderPriority(JSVCall)
extern "C"  void UnityEngine_EventSystems_BaseRaycasterGenerated_BaseRaycaster_renderOrderPriority_m2959826259 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_BaseRaycasterGenerated::BaseRaycaster_Raycast__PointerEventData__ListT1_RaycastResult(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_BaseRaycasterGenerated_BaseRaycaster_Raycast__PointerEventData__ListT1_RaycastResult_m2171021486 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_BaseRaycasterGenerated::BaseRaycaster_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_BaseRaycasterGenerated_BaseRaycaster_ToString_m2097520530 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_BaseRaycasterGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_BaseRaycasterGenerated___Register_m2739247987 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_EventSystems_BaseRaycasterGenerated::ilo_getObject1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_EventSystems_BaseRaycasterGenerated_ilo_getObject1_m1748425105 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
