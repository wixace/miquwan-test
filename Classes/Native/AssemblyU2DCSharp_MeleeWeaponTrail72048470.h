﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// UnityEngine.Transform
struct Transform_t1659122786;
// System.Collections.Generic.List`1<MeleeWeaponTrail/Point>
struct List_1_t1002179655;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Mesh
struct Mesh_t4241756145;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MeleeWeaponTrail
struct  MeleeWeaponTrail_t72048470  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean MeleeWeaponTrail::_emit
	bool ____emit_2;
	// System.Boolean MeleeWeaponTrail::_use
	bool ____use_3;
	// System.Single MeleeWeaponTrail::_emitTime
	float ____emitTime_4;
	// System.Single MeleeWeaponTrail::_initEmitTime
	float ____initEmitTime_5;
	// System.Boolean MeleeWeaponTrail::_initEmit
	bool ____initEmit_6;
	// UnityEngine.Material MeleeWeaponTrail::_material
	Material_t3870600107 * ____material_7;
	// System.Single MeleeWeaponTrail::_lifeTime
	float ____lifeTime_8;
	// UnityEngine.Color[] MeleeWeaponTrail::_colors
	ColorU5BU5D_t2441545636* ____colors_9;
	// System.Single[] MeleeWeaponTrail::_sizes
	SingleU5BU5D_t2316563989* ____sizes_10;
	// System.Single MeleeWeaponTrail::_minVertexDistance
	float ____minVertexDistance_11;
	// System.Single MeleeWeaponTrail::_maxVertexDistance
	float ____maxVertexDistance_12;
	// System.Single MeleeWeaponTrail::_minVertexDistanceSqr
	float ____minVertexDistanceSqr_13;
	// System.Single MeleeWeaponTrail::_maxVertexDistanceSqr
	float ____maxVertexDistanceSqr_14;
	// System.Single MeleeWeaponTrail::_maxAngle
	float ____maxAngle_15;
	// System.Boolean MeleeWeaponTrail::_autoDestruct
	bool ____autoDestruct_16;
	// System.Int32 MeleeWeaponTrail::subdivisions
	int32_t ___subdivisions_17;
	// UnityEngine.Transform MeleeWeaponTrail::_base
	Transform_t1659122786 * ____base_18;
	// UnityEngine.Transform MeleeWeaponTrail::_tip
	Transform_t1659122786 * ____tip_19;
	// System.Collections.Generic.List`1<MeleeWeaponTrail/Point> MeleeWeaponTrail::_points
	List_1_t1002179655 * ____points_20;
	// System.Collections.Generic.List`1<MeleeWeaponTrail/Point> MeleeWeaponTrail::_smoothedPoints
	List_1_t1002179655 * ____smoothedPoints_21;
	// UnityEngine.GameObject MeleeWeaponTrail::_trailObject
	GameObject_t3674682005 * ____trailObject_22;
	// UnityEngine.Mesh MeleeWeaponTrail::_trailMesh
	Mesh_t4241756145 * ____trailMesh_23;
	// UnityEngine.Vector3 MeleeWeaponTrail::_lastPosition
	Vector3_t4282066566  ____lastPosition_24;

public:
	inline static int32_t get_offset_of__emit_2() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____emit_2)); }
	inline bool get__emit_2() const { return ____emit_2; }
	inline bool* get_address_of__emit_2() { return &____emit_2; }
	inline void set__emit_2(bool value)
	{
		____emit_2 = value;
	}

	inline static int32_t get_offset_of__use_3() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____use_3)); }
	inline bool get__use_3() const { return ____use_3; }
	inline bool* get_address_of__use_3() { return &____use_3; }
	inline void set__use_3(bool value)
	{
		____use_3 = value;
	}

	inline static int32_t get_offset_of__emitTime_4() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____emitTime_4)); }
	inline float get__emitTime_4() const { return ____emitTime_4; }
	inline float* get_address_of__emitTime_4() { return &____emitTime_4; }
	inline void set__emitTime_4(float value)
	{
		____emitTime_4 = value;
	}

	inline static int32_t get_offset_of__initEmitTime_5() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____initEmitTime_5)); }
	inline float get__initEmitTime_5() const { return ____initEmitTime_5; }
	inline float* get_address_of__initEmitTime_5() { return &____initEmitTime_5; }
	inline void set__initEmitTime_5(float value)
	{
		____initEmitTime_5 = value;
	}

	inline static int32_t get_offset_of__initEmit_6() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____initEmit_6)); }
	inline bool get__initEmit_6() const { return ____initEmit_6; }
	inline bool* get_address_of__initEmit_6() { return &____initEmit_6; }
	inline void set__initEmit_6(bool value)
	{
		____initEmit_6 = value;
	}

	inline static int32_t get_offset_of__material_7() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____material_7)); }
	inline Material_t3870600107 * get__material_7() const { return ____material_7; }
	inline Material_t3870600107 ** get_address_of__material_7() { return &____material_7; }
	inline void set__material_7(Material_t3870600107 * value)
	{
		____material_7 = value;
		Il2CppCodeGenWriteBarrier(&____material_7, value);
	}

	inline static int32_t get_offset_of__lifeTime_8() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____lifeTime_8)); }
	inline float get__lifeTime_8() const { return ____lifeTime_8; }
	inline float* get_address_of__lifeTime_8() { return &____lifeTime_8; }
	inline void set__lifeTime_8(float value)
	{
		____lifeTime_8 = value;
	}

	inline static int32_t get_offset_of__colors_9() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____colors_9)); }
	inline ColorU5BU5D_t2441545636* get__colors_9() const { return ____colors_9; }
	inline ColorU5BU5D_t2441545636** get_address_of__colors_9() { return &____colors_9; }
	inline void set__colors_9(ColorU5BU5D_t2441545636* value)
	{
		____colors_9 = value;
		Il2CppCodeGenWriteBarrier(&____colors_9, value);
	}

	inline static int32_t get_offset_of__sizes_10() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____sizes_10)); }
	inline SingleU5BU5D_t2316563989* get__sizes_10() const { return ____sizes_10; }
	inline SingleU5BU5D_t2316563989** get_address_of__sizes_10() { return &____sizes_10; }
	inline void set__sizes_10(SingleU5BU5D_t2316563989* value)
	{
		____sizes_10 = value;
		Il2CppCodeGenWriteBarrier(&____sizes_10, value);
	}

	inline static int32_t get_offset_of__minVertexDistance_11() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____minVertexDistance_11)); }
	inline float get__minVertexDistance_11() const { return ____minVertexDistance_11; }
	inline float* get_address_of__minVertexDistance_11() { return &____minVertexDistance_11; }
	inline void set__minVertexDistance_11(float value)
	{
		____minVertexDistance_11 = value;
	}

	inline static int32_t get_offset_of__maxVertexDistance_12() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____maxVertexDistance_12)); }
	inline float get__maxVertexDistance_12() const { return ____maxVertexDistance_12; }
	inline float* get_address_of__maxVertexDistance_12() { return &____maxVertexDistance_12; }
	inline void set__maxVertexDistance_12(float value)
	{
		____maxVertexDistance_12 = value;
	}

	inline static int32_t get_offset_of__minVertexDistanceSqr_13() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____minVertexDistanceSqr_13)); }
	inline float get__minVertexDistanceSqr_13() const { return ____minVertexDistanceSqr_13; }
	inline float* get_address_of__minVertexDistanceSqr_13() { return &____minVertexDistanceSqr_13; }
	inline void set__minVertexDistanceSqr_13(float value)
	{
		____minVertexDistanceSqr_13 = value;
	}

	inline static int32_t get_offset_of__maxVertexDistanceSqr_14() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____maxVertexDistanceSqr_14)); }
	inline float get__maxVertexDistanceSqr_14() const { return ____maxVertexDistanceSqr_14; }
	inline float* get_address_of__maxVertexDistanceSqr_14() { return &____maxVertexDistanceSqr_14; }
	inline void set__maxVertexDistanceSqr_14(float value)
	{
		____maxVertexDistanceSqr_14 = value;
	}

	inline static int32_t get_offset_of__maxAngle_15() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____maxAngle_15)); }
	inline float get__maxAngle_15() const { return ____maxAngle_15; }
	inline float* get_address_of__maxAngle_15() { return &____maxAngle_15; }
	inline void set__maxAngle_15(float value)
	{
		____maxAngle_15 = value;
	}

	inline static int32_t get_offset_of__autoDestruct_16() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____autoDestruct_16)); }
	inline bool get__autoDestruct_16() const { return ____autoDestruct_16; }
	inline bool* get_address_of__autoDestruct_16() { return &____autoDestruct_16; }
	inline void set__autoDestruct_16(bool value)
	{
		____autoDestruct_16 = value;
	}

	inline static int32_t get_offset_of_subdivisions_17() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ___subdivisions_17)); }
	inline int32_t get_subdivisions_17() const { return ___subdivisions_17; }
	inline int32_t* get_address_of_subdivisions_17() { return &___subdivisions_17; }
	inline void set_subdivisions_17(int32_t value)
	{
		___subdivisions_17 = value;
	}

	inline static int32_t get_offset_of__base_18() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____base_18)); }
	inline Transform_t1659122786 * get__base_18() const { return ____base_18; }
	inline Transform_t1659122786 ** get_address_of__base_18() { return &____base_18; }
	inline void set__base_18(Transform_t1659122786 * value)
	{
		____base_18 = value;
		Il2CppCodeGenWriteBarrier(&____base_18, value);
	}

	inline static int32_t get_offset_of__tip_19() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____tip_19)); }
	inline Transform_t1659122786 * get__tip_19() const { return ____tip_19; }
	inline Transform_t1659122786 ** get_address_of__tip_19() { return &____tip_19; }
	inline void set__tip_19(Transform_t1659122786 * value)
	{
		____tip_19 = value;
		Il2CppCodeGenWriteBarrier(&____tip_19, value);
	}

	inline static int32_t get_offset_of__points_20() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____points_20)); }
	inline List_1_t1002179655 * get__points_20() const { return ____points_20; }
	inline List_1_t1002179655 ** get_address_of__points_20() { return &____points_20; }
	inline void set__points_20(List_1_t1002179655 * value)
	{
		____points_20 = value;
		Il2CppCodeGenWriteBarrier(&____points_20, value);
	}

	inline static int32_t get_offset_of__smoothedPoints_21() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____smoothedPoints_21)); }
	inline List_1_t1002179655 * get__smoothedPoints_21() const { return ____smoothedPoints_21; }
	inline List_1_t1002179655 ** get_address_of__smoothedPoints_21() { return &____smoothedPoints_21; }
	inline void set__smoothedPoints_21(List_1_t1002179655 * value)
	{
		____smoothedPoints_21 = value;
		Il2CppCodeGenWriteBarrier(&____smoothedPoints_21, value);
	}

	inline static int32_t get_offset_of__trailObject_22() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____trailObject_22)); }
	inline GameObject_t3674682005 * get__trailObject_22() const { return ____trailObject_22; }
	inline GameObject_t3674682005 ** get_address_of__trailObject_22() { return &____trailObject_22; }
	inline void set__trailObject_22(GameObject_t3674682005 * value)
	{
		____trailObject_22 = value;
		Il2CppCodeGenWriteBarrier(&____trailObject_22, value);
	}

	inline static int32_t get_offset_of__trailMesh_23() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____trailMesh_23)); }
	inline Mesh_t4241756145 * get__trailMesh_23() const { return ____trailMesh_23; }
	inline Mesh_t4241756145 ** get_address_of__trailMesh_23() { return &____trailMesh_23; }
	inline void set__trailMesh_23(Mesh_t4241756145 * value)
	{
		____trailMesh_23 = value;
		Il2CppCodeGenWriteBarrier(&____trailMesh_23, value);
	}

	inline static int32_t get_offset_of__lastPosition_24() { return static_cast<int32_t>(offsetof(MeleeWeaponTrail_t72048470, ____lastPosition_24)); }
	inline Vector3_t4282066566  get__lastPosition_24() const { return ____lastPosition_24; }
	inline Vector3_t4282066566 * get_address_of__lastPosition_24() { return &____lastPosition_24; }
	inline void set__lastPosition_24(Vector3_t4282066566  value)
	{
		____lastPosition_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
