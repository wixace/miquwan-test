﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator29<TypewriterEffect/FadeEntry>
struct U3CGetEnumeratorU3Ec__Iterator29_t310468562;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TypewriterEffect_FadeEntry2858472101.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator29<TypewriterEffect/FadeEntry>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29__ctor_m960196006_gshared (U3CGetEnumeratorU3Ec__Iterator29_t310468562 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29__ctor_m960196006(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t310468562 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29__ctor_m960196006_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator29<TypewriterEffect/FadeEntry>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  FadeEntry_t2858472101  U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m4244963181_gshared (U3CGetEnumeratorU3Ec__Iterator29_t310468562 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m4244963181(__this, method) ((  FadeEntry_t2858472101  (*) (U3CGetEnumeratorU3Ec__Iterator29_t310468562 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m4244963181_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator29<TypewriterEffect/FadeEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m363705536_gshared (U3CGetEnumeratorU3Ec__Iterator29_t310468562 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m363705536(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator29_t310468562 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m363705536_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator29<TypewriterEffect/FadeEntry>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m830737806_gshared (U3CGetEnumeratorU3Ec__Iterator29_t310468562 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m830737806(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator29_t310468562 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m830737806_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator29<TypewriterEffect/FadeEntry>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29_Dispose_m3523643491_gshared (U3CGetEnumeratorU3Ec__Iterator29_t310468562 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_Dispose_m3523643491(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t310468562 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_Dispose_m3523643491_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator29<TypewriterEffect/FadeEntry>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29_Reset_m2901596243_gshared (U3CGetEnumeratorU3Ec__Iterator29_t310468562 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_Reset_m2901596243(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t310468562 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_Reset_m2901596243_gshared)(__this, method)
