﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<UIModelDisplayType,System.Object>
struct ShimEnumerator_t1390318902;
// System.Collections.Generic.Dictionary`2<UIModelDisplayType,System.Object>
struct Dictionary_2_t1674540875;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UIModelDisplayType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m305146287_gshared (ShimEnumerator_t1390318902 * __this, Dictionary_2_t1674540875 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m305146287(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1390318902 *, Dictionary_2_t1674540875 *, const MethodInfo*))ShimEnumerator__ctor_m305146287_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<UIModelDisplayType,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3367438294_gshared (ShimEnumerator_t1390318902 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3367438294(__this, method) ((  bool (*) (ShimEnumerator_t1390318902 *, const MethodInfo*))ShimEnumerator_MoveNext_m3367438294_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<UIModelDisplayType,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m724621268_gshared (ShimEnumerator_t1390318902 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m724621268(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1390318902 *, const MethodInfo*))ShimEnumerator_get_Entry_m724621268_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UIModelDisplayType,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2971322771_gshared (ShimEnumerator_t1390318902 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m2971322771(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1390318902 *, const MethodInfo*))ShimEnumerator_get_Key_m2971322771_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UIModelDisplayType,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m336367461_gshared (ShimEnumerator_t1390318902 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m336367461(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1390318902 *, const MethodInfo*))ShimEnumerator_get_Value_m336367461_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UIModelDisplayType,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2551273645_gshared (ShimEnumerator_t1390318902 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2551273645(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1390318902 *, const MethodInfo*))ShimEnumerator_get_Current_m2551273645_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UIModelDisplayType,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m1934732673_gshared (ShimEnumerator_t1390318902 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1934732673(__this, method) ((  void (*) (ShimEnumerator_t1390318902 *, const MethodInfo*))ShimEnumerator_Reset_m1934732673_gshared)(__this, method)
