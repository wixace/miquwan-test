﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_forhu87
struct M_forhu87_t1831502421;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_forhu87::.ctor()
extern "C"  void M_forhu87__ctor_m3600520366 (M_forhu87_t1831502421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_forhu87::M_jayti0(System.String[],System.Int32)
extern "C"  void M_forhu87_M_jayti0_m981335128 (M_forhu87_t1831502421 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
