﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AIFollow/<WaitToRepath>c__Iterator15
struct U3CWaitToRepathU3Ec__Iterator15_t2392207237;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AIFollow/<WaitToRepath>c__Iterator15::.ctor()
extern "C"  void U3CWaitToRepathU3Ec__Iterator15__ctor_m3309805750 (U3CWaitToRepathU3Ec__Iterator15_t2392207237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AIFollow/<WaitToRepath>c__Iterator15::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitToRepathU3Ec__Iterator15_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1918008422 (U3CWaitToRepathU3Ec__Iterator15_t2392207237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AIFollow/<WaitToRepath>c__Iterator15::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitToRepathU3Ec__Iterator15_System_Collections_IEnumerator_get_Current_m1155927034 (U3CWaitToRepathU3Ec__Iterator15_t2392207237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AIFollow/<WaitToRepath>c__Iterator15::MoveNext()
extern "C"  bool U3CWaitToRepathU3Ec__Iterator15_MoveNext_m1458964326 (U3CWaitToRepathU3Ec__Iterator15_t2392207237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIFollow/<WaitToRepath>c__Iterator15::Dispose()
extern "C"  void U3CWaitToRepathU3Ec__Iterator15_Dispose_m2345809779 (U3CWaitToRepathU3Ec__Iterator15_t2392207237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIFollow/<WaitToRepath>c__Iterator15::Reset()
extern "C"  void U3CWaitToRepathU3Ec__Iterator15_Reset_m956238691 (U3CWaitToRepathU3Ec__Iterator15_t2392207237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
