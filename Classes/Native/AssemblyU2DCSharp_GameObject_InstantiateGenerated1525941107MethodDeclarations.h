﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameObject_InstantiateGenerated
struct GameObject_InstantiateGenerated_t1525941107;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void GameObject_InstantiateGenerated::.ctor()
extern "C"  void GameObject_InstantiateGenerated__ctor_m1186604376 (GameObject_InstantiateGenerated_t1525941107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameObject_InstantiateGenerated::GameObject_Instantiate_Instantiate__GameObject__Vector3__Quaternion(JSVCall,System.Int32)
extern "C"  bool GameObject_InstantiateGenerated_GameObject_Instantiate_Instantiate__GameObject__Vector3__Quaternion_m2823658032 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameObject_InstantiateGenerated::GameObject_Instantiate_Instantiate__GameObject(JSVCall,System.Int32)
extern "C"  bool GameObject_InstantiateGenerated_GameObject_Instantiate_Instantiate__GameObject_m3623538496 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameObject_InstantiateGenerated::__Register()
extern "C"  void GameObject_InstantiateGenerated___Register_m4238405231 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 GameObject_InstantiateGenerated::ilo_getVector3S1(System.Int32)
extern "C"  Vector3_t4282066566  GameObject_InstantiateGenerated_ilo_getVector3S1_m1923590190 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject GameObject_InstantiateGenerated::ilo_Instantiate2(UnityEngine.GameObject)
extern "C"  GameObject_t3674682005 * GameObject_InstantiateGenerated_ilo_Instantiate2_m432416522 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
