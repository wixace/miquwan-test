﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_RemoveAll_GetDelegate_member34_arg0>c__AnonStorey93`1<System.Object>
struct U3CListA1_RemoveAll_GetDelegate_member34_arg0U3Ec__AnonStorey93_1_t3917406903;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_RemoveAll_GetDelegate_member34_arg0>c__AnonStorey93`1<System.Object>::.ctor()
extern "C"  void U3CListA1_RemoveAll_GetDelegate_member34_arg0U3Ec__AnonStorey93_1__ctor_m4139072102_gshared (U3CListA1_RemoveAll_GetDelegate_member34_arg0U3Ec__AnonStorey93_1_t3917406903 * __this, const MethodInfo* method);
#define U3CListA1_RemoveAll_GetDelegate_member34_arg0U3Ec__AnonStorey93_1__ctor_m4139072102(__this, method) ((  void (*) (U3CListA1_RemoveAll_GetDelegate_member34_arg0U3Ec__AnonStorey93_1_t3917406903 *, const MethodInfo*))U3CListA1_RemoveAll_GetDelegate_member34_arg0U3Ec__AnonStorey93_1__ctor_m4139072102_gshared)(__this, method)
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_RemoveAll_GetDelegate_member34_arg0>c__AnonStorey93`1<System.Object>::<>m__B8(T)
extern "C"  bool U3CListA1_RemoveAll_GetDelegate_member34_arg0U3Ec__AnonStorey93_1_U3CU3Em__B8_m309149797_gshared (U3CListA1_RemoveAll_GetDelegate_member34_arg0U3Ec__AnonStorey93_1_t3917406903 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CListA1_RemoveAll_GetDelegate_member34_arg0U3Ec__AnonStorey93_1_U3CU3Em__B8_m309149797(__this, ___obj0, method) ((  bool (*) (U3CListA1_RemoveAll_GetDelegate_member34_arg0U3Ec__AnonStorey93_1_t3917406903 *, Il2CppObject *, const MethodInfo*))U3CListA1_RemoveAll_GetDelegate_member34_arg0U3Ec__AnonStorey93_1_U3CU3Em__B8_m309149797_gshared)(__this, ___obj0, method)
