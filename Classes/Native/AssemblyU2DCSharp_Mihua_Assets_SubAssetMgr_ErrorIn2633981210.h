﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mihua.Assets.SubAssetMgr/ErrorInfo
struct  ErrorInfo_t2633981210 
{
public:
	// System.String Mihua.Assets.SubAssetMgr/ErrorInfo::path
	String_t* ___path_0;
	// System.Int32 Mihua.Assets.SubAssetMgr/ErrorInfo::errCount
	int32_t ___errCount_1;
	// System.Single Mihua.Assets.SubAssetMgr/ErrorInfo::lastTime
	float ___lastTime_2;

public:
	inline static int32_t get_offset_of_path_0() { return static_cast<int32_t>(offsetof(ErrorInfo_t2633981210, ___path_0)); }
	inline String_t* get_path_0() const { return ___path_0; }
	inline String_t** get_address_of_path_0() { return &___path_0; }
	inline void set_path_0(String_t* value)
	{
		___path_0 = value;
		Il2CppCodeGenWriteBarrier(&___path_0, value);
	}

	inline static int32_t get_offset_of_errCount_1() { return static_cast<int32_t>(offsetof(ErrorInfo_t2633981210, ___errCount_1)); }
	inline int32_t get_errCount_1() const { return ___errCount_1; }
	inline int32_t* get_address_of_errCount_1() { return &___errCount_1; }
	inline void set_errCount_1(int32_t value)
	{
		___errCount_1 = value;
	}

	inline static int32_t get_offset_of_lastTime_2() { return static_cast<int32_t>(offsetof(ErrorInfo_t2633981210, ___lastTime_2)); }
	inline float get_lastTime_2() const { return ___lastTime_2; }
	inline float* get_address_of_lastTime_2() { return &___lastTime_2; }
	inline void set_lastTime_2(float value)
	{
		___lastTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Mihua.Assets.SubAssetMgr/ErrorInfo
struct ErrorInfo_t2633981210_marshaled_pinvoke
{
	char* ___path_0;
	int32_t ___errCount_1;
	float ___lastTime_2;
};
// Native definition for marshalling of: Mihua.Assets.SubAssetMgr/ErrorInfo
struct ErrorInfo_t2633981210_marshaled_com
{
	Il2CppChar* ___path_0;
	int32_t ___errCount_1;
	float ___lastTime_2;
};
