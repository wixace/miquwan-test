﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HeroNpc
struct HeroNpc_t2475966567;
// CSHeroUnit
struct CSHeroUnit_t3764358446;
// JSCLevelMonsterConfig
struct JSCLevelMonsterConfig_t1924079698;
// herosCfg
struct herosCfg_t3676934635;
// CSSkillData[]
struct CSSkillDataU5BU5D_t1071816810;
// CombatEntity
struct CombatEntity_t684137495;
// AICtrl
struct AICtrl_t1930422963;
// Monster
struct Monster_t2901270458;
// System.String
struct String_t;
// CombatAttPlus
struct CombatAttPlus_t649400871;
// NpcMgr
struct NpcMgr_t2339534743;
// CSCheckPointUnit
struct CSCheckPointUnit_t3129216924;
// GameMgr
struct GameMgr_t1469029542;
// CSGameDataMgr
struct CSGameDataMgr_t2623305516;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// FightCtrl
struct FightCtrl_t648967803;
// HeroMgr
struct HeroMgr_t2475965342;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// BuffCtrl
struct BuffCtrl_t2836564350;
// ReplayMgr
struct ReplayMgr_t1549183121;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.List`1<CSPlusAtt>
struct List_1_t341533415;
// CSCampfightData
struct CSCampfightData_t179152873;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CSHeroUnit3764358446.h"
#include "AssemblyU2DCSharp_JSCLevelMonsterConfig1924079698.h"
#include "AssemblyU2DCSharp_herosCfg3676934635.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Monster2901270458.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_HERO_COUNTRY1018279985.h"
#include "AssemblyU2DCSharp_HERO_TYPE820012127.h"
#include "AssemblyU2DCSharp_CombatAttPlus649400871.h"
#include "AssemblyU2DCSharp_HERO_ATT_KEY3680959548.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "AssemblyU2DCSharp_HeroNpc2475966567.h"
#include "AssemblyU2DCSharp_FightCtrl648967803.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_NpcMgr2339534743.h"
#include "AssemblyU2DCSharp_BuffCtrl2836564350.h"
#include "AssemblyU2DCSharp_CSCampfightData179152873.h"

// System.Void HeroNpc::.ctor()
extern "C"  void HeroNpc__ctor_m2330555108 (HeroNpc_t2475966567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::set_IsTrigger(System.Boolean)
extern "C"  void HeroNpc_set_IsTrigger_m3635675274 (HeroNpc_t2475966567 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroNpc::get_IsTrigger()
extern "C"  bool HeroNpc_get_IsTrigger_m1337520123 (HeroNpc_t2475966567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::Start()
extern "C"  void HeroNpc_Start_m1277692900 (HeroNpc_t2475966567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::Create(CSHeroUnit,JSCLevelMonsterConfig)
extern "C"  void HeroNpc_Create_m181344328 (HeroNpc_t2475966567 * __this, CSHeroUnit_t3764358446 * ___hcfg0, JSCLevelMonsterConfig_t1924079698 * ___lmcfg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::Create(herosCfg,JSCLevelMonsterConfig,CSSkillData[])
extern "C"  void HeroNpc_Create_m1347925322 (HeroNpc_t2475966567 * __this, herosCfg_t3676934635 * ___mcfg0, JSCLevelMonsterConfig_t1924079698 * ___lmcfg1, CSSkillDataU5BU5D_t1071816810* ___skillList2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::Init()
extern "C"  void HeroNpc_Init_m2243473136 (HeroNpc_t2475966567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::Addhurt(System.Single)
extern "C"  void HeroNpc_Addhurt_m2653576953 (HeroNpc_t2475966567 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::OnTriggerFun(System.Int32)
extern "C"  void HeroNpc_OnTriggerFun_m1970421143 (HeroNpc_t2475966567 * __this, int32_t ___effectId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::OnCheckTarget()
extern "C"  void HeroNpc_OnCheckTarget_m1705131484 (HeroNpc_t2475966567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::InitPos(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  void HeroNpc_InitPos_m3964931646 (HeroNpc_t2475966567 * __this, Vector3_t4282066566  ___pos0, Quaternion_t1553702882  ___rot1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::SearchAtkTarget()
extern "C"  void HeroNpc_SearchAtkTarget_m277122051 (HeroNpc_t2475966567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::Dead()
extern "C"  void HeroNpc_Dead_m2091761988 (HeroNpc_t2475966567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroNpc::BuffInHp()
extern "C"  float HeroNpc_BuffInHp_m2130362900 (HeroNpc_t2475966567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::GadOtherProp()
extern "C"  void HeroNpc_GadOtherProp_m1634339721 (HeroNpc_t2475966567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::CalcuFinalProp()
extern "C"  void HeroNpc_CalcuFinalProp_m256144889 (HeroNpc_t2475966567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::AssinPlusAtt()
extern "C"  void HeroNpc_AssinPlusAtt_m2094132993 (HeroNpc_t2475966567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_Start1(CombatEntity)
extern "C"  void HeroNpc_ilo_Start1_m1680519019 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AICtrl HeroNpc::ilo_get_aiCtrl2(Monster)
extern "C"  AICtrl_t1930422963 * HeroNpc_ilo_get_aiCtrl2_m3736393193 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 HeroNpc::ilo_get_lv3(CSHeroUnit)
extern "C"  uint32_t HeroNpc_ilo_get_lv3_m2386904892 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HeroNpc::ilo_get_name4(CSHeroUnit)
extern "C"  String_t* HeroNpc_ilo_get_name4_m3112248932 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_showname5(CombatEntity,System.String)
extern "C"  void HeroNpc_ilo_set_showname5_m1999140516 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HeroNpc::ilo_get_ResourcesName6(CSHeroUnit)
extern "C"  String_t* HeroNpc_ilo_get_ResourcesName6_m1150150225 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_icon7(CombatEntity,System.String)
extern "C"  void HeroNpc_ilo_set_icon7_m3729839189 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HeroNpc::ilo_get_HeadIcon8(CSHeroUnit)
extern "C"  String_t* HeroNpc_ilo_get_HeadIcon8_m1076685594 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HERO_COUNTRY HeroNpc::ilo_get_Country9(CSHeroUnit)
extern "C"  int32_t HeroNpc_ilo_get_Country9_m3003778021 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HeroNpc::ilo_get_storyID10(CSHeroUnit)
extern "C"  String_t* HeroNpc_ilo_get_storyID10_m3370282314 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_halfwidth11(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_halfwidth11_m3390153907 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_isInFinish12(Monster,System.Boolean)
extern "C"  void HeroNpc_ilo_set_isInFinish12_m902655800 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_atkRange13(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_atkRange13_m2369550789 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_maxRage14(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_maxRage14_m1644932948 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_heromajor15(CombatEntity,HERO_TYPE)
extern "C"  void HeroNpc_ilo_set_heromajor15_m1596656203 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_originSpeed16(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_originSpeed16_m2407410118 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HeroNpc::ilo_get_skills17(CombatEntity)
extern "C"  String_t* HeroNpc_ilo_get_skills17_m2517391210 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_skills18(CombatEntity,System.String)
extern "C"  void HeroNpc_ilo_set_skills18_m3508901726 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_awakenLv19(CSHeroUnit)
extern "C"  int32_t HeroNpc_ilo_get_awakenLv19_m2314441947 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_IntelligenceTip20(CombatEntity,System.Int32)
extern "C"  void HeroNpc_ilo_set_IntelligenceTip20_m2249363458 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// herosCfg HeroNpc::ilo_get_config21(CSHeroUnit)
extern "C"  herosCfg_t3676934635 * HeroNpc_ilo_get_config21_m2376694813 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_modelScale22(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_modelScale22_m3395205695 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_angerSecDecNum23(CombatEntity,System.Int32)
extern "C"  void HeroNpc_ilo_set_angerSecDecNum23_m3249034319 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 HeroNpc::ilo_GetEnemyAttr24(CombatAttPlus,HERO_ATT_KEY,System.UInt32)
extern "C"  uint32_t HeroNpc_ilo_GetEnemyAttr24_m270267760 (Il2CppObject * __this /* static, unused */, CombatAttPlus_t649400871 * ____this0, int32_t ___key1, uint32_t ___basicsAtt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 HeroNpc::ilo_get_phy_def25(CSHeroUnit)
extern "C"  uint32_t HeroNpc_ilo_get_phy_def25_m2192455325 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_PD26(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_PD26_m3781915222 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 HeroNpc::ilo_get_mag_def27(CSHeroUnit)
extern "C"  uint32_t HeroNpc_ilo_get_mag_def27_m1853990641 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_maxHp28(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_maxHp28_m689286834 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroNpc::ilo_get_maxHp29(CombatEntity)
extern "C"  float HeroNpc_ilo_get_maxHp29_m1825378446 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_hp30(Monster,System.Single)
extern "C"  void HeroNpc_ilo_set_hp30_m1362100264 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 HeroNpc::ilo_get_destroy31(CSHeroUnit)
extern "C"  uint32_t HeroNpc_ilo_get_destroy31_m2393756843 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 HeroNpc::ilo_get_block32(CSHeroUnit)
extern "C"  uint32_t HeroNpc_ilo_get_block32_m292115071 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_withstand33(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_withstand33_m2082065808 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 HeroNpc::ilo_get_crit34(CSHeroUnit)
extern "C"  uint32_t HeroNpc_ilo_get_crit34_m3100708750 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_crit35(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_crit35_m486789914 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_tenacity36(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_tenacity36_m300834512 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 HeroNpc::ilo_get_heal_bonus37(CSHeroUnit)
extern "C"  uint32_t HeroNpc_ilo_get_heal_bonus37_m2412049343 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 HeroNpc::ilo_get_damage_bonus38(CSHeroUnit)
extern "C"  uint32_t HeroNpc_ilo_get_damage_bonus38_m794951239 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_hurtP39(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_hurtP39_m4017446055 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 HeroNpc::ilo_get_damage_reduce40(CSHeroUnit)
extern "C"  uint32_t HeroNpc_ilo_get_damage_reduce40_m4188105477 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_hurtL41(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_hurtL41_m1375716154 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_notInvade42(CombatEntity,System.Int32)
extern "C"  void HeroNpc_ilo_set_notInvade42_m91745658 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_unyielding43(CombatEntity,System.Int32)
extern "C"  void HeroNpc_ilo_set_unyielding43_m1581731473 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 HeroNpc::ilo_get_dodge_per44(CSHeroUnit)
extern "C"  uint32_t HeroNpc_ilo_get_dodge_per44_m2197005128 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 HeroNpc::ilo_get_destory_per45(CSHeroUnit)
extern "C"  uint32_t HeroNpc_ilo_get_destory_per45_m1324339654 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 HeroNpc::ilo_get_block_per46(CSHeroUnit)
extern "C"  uint32_t HeroNpc_ilo_get_block_per46_m4128164736 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NpcMgr HeroNpc::ilo_get_instance47()
extern "C"  NpcMgr_t2339534743 * HeroNpc_ilo_get_instance47_m1485746766 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_critHurt48(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_critHurt48_m1173685163 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSCheckPointUnit HeroNpc::ilo_get_checkPoint49(GameMgr)
extern "C"  CSCheckPointUnit_t3129216924 * HeroNpc_ilo_get_checkPoint49_m3121196388 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_rage50(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_rage50_m1062369798 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_anger_initial51(CSHeroUnit)
extern "C"  int32_t HeroNpc_ilo_get_anger_initial51_m1767785710 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSGameDataMgr HeroNpc::ilo_get_CSGameDataMgr52()
extern "C"  CSGameDataMgr_t2623305516 * HeroNpc_ilo_get_CSGameDataMgr52_m1438368470 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSDatacfgManager HeroNpc::ilo_get_CfgDataMgr53()
extern "C"  CSDatacfgManager_t1565254243 * HeroNpc_ilo_get_CfgDataMgr53_m3614260618 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroNpc::ilo_get_PD54(CombatEntity)
extern "C"  float HeroNpc_ilo_get_PD54_m2862720652 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_MD55(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_MD55_m563138837 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_residual_mp56(CSHeroUnit)
extern "C"  int32_t HeroNpc_ilo_get_residual_mp56_m3989827012 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_residual_hpPer57(CSHeroUnit)
extern "C"  int32_t HeroNpc_ilo_get_residual_hpPer57_m1521101213 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroNpc::ilo_get_hp58(Monster)
extern "C"  float HeroNpc_ilo_get_hp58_m893963453 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroNpc::ilo_get_x59(JSCLevelMonsterConfig)
extern "C"  float HeroNpc_ilo_get_x59_m1294224658 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroNpc::ilo_get_rot60(JSCLevelMonsterConfig)
extern "C"  float HeroNpc_ilo_get_rot60_m1712600061 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HeroNpc::ilo_get_startPos61(CombatEntity)
extern "C"  Vector3_t4282066566  HeroNpc_ilo_get_startPos61_m2482288152 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_Init62(HeroNpc)
extern "C"  void HeroNpc_ilo_Init62_m2180194668 (Il2CppObject * __this /* static, unused */, HeroNpc_t2475966567 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightCtrl HeroNpc::ilo_get_fightCtrl63(CombatEntity)
extern "C"  FightCtrl_t648967803 * HeroNpc_ilo_get_fightCtrl63_m1174063231 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_ClearmLastUseTime64(FightCtrl)
extern "C"  void HeroNpc_ilo_ClearmLastUseTime64_m1014020140 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr HeroNpc::ilo_get_instance65()
extern "C"  HeroMgr_t2475965342 * HeroNpc_ilo_get_instance65_m2228771323 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_attack66(CSHeroUnit,System.UInt32)
extern "C"  void HeroNpc_ilo_set_attack66_m1319092158 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_dodge67(CSHeroUnit,System.UInt32)
extern "C"  void HeroNpc_ilo_set_dodge67_m4285994832 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_damage_bonus68(CSHeroUnit,System.UInt32)
extern "C"  void HeroNpc_ilo_set_damage_bonus68_m1458016903 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_stubborn69(CSHeroUnit,System.UInt32)
extern "C"  void HeroNpc_ilo_set_stubborn69_m3290080368 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_dodge_per70(CSHeroUnit,System.UInt32)
extern "C"  void HeroNpc_ilo_set_dodge_per70_m3969944774 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_block_per71(CSHeroUnit,System.UInt32)
extern "C"  void HeroNpc_ilo_set_block_per71_m197386813 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_anger_max72(CSHeroUnit,System.Int32)
extern "C"  void HeroNpc_ilo_set_anger_max72_m1148049260 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_Create73(HeroNpc,CSHeroUnit,JSCLevelMonsterConfig)
extern "C"  void HeroNpc_ilo_Create73_m407256658 (Il2CppObject * __this /* static, unused */, HeroNpc_t2475966567 * ____this0, CSHeroUnit_t3764358446 * ___hcfg1, JSCLevelMonsterConfig_t1924079698 * ___lmcfg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_bvrCtrl74(CombatEntity,Entity.Behavior.IBehaviorCtrl)
extern "C"  void HeroNpc_ilo_set_bvrCtrl74_m371050104 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, IBehaviorCtrl_t4225040900 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl HeroNpc::ilo_get_bvrCtrl75(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * HeroNpc_ilo_get_bvrCtrl75_m226917204 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_TranBehavior76(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void HeroNpc_ilo_TranBehavior76_m2194789285 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___id1, ObjectU5BU5D_t1108656482* ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_IsCheckTarget77(CombatEntity,System.Boolean)
extern "C"  void HeroNpc_ilo_set_IsCheckTarget77_m2980320275 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_startRotation78(CombatEntity,UnityEngine.Quaternion)
extern "C"  void HeroNpc_ilo_set_startRotation78_m2280653045 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, Quaternion_t1553702882  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_ChangeHeroToDead79(NpcMgr,System.Int32)
extern "C"  void HeroNpc_ilo_ChangeHeroToDead79_m483478032 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_Dead80(Monster)
extern "C"  void HeroNpc_ilo_Dead80_m867974121 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroNpc::ilo_get_atkPower81(CombatEntity)
extern "C"  float HeroNpc_ilo_get_atkPower81_m4213560191 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroNpc::ilo_get_isDeath82(CombatEntity)
extern "C"  bool HeroNpc_ilo_get_isDeath82_m2646244231 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 HeroNpc::ilo_get_attack83(CSHeroUnit)
extern "C"  uint32_t HeroNpc_ilo_get_attack83_m366945558 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mBuffATNum84(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mBuffATNum84_m2383824927 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BuffCtrl HeroNpc::ilo_get_buffCtrl85(CombatEntity)
extern "C"  BuffCtrl_t2836564350 * HeroNpc_ilo_get_buffCtrl85_m4025647553 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mAtkPerHp86(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mAtkPerHp86_m915054990 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroNpc::ilo_BuffInHp87(HeroNpc)
extern "C"  float HeroNpc_ilo_BuffInHp87_m4073876837 (Il2CppObject * __this /* static, unused */, HeroNpc_t2475966567 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_atkPower88(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_atkPower88_m1452745579 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mBuffPD89(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mBuffPD89_m865315083 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 HeroNpc::ilo_get_hp90(CSHeroUnit)
extern "C"  uint32_t HeroNpc_ilo_get_hp90_m1894091250 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mBuffMaxHPNum91(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mBuffMaxHPNum91_m2040329672 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mBuffMaxHP92(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mBuffMaxHP92_m2911401333 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_GadOtherProp93(HeroNpc)
extern "C"  void HeroNpc_ilo_GadOtherProp93_m3261345109 (Il2CppObject * __this /* static, unused */, HeroNpc_t2475966567 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 HeroNpc::ilo_get_hitrate94(CSHeroUnit)
extern "C"  uint32_t HeroNpc_ilo_get_hitrate94_m3259545889 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mBuffHitsNum95(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mBuffHitsNum95_m964322866 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_hits96(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_hits96_m3566398523 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_wreck97(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_wreck97_m3046760016 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mBuffWithstandNum98(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mBuffWithstandNum98_m1178106283 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mBuffCrit99(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mBuffCrit99_m2889474224 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 HeroNpc::ilo_get_anticrit100(CSHeroUnit)
extern "C"  uint32_t HeroNpc_ilo_get_anticrit100_m2732120456 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_HPRstNum101(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_HPRstNum101_m2551108759 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_HPRstPer102(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_HPRstPer102_m118735009 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroNpc::ilo_get_mHPSecDecNum103(BuffCtrl)
extern "C"  float HeroNpc_ilo_get_mHPSecDecNum103_m2527279048 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroNpc::ilo_get_mHPSecDecNumATK104(BuffCtrl)
extern "C"  float HeroNpc_ilo_get_mHPSecDecNumATK104_m436906575 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_HPSecDecNum105(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_HPSecDecNum105_m1590300679 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroNpc::ilo_get_mHPSecDecPer106(BuffCtrl)
extern "C"  float HeroNpc_ilo_get_mHPSecDecPer106_m3826472916 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroNpc::ilo_get_mAttackSpeed107(BuffCtrl)
extern "C"  float HeroNpc_ilo_get_mAttackSpeed107_m3941161210 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroNpc::ilo_get_atkSpeed108(CombatEntity)
extern "C"  float HeroNpc_ilo_get_atkSpeed108_m2939581957 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReplayMgr HeroNpc::ilo_get_Instance109()
extern "C"  ReplayMgr_t1549183121 * HeroNpc_ilo_get_Instance109_m208081837 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_atkSpeed110(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_atkSpeed110_m1592263681 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroNpc::ilo_get_runSpeed111(CombatEntity)
extern "C"  float HeroNpc_ilo_get_runSpeed111_m3180585936 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroNpc::ilo_get_modelScale112(CombatEntity)
extern "C"  float HeroNpc_ilo_get_modelScale112_m1299741004 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroNpc::ilo_get_mHurtEffectPlus113(BuffCtrl)
extern "C"  float HeroNpc_ilo_get_mHurtEffectPlus113_m1503345822 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_restraintCountryType114(CombatEntity,System.Int32)
extern "C"  void HeroNpc_ilo_set_restraintCountryType114_m3488446508 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroNpc::ilo_get_mRestrintPer115(BuffCtrl)
extern "C"  float HeroNpc_ilo_get_mRestrintPer115_m1664321930 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mRestrintStateType116(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mRestrintStateType116_m3914100415 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_restraintStatePer117(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_restraintStatePer117_m4128289981 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_NDPAddBuff118(CombatEntity,System.Int32)
extern "C"  void HeroNpc_ilo_set_NDPAddBuff118_m40396670 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mBeAttackedLaterEffectID119(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mBeAttackedLaterEffectID119_m3355715310 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_DeathLaterEffectID120(CombatEntity,System.Int32)
extern "C"  void HeroNpc_ilo_set_DeathLaterEffectID120_m3936268753 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mDeathLaterSpeed121(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mDeathLaterSpeed121_m349230116 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mAnitNum122(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mAnitNum122_m1842739622 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_curAntiNum123(CombatEntity,System.Int32)
extern "C"  void HeroNpc_ilo_set_curAntiNum123_m2892934702 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_AntiEndEffectID124(CombatEntity,System.Int32)
extern "C"  void HeroNpc_ilo_set_AntiEndEffectID124_m1803899504 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mAuraAddHPRadius125(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mAuraAddHPRadius125_m385420927 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_AuraAddHPPer126(CombatEntity,System.Int32)
extern "C"  void HeroNpc_ilo_set_AuraAddHPPer126_m576501886 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mAuraReduceHPRadius127(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mAuraReduceHPRadius127_m3412702838 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mAuraReduceHPNum128(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mAuraReduceHPNum128_m4066705797 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mActiveSkillDamageAddPer129(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mActiveSkillDamageAddPer129_m1922455717 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_ActiveSkillDamageAddPer130(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_ActiveSkillDamageAddPer130_m942115062 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mAddAngerRatio131(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mAddAngerRatio131_m1916275757 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_AddAngerRatio132(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_AddAngerRatio132_m2681688745 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> HeroNpc::ilo_get_mAddDamageValue133(BuffCtrl)
extern "C"  List_1_t2522024052 * HeroNpc_ilo_get_mAddDamageValue133_m2228491775 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_hpLineNum134(CombatEntity,System.Int32)
extern "C"  void HeroNpc_ilo_set_hpLineNum134_m2298817078 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mHpLineBuffID135(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mHpLineBuffID135_m4071305260 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mHpLineUpBuffID136(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mHpLineUpBuffID136_m4272858962 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_hpLineUpBuffID137(CombatEntity,System.Int32)
extern "C"  void HeroNpc_ilo_set_hpLineUpBuffID137_m283025132 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_controlBuffTimeBonus138(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_controlBuffTimeBonus138_m448351252 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_controlBuffTimeBonusPer139(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_controlBuffTimeBonusPer139_m4273469050 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mRuneTakeEffectNum140(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mRuneTakeEffectNum140_m1217819804 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_runeTakeEffectBuffID141(CombatEntity,System.Int32)
extern "C"  void HeroNpc_ilo_set_runeTakeEffectBuffID141_m1438044526 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mRuneTakeEffectBuffID01142(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mRuneTakeEffectBuffID01142_m3162935921 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_godDownTweenTime143(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_godDownTweenTime143_m3605149782 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mAntiDebuffDiscreteID144(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mAntiDebuffDiscreteID144_m2741015274 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_antiDebuffDiscreteID145(CombatEntity,System.Int32)
extern "C"  void HeroNpc_ilo_set_antiDebuffDiscreteID145_m314279700 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 HeroNpc::ilo_get_firm146(CSHeroUnit)
extern "C"  uint32_t HeroNpc_ilo_get_firm146_m4232136904 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 HeroNpc::ilo_get_stubborn147(CSHeroUnit)
extern "C"  uint32_t HeroNpc_ilo_get_stubborn147_m920518512 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mUnyielding148(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mUnyielding148_m104803946 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mWhenCritAddBuffID149(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mWhenCritAddBuffID149_m3551422526 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_thumpSkillID150(CombatEntity,System.Int32)
extern "C"  void HeroNpc_ilo_set_thumpSkillID150_m2340097304 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_thumpAddValue151(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_thumpAddValue151_m1796176855 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_tankBuffID152(CombatEntity,System.Int32)
extern "C"  void HeroNpc_ilo_set_tankBuffID152_m3220674854 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_cleaveSkillID153(CombatEntity,System.Int32)
extern "C"  void HeroNpc_ilo_set_cleaveSkillID153_m3815244391 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mCleaveBuffID154(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mCleaveBuffID154_m1470355985 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mBuffCritPer155(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mBuffCritPer155_m695523972 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mReduceRageHpNum156(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mReduceRageHpNum156_m2162671338 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_deathHarvestLine157(CombatEntity,System.Int32)
extern "C"  void HeroNpc_ilo_set_deathHarvestLine157_m42025138 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mResistanceCountryType158(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mResistanceCountryType158_m2526686528 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroNpc::ilo_get_mResistanceCountryPer159(BuffCtrl)
extern "C"  int32_t HeroNpc_ilo_get_mResistanceCountryPer159_m4032816088 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_set_resistanceCountryPer160(CombatEntity,System.Single)
extern "C"  void HeroNpc_ilo_set_resistanceCountryPer160_m3467597899 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPlusAtt> HeroNpc::ilo_GetEnemyAttPlus_fightfor161(CSCampfightData)
extern "C"  List_1_t341533415 * HeroNpc_ilo_GetEnemyAttPlus_fightfor161_m573894437 (Il2CppObject * __this /* static, unused */, CSCampfightData_t179152873 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroNpc::ilo_AddPlusAttribute162(CombatAttPlus,System.String,System.UInt32,System.UInt32)
extern "C"  void HeroNpc_ilo_AddPlusAttribute162_m2161506676 (Il2CppObject * __this /* static, unused */, CombatAttPlus_t649400871 * ____this0, String_t* ___key1, uint32_t ___value2, uint32_t ___valuePer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
