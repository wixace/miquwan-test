﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3771233898;
// UIModelDisplayMgr
struct UIModelDisplayMgr_t1446490315;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIModelDisplayMgr/<CreatHeroModel>c__AnonStorey160
struct  U3CCreatHeroModelU3Ec__AnonStorey160_t3837456612  : public Il2CppObject
{
public:
	// UIModelDisplayType UIModelDisplayMgr/<CreatHeroModel>c__AnonStorey160::type
	int32_t ___type_0;
	// System.Int32 UIModelDisplayMgr/<CreatHeroModel>c__AnonStorey160::id
	int32_t ___id_1;
	// System.Action UIModelDisplayMgr/<CreatHeroModel>c__AnonStorey160::action
	Action_t3771233898 * ___action_2;
	// UIModelDisplayMgr UIModelDisplayMgr/<CreatHeroModel>c__AnonStorey160::<>f__this
	UIModelDisplayMgr_t1446490315 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CCreatHeroModelU3Ec__AnonStorey160_t3837456612, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(U3CCreatHeroModelU3Ec__AnonStorey160_t3837456612, ___id_1)); }
	inline int32_t get_id_1() const { return ___id_1; }
	inline int32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int32_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(U3CCreatHeroModelU3Ec__AnonStorey160_t3837456612, ___action_2)); }
	inline Action_t3771233898 * get_action_2() const { return ___action_2; }
	inline Action_t3771233898 ** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(Action_t3771233898 * value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier(&___action_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CCreatHeroModelU3Ec__AnonStorey160_t3837456612, ___U3CU3Ef__this_3)); }
	inline UIModelDisplayMgr_t1446490315 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline UIModelDisplayMgr_t1446490315 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(UIModelDisplayMgr_t1446490315 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
