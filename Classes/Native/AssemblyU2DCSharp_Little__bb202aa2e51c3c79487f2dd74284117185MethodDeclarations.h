﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._bb202aa2e51c3c79487f2dd7cdb40803
struct _bb202aa2e51c3c79487f2dd7cdb40803_t4284117185;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._bb202aa2e51c3c79487f2dd7cdb40803::.ctor()
extern "C"  void _bb202aa2e51c3c79487f2dd7cdb40803__ctor_m2927951500 (_bb202aa2e51c3c79487f2dd7cdb40803_t4284117185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._bb202aa2e51c3c79487f2dd7cdb40803::_bb202aa2e51c3c79487f2dd7cdb40803m2(System.Int32)
extern "C"  int32_t _bb202aa2e51c3c79487f2dd7cdb40803__bb202aa2e51c3c79487f2dd7cdb40803m2_m256233977 (_bb202aa2e51c3c79487f2dd7cdb40803_t4284117185 * __this, int32_t ____bb202aa2e51c3c79487f2dd7cdb40803a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._bb202aa2e51c3c79487f2dd7cdb40803::_bb202aa2e51c3c79487f2dd7cdb40803m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _bb202aa2e51c3c79487f2dd7cdb40803__bb202aa2e51c3c79487f2dd7cdb40803m_m3046506717 (_bb202aa2e51c3c79487f2dd7cdb40803_t4284117185 * __this, int32_t ____bb202aa2e51c3c79487f2dd7cdb40803a0, int32_t ____bb202aa2e51c3c79487f2dd7cdb40803381, int32_t ____bb202aa2e51c3c79487f2dd7cdb40803c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
