﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._26902b1caef146e088d6e9b526922abe
struct _26902b1caef146e088d6e9b526922abe_t4158580794;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._26902b1caef146e088d6e9b526922abe::.ctor()
extern "C"  void _26902b1caef146e088d6e9b526922abe__ctor_m2764889907 (_26902b1caef146e088d6e9b526922abe_t4158580794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._26902b1caef146e088d6e9b526922abe::_26902b1caef146e088d6e9b526922abem2(System.Int32)
extern "C"  int32_t _26902b1caef146e088d6e9b526922abe__26902b1caef146e088d6e9b526922abem2_m2456115033 (_26902b1caef146e088d6e9b526922abe_t4158580794 * __this, int32_t ____26902b1caef146e088d6e9b526922abea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._26902b1caef146e088d6e9b526922abe::_26902b1caef146e088d6e9b526922abem(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _26902b1caef146e088d6e9b526922abe__26902b1caef146e088d6e9b526922abem_m1023756157 (_26902b1caef146e088d6e9b526922abe_t4158580794 * __this, int32_t ____26902b1caef146e088d6e9b526922abea0, int32_t ____26902b1caef146e088d6e9b526922abe601, int32_t ____26902b1caef146e088d6e9b526922abec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
