﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIButtonColorGenerated
struct UIButtonColorGenerated_t2842610162;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// UIButtonColor
struct UIButtonColor_t1546108957;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_UIButtonColor_State1650987007.h"
#include "AssemblyU2DCSharp_UIButtonColor1546108957.h"

// System.Void UIButtonColorGenerated::.ctor()
extern "C"  void UIButtonColorGenerated__ctor_m1217166057 (UIButtonColorGenerated_t2842610162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonColorGenerated::UIButtonColor_UIButtonColor1(JSVCall,System.Int32)
extern "C"  bool UIButtonColorGenerated_UIButtonColor_UIButtonColor1_m3582580597 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColorGenerated::UIButtonColor_tweenTarget(JSVCall)
extern "C"  void UIButtonColorGenerated_UIButtonColor_tweenTarget_m792252710 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColorGenerated::UIButtonColor_hover(JSVCall)
extern "C"  void UIButtonColorGenerated_UIButtonColor_hover_m1787046086 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColorGenerated::UIButtonColor_pressed(JSVCall)
extern "C"  void UIButtonColorGenerated_UIButtonColor_pressed_m3298810272 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColorGenerated::UIButtonColor_disabledColor(JSVCall)
extern "C"  void UIButtonColorGenerated_UIButtonColor_disabledColor_m3484549147 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColorGenerated::UIButtonColor_duration(JSVCall)
extern "C"  void UIButtonColorGenerated_UIButtonColor_duration_m3253031926 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColorGenerated::UIButtonColor_state(JSVCall)
extern "C"  void UIButtonColorGenerated_UIButtonColor_state_m2008292625 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColorGenerated::UIButtonColor_defaultColor(JSVCall)
extern "C"  void UIButtonColorGenerated_UIButtonColor_defaultColor_m1962526120 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColorGenerated::UIButtonColor_isEnabled(JSVCall)
extern "C"  void UIButtonColorGenerated_UIButtonColor_isEnabled_m1726200747 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonColorGenerated::UIButtonColor_ResetDefaultColor(JSVCall,System.Int32)
extern "C"  bool UIButtonColorGenerated_UIButtonColor_ResetDefaultColor_m4251747090 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonColorGenerated::UIButtonColor_SetState__State__Boolean(JSVCall,System.Int32)
extern "C"  bool UIButtonColorGenerated_UIButtonColor_SetState__State__Boolean_m3606076519 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonColorGenerated::UIButtonColor_UpdateColor__Boolean(JSVCall,System.Int32)
extern "C"  bool UIButtonColorGenerated_UIButtonColor_UpdateColor__Boolean_m156965295 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColorGenerated::__Register()
extern "C"  void UIButtonColorGenerated___Register_m1365928830 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIButtonColorGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UIButtonColorGenerated_ilo_getObject1_m3654540617 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonColorGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UIButtonColorGenerated_ilo_attachFinalizerObject2_m3899642199 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColorGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UIButtonColorGenerated_ilo_addJSCSRel3_m1900436007 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIButtonColorGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIButtonColorGenerated_ilo_setObject4_m2032174980 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIButtonColorGenerated::ilo_getSingle5(System.Int32)
extern "C"  float UIButtonColorGenerated_ilo_getSingle5_m1434913250 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIButtonColor/State UIButtonColorGenerated::ilo_get_state6(UIButtonColor)
extern "C"  int32_t UIButtonColorGenerated_ilo_get_state6_m3088117029 (Il2CppObject * __this /* static, unused */, UIButtonColor_t1546108957 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIButtonColorGenerated::ilo_getObject7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIButtonColorGenerated_ilo_getObject7_m2597712684 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColorGenerated::ilo_ResetDefaultColor8(UIButtonColor)
extern "C"  void UIButtonColorGenerated_ilo_ResetDefaultColor8_m1397420512 (Il2CppObject * __this /* static, unused */, UIButtonColor_t1546108957 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIButtonColorGenerated::ilo_getEnum9(System.Int32)
extern "C"  int32_t UIButtonColorGenerated_ilo_getEnum9_m3362113167 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonColorGenerated::ilo_getBooleanS10(System.Int32)
extern "C"  bool UIButtonColorGenerated_ilo_getBooleanS10_m1996766001 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColorGenerated::ilo_SetState11(UIButtonColor,UIButtonColor/State,System.Boolean)
extern "C"  void UIButtonColorGenerated_ilo_SetState11_m1729358424 (Il2CppObject * __this /* static, unused */, UIButtonColor_t1546108957 * ____this0, int32_t ___state1, bool ___instant2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColorGenerated::ilo_UpdateColor12(UIButtonColor,System.Boolean)
extern "C"  void UIButtonColorGenerated_ilo_UpdateColor12_m610847383 (Il2CppObject * __this /* static, unused */, UIButtonColor_t1546108957 * ____this0, bool ___instant1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
