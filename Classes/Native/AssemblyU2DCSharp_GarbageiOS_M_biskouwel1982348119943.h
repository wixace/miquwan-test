﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_biskouwel198
struct  M_biskouwel198_t2348119943  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_biskouwel198::_homasir
	bool ____homasir_0;
	// System.String GarbageiOS.M_biskouwel198::_reqaryoo
	String_t* ____reqaryoo_1;
	// System.Boolean GarbageiOS.M_biskouwel198::_tallyeeGolorho
	bool ____tallyeeGolorho_2;
	// System.Single GarbageiOS.M_biskouwel198::_cabu
	float ____cabu_3;
	// System.UInt32 GarbageiOS.M_biskouwel198::_ballporJeezas
	uint32_t ____ballporJeezas_4;
	// System.Boolean GarbageiOS.M_biskouwel198::_seebaKelpem
	bool ____seebaKelpem_5;
	// System.String GarbageiOS.M_biskouwel198::_mora
	String_t* ____mora_6;
	// System.UInt32 GarbageiOS.M_biskouwel198::_kecalSturle
	uint32_t ____kecalSturle_7;

public:
	inline static int32_t get_offset_of__homasir_0() { return static_cast<int32_t>(offsetof(M_biskouwel198_t2348119943, ____homasir_0)); }
	inline bool get__homasir_0() const { return ____homasir_0; }
	inline bool* get_address_of__homasir_0() { return &____homasir_0; }
	inline void set__homasir_0(bool value)
	{
		____homasir_0 = value;
	}

	inline static int32_t get_offset_of__reqaryoo_1() { return static_cast<int32_t>(offsetof(M_biskouwel198_t2348119943, ____reqaryoo_1)); }
	inline String_t* get__reqaryoo_1() const { return ____reqaryoo_1; }
	inline String_t** get_address_of__reqaryoo_1() { return &____reqaryoo_1; }
	inline void set__reqaryoo_1(String_t* value)
	{
		____reqaryoo_1 = value;
		Il2CppCodeGenWriteBarrier(&____reqaryoo_1, value);
	}

	inline static int32_t get_offset_of__tallyeeGolorho_2() { return static_cast<int32_t>(offsetof(M_biskouwel198_t2348119943, ____tallyeeGolorho_2)); }
	inline bool get__tallyeeGolorho_2() const { return ____tallyeeGolorho_2; }
	inline bool* get_address_of__tallyeeGolorho_2() { return &____tallyeeGolorho_2; }
	inline void set__tallyeeGolorho_2(bool value)
	{
		____tallyeeGolorho_2 = value;
	}

	inline static int32_t get_offset_of__cabu_3() { return static_cast<int32_t>(offsetof(M_biskouwel198_t2348119943, ____cabu_3)); }
	inline float get__cabu_3() const { return ____cabu_3; }
	inline float* get_address_of__cabu_3() { return &____cabu_3; }
	inline void set__cabu_3(float value)
	{
		____cabu_3 = value;
	}

	inline static int32_t get_offset_of__ballporJeezas_4() { return static_cast<int32_t>(offsetof(M_biskouwel198_t2348119943, ____ballporJeezas_4)); }
	inline uint32_t get__ballporJeezas_4() const { return ____ballporJeezas_4; }
	inline uint32_t* get_address_of__ballporJeezas_4() { return &____ballporJeezas_4; }
	inline void set__ballporJeezas_4(uint32_t value)
	{
		____ballporJeezas_4 = value;
	}

	inline static int32_t get_offset_of__seebaKelpem_5() { return static_cast<int32_t>(offsetof(M_biskouwel198_t2348119943, ____seebaKelpem_5)); }
	inline bool get__seebaKelpem_5() const { return ____seebaKelpem_5; }
	inline bool* get_address_of__seebaKelpem_5() { return &____seebaKelpem_5; }
	inline void set__seebaKelpem_5(bool value)
	{
		____seebaKelpem_5 = value;
	}

	inline static int32_t get_offset_of__mora_6() { return static_cast<int32_t>(offsetof(M_biskouwel198_t2348119943, ____mora_6)); }
	inline String_t* get__mora_6() const { return ____mora_6; }
	inline String_t** get_address_of__mora_6() { return &____mora_6; }
	inline void set__mora_6(String_t* value)
	{
		____mora_6 = value;
		Il2CppCodeGenWriteBarrier(&____mora_6, value);
	}

	inline static int32_t get_offset_of__kecalSturle_7() { return static_cast<int32_t>(offsetof(M_biskouwel198_t2348119943, ____kecalSturle_7)); }
	inline uint32_t get__kecalSturle_7() const { return ____kecalSturle_7; }
	inline uint32_t* get_address_of__kecalSturle_7() { return &____kecalSturle_7; }
	inline void set__kecalSturle_7(uint32_t value)
	{
		____kecalSturle_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
