﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_LODGenerated
struct UnityEngine_LODGenerated_t2903105718;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t440051646;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_LODGenerated::.ctor()
extern "C"  void UnityEngine_LODGenerated__ctor_m1569331877 (UnityEngine_LODGenerated_t2903105718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LODGenerated::.cctor()
extern "C"  void UnityEngine_LODGenerated__cctor_m922551720 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LODGenerated::LOD_LOD1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LODGenerated_LOD_LOD1_m981284785 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LODGenerated::LOD_LOD2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LODGenerated_LOD_LOD2_m2226049266 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LODGenerated::LOD_screenRelativeTransitionHeight(JSVCall)
extern "C"  void UnityEngine_LODGenerated_LOD_screenRelativeTransitionHeight_m2831768694 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LODGenerated::LOD_fadeTransitionWidth(JSVCall)
extern "C"  void UnityEngine_LODGenerated_LOD_fadeTransitionWidth_m1419643277 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LODGenerated::LOD_renderers(JSVCall)
extern "C"  void UnityEngine_LODGenerated_LOD_renderers_m3285091890 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LODGenerated::__Register()
extern "C"  void UnityEngine_LODGenerated___Register_m1709654338 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Renderer[] UnityEngine_LODGenerated::<LOD_LOD2>m__25A()
extern "C"  RendererU5BU5D_t440051646* UnityEngine_LODGenerated_U3CLOD_LOD2U3Em__25A_m2544954308 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Renderer[] UnityEngine_LODGenerated::<LOD_renderers>m__25B()
extern "C"  RendererU5BU5D_t440051646* UnityEngine_LODGenerated_U3CLOD_renderersU3Em__25B_m3546088796 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LODGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_LODGenerated_ilo_attachFinalizerObject1_m4227977690 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LODGenerated::ilo_getObject2(System.Int32)
extern "C"  int32_t UnityEngine_LODGenerated_ilo_getObject2_m3294005710 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_LODGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_LODGenerated_ilo_getSingle3_m2393407524 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LODGenerated::ilo_addJSCSRel4(System.Int32,System.Object)
extern "C"  void UnityEngine_LODGenerated_ilo_addJSCSRel4_m1622126692 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LODGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void UnityEngine_LODGenerated_ilo_setSingle5_m613787059 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LODGenerated::ilo_setObject6(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_LODGenerated_ilo_setObject6_m2759984714 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LODGenerated::ilo_changeJSObj7(System.Int32,System.Object)
extern "C"  void UnityEngine_LODGenerated_ilo_changeJSObj7_m3603405034 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LODGenerated::ilo_getArrayLength8(System.Int32)
extern "C"  int32_t UnityEngine_LODGenerated_ilo_getArrayLength8_m696115906 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LODGenerated::ilo_getElement9(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_LODGenerated_ilo_getElement9_m2015868945 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
