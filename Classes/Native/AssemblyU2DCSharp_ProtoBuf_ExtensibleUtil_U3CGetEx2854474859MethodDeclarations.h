﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2F
struct U3CGetExtendedValuesU3Ec__Iterator2F_t2854474859;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;

#include "codegen/il2cpp-codegen.h"

// System.Void ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2F::.ctor()
extern "C"  void U3CGetExtendedValuesU3Ec__Iterator2F__ctor_m964698768 (U3CGetExtendedValuesU3Ec__Iterator2F_t2854474859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2F::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetExtendedValuesU3Ec__Iterator2F_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4040237644 (U3CGetExtendedValuesU3Ec__Iterator2F_t2854474859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetExtendedValuesU3Ec__Iterator2F_System_Collections_IEnumerator_get_Current_m3981000160 (U3CGetExtendedValuesU3Ec__Iterator2F_t2854474859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2F::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetExtendedValuesU3Ec__Iterator2F_System_Collections_IEnumerable_GetEnumerator_m1053791329 (U3CGetExtendedValuesU3Ec__Iterator2F_t2854474859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Object> ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2F::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern "C"  Il2CppObject* U3CGetExtendedValuesU3Ec__Iterator2F_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m2240456363 (U3CGetExtendedValuesU3Ec__Iterator2F_t2854474859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2F::MoveNext()
extern "C"  bool U3CGetExtendedValuesU3Ec__Iterator2F_MoveNext_m924491084 (U3CGetExtendedValuesU3Ec__Iterator2F_t2854474859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2F::Dispose()
extern "C"  void U3CGetExtendedValuesU3Ec__Iterator2F_Dispose_m3555830477 (U3CGetExtendedValuesU3Ec__Iterator2F_t2854474859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2F::Reset()
extern "C"  void U3CGetExtendedValuesU3Ec__Iterator2F_Reset_m2906099005 (U3CGetExtendedValuesU3Ec__Iterator2F_t2854474859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2F::<>__Finally0()
extern "C"  void U3CGetExtendedValuesU3Ec__Iterator2F_U3CU3E__Finally0_m3499460067 (U3CGetExtendedValuesU3Ec__Iterator2F_t2854474859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
