﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Poly2Tri.DTSweepBasin
struct DTSweepBasin_t2452859313;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Poly2Tri.DTSweepBasin::.ctor()
extern "C"  void DTSweepBasin__ctor_m328954478 (DTSweepBasin_t2452859313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
