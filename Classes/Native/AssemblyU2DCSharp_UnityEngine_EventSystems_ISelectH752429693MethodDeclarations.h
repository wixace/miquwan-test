﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_ISelectHandlerGenerated
struct UnityEngine_EventSystems_ISelectHandlerGenerated_t752429693;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_EventSystems_ISelectHandlerGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_ISelectHandlerGenerated__ctor_m1044977854 (UnityEngine_EventSystems_ISelectHandlerGenerated_t752429693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_ISelectHandlerGenerated::ISelectHandler_OnSelect__BaseEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_ISelectHandlerGenerated_ISelectHandler_OnSelect__BaseEventData_m2520790824 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_ISelectHandlerGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_ISelectHandlerGenerated___Register_m3850382025 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
