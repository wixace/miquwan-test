﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;
// System.Type
struct Type_t;
// JSCache/TypeInfo
struct TypeInfo_t3198813374;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t1297217088;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineManual
struct  UnityEngineManual_t3586848925  : public Il2CppObject
{
public:

public:
};

struct UnityEngineManual_t3586848925_StaticFields
{
public:
	// UnityEngine.GameObject UnityEngineManual::go
	GameObject_t3674682005 * ___go_0;
	// UnityEngine.GameObject UnityEngineManual::goFromComponent
	GameObject_t3674682005 * ___goFromComponent_1;
	// System.String UnityEngineManual::typeString
	String_t* ___typeString_2;
	// System.Type UnityEngineManual::type
	Type_t * ___type_3;
	// JSCache/TypeInfo UnityEngineManual::typeInfo
	TypeInfo_t3198813374 * ___typeInfo_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> UnityEngineManual::dict
	Dictionary_2_t1297217088 * ___dict_5;

public:
	inline static int32_t get_offset_of_go_0() { return static_cast<int32_t>(offsetof(UnityEngineManual_t3586848925_StaticFields, ___go_0)); }
	inline GameObject_t3674682005 * get_go_0() const { return ___go_0; }
	inline GameObject_t3674682005 ** get_address_of_go_0() { return &___go_0; }
	inline void set_go_0(GameObject_t3674682005 * value)
	{
		___go_0 = value;
		Il2CppCodeGenWriteBarrier(&___go_0, value);
	}

	inline static int32_t get_offset_of_goFromComponent_1() { return static_cast<int32_t>(offsetof(UnityEngineManual_t3586848925_StaticFields, ___goFromComponent_1)); }
	inline GameObject_t3674682005 * get_goFromComponent_1() const { return ___goFromComponent_1; }
	inline GameObject_t3674682005 ** get_address_of_goFromComponent_1() { return &___goFromComponent_1; }
	inline void set_goFromComponent_1(GameObject_t3674682005 * value)
	{
		___goFromComponent_1 = value;
		Il2CppCodeGenWriteBarrier(&___goFromComponent_1, value);
	}

	inline static int32_t get_offset_of_typeString_2() { return static_cast<int32_t>(offsetof(UnityEngineManual_t3586848925_StaticFields, ___typeString_2)); }
	inline String_t* get_typeString_2() const { return ___typeString_2; }
	inline String_t** get_address_of_typeString_2() { return &___typeString_2; }
	inline void set_typeString_2(String_t* value)
	{
		___typeString_2 = value;
		Il2CppCodeGenWriteBarrier(&___typeString_2, value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(UnityEngineManual_t3586848925_StaticFields, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier(&___type_3, value);
	}

	inline static int32_t get_offset_of_typeInfo_4() { return static_cast<int32_t>(offsetof(UnityEngineManual_t3586848925_StaticFields, ___typeInfo_4)); }
	inline TypeInfo_t3198813374 * get_typeInfo_4() const { return ___typeInfo_4; }
	inline TypeInfo_t3198813374 ** get_address_of_typeInfo_4() { return &___typeInfo_4; }
	inline void set_typeInfo_4(TypeInfo_t3198813374 * value)
	{
		___typeInfo_4 = value;
		Il2CppCodeGenWriteBarrier(&___typeInfo_4, value);
	}

	inline static int32_t get_offset_of_dict_5() { return static_cast<int32_t>(offsetof(UnityEngineManual_t3586848925_StaticFields, ___dict_5)); }
	inline Dictionary_2_t1297217088 * get_dict_5() const { return ___dict_5; }
	inline Dictionary_2_t1297217088 ** get_address_of_dict_5() { return &___dict_5; }
	inline void set_dict_5(Dictionary_2_t1297217088 * value)
	{
		___dict_5 = value;
		Il2CppCodeGenWriteBarrier(&___dict_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
