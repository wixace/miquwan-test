﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Pathfinding.Serialization.JsonFx.EcmaScriptIdentifier
struct EcmaScriptIdentifier_t4023709608;
// Pathfinding.Serialization.JsonFx.JsonWriter
struct JsonWriter_t541860733;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// Pathfinding.Serialization.JsonFx.JsonConverter
struct JsonConverter_t3109307074;
// System.Type
struct Type_t;
// Pathfinding.Serialization.JsonFx.JsonReader
struct JsonReader_t386455501;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t696267445;
// Pathfinding.Serialization.JsonFx.JsonDeserializationException
struct JsonDeserializationException_t3911578122;
// System.Exception
struct Exception_t3991598821;
// Pathfinding.Serialization.JsonFx.JsonMemberAttribute
struct JsonMemberAttribute_t1310195012;
// Pathfinding.Serialization.JsonFx.JsonNameAttribute
struct JsonNameAttribute_t1698983795;
// Pathfinding.Serialization.JsonFx.JsonOptInAttribute
struct JsonOptInAttribute_t3750829390;
// Pathfinding.Serialization.JsonFx.JsonReaderSettings
struct JsonReaderSettings_t3095433488;
// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>
struct Dictionary_2_t520966972;
// System.Collections.IEnumerable
struct IEnumerable_t3464557803;
// Pathfinding.Serialization.JsonFx.JsonSerializationException
struct JsonSerializationException_t1208944969;
// Pathfinding.Serialization.JsonFx.JsonSpecifiedPropertyAttribute
struct JsonSpecifiedPropertyAttribute_t1712644481;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// Pathfinding.Serialization.JsonFx.JsonTypeCoercionException
struct JsonTypeCoercionException_t3725515417;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// Pathfinding.Serialization.JsonFx.JsonWriterSettings
struct JsonWriterSettings_t3394579648;
// System.IO.TextWriter
struct TextWriter_t2304124208;
// System.Enum
struct Enum_t2862688501;
// System.Uri
struct Uri_t1116831938;
// System.Version
struct Version_t763695022;
// System.Collections.IDictionary
struct IDictionary_t537317817;
// System.Enum[]
struct EnumU5BU5D_t3205174168;
// Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime>
struct WriteDelegate_1_t4036178619;
// Pathfinding.Serialization.JsonFx.TypeCoercionUtility
struct TypeCoercionUtility_t3154211006;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>>
struct Dictionary_2_t626389457;
// System.Array
struct Il2CppArray;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "Pathfinding_JsonFx_U3CModuleU3E86524790.h"
#include "Pathfinding_JsonFx_U3CModuleU3E86524790MethodDeclarations.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF4023709608.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF4023709608MethodDeclarations.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonFx541860733.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonFx541860733MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_IO_TextWriter2304124208.h"
#include "mscorlib_System_IO_TextWriter2304124208MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "mscorlib_System_Char2862622538MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_Char2862622538.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1974256870MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1974256870.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_StringComparer4230573202MethodDeclarations.h"
#include "mscorlib_System_StringComparer4230573202.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF3109307074.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF3109307074MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge696267445.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonFx386455501.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_Convert1363677321MethodDeclarations.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF3911578122MethodDeclarations.h"
#include "mscorlib_System_Exception3991598821.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF3911578122.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF1208944969MethodDeclarations.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonFx617654540.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonFx617654540MethodDeclarations.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "mscorlib_System_Enum2862688501MethodDeclarations.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF1310195012.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF1310195012MethodDeclarations.h"
#include "mscorlib_System_Attribute2523058482MethodDeclarations.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF1698983795.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF1698983795MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "mscorlib_System_Attribute2523058482.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF3750829390.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF3750829390MethodDeclarations.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonFx386455501MethodDeclarations.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF3095433488.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF3095433488MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_gen47628255MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627.h"
#include "System_System_Collections_Generic_Stack_1_gen47628255.h"
#include "mscorlib_System_Console1363597357MethodDeclarations.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF2515450139.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF3725515417.h"
#include "mscorlib_System_Double3868226565.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF3154211006MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge520966972.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF3154211006.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge696267445MethodDeclarations.h"
#include "mscorlib_System_Array1146569071MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder243639308MethodDeclarations.h"
#include "mscorlib_System_Globalization_NumberFormatInfo1637637232MethodDeclarations.h"
#include "mscorlib_System_Int321153838500MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "mscorlib_System_Globalization_NumberFormatInfo1637637232.h"
#include "mscorlib_System_Globalization_NumberStyles2609490573.h"
#include "mscorlib_System_Decimal1954350631MethodDeclarations.h"
#include "mscorlib_System_Double3868226565MethodDeclarations.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_Int641153838595.h"
#include "mscorlib_System_Math2862914300MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen182525330MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen182525330.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF1208944969.h"
#include "mscorlib_System_InvalidOperationException1589641621MethodDeclarations.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF1712644481.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF1712644481MethodDeclarations.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF2515450139MethodDeclarations.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF3725515417MethodDeclarations.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF3394579648.h"
#include "mscorlib_System_ArgumentNullException3573189601MethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142MethodDeclarations.h"
#include "mscorlib_System_IO_StringWriter4216882900MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3573189601.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142.h"
#include "mscorlib_System_IO_StringWriter4216882900.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF3394579648MethodDeclarations.h"
#include "mscorlib_System_TypeCode1814089915.h"
#include "mscorlib_System_Enum2862688501.h"
#include "mscorlib_System_Byte2862609660.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_Int161153838442.h"
#include "mscorlib_System_SByte1161769777.h"
#include "mscorlib_System_UInt1624667923.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_UInt6424668076.h"
#include "mscorlib_System_Guid2862754429.h"
#include "System_System_Uri1116831938.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "mscorlib_System_Version763695022.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF4036178619MethodDeclarations.h"
#include "mscorlib_System_DateTime4283661327MethodDeclarations.h"
#include "mscorlib_System_DateTimeKind1472618179.h"
#include "Pathfinding_JsonFx_Pathfinding_Serialization_JsonF4036178619.h"
#include "mscorlib_System_Guid2862754429MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898MethodDeclarations.h"
#include "mscorlib_System_Byte2862609660MethodDeclarations.h"
#include "mscorlib_System_SByte1161769777MethodDeclarations.h"
#include "mscorlib_System_Int161153838442MethodDeclarations.h"
#include "mscorlib_System_UInt1624667923MethodDeclarations.h"
#include "mscorlib_System_UInt3224667981MethodDeclarations.h"
#include "mscorlib_System_Int641153838595MethodDeclarations.h"
#include "mscorlib_System_UInt6424668076MethodDeclarations.h"
#include "mscorlib_System_Single4291918972MethodDeclarations.h"
#include "mscorlib_System_TimeSpan413522987MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3323877696MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3323877696.h"
#include "mscorlib_System_Reflection_AssemblyName2915647011MethodDeclarations.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266MethodDeclarations.h"
#include "mscorlib_System_Reflection_PropertyInfo924268725.h"
#include "mscorlib_System_Reflection_Assembly1418687608.h"
#include "mscorlib_System_Reflection_Assembly1418687608MethodDeclarations.h"
#include "mscorlib_System_Reflection_AssemblyName2915647011.h"
#include "mscorlib_System_Reflection_PropertyInfo924268725MethodDeclarations.h"
#include "mscorlib_System_Reflection_ParameterInfo2235474049.h"
#include "mscorlib_System_Reflection_BindingFlags1523912596.h"
#include "System_System_ComponentModel_DefaultValueAttribute3756983154.h"
#include "System_System_ComponentModel_DefaultValueAttribute3756983154MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4230874053MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4230874053.h"
#include "mscorlib_System_Environment4152990825MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge626389457.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge626389457MethodDeclarations.h"
#include "mscorlib_System_Reflection_ConstructorInfo4136801618MethodDeclarations.h"
#include "mscorlib_System_Exception3991598821MethodDeclarations.h"
#include "mscorlib_System_Reflection_ConstructorInfo4136801618.h"
#include "mscorlib_System_Reflection_TargetInvocationExcepti3880899288.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge520966972MethodDeclarations.h"
#include "mscorlib_System_Globalization_DateTimeFormatInfo2490955586MethodDeclarations.h"
#include "System_System_Uri1116831938MethodDeclarations.h"
#include "mscorlib_System_Version763695022MethodDeclarations.h"
#include "System_System_ComponentModel_TypeDescriptor1537159061MethodDeclarations.h"
#include "System_System_ComponentModel_TypeConverter1753450284MethodDeclarations.h"
#include "System_System_ComponentModel_TypeConverter1753450284.h"
#include "mscorlib_System_Globalization_DateTimeFormatInfo2490955586.h"
#include "mscorlib_System_Globalization_DateTimeStyles1282965087.h"
#include "System_System_UriKind238866934.h"
#include "mscorlib_System_Reflection_MethodBase318515428MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "mscorlib_System_Reflection_MethodBase318515428.h"
#include "mscorlib_System_Reflection_ParameterInfo2235474049MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Pathfinding.Serialization.JsonFx.EcmaScriptIdentifier::Pathfinding.Serialization.JsonFx.IJsonSerializable.WriteJson(Pathfinding.Serialization.JsonFx.JsonWriter)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t EcmaScriptIdentifier_Pathfinding_Serialization_JsonFx_IJsonSerializable_WriteJson_m3353525581_MetadataUsageId;
extern "C"  void EcmaScriptIdentifier_Pathfinding_Serialization_JsonFx_IJsonSerializable_WriteJson_m3353525581 (EcmaScriptIdentifier_t4023709608 * __this, JsonWriter_t541860733 * ___writer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EcmaScriptIdentifier_Pathfinding_Serialization_JsonFx_IJsonSerializable_WriteJson_m3353525581_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_identifier_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		JsonWriter_t541860733 * L_2 = ___writer0;
		NullCheck(L_2);
		TextWriter_t2304124208 * L_3 = JsonWriter_get_TextWriter_m1060131231(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_3, _stringLiteral3392903);
		goto IL_0036;
	}

IL_0025:
	{
		JsonWriter_t541860733 * L_4 = ___writer0;
		NullCheck(L_4);
		TextWriter_t2304124208 * L_5 = JsonWriter_get_TextWriter_m1060131231(L_4, /*hidden argument*/NULL);
		String_t* L_6 = __this->get_identifier_0();
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_5, L_6);
	}

IL_0036:
	{
		return;
	}
}
// System.String Pathfinding.Serialization.JsonFx.EcmaScriptIdentifier::EnsureValidIdentifier(System.String,System.Boolean)
extern "C"  String_t* EcmaScriptIdentifier_EnsureValidIdentifier_m1404313670 (Il2CppObject * __this /* static, unused */, String_t* ___varExpr0, bool ___nested1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___varExpr0;
		bool L_1 = ___nested1;
		String_t* L_2 = EcmaScriptIdentifier_EnsureValidIdentifier_m1118494327(NULL /*static, unused*/, L_0, L_1, (bool)1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String Pathfinding.Serialization.JsonFx.EcmaScriptIdentifier::EnsureValidIdentifier(System.String,System.Boolean,System.Boolean)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3920529331;
extern Il2CppCodeGenString* _stringLiteral32;
extern Il2CppCodeGenString* _stringLiteral963741598;
extern Il2CppCodeGenString* _stringLiteral4111973861;
extern const uint32_t EcmaScriptIdentifier_EnsureValidIdentifier_m1118494327_MetadataUsageId;
extern "C"  String_t* EcmaScriptIdentifier_EnsureValidIdentifier_m1118494327 (Il2CppObject * __this /* static, unused */, String_t* ___varExpr0, bool ___nested1, bool ___throwOnEmpty2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EcmaScriptIdentifier_EnsureValidIdentifier_m1118494327_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___varExpr0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		bool L_2 = ___throwOnEmpty2;
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		ArgumentException_t928607144 * L_3 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, _stringLiteral3920529331, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}

IL_0022:
	{
		String_t* L_5 = ___varExpr0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_5);
		String_t* L_7 = String_Replace_m2915759397(L_5, _stringLiteral32, L_6, /*hidden argument*/NULL);
		___varExpr0 = L_7;
		String_t* L_8 = ___varExpr0;
		bool L_9 = ___nested1;
		bool L_10 = EcmaScriptIdentifier_IsValidIdentifier_m1345170647(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0056;
		}
	}
	{
		String_t* L_11 = ___varExpr0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral963741598, L_11, _stringLiteral4111973861, /*hidden argument*/NULL);
		ArgumentException_t928607144 * L_13 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_13, L_12, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_0056:
	{
		String_t* L_14 = ___varExpr0;
		return L_14;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.EcmaScriptIdentifier::IsValidIdentifier(System.String,System.Boolean)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t3324145743_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern const uint32_t EcmaScriptIdentifier_IsValidIdentifier_m1345170647_MetadataUsageId;
extern "C"  bool EcmaScriptIdentifier_IsValidIdentifier_m1345170647 (Il2CppObject * __this /* static, unused */, String_t* ___varExpr0, bool ___nested1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EcmaScriptIdentifier_IsValidIdentifier_m1345170647_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t4054002952* V_0 = NULL;
	StringU5BU5D_t4054002952* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	bool V_4 = false;
	String_t* V_5 = NULL;
	int32_t V_6 = 0;
	Il2CppChar V_7 = 0x0;
	{
		String_t* L_0 = ___varExpr0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		bool L_2 = ___nested1;
		if (!L_2)
		{
			goto IL_004f;
		}
	}
	{
		String_t* L_3 = ___varExpr0;
		CharU5BU5D_t3324145743* L_4 = ((CharU5BU5D_t3324145743*)SZArrayNew(CharU5BU5D_t3324145743_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)46));
		NullCheck(L_3);
		StringU5BU5D_t4054002952* L_5 = String_Split_m290179486(L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		StringU5BU5D_t4054002952* L_6 = V_0;
		V_1 = L_6;
		V_2 = 0;
		goto IL_0044;
	}

IL_002e:
	{
		StringU5BU5D_t4054002952* L_7 = V_1;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		String_t* L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_3 = L_10;
		String_t* L_11 = V_3;
		bool L_12 = EcmaScriptIdentifier_IsValidIdentifier_m1345170647(NULL /*static, unused*/, L_11, (bool)0, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0040;
		}
	}
	{
		return (bool)0;
	}

IL_0040:
	{
		int32_t L_13 = V_2;
		V_2 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0044:
	{
		int32_t L_14 = V_2;
		StringU5BU5D_t4054002952* L_15 = V_1;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_002e;
		}
	}
	{
		return (bool)1;
	}

IL_004f:
	{
		String_t* L_16 = ___varExpr0;
		bool L_17 = EcmaScriptIdentifier_IsReservedWord_m1103390885(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_005c;
		}
	}
	{
		return (bool)0;
	}

IL_005c:
	{
		V_4 = (bool)0;
		String_t* L_18 = ___varExpr0;
		V_5 = L_18;
		V_6 = 0;
		goto IL_00bb;
	}

IL_006a:
	{
		String_t* L_19 = V_5;
		int32_t L_20 = V_6;
		NullCheck(L_19);
		Il2CppChar L_21 = String_get_Chars_m3015341861(L_19, L_20, /*hidden argument*/NULL);
		V_7 = L_21;
		bool L_22 = V_4;
		if (!L_22)
		{
			goto IL_008d;
		}
	}
	{
		Il2CppChar L_23 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_24 = Char_IsDigit_m1743537211(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_008d;
		}
	}
	{
		goto IL_00b5;
	}

IL_008d:
	{
		Il2CppChar L_25 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_26 = Char_IsLetter_m1699036490(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		if (L_26)
		{
			goto IL_00ab;
		}
	}
	{
		Il2CppChar L_27 = V_7;
		if ((((int32_t)L_27) == ((int32_t)((int32_t)95))))
		{
			goto IL_00ab;
		}
	}
	{
		Il2CppChar L_28 = V_7;
		if ((!(((uint32_t)L_28) == ((uint32_t)((int32_t)36)))))
		{
			goto IL_00b3;
		}
	}

IL_00ab:
	{
		V_4 = (bool)1;
		goto IL_00b5;
	}

IL_00b3:
	{
		return (bool)0;
	}

IL_00b5:
	{
		int32_t L_29 = V_6;
		V_6 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_00bb:
	{
		int32_t L_30 = V_6;
		String_t* L_31 = V_5;
		NullCheck(L_31);
		int32_t L_32 = String_get_Length_m2979997331(L_31, /*hidden argument*/NULL);
		if ((((int32_t)L_30) < ((int32_t)L_32)))
		{
			goto IL_006a;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.EcmaScriptIdentifier::IsReservedWord(System.String)
extern Il2CppClass* EcmaScriptIdentifier_t4023709608_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1974256870_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m4235384975_MethodInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m337170132_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3392903;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral94001407;
extern Il2CppCodeGenString* _stringLiteral3046192;
extern Il2CppCodeGenString* _stringLiteral94432955;
extern Il2CppCodeGenString* _stringLiteral3727764647;
extern Il2CppCodeGenString* _stringLiteral547812385;
extern Il2CppCodeGenString* _stringLiteral1544803905;
extern Il2CppCodeGenString* _stringLiteral2959508907;
extern Il2CppCodeGenString* _stringLiteral3220;
extern Il2CppCodeGenString* _stringLiteral3116345;
extern Il2CppCodeGenString* _stringLiteral3441707395;
extern Il2CppCodeGenString* _stringLiteral101577;
extern Il2CppCodeGenString* _stringLiteral1380938712;
extern Il2CppCodeGenString* _stringLiteral3357;
extern Il2CppCodeGenString* _stringLiteral3365;
extern Il2CppCodeGenString* _stringLiteral902025516;
extern Il2CppCodeGenString* _stringLiteral108960;
extern Il2CppCodeGenString* _stringLiteral3360570672;
extern Il2CppCodeGenString* _stringLiteral3405494068;
extern Il2CppCodeGenString* _stringLiteral3559070;
extern Il2CppCodeGenString* _stringLiteral110339814;
extern Il2CppCodeGenString* _stringLiteral115131;
extern Il2CppCodeGenString* _stringLiteral3436164753;
extern Il2CppCodeGenString* _stringLiteral116519;
extern Il2CppCodeGenString* _stringLiteral3625364;
extern Il2CppCodeGenString* _stringLiteral113101617;
extern Il2CppCodeGenString* _stringLiteral3649734;
extern Il2CppCodeGenString* _stringLiteral1732898850;
extern Il2CppCodeGenString* _stringLiteral64711720;
extern Il2CppCodeGenString* _stringLiteral3039496;
extern Il2CppCodeGenString* _stringLiteral3052374;
extern Il2CppCodeGenString* _stringLiteral94742904;
extern Il2CppCodeGenString* _stringLiteral94844771;
extern Il2CppCodeGenString* _stringLiteral2969009105;
extern Il2CppCodeGenString* _stringLiteral3118337;
extern Il2CppCodeGenString* _stringLiteral3005813684;
extern Il2CppCodeGenString* _stringLiteral2989302937;
extern Il2CppCodeGenString* _stringLiteral97436022;
extern Il2CppCodeGenString* _stringLiteral97526364;
extern Il2CppCodeGenString* _stringLiteral3178851;
extern Il2CppCodeGenString* _stringLiteral3379582896;
extern Il2CppCodeGenString* _stringLiteral3110171557;
extern Il2CppCodeGenString* _stringLiteral104431;
extern Il2CppCodeGenString* _stringLiteral502623545;
extern Il2CppCodeGenString* _stringLiteral3327612;
extern Il2CppCodeGenString* _stringLiteral3242348567;
extern Il2CppCodeGenString* _stringLiteral3487904838;
extern Il2CppCodeGenString* _stringLiteral3980469635;
extern Il2CppCodeGenString* _stringLiteral3686427566;
extern Il2CppCodeGenString* _stringLiteral3317543529;
extern Il2CppCodeGenString* _stringLiteral109413500;
extern Il2CppCodeGenString* _stringLiteral3402485358;
extern Il2CppCodeGenString* _stringLiteral109801339;
extern Il2CppCodeGenString* _stringLiteral2828371220;
extern Il2CppCodeGenString* _stringLiteral3420534349;
extern Il2CppCodeGenString* _stringLiteral1052746378;
extern Il2CppCodeGenString* _stringLiteral2406940060;
extern Il2CppCodeGenString* _stringLiteral107035;
extern Il2CppCodeGenString* _stringLiteral114974605;
extern const uint32_t EcmaScriptIdentifier_IsReservedWord_m1103390885_MetadataUsageId;
extern "C"  bool EcmaScriptIdentifier_IsReservedWord_m1103390885 (Il2CppObject * __this /* static, unused */, String_t* ___varExpr0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EcmaScriptIdentifier_IsReservedWord_m1103390885_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Dictionary_2_t1974256870 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		String_t* L_0 = ___varExpr0;
		V_0 = L_0;
		String_t* L_1 = V_0;
		if (!L_1)
		{
			goto IL_031b;
		}
	}
	{
		Dictionary_2_t1974256870 * L_2 = ((EcmaScriptIdentifier_t4023709608_StaticFields*)EcmaScriptIdentifier_t4023709608_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_1();
		if (L_2)
		{
			goto IL_02fc;
		}
	}
	{
		Dictionary_2_t1974256870 * L_3 = (Dictionary_2_t1974256870 *)il2cpp_codegen_object_new(Dictionary_2_t1974256870_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_3, ((int32_t)61), /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_1 = L_3;
		Dictionary_2_t1974256870 * L_4 = V_1;
		NullCheck(L_4);
		Dictionary_2_Add_m4235384975(L_4, _stringLiteral3392903, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_5 = V_1;
		NullCheck(L_5);
		Dictionary_2_Add_m4235384975(L_5, _stringLiteral97196323, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_6 = V_1;
		NullCheck(L_6);
		Dictionary_2_Add_m4235384975(L_6, _stringLiteral3569038, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_7 = V_1;
		NullCheck(L_7);
		Dictionary_2_Add_m4235384975(L_7, _stringLiteral94001407, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_8 = V_1;
		NullCheck(L_8);
		Dictionary_2_Add_m4235384975(L_8, _stringLiteral3046192, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_9 = V_1;
		NullCheck(L_9);
		Dictionary_2_Add_m4235384975(L_9, _stringLiteral94432955, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_10 = V_1;
		NullCheck(L_10);
		Dictionary_2_Add_m4235384975(L_10, _stringLiteral3727764647, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_11 = V_1;
		NullCheck(L_11);
		Dictionary_2_Add_m4235384975(L_11, _stringLiteral547812385, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_12 = V_1;
		NullCheck(L_12);
		Dictionary_2_Add_m4235384975(L_12, _stringLiteral1544803905, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_13 = V_1;
		NullCheck(L_13);
		Dictionary_2_Add_m4235384975(L_13, _stringLiteral2959508907, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_14 = V_1;
		NullCheck(L_14);
		Dictionary_2_Add_m4235384975(L_14, _stringLiteral3220, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_15 = V_1;
		NullCheck(L_15);
		Dictionary_2_Add_m4235384975(L_15, _stringLiteral3116345, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_16 = V_1;
		NullCheck(L_16);
		Dictionary_2_Add_m4235384975(L_16, _stringLiteral3441707395, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_17 = V_1;
		NullCheck(L_17);
		Dictionary_2_Add_m4235384975(L_17, _stringLiteral101577, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_18 = V_1;
		NullCheck(L_18);
		Dictionary_2_Add_m4235384975(L_18, _stringLiteral1380938712, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_19 = V_1;
		NullCheck(L_19);
		Dictionary_2_Add_m4235384975(L_19, _stringLiteral3357, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_20 = V_1;
		NullCheck(L_20);
		Dictionary_2_Add_m4235384975(L_20, _stringLiteral3365, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_21 = V_1;
		NullCheck(L_21);
		Dictionary_2_Add_m4235384975(L_21, _stringLiteral902025516, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_22 = V_1;
		NullCheck(L_22);
		Dictionary_2_Add_m4235384975(L_22, _stringLiteral108960, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_23 = V_1;
		NullCheck(L_23);
		Dictionary_2_Add_m4235384975(L_23, _stringLiteral3360570672, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_24 = V_1;
		NullCheck(L_24);
		Dictionary_2_Add_m4235384975(L_24, _stringLiteral3405494068, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_25 = V_1;
		NullCheck(L_25);
		Dictionary_2_Add_m4235384975(L_25, _stringLiteral3559070, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_26 = V_1;
		NullCheck(L_26);
		Dictionary_2_Add_m4235384975(L_26, _stringLiteral110339814, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_27 = V_1;
		NullCheck(L_27);
		Dictionary_2_Add_m4235384975(L_27, _stringLiteral115131, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_28 = V_1;
		NullCheck(L_28);
		Dictionary_2_Add_m4235384975(L_28, _stringLiteral3436164753, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_29 = V_1;
		NullCheck(L_29);
		Dictionary_2_Add_m4235384975(L_29, _stringLiteral116519, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_30 = V_1;
		NullCheck(L_30);
		Dictionary_2_Add_m4235384975(L_30, _stringLiteral3625364, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_31 = V_1;
		NullCheck(L_31);
		Dictionary_2_Add_m4235384975(L_31, _stringLiteral113101617, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_32 = V_1;
		NullCheck(L_32);
		Dictionary_2_Add_m4235384975(L_32, _stringLiteral3649734, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_33 = V_1;
		NullCheck(L_33);
		Dictionary_2_Add_m4235384975(L_33, _stringLiteral1732898850, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_34 = V_1;
		NullCheck(L_34);
		Dictionary_2_Add_m4235384975(L_34, _stringLiteral64711720, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_35 = V_1;
		NullCheck(L_35);
		Dictionary_2_Add_m4235384975(L_35, _stringLiteral3039496, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_36 = V_1;
		NullCheck(L_36);
		Dictionary_2_Add_m4235384975(L_36, _stringLiteral3052374, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_37 = V_1;
		NullCheck(L_37);
		Dictionary_2_Add_m4235384975(L_37, _stringLiteral94742904, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_38 = V_1;
		NullCheck(L_38);
		Dictionary_2_Add_m4235384975(L_38, _stringLiteral94844771, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_39 = V_1;
		NullCheck(L_39);
		Dictionary_2_Add_m4235384975(L_39, _stringLiteral2969009105, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_40 = V_1;
		NullCheck(L_40);
		Dictionary_2_Add_m4235384975(L_40, _stringLiteral3118337, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_41 = V_1;
		NullCheck(L_41);
		Dictionary_2_Add_m4235384975(L_41, _stringLiteral3005813684, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_42 = V_1;
		NullCheck(L_42);
		Dictionary_2_Add_m4235384975(L_42, _stringLiteral2989302937, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_43 = V_1;
		NullCheck(L_43);
		Dictionary_2_Add_m4235384975(L_43, _stringLiteral97436022, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_44 = V_1;
		NullCheck(L_44);
		Dictionary_2_Add_m4235384975(L_44, _stringLiteral97526364, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_45 = V_1;
		NullCheck(L_45);
		Dictionary_2_Add_m4235384975(L_45, _stringLiteral3178851, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_46 = V_1;
		NullCheck(L_46);
		Dictionary_2_Add_m4235384975(L_46, _stringLiteral3379582896, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_47 = V_1;
		NullCheck(L_47);
		Dictionary_2_Add_m4235384975(L_47, _stringLiteral3110171557, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_48 = V_1;
		NullCheck(L_48);
		Dictionary_2_Add_m4235384975(L_48, _stringLiteral104431, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_49 = V_1;
		NullCheck(L_49);
		Dictionary_2_Add_m4235384975(L_49, _stringLiteral502623545, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_50 = V_1;
		NullCheck(L_50);
		Dictionary_2_Add_m4235384975(L_50, _stringLiteral3327612, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_51 = V_1;
		NullCheck(L_51);
		Dictionary_2_Add_m4235384975(L_51, _stringLiteral3242348567, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_52 = V_1;
		NullCheck(L_52);
		Dictionary_2_Add_m4235384975(L_52, _stringLiteral3487904838, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_53 = V_1;
		NullCheck(L_53);
		Dictionary_2_Add_m4235384975(L_53, _stringLiteral3980469635, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_54 = V_1;
		NullCheck(L_54);
		Dictionary_2_Add_m4235384975(L_54, _stringLiteral3686427566, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_55 = V_1;
		NullCheck(L_55);
		Dictionary_2_Add_m4235384975(L_55, _stringLiteral3317543529, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_56 = V_1;
		NullCheck(L_56);
		Dictionary_2_Add_m4235384975(L_56, _stringLiteral109413500, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_57 = V_1;
		NullCheck(L_57);
		Dictionary_2_Add_m4235384975(L_57, _stringLiteral3402485358, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_58 = V_1;
		NullCheck(L_58);
		Dictionary_2_Add_m4235384975(L_58, _stringLiteral109801339, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_59 = V_1;
		NullCheck(L_59);
		Dictionary_2_Add_m4235384975(L_59, _stringLiteral2828371220, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_60 = V_1;
		NullCheck(L_60);
		Dictionary_2_Add_m4235384975(L_60, _stringLiteral3420534349, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_61 = V_1;
		NullCheck(L_61);
		Dictionary_2_Add_m4235384975(L_61, _stringLiteral1052746378, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_62 = V_1;
		NullCheck(L_62);
		Dictionary_2_Add_m4235384975(L_62, _stringLiteral2406940060, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_63 = V_1;
		NullCheck(L_63);
		Dictionary_2_Add_m4235384975(L_63, _stringLiteral107035, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_64 = V_1;
		NullCheck(L_64);
		Dictionary_2_Add_m4235384975(L_64, _stringLiteral114974605, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_65 = V_1;
		((EcmaScriptIdentifier_t4023709608_StaticFields*)EcmaScriptIdentifier_t4023709608_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map0_1(L_65);
	}

IL_02fc:
	{
		Dictionary_2_t1974256870 * L_66 = ((EcmaScriptIdentifier_t4023709608_StaticFields*)EcmaScriptIdentifier_t4023709608_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_1();
		String_t* L_67 = V_0;
		NullCheck(L_66);
		bool L_68 = Dictionary_2_TryGetValue_m337170132(L_66, L_67, (&V_2), /*hidden argument*/Dictionary_2_TryGetValue_m337170132_MethodInfo_var);
		if (!L_68)
		{
			goto IL_031b;
		}
	}
	{
		int32_t L_69 = V_2;
		if (!L_69)
		{
			goto IL_0319;
		}
	}
	{
		goto IL_031b;
	}

IL_0319:
	{
		return (bool)1;
	}

IL_031b:
	{
		return (bool)0;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.EcmaScriptIdentifier::Equals(System.Object)
extern Il2CppClass* EcmaScriptIdentifier_t4023709608_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringComparer_t4230573202_il2cpp_TypeInfo_var;
extern const uint32_t EcmaScriptIdentifier_Equals_m1423181236_MetadataUsageId;
extern "C"  bool EcmaScriptIdentifier_Equals_m1423181236 (EcmaScriptIdentifier_t4023709608 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EcmaScriptIdentifier_Equals_m1423181236_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EcmaScriptIdentifier_t4023709608 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = ((EcmaScriptIdentifier_t4023709608 *)IsInstClass(L_0, EcmaScriptIdentifier_t4023709608_il2cpp_TypeInfo_var));
		EcmaScriptIdentifier_t4023709608 * L_1 = V_0;
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		bool L_3 = Object_Equals_m2558036873(__this, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0015:
	{
		String_t* L_4 = __this->get_identifier_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0037;
		}
	}
	{
		EcmaScriptIdentifier_t4023709608 * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = L_6->get_identifier_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0037;
		}
	}
	{
		return (bool)1;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t4230573202_il2cpp_TypeInfo_var);
		StringComparer_t4230573202 * L_9 = StringComparer_get_Ordinal_m2543279027(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_10 = __this->get_identifier_0();
		EcmaScriptIdentifier_t4023709608 * L_11 = V_0;
		NullCheck(L_11);
		String_t* L_12 = L_11->get_identifier_0();
		NullCheck(L_9);
		bool L_13 = VirtFuncInvoker2< bool, String_t*, String_t* >::Invoke(11 /* System.Boolean System.StringComparer::Equals(System.String,System.String) */, L_9, L_10, L_12);
		return L_13;
	}
}
// System.String Pathfinding.Serialization.JsonFx.EcmaScriptIdentifier::ToString()
extern "C"  String_t* EcmaScriptIdentifier_ToString_m2780159466 (EcmaScriptIdentifier_t4023709608 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_identifier_0();
		return L_0;
	}
}
// System.Int32 Pathfinding.Serialization.JsonFx.EcmaScriptIdentifier::GetHashCode()
extern "C"  int32_t EcmaScriptIdentifier_GetHashCode_m2065690444 (EcmaScriptIdentifier_t4023709608 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_identifier_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		String_t* L_1 = __this->get_identifier_0();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_1);
		return L_2;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonConverter::.ctor()
extern "C"  void JsonConverter__ctor_m98638373 (JsonConverter_t3109307074 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonConverter::Write(Pathfinding.Serialization.JsonFx.JsonWriter,System.Type,System.Object)
extern "C"  void JsonConverter_Write_m275892874 (JsonConverter_t3109307074 * __this, JsonWriter_t541860733 * ___writer0, Type_t * ___type1, Il2CppObject * ___value2, const MethodInfo* method)
{
	Dictionary_2_t696267445 * V_0 = NULL;
	{
		Type_t * L_0 = ___type1;
		Il2CppObject * L_1 = ___value2;
		Dictionary_2_t696267445 * L_2 = VirtFuncInvoker2< Dictionary_2_t696267445 *, Type_t *, Il2CppObject * >::Invoke(5 /* System.Collections.Generic.Dictionary`2<System.String,System.Object> Pathfinding.Serialization.JsonFx.JsonConverter::WriteJson(System.Type,System.Object) */, __this, L_0, L_1);
		V_0 = L_2;
		JsonWriter_t541860733 * L_3 = ___writer0;
		Dictionary_2_t696267445 * L_4 = V_0;
		NullCheck(L_3);
		JsonWriter_Write_m456059081(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.JsonConverter::Read(Pathfinding.Serialization.JsonFx.JsonReader,System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  Il2CppObject * JsonConverter_Read_m2924094753 (JsonConverter_t3109307074 * __this, JsonReader_t386455501 * ___reader0, Type_t * ___type1, Dictionary_2_t696267445 * ___value2, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type1;
		Dictionary_2_t696267445 * L_1 = ___value2;
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, Type_t *, Dictionary_2_t696267445 * >::Invoke(6 /* System.Object Pathfinding.Serialization.JsonFx.JsonConverter::ReadJson(System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Object>) */, __this, L_0, L_1);
		return L_2;
	}
}
// System.Single Pathfinding.Serialization.JsonFx.JsonConverter::CastFloat(System.Object)
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2029936258;
extern const uint32_t JsonConverter_CastFloat_m2051699328_MetadataUsageId;
extern "C"  float JsonConverter_CastFloat_m2051699328 (JsonConverter_t3109307074 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonConverter_CastFloat_m2051699328_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Exception_t3991598821 * V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = ___o0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return (0.0f);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_1 = ___o0;
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
			float L_2 = Convert_ToSingle_m1579085759(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
			V_0 = L_2;
			goto IL_003b;
		}

IL_0018:
		{
			; // IL_0018: leave IL_003b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_001d:
	{ // begin catch(System.Exception)
		{
			V_1 = ((Exception_t3991598821 *)__exception_local);
			Il2CppObject * L_3 = ___o0;
			NullCheck(L_3);
			Type_t * L_4 = Object_GetType_m2022236990(L_3, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_5 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2029936258, L_4, /*hidden argument*/NULL);
			Exception_t3991598821 * L_6 = V_1;
			JsonDeserializationException_t3911578122 * L_7 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
			JsonDeserializationException__ctor_m2068273986(L_7, L_5, L_6, 0, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
		}

IL_0036:
		{
			goto IL_003b;
		}
	} // end catch (depth: 1)

IL_003b:
	{
		float L_8 = V_0;
		return L_8;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonDeserializationException::.ctor(System.String,System.Int32)
extern "C"  void JsonDeserializationException__ctor_m1655578326 (JsonDeserializationException_t3911578122 * __this, String_t* ___message0, int32_t ___index1, const MethodInfo* method)
{
	{
		__this->set_index_12((-1));
		String_t* L_0 = ___message0;
		JsonSerializationException__ctor_m1980876064(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___index1;
		__this->set_index_12(L_1);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonDeserializationException::.ctor(System.String,System.Exception,System.Int32)
extern "C"  void JsonDeserializationException__ctor_m2068273986 (JsonDeserializationException_t3911578122 * __this, String_t* ___message0, Exception_t3991598821 * ___innerException1, int32_t ___index2, const MethodInfo* method)
{
	{
		__this->set_index_12((-1));
		String_t* L_0 = ___message0;
		Exception_t3991598821 * L_1 = ___innerException1;
		JsonSerializationException__ctor_m906028054(__this, L_0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___index2;
		__this->set_index_12(L_2);
		return;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonIgnoreAttribute::IsJsonIgnore(System.Object)
extern const Il2CppType* JsonIgnoreAttribute_t617654540_0_0_0_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* ICustomAttributeProvider_t1425685797_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t JsonIgnoreAttribute_IsJsonIgnore_m3417392871_MetadataUsageId;
extern "C"  bool JsonIgnoreAttribute_IsJsonIgnore_m3417392871 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonIgnoreAttribute_IsJsonIgnore_m3417392871_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___value0;
		NullCheck(L_1);
		Type_t * L_2 = Object_GetType_m2022236990(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = (Il2CppObject *)NULL;
		Type_t * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = Type_get_IsEnum_m3878730619(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002f;
		}
	}
	{
		Type_t * L_5 = V_0;
		Type_t * L_6 = V_0;
		Il2CppObject * L_7 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_8 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		FieldInfo_t * L_9 = Type_GetField_m1054209156(L_5, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		goto IL_0036;
	}

IL_002f:
	{
		Il2CppObject * L_10 = ___value0;
		V_1 = ((Il2CppObject *)IsInst(L_10, ICustomAttributeProvider_t1425685797_il2cpp_TypeInfo_var));
	}

IL_0036:
	{
		Il2CppObject * L_11 = V_1;
		if (L_11)
		{
			goto IL_0042;
		}
	}
	{
		ArgumentException_t928607144 * L_12 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m571182463(L_12, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0042:
	{
		Il2CppObject * L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_14 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(JsonIgnoreAttribute_t617654540_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_13);
		bool L_15 = InterfaceFuncInvoker2< bool, Type_t *, bool >::Invoke(1 /* System.Boolean System.Reflection.ICustomAttributeProvider::IsDefined(System.Type,System.Boolean) */, ICustomAttributeProvider_t1425685797_il2cpp_TypeInfo_var, L_13, L_14, (bool)1);
		return L_15;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonIgnoreAttribute::IsXmlIgnore(System.Object)
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* ICustomAttributeProvider_t1425685797_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern const uint32_t JsonIgnoreAttribute_IsXmlIgnore_m2733425596_MetadataUsageId;
extern "C"  bool JsonIgnoreAttribute_IsXmlIgnore_m2733425596 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonIgnoreAttribute_IsXmlIgnore_m2733425596_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___value0;
		NullCheck(L_1);
		Type_t * L_2 = Object_GetType_m2022236990(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = (Il2CppObject *)NULL;
		Type_t * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = Type_get_IsEnum_m3878730619(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002f;
		}
	}
	{
		Type_t * L_5 = V_0;
		Type_t * L_6 = V_0;
		Il2CppObject * L_7 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_8 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		FieldInfo_t * L_9 = Type_GetField_m1054209156(L_5, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		goto IL_0036;
	}

IL_002f:
	{
		Il2CppObject * L_10 = ___value0;
		V_1 = ((Il2CppObject *)IsInst(L_10, ICustomAttributeProvider_t1425685797_il2cpp_TypeInfo_var));
	}

IL_0036:
	{
		Il2CppObject * L_11 = V_1;
		if (L_11)
		{
			goto IL_0042;
		}
	}
	{
		ArgumentException_t928607144 * L_12 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m571182463(L_12, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0042:
	{
		return (bool)0;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonMemberAttribute::.ctor()
extern "C"  void JsonMemberAttribute__ctor_m1290858339 (JsonMemberAttribute_t1310195012 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonNameAttribute::.ctor(System.String)
extern "C"  void JsonNameAttribute__ctor_m2079629806 (JsonNameAttribute_t1698983795 * __this, String_t* ___jsonName0, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___jsonName0;
		String_t* L_1 = EcmaScriptIdentifier_EnsureValidIdentifier_m1404313670(NULL /*static, unused*/, L_0, (bool)0, /*hidden argument*/NULL);
		__this->set_jsonName_0(L_1);
		return;
	}
}
// System.String Pathfinding.Serialization.JsonFx.JsonNameAttribute::get_Name()
extern "C"  String_t* JsonNameAttribute_get_Name_m2559690471 (JsonNameAttribute_t1698983795 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_jsonName_0();
		return L_0;
	}
}
// System.String Pathfinding.Serialization.JsonFx.JsonNameAttribute::GetJsonName(System.Object)
extern const Il2CppType* JsonNameAttribute_t1698983795_0_0_0_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MemberInfo_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonNameAttribute_t1698983795_il2cpp_TypeInfo_var;
extern const uint32_t JsonNameAttribute_GetJsonName_m166270748_MetadataUsageId;
extern "C"  String_t* JsonNameAttribute_GetJsonName_m166270748 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonNameAttribute_GetJsonName_m166270748_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	MemberInfo_t * V_1 = NULL;
	String_t* V_2 = NULL;
	JsonNameAttribute_t1698983795 * V_3 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___value0;
		NullCheck(L_1);
		Type_t * L_2 = Object_GetType_m2022236990(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = (MemberInfo_t *)NULL;
		Type_t * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = Type_get_IsEnum_m3878730619(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_5 = V_0;
		Il2CppObject * L_6 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_7 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		String_t* L_8 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0031;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0031:
	{
		Type_t * L_10 = V_0;
		String_t* L_11 = V_2;
		NullCheck(L_10);
		FieldInfo_t * L_12 = Type_GetField_m1054209156(L_10, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		goto IL_0045;
	}

IL_003e:
	{
		Il2CppObject * L_13 = ___value0;
		V_1 = ((MemberInfo_t *)IsInstClass(L_13, MemberInfo_t_il2cpp_TypeInfo_var));
	}

IL_0045:
	{
		MemberInfo_t * L_14 = V_1;
		if (L_14)
		{
			goto IL_0051;
		}
	}
	{
		ArgumentException_t928607144 * L_15 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m571182463(L_15, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_0051:
	{
		MemberInfo_t * L_16 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(JsonNameAttribute_t1698983795_0_0_0_var), /*hidden argument*/NULL);
		bool L_18 = Attribute_IsDefined_m3508553943(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_0068;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0068:
	{
		MemberInfo_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(JsonNameAttribute_t1698983795_0_0_0_var), /*hidden argument*/NULL);
		Attribute_t2523058482 * L_21 = Attribute_GetCustomAttribute_m506754809(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		V_3 = ((JsonNameAttribute_t1698983795 *)CastclassClass(L_21, JsonNameAttribute_t1698983795_il2cpp_TypeInfo_var));
		JsonNameAttribute_t1698983795 * L_22 = V_3;
		NullCheck(L_22);
		String_t* L_23 = JsonNameAttribute_get_Name_m2559690471(L_22, /*hidden argument*/NULL);
		return L_23;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonOptInAttribute::.ctor()
extern "C"  void JsonOptInAttribute__ctor_m811349181 (JsonOptInAttribute_t3750829390 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonReader::.ctor(System.String,Pathfinding.Serialization.JsonFx.JsonReaderSettings)
extern Il2CppClass* JsonReaderSettings_t3095433488_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1244034627_il2cpp_TypeInfo_var;
extern Il2CppClass* Stack_1_t47628255_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1239231859_MethodInfo_var;
extern const MethodInfo* Stack_1__ctor_m3860504067_MethodInfo_var;
extern const uint32_t JsonReader__ctor_m1588328406_MetadataUsageId;
extern "C"  void JsonReader__ctor_m1588328406 (JsonReader_t386455501 * __this, String_t* ___input0, JsonReaderSettings_t3095433488 * ___settings1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader__ctor_m1588328406_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonReaderSettings_t3095433488 * L_0 = (JsonReaderSettings_t3095433488 *)il2cpp_codegen_object_new(JsonReaderSettings_t3095433488_il2cpp_TypeInfo_var);
		JsonReaderSettings__ctor_m298179387(L_0, /*hidden argument*/NULL);
		__this->set_Settings_8(L_0);
		List_1_t1244034627 * L_1 = (List_1_t1244034627 *)il2cpp_codegen_object_new(List_1_t1244034627_il2cpp_TypeInfo_var);
		List_1__ctor_m1239231859(L_1, /*hidden argument*/List_1__ctor_m1239231859_MethodInfo_var);
		__this->set_previouslyDeserialized_12(L_1);
		Stack_1_t47628255 * L_2 = (Stack_1_t47628255 *)il2cpp_codegen_object_new(Stack_1_t47628255_il2cpp_TypeInfo_var);
		Stack_1__ctor_m3860504067(L_2, /*hidden argument*/Stack_1__ctor_m3860504067_MethodInfo_var);
		__this->set_jsArrays_13(L_2);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		JsonReaderSettings_t3095433488 * L_3 = ___settings1;
		__this->set_Settings_8(L_3);
		String_t* L_4 = ___input0;
		__this->set_Source_9(L_4);
		String_t* L_5 = __this->get_Source_9();
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m2979997331(L_5, /*hidden argument*/NULL);
		__this->set_SourceLength_10(L_6);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonReader::.cctor()
extern Il2CppClass* JsonReader_t386455501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral3392903;
extern Il2CppCodeGenString* _stringLiteral3256836432;
extern Il2CppCodeGenString* _stringLiteral78043;
extern Il2CppCodeGenString* _stringLiteral237817416;
extern Il2CppCodeGenString* _stringLiteral506745205;
extern Il2CppCodeGenString* _stringLiteral2235684418;
extern const uint32_t JsonReader__cctor_m3807380559_MetadataUsageId;
extern "C"  void JsonReader__cctor_m3807380559 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader__cctor_m3807380559_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->set_LiteralFalse_0(_stringLiteral97196323);
		((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->set_LiteralTrue_1(_stringLiteral3569038);
		((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->set_LiteralNull_2(_stringLiteral3392903);
		((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->set_LiteralUndefined_3(_stringLiteral3256836432);
		((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->set_LiteralNotANumber_4(_stringLiteral78043);
		((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->set_LiteralPositiveInfinity_5(_stringLiteral237817416);
		((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->set_LiteralNegativeInfinity_6(_stringLiteral506745205);
		((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->set_TypeGenericIDictionary_7(_stringLiteral2235684418);
		return;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.JsonReader::Deserialize(System.Type)
extern "C"  Il2CppObject * JsonReader_Deserialize_m3202272201 (JsonReader_t386455501 * __this, Type_t * ___type0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		Il2CppObject * L_1 = JsonReader_Read_m1957631563(__this, L_0, (bool)0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.JsonReader::Read(System.Type,System.Boolean)
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern const Il2CppType* Dictionary_2_t696267445_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t696267445_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonTypeCoercionException_t3725515417_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Console_t1363597357_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonReader_t386455501_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1174020667;
extern const uint32_t JsonReader_Read_m1957631563_MetadataUsageId;
extern "C"  Il2CppObject * JsonReader_Read_m1957631563 (JsonReader_t386455501 * __this, Type_t * ___expectedType0, bool ___typeIsHint1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_Read_m1957631563_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	JsonConverter_t3109307074 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Dictionary_2_t696267445 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	JsonTypeCoercionException_t3725515417 * V_6 = NULL;
	int32_t V_7 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	JsonReader_t386455501 * G_B15_0 = NULL;
	JsonReader_t386455501 * G_B14_0 = NULL;
	Type_t * G_B16_0 = NULL;
	JsonReader_t386455501 * G_B16_1 = NULL;
	JsonReader_t386455501 * G_B19_0 = NULL;
	JsonReader_t386455501 * G_B18_0 = NULL;
	Type_t * G_B20_0 = NULL;
	JsonReader_t386455501 * G_B20_1 = NULL;
	JsonReader_t386455501 * G_B23_0 = NULL;
	JsonReader_t386455501 * G_B22_0 = NULL;
	Type_t * G_B24_0 = NULL;
	JsonReader_t386455501 * G_B24_1 = NULL;
	JsonReader_t386455501 * G_B27_0 = NULL;
	JsonReader_t386455501 * G_B26_0 = NULL;
	Type_t * G_B28_0 = NULL;
	JsonReader_t386455501 * G_B28_1 = NULL;
	{
		Type_t * L_0 = ___expectedType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))))
		{
			goto IL_0013;
		}
	}
	{
		___expectedType0 = (Type_t *)NULL;
	}

IL_0013:
	{
		int32_t L_2 = JsonReader_Tokenize_m4160635357(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		Type_t * L_3 = ___expectedType0;
		if (!L_3)
		{
			goto IL_0098;
		}
	}
	{
		Type_t * L_4 = ___expectedType0;
		NullCheck(L_4);
		bool L_5 = Type_get_IsPrimitive_m992199183(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0098;
		}
	}
	{
		JsonReaderSettings_t3095433488 * L_6 = __this->get_Settings_8();
		Type_t * L_7 = ___expectedType0;
		NullCheck(L_6);
		JsonConverter_t3109307074 * L_8 = VirtFuncInvoker1< JsonConverter_t3109307074 *, Type_t * >::Invoke(4 /* Pathfinding.Serialization.JsonFx.JsonConverter Pathfinding.Serialization.JsonFx.JsonReaderSettings::GetConverter(System.Type) */, L_6, L_7);
		V_1 = L_8;
		JsonConverter_t3109307074 * L_9 = V_1;
		if (!L_9)
		{
			goto IL_0098;
		}
	}

IL_003e:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_10 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Dictionary_2_t696267445_0_0_0_var), /*hidden argument*/NULL);
			Il2CppObject * L_11 = JsonReader_Read_m1957631563(__this, L_10, (bool)0, /*hidden argument*/NULL);
			V_2 = L_11;
			Il2CppObject * L_12 = V_2;
			V_3 = ((Dictionary_2_t696267445 *)IsInstClass(L_12, Dictionary_2_t696267445_il2cpp_TypeInfo_var));
			Dictionary_2_t696267445 * L_13 = V_3;
			if (L_13)
			{
				goto IL_0065;
			}
		}

IL_005d:
		{
			V_4 = NULL;
			goto IL_020d;
		}

IL_0065:
		{
			JsonConverter_t3109307074 * L_14 = V_1;
			Type_t * L_15 = ___expectedType0;
			Dictionary_2_t696267445 * L_16 = V_3;
			NullCheck(L_14);
			Il2CppObject * L_17 = JsonConverter_Read_m2924094753(L_14, __this, L_15, L_16, /*hidden argument*/NULL);
			V_5 = L_17;
			Il2CppObject * L_18 = V_5;
			V_4 = L_18;
			goto IL_020d;
		}

IL_0079:
		{
			; // IL_0079: leave IL_0096
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (JsonTypeCoercionException_t3725515417_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_007e;
		throw e;
	}

CATCH_007e:
	{ // begin catch(Pathfinding.Serialization.JsonFx.JsonTypeCoercionException)
		V_6 = ((JsonTypeCoercionException_t3725515417 *)__exception_local);
		JsonTypeCoercionException_t3725515417 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral1174020667, L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t1363597357_il2cpp_TypeInfo_var);
		Console_WriteLine_m2829201975(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		goto IL_0096;
	} // end catch (depth: 1)

IL_0096:
	{
		return NULL;
	}

IL_0098:
	{
		int32_t L_21 = V_0;
		V_7 = L_21;
		int32_t L_22 = V_7;
		if (L_22 == 0)
		{
			goto IL_020b;
		}
		if (L_22 == 1)
		{
			goto IL_01f2;
		}
		if (L_22 == 2)
		{
			goto IL_0167;
		}
		if (L_22 == 3)
		{
			goto IL_012b;
		}
		if (L_22 == 4)
		{
			goto IL_0149;
		}
		if (L_22 == 5)
		{
			goto IL_0180;
		}
		if (L_22 == 6)
		{
			goto IL_01a6;
		}
		if (L_22 == 7)
		{
			goto IL_01cc;
		}
		if (L_22 == 8)
		{
			goto IL_0117;
		}
		if (L_22 == 9)
		{
			goto IL_0103;
		}
		if (L_22 == 10)
		{
			goto IL_00ef;
		}
		if (L_22 == 11)
		{
			goto IL_020b;
		}
		if (L_22 == 12)
		{
			goto IL_00db;
		}
	}
	{
		goto IL_020b;
	}

IL_00db:
	{
		bool L_23 = ___typeIsHint1;
		G_B14_0 = __this;
		if (!L_23)
		{
			G_B15_0 = __this;
			goto IL_00e8;
		}
	}
	{
		G_B16_0 = ((Type_t *)(NULL));
		G_B16_1 = G_B14_0;
		goto IL_00e9;
	}

IL_00e8:
	{
		Type_t * L_24 = ___expectedType0;
		G_B16_0 = L_24;
		G_B16_1 = G_B15_0;
	}

IL_00e9:
	{
		NullCheck(G_B16_1);
		Il2CppObject * L_25 = JsonReader_ReadObject_m649863795(G_B16_1, G_B16_0, /*hidden argument*/NULL);
		return L_25;
	}

IL_00ef:
	{
		bool L_26 = ___typeIsHint1;
		G_B18_0 = __this;
		if (!L_26)
		{
			G_B19_0 = __this;
			goto IL_00fc;
		}
	}
	{
		G_B20_0 = ((Type_t *)(NULL));
		G_B20_1 = G_B18_0;
		goto IL_00fd;
	}

IL_00fc:
	{
		Type_t * L_27 = ___expectedType0;
		G_B20_0 = L_27;
		G_B20_1 = G_B19_0;
	}

IL_00fd:
	{
		NullCheck(G_B20_1);
		Il2CppObject * L_28 = JsonReader_ReadArray_m4139571658(G_B20_1, G_B20_0, /*hidden argument*/NULL);
		return L_28;
	}

IL_0103:
	{
		bool L_29 = ___typeIsHint1;
		G_B22_0 = __this;
		if (!L_29)
		{
			G_B23_0 = __this;
			goto IL_0110;
		}
	}
	{
		G_B24_0 = ((Type_t *)(NULL));
		G_B24_1 = G_B22_0;
		goto IL_0111;
	}

IL_0110:
	{
		Type_t * L_30 = ___expectedType0;
		G_B24_0 = L_30;
		G_B24_1 = G_B23_0;
	}

IL_0111:
	{
		NullCheck(G_B24_1);
		Il2CppObject * L_31 = JsonReader_ReadString_m2674412257(G_B24_1, G_B24_0, /*hidden argument*/NULL);
		return L_31;
	}

IL_0117:
	{
		bool L_32 = ___typeIsHint1;
		G_B26_0 = __this;
		if (!L_32)
		{
			G_B27_0 = __this;
			goto IL_0124;
		}
	}
	{
		G_B28_0 = ((Type_t *)(NULL));
		G_B28_1 = G_B26_0;
		goto IL_0125;
	}

IL_0124:
	{
		Type_t * L_33 = ___expectedType0;
		G_B28_0 = L_33;
		G_B28_1 = G_B27_0;
	}

IL_0125:
	{
		NullCheck(G_B28_1);
		Il2CppObject * L_34 = JsonReader_ReadNumber_m757648041(G_B28_1, G_B28_0, /*hidden argument*/NULL);
		return L_34;
	}

IL_012b:
	{
		int32_t L_35 = __this->get_index_11();
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_36 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_LiteralFalse_0();
		NullCheck(L_36);
		int32_t L_37 = String_get_Length_m2979997331(L_36, /*hidden argument*/NULL);
		__this->set_index_11(((int32_t)((int32_t)L_35+(int32_t)L_37)));
		bool L_38 = ((bool)0);
		Il2CppObject * L_39 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_38);
		return L_39;
	}

IL_0149:
	{
		int32_t L_40 = __this->get_index_11();
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_41 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_LiteralTrue_1();
		NullCheck(L_41);
		int32_t L_42 = String_get_Length_m2979997331(L_41, /*hidden argument*/NULL);
		__this->set_index_11(((int32_t)((int32_t)L_40+(int32_t)L_42)));
		bool L_43 = ((bool)1);
		Il2CppObject * L_44 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_43);
		return L_44;
	}

IL_0167:
	{
		int32_t L_45 = __this->get_index_11();
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_46 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_LiteralNull_2();
		NullCheck(L_46);
		int32_t L_47 = String_get_Length_m2979997331(L_46, /*hidden argument*/NULL);
		__this->set_index_11(((int32_t)((int32_t)L_45+(int32_t)L_47)));
		return NULL;
	}

IL_0180:
	{
		int32_t L_48 = __this->get_index_11();
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_49 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_LiteralNotANumber_4();
		NullCheck(L_49);
		int32_t L_50 = String_get_Length_m2979997331(L_49, /*hidden argument*/NULL);
		__this->set_index_11(((int32_t)((int32_t)L_48+(int32_t)L_50)));
		double L_51 = (std::numeric_limits<double>::quiet_NaN());
		Il2CppObject * L_52 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_51);
		return L_52;
	}

IL_01a6:
	{
		int32_t L_53 = __this->get_index_11();
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_54 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_LiteralPositiveInfinity_5();
		NullCheck(L_54);
		int32_t L_55 = String_get_Length_m2979997331(L_54, /*hidden argument*/NULL);
		__this->set_index_11(((int32_t)((int32_t)L_53+(int32_t)L_55)));
		double L_56 = (std::numeric_limits<double>::infinity());
		Il2CppObject * L_57 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_56);
		return L_57;
	}

IL_01cc:
	{
		int32_t L_58 = __this->get_index_11();
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_59 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_LiteralNegativeInfinity_6();
		NullCheck(L_59);
		int32_t L_60 = String_get_Length_m2979997331(L_59, /*hidden argument*/NULL);
		__this->set_index_11(((int32_t)((int32_t)L_58+(int32_t)L_60)));
		double L_61 = (-std::numeric_limits<double>::infinity());
		Il2CppObject * L_62 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_61);
		return L_62;
	}

IL_01f2:
	{
		int32_t L_63 = __this->get_index_11();
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_64 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_LiteralUndefined_3();
		NullCheck(L_64);
		int32_t L_65 = String_get_Length_m2979997331(L_64, /*hidden argument*/NULL);
		__this->set_index_11(((int32_t)((int32_t)L_63+(int32_t)L_65)));
		return NULL;
	}

IL_020b:
	{
		return NULL;
	}

IL_020d:
	{
		Il2CppObject * L_66 = V_4;
		return L_66;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonReader::PopulateObject(System.Object&)
extern "C"  void JsonReader_PopulateObject_m3291320615 (JsonReader_t386455501 * __this, Il2CppObject ** ___obj0, const MethodInfo* method)
{
	Type_t * V_0 = NULL;
	Dictionary_2_t520966972 * V_1 = NULL;
	Type_t * V_2 = NULL;
	{
		Il2CppObject ** L_0 = ___obj0;
		NullCheck((*((Il2CppObject **)L_0)));
		Type_t * L_1 = Object_GetType_m2022236990((*((Il2CppObject **)L_0)), /*hidden argument*/NULL);
		V_0 = L_1;
		JsonReaderSettings_t3095433488 * L_2 = __this->get_Settings_8();
		NullCheck(L_2);
		TypeCoercionUtility_t3154211006 * L_3 = L_2->get_Coercion_0();
		Type_t * L_4 = V_0;
		NullCheck(L_3);
		Dictionary_2_t520966972 * L_5 = TypeCoercionUtility_GetMemberMap_m1277459664(L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		V_2 = (Type_t *)NULL;
		Dictionary_2_t520966972 * L_6 = V_1;
		if (L_6)
		{
			goto IL_002a;
		}
	}
	{
		Type_t * L_7 = V_0;
		Type_t * L_8 = JsonReader_GetGenericDictionaryType_m2489104668(__this, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
	}

IL_002a:
	{
		Il2CppObject ** L_9 = ___obj0;
		Type_t * L_10 = V_0;
		Dictionary_2_t520966972 * L_11 = V_1;
		Type_t * L_12 = V_2;
		JsonReader_PopulateObject_m1213129480(__this, L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.JsonReader::ReadObject(System.Type)
extern Il2CppClass* Dictionary_2_t696267445_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m933592675_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m529899303_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3549598589_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m2208877785_MethodInfo_var;
extern const uint32_t JsonReader_ReadObject_m649863795_MetadataUsageId;
extern "C"  Il2CppObject * JsonReader_ReadObject_m649863795 (JsonReader_t386455501 * __this, Type_t * ___objectType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_ReadObject_m649863795_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Dictionary_2_t520966972 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	{
		V_0 = (Type_t *)NULL;
		V_1 = (Dictionary_2_t520966972 *)NULL;
		Type_t * L_0 = ___objectType0;
		if (!L_0)
		{
			goto IL_003d;
		}
	}
	{
		JsonReaderSettings_t3095433488 * L_1 = __this->get_Settings_8();
		NullCheck(L_1);
		TypeCoercionUtility_t3154211006 * L_2 = L_1->get_Coercion_0();
		Type_t * L_3 = ___objectType0;
		NullCheck(L_2);
		Il2CppObject * L_4 = TypeCoercionUtility_InstantiateObject_m3064453651(L_2, L_3, (&V_1), /*hidden argument*/NULL);
		V_2 = L_4;
		List_1_t1244034627 * L_5 = __this->get_previouslyDeserialized_12();
		Il2CppObject * L_6 = V_2;
		NullCheck(L_5);
		List_1_Add_m933592675(L_5, L_6, /*hidden argument*/List_1_Add_m933592675_MethodInfo_var);
		Dictionary_2_t520966972 * L_7 = V_1;
		if (L_7)
		{
			goto IL_0038;
		}
	}
	{
		Type_t * L_8 = ___objectType0;
		Type_t * L_9 = JsonReader_GetGenericDictionaryType_m2489104668(__this, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
	}

IL_0038:
	{
		goto IL_0043;
	}

IL_003d:
	{
		Dictionary_2_t696267445 * L_10 = (Dictionary_2_t696267445 *)il2cpp_codegen_object_new(Dictionary_2_t696267445_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m529899303(L_10, /*hidden argument*/Dictionary_2__ctor_m529899303_MethodInfo_var);
		V_2 = L_10;
	}

IL_0043:
	{
		Il2CppObject * L_11 = V_2;
		V_3 = L_11;
		Type_t * L_12 = ___objectType0;
		Dictionary_2_t520966972 * L_13 = V_1;
		Type_t * L_14 = V_0;
		JsonReader_PopulateObject_m1213129480(__this, (&V_2), L_12, L_13, L_14, /*hidden argument*/NULL);
		Il2CppObject * L_15 = V_3;
		Il2CppObject * L_16 = V_2;
		if ((((Il2CppObject*)(Il2CppObject *)L_15) == ((Il2CppObject*)(Il2CppObject *)L_16)))
		{
			goto IL_006f;
		}
	}
	{
		List_1_t1244034627 * L_17 = __this->get_previouslyDeserialized_12();
		List_1_t1244034627 * L_18 = __this->get_previouslyDeserialized_12();
		NullCheck(L_18);
		int32_t L_19 = List_1_get_Count_m3549598589(L_18, /*hidden argument*/List_1_get_Count_m3549598589_MethodInfo_var);
		NullCheck(L_17);
		List_1_RemoveAt_m2208877785(L_17, ((int32_t)((int32_t)L_19-(int32_t)1)), /*hidden argument*/List_1_RemoveAt_m2208877785_MethodInfo_var);
	}

IL_006f:
	{
		Il2CppObject * L_20 = V_2;
		return L_20;
	}
}
// System.Type Pathfinding.Serialization.JsonFx.JsonReader::GetGenericDictionaryType(System.Type)
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern Il2CppClass* JsonReader_t386455501_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral622120811;
extern const uint32_t JsonReader_GetGenericDictionaryType_m2489104668_MetadataUsageId;
extern "C"  Type_t * JsonReader_GetGenericDictionaryType_m2489104668 (JsonReader_t386455501 * __this, Type_t * ___objectType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_GetGenericDictionaryType_m2489104668_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	TypeU5BU5D_t3339007067* V_1 = NULL;
	{
		Type_t * L_0 = ___objectType0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_1 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_TypeGenericIDictionary_7();
		NullCheck(L_0);
		Type_t * L_2 = Type_GetInterface_m1133348080(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Type_t * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0061;
		}
	}
	{
		Type_t * L_4 = V_0;
		NullCheck(L_4);
		TypeU5BU5D_t3339007067* L_5 = VirtFuncInvoker0< TypeU5BU5D_t3339007067* >::Invoke(92 /* System.Type[] System.Type::GetGenericArguments() */, L_4);
		V_1 = L_5;
		TypeU5BU5D_t3339007067* L_6 = V_1;
		NullCheck(L_6);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_0061;
		}
	}
	{
		TypeU5BU5D_t3339007067* L_7 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		int32_t L_8 = 0;
		Type_t * L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_9) == ((Il2CppObject*)(Type_t *)L_10)))
		{
			goto IL_004b;
		}
	}
	{
		Type_t * L_11 = ___objectType0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral622120811, L_11, /*hidden argument*/NULL);
		int32_t L_13 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_14 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_14, L_12, L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_004b:
	{
		TypeU5BU5D_t3339007067* L_15 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
		int32_t L_16 = 1;
		Type_t * L_17 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_0061;
		}
	}
	{
		TypeU5BU5D_t3339007067* L_19 = V_1;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 1);
		int32_t L_20 = 1;
		Type_t * L_21 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		return L_21;
	}

IL_0061:
	{
		return (Type_t *)NULL;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonReader::PopulateObject(System.Object&,System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>,System.Type)
extern const Il2CppType* Int32_t1153838500_0_0_0_var;
extern Il2CppClass* JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonReader_t386455501_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m404118264_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral264921567;
extern Il2CppCodeGenString* _stringLiteral237437267;
extern Il2CppCodeGenString* _stringLiteral3637437851;
extern Il2CppCodeGenString* _stringLiteral1581823241;
extern Il2CppCodeGenString* _stringLiteral525226562;
extern Il2CppCodeGenString* _stringLiteral2019411;
extern const uint32_t JsonReader_PopulateObject_m1213129480_MetadataUsageId;
extern "C"  void JsonReader_PopulateObject_m1213129480 (JsonReader_t386455501 * __this, Il2CppObject ** ___result0, Type_t * ___objectType1, Dictionary_2_t520966972 * ___memberMap2, Type_t * ___genericDictionaryType3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_PopulateObject_m1213129480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	Type_t * V_2 = NULL;
	MemberInfo_t * V_3 = NULL;
	String_t* V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	int32_t V_6 = 0;
	String_t* G_B15_0 = NULL;
	{
		String_t* L_0 = __this->get_Source_9();
		int32_t L_1 = __this->get_index_11();
		NullCheck(L_0);
		Il2CppChar L_2 = String_get_Chars_m3015341861(L_0, L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)123))))
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_3 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_4 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_4, _stringLiteral264921567, L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0029:
	{
		Il2CppObject ** L_5 = ___result0;
		V_0 = ((Il2CppObject *)IsInst((*((Il2CppObject **)L_5)), IDictionary_t537317817_il2cpp_TypeInfo_var));
		Il2CppObject * L_6 = V_0;
		if (L_6)
		{
			goto IL_005e;
		}
	}
	{
		Type_t * L_7 = ___objectType1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_8 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_TypeGenericIDictionary_7();
		NullCheck(L_7);
		Type_t * L_9 = Type_GetInterface_m1133348080(L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_005e;
		}
	}
	{
		Type_t * L_10 = ___objectType1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral237437267, L_10, /*hidden argument*/NULL);
		int32_t L_12 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_13 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_13, L_11, L_12, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_005e:
	{
		int32_t L_14 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_14+(int32_t)1)));
		int32_t L_15 = __this->get_index_11();
		int32_t L_16 = __this->get_SourceLength_10();
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_008e;
		}
	}
	{
		int32_t L_17 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_18 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_18, _stringLiteral3637437851, L_17, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18);
	}

IL_008e:
	{
		JsonReaderSettings_t3095433488 * L_19 = __this->get_Settings_8();
		NullCheck(L_19);
		bool L_20 = JsonReaderSettings_get_AllowUnquotedObjectKeys_m2993889901(L_19, /*hidden argument*/NULL);
		int32_t L_21 = JsonReader_Tokenize_m1850339476(__this, L_20, /*hidden argument*/NULL);
		V_1 = L_21;
		int32_t L_22 = V_1;
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_00ad;
		}
	}
	{
		goto IL_0234;
	}

IL_00ad:
	{
		int32_t L_23 = V_1;
		if ((((int32_t)L_23) == ((int32_t)((int32_t)9))))
		{
			goto IL_00ce;
		}
	}
	{
		int32_t L_24 = V_1;
		if ((((int32_t)L_24) == ((int32_t)((int32_t)16))))
		{
			goto IL_00ce;
		}
	}
	{
		int32_t L_25 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_26 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_26, _stringLiteral1581823241, L_25, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_26);
	}

IL_00ce:
	{
		int32_t L_27 = V_1;
		if ((!(((uint32_t)L_27) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_00e7;
		}
	}
	{
		Il2CppObject * L_28 = JsonReader_ReadString_m2674412257(__this, (Type_t *)NULL, /*hidden argument*/NULL);
		G_B15_0 = ((String_t*)CastclassSealed(L_28, String_t_il2cpp_TypeInfo_var));
		goto IL_00ed;
	}

IL_00e7:
	{
		String_t* L_29 = JsonReader_ReadUnquotedKey_m1713113799(__this, /*hidden argument*/NULL);
		G_B15_0 = L_29;
	}

IL_00ed:
	{
		V_4 = G_B15_0;
		Type_t * L_30 = ___genericDictionaryType3;
		if (L_30)
		{
			goto IL_010c;
		}
	}
	{
		Dictionary_2_t520966972 * L_31 = ___memberMap2;
		if (!L_31)
		{
			goto IL_010c;
		}
	}
	{
		Dictionary_2_t520966972 * L_32 = ___memberMap2;
		String_t* L_33 = V_4;
		Type_t * L_34 = TypeCoercionUtility_GetMemberInfo_m68305156(NULL /*static, unused*/, L_32, L_33, (&V_3), /*hidden argument*/NULL);
		V_2 = L_34;
		goto IL_0111;
	}

IL_010c:
	{
		Type_t * L_35 = ___genericDictionaryType3;
		V_2 = L_35;
		V_3 = (MemberInfo_t *)NULL;
	}

IL_0111:
	{
		int32_t L_36 = JsonReader_Tokenize_m4160635357(__this, /*hidden argument*/NULL);
		V_1 = L_36;
		int32_t L_37 = V_1;
		if ((((int32_t)L_37) == ((int32_t)((int32_t)14))))
		{
			goto IL_0131;
		}
	}
	{
		int32_t L_38 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_39 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_39, _stringLiteral525226562, L_38, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_39);
	}

IL_0131:
	{
		int32_t L_40 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_40+(int32_t)1)));
		int32_t L_41 = __this->get_index_11();
		int32_t L_42 = __this->get_SourceLength_10();
		if ((((int32_t)L_41) < ((int32_t)L_42)))
		{
			goto IL_0161;
		}
	}
	{
		int32_t L_43 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_44 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_44, _stringLiteral3637437851, L_43, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_44);
	}

IL_0161:
	{
		JsonReaderSettings_t3095433488 * L_45 = __this->get_Settings_8();
		NullCheck(L_45);
		bool L_46 = JsonReaderSettings_get_HandleCyclicReferences_m3959955387(L_45, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_01b5;
		}
	}
	{
		String_t* L_47 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_48 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_47, _stringLiteral2019411, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_01b5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_49 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t1153838500_0_0_0_var), /*hidden argument*/NULL);
		Il2CppObject * L_50 = JsonReader_Read_m1957631563(__this, L_49, (bool)0, /*hidden argument*/NULL);
		V_6 = ((*(int32_t*)((int32_t*)UnBox (L_50, Int32_t1153838500_il2cpp_TypeInfo_var))));
		Il2CppObject ** L_51 = ___result0;
		List_1_t1244034627 * L_52 = __this->get_previouslyDeserialized_12();
		int32_t L_53 = V_6;
		NullCheck(L_52);
		Il2CppObject * L_54 = List_1_get_Item_m404118264(L_52, L_53, /*hidden argument*/List_1_get_Item_m404118264_MethodInfo_var);
		*((Il2CppObject **)(L_51)) = (Il2CppObject *)L_54;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_51), (Il2CppObject *)L_54);
		int32_t L_55 = JsonReader_Tokenize_m4160635357(__this, /*hidden argument*/NULL);
		V_1 = L_55;
		goto IL_022c;
	}

IL_01b5:
	{
		Type_t * L_56 = V_2;
		Il2CppObject * L_57 = JsonReader_Read_m1957631563(__this, L_56, (bool)0, /*hidden argument*/NULL);
		V_5 = L_57;
		Il2CppObject * L_58 = V_0;
		if (!L_58)
		{
			goto IL_020f;
		}
	}
	{
		Type_t * L_59 = ___objectType1;
		if (L_59)
		{
			goto IL_0200;
		}
	}
	{
		JsonReaderSettings_t3095433488 * L_60 = __this->get_Settings_8();
		String_t* L_61 = V_4;
		NullCheck(L_60);
		bool L_62 = JsonReaderSettings_IsTypeHintName_m2115719875(L_60, L_61, /*hidden argument*/NULL);
		if (!L_62)
		{
			goto IL_0200;
		}
	}
	{
		Il2CppObject ** L_63 = ___result0;
		JsonReaderSettings_t3095433488 * L_64 = __this->get_Settings_8();
		NullCheck(L_64);
		TypeCoercionUtility_t3154211006 * L_65 = L_64->get_Coercion_0();
		Il2CppObject * L_66 = V_0;
		Il2CppObject * L_67 = V_5;
		NullCheck(L_65);
		Il2CppObject * L_68 = TypeCoercionUtility_ProcessTypeHint_m421374191(L_65, L_66, ((String_t*)IsInstSealed(L_67, String_t_il2cpp_TypeInfo_var)), (&___objectType1), (&___memberMap2), /*hidden argument*/NULL);
		*((Il2CppObject **)(L_63)) = (Il2CppObject *)L_68;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_63), (Il2CppObject *)L_68);
		goto IL_020a;
	}

IL_0200:
	{
		Il2CppObject * L_69 = V_0;
		String_t* L_70 = V_4;
		Il2CppObject * L_71 = V_5;
		NullCheck(L_69);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, L_69, L_70, L_71);
	}

IL_020a:
	{
		goto IL_0225;
	}

IL_020f:
	{
		JsonReaderSettings_t3095433488 * L_72 = __this->get_Settings_8();
		NullCheck(L_72);
		TypeCoercionUtility_t3154211006 * L_73 = L_72->get_Coercion_0();
		Il2CppObject ** L_74 = ___result0;
		Type_t * L_75 = V_2;
		MemberInfo_t * L_76 = V_3;
		Il2CppObject * L_77 = V_5;
		NullCheck(L_73);
		TypeCoercionUtility_SetMemberValue_m3423425905(L_73, (*((Il2CppObject **)L_74)), L_75, L_76, L_77, /*hidden argument*/NULL);
	}

IL_0225:
	{
		int32_t L_78 = JsonReader_Tokenize_m4160635357(__this, /*hidden argument*/NULL);
		V_1 = L_78;
	}

IL_022c:
	{
		int32_t L_79 = V_1;
		if ((((int32_t)L_79) == ((int32_t)((int32_t)15))))
		{
			goto IL_005e;
		}
	}

IL_0234:
	{
		int32_t L_80 = V_1;
		if ((((int32_t)L_80) == ((int32_t)((int32_t)13))))
		{
			goto IL_024d;
		}
	}
	{
		int32_t L_81 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_82 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_82, _stringLiteral3637437851, L_81, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_82);
	}

IL_024d:
	{
		int32_t L_83 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_83+(int32_t)1)));
		return;
	}
}
// System.Collections.IEnumerable Pathfinding.Serialization.JsonFx.JsonReader::ReadArray(System.Type)
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern Il2CppClass* JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1244034627_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1_get_Count_m2181198093_MethodInfo_var;
extern const MethodInfo* Stack_1_Pop_m865655448_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1239231859_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m2940332446_MethodInfo_var;
extern const MethodInfo* List_1_Add_m933592675_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m1595410604_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3549598589_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m404118264_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m1449333879_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3363527141;
extern Il2CppCodeGenString* _stringLiteral3610865321;
extern const uint32_t JsonReader_ReadArray_m4139571658_MetadataUsageId;
extern "C"  Il2CppObject * JsonReader_ReadArray_m4139571658 (JsonReader_t386455501 * __this, Type_t * ___arrayType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_ReadArray_m4139571658_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	Type_t * V_2 = NULL;
	TypeU5BU5D_t3339007067* V_3 = NULL;
	List_1_t1244034627 * V_4 = NULL;
	int32_t V_5 = 0;
	Il2CppObject * V_6 = NULL;
	Il2CppArray * V_7 = NULL;
	int32_t V_8 = 0;
	List_1_t1244034627 * G_B11_0 = NULL;
	{
		String_t* L_0 = __this->get_Source_9();
		int32_t L_1 = __this->get_index_11();
		NullCheck(L_0);
		Il2CppChar L_2 = String_get_Chars_m3015341861(L_0, L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)91))))
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_3 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_4 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_4, _stringLiteral3363527141, L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0029:
	{
		Type_t * L_5 = ___arrayType0;
		V_0 = (bool)((((int32_t)((((Il2CppObject*)(Type_t *)L_5) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_6 = V_0;
		V_1 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		V_2 = (Type_t *)NULL;
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0074;
		}
	}
	{
		Type_t * L_8 = ___arrayType0;
		NullCheck(L_8);
		bool L_9 = Type_get_HasElementType_m4257202252(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0055;
		}
	}
	{
		Type_t * L_10 = ___arrayType0;
		NullCheck(L_10);
		Type_t * L_11 = VirtFuncInvoker0< Type_t * >::Invoke(46 /* System.Type System.Type::GetElementType() */, L_10);
		V_2 = L_11;
		goto IL_0074;
	}

IL_0055:
	{
		Type_t * L_12 = ___arrayType0;
		NullCheck(L_12);
		bool L_13 = VirtFuncInvoker0< bool >::Invoke(96 /* System.Boolean System.Type::get_IsGenericType() */, L_12);
		if (!L_13)
		{
			goto IL_0074;
		}
	}
	{
		Type_t * L_14 = ___arrayType0;
		NullCheck(L_14);
		TypeU5BU5D_t3339007067* L_15 = VirtFuncInvoker0< TypeU5BU5D_t3339007067* >::Invoke(92 /* System.Type[] System.Type::GetGenericArguments() */, L_14);
		V_3 = L_15;
		TypeU5BU5D_t3339007067* L_16 = V_3;
		NullCheck(L_16);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_0074;
		}
	}
	{
		TypeU5BU5D_t3339007067* L_17 = V_3;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 0);
		int32_t L_18 = 0;
		Type_t * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		V_2 = L_19;
	}

IL_0074:
	{
		Stack_1_t47628255 * L_20 = __this->get_jsArrays_13();
		NullCheck(L_20);
		int32_t L_21 = Stack_1_get_Count_m2181198093(L_20, /*hidden argument*/Stack_1_get_Count_m2181198093_MethodInfo_var);
		if ((((int32_t)L_21) <= ((int32_t)0)))
		{
			goto IL_0095;
		}
	}
	{
		Stack_1_t47628255 * L_22 = __this->get_jsArrays_13();
		NullCheck(L_22);
		List_1_t1244034627 * L_23 = Stack_1_Pop_m865655448(L_22, /*hidden argument*/Stack_1_Pop_m865655448_MethodInfo_var);
		G_B11_0 = L_23;
		goto IL_009a;
	}

IL_0095:
	{
		List_1_t1244034627 * L_24 = (List_1_t1244034627 *)il2cpp_codegen_object_new(List_1_t1244034627_il2cpp_TypeInfo_var);
		List_1__ctor_m1239231859(L_24, /*hidden argument*/List_1__ctor_m1239231859_MethodInfo_var);
		G_B11_0 = L_24;
	}

IL_009a:
	{
		V_4 = G_B11_0;
		List_1_t1244034627 * L_25 = V_4;
		NullCheck(L_25);
		List_1_Clear_m2940332446(L_25, /*hidden argument*/List_1_Clear_m2940332446_MethodInfo_var);
	}

IL_00a3:
	{
		int32_t L_26 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_26+(int32_t)1)));
		int32_t L_27 = __this->get_index_11();
		int32_t L_28 = __this->get_SourceLength_10();
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_00d3;
		}
	}
	{
		int32_t L_29 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_30 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_30, _stringLiteral3610865321, L_29, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_30);
	}

IL_00d3:
	{
		int32_t L_31 = JsonReader_Tokenize_m4160635357(__this, /*hidden argument*/NULL);
		V_5 = L_31;
		int32_t L_32 = V_5;
		if ((!(((uint32_t)L_32) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_00e9;
		}
	}
	{
		goto IL_017e;
	}

IL_00e9:
	{
		Type_t * L_33 = V_2;
		bool L_34 = V_1;
		Il2CppObject * L_35 = JsonReader_Read_m1957631563(__this, L_33, L_34, /*hidden argument*/NULL);
		V_6 = L_35;
		List_1_t1244034627 * L_36 = V_4;
		Il2CppObject * L_37 = V_6;
		NullCheck(L_36);
		List_1_Add_m933592675(L_36, L_37, /*hidden argument*/List_1_Add_m933592675_MethodInfo_var);
		Il2CppObject * L_38 = V_6;
		if (L_38)
		{
			goto IL_011d;
		}
	}
	{
		Type_t * L_39 = V_2;
		if (!L_39)
		{
			goto IL_0116;
		}
	}
	{
		Type_t * L_40 = V_2;
		NullCheck(L_40);
		bool L_41 = Type_get_IsValueType_m1914757235(L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0116;
		}
	}
	{
		V_2 = (Type_t *)NULL;
	}

IL_0116:
	{
		V_0 = (bool)1;
		goto IL_016d;
	}

IL_011d:
	{
		Type_t * L_42 = V_2;
		if (!L_42)
		{
			goto IL_015d;
		}
	}
	{
		Type_t * L_43 = V_2;
		Il2CppObject * L_44 = V_6;
		NullCheck(L_44);
		Type_t * L_45 = Object_GetType_m2022236990(L_44, /*hidden argument*/NULL);
		NullCheck(L_43);
		bool L_46 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_43, L_45);
		if (L_46)
		{
			goto IL_015d;
		}
	}
	{
		Il2CppObject * L_47 = V_6;
		NullCheck(L_47);
		Type_t * L_48 = Object_GetType_m2022236990(L_47, /*hidden argument*/NULL);
		Type_t * L_49 = V_2;
		NullCheck(L_48);
		bool L_50 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_48, L_49);
		if (!L_50)
		{
			goto IL_0154;
		}
	}
	{
		Il2CppObject * L_51 = V_6;
		NullCheck(L_51);
		Type_t * L_52 = Object_GetType_m2022236990(L_51, /*hidden argument*/NULL);
		V_2 = L_52;
		goto IL_0158;
	}

IL_0154:
	{
		V_2 = (Type_t *)NULL;
		V_0 = (bool)1;
	}

IL_0158:
	{
		goto IL_016d;
	}

IL_015d:
	{
		bool L_53 = V_0;
		if (L_53)
		{
			goto IL_016d;
		}
	}
	{
		Il2CppObject * L_54 = V_6;
		NullCheck(L_54);
		Type_t * L_55 = Object_GetType_m2022236990(L_54, /*hidden argument*/NULL);
		V_2 = L_55;
		V_0 = (bool)1;
	}

IL_016d:
	{
		int32_t L_56 = JsonReader_Tokenize_m4160635357(__this, /*hidden argument*/NULL);
		V_5 = L_56;
		int32_t L_57 = V_5;
		if ((((int32_t)L_57) == ((int32_t)((int32_t)15))))
		{
			goto IL_00a3;
		}
	}

IL_017e:
	{
		int32_t L_58 = V_5;
		if ((((int32_t)L_58) == ((int32_t)((int32_t)11))))
		{
			goto IL_0198;
		}
	}
	{
		int32_t L_59 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_60 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_60, _stringLiteral3610865321, L_59, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_60);
	}

IL_0198:
	{
		int32_t L_61 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_61+(int32_t)1)));
		Stack_1_t47628255 * L_62 = __this->get_jsArrays_13();
		List_1_t1244034627 * L_63 = V_4;
		NullCheck(L_62);
		Stack_1_Push_m1595410604(L_62, L_63, /*hidden argument*/Stack_1_Push_m1595410604_MethodInfo_var);
		Type_t * L_64 = V_2;
		if (!L_64)
		{
			goto IL_0209;
		}
	}
	{
		Type_t * L_65 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_66 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_65) == ((Il2CppObject*)(Type_t *)L_66)))
		{
			goto IL_0209;
		}
	}
	{
		Type_t * L_67 = V_2;
		List_1_t1244034627 * L_68 = V_4;
		NullCheck(L_68);
		int32_t L_69 = List_1_get_Count_m3549598589(L_68, /*hidden argument*/List_1_get_Count_m3549598589_MethodInfo_var);
		Il2CppArray * L_70 = Array_CreateInstance_m1364223436(NULL /*static, unused*/, L_67, L_69, /*hidden argument*/NULL);
		V_7 = L_70;
		V_8 = 0;
		goto IL_01f8;
	}

IL_01e0:
	{
		Il2CppArray * L_71 = V_7;
		List_1_t1244034627 * L_72 = V_4;
		int32_t L_73 = V_8;
		NullCheck(L_72);
		Il2CppObject * L_74 = List_1_get_Item_m404118264(L_72, L_73, /*hidden argument*/List_1_get_Item_m404118264_MethodInfo_var);
		int32_t L_75 = V_8;
		NullCheck(L_71);
		Array_SetValue_m3564402974(L_71, L_74, L_75, /*hidden argument*/NULL);
		int32_t L_76 = V_8;
		V_8 = ((int32_t)((int32_t)L_76+(int32_t)1));
	}

IL_01f8:
	{
		int32_t L_77 = V_8;
		List_1_t1244034627 * L_78 = V_4;
		NullCheck(L_78);
		int32_t L_79 = List_1_get_Count_m3549598589(L_78, /*hidden argument*/List_1_get_Count_m3549598589_MethodInfo_var);
		if ((((int32_t)L_77) < ((int32_t)L_79)))
		{
			goto IL_01e0;
		}
	}
	{
		Il2CppArray * L_80 = V_7;
		return L_80;
	}

IL_0209:
	{
		List_1_t1244034627 * L_81 = V_4;
		NullCheck(L_81);
		ObjectU5BU5D_t1108656482* L_82 = List_1_ToArray_m1449333879(L_81, /*hidden argument*/List_1_ToArray_m1449333879_MethodInfo_var);
		return (Il2CppObject *)L_82;
	}
}
// System.String Pathfinding.Serialization.JsonFx.JsonReader::ReadUnquotedKey()
extern "C"  String_t* JsonReader_ReadUnquotedKey_m1713113799 (JsonReader_t386455501 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_index_11();
		V_0 = L_0;
	}

IL_0007:
	{
		int32_t L_1 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_1+(int32_t)1)));
		int32_t L_2 = JsonReader_Tokenize_m1850339476(__this, (bool)1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)16))))
		{
			goto IL_0007;
		}
	}
	{
		String_t* L_3 = __this->get_Source_9();
		int32_t L_4 = V_0;
		int32_t L_5 = __this->get_index_11();
		int32_t L_6 = V_0;
		NullCheck(L_3);
		String_t* L_7 = String_Substring_m675079568(L_3, L_4, ((int32_t)((int32_t)L_5-(int32_t)L_6)), /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.JsonReader::ReadString(System.Type)
extern const Il2CppType* String_t_0_0_0_var;
extern Il2CppClass* JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppClass* NumberFormatInfo_t1637637232_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral42811213;
extern Il2CppCodeGenString* _stringLiteral3415327497;
extern const uint32_t JsonReader_ReadString_m2674412257_MetadataUsageId;
extern "C"  Il2CppObject * JsonReader_ReadString_m2674412257 (JsonReader_t386455501 * __this, Type_t * ___expectedType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_ReadString_m2674412257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppChar V_0 = 0x0;
	int32_t V_1 = 0;
	StringBuilder_t243639308 * V_2 = NULL;
	Il2CppChar V_3 = 0x0;
	int32_t V_4 = 0;
	{
		String_t* L_0 = __this->get_Source_9();
		int32_t L_1 = __this->get_index_11();
		NullCheck(L_0);
		Il2CppChar L_2 = String_get_Chars_m3015341861(L_0, L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)34))))
		{
			goto IL_0041;
		}
	}
	{
		String_t* L_3 = __this->get_Source_9();
		int32_t L_4 = __this->get_index_11();
		NullCheck(L_3);
		Il2CppChar L_5 = String_get_Chars_m3015341861(L_3, L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)((int32_t)39))))
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_7 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_7, _stringLiteral42811213, L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0041:
	{
		String_t* L_8 = __this->get_Source_9();
		int32_t L_9 = __this->get_index_11();
		NullCheck(L_8);
		Il2CppChar L_10 = String_get_Chars_m3015341861(L_8, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		int32_t L_11 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_11+(int32_t)1)));
		int32_t L_12 = __this->get_index_11();
		int32_t L_13 = __this->get_SourceLength_10();
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_14 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_15 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_15, _stringLiteral3415327497, L_14, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_0083:
	{
		int32_t L_16 = __this->get_index_11();
		V_1 = L_16;
		StringBuilder_t243639308 * L_17 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_17, /*hidden argument*/NULL);
		V_2 = L_17;
		goto IL_0299;
	}

IL_0095:
	{
		String_t* L_18 = __this->get_Source_9();
		int32_t L_19 = __this->get_index_11();
		NullCheck(L_18);
		Il2CppChar L_20 = String_get_Chars_m3015341861(L_18, L_19, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_0269;
		}
	}
	{
		StringBuilder_t243639308 * L_21 = V_2;
		String_t* L_22 = __this->get_Source_9();
		int32_t L_23 = V_1;
		int32_t L_24 = __this->get_index_11();
		int32_t L_25 = V_1;
		NullCheck(L_21);
		StringBuilder_Append_m2996071419(L_21, L_22, L_23, ((int32_t)((int32_t)L_24-(int32_t)L_25)), /*hidden argument*/NULL);
		int32_t L_26 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_26+(int32_t)1)));
		int32_t L_27 = __this->get_index_11();
		int32_t L_28 = __this->get_SourceLength_10();
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_00f3;
		}
	}
	{
		int32_t L_29 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_30 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_30, _stringLiteral3415327497, L_29, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_30);
	}

IL_00f3:
	{
		String_t* L_31 = __this->get_Source_9();
		int32_t L_32 = __this->get_index_11();
		NullCheck(L_31);
		Il2CppChar L_33 = String_get_Chars_m3015341861(L_31, L_32, /*hidden argument*/NULL);
		V_3 = L_33;
		Il2CppChar L_34 = V_3;
		if (((int32_t)((int32_t)L_34-(int32_t)((int32_t)110))) == 0)
		{
			goto IL_016b;
		}
		if (((int32_t)((int32_t)L_34-(int32_t)((int32_t)110))) == 1)
		{
			goto IL_012e;
		}
		if (((int32_t)((int32_t)L_34-(int32_t)((int32_t)110))) == 2)
		{
			goto IL_012e;
		}
		if (((int32_t)((int32_t)L_34-(int32_t)((int32_t)110))) == 3)
		{
			goto IL_012e;
		}
		if (((int32_t)((int32_t)L_34-(int32_t)((int32_t)110))) == 4)
		{
			goto IL_0179;
		}
		if (((int32_t)((int32_t)L_34-(int32_t)((int32_t)110))) == 5)
		{
			goto IL_012e;
		}
		if (((int32_t)((int32_t)L_34-(int32_t)((int32_t)110))) == 6)
		{
			goto IL_0187;
		}
		if (((int32_t)((int32_t)L_34-(int32_t)((int32_t)110))) == 7)
		{
			goto IL_0195;
		}
	}

IL_012e:
	{
		Il2CppChar L_35 = V_3;
		if ((((int32_t)L_35) == ((int32_t)((int32_t)48))))
		{
			goto IL_014b;
		}
	}
	{
		Il2CppChar L_36 = V_3;
		if ((((int32_t)L_36) == ((int32_t)((int32_t)98))))
		{
			goto IL_0150;
		}
	}
	{
		Il2CppChar L_37 = V_3;
		if ((((int32_t)L_37) == ((int32_t)((int32_t)102))))
		{
			goto IL_015d;
		}
	}
	{
		goto IL_0210;
	}

IL_014b:
	{
		goto IL_022d;
	}

IL_0150:
	{
		StringBuilder_t243639308 * L_38 = V_2;
		NullCheck(L_38);
		StringBuilder_Append_m2143093878(L_38, 8, /*hidden argument*/NULL);
		goto IL_022d;
	}

IL_015d:
	{
		StringBuilder_t243639308 * L_39 = V_2;
		NullCheck(L_39);
		StringBuilder_Append_m2143093878(L_39, ((int32_t)12), /*hidden argument*/NULL);
		goto IL_022d;
	}

IL_016b:
	{
		StringBuilder_t243639308 * L_40 = V_2;
		NullCheck(L_40);
		StringBuilder_Append_m2143093878(L_40, ((int32_t)10), /*hidden argument*/NULL);
		goto IL_022d;
	}

IL_0179:
	{
		StringBuilder_t243639308 * L_41 = V_2;
		NullCheck(L_41);
		StringBuilder_Append_m2143093878(L_41, ((int32_t)13), /*hidden argument*/NULL);
		goto IL_022d;
	}

IL_0187:
	{
		StringBuilder_t243639308 * L_42 = V_2;
		NullCheck(L_42);
		StringBuilder_Append_m2143093878(L_42, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_022d;
	}

IL_0195:
	{
		int32_t L_43 = __this->get_index_11();
		int32_t L_44 = __this->get_SourceLength_10();
		if ((((int32_t)((int32_t)((int32_t)L_43+(int32_t)4))) >= ((int32_t)L_44)))
		{
			goto IL_01f3;
		}
	}
	{
		String_t* L_45 = __this->get_Source_9();
		int32_t L_46 = __this->get_index_11();
		NullCheck(L_45);
		String_t* L_47 = String_Substring_m675079568(L_45, ((int32_t)((int32_t)L_46+(int32_t)1)), 4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t1637637232_il2cpp_TypeInfo_var);
		NumberFormatInfo_t1637637232 * L_48 = NumberFormatInfo_get_InvariantInfo_m1900501910(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_49 = Int32_TryParse_m2457543725(NULL /*static, unused*/, L_47, ((int32_t)512), L_48, (&V_4), /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_01f3;
		}
	}
	{
		StringBuilder_t243639308 * L_50 = V_2;
		int32_t L_51 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		String_t* L_52 = Char_ConvertFromUtf32_m567781788(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		NullCheck(L_50);
		StringBuilder_Append_m3898090075(L_50, L_52, /*hidden argument*/NULL);
		int32_t L_53 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_53+(int32_t)4)));
		goto IL_020b;
	}

IL_01f3:
	{
		StringBuilder_t243639308 * L_54 = V_2;
		String_t* L_55 = __this->get_Source_9();
		int32_t L_56 = __this->get_index_11();
		NullCheck(L_55);
		Il2CppChar L_57 = String_get_Chars_m3015341861(L_55, L_56, /*hidden argument*/NULL);
		NullCheck(L_54);
		StringBuilder_Append_m2143093878(L_54, L_57, /*hidden argument*/NULL);
	}

IL_020b:
	{
		goto IL_022d;
	}

IL_0210:
	{
		StringBuilder_t243639308 * L_58 = V_2;
		String_t* L_59 = __this->get_Source_9();
		int32_t L_60 = __this->get_index_11();
		NullCheck(L_59);
		Il2CppChar L_61 = String_get_Chars_m3015341861(L_59, L_60, /*hidden argument*/NULL);
		NullCheck(L_58);
		StringBuilder_Append_m2143093878(L_58, L_61, /*hidden argument*/NULL);
		goto IL_022d;
	}

IL_022d:
	{
		int32_t L_62 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_62+(int32_t)1)));
		int32_t L_63 = __this->get_index_11();
		int32_t L_64 = __this->get_SourceLength_10();
		if ((((int32_t)L_63) < ((int32_t)L_64)))
		{
			goto IL_025d;
		}
	}
	{
		int32_t L_65 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_66 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_66, _stringLiteral3415327497, L_65, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_66);
	}

IL_025d:
	{
		int32_t L_67 = __this->get_index_11();
		V_1 = L_67;
		goto IL_0299;
	}

IL_0269:
	{
		int32_t L_68 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_68+(int32_t)1)));
		int32_t L_69 = __this->get_index_11();
		int32_t L_70 = __this->get_SourceLength_10();
		if ((((int32_t)L_69) < ((int32_t)L_70)))
		{
			goto IL_0299;
		}
	}
	{
		int32_t L_71 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_72 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_72, _stringLiteral3415327497, L_71, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_72);
	}

IL_0299:
	{
		String_t* L_73 = __this->get_Source_9();
		int32_t L_74 = __this->get_index_11();
		NullCheck(L_73);
		Il2CppChar L_75 = String_get_Chars_m3015341861(L_73, L_74, /*hidden argument*/NULL);
		Il2CppChar L_76 = V_0;
		if ((!(((uint32_t)L_75) == ((uint32_t)L_76))))
		{
			goto IL_0095;
		}
	}
	{
		StringBuilder_t243639308 * L_77 = V_2;
		String_t* L_78 = __this->get_Source_9();
		int32_t L_79 = V_1;
		int32_t L_80 = __this->get_index_11();
		int32_t L_81 = V_1;
		NullCheck(L_77);
		StringBuilder_Append_m2996071419(L_77, L_78, L_79, ((int32_t)((int32_t)L_80-(int32_t)L_81)), /*hidden argument*/NULL);
		int32_t L_82 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_82+(int32_t)1)));
		Type_t * L_83 = ___expectedType0;
		if (!L_83)
		{
			goto IL_0302;
		}
	}
	{
		Type_t * L_84 = ___expectedType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_85 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_84) == ((Il2CppObject*)(Type_t *)L_85)))
		{
			goto IL_0302;
		}
	}
	{
		JsonReaderSettings_t3095433488 * L_86 = __this->get_Settings_8();
		NullCheck(L_86);
		TypeCoercionUtility_t3154211006 * L_87 = L_86->get_Coercion_0();
		Type_t * L_88 = ___expectedType0;
		StringBuilder_t243639308 * L_89 = V_2;
		NullCheck(L_89);
		String_t* L_90 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_89);
		NullCheck(L_87);
		Il2CppObject * L_91 = TypeCoercionUtility_CoerceType_m3345524962(L_87, L_88, L_90, /*hidden argument*/NULL);
		return L_91;
	}

IL_0302:
	{
		StringBuilder_t243639308 * L_92 = V_2;
		NullCheck(L_92);
		String_t* L_93 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_92);
		return L_93;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.JsonReader::ReadNumber(System.Type)
extern const Il2CppType* Decimal_t1954350631_0_0_0_var;
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var;
extern Il2CppClass* NumberFormatInfo_t1637637232_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t1954350631_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral222848855;
extern const uint32_t JsonReader_ReadNumber_m757648041_MetadataUsageId;
extern "C"  Il2CppObject * JsonReader_ReadNumber_m757648041 (JsonReader_t386455501 * __this, Type_t * ___expectedType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_ReadNumber_m757648041_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	String_t* V_6 = NULL;
	Decimal_t1954350631  V_7;
	memset(&V_7, 0, sizeof(V_7));
	double V_8 = 0.0;
	int32_t G_B19_0 = 0;
	int32_t G_B18_0 = 0;
	int32_t G_B20_0 = 0;
	int32_t G_B20_1 = 0;
	{
		V_0 = (bool)0;
		V_1 = (bool)0;
		int32_t L_0 = __this->get_index_11();
		V_2 = L_0;
		V_3 = 0;
		V_4 = 0;
		String_t* L_1 = __this->get_Source_9();
		int32_t L_2 = __this->get_index_11();
		NullCheck(L_1);
		Il2CppChar L_3 = String_get_Chars_m3015341861(L_1, L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)45)))))
		{
			goto IL_0073;
		}
	}
	{
		int32_t L_4 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = __this->get_index_11();
		int32_t L_6 = __this->get_SourceLength_10();
		if ((((int32_t)L_5) >= ((int32_t)L_6)))
		{
			goto IL_0062;
		}
	}
	{
		String_t* L_7 = __this->get_Source_9();
		int32_t L_8 = __this->get_index_11();
		NullCheck(L_7);
		Il2CppChar L_9 = String_get_Chars_m3015341861(L_7, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_10 = Char_IsDigit_m1743537211(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0073;
		}
	}

IL_0062:
	{
		int32_t L_11 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_12 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_12, _stringLiteral222848855, L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0073:
	{
		goto IL_0086;
	}

IL_0078:
	{
		int32_t L_13 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_13+(int32_t)1)));
	}

IL_0086:
	{
		int32_t L_14 = __this->get_index_11();
		int32_t L_15 = __this->get_SourceLength_10();
		if ((((int32_t)L_14) >= ((int32_t)L_15)))
		{
			goto IL_00b2;
		}
	}
	{
		String_t* L_16 = __this->get_Source_9();
		int32_t L_17 = __this->get_index_11();
		NullCheck(L_16);
		Il2CppChar L_18 = String_get_Chars_m3015341861(L_16, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_19 = Char_IsDigit_m1743537211(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0078;
		}
	}

IL_00b2:
	{
		int32_t L_20 = __this->get_index_11();
		int32_t L_21 = __this->get_SourceLength_10();
		if ((((int32_t)L_20) >= ((int32_t)L_21)))
		{
			goto IL_0167;
		}
	}
	{
		String_t* L_22 = __this->get_Source_9();
		int32_t L_23 = __this->get_index_11();
		NullCheck(L_22);
		Il2CppChar L_24 = String_get_Chars_m3015341861(L_22, L_23, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_24) == ((uint32_t)((int32_t)46)))))
		{
			goto IL_0167;
		}
	}
	{
		V_0 = (bool)1;
		int32_t L_25 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_25+(int32_t)1)));
		int32_t L_26 = __this->get_index_11();
		int32_t L_27 = __this->get_SourceLength_10();
		if ((((int32_t)L_26) >= ((int32_t)L_27)))
		{
			goto IL_0117;
		}
	}
	{
		String_t* L_28 = __this->get_Source_9();
		int32_t L_29 = __this->get_index_11();
		NullCheck(L_28);
		Il2CppChar L_30 = String_get_Chars_m3015341861(L_28, L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_31 = Char_IsDigit_m1743537211(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_0128;
		}
	}

IL_0117:
	{
		int32_t L_32 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_33 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_33, _stringLiteral222848855, L_32, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_33);
	}

IL_0128:
	{
		goto IL_013b;
	}

IL_012d:
	{
		int32_t L_34 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_34+(int32_t)1)));
	}

IL_013b:
	{
		int32_t L_35 = __this->get_index_11();
		int32_t L_36 = __this->get_SourceLength_10();
		if ((((int32_t)L_35) >= ((int32_t)L_36)))
		{
			goto IL_0167;
		}
	}
	{
		String_t* L_37 = __this->get_Source_9();
		int32_t L_38 = __this->get_index_11();
		NullCheck(L_37);
		Il2CppChar L_39 = String_get_Chars_m3015341861(L_37, L_38, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_40 = Char_IsDigit_m1743537211(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		if (L_40)
		{
			goto IL_012d;
		}
	}

IL_0167:
	{
		int32_t L_41 = __this->get_index_11();
		int32_t L_42 = V_2;
		bool L_43 = V_0;
		G_B18_0 = ((int32_t)((int32_t)L_41-(int32_t)L_42));
		if (!L_43)
		{
			G_B19_0 = ((int32_t)((int32_t)L_41-(int32_t)L_42));
			goto IL_017b;
		}
	}
	{
		G_B20_0 = 1;
		G_B20_1 = G_B18_0;
		goto IL_017c;
	}

IL_017b:
	{
		G_B20_0 = 0;
		G_B20_1 = G_B19_0;
	}

IL_017c:
	{
		V_3 = ((int32_t)((int32_t)G_B20_1-(int32_t)G_B20_0));
		int32_t L_44 = __this->get_index_11();
		int32_t L_45 = __this->get_SourceLength_10();
		if ((((int32_t)L_44) >= ((int32_t)L_45)))
		{
			goto IL_0308;
		}
	}
	{
		String_t* L_46 = __this->get_Source_9();
		int32_t L_47 = __this->get_index_11();
		NullCheck(L_46);
		Il2CppChar L_48 = String_get_Chars_m3015341861(L_46, L_47, /*hidden argument*/NULL);
		if ((((int32_t)L_48) == ((int32_t)((int32_t)101))))
		{
			goto IL_01bf;
		}
	}
	{
		String_t* L_49 = __this->get_Source_9();
		int32_t L_50 = __this->get_index_11();
		NullCheck(L_49);
		Il2CppChar L_51 = String_get_Chars_m3015341861(L_49, L_50, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_51) == ((uint32_t)((int32_t)69)))))
		{
			goto IL_0308;
		}
	}

IL_01bf:
	{
		V_1 = (bool)1;
		int32_t L_52 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_52+(int32_t)1)));
		int32_t L_53 = __this->get_index_11();
		int32_t L_54 = __this->get_SourceLength_10();
		if ((((int32_t)L_53) < ((int32_t)L_54)))
		{
			goto IL_01f1;
		}
	}
	{
		int32_t L_55 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_56 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_56, _stringLiteral222848855, L_55, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_56);
	}

IL_01f1:
	{
		int32_t L_57 = __this->get_index_11();
		V_5 = L_57;
		String_t* L_58 = __this->get_Source_9();
		int32_t L_59 = __this->get_index_11();
		NullCheck(L_58);
		Il2CppChar L_60 = String_get_Chars_m3015341861(L_58, L_59, /*hidden argument*/NULL);
		if ((((int32_t)L_60) == ((int32_t)((int32_t)45))))
		{
			goto IL_0229;
		}
	}
	{
		String_t* L_61 = __this->get_Source_9();
		int32_t L_62 = __this->get_index_11();
		NullCheck(L_61);
		Il2CppChar L_63 = String_get_Chars_m3015341861(L_61, L_62, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_63) == ((uint32_t)((int32_t)43)))))
		{
			goto IL_0279;
		}
	}

IL_0229:
	{
		int32_t L_64 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_64+(int32_t)1)));
		int32_t L_65 = __this->get_index_11();
		int32_t L_66 = __this->get_SourceLength_10();
		if ((((int32_t)L_65) >= ((int32_t)L_66)))
		{
			goto IL_0263;
		}
	}
	{
		String_t* L_67 = __this->get_Source_9();
		int32_t L_68 = __this->get_index_11();
		NullCheck(L_67);
		Il2CppChar L_69 = String_get_Chars_m3015341861(L_67, L_68, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_70 = Char_IsDigit_m1743537211(NULL /*static, unused*/, L_69, /*hidden argument*/NULL);
		if (L_70)
		{
			goto IL_0274;
		}
	}

IL_0263:
	{
		int32_t L_71 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_72 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_72, _stringLiteral222848855, L_71, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_72);
	}

IL_0274:
	{
		goto IL_02a5;
	}

IL_0279:
	{
		String_t* L_73 = __this->get_Source_9();
		int32_t L_74 = __this->get_index_11();
		NullCheck(L_73);
		Il2CppChar L_75 = String_get_Chars_m3015341861(L_73, L_74, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_76 = Char_IsDigit_m1743537211(NULL /*static, unused*/, L_75, /*hidden argument*/NULL);
		if (L_76)
		{
			goto IL_02a5;
		}
	}
	{
		int32_t L_77 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_78 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_78, _stringLiteral222848855, L_77, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_78);
	}

IL_02a5:
	{
		goto IL_02b8;
	}

IL_02aa:
	{
		int32_t L_79 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_79+(int32_t)1)));
	}

IL_02b8:
	{
		int32_t L_80 = __this->get_index_11();
		int32_t L_81 = __this->get_SourceLength_10();
		if ((((int32_t)L_80) >= ((int32_t)L_81)))
		{
			goto IL_02e4;
		}
	}
	{
		String_t* L_82 = __this->get_Source_9();
		int32_t L_83 = __this->get_index_11();
		NullCheck(L_82);
		Il2CppChar L_84 = String_get_Chars_m3015341861(L_82, L_83, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_85 = Char_IsDigit_m1743537211(NULL /*static, unused*/, L_84, /*hidden argument*/NULL);
		if (L_85)
		{
			goto IL_02aa;
		}
	}

IL_02e4:
	{
		String_t* L_86 = __this->get_Source_9();
		int32_t L_87 = V_5;
		int32_t L_88 = __this->get_index_11();
		int32_t L_89 = V_5;
		NullCheck(L_86);
		String_t* L_90 = String_Substring_m675079568(L_86, L_87, ((int32_t)((int32_t)L_88-(int32_t)L_89)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t1637637232_il2cpp_TypeInfo_var);
		NumberFormatInfo_t1637637232 * L_91 = NumberFormatInfo_get_InvariantInfo_m1900501910(NULL /*static, unused*/, /*hidden argument*/NULL);
		Int32_TryParse_m2457543725(NULL /*static, unused*/, L_90, 7, L_91, (&V_4), /*hidden argument*/NULL);
	}

IL_0308:
	{
		String_t* L_92 = __this->get_Source_9();
		int32_t L_93 = V_2;
		int32_t L_94 = __this->get_index_11();
		int32_t L_95 = V_2;
		NullCheck(L_92);
		String_t* L_96 = String_Substring_m675079568(L_92, L_93, ((int32_t)((int32_t)L_94-(int32_t)L_95)), /*hidden argument*/NULL);
		V_6 = L_96;
		bool L_97 = V_0;
		if (L_97)
		{
			goto IL_03e2;
		}
	}
	{
		bool L_98 = V_1;
		if (L_98)
		{
			goto IL_03e2;
		}
	}
	{
		int32_t L_99 = V_3;
		if ((((int32_t)L_99) >= ((int32_t)((int32_t)19))))
		{
			goto IL_03e2;
		}
	}
	{
		String_t* L_100 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t1637637232_il2cpp_TypeInfo_var);
		NumberFormatInfo_t1637637232 * L_101 = NumberFormatInfo_get_InvariantInfo_m1900501910(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		Decimal_t1954350631  L_102 = Decimal_Parse_m2075137301(NULL /*static, unused*/, L_100, 7, L_101, /*hidden argument*/NULL);
		V_7 = L_102;
		Type_t * L_103 = ___expectedType0;
		if (!L_103)
		{
			goto IL_0360;
		}
	}
	{
		JsonReaderSettings_t3095433488 * L_104 = __this->get_Settings_8();
		NullCheck(L_104);
		TypeCoercionUtility_t3154211006 * L_105 = L_104->get_Coercion_0();
		Type_t * L_106 = ___expectedType0;
		Decimal_t1954350631  L_107 = V_7;
		Decimal_t1954350631  L_108 = L_107;
		Il2CppObject * L_109 = Box(Decimal_t1954350631_il2cpp_TypeInfo_var, &L_108);
		NullCheck(L_105);
		Il2CppObject * L_110 = TypeCoercionUtility_CoerceType_m3345524962(L_105, L_106, L_109, /*hidden argument*/NULL);
		return L_110;
	}

IL_0360:
	{
		Decimal_t1954350631  L_111 = V_7;
		Decimal_t1954350631  L_112;
		memset(&L_112, 0, sizeof(L_112));
		Decimal__ctor_m3224507889(&L_112, ((int32_t)-2147483648LL), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		bool L_113 = Decimal_op_GreaterThanOrEqual_m2249474966(NULL /*static, unused*/, L_111, L_112, /*hidden argument*/NULL);
		if (!L_113)
		{
			goto IL_0399;
		}
	}
	{
		Decimal_t1954350631  L_114 = V_7;
		Decimal_t1954350631  L_115;
		memset(&L_115, 0, sizeof(L_115));
		Decimal__ctor_m3224507889(&L_115, ((int32_t)2147483647LL), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		bool L_116 = Decimal_op_LessThanOrEqual_m3623476551(NULL /*static, unused*/, L_114, L_115, /*hidden argument*/NULL);
		if (!L_116)
		{
			goto IL_0399;
		}
	}
	{
		Decimal_t1954350631  L_117 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		int32_t L_118 = Decimal_op_Explicit_m2040523874(NULL /*static, unused*/, L_117, /*hidden argument*/NULL);
		int32_t L_119 = L_118;
		Il2CppObject * L_120 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_119);
		return L_120;
	}

IL_0399:
	{
		Decimal_t1954350631  L_121 = V_7;
		Decimal_t1954350631  L_122;
		memset(&L_122, 0, sizeof(L_122));
		Decimal__ctor_m3224510834(&L_122, ((int64_t)std::numeric_limits<int64_t>::min()), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		bool L_123 = Decimal_op_GreaterThanOrEqual_m2249474966(NULL /*static, unused*/, L_121, L_122, /*hidden argument*/NULL);
		if (!L_123)
		{
			goto IL_03da;
		}
	}
	{
		Decimal_t1954350631  L_124 = V_7;
		Decimal_t1954350631  L_125;
		memset(&L_125, 0, sizeof(L_125));
		Decimal__ctor_m3224510834(&L_125, ((int64_t)std::numeric_limits<int64_t>::max()), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		bool L_126 = Decimal_op_LessThanOrEqual_m3623476551(NULL /*static, unused*/, L_124, L_125, /*hidden argument*/NULL);
		if (!L_126)
		{
			goto IL_03da;
		}
	}
	{
		Decimal_t1954350631  L_127 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		int64_t L_128 = Decimal_op_Explicit_m3678935617(NULL /*static, unused*/, L_127, /*hidden argument*/NULL);
		int64_t L_129 = L_128;
		Il2CppObject * L_130 = Box(Int64_t1153838595_il2cpp_TypeInfo_var, &L_129);
		return L_130;
	}

IL_03da:
	{
		Decimal_t1954350631  L_131 = V_7;
		Decimal_t1954350631  L_132 = L_131;
		Il2CppObject * L_133 = Box(Decimal_t1954350631_il2cpp_TypeInfo_var, &L_132);
		return L_133;
	}

IL_03e2:
	{
		Type_t * L_134 = ___expectedType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_135 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Decimal_t1954350631_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_134) == ((Il2CppObject*)(Type_t *)L_135))))
		{
			goto IL_0409;
		}
	}
	{
		String_t* L_136 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t1637637232_il2cpp_TypeInfo_var);
		NumberFormatInfo_t1637637232 * L_137 = NumberFormatInfo_get_InvariantInfo_m1900501910(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		Decimal_t1954350631  L_138 = Decimal_Parse_m2075137301(NULL /*static, unused*/, L_136, ((int32_t)167), L_137, /*hidden argument*/NULL);
		Decimal_t1954350631  L_139 = L_138;
		Il2CppObject * L_140 = Box(Decimal_t1954350631_il2cpp_TypeInfo_var, &L_139);
		return L_140;
	}

IL_0409:
	{
		String_t* L_141 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t1637637232_il2cpp_TypeInfo_var);
		NumberFormatInfo_t1637637232 * L_142 = NumberFormatInfo_get_InvariantInfo_m1900501910(NULL /*static, unused*/, /*hidden argument*/NULL);
		double L_143 = Double_Parse_m3249074997(NULL /*static, unused*/, L_141, ((int32_t)167), L_142, /*hidden argument*/NULL);
		V_8 = L_143;
		Type_t * L_144 = ___expectedType0;
		if (!L_144)
		{
			goto IL_043b;
		}
	}
	{
		JsonReaderSettings_t3095433488 * L_145 = __this->get_Settings_8();
		NullCheck(L_145);
		TypeCoercionUtility_t3154211006 * L_146 = L_145->get_Coercion_0();
		Type_t * L_147 = ___expectedType0;
		double L_148 = V_8;
		double L_149 = L_148;
		Il2CppObject * L_150 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_149);
		NullCheck(L_146);
		Il2CppObject * L_151 = TypeCoercionUtility_CoerceType_m3345524962(L_146, L_147, L_150, /*hidden argument*/NULL);
		return L_151;
	}

IL_043b:
	{
		double L_152 = V_8;
		double L_153 = L_152;
		Il2CppObject * L_154 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_153);
		return L_154;
	}
}
// Pathfinding.Serialization.JsonFx.JsonToken Pathfinding.Serialization.JsonFx.JsonReader::Tokenize()
extern "C"  int32_t JsonReader_Tokenize_m4160635357 (JsonReader_t386455501 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = JsonReader_Tokenize_m1850339476(__this, (bool)0, /*hidden argument*/NULL);
		return L_0;
	}
}
// Pathfinding.Serialization.JsonFx.JsonToken Pathfinding.Serialization.JsonFx.JsonReader::Tokenize(System.Boolean)
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonReader_t386455501_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1499;
extern Il2CppCodeGenString* _stringLiteral433856599;
extern Il2CppCodeGenString* _stringLiteral1504;
extern Il2CppCodeGenString* _stringLiteral940789087;
extern Il2CppCodeGenString* _stringLiteral911885606;
extern Il2CppCodeGenString* _stringLiteral1349;
extern Il2CppCodeGenString* _stringLiteral413;
extern Il2CppCodeGenString* _stringLiteral4165272248;
extern Il2CppCodeGenString* _stringLiteral1241;
extern Il2CppCodeGenString* _stringLiteral4160127716;
extern Il2CppCodeGenString* _stringLiteral809219050;
extern Il2CppCodeGenString* _stringLiteral39;
extern const uint32_t JsonReader_Tokenize_m1850339476_MetadataUsageId;
extern "C"  int32_t JsonReader_Tokenize_m1850339476 (JsonReader_t386455501 * __this, bool ___allowUnquotedString0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_Tokenize_m1850339476_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	Il2CppChar V_2 = 0x0;
	String_t* V_3 = NULL;
	{
		int32_t L_0 = __this->get_index_11();
		int32_t L_1 = __this->get_SourceLength_10();
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0013;
		}
	}
	{
		return (int32_t)(0);
	}

IL_0013:
	{
		goto IL_0039;
	}

IL_0018:
	{
		int32_t L_2 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = __this->get_index_11();
		int32_t L_4 = __this->get_SourceLength_10();
		if ((((int32_t)L_3) < ((int32_t)L_4)))
		{
			goto IL_0039;
		}
	}
	{
		return (int32_t)(0);
	}

IL_0039:
	{
		String_t* L_5 = __this->get_Source_9();
		int32_t L_6 = __this->get_index_11();
		NullCheck(L_5);
		Il2CppChar L_7 = String_get_Chars_m3015341861(L_5, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_8 = Char_IsWhiteSpace_m2745315955(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_9 = __this->get_Source_9();
		int32_t L_10 = __this->get_index_11();
		NullCheck(L_9);
		Il2CppChar L_11 = String_get_Chars_m3015341861(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(_stringLiteral1499);
		Il2CppChar L_12 = String_get_Chars_m3015341861(_stringLiteral1499, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_0263;
		}
	}
	{
		int32_t L_13 = __this->get_index_11();
		int32_t L_14 = __this->get_SourceLength_10();
		if ((((int32_t)((int32_t)((int32_t)L_13+(int32_t)1))) < ((int32_t)L_14)))
		{
			goto IL_0099;
		}
	}
	{
		int32_t L_15 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_16 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_16, _stringLiteral433856599, L_15, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_0099:
	{
		int32_t L_17 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_17+(int32_t)1)));
		V_0 = (bool)0;
		String_t* L_18 = __this->get_Source_9();
		int32_t L_19 = __this->get_index_11();
		NullCheck(L_18);
		Il2CppChar L_20 = String_get_Chars_m3015341861(L_18, L_19, /*hidden argument*/NULL);
		NullCheck(_stringLiteral1499);
		Il2CppChar L_21 = String_get_Chars_m3015341861(_stringLiteral1499, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)L_21))))
		{
			goto IL_00d1;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0103;
	}

IL_00d1:
	{
		String_t* L_22 = __this->get_Source_9();
		int32_t L_23 = __this->get_index_11();
		NullCheck(L_22);
		Il2CppChar L_24 = String_get_Chars_m3015341861(L_22, L_23, /*hidden argument*/NULL);
		NullCheck(_stringLiteral1504);
		Il2CppChar L_25 = String_get_Chars_m3015341861(_stringLiteral1504, 1, /*hidden argument*/NULL);
		if ((((int32_t)L_24) == ((int32_t)L_25)))
		{
			goto IL_0103;
		}
	}
	{
		int32_t L_26 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_27 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_27, _stringLiteral940789087, L_26, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_27);
	}

IL_0103:
	{
		int32_t L_28 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_28+(int32_t)1)));
		bool L_29 = V_0;
		if (!L_29)
		{
			goto IL_01db;
		}
	}
	{
		int32_t L_30 = __this->get_index_11();
		V_1 = ((int32_t)((int32_t)L_30-(int32_t)2));
		int32_t L_31 = __this->get_index_11();
		int32_t L_32 = __this->get_SourceLength_10();
		if ((((int32_t)((int32_t)((int32_t)L_31+(int32_t)1))) < ((int32_t)L_32)))
		{
			goto IL_013f;
		}
	}
	{
		int32_t L_33 = V_1;
		JsonDeserializationException_t3911578122 * L_34 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_34, _stringLiteral911885606, L_33, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_34);
	}

IL_013f:
	{
		goto IL_0171;
	}

IL_0144:
	{
		int32_t L_35 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_35+(int32_t)1)));
		int32_t L_36 = __this->get_index_11();
		int32_t L_37 = __this->get_SourceLength_10();
		if ((((int32_t)((int32_t)((int32_t)L_36+(int32_t)1))) < ((int32_t)L_37)))
		{
			goto IL_0171;
		}
	}
	{
		int32_t L_38 = V_1;
		JsonDeserializationException_t3911578122 * L_39 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_39, _stringLiteral911885606, L_38, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_39);
	}

IL_0171:
	{
		String_t* L_40 = __this->get_Source_9();
		int32_t L_41 = __this->get_index_11();
		NullCheck(L_40);
		Il2CppChar L_42 = String_get_Chars_m3015341861(L_40, L_41, /*hidden argument*/NULL);
		NullCheck(_stringLiteral1349);
		Il2CppChar L_43 = String_get_Chars_m3015341861(_stringLiteral1349, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_42) == ((uint32_t)L_43))))
		{
			goto IL_0144;
		}
	}
	{
		String_t* L_44 = __this->get_Source_9();
		int32_t L_45 = __this->get_index_11();
		NullCheck(L_44);
		Il2CppChar L_46 = String_get_Chars_m3015341861(L_44, ((int32_t)((int32_t)L_45+(int32_t)1)), /*hidden argument*/NULL);
		NullCheck(_stringLiteral1349);
		Il2CppChar L_47 = String_get_Chars_m3015341861(_stringLiteral1349, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_46) == ((uint32_t)L_47))))
		{
			goto IL_0144;
		}
	}
	{
		int32_t L_48 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_48+(int32_t)2)));
		int32_t L_49 = __this->get_index_11();
		int32_t L_50 = __this->get_SourceLength_10();
		if ((((int32_t)L_49) < ((int32_t)L_50)))
		{
			goto IL_01d6;
		}
	}
	{
		return (int32_t)(0);
	}

IL_01d6:
	{
		goto IL_0222;
	}

IL_01db:
	{
		goto IL_0201;
	}

IL_01e0:
	{
		int32_t L_51 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_51+(int32_t)1)));
		int32_t L_52 = __this->get_index_11();
		int32_t L_53 = __this->get_SourceLength_10();
		if ((((int32_t)L_52) < ((int32_t)L_53)))
		{
			goto IL_0201;
		}
	}
	{
		return (int32_t)(0);
	}

IL_0201:
	{
		String_t* L_54 = __this->get_Source_9();
		int32_t L_55 = __this->get_index_11();
		NullCheck(L_54);
		Il2CppChar L_56 = String_get_Chars_m3015341861(L_54, L_55, /*hidden argument*/NULL);
		NullCheck(_stringLiteral413);
		int32_t L_57 = String_IndexOf_m2775210486(_stringLiteral413, L_56, /*hidden argument*/NULL);
		if ((((int32_t)L_57) < ((int32_t)0)))
		{
			goto IL_01e0;
		}
	}

IL_0222:
	{
		goto IL_0248;
	}

IL_0227:
	{
		int32_t L_58 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_58+(int32_t)1)));
		int32_t L_59 = __this->get_index_11();
		int32_t L_60 = __this->get_SourceLength_10();
		if ((((int32_t)L_59) < ((int32_t)L_60)))
		{
			goto IL_0248;
		}
	}
	{
		return (int32_t)(0);
	}

IL_0248:
	{
		String_t* L_61 = __this->get_Source_9();
		int32_t L_62 = __this->get_index_11();
		NullCheck(L_61);
		Il2CppChar L_63 = String_get_Chars_m3015341861(L_61, L_62, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_64 = Char_IsWhiteSpace_m2745315955(NULL /*static, unused*/, L_63, /*hidden argument*/NULL);
		if (L_64)
		{
			goto IL_0227;
		}
	}

IL_0263:
	{
		String_t* L_65 = __this->get_Source_9();
		int32_t L_66 = __this->get_index_11();
		NullCheck(L_65);
		Il2CppChar L_67 = String_get_Chars_m3015341861(L_65, L_66, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_67) == ((uint32_t)((int32_t)43)))))
		{
			goto IL_029c;
		}
	}
	{
		int32_t L_68 = __this->get_index_11();
		__this->set_index_11(((int32_t)((int32_t)L_68+(int32_t)1)));
		int32_t L_69 = __this->get_index_11();
		int32_t L_70 = __this->get_SourceLength_10();
		if ((((int32_t)L_69) < ((int32_t)L_70)))
		{
			goto IL_029c;
		}
	}
	{
		return (int32_t)(0);
	}

IL_029c:
	{
		String_t* L_71 = __this->get_Source_9();
		int32_t L_72 = __this->get_index_11();
		NullCheck(L_71);
		Il2CppChar L_73 = String_get_Chars_m3015341861(L_71, L_72, /*hidden argument*/NULL);
		V_2 = L_73;
		Il2CppChar L_74 = V_2;
		if (((int32_t)((int32_t)L_74-(int32_t)((int32_t)91))) == 0)
		{
			goto IL_02fd;
		}
		if (((int32_t)((int32_t)L_74-(int32_t)((int32_t)91))) == 1)
		{
			goto IL_02c3;
		}
		if (((int32_t)((int32_t)L_74-(int32_t)((int32_t)91))) == 2)
		{
			goto IL_0300;
		}
	}

IL_02c3:
	{
		Il2CppChar L_75 = V_2;
		if (((int32_t)((int32_t)L_75-(int32_t)((int32_t)123))) == 0)
		{
			goto IL_0303;
		}
		if (((int32_t)((int32_t)L_75-(int32_t)((int32_t)123))) == 1)
		{
			goto IL_02d8;
		}
		if (((int32_t)((int32_t)L_75-(int32_t)((int32_t)123))) == 2)
		{
			goto IL_0306;
		}
	}

IL_02d8:
	{
		Il2CppChar L_76 = V_2;
		if ((((int32_t)L_76) == ((int32_t)((int32_t)34))))
		{
			goto IL_0309;
		}
	}
	{
		Il2CppChar L_77 = V_2;
		if ((((int32_t)L_77) == ((int32_t)((int32_t)39))))
		{
			goto IL_0309;
		}
	}
	{
		Il2CppChar L_78 = V_2;
		if ((((int32_t)L_78) == ((int32_t)((int32_t)44))))
		{
			goto IL_030c;
		}
	}
	{
		Il2CppChar L_79 = V_2;
		if ((((int32_t)L_79) == ((int32_t)((int32_t)58))))
		{
			goto IL_030f;
		}
	}
	{
		goto IL_0312;
	}

IL_02fd:
	{
		return (int32_t)(((int32_t)10));
	}

IL_0300:
	{
		return (int32_t)(((int32_t)11));
	}

IL_0303:
	{
		return (int32_t)(((int32_t)12));
	}

IL_0306:
	{
		return (int32_t)(((int32_t)13));
	}

IL_0309:
	{
		return (int32_t)(((int32_t)9));
	}

IL_030c:
	{
		return (int32_t)(((int32_t)15));
	}

IL_030f:
	{
		return (int32_t)(((int32_t)14));
	}

IL_0312:
	{
		goto IL_0317;
	}

IL_0317:
	{
		String_t* L_80 = __this->get_Source_9();
		int32_t L_81 = __this->get_index_11();
		NullCheck(L_80);
		Il2CppChar L_82 = String_get_Chars_m3015341861(L_80, L_81, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_83 = Char_IsDigit_m1743537211(NULL /*static, unused*/, L_82, /*hidden argument*/NULL);
		if (L_83)
		{
			goto IL_037a;
		}
	}
	{
		String_t* L_84 = __this->get_Source_9();
		int32_t L_85 = __this->get_index_11();
		NullCheck(L_84);
		Il2CppChar L_86 = String_get_Chars_m3015341861(L_84, L_85, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_86) == ((uint32_t)((int32_t)45)))))
		{
			goto IL_037c;
		}
	}
	{
		int32_t L_87 = __this->get_index_11();
		int32_t L_88 = __this->get_SourceLength_10();
		if ((((int32_t)((int32_t)((int32_t)L_87+(int32_t)1))) >= ((int32_t)L_88)))
		{
			goto IL_037c;
		}
	}
	{
		String_t* L_89 = __this->get_Source_9();
		int32_t L_90 = __this->get_index_11();
		NullCheck(L_89);
		Il2CppChar L_91 = String_get_Chars_m3015341861(L_89, ((int32_t)((int32_t)L_90+(int32_t)1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_92 = Char_IsDigit_m1743537211(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
		if (!L_92)
		{
			goto IL_037c;
		}
	}

IL_037a:
	{
		return (int32_t)(8);
	}

IL_037c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_93 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_LiteralFalse_0();
		bool L_94 = JsonReader_MatchLiteral_m923120850(__this, L_93, /*hidden argument*/NULL);
		if (!L_94)
		{
			goto IL_038e;
		}
	}
	{
		return (int32_t)(3);
	}

IL_038e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_95 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_LiteralTrue_1();
		bool L_96 = JsonReader_MatchLiteral_m923120850(__this, L_95, /*hidden argument*/NULL);
		if (!L_96)
		{
			goto IL_03a0;
		}
	}
	{
		return (int32_t)(4);
	}

IL_03a0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_97 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_LiteralNull_2();
		bool L_98 = JsonReader_MatchLiteral_m923120850(__this, L_97, /*hidden argument*/NULL);
		if (!L_98)
		{
			goto IL_03b2;
		}
	}
	{
		return (int32_t)(2);
	}

IL_03b2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_99 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_LiteralNotANumber_4();
		bool L_100 = JsonReader_MatchLiteral_m923120850(__this, L_99, /*hidden argument*/NULL);
		if (!L_100)
		{
			goto IL_03c4;
		}
	}
	{
		return (int32_t)(5);
	}

IL_03c4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_101 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_LiteralPositiveInfinity_5();
		bool L_102 = JsonReader_MatchLiteral_m923120850(__this, L_101, /*hidden argument*/NULL);
		if (!L_102)
		{
			goto IL_03d6;
		}
	}
	{
		return (int32_t)(6);
	}

IL_03d6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_103 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_LiteralNegativeInfinity_6();
		bool L_104 = JsonReader_MatchLiteral_m923120850(__this, L_103, /*hidden argument*/NULL);
		if (!L_104)
		{
			goto IL_03e8;
		}
	}
	{
		return (int32_t)(7);
	}

IL_03e8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_105 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_LiteralUndefined_3();
		bool L_106 = JsonReader_MatchLiteral_m923120850(__this, L_105, /*hidden argument*/NULL);
		if (!L_106)
		{
			goto IL_03fa;
		}
	}
	{
		return (int32_t)(1);
	}

IL_03fa:
	{
		bool L_107 = ___allowUnquotedString0;
		if (!L_107)
		{
			goto IL_0403;
		}
	}
	{
		return (int32_t)(((int32_t)16));
	}

IL_0403:
	{
		String_t* L_108 = __this->get_Source_9();
		int32_t L_109 = __this->get_index_11();
		int32_t L_110 = Math_Max_m1309380475(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_109-(int32_t)5)), /*hidden argument*/NULL);
		int32_t L_111 = __this->get_SourceLength_10();
		int32_t L_112 = __this->get_index_11();
		int32_t L_113 = Math_Min_m811624909(NULL /*static, unused*/, ((int32_t)((int32_t)((int32_t)((int32_t)L_111-(int32_t)L_112))-(int32_t)1)), ((int32_t)20), /*hidden argument*/NULL);
		NullCheck(L_108);
		String_t* L_114 = String_Substring_m675079568(L_108, L_110, L_113, /*hidden argument*/NULL);
		V_3 = L_114;
		ObjectU5BU5D_t1108656482* L_115 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9)));
		NullCheck(L_115);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_115, 0);
		ArrayElementTypeCheck (L_115, _stringLiteral4165272248);
		(L_115)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral4165272248);
		ObjectU5BU5D_t1108656482* L_116 = L_115;
		String_t* L_117 = __this->get_Source_9();
		int32_t L_118 = __this->get_index_11();
		NullCheck(L_117);
		Il2CppChar L_119 = String_get_Chars_m3015341861(L_117, L_118, /*hidden argument*/NULL);
		Il2CppChar L_120 = L_119;
		Il2CppObject * L_121 = Box(Char_t2862622538_il2cpp_TypeInfo_var, &L_120);
		NullCheck(L_116);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_116, 1);
		ArrayElementTypeCheck (L_116, L_121);
		(L_116)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_121);
		ObjectU5BU5D_t1108656482* L_122 = L_116;
		NullCheck(L_122);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_122, 2);
		ArrayElementTypeCheck (L_122, _stringLiteral1241);
		(L_122)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1241);
		ObjectU5BU5D_t1108656482* L_123 = L_122;
		String_t* L_124 = __this->get_Source_9();
		int32_t L_125 = __this->get_index_11();
		NullCheck(L_124);
		Il2CppChar L_126 = String_get_Chars_m3015341861(L_124, L_125, /*hidden argument*/NULL);
		int32_t L_127 = ((int32_t)L_126);
		Il2CppObject * L_128 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_127);
		NullCheck(L_123);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_123, 3);
		ArrayElementTypeCheck (L_123, L_128);
		(L_123)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_128);
		ObjectU5BU5D_t1108656482* L_129 = L_123;
		NullCheck(L_129);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_129, 4);
		ArrayElementTypeCheck (L_129, _stringLiteral4160127716);
		(L_129)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral4160127716);
		ObjectU5BU5D_t1108656482* L_130 = L_129;
		int32_t L_131 = __this->get_index_11();
		int32_t L_132 = L_131;
		Il2CppObject * L_133 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_132);
		NullCheck(L_130);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_130, 5);
		ArrayElementTypeCheck (L_130, L_133);
		(L_130)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_133);
		ObjectU5BU5D_t1108656482* L_134 = L_130;
		NullCheck(L_134);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_134, 6);
		ArrayElementTypeCheck (L_134, _stringLiteral809219050);
		(L_134)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral809219050);
		ObjectU5BU5D_t1108656482* L_135 = L_134;
		String_t* L_136 = V_3;
		NullCheck(L_135);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_135, 7);
		ArrayElementTypeCheck (L_135, L_136);
		(L_135)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_136);
		ObjectU5BU5D_t1108656482* L_137 = L_135;
		NullCheck(L_137);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_137, 8);
		ArrayElementTypeCheck (L_137, _stringLiteral39);
		(L_137)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral39);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_138 = String_Concat_m3016520001(NULL /*static, unused*/, L_137, /*hidden argument*/NULL);
		int32_t L_139 = __this->get_index_11();
		JsonDeserializationException_t3911578122 * L_140 = (JsonDeserializationException_t3911578122 *)il2cpp_codegen_object_new(JsonDeserializationException_t3911578122_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m1655578326(L_140, L_138, L_139, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_140);
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonReader::MatchLiteral(System.String)
extern "C"  bool JsonReader_MatchLiteral_m923120850 (JsonReader_t386455501 * __this, String_t* ___literal0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___literal0;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m2979997331(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_index_11();
		int32_t L_3 = V_0;
		int32_t L_4 = __this->get_SourceLength_10();
		if ((((int32_t)((int32_t)((int32_t)L_2+(int32_t)L_3))) <= ((int32_t)L_4)))
		{
			goto IL_001c;
		}
	}
	{
		return (bool)0;
	}

IL_001c:
	{
		V_1 = 0;
		goto IL_0048;
	}

IL_0023:
	{
		String_t* L_5 = ___literal0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		Il2CppChar L_7 = String_get_Chars_m3015341861(L_5, L_6, /*hidden argument*/NULL);
		String_t* L_8 = __this->get_Source_9();
		int32_t L_9 = __this->get_index_11();
		int32_t L_10 = V_1;
		NullCheck(L_8);
		Il2CppChar L_11 = String_get_Chars_m3015341861(L_8, ((int32_t)((int32_t)L_9+(int32_t)L_10)), /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)L_11)))
		{
			goto IL_0044;
		}
	}
	{
		return (bool)0;
	}

IL_0044:
	{
		int32_t L_12 = V_1;
		V_1 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0048:
	{
		int32_t L_13 = V_1;
		int32_t L_14 = V_0;
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_0023;
		}
	}
	{
		return (bool)1;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonReaderSettings::.ctor()
extern Il2CppClass* TypeCoercionUtility_t3154211006_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t182525330_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1836678241_MethodInfo_var;
extern const uint32_t JsonReaderSettings__ctor_m298179387_MetadataUsageId;
extern "C"  void JsonReaderSettings__ctor_m298179387 (JsonReaderSettings_t3095433488 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReaderSettings__ctor_m298179387_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeCoercionUtility_t3154211006 * L_0 = (TypeCoercionUtility_t3154211006 *)il2cpp_codegen_object_new(TypeCoercionUtility_t3154211006_il2cpp_TypeInfo_var);
		TypeCoercionUtility__ctor_m3724068137(L_0, /*hidden argument*/NULL);
		__this->set_Coercion_0(L_0);
		List_1_t182525330 * L_1 = (List_1_t182525330 *)il2cpp_codegen_object_new(List_1_t182525330_il2cpp_TypeInfo_var);
		List_1__ctor_m1836678241(L_1, /*hidden argument*/List_1__ctor_m1836678241_MethodInfo_var);
		__this->set_converters_3(L_1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonReaderSettings::get_HandleCyclicReferences()
extern "C"  bool JsonReaderSettings_get_HandleCyclicReferences_m3959955387 (JsonReaderSettings_t3095433488 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CHandleCyclicReferencesU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonReaderSettings::get_AllowUnquotedObjectKeys()
extern "C"  bool JsonReaderSettings_get_AllowUnquotedObjectKeys_m2993889901 (JsonReaderSettings_t3095433488 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_allowUnquotedObjectKeys_1();
		return L_0;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonReaderSettings::IsTypeHintName(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringComparer_t4230573202_il2cpp_TypeInfo_var;
extern const uint32_t JsonReaderSettings_IsTypeHintName_m2115719875_MetadataUsageId;
extern "C"  bool JsonReaderSettings_IsTypeHintName_m2115719875 (JsonReaderSettings_t3095433488 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReaderSettings_IsTypeHintName_m2115719875_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		String_t* L_0 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_002e;
		}
	}
	{
		String_t* L_2 = __this->get_typeHintName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t4230573202_il2cpp_TypeInfo_var);
		StringComparer_t4230573202 * L_4 = StringComparer_get_Ordinal_m2543279027(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = __this->get_typeHintName_2();
		String_t* L_6 = ___name0;
		NullCheck(L_4);
		bool L_7 = VirtFuncInvoker2< bool, String_t*, String_t* >::Invoke(11 /* System.Boolean System.StringComparer::Equals(System.String,System.String) */, L_4, L_5, L_6);
		G_B4_0 = ((int32_t)(L_7));
		goto IL_002f;
	}

IL_002e:
	{
		G_B4_0 = 0;
	}

IL_002f:
	{
		return (bool)G_B4_0;
	}
}
// Pathfinding.Serialization.JsonFx.JsonConverter Pathfinding.Serialization.JsonFx.JsonReaderSettings::GetConverter(System.Type)
extern const MethodInfo* List_1_get_Item_m3135349974_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3861194015_MethodInfo_var;
extern const uint32_t JsonReaderSettings_GetConverter_m1813240067_MetadataUsageId;
extern "C"  JsonConverter_t3109307074 * JsonReaderSettings_GetConverter_m1813240067 (JsonReaderSettings_t3095433488 * __this, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReaderSettings_GetConverter_m1813240067_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_002f;
	}

IL_0007:
	{
		List_1_t182525330 * L_0 = __this->get_converters_3();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		JsonConverter_t3109307074 * L_2 = List_1_get_Item_m3135349974(L_0, L_1, /*hidden argument*/List_1_get_Item_m3135349974_MethodInfo_var);
		Type_t * L_3 = ___type0;
		NullCheck(L_2);
		bool L_4 = VirtFuncInvoker1< bool, Type_t * >::Invoke(4 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonConverter::CanConvert(System.Type) */, L_2, L_3);
		if (!L_4)
		{
			goto IL_002b;
		}
	}
	{
		List_1_t182525330 * L_5 = __this->get_converters_3();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		JsonConverter_t3109307074 * L_7 = List_1_get_Item_m3135349974(L_5, L_6, /*hidden argument*/List_1_get_Item_m3135349974_MethodInfo_var);
		return L_7;
	}

IL_002b:
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002f:
	{
		int32_t L_9 = V_0;
		List_1_t182525330 * L_10 = __this->get_converters_3();
		NullCheck(L_10);
		int32_t L_11 = List_1_get_Count_m3861194015(L_10, /*hidden argument*/List_1_get_Count_m3861194015_MethodInfo_var);
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_0007;
		}
	}
	{
		return (JsonConverter_t3109307074 *)NULL;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonReaderSettings::AddTypeConverter(Pathfinding.Serialization.JsonFx.JsonConverter)
extern const MethodInfo* List_1_Add_m1531039057_MethodInfo_var;
extern const uint32_t JsonReaderSettings_AddTypeConverter_m4147446306_MetadataUsageId;
extern "C"  void JsonReaderSettings_AddTypeConverter_m4147446306 (JsonReaderSettings_t3095433488 * __this, JsonConverter_t3109307074 * ___converter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReaderSettings_AddTypeConverter_m4147446306_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t182525330 * L_0 = __this->get_converters_3();
		JsonConverter_t3109307074 * L_1 = ___converter0;
		NullCheck(L_0);
		List_1_Add_m1531039057(L_0, L_1, /*hidden argument*/List_1_Add_m1531039057_MethodInfo_var);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonSerializationException::.ctor(System.String)
extern "C"  void JsonSerializationException__ctor_m1980876064 (JsonSerializationException_t1208944969 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		InvalidOperationException__ctor_m1485483280(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonSerializationException::.ctor(System.String,System.Exception)
extern "C"  void JsonSerializationException__ctor_m906028054 (JsonSerializationException_t1208944969 * __this, String_t* ___message0, Exception_t3991598821 * ___innerException1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t3991598821 * L_1 = ___innerException1;
		InvalidOperationException__ctor_m2119621158(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String Pathfinding.Serialization.JsonFx.JsonSpecifiedPropertyAttribute::get_SpecifiedProperty()
extern "C"  String_t* JsonSpecifiedPropertyAttribute_get_SpecifiedProperty_m3397186727 (JsonSpecifiedPropertyAttribute_t1712644481 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_specifiedProperty_0();
		return L_0;
	}
}
// System.String Pathfinding.Serialization.JsonFx.JsonSpecifiedPropertyAttribute::GetJsonSpecifiedProperty(System.Reflection.MemberInfo)
extern const Il2CppType* JsonSpecifiedPropertyAttribute_t1712644481_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonSpecifiedPropertyAttribute_t1712644481_il2cpp_TypeInfo_var;
extern const uint32_t JsonSpecifiedPropertyAttribute_GetJsonSpecifiedProperty_m1760565138_MetadataUsageId;
extern "C"  String_t* JsonSpecifiedPropertyAttribute_GetJsonSpecifiedProperty_m1760565138 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___memberInfo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonSpecifiedPropertyAttribute_GetJsonSpecifiedProperty_m1760565138_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JsonSpecifiedPropertyAttribute_t1712644481 * V_0 = NULL;
	{
		MemberInfo_t * L_0 = ___memberInfo0;
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		MemberInfo_t * L_1 = ___memberInfo0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(JsonSpecifiedPropertyAttribute_t1712644481_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Attribute_IsDefined_m3508553943(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_001d;
		}
	}

IL_001b:
	{
		return (String_t*)NULL;
	}

IL_001d:
	{
		MemberInfo_t * L_4 = ___memberInfo0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(JsonSpecifiedPropertyAttribute_t1712644481_0_0_0_var), /*hidden argument*/NULL);
		Attribute_t2523058482 * L_6 = Attribute_GetCustomAttribute_m506754809(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_0 = ((JsonSpecifiedPropertyAttribute_t1712644481 *)CastclassClass(L_6, JsonSpecifiedPropertyAttribute_t1712644481_il2cpp_TypeInfo_var));
		JsonSpecifiedPropertyAttribute_t1712644481 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = JsonSpecifiedPropertyAttribute_get_SpecifiedProperty_m3397186727(L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonTypeCoercionException::.ctor(System.String)
extern "C"  void JsonTypeCoercionException__ctor_m2129803924 (JsonTypeCoercionException_t3725515417 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		ArgumentException__ctor_m3544856547(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonTypeCoercionException::.ctor(System.String,System.Exception)
extern "C"  void JsonTypeCoercionException__ctor_m3348333858 (JsonTypeCoercionException_t3725515417 * __this, String_t* ___message0, Exception_t3991598821 * ___innerException1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t3991598821 * L_1 = ___innerException1;
		ArgumentException__ctor_m2711220147(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::.ctor(System.Text.StringBuilder,Pathfinding.Serialization.JsonFx.JsonWriterSettings)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t1065375142_il2cpp_TypeInfo_var;
extern Il2CppClass* StringWriter_t4216882900_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3289454849;
extern Il2CppCodeGenString* _stringLiteral1434631203;
extern const uint32_t JsonWriter__ctor_m3320010686_MetadataUsageId;
extern "C"  void JsonWriter__ctor_m3320010686 (JsonWriter_t541860733 * __this, StringBuilder_t243639308 * ___output0, JsonWriterSettings_t3394579648 * ___settings1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter__ctor_m3320010686_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_0 = ___output0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral3289454849, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		JsonWriterSettings_t3394579648 * L_2 = ___settings1;
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_3 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_3, _stringLiteral1434631203, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0028:
	{
		StringBuilder_t243639308 * L_4 = ___output0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_5 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		StringWriter_t4216882900 * L_6 = (StringWriter_t4216882900 *)il2cpp_codegen_object_new(StringWriter_t4216882900_il2cpp_TypeInfo_var);
		StringWriter__ctor_m2916801583(L_6, L_4, L_5, /*hidden argument*/NULL);
		__this->set_Writer_0(L_6);
		JsonWriterSettings_t3394579648 * L_7 = ___settings1;
		__this->set_settings_1(L_7);
		TextWriter_t2304124208 * L_8 = __this->get_Writer_0();
		JsonWriterSettings_t3394579648 * L_9 = __this->get_settings_1();
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_NewLine() */, L_9);
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(7 /* System.Void System.IO.TextWriter::set_NewLine(System.String) */, L_8, L_10);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::System.IDisposable.Dispose()
extern "C"  void JsonWriter_System_IDisposable_Dispose_m3492616945 (JsonWriter_t541860733 * __this, const MethodInfo* method)
{
	{
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		TextWriter_t2304124208 * L_1 = __this->get_Writer_0();
		NullCheck(L_1);
		TextWriter_Dispose_m183705414(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.IO.TextWriter Pathfinding.Serialization.JsonFx.JsonWriter::get_TextWriter()
extern "C"  TextWriter_t2304124208 * JsonWriter_get_TextWriter_m1060131231 (JsonWriter_t541860733 * __this, const MethodInfo* method)
{
	{
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		return L_0;
	}
}
// Pathfinding.Serialization.JsonFx.JsonWriterSettings Pathfinding.Serialization.JsonFx.JsonWriter::get_Settings()
extern "C"  JsonWriterSettings_t3394579648 * JsonWriter_get_Settings_m3312657913 (JsonWriter_t541860733 * __this, const MethodInfo* method)
{
	{
		JsonWriterSettings_t3394579648 * L_0 = __this->get_settings_1();
		return L_0;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Object)
extern "C"  void JsonWriter_Write_m456059081 (JsonWriter_t541860733 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		VirtActionInvoker2< Il2CppObject *, bool >::Invoke(5 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Object,System.Boolean) */, __this, L_0, (bool)0);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Object,System.Boolean)
extern Il2CppClass* JsonReader_t386455501_il2cpp_TypeInfo_var;
extern Il2CppClass* IJsonSerializable_t572835770_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonSerializationException_t1208944969_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t2862609660_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t1954350631_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16_t1153838442_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern Il2CppClass* SByte_t1161769777_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t24667981_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t24668076_il2cpp_TypeInfo_var;
extern Il2CppClass* Guid_t2862754429_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t1116831938_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeSpan_t413522987_il2cpp_TypeInfo_var;
extern Il2CppClass* Version_t763695022_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral107111942;
extern const uint32_t JsonWriter_Write_m3136003988_MetadataUsageId;
extern "C"  void JsonWriter_Write_m3136003988 (JsonWriter_t541860733 * __this, Il2CppObject * ___value0, bool ___isProperty1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m3136003988_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	JsonConverter_t3109307074 * V_1 = NULL;
	int32_t V_2 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = ___isProperty1;
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		JsonWriterSettings_t3394579648 * L_1 = __this->get_settings_1();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_PrettyPrint() */, L_1);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		TextWriter_t2304124208 * L_3 = __this->get_Writer_0();
		NullCheck(L_3);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_3, ((int32_t)32));
	}

IL_0023:
	{
		Il2CppObject * L_4 = ___value0;
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		TextWriter_t2304124208 * L_5 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_6 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_LiteralNull_2();
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_5, L_6);
		return;
	}

IL_003a:
	{
		Il2CppObject * L_7 = ___value0;
		if (!((Il2CppObject *)IsInst(L_7, IJsonSerializable_t572835770_il2cpp_TypeInfo_var)))
		{
			goto IL_00bc;
		}
	}

IL_0045:
	try
	{ // begin try (depth: 1)
		{
			bool L_8 = ___isProperty1;
			if (!L_8)
			{
				goto IL_0095;
			}
		}

IL_004b:
		{
			int32_t L_9 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_9+(int32_t)1)));
			int32_t L_10 = __this->get_depth_2();
			JsonWriterSettings_t3394579648 * L_11 = __this->get_settings_1();
			NullCheck(L_11);
			int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_11);
			if ((((int32_t)L_10) <= ((int32_t)L_12)))
			{
				goto IL_008f;
			}
		}

IL_006f:
		{
			JsonWriterSettings_t3394579648 * L_13 = __this->get_settings_1();
			NullCheck(L_13);
			int32_t L_14 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_13);
			int32_t L_15 = L_14;
			Il2CppObject * L_16 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_15);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_17 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral107111942, L_16, /*hidden argument*/NULL);
			JsonSerializationException_t1208944969 * L_18 = (JsonSerializationException_t1208944969 *)il2cpp_codegen_object_new(JsonSerializationException_t1208944969_il2cpp_TypeInfo_var);
			JsonSerializationException__ctor_m1980876064(L_18, L_17, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_18);
		}

IL_008f:
		{
			VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
		}

IL_0095:
		{
			Il2CppObject * L_19 = ___value0;
			NullCheck(((Il2CppObject *)Castclass(L_19, IJsonSerializable_t572835770_il2cpp_TypeInfo_var)));
			InterfaceActionInvoker1< JsonWriter_t541860733 * >::Invoke(0 /* System.Void Pathfinding.Serialization.JsonFx.IJsonSerializable::WriteJson(Pathfinding.Serialization.JsonFx.JsonWriter) */, IJsonSerializable_t572835770_il2cpp_TypeInfo_var, ((Il2CppObject *)Castclass(L_19, IJsonSerializable_t572835770_il2cpp_TypeInfo_var)), __this);
			IL2CPP_LEAVE(0xBB, FINALLY_00a6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00a6;
	}

FINALLY_00a6:
	{ // begin finally (depth: 1)
		{
			bool L_20 = ___isProperty1;
			if (!L_20)
			{
				goto IL_00ba;
			}
		}

IL_00ac:
		{
			int32_t L_21 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_21-(int32_t)1)));
		}

IL_00ba:
		{
			IL2CPP_END_FINALLY(166)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(166)
	{
		IL2CPP_JUMP_TBL(0xBB, IL_00bb)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00bb:
	{
		return;
	}

IL_00bc:
	{
		Il2CppObject * L_22 = ___value0;
		if (!((Enum_t2862688501 *)IsInstClass(L_22, Enum_t2862688501_il2cpp_TypeInfo_var)))
		{
			goto IL_00d4;
		}
	}
	{
		Il2CppObject * L_23 = ___value0;
		VirtActionInvoker1< Enum_t2862688501 * >::Invoke(8 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Enum) */, __this, ((Enum_t2862688501 *)CastclassClass(L_23, Enum_t2862688501_il2cpp_TypeInfo_var)));
		return;
	}

IL_00d4:
	{
		Il2CppObject * L_24 = ___value0;
		NullCheck(L_24);
		Type_t * L_25 = Object_GetType_m2022236990(L_24, /*hidden argument*/NULL);
		V_0 = L_25;
		JsonWriterSettings_t3394579648 * L_26 = JsonWriter_get_Settings_m3312657913(__this, /*hidden argument*/NULL);
		Type_t * L_27 = V_0;
		NullCheck(L_26);
		JsonConverter_t3109307074 * L_28 = VirtFuncInvoker1< JsonConverter_t3109307074 *, Type_t * >::Invoke(13 /* Pathfinding.Serialization.JsonFx.JsonConverter Pathfinding.Serialization.JsonFx.JsonWriterSettings::GetConverter(System.Type) */, L_26, L_27);
		V_1 = L_28;
		JsonConverter_t3109307074 * L_29 = V_1;
		if (!L_29)
		{
			goto IL_00f8;
		}
	}
	{
		JsonConverter_t3109307074 * L_30 = V_1;
		Type_t * L_31 = V_0;
		Il2CppObject * L_32 = ___value0;
		NullCheck(L_30);
		JsonConverter_Write_m275892874(L_30, __this, L_31, L_32, /*hidden argument*/NULL);
		return;
	}

IL_00f8:
	{
		Type_t * L_33 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_34 = Type_GetTypeCode_m2969996822(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		V_2 = L_34;
		int32_t L_35 = V_2;
		if (L_35 == 0)
		{
			goto IL_018a;
		}
		if (L_35 == 1)
		{
			goto IL_022a;
		}
		if (L_35 == 2)
		{
			goto IL_018a;
		}
		if (L_35 == 3)
		{
			goto IL_0156;
		}
		if (L_35 == 4)
		{
			goto IL_0170;
		}
		if (L_35 == 5)
		{
			goto IL_01dc;
		}
		if (L_35 == 6)
		{
			goto IL_0163;
		}
		if (L_35 == 7)
		{
			goto IL_01b5;
		}
		if (L_35 == 8)
		{
			goto IL_0203;
		}
		if (L_35 == 9)
		{
			goto IL_01c2;
		}
		if (L_35 == 10)
		{
			goto IL_0210;
		}
		if (L_35 == 11)
		{
			goto IL_01cf;
		}
		if (L_35 == 12)
		{
			goto IL_021d;
		}
		if (L_35 == 13)
		{
			goto IL_01e9;
		}
		if (L_35 == 14)
		{
			goto IL_01a8;
		}
		if (L_35 == 15)
		{
			goto IL_019b;
		}
		if (L_35 == 16)
		{
			goto IL_017d;
		}
		if (L_35 == 17)
		{
			goto IL_022a;
		}
		if (L_35 == 18)
		{
			goto IL_01f6;
		}
	}
	{
		goto IL_022a;
	}

IL_0156:
	{
		Il2CppObject * L_36 = ___value0;
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Boolean) */, __this, ((*(bool*)((bool*)UnBox (L_36, Boolean_t476798718_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0163:
	{
		Il2CppObject * L_37 = ___value0;
		VirtActionInvoker1< uint8_t >::Invoke(11 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Byte) */, __this, ((*(uint8_t*)((uint8_t*)UnBox (L_37, Byte_t2862609660_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0170:
	{
		Il2CppObject * L_38 = ___value0;
		VirtActionInvoker1< Il2CppChar >::Invoke(22 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Char) */, __this, ((*(Il2CppChar*)((Il2CppChar*)UnBox (L_38, Char_t2862622538_il2cpp_TypeInfo_var)))));
		return;
	}

IL_017d:
	{
		Il2CppObject * L_39 = ___value0;
		VirtActionInvoker1< DateTime_t4283661327  >::Invoke(6 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.DateTime) */, __this, ((*(DateTime_t4283661327 *)((DateTime_t4283661327 *)UnBox (L_39, DateTime_t4283661327_il2cpp_TypeInfo_var)))));
		return;
	}

IL_018a:
	{
		TextWriter_t2304124208 * L_40 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_41 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_LiteralNull_2();
		NullCheck(L_40);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_40, L_41);
		return;
	}

IL_019b:
	{
		Il2CppObject * L_42 = ___value0;
		VirtActionInvoker1< Decimal_t1954350631  >::Invoke(21 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Decimal) */, __this, ((*(Decimal_t1954350631 *)((Decimal_t1954350631 *)UnBox (L_42, Decimal_t1954350631_il2cpp_TypeInfo_var)))));
		return;
	}

IL_01a8:
	{
		Il2CppObject * L_43 = ___value0;
		VirtActionInvoker1< double >::Invoke(20 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Double) */, __this, ((*(double*)((double*)UnBox (L_43, Double_t3868226565_il2cpp_TypeInfo_var)))));
		return;
	}

IL_01b5:
	{
		Il2CppObject * L_44 = ___value0;
		VirtActionInvoker1< int16_t >::Invoke(13 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Int16) */, __this, ((*(int16_t*)((int16_t*)UnBox (L_44, Int16_t1153838442_il2cpp_TypeInfo_var)))));
		return;
	}

IL_01c2:
	{
		Il2CppObject * L_45 = ___value0;
		VirtActionInvoker1< int32_t >::Invoke(15 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Int32) */, __this, ((*(int32_t*)((int32_t*)UnBox (L_45, Int32_t1153838500_il2cpp_TypeInfo_var)))));
		return;
	}

IL_01cf:
	{
		Il2CppObject * L_46 = ___value0;
		VirtActionInvoker1< int64_t >::Invoke(17 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Int64) */, __this, ((*(int64_t*)((int64_t*)UnBox (L_46, Int64_t1153838595_il2cpp_TypeInfo_var)))));
		return;
	}

IL_01dc:
	{
		Il2CppObject * L_47 = ___value0;
		VirtActionInvoker1< int8_t >::Invoke(12 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.SByte) */, __this, ((*(int8_t*)((int8_t*)UnBox (L_47, SByte_t1161769777_il2cpp_TypeInfo_var)))));
		return;
	}

IL_01e9:
	{
		Il2CppObject * L_48 = ___value0;
		VirtActionInvoker1< float >::Invoke(19 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Single) */, __this, ((*(float*)((float*)UnBox (L_48, Single_t4291918972_il2cpp_TypeInfo_var)))));
		return;
	}

IL_01f6:
	{
		Il2CppObject * L_49 = ___value0;
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, ((String_t*)CastclassSealed(L_49, String_t_il2cpp_TypeInfo_var)));
		return;
	}

IL_0203:
	{
		Il2CppObject * L_50 = ___value0;
		VirtActionInvoker1< uint16_t >::Invoke(14 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.UInt16) */, __this, ((*(uint16_t*)((uint16_t*)UnBox (L_50, UInt16_t24667923_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0210:
	{
		Il2CppObject * L_51 = ___value0;
		VirtActionInvoker1< uint32_t >::Invoke(16 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.UInt32) */, __this, ((*(uint32_t*)((uint32_t*)UnBox (L_51, UInt32_t24667981_il2cpp_TypeInfo_var)))));
		return;
	}

IL_021d:
	{
		Il2CppObject * L_52 = ___value0;
		VirtActionInvoker1< uint64_t >::Invoke(18 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.UInt64) */, __this, ((*(uint64_t*)((uint64_t*)UnBox (L_52, UInt64_t24668076_il2cpp_TypeInfo_var)))));
		return;
	}

IL_022a:
	{
		goto IL_022f;
	}

IL_022f:
	{
		Il2CppObject * L_53 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_53, Guid_t2862754429_il2cpp_TypeInfo_var)))
		{
			goto IL_0247;
		}
	}
	{
		Il2CppObject * L_54 = ___value0;
		VirtActionInvoker1< Guid_t2862754429  >::Invoke(7 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Guid) */, __this, ((*(Guid_t2862754429 *)((Guid_t2862754429 *)UnBox (L_54, Guid_t2862754429_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0247:
	{
		Il2CppObject * L_55 = ___value0;
		if (!((Uri_t1116831938 *)IsInstClass(L_55, Uri_t1116831938_il2cpp_TypeInfo_var)))
		{
			goto IL_025f;
		}
	}
	{
		Il2CppObject * L_56 = ___value0;
		VirtActionInvoker1< Uri_t1116831938 * >::Invoke(24 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Uri) */, __this, ((Uri_t1116831938 *)CastclassClass(L_56, Uri_t1116831938_il2cpp_TypeInfo_var)));
		return;
	}

IL_025f:
	{
		Il2CppObject * L_57 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_57, TimeSpan_t413522987_il2cpp_TypeInfo_var)))
		{
			goto IL_0277;
		}
	}
	{
		Il2CppObject * L_58 = ___value0;
		VirtActionInvoker1< TimeSpan_t413522987  >::Invoke(23 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.TimeSpan) */, __this, ((*(TimeSpan_t413522987 *)((TimeSpan_t413522987 *)UnBox (L_58, TimeSpan_t413522987_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0277:
	{
		Il2CppObject * L_59 = ___value0;
		if (!((Version_t763695022 *)IsInstSealed(L_59, Version_t763695022_il2cpp_TypeInfo_var)))
		{
			goto IL_028f;
		}
	}
	{
		Il2CppObject * L_60 = ___value0;
		VirtActionInvoker1< Version_t763695022 * >::Invoke(25 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Version) */, __this, ((Version_t763695022 *)CastclassSealed(L_60, Version_t763695022_il2cpp_TypeInfo_var)));
		return;
	}

IL_028f:
	{
		Il2CppObject * L_61 = ___value0;
		if (!((Il2CppObject *)IsInst(L_61, IDictionary_t537317817_il2cpp_TypeInfo_var)))
		{
			goto IL_0311;
		}
	}

IL_029a:
	try
	{ // begin try (depth: 1)
		{
			bool L_62 = ___isProperty1;
			if (!L_62)
			{
				goto IL_02ea;
			}
		}

IL_02a0:
		{
			int32_t L_63 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_63+(int32_t)1)));
			int32_t L_64 = __this->get_depth_2();
			JsonWriterSettings_t3394579648 * L_65 = __this->get_settings_1();
			NullCheck(L_65);
			int32_t L_66 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_65);
			if ((((int32_t)L_64) <= ((int32_t)L_66)))
			{
				goto IL_02e4;
			}
		}

IL_02c4:
		{
			JsonWriterSettings_t3394579648 * L_67 = __this->get_settings_1();
			NullCheck(L_67);
			int32_t L_68 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_67);
			int32_t L_69 = L_68;
			Il2CppObject * L_70 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_69);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_71 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral107111942, L_70, /*hidden argument*/NULL);
			JsonSerializationException_t1208944969 * L_72 = (JsonSerializationException_t1208944969 *)il2cpp_codegen_object_new(JsonSerializationException_t1208944969_il2cpp_TypeInfo_var);
			JsonSerializationException__ctor_m1980876064(L_72, L_71, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_72);
		}

IL_02e4:
		{
			VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
		}

IL_02ea:
		{
			Il2CppObject * L_73 = ___value0;
			VirtActionInvoker1< Il2CppObject * >::Invoke(28 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObject(System.Collections.IDictionary) */, __this, ((Il2CppObject *)Castclass(L_73, IDictionary_t537317817_il2cpp_TypeInfo_var)));
			IL2CPP_LEAVE(0x310, FINALLY_02fb);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_02fb;
	}

FINALLY_02fb:
	{ // begin finally (depth: 1)
		{
			bool L_74 = ___isProperty1;
			if (!L_74)
			{
				goto IL_030f;
			}
		}

IL_0301:
		{
			int32_t L_75 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_75-(int32_t)1)));
		}

IL_030f:
		{
			IL2CPP_END_FINALLY(763)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(763)
	{
		IL2CPP_JUMP_TBL(0x310, IL_0310)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0310:
	{
		return;
	}

IL_0311:
	{
		Type_t * L_76 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_77 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_TypeGenericIDictionary_7();
		NullCheck(L_76);
		Type_t * L_78 = Type_GetInterface_m1133348080(L_76, L_77, /*hidden argument*/NULL);
		if (!L_78)
		{
			goto IL_0398;
		}
	}

IL_0321:
	try
	{ // begin try (depth: 1)
		{
			bool L_79 = ___isProperty1;
			if (!L_79)
			{
				goto IL_0371;
			}
		}

IL_0327:
		{
			int32_t L_80 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_80+(int32_t)1)));
			int32_t L_81 = __this->get_depth_2();
			JsonWriterSettings_t3394579648 * L_82 = __this->get_settings_1();
			NullCheck(L_82);
			int32_t L_83 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_82);
			if ((((int32_t)L_81) <= ((int32_t)L_83)))
			{
				goto IL_036b;
			}
		}

IL_034b:
		{
			JsonWriterSettings_t3394579648 * L_84 = __this->get_settings_1();
			NullCheck(L_84);
			int32_t L_85 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_84);
			int32_t L_86 = L_85;
			Il2CppObject * L_87 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_86);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_88 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral107111942, L_87, /*hidden argument*/NULL);
			JsonSerializationException_t1208944969 * L_89 = (JsonSerializationException_t1208944969 *)il2cpp_codegen_object_new(JsonSerializationException_t1208944969_il2cpp_TypeInfo_var);
			JsonSerializationException__ctor_m1980876064(L_89, L_88, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_89);
		}

IL_036b:
		{
			VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
		}

IL_0371:
		{
			Il2CppObject * L_90 = ___value0;
			VirtActionInvoker1< Il2CppObject * >::Invoke(29 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteDictionary(System.Collections.IEnumerable) */, __this, ((Il2CppObject *)Castclass(L_90, IEnumerable_t3464557803_il2cpp_TypeInfo_var)));
			IL2CPP_LEAVE(0x397, FINALLY_0382);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0382;
	}

FINALLY_0382:
	{ // begin finally (depth: 1)
		{
			bool L_91 = ___isProperty1;
			if (!L_91)
			{
				goto IL_0396;
			}
		}

IL_0388:
		{
			int32_t L_92 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_92-(int32_t)1)));
		}

IL_0396:
		{
			IL2CPP_END_FINALLY(898)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(898)
	{
		IL2CPP_JUMP_TBL(0x397, IL_0397)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0397:
	{
		return;
	}

IL_0398:
	{
		Il2CppObject * L_93 = ___value0;
		if (!((Il2CppObject *)IsInst(L_93, IEnumerable_t3464557803_il2cpp_TypeInfo_var)))
		{
			goto IL_041a;
		}
	}

IL_03a3:
	try
	{ // begin try (depth: 1)
		{
			bool L_94 = ___isProperty1;
			if (!L_94)
			{
				goto IL_03f3;
			}
		}

IL_03a9:
		{
			int32_t L_95 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_95+(int32_t)1)));
			int32_t L_96 = __this->get_depth_2();
			JsonWriterSettings_t3394579648 * L_97 = __this->get_settings_1();
			NullCheck(L_97);
			int32_t L_98 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_97);
			if ((((int32_t)L_96) <= ((int32_t)L_98)))
			{
				goto IL_03ed;
			}
		}

IL_03cd:
		{
			JsonWriterSettings_t3394579648 * L_99 = __this->get_settings_1();
			NullCheck(L_99);
			int32_t L_100 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_99);
			int32_t L_101 = L_100;
			Il2CppObject * L_102 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_101);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_103 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral107111942, L_102, /*hidden argument*/NULL);
			JsonSerializationException_t1208944969 * L_104 = (JsonSerializationException_t1208944969 *)il2cpp_codegen_object_new(JsonSerializationException_t1208944969_il2cpp_TypeInfo_var);
			JsonSerializationException__ctor_m1980876064(L_104, L_103, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_104);
		}

IL_03ed:
		{
			VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
		}

IL_03f3:
		{
			Il2CppObject * L_105 = ___value0;
			VirtActionInvoker1< Il2CppObject * >::Invoke(26 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteArray(System.Collections.IEnumerable) */, __this, ((Il2CppObject *)Castclass(L_105, IEnumerable_t3464557803_il2cpp_TypeInfo_var)));
			IL2CPP_LEAVE(0x419, FINALLY_0404);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0404;
	}

FINALLY_0404:
	{ // begin finally (depth: 1)
		{
			bool L_106 = ___isProperty1;
			if (!L_106)
			{
				goto IL_0418;
			}
		}

IL_040a:
		{
			int32_t L_107 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_107-(int32_t)1)));
		}

IL_0418:
		{
			IL2CPP_END_FINALLY(1028)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1028)
	{
		IL2CPP_JUMP_TBL(0x419, IL_0419)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0419:
	{
		return;
	}

IL_041a:
	try
	{ // begin try (depth: 1)
		{
			bool L_108 = ___isProperty1;
			if (!L_108)
			{
				goto IL_046a;
			}
		}

IL_0420:
		{
			int32_t L_109 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_109+(int32_t)1)));
			int32_t L_110 = __this->get_depth_2();
			JsonWriterSettings_t3394579648 * L_111 = __this->get_settings_1();
			NullCheck(L_111);
			int32_t L_112 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_111);
			if ((((int32_t)L_110) <= ((int32_t)L_112)))
			{
				goto IL_0464;
			}
		}

IL_0444:
		{
			JsonWriterSettings_t3394579648 * L_113 = __this->get_settings_1();
			NullCheck(L_113);
			int32_t L_114 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_113);
			int32_t L_115 = L_114;
			Il2CppObject * L_116 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_115);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_117 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral107111942, L_116, /*hidden argument*/NULL);
			JsonSerializationException_t1208944969 * L_118 = (JsonSerializationException_t1208944969 *)il2cpp_codegen_object_new(JsonSerializationException_t1208944969_il2cpp_TypeInfo_var);
			JsonSerializationException__ctor_m1980876064(L_118, L_117, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_118);
		}

IL_0464:
		{
			VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
		}

IL_046a:
		{
			Il2CppObject * L_119 = ___value0;
			Type_t * L_120 = V_0;
			VirtActionInvoker3< Il2CppObject *, Type_t *, bool >::Invoke(32 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObject(System.Object,System.Type,System.Boolean) */, __this, L_119, L_120, (bool)0);
			IL2CPP_LEAVE(0x48D, FINALLY_0478);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0478;
	}

FINALLY_0478:
	{ // begin finally (depth: 1)
		{
			bool L_121 = ___isProperty1;
			if (!L_121)
			{
				goto IL_048c;
			}
		}

IL_047e:
		{
			int32_t L_122 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_122-(int32_t)1)));
		}

IL_048c:
		{
			IL2CPP_END_FINALLY(1144)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1144)
	{
		IL2CPP_JUMP_TBL(0x48D, IL_048d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_048d:
	{
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.DateTime)
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* WriteDelegate_1_Invoke_m3448592485_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3567556939;
extern Il2CppCodeGenString* _stringLiteral115082479;
extern const uint32_t JsonWriter_Write_m1416422445_MetadataUsageId;
extern "C"  void JsonWriter_Write_m1416422445 (JsonWriter_t541860733 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m1416422445_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		JsonWriterSettings_t3394579648 * L_0 = __this->get_settings_1();
		NullCheck(L_0);
		WriteDelegate_1_t4036178619 * L_1 = VirtFuncInvoker0< WriteDelegate_1_t4036178619 * >::Invoke(11 /* Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime> Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DateTimeSerializer() */, L_0);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		JsonWriterSettings_t3394579648 * L_2 = __this->get_settings_1();
		NullCheck(L_2);
		WriteDelegate_1_t4036178619 * L_3 = VirtFuncInvoker0< WriteDelegate_1_t4036178619 * >::Invoke(11 /* Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime> Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DateTimeSerializer() */, L_2);
		DateTime_t4283661327  L_4 = ___value0;
		NullCheck(L_3);
		WriteDelegate_1_Invoke_m3448592485(L_3, __this, L_4, /*hidden argument*/WriteDelegate_1_Invoke_m3448592485_MethodInfo_var);
		return;
	}

IL_0023:
	{
		int32_t L_5 = DateTime_get_Kind_m3496013602((&___value0), /*hidden argument*/NULL);
		V_0 = L_5;
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) == ((int32_t)1)))
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) == ((int32_t)2)))
		{
			goto IL_003e;
		}
	}
	{
		goto IL_0067;
	}

IL_003e:
	{
		DateTime_t4283661327  L_8 = DateTime_ToUniversalTime_m691668206((&___value0), /*hidden argument*/NULL);
		___value0 = L_8;
		goto IL_004c;
	}

IL_004c:
	{
		DateTime_t4283661327  L_9 = ___value0;
		DateTime_t4283661327  L_10 = L_9;
		Il2CppObject * L_11 = Box(DateTime_t4283661327_il2cpp_TypeInfo_var, &L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral3567556939, L_11, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_12);
		goto IL_0082;
	}

IL_0067:
	{
		DateTime_t4283661327  L_13 = ___value0;
		DateTime_t4283661327  L_14 = L_13;
		Il2CppObject * L_15 = Box(DateTime_t4283661327_il2cpp_TypeInfo_var, &L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral115082479, L_15, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_16);
		goto IL_0082;
	}

IL_0082:
	{
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Guid)
extern Il2CppCodeGenString* _stringLiteral68;
extern const uint32_t JsonWriter_Write_m203707007_MetadataUsageId;
extern "C"  void JsonWriter_Write_m203707007 (JsonWriter_t541860733 * __this, Guid_t2862754429  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m203707007_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Guid_ToString_m2135662273((&___value0), _stringLiteral68, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_0);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Enum)
extern const Il2CppType* FlagsAttribute_t423505161_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral102;
extern Il2CppCodeGenString* _stringLiteral1396;
extern const uint32_t JsonWriter_Write_m201663239_MetadataUsageId;
extern "C"  void JsonWriter_Write_m201663239 (JsonWriter_t541860733 * __this, Enum_t2862688501 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m201663239_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Type_t * V_1 = NULL;
	EnumU5BU5D_t3205174168* V_2 = NULL;
	StringU5BU5D_t4054002952* V_3 = NULL;
	int32_t V_4 = 0;
	{
		V_0 = (String_t*)NULL;
		Enum_t2862688501 * L_0 = ___value0;
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m2022236990(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		Type_t * L_2 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(FlagsAttribute_t423505161_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_4 = VirtFuncInvoker2< bool, Type_t *, bool >::Invoke(11 /* System.Boolean System.Reflection.MemberInfo::IsDefined(System.Type,System.Boolean) */, L_2, L_3, (bool)1);
		if (!L_4)
		{
			goto IL_0092;
		}
	}
	{
		Type_t * L_5 = V_1;
		Enum_t2862688501 * L_6 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		bool L_7 = Enum_IsDefined_m2781598580(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0092;
		}
	}
	{
		Type_t * L_8 = V_1;
		Enum_t2862688501 * L_9 = ___value0;
		EnumU5BU5D_t3205174168* L_10 = JsonWriter_GetFlagList_m4244203690(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		EnumU5BU5D_t3205174168* L_11 = V_2;
		NullCheck(L_11);
		V_3 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length))))));
		V_4 = 0;
		goto IL_0077;
	}

IL_0044:
	{
		StringU5BU5D_t4054002952* L_12 = V_3;
		int32_t L_13 = V_4;
		EnumU5BU5D_t3205174168* L_14 = V_2;
		int32_t L_15 = V_4;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		Enum_t2862688501 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		String_t* L_18 = JsonNameAttribute_GetJsonName_m166270748(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		ArrayElementTypeCheck (L_12, L_18);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(L_13), (String_t*)L_18);
		StringU5BU5D_t4054002952* L_19 = V_3;
		int32_t L_20 = V_4;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = L_20;
		String_t* L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_23 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0071;
		}
	}
	{
		StringU5BU5D_t4054002952* L_24 = V_3;
		int32_t L_25 = V_4;
		EnumU5BU5D_t3205174168* L_26 = V_2;
		int32_t L_27 = V_4;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = L_27;
		Enum_t2862688501 * L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_29);
		String_t* L_30 = Enum_ToString_m1466259273(L_29, _stringLiteral102, /*hidden argument*/NULL);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		ArrayElementTypeCheck (L_24, L_30);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(L_25), (String_t*)L_30);
	}

IL_0071:
	{
		int32_t L_31 = V_4;
		V_4 = ((int32_t)((int32_t)L_31+(int32_t)1));
	}

IL_0077:
	{
		int32_t L_32 = V_4;
		EnumU5BU5D_t3205174168* L_33 = V_2;
		NullCheck(L_33);
		if ((((int32_t)L_32) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_33)->max_length)))))))
		{
			goto IL_0044;
		}
	}
	{
		StringU5BU5D_t4054002952* L_34 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Join_m2789530325(NULL /*static, unused*/, _stringLiteral1396, L_34, /*hidden argument*/NULL);
		V_0 = L_35;
		goto IL_00b0;
	}

IL_0092:
	{
		Enum_t2862688501 * L_36 = ___value0;
		String_t* L_37 = JsonNameAttribute_GetJsonName_m166270748(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		V_0 = L_37;
		String_t* L_38 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_39 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_00b0;
		}
	}
	{
		Enum_t2862688501 * L_40 = ___value0;
		NullCheck(L_40);
		String_t* L_41 = Enum_ToString_m1466259273(L_40, _stringLiteral102, /*hidden argument*/NULL);
		V_0 = L_41;
	}

IL_00b0:
	{
		String_t* L_42 = V_0;
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_42);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String)
extern Il2CppClass* JsonReader_t386455501_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2950;
extern Il2CppCodeGenString* _stringLiteral2954;
extern Il2CppCodeGenString* _stringLiteral2962;
extern Il2CppCodeGenString* _stringLiteral2966;
extern Il2CppCodeGenString* _stringLiteral2968;
extern Il2CppCodeGenString* _stringLiteral2969;
extern Il2CppCodeGenString* _stringLiteral2780;
extern const uint32_t JsonWriter_Write_m233948727_MetadataUsageId;
extern "C"  void JsonWriter_Write_m233948727 (JsonWriter_t541860733 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m233948727_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Il2CppChar V_3 = 0x0;
	Il2CppChar V_4 = 0x0;
	int32_t V_5 = 0;
	{
		String_t* L_0 = ___value0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		TextWriter_t2304124208 * L_1 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_2 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_LiteralNull_2();
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_1, L_2);
		return;
	}

IL_0017:
	{
		V_0 = 0;
		String_t* L_3 = ___value0;
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m2979997331(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		TextWriter_t2304124208 * L_5 = __this->get_Writer_0();
		NullCheck(L_5);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_5, ((int32_t)34));
		int32_t L_6 = V_0;
		V_2 = L_6;
		goto IL_017f;
	}

IL_0034:
	{
		String_t* L_7 = ___value0;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		Il2CppChar L_9 = String_get_Chars_m3015341861(L_7, L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		Il2CppChar L_10 = V_3;
		if ((((int32_t)L_10) <= ((int32_t)((int32_t)31))))
		{
			goto IL_0064;
		}
	}
	{
		Il2CppChar L_11 = V_3;
		if ((((int32_t)L_11) >= ((int32_t)((int32_t)127))))
		{
			goto IL_0064;
		}
	}
	{
		Il2CppChar L_12 = V_3;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)60))))
		{
			goto IL_0064;
		}
	}
	{
		Il2CppChar L_13 = V_3;
		if ((((int32_t)L_13) == ((int32_t)((int32_t)34))))
		{
			goto IL_0064;
		}
	}
	{
		Il2CppChar L_14 = V_3;
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_017b;
		}
	}

IL_0064:
	{
		int32_t L_15 = V_2;
		int32_t L_16 = V_0;
		if ((((int32_t)L_15) <= ((int32_t)L_16)))
		{
			goto IL_0080;
		}
	}
	{
		TextWriter_t2304124208 * L_17 = __this->get_Writer_0();
		String_t* L_18 = ___value0;
		int32_t L_19 = V_0;
		int32_t L_20 = V_2;
		int32_t L_21 = V_0;
		NullCheck(L_18);
		String_t* L_22 = String_Substring_m675079568(L_18, L_19, ((int32_t)((int32_t)L_20-(int32_t)L_21)), /*hidden argument*/NULL);
		NullCheck(L_17);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_17, L_22);
	}

IL_0080:
	{
		int32_t L_23 = V_2;
		V_0 = ((int32_t)((int32_t)L_23+(int32_t)1));
		Il2CppChar L_24 = V_3;
		V_4 = L_24;
		Il2CppChar L_25 = V_4;
		if (((int32_t)((int32_t)L_25-(int32_t)8)) == 0)
		{
			goto IL_00dd;
		}
		if (((int32_t)((int32_t)L_25-(int32_t)8)) == 1)
		{
			goto IL_0131;
		}
		if (((int32_t)((int32_t)L_25-(int32_t)8)) == 2)
		{
			goto IL_0107;
		}
		if (((int32_t)((int32_t)L_25-(int32_t)8)) == 3)
		{
			goto IL_00a8;
		}
		if (((int32_t)((int32_t)L_25-(int32_t)8)) == 4)
		{
			goto IL_00f2;
		}
		if (((int32_t)((int32_t)L_25-(int32_t)8)) == 5)
		{
			goto IL_011c;
		}
	}

IL_00a8:
	{
		Il2CppChar L_26 = V_4;
		if ((((int32_t)L_26) == ((int32_t)((int32_t)34))))
		{
			goto IL_00bf;
		}
	}
	{
		Il2CppChar L_27 = V_4;
		if ((((int32_t)L_27) == ((int32_t)((int32_t)92))))
		{
			goto IL_00bf;
		}
	}
	{
		goto IL_0146;
	}

IL_00bf:
	{
		TextWriter_t2304124208 * L_28 = __this->get_Writer_0();
		NullCheck(L_28);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_28, ((int32_t)92));
		TextWriter_t2304124208 * L_29 = __this->get_Writer_0();
		Il2CppChar L_30 = V_3;
		NullCheck(L_29);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_29, L_30);
		goto IL_017b;
	}

IL_00dd:
	{
		TextWriter_t2304124208 * L_31 = __this->get_Writer_0();
		NullCheck(L_31);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_31, _stringLiteral2950);
		goto IL_017b;
	}

IL_00f2:
	{
		TextWriter_t2304124208 * L_32 = __this->get_Writer_0();
		NullCheck(L_32);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_32, _stringLiteral2954);
		goto IL_017b;
	}

IL_0107:
	{
		TextWriter_t2304124208 * L_33 = __this->get_Writer_0();
		NullCheck(L_33);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_33, _stringLiteral2962);
		goto IL_017b;
	}

IL_011c:
	{
		TextWriter_t2304124208 * L_34 = __this->get_Writer_0();
		NullCheck(L_34);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_34, _stringLiteral2966);
		goto IL_017b;
	}

IL_0131:
	{
		TextWriter_t2304124208 * L_35 = __this->get_Writer_0();
		NullCheck(L_35);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_35, _stringLiteral2968);
		goto IL_017b;
	}

IL_0146:
	{
		TextWriter_t2304124208 * L_36 = __this->get_Writer_0();
		NullCheck(L_36);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_36, _stringLiteral2969);
		TextWriter_t2304124208 * L_37 = __this->get_Writer_0();
		String_t* L_38 = ___value0;
		int32_t L_39 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		int32_t L_40 = Char_ConvertToUtf32_m1170476182(NULL /*static, unused*/, L_38, L_39, /*hidden argument*/NULL);
		V_5 = L_40;
		String_t* L_41 = Int32_ToString_m3853485906((&V_5), _stringLiteral2780, /*hidden argument*/NULL);
		NullCheck(L_37);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_37, L_41);
		goto IL_017b;
	}

IL_017b:
	{
		int32_t L_42 = V_2;
		V_2 = ((int32_t)((int32_t)L_42+(int32_t)1));
	}

IL_017f:
	{
		int32_t L_43 = V_2;
		int32_t L_44 = V_1;
		if ((((int32_t)L_43) < ((int32_t)L_44)))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_45 = V_1;
		int32_t L_46 = V_0;
		if ((((int32_t)L_45) <= ((int32_t)L_46)))
		{
			goto IL_01a2;
		}
	}
	{
		TextWriter_t2304124208 * L_47 = __this->get_Writer_0();
		String_t* L_48 = ___value0;
		int32_t L_49 = V_0;
		int32_t L_50 = V_1;
		int32_t L_51 = V_0;
		NullCheck(L_48);
		String_t* L_52 = String_Substring_m675079568(L_48, L_49, ((int32_t)((int32_t)L_50-(int32_t)L_51)), /*hidden argument*/NULL);
		NullCheck(L_47);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_47, L_52);
	}

IL_01a2:
	{
		TextWriter_t2304124208 * L_53 = __this->get_Writer_0();
		NullCheck(L_53);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_53, ((int32_t)34));
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Boolean)
extern Il2CppClass* JsonReader_t386455501_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter_Write_m3168498914_MetadataUsageId;
extern "C"  void JsonWriter_Write_m3168498914 (JsonWriter_t541860733 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m3168498914_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TextWriter_t2304124208 * G_B2_0 = NULL;
	TextWriter_t2304124208 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	TextWriter_t2304124208 * G_B3_1 = NULL;
	{
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		bool L_1 = ___value0;
		G_B1_0 = L_0;
		if (!L_1)
		{
			G_B2_0 = L_0;
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_2 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_LiteralTrue_1();
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_001b;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_3 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_LiteralFalse_0();
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_001b:
	{
		NullCheck(G_B3_1);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, G_B3_1, G_B3_0);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Byte)
extern Il2CppClass* CultureInfo_t1065375142_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral103;
extern const uint32_t JsonWriter_Write_m199219168_MetadataUsageId;
extern "C"  void JsonWriter_Write_m199219168 (JsonWriter_t541860733 * __this, uint8_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m199219168_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_1 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = Byte_ToString_m1210459538((&___value0), _stringLiteral103, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_0, L_2);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.SByte)
extern Il2CppClass* CultureInfo_t1065375142_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral103;
extern const uint32_t JsonWriter_Write_m2316894095_MetadataUsageId;
extern "C"  void JsonWriter_Write_m2316894095 (JsonWriter_t541860733 * __this, int8_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m2316894095_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_1 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = SByte_ToString_m926863599((&___value0), _stringLiteral103, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_0, L_2);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Int16)
extern Il2CppClass* CultureInfo_t1065375142_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral103;
extern const uint32_t JsonWriter_Write_m2071022710_MetadataUsageId;
extern "C"  void JsonWriter_Write_m2071022710 (JsonWriter_t541860733 * __this, int16_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m2071022710_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_1 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = Int16_ToString_m416093480((&___value0), _stringLiteral103, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_0, L_2);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.UInt16)
extern Il2CppClass* CultureInfo_t1065375142_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral103;
extern const uint32_t JsonWriter_Write_m774476073_MetadataUsageId;
extern "C"  void JsonWriter_Write_m774476073 (JsonWriter_t541860733 * __this, uint16_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m774476073_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_1 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = UInt16_ToString_m1090572969((&___value0), _stringLiteral103, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_0, L_2);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Int32)
extern Il2CppClass* CultureInfo_t1065375142_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral103;
extern const uint32_t JsonWriter_Write_m2071024508_MetadataUsageId;
extern "C"  void JsonWriter_Write_m2071024508 (JsonWriter_t541860733 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m2071024508_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_1 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = Int32_ToString_m4261017954((&___value0), _stringLiteral103, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_0, L_2);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.UInt32)
extern Il2CppClass* Decimal_t1954350631_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t1065375142_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral103;
extern const uint32_t JsonWriter_Write_m774477871_MetadataUsageId;
extern "C"  void JsonWriter_Write_m774477871 (JsonWriter_t541860733 * __this, uint32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m774477871_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		Decimal_t1954350631  L_1 = Decimal_op_Implicit_m3967125074(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		bool L_2 = VirtFuncInvoker1< bool, Decimal_t1954350631  >::Invoke(36 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriter::InvalidIeee754(System.Decimal) */, __this, L_1);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_3 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_4 = UInt32_ToString_m640530147((&___value0), _stringLiteral103, L_3, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_4);
		return;
	}

IL_0029:
	{
		TextWriter_t2304124208 * L_5 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_6 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_7 = UInt32_ToString_m640530147((&___value0), _stringLiteral103, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_5, L_7);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Int64)
extern Il2CppClass* Decimal_t1954350631_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t1065375142_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral103;
extern const uint32_t JsonWriter_Write_m2071027453_MetadataUsageId;
extern "C"  void JsonWriter_Write_m2071027453 (JsonWriter_t541860733 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m2071027453_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int64_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		Decimal_t1954350631  L_1 = Decimal_op_Implicit_m3836584058(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		bool L_2 = VirtFuncInvoker1< bool, Decimal_t1954350631  >::Invoke(36 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriter::InvalidIeee754(System.Decimal) */, __this, L_1);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_3 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_4 = Int64_ToString_m3523878849((&___value0), _stringLiteral103, L_3, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_4);
		return;
	}

IL_0029:
	{
		TextWriter_t2304124208 * L_5 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_6 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_7 = Int64_ToString_m3523878849((&___value0), _stringLiteral103, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_5, L_7);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.UInt64)
extern Il2CppClass* Decimal_t1954350631_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t1065375142_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral103;
extern const uint32_t JsonWriter_Write_m774480816_MetadataUsageId;
extern "C"  void JsonWriter_Write_m774480816 (JsonWriter_t541860733 * __this, uint64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m774480816_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint64_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		Decimal_t1954350631  L_1 = Decimal_op_Implicit_m3967128019(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		bool L_2 = VirtFuncInvoker1< bool, Decimal_t1954350631  >::Invoke(36 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriter::InvalidIeee754(System.Decimal) */, __this, L_1);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_3 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_4 = UInt64_ToString_m4198358338((&___value0), _stringLiteral103, L_3, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_4);
		return;
	}

IL_0029:
	{
		TextWriter_t2304124208 * L_5 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_6 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_7 = UInt64_ToString_m4198358338((&___value0), _stringLiteral103, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_5, L_7);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Single)
extern Il2CppClass* JsonReader_t386455501_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t1065375142_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral114;
extern const uint32_t JsonWriter_Write_m4210239712_MetadataUsageId;
extern "C"  void JsonWriter_Write_m4210239712 (JsonWriter_t541860733 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m4210239712_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value0;
		bool L_1 = Single_IsNaN_m2036953453(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		float L_2 = ___value0;
		bool L_3 = Single_IsInfinity_m1725004900(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002b;
		}
	}

IL_0016:
	{
		TextWriter_t2304124208 * L_4 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_5 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_LiteralNull_2();
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_4, L_5);
		goto IL_0047;
	}

IL_002b:
	{
		TextWriter_t2304124208 * L_6 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_7 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = Single_ToString_m3798733330((&___value0), _stringLiteral114, L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_6, L_8);
	}

IL_0047:
	{
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Double)
extern Il2CppClass* JsonReader_t386455501_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t1065375142_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral114;
extern const uint32_t JsonWriter_Write_m3960676983_MetadataUsageId;
extern "C"  void JsonWriter_Write_m3960676983 (JsonWriter_t541860733 * __this, double ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m3960676983_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ___value0;
		bool L_1 = Double_IsNaN_m506471885(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		double L_2 = ___value0;
		bool L_3 = Double_IsInfinity_m3094155474(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002b;
		}
	}

IL_0016:
	{
		TextWriter_t2304124208 * L_4 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t386455501_il2cpp_TypeInfo_var);
		String_t* L_5 = ((JsonReader_t386455501_StaticFields*)JsonReader_t386455501_il2cpp_TypeInfo_var->static_fields)->get_LiteralNull_2();
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_4, L_5);
		goto IL_0047;
	}

IL_002b:
	{
		TextWriter_t2304124208 * L_6 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_7 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = Double_ToString_m2573497243((&___value0), _stringLiteral114, L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_6, L_8);
	}

IL_0047:
	{
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Decimal)
extern Il2CppClass* CultureInfo_t1065375142_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral103;
extern const uint32_t JsonWriter_Write_m1727967961_MetadataUsageId;
extern "C"  void JsonWriter_Write_m1727967961 (JsonWriter_t541860733 * __this, Decimal_t1954350631  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m1727967961_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Decimal_t1954350631  L_0 = ___value0;
		bool L_1 = VirtFuncInvoker1< bool, Decimal_t1954350631  >::Invoke(36 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriter::InvalidIeee754(System.Decimal) */, __this, L_0);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_2 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = Decimal_ToString_m3542473445((&___value0), _stringLiteral103, L_2, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_3);
		return;
	}

IL_0024:
	{
		TextWriter_t2304124208 * L_4 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_5 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_6 = Decimal_ToString_m3542473445((&___value0), _stringLiteral103, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_4, L_6);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Char)
extern "C"  void JsonWriter_Write_m199618386 (JsonWriter_t541860733 * __this, Il2CppChar ___value0, const MethodInfo* method)
{
	{
		Il2CppChar L_0 = ___value0;
		String_t* L_1 = String_CreateString_m356585284(NULL, L_0, 1, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_1);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.TimeSpan)
extern "C"  void JsonWriter_Write_m1701218193 (JsonWriter_t541860733 * __this, TimeSpan_t413522987  ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = TimeSpan_get_Ticks_m315930342((&___value0), /*hidden argument*/NULL);
		VirtActionInvoker1< int64_t >::Invoke(17 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Int64) */, __this, L_0);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Uri)
extern "C"  void JsonWriter_Write_m1669553310 (JsonWriter_t541860733 * __this, Uri_t1116831938 * ___value0, const MethodInfo* method)
{
	{
		Uri_t1116831938 * L_0 = ___value0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_1);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Version)
extern "C"  void JsonWriter_Write_m3472349746 (JsonWriter_t541860733 * __this, Version_t763695022 * ___value0, const MethodInfo* method)
{
	{
		Version_t763695022 * L_0 = ___value0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_1);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteArray(System.Collections.IEnumerable)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonSerializationException_t1208944969_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral107111942;
extern const uint32_t JsonWriter_WriteArray_m3196329751_MetadataUsageId;
extern "C"  void JsonWriter_WriteArray_m3196329751 (JsonWriter_t541860733 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_WriteArray_m3196329751_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)0;
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_0, ((int32_t)91));
		int32_t L_1 = __this->get_depth_2();
		__this->set_depth_2(((int32_t)((int32_t)L_1+(int32_t)1)));
		int32_t L_2 = __this->get_depth_2();
		JsonWriterSettings_t3394579648 * L_3 = __this->get_settings_1();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_3);
		if ((((int32_t)L_2) <= ((int32_t)L_4)))
		{
			goto IL_0053;
		}
	}
	{
		JsonWriterSettings_t3394579648 * L_5 = __this->get_settings_1();
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_5);
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral107111942, L_8, /*hidden argument*/NULL);
		JsonSerializationException_t1208944969 * L_10 = (JsonSerializationException_t1208944969 *)il2cpp_codegen_object_new(JsonSerializationException_t1208944969_il2cpp_TypeInfo_var);
		JsonSerializationException__ctor_m1980876064(L_10, L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_0053:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_11 = ___value0;
			NullCheck(L_11);
			Il2CppObject * L_12 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, L_11);
			V_1 = L_12;
		}

IL_005a:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0086;
			}

IL_005f:
			{
				Il2CppObject * L_13 = V_1;
				NullCheck(L_13);
				Il2CppObject * L_14 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_13);
				V_2 = L_14;
				bool L_15 = V_0;
				if (!L_15)
				{
					goto IL_0077;
				}
			}

IL_006c:
			{
				VirtActionInvoker0::Invoke(33 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteArrayItemDelim() */, __this);
				goto IL_0079;
			}

IL_0077:
			{
				V_0 = (bool)1;
			}

IL_0079:
			{
				VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
				Il2CppObject * L_16 = V_2;
				VirtActionInvoker1< Il2CppObject * >::Invoke(27 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteArrayItem(System.Object) */, __this, L_16);
			}

IL_0086:
			{
				Il2CppObject * L_17 = V_1;
				NullCheck(L_17);
				bool L_18 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_17);
				if (L_18)
				{
					goto IL_005f;
				}
			}

IL_0091:
			{
				IL2CPP_LEAVE(0xAA, FINALLY_0096);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
			goto FINALLY_0096;
		}

FINALLY_0096:
		{ // begin finally (depth: 2)
			{
				Il2CppObject * L_19 = V_1;
				Il2CppObject * L_20 = ((Il2CppObject *)IsInst(L_19, IDisposable_t1423340799_il2cpp_TypeInfo_var));
				V_3 = L_20;
				if (!L_20)
				{
					goto IL_00a9;
				}
			}

IL_00a3:
			{
				Il2CppObject * L_21 = V_3;
				NullCheck(L_21);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_21);
			}

IL_00a9:
			{
				IL2CPP_END_FINALLY(150)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(150)
		{
			IL2CPP_JUMP_TBL(0xAA, IL_00aa)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
		}

IL_00aa:
		{
			IL2CPP_LEAVE(0xBE, FINALLY_00af);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00af;
	}

FINALLY_00af:
	{ // begin finally (depth: 1)
		int32_t L_22 = __this->get_depth_2();
		__this->set_depth_2(((int32_t)((int32_t)L_22-(int32_t)1)));
		IL2CPP_END_FINALLY(175)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(175)
	{
		IL2CPP_JUMP_TBL(0xBE, IL_00be)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00be:
	{
		bool L_23 = V_0;
		if (!L_23)
		{
			goto IL_00ca;
		}
	}
	{
		VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
	}

IL_00ca:
	{
		TextWriter_t2304124208 * L_24 = __this->get_Writer_0();
		NullCheck(L_24);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_24, ((int32_t)93));
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteArrayItem(System.Object)
extern "C"  void JsonWriter_WriteArrayItem_m2875804145 (JsonWriter_t541860733 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___item0;
		VirtActionInvoker2< Il2CppObject *, bool >::Invoke(5 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Object,System.Boolean) */, __this, L_0, (bool)0);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObject(System.Collections.IDictionary)
extern "C"  void JsonWriter_WriteObject_m1204844483 (JsonWriter_t541860733 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		VirtActionInvoker1< Il2CppObject * >::Invoke(29 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteDictionary(System.Collections.IEnumerable) */, __this, L_0);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteDictionary(System.Collections.IEnumerable)
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonSerializationException_t1208944969_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m3406889972_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Count_m1389733239_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m3597588045_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral649209863;
extern Il2CppCodeGenString* _stringLiteral2019411;
extern Il2CppCodeGenString* _stringLiteral107111942;
extern const uint32_t JsonWriter_WriteDictionary_m862648104_MetadataUsageId;
extern "C"  void JsonWriter_WriteDictionary_m862648104 (JsonWriter_t541860733 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_WriteDictionary_m862648104_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	DictionaryEntry_t1751606614  V_3;
	memset(&V_3, 0, sizeof(V_3));
	DictionaryEntry_t1751606614  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, L_0);
		V_0 = ((Il2CppObject *)IsInst(L_1, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var));
		Il2CppObject * L_2 = V_0;
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		Il2CppObject * L_3 = ___value0;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m2022236990(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral649209863, L_4, /*hidden argument*/NULL);
		JsonSerializationException_t1208944969 * L_6 = (JsonSerializationException_t1208944969 *)il2cpp_codegen_object_new(JsonSerializationException_t1208944969_il2cpp_TypeInfo_var);
		JsonSerializationException__ctor_m1980876064(L_6, L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0028:
	{
		V_1 = (bool)0;
		JsonWriterSettings_t3394579648 * L_7 = __this->get_settings_1();
		NullCheck(L_7);
		bool L_8 = JsonWriterSettings_get_HandleCyclicReferences_m1680156523(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0098;
		}
	}
	{
		V_2 = 0;
		Dictionary_2_t3323877696 * L_9 = __this->get_previouslySerializedObjects_3();
		Il2CppObject * L_10 = ___value0;
		NullCheck(L_9);
		bool L_11 = Dictionary_2_TryGetValue_m3406889972(L_9, L_10, (&V_2), /*hidden argument*/Dictionary_2_TryGetValue_m3406889972_MethodInfo_var);
		if (!L_11)
		{
			goto IL_0081;
		}
	}
	{
		TextWriter_t2304124208 * L_12 = __this->get_Writer_0();
		NullCheck(L_12);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_12, ((int32_t)123));
		int32_t L_13 = V_2;
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_14);
		JsonWriter_WriteObjectProperty_m92869745(__this, _stringLiteral2019411, L_15, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
		TextWriter_t2304124208 * L_16 = __this->get_Writer_0();
		NullCheck(L_16);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_16, ((int32_t)125));
		return;
	}

IL_0081:
	{
		Dictionary_2_t3323877696 * L_17 = __this->get_previouslySerializedObjects_3();
		Il2CppObject * L_18 = ___value0;
		Dictionary_2_t3323877696 * L_19 = __this->get_previouslySerializedObjects_3();
		NullCheck(L_19);
		int32_t L_20 = Dictionary_2_get_Count_m1389733239(L_19, /*hidden argument*/Dictionary_2_get_Count_m1389733239_MethodInfo_var);
		NullCheck(L_17);
		Dictionary_2_Add_m3597588045(L_17, L_18, L_20, /*hidden argument*/Dictionary_2_Add_m3597588045_MethodInfo_var);
	}

IL_0098:
	{
		TextWriter_t2304124208 * L_21 = __this->get_Writer_0();
		NullCheck(L_21);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_21, ((int32_t)123));
		int32_t L_22 = __this->get_depth_2();
		__this->set_depth_2(((int32_t)((int32_t)L_22+(int32_t)1)));
		int32_t L_23 = __this->get_depth_2();
		JsonWriterSettings_t3394579648 * L_24 = __this->get_settings_1();
		NullCheck(L_24);
		int32_t L_25 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_24);
		if ((((int32_t)L_23) <= ((int32_t)L_25)))
		{
			goto IL_00e9;
		}
	}
	{
		JsonWriterSettings_t3394579648 * L_26 = __this->get_settings_1();
		NullCheck(L_26);
		int32_t L_27 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_26);
		int32_t L_28 = L_27;
		Il2CppObject * L_29 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_28);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral107111942, L_29, /*hidden argument*/NULL);
		JsonSerializationException_t1208944969 * L_31 = (JsonSerializationException_t1208944969 *)il2cpp_codegen_object_new(JsonSerializationException_t1208944969_il2cpp_TypeInfo_var);
		JsonSerializationException__ctor_m1980876064(L_31, L_30, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_31);
	}

IL_00e9:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0129;
		}

IL_00ee:
		{
			bool L_32 = V_1;
			if (!L_32)
			{
				goto IL_00ff;
			}
		}

IL_00f4:
		{
			VirtActionInvoker0::Invoke(34 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectPropertyDelim() */, __this);
			goto IL_0101;
		}

IL_00ff:
		{
			V_1 = (bool)1;
		}

IL_0101:
		{
			Il2CppObject * L_33 = V_0;
			NullCheck(L_33);
			DictionaryEntry_t1751606614  L_34 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, L_33);
			V_3 = L_34;
			Il2CppObject * L_35 = DictionaryEntry_get_Key_m3516209325((&V_3), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
			String_t* L_36 = Convert_ToString_m427788191(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
			Il2CppObject * L_37 = V_0;
			NullCheck(L_37);
			DictionaryEntry_t1751606614  L_38 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, L_37);
			V_4 = L_38;
			Il2CppObject * L_39 = DictionaryEntry_get_Value_m4281303039((&V_4), /*hidden argument*/NULL);
			JsonWriter_WriteObjectProperty_m92869745(__this, L_36, L_39, /*hidden argument*/NULL);
		}

IL_0129:
		{
			Il2CppObject * L_40 = V_0;
			NullCheck(L_40);
			bool L_41 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_40);
			if (L_41)
			{
				goto IL_00ee;
			}
		}

IL_0134:
		{
			IL2CPP_LEAVE(0x148, FINALLY_0139);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0139;
	}

FINALLY_0139:
	{ // begin finally (depth: 1)
		int32_t L_42 = __this->get_depth_2();
		__this->set_depth_2(((int32_t)((int32_t)L_42-(int32_t)1)));
		IL2CPP_END_FINALLY(313)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(313)
	{
		IL2CPP_JUMP_TBL(0x148, IL_0148)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0148:
	{
		bool L_43 = V_1;
		if (!L_43)
		{
			goto IL_0154;
		}
	}
	{
		VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
	}

IL_0154:
	{
		TextWriter_t2304124208 * L_44 = __this->get_Writer_0();
		NullCheck(L_44);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_44, ((int32_t)125));
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectProperty(System.String,System.Object)
extern "C"  void JsonWriter_WriteObjectProperty_m92869745 (JsonWriter_t541860733 * __this, String_t* ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
		String_t* L_0 = ___key0;
		VirtActionInvoker1< String_t* >::Invoke(30 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectPropertyName(System.String) */, __this, L_0);
		TextWriter_t2304124208 * L_1 = __this->get_Writer_0();
		NullCheck(L_1);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_1, ((int32_t)58));
		Il2CppObject * L_2 = ___value1;
		VirtActionInvoker1< Il2CppObject * >::Invoke(31 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectPropertyValue(System.Object) */, __this, L_2);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectPropertyName(System.String)
extern "C"  void JsonWriter_WriteObjectPropertyName_m3042846680 (JsonWriter_t541860733 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_0);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectPropertyValue(System.Object)
extern "C"  void JsonWriter_WriteObjectPropertyValue_m4189700800 (JsonWriter_t541860733 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		VirtActionInvoker2< Il2CppObject *, bool >::Invoke(5 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Object,System.Boolean) */, __this, L_0, (bool)1);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObject(System.Object,System.Type,System.Boolean)
extern const Il2CppType* JsonMemberAttribute_t1310195012_0_0_0_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonSerializationException_t1208944969_il2cpp_TypeInfo_var;
extern Il2CppClass* Console_t1363597357_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m3406889972_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Count_m1389733239_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m3597588045_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2019411;
extern Il2CppCodeGenString* _stringLiteral107111942;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral380572579;
extern Il2CppCodeGenString* _stringLiteral2660172893;
extern Il2CppCodeGenString* _stringLiteral1875473293;
extern Il2CppCodeGenString* _stringLiteral2310110408;
extern Il2CppCodeGenString* _stringLiteral757007950;
extern Il2CppCodeGenString* _stringLiteral1745733569;
extern Il2CppCodeGenString* _stringLiteral1422600610;
extern Il2CppCodeGenString* _stringLiteral2428103372;
extern Il2CppCodeGenString* _stringLiteral509017720;
extern const uint32_t JsonWriter_WriteObject_m2758146720_MetadataUsageId;
extern "C"  void JsonWriter_WriteObject_m2758146720 (JsonWriter_t541860733 * __this, Il2CppObject * ___value0, Type_t * ___type1, bool ___serializePrivate2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_WriteObject_m2758146720_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	bool V_2 = false;
	PropertyInfoU5BU5D_t4286713048* V_3 = NULL;
	PropertyInfoU5BU5D_t4286713048* V_4 = NULL;
	int32_t V_5 = 0;
	PropertyInfo_t * V_6 = NULL;
	Il2CppObject * V_7 = NULL;
	String_t* V_8 = NULL;
	FieldInfoU5BU5D_t2567562023* V_9 = NULL;
	FieldInfoU5BU5D_t2567562023* V_10 = NULL;
	int32_t V_11 = 0;
	FieldInfo_t * V_12 = NULL;
	Il2CppObject * V_13 = NULL;
	String_t* V_14 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B15_0 = 0;
	{
		V_0 = (bool)0;
		JsonWriterSettings_t3394579648 * L_0 = __this->get_settings_1();
		NullCheck(L_0);
		bool L_1 = JsonWriterSettings_get_HandleCyclicReferences_m1680156523(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007b;
		}
	}
	{
		Type_t * L_2 = ___type1;
		NullCheck(L_2);
		bool L_3 = Type_get_IsValueType_m1914757235(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_007b;
		}
	}
	{
		V_1 = 0;
		Dictionary_2_t3323877696 * L_4 = __this->get_previouslySerializedObjects_3();
		Il2CppObject * L_5 = ___value0;
		NullCheck(L_4);
		bool L_6 = Dictionary_2_TryGetValue_m3406889972(L_4, L_5, (&V_1), /*hidden argument*/Dictionary_2_TryGetValue_m3406889972_MethodInfo_var);
		if (!L_6)
		{
			goto IL_0064;
		}
	}
	{
		TextWriter_t2304124208 * L_7 = __this->get_Writer_0();
		NullCheck(L_7);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_7, ((int32_t)123));
		int32_t L_8 = V_1;
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_9);
		JsonWriter_WriteObjectProperty_m92869745(__this, _stringLiteral2019411, L_10, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
		TextWriter_t2304124208 * L_11 = __this->get_Writer_0();
		NullCheck(L_11);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_11, ((int32_t)125));
		return;
	}

IL_0064:
	{
		Dictionary_2_t3323877696 * L_12 = __this->get_previouslySerializedObjects_3();
		Il2CppObject * L_13 = ___value0;
		Dictionary_2_t3323877696 * L_14 = __this->get_previouslySerializedObjects_3();
		NullCheck(L_14);
		int32_t L_15 = Dictionary_2_get_Count_m1389733239(L_14, /*hidden argument*/Dictionary_2_get_Count_m1389733239_MethodInfo_var);
		NullCheck(L_12);
		Dictionary_2_Add_m3597588045(L_12, L_13, L_15, /*hidden argument*/Dictionary_2_Add_m3597588045_MethodInfo_var);
	}

IL_007b:
	{
		TextWriter_t2304124208 * L_16 = __this->get_Writer_0();
		NullCheck(L_16);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_16, ((int32_t)123));
		int32_t L_17 = __this->get_depth_2();
		__this->set_depth_2(((int32_t)((int32_t)L_17+(int32_t)1)));
		int32_t L_18 = __this->get_depth_2();
		JsonWriterSettings_t3394579648 * L_19 = __this->get_settings_1();
		NullCheck(L_19);
		int32_t L_20 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_19);
		if ((((int32_t)L_18) <= ((int32_t)L_20)))
		{
			goto IL_00cc;
		}
	}
	{
		JsonWriterSettings_t3394579648 * L_21 = __this->get_settings_1();
		NullCheck(L_21);
		int32_t L_22 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_21);
		int32_t L_23 = L_22;
		Il2CppObject * L_24 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_23);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral107111942, L_24, /*hidden argument*/NULL);
		JsonSerializationException_t1208944969 * L_26 = (JsonSerializationException_t1208944969 *)il2cpp_codegen_object_new(JsonSerializationException_t1208944969_il2cpp_TypeInfo_var);
		JsonSerializationException__ctor_m1980876064(L_26, L_25, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_26);
	}

IL_00cc:
	try
	{ // begin try (depth: 1)
		{
			JsonWriterSettings_t3394579648 * L_27 = __this->get_settings_1();
			NullCheck(L_27);
			String_t* L_28 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_TypeHintName() */, L_27);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_29 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
			if (L_29)
			{
				goto IL_0125;
			}
		}

IL_00e1:
		{
			bool L_30 = V_0;
			if (!L_30)
			{
				goto IL_00f2;
			}
		}

IL_00e7:
		{
			VirtActionInvoker0::Invoke(34 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectPropertyDelim() */, __this);
			goto IL_00f4;
		}

IL_00f2:
		{
			V_0 = (bool)1;
		}

IL_00f4:
		{
			JsonWriterSettings_t3394579648 * L_31 = __this->get_settings_1();
			NullCheck(L_31);
			String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_TypeHintName() */, L_31);
			Type_t * L_33 = ___type1;
			NullCheck(L_33);
			String_t* L_34 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_33);
			Type_t * L_35 = ___type1;
			NullCheck(L_35);
			Assembly_t1418687608 * L_36 = VirtFuncInvoker0< Assembly_t1418687608 * >::Invoke(14 /* System.Reflection.Assembly System.Type::get_Assembly() */, L_35);
			NullCheck(L_36);
			AssemblyName_t2915647011 * L_37 = VirtFuncInvoker0< AssemblyName_t2915647011 * >::Invoke(19 /* System.Reflection.AssemblyName System.Reflection.Assembly::GetName() */, L_36);
			NullCheck(L_37);
			String_t* L_38 = AssemblyName_get_Name_m1123490526(L_37, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_39 = String_Concat_m1825781833(NULL /*static, unused*/, L_34, _stringLiteral1396, L_38, /*hidden argument*/NULL);
			JsonWriter_WriteObjectProperty_m92869745(__this, L_32, L_39, /*hidden argument*/NULL);
		}

IL_0125:
		{
			Type_t * L_40 = ___type1;
			NullCheck(L_40);
			bool L_41 = VirtFuncInvoker0< bool >::Invoke(96 /* System.Boolean System.Type::get_IsGenericType() */, L_40);
			if (!L_41)
			{
				goto IL_0142;
			}
		}

IL_0130:
		{
			Type_t * L_42 = ___type1;
			NullCheck(L_42);
			String_t* L_43 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_42);
			NullCheck(L_43);
			bool L_44 = String_StartsWith_m1500793453(L_43, _stringLiteral380572579, /*hidden argument*/NULL);
			G_B15_0 = ((int32_t)(L_44));
			goto IL_0143;
		}

IL_0142:
		{
			G_B15_0 = 0;
		}

IL_0143:
		{
			V_2 = (bool)G_B15_0;
			Type_t * L_45 = ___type1;
			NullCheck(L_45);
			PropertyInfoU5BU5D_t4286713048* L_46 = Type_GetProperties_m3853308260(L_45, /*hidden argument*/NULL);
			V_3 = L_46;
			PropertyInfoU5BU5D_t4286713048* L_47 = V_3;
			V_4 = L_47;
			V_5 = 0;
			goto IL_02e3;
		}

IL_0156:
		{
			PropertyInfoU5BU5D_t4286713048* L_48 = V_4;
			int32_t L_49 = V_5;
			NullCheck(L_48);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_48, L_49);
			int32_t L_50 = L_49;
			PropertyInfo_t * L_51 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
			V_6 = L_51;
			PropertyInfo_t * L_52 = V_6;
			NullCheck(L_52);
			bool L_53 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Reflection.PropertyInfo::get_CanRead() */, L_52);
			if (L_53)
			{
				goto IL_0199;
			}
		}

IL_0169:
		{
			JsonWriterSettings_t3394579648 * L_54 = JsonWriter_get_Settings_m3312657913(__this, /*hidden argument*/NULL);
			NullCheck(L_54);
			bool L_55 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DebugMode() */, L_54);
			if (!L_55)
			{
				goto IL_0194;
			}
		}

IL_0179:
		{
			PropertyInfo_t * L_56 = V_6;
			NullCheck(L_56);
			String_t* L_57 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_56);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_58 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2660172893, L_57, _stringLiteral1875473293, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Console_t1363597357_il2cpp_TypeInfo_var);
			Console_WriteLine_m2829201975(NULL /*static, unused*/, L_58, /*hidden argument*/NULL);
		}

IL_0194:
		{
			goto IL_02dd;
		}

IL_0199:
		{
			PropertyInfo_t * L_59 = V_6;
			NullCheck(L_59);
			bool L_60 = VirtFuncInvoker0< bool >::Invoke(16 /* System.Boolean System.Reflection.PropertyInfo::get_CanWrite() */, L_59);
			if (L_60)
			{
				goto IL_01db;
			}
		}

IL_01a5:
		{
			bool L_61 = V_2;
			if (L_61)
			{
				goto IL_01db;
			}
		}

IL_01ab:
		{
			JsonWriterSettings_t3394579648 * L_62 = JsonWriter_get_Settings_m3312657913(__this, /*hidden argument*/NULL);
			NullCheck(L_62);
			bool L_63 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DebugMode() */, L_62);
			if (!L_63)
			{
				goto IL_01d6;
			}
		}

IL_01bb:
		{
			PropertyInfo_t * L_64 = V_6;
			NullCheck(L_64);
			String_t* L_65 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_64);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_66 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2660172893, L_65, _stringLiteral2310110408, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Console_t1363597357_il2cpp_TypeInfo_var);
			Console_WriteLine_m2829201975(NULL /*static, unused*/, L_66, /*hidden argument*/NULL);
		}

IL_01d6:
		{
			goto IL_02dd;
		}

IL_01db:
		{
			Type_t * L_67 = ___type1;
			PropertyInfo_t * L_68 = V_6;
			Il2CppObject * L_69 = ___value0;
			bool L_70 = JsonWriter_IsIgnored_m1265037635(__this, L_67, L_68, L_69, /*hidden argument*/NULL);
			if (!L_70)
			{
				goto IL_021a;
			}
		}

IL_01ea:
		{
			JsonWriterSettings_t3394579648 * L_71 = JsonWriter_get_Settings_m3312657913(__this, /*hidden argument*/NULL);
			NullCheck(L_71);
			bool L_72 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DebugMode() */, L_71);
			if (!L_72)
			{
				goto IL_0215;
			}
		}

IL_01fa:
		{
			PropertyInfo_t * L_73 = V_6;
			NullCheck(L_73);
			String_t* L_74 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_73);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_75 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2660172893, L_74, _stringLiteral757007950, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Console_t1363597357_il2cpp_TypeInfo_var);
			Console_WriteLine_m2829201975(NULL /*static, unused*/, L_75, /*hidden argument*/NULL);
		}

IL_0215:
		{
			goto IL_02dd;
		}

IL_021a:
		{
			PropertyInfo_t * L_76 = V_6;
			NullCheck(L_76);
			ParameterInfoU5BU5D_t2015293532* L_77 = VirtFuncInvoker0< ParameterInfoU5BU5D_t2015293532* >::Invoke(21 /* System.Reflection.ParameterInfo[] System.Reflection.PropertyInfo::GetIndexParameters() */, L_76);
			NullCheck(L_77);
			if (!(((int32_t)((int32_t)(((Il2CppArray *)L_77)->max_length)))))
			{
				goto IL_0258;
			}
		}

IL_0228:
		{
			JsonWriterSettings_t3394579648 * L_78 = JsonWriter_get_Settings_m3312657913(__this, /*hidden argument*/NULL);
			NullCheck(L_78);
			bool L_79 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DebugMode() */, L_78);
			if (!L_79)
			{
				goto IL_0253;
			}
		}

IL_0238:
		{
			PropertyInfo_t * L_80 = V_6;
			NullCheck(L_80);
			String_t* L_81 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_80);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_82 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2660172893, L_81, _stringLiteral1745733569, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Console_t1363597357_il2cpp_TypeInfo_var);
			Console_WriteLine_m2829201975(NULL /*static, unused*/, L_82, /*hidden argument*/NULL);
		}

IL_0253:
		{
			goto IL_02dd;
		}

IL_0258:
		{
			PropertyInfo_t * L_83 = V_6;
			Il2CppObject * L_84 = ___value0;
			NullCheck(L_83);
			Il2CppObject * L_85 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t1108656482* >::Invoke(24 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_83, L_84, (ObjectU5BU5D_t1108656482*)(ObjectU5BU5D_t1108656482*)NULL);
			V_7 = L_85;
			PropertyInfo_t * L_86 = V_6;
			Il2CppObject * L_87 = V_7;
			bool L_88 = JsonWriter_IsDefaultValue_m1681459346(__this, L_86, L_87, /*hidden argument*/NULL);
			if (!L_88)
			{
				goto IL_02a2;
			}
		}

IL_0272:
		{
			JsonWriterSettings_t3394579648 * L_89 = JsonWriter_get_Settings_m3312657913(__this, /*hidden argument*/NULL);
			NullCheck(L_89);
			bool L_90 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DebugMode() */, L_89);
			if (!L_90)
			{
				goto IL_029d;
			}
		}

IL_0282:
		{
			PropertyInfo_t * L_91 = V_6;
			NullCheck(L_91);
			String_t* L_92 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_91);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_93 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2660172893, L_92, _stringLiteral1422600610, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Console_t1363597357_il2cpp_TypeInfo_var);
			Console_WriteLine_m2829201975(NULL /*static, unused*/, L_93, /*hidden argument*/NULL);
		}

IL_029d:
		{
			goto IL_02dd;
		}

IL_02a2:
		{
			bool L_94 = V_0;
			if (!L_94)
			{
				goto IL_02b3;
			}
		}

IL_02a8:
		{
			VirtActionInvoker0::Invoke(34 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectPropertyDelim() */, __this);
			goto IL_02b5;
		}

IL_02b3:
		{
			V_0 = (bool)1;
		}

IL_02b5:
		{
			PropertyInfo_t * L_95 = V_6;
			String_t* L_96 = JsonNameAttribute_GetJsonName_m166270748(NULL /*static, unused*/, L_95, /*hidden argument*/NULL);
			V_8 = L_96;
			String_t* L_97 = V_8;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_98 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_97, /*hidden argument*/NULL);
			if (!L_98)
			{
				goto IL_02d3;
			}
		}

IL_02ca:
		{
			PropertyInfo_t * L_99 = V_6;
			NullCheck(L_99);
			String_t* L_100 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_99);
			V_8 = L_100;
		}

IL_02d3:
		{
			String_t* L_101 = V_8;
			Il2CppObject * L_102 = V_7;
			JsonWriter_WriteObjectProperty_m92869745(__this, L_101, L_102, /*hidden argument*/NULL);
		}

IL_02dd:
		{
			int32_t L_103 = V_5;
			V_5 = ((int32_t)((int32_t)L_103+(int32_t)1));
		}

IL_02e3:
		{
			int32_t L_104 = V_5;
			PropertyInfoU5BU5D_t4286713048* L_105 = V_4;
			NullCheck(L_105);
			if ((((int32_t)L_104) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_105)->max_length)))))))
			{
				goto IL_0156;
			}
		}

IL_02ee:
		{
			Type_t * L_106 = ___type1;
			NullCheck(L_106);
			FieldInfoU5BU5D_t2567562023* L_107 = VirtFuncInvoker1< FieldInfoU5BU5D_t2567562023*, int32_t >::Invoke(54 /* System.Reflection.FieldInfo[] System.Type::GetFields(System.Reflection.BindingFlags) */, L_106, ((int32_t)52));
			V_9 = L_107;
			FieldInfoU5BU5D_t2567562023* L_108 = V_9;
			V_10 = L_108;
			V_11 = 0;
			goto IL_043b;
		}

IL_0304:
		{
			FieldInfoU5BU5D_t2567562023* L_109 = V_10;
			int32_t L_110 = V_11;
			NullCheck(L_109);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_109, L_110);
			int32_t L_111 = L_110;
			FieldInfo_t * L_112 = (L_109)->GetAt(static_cast<il2cpp_array_size_t>(L_111));
			V_12 = L_112;
			FieldInfo_t * L_113 = V_12;
			NullCheck(L_113);
			bool L_114 = FieldInfo_get_IsStatic_m24721619(L_113, /*hidden argument*/NULL);
			if (L_114)
			{
				goto IL_033c;
			}
		}

IL_0317:
		{
			FieldInfo_t * L_115 = V_12;
			NullCheck(L_115);
			bool L_116 = FieldInfo_get_IsPublic_m2574(L_115, /*hidden argument*/NULL);
			if (L_116)
			{
				goto IL_036c;
			}
		}

IL_0323:
		{
			FieldInfo_t * L_117 = V_12;
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_118 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(JsonMemberAttribute_t1310195012_0_0_0_var), /*hidden argument*/NULL);
			NullCheck(L_117);
			ObjectU5BU5D_t1108656482* L_119 = VirtFuncInvoker2< ObjectU5BU5D_t1108656482*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_117, L_118, (bool)1);
			NullCheck(L_119);
			if ((((int32_t)((int32_t)(((Il2CppArray *)L_119)->max_length)))))
			{
				goto IL_036c;
			}
		}

IL_033c:
		{
			JsonWriterSettings_t3394579648 * L_120 = JsonWriter_get_Settings_m3312657913(__this, /*hidden argument*/NULL);
			NullCheck(L_120);
			bool L_121 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DebugMode() */, L_120);
			if (!L_121)
			{
				goto IL_0367;
			}
		}

IL_034c:
		{
			FieldInfo_t * L_122 = V_12;
			NullCheck(L_122);
			String_t* L_123 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_122);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_124 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2660172893, L_123, _stringLiteral2428103372, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Console_t1363597357_il2cpp_TypeInfo_var);
			Console_WriteLine_m2829201975(NULL /*static, unused*/, L_124, /*hidden argument*/NULL);
		}

IL_0367:
		{
			goto IL_0435;
		}

IL_036c:
		{
			Type_t * L_125 = ___type1;
			FieldInfo_t * L_126 = V_12;
			Il2CppObject * L_127 = ___value0;
			bool L_128 = JsonWriter_IsIgnored_m1265037635(__this, L_125, L_126, L_127, /*hidden argument*/NULL);
			if (!L_128)
			{
				goto IL_03ab;
			}
		}

IL_037b:
		{
			JsonWriterSettings_t3394579648 * L_129 = JsonWriter_get_Settings_m3312657913(__this, /*hidden argument*/NULL);
			NullCheck(L_129);
			bool L_130 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DebugMode() */, L_129);
			if (!L_130)
			{
				goto IL_03a6;
			}
		}

IL_038b:
		{
			FieldInfo_t * L_131 = V_12;
			NullCheck(L_131);
			String_t* L_132 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_131);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_133 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2660172893, L_132, _stringLiteral509017720, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Console_t1363597357_il2cpp_TypeInfo_var);
			Console_WriteLine_m2829201975(NULL /*static, unused*/, L_133, /*hidden argument*/NULL);
		}

IL_03a6:
		{
			goto IL_0435;
		}

IL_03ab:
		{
			FieldInfo_t * L_134 = V_12;
			Il2CppObject * L_135 = ___value0;
			NullCheck(L_134);
			Il2CppObject * L_136 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(17 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_134, L_135);
			V_13 = L_136;
			FieldInfo_t * L_137 = V_12;
			Il2CppObject * L_138 = V_13;
			bool L_139 = JsonWriter_IsDefaultValue_m1681459346(__this, L_137, L_138, /*hidden argument*/NULL);
			if (!L_139)
			{
				goto IL_03f4;
			}
		}

IL_03c4:
		{
			JsonWriterSettings_t3394579648 * L_140 = JsonWriter_get_Settings_m3312657913(__this, /*hidden argument*/NULL);
			NullCheck(L_140);
			bool L_141 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DebugMode() */, L_140);
			if (!L_141)
			{
				goto IL_03ef;
			}
		}

IL_03d4:
		{
			FieldInfo_t * L_142 = V_12;
			NullCheck(L_142);
			String_t* L_143 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_142);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_144 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2660172893, L_143, _stringLiteral1422600610, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Console_t1363597357_il2cpp_TypeInfo_var);
			Console_WriteLine_m2829201975(NULL /*static, unused*/, L_144, /*hidden argument*/NULL);
		}

IL_03ef:
		{
			goto IL_0435;
		}

IL_03f4:
		{
			bool L_145 = V_0;
			if (!L_145)
			{
				goto IL_040b;
			}
		}

IL_03fa:
		{
			VirtActionInvoker0::Invoke(34 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectPropertyDelim() */, __this);
			VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
			goto IL_040d;
		}

IL_040b:
		{
			V_0 = (bool)1;
		}

IL_040d:
		{
			FieldInfo_t * L_146 = V_12;
			String_t* L_147 = JsonNameAttribute_GetJsonName_m166270748(NULL /*static, unused*/, L_146, /*hidden argument*/NULL);
			V_14 = L_147;
			String_t* L_148 = V_14;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_149 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_148, /*hidden argument*/NULL);
			if (!L_149)
			{
				goto IL_042b;
			}
		}

IL_0422:
		{
			FieldInfo_t * L_150 = V_12;
			NullCheck(L_150);
			String_t* L_151 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_150);
			V_14 = L_151;
		}

IL_042b:
		{
			String_t* L_152 = V_14;
			Il2CppObject * L_153 = V_13;
			JsonWriter_WriteObjectProperty_m92869745(__this, L_152, L_153, /*hidden argument*/NULL);
		}

IL_0435:
		{
			int32_t L_154 = V_11;
			V_11 = ((int32_t)((int32_t)L_154+(int32_t)1));
		}

IL_043b:
		{
			int32_t L_155 = V_11;
			FieldInfoU5BU5D_t2567562023* L_156 = V_10;
			NullCheck(L_156);
			if ((((int32_t)L_155) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_156)->max_length)))))))
			{
				goto IL_0304;
			}
		}

IL_0446:
		{
			IL2CPP_LEAVE(0x45A, FINALLY_044b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_044b;
	}

FINALLY_044b:
	{ // begin finally (depth: 1)
		int32_t L_157 = __this->get_depth_2();
		__this->set_depth_2(((int32_t)((int32_t)L_157-(int32_t)1)));
		IL2CPP_END_FINALLY(1099)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1099)
	{
		IL2CPP_JUMP_TBL(0x45A, IL_045a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_045a:
	{
		bool L_158 = V_0;
		if (!L_158)
		{
			goto IL_0466;
		}
	}
	{
		VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
	}

IL_0466:
	{
		TextWriter_t2304124208 * L_159 = __this->get_Writer_0();
		NullCheck(L_159);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_159, ((int32_t)125));
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteArrayItemDelim()
extern "C"  void JsonWriter_WriteArrayItemDelim_m2561128782 (JsonWriter_t541860733 * __this, const MethodInfo* method)
{
	{
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_0, ((int32_t)44));
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectPropertyDelim()
extern "C"  void JsonWriter_WriteObjectPropertyDelim_m4015810770 (JsonWriter_t541860733 * __this, const MethodInfo* method)
{
	{
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_0, ((int32_t)44));
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine()
extern "C"  void JsonWriter_WriteLine_m3952464063 (JsonWriter_t541860733 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		JsonWriterSettings_t3394579648 * L_0 = __this->get_settings_1();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_PrettyPrint() */, L_0);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		TextWriter_t2304124208 * L_2 = __this->get_Writer_0();
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(20 /* System.Void System.IO.TextWriter::WriteLine() */, L_2);
		V_0 = 0;
		goto IL_003d;
	}

IL_0023:
	{
		TextWriter_t2304124208 * L_3 = __this->get_Writer_0();
		JsonWriterSettings_t3394579648 * L_4 = __this->get_settings_1();
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_Tab() */, L_4);
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.String) */, L_3, L_5);
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_003d:
	{
		int32_t L_7 = V_0;
		int32_t L_8 = __this->get_depth_2();
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_0023;
		}
	}
	{
		return;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonWriter::IsIgnored(System.Type,System.Reflection.MemberInfo,System.Object)
extern const Il2CppType* JsonOptInAttribute_t3750829390_0_0_0_var;
extern const Il2CppType* JsonMemberAttribute_t1310195012_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral309353872;
extern const uint32_t JsonWriter_IsIgnored_m1265037635_MetadataUsageId;
extern "C"  bool JsonWriter_IsIgnored_m1265037635 (JsonWriter_t541860733 * __this, Type_t * ___objType0, MemberInfo_t * ___member1, Il2CppObject * ___obj2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_IsIgnored_m1265037635_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	PropertyInfo_t * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	PropertyInfo_t * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	{
		MemberInfo_t * L_0 = ___member1;
		bool L_1 = JsonIgnoreAttribute_IsJsonIgnore_m3417392871(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)1;
	}

IL_000d:
	{
		MemberInfo_t * L_2 = ___member1;
		String_t* L_3 = JsonSpecifiedPropertyAttribute_GetJsonSpecifiedProperty_m1760565138(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Type_t * L_6 = ___objType0;
		String_t* L_7 = V_0;
		NullCheck(L_6);
		PropertyInfo_t * L_8 = Type_GetProperty_m1904930970(L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		PropertyInfo_t * L_9 = V_1;
		if (!L_9)
		{
			goto IL_004e;
		}
	}
	{
		PropertyInfo_t * L_10 = V_1;
		Il2CppObject * L_11 = ___obj2;
		NullCheck(L_10);
		Il2CppObject * L_12 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t1108656482* >::Invoke(24 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_10, L_11, (ObjectU5BU5D_t1108656482*)(ObjectU5BU5D_t1108656482*)NULL);
		V_2 = L_12;
		Il2CppObject * L_13 = V_2;
		if (!((Il2CppObject *)IsInstSealed(L_13, Boolean_t476798718_il2cpp_TypeInfo_var)))
		{
			goto IL_004e;
		}
	}
	{
		Il2CppObject * L_14 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		bool L_15 = Convert_ToBoolean_m531114797(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_004e;
		}
	}
	{
		return (bool)1;
	}

IL_004e:
	{
		Type_t * L_16 = ___objType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(JsonOptInAttribute_t3750829390_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_16);
		ObjectU5BU5D_t1108656482* L_18 = VirtFuncInvoker2< ObjectU5BU5D_t1108656482*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_16, L_17, (bool)1);
		NullCheck(L_18);
		if (!(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))
		{
			goto IL_0080;
		}
	}
	{
		MemberInfo_t * L_19 = ___member1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(JsonMemberAttribute_t1310195012_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_19);
		ObjectU5BU5D_t1108656482* L_21 = VirtFuncInvoker2< ObjectU5BU5D_t1108656482*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_19, L_20, (bool)1);
		NullCheck(L_21);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))
		{
			goto IL_0080;
		}
	}
	{
		return (bool)1;
	}

IL_0080:
	{
		JsonWriterSettings_t3394579648 * L_22 = __this->get_settings_1();
		NullCheck(L_22);
		bool L_23 = VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_UseXmlSerializationAttributes() */, L_22);
		if (!L_23)
		{
			goto IL_00de;
		}
	}
	{
		MemberInfo_t * L_24 = ___member1;
		bool L_25 = JsonIgnoreAttribute_IsXmlIgnore_m2733425596(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_009d;
		}
	}
	{
		return (bool)1;
	}

IL_009d:
	{
		Type_t * L_26 = ___objType0;
		MemberInfo_t * L_27 = ___member1;
		NullCheck(L_27);
		String_t* L_28 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_27);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m138640077(NULL /*static, unused*/, L_28, _stringLiteral309353872, /*hidden argument*/NULL);
		NullCheck(L_26);
		PropertyInfo_t * L_30 = Type_GetProperty_m1904930970(L_26, L_29, /*hidden argument*/NULL);
		V_3 = L_30;
		PropertyInfo_t * L_31 = V_3;
		if (!L_31)
		{
			goto IL_00de;
		}
	}
	{
		PropertyInfo_t * L_32 = V_3;
		Il2CppObject * L_33 = ___obj2;
		NullCheck(L_32);
		Il2CppObject * L_34 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t1108656482* >::Invoke(24 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_32, L_33, (ObjectU5BU5D_t1108656482*)(ObjectU5BU5D_t1108656482*)NULL);
		V_4 = L_34;
		Il2CppObject * L_35 = V_4;
		if (!((Il2CppObject *)IsInstSealed(L_35, Boolean_t476798718_il2cpp_TypeInfo_var)))
		{
			goto IL_00de;
		}
	}
	{
		Il2CppObject * L_36 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		bool L_37 = Convert_ToBoolean_m531114797(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		if (L_37)
		{
			goto IL_00de;
		}
	}
	{
		return (bool)1;
	}

IL_00de:
	{
		return (bool)0;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonWriter::IsDefaultValue(System.Reflection.MemberInfo,System.Object)
extern const Il2CppType* DefaultValueAttribute_t3756983154_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DefaultValueAttribute_t3756983154_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter_IsDefaultValue_m1681459346_MetadataUsageId;
extern "C"  bool JsonWriter_IsDefaultValue_m1681459346 (JsonWriter_t541860733 * __this, MemberInfo_t * ___member0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_IsDefaultValue_m1681459346_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DefaultValueAttribute_t3756983154 * V_0 = NULL;
	{
		MemberInfo_t * L_0 = ___member0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(DefaultValueAttribute_t3756983154_0_0_0_var), /*hidden argument*/NULL);
		Attribute_t2523058482 * L_2 = Attribute_GetCustomAttribute_m506754809(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = ((DefaultValueAttribute_t3756983154 *)IsInstClass(L_2, DefaultValueAttribute_t3756983154_il2cpp_TypeInfo_var));
		DefaultValueAttribute_t3756983154 * L_3 = V_0;
		if (L_3)
		{
			goto IL_001e;
		}
	}
	{
		return (bool)0;
	}

IL_001e:
	{
		DefaultValueAttribute_t3756983154 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_5 = VirtFuncInvoker0< Il2CppObject * >::Invoke(6 /* System.Object System.ComponentModel.DefaultValueAttribute::get_Value() */, L_4);
		if (L_5)
		{
			goto IL_002e;
		}
	}
	{
		Il2CppObject * L_6 = ___value1;
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_6) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_002e:
	{
		DefaultValueAttribute_t3756983154 * L_7 = V_0;
		NullCheck(L_7);
		Il2CppObject * L_8 = VirtFuncInvoker0< Il2CppObject * >::Invoke(6 /* System.Object System.ComponentModel.DefaultValueAttribute::get_Value() */, L_7);
		Il2CppObject * L_9 = ___value1;
		NullCheck(L_8);
		bool L_10 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_8, L_9);
		return L_10;
	}
}
// System.Enum[] Pathfinding.Serialization.JsonFx.JsonWriter::GetFlagList(System.Type,System.Object)
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t4230874053_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4058313286_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2047055205_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m545522937_MethodInfo_var;
extern const uint32_t JsonWriter_GetFlagList_m4244203690_MetadataUsageId;
extern "C"  EnumU5BU5D_t3205174168* JsonWriter_GetFlagList_m4244203690 (Il2CppObject * __this /* static, unused */, Type_t * ___enumType0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_GetFlagList_m4244203690_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint64_t V_0 = 0;
	Il2CppArray * V_1 = NULL;
	List_1_t4230874053 * V_2 = NULL;
	int32_t V_3 = 0;
	uint64_t V_4 = 0;
	{
		Il2CppObject * L_0 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		uint64_t L_1 = Convert_ToUInt64_m2531670463(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Type_t * L_2 = ___enumType0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		Il2CppArray * L_3 = Enum_GetValues_m1513312286(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Il2CppArray * L_4 = V_1;
		NullCheck(L_4);
		int32_t L_5 = Array_get_Length_m1203127607(L_4, /*hidden argument*/NULL);
		List_1_t4230874053 * L_6 = (List_1_t4230874053 *)il2cpp_codegen_object_new(List_1_t4230874053_il2cpp_TypeInfo_var);
		List_1__ctor_m4058313286(L_6, L_5, /*hidden argument*/List_1__ctor_m4058313286_MethodInfo_var);
		V_2 = L_6;
		uint64_t L_7 = V_0;
		if ((!(((uint64_t)L_7) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_003b;
		}
	}
	{
		List_1_t4230874053 * L_8 = V_2;
		Il2CppObject * L_9 = ___value1;
		Type_t * L_10 = ___enumType0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		Il2CppObject * L_11 = Convert_ChangeType_m2922880930(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		List_1_Add_m2047055205(L_8, ((Enum_t2862688501 *)CastclassClass(L_11, Enum_t2862688501_il2cpp_TypeInfo_var)), /*hidden argument*/List_1_Add_m2047055205_MethodInfo_var);
		List_1_t4230874053 * L_12 = V_2;
		NullCheck(L_12);
		EnumU5BU5D_t3205174168* L_13 = List_1_ToArray_m545522937(L_12, /*hidden argument*/List_1_ToArray_m545522937_MethodInfo_var);
		return L_13;
	}

IL_003b:
	{
		Il2CppArray * L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = Array_get_Length_m1203127607(L_14, /*hidden argument*/NULL);
		V_3 = ((int32_t)((int32_t)L_15-(int32_t)1));
		goto IL_0091;
	}

IL_0049:
	{
		Il2CppArray * L_16 = V_1;
		int32_t L_17 = V_3;
		NullCheck(L_16);
		Il2CppObject * L_18 = Array_GetValue_m244209261(L_16, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		uint64_t L_19 = Convert_ToUInt64_m2531670463(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		V_4 = L_19;
		int32_t L_20 = V_3;
		if (L_20)
		{
			goto IL_006b;
		}
	}
	{
		uint64_t L_21 = V_4;
		if ((!(((uint64_t)L_21) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_006b;
		}
	}
	{
		goto IL_008d;
	}

IL_006b:
	{
		uint64_t L_22 = V_0;
		uint64_t L_23 = V_4;
		uint64_t L_24 = V_4;
		if ((!(((uint64_t)((int64_t)((int64_t)L_22&(int64_t)L_23))) == ((uint64_t)L_24))))
		{
			goto IL_008d;
		}
	}
	{
		uint64_t L_25 = V_0;
		uint64_t L_26 = V_4;
		V_0 = ((int64_t)((int64_t)L_25-(int64_t)L_26));
		List_1_t4230874053 * L_27 = V_2;
		Il2CppArray * L_28 = V_1;
		int32_t L_29 = V_3;
		NullCheck(L_28);
		Il2CppObject * L_30 = Array_GetValue_m244209261(L_28, L_29, /*hidden argument*/NULL);
		NullCheck(L_27);
		List_1_Add_m2047055205(L_27, ((Enum_t2862688501 *)IsInstClass(L_30, Enum_t2862688501_il2cpp_TypeInfo_var)), /*hidden argument*/List_1_Add_m2047055205_MethodInfo_var);
	}

IL_008d:
	{
		int32_t L_31 = V_3;
		V_3 = ((int32_t)((int32_t)L_31-(int32_t)1));
	}

IL_0091:
	{
		int32_t L_32 = V_3;
		if ((((int32_t)L_32) >= ((int32_t)0)))
		{
			goto IL_0049;
		}
	}
	{
		uint64_t L_33 = V_0;
		if ((((int64_t)L_33) == ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_00b2;
		}
	}
	{
		List_1_t4230874053 * L_34 = V_2;
		Type_t * L_35 = ___enumType0;
		uint64_t L_36 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		Il2CppObject * L_37 = Enum_ToObject_m1448258009(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		NullCheck(L_34);
		List_1_Add_m2047055205(L_34, ((Enum_t2862688501 *)IsInstClass(L_37, Enum_t2862688501_il2cpp_TypeInfo_var)), /*hidden argument*/List_1_Add_m2047055205_MethodInfo_var);
	}

IL_00b2:
	{
		List_1_t4230874053 * L_38 = V_2;
		NullCheck(L_38);
		EnumU5BU5D_t3205174168* L_39 = List_1_ToArray_m545522937(L_38, /*hidden argument*/List_1_ToArray_m545522937_MethodInfo_var);
		return L_39;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonWriter::InvalidIeee754(System.Decimal)
extern Il2CppClass* Decimal_t1954350631_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter_InvalidIeee754_m2029422535_MetadataUsageId;
extern "C"  bool JsonWriter_InvalidIeee754_m2029422535 (JsonWriter_t541860733 * __this, Decimal_t1954350631  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_InvalidIeee754_m2029422535_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Decimal_t1954350631  L_0 = ___value0;
			IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
			double L_1 = Decimal_op_Explicit_m2624557563(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			Decimal_t1954350631  L_2 = Decimal_op_Explicit_m1230483563(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
			Decimal_t1954350631  L_3 = ___value0;
			bool L_4 = Decimal_op_Inequality_m4269318701(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
			V_0 = L_4;
			goto IL_0029;
		}

IL_0017:
		{
			; // IL_0017: leave IL_0029
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001c;
		throw e;
	}

CATCH_001c:
	{ // begin catch(System.Object)
		{
			V_0 = (bool)1;
			goto IL_0029;
		}

IL_0024:
		{
			; // IL_0024: leave IL_0029
		}
	} // end catch (depth: 1)

IL_0029:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriterSettings::.ctor()
extern Il2CppClass* List_1_t182525330_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1836678241_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral9;
extern const uint32_t JsonWriterSettings__ctor_m946693515_MetadataUsageId;
extern "C"  void JsonWriterSettings__ctor_m946693515 (JsonWriterSettings_t3394579648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriterSettings__ctor_m946693515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_maxDepth_1(((int32_t)25));
		String_t* L_0 = Environment_get_NewLine_m1034655108(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_newLine_2(L_0);
		__this->set_tab_4(_stringLiteral9);
		List_1_t182525330 * L_1 = (List_1_t182525330 *)il2cpp_codegen_object_new(List_1_t182525330_il2cpp_TypeInfo_var);
		List_1__ctor_m1836678241(L_1, /*hidden argument*/List_1__ctor_m1836678241_MethodInfo_var);
		__this->set_converters_7(L_1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_HandleCyclicReferences()
extern "C"  bool JsonWriterSettings_get_HandleCyclicReferences_m1680156523 (JsonWriterSettings_t3394579648 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CHandleCyclicReferencesU3Ek__BackingField_8();
		return L_0;
	}
}
// System.String Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_TypeHintName()
extern "C"  String_t* JsonWriterSettings_get_TypeHintName_m5140843 (JsonWriterSettings_t3394579648 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_typeHintName_5();
		return L_0;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_PrettyPrint()
extern "C"  bool JsonWriterSettings_get_PrettyPrint_m817954679 (JsonWriterSettings_t3394579648 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_prettyPrint_3();
		return L_0;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriterSettings::set_PrettyPrint(System.Boolean)
extern "C"  void JsonWriterSettings_set_PrettyPrint_m3532634586 (JsonWriterSettings_t3394579648 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_prettyPrint_3(L_0);
		return;
	}
}
// System.String Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_Tab()
extern "C"  String_t* JsonWriterSettings_get_Tab_m629898552 (JsonWriterSettings_t3394579648 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_tab_4();
		return L_0;
	}
}
// System.String Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_NewLine()
extern "C"  String_t* JsonWriterSettings_get_NewLine_m177139351 (JsonWriterSettings_t3394579648 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_newLine_2();
		return L_0;
	}
}
// System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth()
extern "C"  int32_t JsonWriterSettings_get_MaxDepth_m1911450087 (JsonWriterSettings_t3394579648 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_maxDepth_1();
		return L_0;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_UseXmlSerializationAttributes()
extern "C"  bool JsonWriterSettings_get_UseXmlSerializationAttributes_m2162988055 (JsonWriterSettings_t3394579648 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_useXmlSerializationAttributes_6();
		return L_0;
	}
}
// Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime> Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DateTimeSerializer()
extern "C"  WriteDelegate_1_t4036178619 * JsonWriterSettings_get_DateTimeSerializer_m1011810803 (JsonWriterSettings_t3394579648 * __this, const MethodInfo* method)
{
	{
		WriteDelegate_1_t4036178619 * L_0 = __this->get_dateTimeSerializer_0();
		return L_0;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DebugMode()
extern "C"  bool JsonWriterSettings_get_DebugMode_m4262594102 (JsonWriterSettings_t3394579648 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CDebugModeU3Ek__BackingField_9();
		return L_0;
	}
}
// Pathfinding.Serialization.JsonFx.JsonConverter Pathfinding.Serialization.JsonFx.JsonWriterSettings::GetConverter(System.Type)
extern const MethodInfo* List_1_get_Item_m3135349974_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3861194015_MethodInfo_var;
extern const uint32_t JsonWriterSettings_GetConverter_m4109070163_MetadataUsageId;
extern "C"  JsonConverter_t3109307074 * JsonWriterSettings_GetConverter_m4109070163 (JsonWriterSettings_t3394579648 * __this, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriterSettings_GetConverter_m4109070163_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_002f;
	}

IL_0007:
	{
		List_1_t182525330 * L_0 = __this->get_converters_7();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		JsonConverter_t3109307074 * L_2 = List_1_get_Item_m3135349974(L_0, L_1, /*hidden argument*/List_1_get_Item_m3135349974_MethodInfo_var);
		Type_t * L_3 = ___type0;
		NullCheck(L_2);
		bool L_4 = VirtFuncInvoker1< bool, Type_t * >::Invoke(4 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonConverter::CanConvert(System.Type) */, L_2, L_3);
		if (!L_4)
		{
			goto IL_002b;
		}
	}
	{
		List_1_t182525330 * L_5 = __this->get_converters_7();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		JsonConverter_t3109307074 * L_7 = List_1_get_Item_m3135349974(L_5, L_6, /*hidden argument*/List_1_get_Item_m3135349974_MethodInfo_var);
		return L_7;
	}

IL_002b:
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002f:
	{
		int32_t L_9 = V_0;
		List_1_t182525330 * L_10 = __this->get_converters_7();
		NullCheck(L_10);
		int32_t L_11 = List_1_get_Count_m3861194015(L_10, /*hidden argument*/List_1_get_Count_m3861194015_MethodInfo_var);
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_0007;
		}
	}
	{
		return (JsonConverter_t3109307074 *)NULL;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriterSettings::AddTypeConverter(Pathfinding.Serialization.JsonFx.JsonConverter)
extern const MethodInfo* List_1_Add_m1531039057_MethodInfo_var;
extern const uint32_t JsonWriterSettings_AddTypeConverter_m2130440658_MetadataUsageId;
extern "C"  void JsonWriterSettings_AddTypeConverter_m2130440658 (JsonWriterSettings_t3394579648 * __this, JsonConverter_t3109307074 * ___converter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriterSettings_AddTypeConverter_m2130440658_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t182525330 * L_0 = __this->get_converters_7();
		JsonConverter_t3109307074 * L_1 = ___converter0;
		NullCheck(L_0);
		List_1_Add_m1531039057(L_0, L_1, /*hidden argument*/List_1_Add_m1531039057_MethodInfo_var);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.TypeCoercionUtility::.ctor()
extern "C"  void TypeCoercionUtility__ctor_m3724068137 (TypeCoercionUtility_t3154211006 * __this, const MethodInfo* method)
{
	{
		__this->set_allowNullValueTypes_1((bool)1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>> Pathfinding.Serialization.JsonFx.TypeCoercionUtility::get_MemberMapCache()
extern Il2CppClass* Dictionary_2_t626389457_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m726257740_MethodInfo_var;
extern const uint32_t TypeCoercionUtility_get_MemberMapCache_m994727592_MetadataUsageId;
extern "C"  Dictionary_2_t626389457 * TypeCoercionUtility_get_MemberMapCache_m994727592 (TypeCoercionUtility_t3154211006 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_get_MemberMapCache_m994727592_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t626389457 * L_0 = __this->get_memberMapCache_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Dictionary_2_t626389457 * L_1 = (Dictionary_2_t626389457 *)il2cpp_codegen_object_new(Dictionary_2_t626389457_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m726257740(L_1, /*hidden argument*/Dictionary_2__ctor_m726257740_MethodInfo_var);
		__this->set_memberMapCache_0(L_1);
	}

IL_0016:
	{
		Dictionary_2_t626389457 * L_2 = __this->get_memberMapCache_0();
		return L_2;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.TypeCoercionUtility::ProcessTypeHint(System.Collections.IDictionary,System.String,System.Type&,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>&)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TypeCoercionUtility_ProcessTypeHint_m421374191_MetadataUsageId;
extern "C"  Il2CppObject * TypeCoercionUtility_ProcessTypeHint_m421374191 (TypeCoercionUtility_t3154211006 * __this, Il2CppObject * ___result0, String_t* ___typeInfo1, Type_t ** ___objectType2, Dictionary_2_t520966972 ** ___memberMap3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_ProcessTypeHint_m421374191_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		String_t* L_0 = ___typeInfo1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		Type_t ** L_2 = ___objectType2;
		*((Il2CppObject **)(L_2)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_2), (Il2CppObject *)NULL);
		Dictionary_2_t520966972 ** L_3 = ___memberMap3;
		*((Il2CppObject **)(L_3)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_3), (Il2CppObject *)NULL);
		Il2CppObject * L_4 = ___result0;
		return L_4;
	}

IL_0014:
	{
		String_t* L_5 = ___typeInfo1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m3430407454, L_5, (bool)0, "Pathfinding.JsonFx, Version=1.4.1003.3008, Culture=neutral, PublicKeyToken=null");
		V_0 = L_6;
		Type_t * L_7 = V_0;
		if (L_7)
		{
			goto IL_002b;
		}
	}
	{
		Type_t ** L_8 = ___objectType2;
		*((Il2CppObject **)(L_8)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_8), (Il2CppObject *)NULL);
		Dictionary_2_t520966972 ** L_9 = ___memberMap3;
		*((Il2CppObject **)(L_9)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_9), (Il2CppObject *)NULL);
		Il2CppObject * L_10 = ___result0;
		return L_10;
	}

IL_002b:
	{
		Type_t ** L_11 = ___objectType2;
		Type_t * L_12 = V_0;
		*((Il2CppObject **)(L_11)) = (Il2CppObject *)L_12;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_11), (Il2CppObject *)L_12);
		Type_t * L_13 = V_0;
		Il2CppObject * L_14 = ___result0;
		Dictionary_2_t520966972 ** L_15 = ___memberMap3;
		Il2CppObject * L_16 = TypeCoercionUtility_CoerceType_m967108774(__this, L_13, L_14, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.TypeCoercionUtility::InstantiateObject(System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>&)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonTypeCoercionException_t3725515417_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TargetInvocationException_t3880899288_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1703456824;
extern Il2CppCodeGenString* _stringLiteral1496450899;
extern Il2CppCodeGenString* _stringLiteral4160581905;
extern const uint32_t TypeCoercionUtility_InstantiateObject_m3064453651_MetadataUsageId;
extern "C"  Il2CppObject * TypeCoercionUtility_InstantiateObject_m3064453651 (TypeCoercionUtility_t3154211006 * __this, Type_t * ___objectType0, Dictionary_2_t520966972 ** ___memberMap1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_InstantiateObject_m3064453651_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ConstructorInfo_t4136801618 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	TargetInvocationException_t3880899288 * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Type_t * L_0 = ___objectType0;
		NullCheck(L_0);
		bool L_1 = Type_get_IsInterface_m996103649(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		Type_t * L_2 = ___objectType0;
		NullCheck(L_2);
		bool L_3 = Type_get_IsAbstract_m2161724892(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0021;
		}
	}
	{
		Type_t * L_4 = ___objectType0;
		NullCheck(L_4);
		bool L_5 = Type_get_IsValueType_m1914757235(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0037;
		}
	}

IL_0021:
	{
		Type_t * L_6 = ___objectType0;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1703456824, L_7, /*hidden argument*/NULL);
		JsonTypeCoercionException_t3725515417 * L_9 = (JsonTypeCoercionException_t3725515417 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t3725515417_il2cpp_TypeInfo_var);
		JsonTypeCoercionException__ctor_m2129803924(L_9, L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0037:
	{
		Type_t * L_10 = ___objectType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		TypeU5BU5D_t3339007067* L_11 = ((Type_t_StaticFields*)Type_t_il2cpp_TypeInfo_var->static_fields)->get_EmptyTypes_3();
		NullCheck(L_10);
		ConstructorInfo_t4136801618 * L_12 = Type_GetConstructor_m2586438681(L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		ConstructorInfo_t4136801618 * L_13 = V_0;
		if (L_13)
		{
			goto IL_005f;
		}
	}
	{
		Type_t * L_14 = ___objectType0;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1496450899, L_15, /*hidden argument*/NULL);
		JsonTypeCoercionException_t3725515417 * L_17 = (JsonTypeCoercionException_t3725515417 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t3725515417_il2cpp_TypeInfo_var);
		JsonTypeCoercionException__ctor_m2129803924(L_17, L_16, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	}

IL_005f:
	try
	{ // begin try (depth: 1)
		ConstructorInfo_t4136801618 * L_18 = V_0;
		NullCheck(L_18);
		Il2CppObject * L_19 = ConstructorInfo_Invoke_m759007899(L_18, (ObjectU5BU5D_t1108656482*)(ObjectU5BU5D_t1108656482*)NULL, /*hidden argument*/NULL);
		V_1 = L_19;
		goto IL_00ab;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (TargetInvocationException_t3880899288_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_006c;
		throw e;
	}

CATCH_006c:
	{ // begin catch(System.Reflection.TargetInvocationException)
		{
			V_2 = ((TargetInvocationException_t3880899288 *)__exception_local);
			TargetInvocationException_t3880899288 * L_20 = V_2;
			NullCheck(L_20);
			Exception_t3991598821 * L_21 = Exception_get_InnerException_m1427945535(L_20, /*hidden argument*/NULL);
			if (!L_21)
			{
				goto IL_008f;
			}
		}

IL_0078:
		{
			TargetInvocationException_t3880899288 * L_22 = V_2;
			NullCheck(L_22);
			Exception_t3991598821 * L_23 = Exception_get_InnerException_m1427945535(L_22, /*hidden argument*/NULL);
			NullCheck(L_23);
			String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(21 /* System.String System.Exception::get_Message() */, L_23);
			TargetInvocationException_t3880899288 * L_25 = V_2;
			NullCheck(L_25);
			Exception_t3991598821 * L_26 = Exception_get_InnerException_m1427945535(L_25, /*hidden argument*/NULL);
			JsonTypeCoercionException_t3725515417 * L_27 = (JsonTypeCoercionException_t3725515417 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t3725515417_il2cpp_TypeInfo_var);
			JsonTypeCoercionException__ctor_m3348333858(L_27, L_24, L_26, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_27);
		}

IL_008f:
		{
			Type_t * L_28 = ___objectType0;
			NullCheck(L_28);
			String_t* L_29 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_28);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_30 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral4160581905, L_29, /*hidden argument*/NULL);
			TargetInvocationException_t3880899288 * L_31 = V_2;
			JsonTypeCoercionException_t3725515417 * L_32 = (JsonTypeCoercionException_t3725515417 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t3725515417_il2cpp_TypeInfo_var);
			JsonTypeCoercionException__ctor_m3348333858(L_32, L_30, L_31, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_32);
		}

IL_00a6:
		{
			goto IL_00ab;
		}
	} // end catch (depth: 1)

IL_00ab:
	{
		Dictionary_2_t520966972 ** L_33 = ___memberMap1;
		Type_t * L_34 = ___objectType0;
		Dictionary_2_t520966972 * L_35 = TypeCoercionUtility_GetMemberMap_m1277459664(__this, L_34, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_33)) = (Il2CppObject *)L_35;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_33), (Il2CppObject *)L_35);
		Il2CppObject * L_36 = V_1;
		return L_36;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo> Pathfinding.Serialization.JsonFx.TypeCoercionUtility::GetMemberMap(System.Type)
extern const Il2CppType* IDictionary_t537317817_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TypeCoercionUtility_GetMemberMap_m1277459664_MetadataUsageId;
extern "C"  Dictionary_2_t520966972 * TypeCoercionUtility_GetMemberMap_m1277459664 (TypeCoercionUtility_t3154211006 * __this, Type_t * ___objectType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_GetMemberMap_m1277459664_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(IDictionary_t537317817_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_1 = ___objectType0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		return (Dictionary_2_t520966972 *)NULL;
	}

IL_0017:
	{
		Type_t * L_3 = ___objectType0;
		Dictionary_2_t520966972 * L_4 = TypeCoercionUtility_CreateMemberMap_m856110382(__this, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo> Pathfinding.Serialization.JsonFx.TypeCoercionUtility::CreateMemberMap(System.Type)
extern const Il2CppType* JsonMemberAttribute_t1310195012_0_0_0_var;
extern Il2CppClass* Dictionary_2_t520966972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m509279431_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1401030532_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m2880590069_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m413716925_MethodInfo_var;
extern const uint32_t TypeCoercionUtility_CreateMemberMap_m856110382_MetadataUsageId;
extern "C"  Dictionary_2_t520966972 * TypeCoercionUtility_CreateMemberMap_m856110382 (TypeCoercionUtility_t3154211006 * __this, Type_t * ___objectType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_CreateMemberMap_m856110382_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t520966972 * V_0 = NULL;
	PropertyInfoU5BU5D_t4286713048* V_1 = NULL;
	PropertyInfoU5BU5D_t4286713048* V_2 = NULL;
	int32_t V_3 = 0;
	PropertyInfo_t * V_4 = NULL;
	String_t* V_5 = NULL;
	FieldInfoU5BU5D_t2567562023* V_6 = NULL;
	FieldInfoU5BU5D_t2567562023* V_7 = NULL;
	int32_t V_8 = 0;
	FieldInfo_t * V_9 = NULL;
	String_t* V_10 = NULL;
	{
		Dictionary_2_t626389457 * L_0 = TypeCoercionUtility_get_MemberMapCache_m994727592(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___objectType0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m509279431(L_0, L_1, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m509279431_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Dictionary_2_t520966972 * L_3 = V_0;
		return L_3;
	}

IL_0015:
	{
		Dictionary_2_t520966972 * L_4 = (Dictionary_2_t520966972 *)il2cpp_codegen_object_new(Dictionary_2_t520966972_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1401030532(L_4, /*hidden argument*/Dictionary_2__ctor_m1401030532_MethodInfo_var);
		V_0 = L_4;
		Type_t * L_5 = ___objectType0;
		NullCheck(L_5);
		PropertyInfoU5BU5D_t4286713048* L_6 = VirtFuncInvoker1< PropertyInfoU5BU5D_t4286713048*, int32_t >::Invoke(68 /* System.Reflection.PropertyInfo[] System.Type::GetProperties(System.Reflection.BindingFlags) */, L_5, ((int32_t)52));
		V_1 = L_6;
		PropertyInfoU5BU5D_t4286713048* L_7 = V_1;
		V_2 = L_7;
		V_3 = 0;
		goto IL_0097;
	}

IL_002d:
	{
		PropertyInfoU5BU5D_t4286713048* L_8 = V_2;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		PropertyInfo_t * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_4 = L_11;
		PropertyInfo_t * L_12 = V_4;
		NullCheck(L_12);
		bool L_13 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Reflection.PropertyInfo::get_CanRead() */, L_12);
		if (!L_13)
		{
			goto IL_004a;
		}
	}
	{
		PropertyInfo_t * L_14 = V_4;
		NullCheck(L_14);
		bool L_15 = VirtFuncInvoker0< bool >::Invoke(16 /* System.Boolean System.Reflection.PropertyInfo::get_CanWrite() */, L_14);
		if (L_15)
		{
			goto IL_004f;
		}
	}

IL_004a:
	{
		goto IL_0093;
	}

IL_004f:
	{
		PropertyInfo_t * L_16 = V_4;
		bool L_17 = JsonIgnoreAttribute_IsJsonIgnore_m3417392871(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0060;
		}
	}
	{
		goto IL_0093;
	}

IL_0060:
	{
		PropertyInfo_t * L_18 = V_4;
		String_t* L_19 = JsonNameAttribute_GetJsonName_m166270748(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		V_5 = L_19;
		String_t* L_20 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_21 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0089;
		}
	}
	{
		Dictionary_2_t520966972 * L_22 = V_0;
		PropertyInfo_t * L_23 = V_4;
		NullCheck(L_23);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_23);
		PropertyInfo_t * L_25 = V_4;
		NullCheck(L_22);
		Dictionary_2_set_Item_m2880590069(L_22, L_24, L_25, /*hidden argument*/Dictionary_2_set_Item_m2880590069_MethodInfo_var);
		goto IL_0093;
	}

IL_0089:
	{
		Dictionary_2_t520966972 * L_26 = V_0;
		String_t* L_27 = V_5;
		PropertyInfo_t * L_28 = V_4;
		NullCheck(L_26);
		Dictionary_2_set_Item_m2880590069(L_26, L_27, L_28, /*hidden argument*/Dictionary_2_set_Item_m2880590069_MethodInfo_var);
	}

IL_0093:
	{
		int32_t L_29 = V_3;
		V_3 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_0097:
	{
		int32_t L_30 = V_3;
		PropertyInfoU5BU5D_t4286713048* L_31 = V_2;
		NullCheck(L_31);
		if ((((int32_t)L_30) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_31)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		Type_t * L_32 = ___objectType0;
		NullCheck(L_32);
		FieldInfoU5BU5D_t2567562023* L_33 = VirtFuncInvoker1< FieldInfoU5BU5D_t2567562023*, int32_t >::Invoke(54 /* System.Reflection.FieldInfo[] System.Type::GetFields(System.Reflection.BindingFlags) */, L_32, ((int32_t)52));
		V_6 = L_33;
		FieldInfoU5BU5D_t2567562023* L_34 = V_6;
		V_7 = L_34;
		V_8 = 0;
		goto IL_0131;
	}

IL_00b6:
	{
		FieldInfoU5BU5D_t2567562023* L_35 = V_7;
		int32_t L_36 = V_8;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, L_36);
		int32_t L_37 = L_36;
		FieldInfo_t * L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		V_9 = L_38;
		FieldInfo_t * L_39 = V_9;
		NullCheck(L_39);
		bool L_40 = FieldInfo_get_IsPublic_m2574(L_39, /*hidden argument*/NULL);
		if (L_40)
		{
			goto IL_00e7;
		}
	}
	{
		FieldInfo_t * L_41 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(JsonMemberAttribute_t1310195012_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_41);
		ObjectU5BU5D_t1108656482* L_43 = VirtFuncInvoker2< ObjectU5BU5D_t1108656482*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_41, L_42, (bool)1);
		NullCheck(L_43);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_43)->max_length)))))
		{
			goto IL_00e7;
		}
	}
	{
		goto IL_012b;
	}

IL_00e7:
	{
		FieldInfo_t * L_44 = V_9;
		bool L_45 = JsonIgnoreAttribute_IsJsonIgnore_m3417392871(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_00f8;
		}
	}
	{
		goto IL_012b;
	}

IL_00f8:
	{
		FieldInfo_t * L_46 = V_9;
		String_t* L_47 = JsonNameAttribute_GetJsonName_m166270748(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		V_10 = L_47;
		String_t* L_48 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_49 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_0121;
		}
	}
	{
		Dictionary_2_t520966972 * L_50 = V_0;
		FieldInfo_t * L_51 = V_9;
		NullCheck(L_51);
		String_t* L_52 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_51);
		FieldInfo_t * L_53 = V_9;
		NullCheck(L_50);
		Dictionary_2_set_Item_m2880590069(L_50, L_52, L_53, /*hidden argument*/Dictionary_2_set_Item_m2880590069_MethodInfo_var);
		goto IL_012b;
	}

IL_0121:
	{
		Dictionary_2_t520966972 * L_54 = V_0;
		String_t* L_55 = V_10;
		FieldInfo_t * L_56 = V_9;
		NullCheck(L_54);
		Dictionary_2_set_Item_m2880590069(L_54, L_55, L_56, /*hidden argument*/Dictionary_2_set_Item_m2880590069_MethodInfo_var);
	}

IL_012b:
	{
		int32_t L_57 = V_8;
		V_8 = ((int32_t)((int32_t)L_57+(int32_t)1));
	}

IL_0131:
	{
		int32_t L_58 = V_8;
		FieldInfoU5BU5D_t2567562023* L_59 = V_7;
		NullCheck(L_59);
		if ((((int32_t)L_58) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_59)->max_length)))))))
		{
			goto IL_00b6;
		}
	}
	{
		Dictionary_2_t626389457 * L_60 = TypeCoercionUtility_get_MemberMapCache_m994727592(__this, /*hidden argument*/NULL);
		Type_t * L_61 = ___objectType0;
		Dictionary_2_t520966972 * L_62 = V_0;
		NullCheck(L_60);
		Dictionary_2_set_Item_m413716925(L_60, L_61, L_62, /*hidden argument*/Dictionary_2_set_Item_m413716925_MethodInfo_var);
		Dictionary_2_t520966972 * L_63 = V_0;
		return L_63;
	}
}
// System.Type Pathfinding.Serialization.JsonFx.TypeCoercionUtility::GetMemberInfo(System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>,System.String,System.Reflection.MemberInfo&)
extern Il2CppClass* PropertyInfo_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FieldInfo_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m961959871_MethodInfo_var;
extern const uint32_t TypeCoercionUtility_GetMemberInfo_m68305156_MetadataUsageId;
extern "C"  Type_t * TypeCoercionUtility_GetMemberInfo_m68305156 (Il2CppObject * __this /* static, unused */, Dictionary_2_t520966972 * ___memberMap0, String_t* ___memberName1, MemberInfo_t ** ___memberInfo2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_GetMemberInfo_m68305156_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t520966972 * L_0 = ___memberMap0;
		if (!L_0)
		{
			goto IL_0045;
		}
	}
	{
		Dictionary_2_t520966972 * L_1 = ___memberMap0;
		String_t* L_2 = ___memberName1;
		MemberInfo_t ** L_3 = ___memberInfo2;
		NullCheck(L_1);
		bool L_4 = Dictionary_2_TryGetValue_m961959871(L_1, L_2, L_3, /*hidden argument*/Dictionary_2_TryGetValue_m961959871_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0045;
		}
	}
	{
		MemberInfo_t ** L_5 = ___memberInfo2;
		if (!((PropertyInfo_t *)IsInstClass((*((MemberInfo_t **)L_5)), PropertyInfo_t_il2cpp_TypeInfo_var)))
		{
			goto IL_002c;
		}
	}
	{
		MemberInfo_t ** L_6 = ___memberInfo2;
		NullCheck(((PropertyInfo_t *)CastclassClass((*((MemberInfo_t **)L_6)), PropertyInfo_t_il2cpp_TypeInfo_var)));
		Type_t * L_7 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, ((PropertyInfo_t *)CastclassClass((*((MemberInfo_t **)L_6)), PropertyInfo_t_il2cpp_TypeInfo_var)));
		return L_7;
	}

IL_002c:
	{
		MemberInfo_t ** L_8 = ___memberInfo2;
		if (!((FieldInfo_t *)IsInstClass((*((MemberInfo_t **)L_8)), FieldInfo_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0045;
		}
	}
	{
		MemberInfo_t ** L_9 = ___memberInfo2;
		NullCheck(((FieldInfo_t *)CastclassClass((*((MemberInfo_t **)L_9)), FieldInfo_t_il2cpp_TypeInfo_var)));
		Type_t * L_10 = VirtFuncInvoker0< Type_t * >::Invoke(16 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, ((FieldInfo_t *)CastclassClass((*((MemberInfo_t **)L_9)), FieldInfo_t_il2cpp_TypeInfo_var)));
		return L_10;
	}

IL_0045:
	{
		MemberInfo_t ** L_11 = ___memberInfo2;
		*((Il2CppObject **)(L_11)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_11), (Il2CppObject *)NULL);
		return (Type_t *)NULL;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.TypeCoercionUtility::SetMemberValue(System.Object,System.Type,System.Reflection.MemberInfo,System.Object)
extern Il2CppClass* PropertyInfo_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FieldInfo_t_il2cpp_TypeInfo_var;
extern const uint32_t TypeCoercionUtility_SetMemberValue_m3423425905_MetadataUsageId;
extern "C"  void TypeCoercionUtility_SetMemberValue_m3423425905 (TypeCoercionUtility_t3154211006 * __this, Il2CppObject * ___result0, Type_t * ___memberType1, MemberInfo_t * ___memberInfo2, Il2CppObject * ___value3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_SetMemberValue_m3423425905_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MemberInfo_t * L_0 = ___memberInfo2;
		if (!((PropertyInfo_t *)IsInstClass(L_0, PropertyInfo_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0026;
		}
	}
	{
		MemberInfo_t * L_1 = ___memberInfo2;
		Il2CppObject * L_2 = ___result0;
		Type_t * L_3 = ___memberType1;
		Il2CppObject * L_4 = ___value3;
		Il2CppObject * L_5 = TypeCoercionUtility_CoerceType_m3345524962(__this, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(((PropertyInfo_t *)CastclassClass(L_1, PropertyInfo_t_il2cpp_TypeInfo_var)));
		VirtActionInvoker3< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t1108656482* >::Invoke(26 /* System.Void System.Reflection.PropertyInfo::SetValue(System.Object,System.Object,System.Object[]) */, ((PropertyInfo_t *)CastclassClass(L_1, PropertyInfo_t_il2cpp_TypeInfo_var)), L_2, L_5, (ObjectU5BU5D_t1108656482*)(ObjectU5BU5D_t1108656482*)NULL);
		goto IL_0046;
	}

IL_0026:
	{
		MemberInfo_t * L_6 = ___memberInfo2;
		if (!((FieldInfo_t *)IsInstClass(L_6, FieldInfo_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0046;
		}
	}
	{
		MemberInfo_t * L_7 = ___memberInfo2;
		Il2CppObject * L_8 = ___result0;
		Type_t * L_9 = ___memberType1;
		Il2CppObject * L_10 = ___value3;
		Il2CppObject * L_11 = TypeCoercionUtility_CoerceType_m3345524962(__this, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(((FieldInfo_t *)CastclassClass(L_7, FieldInfo_t_il2cpp_TypeInfo_var)));
		FieldInfo_SetValue_m1669444927(((FieldInfo_t *)CastclassClass(L_7, FieldInfo_t_il2cpp_TypeInfo_var)), L_8, L_11, /*hidden argument*/NULL);
	}

IL_0046:
	{
		return;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.TypeCoercionUtility::CoerceType(System.Type,System.Object)
extern const Il2CppType* IEnumerable_t3464557803_0_0_0_var;
extern const Il2CppType* DateTime_t4283661327_0_0_0_var;
extern const Il2CppType* Guid_t2862754429_0_0_0_var;
extern const Il2CppType* Char_t2862622538_0_0_0_var;
extern const Il2CppType* Uri_t1116831938_0_0_0_var;
extern const Il2CppType* Version_t763695022_0_0_0_var;
extern const Il2CppType* TimeSpan_t413522987_0_0_0_var;
extern const Il2CppType* Int64_t1153838595_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonTypeCoercionException_t3725515417_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTimeFormatInfo_t2490955586_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* Guid_t2862754429_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t1116831938_il2cpp_TypeInfo_var;
extern Il2CppClass* Version_t763695022_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeSpan_t413522987_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeDescriptor_t1537159061_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral358898177;
extern Il2CppCodeGenString* _stringLiteral2887095123;
extern const uint32_t TypeCoercionUtility_CoerceType_m3345524962_MetadataUsageId;
extern "C"  Il2CppObject * TypeCoercionUtility_CoerceType_m3345524962 (TypeCoercionUtility_t3154211006 * __this, Type_t * ___targetType0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_CoerceType_m3345524962_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	TypeU5BU5D_t3339007067* V_1 = NULL;
	Type_t * V_2 = NULL;
	FieldInfoU5BU5D_t2567562023* V_3 = NULL;
	int32_t V_4 = 0;
	FieldInfo_t * V_5 = NULL;
	String_t* V_6 = NULL;
	Dictionary_2_t520966972 * V_7 = NULL;
	DateTime_t4283661327  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Uri_t1116831938 * V_9 = NULL;
	TypeConverter_t1753450284 * V_10 = NULL;
	Il2CppObject * V_11 = NULL;
	Exception_t3991598821 * V_12 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Type_t * L_0 = ___targetType0;
		bool L_1 = TypeCoercionUtility_IsNullable_m187284083(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Il2CppObject * L_2 = ___value1;
		if (L_2)
		{
			goto IL_0041;
		}
	}
	{
		bool L_3 = __this->get_allowNullValueTypes_1();
		if (L_3)
		{
			goto IL_003f;
		}
	}
	{
		Type_t * L_4 = ___targetType0;
		NullCheck(L_4);
		bool L_5 = Type_get_IsValueType_m1914757235(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003f;
		}
	}
	{
		bool L_6 = V_0;
		if (L_6)
		{
			goto IL_003f;
		}
	}
	{
		Type_t * L_7 = ___targetType0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral358898177, L_8, /*hidden argument*/NULL);
		JsonTypeCoercionException_t3725515417 * L_10 = (JsonTypeCoercionException_t3725515417 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t3725515417_il2cpp_TypeInfo_var);
		JsonTypeCoercionException__ctor_m2129803924(L_10, L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_003f:
	{
		Il2CppObject * L_11 = ___value1;
		return L_11;
	}

IL_0041:
	{
		bool L_12 = V_0;
		if (!L_12)
		{
			goto IL_005c;
		}
	}
	{
		Type_t * L_13 = ___targetType0;
		NullCheck(L_13);
		TypeU5BU5D_t3339007067* L_14 = VirtFuncInvoker0< TypeU5BU5D_t3339007067* >::Invoke(92 /* System.Type[] System.Type::GetGenericArguments() */, L_13);
		V_1 = L_14;
		TypeU5BU5D_t3339007067* L_15 = V_1;
		NullCheck(L_15);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_005c;
		}
	}
	{
		TypeU5BU5D_t3339007067* L_16 = V_1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 0);
		int32_t L_17 = 0;
		Type_t * L_18 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		___targetType0 = L_18;
	}

IL_005c:
	{
		Il2CppObject * L_19 = ___value1;
		NullCheck(L_19);
		Type_t * L_20 = Object_GetType_m2022236990(L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		Type_t * L_21 = ___targetType0;
		Type_t * L_22 = V_2;
		NullCheck(L_21);
		bool L_23 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_21, L_22);
		if (!L_23)
		{
			goto IL_0071;
		}
	}
	{
		Il2CppObject * L_24 = ___value1;
		return L_24;
	}

IL_0071:
	{
		Type_t * L_25 = ___targetType0;
		NullCheck(L_25);
		bool L_26 = Type_get_IsEnum_m3878730619(L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_0105;
		}
	}
	{
		Il2CppObject * L_27 = ___value1;
		if (!((String_t*)IsInstSealed(L_27, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_00ee;
		}
	}
	{
		Type_t * L_28 = ___targetType0;
		Il2CppObject * L_29 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		bool L_30 = Enum_IsDefined_m2781598580(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		if (L_30)
		{
			goto IL_00e1;
		}
	}
	{
		Type_t * L_31 = ___targetType0;
		NullCheck(L_31);
		FieldInfoU5BU5D_t2567562023* L_32 = Type_GetFields_m3137302773(L_31, /*hidden argument*/NULL);
		V_3 = L_32;
		V_4 = 0;
		goto IL_00d7;
	}

IL_00a2:
	{
		FieldInfoU5BU5D_t2567562023* L_33 = V_3;
		int32_t L_34 = V_4;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_34);
		int32_t L_35 = L_34;
		FieldInfo_t * L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		V_5 = L_36;
		FieldInfo_t * L_37 = V_5;
		String_t* L_38 = JsonNameAttribute_GetJsonName_m166270748(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		V_6 = L_38;
		Il2CppObject * L_39 = ___value1;
		String_t* L_40 = V_6;
		NullCheck(((String_t*)CastclassSealed(L_39, String_t_il2cpp_TypeInfo_var)));
		bool L_41 = String_Equals_m3541721061(((String_t*)CastclassSealed(L_39, String_t_il2cpp_TypeInfo_var)), L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_00d1;
		}
	}
	{
		FieldInfo_t * L_42 = V_5;
		NullCheck(L_42);
		String_t* L_43 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_42);
		___value1 = L_43;
		goto IL_00e1;
	}

IL_00d1:
	{
		int32_t L_44 = V_4;
		V_4 = ((int32_t)((int32_t)L_44+(int32_t)1));
	}

IL_00d7:
	{
		int32_t L_45 = V_4;
		FieldInfoU5BU5D_t2567562023* L_46 = V_3;
		NullCheck(L_46);
		if ((((int32_t)L_45) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_46)->max_length)))))))
		{
			goto IL_00a2;
		}
	}

IL_00e1:
	{
		Type_t * L_47 = ___targetType0;
		Il2CppObject * L_48 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		Il2CppObject * L_49 = Enum_Parse_m2929309979(NULL /*static, unused*/, L_47, ((String_t*)CastclassSealed(L_48, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_49;
	}

IL_00ee:
	{
		Type_t * L_50 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		Type_t * L_51 = Enum_GetUnderlyingType_m2468052512(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Il2CppObject * L_52 = ___value1;
		Il2CppObject * L_53 = TypeCoercionUtility_CoerceType_m3345524962(__this, L_51, L_52, /*hidden argument*/NULL);
		___value1 = L_53;
		Type_t * L_54 = ___targetType0;
		Il2CppObject * L_55 = ___value1;
		Il2CppObject * L_56 = Enum_ToObject_m1129836274(NULL /*static, unused*/, L_54, L_55, /*hidden argument*/NULL);
		return L_56;
	}

IL_0105:
	{
		Il2CppObject * L_57 = ___value1;
		if (!((Il2CppObject *)IsInst(L_57, IDictionary_t537317817_il2cpp_TypeInfo_var)))
		{
			goto IL_0120;
		}
	}
	{
		Type_t * L_58 = ___targetType0;
		Il2CppObject * L_59 = ___value1;
		Il2CppObject * L_60 = TypeCoercionUtility_CoerceType_m967108774(__this, L_58, ((Il2CppObject *)Castclass(L_59, IDictionary_t537317817_il2cpp_TypeInfo_var)), (&V_7), /*hidden argument*/NULL);
		return L_60;
	}

IL_0120:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_61 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(IEnumerable_t3464557803_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_62 = ___targetType0;
		NullCheck(L_61);
		bool L_63 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_61, L_62);
		if (!L_63)
		{
			goto IL_0159;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_64 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(IEnumerable_t3464557803_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_65 = V_2;
		NullCheck(L_64);
		bool L_66 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_64, L_65);
		if (!L_66)
		{
			goto IL_0159;
		}
	}
	{
		Type_t * L_67 = ___targetType0;
		Type_t * L_68 = V_2;
		Il2CppObject * L_69 = ___value1;
		Il2CppObject * L_70 = TypeCoercionUtility_CoerceList_m3530535370(__this, L_67, L_68, ((Il2CppObject *)Castclass(L_69, IEnumerable_t3464557803_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_70;
	}

IL_0159:
	{
		Il2CppObject * L_71 = ___value1;
		if (!((String_t*)IsInstSealed(L_71, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0242;
		}
	}
	{
		Type_t * L_72 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_73 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(DateTime_t4283661327_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_72) == ((Il2CppObject*)(Type_t *)L_73))))
		{
			goto IL_019d;
		}
	}
	{
		Il2CppObject * L_74 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(DateTimeFormatInfo_t2490955586_il2cpp_TypeInfo_var);
		DateTimeFormatInfo_t2490955586 * L_75 = DateTimeFormatInfo_get_InvariantInfo_m1430381298(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		bool L_76 = DateTime_TryParse_m947353861(NULL /*static, unused*/, ((String_t*)CastclassSealed(L_74, String_t_il2cpp_TypeInfo_var)), L_75, ((int32_t)143), (&V_8), /*hidden argument*/NULL);
		if (!L_76)
		{
			goto IL_0198;
		}
	}
	{
		DateTime_t4283661327  L_77 = V_8;
		DateTime_t4283661327  L_78 = L_77;
		Il2CppObject * L_79 = Box(DateTime_t4283661327_il2cpp_TypeInfo_var, &L_78);
		return L_79;
	}

IL_0198:
	{
		goto IL_023d;
	}

IL_019d:
	{
		Type_t * L_80 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_81 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Guid_t2862754429_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_80) == ((Il2CppObject*)(Type_t *)L_81))))
		{
			goto IL_01be;
		}
	}
	{
		Il2CppObject * L_82 = ___value1;
		Guid_t2862754429  L_83;
		memset(&L_83, 0, sizeof(L_83));
		Guid__ctor_m1994687478(&L_83, ((String_t*)CastclassSealed(L_82, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Guid_t2862754429  L_84 = L_83;
		Il2CppObject * L_85 = Box(Guid_t2862754429_il2cpp_TypeInfo_var, &L_84);
		return L_85;
	}

IL_01be:
	{
		Type_t * L_86 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_87 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Char_t2862622538_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_86) == ((Il2CppObject*)(Type_t *)L_87))))
		{
			goto IL_01f6;
		}
	}
	{
		Il2CppObject * L_88 = ___value1;
		NullCheck(((String_t*)CastclassSealed(L_88, String_t_il2cpp_TypeInfo_var)));
		int32_t L_89 = String_get_Length_m2979997331(((String_t*)CastclassSealed(L_88, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_89) == ((uint32_t)1))))
		{
			goto IL_01f1;
		}
	}
	{
		Il2CppObject * L_90 = ___value1;
		NullCheck(((String_t*)CastclassSealed(L_90, String_t_il2cpp_TypeInfo_var)));
		Il2CppChar L_91 = String_get_Chars_m3015341861(((String_t*)CastclassSealed(L_90, String_t_il2cpp_TypeInfo_var)), 0, /*hidden argument*/NULL);
		Il2CppChar L_92 = L_91;
		Il2CppObject * L_93 = Box(Char_t2862622538_il2cpp_TypeInfo_var, &L_92);
		return L_93;
	}

IL_01f1:
	{
		goto IL_023d;
	}

IL_01f6:
	{
		Type_t * L_94 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_95 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Uri_t1116831938_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_94) == ((Il2CppObject*)(Type_t *)L_95))))
		{
			goto IL_0221;
		}
	}
	{
		Il2CppObject * L_96 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t1116831938_il2cpp_TypeInfo_var);
		bool L_97 = Uri_TryCreate_m1126538084(NULL /*static, unused*/, ((String_t*)CastclassSealed(L_96, String_t_il2cpp_TypeInfo_var)), 0, (&V_9), /*hidden argument*/NULL);
		if (!L_97)
		{
			goto IL_021c;
		}
	}
	{
		Uri_t1116831938 * L_98 = V_9;
		return L_98;
	}

IL_021c:
	{
		goto IL_023d;
	}

IL_0221:
	{
		Type_t * L_99 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_100 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Version_t763695022_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_99) == ((Il2CppObject*)(Type_t *)L_100))))
		{
			goto IL_023d;
		}
	}
	{
		Il2CppObject * L_101 = ___value1;
		Version_t763695022 * L_102 = (Version_t763695022 *)il2cpp_codegen_object_new(Version_t763695022_il2cpp_TypeInfo_var);
		Version__ctor_m48000169(L_102, ((String_t*)CastclassSealed(L_101, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_102;
	}

IL_023d:
	{
		goto IL_0273;
	}

IL_0242:
	{
		Type_t * L_103 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_104 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(TimeSpan_t413522987_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_103) == ((Il2CppObject*)(Type_t *)L_104))))
		{
			goto IL_0273;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_105 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int64_t1153838595_0_0_0_var), /*hidden argument*/NULL);
		Il2CppObject * L_106 = ___value1;
		Il2CppObject * L_107 = TypeCoercionUtility_CoerceType_m3345524962(__this, L_105, L_106, /*hidden argument*/NULL);
		TimeSpan_t413522987  L_108;
		memset(&L_108, 0, sizeof(L_108));
		TimeSpan__ctor_m477860848(&L_108, ((*(int64_t*)((int64_t*)UnBox (L_107, Int64_t1153838595_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		TimeSpan_t413522987  L_109 = L_108;
		Il2CppObject * L_110 = Box(TimeSpan_t413522987_il2cpp_TypeInfo_var, &L_109);
		return L_110;
	}

IL_0273:
	{
		Type_t * L_111 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(TypeDescriptor_t1537159061_il2cpp_TypeInfo_var);
		TypeConverter_t1753450284 * L_112 = TypeDescriptor_GetConverter_m3573588811(NULL /*static, unused*/, L_111, /*hidden argument*/NULL);
		V_10 = L_112;
		TypeConverter_t1753450284 * L_113 = V_10;
		Type_t * L_114 = V_2;
		NullCheck(L_113);
		bool L_115 = TypeConverter_CanConvertFrom_m3377278021(L_113, L_114, /*hidden argument*/NULL);
		if (!L_115)
		{
			goto IL_0291;
		}
	}
	{
		TypeConverter_t1753450284 * L_116 = V_10;
		Il2CppObject * L_117 = ___value1;
		NullCheck(L_116);
		Il2CppObject * L_118 = TypeConverter_ConvertFrom_m891071421(L_116, L_117, /*hidden argument*/NULL);
		return L_118;
	}

IL_0291:
	{
		Type_t * L_119 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(TypeDescriptor_t1537159061_il2cpp_TypeInfo_var);
		TypeConverter_t1753450284 * L_120 = TypeDescriptor_GetConverter_m3573588811(NULL /*static, unused*/, L_119, /*hidden argument*/NULL);
		V_10 = L_120;
		TypeConverter_t1753450284 * L_121 = V_10;
		Type_t * L_122 = ___targetType0;
		NullCheck(L_121);
		bool L_123 = TypeConverter_CanConvertTo_m3534396116(L_121, L_122, /*hidden argument*/NULL);
		if (!L_123)
		{
			goto IL_02b0;
		}
	}
	{
		TypeConverter_t1753450284 * L_124 = V_10;
		Il2CppObject * L_125 = ___value1;
		Type_t * L_126 = ___targetType0;
		NullCheck(L_124);
		Il2CppObject * L_127 = TypeConverter_ConvertTo_m3286262527(L_124, L_125, L_126, /*hidden argument*/NULL);
		return L_127;
	}

IL_02b0:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_128 = ___value1;
			Type_t * L_129 = ___targetType0;
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
			Il2CppObject * L_130 = Convert_ChangeType_m2922880930(NULL /*static, unused*/, L_128, L_129, /*hidden argument*/NULL);
			V_11 = L_130;
			goto IL_02ed;
		}

IL_02be:
		{
			; // IL_02be: leave IL_02ed
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_02c3;
		throw e;
	}

CATCH_02c3:
	{ // begin catch(System.Exception)
		{
			V_12 = ((Exception_t3991598821 *)__exception_local);
			Il2CppObject * L_131 = ___value1;
			NullCheck(L_131);
			Type_t * L_132 = Object_GetType_m2022236990(L_131, /*hidden argument*/NULL);
			NullCheck(L_132);
			String_t* L_133 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_132);
			Type_t * L_134 = ___targetType0;
			NullCheck(L_134);
			String_t* L_135 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_134);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_136 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral2887095123, L_133, L_135, /*hidden argument*/NULL);
			Exception_t3991598821 * L_137 = V_12;
			JsonTypeCoercionException_t3725515417 * L_138 = (JsonTypeCoercionException_t3725515417 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t3725515417_il2cpp_TypeInfo_var);
			JsonTypeCoercionException__ctor_m3348333858(L_138, L_136, L_137, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_138);
		}

IL_02e8:
		{
			goto IL_02ed;
		}
	} // end catch (depth: 1)

IL_02ed:
	{
		Il2CppObject * L_139 = V_11;
		return L_139;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.TypeCoercionUtility::CoerceType(System.Type,System.Collections.IDictionary,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>&)
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t TypeCoercionUtility_CoerceType_m967108774_MetadataUsageId;
extern "C"  Il2CppObject * TypeCoercionUtility_CoerceType_m967108774 (TypeCoercionUtility_t3154211006 * __this, Type_t * ___targetType0, Il2CppObject * ___value1, Dictionary_2_t520966972 ** ___memberMap2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_CoerceType_m967108774_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	MemberInfo_t * V_3 = NULL;
	Type_t * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Type_t * L_0 = ___targetType0;
		Dictionary_2_t520966972 ** L_1 = ___memberMap2;
		Il2CppObject * L_2 = TypeCoercionUtility_InstantiateObject_m3064453651(__this, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Dictionary_2_t520966972 ** L_3 = ___memberMap2;
		if (!(*((Dictionary_2_t520966972 **)L_3)))
		{
			goto IL_0070;
		}
	}
	{
		Il2CppObject * L_4 = ___value1;
		NullCheck(L_4);
		Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(4 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t537317817_il2cpp_TypeInfo_var, L_4);
		NullCheck(L_5);
		Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, L_5);
		V_1 = L_6;
	}

IL_001c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004a;
		}

IL_0021:
		{
			Il2CppObject * L_7 = V_1;
			NullCheck(L_7);
			Il2CppObject * L_8 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_7);
			V_2 = L_8;
			Dictionary_2_t520966972 ** L_9 = ___memberMap2;
			Il2CppObject * L_10 = V_2;
			Type_t * L_11 = TypeCoercionUtility_GetMemberInfo_m68305156(NULL /*static, unused*/, (*((Dictionary_2_t520966972 **)L_9)), ((String_t*)IsInstSealed(L_10, String_t_il2cpp_TypeInfo_var)), (&V_3), /*hidden argument*/NULL);
			V_4 = L_11;
			Il2CppObject * L_12 = V_0;
			Type_t * L_13 = V_4;
			MemberInfo_t * L_14 = V_3;
			Il2CppObject * L_15 = ___value1;
			Il2CppObject * L_16 = V_2;
			NullCheck(L_15);
			Il2CppObject * L_17 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, L_15, L_16);
			TypeCoercionUtility_SetMemberValue_m3423425905(__this, L_12, L_13, L_14, L_17, /*hidden argument*/NULL);
		}

IL_004a:
		{
			Il2CppObject * L_18 = V_1;
			NullCheck(L_18);
			bool L_19 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_18);
			if (L_19)
			{
				goto IL_0021;
			}
		}

IL_0055:
		{
			IL2CPP_LEAVE(0x70, FINALLY_005a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_005a;
	}

FINALLY_005a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_20 = V_1;
			Il2CppObject * L_21 = ((Il2CppObject *)IsInst(L_20, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			V_5 = L_21;
			if (!L_21)
			{
				goto IL_006f;
			}
		}

IL_0068:
		{
			Il2CppObject * L_22 = V_5;
			NullCheck(L_22);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_22);
		}

IL_006f:
		{
			IL2CPP_END_FINALLY(90)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(90)
	{
		IL2CPP_JUMP_TBL(0x70, IL_0070)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0070:
	{
		Il2CppObject * L_23 = V_0;
		return L_23;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.TypeCoercionUtility::CoerceList(System.Type,System.Type,System.Collections.IEnumerable)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonTypeCoercionException_t3725515417_il2cpp_TypeInfo_var;
extern Il2CppClass* TargetInvocationException_t3880899288_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1496450899;
extern Il2CppCodeGenString* _stringLiteral4160581905;
extern Il2CppCodeGenString* _stringLiteral3111219516;
extern Il2CppCodeGenString* _stringLiteral1488310865;
extern Il2CppCodeGenString* _stringLiteral65665;
extern Il2CppCodeGenString* _stringLiteral2032013422;
extern Il2CppCodeGenString* _stringLiteral2887095123;
extern const uint32_t TypeCoercionUtility_CoerceList_m3530535370_MetadataUsageId;
extern "C"  Il2CppObject * TypeCoercionUtility_CoerceList_m3530535370 (TypeCoercionUtility_t3154211006 * __this, Type_t * ___targetType0, Type_t * ___arrayType1, Il2CppObject * ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_CoerceList_m3530535370_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ConstructorInfoU5BU5D_t2079826215* V_0 = NULL;
	ConstructorInfo_t4136801618 * V_1 = NULL;
	ConstructorInfoU5BU5D_t2079826215* V_2 = NULL;
	int32_t V_3 = 0;
	ConstructorInfo_t4136801618 * V_4 = NULL;
	ParameterInfoU5BU5D_t2015293532* V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	Il2CppObject * V_7 = NULL;
	TargetInvocationException_t3880899288 * V_8 = NULL;
	MethodInfo_t * V_9 = NULL;
	ParameterInfoU5BU5D_t2015293532* V_10 = NULL;
	Type_t * V_11 = NULL;
	TargetInvocationException_t3880899288 * V_12 = NULL;
	Il2CppObject * V_13 = NULL;
	Il2CppObject * V_14 = NULL;
	TargetInvocationException_t3880899288 * V_15 = NULL;
	Il2CppObject * V_16 = NULL;
	Exception_t3991598821 * V_17 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	ParameterInfoU5BU5D_t2015293532* G_B23_0 = NULL;
	Type_t * G_B27_0 = NULL;
	ParameterInfoU5BU5D_t2015293532* G_B38_0 = NULL;
	Type_t * G_B42_0 = NULL;
	{
		Type_t * L_0 = ___targetType0;
		NullCheck(L_0);
		bool L_1 = Type_get_IsArray_m837983873(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Type_t * L_2 = ___targetType0;
		NullCheck(L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(46 /* System.Type System.Type::GetElementType() */, L_2);
		Il2CppObject * L_4 = ___value2;
		Il2CppArray * L_5 = TypeCoercionUtility_CoerceArray_m1490009066(__this, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0019:
	{
		Type_t * L_6 = ___targetType0;
		NullCheck(L_6);
		ConstructorInfoU5BU5D_t2079826215* L_7 = Type_GetConstructors_m3837181109(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		V_1 = (ConstructorInfo_t4136801618 *)NULL;
		ConstructorInfoU5BU5D_t2079826215* L_8 = V_0;
		V_2 = L_8;
		V_3 = 0;
		goto IL_0094;
	}

IL_002b:
	{
		ConstructorInfoU5BU5D_t2079826215* L_9 = V_2;
		int32_t L_10 = V_3;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		ConstructorInfo_t4136801618 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		V_4 = L_12;
		ConstructorInfo_t4136801618 * L_13 = V_4;
		NullCheck(L_13);
		ParameterInfoU5BU5D_t2015293532* L_14 = VirtFuncInvoker0< ParameterInfoU5BU5D_t2015293532* >::Invoke(15 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_13);
		V_5 = L_14;
		ParameterInfoU5BU5D_t2015293532* L_15 = V_5;
		NullCheck(L_15);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))
		{
			goto IL_004a;
		}
	}
	{
		ConstructorInfo_t4136801618 * L_16 = V_4;
		V_1 = L_16;
		goto IL_0090;
	}

IL_004a:
	{
		ParameterInfoU5BU5D_t2015293532* L_17 = V_5;
		NullCheck(L_17);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_0090;
		}
	}
	{
		ParameterInfoU5BU5D_t2015293532* L_18 = V_5;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 0);
		int32_t L_19 = 0;
		ParameterInfo_t2235474049 * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		NullCheck(L_20);
		Type_t * L_21 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_20);
		Type_t * L_22 = ___arrayType1;
		NullCheck(L_21);
		bool L_23 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_21, L_22);
		if (!L_23)
		{
			goto IL_0090;
		}
	}

IL_0068:
	try
	{ // begin try (depth: 1)
		{
			ConstructorInfo_t4136801618 * L_24 = V_4;
			ObjectU5BU5D_t1108656482* L_25 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
			Il2CppObject * L_26 = ___value2;
			NullCheck(L_25);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 0);
			ArrayElementTypeCheck (L_25, L_26);
			(L_25)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_26);
			NullCheck(L_24);
			Il2CppObject * L_27 = ConstructorInfo_Invoke_m759007899(L_24, L_25, /*hidden argument*/NULL);
			V_6 = L_27;
			goto IL_02f4;
		}

IL_0080:
		{
			; // IL_0080: leave IL_0090
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0085;
		throw e;
	}

CATCH_0085:
	{ // begin catch(System.Object)
		{
			goto IL_0090;
		}

IL_008b:
		{
			; // IL_008b: leave IL_0090
		}
	} // end catch (depth: 1)

IL_0090:
	{
		int32_t L_28 = V_3;
		V_3 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_0094:
	{
		int32_t L_29 = V_3;
		ConstructorInfoU5BU5D_t2079826215* L_30 = V_2;
		NullCheck(L_30);
		if ((((int32_t)L_29) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length)))))))
		{
			goto IL_002b;
		}
	}
	{
		ConstructorInfo_t4136801618 * L_31 = V_1;
		if (L_31)
		{
			goto IL_00b9;
		}
	}
	{
		Type_t * L_32 = ___targetType0;
		NullCheck(L_32);
		String_t* L_33 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_32);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1496450899, L_33, /*hidden argument*/NULL);
		JsonTypeCoercionException_t3725515417 * L_35 = (JsonTypeCoercionException_t3725515417 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t3725515417_il2cpp_TypeInfo_var);
		JsonTypeCoercionException__ctor_m2129803924(L_35, L_34, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_35);
	}

IL_00b9:
	try
	{ // begin try (depth: 1)
		ConstructorInfo_t4136801618 * L_36 = V_1;
		NullCheck(L_36);
		Il2CppObject * L_37 = ConstructorInfo_Invoke_m759007899(L_36, (ObjectU5BU5D_t1108656482*)(ObjectU5BU5D_t1108656482*)NULL, /*hidden argument*/NULL);
		V_7 = L_37;
		goto IL_010b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (TargetInvocationException_t3880899288_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00c7;
		throw e;
	}

CATCH_00c7:
	{ // begin catch(System.Reflection.TargetInvocationException)
		{
			V_8 = ((TargetInvocationException_t3880899288 *)__exception_local);
			TargetInvocationException_t3880899288 * L_38 = V_8;
			NullCheck(L_38);
			Exception_t3991598821 * L_39 = Exception_get_InnerException_m1427945535(L_38, /*hidden argument*/NULL);
			if (!L_39)
			{
				goto IL_00ee;
			}
		}

IL_00d5:
		{
			TargetInvocationException_t3880899288 * L_40 = V_8;
			NullCheck(L_40);
			Exception_t3991598821 * L_41 = Exception_get_InnerException_m1427945535(L_40, /*hidden argument*/NULL);
			NullCheck(L_41);
			String_t* L_42 = VirtFuncInvoker0< String_t* >::Invoke(21 /* System.String System.Exception::get_Message() */, L_41);
			TargetInvocationException_t3880899288 * L_43 = V_8;
			NullCheck(L_43);
			Exception_t3991598821 * L_44 = Exception_get_InnerException_m1427945535(L_43, /*hidden argument*/NULL);
			JsonTypeCoercionException_t3725515417 * L_45 = (JsonTypeCoercionException_t3725515417 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t3725515417_il2cpp_TypeInfo_var);
			JsonTypeCoercionException__ctor_m3348333858(L_45, L_42, L_44, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_45);
		}

IL_00ee:
		{
			Type_t * L_46 = ___targetType0;
			NullCheck(L_46);
			String_t* L_47 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_46);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_48 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral4160581905, L_47, /*hidden argument*/NULL);
			TargetInvocationException_t3880899288 * L_49 = V_8;
			JsonTypeCoercionException_t3725515417 * L_50 = (JsonTypeCoercionException_t3725515417 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t3725515417_il2cpp_TypeInfo_var);
			JsonTypeCoercionException__ctor_m3348333858(L_50, L_48, L_49, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_50);
		}

IL_0106:
		{
			goto IL_010b;
		}
	} // end catch (depth: 1)

IL_010b:
	{
		Type_t * L_51 = ___targetType0;
		NullCheck(L_51);
		MethodInfo_t * L_52 = Type_GetMethod_m2884801946(L_51, _stringLiteral3111219516, /*hidden argument*/NULL);
		V_9 = L_52;
		MethodInfo_t * L_53 = V_9;
		if (L_53)
		{
			goto IL_0125;
		}
	}
	{
		G_B23_0 = ((ParameterInfoU5BU5D_t2015293532*)(NULL));
		goto IL_012c;
	}

IL_0125:
	{
		MethodInfo_t * L_54 = V_9;
		NullCheck(L_54);
		ParameterInfoU5BU5D_t2015293532* L_55 = VirtFuncInvoker0< ParameterInfoU5BU5D_t2015293532* >::Invoke(15 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_54);
		G_B23_0 = L_55;
	}

IL_012c:
	{
		V_10 = G_B23_0;
		ParameterInfoU5BU5D_t2015293532* L_56 = V_10;
		if (!L_56)
		{
			goto IL_013f;
		}
	}
	{
		ParameterInfoU5BU5D_t2015293532* L_57 = V_10;
		NullCheck(L_57);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0145;
		}
	}

IL_013f:
	{
		G_B27_0 = ((Type_t *)(NULL));
		goto IL_014e;
	}

IL_0145:
	{
		ParameterInfoU5BU5D_t2015293532* L_58 = V_10;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, 0);
		int32_t L_59 = 0;
		ParameterInfo_t2235474049 * L_60 = (L_58)->GetAt(static_cast<il2cpp_array_size_t>(L_59));
		NullCheck(L_60);
		Type_t * L_61 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_60);
		G_B27_0 = L_61;
	}

IL_014e:
	{
		V_11 = G_B27_0;
		Type_t * L_62 = V_11;
		if (!L_62)
		{
			goto IL_01c4;
		}
	}
	{
		Type_t * L_63 = V_11;
		Type_t * L_64 = ___arrayType1;
		NullCheck(L_63);
		bool L_65 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_63, L_64);
		if (!L_65)
		{
			goto IL_01c4;
		}
	}

IL_0164:
	try
	{ // begin try (depth: 1)
		MethodInfo_t * L_66 = V_9;
		Il2CppObject * L_67 = V_7;
		ObjectU5BU5D_t1108656482* L_68 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_69 = ___value2;
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, 0);
		ArrayElementTypeCheck (L_68, L_69);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_69);
		NullCheck(L_66);
		MethodBase_Invoke_m3435166155(L_66, L_67, L_68, /*hidden argument*/NULL);
		goto IL_01c1;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (TargetInvocationException_t3880899288_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_017d;
		throw e;
	}

CATCH_017d:
	{ // begin catch(System.Reflection.TargetInvocationException)
		{
			V_12 = ((TargetInvocationException_t3880899288 *)__exception_local);
			TargetInvocationException_t3880899288 * L_70 = V_12;
			NullCheck(L_70);
			Exception_t3991598821 * L_71 = Exception_get_InnerException_m1427945535(L_70, /*hidden argument*/NULL);
			if (!L_71)
			{
				goto IL_01a4;
			}
		}

IL_018b:
		{
			TargetInvocationException_t3880899288 * L_72 = V_12;
			NullCheck(L_72);
			Exception_t3991598821 * L_73 = Exception_get_InnerException_m1427945535(L_72, /*hidden argument*/NULL);
			NullCheck(L_73);
			String_t* L_74 = VirtFuncInvoker0< String_t* >::Invoke(21 /* System.String System.Exception::get_Message() */, L_73);
			TargetInvocationException_t3880899288 * L_75 = V_12;
			NullCheck(L_75);
			Exception_t3991598821 * L_76 = Exception_get_InnerException_m1427945535(L_75, /*hidden argument*/NULL);
			JsonTypeCoercionException_t3725515417 * L_77 = (JsonTypeCoercionException_t3725515417 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t3725515417_il2cpp_TypeInfo_var);
			JsonTypeCoercionException__ctor_m3348333858(L_77, L_74, L_76, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_77);
		}

IL_01a4:
		{
			Type_t * L_78 = ___targetType0;
			NullCheck(L_78);
			String_t* L_79 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_78);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_80 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1488310865, L_79, /*hidden argument*/NULL);
			TargetInvocationException_t3880899288 * L_81 = V_12;
			JsonTypeCoercionException_t3725515417 * L_82 = (JsonTypeCoercionException_t3725515417 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t3725515417_il2cpp_TypeInfo_var);
			JsonTypeCoercionException__ctor_m3348333858(L_82, L_80, L_81, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_82);
		}

IL_01bc:
		{
			goto IL_01c1;
		}
	} // end catch (depth: 1)

IL_01c1:
	{
		Il2CppObject * L_83 = V_7;
		return L_83;
	}

IL_01c4:
	{
		Type_t * L_84 = ___targetType0;
		NullCheck(L_84);
		MethodInfo_t * L_85 = Type_GetMethod_m2884801946(L_84, _stringLiteral65665, /*hidden argument*/NULL);
		V_9 = L_85;
		MethodInfo_t * L_86 = V_9;
		if (L_86)
		{
			goto IL_01de;
		}
	}
	{
		G_B38_0 = ((ParameterInfoU5BU5D_t2015293532*)(NULL));
		goto IL_01e5;
	}

IL_01de:
	{
		MethodInfo_t * L_87 = V_9;
		NullCheck(L_87);
		ParameterInfoU5BU5D_t2015293532* L_88 = VirtFuncInvoker0< ParameterInfoU5BU5D_t2015293532* >::Invoke(15 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_87);
		G_B38_0 = L_88;
	}

IL_01e5:
	{
		V_10 = G_B38_0;
		ParameterInfoU5BU5D_t2015293532* L_89 = V_10;
		if (!L_89)
		{
			goto IL_01f8;
		}
	}
	{
		ParameterInfoU5BU5D_t2015293532* L_90 = V_10;
		NullCheck(L_90);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_90)->max_length))))) == ((int32_t)1)))
		{
			goto IL_01fe;
		}
	}

IL_01f8:
	{
		G_B42_0 = ((Type_t *)(NULL));
		goto IL_0207;
	}

IL_01fe:
	{
		ParameterInfoU5BU5D_t2015293532* L_91 = V_10;
		NullCheck(L_91);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_91, 0);
		int32_t L_92 = 0;
		ParameterInfo_t2235474049 * L_93 = (L_91)->GetAt(static_cast<il2cpp_array_size_t>(L_92));
		NullCheck(L_93);
		Type_t * L_94 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_93);
		G_B42_0 = L_94;
	}

IL_0207:
	{
		V_11 = G_B42_0;
		Type_t * L_95 = V_11;
		if (!L_95)
		{
			goto IL_02b7;
		}
	}
	{
		Il2CppObject * L_96 = ___value2;
		NullCheck(L_96);
		Il2CppObject * L_97 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, L_96);
		V_13 = L_97;
	}

IL_0218:
	try
	{ // begin try (depth: 1)
		{
			goto IL_028c;
		}

IL_021d:
		{
			Il2CppObject * L_98 = V_13;
			NullCheck(L_98);
			Il2CppObject * L_99 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_98);
			V_14 = L_99;
		}

IL_0226:
		try
		{ // begin try (depth: 2)
			MethodInfo_t * L_100 = V_9;
			Il2CppObject * L_101 = V_7;
			ObjectU5BU5D_t1108656482* L_102 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
			Type_t * L_103 = V_11;
			Il2CppObject * L_104 = V_14;
			Il2CppObject * L_105 = TypeCoercionUtility_CoerceType_m3345524962(__this, L_103, L_104, /*hidden argument*/NULL);
			NullCheck(L_102);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_102, 0);
			ArrayElementTypeCheck (L_102, L_105);
			(L_102)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_105);
			NullCheck(L_100);
			MethodBase_Invoke_m3435166155(L_100, L_101, L_102, /*hidden argument*/NULL);
			goto IL_028c;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t3991598821 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (TargetInvocationException_t3880899288_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0248;
			throw e;
		}

CATCH_0248:
		{ // begin catch(System.Reflection.TargetInvocationException)
			{
				V_15 = ((TargetInvocationException_t3880899288 *)__exception_local);
				TargetInvocationException_t3880899288 * L_106 = V_15;
				NullCheck(L_106);
				Exception_t3991598821 * L_107 = Exception_get_InnerException_m1427945535(L_106, /*hidden argument*/NULL);
				if (!L_107)
				{
					goto IL_026f;
				}
			}

IL_0256:
			{
				TargetInvocationException_t3880899288 * L_108 = V_15;
				NullCheck(L_108);
				Exception_t3991598821 * L_109 = Exception_get_InnerException_m1427945535(L_108, /*hidden argument*/NULL);
				NullCheck(L_109);
				String_t* L_110 = VirtFuncInvoker0< String_t* >::Invoke(21 /* System.String System.Exception::get_Message() */, L_109);
				TargetInvocationException_t3880899288 * L_111 = V_15;
				NullCheck(L_111);
				Exception_t3991598821 * L_112 = Exception_get_InnerException_m1427945535(L_111, /*hidden argument*/NULL);
				JsonTypeCoercionException_t3725515417 * L_113 = (JsonTypeCoercionException_t3725515417 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t3725515417_il2cpp_TypeInfo_var);
				JsonTypeCoercionException__ctor_m3348333858(L_113, L_110, L_112, /*hidden argument*/NULL);
				IL2CPP_RAISE_MANAGED_EXCEPTION(L_113);
			}

IL_026f:
			{
				Type_t * L_114 = ___targetType0;
				NullCheck(L_114);
				String_t* L_115 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_114);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_116 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2032013422, L_115, /*hidden argument*/NULL);
				TargetInvocationException_t3880899288 * L_117 = V_15;
				JsonTypeCoercionException_t3725515417 * L_118 = (JsonTypeCoercionException_t3725515417 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t3725515417_il2cpp_TypeInfo_var);
				JsonTypeCoercionException__ctor_m3348333858(L_118, L_116, L_117, /*hidden argument*/NULL);
				IL2CPP_RAISE_MANAGED_EXCEPTION(L_118);
			}

IL_0287:
			{
				goto IL_028c;
			}
		} // end catch (depth: 2)

IL_028c:
		{
			Il2CppObject * L_119 = V_13;
			NullCheck(L_119);
			bool L_120 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_119);
			if (L_120)
			{
				goto IL_021d;
			}
		}

IL_0298:
		{
			IL2CPP_LEAVE(0x2B4, FINALLY_029d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_029d;
	}

FINALLY_029d:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_121 = V_13;
			Il2CppObject * L_122 = ((Il2CppObject *)IsInst(L_121, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			V_16 = L_122;
			if (!L_122)
			{
				goto IL_02b3;
			}
		}

IL_02ac:
		{
			Il2CppObject * L_123 = V_16;
			NullCheck(L_123);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_123);
		}

IL_02b3:
		{
			IL2CPP_END_FINALLY(669)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(669)
	{
		IL2CPP_JUMP_TBL(0x2B4, IL_02b4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_02b4:
	{
		Il2CppObject * L_124 = V_7;
		return L_124;
	}

IL_02b7:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_125 = ___value2;
			Type_t * L_126 = ___targetType0;
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
			Il2CppObject * L_127 = Convert_ChangeType_m2922880930(NULL /*static, unused*/, L_125, L_126, /*hidden argument*/NULL);
			V_6 = L_127;
			goto IL_02f4;
		}

IL_02c5:
		{
			; // IL_02c5: leave IL_02f4
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_02ca;
		throw e;
	}

CATCH_02ca:
	{ // begin catch(System.Exception)
		{
			V_17 = ((Exception_t3991598821 *)__exception_local);
			Il2CppObject * L_128 = ___value2;
			NullCheck(L_128);
			Type_t * L_129 = Object_GetType_m2022236990(L_128, /*hidden argument*/NULL);
			NullCheck(L_129);
			String_t* L_130 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_129);
			Type_t * L_131 = ___targetType0;
			NullCheck(L_131);
			String_t* L_132 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_131);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_133 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral2887095123, L_130, L_132, /*hidden argument*/NULL);
			Exception_t3991598821 * L_134 = V_17;
			JsonTypeCoercionException_t3725515417 * L_135 = (JsonTypeCoercionException_t3725515417 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t3725515417_il2cpp_TypeInfo_var);
			JsonTypeCoercionException__ctor_m3348333858(L_135, L_133, L_134, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_135);
		}

IL_02ef:
		{
			goto IL_02f4;
		}
	} // end catch (depth: 1)

IL_02f4:
	{
		Il2CppObject * L_136 = V_6;
		return L_136;
	}
}
// System.Array Pathfinding.Serialization.JsonFx.TypeCoercionUtility::CoerceArray(System.Type,System.Collections.IEnumerable)
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t TypeCoercionUtility_CoerceArray_m1490009066_MetadataUsageId;
extern "C"  Il2CppArray * TypeCoercionUtility_CoerceArray_m1490009066 (TypeCoercionUtility_t3154211006 * __this, Type_t * ___elementType0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_CoerceArray_m1490009066_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Il2CppArray * V_4 = NULL;
	int32_t V_5 = 0;
	Il2CppObject * V_6 = NULL;
	Il2CppObject * V_7 = NULL;
	Il2CppObject * V_8 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = 0;
		Il2CppObject * L_0 = ___value1;
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, L_0);
		V_1 = L_1;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0019;
		}

IL_000e:
		{
			Il2CppObject * L_2 = V_1;
			NullCheck(L_2);
			Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_2);
			V_2 = L_3;
			int32_t L_4 = V_0;
			V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
		}

IL_0019:
		{
			Il2CppObject * L_5 = V_1;
			NullCheck(L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_5);
			if (L_6)
			{
				goto IL_000e;
			}
		}

IL_0024:
		{
			IL2CPP_LEAVE(0x3D, FINALLY_0029);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0029;
	}

FINALLY_0029:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_7 = V_1;
			Il2CppObject * L_8 = ((Il2CppObject *)IsInst(L_7, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			V_3 = L_8;
			if (!L_8)
			{
				goto IL_003c;
			}
		}

IL_0036:
		{
			Il2CppObject * L_9 = V_3;
			NullCheck(L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_9);
		}

IL_003c:
		{
			IL2CPP_END_FINALLY(41)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(41)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_003d:
	{
		Type_t * L_10 = ___elementType0;
		int32_t L_11 = V_0;
		Il2CppArray * L_12 = Array_CreateInstance_m1364223436(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_4 = L_12;
		V_5 = 0;
		Il2CppObject * L_13 = ___value1;
		NullCheck(L_13);
		Il2CppObject * L_14 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, L_13);
		V_6 = L_14;
	}

IL_0051:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0077;
		}

IL_0056:
		{
			Il2CppObject * L_15 = V_6;
			NullCheck(L_15);
			Il2CppObject * L_16 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_15);
			V_7 = L_16;
			Il2CppArray * L_17 = V_4;
			Type_t * L_18 = ___elementType0;
			Il2CppObject * L_19 = V_7;
			Il2CppObject * L_20 = TypeCoercionUtility_CoerceType_m3345524962(__this, L_18, L_19, /*hidden argument*/NULL);
			int32_t L_21 = V_5;
			NullCheck(L_17);
			Array_SetValue_m3564402974(L_17, L_20, L_21, /*hidden argument*/NULL);
			int32_t L_22 = V_5;
			V_5 = ((int32_t)((int32_t)L_22+(int32_t)1));
		}

IL_0077:
		{
			Il2CppObject * L_23 = V_6;
			NullCheck(L_23);
			bool L_24 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_23);
			if (L_24)
			{
				goto IL_0056;
			}
		}

IL_0083:
		{
			IL2CPP_LEAVE(0x9F, FINALLY_0088);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0088;
	}

FINALLY_0088:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_25 = V_6;
			Il2CppObject * L_26 = ((Il2CppObject *)IsInst(L_25, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			V_8 = L_26;
			if (!L_26)
			{
				goto IL_009e;
			}
		}

IL_0097:
		{
			Il2CppObject * L_27 = V_8;
			NullCheck(L_27);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_27);
		}

IL_009e:
		{
			IL2CPP_END_FINALLY(136)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(136)
	{
		IL2CPP_JUMP_TBL(0x9F, IL_009f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_009f:
	{
		Il2CppArray * L_28 = V_4;
		return L_28;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.TypeCoercionUtility::IsNullable(System.Type)
extern const Il2CppType* Nullable_1_t1122404262_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TypeCoercionUtility_IsNullable_m187284083_MetadataUsageId;
extern "C"  bool TypeCoercionUtility_IsNullable_m187284083 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_IsNullable_m187284083_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(96 /* System.Boolean System.Type::get_IsGenericType() */, L_0);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Nullable_1_t1122404262_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_3 = ___type0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(95 /* System.Type System.Type::GetGenericTypeDefinition() */, L_3);
		G_B3_0 = ((((Il2CppObject*)(Type_t *)L_2) == ((Il2CppObject*)(Type_t *)L_4))? 1 : 0);
		goto IL_0020;
	}

IL_001f:
	{
		G_B3_0 = 0;
	}

IL_0020:
	{
		return (bool)G_B3_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
