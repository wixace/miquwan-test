﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Experimental.Networking.UnityWebRequest
struct UnityWebRequest_t327863158;
// System.String
struct String_t;
// UnityEngine.Experimental.Networking.DownloadHandler
struct DownloadHandler_t2563345192;
// UnityEngine.Experimental.Networking.UploadHandler
struct UploadHandler_t3221259471;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3699081103;
struct UnityWebRequest_t327863158_marshaled_pinvoke;
struct UnityWebRequest_t327863158_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Experimental_Networking_Do2563345192.h"
#include "UnityEngine_UnityEngine_Experimental_Networking_Up3221259471.h"
#include "UnityEngine_UnityEngine_Experimental_Networking_Un1693154023.h"

// System.Void UnityEngine.Experimental.Networking.UnityWebRequest::.ctor(System.String,System.String,UnityEngine.Experimental.Networking.DownloadHandler,UnityEngine.Experimental.Networking.UploadHandler)
extern "C"  void UnityWebRequest__ctor_m1239841252 (UnityWebRequest_t327863158 * __this, String_t* ___url0, String_t* ___method1, DownloadHandler_t2563345192 * ___downloadHandler2, UploadHandler_t3221259471 * ___uploadHandler3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.UnityWebRequest::.cctor()
extern "C"  void UnityWebRequest__cctor_m3651458352 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Experimental.Networking.UnityWebRequest UnityEngine.Experimental.Networking.UnityWebRequest::Get(System.String)
extern "C"  UnityWebRequest_t327863158 * UnityWebRequest_Get_m822293443 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Networking.UnityWebRequest::get_disposeDownloadHandlerOnDispose()
extern "C"  bool UnityWebRequest_get_disposeDownloadHandlerOnDispose_m2541196373 (UnityWebRequest_t327863158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.UnityWebRequest::set_disposeDownloadHandlerOnDispose(System.Boolean)
extern "C"  void UnityWebRequest_set_disposeDownloadHandlerOnDispose_m541527346 (UnityWebRequest_t327863158 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Networking.UnityWebRequest::get_disposeUploadHandlerOnDispose()
extern "C"  bool UnityWebRequest_get_disposeUploadHandlerOnDispose_m3869547278 (UnityWebRequest_t327863158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.UnityWebRequest::set_disposeUploadHandlerOnDispose(System.Boolean)
extern "C"  void UnityWebRequest_set_disposeUploadHandlerOnDispose_m1730031019 (UnityWebRequest_t327863158 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.UnityWebRequest::InternalCreate()
extern "C"  void UnityWebRequest_InternalCreate_m3318922208 (UnityWebRequest_t327863158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.UnityWebRequest::InternalDestroy()
extern "C"  void UnityWebRequest_InternalDestroy_m1195820536 (UnityWebRequest_t327863158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.UnityWebRequest::InternalSetDefaults()
extern "C"  void UnityWebRequest_InternalSetDefaults_m1042407890 (UnityWebRequest_t327863158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.UnityWebRequest::Finalize()
extern "C"  void UnityWebRequest_Finalize_m3898660133 (UnityWebRequest_t327863158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.UnityWebRequest::Dispose()
extern "C"  void UnityWebRequest_Dispose_m3484422938 (UnityWebRequest_t327863158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AsyncOperation UnityEngine.Experimental.Networking.UnityWebRequest::InternalBegin()
extern "C"  AsyncOperation_t3699081103 * UnityWebRequest_InternalBegin_m3231504596 (UnityWebRequest_t327863158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AsyncOperation UnityEngine.Experimental.Networking.UnityWebRequest::Send()
extern "C"  AsyncOperation_t3699081103 * UnityWebRequest_Send_m3261576130 (UnityWebRequest_t327863158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.UnityWebRequest::InternalSetMethod(UnityEngine.Experimental.Networking.UnityWebRequest/UnityWebRequestMethod)
extern "C"  void UnityWebRequest_InternalSetMethod_m3342128362 (UnityWebRequest_t327863158 * __this, int32_t ___methodType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.UnityWebRequest::InternalSetCustomMethod(System.String)
extern "C"  void UnityWebRequest_InternalSetCustomMethod_m868188304 (UnityWebRequest_t327863158 * __this, String_t* ___customMethodName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.UnityWebRequest::set_method(System.String)
extern "C"  void UnityWebRequest_set_method_m2459450461 (UnityWebRequest_t327863158 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.UnityWebRequest::set_url(System.String)
extern "C"  void UnityWebRequest_set_url_m2691181493 (UnityWebRequest_t327863158 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.UnityWebRequest::InternalSetUrl(System.String)
extern "C"  void UnityWebRequest_InternalSetUrl_m2689407569 (UnityWebRequest_t327863158 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Networking.UnityWebRequest::get_isError()
extern "C"  bool UnityWebRequest_get_isError_m3269922582 (UnityWebRequest_t327863158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.UnityWebRequest::InternalSetRequestHeader(System.String,System.String)
extern "C"  void UnityWebRequest_InternalSetRequestHeader_m3663524000 (UnityWebRequest_t327863158 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.UnityWebRequest::SetRequestHeader(System.String,System.String)
extern "C"  void UnityWebRequest_SetRequestHeader_m1217204829 (UnityWebRequest_t327863158 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Experimental.Networking.UploadHandler UnityEngine.Experimental.Networking.UnityWebRequest::get_uploadHandler()
extern "C"  UploadHandler_t3221259471 * UnityWebRequest_get_uploadHandler_m1112692240 (UnityWebRequest_t327863158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.UnityWebRequest::set_uploadHandler(UnityEngine.Experimental.Networking.UploadHandler)
extern "C"  void UnityWebRequest_set_uploadHandler_m1095161229 (UnityWebRequest_t327863158 * __this, UploadHandler_t3221259471 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Experimental.Networking.DownloadHandler UnityEngine.Experimental.Networking.UnityWebRequest::get_downloadHandler()
extern "C"  DownloadHandler_t2563345192 * UnityWebRequest_get_downloadHandler_m3194482032 (UnityWebRequest_t327863158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.UnityWebRequest::set_downloadHandler(UnityEngine.Experimental.Networking.DownloadHandler)
extern "C"  void UnityWebRequest_set_downloadHandler_m382323483 (UnityWebRequest_t327863158 * __this, DownloadHandler_t2563345192 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Networking.UnityWebRequest::ContainsForbiddenCharacters(System.String,System.Int32)
extern "C"  bool UnityWebRequest_ContainsForbiddenCharacters_m1496380314 (Il2CppObject * __this /* static, unused */, String_t* ___s0, int32_t ___firstAllowedCharCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Networking.UnityWebRequest::IsHeaderNameLegal(System.String)
extern "C"  bool UnityWebRequest_IsHeaderNameLegal_m3244357194 (Il2CppObject * __this /* static, unused */, String_t* ___headerName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Networking.UnityWebRequest::IsHeaderValueLegal(System.String)
extern "C"  bool UnityWebRequest_IsHeaderValueLegal_m1833861730 (Il2CppObject * __this /* static, unused */, String_t* ___headerValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct UnityWebRequest_t327863158;
struct UnityWebRequest_t327863158_marshaled_pinvoke;

extern "C" void UnityWebRequest_t327863158_marshal_pinvoke(const UnityWebRequest_t327863158& unmarshaled, UnityWebRequest_t327863158_marshaled_pinvoke& marshaled);
extern "C" void UnityWebRequest_t327863158_marshal_pinvoke_back(const UnityWebRequest_t327863158_marshaled_pinvoke& marshaled, UnityWebRequest_t327863158& unmarshaled);
extern "C" void UnityWebRequest_t327863158_marshal_pinvoke_cleanup(UnityWebRequest_t327863158_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct UnityWebRequest_t327863158;
struct UnityWebRequest_t327863158_marshaled_com;

extern "C" void UnityWebRequest_t327863158_marshal_com(const UnityWebRequest_t327863158& unmarshaled, UnityWebRequest_t327863158_marshaled_com& marshaled);
extern "C" void UnityWebRequest_t327863158_marshal_com_back(const UnityWebRequest_t327863158_marshaled_com& marshaled, UnityWebRequest_t327863158& unmarshaled);
extern "C" void UnityWebRequest_t327863158_marshal_com_cleanup(UnityWebRequest_t327863158_marshaled_com& marshaled);
