﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Poly2Tri.AdvancingFront
struct AdvancingFront_t240556382;
// Pathfinding.Poly2Tri.AdvancingFrontNode
struct AdvancingFrontNode_t688967424;
// System.String
struct String_t;
// Pathfinding.Poly2Tri.TriangulationPoint
struct TriangulationPoint_t3810082933;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Advancing688967424.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3810082933.h"

// System.Void Pathfinding.Poly2Tri.AdvancingFront::.ctor(Pathfinding.Poly2Tri.AdvancingFrontNode,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void AdvancingFront__ctor_m2136454471 (AdvancingFront_t240556382 * __this, AdvancingFrontNode_t688967424 * ___head0, AdvancingFrontNode_t688967424 * ___tail1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.AdvancingFront::AddNode(Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void AdvancingFront_AddNode_m1666580389 (AdvancingFront_t240556382 * __this, AdvancingFrontNode_t688967424 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.AdvancingFront::RemoveNode(Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void AdvancingFront_RemoveNode_m1553367390 (AdvancingFront_t240556382 * __this, AdvancingFrontNode_t688967424 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.Poly2Tri.AdvancingFront::ToString()
extern "C"  String_t* AdvancingFront_ToString_m1353602860 (AdvancingFront_t240556382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.AdvancingFront::FindSearchNode(System.Double)
extern "C"  AdvancingFrontNode_t688967424 * AdvancingFront_FindSearchNode_m3770481444 (AdvancingFront_t240556382 * __this, double ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.AdvancingFront::LocateNode(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  AdvancingFrontNode_t688967424 * AdvancingFront_LocateNode_m3646659395 (AdvancingFront_t240556382 * __this, TriangulationPoint_t3810082933 * ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.AdvancingFront::LocateNode(System.Double)
extern "C"  AdvancingFrontNode_t688967424 * AdvancingFront_LocateNode_m4055557107 (AdvancingFront_t240556382 * __this, double ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.AdvancingFront::LocatePoint(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  AdvancingFrontNode_t688967424 * AdvancingFront_LocatePoint_m1012370253 (AdvancingFront_t240556382 * __this, TriangulationPoint_t3810082933 * ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
