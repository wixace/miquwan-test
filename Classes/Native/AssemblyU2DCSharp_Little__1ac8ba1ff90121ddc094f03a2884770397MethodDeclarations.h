﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._1ac8ba1ff90121ddc094f03a9e7fa3e0
struct _1ac8ba1ff90121ddc094f03a9e7fa3e0_t2884770397;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__1ac8ba1ff90121ddc094f03a2884770397.h"

// System.Void Little._1ac8ba1ff90121ddc094f03a9e7fa3e0::.ctor()
extern "C"  void _1ac8ba1ff90121ddc094f03a9e7fa3e0__ctor_m2486985840 (_1ac8ba1ff90121ddc094f03a9e7fa3e0_t2884770397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1ac8ba1ff90121ddc094f03a9e7fa3e0::_1ac8ba1ff90121ddc094f03a9e7fa3e0m2(System.Int32)
extern "C"  int32_t _1ac8ba1ff90121ddc094f03a9e7fa3e0__1ac8ba1ff90121ddc094f03a9e7fa3e0m2_m3370028153 (_1ac8ba1ff90121ddc094f03a9e7fa3e0_t2884770397 * __this, int32_t ____1ac8ba1ff90121ddc094f03a9e7fa3e0a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1ac8ba1ff90121ddc094f03a9e7fa3e0::_1ac8ba1ff90121ddc094f03a9e7fa3e0m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _1ac8ba1ff90121ddc094f03a9e7fa3e0__1ac8ba1ff90121ddc094f03a9e7fa3e0m_m1242306141 (_1ac8ba1ff90121ddc094f03a9e7fa3e0_t2884770397 * __this, int32_t ____1ac8ba1ff90121ddc094f03a9e7fa3e0a0, int32_t ____1ac8ba1ff90121ddc094f03a9e7fa3e091, int32_t ____1ac8ba1ff90121ddc094f03a9e7fa3e0c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1ac8ba1ff90121ddc094f03a9e7fa3e0::ilo__1ac8ba1ff90121ddc094f03a9e7fa3e0m21(Little._1ac8ba1ff90121ddc094f03a9e7fa3e0,System.Int32)
extern "C"  int32_t _1ac8ba1ff90121ddc094f03a9e7fa3e0_ilo__1ac8ba1ff90121ddc094f03a9e7fa3e0m21_m853254852 (Il2CppObject * __this /* static, unused */, _1ac8ba1ff90121ddc094f03a9e7fa3e0_t2884770397 * ____this0, int32_t ____1ac8ba1ff90121ddc094f03a9e7fa3e0a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
