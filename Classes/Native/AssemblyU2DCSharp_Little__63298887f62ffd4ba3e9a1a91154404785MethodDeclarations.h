﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._63298887f62ffd4ba3e9a1a910ad45b6
struct _63298887f62ffd4ba3e9a1a910ad45b6_t154404785;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__63298887f62ffd4ba3e9a1a91154404785.h"

// System.Void Little._63298887f62ffd4ba3e9a1a910ad45b6::.ctor()
extern "C"  void _63298887f62ffd4ba3e9a1a910ad45b6__ctor_m3052287388 (_63298887f62ffd4ba3e9a1a910ad45b6_t154404785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._63298887f62ffd4ba3e9a1a910ad45b6::_63298887f62ffd4ba3e9a1a910ad45b6m2(System.Int32)
extern "C"  int32_t _63298887f62ffd4ba3e9a1a910ad45b6__63298887f62ffd4ba3e9a1a910ad45b6m2_m2239687673 (_63298887f62ffd4ba3e9a1a910ad45b6_t154404785 * __this, int32_t ____63298887f62ffd4ba3e9a1a910ad45b6a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._63298887f62ffd4ba3e9a1a910ad45b6::_63298887f62ffd4ba3e9a1a910ad45b6m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _63298887f62ffd4ba3e9a1a910ad45b6__63298887f62ffd4ba3e9a1a910ad45b6m_m2481858269 (_63298887f62ffd4ba3e9a1a910ad45b6_t154404785 * __this, int32_t ____63298887f62ffd4ba3e9a1a910ad45b6a0, int32_t ____63298887f62ffd4ba3e9a1a910ad45b6861, int32_t ____63298887f62ffd4ba3e9a1a910ad45b6c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._63298887f62ffd4ba3e9a1a910ad45b6::ilo__63298887f62ffd4ba3e9a1a910ad45b6m21(Little._63298887f62ffd4ba3e9a1a910ad45b6,System.Int32)
extern "C"  int32_t _63298887f62ffd4ba3e9a1a910ad45b6_ilo__63298887f62ffd4ba3e9a1a910ad45b6m21_m130889368 (Il2CppObject * __this /* static, unused */, _63298887f62ffd4ba3e9a1a910ad45b6_t154404785 * ____this0, int32_t ____63298887f62ffd4ba3e9a1a910ad45b6a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
