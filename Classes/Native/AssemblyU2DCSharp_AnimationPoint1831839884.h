﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationPoint
struct  AnimationPoint_t1831839884  : public MonoBehaviour_t667441552
{
public:
	// AnimationRunner/AniType AnimationPoint::AniType
	int32_t ___AniType_2;
	// System.Single AnimationPoint::AniTime
	float ___AniTime_3;
	// UnityEngine.Vector3 AnimationPoint::Pos
	Vector3_t4282066566  ___Pos_4;
	// UnityEngine.Vector3 AnimationPoint::Rot
	Vector3_t4282066566  ___Rot_5;
	// System.Single AnimationPoint::Delay
	float ___Delay_6;
	// System.Single AnimationPoint::Time
	float ___Time_7;
	// System.String AnimationPoint::Fun
	String_t* ___Fun_8;
	// UnityEngine.Vector3 AnimationPoint::VibrationV3
	Vector3_t4282066566  ___VibrationV3_9;
	// System.Single AnimationPoint::VibrationTime
	float ___VibrationTime_10;
	// System.Single AnimationPoint::VibrationDelay
	float ___VibrationDelay_11;
	// UnityEngine.GameObject AnimationPoint::targe
	GameObject_t3674682005 * ___targe_12;

public:
	inline static int32_t get_offset_of_AniType_2() { return static_cast<int32_t>(offsetof(AnimationPoint_t1831839884, ___AniType_2)); }
	inline int32_t get_AniType_2() const { return ___AniType_2; }
	inline int32_t* get_address_of_AniType_2() { return &___AniType_2; }
	inline void set_AniType_2(int32_t value)
	{
		___AniType_2 = value;
	}

	inline static int32_t get_offset_of_AniTime_3() { return static_cast<int32_t>(offsetof(AnimationPoint_t1831839884, ___AniTime_3)); }
	inline float get_AniTime_3() const { return ___AniTime_3; }
	inline float* get_address_of_AniTime_3() { return &___AniTime_3; }
	inline void set_AniTime_3(float value)
	{
		___AniTime_3 = value;
	}

	inline static int32_t get_offset_of_Pos_4() { return static_cast<int32_t>(offsetof(AnimationPoint_t1831839884, ___Pos_4)); }
	inline Vector3_t4282066566  get_Pos_4() const { return ___Pos_4; }
	inline Vector3_t4282066566 * get_address_of_Pos_4() { return &___Pos_4; }
	inline void set_Pos_4(Vector3_t4282066566  value)
	{
		___Pos_4 = value;
	}

	inline static int32_t get_offset_of_Rot_5() { return static_cast<int32_t>(offsetof(AnimationPoint_t1831839884, ___Rot_5)); }
	inline Vector3_t4282066566  get_Rot_5() const { return ___Rot_5; }
	inline Vector3_t4282066566 * get_address_of_Rot_5() { return &___Rot_5; }
	inline void set_Rot_5(Vector3_t4282066566  value)
	{
		___Rot_5 = value;
	}

	inline static int32_t get_offset_of_Delay_6() { return static_cast<int32_t>(offsetof(AnimationPoint_t1831839884, ___Delay_6)); }
	inline float get_Delay_6() const { return ___Delay_6; }
	inline float* get_address_of_Delay_6() { return &___Delay_6; }
	inline void set_Delay_6(float value)
	{
		___Delay_6 = value;
	}

	inline static int32_t get_offset_of_Time_7() { return static_cast<int32_t>(offsetof(AnimationPoint_t1831839884, ___Time_7)); }
	inline float get_Time_7() const { return ___Time_7; }
	inline float* get_address_of_Time_7() { return &___Time_7; }
	inline void set_Time_7(float value)
	{
		___Time_7 = value;
	}

	inline static int32_t get_offset_of_Fun_8() { return static_cast<int32_t>(offsetof(AnimationPoint_t1831839884, ___Fun_8)); }
	inline String_t* get_Fun_8() const { return ___Fun_8; }
	inline String_t** get_address_of_Fun_8() { return &___Fun_8; }
	inline void set_Fun_8(String_t* value)
	{
		___Fun_8 = value;
		Il2CppCodeGenWriteBarrier(&___Fun_8, value);
	}

	inline static int32_t get_offset_of_VibrationV3_9() { return static_cast<int32_t>(offsetof(AnimationPoint_t1831839884, ___VibrationV3_9)); }
	inline Vector3_t4282066566  get_VibrationV3_9() const { return ___VibrationV3_9; }
	inline Vector3_t4282066566 * get_address_of_VibrationV3_9() { return &___VibrationV3_9; }
	inline void set_VibrationV3_9(Vector3_t4282066566  value)
	{
		___VibrationV3_9 = value;
	}

	inline static int32_t get_offset_of_VibrationTime_10() { return static_cast<int32_t>(offsetof(AnimationPoint_t1831839884, ___VibrationTime_10)); }
	inline float get_VibrationTime_10() const { return ___VibrationTime_10; }
	inline float* get_address_of_VibrationTime_10() { return &___VibrationTime_10; }
	inline void set_VibrationTime_10(float value)
	{
		___VibrationTime_10 = value;
	}

	inline static int32_t get_offset_of_VibrationDelay_11() { return static_cast<int32_t>(offsetof(AnimationPoint_t1831839884, ___VibrationDelay_11)); }
	inline float get_VibrationDelay_11() const { return ___VibrationDelay_11; }
	inline float* get_address_of_VibrationDelay_11() { return &___VibrationDelay_11; }
	inline void set_VibrationDelay_11(float value)
	{
		___VibrationDelay_11 = value;
	}

	inline static int32_t get_offset_of_targe_12() { return static_cast<int32_t>(offsetof(AnimationPoint_t1831839884, ___targe_12)); }
	inline GameObject_t3674682005 * get_targe_12() const { return ___targe_12; }
	inline GameObject_t3674682005 ** get_address_of_targe_12() { return &___targe_12; }
	inline void set_targe_12(GameObject_t3674682005 * value)
	{
		___targe_12 = value;
		Il2CppCodeGenWriteBarrier(&___targe_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
