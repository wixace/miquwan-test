﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JPath
struct JPath_t1935141997;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1244034627;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Newtonsoft.Json.Linq.JContainer
struct JContainer_t3364442311;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JPath1935141997.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JContainer3364442311.h"

// System.Void Newtonsoft.Json.Linq.JPath::.ctor(System.String)
extern "C"  void JPath__ctor_m1958037821 (JPath_t1935141997 * __this, String_t* ___expression0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> Newtonsoft.Json.Linq.JPath::get_Parts()
extern "C"  List_1_t1244034627 * JPath_get_Parts_m288212603 (JPath_t1935141997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JPath::set_Parts(System.Collections.Generic.List`1<System.Object>)
extern "C"  void JPath_set_Parts_m1797060072 (JPath_t1935141997 * __this, List_1_t1244034627 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JPath::ParseMain()
extern "C"  void JPath_ParseMain_m611855 (JPath_t1935141997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JPath::ParseIndexer(System.Char)
extern "C"  void JPath_ParseIndexer_m3780213106 (JPath_t1935141997 * __this, Il2CppChar ___indexerOpenChar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JPath::Evaluate(Newtonsoft.Json.Linq.JToken,System.Boolean)
extern "C"  JToken_t3412245951 * JPath_Evaluate_m1768163318 (JPath_t1935141997 * __this, JToken_t3412245951 * ___root0, bool ___errorWhenNoMatch1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> Newtonsoft.Json.Linq.JPath::ilo_get_Parts1(Newtonsoft.Json.Linq.JPath)
extern "C"  List_1_t1244034627 * JPath_ilo_get_Parts1_m330494645 (Il2CppObject * __this /* static, unused */, JPath_t1935141997 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JPath::ilo_get_Item2(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * JPath_ilo_get_Item2_m4207940551 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JPath::ilo_FormatWith3(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* JPath_ilo_FormatWith3_m2800724865 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JPath::ilo_get_Count4(Newtonsoft.Json.Linq.JContainer)
extern "C"  int32_t JPath_ilo_get_Count4_m2155463508 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
