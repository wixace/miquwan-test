﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// skill_upgradeCfg
struct skill_upgradeCfg_t790726486;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void skill_upgradeCfg::.ctor()
extern "C"  void skill_upgradeCfg__ctor_m4188504069 (skill_upgradeCfg_t790726486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skill_upgradeCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void skill_upgradeCfg_Init_m3310048608 (skill_upgradeCfg_t790726486 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
