﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuglyInit
struct BuglyInit_t3142012817;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void BuglyInit::.ctor()
extern "C"  void BuglyInit__ctor_m134984902 (BuglyInit_t3142012817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuglyInit::Awake()
extern "C"  void BuglyInit_Awake_m372590121 (BuglyInit_t3142012817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> BuglyInit::MyLogCallbackExtrasHandler()
extern "C"  Dictionary_2_t827649927 * BuglyInit_MyLogCallbackExtrasHandler_m1398771442 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
