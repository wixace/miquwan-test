﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginYiwanSuper
struct PluginYiwanSuper_t2354859450;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.String
struct String_t;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionInfo
struct VersionInfo_t2356638086;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginYiwanSuper2354859450.h"

// System.Void PluginYiwanSuper::.ctor()
extern "C"  void PluginYiwanSuper__ctor_m400271905 (PluginYiwanSuper_t2354859450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::Init()
extern "C"  void PluginYiwanSuper_Init_m1211374611 (PluginYiwanSuper_t2354859450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginYiwanSuper::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginYiwanSuper_ReqSDKHttpLogin_m1700033590 (PluginYiwanSuper_t2354859450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYiwanSuper::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginYiwanSuper_IsLoginSuccess_m615482898 (PluginYiwanSuper_t2354859450 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::OpenUserLogin()
extern "C"  void PluginYiwanSuper_OpenUserLogin_m3575454579 (PluginYiwanSuper_t2354859450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::OnSDKInitSuccess(System.String)
extern "C"  void PluginYiwanSuper_OnSDKInitSuccess_m3428357351 (PluginYiwanSuper_t2354859450 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::OnSDKInitFail(System.String)
extern "C"  void PluginYiwanSuper_OnSDKInitFail_m3070516666 (PluginYiwanSuper_t2354859450 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::OnLoginSuccess(System.String)
extern "C"  void PluginYiwanSuper_OnLoginSuccess_m509796966 (PluginYiwanSuper_t2354859450 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::OnLoginFail(System.String)
extern "C"  void PluginYiwanSuper_OnLoginFail_m2840467611 (PluginYiwanSuper_t2354859450 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::OnLoginCancel(System.String)
extern "C"  void PluginYiwanSuper_OnLoginCancel_m234031039 (PluginYiwanSuper_t2354859450 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::OnLogoutSuccess(System.String)
extern "C"  void PluginYiwanSuper_OnLogoutSuccess_m2922346729 (PluginYiwanSuper_t2354859450 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::OnSDKLogout(System.String)
extern "C"  void PluginYiwanSuper_OnSDKLogout_m4062719678 (PluginYiwanSuper_t2354859450 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::OnGameSwitchSuccess(System.String)
extern "C"  void PluginYiwanSuper_OnGameSwitchSuccess_m3372111781 (PluginYiwanSuper_t2354859450 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::OnUserSwitchSuccess(System.String)
extern "C"  void PluginYiwanSuper_OnUserSwitchSuccess_m3349751806 (PluginYiwanSuper_t2354859450 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::UserPay(CEvent.ZEvent)
extern "C"  void PluginYiwanSuper_UserPay_m3750314143 (PluginYiwanSuper_t2354859450 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::OnZFSuccess(System.String)
extern "C"  void PluginYiwanSuper_OnZFSuccess_m1617653099 (PluginYiwanSuper_t2354859450 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::OnZFFail(System.String)
extern "C"  void PluginYiwanSuper_OnZFFail_m4181717942 (PluginYiwanSuper_t2354859450 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::OnEnterGame(CEvent.ZEvent)
extern "C"  void PluginYiwanSuper_OnEnterGame_m894128881 (PluginYiwanSuper_t2354859450 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::OnCreateRole(CEvent.ZEvent)
extern "C"  void PluginYiwanSuper_OnCreateRole_m4089857383 (PluginYiwanSuper_t2354859450 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::OnLevelUp(CEvent.ZEvent)
extern "C"  void PluginYiwanSuper_OnLevelUp_m3315535932 (PluginYiwanSuper_t2354859450 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::ClollectData(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYiwanSuper_ClollectData_m2321723661 (PluginYiwanSuper_t2354859450 * __this, String_t* ___type0, String_t* ___serverid1, String_t* ___servername2, String_t* ___roleid3, String_t* ___rolename4, String_t* ___rolelevel5, String_t* ___extend6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::initSdk(System.String,System.String,System.String,System.String)
extern "C"  void PluginYiwanSuper_initSdk_m1203512749 (PluginYiwanSuper_t2354859450 * __this, String_t* ___AppID0, String_t* ___SignKey1, String_t* ___PacketID2, String_t* ___DebugMode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::login()
extern "C"  void PluginYiwanSuper_login_m4217254024 (PluginYiwanSuper_t2354859450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::goZF(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYiwanSuper_goZF_m1951641427 (PluginYiwanSuper_t2354859450 * __this, String_t* ___amount0, String_t* ___serverId1, String_t* ___orderId2, String_t* ___ProductName3, String_t* ___ProductTotal4, String_t* ___strCustomInfo5, String_t* ___extstr6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::coloectData(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYiwanSuper_coloectData_m1823644474 (PluginYiwanSuper_t2354859450 * __this, String_t* ___dataType0, String_t* ___serverId1, String_t* ___RoleName2, String_t* ___Extend3, String_t* ___RoleLevel4, String_t* ___ServerName5, String_t* ___RoleID6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::<OnLogoutSuccess>m__472()
extern "C"  void PluginYiwanSuper_U3COnLogoutSuccessU3Em__472_m1595702689 (PluginYiwanSuper_t2354859450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginYiwanSuper_ilo_AddEventListener1_m3408692915 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginYiwanSuper::ilo_get_currentVS2(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginYiwanSuper_ilo_get_currentVS2_m3213315048 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginYiwanSuper::ilo_get_Instance3()
extern "C"  VersionMgr_t1322950208 * PluginYiwanSuper_ilo_get_Instance3_m2189591524 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginYiwanSuper::ilo_get_PluginsSdkMgr4()
extern "C"  PluginsSdkMgr_t3884624670 * PluginYiwanSuper_ilo_get_PluginsSdkMgr4_m410750314 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::ilo_goZF5(PluginYiwanSuper,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYiwanSuper_ilo_goZF5_m2212541591 (Il2CppObject * __this /* static, unused */, PluginYiwanSuper_t2354859450 * ____this0, String_t* ___amount1, String_t* ___serverId2, String_t* ___orderId3, String_t* ___ProductName4, String_t* ___ProductTotal5, String_t* ___strCustomInfo6, String_t* ___extstr7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiwanSuper::ilo_ClollectData6(PluginYiwanSuper,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYiwanSuper_ilo_ClollectData6_m3409717330 (Il2CppObject * __this /* static, unused */, PluginYiwanSuper_t2354859450 * ____this0, String_t* ___type1, String_t* ___serverid2, String_t* ___servername3, String_t* ___roleid4, String_t* ___rolename5, String_t* ___rolelevel6, String_t* ___extend7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
