﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_siscewisKassarqel79
struct M_siscewisKassarqel79_t189522833;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_siscewisKassarqel79::.ctor()
extern "C"  void M_siscewisKassarqel79__ctor_m3335222002 (M_siscewisKassarqel79_t189522833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_siscewisKassarqel79::M_cooparnu0(System.String[],System.Int32)
extern "C"  void M_siscewisKassarqel79_M_cooparnu0_m772302656 (M_siscewisKassarqel79_t189522833 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_siscewisKassarqel79::M_gearsine1(System.String[],System.Int32)
extern "C"  void M_siscewisKassarqel79_M_gearsine1_m19359370 (M_siscewisKassarqel79_t189522833 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
