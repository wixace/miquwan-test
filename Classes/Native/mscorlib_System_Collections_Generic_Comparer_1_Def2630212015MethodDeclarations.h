﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/MeshInstance>
struct DefaultComparer_t2630212015;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_MeshSubsetCombineUtility_M4121966238.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::.ctor()
extern "C"  void DefaultComparer__ctor_m177322626_gshared (DefaultComparer_t2630212015 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m177322626(__this, method) ((  void (*) (DefaultComparer_t2630212015 *, const MethodInfo*))DefaultComparer__ctor_m177322626_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m554587373_gshared (DefaultComparer_t2630212015 * __this, MeshInstance_t4121966238  ___x0, MeshInstance_t4121966238  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m554587373(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t2630212015 *, MeshInstance_t4121966238 , MeshInstance_t4121966238 , const MethodInfo*))DefaultComparer_Compare_m554587373_gshared)(__this, ___x0, ___y1, method)
