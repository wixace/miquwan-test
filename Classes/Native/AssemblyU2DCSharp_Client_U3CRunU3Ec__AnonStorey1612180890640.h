﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Diagnostics.Stopwatch
struct Stopwatch_t3420517611;
// Client
struct Client_t2021122027;
// System.Action
struct Action_t3771233898;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Client/<Run>c__AnonStorey161
struct  U3CRunU3Ec__AnonStorey161_t2180890640  : public Il2CppObject
{
public:
	// System.Diagnostics.Stopwatch Client/<Run>c__AnonStorey161::stopwatch
	Stopwatch_t3420517611 * ___stopwatch_0;
	// Client Client/<Run>c__AnonStorey161::<>f__this
	Client_t2021122027 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_stopwatch_0() { return static_cast<int32_t>(offsetof(U3CRunU3Ec__AnonStorey161_t2180890640, ___stopwatch_0)); }
	inline Stopwatch_t3420517611 * get_stopwatch_0() const { return ___stopwatch_0; }
	inline Stopwatch_t3420517611 ** get_address_of_stopwatch_0() { return &___stopwatch_0; }
	inline void set_stopwatch_0(Stopwatch_t3420517611 * value)
	{
		___stopwatch_0 = value;
		Il2CppCodeGenWriteBarrier(&___stopwatch_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CRunU3Ec__AnonStorey161_t2180890640, ___U3CU3Ef__this_1)); }
	inline Client_t2021122027 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline Client_t2021122027 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(Client_t2021122027 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

struct U3CRunU3Ec__AnonStorey161_t2180890640_StaticFields
{
public:
	// System.Action Client/<Run>c__AnonStorey161::<>f__am$cache2
	Action_t3771233898 * ___U3CU3Ef__amU24cache2_2;
	// System.Action Client/<Run>c__AnonStorey161::<>f__am$cache3
	Action_t3771233898 * ___U3CU3Ef__amU24cache3_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(U3CRunU3Ec__AnonStorey161_t2180890640_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(U3CRunU3Ec__AnonStorey161_t2180890640_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
