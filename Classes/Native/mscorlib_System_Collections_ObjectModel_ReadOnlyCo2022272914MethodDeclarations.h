﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>
struct ReadOnlyCollection_1_t2022272914;
// System.Collections.Generic.IList`1<Pathfinding.AdvancedSmooth/Turn>
struct IList_1_t3159842581;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.AdvancedSmooth/Turn[]
struct TurnU5BU5D_t2705444999;
// System.Collections.Generic.IEnumerator`1<Pathfinding.AdvancedSmooth/Turn>
struct IEnumerator_1_t2377060427;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_AdvancedSmooth_Turn465195378.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3344836798_gshared (ReadOnlyCollection_1_t2022272914 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3344836798(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2022272914 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3344836798_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m476302248_gshared (ReadOnlyCollection_1_t2022272914 * __this, Turn_t465195378  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m476302248(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2022272914 *, Turn_t465195378 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m476302248_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3887158818_gshared (ReadOnlyCollection_1_t2022272914 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3887158818(__this, method) ((  void (*) (ReadOnlyCollection_1_t2022272914 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3887158818_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3992181903_gshared (ReadOnlyCollection_1_t2022272914 * __this, int32_t ___index0, Turn_t465195378  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3992181903(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2022272914 *, int32_t, Turn_t465195378 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3992181903_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2683302347_gshared (ReadOnlyCollection_1_t2022272914 * __this, Turn_t465195378  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2683302347(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2022272914 *, Turn_t465195378 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2683302347_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1866034773_gshared (ReadOnlyCollection_1_t2022272914 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1866034773(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2022272914 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1866034773_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  Turn_t465195378  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2047611097_gshared (ReadOnlyCollection_1_t2022272914 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2047611097(__this, ___index0, method) ((  Turn_t465195378  (*) (ReadOnlyCollection_1_t2022272914 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2047611097_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2890610086_gshared (ReadOnlyCollection_1_t2022272914 * __this, int32_t ___index0, Turn_t465195378  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2890610086(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2022272914 *, int32_t, Turn_t465195378 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2890610086_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m754270564_gshared (ReadOnlyCollection_1_t2022272914 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m754270564(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2022272914 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m754270564_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3363975277_gshared (ReadOnlyCollection_1_t2022272914 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3363975277(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2022272914 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3363975277_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3226379176_gshared (ReadOnlyCollection_1_t2022272914 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3226379176(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2022272914 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3226379176_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m951685449_gshared (ReadOnlyCollection_1_t2022272914 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m951685449(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2022272914 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m951685449_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2980328315_gshared (ReadOnlyCollection_1_t2022272914 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2980328315(__this, method) ((  void (*) (ReadOnlyCollection_1_t2022272914 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2980328315_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1199186211_gshared (ReadOnlyCollection_1_t2022272914 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1199186211(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2022272914 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1199186211_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3805149985_gshared (ReadOnlyCollection_1_t2022272914 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3805149985(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2022272914 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3805149985_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1013547980_gshared (ReadOnlyCollection_1_t2022272914 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1013547980(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2022272914 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1013547980_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m851700188_gshared (ReadOnlyCollection_1_t2022272914 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m851700188(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2022272914 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m851700188_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2218257756_gshared (ReadOnlyCollection_1_t2022272914 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2218257756(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2022272914 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2218257756_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3708387161_gshared (ReadOnlyCollection_1_t2022272914 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3708387161(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2022272914 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3708387161_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3410483589_gshared (ReadOnlyCollection_1_t2022272914 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3410483589(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2022272914 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3410483589_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1869489874_gshared (ReadOnlyCollection_1_t2022272914 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1869489874(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2022272914 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1869489874_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1345598951_gshared (ReadOnlyCollection_1_t2022272914 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1345598951(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2022272914 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1345598951_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1480293836_gshared (ReadOnlyCollection_1_t2022272914 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1480293836(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2022272914 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1480293836_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3970952355_gshared (ReadOnlyCollection_1_t2022272914 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3970952355(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2022272914 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3970952355_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m40199696_gshared (ReadOnlyCollection_1_t2022272914 * __this, Turn_t465195378  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m40199696(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2022272914 *, Turn_t465195378 , const MethodInfo*))ReadOnlyCollection_1_Contains_m40199696_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m1468110552_gshared (ReadOnlyCollection_1_t2022272914 * __this, TurnU5BU5D_t2705444999* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1468110552(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2022272914 *, TurnU5BU5D_t2705444999*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1468110552_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m1065394547_gshared (ReadOnlyCollection_1_t2022272914 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1065394547(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2022272914 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1065394547_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m3833700444_gshared (ReadOnlyCollection_1_t2022272914 * __this, Turn_t465195378  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m3833700444(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2022272914 *, Turn_t465195378 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m3833700444_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2697904287_gshared (ReadOnlyCollection_1_t2022272914 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m2697904287(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2022272914 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2697904287_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.AdvancedSmooth/Turn>::get_Item(System.Int32)
extern "C"  Turn_t465195378  ReadOnlyCollection_1_get_Item_m237939481_gshared (ReadOnlyCollection_1_t2022272914 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m237939481(__this, ___index0, method) ((  Turn_t465195378  (*) (ReadOnlyCollection_1_t2022272914 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m237939481_gshared)(__this, ___index0, method)
