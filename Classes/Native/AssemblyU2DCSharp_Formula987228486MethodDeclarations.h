﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Formula
struct Formula_t987228486;
// Formula/PlusAtt
struct PlusAtt_t2698014398;

#include "codegen/il2cpp-codegen.h"

// System.Void Formula::.ctor()
extern "C"  void Formula__ctor_m1564234981 (Formula_t987228486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Formula/PlusAtt Formula::ComputReinPlusAtt(System.UInt32,System.UInt32,System.UInt32,System.UInt32,System.Int32)
extern "C"  PlusAtt_t2698014398 * Formula_ComputReinPlusAtt_m105334906 (Il2CppObject * __this /* static, unused */, uint32_t ___bsHp0, uint32_t ___bsAttack1, uint32_t ___bsPhy_def2, uint32_t ___bsMag_def3, int32_t ___ratio4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Formula::ComputStarUpGrowUp(System.UInt32,System.Int32,System.Int32)
extern "C"  int32_t Formula_ComputStarUpGrowUp_m2270091488 (Il2CppObject * __this /* static, unused */, uint32_t ___starLv0, int32_t ___intelligence1, int32_t ___initGrowUp2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Formula::ComputStarUpGrowUpAdd(System.Int32,System.Int32,System.UInt32)
extern "C"  int32_t Formula_ComputStarUpGrowUpAdd_m3762934481 (Il2CppObject * __this /* static, unused */, int32_t ___intelligence0, int32_t ___initGrowUp1, uint32_t ___heroLv2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Formula::ComputStarUpCurAtt(System.UInt32,System.Int32,System.Int32,System.UInt32)
extern "C"  int32_t Formula_ComputStarUpCurAtt_m3409744769 (Il2CppObject * __this /* static, unused */, uint32_t ___starLv0, int32_t ___intelligence1, int32_t ___initGrowUp2, uint32_t ___heroLv3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Formula::ComputHeroTrainUpperlimit(System.Int32,System.UInt32,System.Int32)
extern "C"  int32_t Formula_ComputHeroTrainUpperlimit_m1538463176 (Il2CppObject * __this /* static, unused */, int32_t ___initValue0, uint32_t ___heroLv1, int32_t ___upLimit2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
