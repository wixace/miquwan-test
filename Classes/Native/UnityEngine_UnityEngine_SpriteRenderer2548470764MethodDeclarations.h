﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t2548470764;
// UnityEngine.Sprite
struct Sprite_t3199167241;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Sprite3199167241.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Void UnityEngine.SpriteRenderer::.ctor()
extern "C"  void SpriteRenderer__ctor_m1914895429 (SpriteRenderer_t2548470764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.SpriteRenderer::get_sprite()
extern "C"  Sprite_t3199167241 * SpriteRenderer_get_sprite_m3481747968 (SpriteRenderer_t2548470764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
extern "C"  void SpriteRenderer_set_sprite_m1519408453 (SpriteRenderer_t2548470764 * __this, Sprite_t3199167241 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.SpriteRenderer::GetSprite_INTERNAL()
extern "C"  Sprite_t3199167241 * SpriteRenderer_GetSprite_INTERNAL_m2175346003 (SpriteRenderer_t2548470764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::SetSprite_INTERNAL(UnityEngine.Sprite)
extern "C"  void SpriteRenderer_SetSprite_INTERNAL_m3702741208 (SpriteRenderer_t2548470764 * __this, Sprite_t3199167241 * ___sprite0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.SpriteRenderer::get_color()
extern "C"  Color_t4194546905  SpriteRenderer_get_color_m3519203926 (SpriteRenderer_t2548470764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
extern "C"  void SpriteRenderer_set_color_m2701854973 (SpriteRenderer_t2548470764 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::INTERNAL_get_color(UnityEngine.Color&)
extern "C"  void SpriteRenderer_INTERNAL_get_color_m1699852957 (SpriteRenderer_t2548470764 * __this, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::INTERNAL_set_color(UnityEngine.Color&)
extern "C"  void SpriteRenderer_INTERNAL_set_color_m1427138729 (SpriteRenderer_t2548470764 * __this, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SpriteRenderer::get_flipX()
extern "C"  bool SpriteRenderer_get_flipX_m3449700799 (SpriteRenderer_t2548470764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::set_flipX(System.Boolean)
extern "C"  void SpriteRenderer_set_flipX_m4285205544 (SpriteRenderer_t2548470764 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SpriteRenderer::get_flipY()
extern "C"  bool SpriteRenderer_get_flipY_m3449701760 (SpriteRenderer_t2548470764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::set_flipY(System.Boolean)
extern "C"  void SpriteRenderer_set_flipY_m1343547945 (SpriteRenderer_t2548470764 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
