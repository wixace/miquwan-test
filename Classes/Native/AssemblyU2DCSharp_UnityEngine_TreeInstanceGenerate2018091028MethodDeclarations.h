﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_TreeInstanceGenerated
struct UnityEngine_TreeInstanceGenerated_t2018091028;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_TreeInstanceGenerated::.ctor()
extern "C"  void UnityEngine_TreeInstanceGenerated__ctor_m1766069207 (UnityEngine_TreeInstanceGenerated_t2018091028 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TreeInstanceGenerated::.cctor()
extern "C"  void UnityEngine_TreeInstanceGenerated__cctor_m2726441654 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TreeInstanceGenerated::TreeInstance_TreeInstance1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TreeInstanceGenerated_TreeInstance_TreeInstance1_m3337765403 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TreeInstanceGenerated::TreeInstance_position(JSVCall)
extern "C"  void UnityEngine_TreeInstanceGenerated_TreeInstance_position_m2160997661 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TreeInstanceGenerated::TreeInstance_widthScale(JSVCall)
extern "C"  void UnityEngine_TreeInstanceGenerated_TreeInstance_widthScale_m334207362 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TreeInstanceGenerated::TreeInstance_heightScale(JSVCall)
extern "C"  void UnityEngine_TreeInstanceGenerated_TreeInstance_heightScale_m3459833315 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TreeInstanceGenerated::TreeInstance_rotation(JSVCall)
extern "C"  void UnityEngine_TreeInstanceGenerated_TreeInstance_rotation_m1829703272 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TreeInstanceGenerated::TreeInstance_color(JSVCall)
extern "C"  void UnityEngine_TreeInstanceGenerated_TreeInstance_color_m3411492579 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TreeInstanceGenerated::TreeInstance_lightmapColor(JSVCall)
extern "C"  void UnityEngine_TreeInstanceGenerated_TreeInstance_lightmapColor_m3393493257 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TreeInstanceGenerated::TreeInstance_prototypeIndex(JSVCall)
extern "C"  void UnityEngine_TreeInstanceGenerated_TreeInstance_prototypeIndex_m1840974166 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TreeInstanceGenerated::__Register()
extern "C"  void UnityEngine_TreeInstanceGenerated___Register_m30619472 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TreeInstanceGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_TreeInstanceGenerated_ilo_addJSCSRel1_m1534642963 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TreeInstanceGenerated::ilo_changeJSObj2(System.Int32,System.Object)
extern "C"  void UnityEngine_TreeInstanceGenerated_ilo_changeJSObj2_m1754816627 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TreeInstanceGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_TreeInstanceGenerated_ilo_setSingle3_m3586803263 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_TreeInstanceGenerated::ilo_getSingle4(System.Int32)
extern "C"  float UnityEngine_TreeInstanceGenerated_ilo_getSingle4_m1190425179 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_TreeInstanceGenerated::ilo_setObject5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_TreeInstanceGenerated_ilo_setObject5_m1212627643 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_TreeInstanceGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_TreeInstanceGenerated_ilo_getObject6_m3180241267 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
