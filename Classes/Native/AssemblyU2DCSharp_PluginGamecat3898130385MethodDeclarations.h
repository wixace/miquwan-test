﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginGamecat
struct PluginGamecat_t3898130385;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PluginGamecat3898130385.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void PluginGamecat::.ctor()
extern "C"  void PluginGamecat__ctor_m482204858 (PluginGamecat_t3898130385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::Init()
extern "C"  void PluginGamecat_Init_m1768206938 (PluginGamecat_t3898130385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginGamecat::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginGamecat_ReqSDKHttpLogin_m3432191531 (PluginGamecat_t3898130385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginGamecat::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginGamecat_IsLoginSuccess_m3664619121 (PluginGamecat_t3898130385 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::OpenUserLogin()
extern "C"  void PluginGamecat_OpenUserLogin_m3532891404 (PluginGamecat_t3898130385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::UserPay(CEvent.ZEvent)
extern "C"  void PluginGamecat_UserPay_m2280286438 (PluginGamecat_t3898130385 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::EnterGame(CEvent.ZEvent)
extern "C"  void PluginGamecat_EnterGame_m3138492665 (PluginGamecat_t3898130385 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::CreatRole(CEvent.ZEvent)
extern "C"  void PluginGamecat_CreatRole_m3967908516 (PluginGamecat_t3898130385 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::RoleUpdate(CEvent.ZEvent)
extern "C"  void PluginGamecat_RoleUpdate_m3150487410 (PluginGamecat_t3898130385 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::OnLoginSuccess(System.String)
extern "C"  void PluginGamecat_OnLoginSuccess_m1968712767 (PluginGamecat_t3898130385 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::OnLoginFail(System.String)
extern "C"  void PluginGamecat_OnLoginFail_m1891878242 (PluginGamecat_t3898130385 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::OnUserSwitchSuccess(System.String)
extern "C"  void PluginGamecat_OnUserSwitchSuccess_m2246385093 (PluginGamecat_t3898130385 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::OnPaySuccess(System.String)
extern "C"  void PluginGamecat_OnPaySuccess_m3028980286 (PluginGamecat_t3898130385 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::OnPayFail(System.String)
extern "C"  void PluginGamecat_OnPayFail_m1376794563 (PluginGamecat_t3898130385 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::OnLogoutSuccess(System.String)
extern "C"  void PluginGamecat_OnLogoutSuccess_m904096304 (PluginGamecat_t3898130385 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::OnLogout(System.String)
extern "C"  void PluginGamecat_OnLogout_m3708291855 (PluginGamecat_t3898130385 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::initSdk(System.String,System.String,System.Int32,System.Int32)
extern "C"  void PluginGamecat_initSdk_m3256476188 (PluginGamecat_t3898130385 * __this, String_t* ___gameId0, String_t* ___aesKey1, int32_t ___appKey2, int32_t ___cpId3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::login()
extern "C"  void PluginGamecat_login_m4219681 (PluginGamecat_t3898130385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::logout()
extern "C"  void PluginGamecat_logout_m136632820 (PluginGamecat_t3898130385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::userInfo(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginGamecat_userInfo_m20025931 (PluginGamecat_t3898130385 * __this, String_t* ___roleName0, String_t* ___serverName1, String_t* ___roleLv2, String_t* ___unionName3, String_t* ___roleId4, String_t* ___serverId5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginGamecat_pay_m4104412990 (PluginGamecat_t3898130385 * __this, String_t* ___amount0, String_t* ___productname1, String_t* ___orderId2, String_t* ___paycburl3, String_t* ___extra4, String_t* ___productID5, String_t* ___roleId6, String_t* ___roleName7, String_t* ___serverName8, String_t* ___serverId9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::<OnLogout>m__422()
extern "C"  void PluginGamecat_U3COnLogoutU3Em__422_m3952483946 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginGamecat_ilo_AddEventListener1_m751265018 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginGamecat::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginGamecat_ilo_get_Instance2_m1307125334 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::ilo_login3(PluginGamecat)
extern "C"  void PluginGamecat_ilo_login3_m3231746650 (Il2CppObject * __this /* static, unused */, PluginGamecat_t3898130385 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginGamecat::ilo_get_PluginsSdkMgr4()
extern "C"  PluginsSdkMgr_t3884624670 * PluginGamecat_ilo_get_PluginsSdkMgr4_m3494593959 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::ilo_Log5(System.Object,System.Boolean)
extern "C"  void PluginGamecat_ilo_Log5_m1459524695 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGamecat::ilo_logout6(PluginGamecat)
extern "C"  void PluginGamecat_ilo_logout6_m1315404496 (Il2CppObject * __this /* static, unused */, PluginGamecat_t3898130385 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
