﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_kerzir104
struct  M_kerzir104_t2995415946  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_kerzir104::_jedru
	float ____jedru_0;
	// System.Single GarbageiOS.M_kerzir104::_loolalldelKabearjas
	float ____loolalldelKabearjas_1;
	// System.Boolean GarbageiOS.M_kerzir104::_rikurre
	bool ____rikurre_2;
	// System.Int32 GarbageiOS.M_kerzir104::_tisdestouTaworvar
	int32_t ____tisdestouTaworvar_3;
	// System.Single GarbageiOS.M_kerzir104::_bijearwhe
	float ____bijearwhe_4;
	// System.UInt32 GarbageiOS.M_kerzir104::_sacarerXallmairfou
	uint32_t ____sacarerXallmairfou_5;
	// System.Single GarbageiOS.M_kerzir104::_sowyiKiror
	float ____sowyiKiror_6;
	// System.UInt32 GarbageiOS.M_kerzir104::_whutallLaicoosem
	uint32_t ____whutallLaicoosem_7;
	// System.Boolean GarbageiOS.M_kerzir104::_reamoDallharra
	bool ____reamoDallharra_8;
	// System.Single GarbageiOS.M_kerzir104::_rabase
	float ____rabase_9;
	// System.String GarbageiOS.M_kerzir104::_jikewarPawnashai
	String_t* ____jikewarPawnashai_10;
	// System.Boolean GarbageiOS.M_kerzir104::_lawkeda
	bool ____lawkeda_11;

public:
	inline static int32_t get_offset_of__jedru_0() { return static_cast<int32_t>(offsetof(M_kerzir104_t2995415946, ____jedru_0)); }
	inline float get__jedru_0() const { return ____jedru_0; }
	inline float* get_address_of__jedru_0() { return &____jedru_0; }
	inline void set__jedru_0(float value)
	{
		____jedru_0 = value;
	}

	inline static int32_t get_offset_of__loolalldelKabearjas_1() { return static_cast<int32_t>(offsetof(M_kerzir104_t2995415946, ____loolalldelKabearjas_1)); }
	inline float get__loolalldelKabearjas_1() const { return ____loolalldelKabearjas_1; }
	inline float* get_address_of__loolalldelKabearjas_1() { return &____loolalldelKabearjas_1; }
	inline void set__loolalldelKabearjas_1(float value)
	{
		____loolalldelKabearjas_1 = value;
	}

	inline static int32_t get_offset_of__rikurre_2() { return static_cast<int32_t>(offsetof(M_kerzir104_t2995415946, ____rikurre_2)); }
	inline bool get__rikurre_2() const { return ____rikurre_2; }
	inline bool* get_address_of__rikurre_2() { return &____rikurre_2; }
	inline void set__rikurre_2(bool value)
	{
		____rikurre_2 = value;
	}

	inline static int32_t get_offset_of__tisdestouTaworvar_3() { return static_cast<int32_t>(offsetof(M_kerzir104_t2995415946, ____tisdestouTaworvar_3)); }
	inline int32_t get__tisdestouTaworvar_3() const { return ____tisdestouTaworvar_3; }
	inline int32_t* get_address_of__tisdestouTaworvar_3() { return &____tisdestouTaworvar_3; }
	inline void set__tisdestouTaworvar_3(int32_t value)
	{
		____tisdestouTaworvar_3 = value;
	}

	inline static int32_t get_offset_of__bijearwhe_4() { return static_cast<int32_t>(offsetof(M_kerzir104_t2995415946, ____bijearwhe_4)); }
	inline float get__bijearwhe_4() const { return ____bijearwhe_4; }
	inline float* get_address_of__bijearwhe_4() { return &____bijearwhe_4; }
	inline void set__bijearwhe_4(float value)
	{
		____bijearwhe_4 = value;
	}

	inline static int32_t get_offset_of__sacarerXallmairfou_5() { return static_cast<int32_t>(offsetof(M_kerzir104_t2995415946, ____sacarerXallmairfou_5)); }
	inline uint32_t get__sacarerXallmairfou_5() const { return ____sacarerXallmairfou_5; }
	inline uint32_t* get_address_of__sacarerXallmairfou_5() { return &____sacarerXallmairfou_5; }
	inline void set__sacarerXallmairfou_5(uint32_t value)
	{
		____sacarerXallmairfou_5 = value;
	}

	inline static int32_t get_offset_of__sowyiKiror_6() { return static_cast<int32_t>(offsetof(M_kerzir104_t2995415946, ____sowyiKiror_6)); }
	inline float get__sowyiKiror_6() const { return ____sowyiKiror_6; }
	inline float* get_address_of__sowyiKiror_6() { return &____sowyiKiror_6; }
	inline void set__sowyiKiror_6(float value)
	{
		____sowyiKiror_6 = value;
	}

	inline static int32_t get_offset_of__whutallLaicoosem_7() { return static_cast<int32_t>(offsetof(M_kerzir104_t2995415946, ____whutallLaicoosem_7)); }
	inline uint32_t get__whutallLaicoosem_7() const { return ____whutallLaicoosem_7; }
	inline uint32_t* get_address_of__whutallLaicoosem_7() { return &____whutallLaicoosem_7; }
	inline void set__whutallLaicoosem_7(uint32_t value)
	{
		____whutallLaicoosem_7 = value;
	}

	inline static int32_t get_offset_of__reamoDallharra_8() { return static_cast<int32_t>(offsetof(M_kerzir104_t2995415946, ____reamoDallharra_8)); }
	inline bool get__reamoDallharra_8() const { return ____reamoDallharra_8; }
	inline bool* get_address_of__reamoDallharra_8() { return &____reamoDallharra_8; }
	inline void set__reamoDallharra_8(bool value)
	{
		____reamoDallharra_8 = value;
	}

	inline static int32_t get_offset_of__rabase_9() { return static_cast<int32_t>(offsetof(M_kerzir104_t2995415946, ____rabase_9)); }
	inline float get__rabase_9() const { return ____rabase_9; }
	inline float* get_address_of__rabase_9() { return &____rabase_9; }
	inline void set__rabase_9(float value)
	{
		____rabase_9 = value;
	}

	inline static int32_t get_offset_of__jikewarPawnashai_10() { return static_cast<int32_t>(offsetof(M_kerzir104_t2995415946, ____jikewarPawnashai_10)); }
	inline String_t* get__jikewarPawnashai_10() const { return ____jikewarPawnashai_10; }
	inline String_t** get_address_of__jikewarPawnashai_10() { return &____jikewarPawnashai_10; }
	inline void set__jikewarPawnashai_10(String_t* value)
	{
		____jikewarPawnashai_10 = value;
		Il2CppCodeGenWriteBarrier(&____jikewarPawnashai_10, value);
	}

	inline static int32_t get_offset_of__lawkeda_11() { return static_cast<int32_t>(offsetof(M_kerzir104_t2995415946, ____lawkeda_11)); }
	inline bool get__lawkeda_11() const { return ____lawkeda_11; }
	inline bool* get_address_of__lawkeda_11() { return &____lawkeda_11; }
	inline void set__lawkeda_11(bool value)
	{
		____lawkeda_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
