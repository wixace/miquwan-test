﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// ActivateTrigger
struct ActivateTrigger_t3839486245;
// UnityEngine.Animation
struct Animation_t1724966010;
// System.Object
struct Il2CppObject;
// UnityEngine.Collider
struct Collider_t2939674232;
// BlurEffect
struct BlurEffect_t197757176;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;
// BuglyAgent
struct BuglyAgent_t2905516196;
// BuglyAgent/LogCallbackDelegate
struct LogCallbackDelegate_t45039171;
// System.String
struct String_t;
// System.Func`1<System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct Func_1_t1952811501;
// System.Exception
struct Exception_t3991598821;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.UnhandledExceptionEventArgs
struct UnhandledExceptionEventArgs_t3134093121;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// BuglyCallback
struct BuglyCallback_t115794502;
// BuglyInit
struct BuglyInit_t3142012817;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// ColorCorrectionEffect
struct ColorCorrectionEffect_t3496207346;
// ContrastStretchEffect
struct ContrastStretchEffect_t443831540;
// UnityEngine.Texture
struct Texture_t2526458961;
// GlowEffect
struct GlowEffect_t1785780638;
// GrayscaleEffect
struct GrayscaleEffect_t967569688;
// ImageEffectBase
struct ImageEffectBase_t3731393437;
// ImageEffects
struct ImageEffects_t3223621447;
// MotionBlur
struct MotionBlur_t1272018269;
// NoiseEffect
struct NoiseEffect_t505428267;
// Nova.PersistPathMgr
struct PersistPathMgr_t636044869;
// SepiaToneEffect
struct SepiaToneEffect_t2573399065;
// SSAOEffect
struct SSAOEffect_t1959001535;
// UnityEngine.Shader
struct Shader_t3191267369;
// UnityEngine.Camera
struct Camera_t2727095145;
// TwirlEffect
struct TwirlEffect_t1211262097;
// VortexEffect
struct VortexEffect_t1608244223;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ActivateTrigger3839486245.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ActivateTrigger3839486245MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ActivateTrigger_Mode365823085.h"
#include "mscorlib_System_Int321153838500.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour200106419MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animation1724966010MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_Behaviour200106419.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Animation1724966010.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ActivateTrigger_Mode365823085MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BlurEffect197757176.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BlurEffect197757176MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Material3870600107MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Shader3191267369.h"
#include "UnityEngine_UnityEngine_HideFlags1436803931.h"
#include "UnityEngine_UnityEngine_SystemInfo3820892225MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Shader3191267369MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"
#include "UnityEngine_UnityEngine_Graphics3672240399MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BuglyAgent2905516196.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BuglyAgent2905516196MethodDeclarations.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LogSeverity591216193.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BuglyAgent_LogCallback45039171.h"
#include "mscorlib_System_Delegate3310234105MethodDeclarations.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Core_System_Func_1_gen1952811501.h"
#include "mscorlib_System_Exception3991598821.h"
#include "mscorlib_System_Int641153838595.h"
#include "mscorlib_System_Console1363597357MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application2856536070MethodDeclarations.h"
#include "System_Core_System_Func_1_gen1952811501MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge827649927MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder243639308MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2144973319MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_726430633MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge827649927.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_726430633.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2144973319.h"
#include "mscorlib_System_Char2862622538.h"
#include "mscorlib_System_Enum2862688501MethodDeclarations.h"
#include "mscorlib_System_Enum2862688501.h"
#include "UnityEngine_UnityEngine_Application_LogCallback2984951347MethodDeclarations.h"
#include "mscorlib_System_AppDomain3575612635MethodDeclarations.h"
#include "mscorlib_System_UnhandledExceptionEventHandler2544755120MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LogType4286006228.h"
#include "UnityEngine_UnityEngine_Application_LogCallback2984951347.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AppDomain3575612635.h"
#include "mscorlib_System_UnhandledExceptionEventArgs3134093121.h"
#include "mscorlib_System_UnhandledExceptionEventHandler2544755120.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BuglyAgent_LogCallback45039171MethodDeclarations.h"
#include "mscorlib_System_UnhandledExceptionEventArgs3134093121MethodDeclarations.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "mscorlib_System_Exception3991598821MethodDeclarations.h"
#include "mscorlib_System_Environment4152990825MethodDeclarations.h"
#include "mscorlib_System_Diagnostics_StackTrace1047871261MethodDeclarations.h"
#include "mscorlib_System_Diagnostics_StackTrace1047871261.h"
#include "mscorlib_System_Diagnostics_StackFrame1034942277.h"
#include "mscorlib_System_Reflection_ParameterInfo2235474049.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "mscorlib_System_Diagnostics_StackFrame1034942277MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBase318515428.h"
#include "mscorlib_System_Reflection_MethodBase318515428MethodDeclarations.h"
#include "mscorlib_System_Reflection_ParameterInfo2235474049MethodDeclarations.h"
#include "UnityEngine_UnityEngine_StackTraceUtility4217621253MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Regex2161232213MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Group2151468941MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_GroupCollecti982584267MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Capture754001812MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Match2156507859.h"
#include "System_System_Text_RegularExpressions_Regex2161232213.h"
#include "System_System_Text_RegularExpressions_RegexOptions3066443743.h"
#include "System_System_Text_RegularExpressions_Match2156507859MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_GroupCollecti982584267.h"
#include "System_System_Text_RegularExpressions_Group2151468941.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BuglyCallback115794502.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BuglyCallback115794502MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BuglyInit3142012817.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BuglyInit3142012817MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3187157168MethodDeclarations.h"
#include "UnityEngine_UnityEngine_DeviceType3959528308.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ColorCorrectionEffec3496207346.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ColorCorrectionEffec3496207346MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ImageEffectBase3731393437MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ContrastStretchEffect443831540.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ContrastStretchEffect443831540MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GL2267613321MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Vector44282066567MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GlowEffect1785780638.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GlowEffect1785780638MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GrayscaleEffect967569688.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GrayscaleEffect967569688MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ImageEffectBase3731393437.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ImageEffects3223621447.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ImageEffects3223621447MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2526458961MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iOSipv61040200564.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iOSipv61040200564MethodDeclarations.h"
#include "System_System_Net_Sockets_AddressFamily3770679850.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LogSeverity591216193MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MotionBlur1272018269.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MotionBlur1272018269MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_NoiseEffect505428267.h"
#include "AssemblyU2DCSharpU2Dfirstpass_NoiseEffect505428267MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random3156561159MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Nova_PersistPathMgr636044869.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Nova_PersistPathMgr636044869MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaClass1816259147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject2362096582MethodDeclarations.h"
#include "mscorlib_System_IO_Directory1148685675MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaClass1816259147.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject2362096582.h"
#include "mscorlib_System_IO_DirectoryInfo89154617.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SepiaToneEffect2573399065.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SepiaToneEffect2573399065MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SSAOEffect1959001535.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SSAOEffect1959001535MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SSAOEffect_SSAOSampl1128412491.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat2841883826.h"
#include "UnityEngine_UnityEngine_Camera2727095145MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "UnityEngine_UnityEngine_DepthTextureMode658977311.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SSAOEffect_SSAOSampl1128412491MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TwirlEffect1211262097.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TwirlEffect1211262097MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_VortexEffect1608244223.h"
#include "AssemblyU2DCSharpU2Dfirstpass_VortexEffect1608244223MethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m3652735468(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animation>()
#define GameObject_GetComponent_TisAnimation_t1724966010_m3733588168(__this, method) ((  Animation_t1724966010 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.AndroidJavaObject::CallStatic<System.Object>(System.String,System.Object[])
extern "C"  Il2CppObject * AndroidJavaObject_CallStatic_TisIl2CppObject_m4248195924_gshared (AndroidJavaObject_t2362096582 * __this, String_t* p0, ObjectU5BU5D_t1108656482* p1, const MethodInfo* method);
#define AndroidJavaObject_CallStatic_TisIl2CppObject_m4248195924(__this, p0, p1, method) ((  Il2CppObject * (*) (AndroidJavaObject_t2362096582 *, String_t*, ObjectU5BU5D_t1108656482*, const MethodInfo*))AndroidJavaObject_CallStatic_TisIl2CppObject_m4248195924_gshared)(__this, p0, p1, method)
// !!0 UnityEngine.AndroidJavaObject::CallStatic<System.String>(System.String,System.Object[])
#define AndroidJavaObject_CallStatic_TisString_t_m4016486641(__this, p0, p1, method) ((  String_t* (*) (AndroidJavaObject_t2362096582 *, String_t*, ObjectU5BU5D_t1108656482*, const MethodInfo*))AndroidJavaObject_CallStatic_TisIl2CppObject_m4248195924_gshared)(__this, p0, p1, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, method) ((  Camera_t2727095145 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ActivateTrigger::.ctor()
extern "C"  void ActivateTrigger__ctor_m2795915954 (ActivateTrigger_t3839486245 * __this, const MethodInfo* method)
{
	{
		__this->set_action_2(2);
		__this->set_triggerCount_5(1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ActivateTrigger::DoActivateTrigger()
extern Il2CppClass* Behaviour_t200106419_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t3674682005_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimation_t1724966010_m3733588168_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral540546330;
extern const uint32_t ActivateTrigger_DoActivateTrigger_m2979029002_MetadataUsageId;
extern "C"  void ActivateTrigger_DoActivateTrigger_m2979029002 (ActivateTrigger_t3839486245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ActivateTrigger_DoActivateTrigger_m2979029002_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Object_t3071478659 * V_0 = NULL;
	Behaviour_t200106419 * V_1 = NULL;
	GameObject_t3674682005 * V_2 = NULL;
	int32_t V_3 = 0;
	Object_t3071478659 * G_B5_0 = NULL;
	{
		int32_t L_0 = __this->get_triggerCount_5();
		__this->set_triggerCount_5(((int32_t)((int32_t)L_0-(int32_t)1)));
		int32_t L_1 = __this->get_triggerCount_5();
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		bool L_2 = __this->get_repeatTrigger_6();
		if (!L_2)
		{
			goto IL_0121;
		}
	}

IL_0024:
	{
		Object_t3071478659 * L_3 = __this->get_target_3();
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		Object_t3071478659 * L_5 = __this->get_target_3();
		G_B5_0 = L_5;
		goto IL_0046;
	}

IL_0040:
	{
		GameObject_t3674682005 * L_6 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		G_B5_0 = ((Object_t3071478659 *)(L_6));
	}

IL_0046:
	{
		V_0 = G_B5_0;
		Object_t3071478659 * L_7 = V_0;
		V_1 = ((Behaviour_t200106419 *)IsInstClass(L_7, Behaviour_t200106419_il2cpp_TypeInfo_var));
		Object_t3071478659 * L_8 = V_0;
		V_2 = ((GameObject_t3674682005 *)IsInstSealed(L_8, GameObject_t3674682005_il2cpp_TypeInfo_var));
		Behaviour_t200106419 * L_9 = V_1;
		bool L_10 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_9, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0068;
		}
	}
	{
		Behaviour_t200106419 * L_11 = V_1;
		NullCheck(L_11);
		GameObject_t3674682005 * L_12 = Component_get_gameObject_m1170635899(L_11, /*hidden argument*/NULL);
		V_2 = L_12;
	}

IL_0068:
	{
		int32_t L_13 = __this->get_action_2();
		V_3 = L_13;
		int32_t L_14 = V_3;
		if (L_14 == 0)
		{
			goto IL_0092;
		}
		if (L_14 == 1)
		{
			goto IL_00a2;
		}
		if (L_14 == 2)
		{
			goto IL_00e0;
		}
		if (L_14 == 3)
		{
			goto IL_00ec;
		}
		if (L_14 == 4)
		{
			goto IL_0104;
		}
		if (L_14 == 5)
		{
			goto IL_0115;
		}
	}
	{
		goto IL_0121;
	}

IL_0092:
	{
		GameObject_t3674682005 * L_15 = V_2;
		NullCheck(L_15);
		GameObject_BroadcastMessage_m3644001332(L_15, _stringLiteral540546330, /*hidden argument*/NULL);
		goto IL_0121;
	}

IL_00a2:
	{
		GameObject_t3674682005 * L_16 = __this->get_source_4();
		bool L_17 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_16, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00db;
		}
	}
	{
		GameObject_t3674682005 * L_18 = __this->get_source_4();
		GameObject_t3674682005 * L_19 = V_2;
		NullCheck(L_19);
		Transform_t1659122786 * L_20 = GameObject_get_transform_m1278640159(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t4282066566  L_21 = Transform_get_position_m2211398607(L_20, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_22 = V_2;
		NullCheck(L_22);
		Transform_t1659122786 * L_23 = GameObject_get_transform_m1278640159(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Quaternion_t1553702882  L_24 = Transform_get_rotation_m11483428(L_23, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_18, L_21, L_24, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_25 = V_2;
		Object_DestroyObject_m3900253135(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
	}

IL_00db:
	{
		goto IL_0121;
	}

IL_00e0:
	{
		GameObject_t3674682005 * L_26 = V_2;
		NullCheck(L_26);
		GameObject_SetActive_m3538205401(L_26, (bool)1, /*hidden argument*/NULL);
		goto IL_0121;
	}

IL_00ec:
	{
		Behaviour_t200106419 * L_27 = V_1;
		bool L_28 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_27, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00ff;
		}
	}
	{
		Behaviour_t200106419 * L_29 = V_1;
		NullCheck(L_29);
		Behaviour_set_enabled_m2046806933(L_29, (bool)1, /*hidden argument*/NULL);
	}

IL_00ff:
	{
		goto IL_0121;
	}

IL_0104:
	{
		GameObject_t3674682005 * L_30 = V_2;
		NullCheck(L_30);
		Animation_t1724966010 * L_31 = GameObject_GetComponent_TisAnimation_t1724966010_m3733588168(L_30, /*hidden argument*/GameObject_GetComponent_TisAnimation_t1724966010_m3733588168_MethodInfo_var);
		NullCheck(L_31);
		Animation_Play_m4273654237(L_31, /*hidden argument*/NULL);
		goto IL_0121;
	}

IL_0115:
	{
		GameObject_t3674682005 * L_32 = V_2;
		NullCheck(L_32);
		GameObject_SetActive_m3538205401(L_32, (bool)0, /*hidden argument*/NULL);
		goto IL_0121;
	}

IL_0121:
	{
		return;
	}
}
// System.Void ActivateTrigger::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void ActivateTrigger_OnTriggerEnter_m3932239366 (ActivateTrigger_t3839486245 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method)
{
	{
		ActivateTrigger_DoActivateTrigger_m2979029002(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BlurEffect::.ctor()
extern "C"  void BlurEffect__ctor_m3377079383 (BlurEffect_t197757176 * __this, const MethodInfo* method)
{
	{
		__this->set_iterations_2(3);
		__this->set_blurSpread_3((0.6f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BlurEffect::.cctor()
extern "C"  void BlurEffect__cctor_m1128149558 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// UnityEngine.Material BlurEffect::get_material()
extern Il2CppClass* BlurEffect_t197757176_il2cpp_TypeInfo_var;
extern Il2CppClass* Material_t3870600107_il2cpp_TypeInfo_var;
extern const uint32_t BlurEffect_get_material_m2440803492_MetadataUsageId;
extern "C"  Material_t3870600107 * BlurEffect_get_material_m2440803492 (BlurEffect_t197757176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BlurEffect_get_material_m2440803492_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BlurEffect_t197757176_il2cpp_TypeInfo_var);
		Material_t3870600107 * L_0 = ((BlurEffect_t197757176_StaticFields*)BlurEffect_t197757176_il2cpp_TypeInfo_var->static_fields)->get_m_Material_5();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		Shader_t3191267369 * L_2 = __this->get_blurShader_4();
		Material_t3870600107 * L_3 = (Material_t3870600107 *)il2cpp_codegen_object_new(Material_t3870600107_il2cpp_TypeInfo_var);
		Material__ctor_m2685909642(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(BlurEffect_t197757176_il2cpp_TypeInfo_var);
		((BlurEffect_t197757176_StaticFields*)BlurEffect_t197757176_il2cpp_TypeInfo_var->static_fields)->set_m_Material_5(L_3);
		Material_t3870600107 * L_4 = ((BlurEffect_t197757176_StaticFields*)BlurEffect_t197757176_il2cpp_TypeInfo_var->static_fields)->get_m_Material_5();
		NullCheck(L_4);
		Object_set_hideFlags_m41317712(L_4, ((int32_t)52), /*hidden argument*/NULL);
	}

IL_002c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(BlurEffect_t197757176_il2cpp_TypeInfo_var);
		Material_t3870600107 * L_5 = ((BlurEffect_t197757176_StaticFields*)BlurEffect_t197757176_il2cpp_TypeInfo_var->static_fields)->get_m_Material_5();
		return L_5;
	}
}
// System.Void BlurEffect::OnDisable()
extern Il2CppClass* BlurEffect_t197757176_il2cpp_TypeInfo_var;
extern const uint32_t BlurEffect_OnDisable_m577336638_MetadataUsageId;
extern "C"  void BlurEffect_OnDisable_m577336638 (BlurEffect_t197757176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BlurEffect_OnDisable_m577336638_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BlurEffect_t197757176_il2cpp_TypeInfo_var);
		Material_t3870600107 * L_0 = ((BlurEffect_t197757176_StaticFields*)BlurEffect_t197757176_il2cpp_TypeInfo_var->static_fields)->get_m_Material_5();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BlurEffect_t197757176_il2cpp_TypeInfo_var);
		Material_t3870600107 * L_2 = ((BlurEffect_t197757176_StaticFields*)BlurEffect_t197757176_il2cpp_TypeInfo_var->static_fields)->get_m_Material_5();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0019:
	{
		return;
	}
}
// System.Void BlurEffect::Start()
extern "C"  void BlurEffect_Start_m2324217175 (BlurEffect_t197757176 * __this, const MethodInfo* method)
{
	{
		bool L_0 = SystemInfo_get_supportsImageEffects_m2392300814(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_0012:
	{
		Shader_t3191267369 * L_1 = __this->get_blurShader_4();
		bool L_2 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0037;
		}
	}
	{
		Material_t3870600107 * L_3 = BlurEffect_get_material_m2440803492(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Shader_t3191267369 * L_4 = Material_get_shader_m2881845503(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = Shader_get_isSupported_m1422621179(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_003f;
		}
	}

IL_0037:
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_003f:
	{
		return;
	}
}
// System.Void BlurEffect::FourTapCone(UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Int32)
extern Il2CppClass* Vector2U5BU5D_t4024180168_il2cpp_TypeInfo_var;
extern const uint32_t BlurEffect_FourTapCone_m3282173034_MetadataUsageId;
extern "C"  void BlurEffect_FourTapCone_m3282173034 (BlurEffect_t197757176 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___dest1, int32_t ___iteration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BlurEffect_FourTapCone_m3282173034_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___iteration2;
		float L_1 = __this->get_blurSpread_3();
		V_0 = ((float)((float)(0.5f)+(float)((float)((float)(((float)((float)L_0)))*(float)L_1))));
		RenderTexture_t1963041563 * L_2 = ___source0;
		RenderTexture_t1963041563 * L_3 = ___dest1;
		Material_t3870600107 * L_4 = BlurEffect_get_material_m2440803492(__this, /*hidden argument*/NULL);
		Vector2U5BU5D_t4024180168* L_5 = ((Vector2U5BU5D_t4024180168*)SZArrayNew(Vector2U5BU5D_t4024180168_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		float L_6 = V_0;
		float L_7 = V_0;
		Vector2_t4282066565  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector2__ctor_m1517109030(&L_8, ((-L_6)), ((-L_7)), /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_8;
		Vector2U5BU5D_t4024180168* L_9 = L_5;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		float L_10 = V_0;
		float L_11 = V_0;
		Vector2_t4282066565  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector2__ctor_m1517109030(&L_12, ((-L_10)), L_11, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_12;
		Vector2U5BU5D_t4024180168* L_13 = L_9;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
		float L_14 = V_0;
		float L_15 = V_0;
		Vector2_t4282066565  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector2__ctor_m1517109030(&L_16, L_14, L_15, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_16;
		Vector2U5BU5D_t4024180168* L_17 = L_13;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 3);
		float L_18 = V_0;
		float L_19 = V_0;
		Vector2_t4282066565  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector2__ctor_m1517109030(&L_20, L_18, ((-L_19)), /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_20;
		Graphics_BlitMultiTap_m4211810031(NULL /*static, unused*/, L_2, L_3, L_4, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BlurEffect::DownSample4x(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Vector2U5BU5D_t4024180168_il2cpp_TypeInfo_var;
extern const uint32_t BlurEffect_DownSample4x_m2331705765_MetadataUsageId;
extern "C"  void BlurEffect_DownSample4x_m2331705765 (BlurEffect_t197757176 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___dest1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BlurEffect_DownSample4x_m2331705765_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (1.0f);
		RenderTexture_t1963041563 * L_0 = ___source0;
		RenderTexture_t1963041563 * L_1 = ___dest1;
		Material_t3870600107 * L_2 = BlurEffect_get_material_m2440803492(__this, /*hidden argument*/NULL);
		Vector2U5BU5D_t4024180168* L_3 = ((Vector2U5BU5D_t4024180168*)SZArrayNew(Vector2U5BU5D_t4024180168_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		float L_4 = V_0;
		float L_5 = V_0;
		Vector2_t4282066565  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector2__ctor_m1517109030(&L_6, ((-L_4)), ((-L_5)), /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_6;
		Vector2U5BU5D_t4024180168* L_7 = L_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		float L_8 = V_0;
		float L_9 = V_0;
		Vector2_t4282066565  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector2__ctor_m1517109030(&L_10, ((-L_8)), L_9, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_10;
		Vector2U5BU5D_t4024180168* L_11 = L_7;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 2);
		float L_12 = V_0;
		float L_13 = V_0;
		Vector2_t4282066565  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector2__ctor_m1517109030(&L_14, L_12, L_13, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_14;
		Vector2U5BU5D_t4024180168* L_15 = L_11;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 3);
		float L_16 = V_0;
		float L_17 = V_0;
		Vector2_t4282066565  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector2__ctor_m1517109030(&L_18, L_16, ((-L_17)), /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_18;
		Graphics_BlitMultiTap_m4211810031(NULL /*static, unused*/, L_0, L_1, L_2, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BlurEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void BlurEffect_OnRenderImage_m1598977447 (BlurEffect_t197757176 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RenderTexture_t1963041563 * V_2 = NULL;
	int32_t V_3 = 0;
	RenderTexture_t1963041563 * V_4 = NULL;
	{
		RenderTexture_t1963041563 * L_0 = ___source0;
		NullCheck(L_0);
		int32_t L_1 = RenderTexture_get_width_m1498578543(L_0, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_1/(int32_t)4));
		RenderTexture_t1963041563 * L_2 = ___source0;
		NullCheck(L_2);
		int32_t L_3 = RenderTexture_get_height_m4010076224(L_2, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_3/(int32_t)4));
		int32_t L_4 = V_0;
		int32_t L_5 = V_1;
		RenderTexture_t1963041563 * L_6 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, L_4, L_5, 0, /*hidden argument*/NULL);
		V_2 = L_6;
		RenderTexture_t1963041563 * L_7 = ___source0;
		RenderTexture_t1963041563 * L_8 = V_2;
		BlurEffect_DownSample4x_m2331705765(__this, L_7, L_8, /*hidden argument*/NULL);
		V_3 = 0;
		goto IL_004b;
	}

IL_002a:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = V_1;
		RenderTexture_t1963041563 * L_11 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, L_9, L_10, 0, /*hidden argument*/NULL);
		V_4 = L_11;
		RenderTexture_t1963041563 * L_12 = V_2;
		RenderTexture_t1963041563 * L_13 = V_4;
		int32_t L_14 = V_3;
		BlurEffect_FourTapCone_m3282173034(__this, L_12, L_13, L_14, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_15 = V_2;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_16 = V_4;
		V_2 = L_16;
		int32_t L_17 = V_3;
		V_3 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_004b:
	{
		int32_t L_18 = V_3;
		int32_t L_19 = __this->get_iterations_2();
		if ((((int32_t)L_18) < ((int32_t)L_19)))
		{
			goto IL_002a;
		}
	}
	{
		RenderTexture_t1963041563 * L_20 = V_2;
		RenderTexture_t1963041563 * L_21 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_22 = V_2;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::.ctor()
extern "C"  void BuglyAgent__ctor_m4019185195 (BuglyAgent_t2905516196 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::.cctor()
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral46675323;
extern const uint32_t BuglyAgent__cctor_m3853560546_MetadataUsageId;
extern "C"  void BuglyAgent__cctor_m3853560546 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent__cctor_m3853560546_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->set__autoReportLogLevel_2(5);
		((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->set__crashReporterType_3(1);
		((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->set__crashReproterCustomizedLogLevel_4(2);
		((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->set_EXCEPTION_TYPE_UNCAUGHT_7(1);
		((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->set_EXCEPTION_TYPE_CAUGHT_8(2);
		((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->set__pluginVersion_9(_stringLiteral46675323);
		return;
	}
}
// System.Void BuglyAgent::add__LogCallbackEventHandler(BuglyAgent/LogCallbackDelegate)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* LogCallbackDelegate_t45039171_il2cpp_TypeInfo_var;
extern const uint32_t BuglyAgent_add__LogCallbackEventHandler_m3543983054_MetadataUsageId;
extern "C"  void BuglyAgent_add__LogCallbackEventHandler_m3543983054 (Il2CppObject * __this /* static, unused */, LogCallbackDelegate_t45039171 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_add__LogCallbackEventHandler_m3543983054_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		LogCallbackDelegate_t45039171 * L_0 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__LogCallbackEventHandler_12();
		LogCallbackDelegate_t45039171 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->set__LogCallbackEventHandler_12(((LogCallbackDelegate_t45039171 *)CastclassSealed(L_2, LogCallbackDelegate_t45039171_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void BuglyAgent::remove__LogCallbackEventHandler(BuglyAgent/LogCallbackDelegate)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* LogCallbackDelegate_t45039171_il2cpp_TypeInfo_var;
extern const uint32_t BuglyAgent_remove__LogCallbackEventHandler_m1877121307_MetadataUsageId;
extern "C"  void BuglyAgent_remove__LogCallbackEventHandler_m1877121307 (Il2CppObject * __this /* static, unused */, LogCallbackDelegate_t45039171 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_remove__LogCallbackEventHandler_m1877121307_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		LogCallbackDelegate_t45039171 * L_0 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__LogCallbackEventHandler_12();
		LogCallbackDelegate_t45039171 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->set__LogCallbackEventHandler_12(((LogCallbackDelegate_t45039171 *)CastclassSealed(L_2, LogCallbackDelegate_t45039171_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void BuglyAgent::ConfigCrashReporter(System.Int32,System.Int32)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern const uint32_t BuglyAgent_ConfigCrashReporter_m1021581495_MetadataUsageId;
extern "C"  void BuglyAgent_ConfigCrashReporter_m1021581495 (Il2CppObject * __this /* static, unused */, int32_t ___type0, int32_t ___logLevel1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_ConfigCrashReporter_m1021581495_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent__SetCrashReporterType_m3845894329(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___logLevel1;
		BuglyAgent__SetCrashReporterLogLevel_m799001375(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::InitWithAppId(System.String)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3468989700;
extern Il2CppCodeGenString* _stringLiteral764188538;
extern const uint32_t BuglyAgent_InitWithAppId_m2313387731_MetadataUsageId;
extern "C"  void BuglyAgent_InitWithAppId_m2313387731 (Il2CppObject * __this /* static, unused */, String_t* ___appId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_InitWithAppId_m2313387731_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		bool L_0 = BuglyAgent_get_IsInitialized_m1689956366(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_DebugLog_m609003136(NULL /*static, unused*/, (String_t*)NULL, _stringLiteral3468989700, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return;
	}

IL_001c:
	{
		String_t* L_1 = ___appId0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		return;
	}

IL_0028:
	{
		String_t* L_3 = ___appId0;
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_InitBuglyAgent_m1360743349(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_4 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_5 = ___appId0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		BuglyAgent_DebugLog_m609003136(NULL /*static, unused*/, (String_t*)NULL, _stringLiteral764188538, L_4, /*hidden argument*/NULL);
		BuglyAgent__RegisterExceptionHandler_m1437323366(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::EnableExceptionHandler()
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3468989700;
extern Il2CppCodeGenString* _stringLiteral3012504003;
extern const uint32_t BuglyAgent_EnableExceptionHandler_m724280279_MetadataUsageId;
extern "C"  void BuglyAgent_EnableExceptionHandler_m724280279 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_EnableExceptionHandler_m724280279_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		bool L_0 = BuglyAgent_get_IsInitialized_m1689956366(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_DebugLog_m609003136(NULL /*static, unused*/, (String_t*)NULL, _stringLiteral3468989700, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_DebugLog_m609003136(NULL /*static, unused*/, (String_t*)NULL, _stringLiteral3012504003, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		BuglyAgent__RegisterExceptionHandler_m1437323366(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::RegisterLogCallback(BuglyAgent/LogCallbackDelegate)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* LogCallbackDelegate_t45039171_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1817813848;
extern const uint32_t BuglyAgent_RegisterLogCallback_m2466877548_MetadataUsageId;
extern "C"  void BuglyAgent_RegisterLogCallback_m2466877548 (Il2CppObject * __this /* static, unused */, LogCallbackDelegate_t45039171 * ___handler0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_RegisterLogCallback_m2466877548_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LogCallbackDelegate_t45039171 * L_0 = ___handler0;
		if (!L_0)
		{
			goto IL_0030;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_1 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		LogCallbackDelegate_t45039171 * L_2 = ___handler0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_DebugLog_m609003136(NULL /*static, unused*/, (String_t*)NULL, _stringLiteral1817813848, L_1, /*hidden argument*/NULL);
		LogCallbackDelegate_t45039171 * L_3 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__LogCallbackEventHandler_12();
		LogCallbackDelegate_t45039171 * L_4 = ___handler0;
		Delegate_t3310234105 * L_5 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->set__LogCallbackEventHandler_12(((LogCallbackDelegate_t45039171 *)CastclassSealed(L_5, LogCallbackDelegate_t45039171_il2cpp_TypeInfo_var)));
	}

IL_0030:
	{
		return;
	}
}
// System.Void BuglyAgent::SetLogCallbackExtrasHandler(System.Func`1<System.Collections.Generic.Dictionary`2<System.String,System.String>>)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2773640230;
extern const uint32_t BuglyAgent_SetLogCallbackExtrasHandler_m1328908226_MetadataUsageId;
extern "C"  void BuglyAgent_SetLogCallbackExtrasHandler_m1328908226 (Il2CppObject * __this /* static, unused */, Func_1_t1952811501 * ___handler0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_SetLogCallbackExtrasHandler_m1328908226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Func_1_t1952811501 * L_0 = ___handler0;
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Func_1_t1952811501 * L_1 = ___handler0;
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->set__LogCallbackExtrasHandler_10(L_1);
		ObjectU5BU5D_t1108656482* L_2 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		Func_1_t1952811501 * L_3 = ___handler0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		BuglyAgent_DebugLog_m609003136(NULL /*static, unused*/, (String_t*)NULL, _stringLiteral2773640230, L_2, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void BuglyAgent::ReportException(System.Exception,System.String)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2434106562;
extern const uint32_t BuglyAgent_ReportException_m78035440_MetadataUsageId;
extern "C"  void BuglyAgent_ReportException_m78035440 (Il2CppObject * __this /* static, unused */, Exception_t3991598821 * ___e0, String_t* ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_ReportException_m78035440_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		bool L_0 = BuglyAgent_get_IsInitialized_m1689956366(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		ObjectU5BU5D_t1108656482* L_1 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)2));
		String_t* L_2 = ___message1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		ObjectU5BU5D_t1108656482* L_3 = L_1;
		Exception_t3991598821 * L_4 = ___e0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_DebugLog_m609003136(NULL /*static, unused*/, (String_t*)NULL, _stringLiteral2434106562, L_3, /*hidden argument*/NULL);
		Exception_t3991598821 * L_5 = ___e0;
		String_t* L_6 = ___message1;
		BuglyAgent__HandleException_m1464489584(NULL /*static, unused*/, L_5, L_6, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::ReportException(System.String,System.String,System.String)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1610779350;
extern const uint32_t BuglyAgent_ReportException_m2758451190_MetadataUsageId;
extern "C"  void BuglyAgent_ReportException_m2758451190 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___message1, String_t* ___stackTrace2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_ReportException_m2758451190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		bool L_0 = BuglyAgent_get_IsInitialized_m1689956366(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		ObjectU5BU5D_t1108656482* L_1 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)3));
		String_t* L_2 = ___name0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		ObjectU5BU5D_t1108656482* L_3 = L_1;
		String_t* L_4 = ___message1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t1108656482* L_5 = L_3;
		String_t* L_6 = ___stackTrace2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_6);
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_DebugLog_m609003136(NULL /*static, unused*/, (String_t*)NULL, _stringLiteral1610779350, L_5, /*hidden argument*/NULL);
		String_t* L_7 = ___name0;
		String_t* L_8 = ___message1;
		String_t* L_9 = ___stackTrace2;
		BuglyAgent__HandleException_m1838932569(NULL /*static, unused*/, 6, L_7, L_8, L_9, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::UnregisterLogCallback(BuglyAgent/LogCallbackDelegate)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* LogCallbackDelegate_t45039171_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2974049735;
extern const uint32_t BuglyAgent_UnregisterLogCallback_m60396339_MetadataUsageId;
extern "C"  void BuglyAgent_UnregisterLogCallback_m60396339 (Il2CppObject * __this /* static, unused */, LogCallbackDelegate_t45039171 * ___handler0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_UnregisterLogCallback_m60396339_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LogCallbackDelegate_t45039171 * L_0 = ___handler0;
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_DebugLog_m609003136(NULL /*static, unused*/, (String_t*)NULL, _stringLiteral2974049735, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		LogCallbackDelegate_t45039171 * L_1 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__LogCallbackEventHandler_12();
		LogCallbackDelegate_t45039171 * L_2 = ___handler0;
		Delegate_t3310234105 * L_3 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->set__LogCallbackEventHandler_12(((LogCallbackDelegate_t45039171 *)CastclassSealed(L_3, LogCallbackDelegate_t45039171_il2cpp_TypeInfo_var)));
	}

IL_002c:
	{
		return;
	}
}
// System.Void BuglyAgent::SetUserId(System.String)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3918934704;
extern const uint32_t BuglyAgent_SetUserId_m813488145_MetadataUsageId;
extern "C"  void BuglyAgent_SetUserId_m813488145 (Il2CppObject * __this /* static, unused */, String_t* ___userId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_SetUserId_m813488145_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		bool L_0 = BuglyAgent_get_IsInitialized_m1689956366(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		ObjectU5BU5D_t1108656482* L_1 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_2 = ___userId0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_DebugLog_m609003136(NULL /*static, unused*/, (String_t*)NULL, _stringLiteral3918934704, L_1, /*hidden argument*/NULL);
		String_t* L_3 = ___userId0;
		BuglyAgent_SetUserInfo_m840563038(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::SetScene(System.Int32)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3884140724;
extern const uint32_t BuglyAgent_SetScene_m2773184852_MetadataUsageId;
extern "C"  void BuglyAgent_SetScene_m2773184852 (Il2CppObject * __this /* static, unused */, int32_t ___sceneId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_SetScene_m2773184852_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		bool L_0 = BuglyAgent_get_IsInitialized_m1689956366(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		ObjectU5BU5D_t1108656482* L_1 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		int32_t L_2 = ___sceneId0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_DebugLog_m609003136(NULL /*static, unused*/, (String_t*)NULL, _stringLiteral3884140724, L_1, /*hidden argument*/NULL);
		int32_t L_5 = ___sceneId0;
		BuglyAgent_SetCurrentScene_m1379816559(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::AddSceneData(System.String,System.String)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1857538170;
extern const uint32_t BuglyAgent_AddSceneData_m2933483472_MetadataUsageId;
extern "C"  void BuglyAgent_AddSceneData_m2933483472 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_AddSceneData_m2933483472_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		bool L_0 = BuglyAgent_get_IsInitialized_m1689956366(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		ObjectU5BU5D_t1108656482* L_1 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)2));
		String_t* L_2 = ___key0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		ObjectU5BU5D_t1108656482* L_3 = L_1;
		String_t* L_4 = ___value1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_DebugLog_m609003136(NULL /*static, unused*/, (String_t*)NULL, _stringLiteral1857538170, L_3, /*hidden argument*/NULL);
		String_t* L_5 = ___key0;
		String_t* L_6 = ___value1;
		BuglyAgent_AddKeyAndValueInScene_m1823870950(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::ConfigDebugMode(System.Boolean)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4231416513;
extern Il2CppCodeGenString* _stringLiteral2079986083;
extern Il2CppCodeGenString* _stringLiteral3335961288;
extern const uint32_t BuglyAgent_ConfigDebugMode_m3082024052_MetadataUsageId;
extern "C"  void BuglyAgent_ConfigDebugMode_m3082024052 (Il2CppObject * __this /* static, unused */, bool ___enable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_ConfigDebugMode_m3082024052_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	ObjectU5BU5D_t1108656482* G_B2_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	Il2CppObject * G_B2_4 = NULL;
	int32_t G_B1_0 = 0;
	ObjectU5BU5D_t1108656482* G_B1_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	Il2CppObject * G_B1_4 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	ObjectU5BU5D_t1108656482* G_B3_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B3_3 = NULL;
	String_t* G_B3_4 = NULL;
	Il2CppObject * G_B3_5 = NULL;
	{
		bool L_0 = ___enable0;
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_EnableDebugMode_m2898701651(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_1 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		bool L_2 = ___enable0;
		G_B1_0 = 0;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		G_B1_3 = _stringLiteral4231416513;
		G_B1_4 = NULL;
		if (!L_2)
		{
			G_B2_0 = 0;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			G_B2_3 = _stringLiteral4231416513;
			G_B2_4 = NULL;
			goto IL_0024;
		}
	}
	{
		G_B3_0 = _stringLiteral2079986083;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		G_B3_5 = G_B1_4;
		goto IL_0029;
	}

IL_0024:
	{
		G_B3_0 = _stringLiteral3335961288;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
		G_B3_5 = G_B2_4;
	}

IL_0029:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (Il2CppObject *)G_B3_0);
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_DebugLog_m609003136(NULL /*static, unused*/, (String_t*)G_B3_5, G_B3_4, G_B3_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::ConfigAutoQuitApplication(System.Boolean)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern const uint32_t BuglyAgent_ConfigAutoQuitApplication_m4019756080_MetadataUsageId;
extern "C"  void BuglyAgent_ConfigAutoQuitApplication_m4019756080 (Il2CppObject * __this /* static, unused */, bool ___autoQuit0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_ConfigAutoQuitApplication_m4019756080_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___autoQuit0;
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->set__autoQuitApplicationAfterReport_6(L_0);
		return;
	}
}
// System.Void BuglyAgent::ConfigAutoReportLogLevel(LogSeverity)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern const uint32_t BuglyAgent_ConfigAutoReportLogLevel_m3131243571_MetadataUsageId;
extern "C"  void BuglyAgent_ConfigAutoReportLogLevel_m3131243571 (Il2CppObject * __this /* static, unused */, int32_t ___level0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_ConfigAutoReportLogLevel_m3131243571_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___level0;
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->set__autoReportLogLevel_2(L_0);
		return;
	}
}
// System.Void BuglyAgent::ConfigDefault(System.String,System.String,System.String,System.Int64)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2444134290;
extern const uint32_t BuglyAgent_ConfigDefault_m4037068102_MetadataUsageId;
extern "C"  void BuglyAgent_ConfigDefault_m4037068102 (Il2CppObject * __this /* static, unused */, String_t* ___channel0, String_t* ___version1, String_t* ___user2, int64_t ___delay3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_ConfigDefault_m4037068102_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		String_t* L_1 = ___channel0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t1108656482* L_2 = L_0;
		String_t* L_3 = ___version1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_2;
		String_t* L_5 = ___user2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_5);
		ObjectU5BU5D_t1108656482* L_6 = L_4;
		int64_t L_7 = ___delay3;
		int64_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int64_t1153838595_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_DebugLog_m609003136(NULL /*static, unused*/, (String_t*)NULL, _stringLiteral2444134290, L_6, /*hidden argument*/NULL);
		String_t* L_10 = ___channel0;
		String_t* L_11 = ___version1;
		String_t* L_12 = ___user2;
		int64_t L_13 = ___delay3;
		BuglyAgent_ConfigDefaultBeforeInit_m3838653653(NULL /*static, unused*/, L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::DebugLog(System.String,System.String,System.Object[])
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Console_t1363597357_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2590640091;
extern const uint32_t BuglyAgent_DebugLog_m609003136_MetadataUsageId;
extern "C"  void BuglyAgent_DebugLog_m609003136 (Il2CppObject * __this /* static, unused */, String_t* ___tag0, String_t* ___format1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_DebugLog_m609003136_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		bool L_0 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__debugMode_5();
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		String_t* L_1 = ___format1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		String_t* L_3 = ___tag0;
		String_t* L_4 = ___format1;
		ObjectU5BU5D_t1108656482* L_5 = ___args2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Format_m4050103162(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t1363597357_il2cpp_TypeInfo_var);
		Console_WriteLine_m2090852051(NULL /*static, unused*/, _stringLiteral2590640091, L_3, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::PrintLog(LogSeverity,System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern const uint32_t BuglyAgent_PrintLog_m1023618409_MetadataUsageId;
extern "C"  void BuglyAgent_PrintLog_m1023618409 (Il2CppObject * __this /* static, unused */, int32_t ___level0, String_t* ___format1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_PrintLog_m1023618409_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___format1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		int32_t L_2 = ___level0;
		String_t* L_3 = ___format1;
		ObjectU5BU5D_t1108656482* L_4 = ___args2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m4050103162(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_LogRecord_m2405146447(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::ConfigCrashReporterType()
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t BuglyAgent_ConfigCrashReporterType_m1930542889_MetadataUsageId;
extern "C"  void BuglyAgent_ConfigCrashReporterType_m1930542889 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_ConfigCrashReporterType_m1930542889_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		bool L_0 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__crashReporterTypeConfiged_0();
		if (L_0)
		{
			goto IL_0025;
		}
	}

IL_000a:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		int32_t L_1 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__crashReporterType_3();
		BuglyAgent__BuglyConfigCrashReporterType_m3603612760(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->set__crashReporterTypeConfiged_0((bool)1);
		goto IL_0025;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.Object)
		goto IL_0025;
	} // end catch (depth: 1)

IL_0025:
	{
		return;
	}
}
// System.Void BuglyAgent::ConfigDefaultBeforeInit(System.String,System.String,System.String,System.Int64)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t BuglyAgent_ConfigDefaultBeforeInit_m3838653653_MetadataUsageId;
extern "C"  void BuglyAgent_ConfigDefaultBeforeInit_m3838653653 (Il2CppObject * __this /* static, unused */, String_t* ___channel0, String_t* ___version1, String_t* ___user2, int64_t ___delay3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_ConfigDefaultBeforeInit_m3838653653_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_ConfigCrashReporterType_m1930542889(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0005:
	try
	{ // begin try (depth: 1)
		String_t* L_0 = ___channel0;
		String_t* L_1 = ___version1;
		String_t* L_2 = ___user2;
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent__BuglyDefaultConfig_m1986590636(NULL /*static, unused*/, L_0, L_1, L_2, (String_t*)NULL, /*hidden argument*/NULL);
		goto IL_0019;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Object)
		goto IL_0019;
	} // end catch (depth: 1)

IL_0019:
	{
		return;
	}
}
// System.Void BuglyAgent::EnableDebugMode(System.Boolean)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern const uint32_t BuglyAgent_EnableDebugMode_m2898701651_MetadataUsageId;
extern "C"  void BuglyAgent_EnableDebugMode_m2898701651 (Il2CppObject * __this /* static, unused */, bool ___enable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_EnableDebugMode_m2898701651_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___enable0;
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->set__debugMode_5(L_0);
		return;
	}
}
// System.Void BuglyAgent::InitBuglyAgent(System.String)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t BuglyAgent_InitBuglyAgent_m1360743349_MetadataUsageId;
extern "C"  void BuglyAgent_InitBuglyAgent_m1360743349 (Il2CppObject * __this /* static, unused */, String_t* ___appId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_InitBuglyAgent_m1360743349_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_ConfigCrashReporterType_m1930542889(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_0 = ___appId0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_2 = ___appId0;
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		bool L_3 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__debugMode_5();
		int32_t L_4 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__crashReproterCustomizedLogLevel_4();
		BuglyAgent__BuglyInit_m2210360241(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void BuglyAgent::SetUnityVersion()
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2063487939;
extern const uint32_t BuglyAgent_SetUnityVersion_m1918232878_MetadataUsageId;
extern "C"  void BuglyAgent_SetUnityVersion_m1918232878 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_SetUnityVersion_m1918232878_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_ConfigCrashReporterType_m1930542889(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_0 = Application_get_unityVersion_m3443350436(NULL /*static, unused*/, /*hidden argument*/NULL);
		BuglyAgent__BuglySetExtraConfig_m2740188051(NULL /*static, unused*/, _stringLiteral2063487939, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::SetUserInfo(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern const uint32_t BuglyAgent_SetUserInfo_m840563038_MetadataUsageId;
extern "C"  void BuglyAgent_SetUserInfo_m840563038 (Il2CppObject * __this /* static, unused */, String_t* ___userInfo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_SetUserInfo_m840563038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___userInfo0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_ConfigCrashReporterType_m1930542889(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = ___userInfo0;
		BuglyAgent__BuglySetUserId_m488354291(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void BuglyAgent::ReportException(System.Int32,System.String,System.String,System.String,System.Boolean)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t827649927_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t2144973319_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_1_Invoke_m1665926390_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Count_m2614934137_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m640701813_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m2915568422_MethodInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m2936947827_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m2759194411_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2871721525_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1739472607_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m730091314_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2577713898_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2063487939;
extern Il2CppCodeGenString* _stringLiteral1956101299;
extern Il2CppCodeGenString* _stringLiteral32148;
extern Il2CppCodeGenString* _stringLiteral2543526776;
extern const uint32_t BuglyAgent_ReportException_m769667476_MetadataUsageId;
extern "C"  void BuglyAgent_ReportException_m769667476 (Il2CppObject * __this /* static, unused */, int32_t ___type0, String_t* ___name1, String_t* ___reason2, String_t* ___stackTrace3, bool ___quitProgram4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_ReportException_m769667476_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Dictionary_2_t827649927 * V_1 = NULL;
	StringBuilder_t243639308 * V_2 = NULL;
	KeyValuePair_2_t726430633  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Enumerator_t2144973319  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_ConfigCrashReporterType_m1930542889(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		V_1 = (Dictionary_2_t827649927 *)NULL;
		Func_1_t1952811501 * L_1 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__LogCallbackExtrasHandler_10();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		Func_1_t1952811501 * L_2 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__LogCallbackExtrasHandler_10();
		NullCheck(L_2);
		Dictionary_2_t827649927 * L_3 = Func_1_Invoke_m1665926390(L_2, /*hidden argument*/Func_1_Invoke_m1665926390_MethodInfo_var);
		V_1 = L_3;
	}

IL_0022:
	{
		Dictionary_2_t827649927 * L_4 = V_1;
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		Dictionary_2_t827649927 * L_5 = V_1;
		NullCheck(L_5);
		int32_t L_6 = Dictionary_2_get_Count_m2614934137(L_5, /*hidden argument*/Dictionary_2_get_Count_m2614934137_MethodInfo_var);
		if (L_6)
		{
			goto IL_0049;
		}
	}

IL_0033:
	{
		Dictionary_2_t827649927 * L_7 = (Dictionary_2_t827649927 *)il2cpp_codegen_object_new(Dictionary_2_t827649927_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m640701813(L_7, /*hidden argument*/Dictionary_2__ctor_m640701813_MethodInfo_var);
		V_1 = L_7;
		Dictionary_2_t827649927 * L_8 = V_1;
		String_t* L_9 = Application_get_unityVersion_m3443350436(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Dictionary_2_Add_m2915568422(L_8, _stringLiteral2063487939, L_9, /*hidden argument*/Dictionary_2_Add_m2915568422_MethodInfo_var);
	}

IL_0049:
	{
		Dictionary_2_t827649927 * L_10 = V_1;
		if (!L_10)
		{
			goto IL_00fd;
		}
	}
	{
		Dictionary_2_t827649927 * L_11 = V_1;
		NullCheck(L_11);
		int32_t L_12 = Dictionary_2_get_Count_m2614934137(L_11, /*hidden argument*/Dictionary_2_get_Count_m2614934137_MethodInfo_var);
		if ((((int32_t)L_12) <= ((int32_t)0)))
		{
			goto IL_00fd;
		}
	}
	{
		Dictionary_2_t827649927 * L_13 = V_1;
		NullCheck(L_13);
		bool L_14 = Dictionary_2_ContainsKey_m2936947827(L_13, _stringLiteral2063487939, /*hidden argument*/Dictionary_2_ContainsKey_m2936947827_MethodInfo_var);
		if (L_14)
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t827649927 * L_15 = V_1;
		String_t* L_16 = Application_get_unityVersion_m3443350436(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		Dictionary_2_Add_m2915568422(L_15, _stringLiteral2063487939, L_16, /*hidden argument*/Dictionary_2_Add_m2915568422_MethodInfo_var);
	}

IL_007b:
	{
		StringBuilder_t243639308 * L_17 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_17, /*hidden argument*/NULL);
		V_2 = L_17;
		Dictionary_2_t827649927 * L_18 = V_1;
		NullCheck(L_18);
		Enumerator_t2144973319  L_19 = Dictionary_2_GetEnumerator_m2759194411(L_18, /*hidden argument*/Dictionary_2_GetEnumerator_m2759194411_MethodInfo_var);
		V_4 = L_19;
	}

IL_0089:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00bf;
		}

IL_008e:
		{
			KeyValuePair_2_t726430633  L_20 = Enumerator_get_Current_m2871721525((&V_4), /*hidden argument*/Enumerator_get_Current_m2871721525_MethodInfo_var);
			V_3 = L_20;
			StringBuilder_t243639308 * L_21 = V_2;
			String_t* L_22 = KeyValuePair_2_get_Key_m1739472607((&V_3), /*hidden argument*/KeyValuePair_2_get_Key_m1739472607_MethodInfo_var);
			String_t* L_23 = KeyValuePair_2_get_Value_m730091314((&V_3), /*hidden argument*/KeyValuePair_2_get_Value_m730091314_MethodInfo_var);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_24 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral1956101299, L_22, L_23, /*hidden argument*/NULL);
			NullCheck(L_21);
			StringBuilder_t243639308 * L_25 = StringBuilder_Append_m3898090075(L_21, L_24, /*hidden argument*/NULL);
			NullCheck(L_25);
			StringBuilder_Append_m3898090075(L_25, _stringLiteral32148, /*hidden argument*/NULL);
		}

IL_00bf:
		{
			bool L_26 = Enumerator_MoveNext_m2577713898((&V_4), /*hidden argument*/Enumerator_MoveNext_m2577713898_MethodInfo_var);
			if (L_26)
			{
				goto IL_008e;
			}
		}

IL_00cb:
		{
			IL2CPP_LEAVE(0xDD, FINALLY_00d0);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00d0;
	}

FINALLY_00d0:
	{ // begin finally (depth: 1)
		Enumerator_t2144973319  L_27 = V_4;
		Enumerator_t2144973319  L_28 = L_27;
		Il2CppObject * L_29 = Box(Enumerator_t2144973319_il2cpp_TypeInfo_var, &L_28);
		NullCheck((Il2CppObject *)L_29);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_29);
		IL2CPP_END_FINALLY(208)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(208)
	{
		IL2CPP_JUMP_TBL(0xDD, IL_00dd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00dd:
	{
		StringBuilder_t243639308 * L_30 = V_2;
		NullCheck(L_30);
		String_t* L_31 = StringBuilder_ToString_m350379841(L_30, /*hidden argument*/NULL);
		NullCheck(_stringLiteral32148);
		CharU5BU5D_t3324145743* L_32 = String_ToCharArray_m1208288742(_stringLiteral32148, /*hidden argument*/NULL);
		NullCheck(L_31);
		String_t* L_33 = String_TrimEnd_m3980947229(L_31, L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral2543526776, L_33, /*hidden argument*/NULL);
		V_0 = L_34;
	}

IL_00fd:
	{
		String_t* L_35 = ___name1;
		String_t* L_36 = ___reason2;
		String_t* L_37 = ___stackTrace3;
		String_t* L_38 = V_0;
		bool L_39 = ___quitProgram4;
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent__BuglyReportException_m987831418(NULL /*static, unused*/, 4, L_35, L_36, L_37, L_38, L_39, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::SetCurrentScene(System.Int32)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern const uint32_t BuglyAgent_SetCurrentScene_m1379816559_MetadataUsageId;
extern "C"  void BuglyAgent_SetCurrentScene_m1379816559 (Il2CppObject * __this /* static, unused */, int32_t ___sceneId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_SetCurrentScene_m1379816559_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_ConfigCrashReporterType_m1930542889(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_0 = ___sceneId0;
		BuglyAgent__BuglySetTag_m1900775492(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::AddKeyAndValueInScene(System.String,System.String)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern const uint32_t BuglyAgent_AddKeyAndValueInScene_m1823870950_MetadataUsageId;
extern "C"  void BuglyAgent_AddKeyAndValueInScene_m1823870950 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_AddKeyAndValueInScene_m1823870950_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_ConfigCrashReporterType_m1930542889(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_0 = ___key0;
		String_t* L_1 = ___value1;
		BuglyAgent__BuglySetKeyValue_m3618607715(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::AddExtraDataWithException(System.String,System.String)
extern "C"  void BuglyAgent_AddExtraDataWithException_m1806990949 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void BuglyAgent::LogRecord(LogSeverity,System.String)
extern Il2CppClass* LogSeverity_t591216193_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern const uint32_t BuglyAgent_LogRecord_m2405146447_MetadataUsageId;
extern "C"  void BuglyAgent_LogRecord_m2405146447 (Il2CppObject * __this /* static, unused */, int32_t ___level0, String_t* ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_LogRecord_m2405146447_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___level0;
		if ((((int32_t)L_0) >= ((int32_t)3)))
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = ___level0;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(LogSeverity_t591216193_il2cpp_TypeInfo_var, &L_2);
		NullCheck((Enum_t2862688501 *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_3);
		String_t* L_5 = ___message1;
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_DebugLog_m609003136(NULL /*static, unused*/, L_4, L_5, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
	}

IL_001e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_ConfigCrashReporterType_m1930542889(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_6 = ___level0;
		int32_t L_7 = BuglyAgent_LogSeverityToInt_m2971150991(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		String_t* L_8 = ___message1;
		BuglyAgent__BuglyLogMessage_m58191687(NULL /*static, unused*/, L_7, (String_t*)NULL, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 BuglyAgent::LogSeverityToInt(LogSeverity)
extern "C"  int32_t BuglyAgent_LogSeverityToInt_m2971150991 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 5;
		int32_t L_0 = ___logLevel0;
		V_1 = L_0;
		int32_t L_1 = V_1;
		if (L_1 == 0)
		{
			goto IL_002b;
		}
		if (L_1 == 1)
		{
			goto IL_0032;
		}
		if (L_1 == 2)
		{
			goto IL_0039;
		}
		if (L_1 == 3)
		{
			goto IL_0040;
		}
		if (L_1 == 4)
		{
			goto IL_0040;
		}
		if (L_1 == 5)
		{
			goto IL_0047;
		}
		if (L_1 == 6)
		{
			goto IL_0047;
		}
	}
	{
		goto IL_004e;
	}

IL_002b:
	{
		V_0 = 5;
		goto IL_0055;
	}

IL_0032:
	{
		V_0 = 4;
		goto IL_0055;
	}

IL_0039:
	{
		V_0 = 3;
		goto IL_0055;
	}

IL_0040:
	{
		V_0 = 2;
		goto IL_0055;
	}

IL_0047:
	{
		V_0 = 1;
		goto IL_0055;
	}

IL_004e:
	{
		V_0 = 0;
		goto IL_0055;
	}

IL_0055:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
extern "C" void DEFAULT_CALL _BuglyInit(char*, int32_t, int32_t);
// System.Void BuglyAgent::_BuglyInit(System.String,System.Boolean,System.Int32)
extern "C"  void BuglyAgent__BuglyInit_m2210360241 (Il2CppObject * __this /* static, unused */, String_t* ___appId0, bool ___debug1, int32_t ___level2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, int32_t, int32_t);

	// Marshaling of parameter '___appId0' to native representation
	char* ____appId0_marshaled = NULL;
	____appId0_marshaled = il2cpp_codegen_marshal_string(___appId0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_BuglyInit)(____appId0_marshaled, ___debug1, ___level2);

	// Marshaling cleanup of parameter '___appId0' native representation
	il2cpp_codegen_marshal_free(____appId0_marshaled);
	____appId0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _BuglySetUserId(char*);
// System.Void BuglyAgent::_BuglySetUserId(System.String)
extern "C"  void BuglyAgent__BuglySetUserId_m488354291 (Il2CppObject * __this /* static, unused */, String_t* ___userId0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___userId0' to native representation
	char* ____userId0_marshaled = NULL;
	____userId0_marshaled = il2cpp_codegen_marshal_string(___userId0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_BuglySetUserId)(____userId0_marshaled);

	// Marshaling cleanup of parameter '___userId0' native representation
	il2cpp_codegen_marshal_free(____userId0_marshaled);
	____userId0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _BuglySetTag(int32_t);
// System.Void BuglyAgent::_BuglySetTag(System.Int32)
extern "C"  void BuglyAgent__BuglySetTag_m1900775492 (Il2CppObject * __this /* static, unused */, int32_t ___tag0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_BuglySetTag)(___tag0);

}
extern "C" void DEFAULT_CALL _BuglySetKeyValue(char*, char*);
// System.Void BuglyAgent::_BuglySetKeyValue(System.String,System.String)
extern "C"  void BuglyAgent__BuglySetKeyValue_m3618607715 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___key0' to native representation
	char* ____key0_marshaled = NULL;
	____key0_marshaled = il2cpp_codegen_marshal_string(___key0);

	// Marshaling of parameter '___value1' to native representation
	char* ____value1_marshaled = NULL;
	____value1_marshaled = il2cpp_codegen_marshal_string(___value1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_BuglySetKeyValue)(____key0_marshaled, ____value1_marshaled);

	// Marshaling cleanup of parameter '___key0' native representation
	il2cpp_codegen_marshal_free(____key0_marshaled);
	____key0_marshaled = NULL;

	// Marshaling cleanup of parameter '___value1' native representation
	il2cpp_codegen_marshal_free(____value1_marshaled);
	____value1_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _BuglyReportException(int32_t, char*, char*, char*, char*, int32_t);
// System.Void BuglyAgent::_BuglyReportException(System.Int32,System.String,System.String,System.String,System.String,System.Boolean)
extern "C"  void BuglyAgent__BuglyReportException_m987831418 (Il2CppObject * __this /* static, unused */, int32_t ___type0, String_t* ___name1, String_t* ___reason2, String_t* ___stackTrace3, String_t* ___extras4, bool ___quit5, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, char*, char*, char*, char*, int32_t);

	// Marshaling of parameter '___name1' to native representation
	char* ____name1_marshaled = NULL;
	____name1_marshaled = il2cpp_codegen_marshal_string(___name1);

	// Marshaling of parameter '___reason2' to native representation
	char* ____reason2_marshaled = NULL;
	____reason2_marshaled = il2cpp_codegen_marshal_string(___reason2);

	// Marshaling of parameter '___stackTrace3' to native representation
	char* ____stackTrace3_marshaled = NULL;
	____stackTrace3_marshaled = il2cpp_codegen_marshal_string(___stackTrace3);

	// Marshaling of parameter '___extras4' to native representation
	char* ____extras4_marshaled = NULL;
	____extras4_marshaled = il2cpp_codegen_marshal_string(___extras4);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_BuglyReportException)(___type0, ____name1_marshaled, ____reason2_marshaled, ____stackTrace3_marshaled, ____extras4_marshaled, ___quit5);

	// Marshaling cleanup of parameter '___name1' native representation
	il2cpp_codegen_marshal_free(____name1_marshaled);
	____name1_marshaled = NULL;

	// Marshaling cleanup of parameter '___reason2' native representation
	il2cpp_codegen_marshal_free(____reason2_marshaled);
	____reason2_marshaled = NULL;

	// Marshaling cleanup of parameter '___stackTrace3' native representation
	il2cpp_codegen_marshal_free(____stackTrace3_marshaled);
	____stackTrace3_marshaled = NULL;

	// Marshaling cleanup of parameter '___extras4' native representation
	il2cpp_codegen_marshal_free(____extras4_marshaled);
	____extras4_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _BuglyDefaultConfig(char*, char*, char*, char*);
// System.Void BuglyAgent::_BuglyDefaultConfig(System.String,System.String,System.String,System.String)
extern "C"  void BuglyAgent__BuglyDefaultConfig_m1986590636 (Il2CppObject * __this /* static, unused */, String_t* ___channel0, String_t* ___version1, String_t* ___user2, String_t* ___deviceId3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*, char*);

	// Marshaling of parameter '___channel0' to native representation
	char* ____channel0_marshaled = NULL;
	____channel0_marshaled = il2cpp_codegen_marshal_string(___channel0);

	// Marshaling of parameter '___version1' to native representation
	char* ____version1_marshaled = NULL;
	____version1_marshaled = il2cpp_codegen_marshal_string(___version1);

	// Marshaling of parameter '___user2' to native representation
	char* ____user2_marshaled = NULL;
	____user2_marshaled = il2cpp_codegen_marshal_string(___user2);

	// Marshaling of parameter '___deviceId3' to native representation
	char* ____deviceId3_marshaled = NULL;
	____deviceId3_marshaled = il2cpp_codegen_marshal_string(___deviceId3);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_BuglyDefaultConfig)(____channel0_marshaled, ____version1_marshaled, ____user2_marshaled, ____deviceId3_marshaled);

	// Marshaling cleanup of parameter '___channel0' native representation
	il2cpp_codegen_marshal_free(____channel0_marshaled);
	____channel0_marshaled = NULL;

	// Marshaling cleanup of parameter '___version1' native representation
	il2cpp_codegen_marshal_free(____version1_marshaled);
	____version1_marshaled = NULL;

	// Marshaling cleanup of parameter '___user2' native representation
	il2cpp_codegen_marshal_free(____user2_marshaled);
	____user2_marshaled = NULL;

	// Marshaling cleanup of parameter '___deviceId3' native representation
	il2cpp_codegen_marshal_free(____deviceId3_marshaled);
	____deviceId3_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _BuglyLogMessage(int32_t, char*, char*);
// System.Void BuglyAgent::_BuglyLogMessage(System.Int32,System.String,System.String)
extern "C"  void BuglyAgent__BuglyLogMessage_m58191687 (Il2CppObject * __this /* static, unused */, int32_t ___level0, String_t* ___tag1, String_t* ___log2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, char*, char*);

	// Marshaling of parameter '___tag1' to native representation
	char* ____tag1_marshaled = NULL;
	____tag1_marshaled = il2cpp_codegen_marshal_string(___tag1);

	// Marshaling of parameter '___log2' to native representation
	char* ____log2_marshaled = NULL;
	____log2_marshaled = il2cpp_codegen_marshal_string(___log2);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_BuglyLogMessage)(___level0, ____tag1_marshaled, ____log2_marshaled);

	// Marshaling cleanup of parameter '___tag1' native representation
	il2cpp_codegen_marshal_free(____tag1_marshaled);
	____tag1_marshaled = NULL;

	// Marshaling cleanup of parameter '___log2' native representation
	il2cpp_codegen_marshal_free(____log2_marshaled);
	____log2_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _BuglyConfigCrashReporterType(int32_t);
// System.Void BuglyAgent::_BuglyConfigCrashReporterType(System.Int32)
extern "C"  void BuglyAgent__BuglyConfigCrashReporterType_m3603612760 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_BuglyConfigCrashReporterType)(___type0);

}
extern "C" void DEFAULT_CALL _BuglySetExtraConfig(char*, char*);
// System.Void BuglyAgent::_BuglySetExtraConfig(System.String,System.String)
extern "C"  void BuglyAgent__BuglySetExtraConfig_m2740188051 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___key0' to native representation
	char* ____key0_marshaled = NULL;
	____key0_marshaled = il2cpp_codegen_marshal_string(___key0);

	// Marshaling of parameter '___value1' to native representation
	char* ____value1_marshaled = NULL;
	____value1_marshaled = il2cpp_codegen_marshal_string(___value1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_BuglySetExtraConfig)(____key0_marshaled, ____value1_marshaled);

	// Marshaling cleanup of parameter '___key0' native representation
	il2cpp_codegen_marshal_free(____key0_marshaled);
	____key0_marshaled = NULL;

	// Marshaling cleanup of parameter '___value1' native representation
	il2cpp_codegen_marshal_free(____value1_marshaled);
	____value1_marshaled = NULL;

}
// System.String BuglyAgent::get_PluginVersion()
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern const uint32_t BuglyAgent_get_PluginVersion_m1696173506_MetadataUsageId;
extern "C"  String_t* BuglyAgent_get_PluginVersion_m1696173506 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_get_PluginVersion_m1696173506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		String_t* L_0 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__pluginVersion_9();
		return L_0;
	}
}
// System.Boolean BuglyAgent::get_IsInitialized()
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern const uint32_t BuglyAgent_get_IsInitialized_m1689956366_MetadataUsageId;
extern "C"  bool BuglyAgent_get_IsInitialized_m1689956366 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_get_IsInitialized_m1689956366_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		bool L_0 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__isInitialized_1();
		return L_0;
	}
}
// System.Boolean BuglyAgent::get_AutoQuitApplicationAfterReport()
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern const uint32_t BuglyAgent_get_AutoQuitApplicationAfterReport_m1259078300_MetadataUsageId;
extern "C"  bool BuglyAgent_get_AutoQuitApplicationAfterReport_m1259078300 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent_get_AutoQuitApplicationAfterReport_m1259078300_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		bool L_0 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__autoQuitApplicationAfterReport_6();
		return L_0;
	}
}
// System.Void BuglyAgent::_SetCrashReporterType(System.Int32)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern const uint32_t BuglyAgent__SetCrashReporterType_m3845894329_MetadataUsageId;
extern "C"  void BuglyAgent__SetCrashReporterType_m3845894329 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent__SetCrashReporterType_m3845894329_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->set__crashReporterType_3(L_0);
		int32_t L_1 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__crashReporterType_3();
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0011;
		}
	}

IL_0011:
	{
		return;
	}
}
// System.Void BuglyAgent::_SetCrashReporterLogLevel(System.Int32)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern const uint32_t BuglyAgent__SetCrashReporterLogLevel_m799001375_MetadataUsageId;
extern "C"  void BuglyAgent__SetCrashReporterLogLevel_m799001375 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent__SetCrashReporterLogLevel_m799001375_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___logLevel0;
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->set__crashReproterCustomizedLogLevel_4(L_0);
		return;
	}
}
// System.Void BuglyAgent::_RegisterExceptionHandler()
extern Il2CppClass* LogCallback_t2984951347_il2cpp_TypeInfo_var;
extern Il2CppClass* UnhandledExceptionEventHandler_t2544755120_il2cpp_TypeInfo_var;
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const MethodInfo* BuglyAgent__OnLogCallbackHandler_m3249997691_MethodInfo_var;
extern const MethodInfo* BuglyAgent__OnUncaughtExceptionHandler_m228339741_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1146284053;
extern const uint32_t BuglyAgent__RegisterExceptionHandler_m1437323366_MetadataUsageId;
extern "C"  void BuglyAgent__RegisterExceptionHandler_m1437323366 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent__RegisterExceptionHandler_m1437323366_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)BuglyAgent__OnLogCallbackHandler_m3249997691_MethodInfo_var);
		LogCallback_t2984951347 * L_1 = (LogCallback_t2984951347 *)il2cpp_codegen_object_new(LogCallback_t2984951347_il2cpp_TypeInfo_var);
		LogCallback__ctor_m286543475(L_1, NULL, L_0, /*hidden argument*/NULL);
		Application_add_logMessageReceived_m601763714(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		AppDomain_t3575612635 * L_2 = AppDomain_get_CurrentDomain_m3448347417(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)BuglyAgent__OnUncaughtExceptionHandler_m228339741_MethodInfo_var);
		UnhandledExceptionEventHandler_t2544755120 * L_4 = (UnhandledExceptionEventHandler_t2544755120 *)il2cpp_codegen_object_new(UnhandledExceptionEventHandler_t2544755120_il2cpp_TypeInfo_var);
		UnhandledExceptionEventHandler__ctor_m2560140041(L_4, NULL, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		AppDomain_add_UnhandledException_m1729490677(L_2, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->set__isInitialized_1((bool)1);
		ObjectU5BU5D_t1108656482* L_5 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_6 = Application_get_unityVersion_m3443350436(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_6);
		BuglyAgent_DebugLog_m609003136(NULL /*static, unused*/, (String_t*)NULL, _stringLiteral1146284053, L_5, /*hidden argument*/NULL);
		goto IL_0051;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_004b;
		throw e;
	}

CATCH_004b:
	{ // begin catch(System.Object)
		goto IL_0051;
	} // end catch (depth: 1)

IL_0051:
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_SetUnityVersion_m1918232878(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::_UnregisterExceptionHandler()
extern Il2CppClass* LogCallback_t2984951347_il2cpp_TypeInfo_var;
extern Il2CppClass* UnhandledExceptionEventHandler_t2544755120_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const MethodInfo* BuglyAgent__OnLogCallbackHandler_m3249997691_MethodInfo_var;
extern const MethodInfo* BuglyAgent__OnUncaughtExceptionHandler_m228339741_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1116705806;
extern const uint32_t BuglyAgent__UnregisterExceptionHandler_m963802111_MetadataUsageId;
extern "C"  void BuglyAgent__UnregisterExceptionHandler_m963802111 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent__UnregisterExceptionHandler_m963802111_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)BuglyAgent__OnLogCallbackHandler_m3249997691_MethodInfo_var);
		LogCallback_t2984951347 * L_1 = (LogCallback_t2984951347 *)il2cpp_codegen_object_new(LogCallback_t2984951347_il2cpp_TypeInfo_var);
		LogCallback__ctor_m286543475(L_1, NULL, L_0, /*hidden argument*/NULL);
		Application_remove_logMessageReceived_m293388825(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		AppDomain_t3575612635 * L_2 = AppDomain_get_CurrentDomain_m3448347417(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)BuglyAgent__OnUncaughtExceptionHandler_m228339741_MethodInfo_var);
		UnhandledExceptionEventHandler_t2544755120 * L_4 = (UnhandledExceptionEventHandler_t2544755120 *)il2cpp_codegen_object_new(UnhandledExceptionEventHandler_t2544755120_il2cpp_TypeInfo_var);
		UnhandledExceptionEventHandler__ctor_m2560140041(L_4, NULL, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		AppDomain_remove_UnhandledException_m1873750054(L_2, L_4, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_5 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_6 = Application_get_unityVersion_m3443350436(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_6);
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_DebugLog_m609003136(NULL /*static, unused*/, (String_t*)NULL, _stringLiteral1116705806, L_5, /*hidden argument*/NULL);
		goto IL_004b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0045;
		throw e;
	}

CATCH_0045:
	{ // begin catch(System.Object)
		goto IL_004b;
	} // end catch (depth: 1)

IL_004b:
	{
		return;
	}
}
// System.Void BuglyAgent::_OnLogCallbackHandler(System.String,System.String,UnityEngine.LogType)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1716571700;
extern const uint32_t BuglyAgent__OnLogCallbackHandler_m3249997691_MetadataUsageId;
extern "C"  void BuglyAgent__OnLogCallbackHandler_m3249997691 (Il2CppObject * __this /* static, unused */, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent__OnLogCallbackHandler_m3249997691_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		LogCallbackDelegate_t45039171 * L_0 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__LogCallbackEventHandler_12();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		LogCallbackDelegate_t45039171 * L_1 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__LogCallbackEventHandler_12();
		String_t* L_2 = ___condition0;
		String_t* L_3 = ___stackTrace1;
		int32_t L_4 = ___type2;
		NullCheck(L_1);
		LogCallbackDelegate_Invoke_m2974759347(L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		bool L_5 = BuglyAgent_get_IsInitialized_m1689956366(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0022;
		}
	}
	{
		return;
	}

IL_0022:
	{
		String_t* L_6 = ___condition0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_003e;
		}
	}
	{
		String_t* L_8 = ___condition0;
		NullCheck(L_8);
		bool L_9 = String_Contains_m3032019141(L_8, _stringLiteral1716571700, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003e;
		}
	}
	{
		return;
	}

IL_003e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		bool L_10 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__uncaughtAutoReportOnce_11();
		if (!L_10)
		{
			goto IL_0049;
		}
	}
	{
		return;
	}

IL_0049:
	{
		V_0 = 0;
		int32_t L_11 = ___type2;
		V_1 = L_11;
		int32_t L_12 = V_1;
		if (L_12 == 0)
		{
			goto IL_0073;
		}
		if (L_12 == 1)
		{
			goto IL_007a;
		}
		if (L_12 == 2)
		{
			goto IL_0081;
		}
		if (L_12 == 3)
		{
			goto IL_0088;
		}
		if (L_12 == 4)
		{
			goto IL_006c;
		}
	}
	{
		goto IL_008f;
	}

IL_006c:
	{
		V_0 = 6;
		goto IL_0094;
	}

IL_0073:
	{
		V_0 = 5;
		goto IL_0094;
	}

IL_007a:
	{
		V_0 = 4;
		goto IL_0094;
	}

IL_0081:
	{
		V_0 = 3;
		goto IL_0094;
	}

IL_0088:
	{
		V_0 = 1;
		goto IL_0094;
	}

IL_008f:
	{
		goto IL_0094;
	}

IL_0094:
	{
		int32_t L_13 = V_0;
		if (L_13)
		{
			goto IL_009b;
		}
	}
	{
		return;
	}

IL_009b:
	{
		int32_t L_14 = V_0;
		String_t* L_15 = ___condition0;
		String_t* L_16 = ___stackTrace1;
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent__HandleException_m1838932569(NULL /*static, unused*/, L_14, (String_t*)NULL, L_15, L_16, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::_OnUncaughtExceptionHandler(System.Object,System.UnhandledExceptionEventArgs)
extern const Il2CppType* Exception_t3991598821_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral717819172;
extern const uint32_t BuglyAgent__OnUncaughtExceptionHandler_m228339741_MetadataUsageId;
extern "C"  void BuglyAgent__OnUncaughtExceptionHandler_m228339741 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___sender0, UnhandledExceptionEventArgs_t3134093121 * ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent__OnUncaughtExceptionHandler_m228339741_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		UnhandledExceptionEventArgs_t3134093121 * L_0 = ___args1;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		UnhandledExceptionEventArgs_t3134093121 * L_1 = ___args1;
		NullCheck(L_1);
		Il2CppObject * L_2 = UnhandledExceptionEventArgs_get_ExceptionObject_m3487900308(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0012;
		}
	}

IL_0011:
	{
		return;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			UnhandledExceptionEventArgs_t3134093121 * L_3 = ___args1;
			NullCheck(L_3);
			Il2CppObject * L_4 = UnhandledExceptionEventArgs_get_ExceptionObject_m3487900308(L_3, /*hidden argument*/NULL);
			NullCheck(L_4);
			Type_t * L_5 = Object_GetType_m2022236990(L_4, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_6 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Exception_t3991598821_0_0_0_var), /*hidden argument*/NULL);
			if ((((Il2CppObject*)(Type_t *)L_5) == ((Il2CppObject*)(Type_t *)L_6)))
			{
				goto IL_0031;
			}
		}

IL_002c:
		{
			goto IL_007d;
		}

IL_0031:
		{
			goto IL_0055;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0036;
		throw e;
	}

CATCH_0036:
	{ // begin catch(System.Object)
		{
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
			bool L_7 = Debug_get_isDebugBuild_m351497798(NULL /*static, unused*/, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_004b;
			}
		}

IL_0041:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
			Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral717819172, /*hidden argument*/NULL);
		}

IL_004b:
		{
			goto IL_007d;
		}

IL_0050:
		{
			; // IL_0050: leave IL_0055
		}
	} // end catch (depth: 1)

IL_0055:
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		bool L_8 = BuglyAgent_get_IsInitialized_m1689956366(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		bool L_9 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__uncaughtAutoReportOnce_11();
		if (!L_9)
		{
			goto IL_006b;
		}
	}
	{
		return;
	}

IL_006b:
	{
		UnhandledExceptionEventArgs_t3134093121 * L_10 = ___args1;
		NullCheck(L_10);
		Il2CppObject * L_11 = UnhandledExceptionEventArgs_get_ExceptionObject_m3487900308(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent__HandleException_m1464489584(NULL /*static, unused*/, ((Exception_t3991598821 *)CastclassClass(L_11, Exception_t3991598821_il2cpp_TypeInfo_var)), (String_t*)NULL, (bool)1, /*hidden argument*/NULL);
	}

IL_007d:
	{
		return;
	}
}
// System.Void BuglyAgent::_HandleException(System.Exception,System.String,System.Boolean)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppClass* StackTrace_t1047871261_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral579756283;
extern Il2CppCodeGenString* _stringLiteral3279899969;
extern Il2CppCodeGenString* _stringLiteral993055;
extern Il2CppCodeGenString* _stringLiteral1032;
extern Il2CppCodeGenString* _stringLiteral3279482895;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral1303;
extern Il2CppCodeGenString* _stringLiteral4010126410;
extern Il2CppCodeGenString* _stringLiteral92;
extern Il2CppCodeGenString* _stringLiteral47;
extern Il2CppCodeGenString* _stringLiteral3897899517;
extern Il2CppCodeGenString* _stringLiteral3590192396;
extern Il2CppCodeGenString* _stringLiteral2236754233;
extern const uint32_t BuglyAgent__HandleException_m1464489584_MetadataUsageId;
extern "C"  void BuglyAgent__HandleException_m1464489584 (Il2CppObject * __this /* static, unused */, Exception_t3991598821 * ___e0, String_t* ___message1, bool ___uncaught2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent__HandleException_m1464489584_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	StringBuilder_t243639308 * V_2 = NULL;
	StackTrace_t1047871261 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	StackFrame_t1034942277 * V_6 = NULL;
	ParameterInfoU5BU5D_t2015293532* V_7 = NULL;
	int32_t V_8 = 0;
	ParameterInfo_t2235474049 * V_9 = NULL;
	int32_t V_10 = 0;
	String_t* V_11 = NULL;
	int32_t V_12 = 0;
	{
		Exception_t3991598821 * L_0 = ___e0;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		bool L_1 = BuglyAgent_get_IsInitialized_m1689956366(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Exception_t3991598821 * L_2 = ___e0;
		NullCheck(L_2);
		Type_t * L_3 = Exception_GetType_m913902486(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_3);
		V_0 = L_4;
		Exception_t3991598821 * L_5 = ___e0;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(21 /* System.String System.Exception::get_Message() */, L_5);
		V_1 = L_6;
		String_t* L_7 = ___message1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0042;
		}
	}
	{
		String_t* L_9 = V_1;
		String_t* L_10 = Environment_get_NewLine_m1034655108(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_11 = ___message1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Format_m3928391288(NULL /*static, unused*/, _stringLiteral579756283, L_9, L_10, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
	}

IL_0042:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		StringBuilder_t243639308 * L_14 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1143895062(L_14, L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		Exception_t3991598821 * L_15 = ___e0;
		StackTrace_t1047871261 * L_16 = (StackTrace_t1047871261 *)il2cpp_codegen_object_new(StackTrace_t1047871261_il2cpp_TypeInfo_var);
		StackTrace__ctor_m109564407(L_16, L_15, (bool)1, /*hidden argument*/NULL);
		V_3 = L_16;
		StackTrace_t1047871261 * L_17 = V_3;
		NullCheck(L_17);
		int32_t L_18 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackTrace::get_FrameCount() */, L_17);
		V_4 = L_18;
		V_5 = 0;
		goto IL_01e5;
	}

IL_0065:
	{
		StackTrace_t1047871261 * L_19 = V_3;
		int32_t L_20 = V_5;
		NullCheck(L_19);
		StackFrame_t1034942277 * L_21 = VirtFuncInvoker1< StackFrame_t1034942277 *, int32_t >::Invoke(5 /* System.Diagnostics.StackFrame System.Diagnostics.StackTrace::GetFrame(System.Int32) */, L_19, L_20);
		V_6 = L_21;
		StringBuilder_t243639308 * L_22 = V_2;
		StackFrame_t1034942277 * L_23 = V_6;
		NullCheck(L_23);
		MethodBase_t318515428 * L_24 = VirtFuncInvoker0< MethodBase_t318515428 * >::Invoke(7 /* System.Reflection.MethodBase System.Diagnostics.StackFrame::GetMethod() */, L_23);
		NullCheck(L_24);
		Type_t * L_25 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_24);
		NullCheck(L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_25);
		StackFrame_t1034942277 * L_27 = V_6;
		NullCheck(L_27);
		MethodBase_t318515428 * L_28 = VirtFuncInvoker0< MethodBase_t318515428 * >::Invoke(7 /* System.Reflection.MethodBase System.Diagnostics.StackFrame::GetMethod() */, L_27);
		NullCheck(L_28);
		String_t* L_29 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_28);
		NullCheck(L_22);
		StringBuilder_AppendFormat_m3487355136(L_22, _stringLiteral3279899969, L_26, L_29, /*hidden argument*/NULL);
		StackFrame_t1034942277 * L_30 = V_6;
		NullCheck(L_30);
		MethodBase_t318515428 * L_31 = VirtFuncInvoker0< MethodBase_t318515428 * >::Invoke(7 /* System.Reflection.MethodBase System.Diagnostics.StackFrame::GetMethod() */, L_30);
		NullCheck(L_31);
		ParameterInfoU5BU5D_t2015293532* L_32 = VirtFuncInvoker0< ParameterInfoU5BU5D_t2015293532* >::Invoke(15 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_31);
		V_7 = L_32;
		ParameterInfoU5BU5D_t2015293532* L_33 = V_7;
		if (!L_33)
		{
			goto IL_00b6;
		}
	}
	{
		ParameterInfoU5BU5D_t2015293532* L_34 = V_7;
		NullCheck(L_34);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_34)->max_length)))))
		{
			goto IL_00c7;
		}
	}

IL_00b6:
	{
		StringBuilder_t243639308 * L_35 = V_2;
		NullCheck(L_35);
		StringBuilder_Append_m3898090075(L_35, _stringLiteral993055, /*hidden argument*/NULL);
		goto IL_013f;
	}

IL_00c7:
	{
		StringBuilder_t243639308 * L_36 = V_2;
		NullCheck(L_36);
		StringBuilder_Append_m3898090075(L_36, _stringLiteral1032, /*hidden argument*/NULL);
		ParameterInfoU5BU5D_t2015293532* L_37 = V_7;
		NullCheck(L_37);
		V_8 = (((int32_t)((int32_t)(((Il2CppArray *)L_37)->max_length))));
		V_9 = (ParameterInfo_t2235474049 *)NULL;
		V_10 = 0;
		goto IL_0127;
	}

IL_00e4:
	{
		ParameterInfoU5BU5D_t2015293532* L_38 = V_7;
		int32_t L_39 = V_10;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		int32_t L_40 = L_39;
		ParameterInfo_t2235474049 * L_41 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		V_9 = L_41;
		StringBuilder_t243639308 * L_42 = V_2;
		ParameterInfo_t2235474049 * L_43 = V_9;
		NullCheck(L_43);
		Type_t * L_44 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_43);
		NullCheck(L_44);
		String_t* L_45 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_44);
		ParameterInfo_t2235474049 * L_46 = V_9;
		NullCheck(L_46);
		String_t* L_47 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.ParameterInfo::get_Name() */, L_46);
		NullCheck(L_42);
		StringBuilder_AppendFormat_m3487355136(L_42, _stringLiteral3279482895, L_45, L_47, /*hidden argument*/NULL);
		int32_t L_48 = V_10;
		int32_t L_49 = V_8;
		if ((((int32_t)L_48) == ((int32_t)((int32_t)((int32_t)L_49-(int32_t)1)))))
		{
			goto IL_0121;
		}
	}
	{
		StringBuilder_t243639308 * L_50 = V_2;
		NullCheck(L_50);
		StringBuilder_Append_m3898090075(L_50, _stringLiteral1396, /*hidden argument*/NULL);
	}

IL_0121:
	{
		int32_t L_51 = V_10;
		V_10 = ((int32_t)((int32_t)L_51+(int32_t)1));
	}

IL_0127:
	{
		int32_t L_52 = V_10;
		int32_t L_53 = V_8;
		if ((((int32_t)L_52) < ((int32_t)L_53)))
		{
			goto IL_00e4;
		}
	}
	{
		V_9 = (ParameterInfo_t2235474049 *)NULL;
		StringBuilder_t243639308 * L_54 = V_2;
		NullCheck(L_54);
		StringBuilder_Append_m3898090075(L_54, _stringLiteral1303, /*hidden argument*/NULL);
	}

IL_013f:
	{
		StackFrame_t1034942277 * L_55 = V_6;
		NullCheck(L_55);
		String_t* L_56 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Diagnostics.StackFrame::GetFileName() */, L_55);
		V_11 = L_56;
		String_t* L_57 = V_11;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_58 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		if (L_58)
		{
			goto IL_01d8;
		}
	}
	{
		String_t* L_59 = V_11;
		NullCheck(L_59);
		String_t* L_60 = String_ToLower_m2421900555(L_59, /*hidden argument*/NULL);
		NullCheck(L_60);
		bool L_61 = String_Equals_m3541721061(L_60, _stringLiteral4010126410, /*hidden argument*/NULL);
		if (L_61)
		{
			goto IL_01d8;
		}
	}
	{
		String_t* L_62 = V_11;
		NullCheck(L_62);
		String_t* L_63 = String_Replace_m2915759397(L_62, _stringLiteral92, _stringLiteral47, /*hidden argument*/NULL);
		V_11 = L_63;
		String_t* L_64 = V_11;
		NullCheck(L_64);
		String_t* L_65 = String_ToLower_m2421900555(L_64, /*hidden argument*/NULL);
		NullCheck(L_65);
		int32_t L_66 = String_IndexOf_m1476794331(L_65, _stringLiteral3897899517, /*hidden argument*/NULL);
		V_12 = L_66;
		int32_t L_67 = V_12;
		if ((((int32_t)L_67) >= ((int32_t)0)))
		{
			goto IL_01ab;
		}
	}
	{
		String_t* L_68 = V_11;
		NullCheck(L_68);
		String_t* L_69 = String_ToLower_m2421900555(L_68, /*hidden argument*/NULL);
		NullCheck(L_69);
		int32_t L_70 = String_IndexOf_m1476794331(L_69, _stringLiteral3590192396, /*hidden argument*/NULL);
		V_12 = L_70;
	}

IL_01ab:
	{
		int32_t L_71 = V_12;
		if ((((int32_t)L_71) <= ((int32_t)0)))
		{
			goto IL_01be;
		}
	}
	{
		String_t* L_72 = V_11;
		int32_t L_73 = V_12;
		NullCheck(L_72);
		String_t* L_74 = String_Substring_m2809233063(L_72, L_73, /*hidden argument*/NULL);
		V_11 = L_74;
	}

IL_01be:
	{
		StringBuilder_t243639308 * L_75 = V_2;
		String_t* L_76 = V_11;
		StackFrame_t1034942277 * L_77 = V_6;
		NullCheck(L_77);
		int32_t L_78 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackFrame::GetFileLineNumber() */, L_77);
		int32_t L_79 = L_78;
		Il2CppObject * L_80 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_79);
		NullCheck(L_75);
		StringBuilder_AppendFormat_m3487355136(L_75, _stringLiteral2236754233, L_76, L_80, /*hidden argument*/NULL);
	}

IL_01d8:
	{
		StringBuilder_t243639308 * L_81 = V_2;
		NullCheck(L_81);
		StringBuilder_AppendLine_m568622107(L_81, /*hidden argument*/NULL);
		int32_t L_82 = V_5;
		V_5 = ((int32_t)((int32_t)L_82+(int32_t)1));
	}

IL_01e5:
	{
		int32_t L_83 = V_5;
		int32_t L_84 = V_4;
		if ((((int32_t)L_83) < ((int32_t)L_84)))
		{
			goto IL_0065;
		}
	}
	{
		bool L_85 = ___uncaught2;
		String_t* L_86 = V_0;
		String_t* L_87 = V_1;
		StringBuilder_t243639308 * L_88 = V_2;
		NullCheck(L_88);
		String_t* L_89 = StringBuilder_ToString_m350379841(L_88, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent__reportException_m40291648(NULL /*static, unused*/, L_85, L_86, L_87, L_89, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::_reportException(System.Boolean,System.String,System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StackTraceUtility_t4217621253_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t3324145743_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral67081517;
extern Il2CppCodeGenString* _stringLiteral2765235183;
extern Il2CppCodeGenString* _stringLiteral3981551357;
extern Il2CppCodeGenString* _stringLiteral64540385;
extern Il2CppCodeGenString* _stringLiteral1362487252;
extern Il2CppCodeGenString* _stringLiteral41563;
extern Il2CppCodeGenString* _stringLiteral3897899517;
extern Il2CppCodeGenString* _stringLiteral3573772250;
extern Il2CppCodeGenString* _stringLiteral58;
extern Il2CppCodeGenString* _stringLiteral46;
extern Il2CppCodeGenString* _stringLiteral119816;
extern Il2CppCodeGenString* _stringLiteral836621412;
extern Il2CppCodeGenString* _stringLiteral3050441450;
extern const uint32_t BuglyAgent__reportException_m40291648_MetadataUsageId;
extern "C"  void BuglyAgent__reportException_m40291648 (Il2CppObject * __this /* static, unused */, bool ___uncaught0, String_t* ___name1, String_t* ___reason2, String_t* ___stackTrace3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent__reportException_m40291648_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t4054002952* V_0 = NULL;
	StringBuilder_t243639308 * V_1 = NULL;
	String_t* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B32_0 = 0;
	int32_t G_B35_0 = 0;
	String_t* G_B37_0 = NULL;
	String_t* G_B37_1 = NULL;
	String_t* G_B37_2 = NULL;
	int32_t G_B37_3 = 0;
	String_t* G_B36_0 = NULL;
	String_t* G_B36_1 = NULL;
	String_t* G_B36_2 = NULL;
	int32_t G_B36_3 = 0;
	int32_t G_B38_0 = 0;
	String_t* G_B38_1 = NULL;
	String_t* G_B38_2 = NULL;
	String_t* G_B38_3 = NULL;
	int32_t G_B38_4 = 0;
	{
		String_t* L_0 = ___name1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		String_t* L_2 = ___stackTrace3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t4217621253_il2cpp_TypeInfo_var);
		String_t* L_4 = StackTraceUtility_ExtractStackTrace_m235366505(NULL /*static, unused*/, /*hidden argument*/NULL);
		___stackTrace3 = L_4;
	}

IL_001e:
	{
		String_t* L_5 = ___stackTrace3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0035;
		}
	}
	{
		___stackTrace3 = _stringLiteral67081517;
		goto IL_01a2;
	}

IL_0035:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_7 = ___stackTrace3;
			CharU5BU5D_t3324145743* L_8 = ((CharU5BU5D_t3324145743*)SZArrayNew(CharU5BU5D_t3324145743_il2cpp_TypeInfo_var, (uint32_t)1));
			NullCheck(L_8);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
			(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)10));
			NullCheck(L_7);
			StringU5BU5D_t4054002952* L_9 = String_Split_m290179486(L_7, L_8, /*hidden argument*/NULL);
			V_0 = L_9;
			StringU5BU5D_t4054002952* L_10 = V_0;
			if (!L_10)
			{
				goto IL_017e;
			}
		}

IL_004d:
		{
			StringU5BU5D_t4054002952* L_11 = V_0;
			NullCheck(L_11);
			if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length))))) <= ((int32_t)0)))
			{
				goto IL_017e;
			}
		}

IL_0056:
		{
			StringBuilder_t243639308 * L_12 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
			StringBuilder__ctor_m135953004(L_12, /*hidden argument*/NULL);
			V_1 = L_12;
			V_2 = (String_t*)NULL;
			StringU5BU5D_t4054002952* L_13 = V_0;
			NullCheck(L_13);
			V_3 = (((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length))));
			V_4 = 0;
			goto IL_016e;
		}

IL_006a:
		{
			StringU5BU5D_t4054002952* L_14 = V_0;
			int32_t L_15 = V_4;
			NullCheck(L_14);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
			int32_t L_16 = L_15;
			String_t* L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
			V_2 = L_17;
			String_t* L_18 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_19 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
			if (L_19)
			{
				goto IL_008a;
			}
		}

IL_007a:
		{
			String_t* L_20 = V_2;
			NullCheck(L_20);
			String_t* L_21 = String_Trim_m1030489823(L_20, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_22 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
			if (!L_22)
			{
				goto IL_008f;
			}
		}

IL_008a:
		{
			goto IL_0168;
		}

IL_008f:
		{
			String_t* L_23 = V_2;
			NullCheck(L_23);
			String_t* L_24 = String_Trim_m1030489823(L_23, /*hidden argument*/NULL);
			V_2 = L_24;
			String_t* L_25 = V_2;
			NullCheck(L_25);
			bool L_26 = String_StartsWith_m1500793453(L_25, _stringLiteral2765235183, /*hidden argument*/NULL);
			if (L_26)
			{
				goto IL_00b6;
			}
		}

IL_00a6:
		{
			String_t* L_27 = V_2;
			NullCheck(L_27);
			bool L_28 = String_StartsWith_m1500793453(L_27, _stringLiteral3981551357, /*hidden argument*/NULL);
			if (!L_28)
			{
				goto IL_00bb;
			}
		}

IL_00b6:
		{
			goto IL_0168;
		}

IL_00bb:
		{
			String_t* L_29 = V_2;
			NullCheck(L_29);
			bool L_30 = String_StartsWith_m1500793453(L_29, _stringLiteral64540385, /*hidden argument*/NULL);
			if (!L_30)
			{
				goto IL_00d0;
			}
		}

IL_00cb:
		{
			goto IL_0168;
		}

IL_00d0:
		{
			String_t* L_31 = V_2;
			NullCheck(L_31);
			bool L_32 = String_Contains_m3032019141(L_31, _stringLiteral1362487252, /*hidden argument*/NULL);
			if (!L_32)
			{
				goto IL_00e5;
			}
		}

IL_00e0:
		{
			goto IL_0168;
		}

IL_00e5:
		{
			String_t* L_33 = V_2;
			NullCheck(L_33);
			String_t* L_34 = String_ToLower_m2421900555(L_33, /*hidden argument*/NULL);
			NullCheck(L_34);
			int32_t L_35 = String_IndexOf_m1476794331(L_34, _stringLiteral41563, /*hidden argument*/NULL);
			V_5 = L_35;
			String_t* L_36 = V_2;
			NullCheck(L_36);
			String_t* L_37 = String_ToLower_m2421900555(L_36, /*hidden argument*/NULL);
			NullCheck(L_37);
			int32_t L_38 = String_IndexOf_m1476794331(L_37, _stringLiteral3897899517, /*hidden argument*/NULL);
			V_6 = L_38;
			int32_t L_39 = V_5;
			if ((((int32_t)L_39) <= ((int32_t)0)))
			{
				goto IL_014a;
			}
		}

IL_0111:
		{
			int32_t L_40 = V_6;
			if ((((int32_t)L_40) <= ((int32_t)0)))
			{
				goto IL_014a;
			}
		}

IL_0119:
		{
			StringBuilder_t243639308 * L_41 = V_1;
			String_t* L_42 = V_2;
			int32_t L_43 = V_5;
			NullCheck(L_42);
			String_t* L_44 = String_Substring_m675079568(L_42, 0, L_43, /*hidden argument*/NULL);
			NullCheck(L_44);
			String_t* L_45 = String_Replace_m2915759397(L_44, _stringLiteral58, _stringLiteral46, /*hidden argument*/NULL);
			String_t* L_46 = V_2;
			int32_t L_47 = V_6;
			NullCheck(L_46);
			String_t* L_48 = String_Substring_m2809233063(L_46, L_47, /*hidden argument*/NULL);
			NullCheck(L_41);
			StringBuilder_AppendFormat_m3487355136(L_41, _stringLiteral3573772250, L_45, L_48, /*hidden argument*/NULL);
			goto IL_0161;
		}

IL_014a:
		{
			StringBuilder_t243639308 * L_49 = V_1;
			String_t* L_50 = V_2;
			NullCheck(L_50);
			String_t* L_51 = String_Replace_m2915759397(L_50, _stringLiteral58, _stringLiteral46, /*hidden argument*/NULL);
			NullCheck(L_49);
			StringBuilder_Append_m3898090075(L_49, L_51, /*hidden argument*/NULL);
		}

IL_0161:
		{
			StringBuilder_t243639308 * L_52 = V_1;
			NullCheck(L_52);
			StringBuilder_AppendLine_m568622107(L_52, /*hidden argument*/NULL);
		}

IL_0168:
		{
			int32_t L_53 = V_4;
			V_4 = ((int32_t)((int32_t)L_53+(int32_t)1));
		}

IL_016e:
		{
			int32_t L_54 = V_4;
			int32_t L_55 = V_3;
			if ((((int32_t)L_54) < ((int32_t)L_55)))
			{
				goto IL_006a;
			}
		}

IL_0176:
		{
			StringBuilder_t243639308 * L_56 = V_1;
			NullCheck(L_56);
			String_t* L_57 = StringBuilder_ToString_m350379841(L_56, /*hidden argument*/NULL);
			___stackTrace3 = L_57;
		}

IL_017e:
		{
			goto IL_01a2;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0183;
		throw e;
	}

CATCH_0183:
	{ // begin catch(System.Object)
		ObjectU5BU5D_t1108656482* L_58 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, 0);
		ArrayElementTypeCheck (L_58, _stringLiteral836621412);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral836621412);
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_PrintLog_m1023618409(NULL /*static, unused*/, 3, _stringLiteral119816, L_58, /*hidden argument*/NULL);
		goto IL_01a2;
	} // end catch (depth: 1)

IL_01a2:
	{
		ObjectU5BU5D_t1108656482* L_59 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)3));
		String_t* L_60 = ___name1;
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, 0);
		ArrayElementTypeCheck (L_59, L_60);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_60);
		ObjectU5BU5D_t1108656482* L_61 = L_59;
		String_t* L_62 = ___reason2;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, 1);
		ArrayElementTypeCheck (L_61, L_62);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_62);
		ObjectU5BU5D_t1108656482* L_63 = L_61;
		String_t* L_64 = ___stackTrace3;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, 2);
		ArrayElementTypeCheck (L_63, L_64);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_64);
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_PrintLog_m1023618409(NULL /*static, unused*/, 5, _stringLiteral3050441450, L_63, /*hidden argument*/NULL);
		bool L_65 = ___uncaught0;
		if (!L_65)
		{
			goto IL_01cc;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		bool L_66 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__autoQuitApplicationAfterReport_6();
		G_B32_0 = ((int32_t)(L_66));
		goto IL_01cd;
	}

IL_01cc:
	{
		G_B32_0 = 0;
	}

IL_01cd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->set__uncaughtAutoReportOnce_11((bool)G_B32_0);
		bool L_67 = ___uncaught0;
		if (!L_67)
		{
			goto IL_01e2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		int32_t L_68 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get_EXCEPTION_TYPE_UNCAUGHT_7();
		G_B35_0 = L_68;
		goto IL_01e7;
	}

IL_01e2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		int32_t L_69 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get_EXCEPTION_TYPE_CAUGHT_8();
		G_B35_0 = L_69;
	}

IL_01e7:
	{
		String_t* L_70 = ___name1;
		String_t* L_71 = ___reason2;
		String_t* L_72 = ___stackTrace3;
		bool L_73 = ___uncaught0;
		G_B36_0 = L_72;
		G_B36_1 = L_71;
		G_B36_2 = L_70;
		G_B36_3 = G_B35_0;
		if (!L_73)
		{
			G_B37_0 = L_72;
			G_B37_1 = L_71;
			G_B37_2 = L_70;
			G_B37_3 = G_B35_0;
			goto IL_01f7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		bool L_74 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__autoQuitApplicationAfterReport_6();
		G_B38_0 = ((int32_t)(L_74));
		G_B38_1 = G_B36_0;
		G_B38_2 = G_B36_1;
		G_B38_3 = G_B36_2;
		G_B38_4 = G_B36_3;
		goto IL_01f8;
	}

IL_01f7:
	{
		G_B38_0 = 0;
		G_B38_1 = G_B37_0;
		G_B38_2 = G_B37_1;
		G_B38_3 = G_B37_2;
		G_B38_4 = G_B37_3;
	}

IL_01f8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_ReportException_m769667476(NULL /*static, unused*/, G_B38_4, G_B38_3, G_B38_2, G_B38_1, (bool)G_B38_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent::_HandleException(LogSeverity,System.String,System.String,System.String,System.Boolean)
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* LogSeverity_t591216193_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Regex_t2161232213_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3926246310;
extern Il2CppCodeGenString* _stringLiteral3159459045;
extern Il2CppCodeGenString* _stringLiteral3485593647;
extern Il2CppCodeGenString* _stringLiteral2071761122;
extern Il2CppCodeGenString* _stringLiteral329552226;
extern Il2CppCodeGenString* _stringLiteral1203236063;
extern Il2CppCodeGenString* _stringLiteral3819939784;
extern Il2CppCodeGenString* _stringLiteral3520912712;
extern Il2CppCodeGenString* _stringLiteral275119994;
extern Il2CppCodeGenString* _stringLiteral2114919808;
extern Il2CppCodeGenString* _stringLiteral46;
extern Il2CppCodeGenString* _stringLiteral1050157;
extern Il2CppCodeGenString* _stringLiteral10;
extern Il2CppCodeGenString* _stringLiteral1522445698;
extern Il2CppCodeGenString* _stringLiteral1327183824;
extern Il2CppCodeGenString* _stringLiteral3278827493;
extern Il2CppCodeGenString* _stringLiteral795620556;
extern Il2CppCodeGenString* _stringLiteral1477898;
extern Il2CppCodeGenString* _stringLiteral3968195238;
extern Il2CppCodeGenString* _stringLiteral1083;
extern Il2CppCodeGenString* _stringLiteral31153;
extern Il2CppCodeGenString* _stringLiteral4068061331;
extern const uint32_t BuglyAgent__HandleException_m1838932569_MetadataUsageId;
extern "C"  void BuglyAgent__HandleException_m1838932569 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, String_t* ___name1, String_t* ___message2, String_t* ___stackTrace3, bool ___uncaught4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyAgent__HandleException_m1838932569_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	Match_t2156507859 * V_2 = NULL;
	Match_t2156507859 * V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		bool L_0 = BuglyAgent_get_IsInitialized_m1689956366(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_DebugLog_m609003136(NULL /*static, unused*/, (String_t*)NULL, _stringLiteral3926246310, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return;
	}

IL_001c:
	{
		int32_t L_1 = ___logLevel0;
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		return;
	}

IL_0023:
	{
		bool L_2 = ___uncaught4;
		if (!L_2)
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_3 = ___logLevel0;
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		int32_t L_4 = ((BuglyAgent_t2905516196_StaticFields*)BuglyAgent_t2905516196_il2cpp_TypeInfo_var->static_fields)->get__autoReportLogLevel_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0055;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_5 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		int32_t L_6 = ___logLevel0;
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(LogSeverity_t591216193_il2cpp_TypeInfo_var, &L_7);
		NullCheck((Enum_t2862688501 *)L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_8);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_9);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_9);
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_DebugLog_m609003136(NULL /*static, unused*/, (String_t*)NULL, _stringLiteral3159459045, L_5, /*hidden argument*/NULL);
		return;
	}

IL_0055:
	{
		V_0 = (String_t*)NULL;
		V_1 = (String_t*)NULL;
		String_t* L_10 = ___message2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_028c;
		}
	}

IL_0064:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_12 = ___logLevel0;
			if ((!(((uint32_t)L_12) == ((uint32_t)6))))
			{
				goto IL_00d4;
			}
		}

IL_006b:
		{
			String_t* L_13 = ___message2;
			NullCheck(L_13);
			bool L_14 = String_Contains_m3032019141(L_13, _stringLiteral3485593647, /*hidden argument*/NULL);
			if (!L_14)
			{
				goto IL_00d4;
			}
		}

IL_007b:
		{
			Regex_t2161232213 * L_15 = (Regex_t2161232213 *)il2cpp_codegen_object_new(Regex_t2161232213_il2cpp_TypeInfo_var);
			Regex__ctor_m2068483208(L_15, _stringLiteral2071761122, ((int32_t)16), /*hidden argument*/NULL);
			String_t* L_16 = ___message2;
			NullCheck(L_15);
			Match_t2156507859 * L_17 = Regex_Match_m2003175236(L_15, L_16, /*hidden argument*/NULL);
			V_2 = L_17;
			Match_t2156507859 * L_18 = V_2;
			NullCheck(L_18);
			bool L_19 = Group_get_Success_m139536080(L_18, /*hidden argument*/NULL);
			if (!L_19)
			{
				goto IL_00cf;
			}
		}

IL_0099:
		{
			Match_t2156507859 * L_20 = V_2;
			NullCheck(L_20);
			GroupCollection_t982584267 * L_21 = VirtFuncInvoker0< GroupCollection_t982584267 * >::Invoke(4 /* System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::get_Groups() */, L_20);
			NullCheck(L_21);
			Group_t2151468941 * L_22 = GroupCollection_get_Item_m4186628929(L_21, _stringLiteral329552226, /*hidden argument*/NULL);
			NullCheck(L_22);
			String_t* L_23 = Capture_get_Value_m2241099810(L_22, /*hidden argument*/NULL);
			NullCheck(L_23);
			String_t* L_24 = String_Trim_m1030489823(L_23, /*hidden argument*/NULL);
			V_0 = L_24;
			Match_t2156507859 * L_25 = V_2;
			NullCheck(L_25);
			GroupCollection_t982584267 * L_26 = VirtFuncInvoker0< GroupCollection_t982584267 * >::Invoke(4 /* System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::get_Groups() */, L_25);
			NullCheck(L_26);
			Group_t2151468941 * L_27 = GroupCollection_get_Item_m4186628929(L_26, _stringLiteral1203236063, /*hidden argument*/NULL);
			NullCheck(L_27);
			String_t* L_28 = Capture_get_Value_m2241099810(L_27, /*hidden argument*/NULL);
			NullCheck(L_28);
			String_t* L_29 = String_Trim_m1030489823(L_28, /*hidden argument*/NULL);
			V_1 = L_29;
		}

IL_00cf:
		{
			goto IL_0274;
		}

IL_00d4:
		{
			int32_t L_30 = ___logLevel0;
			if ((!(((uint32_t)L_30) == ((uint32_t)5))))
			{
				goto IL_0274;
			}
		}

IL_00db:
		{
			String_t* L_31 = ___message2;
			NullCheck(L_31);
			bool L_32 = String_StartsWith_m1500793453(L_31, _stringLiteral3819939784, /*hidden argument*/NULL);
			if (!L_32)
			{
				goto IL_0274;
			}
		}

IL_00eb:
		{
			Regex_t2161232213 * L_33 = (Regex_t2161232213 *)il2cpp_codegen_object_new(Regex_t2161232213_il2cpp_TypeInfo_var);
			Regex__ctor_m2068483208(L_33, _stringLiteral3520912712, ((int32_t)16), /*hidden argument*/NULL);
			String_t* L_34 = ___message2;
			NullCheck(L_33);
			Match_t2156507859 * L_35 = Regex_Match_m2003175236(L_33, L_34, /*hidden argument*/NULL);
			V_3 = L_35;
			Match_t2156507859 * L_36 = V_3;
			NullCheck(L_36);
			bool L_37 = Group_get_Success_m139536080(L_36, /*hidden argument*/NULL);
			if (!L_37)
			{
				goto IL_0274;
			}
		}

IL_0109:
		{
			Match_t2156507859 * L_38 = V_3;
			NullCheck(L_38);
			GroupCollection_t982584267 * L_39 = VirtFuncInvoker0< GroupCollection_t982584267 * >::Invoke(4 /* System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::get_Groups() */, L_38);
			NullCheck(L_39);
			Group_t2151468941 * L_40 = GroupCollection_get_Item_m4186628929(L_39, _stringLiteral275119994, /*hidden argument*/NULL);
			NullCheck(L_40);
			String_t* L_41 = Capture_get_Value_m2241099810(L_40, /*hidden argument*/NULL);
			NullCheck(L_41);
			String_t* L_42 = String_Trim_m1030489823(L_41, /*hidden argument*/NULL);
			V_4 = L_42;
			Match_t2156507859 * L_43 = V_3;
			NullCheck(L_43);
			GroupCollection_t982584267 * L_44 = VirtFuncInvoker0< GroupCollection_t982584267 * >::Invoke(4 /* System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::get_Groups() */, L_43);
			NullCheck(L_44);
			Group_t2151468941 * L_45 = GroupCollection_get_Item_m4186628929(L_44, _stringLiteral2114919808, /*hidden argument*/NULL);
			NullCheck(L_45);
			String_t* L_46 = Capture_get_Value_m2241099810(L_45, /*hidden argument*/NULL);
			NullCheck(L_46);
			String_t* L_47 = String_Trim_m1030489823(L_46, /*hidden argument*/NULL);
			V_5 = L_47;
			String_t* L_48 = V_4;
			NullCheck(L_48);
			int32_t L_49 = String_LastIndexOf_m2747144337(L_48, _stringLiteral46, /*hidden argument*/NULL);
			V_6 = L_49;
			int32_t L_50 = V_6;
			if ((((int32_t)L_50) <= ((int32_t)0)))
			{
				goto IL_0176;
			}
		}

IL_0157:
		{
			int32_t L_51 = V_6;
			String_t* L_52 = V_4;
			NullCheck(L_52);
			int32_t L_53 = String_get_Length_m2979997331(L_52, /*hidden argument*/NULL);
			if ((((int32_t)L_51) == ((int32_t)L_53)))
			{
				goto IL_0176;
			}
		}

IL_0165:
		{
			String_t* L_54 = V_4;
			int32_t L_55 = V_6;
			NullCheck(L_54);
			String_t* L_56 = String_Substring_m2809233063(L_54, ((int32_t)((int32_t)L_55+(int32_t)1)), /*hidden argument*/NULL);
			V_0 = L_56;
			goto IL_0179;
		}

IL_0176:
		{
			String_t* L_57 = V_4;
			V_0 = L_57;
		}

IL_0179:
		{
			String_t* L_58 = V_5;
			NullCheck(L_58);
			int32_t L_59 = String_IndexOf_m1476794331(L_58, _stringLiteral1050157, /*hidden argument*/NULL);
			V_7 = L_59;
			int32_t L_60 = V_7;
			if ((((int32_t)L_60) <= ((int32_t)0)))
			{
				goto IL_01ed;
			}
		}

IL_018f:
		{
			String_t* L_61 = V_5;
			int32_t L_62 = V_7;
			NullCheck(L_61);
			String_t* L_63 = String_Substring_m675079568(L_61, 0, L_62, /*hidden argument*/NULL);
			V_1 = L_63;
			String_t* L_64 = V_5;
			int32_t L_65 = V_7;
			NullCheck(L_64);
			String_t* L_66 = String_Substring_m2809233063(L_64, ((int32_t)((int32_t)L_65+(int32_t)3)), /*hidden argument*/NULL);
			NullCheck(L_66);
			String_t* L_67 = String_Replace_m2915759397(L_66, _stringLiteral1050157, _stringLiteral10, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_68 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
			NullCheck(L_67);
			String_t* L_69 = String_Replace_m2915759397(L_67, _stringLiteral1522445698, L_68, /*hidden argument*/NULL);
			String_t* L_70 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
			NullCheck(L_69);
			String_t* L_71 = String_Replace_m2915759397(L_69, _stringLiteral1327183824, L_70, /*hidden argument*/NULL);
			V_8 = L_71;
			String_t* L_72 = ___stackTrace3;
			String_t* L_73 = V_8;
			NullCheck(L_73);
			String_t* L_74 = String_Trim_m1030489823(L_73, /*hidden argument*/NULL);
			String_t* L_75 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral3278827493, L_72, L_74, /*hidden argument*/NULL);
			___stackTrace3 = L_75;
			goto IL_01f0;
		}

IL_01ed:
		{
			String_t* L_76 = V_5;
			V_1 = L_76;
		}

IL_01f0:
		{
			String_t* L_77 = V_0;
			NullCheck(L_77);
			bool L_78 = String_Equals_m3541721061(L_77, _stringLiteral795620556, /*hidden argument*/NULL);
			if (!L_78)
			{
				goto IL_0274;
			}
		}

IL_0200:
		{
			String_t* L_79 = V_5;
			NullCheck(L_79);
			bool L_80 = String_Contains_m3032019141(L_79, _stringLiteral1477898, /*hidden argument*/NULL);
			if (!L_80)
			{
				goto IL_0274;
			}
		}

IL_0211:
		{
			String_t* L_81 = V_5;
			NullCheck(L_81);
			bool L_82 = String_Contains_m3032019141(L_81, _stringLiteral3968195238, /*hidden argument*/NULL);
			if (!L_82)
			{
				goto IL_0274;
			}
		}

IL_0222:
		{
			String_t* L_83 = V_5;
			NullCheck(L_83);
			int32_t L_84 = String_IndexOf_m1476794331(L_83, _stringLiteral3968195238, /*hidden argument*/NULL);
			V_7 = L_84;
			int32_t L_85 = V_7;
			if ((((int32_t)L_85) <= ((int32_t)0)))
			{
				goto IL_0274;
			}
		}

IL_0238:
		{
			String_t* L_86 = V_5;
			int32_t L_87 = V_7;
			NullCheck(L_86);
			String_t* L_88 = String_Substring_m675079568(L_86, 0, L_87, /*hidden argument*/NULL);
			V_1 = L_88;
			String_t* L_89 = V_5;
			int32_t L_90 = V_7;
			NullCheck(L_89);
			String_t* L_91 = String_Substring_m2809233063(L_89, ((int32_t)((int32_t)L_90+(int32_t)((int32_t)16))), /*hidden argument*/NULL);
			NullCheck(L_91);
			String_t* L_92 = String_Replace_m2915759397(L_91, _stringLiteral1083, _stringLiteral31153, /*hidden argument*/NULL);
			V_9 = L_92;
			String_t* L_93 = ___stackTrace3;
			String_t* L_94 = V_9;
			NullCheck(L_94);
			String_t* L_95 = String_Trim_m1030489823(L_94, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_96 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral3278827493, L_93, L_95, /*hidden argument*/NULL);
			___stackTrace3 = L_96;
		}

IL_0274:
		{
			goto IL_027f;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0279;
		throw e;
	}

CATCH_0279:
	{ // begin catch(System.Object)
		goto IL_027f;
	} // end catch (depth: 1)

IL_027f:
	{
		String_t* L_97 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_98 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_97, /*hidden argument*/NULL);
		if (!L_98)
		{
			goto IL_028c;
		}
	}
	{
		String_t* L_99 = ___message2;
		V_1 = L_99;
	}

IL_028c:
	{
		String_t* L_100 = ___name1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_101 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_100, /*hidden argument*/NULL);
		if (!L_101)
		{
			goto IL_02bd;
		}
	}
	{
		String_t* L_102 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_103 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_102, /*hidden argument*/NULL);
		if (!L_103)
		{
			goto IL_02b8;
		}
	}
	{
		int32_t L_104 = ___logLevel0;
		int32_t L_105 = L_104;
		Il2CppObject * L_106 = Box(LogSeverity_t591216193_il2cpp_TypeInfo_var, &L_105);
		NullCheck((Enum_t2862688501 *)L_106);
		String_t* L_107 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_106);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_108 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral4068061331, L_107, /*hidden argument*/NULL);
		V_0 = L_108;
	}

IL_02b8:
	{
		goto IL_02bf;
	}

IL_02bd:
	{
		String_t* L_109 = ___name1;
		V_0 = L_109;
	}

IL_02bf:
	{
		bool L_110 = ___uncaught4;
		String_t* L_111 = V_0;
		String_t* L_112 = V_1;
		String_t* L_113 = ___stackTrace3;
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent__reportException_m40291648(NULL /*static, unused*/, L_110, L_111, L_112, L_113, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyAgent/LogCallbackDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void LogCallbackDelegate__ctor_m1543750454 (LogCallbackDelegate_t45039171 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void BuglyAgent/LogCallbackDelegate::Invoke(System.String,System.String,UnityEngine.LogType)
extern "C"  void LogCallbackDelegate_Invoke_m2974759347 (LogCallbackDelegate_t45039171 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LogCallbackDelegate_Invoke_m2974759347((LogCallbackDelegate_t45039171 *)__this->get_prev_9(),___condition0, ___stackTrace1, ___type2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___condition0, ___stackTrace1, ___type2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___condition0, ___stackTrace1, ___type2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___condition0, ___stackTrace1, ___type2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_LogCallbackDelegate_t45039171 (LogCallbackDelegate_t45039171 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___condition0' to native representation
	char* ____condition0_marshaled = NULL;
	____condition0_marshaled = il2cpp_codegen_marshal_string(___condition0);

	// Marshaling of parameter '___stackTrace1' to native representation
	char* ____stackTrace1_marshaled = NULL;
	____stackTrace1_marshaled = il2cpp_codegen_marshal_string(___stackTrace1);

	// Native function invocation
	il2cppPInvokeFunc(____condition0_marshaled, ____stackTrace1_marshaled, ___type2);

	// Marshaling cleanup of parameter '___condition0' native representation
	il2cpp_codegen_marshal_free(____condition0_marshaled);
	____condition0_marshaled = NULL;

	// Marshaling cleanup of parameter '___stackTrace1' native representation
	il2cpp_codegen_marshal_free(____stackTrace1_marshaled);
	____stackTrace1_marshaled = NULL;

}
// System.IAsyncResult BuglyAgent/LogCallbackDelegate::BeginInvoke(System.String,System.String,UnityEngine.LogType,System.AsyncCallback,System.Object)
extern Il2CppClass* LogType_t4286006228_il2cpp_TypeInfo_var;
extern const uint32_t LogCallbackDelegate_BeginInvoke_m3880897110_MetadataUsageId;
extern "C"  Il2CppObject * LogCallbackDelegate_BeginInvoke_m3880897110 (LogCallbackDelegate_t45039171 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LogCallbackDelegate_BeginInvoke_m3880897110_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___condition0;
	__d_args[1] = ___stackTrace1;
	__d_args[2] = Box(LogType_t4286006228_il2cpp_TypeInfo_var, &___type2);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void BuglyAgent/LogCallbackDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void LogCallbackDelegate_EndInvoke_m586498118 (LogCallbackDelegate_t45039171 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void BuglyCallback::.ctor()
extern "C"  void BuglyCallback__ctor_m3441550257 (BuglyCallback_t115794502 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyInit::.ctor()
extern "C"  void BuglyInit__ctor_m134984902 (BuglyInit_t3142012817 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BuglyInit::Awake()
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_1_t1952811501_il2cpp_TypeInfo_var;
extern const MethodInfo* BuglyInit_MyLogCallbackExtrasHandler_m1398771442_MethodInfo_var;
extern const MethodInfo* Func_1__ctor_m1057050994_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral949212545;
extern const uint32_t BuglyInit_Awake_m372590121_MetadataUsageId;
extern "C"  void BuglyInit_Awake_m372590121 (BuglyInit_t3142012817 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyInit_Awake_m372590121_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_ConfigDebugMode_m3082024052(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		BuglyAgent_ConfigDefault_m4037068102(NULL /*static, unused*/, (String_t*)NULL, (String_t*)NULL, (String_t*)NULL, (((int64_t)((int64_t)0))), /*hidden argument*/NULL);
		BuglyAgent_ConfigAutoReportLogLevel_m3131243571(NULL /*static, unused*/, 5, /*hidden argument*/NULL);
		BuglyAgent_ConfigAutoQuitApplication_m4019756080(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		BuglyAgent_RegisterLogCallback_m2466877548(NULL /*static, unused*/, (LogCallbackDelegate_t45039171 *)NULL, /*hidden argument*/NULL);
		BuglyAgent_InitWithAppId_m2313387731(NULL /*static, unused*/, _stringLiteral949212545, /*hidden argument*/NULL);
		BuglyAgent_EnableExceptionHandler_m724280279(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)BuglyInit_MyLogCallbackExtrasHandler_m1398771442_MethodInfo_var);
		Func_1_t1952811501 * L_1 = (Func_1_t1952811501 *)il2cpp_codegen_object_new(Func_1_t1952811501_il2cpp_TypeInfo_var);
		Func_1__ctor_m1057050994(L_1, NULL, L_0, /*hidden argument*/Func_1__ctor_m1057050994_MethodInfo_var);
		BuglyAgent_SetLogCallbackExtrasHandler_m1328908226(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> BuglyInit::MyLogCallbackExtrasHandler()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* BuglyAgent_t2905516196_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t827649927_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DeviceType_t3959528308_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m640701813_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m2915568422_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1219196730;
extern Il2CppCodeGenString* _stringLiteral370348581;
extern Il2CppCodeGenString* _stringLiteral3282104503;
extern Il2CppCodeGenString* _stringLiteral2735305331;
extern Il2CppCodeGenString* _stringLiteral780988929;
extern Il2CppCodeGenString* _stringLiteral781190832;
extern Il2CppCodeGenString* _stringLiteral25199162;
extern Il2CppCodeGenString* _stringLiteral3136184;
extern Il2CppCodeGenString* _stringLiteral119816;
extern Il2CppCodeGenString* _stringLiteral3014022376;
extern Il2CppCodeGenString* _stringLiteral97234311;
extern Il2CppCodeGenString* _stringLiteral97234342;
extern Il2CppCodeGenString* _stringLiteral3247861986;
extern Il2CppCodeGenString* _stringLiteral704953741;
extern Il2CppCodeGenString* _stringLiteral3000387633;
extern Il2CppCodeGenString* _stringLiteral2063487939;
extern Il2CppCodeGenString* _stringLiteral580120052;
extern const uint32_t BuglyInit_MyLogCallbackExtrasHandler_m1398771442_MetadataUsageId;
extern "C"  Dictionary_2_t827649927 * BuglyInit_MyLogCallbackExtrasHandler_m1398771442 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BuglyInit_MyLogCallbackExtrasHandler_m1398771442_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t827649927 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(BuglyAgent_t2905516196_il2cpp_TypeInfo_var);
		BuglyAgent_PrintLog_m1023618409(NULL /*static, unused*/, 0, _stringLiteral1219196730, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		Dictionary_2_t827649927 * L_0 = (Dictionary_2_t827649927 *)il2cpp_codegen_object_new(Dictionary_2_t827649927_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m640701813(L_0, /*hidden argument*/Dictionary_2__ctor_m640701813_MethodInfo_var);
		V_0 = L_0;
		Dictionary_2_t827649927 * L_1 = V_0;
		int32_t L_2 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_3);
		int32_t L_5 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral3282104503, L_4, L_7, /*hidden argument*/NULL);
		NullCheck(L_1);
		Dictionary_2_Add_m2915568422(L_1, _stringLiteral370348581, L_8, /*hidden argument*/Dictionary_2_Add_m2915568422_MethodInfo_var);
		Dictionary_2_t827649927 * L_9 = V_0;
		String_t* L_10 = SystemInfo_get_deviceModel_m3014844565(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		Dictionary_2_Add_m2915568422(L_9, _stringLiteral2735305331, L_10, /*hidden argument*/Dictionary_2_Add_m2915568422_MethodInfo_var);
		Dictionary_2_t827649927 * L_11 = V_0;
		String_t* L_12 = SystemInfo_get_deviceName_m3161260225(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		Dictionary_2_Add_m2915568422(L_11, _stringLiteral780988929, L_12, /*hidden argument*/Dictionary_2_Add_m2915568422_MethodInfo_var);
		Dictionary_2_t827649927 * L_13 = V_0;
		int32_t L_14 = SystemInfo_get_deviceType_m2827604277(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(DeviceType_t3959528308_il2cpp_TypeInfo_var, &L_15);
		NullCheck((Enum_t2862688501 *)L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_16);
		NullCheck(L_13);
		Dictionary_2_Add_m2915568422(L_13, _stringLiteral781190832, L_17, /*hidden argument*/Dictionary_2_Add_m2915568422_MethodInfo_var);
		Dictionary_2_t827649927 * L_18 = V_0;
		String_t* L_19 = SystemInfo_get_deviceUniqueIdentifier_m983206480(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		Dictionary_2_Add_m2915568422(L_18, _stringLiteral25199162, L_19, /*hidden argument*/Dictionary_2_Add_m2915568422_MethodInfo_var);
		Dictionary_2_t827649927 * L_20 = V_0;
		int32_t L_21 = SystemInfo_get_graphicsDeviceID_m2460390329(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_22 = L_21;
		Il2CppObject * L_23 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_22);
		String_t* L_24 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral119816, L_23, /*hidden argument*/NULL);
		NullCheck(L_20);
		Dictionary_2_Add_m2915568422(L_20, _stringLiteral3136184, L_24, /*hidden argument*/Dictionary_2_Add_m2915568422_MethodInfo_var);
		Dictionary_2_t827649927 * L_25 = V_0;
		String_t* L_26 = SystemInfo_get_graphicsDeviceName_m4186183660(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_25);
		Dictionary_2_Add_m2915568422(L_25, _stringLiteral3014022376, L_26, /*hidden argument*/Dictionary_2_Add_m2915568422_MethodInfo_var);
		Dictionary_2_t827649927 * L_27 = V_0;
		String_t* L_28 = SystemInfo_get_graphicsDeviceVendor_m3175628649(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_27);
		Dictionary_2_Add_m2915568422(L_27, _stringLiteral97234311, L_28, /*hidden argument*/Dictionary_2_Add_m2915568422_MethodInfo_var);
		Dictionary_2_t827649927 * L_29 = V_0;
		String_t* L_30 = SystemInfo_get_graphicsDeviceVersion_m3634129081(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_29);
		Dictionary_2_Add_m2915568422(L_29, _stringLiteral97234342, L_30, /*hidden argument*/Dictionary_2_Add_m2915568422_MethodInfo_var);
		Dictionary_2_t827649927 * L_31 = V_0;
		int32_t L_32 = SystemInfo_get_graphicsDeviceVendorID_m3448682337(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_33 = L_32;
		Il2CppObject * L_34 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_33);
		String_t* L_35 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral119816, L_34, /*hidden argument*/NULL);
		NullCheck(L_31);
		Dictionary_2_Add_m2915568422(L_31, _stringLiteral3247861986, L_35, /*hidden argument*/Dictionary_2_Add_m2915568422_MethodInfo_var);
		Dictionary_2_t827649927 * L_36 = V_0;
		int32_t L_37 = SystemInfo_get_graphicsMemorySize_m4144186474(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_38 = L_37;
		Il2CppObject * L_39 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_38);
		String_t* L_40 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral119816, L_39, /*hidden argument*/NULL);
		NullCheck(L_36);
		Dictionary_2_Add_m2915568422(L_36, _stringLiteral704953741, L_40, /*hidden argument*/Dictionary_2_Add_m2915568422_MethodInfo_var);
		Dictionary_2_t827649927 * L_41 = V_0;
		int32_t L_42 = SystemInfo_get_systemMemorySize_m114183438(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_43 = L_42;
		Il2CppObject * L_44 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_43);
		String_t* L_45 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral119816, L_44, /*hidden argument*/NULL);
		NullCheck(L_41);
		Dictionary_2_Add_m2915568422(L_41, _stringLiteral3000387633, L_45, /*hidden argument*/Dictionary_2_Add_m2915568422_MethodInfo_var);
		Dictionary_2_t827649927 * L_46 = V_0;
		String_t* L_47 = Application_get_unityVersion_m3443350436(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_46);
		Dictionary_2_Add_m2915568422(L_46, _stringLiteral2063487939, L_47, /*hidden argument*/Dictionary_2_Add_m2915568422_MethodInfo_var);
		BuglyAgent_PrintLog_m1023618409(NULL /*static, unused*/, 2, _stringLiteral580120052, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		Dictionary_2_t827649927 * L_48 = V_0;
		return L_48;
	}
}
// System.Void ColorCorrectionEffect::.ctor()
extern "C"  void ColorCorrectionEffect__ctor_m2923302789 (ColorCorrectionEffect_t3496207346 * __this, const MethodInfo* method)
{
	{
		ImageEffectBase__ctor_m2296049466(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColorCorrectionEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral705227094;
extern const uint32_t ColorCorrectionEffect_OnRenderImage_m2530883001_MetadataUsageId;
extern "C"  void ColorCorrectionEffect_OnRenderImage_m2530883001 (ColorCorrectionEffect_t3496207346 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ColorCorrectionEffect_OnRenderImage_m2530883001_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t3870600107 * L_0 = ImageEffectBase_get_material_m2726075187(__this, /*hidden argument*/NULL);
		Texture_t2526458961 * L_1 = __this->get_textureRamp_4();
		NullCheck(L_0);
		Material_SetTexture_m1833724755(L_0, _stringLiteral705227094, L_1, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_2 = ___source0;
		RenderTexture_t1963041563 * L_3 = ___destination1;
		Material_t3870600107 * L_4 = ImageEffectBase_get_material_m2726075187(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ContrastStretchEffect::.ctor()
extern Il2CppClass* RenderTextureU5BU5D_t1576771098_il2cpp_TypeInfo_var;
extern const uint32_t ContrastStretchEffect__ctor_m162504899_MetadataUsageId;
extern "C"  void ContrastStretchEffect__ctor_m162504899 (ContrastStretchEffect_t443831540 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContrastStretchEffect__ctor_m162504899_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_adaptationSpeed_2((0.02f));
		__this->set_limitMinimum_3((0.2f));
		__this->set_limitMaximum_4((0.6f));
		__this->set_adaptRenderTex_5(((RenderTextureU5BU5D_t1576771098*)SZArrayNew(RenderTextureU5BU5D_t1576771098_il2cpp_TypeInfo_var, (uint32_t)2)));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material ContrastStretchEffect::get_materialLum()
extern Il2CppClass* Material_t3870600107_il2cpp_TypeInfo_var;
extern const uint32_t ContrastStretchEffect_get_materialLum_m1594234236_MetadataUsageId;
extern "C"  Material_t3870600107 * ContrastStretchEffect_get_materialLum_m1594234236 (ContrastStretchEffect_t443831540 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContrastStretchEffect_get_materialLum_m1594234236_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t3870600107 * L_0 = __this->get_m_materialLum_8();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t3191267369 * L_2 = __this->get_shaderLum_7();
		Material_t3870600107 * L_3 = (Material_t3870600107 *)il2cpp_codegen_object_new(Material_t3870600107_il2cpp_TypeInfo_var);
		Material__ctor_m2685909642(L_3, L_2, /*hidden argument*/NULL);
		__this->set_m_materialLum_8(L_3);
		Material_t3870600107 * L_4 = __this->get_m_materialLum_8();
		NullCheck(L_4);
		Object_set_hideFlags_m41317712(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t3870600107 * L_5 = __this->get_m_materialLum_8();
		return L_5;
	}
}
// UnityEngine.Material ContrastStretchEffect::get_materialReduce()
extern Il2CppClass* Material_t3870600107_il2cpp_TypeInfo_var;
extern const uint32_t ContrastStretchEffect_get_materialReduce_m450810896_MetadataUsageId;
extern "C"  Material_t3870600107 * ContrastStretchEffect_get_materialReduce_m450810896 (ContrastStretchEffect_t443831540 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContrastStretchEffect_get_materialReduce_m450810896_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t3870600107 * L_0 = __this->get_m_materialReduce_10();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t3191267369 * L_2 = __this->get_shaderReduce_9();
		Material_t3870600107 * L_3 = (Material_t3870600107 *)il2cpp_codegen_object_new(Material_t3870600107_il2cpp_TypeInfo_var);
		Material__ctor_m2685909642(L_3, L_2, /*hidden argument*/NULL);
		__this->set_m_materialReduce_10(L_3);
		Material_t3870600107 * L_4 = __this->get_m_materialReduce_10();
		NullCheck(L_4);
		Object_set_hideFlags_m41317712(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t3870600107 * L_5 = __this->get_m_materialReduce_10();
		return L_5;
	}
}
// UnityEngine.Material ContrastStretchEffect::get_materialAdapt()
extern Il2CppClass* Material_t3870600107_il2cpp_TypeInfo_var;
extern const uint32_t ContrastStretchEffect_get_materialAdapt_m1382578010_MetadataUsageId;
extern "C"  Material_t3870600107 * ContrastStretchEffect_get_materialAdapt_m1382578010 (ContrastStretchEffect_t443831540 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContrastStretchEffect_get_materialAdapt_m1382578010_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t3870600107 * L_0 = __this->get_m_materialAdapt_12();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t3191267369 * L_2 = __this->get_shaderAdapt_11();
		Material_t3870600107 * L_3 = (Material_t3870600107 *)il2cpp_codegen_object_new(Material_t3870600107_il2cpp_TypeInfo_var);
		Material__ctor_m2685909642(L_3, L_2, /*hidden argument*/NULL);
		__this->set_m_materialAdapt_12(L_3);
		Material_t3870600107 * L_4 = __this->get_m_materialAdapt_12();
		NullCheck(L_4);
		Object_set_hideFlags_m41317712(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t3870600107 * L_5 = __this->get_m_materialAdapt_12();
		return L_5;
	}
}
// UnityEngine.Material ContrastStretchEffect::get_materialApply()
extern Il2CppClass* Material_t3870600107_il2cpp_TypeInfo_var;
extern const uint32_t ContrastStretchEffect_get_materialApply_m1739866278_MetadataUsageId;
extern "C"  Material_t3870600107 * ContrastStretchEffect_get_materialApply_m1739866278 (ContrastStretchEffect_t443831540 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContrastStretchEffect_get_materialApply_m1739866278_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t3870600107 * L_0 = __this->get_m_materialApply_14();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t3191267369 * L_2 = __this->get_shaderApply_13();
		Material_t3870600107 * L_3 = (Material_t3870600107 *)il2cpp_codegen_object_new(Material_t3870600107_il2cpp_TypeInfo_var);
		Material__ctor_m2685909642(L_3, L_2, /*hidden argument*/NULL);
		__this->set_m_materialApply_14(L_3);
		Material_t3870600107 * L_4 = __this->get_m_materialApply_14();
		NullCheck(L_4);
		Object_set_hideFlags_m41317712(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t3870600107 * L_5 = __this->get_m_materialApply_14();
		return L_5;
	}
}
// System.Void ContrastStretchEffect::Start()
extern "C"  void ContrastStretchEffect_Start_m3404609987 (ContrastStretchEffect_t443831540 * __this, const MethodInfo* method)
{
	{
		bool L_0 = SystemInfo_get_supportsImageEffects_m2392300814(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_0012:
	{
		Shader_t3191267369 * L_1 = __this->get_shaderAdapt_11();
		NullCheck(L_1);
		bool L_2 = Shader_get_isSupported_m1422621179(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0052;
		}
	}
	{
		Shader_t3191267369 * L_3 = __this->get_shaderApply_13();
		NullCheck(L_3);
		bool L_4 = Shader_get_isSupported_m1422621179(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0052;
		}
	}
	{
		Shader_t3191267369 * L_5 = __this->get_shaderLum_7();
		NullCheck(L_5);
		bool L_6 = Shader_get_isSupported_m1422621179(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0052;
		}
	}
	{
		Shader_t3191267369 * L_7 = __this->get_shaderReduce_9();
		NullCheck(L_7);
		bool L_8 = Shader_get_isSupported_m1422621179(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_005a;
		}
	}

IL_0052:
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_005a:
	{
		return;
	}
}
// System.Void ContrastStretchEffect::OnEnable()
extern Il2CppClass* RenderTexture_t1963041563_il2cpp_TypeInfo_var;
extern const uint32_t ContrastStretchEffect_OnEnable_m2826882019_MetadataUsageId;
extern "C"  void ContrastStretchEffect_OnEnable_m2826882019 (ContrastStretchEffect_t443831540 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContrastStretchEffect_OnEnable_m2826882019_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_003c;
	}

IL_0007:
	{
		RenderTextureU5BU5D_t1576771098* L_0 = __this->get_adaptRenderTex_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		RenderTexture_t1963041563 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		bool L_4 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0038;
		}
	}
	{
		RenderTextureU5BU5D_t1576771098* L_5 = __this->get_adaptRenderTex_5();
		int32_t L_6 = V_0;
		RenderTexture_t1963041563 * L_7 = (RenderTexture_t1963041563 *)il2cpp_codegen_object_new(RenderTexture_t1963041563_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m591418693(L_7, 1, 1, 0, /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (RenderTexture_t1963041563 *)L_7);
		RenderTextureU5BU5D_t1576771098* L_8 = __this->get_adaptRenderTex_5();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		RenderTexture_t1963041563 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		Object_set_hideFlags_m41317712(L_11, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_0038:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_13 = V_0;
		if ((((int32_t)L_13) < ((int32_t)2)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ContrastStretchEffect::OnDisable()
extern "C"  void ContrastStretchEffect_OnDisable_m2174933930 (ContrastStretchEffect_t443831540 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0021;
	}

IL_0007:
	{
		RenderTextureU5BU5D_t1576771098* L_0 = __this->get_adaptRenderTex_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		RenderTexture_t1963041563 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		RenderTextureU5BU5D_t1576771098* L_4 = __this->get_adaptRenderTex_5();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, NULL);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (RenderTexture_t1963041563 *)NULL);
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0021:
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) < ((int32_t)2)))
		{
			goto IL_0007;
		}
	}
	{
		Material_t3870600107 * L_8 = __this->get_m_materialLum_8();
		bool L_9 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0043;
		}
	}
	{
		Material_t3870600107 * L_10 = __this->get_m_materialLum_8();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_0043:
	{
		Material_t3870600107 * L_11 = __this->get_m_materialReduce_10();
		bool L_12 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_005e;
		}
	}
	{
		Material_t3870600107 * L_13 = __this->get_m_materialReduce_10();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
	}

IL_005e:
	{
		Material_t3870600107 * L_14 = __this->get_m_materialAdapt_12();
		bool L_15 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0079;
		}
	}
	{
		Material_t3870600107 * L_16 = __this->get_m_materialAdapt_12();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
	}

IL_0079:
	{
		Material_t3870600107 * L_17 = __this->get_m_materialApply_14();
		bool L_18 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0094;
		}
	}
	{
		Material_t3870600107 * L_19 = __this->get_m_materialApply_14();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
	}

IL_0094:
	{
		return;
	}
}
// System.Void ContrastStretchEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral3144096548;
extern const uint32_t ContrastStretchEffect_OnRenderImage_m3152364987_MetadataUsageId;
extern "C"  void ContrastStretchEffect_OnRenderImage_m3152364987 (ContrastStretchEffect_t443831540 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContrastStretchEffect_OnRenderImage_m3152364987_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RenderTexture_t1963041563 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	RenderTexture_t1963041563 * V_6 = NULL;
	{
		RenderTexture_t1963041563 * L_0 = ___source0;
		NullCheck(L_0);
		int32_t L_1 = RenderTexture_get_width_m1498578543(L_0, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_2 = ___source0;
		NullCheck(L_2);
		int32_t L_3 = RenderTexture_get_height_m4010076224(L_2, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_4 = RenderTexture_GetTemporary_m469965696(NULL /*static, unused*/, ((int32_t)((int32_t)L_1/(int32_t)1)), ((int32_t)((int32_t)L_3/(int32_t)1)), /*hidden argument*/NULL);
		V_1 = L_4;
		RenderTexture_t1963041563 * L_5 = ___source0;
		RenderTexture_t1963041563 * L_6 = V_1;
		Material_t3870600107 * L_7 = ContrastStretchEffect_get_materialLum_m1594234236(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		goto IL_0074;
	}

IL_0028:
	{
		RenderTexture_t1963041563 * L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = RenderTexture_get_width_m1498578543(L_8, /*hidden argument*/NULL);
		V_4 = ((int32_t)((int32_t)L_9/(int32_t)2));
		int32_t L_10 = V_4;
		if ((((int32_t)L_10) >= ((int32_t)1)))
		{
			goto IL_003d;
		}
	}
	{
		V_4 = 1;
	}

IL_003d:
	{
		RenderTexture_t1963041563 * L_11 = V_1;
		NullCheck(L_11);
		int32_t L_12 = RenderTexture_get_height_m4010076224(L_11, /*hidden argument*/NULL);
		V_5 = ((int32_t)((int32_t)L_12/(int32_t)2));
		int32_t L_13 = V_5;
		if ((((int32_t)L_13) >= ((int32_t)1)))
		{
			goto IL_0052;
		}
	}
	{
		V_5 = 1;
	}

IL_0052:
	{
		int32_t L_14 = V_4;
		int32_t L_15 = V_5;
		RenderTexture_t1963041563 * L_16 = RenderTexture_GetTemporary_m469965696(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		V_6 = L_16;
		RenderTexture_t1963041563 * L_17 = V_1;
		RenderTexture_t1963041563 * L_18 = V_6;
		Material_t3870600107 * L_19 = ContrastStretchEffect_get_materialReduce_m450810896(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_20 = V_1;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_21 = V_6;
		V_1 = L_21;
	}

IL_0074:
	{
		RenderTexture_t1963041563 * L_22 = V_1;
		NullCheck(L_22);
		int32_t L_23 = RenderTexture_get_width_m1498578543(L_22, /*hidden argument*/NULL);
		if ((((int32_t)L_23) > ((int32_t)1)))
		{
			goto IL_0028;
		}
	}
	{
		RenderTexture_t1963041563 * L_24 = V_1;
		NullCheck(L_24);
		int32_t L_25 = RenderTexture_get_height_m4010076224(L_24, /*hidden argument*/NULL);
		if ((((int32_t)L_25) > ((int32_t)1)))
		{
			goto IL_0028;
		}
	}
	{
		RenderTexture_t1963041563 * L_26 = V_1;
		ContrastStretchEffect_CalculateAdaptation_m2954666100(__this, L_26, /*hidden argument*/NULL);
		Material_t3870600107 * L_27 = ContrastStretchEffect_get_materialApply_m1739866278(__this, /*hidden argument*/NULL);
		RenderTextureU5BU5D_t1576771098* L_28 = __this->get_adaptRenderTex_5();
		int32_t L_29 = __this->get_curAdaptIndex_6();
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		int32_t L_30 = L_29;
		RenderTexture_t1963041563 * L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		NullCheck(L_27);
		Material_SetTexture_m1833724755(L_27, _stringLiteral3144096548, L_31, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_32 = ___source0;
		RenderTexture_t1963041563 * L_33 = ___destination1;
		Material_t3870600107 * L_34 = ContrastStretchEffect_get_materialApply_m1739866278(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_32, L_33, L_34, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_35 = V_1;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ContrastStretchEffect::CalculateAdaptation(UnityEngine.Texture)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral443188998;
extern Il2CppCodeGenString* _stringLiteral1015177577;
extern const uint32_t ContrastStretchEffect_CalculateAdaptation_m2954666100_MetadataUsageId;
extern "C"  void ContrastStretchEffect_CalculateAdaptation_m2954666100 (ContrastStretchEffect_t443831540 * __this, Texture_t2526458961 * ___curTexture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ContrastStretchEffect_CalculateAdaptation_m2954666100_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		int32_t L_0 = __this->get_curAdaptIndex_6();
		V_0 = L_0;
		int32_t L_1 = __this->get_curAdaptIndex_6();
		__this->set_curAdaptIndex_6(((int32_t)((int32_t)((int32_t)((int32_t)L_1+(int32_t)1))%(int32_t)2)));
		float L_2 = __this->get_adaptationSpeed_2();
		float L_3 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_4 = powf(((float)((float)(1.0f)-(float)L_2)), ((float)((float)(30.0f)*(float)L_3)));
		V_1 = ((float)((float)(1.0f)-(float)L_4));
		float L_5 = V_1;
		float L_6 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_5, (0.01f), (1.0f), /*hidden argument*/NULL);
		V_1 = L_6;
		Material_t3870600107 * L_7 = ContrastStretchEffect_get_materialAdapt_m1382578010(__this, /*hidden argument*/NULL);
		Texture_t2526458961 * L_8 = ___curTexture0;
		NullCheck(L_7);
		Material_SetTexture_m1833724755(L_7, _stringLiteral443188998, L_8, /*hidden argument*/NULL);
		Material_t3870600107 * L_9 = ContrastStretchEffect_get_materialAdapt_m1382578010(__this, /*hidden argument*/NULL);
		float L_10 = V_1;
		float L_11 = __this->get_limitMinimum_3();
		float L_12 = __this->get_limitMaximum_4();
		Vector4_t4282066567  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector4__ctor_m2441427762(&L_13, L_10, L_11, L_12, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_SetVector_m3505096203(L_9, _stringLiteral1015177577, L_13, /*hidden argument*/NULL);
		RenderTextureU5BU5D_t1576771098* L_14 = __this->get_adaptRenderTex_5();
		int32_t L_15 = __this->get_curAdaptIndex_6();
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		RenderTexture_t1963041563 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		Graphics_SetRenderTarget_m3051614107(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		Color_t4194546905  L_18 = Color_get_black_m1687201969(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_Clear_m2980983731(NULL /*static, unused*/, (bool)0, (bool)1, L_18, /*hidden argument*/NULL);
		RenderTextureU5BU5D_t1576771098* L_19 = __this->get_adaptRenderTex_5();
		int32_t L_20 = V_0;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = L_20;
		RenderTexture_t1963041563 * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		RenderTextureU5BU5D_t1576771098* L_23 = __this->get_adaptRenderTex_5();
		int32_t L_24 = __this->get_curAdaptIndex_6();
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		RenderTexture_t1963041563 * L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		Material_t3870600107 * L_27 = ContrastStretchEffect_get_materialAdapt_m1382578010(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_22, L_26, L_27, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GlowEffect::.ctor()
extern "C"  void GlowEffect__ctor_m1367092849 (GlowEffect_t1785780638 * __this, const MethodInfo* method)
{
	{
		__this->set_glowIntensity_2((1.5f));
		__this->set_blurIterations_3(3);
		__this->set_blurSpread_4((0.7f));
		Color_t4194546905  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2252924356(&L_0, (1.0f), (1.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_glowTint_5(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material GlowEffect::get_compositeMaterial()
extern Il2CppClass* Material_t3870600107_il2cpp_TypeInfo_var;
extern const uint32_t GlowEffect_get_compositeMaterial_m711258029_MetadataUsageId;
extern "C"  Material_t3870600107 * GlowEffect_get_compositeMaterial_m711258029 (GlowEffect_t1785780638 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GlowEffect_get_compositeMaterial_m711258029_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t3870600107 * L_0 = __this->get_m_CompositeMaterial_7();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t3191267369 * L_2 = __this->get_compositeShader_6();
		Material_t3870600107 * L_3 = (Material_t3870600107 *)il2cpp_codegen_object_new(Material_t3870600107_il2cpp_TypeInfo_var);
		Material__ctor_m2685909642(L_3, L_2, /*hidden argument*/NULL);
		__this->set_m_CompositeMaterial_7(L_3);
		Material_t3870600107 * L_4 = __this->get_m_CompositeMaterial_7();
		NullCheck(L_4);
		Object_set_hideFlags_m41317712(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t3870600107 * L_5 = __this->get_m_CompositeMaterial_7();
		return L_5;
	}
}
// UnityEngine.Material GlowEffect::get_blurMaterial()
extern Il2CppClass* Material_t3870600107_il2cpp_TypeInfo_var;
extern const uint32_t GlowEffect_get_blurMaterial_m3248730129_MetadataUsageId;
extern "C"  Material_t3870600107 * GlowEffect_get_blurMaterial_m3248730129 (GlowEffect_t1785780638 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GlowEffect_get_blurMaterial_m3248730129_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t3870600107 * L_0 = __this->get_m_BlurMaterial_9();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t3191267369 * L_2 = __this->get_blurShader_8();
		Material_t3870600107 * L_3 = (Material_t3870600107 *)il2cpp_codegen_object_new(Material_t3870600107_il2cpp_TypeInfo_var);
		Material__ctor_m2685909642(L_3, L_2, /*hidden argument*/NULL);
		__this->set_m_BlurMaterial_9(L_3);
		Material_t3870600107 * L_4 = __this->get_m_BlurMaterial_9();
		NullCheck(L_4);
		Object_set_hideFlags_m41317712(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t3870600107 * L_5 = __this->get_m_BlurMaterial_9();
		return L_5;
	}
}
// UnityEngine.Material GlowEffect::get_downsampleMaterial()
extern Il2CppClass* Material_t3870600107_il2cpp_TypeInfo_var;
extern const uint32_t GlowEffect_get_downsampleMaterial_m2739088566_MetadataUsageId;
extern "C"  Material_t3870600107 * GlowEffect_get_downsampleMaterial_m2739088566 (GlowEffect_t1785780638 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GlowEffect_get_downsampleMaterial_m2739088566_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t3870600107 * L_0 = __this->get_m_DownsampleMaterial_11();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t3191267369 * L_2 = __this->get_downsampleShader_10();
		Material_t3870600107 * L_3 = (Material_t3870600107 *)il2cpp_codegen_object_new(Material_t3870600107_il2cpp_TypeInfo_var);
		Material__ctor_m2685909642(L_3, L_2, /*hidden argument*/NULL);
		__this->set_m_DownsampleMaterial_11(L_3);
		Material_t3870600107 * L_4 = __this->get_m_DownsampleMaterial_11();
		NullCheck(L_4);
		Object_set_hideFlags_m41317712(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t3870600107 * L_5 = __this->get_m_DownsampleMaterial_11();
		return L_5;
	}
}
// System.Void GlowEffect::OnDisable()
extern "C"  void GlowEffect_OnDisable_m3488932440 (GlowEffect_t1785780638 * __this, const MethodInfo* method)
{
	{
		Material_t3870600107 * L_0 = __this->get_m_CompositeMaterial_7();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t3870600107 * L_2 = __this->get_m_CompositeMaterial_7();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		Material_t3870600107 * L_3 = __this->get_m_BlurMaterial_9();
		bool L_4 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		Material_t3870600107 * L_5 = __this->get_m_BlurMaterial_9();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0036:
	{
		Material_t3870600107 * L_6 = __this->get_m_DownsampleMaterial_11();
		bool L_7 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0051;
		}
	}
	{
		Material_t3870600107 * L_8 = __this->get_m_DownsampleMaterial_11();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0051:
	{
		return;
	}
}
// System.Void GlowEffect::Start()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1950806131;
extern const uint32_t GlowEffect_Start_m314230641_MetadataUsageId;
extern "C"  void GlowEffect_Start_m314230641 (GlowEffect_t1785780638 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GlowEffect_Start_m314230641_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = SystemInfo_get_supportsImageEffects_m2392300814(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_0012:
	{
		Shader_t3191267369 * L_1 = __this->get_downsampleShader_10();
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1950806131, /*hidden argument*/NULL);
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		goto IL_008d;
	}

IL_0039:
	{
		Material_t3870600107 * L_3 = GlowEffect_get_blurMaterial_m3248730129(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Shader_t3191267369 * L_4 = Material_get_shader_m2881845503(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = Shader_get_isSupported_m1422621179(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0055;
		}
	}
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_0055:
	{
		Material_t3870600107 * L_6 = GlowEffect_get_compositeMaterial_m711258029(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Shader_t3191267369 * L_7 = Material_get_shader_m2881845503(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Shader_get_isSupported_m1422621179(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0071;
		}
	}
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_0071:
	{
		Material_t3870600107 * L_9 = GlowEffect_get_downsampleMaterial_m2739088566(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Shader_t3191267369 * L_10 = Material_get_shader_m2881845503(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Shader_get_isSupported_m1422621179(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_008d;
		}
	}
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_008d:
	{
		return;
	}
}
// System.Void GlowEffect::FourTapCone(UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Int32)
extern Il2CppClass* Vector2U5BU5D_t4024180168_il2cpp_TypeInfo_var;
extern const uint32_t GlowEffect_FourTapCone_m365214468_MetadataUsageId;
extern "C"  void GlowEffect_FourTapCone_m365214468 (GlowEffect_t1785780638 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___dest1, int32_t ___iteration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GlowEffect_FourTapCone_m365214468_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___iteration2;
		float L_1 = __this->get_blurSpread_4();
		V_0 = ((float)((float)(0.5f)+(float)((float)((float)(((float)((float)L_0)))*(float)L_1))));
		RenderTexture_t1963041563 * L_2 = ___source0;
		RenderTexture_t1963041563 * L_3 = ___dest1;
		Material_t3870600107 * L_4 = GlowEffect_get_blurMaterial_m3248730129(__this, /*hidden argument*/NULL);
		Vector2U5BU5D_t4024180168* L_5 = ((Vector2U5BU5D_t4024180168*)SZArrayNew(Vector2U5BU5D_t4024180168_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		float L_6 = V_0;
		float L_7 = V_0;
		Vector2_t4282066565  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector2__ctor_m1517109030(&L_8, L_6, L_7, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_8;
		Vector2U5BU5D_t4024180168* L_9 = L_5;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		float L_10 = V_0;
		float L_11 = V_0;
		Vector2_t4282066565  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector2__ctor_m1517109030(&L_12, ((-L_10)), L_11, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_12;
		Vector2U5BU5D_t4024180168* L_13 = L_9;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
		float L_14 = V_0;
		float L_15 = V_0;
		Vector2_t4282066565  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector2__ctor_m1517109030(&L_16, L_14, ((-L_15)), /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_16;
		Vector2U5BU5D_t4024180168* L_17 = L_13;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 3);
		float L_18 = V_0;
		float L_19 = V_0;
		Vector2_t4282066565  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector2__ctor_m1517109030(&L_20, ((-L_18)), ((-L_19)), /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_20;
		Graphics_BlitMultiTap_m4211810031(NULL /*static, unused*/, L_2, L_3, L_4, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GlowEffect::DownSample4x(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void GlowEffect_DownSample4x_m3710080831 (GlowEffect_t1785780638 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___dest1, const MethodInfo* method)
{
	{
		Material_t3870600107 * L_0 = GlowEffect_get_downsampleMaterial_m2739088566(__this, /*hidden argument*/NULL);
		Color_t4194546905 * L_1 = __this->get_address_of_glowTint_5();
		float L_2 = L_1->get_r_0();
		Color_t4194546905 * L_3 = __this->get_address_of_glowTint_5();
		float L_4 = L_3->get_g_1();
		Color_t4194546905 * L_5 = __this->get_address_of_glowTint_5();
		float L_6 = L_5->get_b_2();
		Color_t4194546905 * L_7 = __this->get_address_of_glowTint_5();
		float L_8 = L_7->get_a_3();
		Color_t4194546905  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Color__ctor_m2252924356(&L_9, L_2, L_4, L_6, ((float)((float)L_8/(float)(4.0f))), /*hidden argument*/NULL);
		NullCheck(L_0);
		Material_set_color_m3296857020(L_0, L_9, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_10 = ___source0;
		RenderTexture_t1963041563 * L_11 = ___dest1;
		Material_t3870600107 * L_12 = GlowEffect_get_downsampleMaterial_m2739088566(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_10, L_11, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GlowEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t GlowEffect_OnRenderImage_m1378931533_MetadataUsageId;
extern "C"  void GlowEffect_OnRenderImage_m1378931533 (GlowEffect_t1785780638 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GlowEffect_OnRenderImage_m1378931533_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RenderTexture_t1963041563 * V_2 = NULL;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	RenderTexture_t1963041563 * V_5 = NULL;
	{
		float L_0 = __this->get_glowIntensity_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_0, (0.0f), (10.0f), /*hidden argument*/NULL);
		__this->set_glowIntensity_2(L_1);
		int32_t L_2 = __this->get_blurIterations_3();
		int32_t L_3 = Mathf_Clamp_m510460741(NULL /*static, unused*/, L_2, 0, ((int32_t)30), /*hidden argument*/NULL);
		__this->set_blurIterations_3(L_3);
		float L_4 = __this->get_blurSpread_4();
		float L_5 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_4, (0.5f), (1.0f), /*hidden argument*/NULL);
		__this->set_blurSpread_4(L_5);
		RenderTexture_t1963041563 * L_6 = ___source0;
		NullCheck(L_6);
		int32_t L_7 = RenderTexture_get_width_m1498578543(L_6, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_7/(int32_t)4));
		RenderTexture_t1963041563 * L_8 = ___source0;
		NullCheck(L_8);
		int32_t L_9 = RenderTexture_get_height_m4010076224(L_8, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_9/(int32_t)4));
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		RenderTexture_t1963041563 * L_12 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, L_10, L_11, 0, /*hidden argument*/NULL);
		V_2 = L_12;
		RenderTexture_t1963041563 * L_13 = ___source0;
		RenderTexture_t1963041563 * L_14 = V_2;
		GlowEffect_DownSample4x_m3710080831(__this, L_13, L_14, /*hidden argument*/NULL);
		float L_15 = __this->get_glowIntensity_2();
		float L_16 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, ((float)((float)((float)((float)L_15-(float)(1.0f)))/(float)(4.0f))), /*hidden argument*/NULL);
		V_3 = L_16;
		Material_t3870600107 * L_17 = GlowEffect_get_blurMaterial_m3248730129(__this, /*hidden argument*/NULL);
		float L_18 = V_3;
		Color_t4194546905  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Color__ctor_m2252924356(&L_19, (1.0f), (1.0f), (1.0f), ((float)((float)(0.25f)+(float)L_18)), /*hidden argument*/NULL);
		NullCheck(L_17);
		Material_set_color_m3296857020(L_17, L_19, /*hidden argument*/NULL);
		V_4 = 0;
		goto IL_00d7;
	}

IL_00b3:
	{
		int32_t L_20 = V_0;
		int32_t L_21 = V_1;
		RenderTexture_t1963041563 * L_22 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, L_20, L_21, 0, /*hidden argument*/NULL);
		V_5 = L_22;
		RenderTexture_t1963041563 * L_23 = V_2;
		RenderTexture_t1963041563 * L_24 = V_5;
		int32_t L_25 = V_4;
		GlowEffect_FourTapCone_m365214468(__this, L_23, L_24, L_25, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_26 = V_2;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_27 = V_5;
		V_2 = L_27;
		int32_t L_28 = V_4;
		V_4 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_00d7:
	{
		int32_t L_29 = V_4;
		int32_t L_30 = __this->get_blurIterations_3();
		if ((((int32_t)L_29) < ((int32_t)L_30)))
		{
			goto IL_00b3;
		}
	}
	{
		RenderTexture_t1963041563 * L_31 = ___source0;
		RenderTexture_t1963041563 * L_32 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_33 = V_2;
		RenderTexture_t1963041563 * L_34 = ___destination1;
		GlowEffect_BlitGlow_m1698331661(__this, L_33, L_34, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_35 = V_2;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GlowEffect::BlitGlow(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t GlowEffect_BlitGlow_m1698331661_MetadataUsageId;
extern "C"  void GlowEffect_BlitGlow_m1698331661 (GlowEffect_t1785780638 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___dest1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GlowEffect_BlitGlow_m1698331661_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t3870600107 * L_0 = GlowEffect_get_compositeMaterial_m711258029(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_glowIntensity_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Color_t4194546905  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Color__ctor_m2252924356(&L_3, (1.0f), (1.0f), (1.0f), L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Material_set_color_m3296857020(L_0, L_3, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_4 = ___source0;
		RenderTexture_t1963041563 * L_5 = ___dest1;
		Material_t3870600107 * L_6 = GlowEffect_get_compositeMaterial_m711258029(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GrayscaleEffect::.ctor()
extern "C"  void GrayscaleEffect__ctor_m2179105823 (GrayscaleEffect_t967569688 * __this, const MethodInfo* method)
{
	{
		ImageEffectBase__ctor_m2296049466(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GrayscaleEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral705227094;
extern Il2CppCodeGenString* _stringLiteral2592667908;
extern const uint32_t GrayscaleEffect_OnRenderImage_m2200279263_MetadataUsageId;
extern "C"  void GrayscaleEffect_OnRenderImage_m2200279263 (GrayscaleEffect_t967569688 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GrayscaleEffect_OnRenderImage_m2200279263_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t3870600107 * L_0 = ImageEffectBase_get_material_m2726075187(__this, /*hidden argument*/NULL);
		Texture_t2526458961 * L_1 = __this->get_textureRamp_4();
		NullCheck(L_0);
		Material_SetTexture_m1833724755(L_0, _stringLiteral705227094, L_1, /*hidden argument*/NULL);
		Material_t3870600107 * L_2 = ImageEffectBase_get_material_m2726075187(__this, /*hidden argument*/NULL);
		float L_3 = __this->get_rampOffset_5();
		NullCheck(L_2);
		Material_SetFloat_m981710063(L_2, _stringLiteral2592667908, L_3, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_4 = ___source0;
		RenderTexture_t1963041563 * L_5 = ___destination1;
		Material_t3870600107 * L_6 = ImageEffectBase_get_material_m2726075187(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ImageEffectBase::.ctor()
extern "C"  void ImageEffectBase__ctor_m2296049466 (ImageEffectBase_t3731393437 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ImageEffectBase::Start()
extern "C"  void ImageEffectBase_Start_m1243187258 (ImageEffectBase_t3731393437 * __this, const MethodInfo* method)
{
	{
		bool L_0 = SystemInfo_get_supportsImageEffects_m2392300814(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_0012:
	{
		Shader_t3191267369 * L_1 = __this->get_shader_2();
		bool L_2 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		Shader_t3191267369 * L_3 = __this->get_shader_2();
		NullCheck(L_3);
		bool L_4 = Shader_get_isSupported_m1422621179(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0039;
		}
	}

IL_0032:
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_0039:
	{
		return;
	}
}
// UnityEngine.Material ImageEffectBase::get_material()
extern Il2CppClass* Material_t3870600107_il2cpp_TypeInfo_var;
extern const uint32_t ImageEffectBase_get_material_m2726075187_MetadataUsageId;
extern "C"  Material_t3870600107 * ImageEffectBase_get_material_m2726075187 (ImageEffectBase_t3731393437 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ImageEffectBase_get_material_m2726075187_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t3870600107 * L_0 = __this->get_m_Material_3();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t3191267369 * L_2 = __this->get_shader_2();
		Material_t3870600107 * L_3 = (Material_t3870600107 *)il2cpp_codegen_object_new(Material_t3870600107_il2cpp_TypeInfo_var);
		Material__ctor_m2685909642(L_3, L_2, /*hidden argument*/NULL);
		__this->set_m_Material_3(L_3);
		Material_t3870600107 * L_4 = __this->get_m_Material_3();
		NullCheck(L_4);
		Object_set_hideFlags_m41317712(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t3870600107 * L_5 = __this->get_m_Material_3();
		return L_5;
	}
}
// System.Void ImageEffectBase::OnDisable()
extern "C"  void ImageEffectBase_OnDisable_m3305379489 (ImageEffectBase_t3731393437 * __this, const MethodInfo* method)
{
	{
		Material_t3870600107 * L_0 = __this->get_m_Material_3();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t3870600107 * L_2 = __this->get_m_Material_3();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void ImageEffects::.ctor()
extern "C"  void ImageEffects__ctor_m2117305320 (ImageEffects_t3223621447 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ImageEffects::RenderDistortion(UnityEngine.Material,UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Single,UnityEngine.Vector2,UnityEngine.Vector2)
extern Il2CppCodeGenString* _stringLiteral3544758046;
extern Il2CppCodeGenString* _stringLiteral2260307782;
extern Il2CppCodeGenString* _stringLiteral2783177652;
extern const uint32_t ImageEffects_RenderDistortion_m1051594010_MetadataUsageId;
extern "C"  void ImageEffects_RenderDistortion_m1051594010 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___material0, RenderTexture_t1963041563 * ___source1, RenderTexture_t1963041563 * ___destination2, float ___angle3, Vector2_t4282066565  ___center4, Vector2_t4282066565  ___radius5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ImageEffects_RenderDistortion_m1051594010_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Matrix4x4_t1651859333  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t4282066565  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		RenderTexture_t1963041563 * L_0 = ___source1;
		NullCheck(L_0);
		Vector2_t4282066565  L_1 = Texture_get_texelSize_m3893699517(L_0, /*hidden argument*/NULL);
		V_2 = L_1;
		float L_2 = (&V_2)->get_y_2();
		V_0 = (bool)((((float)L_2) < ((float)(0.0f)))? 1 : 0);
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0034;
		}
	}
	{
		float L_4 = (&___center4)->get_y_2();
		(&___center4)->set_y_2(((float)((float)(1.0f)-(float)L_4)));
		float L_5 = ___angle3;
		___angle3 = ((-L_5));
	}

IL_0034:
	{
		Vector3_t4282066566  L_6 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = ___angle3;
		Quaternion_t1553702882  L_8 = Quaternion_Euler_m1204688217(NULL /*static, unused*/, (0.0f), (0.0f), L_7, /*hidden argument*/NULL);
		Vector3_t4282066566  L_9 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		Matrix4x4_t1651859333  L_10 = Matrix4x4_TRS_m3596398659(NULL /*static, unused*/, L_6, L_8, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		Material_t3870600107 * L_11 = ___material0;
		Matrix4x4_t1651859333  L_12 = V_1;
		NullCheck(L_11);
		Material_SetMatrix_m3693790735(L_11, _stringLiteral3544758046, L_12, /*hidden argument*/NULL);
		Material_t3870600107 * L_13 = ___material0;
		float L_14 = (&___center4)->get_x_1();
		float L_15 = (&___center4)->get_y_2();
		float L_16 = (&___radius5)->get_x_1();
		float L_17 = (&___radius5)->get_y_2();
		Vector4_t4282066567  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector4__ctor_m2441427762(&L_18, L_14, L_15, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_13);
		Material_SetVector_m3505096203(L_13, _stringLiteral2260307782, L_18, /*hidden argument*/NULL);
		Material_t3870600107 * L_19 = ___material0;
		float L_20 = ___angle3;
		NullCheck(L_19);
		Material_SetFloat_m981710063(L_19, _stringLiteral2783177652, ((float)((float)L_20*(float)(0.0174532924f))), /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_21 = ___source1;
		RenderTexture_t1963041563 * L_22 = ___destination2;
		Material_t3870600107 * L_23 = ___material0;
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ImageEffects::Blit(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void ImageEffects_Blit_m159867345 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___dest1, const MethodInfo* method)
{
	{
		RenderTexture_t1963041563 * L_0 = ___source0;
		RenderTexture_t1963041563 * L_1 = ___dest1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ImageEffects::BlitWithMaterial(UnityEngine.Material,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void ImageEffects_BlitWithMaterial_m1053377564 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___material0, RenderTexture_t1963041563 * ___source1, RenderTexture_t1963041563 * ___dest2, const MethodInfo* method)
{
	{
		RenderTexture_t1963041563 * L_0 = ___source1;
		RenderTexture_t1963041563 * L_1 = ___dest2;
		Material_t3870600107 * L_2 = ___material0;
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" char* DEFAULT_CALL getIPv6(char*, char*);
// System.String iOSipv6::getIPv6(System.String,System.String)
extern "C"  String_t* iOSipv6_getIPv6_m1716917565 (Il2CppObject * __this /* static, unused */, String_t* ___host0, String_t* ___port1, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___host0' to native representation
	char* ____host0_marshaled = NULL;
	____host0_marshaled = il2cpp_codegen_marshal_string(___host0);

	// Marshaling of parameter '___port1' to native representation
	char* ____port1_marshaled = NULL;
	____port1_marshaled = il2cpp_codegen_marshal_string(___port1);

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(getIPv6)(____host0_marshaled, ____port1_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	// Marshaling cleanup of parameter '___host0' native representation
	il2cpp_codegen_marshal_free(____host0_marshaled);
	____host0_marshaled = NULL;

	// Marshaling cleanup of parameter '___port1' native representation
	il2cpp_codegen_marshal_free(____port1_marshaled);
	____port1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
// System.String iOSipv6::GetIPv6(System.String,System.String)
extern "C"  String_t* iOSipv6_GetIPv6_m3731818845 (Il2CppObject * __this /* static, unused */, String_t* ___host0, String_t* ___port1, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___host0;
		String_t* L_1 = ___port1;
		String_t* L_2 = iOSipv6_getIPv6_m1716917565(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		return L_3;
	}
}
// System.Void iOSipv6::getIPType(System.String,System.String,System.String&,System.Net.Sockets.AddressFamily&)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Regex_t2161232213_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1216;
extern Il2CppCodeGenString* _stringLiteral3239399;
extern Il2CppCodeGenString* _stringLiteral3113064269;
extern const uint32_t iOSipv6_getIPType_m4179540682_MetadataUsageId;
extern "C"  void iOSipv6_getIPType_m4179540682 (Il2CppObject * __this /* static, unused */, String_t* ___serverIp0, String_t* ___serverPorts1, String_t** ___newServerIp2, int32_t* ___mIPType3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iOSipv6_getIPType_m4179540682_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_t4054002952* V_1 = NULL;
	String_t* V_2 = NULL;
	Exception_t3991598821 * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t* L_0 = ___mIPType3;
		*((int32_t*)(L_0)) = (int32_t)2;
		String_t** L_1 = ___newServerIp2;
		String_t* L_2 = ___serverIp0;
		*((Il2CppObject **)(L_1)) = (Il2CppObject *)L_2;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_1), (Il2CppObject *)L_2);
	}

IL_0006:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_3 = ___serverIp0;
			String_t* L_4 = ___serverPorts1;
			String_t* L_5 = iOSipv6_GetIPv6_m3731818845(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
			V_0 = L_5;
			String_t* L_6 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_7 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
			if (L_7)
			{
				goto IL_0051;
			}
		}

IL_0019:
		{
			String_t* L_8 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Regex_t2161232213_il2cpp_TypeInfo_var);
			StringU5BU5D_t4054002952* L_9 = Regex_Split_m3595096453(NULL /*static, unused*/, L_8, _stringLiteral1216, /*hidden argument*/NULL);
			V_1 = L_9;
			StringU5BU5D_t4054002952* L_10 = V_1;
			if (!L_10)
			{
				goto IL_0051;
			}
		}

IL_002b:
		{
			StringU5BU5D_t4054002952* L_11 = V_1;
			NullCheck(L_11);
			if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length))))) < ((int32_t)2)))
			{
				goto IL_0051;
			}
		}

IL_0034:
		{
			StringU5BU5D_t4054002952* L_12 = V_1;
			NullCheck(L_12);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
			int32_t L_13 = 1;
			String_t* L_14 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
			V_2 = L_14;
			String_t* L_15 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_16 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_15, _stringLiteral3239399, /*hidden argument*/NULL);
			if (!L_16)
			{
				goto IL_0051;
			}
		}

IL_0048:
		{
			String_t** L_17 = ___newServerIp2;
			StringU5BU5D_t4054002952* L_18 = V_1;
			NullCheck(L_18);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 0);
			int32_t L_19 = 0;
			String_t* L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
			*((Il2CppObject **)(L_17)) = (Il2CppObject *)L_20;
			Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_17), (Il2CppObject *)L_20);
			int32_t* L_21 = ___mIPType3;
			*((int32_t*)(L_21)) = (int32_t)((int32_t)23);
		}

IL_0051:
		{
			goto IL_0071;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0056;
		throw e;
	}

CATCH_0056:
	{ // begin catch(System.Exception)
		V_3 = ((Exception_t3991598821 *)__exception_local);
		Exception_t3991598821 * L_22 = V_3;
		NullCheck(L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(21 /* System.String System.Exception::get_Message() */, L_22);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3113064269, L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		goto IL_0071;
	} // end catch (depth: 1)

IL_0071:
	{
		return;
	}
}
// System.Void MotionBlur::.ctor()
extern "C"  void MotionBlur__ctor_m3060095378 (MotionBlur_t1272018269 * __this, const MethodInfo* method)
{
	{
		__this->set_blurAmount_4((0.8f));
		ImageEffectBase__ctor_m2296049466(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MotionBlur::Start()
extern "C"  void MotionBlur_Start_m2007233170 (MotionBlur_t1272018269 * __this, const MethodInfo* method)
{
	{
		bool L_0 = SystemInfo_get_supportsRenderTextures_m3098351893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_0012:
	{
		ImageEffectBase_Start_m1243187258(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MotionBlur::OnDisable()
extern "C"  void MotionBlur_OnDisable_m4162950393 (MotionBlur_t1272018269 * __this, const MethodInfo* method)
{
	{
		ImageEffectBase_OnDisable_m3305379489(__this, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_0 = __this->get_accumTexture_6();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MotionBlur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* RenderTexture_t1963041563_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral558922319;
extern Il2CppCodeGenString* _stringLiteral3146453787;
extern const uint32_t MotionBlur_OnRenderImage_m4048420748_MetadataUsageId;
extern "C"  void MotionBlur_OnRenderImage_m4048420748 (MotionBlur_t1272018269 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MotionBlur_OnRenderImage_m4048420748_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RenderTexture_t1963041563 * V_0 = NULL;
	{
		RenderTexture_t1963041563 * L_0 = __this->get_accumTexture_6();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_003d;
		}
	}
	{
		RenderTexture_t1963041563 * L_2 = __this->get_accumTexture_6();
		NullCheck(L_2);
		int32_t L_3 = RenderTexture_get_width_m1498578543(L_2, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_4 = ___source0;
		NullCheck(L_4);
		int32_t L_5 = RenderTexture_get_width_m1498578543(L_4, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)L_5))))
		{
			goto IL_003d;
		}
	}
	{
		RenderTexture_t1963041563 * L_6 = __this->get_accumTexture_6();
		NullCheck(L_6);
		int32_t L_7 = RenderTexture_get_height_m4010076224(L_6, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_8 = ___source0;
		NullCheck(L_8);
		int32_t L_9 = RenderTexture_get_height_m4010076224(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)L_9)))
		{
			goto IL_0079;
		}
	}

IL_003d:
	{
		RenderTexture_t1963041563 * L_10 = __this->get_accumTexture_6();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_11 = ___source0;
		NullCheck(L_11);
		int32_t L_12 = RenderTexture_get_width_m1498578543(L_11, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_13 = ___source0;
		NullCheck(L_13);
		int32_t L_14 = RenderTexture_get_height_m4010076224(L_13, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_15 = (RenderTexture_t1963041563 *)il2cpp_codegen_object_new(RenderTexture_t1963041563_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m591418693(L_15, L_12, L_14, 0, /*hidden argument*/NULL);
		__this->set_accumTexture_6(L_15);
		RenderTexture_t1963041563 * L_16 = __this->get_accumTexture_6();
		NullCheck(L_16);
		Object_set_hideFlags_m41317712(L_16, ((int32_t)61), /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_17 = ___source0;
		RenderTexture_t1963041563 * L_18 = __this->get_accumTexture_6();
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
	}

IL_0079:
	{
		bool L_19 = __this->get_extraBlur_5();
		if (!L_19)
		{
			goto IL_00c4;
		}
	}
	{
		RenderTexture_t1963041563 * L_20 = ___source0;
		NullCheck(L_20);
		int32_t L_21 = RenderTexture_get_width_m1498578543(L_20, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_22 = ___source0;
		NullCheck(L_22);
		int32_t L_23 = RenderTexture_get_height_m4010076224(L_22, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_24 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, ((int32_t)((int32_t)L_21/(int32_t)4)), ((int32_t)((int32_t)L_23/(int32_t)4)), 0, /*hidden argument*/NULL);
		V_0 = L_24;
		RenderTexture_t1963041563 * L_25 = __this->get_accumTexture_6();
		NullCheck(L_25);
		RenderTexture_MarkRestoreExpected_m2220245707(L_25, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_26 = __this->get_accumTexture_6();
		RenderTexture_t1963041563 * L_27 = V_0;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_28 = V_0;
		RenderTexture_t1963041563 * L_29 = __this->get_accumTexture_6();
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_30 = V_0;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
	}

IL_00c4:
	{
		float L_31 = __this->get_blurAmount_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_32 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_31, (0.0f), (0.92f), /*hidden argument*/NULL);
		__this->set_blurAmount_4(L_32);
		Material_t3870600107 * L_33 = ImageEffectBase_get_material_m2726075187(__this, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_34 = __this->get_accumTexture_6();
		NullCheck(L_33);
		Material_SetTexture_m1833724755(L_33, _stringLiteral558922319, L_34, /*hidden argument*/NULL);
		Material_t3870600107 * L_35 = ImageEffectBase_get_material_m2726075187(__this, /*hidden argument*/NULL);
		float L_36 = __this->get_blurAmount_4();
		NullCheck(L_35);
		Material_SetFloat_m981710063(L_35, _stringLiteral3146453787, ((float)((float)(1.0f)-(float)L_36)), /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_37 = __this->get_accumTexture_6();
		NullCheck(L_37);
		RenderTexture_MarkRestoreExpected_m2220245707(L_37, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_38 = ___source0;
		RenderTexture_t1963041563 * L_39 = __this->get_accumTexture_6();
		Material_t3870600107 * L_40 = ImageEffectBase_get_material_m2726075187(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_38, L_39, L_40, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_41 = __this->get_accumTexture_6();
		RenderTexture_t1963041563 * L_42 = ___destination1;
		Graphics_Blit_m3408836917(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NoiseEffect::.ctor()
extern "C"  void NoiseEffect__ctor_m2631375724 (NoiseEffect_t505428267 * __this, const MethodInfo* method)
{
	{
		__this->set_monochrome_2((bool)1);
		__this->set_grainIntensityMin_4((0.1f));
		__this->set_grainIntensityMax_5((0.2f));
		__this->set_grainSize_6((2.0f));
		__this->set_scratchIntensityMin_7((0.05f));
		__this->set_scratchIntensityMax_8((0.25f));
		__this->set_scratchFPS_9((10.0f));
		__this->set_scratchJitter_10((0.01f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NoiseEffect::Start()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2007177011;
extern const uint32_t NoiseEffect_Start_m1578513516_MetadataUsageId;
extern "C"  void NoiseEffect_Start_m1578513516 (NoiseEffect_t505428267 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NoiseEffect_Start_m1578513516_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = SystemInfo_get_supportsImageEffects_m2392300814(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_0012:
	{
		Shader_t3191267369 * L_1 = __this->get_shaderRGB_13();
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0034;
		}
	}
	{
		Shader_t3191267369 * L_3 = __this->get_shaderYUV_14();
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004a;
		}
	}

IL_0034:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2007177011, /*hidden argument*/NULL);
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		goto IL_007d;
	}

IL_004a:
	{
		Shader_t3191267369 * L_5 = __this->get_shaderRGB_13();
		NullCheck(L_5);
		bool L_6 = Shader_get_isSupported_m1422621179(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0066;
		}
	}
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		goto IL_007d;
	}

IL_0066:
	{
		Shader_t3191267369 * L_7 = __this->get_shaderYUV_14();
		NullCheck(L_7);
		bool L_8 = Shader_get_isSupported_m1422621179(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_007d;
		}
	}
	{
		__this->set_rgbFallback_3((bool)1);
	}

IL_007d:
	{
		return;
	}
}
// UnityEngine.Material NoiseEffect::get_material()
extern Il2CppClass* Material_t3870600107_il2cpp_TypeInfo_var;
extern const uint32_t NoiseEffect_get_material_m738060097_MetadataUsageId;
extern "C"  Material_t3870600107 * NoiseEffect_get_material_m738060097 (NoiseEffect_t505428267 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NoiseEffect_get_material_m738060097_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Material_t3870600107 * G_B9_0 = NULL;
	{
		Material_t3870600107 * L_0 = __this->get_m_MaterialRGB_15();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t3191267369 * L_2 = __this->get_shaderRGB_13();
		Material_t3870600107 * L_3 = (Material_t3870600107 *)il2cpp_codegen_object_new(Material_t3870600107_il2cpp_TypeInfo_var);
		Material__ctor_m2685909642(L_3, L_2, /*hidden argument*/NULL);
		__this->set_m_MaterialRGB_15(L_3);
		Material_t3870600107 * L_4 = __this->get_m_MaterialRGB_15();
		NullCheck(L_4);
		Object_set_hideFlags_m41317712(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t3870600107 * L_5 = __this->get_m_MaterialYUV_16();
		bool L_6 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_5, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0069;
		}
	}
	{
		bool L_7 = __this->get_rgbFallback_3();
		if (L_7)
		{
			goto IL_0069;
		}
	}
	{
		Shader_t3191267369 * L_8 = __this->get_shaderYUV_14();
		Material_t3870600107 * L_9 = (Material_t3870600107 *)il2cpp_codegen_object_new(Material_t3870600107_il2cpp_TypeInfo_var);
		Material__ctor_m2685909642(L_9, L_8, /*hidden argument*/NULL);
		__this->set_m_MaterialYUV_16(L_9);
		Material_t3870600107 * L_10 = __this->get_m_MaterialYUV_16();
		NullCheck(L_10);
		Object_set_hideFlags_m41317712(L_10, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_0069:
	{
		bool L_11 = __this->get_rgbFallback_3();
		if (L_11)
		{
			goto IL_008a;
		}
	}
	{
		bool L_12 = __this->get_monochrome_2();
		if (L_12)
		{
			goto IL_008a;
		}
	}
	{
		Material_t3870600107 * L_13 = __this->get_m_MaterialYUV_16();
		G_B9_0 = L_13;
		goto IL_0090;
	}

IL_008a:
	{
		Material_t3870600107 * L_14 = __this->get_m_MaterialRGB_15();
		G_B9_0 = L_14;
	}

IL_0090:
	{
		return G_B9_0;
	}
}
// System.Void NoiseEffect::OnDisable()
extern "C"  void NoiseEffect_OnDisable_m4119550419 (NoiseEffect_t505428267 * __this, const MethodInfo* method)
{
	{
		Material_t3870600107 * L_0 = __this->get_m_MaterialRGB_15();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t3870600107 * L_2 = __this->get_m_MaterialRGB_15();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		Material_t3870600107 * L_3 = __this->get_m_MaterialYUV_16();
		bool L_4 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		Material_t3870600107 * L_5 = __this->get_m_MaterialYUV_16();
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0036:
	{
		return;
	}
}
// System.Void NoiseEffect::SanitizeParameters()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t NoiseEffect_SanitizeParameters_m4257769099_MetadataUsageId;
extern "C"  void NoiseEffect_SanitizeParameters_m4257769099 (NoiseEffect_t505428267 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NoiseEffect_SanitizeParameters_m4257769099_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_grainIntensityMin_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_0, (0.0f), (5.0f), /*hidden argument*/NULL);
		__this->set_grainIntensityMin_4(L_1);
		float L_2 = __this->get_grainIntensityMax_5();
		float L_3 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_2, (0.0f), (5.0f), /*hidden argument*/NULL);
		__this->set_grainIntensityMax_5(L_3);
		float L_4 = __this->get_scratchIntensityMin_7();
		float L_5 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_4, (0.0f), (5.0f), /*hidden argument*/NULL);
		__this->set_scratchIntensityMin_7(L_5);
		float L_6 = __this->get_scratchIntensityMax_8();
		float L_7 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_6, (0.0f), (5.0f), /*hidden argument*/NULL);
		__this->set_scratchIntensityMax_8(L_7);
		float L_8 = __this->get_scratchFPS_9();
		float L_9 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_8, (1.0f), (30.0f), /*hidden argument*/NULL);
		__this->set_scratchFPS_9(L_9);
		float L_10 = __this->get_scratchJitter_10();
		float L_11 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_10, (0.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_scratchJitter_10(L_11);
		float L_12 = __this->get_grainSize_6();
		float L_13 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_12, (0.1f), (50.0f), /*hidden argument*/NULL);
		__this->set_grainSize_6(L_13);
		return;
	}
}
// System.Void NoiseEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral249562923;
extern Il2CppCodeGenString* _stringLiteral3829214348;
extern Il2CppCodeGenString* _stringLiteral3893926779;
extern Il2CppCodeGenString* _stringLiteral577687516;
extern Il2CppCodeGenString* _stringLiteral1014379156;
extern const uint32_t NoiseEffect_OnRenderImage_m4051324274_MetadataUsageId;
extern "C"  void NoiseEffect_OnRenderImage_m4051324274 (NoiseEffect_t505428267 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NoiseEffect_OnRenderImage_m4051324274_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Material_t3870600107 * V_0 = NULL;
	float V_1 = 0.0f;
	{
		NoiseEffect_SanitizeParameters_m4257769099(__this, /*hidden argument*/NULL);
		float L_0 = __this->get_scratchTimeLeft_17();
		if ((!(((float)L_0) <= ((float)(0.0f)))))
		{
			goto IL_0044;
		}
	}
	{
		float L_1 = Random_get_value_m2402066692(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = __this->get_scratchFPS_9();
		__this->set_scratchTimeLeft_17(((float)((float)((float)((float)L_1*(float)(2.0f)))/(float)L_2)));
		float L_3 = Random_get_value_m2402066692(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_scratchX_18(L_3);
		float L_4 = Random_get_value_m2402066692(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_scratchY_19(L_4);
	}

IL_0044:
	{
		float L_5 = __this->get_scratchTimeLeft_17();
		float L_6 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_scratchTimeLeft_17(((float)((float)L_5-(float)L_6)));
		Material_t3870600107 * L_7 = NoiseEffect_get_material_m738060097(__this, /*hidden argument*/NULL);
		V_0 = L_7;
		Material_t3870600107 * L_8 = V_0;
		Texture_t2526458961 * L_9 = __this->get_grainTexture_11();
		NullCheck(L_8);
		Material_SetTexture_m1833724755(L_8, _stringLiteral249562923, L_9, /*hidden argument*/NULL);
		Material_t3870600107 * L_10 = V_0;
		Texture_t2526458961 * L_11 = __this->get_scratchTexture_12();
		NullCheck(L_10);
		Material_SetTexture_m1833724755(L_10, _stringLiteral3829214348, L_11, /*hidden argument*/NULL);
		float L_12 = __this->get_grainSize_6();
		V_1 = ((float)((float)(1.0f)/(float)L_12));
		Material_t3870600107 * L_13 = V_0;
		float L_14 = Random_get_value_m2402066692(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_15 = Random_get_value_m2402066692(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_16 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Texture_t2526458961 * L_17 = __this->get_grainTexture_11();
		NullCheck(L_17);
		int32_t L_18 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_17);
		float L_19 = V_1;
		int32_t L_20 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Texture_t2526458961 * L_21 = __this->get_grainTexture_11();
		NullCheck(L_21);
		int32_t L_22 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_21);
		float L_23 = V_1;
		Vector4_t4282066567  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector4__ctor_m2441427762(&L_24, L_14, L_15, ((float)((float)((float)((float)(((float)((float)L_16)))/(float)(((float)((float)L_18)))))*(float)L_19)), ((float)((float)((float)((float)(((float)((float)L_20)))/(float)(((float)((float)L_22)))))*(float)L_23)), /*hidden argument*/NULL);
		NullCheck(L_13);
		Material_SetVector_m3505096203(L_13, _stringLiteral3893926779, L_24, /*hidden argument*/NULL);
		Material_t3870600107 * L_25 = V_0;
		float L_26 = __this->get_scratchX_18();
		float L_27 = Random_get_value_m2402066692(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_28 = __this->get_scratchJitter_10();
		float L_29 = __this->get_scratchY_19();
		float L_30 = Random_get_value_m2402066692(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_31 = __this->get_scratchJitter_10();
		int32_t L_32 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Texture_t2526458961 * L_33 = __this->get_scratchTexture_12();
		NullCheck(L_33);
		int32_t L_34 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_33);
		int32_t L_35 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Texture_t2526458961 * L_36 = __this->get_scratchTexture_12();
		NullCheck(L_36);
		int32_t L_37 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_36);
		Vector4_t4282066567  L_38;
		memset(&L_38, 0, sizeof(L_38));
		Vector4__ctor_m2441427762(&L_38, ((float)((float)L_26+(float)((float)((float)L_27*(float)L_28)))), ((float)((float)L_29+(float)((float)((float)L_30*(float)L_31)))), ((float)((float)(((float)((float)L_32)))/(float)(((float)((float)L_34))))), ((float)((float)(((float)((float)L_35)))/(float)(((float)((float)L_37))))), /*hidden argument*/NULL);
		NullCheck(L_25);
		Material_SetVector_m3505096203(L_25, _stringLiteral577687516, L_38, /*hidden argument*/NULL);
		Material_t3870600107 * L_39 = V_0;
		float L_40 = __this->get_grainIntensityMin_4();
		float L_41 = __this->get_grainIntensityMax_5();
		float L_42 = Random_Range_m3362417303(NULL /*static, unused*/, L_40, L_41, /*hidden argument*/NULL);
		float L_43 = __this->get_scratchIntensityMin_7();
		float L_44 = __this->get_scratchIntensityMax_8();
		float L_45 = Random_Range_m3362417303(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		Vector4_t4282066567  L_46;
		memset(&L_46, 0, sizeof(L_46));
		Vector4__ctor_m2441427762(&L_46, L_42, L_45, (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_39);
		Material_SetVector_m3505096203(L_39, _stringLiteral1014379156, L_46, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_47 = ___source0;
		RenderTexture_t1963041563 * L_48 = ___destination1;
		Material_t3870600107 * L_49 = V_0;
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_47, L_48, L_49, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Nova.PersistPathMgr::.ctor()
extern "C"  void PersistPathMgr__ctor_m2564585494 (PersistPathMgr_t636044869 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Nova.PersistPathMgr::.cctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* PersistPathMgr_t636044869_il2cpp_TypeInfo_var;
extern const uint32_t PersistPathMgr__cctor_m1710642775_MetadataUsageId;
extern "C"  void PersistPathMgr__cctor_m1710642775 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PersistPathMgr__cctor_m1710642775_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		((PersistPathMgr_t636044869_StaticFields*)PersistPathMgr_t636044869_il2cpp_TypeInfo_var->static_fields)->set_s_persistentDataPath_0(L_0);
		return;
	}
}
// System.String Nova.PersistPathMgr::GetPersistentDataPath()
extern Il2CppClass* PersistPathMgr_t636044869_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PersistPathMgr_GetPersistentDataPath_m4140785523_MetadataUsageId;
extern "C"  String_t* PersistPathMgr_GetPersistentDataPath_m4140785523 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PersistPathMgr_GetPersistentDataPath_m4140785523_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PersistPathMgr_t636044869_il2cpp_TypeInfo_var);
		String_t* L_0 = ((PersistPathMgr_t636044869_StaticFields*)PersistPathMgr_t636044869_il2cpp_TypeInfo_var->static_fields)->get_s_persistentDataPath_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0037;
		}
	}
	{
		String_t* L_2 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PersistPathMgr_t636044869_il2cpp_TypeInfo_var);
		String_t* L_4 = PersistPathMgr_InternalGetPersistentDataPath_m3594057206(NULL /*static, unused*/, /*hidden argument*/NULL);
		((PersistPathMgr_t636044869_StaticFields*)PersistPathMgr_t636044869_il2cpp_TypeInfo_var->static_fields)->set_s_persistentDataPath_0(L_4);
		goto IL_0037;
	}

IL_002d:
	{
		String_t* L_5 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PersistPathMgr_t636044869_il2cpp_TypeInfo_var);
		((PersistPathMgr_t636044869_StaticFields*)PersistPathMgr_t636044869_il2cpp_TypeInfo_var->static_fields)->set_s_persistentDataPath_0(L_5);
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PersistPathMgr_t636044869_il2cpp_TypeInfo_var);
		String_t* L_6 = ((PersistPathMgr_t636044869_StaticFields*)PersistPathMgr_t636044869_il2cpp_TypeInfo_var->static_fields)->get_s_persistentDataPath_0();
		return L_6;
	}
}
// System.String Nova.PersistPathMgr::InternalGetPersistentDataPath()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaClass_t1816259147_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const MethodInfo* AndroidJavaObject_CallStatic_TisString_t_m4016486641_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1564156859;
extern Il2CppCodeGenString* _stringLiteral119774211;
extern Il2CppCodeGenString* _stringLiteral4004558545;
extern const uint32_t PersistPathMgr_InternalGetPersistentDataPath_m3594057206_MetadataUsageId;
extern "C"  String_t* PersistPathMgr_InternalGetPersistentDataPath_m3594057206 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PersistPathMgr_InternalGetPersistentDataPath_m3594057206_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	AndroidJavaClass_t1816259147 * V_1 = NULL;
	bool V_2 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B8_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		AndroidJavaClass_t1816259147 * L_1 = (AndroidJavaClass_t1816259147 *)il2cpp_codegen_object_new(AndroidJavaClass_t1816259147_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m2757518396(L_1, _stringLiteral1564156859, /*hidden argument*/NULL);
		V_1 = L_1;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			AndroidJavaClass_t1816259147 * L_2 = V_1;
			NullCheck(L_2);
			String_t* L_3 = AndroidJavaObject_CallStatic_TisString_t_m4016486641(L_2, _stringLiteral119774211, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/AndroidJavaObject_CallStatic_TisString_t_m4016486641_MethodInfo_var);
			V_0 = L_3;
			String_t* L_4 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_5 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_004b;
			}
		}

IL_002e:
		{
			String_t* L_6 = V_0;
			bool L_7 = Directory_Exists_m4117375188(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
			if (L_7)
			{
				goto IL_004b;
			}
		}

IL_0039:
		try
		{ // begin try (depth: 2)
			String_t* L_8 = V_0;
			Directory_CreateDirectory_m677877474(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			goto IL_004b;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t3991598821 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0045;
			throw e;
		}

CATCH_0045:
		{ // begin catch(System.Object)
			goto IL_004b;
		} // end catch (depth: 2)

IL_004b:
		{
			String_t* L_9 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_10 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
			if (L_10)
			{
				goto IL_005e;
			}
		}

IL_0056:
		{
			String_t* L_11 = V_0;
			bool L_12 = Directory_Exists_m4117375188(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
			G_B8_0 = ((int32_t)(L_12));
			goto IL_005f;
		}

IL_005e:
		{
			G_B8_0 = 0;
		}

IL_005f:
		{
			V_2 = (bool)G_B8_0;
			bool L_13 = V_2;
			if (L_13)
			{
				goto IL_0078;
			}
		}

IL_0066:
		{
			AndroidJavaClass_t1816259147 * L_14 = V_1;
			NullCheck(L_14);
			String_t* L_15 = AndroidJavaObject_CallStatic_TisString_t_m4016486641(L_14, _stringLiteral4004558545, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/AndroidJavaObject_CallStatic_TisString_t_m4016486641_MethodInfo_var);
			V_0 = L_15;
		}

IL_0078:
		{
			IL2CPP_LEAVE(0x8A, FINALLY_007d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_007d;
	}

FINALLY_007d:
	{ // begin finally (depth: 1)
		{
			AndroidJavaClass_t1816259147 * L_16 = V_1;
			if (!L_16)
			{
				goto IL_0089;
			}
		}

IL_0083:
		{
			AndroidJavaClass_t1816259147 * L_17 = V_1;
			NullCheck(L_17);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_17);
		}

IL_0089:
		{
			IL2CPP_END_FINALLY(125)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(125)
	{
		IL2CPP_JUMP_TBL(0x8A, IL_008a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_008a:
	{
		String_t* L_18 = V_0;
		return L_18;
	}
}
// System.String Nova.PersistPathMgr::get_persistentDataPath_fromC()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* PersistPathMgr_t636044869_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3385512479;
extern Il2CppCodeGenString* _stringLiteral2701074741;
extern const uint32_t PersistPathMgr_get_persistentDataPath_fromC_m798234836_MetadataUsageId;
extern "C"  String_t* PersistPathMgr_get_persistentDataPath_fromC_m798234836 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PersistPathMgr_get_persistentDataPath_fromC_m798234836_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral3385512479, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PersistPathMgr_t636044869_il2cpp_TypeInfo_var);
		String_t* L_0 = PersistPathMgr_GetPersistentDataPath_m4140785523(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, L_0, _stringLiteral2701074741, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		bool L_3 = Directory_Exists_m4117375188(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_002c;
		}
	}
	{
		String_t* L_4 = V_0;
		Directory_CreateDirectory_m677877474(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_002c:
	{
		String_t* L_5 = V_0;
		return L_5;
	}
}
// System.Void SepiaToneEffect::.ctor()
extern "C"  void SepiaToneEffect__ctor_m2963821118 (SepiaToneEffect_t2573399065 * __this, const MethodInfo* method)
{
	{
		ImageEffectBase__ctor_m2296049466(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SepiaToneEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void SepiaToneEffect_OnRenderImage_m3713837920 (SepiaToneEffect_t2573399065 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	{
		RenderTexture_t1963041563 * L_0 = ___source0;
		RenderTexture_t1963041563 * L_1 = ___destination1;
		Material_t3870600107 * L_2 = ImageEffectBase_get_material_m2726075187(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2695454291(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SSAOEffect::.ctor()
extern "C"  void SSAOEffect__ctor_m99164016 (SSAOEffect_t1959001535 * __this, const MethodInfo* method)
{
	{
		__this->set_m_Radius_2((0.4f));
		__this->set_m_SampleCount_3(1);
		__this->set_m_OcclusionIntensity_4((1.5f));
		__this->set_m_Blur_5(2);
		__this->set_m_Downsampling_6(2);
		__this->set_m_OcclusionAttenuation_7((1.0f));
		__this->set_m_MinZ_8((0.01f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material SSAOEffect::CreateMaterial(UnityEngine.Shader)
extern Il2CppClass* Material_t3870600107_il2cpp_TypeInfo_var;
extern const uint32_t SSAOEffect_CreateMaterial_m1199633154_MetadataUsageId;
extern "C"  Material_t3870600107 * SSAOEffect_CreateMaterial_m1199633154 (Il2CppObject * __this /* static, unused */, Shader_t3191267369 * ___shader0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SSAOEffect_CreateMaterial_m1199633154_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Material_t3870600107 * V_0 = NULL;
	{
		Shader_t3191267369 * L_0 = ___shader0;
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (Material_t3870600107 *)NULL;
	}

IL_000d:
	{
		Shader_t3191267369 * L_2 = ___shader0;
		Material_t3870600107 * L_3 = (Material_t3870600107 *)il2cpp_codegen_object_new(Material_t3870600107_il2cpp_TypeInfo_var);
		Material__ctor_m2685909642(L_3, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Material_t3870600107 * L_4 = V_0;
		NullCheck(L_4);
		Object_set_hideFlags_m41317712(L_4, ((int32_t)61), /*hidden argument*/NULL);
		Material_t3870600107 * L_5 = V_0;
		return L_5;
	}
}
// System.Void SSAOEffect::DestroyMaterial(UnityEngine.Material)
extern "C"  void SSAOEffect_DestroyMaterial_m4059222193 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___mat0, const MethodInfo* method)
{
	{
		Material_t3870600107 * L_0 = ___mat0;
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		Material_t3870600107 * L_2 = ___mat0;
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		___mat0 = (Material_t3870600107 *)NULL;
	}

IL_0014:
	{
		return;
	}
}
// System.Void SSAOEffect::OnDisable()
extern "C"  void SSAOEffect_OnDisable_m2993896407 (SSAOEffect_t1959001535 * __this, const MethodInfo* method)
{
	{
		Material_t3870600107 * L_0 = __this->get_m_SSAOMaterial_10();
		SSAOEffect_DestroyMaterial_m4059222193(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SSAOEffect::Start()
extern "C"  void SSAOEffect_Start_m3341269104 (SSAOEffect_t1959001535 * __this, const MethodInfo* method)
{
	{
		bool L_0 = SystemInfo_get_supportsImageEffects_m2392300814(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		bool L_1 = SystemInfo_SupportsRenderTextureFormat_m1773213581(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0024;
		}
	}

IL_0015:
	{
		__this->set_m_Supported_12((bool)0);
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_0024:
	{
		SSAOEffect_CreateMaterials_m675868382(__this, /*hidden argument*/NULL);
		Material_t3870600107 * L_2 = __this->get_m_SSAOMaterial_10();
		bool L_3 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004b;
		}
	}
	{
		Material_t3870600107 * L_4 = __this->get_m_SSAOMaterial_10();
		NullCheck(L_4);
		int32_t L_5 = Material_get_passCount_m2926759545(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)5)))
		{
			goto IL_005a;
		}
	}

IL_004b:
	{
		__this->set_m_Supported_12((bool)0);
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_005a:
	{
		__this->set_m_Supported_12((bool)1);
		return;
	}
}
// System.Void SSAOEffect::OnEnable()
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern const uint32_t SSAOEffect_OnEnable_m1329279510_MetadataUsageId;
extern "C"  void SSAOEffect_OnEnable_m1329279510 (SSAOEffect_t1959001535 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SSAOEffect_OnEnable_m1329279510_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t2727095145 * L_0 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		Camera_t2727095145 * L_1 = L_0;
		NullCheck(L_1);
		int32_t L_2 = Camera_get_depthTextureMode_m2117446653(L_1, /*hidden argument*/NULL);
		NullCheck(L_1);
		Camera_set_depthTextureMode_m2368326786(L_1, ((int32_t)((int32_t)L_2|(int32_t)2)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SSAOEffect::CreateMaterials()
extern Il2CppCodeGenString* _stringLiteral466434233;
extern const uint32_t SSAOEffect_CreateMaterials_m675868382_MetadataUsageId;
extern "C"  void SSAOEffect_CreateMaterials_m675868382 (SSAOEffect_t1959001535 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SSAOEffect_CreateMaterials_m675868382_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t3870600107 * L_0 = __this->get_m_SSAOMaterial_10();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0047;
		}
	}
	{
		Shader_t3191267369 * L_2 = __this->get_m_SSAOShader_9();
		NullCheck(L_2);
		bool L_3 = Shader_get_isSupported_m1422621179(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0047;
		}
	}
	{
		Shader_t3191267369 * L_4 = __this->get_m_SSAOShader_9();
		Material_t3870600107 * L_5 = SSAOEffect_CreateMaterial_m1199633154(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_m_SSAOMaterial_10(L_5);
		Material_t3870600107 * L_6 = __this->get_m_SSAOMaterial_10();
		Texture2D_t3884108195 * L_7 = __this->get_m_RandomTexture_11();
		NullCheck(L_6);
		Material_SetTexture_m1833724755(L_6, _stringLiteral466434233, L_7, /*hidden argument*/NULL);
	}

IL_0047:
	{
		return;
	}
}
// System.Void SSAOEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2507544237;
extern Il2CppCodeGenString* _stringLiteral147620879;
extern Il2CppCodeGenString* _stringLiteral796910277;
extern Il2CppCodeGenString* _stringLiteral1306145544;
extern Il2CppCodeGenString* _stringLiteral90289005;
extern const uint32_t SSAOEffect_OnRenderImage_m1991603950_MetadataUsageId;
extern "C"  void SSAOEffect_OnRenderImage_m1991603950 (SSAOEffect_t1959001535 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SSAOEffect_OnRenderImage_m1991603950_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RenderTexture_t1963041563 * V_0 = NULL;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	bool V_7 = false;
	RenderTexture_t1963041563 * V_8 = NULL;
	RenderTexture_t1963041563 * V_9 = NULL;
	RenderTexture_t1963041563 * G_B9_0 = NULL;
	{
		bool L_0 = __this->get_m_Supported_12();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Shader_t3191267369 * L_1 = __this->get_m_SSAOShader_9();
		NullCheck(L_1);
		bool L_2 = Shader_get_isSupported_m1422621179(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0023;
		}
	}

IL_001b:
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_0023:
	{
		SSAOEffect_CreateMaterials_m675868382(__this, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_m_Downsampling_6();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_Clamp_m510460741(NULL /*static, unused*/, L_3, 1, 6, /*hidden argument*/NULL);
		__this->set_m_Downsampling_6(L_4);
		float L_5 = __this->get_m_Radius_2();
		float L_6 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_5, (0.05f), (1.0f), /*hidden argument*/NULL);
		__this->set_m_Radius_2(L_6);
		float L_7 = __this->get_m_MinZ_8();
		float L_8 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_7, (1.0E-05f), (0.5f), /*hidden argument*/NULL);
		__this->set_m_MinZ_8(L_8);
		float L_9 = __this->get_m_OcclusionIntensity_4();
		float L_10 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_9, (0.5f), (4.0f), /*hidden argument*/NULL);
		__this->set_m_OcclusionIntensity_4(L_10);
		float L_11 = __this->get_m_OcclusionAttenuation_7();
		float L_12 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_11, (0.2f), (2.0f), /*hidden argument*/NULL);
		__this->set_m_OcclusionAttenuation_7(L_12);
		int32_t L_13 = __this->get_m_Blur_5();
		int32_t L_14 = Mathf_Clamp_m510460741(NULL /*static, unused*/, L_13, 0, 4, /*hidden argument*/NULL);
		__this->set_m_Blur_5(L_14);
		RenderTexture_t1963041563 * L_15 = ___source0;
		NullCheck(L_15);
		int32_t L_16 = RenderTexture_get_width_m1498578543(L_15, /*hidden argument*/NULL);
		int32_t L_17 = __this->get_m_Downsampling_6();
		RenderTexture_t1963041563 * L_18 = ___source0;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m4010076224(L_18, /*hidden argument*/NULL);
		int32_t L_20 = __this->get_m_Downsampling_6();
		RenderTexture_t1963041563 * L_21 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, ((int32_t)((int32_t)L_16/(int32_t)L_17)), ((int32_t)((int32_t)L_19/(int32_t)L_20)), 0, /*hidden argument*/NULL);
		V_0 = L_21;
		Camera_t2727095145 * L_22 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_22);
		float L_23 = Camera_get_fieldOfView_m65126887(L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		Camera_t2727095145 * L_24 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_24);
		float L_25 = Camera_get_farClipPlane_m388706726(L_24, /*hidden argument*/NULL);
		V_2 = L_25;
		float L_26 = V_1;
		float L_27 = tanf(((float)((float)((float)((float)L_26*(float)(0.0174532924f)))*(float)(0.5f))));
		float L_28 = V_2;
		V_3 = ((float)((float)L_27*(float)L_28));
		float L_29 = V_3;
		Camera_t2727095145 * L_30 = Component_GetComponent_TisCamera_t2727095145_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2727095145_m3804104198_MethodInfo_var);
		NullCheck(L_30);
		float L_31 = Camera_get_aspect_m4145685929(L_30, /*hidden argument*/NULL);
		V_4 = ((float)((float)L_29*(float)L_31));
		Material_t3870600107 * L_32 = __this->get_m_SSAOMaterial_10();
		float L_33 = V_4;
		float L_34 = V_3;
		float L_35 = V_2;
		Vector3_t4282066566  L_36;
		memset(&L_36, 0, sizeof(L_36));
		Vector3__ctor_m2926210380(&L_36, L_33, L_34, L_35, /*hidden argument*/NULL);
		Vector4_t4282066567  L_37 = Vector4_op_Implicit_m331673271(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		NullCheck(L_32);
		Material_SetVector_m3505096203(L_32, _stringLiteral2507544237, L_37, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_38 = __this->get_m_RandomTexture_11();
		bool L_39 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0165;
		}
	}
	{
		Texture2D_t3884108195 * L_40 = __this->get_m_RandomTexture_11();
		NullCheck(L_40);
		int32_t L_41 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_40);
		V_5 = L_41;
		Texture2D_t3884108195 * L_42 = __this->get_m_RandomTexture_11();
		NullCheck(L_42);
		int32_t L_43 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_42);
		V_6 = L_43;
		goto IL_016b;
	}

IL_0165:
	{
		V_5 = 1;
		V_6 = 1;
	}

IL_016b:
	{
		Material_t3870600107 * L_44 = __this->get_m_SSAOMaterial_10();
		RenderTexture_t1963041563 * L_45 = V_0;
		NullCheck(L_45);
		int32_t L_46 = RenderTexture_get_width_m1498578543(L_45, /*hidden argument*/NULL);
		int32_t L_47 = V_5;
		RenderTexture_t1963041563 * L_48 = V_0;
		NullCheck(L_48);
		int32_t L_49 = RenderTexture_get_height_m4010076224(L_48, /*hidden argument*/NULL);
		int32_t L_50 = V_6;
		Vector3_t4282066566  L_51;
		memset(&L_51, 0, sizeof(L_51));
		Vector3__ctor_m2926210380(&L_51, ((float)((float)(((float)((float)L_46)))/(float)(((float)((float)L_47))))), ((float)((float)(((float)((float)L_49)))/(float)(((float)((float)L_50))))), (0.0f), /*hidden argument*/NULL);
		Vector4_t4282066567  L_52 = Vector4_op_Implicit_m331673271(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		NullCheck(L_44);
		Material_SetVector_m3505096203(L_44, _stringLiteral147620879, L_52, /*hidden argument*/NULL);
		Material_t3870600107 * L_53 = __this->get_m_SSAOMaterial_10();
		float L_54 = __this->get_m_Radius_2();
		float L_55 = __this->get_m_MinZ_8();
		float L_56 = __this->get_m_OcclusionAttenuation_7();
		float L_57 = __this->get_m_OcclusionIntensity_4();
		Vector4_t4282066567  L_58;
		memset(&L_58, 0, sizeof(L_58));
		Vector4__ctor_m2441427762(&L_58, L_54, L_55, ((float)((float)(1.0f)/(float)L_56)), L_57, /*hidden argument*/NULL);
		NullCheck(L_53);
		Material_SetVector_m3505096203(L_53, _stringLiteral796910277, L_58, /*hidden argument*/NULL);
		int32_t L_59 = __this->get_m_Blur_5();
		V_7 = (bool)((((int32_t)L_59) > ((int32_t)0))? 1 : 0);
		bool L_60 = V_7;
		if (!L_60)
		{
			goto IL_01eb;
		}
	}
	{
		G_B9_0 = ((RenderTexture_t1963041563 *)(NULL));
		goto IL_01ec;
	}

IL_01eb:
	{
		RenderTexture_t1963041563 * L_61 = ___source0;
		G_B9_0 = L_61;
	}

IL_01ec:
	{
		RenderTexture_t1963041563 * L_62 = V_0;
		Material_t3870600107 * L_63 = __this->get_m_SSAOMaterial_10();
		int32_t L_64 = __this->get_m_SampleCount_3();
		Graphics_Blit_m336256356(NULL /*static, unused*/, G_B9_0, L_62, L_63, L_64, /*hidden argument*/NULL);
		bool L_65 = V_7;
		if (!L_65)
		{
			goto IL_02e4;
		}
	}
	{
		RenderTexture_t1963041563 * L_66 = ___source0;
		NullCheck(L_66);
		int32_t L_67 = RenderTexture_get_width_m1498578543(L_66, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_68 = ___source0;
		NullCheck(L_68);
		int32_t L_69 = RenderTexture_get_height_m4010076224(L_68, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_70 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, L_67, L_69, 0, /*hidden argument*/NULL);
		V_8 = L_70;
		Material_t3870600107 * L_71 = __this->get_m_SSAOMaterial_10();
		int32_t L_72 = __this->get_m_Blur_5();
		RenderTexture_t1963041563 * L_73 = ___source0;
		NullCheck(L_73);
		int32_t L_74 = RenderTexture_get_width_m1498578543(L_73, /*hidden argument*/NULL);
		Vector4_t4282066567  L_75;
		memset(&L_75, 0, sizeof(L_75));
		Vector4__ctor_m2441427762(&L_75, ((float)((float)(((float)((float)L_72)))/(float)(((float)((float)L_74))))), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_71);
		Material_SetVector_m3505096203(L_71, _stringLiteral1306145544, L_75, /*hidden argument*/NULL);
		Material_t3870600107 * L_76 = __this->get_m_SSAOMaterial_10();
		RenderTexture_t1963041563 * L_77 = V_0;
		NullCheck(L_76);
		Material_SetTexture_m1833724755(L_76, _stringLiteral90289005, L_77, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_78 = V_8;
		Material_t3870600107 * L_79 = __this->get_m_SSAOMaterial_10();
		Graphics_Blit_m336256356(NULL /*static, unused*/, (Texture_t2526458961 *)NULL, L_78, L_79, 3, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_80 = V_0;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_80, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_81 = ___source0;
		NullCheck(L_81);
		int32_t L_82 = RenderTexture_get_width_m1498578543(L_81, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_83 = ___source0;
		NullCheck(L_83);
		int32_t L_84 = RenderTexture_get_height_m4010076224(L_83, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_85 = RenderTexture_GetTemporary_m52998487(NULL /*static, unused*/, L_82, L_84, 0, /*hidden argument*/NULL);
		V_9 = L_85;
		Material_t3870600107 * L_86 = __this->get_m_SSAOMaterial_10();
		int32_t L_87 = __this->get_m_Blur_5();
		RenderTexture_t1963041563 * L_88 = ___source0;
		NullCheck(L_88);
		int32_t L_89 = RenderTexture_get_height_m4010076224(L_88, /*hidden argument*/NULL);
		Vector4_t4282066567  L_90;
		memset(&L_90, 0, sizeof(L_90));
		Vector4__ctor_m2441427762(&L_90, (0.0f), ((float)((float)(((float)((float)L_87)))/(float)(((float)((float)L_89))))), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_86);
		Material_SetVector_m3505096203(L_86, _stringLiteral1306145544, L_90, /*hidden argument*/NULL);
		Material_t3870600107 * L_91 = __this->get_m_SSAOMaterial_10();
		RenderTexture_t1963041563 * L_92 = V_8;
		NullCheck(L_91);
		Material_SetTexture_m1833724755(L_91, _stringLiteral90289005, L_92, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_93 = ___source0;
		RenderTexture_t1963041563 * L_94 = V_9;
		Material_t3870600107 * L_95 = __this->get_m_SSAOMaterial_10();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_93, L_94, L_95, 3, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_96 = V_8;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_96, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_97 = V_9;
		V_0 = L_97;
	}

IL_02e4:
	{
		Material_t3870600107 * L_98 = __this->get_m_SSAOMaterial_10();
		RenderTexture_t1963041563 * L_99 = V_0;
		NullCheck(L_98);
		Material_SetTexture_m1833724755(L_98, _stringLiteral90289005, L_99, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_100 = ___source0;
		RenderTexture_t1963041563 * L_101 = ___destination1;
		Material_t3870600107 * L_102 = __this->get_m_SSAOMaterial_10();
		Graphics_Blit_m336256356(NULL /*static, unused*/, L_100, L_101, L_102, 4, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_103 = V_0;
		RenderTexture_ReleaseTemporary_m3045961066(NULL /*static, unused*/, L_103, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwirlEffect::.ctor()
extern "C"  void TwirlEffect__ctor_m1380508358 (TwirlEffect_t1211262097 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m1517109030(&L_0, (0.3f), (0.3f), /*hidden argument*/NULL);
		__this->set_radius_4(L_0);
		__this->set_angle_5((50.0f));
		Vector2_t4282066565  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector2__ctor_m1517109030(&L_1, (0.5f), (0.5f), /*hidden argument*/NULL);
		__this->set_center_6(L_1);
		ImageEffectBase__ctor_m2296049466(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwirlEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void TwirlEffect_OnRenderImage_m203826648 (TwirlEffect_t1211262097 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	{
		Material_t3870600107 * L_0 = ImageEffectBase_get_material_m2726075187(__this, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		float L_3 = __this->get_angle_5();
		Vector2_t4282066565  L_4 = __this->get_center_6();
		Vector2_t4282066565  L_5 = __this->get_radius_4();
		ImageEffects_RenderDistortion_m1051594010(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VortexEffect::.ctor()
extern "C"  void VortexEffect__ctor_m4273669680 (VortexEffect_t1608244223 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m1517109030(&L_0, (0.4f), (0.4f), /*hidden argument*/NULL);
		__this->set_radius_4(L_0);
		__this->set_angle_5((50.0f));
		Vector2_t4282066565  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector2__ctor_m1517109030(&L_1, (0.5f), (0.5f), /*hidden argument*/NULL);
		__this->set_center_6(L_1);
		ImageEffectBase__ctor_m2296049466(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VortexEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void VortexEffect_OnRenderImage_m1926518830 (VortexEffect_t1608244223 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method)
{
	{
		Material_t3870600107 * L_0 = ImageEffectBase_get_material_m2726075187(__this, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_1 = ___source0;
		RenderTexture_t1963041563 * L_2 = ___destination1;
		float L_3 = __this->get_angle_5();
		Vector2_t4282066565  L_4 = __this->get_center_6();
		Vector2_t4282066565  L_5 = __this->get_radius_4();
		ImageEffects_RenderDistortion_m1051594010(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
