﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_jisjeeFigoka120
struct  M_jisjeeFigoka120_t893448696  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_jisjeeFigoka120::_morchas
	float ____morchas_0;
	// System.Int32 GarbageiOS.M_jisjeeFigoka120::_girjasremTeewhem
	int32_t ____girjasremTeewhem_1;
	// System.Single GarbageiOS.M_jisjeeFigoka120::_jadoGojis
	float ____jadoGojis_2;
	// System.Int32 GarbageiOS.M_jisjeeFigoka120::_memtexePembe
	int32_t ____memtexePembe_3;
	// System.Single GarbageiOS.M_jisjeeFigoka120::_haskaYalldrow
	float ____haskaYalldrow_4;
	// System.Single GarbageiOS.M_jisjeeFigoka120::_peldelel
	float ____peldelel_5;
	// System.Boolean GarbageiOS.M_jisjeeFigoka120::_heha
	bool ____heha_6;
	// System.UInt32 GarbageiOS.M_jisjeeFigoka120::_whemerPajel
	uint32_t ____whemerPajel_7;
	// System.Int32 GarbageiOS.M_jisjeeFigoka120::_fasajiHisfoo
	int32_t ____fasajiHisfoo_8;
	// System.Single GarbageiOS.M_jisjeeFigoka120::_trounuSicanair
	float ____trounuSicanair_9;
	// System.Int32 GarbageiOS.M_jisjeeFigoka120::_dratraNatutra
	int32_t ____dratraNatutra_10;
	// System.UInt32 GarbageiOS.M_jisjeeFigoka120::_gipetirFemjagel
	uint32_t ____gipetirFemjagel_11;
	// System.Int32 GarbageiOS.M_jisjeeFigoka120::_maijatras
	int32_t ____maijatras_12;
	// System.String GarbageiOS.M_jisjeeFigoka120::_pici
	String_t* ____pici_13;
	// System.String GarbageiOS.M_jisjeeFigoka120::_cheesemnePigair
	String_t* ____cheesemnePigair_14;
	// System.String GarbageiOS.M_jisjeeFigoka120::_jerneejar
	String_t* ____jerneejar_15;
	// System.Int32 GarbageiOS.M_jisjeeFigoka120::_teheadra
	int32_t ____teheadra_16;
	// System.Int32 GarbageiOS.M_jisjeeFigoka120::_kearwhohall
	int32_t ____kearwhohall_17;
	// System.Single GarbageiOS.M_jisjeeFigoka120::_laykeste
	float ____laykeste_18;
	// System.Single GarbageiOS.M_jisjeeFigoka120::_kelate
	float ____kelate_19;
	// System.Int32 GarbageiOS.M_jisjeeFigoka120::_relcuyouWousor
	int32_t ____relcuyouWousor_20;
	// System.UInt32 GarbageiOS.M_jisjeeFigoka120::_zoseTomouhoo
	uint32_t ____zoseTomouhoo_21;
	// System.String GarbageiOS.M_jisjeeFigoka120::_firdiJowreese
	String_t* ____firdiJowreese_22;
	// System.UInt32 GarbageiOS.M_jisjeeFigoka120::_bofecuDrachem
	uint32_t ____bofecuDrachem_23;
	// System.Single GarbageiOS.M_jisjeeFigoka120::_reba
	float ____reba_24;
	// System.UInt32 GarbageiOS.M_jisjeeFigoka120::_dilawna
	uint32_t ____dilawna_25;
	// System.Single GarbageiOS.M_jisjeeFigoka120::_qerceYineko
	float ____qerceYineko_26;
	// System.Int32 GarbageiOS.M_jisjeeFigoka120::_jigelBalta
	int32_t ____jigelBalta_27;
	// System.String GarbageiOS.M_jisjeeFigoka120::_mowkiCayrir
	String_t* ____mowkiCayrir_28;
	// System.String GarbageiOS.M_jisjeeFigoka120::_jowtraysair
	String_t* ____jowtraysair_29;

public:
	inline static int32_t get_offset_of__morchas_0() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____morchas_0)); }
	inline float get__morchas_0() const { return ____morchas_0; }
	inline float* get_address_of__morchas_0() { return &____morchas_0; }
	inline void set__morchas_0(float value)
	{
		____morchas_0 = value;
	}

	inline static int32_t get_offset_of__girjasremTeewhem_1() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____girjasremTeewhem_1)); }
	inline int32_t get__girjasremTeewhem_1() const { return ____girjasremTeewhem_1; }
	inline int32_t* get_address_of__girjasremTeewhem_1() { return &____girjasremTeewhem_1; }
	inline void set__girjasremTeewhem_1(int32_t value)
	{
		____girjasremTeewhem_1 = value;
	}

	inline static int32_t get_offset_of__jadoGojis_2() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____jadoGojis_2)); }
	inline float get__jadoGojis_2() const { return ____jadoGojis_2; }
	inline float* get_address_of__jadoGojis_2() { return &____jadoGojis_2; }
	inline void set__jadoGojis_2(float value)
	{
		____jadoGojis_2 = value;
	}

	inline static int32_t get_offset_of__memtexePembe_3() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____memtexePembe_3)); }
	inline int32_t get__memtexePembe_3() const { return ____memtexePembe_3; }
	inline int32_t* get_address_of__memtexePembe_3() { return &____memtexePembe_3; }
	inline void set__memtexePembe_3(int32_t value)
	{
		____memtexePembe_3 = value;
	}

	inline static int32_t get_offset_of__haskaYalldrow_4() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____haskaYalldrow_4)); }
	inline float get__haskaYalldrow_4() const { return ____haskaYalldrow_4; }
	inline float* get_address_of__haskaYalldrow_4() { return &____haskaYalldrow_4; }
	inline void set__haskaYalldrow_4(float value)
	{
		____haskaYalldrow_4 = value;
	}

	inline static int32_t get_offset_of__peldelel_5() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____peldelel_5)); }
	inline float get__peldelel_5() const { return ____peldelel_5; }
	inline float* get_address_of__peldelel_5() { return &____peldelel_5; }
	inline void set__peldelel_5(float value)
	{
		____peldelel_5 = value;
	}

	inline static int32_t get_offset_of__heha_6() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____heha_6)); }
	inline bool get__heha_6() const { return ____heha_6; }
	inline bool* get_address_of__heha_6() { return &____heha_6; }
	inline void set__heha_6(bool value)
	{
		____heha_6 = value;
	}

	inline static int32_t get_offset_of__whemerPajel_7() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____whemerPajel_7)); }
	inline uint32_t get__whemerPajel_7() const { return ____whemerPajel_7; }
	inline uint32_t* get_address_of__whemerPajel_7() { return &____whemerPajel_7; }
	inline void set__whemerPajel_7(uint32_t value)
	{
		____whemerPajel_7 = value;
	}

	inline static int32_t get_offset_of__fasajiHisfoo_8() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____fasajiHisfoo_8)); }
	inline int32_t get__fasajiHisfoo_8() const { return ____fasajiHisfoo_8; }
	inline int32_t* get_address_of__fasajiHisfoo_8() { return &____fasajiHisfoo_8; }
	inline void set__fasajiHisfoo_8(int32_t value)
	{
		____fasajiHisfoo_8 = value;
	}

	inline static int32_t get_offset_of__trounuSicanair_9() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____trounuSicanair_9)); }
	inline float get__trounuSicanair_9() const { return ____trounuSicanair_9; }
	inline float* get_address_of__trounuSicanair_9() { return &____trounuSicanair_9; }
	inline void set__trounuSicanair_9(float value)
	{
		____trounuSicanair_9 = value;
	}

	inline static int32_t get_offset_of__dratraNatutra_10() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____dratraNatutra_10)); }
	inline int32_t get__dratraNatutra_10() const { return ____dratraNatutra_10; }
	inline int32_t* get_address_of__dratraNatutra_10() { return &____dratraNatutra_10; }
	inline void set__dratraNatutra_10(int32_t value)
	{
		____dratraNatutra_10 = value;
	}

	inline static int32_t get_offset_of__gipetirFemjagel_11() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____gipetirFemjagel_11)); }
	inline uint32_t get__gipetirFemjagel_11() const { return ____gipetirFemjagel_11; }
	inline uint32_t* get_address_of__gipetirFemjagel_11() { return &____gipetirFemjagel_11; }
	inline void set__gipetirFemjagel_11(uint32_t value)
	{
		____gipetirFemjagel_11 = value;
	}

	inline static int32_t get_offset_of__maijatras_12() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____maijatras_12)); }
	inline int32_t get__maijatras_12() const { return ____maijatras_12; }
	inline int32_t* get_address_of__maijatras_12() { return &____maijatras_12; }
	inline void set__maijatras_12(int32_t value)
	{
		____maijatras_12 = value;
	}

	inline static int32_t get_offset_of__pici_13() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____pici_13)); }
	inline String_t* get__pici_13() const { return ____pici_13; }
	inline String_t** get_address_of__pici_13() { return &____pici_13; }
	inline void set__pici_13(String_t* value)
	{
		____pici_13 = value;
		Il2CppCodeGenWriteBarrier(&____pici_13, value);
	}

	inline static int32_t get_offset_of__cheesemnePigair_14() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____cheesemnePigair_14)); }
	inline String_t* get__cheesemnePigair_14() const { return ____cheesemnePigair_14; }
	inline String_t** get_address_of__cheesemnePigair_14() { return &____cheesemnePigair_14; }
	inline void set__cheesemnePigair_14(String_t* value)
	{
		____cheesemnePigair_14 = value;
		Il2CppCodeGenWriteBarrier(&____cheesemnePigair_14, value);
	}

	inline static int32_t get_offset_of__jerneejar_15() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____jerneejar_15)); }
	inline String_t* get__jerneejar_15() const { return ____jerneejar_15; }
	inline String_t** get_address_of__jerneejar_15() { return &____jerneejar_15; }
	inline void set__jerneejar_15(String_t* value)
	{
		____jerneejar_15 = value;
		Il2CppCodeGenWriteBarrier(&____jerneejar_15, value);
	}

	inline static int32_t get_offset_of__teheadra_16() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____teheadra_16)); }
	inline int32_t get__teheadra_16() const { return ____teheadra_16; }
	inline int32_t* get_address_of__teheadra_16() { return &____teheadra_16; }
	inline void set__teheadra_16(int32_t value)
	{
		____teheadra_16 = value;
	}

	inline static int32_t get_offset_of__kearwhohall_17() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____kearwhohall_17)); }
	inline int32_t get__kearwhohall_17() const { return ____kearwhohall_17; }
	inline int32_t* get_address_of__kearwhohall_17() { return &____kearwhohall_17; }
	inline void set__kearwhohall_17(int32_t value)
	{
		____kearwhohall_17 = value;
	}

	inline static int32_t get_offset_of__laykeste_18() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____laykeste_18)); }
	inline float get__laykeste_18() const { return ____laykeste_18; }
	inline float* get_address_of__laykeste_18() { return &____laykeste_18; }
	inline void set__laykeste_18(float value)
	{
		____laykeste_18 = value;
	}

	inline static int32_t get_offset_of__kelate_19() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____kelate_19)); }
	inline float get__kelate_19() const { return ____kelate_19; }
	inline float* get_address_of__kelate_19() { return &____kelate_19; }
	inline void set__kelate_19(float value)
	{
		____kelate_19 = value;
	}

	inline static int32_t get_offset_of__relcuyouWousor_20() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____relcuyouWousor_20)); }
	inline int32_t get__relcuyouWousor_20() const { return ____relcuyouWousor_20; }
	inline int32_t* get_address_of__relcuyouWousor_20() { return &____relcuyouWousor_20; }
	inline void set__relcuyouWousor_20(int32_t value)
	{
		____relcuyouWousor_20 = value;
	}

	inline static int32_t get_offset_of__zoseTomouhoo_21() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____zoseTomouhoo_21)); }
	inline uint32_t get__zoseTomouhoo_21() const { return ____zoseTomouhoo_21; }
	inline uint32_t* get_address_of__zoseTomouhoo_21() { return &____zoseTomouhoo_21; }
	inline void set__zoseTomouhoo_21(uint32_t value)
	{
		____zoseTomouhoo_21 = value;
	}

	inline static int32_t get_offset_of__firdiJowreese_22() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____firdiJowreese_22)); }
	inline String_t* get__firdiJowreese_22() const { return ____firdiJowreese_22; }
	inline String_t** get_address_of__firdiJowreese_22() { return &____firdiJowreese_22; }
	inline void set__firdiJowreese_22(String_t* value)
	{
		____firdiJowreese_22 = value;
		Il2CppCodeGenWriteBarrier(&____firdiJowreese_22, value);
	}

	inline static int32_t get_offset_of__bofecuDrachem_23() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____bofecuDrachem_23)); }
	inline uint32_t get__bofecuDrachem_23() const { return ____bofecuDrachem_23; }
	inline uint32_t* get_address_of__bofecuDrachem_23() { return &____bofecuDrachem_23; }
	inline void set__bofecuDrachem_23(uint32_t value)
	{
		____bofecuDrachem_23 = value;
	}

	inline static int32_t get_offset_of__reba_24() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____reba_24)); }
	inline float get__reba_24() const { return ____reba_24; }
	inline float* get_address_of__reba_24() { return &____reba_24; }
	inline void set__reba_24(float value)
	{
		____reba_24 = value;
	}

	inline static int32_t get_offset_of__dilawna_25() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____dilawna_25)); }
	inline uint32_t get__dilawna_25() const { return ____dilawna_25; }
	inline uint32_t* get_address_of__dilawna_25() { return &____dilawna_25; }
	inline void set__dilawna_25(uint32_t value)
	{
		____dilawna_25 = value;
	}

	inline static int32_t get_offset_of__qerceYineko_26() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____qerceYineko_26)); }
	inline float get__qerceYineko_26() const { return ____qerceYineko_26; }
	inline float* get_address_of__qerceYineko_26() { return &____qerceYineko_26; }
	inline void set__qerceYineko_26(float value)
	{
		____qerceYineko_26 = value;
	}

	inline static int32_t get_offset_of__jigelBalta_27() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____jigelBalta_27)); }
	inline int32_t get__jigelBalta_27() const { return ____jigelBalta_27; }
	inline int32_t* get_address_of__jigelBalta_27() { return &____jigelBalta_27; }
	inline void set__jigelBalta_27(int32_t value)
	{
		____jigelBalta_27 = value;
	}

	inline static int32_t get_offset_of__mowkiCayrir_28() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____mowkiCayrir_28)); }
	inline String_t* get__mowkiCayrir_28() const { return ____mowkiCayrir_28; }
	inline String_t** get_address_of__mowkiCayrir_28() { return &____mowkiCayrir_28; }
	inline void set__mowkiCayrir_28(String_t* value)
	{
		____mowkiCayrir_28 = value;
		Il2CppCodeGenWriteBarrier(&____mowkiCayrir_28, value);
	}

	inline static int32_t get_offset_of__jowtraysair_29() { return static_cast<int32_t>(offsetof(M_jisjeeFigoka120_t893448696, ____jowtraysair_29)); }
	inline String_t* get__jowtraysair_29() const { return ____jowtraysair_29; }
	inline String_t** get_address_of__jowtraysair_29() { return &____jowtraysair_29; }
	inline void set__jowtraysair_29(String_t* value)
	{
		____jowtraysair_29 = value;
		Il2CppCodeGenWriteBarrier(&____jowtraysair_29, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
