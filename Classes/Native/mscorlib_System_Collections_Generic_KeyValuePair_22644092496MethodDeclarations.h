﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,ACData>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1237271847(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2644092496 *, String_t*, ACData_t1924893420 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,ACData>::get_Key()
#define KeyValuePair_2_get_Key_m345820897(__this, method) ((  String_t* (*) (KeyValuePair_2_t2644092496 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,ACData>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3246733474(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2644092496 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,ACData>::get_Value()
#define KeyValuePair_2_get_Value_m2020548449(__this, method) ((  ACData_t1924893420 * (*) (KeyValuePair_2_t2644092496 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,ACData>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2305780642(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2644092496 *, ACData_t1924893420 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,ACData>::ToString()
#define KeyValuePair_2_ToString_m1930687680(__this, method) ((  String_t* (*) (KeyValuePair_2_t2644092496 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
