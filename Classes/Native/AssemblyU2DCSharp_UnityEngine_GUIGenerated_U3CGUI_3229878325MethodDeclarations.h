﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUIGenerated/<GUI_Window_GetDelegate_member108_arg2>c__AnonStoreyF0
struct U3CGUI_Window_GetDelegate_member108_arg2U3Ec__AnonStoreyF0_t3229878325;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_GUIGenerated/<GUI_Window_GetDelegate_member108_arg2>c__AnonStoreyF0::.ctor()
extern "C"  void U3CGUI_Window_GetDelegate_member108_arg2U3Ec__AnonStoreyF0__ctor_m2977295062 (U3CGUI_Window_GetDelegate_member108_arg2U3Ec__AnonStoreyF0_t3229878325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated/<GUI_Window_GetDelegate_member108_arg2>c__AnonStoreyF0::<>m__1D6(System.Int32)
extern "C"  void U3CGUI_Window_GetDelegate_member108_arg2U3Ec__AnonStoreyF0_U3CU3Em__1D6_m1707812247 (U3CGUI_Window_GetDelegate_member108_arg2U3Ec__AnonStoreyF0_t3229878325 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
