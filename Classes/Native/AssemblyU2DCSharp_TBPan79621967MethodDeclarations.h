﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TBPan
struct TBPan_t79621967;
// TBPan/PanEventHandler
struct PanEventHandler_t1012212141;
// DragGesture
struct DragGesture_t2914643285;
// Gesture
struct Gesture_t1589572905;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TBPan_PanEventHandler1012212141.h"
#include "AssemblyU2DCSharp_DragGesture2914643285.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_GestureRecognitionState3604239971.h"
#include "AssemblyU2DCSharp_Gesture1589572905.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_TBPan79621967.h"

// System.Void TBPan::.ctor()
extern "C"  void TBPan__ctor_m2352849660 (TBPan_t79621967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPan::add_OnPan(TBPan/PanEventHandler)
extern "C"  void TBPan_add_OnPan_m4267754539 (TBPan_t79621967 * __this, PanEventHandler_t1012212141 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPan::remove_OnPan(TBPan/PanEventHandler)
extern "C"  void TBPan_remove_OnPan_m2429691898 (TBPan_t79621967 * __this, PanEventHandler_t1012212141 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPan::Awake()
extern "C"  void TBPan_Awake_m2590454879 (TBPan_t79621967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPan::Start()
extern "C"  void TBPan_Start_m1299987452 (TBPan_t79621967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPan::OnDrag(DragGesture)
extern "C"  void TBPan_OnDrag_m4277115490 (TBPan_t79621967 * __this, DragGesture_t2914643285 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPan::Update()
extern "C"  void TBPan_Update_m1650757521 (TBPan_t79621967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TBPan::ConstrainToPanningPlane(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  TBPan_ConstrainToPanningPlane_m141584552 (TBPan_t79621967 * __this, Vector3_t4282066566  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPan::TeleportTo(UnityEngine.Vector3)
extern "C"  void TBPan_TeleportTo_m2152384011 (TBPan_t79621967 * __this, Vector3_t4282066566  ___worldPos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPan::FlyTo(UnityEngine.Vector3)
extern "C"  void TBPan_FlyTo_m2654946289 (TBPan_t79621967 * __this, Vector3_t4282066566  ___worldPos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TBPan::ConstrainToMoveArea(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  TBPan_ConstrainToMoveArea_m4084946453 (TBPan_t79621967 * __this, Vector3_t4282066566  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GestureRecognitionState TBPan::ilo_get_State1(Gesture)
extern "C"  int32_t TBPan_ilo_get_State1_m1667553461 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TBPan::ilo_get_DeltaMove2(DragGesture)
extern "C"  Vector2_t4282066565  TBPan_ilo_get_DeltaMove2_m4175438795 (Il2CppObject * __this /* static, unused */, DragGesture_t2914643285 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPan::ilo_Invoke3(TBPan/PanEventHandler,TBPan,UnityEngine.Vector3)
extern "C"  void TBPan_ilo_Invoke3_m3674123571 (Il2CppObject * __this /* static, unused */, PanEventHandler_t1012212141 * ____this0, TBPan_t79621967 * ___source1, Vector3_t4282066566  ___move2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TBPan::ilo_ConstrainToMoveArea4(TBPan,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  TBPan_ilo_ConstrainToMoveArea4_m1805022513 (Il2CppObject * __this /* static, unused */, TBPan_t79621967 * ____this0, Vector3_t4282066566  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TBPan::ilo_ConstrainToPanningPlane5(TBPan,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  TBPan_ilo_ConstrainToPanningPlane5_m517156893 (Il2CppObject * __this /* static, unused */, TBPan_t79621967 * ____this0, Vector3_t4282066566  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
