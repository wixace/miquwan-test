﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.LightProbes
struct LightProbes_t2995644975;
// UnityEngine.Renderer
struct Renderer_t3076687687;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.Rendering.SphericalHarmonicsL2[]
struct SphericalHarmonicsL2U5BU5D_t3690964838;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Renderer3076687687.h"
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmoni3881980607.h"

// System.Void UnityEngine.LightProbes::.ctor()
extern "C"  void LightProbes__ctor_m2106898432 (LightProbes_t2995644975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LightProbes::GetInterpolatedProbe(UnityEngine.Vector3,UnityEngine.Renderer,UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C"  void LightProbes_GetInterpolatedProbe_m2074537340 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___position0, Renderer_t3076687687 * ___renderer1, SphericalHarmonicsL2_t3881980607 * ___probe2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LightProbes::INTERNAL_CALL_GetInterpolatedProbe(UnityEngine.Vector3&,UnityEngine.Renderer,UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C"  void LightProbes_INTERNAL_CALL_GetInterpolatedProbe_m5378189 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___position0, Renderer_t3076687687 * ___renderer1, SphericalHarmonicsL2_t3881980607 * ___probe2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine.LightProbes::get_positions()
extern "C"  Vector3U5BU5D_t215400611* LightProbes_get_positions_m1300261847 (LightProbes_t2995644975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rendering.SphericalHarmonicsL2[] UnityEngine.LightProbes::get_bakedProbes()
extern "C"  SphericalHarmonicsL2U5BU5D_t3690964838* LightProbes_get_bakedProbes_m220326024 (LightProbes_t2995644975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LightProbes::set_bakedProbes(UnityEngine.Rendering.SphericalHarmonicsL2[])
extern "C"  void LightProbes_set_bakedProbes_m1104667941 (LightProbes_t2995644975 * __this, SphericalHarmonicsL2U5BU5D_t3690964838* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.LightProbes::get_count()
extern "C"  int32_t LightProbes_get_count_m1391614116 (LightProbes_t2995644975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.LightProbes::get_cellCount()
extern "C"  int32_t LightProbes_get_cellCount_m931015394 (LightProbes_t2995644975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
