﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEventListenerGenerated/<UIEventListener_onDragOut_GetDelegate_member11_arg0>c__AnonStoreyBF
struct U3CUIEventListener_onDragOut_GetDelegate_member11_arg0U3Ec__AnonStoreyBF_t534698653;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UIEventListenerGenerated/<UIEventListener_onDragOut_GetDelegate_member11_arg0>c__AnonStoreyBF::.ctor()
extern "C"  void U3CUIEventListener_onDragOut_GetDelegate_member11_arg0U3Ec__AnonStoreyBF__ctor_m1954617198 (U3CUIEventListener_onDragOut_GetDelegate_member11_arg0U3Ec__AnonStoreyBF_t534698653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated/<UIEventListener_onDragOut_GetDelegate_member11_arg0>c__AnonStoreyBF::<>m__142(UnityEngine.GameObject)
extern "C"  void U3CUIEventListener_onDragOut_GetDelegate_member11_arg0U3Ec__AnonStoreyBF_U3CU3Em__142_m101845170 (U3CUIEventListener_onDragOut_GetDelegate_member11_arg0U3Ec__AnonStoreyBF_t534698653 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
