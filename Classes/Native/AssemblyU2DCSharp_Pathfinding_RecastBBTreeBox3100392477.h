﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.RecastMeshObj
struct RecastMeshObj_t2069195738;
// Pathfinding.RecastBBTreeBox
struct RecastBBTreeBox_t3100392477;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RecastBBTreeBox
struct  RecastBBTreeBox_t3100392477  : public Il2CppObject
{
public:
	// UnityEngine.Rect Pathfinding.RecastBBTreeBox::rect
	Rect_t4241904616  ___rect_0;
	// Pathfinding.RecastMeshObj Pathfinding.RecastBBTreeBox::mesh
	RecastMeshObj_t2069195738 * ___mesh_1;
	// Pathfinding.RecastBBTreeBox Pathfinding.RecastBBTreeBox::c1
	RecastBBTreeBox_t3100392477 * ___c1_2;
	// Pathfinding.RecastBBTreeBox Pathfinding.RecastBBTreeBox::c2
	RecastBBTreeBox_t3100392477 * ___c2_3;

public:
	inline static int32_t get_offset_of_rect_0() { return static_cast<int32_t>(offsetof(RecastBBTreeBox_t3100392477, ___rect_0)); }
	inline Rect_t4241904616  get_rect_0() const { return ___rect_0; }
	inline Rect_t4241904616 * get_address_of_rect_0() { return &___rect_0; }
	inline void set_rect_0(Rect_t4241904616  value)
	{
		___rect_0 = value;
	}

	inline static int32_t get_offset_of_mesh_1() { return static_cast<int32_t>(offsetof(RecastBBTreeBox_t3100392477, ___mesh_1)); }
	inline RecastMeshObj_t2069195738 * get_mesh_1() const { return ___mesh_1; }
	inline RecastMeshObj_t2069195738 ** get_address_of_mesh_1() { return &___mesh_1; }
	inline void set_mesh_1(RecastMeshObj_t2069195738 * value)
	{
		___mesh_1 = value;
		Il2CppCodeGenWriteBarrier(&___mesh_1, value);
	}

	inline static int32_t get_offset_of_c1_2() { return static_cast<int32_t>(offsetof(RecastBBTreeBox_t3100392477, ___c1_2)); }
	inline RecastBBTreeBox_t3100392477 * get_c1_2() const { return ___c1_2; }
	inline RecastBBTreeBox_t3100392477 ** get_address_of_c1_2() { return &___c1_2; }
	inline void set_c1_2(RecastBBTreeBox_t3100392477 * value)
	{
		___c1_2 = value;
		Il2CppCodeGenWriteBarrier(&___c1_2, value);
	}

	inline static int32_t get_offset_of_c2_3() { return static_cast<int32_t>(offsetof(RecastBBTreeBox_t3100392477, ___c2_3)); }
	inline RecastBBTreeBox_t3100392477 * get_c2_3() const { return ___c2_3; }
	inline RecastBBTreeBox_t3100392477 ** get_address_of_c2_3() { return &___c2_3; }
	inline void set_c2_3(RecastBBTreeBox_t3100392477 * value)
	{
		___c2_3 = value;
		Il2CppCodeGenWriteBarrier(&___c2_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
