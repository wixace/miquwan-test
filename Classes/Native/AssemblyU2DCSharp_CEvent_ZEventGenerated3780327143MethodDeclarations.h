﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CEvent_ZEventGenerated
struct CEvent_ZEventGenerated_t3780327143;
// JSVCall
struct JSVCall_t3708497963;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void CEvent_ZEventGenerated::.ctor()
extern "C"  void CEvent_ZEventGenerated__ctor_m1999294868 (CEvent_ZEventGenerated_t3780327143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CEvent_ZEventGenerated::ZEvent_ZEvent1(JSVCall,System.Int32)
extern "C"  bool CEvent_ZEventGenerated_ZEvent_ZEvent1_m1825601286 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CEvent_ZEventGenerated::ZEvent_ZEvent2(JSVCall,System.Int32)
extern "C"  bool CEvent_ZEventGenerated_ZEvent_ZEvent2_m3070365767 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CEvent_ZEventGenerated::ZEvent_ZEvent3(JSVCall,System.Int32)
extern "C"  bool CEvent_ZEventGenerated_ZEvent_ZEvent3_m20162952 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CEvent_ZEventGenerated::ZEvent_ZEvent4(JSVCall,System.Int32)
extern "C"  bool CEvent_ZEventGenerated_ZEvent_ZEvent4_m1264927433 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CEvent_ZEventGenerated::ZEvent_ZEvent5(JSVCall,System.Int32)
extern "C"  bool CEvent_ZEventGenerated_ZEvent_ZEvent5_m2509691914 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent_ZEventGenerated::ZEvent_args(JSVCall)
extern "C"  void CEvent_ZEventGenerated_ZEvent_args_m3887561593 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent_ZEventGenerated::ZEvent_arg(JSVCall)
extern "C"  void CEvent_ZEventGenerated_ZEvent_arg_m1826092992 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent_ZEventGenerated::ZEvent_arg1(JSVCall)
extern "C"  void CEvent_ZEventGenerated_ZEvent_arg1_m3972551035 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent_ZEventGenerated::ZEvent_arg2(JSVCall)
extern "C"  void CEvent_ZEventGenerated_ZEvent_arg2_m3776037530 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent_ZEventGenerated::__Register()
extern "C"  void CEvent_ZEventGenerated___Register_m2048821171 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] CEvent_ZEventGenerated::<ZEvent_ZEvent2>m__1F()
extern "C"  ObjectU5BU5D_t1108656482* CEvent_ZEventGenerated_U3CZEvent_ZEvent2U3Em__1F_m3255247570 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] CEvent_ZEventGenerated::<ZEvent_args>m__20()
extern "C"  ObjectU5BU5D_t1108656482* CEvent_ZEventGenerated_U3CZEvent_argsU3Em__20_m2642160016 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent_ZEventGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void CEvent_ZEventGenerated_ilo_addJSCSRel1_m1483484752 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CEvent_ZEventGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool CEvent_ZEventGenerated_ilo_attachFinalizerObject2_m95173644 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CEvent_ZEventGenerated::ilo_getObject3(System.Int32)
extern "C"  int32_t CEvent_ZEventGenerated_ilo_getObject3_m363971584 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CEvent_ZEventGenerated::ilo_getStringS4(System.Int32)
extern "C"  String_t* CEvent_ZEventGenerated_ilo_getStringS4_m2762020949 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CEvent_ZEventGenerated::ilo_getWhatever5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * CEvent_ZEventGenerated_ilo_getWhatever5_m4144965532 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CEvent_ZEventGenerated::ilo_setWhatever6(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  void CEvent_ZEventGenerated_ilo_setWhatever6_m3759939114 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CEvent_ZEventGenerated::ilo_getArrayLength7(System.Int32)
extern "C"  int32_t CEvent_ZEventGenerated_ilo_getArrayLength7_m2246833968 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CEvent_ZEventGenerated::ilo_getElement8(System.Int32,System.Int32)
extern "C"  int32_t CEvent_ZEventGenerated_ilo_getElement8_m4144973539 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
