﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUISkinGenerated
struct UnityEngine_GUISkinGenerated_t29238399;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t565654559;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_GUISkinGenerated::.ctor()
extern "C"  void UnityEngine_GUISkinGenerated__ctor_m2885303548 (UnityEngine_GUISkinGenerated_t29238399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUISkinGenerated::GUISkin_GUISkin1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUISkinGenerated_GUISkin_GUISkin1_m2320092812 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_font(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_font_m2810851529 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_box(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_box_m4026842921 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_label(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_label_m2140696736 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_textField(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_textField_m2883304679 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_textArea(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_textArea_m3440977182 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_button(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_button_m2175123654 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_toggle(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_toggle_m2985341668 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_window(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_window_m3467149768 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_horizontalSlider(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_horizontalSlider_m2623417523 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_horizontalSliderThumb(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_horizontalSliderThumb_m3432672643 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_verticalSlider(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_verticalSlider_m3174713249 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_verticalSliderThumb(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_verticalSliderThumb_m3378509653 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_horizontalScrollbar(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_horizontalScrollbar_m403413106 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_horizontalScrollbarThumb(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_horizontalScrollbarThumb_m1904574756 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_horizontalScrollbarLeftButton(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_horizontalScrollbarLeftButton_m2341643097 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_horizontalScrollbarRightButton(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_horizontalScrollbarRightButton_m1089795724 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_verticalScrollbar(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_verticalScrollbar_m99446468 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_verticalScrollbarThumb(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_verticalScrollbarThumb_m3242642962 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_verticalScrollbarUpButton(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_verticalScrollbarUpButton_m3513491863 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_verticalScrollbarDownButton(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_verticalScrollbarDownButton_m2233371952 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_scrollView(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_scrollView_m939324390 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_customStyles(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_customStyles_m647103461 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::GUISkin_settings(JSVCall)
extern "C"  void UnityEngine_GUISkinGenerated_GUISkin_settings_m2443667061 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUISkinGenerated::GUISkin_FindStyle__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUISkinGenerated_GUISkin_FindStyle__String_m3817427384 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUISkinGenerated::GUISkin_GetEnumerator(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUISkinGenerated_GUISkin_GetEnumerator_m1335360041 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUISkinGenerated::GUISkin_GetStyle__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUISkinGenerated_GUISkin_GetStyle__String_m3929326463 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::__Register()
extern "C"  void UnityEngine_GUISkinGenerated___Register_m917634379 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIStyle[] UnityEngine_GUISkinGenerated::<GUISkin_customStyles>m__24F()
extern "C"  GUIStyleU5BU5D_t565654559* UnityEngine_GUISkinGenerated_U3CGUISkin_customStylesU3Em__24F_m1811283637 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUISkinGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_GUISkinGenerated_ilo_getObject1_m4227478166 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUISkinGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_GUISkinGenerated_ilo_attachFinalizerObject2_m1762461348 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UnityEngine_GUISkinGenerated_ilo_addJSCSRel3_m99602106 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUISkinGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_GUISkinGenerated_ilo_setObject4_m4271270097 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_GUISkinGenerated::ilo_getObject5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_GUISkinGenerated_ilo_getObject5_m2015366647 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISkinGenerated::ilo_setArrayS6(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_GUISkinGenerated_ilo_setArrayS6_m190432588 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_GUISkinGenerated::ilo_getStringS7(System.Int32)
extern "C"  String_t* UnityEngine_GUISkinGenerated_ilo_getStringS7_m1430045376 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
