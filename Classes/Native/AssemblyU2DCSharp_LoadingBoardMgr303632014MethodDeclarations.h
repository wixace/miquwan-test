﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadingBoardMgr
struct LoadingBoardMgr_t303632014;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Action
struct Action_t3771233898;
// System.String
struct String_t;
// UILabel
struct UILabel_t291504320;
// UIPanel
struct UIPanel_t295209936;
// UIProgressBar
struct UIProgressBar_t168062834;
// UITexture
struct UITexture_t3903132647;
// UnityEngine.Texture
struct Texture_t2526458961;
// VersionMgr
struct VersionMgr_t1322950208;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "System_Core_System_Action3771233898.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_LoadingBoardMgr303632014.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"
#include "AssemblyU2DCSharp_UIProgressBar168062834.h"
#include "AssemblyU2DCSharp_UITexture3903132647.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"

// System.Void LoadingBoardMgr::.ctor()
extern "C"  void LoadingBoardMgr__ctor_m2434480285 (LoadingBoardMgr_t303632014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::.cctor()
extern "C"  void LoadingBoardMgr__cctor_m1972348592 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LoadingBoardMgr LoadingBoardMgr::get_Instance()
extern "C"  LoadingBoardMgr_t303632014 * LoadingBoardMgr_get_Instance_m1415892224 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::OnAwake(UnityEngine.GameObject)
extern "C"  void LoadingBoardMgr_OnAwake_m3903083865 (LoadingBoardMgr_t303632014 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::OnAwakeAAS(UnityEngine.GameObject)
extern "C"  void LoadingBoardMgr_OnAwakeAAS_m3454793036 (LoadingBoardMgr_t303632014 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::OnDialogBtn(UnityEngine.GameObject)
extern "C"  void LoadingBoardMgr_OnDialogBtn_m1610734920 (LoadingBoardMgr_t303632014 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::OpenDialog(System.Action)
extern "C"  void LoadingBoardMgr_OpenDialog_m1761111524 (LoadingBoardMgr_t303632014 * __this, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::OnDestroy()
extern "C"  void LoadingBoardMgr_OnDestroy_m959805206 (LoadingBoardMgr_t303632014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::Close()
extern "C"  void LoadingBoardMgr_Close_m4145339827 (LoadingBoardMgr_t303632014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::Init()
extern "C"  void LoadingBoardMgr_Init_m30068247 (LoadingBoardMgr_t303632014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::AVProInit()
extern "C"  void LoadingBoardMgr_AVProInit_m1245779523 (LoadingBoardMgr_t303632014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::Reset()
extern "C"  void LoadingBoardMgr_Reset_m80913226 (LoadingBoardMgr_t303632014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::SetDepth(System.Int32)
extern "C"  void LoadingBoardMgr_SetDepth_m3537416857 (LoadingBoardMgr_t303632014 * __this, int32_t ___depth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::SetDescri(System.String)
extern "C"  void LoadingBoardMgr_SetDescri_m1183280477 (LoadingBoardMgr_t303632014 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::SetVer(System.String)
extern "C"  void LoadingBoardMgr_SetVer_m1415499994 (LoadingBoardMgr_t303632014 * __this, String_t* ___ver0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::SetPercent(System.Single,System.String)
extern "C"  void LoadingBoardMgr_SetPercent_m194083613 (LoadingBoardMgr_t303632014 * __this, float ___value0, String_t* ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::ShowAAS()
extern "C"  void LoadingBoardMgr_ShowAAS_m788172241 (LoadingBoardMgr_t303632014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::SetAASAlpha(System.Single)
extern "C"  void LoadingBoardMgr_SetAASAlpha_m3900392547 (LoadingBoardMgr_t303632014 * __this, float ___alpha0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::ShowAASEnd()
extern "C"  void LoadingBoardMgr_ShowAASEnd_m4176926764 (LoadingBoardMgr_t303632014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::Show()
extern "C"  void LoadingBoardMgr_Show_m311000260 (LoadingBoardMgr_t303632014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::UpdateDP()
extern "C"  void LoadingBoardMgr_UpdateDP_m2442378204 (LoadingBoardMgr_t303632014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::Clear()
extern "C"  void LoadingBoardMgr_Clear_m4135580872 (LoadingBoardMgr_t303632014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::ReviewUI(System.Boolean)
extern "C"  void LoadingBoardMgr_ReviewUI_m3363392298 (LoadingBoardMgr_t303632014 * __this, bool ___isReview0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::ilo_Init1(LoadingBoardMgr)
extern "C"  void LoadingBoardMgr_ilo_Init1_m3479411579 (Il2CppObject * __this /* static, unused */, LoadingBoardMgr_t303632014 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::ilo_set_text2(UILabel,System.String)
extern "C"  void LoadingBoardMgr_ilo_set_text2_m2958390814 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::ilo_Reset3(LoadingBoardMgr)
extern "C"  void LoadingBoardMgr_ilo_Reset3_m184076134 (Il2CppObject * __this /* static, unused */, LoadingBoardMgr_t303632014 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject LoadingBoardMgr::ilo_Instantiate4(UnityEngine.GameObject)
extern "C"  GameObject_t3674682005 * LoadingBoardMgr_ilo_Instantiate4_m2664505447 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::ilo_set_alpha5(UIPanel,System.Single)
extern "C"  void LoadingBoardMgr_ilo_set_alpha5_m508589977 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::ilo_OnAwake6(LoadingBoardMgr,UnityEngine.GameObject)
extern "C"  void LoadingBoardMgr_ilo_OnAwake6_m3863058532 (Il2CppObject * __this /* static, unused */, LoadingBoardMgr_t303632014 * ____this0, GameObject_t3674682005 * ___viewGO1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::ilo_set_value7(UIProgressBar,System.Single)
extern "C"  void LoadingBoardMgr_ilo_set_value7_m3950079880 (Il2CppObject * __this /* static, unused */, UIProgressBar_t168062834 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::ilo_OnAwakeAAS8(LoadingBoardMgr,UnityEngine.GameObject)
extern "C"  void LoadingBoardMgr_ilo_OnAwakeAAS8_m93032333 (Il2CppObject * __this /* static, unused */, LoadingBoardMgr_t303632014 * ____this0, GameObject_t3674682005 * ___viewGO1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::ilo_set_mainTexture9(UITexture,UnityEngine.Texture)
extern "C"  void LoadingBoardMgr_ilo_set_mainTexture9_m2789659685 (Il2CppObject * __this /* static, unused */, UITexture_t3903132647 * ____this0, Texture_t2526458961 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr LoadingBoardMgr::ilo_get_Instance10()
extern "C"  VersionMgr_t1322950208 * LoadingBoardMgr_ilo_get_Instance10_m3157835354 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgr::ilo_OnDestroy11(LoadingBoardMgr)
extern "C"  void LoadingBoardMgr_ilo_OnDestroy11_m350456993 (Il2CppObject * __this /* static, unused */, LoadingBoardMgr_t303632014 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
