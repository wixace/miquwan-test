﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>
struct ReadOnlyCollection_1_t4268719385;
// System.Collections.Generic.IList`1<UnityEngine.Bounds>
struct IList_1_t1111321756;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// UnityEngine.Bounds[]
struct BoundsU5BU5D_t3144995076;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Bounds>
struct IEnumerator_1_t328539602;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m4268895172_gshared (ReadOnlyCollection_1_t4268719385 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m4268895172(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t4268719385 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m4268895172_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3558005678_gshared (ReadOnlyCollection_1_t4268719385 * __this, Bounds_t2711641849  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3558005678(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t4268719385 *, Bounds_t2711641849 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3558005678_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m635717340_gshared (ReadOnlyCollection_1_t4268719385 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m635717340(__this, method) ((  void (*) (ReadOnlyCollection_1_t4268719385 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m635717340_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3748338965_gshared (ReadOnlyCollection_1_t4268719385 * __this, int32_t ___index0, Bounds_t2711641849  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3748338965(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t4268719385 *, int32_t, Bounds_t2711641849 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3748338965_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m273688777_gshared (ReadOnlyCollection_1_t4268719385 * __this, Bounds_t2711641849  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m273688777(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t4268719385 *, Bounds_t2711641849 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m273688777_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1622191835_gshared (ReadOnlyCollection_1_t4268719385 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1622191835(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t4268719385 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1622191835_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  Bounds_t2711641849  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3265101185_gshared (ReadOnlyCollection_1_t4268719385 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3265101185(__this, ___index0, method) ((  Bounds_t2711641849  (*) (ReadOnlyCollection_1_t4268719385 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3265101185_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m485780652_gshared (ReadOnlyCollection_1_t4268719385 * __this, int32_t ___index0, Bounds_t2711641849  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m485780652(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4268719385 *, int32_t, Bounds_t2711641849 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m485780652_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m561434790_gshared (ReadOnlyCollection_1_t4268719385 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m561434790(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4268719385 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m561434790_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3289150579_gshared (ReadOnlyCollection_1_t4268719385 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3289150579(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t4268719385 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3289150579_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1397036610_gshared (ReadOnlyCollection_1_t4268719385 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1397036610(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4268719385 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1397036610_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1984730171_gshared (ReadOnlyCollection_1_t4268719385 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1984730171(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4268719385 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1984730171_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m3230666625_gshared (ReadOnlyCollection_1_t4268719385 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3230666625(__this, method) ((  void (*) (ReadOnlyCollection_1_t4268719385 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3230666625_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1623685861_gshared (ReadOnlyCollection_1_t4268719385 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1623685861(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t4268719385 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1623685861_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1214395667_gshared (ReadOnlyCollection_1_t4268719385 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1214395667(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4268719385 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1214395667_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2044351494_gshared (ReadOnlyCollection_1_t4268719385 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2044351494(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4268719385 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2044351494_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m4108534626_gshared (ReadOnlyCollection_1_t4268719385 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m4108534626(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t4268719385 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m4108534626_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m100910230_gshared (ReadOnlyCollection_1_t4268719385 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m100910230(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t4268719385 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m100910230_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1298773591_gshared (ReadOnlyCollection_1_t4268719385 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1298773591(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4268719385 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1298773591_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4088005257_gshared (ReadOnlyCollection_1_t4268719385 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4088005257(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4268719385 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4088005257_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2099935764_gshared (ReadOnlyCollection_1_t4268719385 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2099935764(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4268719385 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2099935764_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m937390693_gshared (ReadOnlyCollection_1_t4268719385 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m937390693(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4268719385 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m937390693_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m4038555088_gshared (ReadOnlyCollection_1_t4268719385 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m4038555088(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4268719385 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m4038555088_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2435683933_gshared (ReadOnlyCollection_1_t4268719385 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2435683933(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4268719385 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2435683933_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3646354190_gshared (ReadOnlyCollection_1_t4268719385 * __this, Bounds_t2711641849  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3646354190(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t4268719385 *, Bounds_t2711641849 , const MethodInfo*))ReadOnlyCollection_1_Contains_m3646354190_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2872367326_gshared (ReadOnlyCollection_1_t4268719385 * __this, BoundsU5BU5D_t3144995076* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m2872367326(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t4268719385 *, BoundsU5BU5D_t3144995076*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m2872367326_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3191613797_gshared (ReadOnlyCollection_1_t4268719385 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3191613797(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t4268719385 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3191613797_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1040723754_gshared (ReadOnlyCollection_1_t4268719385 * __this, Bounds_t2711641849  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1040723754(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4268719385 *, Bounds_t2711641849 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1040723754_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2014972817_gshared (ReadOnlyCollection_1_t4268719385 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m2014972817(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t4268719385 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2014972817_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>::get_Item(System.Int32)
extern "C"  Bounds_t2711641849  ReadOnlyCollection_1_get_Item_m3547155393_gshared (ReadOnlyCollection_1_t4268719385 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3547155393(__this, ___index0, method) ((  Bounds_t2711641849  (*) (ReadOnlyCollection_1_t4268719385 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3547155393_gshared)(__this, ___index0, method)
