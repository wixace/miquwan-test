﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._fbeb1eb93c599e5b689303114b3337bd
struct _fbeb1eb93c599e5b689303114b3337bd_t4219531997;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._fbeb1eb93c599e5b689303114b3337bd::.ctor()
extern "C"  void _fbeb1eb93c599e5b689303114b3337bd__ctor_m10034160 (_fbeb1eb93c599e5b689303114b3337bd_t4219531997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._fbeb1eb93c599e5b689303114b3337bd::_fbeb1eb93c599e5b689303114b3337bdm2(System.Int32)
extern "C"  int32_t _fbeb1eb93c599e5b689303114b3337bd__fbeb1eb93c599e5b689303114b3337bdm2_m2230041721 (_fbeb1eb93c599e5b689303114b3337bd_t4219531997 * __this, int32_t ____fbeb1eb93c599e5b689303114b3337bda0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._fbeb1eb93c599e5b689303114b3337bd::_fbeb1eb93c599e5b689303114b3337bdm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _fbeb1eb93c599e5b689303114b3337bd__fbeb1eb93c599e5b689303114b3337bdm_m104130141 (_fbeb1eb93c599e5b689303114b3337bd_t4219531997 * __this, int32_t ____fbeb1eb93c599e5b689303114b3337bda0, int32_t ____fbeb1eb93c599e5b689303114b3337bd691, int32_t ____fbeb1eb93c599e5b689303114b3337bdc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
