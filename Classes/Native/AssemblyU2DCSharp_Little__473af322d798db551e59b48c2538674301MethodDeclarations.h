﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._473af322d798db551e59b48ccc465fd2
struct _473af322d798db551e59b48ccc465fd2_t2538674301;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__473af322d798db551e59b48c2538674301.h"

// System.Void Little._473af322d798db551e59b48ccc465fd2::.ctor()
extern "C"  void _473af322d798db551e59b48ccc465fd2__ctor_m4194912848 (_473af322d798db551e59b48ccc465fd2_t2538674301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._473af322d798db551e59b48ccc465fd2::_473af322d798db551e59b48ccc465fd2m2(System.Int32)
extern "C"  int32_t _473af322d798db551e59b48ccc465fd2__473af322d798db551e59b48ccc465fd2m2_m1608308857 (_473af322d798db551e59b48ccc465fd2_t2538674301 * __this, int32_t ____473af322d798db551e59b48ccc465fd2a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._473af322d798db551e59b48ccc465fd2::_473af322d798db551e59b48ccc465fd2m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _473af322d798db551e59b48ccc465fd2__473af322d798db551e59b48ccc465fd2m_m1903905373 (_473af322d798db551e59b48ccc465fd2_t2538674301 * __this, int32_t ____473af322d798db551e59b48ccc465fd2a0, int32_t ____473af322d798db551e59b48ccc465fd261, int32_t ____473af322d798db551e59b48ccc465fd2c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._473af322d798db551e59b48ccc465fd2::ilo__473af322d798db551e59b48ccc465fd2m21(Little._473af322d798db551e59b48ccc465fd2,System.Int32)
extern "C"  int32_t _473af322d798db551e59b48ccc465fd2_ilo__473af322d798db551e59b48ccc465fd2m21_m3726951652 (Il2CppObject * __this /* static, unused */, _473af322d798db551e59b48ccc465fd2_t2538674301 * ____this0, int32_t ____473af322d798db551e59b48ccc465fd2a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
