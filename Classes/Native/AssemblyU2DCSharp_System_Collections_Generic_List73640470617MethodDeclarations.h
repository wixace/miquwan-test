﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_Sort_GetDelegate_member40_arg0>c__AnonStorey95`1<System.Object>
struct U3CListA1_Sort_GetDelegate_member40_arg0U3Ec__AnonStorey95_1_t3640470617;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_Sort_GetDelegate_member40_arg0>c__AnonStorey95`1<System.Object>::.ctor()
extern "C"  void U3CListA1_Sort_GetDelegate_member40_arg0U3Ec__AnonStorey95_1__ctor_m2440284680_gshared (U3CListA1_Sort_GetDelegate_member40_arg0U3Ec__AnonStorey95_1_t3640470617 * __this, const MethodInfo* method);
#define U3CListA1_Sort_GetDelegate_member40_arg0U3Ec__AnonStorey95_1__ctor_m2440284680(__this, method) ((  void (*) (U3CListA1_Sort_GetDelegate_member40_arg0U3Ec__AnonStorey95_1_t3640470617 *, const MethodInfo*))U3CListA1_Sort_GetDelegate_member40_arg0U3Ec__AnonStorey95_1__ctor_m2440284680_gshared)(__this, method)
// System.Int32 System_Collections_Generic_List77Generated/<ListA1_Sort_GetDelegate_member40_arg0>c__AnonStorey95`1<System.Object>::<>m__BA(T,T)
extern "C"  int32_t U3CListA1_Sort_GetDelegate_member40_arg0U3Ec__AnonStorey95_1_U3CU3Em__BA_m3903861812_gshared (U3CListA1_Sort_GetDelegate_member40_arg0U3Ec__AnonStorey95_1_t3640470617 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define U3CListA1_Sort_GetDelegate_member40_arg0U3Ec__AnonStorey95_1_U3CU3Em__BA_m3903861812(__this, ___x0, ___y1, method) ((  int32_t (*) (U3CListA1_Sort_GetDelegate_member40_arg0U3Ec__AnonStorey95_1_t3640470617 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CListA1_Sort_GetDelegate_member40_arg0U3Ec__AnonStorey95_1_U3CU3Em__BA_m3903861812_gshared)(__this, ___x0, ___y1, method)
