﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIProgressBarGenerated
struct UIProgressBarGenerated_t600410685;
// JSVCall
struct JSVCall_t3708497963;
// UIProgressBar/OnDragFinished
struct OnDragFinished_t4207031714;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// UnityEngine.Transform
struct Transform_t1659122786;
// UIProgressBar
struct UIProgressBar_t168062834;
// UIWidget
struct UIWidget_t769069560;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_UIProgressBar168062834.h"
#include "AssemblyU2DCSharp_UIProgressBar_FillDirection1321036095.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UIProgressBarGenerated::.ctor()
extern "C"  void UIProgressBarGenerated__ctor_m2043535102 (UIProgressBarGenerated_t600410685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIProgressBarGenerated::UIProgressBar_UIProgressBar1(JSVCall,System.Int32)
extern "C"  bool UIProgressBarGenerated_UIProgressBar_UIProgressBar1_m690760470 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIProgressBarGenerated::UIProgressBar_current(JSVCall)
extern "C"  void UIProgressBarGenerated_UIProgressBar_current_m2108322751 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIProgressBar/OnDragFinished UIProgressBarGenerated::UIProgressBar_onDragFinished_GetDelegate_member1_arg0(CSRepresentedObject)
extern "C"  OnDragFinished_t4207031714 * UIProgressBarGenerated_UIProgressBar_onDragFinished_GetDelegate_member1_arg0_m369723080 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIProgressBarGenerated::UIProgressBar_onDragFinished(JSVCall)
extern "C"  void UIProgressBarGenerated_UIProgressBar_onDragFinished_m2937957903 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIProgressBarGenerated::UIProgressBar_thumb(JSVCall)
extern "C"  void UIProgressBarGenerated_UIProgressBar_thumb_m3895252098 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIProgressBarGenerated::UIProgressBar_numberOfSteps(JSVCall)
extern "C"  void UIProgressBarGenerated_UIProgressBar_numberOfSteps_m1610166129 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIProgressBarGenerated::UIProgressBar_onChange(JSVCall)
extern "C"  void UIProgressBarGenerated_UIProgressBar_onChange_m2525894309 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIProgressBarGenerated::UIProgressBar_cachedTransform(JSVCall)
extern "C"  void UIProgressBarGenerated_UIProgressBar_cachedTransform_m416861678 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIProgressBarGenerated::UIProgressBar_cachedCamera(JSVCall)
extern "C"  void UIProgressBarGenerated_UIProgressBar_cachedCamera_m3678237229 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIProgressBarGenerated::UIProgressBar_foregroundWidget(JSVCall)
extern "C"  void UIProgressBarGenerated_UIProgressBar_foregroundWidget_m1810913453 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIProgressBarGenerated::UIProgressBar_backgroundWidget(JSVCall)
extern "C"  void UIProgressBarGenerated_UIProgressBar_backgroundWidget_m1632876898 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIProgressBarGenerated::UIProgressBar_fillDirection(JSVCall)
extern "C"  void UIProgressBarGenerated_UIProgressBar_fillDirection_m3896775132 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIProgressBarGenerated::UIProgressBar_value(JSVCall)
extern "C"  void UIProgressBarGenerated_UIProgressBar_value_m1695961927 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIProgressBarGenerated::UIProgressBar_alpha(JSVCall)
extern "C"  void UIProgressBarGenerated_UIProgressBar_alpha_m3683241466 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIProgressBarGenerated::UIProgressBar_ForceUpdate(JSVCall,System.Int32)
extern "C"  bool UIProgressBarGenerated_UIProgressBar_ForceUpdate_m109637375 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIProgressBarGenerated::UIProgressBar_OnPan__Vector2(JSVCall,System.Int32)
extern "C"  bool UIProgressBarGenerated_UIProgressBar_OnPan__Vector2_m1787038088 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIProgressBarGenerated::__Register()
extern "C"  void UIProgressBarGenerated___Register_m2302495881 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIProgressBar/OnDragFinished UIProgressBarGenerated::<UIProgressBar_onDragFinished>m__15A()
extern "C"  OnDragFinished_t4207031714 * UIProgressBarGenerated_U3CUIProgressBar_onDragFinishedU3Em__15A_m374513793 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIProgressBarGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UIProgressBarGenerated_ilo_attachFinalizerObject1_m3047856545 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIProgressBarGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UIProgressBarGenerated_ilo_addJSCSRel2_m3625595707 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIProgressBarGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void UIProgressBarGenerated_ilo_setInt323_m45882198 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIProgressBarGenerated::ilo_getInt324(System.Int32)
extern "C"  int32_t UIProgressBarGenerated_ilo_getInt324_m1590602120 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIProgressBarGenerated::ilo_getObject5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIProgressBarGenerated_ilo_getObject5_m1313954165 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UIProgressBarGenerated::ilo_get_cachedTransform6(UIProgressBar)
extern "C"  Transform_t1659122786 * UIProgressBarGenerated_ilo_get_cachedTransform6_m1012791732 (Il2CppObject * __this /* static, unused */, UIProgressBar_t168062834 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIProgressBarGenerated::ilo_setObject7(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIProgressBarGenerated_ilo_setObject7_m1353021778 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget UIProgressBarGenerated::ilo_get_backgroundWidget8(UIProgressBar)
extern "C"  UIWidget_t769069560 * UIProgressBarGenerated_ilo_get_backgroundWidget8_m2419053919 (Il2CppObject * __this /* static, unused */, UIProgressBar_t168062834 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIProgressBar/FillDirection UIProgressBarGenerated::ilo_get_fillDirection9(UIProgressBar)
extern "C"  int32_t UIProgressBarGenerated_ilo_get_fillDirection9_m1927941197 (Il2CppObject * __this /* static, unused */, UIProgressBar_t168062834 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIProgressBarGenerated::ilo_set_value10(UIProgressBar,System.Single)
extern "C"  void UIProgressBarGenerated_ilo_set_value10_m1236777737 (Il2CppObject * __this /* static, unused */, UIProgressBar_t168062834 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UIProgressBarGenerated::ilo_getVector2S11(System.Int32)
extern "C"  Vector2_t4282066565  UIProgressBarGenerated_ilo_getVector2S11_m2833030073 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
