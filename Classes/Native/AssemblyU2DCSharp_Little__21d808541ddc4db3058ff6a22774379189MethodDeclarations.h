﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._21d808541ddc4db3058ff6a273048f7d
struct _21d808541ddc4db3058ff6a273048f7d_t2774379189;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._21d808541ddc4db3058ff6a273048f7d::.ctor()
extern "C"  void _21d808541ddc4db3058ff6a273048f7d__ctor_m1276229400 (_21d808541ddc4db3058ff6a273048f7d_t2774379189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._21d808541ddc4db3058ff6a273048f7d::_21d808541ddc4db3058ff6a273048f7dm2(System.Int32)
extern "C"  int32_t _21d808541ddc4db3058ff6a273048f7d__21d808541ddc4db3058ff6a273048f7dm2_m1502473593 (_21d808541ddc4db3058ff6a273048f7d_t2774379189 * __this, int32_t ____21d808541ddc4db3058ff6a273048f7da0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._21d808541ddc4db3058ff6a273048f7d::_21d808541ddc4db3058ff6a273048f7dm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _21d808541ddc4db3058ff6a273048f7d__21d808541ddc4db3058ff6a273048f7dm_m3837515101 (_21d808541ddc4db3058ff6a273048f7d_t2774379189 * __this, int32_t ____21d808541ddc4db3058ff6a273048f7da0, int32_t ____21d808541ddc4db3058ff6a273048f7d21, int32_t ____21d808541ddc4db3058ff6a273048f7dc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
