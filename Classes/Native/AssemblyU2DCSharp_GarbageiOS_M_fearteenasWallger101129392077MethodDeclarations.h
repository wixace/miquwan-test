﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_fearteenasWallger10
struct M_fearteenasWallger10_t1129392077;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_fearteenasWallger10::.ctor()
extern "C"  void M_fearteenasWallger10__ctor_m2276012598 (M_fearteenasWallger10_t1129392077 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fearteenasWallger10::M_qoojinalNootivu0(System.String[],System.Int32)
extern "C"  void M_fearteenasWallger10_M_qoojinalNootivu0_m1831982958 (M_fearteenasWallger10_t1129392077 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fearteenasWallger10::M_sezawcawLewehai1(System.String[],System.Int32)
extern "C"  void M_fearteenasWallger10_M_sezawcawLewehai1_m1565347418 (M_fearteenasWallger10_t1129392077 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fearteenasWallger10::M_meavirsallKawmedir2(System.String[],System.Int32)
extern "C"  void M_fearteenasWallger10_M_meavirsallKawmedir2_m876577555 (M_fearteenasWallger10_t1129392077 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fearteenasWallger10::M_yuballpem3(System.String[],System.Int32)
extern "C"  void M_fearteenasWallger10_M_yuballpem3_m1335670349 (M_fearteenasWallger10_t1129392077 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fearteenasWallger10::M_kateato4(System.String[],System.Int32)
extern "C"  void M_fearteenasWallger10_M_kateato4_m1310604406 (M_fearteenasWallger10_t1129392077 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
