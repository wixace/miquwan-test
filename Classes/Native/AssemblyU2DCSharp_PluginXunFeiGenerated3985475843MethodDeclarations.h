﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginXunFeiGenerated
struct PluginXunFeiGenerated_t3985475843;
// JSVCall
struct JSVCall_t3708497963;
// PluginXunFei
struct PluginXunFei_t2016385516;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_PluginXunFei2016385516.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void PluginXunFeiGenerated::.ctor()
extern "C"  void PluginXunFeiGenerated__ctor_m1860166600 (PluginXunFeiGenerated_t3985475843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_PluginXunFei1(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_PluginXunFei1_m3094628010 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFeiGenerated::PluginXunFei_isHasPermission(JSVCall)
extern "C"  void PluginXunFeiGenerated_PluginXunFei_isHasPermission_m2198854047 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFeiGenerated::PluginXunFei_IsLoginSuccess(JSVCall)
extern "C"  void PluginXunFeiGenerated_PluginXunFei_IsLoginSuccess_m2303730698 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_AddUserByGroup__String__Int32(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_AddUserByGroup__String__Int32_m230913254 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_CheckGroups__String(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_CheckGroups__String_m1224120018 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_CheckHasPermissionIMClientMgr(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_CheckHasPermissionIMClientMgr_m2543084941 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_CreateGroup__ZEvent(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_CreateGroup__ZEvent_m4041828200 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_CreateGroupInfo__Int32__String__String__String(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_CreateGroupInfo__Int32__String__String__String_m775084429 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_Init(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_Init_m367459373 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_InitPlayerIMClientMgr__String(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_InitPlayerIMClientMgr__String_m835093678 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_IsExistSound__String__String(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_IsExistSound__String__String_m2853665889 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_JoinChat__ZEvent(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_JoinChat__ZEvent_m1028384639 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_LoginIMClientMgr__String__String__String__String__String(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_LoginIMClientMgr__String__String__String__String__String_m4010984498 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_OnDestory(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_OnDestory_m4226262778 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_PausePlayIMClientMgr(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_PausePlayIMClientMgr_m802023132 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_PlayMsg__String__String__String__Boolean__String(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_PlayMsg__String__String__String__Boolean__String_m77064886 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_QuitChat__ZEvent(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_QuitChat__ZEvent_m3314656100 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_RemoveUserByGroup__String(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_RemoveUserByGroup__String_m699253007 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_RoleEnterGame__ZEvent(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_RoleEnterGame__ZEvent_m3121883865 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_SendUpAudioMsg__String__String__Boolean__String__String(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_SendUpAudioMsg__String__String__Boolean__String__String_m3892785407 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_StartPlayIMClientMgr(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_StartPlayIMClientMgr_m3529513968 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_StartRecordingIMClientMgr__String__String(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_StartRecordingIMClientMgr__String__String_m2844949281 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_StopPlayIMClientMgr(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_StopPlayIMClientMgr_m683869880 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::PluginXunFei_StopRecordingIMClientMgr(JSVCall,System.Int32)
extern "C"  bool PluginXunFeiGenerated_PluginXunFei_StopRecordingIMClientMgr_m862539703 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFeiGenerated::__Register()
extern "C"  void PluginXunFeiGenerated___Register_m461420031 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PluginXunFeiGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t PluginXunFeiGenerated_ilo_getObject1_m869875182 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool PluginXunFeiGenerated_ilo_attachFinalizerObject2_m3740400752 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFeiGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void PluginXunFeiGenerated_ilo_setBooleanS3_m2236057463 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXunFeiGenerated::ilo_getBooleanS4(System.Int32)
extern "C"  bool PluginXunFeiGenerated_ilo_getBooleanS4_m3391223263 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFeiGenerated::ilo_CheckGroups5(PluginXunFei,System.String)
extern "C"  void PluginXunFeiGenerated_ilo_CheckGroups5_m1597953464 (Il2CppObject * __this /* static, unused */, PluginXunFei_t2016385516 * ____this0, String_t* ___groupinfos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFeiGenerated::ilo_CheckHasPermissionIMClientMgr6(PluginXunFei)
extern "C"  void PluginXunFeiGenerated_ilo_CheckHasPermissionIMClientMgr6_m234366385 (Il2CppObject * __this /* static, unused */, PluginXunFei_t2016385516 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PluginXunFeiGenerated::ilo_getObject7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * PluginXunFeiGenerated_ilo_getObject7_m174700771 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFeiGenerated::ilo_InitPlayerIMClientMgr8(PluginXunFei,System.String)
extern "C"  void PluginXunFeiGenerated_ilo_InitPlayerIMClientMgr8_m468096863 (Il2CppObject * __this /* static, unused */, PluginXunFei_t2016385516 * ____this0, String_t* ___filePath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginXunFeiGenerated::ilo_getStringS9(System.Int32)
extern "C"  String_t* PluginXunFeiGenerated_ilo_getStringS9_m117376948 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFeiGenerated::ilo_OnDestory10(PluginXunFei)
extern "C"  void PluginXunFeiGenerated_ilo_OnDestory10_m777195515 (Il2CppObject * __this /* static, unused */, PluginXunFei_t2016385516 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFeiGenerated::ilo_SendUpAudioMsg11(PluginXunFei,System.String,System.String,System.Boolean,System.String,System.String)
extern "C"  void PluginXunFeiGenerated_ilo_SendUpAudioMsg11_m1140527628 (Il2CppObject * __this /* static, unused */, PluginXunFei_t2016385516 * ____this0, String_t* ___name1, String_t* ___path2, bool ___isGroup3, String_t* ___extend4, String_t* ___state5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXunFeiGenerated::ilo_StopRecordingIMClientMgr12(PluginXunFei)
extern "C"  void PluginXunFeiGenerated_ilo_StopRecordingIMClientMgr12_m1281399166 (Il2CppObject * __this /* static, unused */, PluginXunFei_t2016385516 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
