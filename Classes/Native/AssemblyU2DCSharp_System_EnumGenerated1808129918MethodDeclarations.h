﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_EnumGenerated
struct System_EnumGenerated_t1808129918;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_EnumGenerated::.ctor()
extern "C"  void System_EnumGenerated__ctor_m1320356061 (System_EnumGenerated_t1808129918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_CompareTo__Object(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_CompareTo__Object_m2723982572 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_Equals__Object(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_Equals__Object_m2691840115 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_GetHashCode(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_GetHashCode_m1513663614 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_GetTypeCode(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_GetTypeCode_m3278363562 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_ToString__String(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_ToString__String_m319252658 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_ToString(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_ToString_m4233468769 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_Format__Type__Object__String(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_Format__Type__Object__String_m364127606 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_GetName__Type__Object(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_GetName__Type__Object_m2868560103 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_GetNames__Type(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_GetNames__Type_m147443009 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_GetUnderlyingType__Type(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_GetUnderlyingType__Type_m1756303124 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_GetValues__Type(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_GetValues__Type_m2991246207 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_IsDefined__Type__Object(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_IsDefined__Type__Object_m2743444837 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_Parse__Type__String__Boolean(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_Parse__Type__String__Boolean_m3889275807 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_Parse__Type__String(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_Parse__Type__String_m2297344491 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_ToObject__Type__UInt16(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_ToObject__Type__UInt16_m3337309928 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_ToObject__Type__UInt32(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_ToObject__Type__UInt32_m2519205794 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_ToObject__Type__UInt64(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_ToObject__Type__UInt64_m512747201 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_ToObject__Type__Int32(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_ToObject__Type__Int32_m2046773735 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_ToObject__Type__Int16(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_ToObject__Type__Int16_m2864877869 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_ToObject__Type__Byte(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_ToObject__Type__Byte_m3866032721 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_ToObject__Type__SByte(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_ToObject__Type__SByte_m3204970420 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_ToObject__Type__Object(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_ToObject__Type__Object_m621214536 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_EnumGenerated::Enum_ToObject__Type__Int64(JSVCall,System.Int32)
extern "C"  bool System_EnumGenerated_Enum_ToObject__Type__Int64_m40315142 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_EnumGenerated::__Register()
extern "C"  void System_EnumGenerated___Register_m1857189386 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_EnumGenerated::ilo_getWhatever1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_EnumGenerated_ilo_getWhatever1_m1844473775 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_EnumGenerated::ilo_setInt322(System.Int32,System.Int32)
extern "C"  void System_EnumGenerated_ilo_setInt322_m2193285624 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_EnumGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void System_EnumGenerated_ilo_setBooleanS3_m2410362060 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System_EnumGenerated::ilo_getStringS4(System.Int32)
extern "C"  String_t* System_EnumGenerated_ilo_getStringS4_m1711214110 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_EnumGenerated::ilo_getObject5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_EnumGenerated_ilo_getObject5_m3875576822 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_EnumGenerated::ilo_setArrayS6(System.Int32,System.Int32,System.Boolean)
extern "C"  void System_EnumGenerated_ilo_setArrayS6_m4235680651 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_EnumGenerated::ilo_setObject7(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_EnumGenerated_ilo_setObject7_m3507726419 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_EnumGenerated::ilo_setWhatever8(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  void System_EnumGenerated_ilo_setWhatever8_m517207363 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System_EnumGenerated::ilo_getUInt329(System.Int32)
extern "C"  uint32_t System_EnumGenerated_ilo_getUInt329_m2776907536 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System_EnumGenerated::ilo_getUInt6410(System.Int32)
extern "C"  uint64_t System_EnumGenerated_ilo_getUInt6410_m3517204682 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System_EnumGenerated::ilo_getByte11(System.Int32)
extern "C"  uint8_t System_EnumGenerated_ilo_getByte11_m2604302187 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System_EnumGenerated::ilo_getInt6412(System.Int32)
extern "C"  int64_t System_EnumGenerated_ilo_getInt6412_m418591890 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
