﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SoftJointLimitSpring
struct  SoftJointLimitSpring_t4258895628 
{
public:
	// System.Single UnityEngine.SoftJointLimitSpring::m_Spring
	float ___m_Spring_0;
	// System.Single UnityEngine.SoftJointLimitSpring::m_Damper
	float ___m_Damper_1;

public:
	inline static int32_t get_offset_of_m_Spring_0() { return static_cast<int32_t>(offsetof(SoftJointLimitSpring_t4258895628, ___m_Spring_0)); }
	inline float get_m_Spring_0() const { return ___m_Spring_0; }
	inline float* get_address_of_m_Spring_0() { return &___m_Spring_0; }
	inline void set_m_Spring_0(float value)
	{
		___m_Spring_0 = value;
	}

	inline static int32_t get_offset_of_m_Damper_1() { return static_cast<int32_t>(offsetof(SoftJointLimitSpring_t4258895628, ___m_Damper_1)); }
	inline float get_m_Damper_1() const { return ___m_Damper_1; }
	inline float* get_address_of_m_Damper_1() { return &___m_Damper_1; }
	inline void set_m_Damper_1(float value)
	{
		___m_Damper_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: UnityEngine.SoftJointLimitSpring
struct SoftJointLimitSpring_t4258895628_marshaled_pinvoke
{
	float ___m_Spring_0;
	float ___m_Damper_1;
};
// Native definition for marshalling of: UnityEngine.SoftJointLimitSpring
struct SoftJointLimitSpring_t4258895628_marshaled_com
{
	float ___m_Spring_0;
	float ___m_Damper_1;
};
