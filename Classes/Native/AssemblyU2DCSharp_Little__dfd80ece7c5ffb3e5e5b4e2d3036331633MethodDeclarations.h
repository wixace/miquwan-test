﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._dfd80ece7c5ffb3e5e5b4e2dea8e8ef0
struct _dfd80ece7c5ffb3e5e5b4e2dea8e8ef0_t3036331633;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__dfd80ece7c5ffb3e5e5b4e2d3036331633.h"

// System.Void Little._dfd80ece7c5ffb3e5e5b4e2dea8e8ef0::.ctor()
extern "C"  void _dfd80ece7c5ffb3e5e5b4e2dea8e8ef0__ctor_m1449727708 (_dfd80ece7c5ffb3e5e5b4e2dea8e8ef0_t3036331633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._dfd80ece7c5ffb3e5e5b4e2dea8e8ef0::_dfd80ece7c5ffb3e5e5b4e2dea8e8ef0m2(System.Int32)
extern "C"  int32_t _dfd80ece7c5ffb3e5e5b4e2dea8e8ef0__dfd80ece7c5ffb3e5e5b4e2dea8e8ef0m2_m1663923193 (_dfd80ece7c5ffb3e5e5b4e2dea8e8ef0_t3036331633 * __this, int32_t ____dfd80ece7c5ffb3e5e5b4e2dea8e8ef0a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._dfd80ece7c5ffb3e5e5b4e2dea8e8ef0::_dfd80ece7c5ffb3e5e5b4e2dea8e8ef0m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _dfd80ece7c5ffb3e5e5b4e2dea8e8ef0__dfd80ece7c5ffb3e5e5b4e2dea8e8ef0m_m1244176093 (_dfd80ece7c5ffb3e5e5b4e2dea8e8ef0_t3036331633 * __this, int32_t ____dfd80ece7c5ffb3e5e5b4e2dea8e8ef0a0, int32_t ____dfd80ece7c5ffb3e5e5b4e2dea8e8ef0481, int32_t ____dfd80ece7c5ffb3e5e5b4e2dea8e8ef0c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._dfd80ece7c5ffb3e5e5b4e2dea8e8ef0::ilo__dfd80ece7c5ffb3e5e5b4e2dea8e8ef0m21(Little._dfd80ece7c5ffb3e5e5b4e2dea8e8ef0,System.Int32)
extern "C"  int32_t _dfd80ece7c5ffb3e5e5b4e2dea8e8ef0_ilo__dfd80ece7c5ffb3e5e5b4e2dea8e8ef0m21_m2884568408 (Il2CppObject * __this /* static, unused */, _dfd80ece7c5ffb3e5e5b4e2dea8e8ef0_t3036331633 * ____this0, int32_t ____dfd80ece7c5ffb3e5e5b4e2dea8e8ef0a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
