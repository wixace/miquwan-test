﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AstarPath/<ScanLoop>c__AnonStorey106
struct U3CScanLoopU3Ec__AnonStorey106_t3437720936;

#include "codegen/il2cpp-codegen.h"

// System.Void AstarPath/<ScanLoop>c__AnonStorey106::.ctor()
extern "C"  void U3CScanLoopU3Ec__AnonStorey106__ctor_m2058570291 (U3CScanLoopU3Ec__AnonStorey106_t3437720936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
