﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.Join>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m107783805(__this, ___l0, method) ((  void (*) (Enumerator_t1063008830 *, List_1_t1043336060 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.Join>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3760821173(__this, method) ((  void (*) (Enumerator_t1063008830 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.Join>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1358533089(__this, method) ((  Il2CppObject * (*) (Enumerator_t1063008830 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.Join>::Dispose()
#define Enumerator_Dispose_m3900236706(__this, method) ((  void (*) (Enumerator_t1063008830 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.Join>::VerifyState()
#define Enumerator_VerifyState_m3714974171(__this, method) ((  void (*) (Enumerator_t1063008830 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.Join>::MoveNext()
#define Enumerator_MoveNext_m1994272929(__this, method) ((  bool (*) (Enumerator_t1063008830 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.Join>::get_Current()
#define Enumerator_get_Current_m2029203346(__this, method) ((  Join_t3970117804 * (*) (Enumerator_t1063008830 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
