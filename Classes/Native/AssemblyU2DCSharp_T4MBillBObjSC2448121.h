﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Renderer
struct Renderer_t3076687687;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// T4MBillBObjSC
struct  T4MBillBObjSC_t2448121  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Renderer T4MBillBObjSC::Render
	Renderer_t3076687687 * ___Render_2;
	// UnityEngine.Transform T4MBillBObjSC::Transf
	Transform_t1659122786 * ___Transf_3;

public:
	inline static int32_t get_offset_of_Render_2() { return static_cast<int32_t>(offsetof(T4MBillBObjSC_t2448121, ___Render_2)); }
	inline Renderer_t3076687687 * get_Render_2() const { return ___Render_2; }
	inline Renderer_t3076687687 ** get_address_of_Render_2() { return &___Render_2; }
	inline void set_Render_2(Renderer_t3076687687 * value)
	{
		___Render_2 = value;
		Il2CppCodeGenWriteBarrier(&___Render_2, value);
	}

	inline static int32_t get_offset_of_Transf_3() { return static_cast<int32_t>(offsetof(T4MBillBObjSC_t2448121, ___Transf_3)); }
	inline Transform_t1659122786 * get_Transf_3() const { return ___Transf_3; }
	inline Transform_t1659122786 ** get_address_of_Transf_3() { return &___Transf_3; }
	inline void set_Transf_3(Transform_t1659122786 * value)
	{
		___Transf_3 = value;
		Il2CppCodeGenWriteBarrier(&___Transf_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
