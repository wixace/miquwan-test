﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Core.RpsTrigger
struct RpsTrigger_t2657866263;
// UnityEngine.Collider
struct Collider_t2939674232;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"

// System.Void Core.RpsTrigger::.ctor()
extern "C"  void RpsTrigger__ctor_m3479891961 (RpsTrigger_t2657866263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Core.RpsTrigger::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void RpsTrigger_OnTriggerEnter_m2728282655 (RpsTrigger_t2657866263 * __this, Collider_t2939674232 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Core.RpsTrigger::OnTriggerExit(UnityEngine.Collider)
extern "C"  void RpsTrigger_OnTriggerExit_m430668707 (RpsTrigger_t2657866263 * __this, Collider_t2939674232 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
