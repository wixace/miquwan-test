﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Poly2Tri.TriangulationContext
struct TriangulationContext_t3528662164;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Poly2Tri.TriangulationDebugContext
struct  TriangulationDebugContext_t3598090913  : public Il2CppObject
{
public:
	// Pathfinding.Poly2Tri.TriangulationContext Pathfinding.Poly2Tri.TriangulationDebugContext::_tcx
	TriangulationContext_t3528662164 * ____tcx_0;

public:
	inline static int32_t get_offset_of__tcx_0() { return static_cast<int32_t>(offsetof(TriangulationDebugContext_t3598090913, ____tcx_0)); }
	inline TriangulationContext_t3528662164 * get__tcx_0() const { return ____tcx_0; }
	inline TriangulationContext_t3528662164 ** get_address_of__tcx_0() { return &____tcx_0; }
	inline void set__tcx_0(TriangulationContext_t3528662164 * value)
	{
		____tcx_0 = value;
		Il2CppCodeGenWriteBarrier(&____tcx_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
