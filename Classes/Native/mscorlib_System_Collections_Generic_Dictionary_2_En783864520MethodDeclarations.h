﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1190322445MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,CSHeroUnit>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m780273265(__this, ___dictionary0, method) ((  void (*) (Enumerator_t783864520 *, Dictionary_2_t3761508424 *, const MethodInfo*))Enumerator__ctor_m330901371_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,CSHeroUnit>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2942416090(__this, method) ((  Il2CppObject * (*) (Enumerator_t783864520 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1174381702_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,CSHeroUnit>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3561047460(__this, method) ((  void (*) (Enumerator_t783864520 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2713473626_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,CSHeroUnit>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1236173595(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t783864520 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2775655267_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,CSHeroUnit>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2649253430(__this, method) ((  Il2CppObject * (*) (Enumerator_t783864520 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2261181666_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,CSHeroUnit>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m65376072(__this, method) ((  Il2CppObject * (*) (Enumerator_t783864520 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m790565620_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,CSHeroUnit>::MoveNext()
#define Enumerator_MoveNext_m2806133204(__this, method) ((  bool (*) (Enumerator_t783864520 *, const MethodInfo*))Enumerator_MoveNext_m1009697350_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,CSHeroUnit>::get_Current()
#define Enumerator_get_Current_m1212774376(__this, method) ((  KeyValuePair_2_t3660289130  (*) (Enumerator_t783864520 *, const MethodInfo*))Enumerator_get_Current_m303915562_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,CSHeroUnit>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2236293341(__this, method) ((  uint32_t (*) (Enumerator_t783864520 *, const MethodInfo*))Enumerator_get_CurrentKey_m4277694867_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,CSHeroUnit>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1266548573(__this, method) ((  CSHeroUnit_t3764358446 * (*) (Enumerator_t783864520 *, const MethodInfo*))Enumerator_get_CurrentValue_m490765687_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,CSHeroUnit>::Reset()
#define Enumerator_Reset_m1194559299(__this, method) ((  void (*) (Enumerator_t783864520 *, const MethodInfo*))Enumerator_Reset_m1609873741_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,CSHeroUnit>::VerifyState()
#define Enumerator_VerifyState_m1216813324(__this, method) ((  void (*) (Enumerator_t783864520 *, const MethodInfo*))Enumerator_VerifyState_m353966998_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,CSHeroUnit>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3871371636(__this, method) ((  void (*) (Enumerator_t783864520 *, const MethodInfo*))Enumerator_VerifyCurrent_m3604740478_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,CSHeroUnit>::Dispose()
#define Enumerator_Dispose_m3738647379(__this, method) ((  void (*) (Enumerator_t783864520 *, const MethodInfo*))Enumerator_Dispose_m3423867613_gshared)(__this, method)
