﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSPlusAtt
struct  CSPlusAtt_t3268315159  : public Il2CppObject
{
public:
	// System.String CSPlusAtt::key
	String_t* ___key_0;
	// System.UInt32 CSPlusAtt::valuePer
	uint32_t ___valuePer_1;
	// System.UInt32 CSPlusAtt::value
	uint32_t ___value_2;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(CSPlusAtt_t3268315159, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_valuePer_1() { return static_cast<int32_t>(offsetof(CSPlusAtt_t3268315159, ___valuePer_1)); }
	inline uint32_t get_valuePer_1() const { return ___valuePer_1; }
	inline uint32_t* get_address_of_valuePer_1() { return &___valuePer_1; }
	inline void set_valuePer_1(uint32_t value)
	{
		___valuePer_1 = value;
	}

	inline static int32_t get_offset_of_value_2() { return static_cast<int32_t>(offsetof(CSPlusAtt_t3268315159, ___value_2)); }
	inline uint32_t get_value_2() const { return ___value_2; }
	inline uint32_t* get_address_of_value_2() { return &___value_2; }
	inline void set_value_2(uint32_t value)
	{
		___value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
