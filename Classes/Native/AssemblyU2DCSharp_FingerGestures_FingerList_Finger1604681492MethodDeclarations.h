﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FingerGestures/FingerList/FingerPropertyGetterDelegate`1<System.Single>
struct FingerPropertyGetterDelegate_1_t1604681492;
// System.Object
struct Il2CppObject;
// FingerGestures/Finger
struct Finger_t182428197;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_FingerGestures_Finger182428197.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void FingerGestures/FingerList/FingerPropertyGetterDelegate`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void FingerPropertyGetterDelegate_1__ctor_m947566239_gshared (FingerPropertyGetterDelegate_1_t1604681492 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define FingerPropertyGetterDelegate_1__ctor_m947566239(__this, ___object0, ___method1, method) ((  void (*) (FingerPropertyGetterDelegate_1_t1604681492 *, Il2CppObject *, IntPtr_t, const MethodInfo*))FingerPropertyGetterDelegate_1__ctor_m947566239_gshared)(__this, ___object0, ___method1, method)
// T FingerGestures/FingerList/FingerPropertyGetterDelegate`1<System.Single>::Invoke(FingerGestures/Finger)
extern "C"  float FingerPropertyGetterDelegate_1_Invoke_m2252451155_gshared (FingerPropertyGetterDelegate_1_t1604681492 * __this, Finger_t182428197 * ___finger0, const MethodInfo* method);
#define FingerPropertyGetterDelegate_1_Invoke_m2252451155(__this, ___finger0, method) ((  float (*) (FingerPropertyGetterDelegate_1_t1604681492 *, Finger_t182428197 *, const MethodInfo*))FingerPropertyGetterDelegate_1_Invoke_m2252451155_gshared)(__this, ___finger0, method)
// System.IAsyncResult FingerGestures/FingerList/FingerPropertyGetterDelegate`1<System.Single>::BeginInvoke(FingerGestures/Finger,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FingerPropertyGetterDelegate_1_BeginInvoke_m3950532075_gshared (FingerPropertyGetterDelegate_1_t1604681492 * __this, Finger_t182428197 * ___finger0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define FingerPropertyGetterDelegate_1_BeginInvoke_m3950532075(__this, ___finger0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (FingerPropertyGetterDelegate_1_t1604681492 *, Finger_t182428197 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))FingerPropertyGetterDelegate_1_BeginInvoke_m3950532075_gshared)(__this, ___finger0, ___callback1, ___object2, method)
// T FingerGestures/FingerList/FingerPropertyGetterDelegate`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  float FingerPropertyGetterDelegate_1_EndInvoke_m2465840080_gshared (FingerPropertyGetterDelegate_1_t1604681492 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define FingerPropertyGetterDelegate_1_EndInvoke_m2465840080(__this, ___result0, method) ((  float (*) (FingerPropertyGetterDelegate_1_t1604681492 *, Il2CppObject *, const MethodInfo*))FingerPropertyGetterDelegate_1_EndInvoke_m2465840080_gshared)(__this, ___result0, method)
