﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_biskouwel198
struct M_biskouwel198_t2348119943;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_biskouwel198::.ctor()
extern "C"  void M_biskouwel198__ctor_m3220718220 (M_biskouwel198_t2348119943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_biskouwel198::M_yebaHoowhur0(System.String[],System.Int32)
extern "C"  void M_biskouwel198_M_yebaHoowhur0_m2850478822 (M_biskouwel198_t2348119943 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
