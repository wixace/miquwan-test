﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MethodID
struct MethodID_t3916401116;
// JSDataExchangeMgr/DGetV`1<Mihua.Asset.OnLoadAsset>
struct DGetV_1_t4059194466;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mihua_Asset_AssetMgrGenerated
struct  Mihua_Asset_AssetMgrGenerated_t1229231121  : public Il2CppObject
{
public:

public:
};

struct Mihua_Asset_AssetMgrGenerated_t1229231121_StaticFields
{
public:
	// MethodID Mihua_Asset_AssetMgrGenerated::methodID8
	MethodID_t3916401116 * ___methodID8_0;
	// MethodID Mihua_Asset_AssetMgrGenerated::methodID15
	MethodID_t3916401116 * ___methodID15_1;
	// JSDataExchangeMgr/DGetV`1<Mihua.Asset.OnLoadAsset> Mihua_Asset_AssetMgrGenerated::<>f__am$cache2
	DGetV_1_t4059194466 * ___U3CU3Ef__amU24cache2_2;
	// JSDataExchangeMgr/DGetV`1<Mihua.Asset.OnLoadAsset> Mihua_Asset_AssetMgrGenerated::<>f__am$cache3
	DGetV_1_t4059194466 * ___U3CU3Ef__amU24cache3_3;

public:
	inline static int32_t get_offset_of_methodID8_0() { return static_cast<int32_t>(offsetof(Mihua_Asset_AssetMgrGenerated_t1229231121_StaticFields, ___methodID8_0)); }
	inline MethodID_t3916401116 * get_methodID8_0() const { return ___methodID8_0; }
	inline MethodID_t3916401116 ** get_address_of_methodID8_0() { return &___methodID8_0; }
	inline void set_methodID8_0(MethodID_t3916401116 * value)
	{
		___methodID8_0 = value;
		Il2CppCodeGenWriteBarrier(&___methodID8_0, value);
	}

	inline static int32_t get_offset_of_methodID15_1() { return static_cast<int32_t>(offsetof(Mihua_Asset_AssetMgrGenerated_t1229231121_StaticFields, ___methodID15_1)); }
	inline MethodID_t3916401116 * get_methodID15_1() const { return ___methodID15_1; }
	inline MethodID_t3916401116 ** get_address_of_methodID15_1() { return &___methodID15_1; }
	inline void set_methodID15_1(MethodID_t3916401116 * value)
	{
		___methodID15_1 = value;
		Il2CppCodeGenWriteBarrier(&___methodID15_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(Mihua_Asset_AssetMgrGenerated_t1229231121_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline DGetV_1_t4059194466 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline DGetV_1_t4059194466 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(DGetV_1_t4059194466 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(Mihua_Asset_AssetMgrGenerated_t1229231121_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline DGetV_1_t4059194466 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline DGetV_1_t4059194466 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(DGetV_1_t4059194466 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
