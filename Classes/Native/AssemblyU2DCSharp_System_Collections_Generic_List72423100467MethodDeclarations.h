﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_FindLastIndex_GetDelegate_member20_arg1>c__AnonStorey8D`1<System.Object>
struct U3CListA1_FindLastIndex_GetDelegate_member20_arg1U3Ec__AnonStorey8D_1_t2423100467;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_FindLastIndex_GetDelegate_member20_arg1>c__AnonStorey8D`1<System.Object>::.ctor()
extern "C"  void U3CListA1_FindLastIndex_GetDelegate_member20_arg1U3Ec__AnonStorey8D_1__ctor_m3092675650_gshared (U3CListA1_FindLastIndex_GetDelegate_member20_arg1U3Ec__AnonStorey8D_1_t2423100467 * __this, const MethodInfo* method);
#define U3CListA1_FindLastIndex_GetDelegate_member20_arg1U3Ec__AnonStorey8D_1__ctor_m3092675650(__this, method) ((  void (*) (U3CListA1_FindLastIndex_GetDelegate_member20_arg1U3Ec__AnonStorey8D_1_t2423100467 *, const MethodInfo*))U3CListA1_FindLastIndex_GetDelegate_member20_arg1U3Ec__AnonStorey8D_1__ctor_m3092675650_gshared)(__this, method)
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_FindLastIndex_GetDelegate_member20_arg1>c__AnonStorey8D`1<System.Object>::<>m__B2(T)
extern "C"  bool U3CListA1_FindLastIndex_GetDelegate_member20_arg1U3Ec__AnonStorey8D_1_U3CU3Em__B2_m1065509071_gshared (U3CListA1_FindLastIndex_GetDelegate_member20_arg1U3Ec__AnonStorey8D_1_t2423100467 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CListA1_FindLastIndex_GetDelegate_member20_arg1U3Ec__AnonStorey8D_1_U3CU3Em__B2_m1065509071(__this, ___obj0, method) ((  bool (*) (U3CListA1_FindLastIndex_GetDelegate_member20_arg1U3Ec__AnonStorey8D_1_t2423100467 *, Il2CppObject *, const MethodInfo*))U3CListA1_FindLastIndex_GetDelegate_member20_arg1U3Ec__AnonStorey8D_1_U3CU3Em__B2_m1065509071_gshared)(__this, ___obj0, method)
