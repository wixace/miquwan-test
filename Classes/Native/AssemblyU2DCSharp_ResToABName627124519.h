﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t1844984270;

#include "UnityEngine_UnityEngine_ScriptableObject2970544072.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResToABName
struct  ResToABName_t627124519  : public ScriptableObject_t2970544072
{
public:
	// System.Collections.Generic.List`1<System.String> ResToABName::resNameList
	List_1_t1375417109 * ___resNameList_2;
	// System.Collections.Generic.List`1<System.String> ResToABName::loadResNameList
	List_1_t1375417109 * ___loadResNameList_3;
	// System.Collections.Generic.List`1<System.String> ResToABName::abNameList
	List_1_t1375417109 * ___abNameList_4;
	// System.Collections.Generic.List`1<System.Boolean> ResToABName::isCompressList
	List_1_t1844984270 * ___isCompressList_5;

public:
	inline static int32_t get_offset_of_resNameList_2() { return static_cast<int32_t>(offsetof(ResToABName_t627124519, ___resNameList_2)); }
	inline List_1_t1375417109 * get_resNameList_2() const { return ___resNameList_2; }
	inline List_1_t1375417109 ** get_address_of_resNameList_2() { return &___resNameList_2; }
	inline void set_resNameList_2(List_1_t1375417109 * value)
	{
		___resNameList_2 = value;
		Il2CppCodeGenWriteBarrier(&___resNameList_2, value);
	}

	inline static int32_t get_offset_of_loadResNameList_3() { return static_cast<int32_t>(offsetof(ResToABName_t627124519, ___loadResNameList_3)); }
	inline List_1_t1375417109 * get_loadResNameList_3() const { return ___loadResNameList_3; }
	inline List_1_t1375417109 ** get_address_of_loadResNameList_3() { return &___loadResNameList_3; }
	inline void set_loadResNameList_3(List_1_t1375417109 * value)
	{
		___loadResNameList_3 = value;
		Il2CppCodeGenWriteBarrier(&___loadResNameList_3, value);
	}

	inline static int32_t get_offset_of_abNameList_4() { return static_cast<int32_t>(offsetof(ResToABName_t627124519, ___abNameList_4)); }
	inline List_1_t1375417109 * get_abNameList_4() const { return ___abNameList_4; }
	inline List_1_t1375417109 ** get_address_of_abNameList_4() { return &___abNameList_4; }
	inline void set_abNameList_4(List_1_t1375417109 * value)
	{
		___abNameList_4 = value;
		Il2CppCodeGenWriteBarrier(&___abNameList_4, value);
	}

	inline static int32_t get_offset_of_isCompressList_5() { return static_cast<int32_t>(offsetof(ResToABName_t627124519, ___isCompressList_5)); }
	inline List_1_t1844984270 * get_isCompressList_5() const { return ___isCompressList_5; }
	inline List_1_t1844984270 ** get_address_of_isCompressList_5() { return &___isCompressList_5; }
	inline void set_isCompressList_5(List_1_t1844984270 * value)
	{
		___isCompressList_5 = value;
		Il2CppCodeGenWriteBarrier(&___isCompressList_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
