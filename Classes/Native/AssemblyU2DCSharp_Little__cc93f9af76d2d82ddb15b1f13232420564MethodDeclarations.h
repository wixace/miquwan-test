﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._cc93f9af76d2d82ddb15b1f155c0d463
struct _cc93f9af76d2d82ddb15b1f155c0d463_t3232420564;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._cc93f9af76d2d82ddb15b1f155c0d463::.ctor()
extern "C"  void _cc93f9af76d2d82ddb15b1f155c0d463__ctor_m2784379353 (_cc93f9af76d2d82ddb15b1f155c0d463_t3232420564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._cc93f9af76d2d82ddb15b1f155c0d463::_cc93f9af76d2d82ddb15b1f155c0d463m2(System.Int32)
extern "C"  int32_t _cc93f9af76d2d82ddb15b1f155c0d463__cc93f9af76d2d82ddb15b1f155c0d463m2_m1661567257 (_cc93f9af76d2d82ddb15b1f155c0d463_t3232420564 * __this, int32_t ____cc93f9af76d2d82ddb15b1f155c0d463a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._cc93f9af76d2d82ddb15b1f155c0d463::_cc93f9af76d2d82ddb15b1f155c0d463m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _cc93f9af76d2d82ddb15b1f155c0d463__cc93f9af76d2d82ddb15b1f155c0d463m_m3727660477 (_cc93f9af76d2d82ddb15b1f155c0d463_t3232420564 * __this, int32_t ____cc93f9af76d2d82ddb15b1f155c0d463a0, int32_t ____cc93f9af76d2d82ddb15b1f155c0d463931, int32_t ____cc93f9af76d2d82ddb15b1f155c0d463c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
