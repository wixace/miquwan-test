﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1190435706MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<TargetMgr/AngleEntity>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3215746364(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1581933029 *, Dictionary_2_t264609637 *, const MethodInfo*))Enumerator__ctor_m2377115088_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<TargetMgr/AngleEntity>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2365552421(__this, method) ((  Il2CppObject * (*) (Enumerator_t1581933029 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1037642267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<TargetMgr/AngleEntity>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2771181049(__this, method) ((  void (*) (Enumerator_t1581933029 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2809374949_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<TargetMgr/AngleEntity>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m922676610(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1581933029 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2434214620_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<TargetMgr/AngleEntity>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m169870401(__this, method) ((  Il2CppObject * (*) (Enumerator_t1581933029 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3735627447_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<TargetMgr/AngleEntity>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1085134483(__this, method) ((  Il2CppObject * (*) (Enumerator_t1581933029 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m393753481_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<TargetMgr/AngleEntity>>::MoveNext()
#define Enumerator_MoveNext_m3421246693(__this, method) ((  bool (*) (Enumerator_t1581933029 *, const MethodInfo*))Enumerator_MoveNext_m1213995029_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<TargetMgr/AngleEntity>>::get_Current()
#define Enumerator_get_Current_m1439405739(__this, method) ((  KeyValuePair_2_t163390343  (*) (Enumerator_t1581933029 *, const MethodInfo*))Enumerator_get_Current_m1399860359_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<TargetMgr/AngleEntity>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m861130354(__this, method) ((  int32_t (*) (Enumerator_t1581933029 *, const MethodInfo*))Enumerator_get_CurrentKey_m1767398110_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<TargetMgr/AngleEntity>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2199545878(__this, method) ((  List_1_t267346398 * (*) (Enumerator_t1581933029 *, const MethodInfo*))Enumerator_get_CurrentValue_m3384846750_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<TargetMgr/AngleEntity>>::Reset()
#define Enumerator_Reset_m1786701198(__this, method) ((  void (*) (Enumerator_t1581933029 *, const MethodInfo*))Enumerator_Reset_m1080084514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<TargetMgr/AngleEntity>>::VerifyState()
#define Enumerator_VerifyState_m2428982039(__this, method) ((  void (*) (Enumerator_t1581933029 *, const MethodInfo*))Enumerator_VerifyState_m2404513451_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<TargetMgr/AngleEntity>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m534402239(__this, method) ((  void (*) (Enumerator_t1581933029 *, const MethodInfo*))Enumerator_VerifyCurrent_m2789892947_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Collections.Generic.List`1<TargetMgr/AngleEntity>>::Dispose()
#define Enumerator_Dispose_m1556361950(__this, method) ((  void (*) (Enumerator_t1581933029 *, const MethodInfo*))Enumerator_Dispose_m1102561394_gshared)(__this, method)
