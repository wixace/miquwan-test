﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.TerrainCollider
struct TerrainCollider_t520679551;
// UnityEngine.TerrainData
struct TerrainData_t865146677;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_TerrainData865146677.h"

// System.Void UnityEngine.TerrainCollider::.ctor()
extern "C"  void TerrainCollider__ctor_m5579824 (TerrainCollider_t520679551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TerrainData UnityEngine.TerrainCollider::get_terrainData()
extern "C"  TerrainData_t865146677 * TerrainCollider_get_terrainData_m1212282927 (TerrainCollider_t520679551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TerrainCollider::set_terrainData(UnityEngine.TerrainData)
extern "C"  void TerrainCollider_set_terrainData_m89843130 (TerrainCollider_t520679551 * __this, TerrainData_t865146677 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
