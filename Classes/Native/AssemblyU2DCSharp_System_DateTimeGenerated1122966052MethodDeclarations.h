﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_DateTimeGenerated
struct System_DateTimeGenerated_t1122966052;
// JSVCall
struct JSVCall_t3708497963;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_String7231557.h"

// System.Void System_DateTimeGenerated::.ctor()
extern "C"  void System_DateTimeGenerated__ctor_m3942882551 (System_DateTimeGenerated_t1122966052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_DateTime1(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_DateTime1_m4103960291 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_DateTime2(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_DateTime2_m1053757476 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_DateTime3(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_DateTime3_m2298521957 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_DateTime4(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_DateTime4_m3543286438 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_DateTime5(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_DateTime5_m493083623 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_DateTime6(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_DateTime6_m1737848104 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_DateTime7(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_DateTime7_m2982612585 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_DateTime8(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_DateTime8_m4227377066 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_DateTime9(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_DateTime9_m1177174251 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_DateTime10(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_DateTime10_m1487548847 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_DateTime11(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_DateTime11_m2732313328 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::DateTime_MaxValue(JSVCall)
extern "C"  void System_DateTimeGenerated_DateTime_MaxValue_m3500099681 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::DateTime_MinValue(JSVCall)
extern "C"  void System_DateTimeGenerated_DateTime_MinValue_m1520177359 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::DateTime_Date(JSVCall)
extern "C"  void System_DateTimeGenerated_DateTime_Date_m4053598272 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::DateTime_Month(JSVCall)
extern "C"  void System_DateTimeGenerated_DateTime_Month_m60731742 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::DateTime_Day(JSVCall)
extern "C"  void System_DateTimeGenerated_DateTime_Day_m344491458 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::DateTime_DayOfWeek(JSVCall)
extern "C"  void System_DateTimeGenerated_DateTime_DayOfWeek_m997902999 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::DateTime_DayOfYear(JSVCall)
extern "C"  void System_DateTimeGenerated_DateTime_DayOfYear_m1928340590 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::DateTime_TimeOfDay(JSVCall)
extern "C"  void System_DateTimeGenerated_DateTime_TimeOfDay_m3912757094 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::DateTime_Hour(JSVCall)
extern "C"  void System_DateTimeGenerated_DateTime_Hour_m335517386 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::DateTime_Minute(JSVCall)
extern "C"  void System_DateTimeGenerated_DateTime_Minute_m2231046810 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::DateTime_Second(JSVCall)
extern "C"  void System_DateTimeGenerated_DateTime_Second_m959946810 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::DateTime_Millisecond(JSVCall)
extern "C"  void System_DateTimeGenerated_DateTime_Millisecond_m3045837949 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::DateTime_Now(JSVCall)
extern "C"  void System_DateTimeGenerated_DateTime_Now_m2640830408 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::DateTime_Ticks(JSVCall)
extern "C"  void System_DateTimeGenerated_DateTime_Ticks_m2283308584 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::DateTime_Today(JSVCall)
extern "C"  void System_DateTimeGenerated_DateTime_Today_m262536637 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::DateTime_UtcNow(JSVCall)
extern "C"  void System_DateTimeGenerated_DateTime_UtcNow_m3781102556 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::DateTime_Year(JSVCall)
extern "C"  void System_DateTimeGenerated_DateTime_Year_m4226062481 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::DateTime_Kind(JSVCall)
extern "C"  void System_DateTimeGenerated_DateTime_Kind_m1125758746 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_Add__TimeSpan(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_Add__TimeSpan_m1111704197 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_AddDays__Double(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_AddDays__Double_m57994774 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_AddHours__Double(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_AddHours__Double_m75109044 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_AddMilliseconds__Double(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_AddMilliseconds__Double_m2198163857 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_AddMinutes__Double(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_AddMinutes__Double_m566818756 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_AddMonths__Int32(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_AddMonths__Int32_m199889359 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_AddSeconds__Double(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_AddSeconds__Double_m1903005028 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_AddTicks__Int64(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_AddTicks__Int64_m1021685605 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_AddYears__Int32(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_AddYears__Int32_m1891282662 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_CompareTo__Object(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_CompareTo__Object_m606564268 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_CompareTo__DateTime(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_CompareTo__DateTime_m3244775432 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_Equals__Object(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_Equals__Object_m2392616371 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_Equals__DateTime(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_Equals__DateTime_m1653928015 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_GetDateTimeFormats__Char__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_GetDateTimeFormats__Char__IFormatProvider_m2145089853 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_GetDateTimeFormats__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_GetDateTimeFormats__IFormatProvider_m1151896083 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_GetDateTimeFormats__Char(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_GetDateTimeFormats__Char_m3707616406 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_GetDateTimeFormats(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_GetDateTimeFormats_m2907327744 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_GetHashCode(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_GetHashCode_m2969337662 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_GetTypeCode(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_GetTypeCode_m439070314 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_IsDaylightSavingTime(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_IsDaylightSavingTime_m2378814784 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_Subtract__DateTime(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_Subtract__DateTime_m457899684 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_Subtract__TimeSpan(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_Subtract__TimeSpan_m1107435968 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_ToBinary(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_ToBinary_m417257617 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_ToFileTime(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_ToFileTime_m2216728889 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_ToFileTimeUtc(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_ToFileTimeUtc_m2323470957 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_ToLocalTime(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_ToLocalTime_m1749985610 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_ToLongDateString(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_ToLongDateString_m805224203 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_ToLongTimeString(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_ToLongTimeString_m1435520426 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_ToOADate(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_ToOADate_m1028463568 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_ToShortDateString(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_ToShortDateString_m873139821 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_ToShortTimeString(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_ToShortTimeString_m1503436044 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_ToString__String__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_ToString__String__IFormatProvider_m1749109601 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_ToString__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_ToString__IFormatProvider_m2724572306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_ToString__String(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_ToString__String_m528043506 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_ToString(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_ToString_m2392899745 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_ToUniversalTime(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_ToUniversalTime_m3903805354 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_Compare__DateTime__DateTime(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_Compare__DateTime__DateTime_m4100750344 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_DaysInMonth__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_DaysInMonth__Int32__Int32_m806073777 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_Equals__DateTime__DateTime(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_Equals__DateTime__DateTime_m519160874 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_FromBinary__Int64(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_FromBinary__Int64_m3696681519 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_FromFileTime__Int64(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_FromFileTime__Int64_m93715527 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_FromFileTimeUtc__Int64(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_FromFileTimeUtc__Int64_m377060497 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_FromOADate__Double(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_FromOADate__Double_m1617902224 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_IsLeapYear__Int32(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_IsLeapYear__Int32_m1146553036 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_op_Addition__DateTime__TimeSpan(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_op_Addition__DateTime__TimeSpan_m303593945 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_op_Equality__DateTime__DateTime(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_op_Equality__DateTime__DateTime_m3009000251 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_op_GreaterThan__DateTime__DateTime(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_op_GreaterThan__DateTime__DateTime_m1829562888 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_op_GreaterThanOrEqual__DateTime__DateTime(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_op_GreaterThanOrEqual__DateTime__DateTime_m73836695 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_op_Inequality__DateTime__DateTime(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_op_Inequality__DateTime__DateTime_m4166157984 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_op_LessThan__DateTime__DateTime(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_op_LessThan__DateTime__DateTime_m3562657051 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_op_LessThanOrEqual__DateTime__DateTime(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_op_LessThanOrEqual__DateTime__DateTime_m2038542884 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_op_Subtraction__DateTime__TimeSpan(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_op_Subtraction__DateTime__TimeSpan_m2334026141 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_op_Subtraction__DateTime__DateTime(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_op_Subtraction__DateTime__DateTime_m1684489857 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_Parse__String__IFormatProvider__DateTimeStyles(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_Parse__String__IFormatProvider__DateTimeStyles_m1224601503 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_Parse__String__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_Parse__String__IFormatProvider_m1078747394 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_Parse__String(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_Parse__String_m1176952369 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_ParseExact__String__String_Array__IFormatProvider__DateTimeStyles(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_ParseExact__String__String_Array__IFormatProvider__DateTimeStyles_m515470387 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_ParseExact__String__String__IFormatProvider__DateTimeStyles(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_ParseExact__String__String__IFormatProvider__DateTimeStyles_m3721510189 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_ParseExact__String__String__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_ParseExact__String__String__IFormatProvider_m2337868432 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_SpecifyKind__DateTime__DateTimeKind(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_SpecifyKind__DateTime__DateTimeKind_m4285887212 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_TryParse__String__IFormatProvider__DateTimeStyles__DateTime(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_TryParse__String__IFormatProvider__DateTimeStyles__DateTime_m1855251277 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_TryParse__String__DateTime(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_TryParse__String__DateTime_m1145661785 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_TryParseExact__String__String_Array__IFormatProvider__DateTimeStyles__DateTime(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_TryParseExact__String__String_Array__IFormatProvider__DateTimeStyles__DateTime_m3981412443 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::DateTime_TryParseExact__String__String__IFormatProvider__DateTimeStyles__DateTime(JSVCall,System.Int32)
extern "C"  bool System_DateTimeGenerated_DateTime_TryParseExact__String__String__IFormatProvider__DateTimeStyles__DateTime_m2542247573 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::__Register()
extern "C"  void System_DateTimeGenerated___Register_m3187434032 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System_DateTimeGenerated::<DateTime_ParseExact__String__String_Array__IFormatProvider__DateTimeStyles>m__BE()
extern "C"  StringU5BU5D_t4054002952* System_DateTimeGenerated_U3CDateTime_ParseExact__String__String_Array__IFormatProvider__DateTimeStylesU3Em__BE_m1529653892 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System_DateTimeGenerated::<DateTime_TryParseExact__String__String_Array__IFormatProvider__DateTimeStyles__DateTime>m__BF()
extern "C"  StringU5BU5D_t4054002952* System_DateTimeGenerated_U3CDateTime_TryParseExact__String__String_Array__IFormatProvider__DateTimeStyles__DateTimeU3Em__BF_m1734988591 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_DateTimeGenerated::ilo_getInt321(System.Int32)
extern "C"  int32_t System_DateTimeGenerated_ilo_getInt321_m3199299006 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void System_DateTimeGenerated_ilo_addJSCSRel2_m2553424052 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_DateTimeGenerated::ilo_getObject3(System.Int32)
extern "C"  int32_t System_DateTimeGenerated_ilo_getObject3_m3857938685 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DateTimeGenerated::ilo_attachFinalizerObject4(System.Int32)
extern "C"  bool System_DateTimeGenerated_ilo_attachFinalizerObject4_m2231345291 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_DateTimeGenerated::ilo_getObject5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_DateTimeGenerated_ilo_getObject5_m535682076 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_DateTimeGenerated::ilo_getEnum6(System.Int32)
extern "C"  int32_t System_DateTimeGenerated_ilo_getEnum6_m3167253374 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_DateTimeGenerated::ilo_setObject7(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_DateTimeGenerated_ilo_setObject7_m3644750201 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::ilo_setEnum8(System.Int32,System.Int32)
extern "C"  void System_DateTimeGenerated_ilo_setEnum8_m1624719921 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::ilo_setInt329(System.Int32,System.Int32)
extern "C"  void System_DateTimeGenerated_ilo_setInt329_m3636325559 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System_DateTimeGenerated::ilo_getDouble10(System.Int32)
extern "C"  double System_DateTimeGenerated_ilo_getDouble10_m1871758102 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::ilo_changeJSObj11(System.Int32,System.Object)
extern "C"  void System_DateTimeGenerated_ilo_changeJSObj11_m2345948255 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_DateTimeGenerated::ilo_getWhatever12(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_DateTimeGenerated_ilo_getWhatever12_m699813261 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::ilo_setStringS13(System.Int32,System.String)
extern "C"  void System_DateTimeGenerated_ilo_setStringS13_m1291615951 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::ilo_moveSaveID2Arr14(System.Int32)
extern "C"  void System_DateTimeGenerated_ilo_moveSaveID2Arr14_m3444527564 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::ilo_setArrayS15(System.Int32,System.Int32,System.Boolean)
extern "C"  void System_DateTimeGenerated_ilo_setArrayS15_m436869817 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::ilo_setInt6416(System.Int32,System.Int64)
extern "C"  void System_DateTimeGenerated_ilo_setInt6416_m3176540567 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int64_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System_DateTimeGenerated::ilo_getStringS17(System.Int32)
extern "C"  String_t* System_DateTimeGenerated_ilo_getStringS17_m3108420742 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::ilo_setBooleanS18(System.Int32,System.Boolean)
extern "C"  void System_DateTimeGenerated_ilo_setBooleanS18_m2447793430 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DateTimeGenerated::ilo_setArgIndex19(System.Int32)
extern "C"  void System_DateTimeGenerated_ilo_setArgIndex19_m3892849337 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_DateTimeGenerated::ilo_incArgIndex20()
extern "C"  int32_t System_DateTimeGenerated_ilo_incArgIndex20_m3731751468 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_DateTimeGenerated::ilo_getArrayLength21(System.Int32)
extern "C"  int32_t System_DateTimeGenerated_ilo_getArrayLength21_m2846268039 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
