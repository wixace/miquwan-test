﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr_DGetV_1_gen4048467712MethodDeclarations.h"

// System.Void JSDataExchangeMgr/DGetV`1<UIPanel/OnGeometryUpdated>::.ctor(System.Object,System.IntPtr)
#define DGetV_1__ctor_m715973966(__this, ___object0, ___method1, method) ((  void (*) (DGetV_1_t4163425016 *, Il2CppObject *, IntPtr_t, const MethodInfo*))DGetV_1__ctor_m3356914363_gshared)(__this, ___object0, ___method1, method)
// T JSDataExchangeMgr/DGetV`1<UIPanel/OnGeometryUpdated>::Invoke()
#define DGetV_1_Invoke_m2963611239(__this, method) ((  OnGeometryUpdated_t4285773675 * (*) (DGetV_1_t4163425016 *, const MethodInfo*))DGetV_1_Invoke_m1509146772_gshared)(__this, method)
// System.IAsyncResult JSDataExchangeMgr/DGetV`1<UIPanel/OnGeometryUpdated>::BeginInvoke(System.AsyncCallback,System.Object)
#define DGetV_1_BeginInvoke_m2967837403(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (DGetV_1_t4163425016 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))DGetV_1_BeginInvoke_m2681184398_gshared)(__this, ___callback0, ___object1, method)
// T JSDataExchangeMgr/DGetV`1<UIPanel/OnGeometryUpdated>::EndInvoke(System.IAsyncResult)
#define DGetV_1_EndInvoke_m1633889373(__this, ___result0, method) ((  OnGeometryUpdated_t4285773675 * (*) (DGetV_1_t4163425016 *, Il2CppObject *, const MethodInfo*))DGetV_1_EndInvoke_m1776565962_gshared)(__this, ___result0, method)
