﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssetShaderList
struct AssetShaderList_t361542931;

#include "codegen/il2cpp-codegen.h"

// System.Void AssetShaderList::.ctor()
extern "C"  void AssetShaderList__ctor_m1217009592 (AssetShaderList_t361542931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
