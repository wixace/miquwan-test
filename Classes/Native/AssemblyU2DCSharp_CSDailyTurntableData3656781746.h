﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSDailyTurntableData
struct  CSDailyTurntableData_t3656781746  : public Il2CppObject
{
public:
	// System.Int32 CSDailyTurntableData::npcType
	int32_t ___npcType_1;
	// System.Int32 CSDailyTurntableData::NumIndex
	int32_t ___NumIndex_2;
	// System.Int32 CSDailyTurntableData::SurpCount
	int32_t ___SurpCount_3;

public:
	inline static int32_t get_offset_of_npcType_1() { return static_cast<int32_t>(offsetof(CSDailyTurntableData_t3656781746, ___npcType_1)); }
	inline int32_t get_npcType_1() const { return ___npcType_1; }
	inline int32_t* get_address_of_npcType_1() { return &___npcType_1; }
	inline void set_npcType_1(int32_t value)
	{
		___npcType_1 = value;
	}

	inline static int32_t get_offset_of_NumIndex_2() { return static_cast<int32_t>(offsetof(CSDailyTurntableData_t3656781746, ___NumIndex_2)); }
	inline int32_t get_NumIndex_2() const { return ___NumIndex_2; }
	inline int32_t* get_address_of_NumIndex_2() { return &___NumIndex_2; }
	inline void set_NumIndex_2(int32_t value)
	{
		___NumIndex_2 = value;
	}

	inline static int32_t get_offset_of_SurpCount_3() { return static_cast<int32_t>(offsetof(CSDailyTurntableData_t3656781746, ___SurpCount_3)); }
	inline int32_t get_SurpCount_3() const { return ___SurpCount_3; }
	inline int32_t* get_address_of_SurpCount_3() { return &___SurpCount_3; }
	inline void set_SurpCount_3(int32_t value)
	{
		___SurpCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
