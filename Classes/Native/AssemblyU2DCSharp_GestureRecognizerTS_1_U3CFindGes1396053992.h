﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FingerClusterManager/Cluster
struct Cluster_t3870231303;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GestureRecognizerTS`1/<FindGestureByCluster>c__AnonStorey127<System.Object>
struct  U3CFindGestureByClusterU3Ec__AnonStorey127_t1396053992  : public Il2CppObject
{
public:
	// FingerClusterManager/Cluster GestureRecognizerTS`1/<FindGestureByCluster>c__AnonStorey127::cluster
	Cluster_t3870231303 * ___cluster_0;

public:
	inline static int32_t get_offset_of_cluster_0() { return static_cast<int32_t>(offsetof(U3CFindGestureByClusterU3Ec__AnonStorey127_t1396053992, ___cluster_0)); }
	inline Cluster_t3870231303 * get_cluster_0() const { return ___cluster_0; }
	inline Cluster_t3870231303 ** get_address_of_cluster_0() { return &___cluster_0; }
	inline void set_cluster_0(Cluster_t3870231303 * value)
	{
		___cluster_0 = value;
		Il2CppCodeGenWriteBarrier(&___cluster_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
