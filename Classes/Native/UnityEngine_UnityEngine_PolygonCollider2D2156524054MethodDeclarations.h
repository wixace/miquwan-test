﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.PolygonCollider2D
struct PolygonCollider2D_t2156524054;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_PolygonCollider2D2156524054.h"

// System.Void UnityEngine.PolygonCollider2D::.ctor()
extern "C"  void PolygonCollider2D__ctor_m2630979769 (PolygonCollider2D_t2156524054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] UnityEngine.PolygonCollider2D::get_points()
extern "C"  Vector2U5BU5D_t4024180168* PolygonCollider2D_get_points_m3975671390 (PolygonCollider2D_t2156524054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PolygonCollider2D::set_points(UnityEngine.Vector2[])
extern "C"  void PolygonCollider2D_set_points_m3965263885 (PolygonCollider2D_t2156524054 * __this, Vector2U5BU5D_t4024180168* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] UnityEngine.PolygonCollider2D::GetPath(System.Int32)
extern "C"  Vector2U5BU5D_t4024180168* PolygonCollider2D_GetPath_m2325981884 (PolygonCollider2D_t2156524054 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PolygonCollider2D::SetPath(System.Int32,UnityEngine.Vector2[])
extern "C"  void PolygonCollider2D_SetPath_m1487888609 (PolygonCollider2D_t2156524054 * __this, int32_t ___index0, Vector2U5BU5D_t4024180168* ___points1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.PolygonCollider2D::get_pathCount()
extern "C"  int32_t PolygonCollider2D_get_pathCount_m2384916088 (PolygonCollider2D_t2156524054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PolygonCollider2D::set_pathCount(System.Int32)
extern "C"  void PolygonCollider2D_set_pathCount_m874408021 (PolygonCollider2D_t2156524054 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.PolygonCollider2D::GetTotalPointCount()
extern "C"  int32_t PolygonCollider2D_GetTotalPointCount_m2675784120 (PolygonCollider2D_t2156524054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PolygonCollider2D::CreatePrimitive(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void PolygonCollider2D_CreatePrimitive_m1386491675 (PolygonCollider2D_t2156524054 * __this, int32_t ___sides0, Vector2_t4282066565  ___scale1, Vector2_t4282066565  ___offset2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PolygonCollider2D::CreatePrimitive(System.Int32,UnityEngine.Vector2)
extern "C"  void PolygonCollider2D_CreatePrimitive_m1905845031 (PolygonCollider2D_t2156524054 * __this, int32_t ___sides0, Vector2_t4282066565  ___scale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PolygonCollider2D::CreatePrimitive(System.Int32)
extern "C"  void PolygonCollider2D_CreatePrimitive_m3765376307 (PolygonCollider2D_t2156524054 * __this, int32_t ___sides0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PolygonCollider2D::INTERNAL_CALL_CreatePrimitive(UnityEngine.PolygonCollider2D,System.Int32,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern "C"  void PolygonCollider2D_INTERNAL_CALL_CreatePrimitive_m3672603377 (Il2CppObject * __this /* static, unused */, PolygonCollider2D_t2156524054 * ___self0, int32_t ___sides1, Vector2_t4282066565 * ___scale2, Vector2_t4282066565 * ___offset3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
