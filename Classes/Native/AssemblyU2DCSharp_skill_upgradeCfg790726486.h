﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// skill_upgradeCfg
struct  skill_upgradeCfg_t790726486  : public CsCfgBase_t69924517
{
public:
	// System.Int32 skill_upgradeCfg::id
	int32_t ___id_0;
	// System.Int32 skill_upgradeCfg::skill_id
	int32_t ___skill_id_1;
	// System.Int32 skill_upgradeCfg::skill_level
	int32_t ___skill_level_2;
	// System.Int32 skill_upgradeCfg::damage_add_per
	int32_t ___damage_add_per_3;
	// System.Int32 skill_upgradeCfg::damage_add_value
	int32_t ___damage_add_value_4;
	// System.Int32 skill_upgradeCfg::damage_add_text
	int32_t ___damage_add_text_5;
	// System.Int32 skill_upgradeCfg::damage_add_text1
	int32_t ___damage_add_text1_6;
	// System.Boolean skill_upgradeCfg::up_ratio
	bool ___up_ratio_7;
	// System.Boolean skill_upgradeCfg::des_ratio
	bool ___des_ratio_8;
	// System.Int32 skill_upgradeCfg::ratio_add
	int32_t ___ratio_add_9;
	// System.Boolean skill_upgradeCfg::up_cd
	bool ___up_cd_10;
	// System.Boolean skill_upgradeCfg::des_cd
	bool ___des_cd_11;
	// System.Single skill_upgradeCfg::cd
	float ___cd_12;
	// System.String skill_upgradeCfg::att_add1
	String_t* ___att_add1_13;
	// System.Int32 skill_upgradeCfg::att_add_num1
	int32_t ___att_add_num1_14;
	// System.Int32 skill_upgradeCfg::att_add_per1
	int32_t ___att_add_per1_15;
	// System.String skill_upgradeCfg::att_add2
	String_t* ___att_add2_16;
	// System.Int32 skill_upgradeCfg::att_add_num2
	int32_t ___att_add_num2_17;
	// System.Int32 skill_upgradeCfg::att_add_per2
	int32_t ___att_add_per2_18;
	// System.String skill_upgradeCfg::att_add3
	String_t* ___att_add3_19;
	// System.Int32 skill_upgradeCfg::att_add_num3
	int32_t ___att_add_num3_20;
	// System.Int32 skill_upgradeCfg::att_add_per3
	int32_t ___att_add_per3_21;
	// System.String skill_upgradeCfg::att_add4
	String_t* ___att_add4_22;
	// System.Int32 skill_upgradeCfg::att_add_num4
	int32_t ___att_add_num4_23;
	// System.Int32 skill_upgradeCfg::att_add_per4
	int32_t ___att_add_per4_24;
	// System.Int32 skill_upgradeCfg::skillPoint
	int32_t ___skillPoint_25;
	// System.Int32 skill_upgradeCfg::copper
	int32_t ___copper_26;
	// System.String skill_upgradeCfg::buff_key
	String_t* ___buff_key_27;
	// System.Int32 skill_upgradeCfg::buff_value
	int32_t ___buff_value_28;
	// System.Int32 skill_upgradeCfg::buff_per
	int32_t ___buff_per_29;
	// System.String skill_upgradeCfg::buff_key1
	String_t* ___buff_key1_30;
	// System.Int32 skill_upgradeCfg::buff_value1
	int32_t ___buff_value1_31;
	// System.Int32 skill_upgradeCfg::buff_per1
	int32_t ___buff_per1_32;
	// System.String skill_upgradeCfg::buff_key2
	String_t* ___buff_key2_33;
	// System.Int32 skill_upgradeCfg::buff_value2
	int32_t ___buff_value2_34;
	// System.Int32 skill_upgradeCfg::buff_per2
	int32_t ___buff_per2_35;
	// System.String skill_upgradeCfg::buffid
	String_t* ___buffid_36;
	// System.String skill_upgradeCfg::buffprob
	String_t* ___buffprob_37;
	// System.String skill_upgradeCfg::buffevent
	String_t* ___buffevent_38;
	// System.String skill_upgradeCfg::bufftarget
	String_t* ___bufftarget_39;
	// System.String skill_upgradeCfg::buffbreak
	String_t* ___buffbreak_40;
	// System.Int32 skill_upgradeCfg::callNpcId
	int32_t ___callNpcId_41;
	// System.String skill_upgradeCfg::callAttributeId1
	String_t* ___callAttributeId1_42;
	// System.Int32 skill_upgradeCfg::callAttributeValue1
	int32_t ___callAttributeValue1_43;
	// System.String skill_upgradeCfg::callAttributeId2
	String_t* ___callAttributeId2_44;
	// System.Int32 skill_upgradeCfg::callAttributeValue2
	int32_t ___callAttributeValue2_45;
	// System.Single skill_upgradeCfg::sputter_radius
	float ___sputter_radius_46;
	// System.Int32 skill_upgradeCfg::sputter_num
	int32_t ___sputter_num_47;
	// System.Single skill_upgradeCfg::sputter_hurt
	float ___sputter_hurt_48;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_skill_id_1() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___skill_id_1)); }
	inline int32_t get_skill_id_1() const { return ___skill_id_1; }
	inline int32_t* get_address_of_skill_id_1() { return &___skill_id_1; }
	inline void set_skill_id_1(int32_t value)
	{
		___skill_id_1 = value;
	}

	inline static int32_t get_offset_of_skill_level_2() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___skill_level_2)); }
	inline int32_t get_skill_level_2() const { return ___skill_level_2; }
	inline int32_t* get_address_of_skill_level_2() { return &___skill_level_2; }
	inline void set_skill_level_2(int32_t value)
	{
		___skill_level_2 = value;
	}

	inline static int32_t get_offset_of_damage_add_per_3() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___damage_add_per_3)); }
	inline int32_t get_damage_add_per_3() const { return ___damage_add_per_3; }
	inline int32_t* get_address_of_damage_add_per_3() { return &___damage_add_per_3; }
	inline void set_damage_add_per_3(int32_t value)
	{
		___damage_add_per_3 = value;
	}

	inline static int32_t get_offset_of_damage_add_value_4() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___damage_add_value_4)); }
	inline int32_t get_damage_add_value_4() const { return ___damage_add_value_4; }
	inline int32_t* get_address_of_damage_add_value_4() { return &___damage_add_value_4; }
	inline void set_damage_add_value_4(int32_t value)
	{
		___damage_add_value_4 = value;
	}

	inline static int32_t get_offset_of_damage_add_text_5() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___damage_add_text_5)); }
	inline int32_t get_damage_add_text_5() const { return ___damage_add_text_5; }
	inline int32_t* get_address_of_damage_add_text_5() { return &___damage_add_text_5; }
	inline void set_damage_add_text_5(int32_t value)
	{
		___damage_add_text_5 = value;
	}

	inline static int32_t get_offset_of_damage_add_text1_6() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___damage_add_text1_6)); }
	inline int32_t get_damage_add_text1_6() const { return ___damage_add_text1_6; }
	inline int32_t* get_address_of_damage_add_text1_6() { return &___damage_add_text1_6; }
	inline void set_damage_add_text1_6(int32_t value)
	{
		___damage_add_text1_6 = value;
	}

	inline static int32_t get_offset_of_up_ratio_7() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___up_ratio_7)); }
	inline bool get_up_ratio_7() const { return ___up_ratio_7; }
	inline bool* get_address_of_up_ratio_7() { return &___up_ratio_7; }
	inline void set_up_ratio_7(bool value)
	{
		___up_ratio_7 = value;
	}

	inline static int32_t get_offset_of_des_ratio_8() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___des_ratio_8)); }
	inline bool get_des_ratio_8() const { return ___des_ratio_8; }
	inline bool* get_address_of_des_ratio_8() { return &___des_ratio_8; }
	inline void set_des_ratio_8(bool value)
	{
		___des_ratio_8 = value;
	}

	inline static int32_t get_offset_of_ratio_add_9() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___ratio_add_9)); }
	inline int32_t get_ratio_add_9() const { return ___ratio_add_9; }
	inline int32_t* get_address_of_ratio_add_9() { return &___ratio_add_9; }
	inline void set_ratio_add_9(int32_t value)
	{
		___ratio_add_9 = value;
	}

	inline static int32_t get_offset_of_up_cd_10() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___up_cd_10)); }
	inline bool get_up_cd_10() const { return ___up_cd_10; }
	inline bool* get_address_of_up_cd_10() { return &___up_cd_10; }
	inline void set_up_cd_10(bool value)
	{
		___up_cd_10 = value;
	}

	inline static int32_t get_offset_of_des_cd_11() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___des_cd_11)); }
	inline bool get_des_cd_11() const { return ___des_cd_11; }
	inline bool* get_address_of_des_cd_11() { return &___des_cd_11; }
	inline void set_des_cd_11(bool value)
	{
		___des_cd_11 = value;
	}

	inline static int32_t get_offset_of_cd_12() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___cd_12)); }
	inline float get_cd_12() const { return ___cd_12; }
	inline float* get_address_of_cd_12() { return &___cd_12; }
	inline void set_cd_12(float value)
	{
		___cd_12 = value;
	}

	inline static int32_t get_offset_of_att_add1_13() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___att_add1_13)); }
	inline String_t* get_att_add1_13() const { return ___att_add1_13; }
	inline String_t** get_address_of_att_add1_13() { return &___att_add1_13; }
	inline void set_att_add1_13(String_t* value)
	{
		___att_add1_13 = value;
		Il2CppCodeGenWriteBarrier(&___att_add1_13, value);
	}

	inline static int32_t get_offset_of_att_add_num1_14() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___att_add_num1_14)); }
	inline int32_t get_att_add_num1_14() const { return ___att_add_num1_14; }
	inline int32_t* get_address_of_att_add_num1_14() { return &___att_add_num1_14; }
	inline void set_att_add_num1_14(int32_t value)
	{
		___att_add_num1_14 = value;
	}

	inline static int32_t get_offset_of_att_add_per1_15() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___att_add_per1_15)); }
	inline int32_t get_att_add_per1_15() const { return ___att_add_per1_15; }
	inline int32_t* get_address_of_att_add_per1_15() { return &___att_add_per1_15; }
	inline void set_att_add_per1_15(int32_t value)
	{
		___att_add_per1_15 = value;
	}

	inline static int32_t get_offset_of_att_add2_16() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___att_add2_16)); }
	inline String_t* get_att_add2_16() const { return ___att_add2_16; }
	inline String_t** get_address_of_att_add2_16() { return &___att_add2_16; }
	inline void set_att_add2_16(String_t* value)
	{
		___att_add2_16 = value;
		Il2CppCodeGenWriteBarrier(&___att_add2_16, value);
	}

	inline static int32_t get_offset_of_att_add_num2_17() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___att_add_num2_17)); }
	inline int32_t get_att_add_num2_17() const { return ___att_add_num2_17; }
	inline int32_t* get_address_of_att_add_num2_17() { return &___att_add_num2_17; }
	inline void set_att_add_num2_17(int32_t value)
	{
		___att_add_num2_17 = value;
	}

	inline static int32_t get_offset_of_att_add_per2_18() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___att_add_per2_18)); }
	inline int32_t get_att_add_per2_18() const { return ___att_add_per2_18; }
	inline int32_t* get_address_of_att_add_per2_18() { return &___att_add_per2_18; }
	inline void set_att_add_per2_18(int32_t value)
	{
		___att_add_per2_18 = value;
	}

	inline static int32_t get_offset_of_att_add3_19() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___att_add3_19)); }
	inline String_t* get_att_add3_19() const { return ___att_add3_19; }
	inline String_t** get_address_of_att_add3_19() { return &___att_add3_19; }
	inline void set_att_add3_19(String_t* value)
	{
		___att_add3_19 = value;
		Il2CppCodeGenWriteBarrier(&___att_add3_19, value);
	}

	inline static int32_t get_offset_of_att_add_num3_20() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___att_add_num3_20)); }
	inline int32_t get_att_add_num3_20() const { return ___att_add_num3_20; }
	inline int32_t* get_address_of_att_add_num3_20() { return &___att_add_num3_20; }
	inline void set_att_add_num3_20(int32_t value)
	{
		___att_add_num3_20 = value;
	}

	inline static int32_t get_offset_of_att_add_per3_21() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___att_add_per3_21)); }
	inline int32_t get_att_add_per3_21() const { return ___att_add_per3_21; }
	inline int32_t* get_address_of_att_add_per3_21() { return &___att_add_per3_21; }
	inline void set_att_add_per3_21(int32_t value)
	{
		___att_add_per3_21 = value;
	}

	inline static int32_t get_offset_of_att_add4_22() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___att_add4_22)); }
	inline String_t* get_att_add4_22() const { return ___att_add4_22; }
	inline String_t** get_address_of_att_add4_22() { return &___att_add4_22; }
	inline void set_att_add4_22(String_t* value)
	{
		___att_add4_22 = value;
		Il2CppCodeGenWriteBarrier(&___att_add4_22, value);
	}

	inline static int32_t get_offset_of_att_add_num4_23() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___att_add_num4_23)); }
	inline int32_t get_att_add_num4_23() const { return ___att_add_num4_23; }
	inline int32_t* get_address_of_att_add_num4_23() { return &___att_add_num4_23; }
	inline void set_att_add_num4_23(int32_t value)
	{
		___att_add_num4_23 = value;
	}

	inline static int32_t get_offset_of_att_add_per4_24() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___att_add_per4_24)); }
	inline int32_t get_att_add_per4_24() const { return ___att_add_per4_24; }
	inline int32_t* get_address_of_att_add_per4_24() { return &___att_add_per4_24; }
	inline void set_att_add_per4_24(int32_t value)
	{
		___att_add_per4_24 = value;
	}

	inline static int32_t get_offset_of_skillPoint_25() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___skillPoint_25)); }
	inline int32_t get_skillPoint_25() const { return ___skillPoint_25; }
	inline int32_t* get_address_of_skillPoint_25() { return &___skillPoint_25; }
	inline void set_skillPoint_25(int32_t value)
	{
		___skillPoint_25 = value;
	}

	inline static int32_t get_offset_of_copper_26() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___copper_26)); }
	inline int32_t get_copper_26() const { return ___copper_26; }
	inline int32_t* get_address_of_copper_26() { return &___copper_26; }
	inline void set_copper_26(int32_t value)
	{
		___copper_26 = value;
	}

	inline static int32_t get_offset_of_buff_key_27() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___buff_key_27)); }
	inline String_t* get_buff_key_27() const { return ___buff_key_27; }
	inline String_t** get_address_of_buff_key_27() { return &___buff_key_27; }
	inline void set_buff_key_27(String_t* value)
	{
		___buff_key_27 = value;
		Il2CppCodeGenWriteBarrier(&___buff_key_27, value);
	}

	inline static int32_t get_offset_of_buff_value_28() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___buff_value_28)); }
	inline int32_t get_buff_value_28() const { return ___buff_value_28; }
	inline int32_t* get_address_of_buff_value_28() { return &___buff_value_28; }
	inline void set_buff_value_28(int32_t value)
	{
		___buff_value_28 = value;
	}

	inline static int32_t get_offset_of_buff_per_29() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___buff_per_29)); }
	inline int32_t get_buff_per_29() const { return ___buff_per_29; }
	inline int32_t* get_address_of_buff_per_29() { return &___buff_per_29; }
	inline void set_buff_per_29(int32_t value)
	{
		___buff_per_29 = value;
	}

	inline static int32_t get_offset_of_buff_key1_30() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___buff_key1_30)); }
	inline String_t* get_buff_key1_30() const { return ___buff_key1_30; }
	inline String_t** get_address_of_buff_key1_30() { return &___buff_key1_30; }
	inline void set_buff_key1_30(String_t* value)
	{
		___buff_key1_30 = value;
		Il2CppCodeGenWriteBarrier(&___buff_key1_30, value);
	}

	inline static int32_t get_offset_of_buff_value1_31() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___buff_value1_31)); }
	inline int32_t get_buff_value1_31() const { return ___buff_value1_31; }
	inline int32_t* get_address_of_buff_value1_31() { return &___buff_value1_31; }
	inline void set_buff_value1_31(int32_t value)
	{
		___buff_value1_31 = value;
	}

	inline static int32_t get_offset_of_buff_per1_32() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___buff_per1_32)); }
	inline int32_t get_buff_per1_32() const { return ___buff_per1_32; }
	inline int32_t* get_address_of_buff_per1_32() { return &___buff_per1_32; }
	inline void set_buff_per1_32(int32_t value)
	{
		___buff_per1_32 = value;
	}

	inline static int32_t get_offset_of_buff_key2_33() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___buff_key2_33)); }
	inline String_t* get_buff_key2_33() const { return ___buff_key2_33; }
	inline String_t** get_address_of_buff_key2_33() { return &___buff_key2_33; }
	inline void set_buff_key2_33(String_t* value)
	{
		___buff_key2_33 = value;
		Il2CppCodeGenWriteBarrier(&___buff_key2_33, value);
	}

	inline static int32_t get_offset_of_buff_value2_34() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___buff_value2_34)); }
	inline int32_t get_buff_value2_34() const { return ___buff_value2_34; }
	inline int32_t* get_address_of_buff_value2_34() { return &___buff_value2_34; }
	inline void set_buff_value2_34(int32_t value)
	{
		___buff_value2_34 = value;
	}

	inline static int32_t get_offset_of_buff_per2_35() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___buff_per2_35)); }
	inline int32_t get_buff_per2_35() const { return ___buff_per2_35; }
	inline int32_t* get_address_of_buff_per2_35() { return &___buff_per2_35; }
	inline void set_buff_per2_35(int32_t value)
	{
		___buff_per2_35 = value;
	}

	inline static int32_t get_offset_of_buffid_36() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___buffid_36)); }
	inline String_t* get_buffid_36() const { return ___buffid_36; }
	inline String_t** get_address_of_buffid_36() { return &___buffid_36; }
	inline void set_buffid_36(String_t* value)
	{
		___buffid_36 = value;
		Il2CppCodeGenWriteBarrier(&___buffid_36, value);
	}

	inline static int32_t get_offset_of_buffprob_37() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___buffprob_37)); }
	inline String_t* get_buffprob_37() const { return ___buffprob_37; }
	inline String_t** get_address_of_buffprob_37() { return &___buffprob_37; }
	inline void set_buffprob_37(String_t* value)
	{
		___buffprob_37 = value;
		Il2CppCodeGenWriteBarrier(&___buffprob_37, value);
	}

	inline static int32_t get_offset_of_buffevent_38() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___buffevent_38)); }
	inline String_t* get_buffevent_38() const { return ___buffevent_38; }
	inline String_t** get_address_of_buffevent_38() { return &___buffevent_38; }
	inline void set_buffevent_38(String_t* value)
	{
		___buffevent_38 = value;
		Il2CppCodeGenWriteBarrier(&___buffevent_38, value);
	}

	inline static int32_t get_offset_of_bufftarget_39() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___bufftarget_39)); }
	inline String_t* get_bufftarget_39() const { return ___bufftarget_39; }
	inline String_t** get_address_of_bufftarget_39() { return &___bufftarget_39; }
	inline void set_bufftarget_39(String_t* value)
	{
		___bufftarget_39 = value;
		Il2CppCodeGenWriteBarrier(&___bufftarget_39, value);
	}

	inline static int32_t get_offset_of_buffbreak_40() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___buffbreak_40)); }
	inline String_t* get_buffbreak_40() const { return ___buffbreak_40; }
	inline String_t** get_address_of_buffbreak_40() { return &___buffbreak_40; }
	inline void set_buffbreak_40(String_t* value)
	{
		___buffbreak_40 = value;
		Il2CppCodeGenWriteBarrier(&___buffbreak_40, value);
	}

	inline static int32_t get_offset_of_callNpcId_41() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___callNpcId_41)); }
	inline int32_t get_callNpcId_41() const { return ___callNpcId_41; }
	inline int32_t* get_address_of_callNpcId_41() { return &___callNpcId_41; }
	inline void set_callNpcId_41(int32_t value)
	{
		___callNpcId_41 = value;
	}

	inline static int32_t get_offset_of_callAttributeId1_42() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___callAttributeId1_42)); }
	inline String_t* get_callAttributeId1_42() const { return ___callAttributeId1_42; }
	inline String_t** get_address_of_callAttributeId1_42() { return &___callAttributeId1_42; }
	inline void set_callAttributeId1_42(String_t* value)
	{
		___callAttributeId1_42 = value;
		Il2CppCodeGenWriteBarrier(&___callAttributeId1_42, value);
	}

	inline static int32_t get_offset_of_callAttributeValue1_43() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___callAttributeValue1_43)); }
	inline int32_t get_callAttributeValue1_43() const { return ___callAttributeValue1_43; }
	inline int32_t* get_address_of_callAttributeValue1_43() { return &___callAttributeValue1_43; }
	inline void set_callAttributeValue1_43(int32_t value)
	{
		___callAttributeValue1_43 = value;
	}

	inline static int32_t get_offset_of_callAttributeId2_44() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___callAttributeId2_44)); }
	inline String_t* get_callAttributeId2_44() const { return ___callAttributeId2_44; }
	inline String_t** get_address_of_callAttributeId2_44() { return &___callAttributeId2_44; }
	inline void set_callAttributeId2_44(String_t* value)
	{
		___callAttributeId2_44 = value;
		Il2CppCodeGenWriteBarrier(&___callAttributeId2_44, value);
	}

	inline static int32_t get_offset_of_callAttributeValue2_45() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___callAttributeValue2_45)); }
	inline int32_t get_callAttributeValue2_45() const { return ___callAttributeValue2_45; }
	inline int32_t* get_address_of_callAttributeValue2_45() { return &___callAttributeValue2_45; }
	inline void set_callAttributeValue2_45(int32_t value)
	{
		___callAttributeValue2_45 = value;
	}

	inline static int32_t get_offset_of_sputter_radius_46() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___sputter_radius_46)); }
	inline float get_sputter_radius_46() const { return ___sputter_radius_46; }
	inline float* get_address_of_sputter_radius_46() { return &___sputter_radius_46; }
	inline void set_sputter_radius_46(float value)
	{
		___sputter_radius_46 = value;
	}

	inline static int32_t get_offset_of_sputter_num_47() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___sputter_num_47)); }
	inline int32_t get_sputter_num_47() const { return ___sputter_num_47; }
	inline int32_t* get_address_of_sputter_num_47() { return &___sputter_num_47; }
	inline void set_sputter_num_47(int32_t value)
	{
		___sputter_num_47 = value;
	}

	inline static int32_t get_offset_of_sputter_hurt_48() { return static_cast<int32_t>(offsetof(skill_upgradeCfg_t790726486, ___sputter_hurt_48)); }
	inline float get_sputter_hurt_48() const { return ___sputter_hurt_48; }
	inline float* get_address_of_sputter_hurt_48() { return &___sputter_hurt_48; }
	inline void set_sputter_hurt_48(float value)
	{
		___sputter_hurt_48 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
