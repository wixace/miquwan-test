﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UITweenerGenerated
struct UITweenerGenerated_t2713607883;
// JSVCall
struct JSVCall_t3708497963;
// EventDelegate/Callback
struct Callback_t1094463061;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Delegate
struct Delegate_t3310234105;
// UITweener
struct UITweener_t105489188;
// EventDelegate
struct EventDelegate_t4004424223;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "AssemblyU2DCSharp_UITweener105489188.h"
#include "AssemblyU2DCSharp_EventDelegate4004424223.h"

// System.Void UITweenerGenerated::.ctor()
extern "C"  void UITweenerGenerated__ctor_m4193130032 (UITweenerGenerated_t2713607883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::.cctor()
extern "C"  void UITweenerGenerated__cctor_m655915901 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::UITweener_current(JSVCall)
extern "C"  void UITweenerGenerated_UITweener_current_m2829065307 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::UITweener_method(JSVCall)
extern "C"  void UITweenerGenerated_UITweener_method_m4032690135 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::UITweener_style(JSVCall)
extern "C"  void UITweenerGenerated_UITweener_style_m1500556963 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::UITweener_animationCurve(JSVCall)
extern "C"  void UITweenerGenerated_UITweener_animationCurve_m1291793965 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::UITweener_ignoreTimeScale(JSVCall)
extern "C"  void UITweenerGenerated_UITweener_ignoreTimeScale_m417520457 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::UITweener_delay(JSVCall)
extern "C"  void UITweenerGenerated_UITweener_delay_m1873297169 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::UITweener_duration(JSVCall)
extern "C"  void UITweenerGenerated_UITweener_duration_m2603735044 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::UITweener_steeperCurves(JSVCall)
extern "C"  void UITweenerGenerated_UITweener_steeperCurves_m4182662132 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::UITweener_tweenGroup(JSVCall)
extern "C"  void UITweenerGenerated_UITweener_tweenGroup_m2670388804 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::UITweener_onFinished(JSVCall)
extern "C"  void UITweenerGenerated_UITweener_onFinished_m1774939719 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::UITweener_eventReceiver(JSVCall)
extern "C"  void UITweenerGenerated_UITweener_eventReceiver_m1511870123 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::UITweener_callWhenFinished(JSVCall)
extern "C"  void UITweenerGenerated_UITweener_callWhenFinished_m3451312398 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::UITweener_amountPerDelta(JSVCall)
extern "C"  void UITweenerGenerated_UITweener_amountPerDelta_m1216187877 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::UITweener_tweenFactor(JSVCall)
extern "C"  void UITweenerGenerated_UITweener_tweenFactor_m3878189146 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::UITweener_direction(JSVCall)
extern "C"  void UITweenerGenerated_UITweener_direction_m88302997 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Callback UITweenerGenerated::UITweener_AddOnFinished_GetDelegate_member0_arg0(CSRepresentedObject)
extern "C"  Callback_t1094463061 * UITweenerGenerated_UITweener_AddOnFinished_GetDelegate_member0_arg0_m1676292081 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITweenerGenerated::UITweener_AddOnFinished__Callback(JSVCall,System.Int32)
extern "C"  bool UITweenerGenerated_UITweener_AddOnFinished__Callback_m2295242182 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITweenerGenerated::UITweener_AddOnFinished__EventDelegate(JSVCall,System.Int32)
extern "C"  bool UITweenerGenerated_UITweener_AddOnFinished__EventDelegate_m1779831264 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITweenerGenerated::UITweener_Play__Boolean(JSVCall,System.Int32)
extern "C"  bool UITweenerGenerated_UITweener_Play__Boolean_m1393653315 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITweenerGenerated::UITweener_PlayForward(JSVCall,System.Int32)
extern "C"  bool UITweenerGenerated_UITweener_PlayForward_m1794790272 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITweenerGenerated::UITweener_PlayReverse(JSVCall,System.Int32)
extern "C"  bool UITweenerGenerated_UITweener_PlayReverse_m3287511005 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITweenerGenerated::UITweener_RemoveOnFinished__EventDelegate(JSVCall,System.Int32)
extern "C"  bool UITweenerGenerated_UITweener_RemoveOnFinished__EventDelegate_m1633164633 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITweenerGenerated::UITweener_ResetToBeginning(JSVCall,System.Int32)
extern "C"  bool UITweenerGenerated_UITweener_ResetToBeginning_m555105062 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITweenerGenerated::UITweener_Sample__Single__Boolean(JSVCall,System.Int32)
extern "C"  bool UITweenerGenerated_UITweener_Sample__Single__Boolean_m1793741893 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITweenerGenerated::UITweener_SetEndToCurrentValue(JSVCall,System.Int32)
extern "C"  bool UITweenerGenerated_UITweener_SetEndToCurrentValue_m241798303 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITweenerGenerated::UITweener_SetOnFinished__EventDelegate(JSVCall,System.Int32)
extern "C"  bool UITweenerGenerated_UITweener_SetOnFinished__EventDelegate_m70744063 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Callback UITweenerGenerated::UITweener_SetOnFinished_GetDelegate_member10_arg0(CSRepresentedObject)
extern "C"  Callback_t1094463061 * UITweenerGenerated_UITweener_SetOnFinished_GetDelegate_member10_arg0_m442756257 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITweenerGenerated::UITweener_SetOnFinished__Callback(JSVCall,System.Int32)
extern "C"  bool UITweenerGenerated_UITweener_SetOnFinished__Callback_m2132779079 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITweenerGenerated::UITweener_SetStartToCurrentValue(JSVCall,System.Int32)
extern "C"  bool UITweenerGenerated_UITweener_SetStartToCurrentValue_m3465879718 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITweenerGenerated::UITweener_Toggle(JSVCall,System.Int32)
extern "C"  bool UITweenerGenerated_UITweener_Toggle_m2147980903 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITweenerGenerated::UITweener_BeginT1__GameObject__Single(JSVCall,System.Int32)
extern "C"  bool UITweenerGenerated_UITweener_BeginT1__GameObject__Single_m1494954734 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::__Register()
extern "C"  void UITweenerGenerated___Register_m1291437207 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Callback UITweenerGenerated::<UITweener_AddOnFinished__Callback>m__16A()
extern "C"  Callback_t1094463061 * UITweenerGenerated_U3CUITweener_AddOnFinished__CallbackU3Em__16A_m354765043 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Callback UITweenerGenerated::<UITweener_SetOnFinished__Callback>m__16C()
extern "C"  Callback_t1094463061 * UITweenerGenerated_U3CUITweener_SetOnFinished__CallbackU3Em__16C_m3359461460 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::ilo_setEnum1(System.Int32,System.Int32)
extern "C"  void UITweenerGenerated_ilo_setEnum1_m2934924241 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UITweenerGenerated::ilo_getEnum2(System.Int32)
extern "C"  int32_t UITweenerGenerated_ilo_getEnum2_m180768545 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UITweenerGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UITweenerGenerated_ilo_setObject3_m2017819484 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UITweenerGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UITweenerGenerated_ilo_getObject4_m4062663810 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void UITweenerGenerated_ilo_setSingle5_m4025181768 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::ilo_setInt326(System.Int32,System.Int32)
extern "C"  void UITweenerGenerated_ilo_setInt326_m2623940737 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::ilo_setStringS7(System.Int32,System.String)
extern "C"  void UITweenerGenerated_ilo_setStringS7_m2355784703 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::ilo_addJSFunCSDelegateRel8(System.Int32,System.Delegate)
extern "C"  void UITweenerGenerated_ilo_addJSFunCSDelegateRel8_m3235591273 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::ilo_AddOnFinished9(UITweener,EventDelegate)
extern "C"  void UITweenerGenerated_ilo_AddOnFinished9_m1986753069 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, EventDelegate_t4004424223 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::ilo_SetStartToCurrentValue10(UITweener)
extern "C"  void UITweenerGenerated_ilo_SetStartToCurrentValue10_m1920052085 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITweenerGenerated::ilo_Toggle11(UITweener)
extern "C"  void UITweenerGenerated_ilo_Toggle11_m318940659 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITweenerGenerated::ilo_isFunctionS12(System.Int32)
extern "C"  bool UITweenerGenerated_ilo_isFunctionS12_m3414466250 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Callback UITweenerGenerated::ilo_UITweener_AddOnFinished_GetDelegate_member0_arg013(CSRepresentedObject)
extern "C"  Callback_t1094463061 * UITweenerGenerated_ilo_UITweener_AddOnFinished_GetDelegate_member0_arg013_m1029754236 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate/Callback UITweenerGenerated::ilo_UITweener_SetOnFinished_GetDelegate_member10_arg014(CSRepresentedObject)
extern "C"  Callback_t1094463061 * UITweenerGenerated_ilo_UITweener_SetOnFinished_GetDelegate_member10_arg014_m95565457 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
