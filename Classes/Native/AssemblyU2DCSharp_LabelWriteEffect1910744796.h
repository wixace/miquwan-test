﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t844452154;
// UILabel
struct UILabel_t291504320;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LabelWriteEffect
struct  LabelWriteEffect_t1910744796  : public MonoBehaviour_t667441552
{
public:
	// System.Single LabelWriteEffect::_time_way
	float ____time_way_2;
	// System.String LabelWriteEffect::_text
	String_t* ____text_3;
	// System.Single LabelWriteEffect::_time
	float ____time_4;
	// System.Boolean LabelWriteEffect::_isFirst
	bool ____isFirst_5;
	// System.String LabelWriteEffect::_temString
	String_t* ____temString_6;
	// System.Collections.Generic.List`1<System.Action> LabelWriteEffect::_functionList
	List_1_t844452154 * ____functionList_7;
	// System.Int32 LabelWriteEffect::index
	int32_t ___index_8;
	// System.Int32 LabelWriteEffect::offset
	int32_t ___offset_9;
	// UILabel LabelWriteEffect::_label
	UILabel_t291504320 * ____label_10;
	// System.Boolean LabelWriteEffect::<isPlaying>k__BackingField
	bool ___U3CisPlayingU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of__time_way_2() { return static_cast<int32_t>(offsetof(LabelWriteEffect_t1910744796, ____time_way_2)); }
	inline float get__time_way_2() const { return ____time_way_2; }
	inline float* get_address_of__time_way_2() { return &____time_way_2; }
	inline void set__time_way_2(float value)
	{
		____time_way_2 = value;
	}

	inline static int32_t get_offset_of__text_3() { return static_cast<int32_t>(offsetof(LabelWriteEffect_t1910744796, ____text_3)); }
	inline String_t* get__text_3() const { return ____text_3; }
	inline String_t** get_address_of__text_3() { return &____text_3; }
	inline void set__text_3(String_t* value)
	{
		____text_3 = value;
		Il2CppCodeGenWriteBarrier(&____text_3, value);
	}

	inline static int32_t get_offset_of__time_4() { return static_cast<int32_t>(offsetof(LabelWriteEffect_t1910744796, ____time_4)); }
	inline float get__time_4() const { return ____time_4; }
	inline float* get_address_of__time_4() { return &____time_4; }
	inline void set__time_4(float value)
	{
		____time_4 = value;
	}

	inline static int32_t get_offset_of__isFirst_5() { return static_cast<int32_t>(offsetof(LabelWriteEffect_t1910744796, ____isFirst_5)); }
	inline bool get__isFirst_5() const { return ____isFirst_5; }
	inline bool* get_address_of__isFirst_5() { return &____isFirst_5; }
	inline void set__isFirst_5(bool value)
	{
		____isFirst_5 = value;
	}

	inline static int32_t get_offset_of__temString_6() { return static_cast<int32_t>(offsetof(LabelWriteEffect_t1910744796, ____temString_6)); }
	inline String_t* get__temString_6() const { return ____temString_6; }
	inline String_t** get_address_of__temString_6() { return &____temString_6; }
	inline void set__temString_6(String_t* value)
	{
		____temString_6 = value;
		Il2CppCodeGenWriteBarrier(&____temString_6, value);
	}

	inline static int32_t get_offset_of__functionList_7() { return static_cast<int32_t>(offsetof(LabelWriteEffect_t1910744796, ____functionList_7)); }
	inline List_1_t844452154 * get__functionList_7() const { return ____functionList_7; }
	inline List_1_t844452154 ** get_address_of__functionList_7() { return &____functionList_7; }
	inline void set__functionList_7(List_1_t844452154 * value)
	{
		____functionList_7 = value;
		Il2CppCodeGenWriteBarrier(&____functionList_7, value);
	}

	inline static int32_t get_offset_of_index_8() { return static_cast<int32_t>(offsetof(LabelWriteEffect_t1910744796, ___index_8)); }
	inline int32_t get_index_8() const { return ___index_8; }
	inline int32_t* get_address_of_index_8() { return &___index_8; }
	inline void set_index_8(int32_t value)
	{
		___index_8 = value;
	}

	inline static int32_t get_offset_of_offset_9() { return static_cast<int32_t>(offsetof(LabelWriteEffect_t1910744796, ___offset_9)); }
	inline int32_t get_offset_9() const { return ___offset_9; }
	inline int32_t* get_address_of_offset_9() { return &___offset_9; }
	inline void set_offset_9(int32_t value)
	{
		___offset_9 = value;
	}

	inline static int32_t get_offset_of__label_10() { return static_cast<int32_t>(offsetof(LabelWriteEffect_t1910744796, ____label_10)); }
	inline UILabel_t291504320 * get__label_10() const { return ____label_10; }
	inline UILabel_t291504320 ** get_address_of__label_10() { return &____label_10; }
	inline void set__label_10(UILabel_t291504320 * value)
	{
		____label_10 = value;
		Il2CppCodeGenWriteBarrier(&____label_10, value);
	}

	inline static int32_t get_offset_of_U3CisPlayingU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(LabelWriteEffect_t1910744796, ___U3CisPlayingU3Ek__BackingField_11)); }
	inline bool get_U3CisPlayingU3Ek__BackingField_11() const { return ___U3CisPlayingU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CisPlayingU3Ek__BackingField_11() { return &___U3CisPlayingU3Ek__BackingField_11; }
	inline void set_U3CisPlayingU3Ek__BackingField_11(bool value)
	{
		___U3CisPlayingU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
