﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.ClipperLib.ClipperBase
struct ClipperBase_t512275816;
// Pathfinding.ClipperLib.TEdge
struct TEdge_t3950806139;
// Pathfinding.ClipperLib.OutPt
struct OutPt_t3947633180;
// System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>
struct List_1_t399344435;
// Pathfinding.ClipperLib.LocalMinima
struct LocalMinima_t2863342752;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_TEdg3950806139.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_OutP3947633180.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Poly3336442536.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Loca2863342752.h"

// System.Void Pathfinding.ClipperLib.ClipperBase::.ctor()
extern "C"  void ClipperBase__ctor_m848649117 (ClipperBase_t512275816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.ClipperBase::get_PreserveCollinear()
extern "C"  bool ClipperBase_get_PreserveCollinear_m701993455 (ClipperBase_t512275816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.ClipperBase::set_PreserveCollinear(System.Boolean)
extern "C"  void ClipperBase_set_PreserveCollinear_m2366595086 (ClipperBase_t512275816 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.ClipperBase::IsHorizontal(Pathfinding.ClipperLib.TEdge)
extern "C"  bool ClipperBase_IsHorizontal_m1610406924 (Il2CppObject * __this /* static, unused */, TEdge_t3950806139 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.ClipperBase::PointOnLineSegment(Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint,System.Boolean)
extern "C"  bool ClipperBase_PointOnLineSegment_m3036412045 (ClipperBase_t512275816 * __this, IntPoint_t3326126179  ___pt0, IntPoint_t3326126179  ___linePt11, IntPoint_t3326126179  ___linePt22, bool ___UseFullRange3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.ClipperBase::PointOnPolygon(Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.OutPt,System.Boolean)
extern "C"  bool ClipperBase_PointOnPolygon_m903142260 (ClipperBase_t512275816 * __this, IntPoint_t3326126179  ___pt0, OutPt_t3947633180 * ___pp1, bool ___UseFullRange2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.ClipperBase::PointInPolygon(Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.OutPt,System.Boolean)
extern "C"  bool ClipperBase_PointInPolygon_m2483899322 (ClipperBase_t512275816 * __this, IntPoint_t3326126179  ___pt0, OutPt_t3947633180 * ___pp1, bool ___UseFullRange2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.ClipperBase::SlopesEqual(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge,System.Boolean)
extern "C"  bool ClipperBase_SlopesEqual_m3887656866 (Il2CppObject * __this /* static, unused */, TEdge_t3950806139 * ___e10, TEdge_t3950806139 * ___e21, bool ___UseFullRange2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.ClipperBase::SlopesEqual(Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint,System.Boolean)
extern "C"  bool ClipperBase_SlopesEqual_m1151992421 (Il2CppObject * __this /* static, unused */, IntPoint_t3326126179  ___pt10, IntPoint_t3326126179  ___pt21, IntPoint_t3326126179  ___pt32, bool ___UseFullRange3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.ClipperBase::Clear()
extern "C"  void ClipperBase_Clear_m2549749704 (ClipperBase_t512275816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.ClipperBase::DisposeLocalMinimaList()
extern "C"  void ClipperBase_DisposeLocalMinimaList_m1190589532 (ClipperBase_t512275816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.ClipperBase::RangeTest(Pathfinding.ClipperLib.IntPoint,System.Boolean&)
extern "C"  void ClipperBase_RangeTest_m1801249034 (ClipperBase_t512275816 * __this, IntPoint_t3326126179  ___Pt0, bool* ___useFullRange1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.ClipperBase::InitEdge(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.IntPoint)
extern "C"  void ClipperBase_InitEdge_m2546155214 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___e0, TEdge_t3950806139 * ___eNext1, TEdge_t3950806139 * ___ePrev2, IntPoint_t3326126179  ___pt3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.ClipperBase::InitEdge2(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.PolyType)
extern "C"  void ClipperBase_InitEdge2_m1715435253 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___e0, int32_t ___polyType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.ClipperBase::AddPath(System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>,Pathfinding.ClipperLib.PolyType,System.Boolean)
extern "C"  bool ClipperBase_AddPath_m2901119053 (ClipperBase_t512275816 * __this, List_1_t399344435 * ___pg0, int32_t ___polyType1, bool ___Closed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.ClipperBase::AddPolygon(System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>,Pathfinding.ClipperLib.PolyType)
extern "C"  bool ClipperBase_AddPolygon_m1504479783 (ClipperBase_t512275816 * __this, List_1_t399344435 * ___pg0, int32_t ___polyType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.ClipperBase::Pt2IsBetweenPt1AndPt3(Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint)
extern "C"  bool ClipperBase_Pt2IsBetweenPt1AndPt3_m1930781903 (ClipperBase_t512275816 * __this, IntPoint_t3326126179  ___pt10, IntPoint_t3326126179  ___pt21, IntPoint_t3326126179  ___pt32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ClipperLib.TEdge Pathfinding.ClipperLib.ClipperBase::RemoveEdge(Pathfinding.ClipperLib.TEdge)
extern "C"  TEdge_t3950806139 * ClipperBase_RemoveEdge_m2040466051 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ClipperLib.TEdge Pathfinding.ClipperLib.ClipperBase::GetLastHorz(Pathfinding.ClipperLib.TEdge)
extern "C"  TEdge_t3950806139 * ClipperBase_GetLastHorz_m3636788433 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___Edge0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.ClipperBase::SharedVertWithPrevAtTop(Pathfinding.ClipperLib.TEdge)
extern "C"  bool ClipperBase_SharedVertWithPrevAtTop_m4177200205 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___Edge0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.ClipperBase::SharedVertWithNextIsBot(Pathfinding.ClipperLib.TEdge)
extern "C"  bool ClipperBase_SharedVertWithNextIsBot_m1994098280 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___Edge0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.ClipperBase::MoreBelow(Pathfinding.ClipperLib.TEdge)
extern "C"  bool ClipperBase_MoreBelow_m3190791414 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___Edge0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.ClipperBase::JustBeforeLocMin(Pathfinding.ClipperLib.TEdge)
extern "C"  bool ClipperBase_JustBeforeLocMin_m120078395 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___Edge0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.ClipperBase::MoreAbove(Pathfinding.ClipperLib.TEdge)
extern "C"  bool ClipperBase_MoreAbove_m1517449058 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___Edge0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.ClipperBase::AllHorizontal(Pathfinding.ClipperLib.TEdge)
extern "C"  bool ClipperBase_AllHorizontal_m50403071 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___Edge0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.ClipperBase::SetDx(Pathfinding.ClipperLib.TEdge)
extern "C"  void ClipperBase_SetDx_m807634188 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.ClipperBase::DoMinimaLML(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge,System.Boolean)
extern "C"  void ClipperBase_DoMinimaLML_m1736312807 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___E10, TEdge_t3950806139 * ___E21, bool ___IsClosed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ClipperLib.TEdge Pathfinding.ClipperLib.ClipperBase::DescendToMin(Pathfinding.ClipperLib.TEdge&)
extern "C"  TEdge_t3950806139 * ClipperBase_DescendToMin_m3585442493 (ClipperBase_t512275816 * __this, TEdge_t3950806139 ** ___E0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.ClipperBase::AscendToMax(Pathfinding.ClipperLib.TEdge&,System.Boolean,System.Boolean)
extern "C"  void ClipperBase_AscendToMax_m1097068823 (ClipperBase_t512275816 * __this, TEdge_t3950806139 ** ___E0, bool ___Appending1, bool ___IsClosed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ClipperLib.TEdge Pathfinding.ClipperLib.ClipperBase::AddBoundsToLML(Pathfinding.ClipperLib.TEdge,System.Boolean)
extern "C"  TEdge_t3950806139 * ClipperBase_AddBoundsToLML_m1770302753 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___E0, bool ___Closed1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.ClipperBase::InsertLocalMinima(Pathfinding.ClipperLib.LocalMinima)
extern "C"  void ClipperBase_InsertLocalMinima_m2132432750 (ClipperBase_t512275816 * __this, LocalMinima_t2863342752 * ___newLm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.ClipperBase::PopLocalMinima()
extern "C"  void ClipperBase_PopLocalMinima_m3271239084 (ClipperBase_t512275816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.ClipperBase::ReverseHorizontal(Pathfinding.ClipperLib.TEdge)
extern "C"  void ClipperBase_ReverseHorizontal_m3767571996 (ClipperBase_t512275816 * __this, TEdge_t3950806139 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.ClipperBase::Reset()
extern "C"  void ClipperBase_Reset_m2790049354 (ClipperBase_t512275816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
