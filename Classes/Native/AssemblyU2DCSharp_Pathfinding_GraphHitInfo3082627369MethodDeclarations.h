﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.GraphHitInfo
struct GraphHitInfo_t3082627369;
struct GraphHitInfo_t3082627369_marshaled_pinvoke;
struct GraphHitInfo_t3082627369_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphHitInfo3082627369.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void Pathfinding.GraphHitInfo::.ctor(UnityEngine.Vector3)
extern "C"  void GraphHitInfo__ctor_m3292130235 (GraphHitInfo_t3082627369 * __this, Vector3_t4282066566  ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.GraphHitInfo::get_distance()
extern "C"  float GraphHitInfo_get_distance_m3440813328 (GraphHitInfo_t3082627369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct GraphHitInfo_t3082627369;
struct GraphHitInfo_t3082627369_marshaled_pinvoke;

extern "C" void GraphHitInfo_t3082627369_marshal_pinvoke(const GraphHitInfo_t3082627369& unmarshaled, GraphHitInfo_t3082627369_marshaled_pinvoke& marshaled);
extern "C" void GraphHitInfo_t3082627369_marshal_pinvoke_back(const GraphHitInfo_t3082627369_marshaled_pinvoke& marshaled, GraphHitInfo_t3082627369& unmarshaled);
extern "C" void GraphHitInfo_t3082627369_marshal_pinvoke_cleanup(GraphHitInfo_t3082627369_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct GraphHitInfo_t3082627369;
struct GraphHitInfo_t3082627369_marshaled_com;

extern "C" void GraphHitInfo_t3082627369_marshal_com(const GraphHitInfo_t3082627369& unmarshaled, GraphHitInfo_t3082627369_marshaled_com& marshaled);
extern "C" void GraphHitInfo_t3082627369_marshal_com_back(const GraphHitInfo_t3082627369_marshaled_com& marshaled, GraphHitInfo_t3082627369& unmarshaled);
extern "C" void GraphHitInfo_t3082627369_marshal_com_cleanup(GraphHitInfo_t3082627369_marshaled_com& marshaled);
