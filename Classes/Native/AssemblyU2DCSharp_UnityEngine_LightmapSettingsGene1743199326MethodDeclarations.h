﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_LightmapSettingsGenerated
struct UnityEngine_LightmapSettingsGenerated_t1743199326;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.LightmapData[]
struct LightmapDataU5BU5D_t705514461;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_LightmapSettingsGenerated::.ctor()
extern "C"  void UnityEngine_LightmapSettingsGenerated__ctor_m2110265037 (UnityEngine_LightmapSettingsGenerated_t1743199326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LightmapSettingsGenerated::LightmapSettings_LightmapSettings1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LightmapSettingsGenerated_LightmapSettings_LightmapSettings1_m2199066405 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightmapSettingsGenerated::LightmapSettings_lightmaps(JSVCall)
extern "C"  void UnityEngine_LightmapSettingsGenerated_LightmapSettings_lightmaps_m2334982297 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightmapSettingsGenerated::LightmapSettings_lightmapsMode(JSVCall)
extern "C"  void UnityEngine_LightmapSettingsGenerated_LightmapSettings_lightmapsMode_m3075890358 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightmapSettingsGenerated::LightmapSettings_lightProbes(JSVCall)
extern "C"  void UnityEngine_LightmapSettingsGenerated_LightmapSettings_lightProbes_m3792516941 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightmapSettingsGenerated::__Register()
extern "C"  void UnityEngine_LightmapSettingsGenerated___Register_m759733786 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LightmapData[] UnityEngine_LightmapSettingsGenerated::<LightmapSettings_lightmaps>m__260()
extern "C"  LightmapDataU5BU5D_t705514461* UnityEngine_LightmapSettingsGenerated_U3CLightmapSettings_lightmapsU3Em__260_m3183384269 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LightmapSettingsGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_LightmapSettingsGenerated_ilo_getObject1_m1407904777 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightmapSettingsGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_LightmapSettingsGenerated_ilo_addJSCSRel2_m3661164426 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightmapSettingsGenerated::ilo_setArrayS3(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_LightmapSettingsGenerated_ilo_setArrayS3_m3401146200 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightmapSettingsGenerated::ilo_setEnum4(System.Int32,System.Int32)
extern "C"  void UnityEngine_LightmapSettingsGenerated_ilo_setEnum4_m1250608779 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LightmapSettingsGenerated::ilo_getEnum5(System.Int32)
extern "C"  int32_t UnityEngine_LightmapSettingsGenerated_ilo_getEnum5_m788782155 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LightmapSettingsGenerated::ilo_getArrayLength6(System.Int32)
extern "C"  int32_t UnityEngine_LightmapSettingsGenerated_ilo_getArrayLength6_m166702916 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LightmapSettingsGenerated::ilo_getElement7(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_LightmapSettingsGenerated_ilo_getElement7_m816311503 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_LightmapSettingsGenerated::ilo_getObject8(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_LightmapSettingsGenerated_ilo_getObject8_m1501782079 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
