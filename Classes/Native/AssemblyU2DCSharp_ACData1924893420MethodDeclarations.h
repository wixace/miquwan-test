﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ACData
struct ACData_t1924893420;

#include "codegen/il2cpp-codegen.h"

// System.Void ACData::.ctor(System.UInt32,System.UInt32)
extern "C"  void ACData__ctor_m587941151 (ACData_t1924893420 * __this, uint32_t ___value0, uint32_t ___acValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ACData::SetValue(System.UInt32,System.UInt32)
extern "C"  void ACData_SetValue_m539588234 (ACData_t1924893420 * __this, uint32_t ___value0, uint32_t ___acValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ACData::GetValue()
extern "C"  uint32_t ACData_GetValue_m3287219851 (ACData_t1924893420 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
