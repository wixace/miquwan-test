﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CombatAttPlus/FightPlusAtt
struct FightPlusAtt_t4246110079;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CombatAttPlus
struct  CombatAttPlus_t649400871  : public Il2CppObject
{
public:
	// CombatAttPlus/FightPlusAtt CombatAttPlus::plusAtt
	FightPlusAtt_t4246110079 * ___plusAtt_0;

public:
	inline static int32_t get_offset_of_plusAtt_0() { return static_cast<int32_t>(offsetof(CombatAttPlus_t649400871, ___plusAtt_0)); }
	inline FightPlusAtt_t4246110079 * get_plusAtt_0() const { return ___plusAtt_0; }
	inline FightPlusAtt_t4246110079 ** get_address_of_plusAtt_0() { return &___plusAtt_0; }
	inline void set_plusAtt_0(FightPlusAtt_t4246110079 * value)
	{
		___plusAtt_0 = value;
		Il2CppCodeGenWriteBarrier(&___plusAtt_0, value);
	}
};

struct CombatAttPlus_t649400871_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> CombatAttPlus::<>f__switch$map1B
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map1B_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1B_1() { return static_cast<int32_t>(offsetof(CombatAttPlus_t649400871_StaticFields, ___U3CU3Ef__switchU24map1B_1)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map1B_1() const { return ___U3CU3Ef__switchU24map1B_1; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map1B_1() { return &___U3CU3Ef__switchU24map1B_1; }
	inline void set_U3CU3Ef__switchU24map1B_1(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map1B_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1B_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
