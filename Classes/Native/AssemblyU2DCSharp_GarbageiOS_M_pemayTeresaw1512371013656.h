﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_pemayTeresaw151
struct  M_pemayTeresaw151_t2371013656  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_pemayTeresaw151::_morkis
	float ____morkis_0;
	// System.String GarbageiOS.M_pemayTeresaw151::_toxe
	String_t* ____toxe_1;
	// System.Single GarbageiOS.M_pemayTeresaw151::_casgarstasMaircotall
	float ____casgarstasMaircotall_2;
	// System.Boolean GarbageiOS.M_pemayTeresaw151::_memper
	bool ____memper_3;
	// System.UInt32 GarbageiOS.M_pemayTeresaw151::_sawe
	uint32_t ____sawe_4;
	// System.String GarbageiOS.M_pemayTeresaw151::_jairjasdaNooxopair
	String_t* ____jairjasdaNooxopair_5;
	// System.Boolean GarbageiOS.M_pemayTeresaw151::_churiSuchaxa
	bool ____churiSuchaxa_6;
	// System.Int32 GarbageiOS.M_pemayTeresaw151::_hudakaw
	int32_t ____hudakaw_7;
	// System.String GarbageiOS.M_pemayTeresaw151::_cedearMarqair
	String_t* ____cedearMarqair_8;
	// System.String GarbageiOS.M_pemayTeresaw151::_papigir
	String_t* ____papigir_9;
	// System.Single GarbageiOS.M_pemayTeresaw151::_rowkistallNorgifear
	float ____rowkistallNorgifear_10;
	// System.Single GarbageiOS.M_pemayTeresaw151::_bepir
	float ____bepir_11;
	// System.String GarbageiOS.M_pemayTeresaw151::_nirkel
	String_t* ____nirkel_12;
	// System.String GarbageiOS.M_pemayTeresaw151::_dadroosou
	String_t* ____dadroosou_13;

public:
	inline static int32_t get_offset_of__morkis_0() { return static_cast<int32_t>(offsetof(M_pemayTeresaw151_t2371013656, ____morkis_0)); }
	inline float get__morkis_0() const { return ____morkis_0; }
	inline float* get_address_of__morkis_0() { return &____morkis_0; }
	inline void set__morkis_0(float value)
	{
		____morkis_0 = value;
	}

	inline static int32_t get_offset_of__toxe_1() { return static_cast<int32_t>(offsetof(M_pemayTeresaw151_t2371013656, ____toxe_1)); }
	inline String_t* get__toxe_1() const { return ____toxe_1; }
	inline String_t** get_address_of__toxe_1() { return &____toxe_1; }
	inline void set__toxe_1(String_t* value)
	{
		____toxe_1 = value;
		Il2CppCodeGenWriteBarrier(&____toxe_1, value);
	}

	inline static int32_t get_offset_of__casgarstasMaircotall_2() { return static_cast<int32_t>(offsetof(M_pemayTeresaw151_t2371013656, ____casgarstasMaircotall_2)); }
	inline float get__casgarstasMaircotall_2() const { return ____casgarstasMaircotall_2; }
	inline float* get_address_of__casgarstasMaircotall_2() { return &____casgarstasMaircotall_2; }
	inline void set__casgarstasMaircotall_2(float value)
	{
		____casgarstasMaircotall_2 = value;
	}

	inline static int32_t get_offset_of__memper_3() { return static_cast<int32_t>(offsetof(M_pemayTeresaw151_t2371013656, ____memper_3)); }
	inline bool get__memper_3() const { return ____memper_3; }
	inline bool* get_address_of__memper_3() { return &____memper_3; }
	inline void set__memper_3(bool value)
	{
		____memper_3 = value;
	}

	inline static int32_t get_offset_of__sawe_4() { return static_cast<int32_t>(offsetof(M_pemayTeresaw151_t2371013656, ____sawe_4)); }
	inline uint32_t get__sawe_4() const { return ____sawe_4; }
	inline uint32_t* get_address_of__sawe_4() { return &____sawe_4; }
	inline void set__sawe_4(uint32_t value)
	{
		____sawe_4 = value;
	}

	inline static int32_t get_offset_of__jairjasdaNooxopair_5() { return static_cast<int32_t>(offsetof(M_pemayTeresaw151_t2371013656, ____jairjasdaNooxopair_5)); }
	inline String_t* get__jairjasdaNooxopair_5() const { return ____jairjasdaNooxopair_5; }
	inline String_t** get_address_of__jairjasdaNooxopair_5() { return &____jairjasdaNooxopair_5; }
	inline void set__jairjasdaNooxopair_5(String_t* value)
	{
		____jairjasdaNooxopair_5 = value;
		Il2CppCodeGenWriteBarrier(&____jairjasdaNooxopair_5, value);
	}

	inline static int32_t get_offset_of__churiSuchaxa_6() { return static_cast<int32_t>(offsetof(M_pemayTeresaw151_t2371013656, ____churiSuchaxa_6)); }
	inline bool get__churiSuchaxa_6() const { return ____churiSuchaxa_6; }
	inline bool* get_address_of__churiSuchaxa_6() { return &____churiSuchaxa_6; }
	inline void set__churiSuchaxa_6(bool value)
	{
		____churiSuchaxa_6 = value;
	}

	inline static int32_t get_offset_of__hudakaw_7() { return static_cast<int32_t>(offsetof(M_pemayTeresaw151_t2371013656, ____hudakaw_7)); }
	inline int32_t get__hudakaw_7() const { return ____hudakaw_7; }
	inline int32_t* get_address_of__hudakaw_7() { return &____hudakaw_7; }
	inline void set__hudakaw_7(int32_t value)
	{
		____hudakaw_7 = value;
	}

	inline static int32_t get_offset_of__cedearMarqair_8() { return static_cast<int32_t>(offsetof(M_pemayTeresaw151_t2371013656, ____cedearMarqair_8)); }
	inline String_t* get__cedearMarqair_8() const { return ____cedearMarqair_8; }
	inline String_t** get_address_of__cedearMarqair_8() { return &____cedearMarqair_8; }
	inline void set__cedearMarqair_8(String_t* value)
	{
		____cedearMarqair_8 = value;
		Il2CppCodeGenWriteBarrier(&____cedearMarqair_8, value);
	}

	inline static int32_t get_offset_of__papigir_9() { return static_cast<int32_t>(offsetof(M_pemayTeresaw151_t2371013656, ____papigir_9)); }
	inline String_t* get__papigir_9() const { return ____papigir_9; }
	inline String_t** get_address_of__papigir_9() { return &____papigir_9; }
	inline void set__papigir_9(String_t* value)
	{
		____papigir_9 = value;
		Il2CppCodeGenWriteBarrier(&____papigir_9, value);
	}

	inline static int32_t get_offset_of__rowkistallNorgifear_10() { return static_cast<int32_t>(offsetof(M_pemayTeresaw151_t2371013656, ____rowkistallNorgifear_10)); }
	inline float get__rowkistallNorgifear_10() const { return ____rowkistallNorgifear_10; }
	inline float* get_address_of__rowkistallNorgifear_10() { return &____rowkistallNorgifear_10; }
	inline void set__rowkistallNorgifear_10(float value)
	{
		____rowkistallNorgifear_10 = value;
	}

	inline static int32_t get_offset_of__bepir_11() { return static_cast<int32_t>(offsetof(M_pemayTeresaw151_t2371013656, ____bepir_11)); }
	inline float get__bepir_11() const { return ____bepir_11; }
	inline float* get_address_of__bepir_11() { return &____bepir_11; }
	inline void set__bepir_11(float value)
	{
		____bepir_11 = value;
	}

	inline static int32_t get_offset_of__nirkel_12() { return static_cast<int32_t>(offsetof(M_pemayTeresaw151_t2371013656, ____nirkel_12)); }
	inline String_t* get__nirkel_12() const { return ____nirkel_12; }
	inline String_t** get_address_of__nirkel_12() { return &____nirkel_12; }
	inline void set__nirkel_12(String_t* value)
	{
		____nirkel_12 = value;
		Il2CppCodeGenWriteBarrier(&____nirkel_12, value);
	}

	inline static int32_t get_offset_of__dadroosou_13() { return static_cast<int32_t>(offsetof(M_pemayTeresaw151_t2371013656, ____dadroosou_13)); }
	inline String_t* get__dadroosou_13() const { return ____dadroosou_13; }
	inline String_t** get_address_of__dadroosou_13() { return &____dadroosou_13; }
	inline void set__dadroosou_13(String_t* value)
	{
		____dadroosou_13 = value;
		Il2CppCodeGenWriteBarrier(&____dadroosou_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
