﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_rotalsowLeawhuqe160
struct  M_rotalsowLeawhuqe160_t438113858  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_rotalsowLeawhuqe160::_lalwidouHemsace
	String_t* ____lalwidouHemsace_0;
	// System.UInt32 GarbageiOS.M_rotalsowLeawhuqe160::_lousou
	uint32_t ____lousou_1;
	// System.Boolean GarbageiOS.M_rotalsowLeawhuqe160::_habasWhilichay
	bool ____habasWhilichay_2;
	// System.Single GarbageiOS.M_rotalsowLeawhuqe160::_doure
	float ____doure_3;
	// System.String GarbageiOS.M_rotalsowLeawhuqe160::_worjir
	String_t* ____worjir_4;
	// System.String GarbageiOS.M_rotalsowLeawhuqe160::_raigaygee
	String_t* ____raigaygee_5;
	// System.String GarbageiOS.M_rotalsowLeawhuqe160::_mecu
	String_t* ____mecu_6;
	// System.Single GarbageiOS.M_rotalsowLeawhuqe160::_goonaMeedevee
	float ____goonaMeedevee_7;
	// System.Single GarbageiOS.M_rotalsowLeawhuqe160::_steredrardra
	float ____steredrardra_8;
	// System.Single GarbageiOS.M_rotalsowLeawhuqe160::_nalePinoo
	float ____nalePinoo_9;
	// System.Int32 GarbageiOS.M_rotalsowLeawhuqe160::_traiwouNeperesel
	int32_t ____traiwouNeperesel_10;
	// System.Single GarbageiOS.M_rotalsowLeawhuqe160::_tamidall
	float ____tamidall_11;
	// System.UInt32 GarbageiOS.M_rotalsowLeawhuqe160::_belcexemRiwirkou
	uint32_t ____belcexemRiwirkou_12;
	// System.Boolean GarbageiOS.M_rotalsowLeawhuqe160::_qoucouNeefou
	bool ____qoucouNeefou_13;
	// System.Single GarbageiOS.M_rotalsowLeawhuqe160::_datrai
	float ____datrai_14;
	// System.String GarbageiOS.M_rotalsowLeawhuqe160::_herche
	String_t* ____herche_15;

public:
	inline static int32_t get_offset_of__lalwidouHemsace_0() { return static_cast<int32_t>(offsetof(M_rotalsowLeawhuqe160_t438113858, ____lalwidouHemsace_0)); }
	inline String_t* get__lalwidouHemsace_0() const { return ____lalwidouHemsace_0; }
	inline String_t** get_address_of__lalwidouHemsace_0() { return &____lalwidouHemsace_0; }
	inline void set__lalwidouHemsace_0(String_t* value)
	{
		____lalwidouHemsace_0 = value;
		Il2CppCodeGenWriteBarrier(&____lalwidouHemsace_0, value);
	}

	inline static int32_t get_offset_of__lousou_1() { return static_cast<int32_t>(offsetof(M_rotalsowLeawhuqe160_t438113858, ____lousou_1)); }
	inline uint32_t get__lousou_1() const { return ____lousou_1; }
	inline uint32_t* get_address_of__lousou_1() { return &____lousou_1; }
	inline void set__lousou_1(uint32_t value)
	{
		____lousou_1 = value;
	}

	inline static int32_t get_offset_of__habasWhilichay_2() { return static_cast<int32_t>(offsetof(M_rotalsowLeawhuqe160_t438113858, ____habasWhilichay_2)); }
	inline bool get__habasWhilichay_2() const { return ____habasWhilichay_2; }
	inline bool* get_address_of__habasWhilichay_2() { return &____habasWhilichay_2; }
	inline void set__habasWhilichay_2(bool value)
	{
		____habasWhilichay_2 = value;
	}

	inline static int32_t get_offset_of__doure_3() { return static_cast<int32_t>(offsetof(M_rotalsowLeawhuqe160_t438113858, ____doure_3)); }
	inline float get__doure_3() const { return ____doure_3; }
	inline float* get_address_of__doure_3() { return &____doure_3; }
	inline void set__doure_3(float value)
	{
		____doure_3 = value;
	}

	inline static int32_t get_offset_of__worjir_4() { return static_cast<int32_t>(offsetof(M_rotalsowLeawhuqe160_t438113858, ____worjir_4)); }
	inline String_t* get__worjir_4() const { return ____worjir_4; }
	inline String_t** get_address_of__worjir_4() { return &____worjir_4; }
	inline void set__worjir_4(String_t* value)
	{
		____worjir_4 = value;
		Il2CppCodeGenWriteBarrier(&____worjir_4, value);
	}

	inline static int32_t get_offset_of__raigaygee_5() { return static_cast<int32_t>(offsetof(M_rotalsowLeawhuqe160_t438113858, ____raigaygee_5)); }
	inline String_t* get__raigaygee_5() const { return ____raigaygee_5; }
	inline String_t** get_address_of__raigaygee_5() { return &____raigaygee_5; }
	inline void set__raigaygee_5(String_t* value)
	{
		____raigaygee_5 = value;
		Il2CppCodeGenWriteBarrier(&____raigaygee_5, value);
	}

	inline static int32_t get_offset_of__mecu_6() { return static_cast<int32_t>(offsetof(M_rotalsowLeawhuqe160_t438113858, ____mecu_6)); }
	inline String_t* get__mecu_6() const { return ____mecu_6; }
	inline String_t** get_address_of__mecu_6() { return &____mecu_6; }
	inline void set__mecu_6(String_t* value)
	{
		____mecu_6 = value;
		Il2CppCodeGenWriteBarrier(&____mecu_6, value);
	}

	inline static int32_t get_offset_of__goonaMeedevee_7() { return static_cast<int32_t>(offsetof(M_rotalsowLeawhuqe160_t438113858, ____goonaMeedevee_7)); }
	inline float get__goonaMeedevee_7() const { return ____goonaMeedevee_7; }
	inline float* get_address_of__goonaMeedevee_7() { return &____goonaMeedevee_7; }
	inline void set__goonaMeedevee_7(float value)
	{
		____goonaMeedevee_7 = value;
	}

	inline static int32_t get_offset_of__steredrardra_8() { return static_cast<int32_t>(offsetof(M_rotalsowLeawhuqe160_t438113858, ____steredrardra_8)); }
	inline float get__steredrardra_8() const { return ____steredrardra_8; }
	inline float* get_address_of__steredrardra_8() { return &____steredrardra_8; }
	inline void set__steredrardra_8(float value)
	{
		____steredrardra_8 = value;
	}

	inline static int32_t get_offset_of__nalePinoo_9() { return static_cast<int32_t>(offsetof(M_rotalsowLeawhuqe160_t438113858, ____nalePinoo_9)); }
	inline float get__nalePinoo_9() const { return ____nalePinoo_9; }
	inline float* get_address_of__nalePinoo_9() { return &____nalePinoo_9; }
	inline void set__nalePinoo_9(float value)
	{
		____nalePinoo_9 = value;
	}

	inline static int32_t get_offset_of__traiwouNeperesel_10() { return static_cast<int32_t>(offsetof(M_rotalsowLeawhuqe160_t438113858, ____traiwouNeperesel_10)); }
	inline int32_t get__traiwouNeperesel_10() const { return ____traiwouNeperesel_10; }
	inline int32_t* get_address_of__traiwouNeperesel_10() { return &____traiwouNeperesel_10; }
	inline void set__traiwouNeperesel_10(int32_t value)
	{
		____traiwouNeperesel_10 = value;
	}

	inline static int32_t get_offset_of__tamidall_11() { return static_cast<int32_t>(offsetof(M_rotalsowLeawhuqe160_t438113858, ____tamidall_11)); }
	inline float get__tamidall_11() const { return ____tamidall_11; }
	inline float* get_address_of__tamidall_11() { return &____tamidall_11; }
	inline void set__tamidall_11(float value)
	{
		____tamidall_11 = value;
	}

	inline static int32_t get_offset_of__belcexemRiwirkou_12() { return static_cast<int32_t>(offsetof(M_rotalsowLeawhuqe160_t438113858, ____belcexemRiwirkou_12)); }
	inline uint32_t get__belcexemRiwirkou_12() const { return ____belcexemRiwirkou_12; }
	inline uint32_t* get_address_of__belcexemRiwirkou_12() { return &____belcexemRiwirkou_12; }
	inline void set__belcexemRiwirkou_12(uint32_t value)
	{
		____belcexemRiwirkou_12 = value;
	}

	inline static int32_t get_offset_of__qoucouNeefou_13() { return static_cast<int32_t>(offsetof(M_rotalsowLeawhuqe160_t438113858, ____qoucouNeefou_13)); }
	inline bool get__qoucouNeefou_13() const { return ____qoucouNeefou_13; }
	inline bool* get_address_of__qoucouNeefou_13() { return &____qoucouNeefou_13; }
	inline void set__qoucouNeefou_13(bool value)
	{
		____qoucouNeefou_13 = value;
	}

	inline static int32_t get_offset_of__datrai_14() { return static_cast<int32_t>(offsetof(M_rotalsowLeawhuqe160_t438113858, ____datrai_14)); }
	inline float get__datrai_14() const { return ____datrai_14; }
	inline float* get_address_of__datrai_14() { return &____datrai_14; }
	inline void set__datrai_14(float value)
	{
		____datrai_14 = value;
	}

	inline static int32_t get_offset_of__herche_15() { return static_cast<int32_t>(offsetof(M_rotalsowLeawhuqe160_t438113858, ____herche_15)); }
	inline String_t* get__herche_15() const { return ____herche_15; }
	inline String_t** get_address_of__herche_15() { return &____herche_15; }
	inline void set__herche_15(String_t* value)
	{
		____herche_15 = value;
		Il2CppCodeGenWriteBarrier(&____herche_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
