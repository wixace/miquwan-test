﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen181416766.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_CombineInstance1399074090.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.CombineInstance>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3290469154_gshared (InternalEnumerator_1_t181416766 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3290469154(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t181416766 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3290469154_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.CombineInstance>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1707424574_gshared (InternalEnumerator_1_t181416766 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1707424574(__this, method) ((  void (*) (InternalEnumerator_1_t181416766 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1707424574_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.CombineInstance>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3651297204_gshared (InternalEnumerator_1_t181416766 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3651297204(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t181416766 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3651297204_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.CombineInstance>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3441639289_gshared (InternalEnumerator_1_t181416766 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3441639289(__this, method) ((  void (*) (InternalEnumerator_1_t181416766 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3441639289_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.CombineInstance>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2793782766_gshared (InternalEnumerator_1_t181416766 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2793782766(__this, method) ((  bool (*) (InternalEnumerator_1_t181416766 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2793782766_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.CombineInstance>::get_Current()
extern "C"  CombineInstance_t1399074090  InternalEnumerator_1_get_Current_m3397537227_gshared (InternalEnumerator_1_t181416766 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3397537227(__this, method) ((  CombineInstance_t1399074090  (*) (InternalEnumerator_1_t181416766 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3397537227_gshared)(__this, method)
