﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_culerla71
struct M_culerla71_t767256374;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_culerla71::.ctor()
extern "C"  void M_culerla71__ctor_m1563129581 (M_culerla71_t767256374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_culerla71::M_xeadri0(System.String[],System.Int32)
extern "C"  void M_culerla71_M_xeadri0_m10137273 (M_culerla71_t767256374 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_culerla71::M_kirleaka1(System.String[],System.Int32)
extern "C"  void M_culerla71_M_kirleaka1_m2065818519 (M_culerla71_t767256374 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
