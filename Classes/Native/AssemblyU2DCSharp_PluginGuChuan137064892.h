﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginGuChuan
struct  PluginGuChuan_t137064892  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginGuChuan::openid
	String_t* ___openid_2;
	// System.Boolean PluginGuChuan::isOut
	bool ___isOut_3;
	// System.String PluginGuChuan::configId
	String_t* ___configId_4;
	// Mihua.SDK.PayInfo PluginGuChuan::payInfo
	PayInfo_t1775308120 * ___payInfo_5;

public:
	inline static int32_t get_offset_of_openid_2() { return static_cast<int32_t>(offsetof(PluginGuChuan_t137064892, ___openid_2)); }
	inline String_t* get_openid_2() const { return ___openid_2; }
	inline String_t** get_address_of_openid_2() { return &___openid_2; }
	inline void set_openid_2(String_t* value)
	{
		___openid_2 = value;
		Il2CppCodeGenWriteBarrier(&___openid_2, value);
	}

	inline static int32_t get_offset_of_isOut_3() { return static_cast<int32_t>(offsetof(PluginGuChuan_t137064892, ___isOut_3)); }
	inline bool get_isOut_3() const { return ___isOut_3; }
	inline bool* get_address_of_isOut_3() { return &___isOut_3; }
	inline void set_isOut_3(bool value)
	{
		___isOut_3 = value;
	}

	inline static int32_t get_offset_of_configId_4() { return static_cast<int32_t>(offsetof(PluginGuChuan_t137064892, ___configId_4)); }
	inline String_t* get_configId_4() const { return ___configId_4; }
	inline String_t** get_address_of_configId_4() { return &___configId_4; }
	inline void set_configId_4(String_t* value)
	{
		___configId_4 = value;
		Il2CppCodeGenWriteBarrier(&___configId_4, value);
	}

	inline static int32_t get_offset_of_payInfo_5() { return static_cast<int32_t>(offsetof(PluginGuChuan_t137064892, ___payInfo_5)); }
	inline PayInfo_t1775308120 * get_payInfo_5() const { return ___payInfo_5; }
	inline PayInfo_t1775308120 ** get_address_of_payInfo_5() { return &___payInfo_5; }
	inline void set_payInfo_5(PayInfo_t1775308120 * value)
	{
		___payInfo_5 = value;
		Il2CppCodeGenWriteBarrier(&___payInfo_5, value);
	}
};

struct PluginGuChuan_t137064892_StaticFields
{
public:
	// System.Action PluginGuChuan::<>f__am$cache4
	Action_t3771233898 * ___U3CU3Ef__amU24cache4_6;
	// System.Action PluginGuChuan::<>f__am$cache5
	Action_t3771233898 * ___U3CU3Ef__amU24cache5_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_6() { return static_cast<int32_t>(offsetof(PluginGuChuan_t137064892_StaticFields, ___U3CU3Ef__amU24cache4_6)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache4_6() const { return ___U3CU3Ef__amU24cache4_6; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache4_6() { return &___U3CU3Ef__amU24cache4_6; }
	inline void set_U3CU3Ef__amU24cache4_6(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache4_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_7() { return static_cast<int32_t>(offsetof(PluginGuChuan_t137064892_StaticFields, ___U3CU3Ef__amU24cache5_7)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache5_7() const { return ___U3CU3Ef__amU24cache5_7; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache5_7() { return &___U3CU3Ef__amU24cache5_7; }
	inline void set_U3CU3Ef__amU24cache5_7(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache5_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
