﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// friendNpcCfg
struct  friendNpcCfg_t3890310657  : public CsCfgBase_t69924517
{
public:
	// System.Int32 friendNpcCfg::id
	int32_t ___id_0;
	// System.Int32 friendNpcCfg::fightType
	int32_t ___fightType_1;
	// System.Single friendNpcCfg::viewDistance
	float ___viewDistance_2;
	// System.Single friendNpcCfg::fightDistance
	float ___fightDistance_3;
	// System.String friendNpcCfg::targetPoin
	String_t* ___targetPoin_4;
	// System.Int32 friendNpcCfg::effect_friend
	int32_t ___effect_friend_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(friendNpcCfg_t3890310657, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_fightType_1() { return static_cast<int32_t>(offsetof(friendNpcCfg_t3890310657, ___fightType_1)); }
	inline int32_t get_fightType_1() const { return ___fightType_1; }
	inline int32_t* get_address_of_fightType_1() { return &___fightType_1; }
	inline void set_fightType_1(int32_t value)
	{
		___fightType_1 = value;
	}

	inline static int32_t get_offset_of_viewDistance_2() { return static_cast<int32_t>(offsetof(friendNpcCfg_t3890310657, ___viewDistance_2)); }
	inline float get_viewDistance_2() const { return ___viewDistance_2; }
	inline float* get_address_of_viewDistance_2() { return &___viewDistance_2; }
	inline void set_viewDistance_2(float value)
	{
		___viewDistance_2 = value;
	}

	inline static int32_t get_offset_of_fightDistance_3() { return static_cast<int32_t>(offsetof(friendNpcCfg_t3890310657, ___fightDistance_3)); }
	inline float get_fightDistance_3() const { return ___fightDistance_3; }
	inline float* get_address_of_fightDistance_3() { return &___fightDistance_3; }
	inline void set_fightDistance_3(float value)
	{
		___fightDistance_3 = value;
	}

	inline static int32_t get_offset_of_targetPoin_4() { return static_cast<int32_t>(offsetof(friendNpcCfg_t3890310657, ___targetPoin_4)); }
	inline String_t* get_targetPoin_4() const { return ___targetPoin_4; }
	inline String_t** get_address_of_targetPoin_4() { return &___targetPoin_4; }
	inline void set_targetPoin_4(String_t* value)
	{
		___targetPoin_4 = value;
		Il2CppCodeGenWriteBarrier(&___targetPoin_4, value);
	}

	inline static int32_t get_offset_of_effect_friend_5() { return static_cast<int32_t>(offsetof(friendNpcCfg_t3890310657, ___effect_friend_5)); }
	inline int32_t get_effect_friend_5() const { return ___effect_friend_5; }
	inline int32_t* get_address_of_effect_friend_5() { return &___effect_friend_5; }
	inline void set_effect_friend_5(int32_t value)
	{
		___effect_friend_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
