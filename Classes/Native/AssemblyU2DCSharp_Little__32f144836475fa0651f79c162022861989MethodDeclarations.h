﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._32f144836475fa0651f79c1649a2141f
struct _32f144836475fa0651f79c1649a2141f_t2022861989;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__32f144836475fa0651f79c162022861989.h"

// System.Void Little._32f144836475fa0651f79c1649a2141f::.ctor()
extern "C"  void _32f144836475fa0651f79c1649a2141f__ctor_m914367272 (_32f144836475fa0651f79c1649a2141f_t2022861989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._32f144836475fa0651f79c1649a2141f::_32f144836475fa0651f79c1649a2141fm2(System.Int32)
extern "C"  int32_t _32f144836475fa0651f79c1649a2141f__32f144836475fa0651f79c1649a2141fm2_m3827246969 (_32f144836475fa0651f79c1649a2141f_t2022861989 * __this, int32_t ____32f144836475fa0651f79c1649a2141fa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._32f144836475fa0651f79c1649a2141f::_32f144836475fa0651f79c1649a2141fm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _32f144836475fa0651f79c1649a2141f__32f144836475fa0651f79c1649a2141fm_m3961510749 (_32f144836475fa0651f79c1649a2141f_t2022861989 * __this, int32_t ____32f144836475fa0651f79c1649a2141fa0, int32_t ____32f144836475fa0651f79c1649a2141f271, int32_t ____32f144836475fa0651f79c1649a2141fc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._32f144836475fa0651f79c1649a2141f::ilo__32f144836475fa0651f79c1649a2141fm21(Little._32f144836475fa0651f79c1649a2141f,System.Int32)
extern "C"  int32_t _32f144836475fa0651f79c1649a2141f_ilo__32f144836475fa0651f79c1649a2141fm21_m463541772 (Il2CppObject * __this /* static, unused */, _32f144836475fa0651f79c1649a2141f_t2022861989 * ____this0, int32_t ____32f144836475fa0651f79c1649a2141fa1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
