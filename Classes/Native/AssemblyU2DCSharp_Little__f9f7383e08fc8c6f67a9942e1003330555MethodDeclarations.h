﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._f9f7383e08fc8c6f67a9942e03b92888
struct _f9f7383e08fc8c6f67a9942e03b92888_t1003330555;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._f9f7383e08fc8c6f67a9942e03b92888::.ctor()
extern "C"  void _f9f7383e08fc8c6f67a9942e03b92888__ctor_m1914033298 (_f9f7383e08fc8c6f67a9942e03b92888_t1003330555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f9f7383e08fc8c6f67a9942e03b92888::_f9f7383e08fc8c6f67a9942e03b92888m2(System.Int32)
extern "C"  int32_t _f9f7383e08fc8c6f67a9942e03b92888__f9f7383e08fc8c6f67a9942e03b92888m2_m908933049 (_f9f7383e08fc8c6f67a9942e03b92888_t1003330555 * __this, int32_t ____f9f7383e08fc8c6f67a9942e03b92888a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f9f7383e08fc8c6f67a9942e03b92888::_f9f7383e08fc8c6f67a9942e03b92888m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _f9f7383e08fc8c6f67a9942e03b92888__f9f7383e08fc8c6f67a9942e03b92888m_m1901630237 (_f9f7383e08fc8c6f67a9942e03b92888_t1003330555 * __this, int32_t ____f9f7383e08fc8c6f67a9942e03b92888a0, int32_t ____f9f7383e08fc8c6f67a9942e03b92888511, int32_t ____f9f7383e08fc8c6f67a9942e03b92888c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
