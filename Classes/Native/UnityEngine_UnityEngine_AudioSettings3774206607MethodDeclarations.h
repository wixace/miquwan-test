﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AudioSettings
struct AudioSettings_t3774206607;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AudioSpeakerMode2367651856.h"
#include "UnityEngine_UnityEngine_AudioConfiguration4109410948.h"

// System.Void UnityEngine.AudioSettings::.ctor()
extern "C"  void AudioSettings__ctor_m2217406432 (AudioSettings_t3774206607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioSpeakerMode UnityEngine.AudioSettings::get_driverCapabilities()
extern "C"  int32_t AudioSettings_get_driverCapabilities_m1717863295 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioSpeakerMode UnityEngine.AudioSettings::get_speakerMode()
extern "C"  int32_t AudioSettings_get_speakerMode_m1679994051 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSettings::set_speakerMode(UnityEngine.AudioSpeakerMode)
extern "C"  void AudioSettings_set_speakerMode_m487446304 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double UnityEngine.AudioSettings::get_dspTime()
extern "C"  double AudioSettings_get_dspTime_m4286496774 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AudioSettings::get_outputSampleRate()
extern "C"  int32_t AudioSettings_get_outputSampleRate_m3345329432 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSettings::set_outputSampleRate(System.Int32)
extern "C"  void AudioSettings_set_outputSampleRate_m999723101 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSettings::GetDSPBufferSize(System.Int32&,System.Int32&)
extern "C"  void AudioSettings_GetDSPBufferSize_m1577501184 (Il2CppObject * __this /* static, unused */, int32_t* ___bufferLength0, int32_t* ___numBuffers1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioConfiguration UnityEngine.AudioSettings::GetConfiguration()
extern "C"  AudioConfiguration_t4109410948  AudioSettings_GetConfiguration_m1435999052 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSettings::INTERNAL_CALL_GetConfiguration(UnityEngine.AudioConfiguration&)
extern "C"  void AudioSettings_INTERNAL_CALL_GetConfiguration_m2757117248 (Il2CppObject * __this /* static, unused */, AudioConfiguration_t4109410948 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AudioSettings::Reset(UnityEngine.AudioConfiguration)
extern "C"  bool AudioSettings_Reset_m3614368540 (Il2CppObject * __this /* static, unused */, AudioConfiguration_t4109410948  ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AudioSettings::INTERNAL_CALL_Reset(UnityEngine.AudioConfiguration&)
extern "C"  bool AudioSettings_INTERNAL_CALL_Reset_m1099494383 (Il2CppObject * __this /* static, unused */, AudioConfiguration_t4109410948 * ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSettings::InvokeOnAudioConfigurationChanged(System.Boolean)
extern "C"  void AudioSettings_InvokeOnAudioConfigurationChanged_m1851666322 (Il2CppObject * __this /* static, unused */, bool ___deviceWasChanged0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
