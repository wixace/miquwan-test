﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._5ac663cbef4d858a7eaa7e7e41da552e
struct _5ac663cbef4d858a7eaa7e7e41da552e_t3375610690;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._5ac663cbef4d858a7eaa7e7e41da552e::.ctor()
extern "C"  void _5ac663cbef4d858a7eaa7e7e41da552e__ctor_m1015084331 (_5ac663cbef4d858a7eaa7e7e41da552e_t3375610690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._5ac663cbef4d858a7eaa7e7e41da552e::_5ac663cbef4d858a7eaa7e7e41da552em2(System.Int32)
extern "C"  int32_t _5ac663cbef4d858a7eaa7e7e41da552e__5ac663cbef4d858a7eaa7e7e41da552em2_m4012915289 (_5ac663cbef4d858a7eaa7e7e41da552e_t3375610690 * __this, int32_t ____5ac663cbef4d858a7eaa7e7e41da552ea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._5ac663cbef4d858a7eaa7e7e41da552e::_5ac663cbef4d858a7eaa7e7e41da552em(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _5ac663cbef4d858a7eaa7e7e41da552e__5ac663cbef4d858a7eaa7e7e41da552em_m2888811645 (_5ac663cbef4d858a7eaa7e7e41da552e_t3375610690 * __this, int32_t ____5ac663cbef4d858a7eaa7e7e41da552ea0, int32_t ____5ac663cbef4d858a7eaa7e7e41da552e521, int32_t ____5ac663cbef4d858a7eaa7e7e41da552ec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
