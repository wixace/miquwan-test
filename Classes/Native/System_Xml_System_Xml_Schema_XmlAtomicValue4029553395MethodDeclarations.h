﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Schema.XmlAtomicValue
struct XmlAtomicValue_t4029553395;
// System.Object
struct Il2CppObject;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_t4090188264;
// System.String
struct String_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaType4090188264.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_String7231557.h"
#include "System_Xml_System_Xml_Schema_XmlTypeCode3316180404.h"
#include "mscorlib_System_Type2863145774.h"

// System.Void System.Xml.Schema.XmlAtomicValue::.ctor(System.Object,System.Xml.Schema.XmlSchemaType)
extern "C"  void XmlAtomicValue__ctor_m1137026790 (XmlAtomicValue_t4029553395 * __this, Il2CppObject * ___value0, XmlSchemaType_t4090188264 * ___xmlType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Schema.XmlAtomicValue::System.ICloneable.Clone()
extern "C"  Il2CppObject * XmlAtomicValue_System_ICloneable_Clone_m4133325764 (XmlAtomicValue_t4029553395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlAtomicValue::Init(System.Boolean,System.Xml.Schema.XmlSchemaType)
extern "C"  void XmlAtomicValue_Init_m1229416989 (XmlAtomicValue_t4029553395 * __this, bool ___value0, XmlSchemaType_t4090188264 * ___xmlType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlAtomicValue::Init(System.DateTime,System.Xml.Schema.XmlSchemaType)
extern "C"  void XmlAtomicValue_Init_m2288495060 (XmlAtomicValue_t4029553395 * __this, DateTime_t4283661327  ___value0, XmlSchemaType_t4090188264 * ___xmlType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlAtomicValue::Init(System.Decimal,System.Xml.Schema.XmlSchemaType)
extern "C"  void XmlAtomicValue_Init_m2052918292 (XmlAtomicValue_t4029553395 * __this, Decimal_t1954350631  ___value0, XmlSchemaType_t4090188264 * ___xmlType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlAtomicValue::Init(System.Double,System.Xml.Schema.XmlSchemaType)
extern "C"  void XmlAtomicValue_Init_m1521581470 (XmlAtomicValue_t4029553395 * __this, double ___value0, XmlSchemaType_t4090188264 * ___xmlType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlAtomicValue::Init(System.Int32,System.Xml.Schema.XmlSchemaType)
extern "C"  void XmlAtomicValue_Init_m3192877879 (XmlAtomicValue_t4029553395 * __this, int32_t ___value0, XmlSchemaType_t4090188264 * ___xmlType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlAtomicValue::Init(System.Int64,System.Xml.Schema.XmlSchemaType)
extern "C"  void XmlAtomicValue_Init_m1830369976 (XmlAtomicValue_t4029553395 * __this, int64_t ___value0, XmlSchemaType_t4090188264 * ___xmlType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlAtomicValue::Init(System.Single,System.Xml.Schema.XmlSchemaType)
extern "C"  void XmlAtomicValue_Init_m2507513863 (XmlAtomicValue_t4029553395 * __this, float ___value0, XmlSchemaType_t4090188264 * ___xmlType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlAtomicValue::Init(System.String,System.Xml.Schema.XmlSchemaType)
extern "C"  void XmlAtomicValue_Init_m1125196126 (XmlAtomicValue_t4029553395 * __this, String_t* ___value0, XmlSchemaType_t4090188264 * ___xmlType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlAtomicValue::Init(System.Object,System.Xml.Schema.XmlSchemaType)
extern "C"  void XmlAtomicValue_Init_m2286910448 (XmlAtomicValue_t4029553395 * __this, Il2CppObject * ___value0, XmlSchemaType_t4090188264 * ___xmlType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlAtomicValue System.Xml.Schema.XmlAtomicValue::Clone()
extern "C"  XmlAtomicValue_t4029553395 * XmlAtomicValue_Clone_m652145135 (XmlAtomicValue_t4029553395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Schema.XmlAtomicValue::ToString()
extern "C"  String_t* XmlAtomicValue_ToString_m2717362452 (XmlAtomicValue_t4029553395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlTypeCode System.Xml.Schema.XmlAtomicValue::get_ResolvedTypeCode()
extern "C"  int32_t XmlAtomicValue_get_ResolvedTypeCode_m1818765145 (XmlAtomicValue_t4029553395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Schema.XmlAtomicValue::get_Value()
extern "C"  String_t* XmlAtomicValue_get_Value_m3750217346 (XmlAtomicValue_t4029553395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Schema.XmlAtomicValue::get_ValueAsBoolean()
extern "C"  bool XmlAtomicValue_get_ValueAsBoolean_m3431453243 (XmlAtomicValue_t4029553395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Xml.Schema.XmlAtomicValue::get_ValueAsDateTime()
extern "C"  DateTime_t4283661327  XmlAtomicValue_get_ValueAsDateTime_m3751369241 (XmlAtomicValue_t4029553395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Xml.Schema.XmlAtomicValue::get_ValueAsDouble()
extern "C"  double XmlAtomicValue_get_ValueAsDouble_m1238910789 (XmlAtomicValue_t4029553395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.Schema.XmlAtomicValue::get_ValueAsInt()
extern "C"  int32_t XmlAtomicValue_get_ValueAsInt_m2725983388 (XmlAtomicValue_t4029553395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Xml.Schema.XmlAtomicValue::get_ValueAsLong()
extern "C"  int64_t XmlAtomicValue_get_ValueAsLong_m450761072 (XmlAtomicValue_t4029553395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlTypeCode System.Xml.Schema.XmlAtomicValue::XmlTypeCodeFromRuntimeType(System.Type,System.Boolean)
extern "C"  int32_t XmlAtomicValue_XmlTypeCodeFromRuntimeType_m3822362527 (Il2CppObject * __this /* static, unused */, Type_t * ___cliType0, bool ___raiseError1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
