﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3130107104.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Core_RpsChoice52797132.h"

// System.Void System.Array/InternalEnumerator`1<Core.RpsChoice>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m518141880_gshared (InternalEnumerator_1_t3130107104 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m518141880(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3130107104 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m518141880_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Core.RpsChoice>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3482104552_gshared (InternalEnumerator_1_t3130107104 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3482104552(__this, method) ((  void (*) (InternalEnumerator_1_t3130107104 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3482104552_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Core.RpsChoice>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2250149844_gshared (InternalEnumerator_1_t3130107104 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2250149844(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3130107104 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2250149844_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Core.RpsChoice>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1123518351_gshared (InternalEnumerator_1_t3130107104 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1123518351(__this, method) ((  void (*) (InternalEnumerator_1_t3130107104 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1123518351_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Core.RpsChoice>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2971755604_gshared (InternalEnumerator_1_t3130107104 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2971755604(__this, method) ((  bool (*) (InternalEnumerator_1_t3130107104 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2971755604_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Core.RpsChoice>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m265855487_gshared (InternalEnumerator_1_t3130107104 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m265855487(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3130107104 *, const MethodInfo*))InternalEnumerator_1_get_Current_m265855487_gshared)(__this, method)
