﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectMgr/<GetEffectObj>c__AnonStorey155
struct U3CGetEffectObjU3Ec__AnonStorey155_t523641988;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"

// System.Void EffectMgr/<GetEffectObj>c__AnonStorey155::.ctor()
extern "C"  void U3CGetEffectObjU3Ec__AnonStorey155__ctor_m1706233495 (U3CGetEffectObjU3Ec__AnonStorey155_t523641988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectMgr/<GetEffectObj>c__AnonStorey155::<>m__3DA(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  void U3CGetEffectObjU3Ec__AnonStorey155_U3CU3Em__3DA_m954732825 (U3CGetEffectObjU3Ec__AnonStorey155_t523641988 * __this, AssetOperation_t778728221 * ___ao0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
