﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>
struct Comparer_1_t2078752229;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::.ctor()
extern "C"  void Comparer_1__ctor_m2092000675_gshared (Comparer_1_t2078752229 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m2092000675(__this, method) ((  void (*) (Comparer_1_t2078752229 *, const MethodInfo*))Comparer_1__ctor_m2092000675_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::.cctor()
extern "C"  void Comparer_1__cctor_m4240382570_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m4240382570(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m4240382570_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1365309776_gshared (Comparer_1_t2078752229 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m1365309776(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t2078752229 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m1365309776_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::get_Default()
extern "C"  Comparer_1_t2078752229 * Comparer_1_get_Default_m2013430759_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m2013430759(__this /* static, unused */, method) ((  Comparer_1_t2078752229 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m2013430759_gshared)(__this /* static, unused */, method)
