﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpinGenerated
struct SpinGenerated_t1142692685;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void SpinGenerated::.ctor()
extern "C"  void SpinGenerated__ctor_m3139677374 (SpinGenerated_t1142692685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SpinGenerated::Spin_Spin1(JSVCall,System.Int32)
extern "C"  bool SpinGenerated_Spin_Spin1_m4212196276 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpinGenerated::Spin_rotationsPerSecond(JSVCall)
extern "C"  void SpinGenerated_Spin_rotationsPerSecond_m2430901074 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpinGenerated::Spin_ignoreTimeScale(JSVCall)
extern "C"  void SpinGenerated_Spin_ignoreTimeScale_m3607069971 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SpinGenerated::Spin_ApplyDelta__Single(JSVCall,System.Int32)
extern "C"  bool SpinGenerated_Spin_ApplyDelta__Single_m2249849743 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpinGenerated::__Register()
extern "C"  void SpinGenerated___Register_m3995211465 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SpinGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool SpinGenerated_ilo_attachFinalizerObject1_m2271202297 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpinGenerated::ilo_setVector3S2(System.Int32,UnityEngine.Vector3)
extern "C"  void SpinGenerated_ilo_setVector3S2_m1612430986 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SpinGenerated::ilo_getBooleanS3(System.Int32)
extern "C"  bool SpinGenerated_ilo_getBooleanS3_m1802344936 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
