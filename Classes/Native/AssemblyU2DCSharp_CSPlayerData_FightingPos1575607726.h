﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSPlayerData/FightingPos
struct  FightingPos_t1575607726  : public Il2CppObject
{
public:
	// System.UInt32 CSPlayerData/FightingPos::location
	uint32_t ___location_0;
	// System.Int32 CSPlayerData/FightingPos::heroID
	int32_t ___heroID_1;

public:
	inline static int32_t get_offset_of_location_0() { return static_cast<int32_t>(offsetof(FightingPos_t1575607726, ___location_0)); }
	inline uint32_t get_location_0() const { return ___location_0; }
	inline uint32_t* get_address_of_location_0() { return &___location_0; }
	inline void set_location_0(uint32_t value)
	{
		___location_0 = value;
	}

	inline static int32_t get_offset_of_heroID_1() { return static_cast<int32_t>(offsetof(FightingPos_t1575607726, ___heroID_1)); }
	inline int32_t get_heroID_1() const { return ___heroID_1; }
	inline int32_t* get_address_of_heroID_1() { return &___heroID_1; }
	inline void set_heroID_1(int32_t value)
	{
		___heroID_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
