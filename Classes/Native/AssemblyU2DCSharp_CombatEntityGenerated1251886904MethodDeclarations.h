﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CombatEntityGenerated
struct CombatEntityGenerated_t1251886904;
// JSVCall
struct JSVCall_t3708497963;
// OnPathDelegate
struct OnPathDelegate_t598607977;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.Action
struct Action_t3771233898;
// System.Boolean[]
struct BooleanU5BU5D_t3456302923;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// CombatEntity
struct CombatEntity_t684137495;
// TargetMgr/AngleEntity
struct AngleEntity_t3194128142;
// System.String
struct String_t;
// System.Collections.Generic.List`1<AIObject>
struct List_1_t1405465687;
// AnimationRunner
struct AnimationRunner_t1015409588;
// BuffCtrl
struct BuffCtrl_t2836564350;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t3986041494;
// Blood
struct Blood_t64280026;
// FS_ShadowSimple
struct FS_ShadowSimple_t4208748868;
// CEvent.ZEvent
struct ZEvent_t3638018500;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_TargetMgr_AngleEntity3194128142.h"
#include "AssemblyU2DCSharp_EntityEnum_ModelScaleTween3031370284.h"
#include "AssemblyU2DCSharp_EntityEnum_PetrifiedTween965315849.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_SEX_TYPE2342271891.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitType643359572.h"
#include "AssemblyU2DCSharp_HERO_ELEMENT2692579223.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_HeadUpBloodMgr_LabelType4067300664.h"
#include "AssemblyU2DCSharp_FightEnum_EDamageType2535357340.h"
#include "System_Core_System_Action3771233898.h"

// System.Void CombatEntityGenerated::.ctor()
extern "C"  void CombatEntityGenerated__ctor_m3640217907 (CombatEntityGenerated_t1251886904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::.cctor()
extern "C"  void CombatEntityGenerated__cctor_m695509210 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_CombatEntity1(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_CombatEntity1_m2574185407 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_FORCE_SHIFT_OVER(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_FORCE_SHIFT_OVER_m849548553 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_mModel(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_mModel_m1949518514 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_hideList(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_hideList_m387371758 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_skillDic(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_skillDic_m3922008609 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_godEquipAwakenSkillIDs(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_godEquipAwakenSkillIDs_m2193568458 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_godEquipAwakenSkillGradeIDs(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_godEquipAwakenSkillGradeIDs_m346833801 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_NDPNum(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_NDPNum_m1497887650 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isSummoning(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isSummoning_m1401573031 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_effect_leader(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_effect_leader_m1256258183 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isRuneffect(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isRuneffect_m535505036 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_curSuperArmorTime(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_curSuperArmorTime_m3102772589 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_curSuperArmor(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_curSuperArmor_m2118259642 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_runeTime(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_runeTime_m4186992295 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isImmediateActive(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isImmediateActive_m3641847601 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_country(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_country_m839285576 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity__aKill(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity__aKill_m3721621390 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_runEffPS(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_runEffPS_m1625721777 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_atkpoint(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_atkpoint_m1890599158 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_atkpointEntity(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_atkpointEntity_m1339256915 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isFrishSkill(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isFrishSkill_m3361090565 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_lastEntity(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_lastEntity_m3207265269 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isAtkpoint(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isAtkpoint_m2519937772 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_pointNum(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_pointNum_m2288648408 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity__effectid(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity__effectid_m1068605619 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity__effectSpeed(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity__effectSpeed_m3451403863 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_AddDamageSkillIDs(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_AddDamageSkillIDs_m347606983 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_AddDamageValue(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_AddDamageValue_m2958583565 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_AddDamageValuePer(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_AddDamageValuePer_m584664642 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_IsShowRender(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_IsShowRender_m3635671217 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isDoubleClick(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isDoubleClick_m41392657 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_hatredCtrl(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_hatredCtrl_m2597631853 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_attPlus(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_attPlus_m576425123 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_susanooObj(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_susanooObj_m158042773 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_position(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_position_m3897616613 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_angleEntity(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_angleEntity_m3765930056 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_canMove(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_canMove_m1936722813 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_showname(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_showname_m218570214 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_unitCamp(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_unitCamp_m2788030313 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_modelScaleTween(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_modelScaleTween_m3755893268 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_petrifiedTween(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_petrifiedTween_m2184711995 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_playerId(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_playerId_m3066751890 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_id(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_id_m2190258995 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_icon(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_icon_m596145045 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_largeIcon(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_largeIcon_m826567754 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_bossshow(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_bossshow_m4061197124 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_headicon(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_headicon_m1791081909 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_IntelligenceTip(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_IntelligenceTip_m1354113122 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_scale(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_scale_m3896228372 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isSummonMonster(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isSummonMonster_m3131326543 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_halfwidth(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_halfwidth_m1679802379 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_heromajor(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_heromajor_m2729959679 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_atkEffDelayTime(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_atkEffDelayTime_m3107074619 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_skills(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_skills_m1846765548 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_awakenLv(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_awakenLv_m554426971 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_lucencyWait(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_lucencyWait_m3923701488 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_islucency(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_islucency_m1539291023 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_lucencytime(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_lucencytime_m797507768 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_elite(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_elite_m3580407531 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isAim(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isAim_m3945465539 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_dutytype(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_dutytype_m1525139166 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_ifattacked(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_ifattacked_m4250666346 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_dropRwards(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_dropRwards_m2701852762 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_storyId(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_storyId_m3206029102 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_elementEff(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_elementEff_m3724324037 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_atkedEff(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_atkedEff_m1684162688 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_deathEffect(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_deathEffect_m159024793 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_taixudeatheffect(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_taixudeatheffect_m652811682 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_reviveEffectId(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_reviveEffectId_m4070296589 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_appeareffect(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_appeareffect_m1562574664 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_startPos(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_startPos_m3869017500 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_startRotation(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_startRotation_m3551126046 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_atkDis(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_atkDis_m4161283032 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_viewDis(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_viewDis_m2992776341 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_originSpeed(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_originSpeed_m865402493 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_canAttack(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_canAttack_m3495109862 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isStun(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isStun_m794115434 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isDeath(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isDeath_m2743738868 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isRevive(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isRevive_m1977969583 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isFend(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isFend_m36039119 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isKnockingUp(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isKnockingUp_m681636539 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isKnockingDown(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isKnockingDown_m1711085716 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isMovement(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isMovement_m577341973 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isFlame(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isFlame_m4257066005 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isNotSelected(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isNotSelected_m1200218522 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_IsCheckTarget(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_IsCheckTarget_m2565369839 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_IsAutoMove(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_IsAutoMove_m3773612964 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_IsMarshalMoving(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_IsMarshalMoving_m1846791164 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_RvoRadius(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_RvoRadius_m3858573761 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_IsBirthEffect(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_IsBirthEffect_m4011086680 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_starAttackTime(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_starAttackTime_m1713880231 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isAttack(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isAttack_m1283348508 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_canFight(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_canFight_m797838798 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_waistPoint(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_waistPoint_m2839085054 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_InitPosition(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_InitPosition_m1056465301 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_UINamePos(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_UINamePos_m2098695465 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_headBloodPos(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_headBloodPos_m3708247348 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_sexType(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_sexType_m466543966 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_AIDelayList(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_AIDelayList_m851250373 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_initBehavior(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_initBehavior_m1542076076 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_characterAnim(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_characterAnim_m3976689796 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isSkillFrozen(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isSkillFrozen_m959882039 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isFrozen(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isFrozen_m1752929988 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_totalDamage(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_totalDamage_m1403186091 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_atkUnitTag(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_atkUnitTag_m3368881648 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_unitTag(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_unitTag_m2928336744 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_fightCtrl(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_fightCtrl_m3247623139 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_buffCtrl(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_buffCtrl_m375212112 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_bvrCtrl(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_bvrCtrl_m2820559381 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_skinMeshRender(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_skinMeshRender_m4229879470 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_blood(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_blood_m2167948132 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_PlotFightMgr(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_PlotFightMgr_m1681861125 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_shadow(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_shadow_m2796062926 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_unitType(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_unitType_m1963926224 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_Radius(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_Radius_m2189888892 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_atkTarget(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_atkTarget_m2557108213 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_boxCollider(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_boxCollider_m2803984927 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_viewLevel(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_viewLevel_m1844313887 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_viewGrade(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_viewGrade_m1019676716 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_level(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_level_m3582265626 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_atkType(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_atkType_m186992748 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_damageType(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_damageType_m4023069893 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_atkPower(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_atkPower_m2081484097 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_atkPowerP(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_atkPowerP_m1502789339 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_atkPowerL(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_atkPowerL_m2288843359 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_PD(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_PD_m1862867578 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_MD(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_MD_m2958754359 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_defenseP(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_defenseP_m2103466974 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_defenseL(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_defenseL_m2889520994 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_hp(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_hp_m1629048294 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_maxHp(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_maxHp_m4294412306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_PerHP(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_PerHP_m1821099769 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_HPSecDecPer(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_HPSecDecPer_m2338784602 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_HPSecDecNum(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_HPSecDecNum_m81768721 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_HPRstNum(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_HPRstNum_m4090746611 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_HPRstPer(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_HPRstPer_m2052795196 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_starLevel(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_starLevel_m1341413388 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_hits(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_hits_m286654638 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_dodge(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_dodge_m150907431 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_wreck(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_wreck_m97858956 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_withstand(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_withstand_m2161373006 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_crit(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_crit_m1010529108 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_tenacity(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_tenacity_m469385599 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_hitsP(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_hitsP_m1697650958 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_dodgeP(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_dodgeP_m1784454837 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_wreckP(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_wreckP_m139952112 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_withstandP(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_withstandP_m3979345518 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_critP(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_critP_m2662923048 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_tenacityP(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_tenacityP_m3067343453 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_critHurt(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_critHurt_m2089885957 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_rHurt(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_rHurt_m1175911837 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_cureP(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_cureP_m4128925107 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_hurtP(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_hurtP_m416665373 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_hurtL(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_hurtL_m1202719393 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_finalHurt(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_finalHurt_m3140351769 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_atkSpeed(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_atkSpeed_m699105343 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_runSpeed(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_runSpeed_m1947687666 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_atkRange(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_atkRange_m2977037321 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_maxRage(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_maxRage_m2570990189 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_findEnemyRange(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_findEnemyRange_m3068468352 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_rage(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_rage_m956683073 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_rageGrowth(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_rageGrowth_m1461469434 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_element(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_element_m1322100386 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_stable(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_stable_m1104323859 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_notInvade(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_notInvade_m824698074 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_unyielding(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_unyielding_m222477024 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_modelScale(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_modelScale_m1748344685 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_beforModelScale(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_beforModelScale_m2555452439 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_modelScaleBetweenValue(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_modelScaleBetweenValue_m2391626916 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_curModelScaleValue(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_curModelScaleValue_m3963999230 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_vampire(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_vampire_m1771237840 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_tauntDistance(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_tauntDistance_m3844552187 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_angerSecDecNum(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_angerSecDecNum_m3867293216 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_antiHurt(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_antiHurt_m2094781565 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_disMove(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_disMove_m2368875391 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_disUseskill(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_disUseskill_m333288774 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_disNormalattack(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_disNormalattack_m911146273 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_dizzy(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_dizzy_m1038537738 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_antiControl(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_antiControl_m2811163875 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_antiNum(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_antiNum_m2977475034 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isPauseUpdate(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isPauseUpdate_m1562288361 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_curAntiNum(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_curAntiNum_m1615237130 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_inUseAntiNum(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_inUseAntiNum_m2128773612 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_shield(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_shield_m1112597669 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_curShieldNum(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_curShieldNum_m2472445841 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_antiMove(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_antiMove_m4203101947 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_antiBuff(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_antiBuff_m2400001209 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_antiBreak(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_antiBreak_m2921936385 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_forceShift(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_forceShift_m2999005047 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_hidePart(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_hidePart_m4273016633 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isHide(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isHide_m558694882 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_countryRestraint(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_countryRestraint_m964965272 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_restraintCountryType(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_restraintCountryType_m465108010 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_restraintCountryPer(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_restraintCountryPer_m2296874155 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_stateRestraint(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_stateRestraint_m1321923027 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_restraintStateType(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_restraintStateType_m3279475727 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_restraintStatePer(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_restraintStatePer_m1972018214 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_NearDeathProtect(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_NearDeathProtect_m2081520939 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_NDPAddBuff(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_NDPAddBuff_m4178815316 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_NDPAddBuff02(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_NDPAddBuff02_m912099858 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_BeAttackedLater(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_BeAttackedLater_m3537967644 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_BeAttackedLaterSkillID(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_BeAttackedLaterSkillID_m3555696292 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_BeAttackedLaterEffectID(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_BeAttackedLaterEffectID_m1325775632 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_DeathLater(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_DeathLater_m2246864982 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_DeathLaterEffectID(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_DeathLaterEffectID_m1541158730 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_DeathLaterHeroID(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_DeathLaterHeroID_m2073174337 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_BeattackedLaterSpeed(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_BeattackedLaterSpeed_m4190106249 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_DeathLaterSpeed(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_DeathLaterSpeed_m1865504879 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_AntiEndEffectID(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_AntiEndEffectID_m3183257657 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_AuraAddHP(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_AuraAddHP_m2076494136 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_AuraAddHPRadius(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_AuraAddHPRadius_m3113880390 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_AuraAddHPNum(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_AuraAddHPNum_m906353646 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_AuraAddHPPer(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_AuraAddHPPer_m3163369527 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_AuraReduceHP(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_AuraReduceHP_m3003515773 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_AuraReduceHPRadius(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_AuraReduceHPRadius_m664124875 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_AuraReduceHPNum(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_AuraReduceHPNum_m1168228233 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_AuraReduceHPPer(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_AuraReduceHPPer_m3425244114 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_ActiveSkillDamageAddPer(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_ActiveSkillDamageAddPer_m1946825576 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_AddAngerRatio(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_AddAngerRatio_m2016828953 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_beHurtAnger(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_beHurtAnger_m3342904713 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_disUseActionSkill(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_disUseActionSkill_m3405137020 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isBerserker(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isBerserker_m4162672001 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_superArmor(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_superArmor_m1401677802 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_superArmorTime(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_superArmorTime_m1876216221 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isHpLineState(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isHpLineState_m1760806707 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_hpLineNum(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_hpLineNum_m4288792212 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_hpLineBuffID(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_hpLineBuffID_m2374832708 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_hpLineUpBuffID(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_hpLineUpBuffID_m2180355881 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_hpLineCleaveAddBuff(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_hpLineCleaveAddBuff_m119010458 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_controlBuffTimeBonus(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_controlBuffTimeBonus_m3922586188 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_controlBuffTimeBonusPer(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_controlBuffTimeBonusPer_m3035465379 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_runeNum(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_runeNum_m468285874 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_runeNumAllSkill(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_runeNumAllSkill_m1004299874 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_runeState(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_runeState_m1356851335 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_runeStateAllSkill(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_runeStateAllSkill_m3748999223 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_runeDisappearTime(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_runeDisappearTime_m2244660456 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_runeTakeEffectNum(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_runeTakeEffectNum_m2934713418 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_runeTakeEffectBuffID(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_runeTakeEffectBuffID_m1346324686 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_runeTakeEffectBuffID01(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_runeTakeEffectBuffID01_m2094383597 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_runeTakeEffectBuffID02(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_runeTakeEffectBuffID02_m1897870092 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_godDownState(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_godDownState_m2079748443 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_godDownTweenTime(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_godDownTweenTime_m3453158132 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_antiDebuffDiscrete(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_antiDebuffDiscrete_m1274197855 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_antiDebuffDiscreteID(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_antiDebuffDiscreteID_m87075428 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_antiDebuffDiscreteAddBuffID(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_antiDebuffDiscreteAddBuffID_m1941209118 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_discreteBuffID(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_discreteBuffID_m1929402567 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_discreteBuffTime(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_discreteBuffTime_m1766488117 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_discreteBuffTimer(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_discreteBuffTimer_m2236323973 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isPetrified(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isPetrified_m256598704 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_petrifiedChangeTime(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_petrifiedChangeTime_m492045257 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isChangeSkill(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isChangeSkill_m3750874375 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_changeOldSkillID(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_changeOldSkillID_m3270812857 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_changeNewSkillID(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_changeNewSkillID_m2447937074 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_changeSkillPro(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_changeSkillPro_m1326777602 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isWhenCritAddBuff(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isWhenCritAddBuff_m3458697640 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_whenCritAddBuffID(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_whenCritAddBuffID_m4272630691 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isThumpState(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isThumpState_m3314336759 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_thumpSkillID(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_thumpSkillID_m1112001606 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_thumpAddValue(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_thumpAddValue_m2959441738 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_tankBuffID(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_tankBuffID_m313290422 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_HTBuffID(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_HTBuffID_m2525727828 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_cleaveSkillID(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_cleaveSkillID_m834508486 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_cleaveBuffID(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_cleaveBuffID_m644634668 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isReduceRageHp(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isReduceRageHp_m1278326537 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_reduceRageNum(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_reduceRageNum_m1896514763 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isDeathHarvest(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isDeathHarvest_m2229354543 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_deathHarvestLine(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_deathHarvestLine_m2173852197 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_deathHarvestPer(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_deathHarvestPer_m841495702 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isResistanceCountry(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isResistanceCountry_m1064471147 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_resistanceCountryType(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_resistanceCountryType_m4040180743 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_resistanceCountryPer(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_resistanceCountryPer_m2550746414 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isTimeBomb(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isTimeBomb_m1607230389 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isPlagueDust(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isPlagueDust_m1242831840 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isInfection(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isInfection_m3136127605 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::CombatEntity_isFierceWind(JSVCall)
extern "C"  void CombatEntityGenerated_CombatEntity_isFierceWind_m3230422890 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_AddBuffOnShiliFuben(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_AddBuffOnShiliFuben_m2290032451 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_AddDelayId__UInt32(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_AddDelayId__UInt32_m674840115 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_AddHP__Single(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_AddHP__Single_m1722014230 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_Addhurt__Single(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_Addhurt__Single_m390593949 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_AddPetBuff(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_AddPetBuff_m2306324974 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_AddPowerBuff__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_AddPowerBuff__Int32__Int32_m3265940116 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_AddRage__Single(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_AddRage__Single_m3992713339 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_AddStateT1__Object_Array(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_AddStateT1__Object_Array_m667042371 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_AddState__UnitStateBase(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_AddState__UnitStateBase_m2176872467 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_AGAINATTACK(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_AGAINATTACK_m1878784205 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_AIBeAttacked(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_AIBeAttacked_m3699037807 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_AIMonsterDead__Int32(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_AIMonsterDead__Int32_m670247189 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_AIOtherDead__ZEvent(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_AIOtherDead__ZEvent_m1731281 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_Answer__ZEvent(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_Answer__ZEvent_m1237575163 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_ATKDEBUFF(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_ATKDEBUFF_m4007242257 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_BeAttacked__Int32__CombatEntity__EDamageType__Int32(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_BeAttacked__Int32__CombatEntity__EDamageType__Int32_m1634674336 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_BreakSuperArmor(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_BreakSuperArmor_m4279210952 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_CalcuFinalProp(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_CalcuFinalProp_m1785967830 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_ChangeShader(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_ChangeShader_m1875583346 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_Clear(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_Clear_m1799229106 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_ClosePlot__ZEvent(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_ClosePlot__ZEvent_m2573355422 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_CloseTaunt(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_CloseTaunt_m3966233171 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_DelState__UnitStateBase(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_DelState__UnitStateBase_m1050318589 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_EnterCleave(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_EnterCleave_m3611311441 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_EnterGhost__Int32(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_EnterGhost__Int32_m501191420 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_EnterGodDown(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_EnterGodDown_m3857740867 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_EnterPartBloom__Int32(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_EnterPartBloom__Int32_m681777499 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_EnterPetrified(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_EnterPetrified_m551236029 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_EnterSusanoo__Int32(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_EnterSusanoo__Int32_m550799437 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_EnterTankState(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_EnterTankState_m4259758348 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_ExitCleave(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_ExitCleave_m3146778607 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_ExitGhost(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_ExitGhost_m542050166 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_ExitGodDown(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_ExitGodDown_m2342124901 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_ExitPartBloom(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_ExitPartBloom_m1799666935 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_ExitPetrified(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_ExitPetrified_m38206047 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_ExitSusanoo(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_ExitSusanoo_m2542934725 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_ExitTankState(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_ExitTankState_m3746728366 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_FendMove(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_FendMove_m1622088995 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_FocusAttack__CombatEntity(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_FocusAttack__CombatEntity_m3236032540 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_GetActiveSkill(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_GetActiveSkill_m467720306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_getAtkpos__CombatEntity(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_getAtkpos__CombatEntity_m3958352558 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_getposid__Int32(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_getposid__Int32_m1902059802 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_LookAt__Vector3(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_LookAt__Vector3_m2487994627 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_LookAtTarget__Vector3(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_LookAtTarget__Vector3_m4011553714 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_lucency(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_lucency_m3165236318 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_MoceCameraEnd__ZEvent(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_MoceCameraEnd__ZEvent_m2026992951 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_OnAttckTime(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_OnAttckTime_m4209065980 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_OnCleave__ZEvent(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_OnCleave__ZEvent_m1464703856 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_OnDelayTime(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_OnDelayTime_m634077750 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_OnDestroy(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_OnDestroy_m3581812992 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_OneSecAddHP(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_OneSecAddHP_m146316995 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_OneSecAuraAddHP(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_OneSecAuraAddHP_m1185724128 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_OneSecAuraReduceHP(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_OneSecAuraReduceHP_m371609753 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_OneSecReduceHP(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_OneSecReduceHP_m2107914710 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_OnLevelTime(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_OnLevelTime_m1146533111 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_OnTargetReached(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_OnTargetReached_m1764256903 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_OnTriggerFun__Int32(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_OnTriggerFun__Int32_m1831701261 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_OnUseingSkill__Int32(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_OnUseingSkill__Int32_m1954672692 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_OpenTaunt(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_OpenTaunt_m2871154281 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_PlayHpTextEffect__String__LabelType__Int32(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_PlayHpTextEffect__String__LabelType__Int32_m3423225256 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_PlayHpTextEffect__Single__LabelType(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_PlayHpTextEffect__Single__LabelType_m2011943761 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_ReduceHP__Single__CombatEntity__EDamageType__Boolean__Boolean__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_ReduceHP__Single__CombatEntity__EDamageType__Boolean__Boolean__Int32__Int32_m2992194982 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_RefreshShader(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_RefreshShader_m112581317 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_RefreshShaderProperty__String__Object(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_RefreshShaderProperty__String__Object_m1302464330 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_RemoveAllStates(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_RemoveAllStates_m3985844420 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnPathDelegate CombatEntityGenerated::CombatEntity_RemovePathCallback_GetDelegate_member65_arg0(CSRepresentedObject)
extern "C"  OnPathDelegate_t598607977 * CombatEntityGenerated_CombatEntity_RemovePathCallback_GetDelegate_member65_arg0_m612364964 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_RemovePathCallback__OnPathDelegate(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_RemovePathCallback__OnPathDelegate_m768426420 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_ReplayDead(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_ReplayDead_m3523074344 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnPathDelegate CombatEntityGenerated::CombatEntity_RequestAstarPath_GetDelegate_member67_arg1(CSRepresentedObject)
extern "C"  OnPathDelegate_t598607977 * CombatEntityGenerated_CombatEntity_RequestAstarPath_GetDelegate_member67_arg1_m1054228842 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_RequestAstarPath__Vector3__OnPathDelegate(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_RequestAstarPath__Vector3__OnPathDelegate_m3387783861 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_Revive(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_Revive_m3462426642 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_SetAttr__String__Single(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_SetAttr__String__Single_m4144654577 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_SetDistortion__Boolean__Single(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_SetDistortion__Boolean__Single_m3106572654 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_SetHidePart__Int32(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_SetHidePart__Int32_m1004299028 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_SetModelScale(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_SetModelScale_m1389819272 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_SetNotSelected__Boolean(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_SetNotSelected__Boolean_m645537537 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_SetRunSpeed__Single(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_SetRunSpeed__Single_m4041145835 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_SetViewModel__Boolean(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_SetViewModel__Boolean_m2816629963 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_ShaderBuffDead(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_ShaderBuffDead_m3301982681 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_ShaderBuffDestory(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_ShaderBuffDestory_m512077121 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_ShaderBuffInit(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_ShaderBuffInit_m4142375301 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_ShootEffect(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_ShootEffect_m3118345141 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_Start(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_Start_m1322659591 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_StopMove__Boolean(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_StopMove__Boolean_m1508674298 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_Suicide(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_Suicide_m2738892499 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_TankState__ZEvent(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_TankState__ZEvent_m1921444844 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_TargetBuffCheck__Int32(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_TargetBuffCheck__Int32_m81855783 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_Update(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_Update_m3699499142 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_UpdateGodDown(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_UpdateGodDown_m721603738 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_UpdatePetrified(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_UpdatePetrified_m1790496852 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_UpdateScale(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_UpdateScale_m1301491814 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action CombatEntityGenerated::CombatEntity_UpdateTargetPos_GetDelegate_member89_arg1(CSRepresentedObject)
extern "C"  Action_t3771233898 * CombatEntityGenerated_CombatEntity_UpdateTargetPos_GetDelegate_member89_arg1_m3110755899 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_UpdateTargetPos__Vector3__Action(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_UpdateTargetPos__Vector3__Action_m221698313 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_UpdateYPos(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_UpdateYPos_m1460788993 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::CombatEntity_WinDead(JSVCall,System.Int32)
extern "C"  bool CombatEntityGenerated_CombatEntity_WinDead_m2499817861 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::__Register()
extern "C"  void CombatEntityGenerated___Register_m3487167348 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean[] CombatEntityGenerated::<CombatEntity_isAtkpoint>m__32()
extern "C"  BooleanU5BU5D_t3456302923* CombatEntityGenerated_U3CCombatEntity_isAtkpointU3Em__32_m3559968923 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] CombatEntityGenerated::<CombatEntity_AddStateT1__Object_Array>m__33()
extern "C"  ObjectU5BU5D_t1108656482* CombatEntityGenerated_U3CCombatEntity_AddStateT1__Object_ArrayU3Em__33_m3255579103 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnPathDelegate CombatEntityGenerated::<CombatEntity_RemovePathCallback__OnPathDelegate>m__35()
extern "C"  OnPathDelegate_t598607977 * CombatEntityGenerated_U3CCombatEntity_RemovePathCallback__OnPathDelegateU3Em__35_m4267625681 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnPathDelegate CombatEntityGenerated::<CombatEntity_RequestAstarPath__Vector3__OnPathDelegate>m__37()
extern "C"  OnPathDelegate_t598607977 * CombatEntityGenerated_U3CCombatEntity_RequestAstarPath__Vector3__OnPathDelegateU3Em__37_m1845819548 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action CombatEntityGenerated::<CombatEntity_UpdateTargetPos__Vector3__Action>m__39()
extern "C"  Action_t3771233898 * CombatEntityGenerated_U3CCombatEntity_UpdateTargetPos__Vector3__ActionU3Em__39_m3625623616 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void CombatEntityGenerated_ilo_addJSCSRel1_m3798174319 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CombatEntityGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * CombatEntityGenerated_ilo_getObject2_m2584522003 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t CombatEntityGenerated_ilo_setObject3_m3727068509 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_setInt324(System.Int32,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_setInt324_m850103072 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_getInt325(System.Int32)
extern "C"  int32_t CombatEntityGenerated_ilo_getInt325_m3165621466 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::ilo_getBooleanS6(System.Int32)
extern "C"  bool CombatEntityGenerated_ilo_getBooleanS6_m3323688086 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_moveSaveID2Arr7(System.Int32)
extern "C"  void CombatEntityGenerated_ilo_moveSaveID2Arr7_m2441368334 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_setArrayS8(System.Int32,System.Int32,System.Boolean)
extern "C"  void CombatEntityGenerated_ilo_setArrayS8_m3475685239 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CombatEntityGenerated::ilo_get_position9(CombatEntity)
extern "C"  Vector3_t4282066566  CombatEntityGenerated_ilo_get_position9_m400935784 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_angleEntity10(CombatEntity,TargetMgr/AngleEntity)
extern "C"  void CombatEntityGenerated_ilo_set_angleEntity10_m829168157 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, AngleEntity_t3194128142 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CombatEntityGenerated::ilo_get_showname11(CombatEntity)
extern "C"  String_t* CombatEntityGenerated_ilo_get_showname11_m4087654523 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_setEnum12(System.Int32,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_setEnum12_m1818382884 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_modelScaleTween13(CombatEntity,EntityEnum.ModelScaleTween)
extern "C"  void CombatEntityGenerated_ilo_set_modelScaleTween13_m2930685808 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_petrifiedTween14(CombatEntity,EntityEnum.PetrifiedTween)
extern "C"  void CombatEntityGenerated_ilo_set_petrifiedTween14_m359895695 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_setUInt3215(System.Int32,System.UInt32)
extern "C"  void CombatEntityGenerated_ilo_setUInt3215_m4292931682 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_id16(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_id16_m3209971208 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CombatEntityGenerated::ilo_getStringS17(System.Int32)
extern "C"  String_t* CombatEntityGenerated_ilo_getStringS17_m83092836 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_icon18(CombatEntity,System.String)
extern "C"  void CombatEntityGenerated_ilo_set_icon18_m1599467622 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_setStringS19(System.Int32,System.String)
extern "C"  void CombatEntityGenerated_ilo_setStringS19_m1099127577 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_scale20(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_scale20_m18100407 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_halfwidth21(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_halfwidth21_m262288508 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_getSingle22(System.Int32)
extern "C"  float CombatEntityGenerated_ilo_getSingle22_m1674815417 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_atkEffDelayTime23(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_atkEffDelayTime23_m3755611763 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_islucency24(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_islucency24_m1486820989 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_islucency25(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_islucency25_m3357235099 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_setSingle26(System.Int32,System.Single)
extern "C"  void CombatEntityGenerated_ilo_setSingle26_m40512034 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_lucencytime27(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_lucencytime27_m1389987354 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_setBooleanS28(System.Int32,System.Boolean)
extern "C"  void CombatEntityGenerated_ilo_setBooleanS28_m396060059 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CombatEntityGenerated::ilo_get_storyId29(CombatEntity)
extern "C"  String_t* CombatEntityGenerated_ilo_get_storyId29_m3548386840 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_elementEff30(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_elementEff30_m4269180071 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_taixudeatheffect31(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_taixudeatheffect31_m321207986 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_setVector3S32(System.Int32,UnityEngine.Vector3)
extern "C"  void CombatEntityGenerated_ilo_setVector3S32_m512241126 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_startPos33(CombatEntity,UnityEngine.Vector3)
extern "C"  void CombatEntityGenerated_ilo_set_startPos33_m4104897999 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, Vector3_t4282066566  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_atkDis34(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_atkDis34_m480110081 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_atkDis35(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_atkDis35_m945293927 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_viewDis36(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_viewDis36_m2449426390 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_originSpeed37(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_originSpeed37_m2661955375 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_originSpeed38(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_originSpeed38_m2885866901 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_isDeath39(CombatEntity,System.Boolean)
extern "C"  void CombatEntityGenerated_ilo_set_isDeath39_m746249891 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::ilo_get_isRevive40(CombatEntity)
extern "C"  bool CombatEntityGenerated_ilo_get_isRevive40_m2171232829 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::ilo_get_isKnockingUp41(CombatEntity)
extern "C"  bool CombatEntityGenerated_ilo_get_isKnockingUp41_m466280434 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::ilo_get_isKnockingDown42(CombatEntity)
extern "C"  bool CombatEntityGenerated_ilo_get_isKnockingDown42_m3483876922 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_isMovement43(CombatEntity,System.Boolean)
extern "C"  void CombatEntityGenerated_ilo_set_isMovement43_m2525025211 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_isNotSelected44(CombatEntity,System.Boolean)
extern "C"  void CombatEntityGenerated_ilo_set_isNotSelected44_m462511599 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::ilo_get_IsAutoMove45(CombatEntity)
extern "C"  bool CombatEntityGenerated_ilo_get_IsAutoMove45_m413556717 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_IsMarshalMoving46(CombatEntity,System.Boolean)
extern "C"  void CombatEntityGenerated_ilo_set_IsMarshalMoving46_m2596745231 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_isAttack47(CombatEntity,System.Boolean)
extern "C"  void CombatEntityGenerated_ilo_set_isAttack47_m2982029054 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_canFight48(CombatEntity,System.Boolean)
extern "C"  void CombatEntityGenerated_ilo_set_canFight48_m4161885903 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_headBloodPos49(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_headBloodPos49_m505667657 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SEX_TYPE CombatEntityGenerated::ilo_get_sexType50(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_sexType50_m4229008739 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_sexType51(CombatEntity,SEX_TYPE)
extern "C"  void CombatEntityGenerated_ilo_set_sexType51_m1040432237 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<AIObject> CombatEntityGenerated::ilo_get_AIDelayList52(CombatEntity)
extern "C"  List_1_t1405465687 * CombatEntityGenerated_ilo_get_AIDelayList52_m798397870 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_AIDelayList53(CombatEntity,System.Collections.Generic.List`1<AIObject>)
extern "C"  void CombatEntityGenerated_ilo_set_AIDelayList53_m1678238228 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, List_1_t1405465687 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_initBehavior54(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_initBehavior54_m3777496041 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnimationRunner CombatEntityGenerated::ilo_get_characterAnim55(CombatEntity)
extern "C"  AnimationRunner_t1015409588 * CombatEntityGenerated_ilo_get_characterAnim55_m2208855575 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::ilo_get_isSkillFrozen56(CombatEntity)
extern "C"  bool CombatEntityGenerated_ilo_get_isSkillFrozen56_m1615326874 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_isSkillFrozen57(CombatEntity,System.Boolean)
extern "C"  void CombatEntityGenerated_ilo_set_isSkillFrozen57_m1075914474 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_isFrozen58(CombatEntity,System.Boolean)
extern "C"  void CombatEntityGenerated_ilo_set_isFrozen58_m3409636230 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_totalDamage59(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_totalDamage59_m713805030 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BuffCtrl CombatEntityGenerated::ilo_get_buffCtrl60(CombatEntity)
extern "C"  BuffCtrl_t2836564350 * CombatEntityGenerated_ilo_get_buffCtrl60_m227564943 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl CombatEntityGenerated::ilo_get_bvrCtrl61(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * CombatEntityGenerated_ilo_get_bvrCtrl61_m1538701632 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SkinnedMeshRenderer CombatEntityGenerated::ilo_get_skinMeshRender62(CombatEntity)
extern "C"  SkinnedMeshRenderer_t3986041494 * CombatEntityGenerated_ilo_get_skinMeshRender62_m3220718120 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Blood CombatEntityGenerated::ilo_get_blood63(CombatEntity)
extern "C"  Blood_t64280026 * CombatEntityGenerated_ilo_get_blood63_m3350092526 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FS_ShadowSimple CombatEntityGenerated::ilo_get_shadow64(CombatEntity)
extern "C"  FS_ShadowSimple_t4208748868 * CombatEntityGenerated_ilo_get_shadow64_m704875845 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_unitType65(CombatEntity,EntityEnum.UnitType)
extern "C"  void CombatEntityGenerated_ilo_set_unitType65_m1981047499 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity CombatEntityGenerated::ilo_get_atkTarget66(CombatEntity)
extern "C"  CombatEntity_t684137495 * CombatEntityGenerated_ilo_get_atkTarget66_m4188994125 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_atkTarget67(CombatEntity,CombatEntity)
extern "C"  void CombatEntityGenerated_ilo_set_atkTarget67_m2672302267 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, CombatEntity_t684137495 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_viewLevel68(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_viewLevel68_m3867233645 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_level69(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_level69_m141929043 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_atkType70(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_atkType70_m1305985954 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_atkPower71(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_atkPower71_m3769578289 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_atkPowerP72(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_atkPowerP72_m153004872 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_atkPowerL73(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_atkPowerL73_m542285189 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_atkPowerL74(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_atkPowerL74_m1439743339 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_MD75(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_MD75_m258221183 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_defenseP76(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_defenseP76_m3492388153 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_defenseL77(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_defenseL77_m3881668470 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_defenseL78(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_defenseL78_m381408028 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_HPSecDecNum79(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_HPSecDecNum79_m1510857342 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_HPRstNum80(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_HPRstNum80_m1053544669 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_HPRstPer81(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_HPRstPer81_m2485352346 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_hits82(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_hits82_m547124713 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_dodge83(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_dodge83_m1690183324 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_wreck84(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_wreck84_m70736344 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_withstand85(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_withstand85_m4024439804 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_crit86(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_crit86_m3477767906 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_tenacity87(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_tenacity87_m1084135576 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_wreckP88(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_wreckP88_m1576156488 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_withstandP89(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_withstandP89_m3972268464 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_tenacityP90(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_tenacityP90_m520295783 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_hurtP91(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_hurtP91_m3482495555 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_hurtL92(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_hurtL92_m3413255333 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_atkSpeed93(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_atkSpeed93_m2242999987 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_atkSpeed94(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_atkSpeed94_m234343321 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_atkRange95(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_atkRange95_m2938034219 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_maxRage96(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_maxRage96_m3801514077 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_rageGrowth97(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_rageGrowth97_m3615083930 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_rageGrowth98(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_rageGrowth98_m974553690 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HERO_ELEMENT CombatEntityGenerated::ilo_get_element99(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_element99_m1372252256 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_notInvade100(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_notInvade100_m1064884341 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_modelScaleBetweenValue101(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_modelScaleBetweenValue101_m1691273266 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_modelScaleBetweenValue102(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_modelScaleBetweenValue102_m1143866712 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_curModelScaleValue103(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_curModelScaleValue103_m3310636558 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_vampire104(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_vampire104_m2868534614 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_tauntDistance105(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_tauntDistance105_m2829231837 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_angerSecDecNum106(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_angerSecDecNum106_m3394023812 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::ilo_get_isPauseUpdate107(CombatEntity)
extern "C"  bool CombatEntityGenerated_ilo_get_isPauseUpdate107_m2382499685 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_curAntiNum108(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_curAntiNum108_m3459266593 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::ilo_get_antiBuff109(CombatEntity)
extern "C"  bool CombatEntityGenerated_ilo_get_antiBuff109_m4133278007 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::ilo_get_antiBreak110(CombatEntity)
extern "C"  bool CombatEntityGenerated_ilo_get_antiBreak110_m2262016661 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::ilo_get_forceShift111(CombatEntity)
extern "C"  bool CombatEntityGenerated_ilo_get_forceShift111_m1619763660 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_hidePart112(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_hidePart112_m1776109328 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::ilo_get_isHide113(CombatEntity)
extern "C"  bool CombatEntityGenerated_ilo_get_isHide113_m1349193145 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::ilo_get_countryRestraint114(CombatEntity)
extern "C"  bool CombatEntityGenerated_ilo_get_countryRestraint114_m632988592 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_restraintCountryType115(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_restraintCountryType115_m3064064733 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_restraintCountryType116(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_restraintCountryType116_m2866187323 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_restraintCountryPer117(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_restraintCountryPer117_m2278046190 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::ilo_get_stateRestraint118(CombatEntity)
extern "C"  bool CombatEntityGenerated_ilo_get_stateRestraint118_m1636225007 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_restraintStateType119(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_restraintStateType119_m3429348755 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_restraintStatePer120(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_restraintStatePer120_m1975191169 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_restraintStatePer121(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_restraintStatePer121_m3726125799 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_NDPAddBuff122(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_NDPAddBuff122_m569337430 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_BeAttackedLaterSkillID123(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_BeAttackedLaterSkillID123_m588389940 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_BeAttackedLaterEffectID124(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_BeAttackedLaterEffectID124_m1603194888 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_DeathLaterEffectID125(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_DeathLaterEffectID125_m2112748605 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_DeathLaterHeroID126(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_DeathLaterHeroID126_m579530260 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_DeathLaterHeroID127(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_DeathLaterHeroID127_m3097291620 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_AuraAddHPRadius128(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_AuraAddHPRadius128_m4105147150 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_AuraAddHPNum129(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_AuraAddHPNum129_m1094584196 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_AuraAddHPNum130(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_AuraAddHPNum130_m242545599 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_AuraAddHPPer131(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_AuraAddHPPer131_m527142564 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_AuraReduceHPRadius132(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_AuraReduceHPRadius132_m1170929401 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_AuraReduceHPRadius133(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_AuraReduceHPRadius133_m2157254175 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_AuraReduceHPNum134(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_AuraReduceHPNum134_m2773220784 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_beHurtAnger135(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_beHurtAnger135_m3181159183 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::ilo_get_disUseActionSkill136(CombatEntity)
extern "C"  bool CombatEntityGenerated_ilo_get_disUseActionSkill136_m3964469076 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_superArmor137(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_superArmor137_m2506771164 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_superArmorTime138(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_superArmorTime138_m3012169103 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_hpLineNum139(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_hpLineNum139_m833009173 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_hpLineNum140(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_hpLineNum140_m215487626 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_hpLineBuffID141(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_hpLineBuffID141_m1416950160 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_hpLineUpBuffID142(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_hpLineUpBuffID142_m589909302 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_hpLineUpBuffID143(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_hpLineUpBuffID143_m502266882 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_hpLineCleaveAddBuff144(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_hpLineCleaveAddBuff144_m2126352693 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_controlBuffTimeBonus145(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_controlBuffTimeBonus145_m3954486362 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_runeNumAllSkill146(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_runeNumAllSkill146_m2158115766 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::ilo_get_runeState147(CombatEntity)
extern "C"  bool CombatEntityGenerated_ilo_get_runeState147_m1581248639 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_runeTakeEffectNum148(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_runeTakeEffectNum148_m979956585 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_runeTakeEffectNum149(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_runeTakeEffectNum149_m1247749867 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_runeTakeEffectBuffID150(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_runeTakeEffectBuffID150_m2273096673 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_runeTakeEffectBuffID01151(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_runeTakeEffectBuffID01151_m430419352 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_runeTakeEffectBuffID02152(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_runeTakeEffectBuffID02152_m2020946209 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_godDownTweenTime153(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_godDownTweenTime153_m4084650591 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_godDownTweenTime154(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_godDownTweenTime154_m2213620933 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::ilo_get_antiDebuffDiscrete155(CombatEntity)
extern "C"  bool CombatEntityGenerated_ilo_get_antiDebuffDiscrete155_m2628198964 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_antiDebuffDiscreteID156(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_antiDebuffDiscreteID156_m875536596 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_changeSkillPro157(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_changeSkillPro157_m3580412723 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_whenCritAddBuffID158(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_whenCritAddBuffID158_m256001620 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_thumpAddValue159(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_thumpAddValue159_m1777151883 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_tankBuffID160(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_tankBuffID160_m2730669434 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_HTBuffID161(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_HTBuffID161_m3978474590 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_HTBuffID162(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_HTBuffID162_m1762463578 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::ilo_get_isReduceRageHp163(CombatEntity)
extern "C"  bool CombatEntityGenerated_ilo_get_isReduceRageHp163_m794313019 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::ilo_get_isDeathHarvest164(CombatEntity)
extern "C"  bool CombatEntityGenerated_ilo_get_isDeathHarvest164_m1148366498 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_deathHarvestLine165(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_deathHarvestLine165_m1513230387 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_deathHarvestPer166(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_set_deathHarvestPer166_m959241988 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_get_resistanceCountryType167(CombatEntity)
extern "C"  int32_t CombatEntityGenerated_ilo_get_resistanceCountryType167_m1934773411 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CombatEntityGenerated::ilo_get_resistanceCountryPer168(CombatEntity)
extern "C"  float CombatEntityGenerated_ilo_get_resistanceCountryPer168_m378406845 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_set_resistanceCountryPer169(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_set_resistanceCountryPer169_m1914176931 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::ilo_get_isTimeBomb170(CombatEntity)
extern "C"  bool CombatEntityGenerated_ilo_get_isTimeBomb170_m2437251331 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::ilo_get_isFierceWind171(CombatEntity)
extern "C"  bool CombatEntityGenerated_ilo_get_isFierceWind171_m2551720441 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_AddBuffOnShiliFuben172(CombatEntity)
extern "C"  void CombatEntityGenerated_ilo_AddBuffOnShiliFuben172_m1749249563 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_AddDelayId173(CombatEntity,System.UInt32)
extern "C"  void CombatEntityGenerated_ilo_AddDelayId173_m3323659403 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, uint32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_AddHP174(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_AddHP174_m570795479 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___delta1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_AddPetBuff175(CombatEntity)
extern "C"  void CombatEntityGenerated_ilo_AddPetBuff175_m1555430789 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_AddRage176(CombatEntity,System.Single)
extern "C"  void CombatEntityGenerated_ilo_AddRage176_m738884436 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___delta1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_AIMonsterDead177(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_AIMonsterDead177_m1376806095 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___murderID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_CalcuFinalProp178(CombatEntity)
extern "C"  void CombatEntityGenerated_ilo_CalcuFinalProp178_m3679769568 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_Clear179(CombatEntity)
extern "C"  void CombatEntityGenerated_ilo_Clear179_m304146771 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_EnterGhost180(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_EnterGhost180_m2292669694 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_EnterPetrified181(CombatEntity)
extern "C"  void CombatEntityGenerated_ilo_EnterPetrified181_m4011890865 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_EnterSusanoo182(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_EnterSusanoo182_m3310575307 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_ExitCleave183(CombatEntity)
extern "C"  void CombatEntityGenerated_ilo_ExitCleave183_m1099840577 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_ExitPetrified184(CombatEntity)
extern "C"  void CombatEntityGenerated_ilo_ExitPetrified184_m1296779552 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_ExitTankState185(CombatEntity)
extern "C"  void CombatEntityGenerated_ilo_ExitTankState185_m1673825970 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CombatEntityGenerated::ilo_getAtkpos186(CombatEntity,CombatEntity)
extern "C"  Vector3_t4282066566  CombatEntityGenerated_ilo_getAtkpos186_m1417451263 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, CombatEntity_t684137495 * ___killer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_getposid187(CombatEntity,System.Int32)
extern "C"  int32_t CombatEntityGenerated_ilo_getposid187_m1191217067 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CombatEntityGenerated::ilo_getVector3S188(System.Int32)
extern "C"  Vector3_t4282066566  CombatEntityGenerated_ilo_getVector3S188_m370849843 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_LookAt189(CombatEntity,UnityEngine.Vector3)
extern "C"  void CombatEntityGenerated_ilo_LookAt189_m537250842 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, Vector3_t4282066566  ___pos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_LookAtTarget190(CombatEntity,UnityEngine.Vector3)
extern "C"  void CombatEntityGenerated_ilo_LookAtTarget190_m1945300831 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, Vector3_t4282066566  ___targetPos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_lucency191(CombatEntity)
extern "C"  void CombatEntityGenerated_ilo_lucency191_m3989055517 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_MoceCameraEnd192(CombatEntity,CEvent.ZEvent)
extern "C"  void CombatEntityGenerated_ilo_MoceCameraEnd192_m442538458 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, ZEvent_t3638018500 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_OnDelayTime193(CombatEntity)
extern "C"  void CombatEntityGenerated_ilo_OnDelayTime193_m3540782151 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_OnDestroy194(CombatEntity)
extern "C"  void CombatEntityGenerated_ilo_OnDestroy194_m1723126590 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_OneSecAuraReduceHP195(CombatEntity)
extern "C"  void CombatEntityGenerated_ilo_OneSecAuraReduceHP195_m4189989560 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_OnTriggerFun196(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_OnTriggerFun196_m4037981704 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___effectId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_OnUseingSkill197(CombatEntity,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_OnUseingSkill197_m3925494418 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___skillID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_OpenTaunt198(CombatEntity)
extern "C"  void CombatEntityGenerated_ilo_OpenTaunt198_m3343567161 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CombatEntityGenerated::ilo_getEnum199(System.Int32)
extern "C"  int32_t CombatEntityGenerated_ilo_getEnum199_m3611852417 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_PlayHpTextEffect200(CombatEntity,System.Single,HeadUpBloodMgr/LabelType)
extern "C"  void CombatEntityGenerated_ilo_PlayHpTextEffect200_m804606474 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___delta1, int32_t ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_ReduceHP201(CombatEntity,System.Single,CombatEntity,FightEnum.EDamageType,System.Boolean,System.Boolean,System.Int32,System.Int32)
extern "C"  void CombatEntityGenerated_ilo_ReduceHP201_m2331758137 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___delta1, CombatEntity_t684137495 * ___src2, int32_t ___damageType3, bool ___isRepaly4, bool ___isActionSkill5, int32_t ___neardeathprotect6, int32_t ___buffId7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_RemoveAllStates202(CombatEntity)
extern "C"  void CombatEntityGenerated_ilo_RemoveAllStates202_m1858869346 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_Revive203(CombatEntity)
extern "C"  void CombatEntityGenerated_ilo_Revive203_m2534241735 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_SetViewModel204(CombatEntity,System.Boolean)
extern "C"  void CombatEntityGenerated_ilo_SetViewModel204_m3101823010 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___isShow1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_StopMove205(CombatEntity,System.Boolean)
extern "C"  void CombatEntityGenerated_ilo_StopMove205_m2703255282 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___isPlayAnim1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_Suicide206(CombatEntity)
extern "C"  void CombatEntityGenerated_ilo_Suicide206_m1870353655 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_TankState207(CombatEntity,CEvent.ZEvent)
extern "C"  void CombatEntityGenerated_ilo_TankState207_m2367046516 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, ZEvent_t3638018500 * ___ev1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_UpdateGodDown208(CombatEntity)
extern "C"  void CombatEntityGenerated_ilo_UpdateGodDown208_m541480466 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_UpdateTargetPos209(CombatEntity,UnityEngine.Vector3,System.Action)
extern "C"  void CombatEntityGenerated_ilo_UpdateTargetPos209_m3174945720 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, Vector3_t4282066566  ___targetPos1, Action_t3771233898 * ___OnTargetCall2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntityGenerated::ilo_UpdateYPos210(CombatEntity)
extern "C"  void CombatEntityGenerated_ilo_UpdateYPos210_m1142771732 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject CombatEntityGenerated::ilo_getFunctionS211(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * CombatEntityGenerated_ilo_getFunctionS211_m1997976601 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CombatEntityGenerated::ilo_isFunctionS212(System.Int32)
extern "C"  bool CombatEntityGenerated_ilo_isFunctionS212_m188111651 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action CombatEntityGenerated::ilo_CombatEntity_UpdateTargetPos_GetDelegate_member89_arg1213(CSRepresentedObject)
extern "C"  Action_t3771233898 * CombatEntityGenerated_ilo_CombatEntity_UpdateTargetPos_GetDelegate_member89_arg1213_m851480538 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
