﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_muca22
struct M_muca22_t611187734;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_muca22::.ctor()
extern "C"  void M_muca22__ctor_m126106845 (M_muca22_t611187734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_muca22::M_tipucooSuka0(System.String[],System.Int32)
extern "C"  void M_muca22_M_tipucooSuka0_m3167140927 (M_muca22_t611187734 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
