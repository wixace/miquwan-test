﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct Collection_1_t266055896;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// UnityEngine.Rendering.ReflectionProbeBlendInfo[]
struct ReflectionProbeBlendInfoU5BU5D_t558780463;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct IEnumerator_1_t2992462787;
// System.Collections.Generic.IList`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct IList_1_t3775244941;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeB1080597738.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::.ctor()
extern "C"  void Collection_1__ctor_m3973121370_gshared (Collection_1_t266055896 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3973121370(__this, method) ((  void (*) (Collection_1_t266055896 *, const MethodInfo*))Collection_1__ctor_m3973121370_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1284997021_gshared (Collection_1_t266055896 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1284997021(__this, method) ((  bool (*) (Collection_1_t266055896 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1284997021_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m1818876202_gshared (Collection_1_t266055896 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1818876202(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t266055896 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1818876202_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2126254521_gshared (Collection_1_t266055896 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2126254521(__this, method) ((  Il2CppObject * (*) (Collection_1_t266055896 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2126254521_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m621782308_gshared (Collection_1_t266055896 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m621782308(__this, ___value0, method) ((  int32_t (*) (Collection_1_t266055896 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m621782308_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1930502812_gshared (Collection_1_t266055896 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1930502812(__this, ___value0, method) ((  bool (*) (Collection_1_t266055896 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1930502812_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3421546876_gshared (Collection_1_t266055896 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m3421546876(__this, ___value0, method) ((  int32_t (*) (Collection_1_t266055896 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3421546876_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m207657199_gshared (Collection_1_t266055896 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m207657199(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t266055896 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m207657199_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2170697689_gshared (Collection_1_t266055896 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m2170697689(__this, ___value0, method) ((  void (*) (Collection_1_t266055896 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2170697689_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1992154944_gshared (Collection_1_t266055896 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1992154944(__this, method) ((  bool (*) (Collection_1_t266055896 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1992154944_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1921431218_gshared (Collection_1_t266055896 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1921431218(__this, method) ((  Il2CppObject * (*) (Collection_1_t266055896 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1921431218_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1390278411_gshared (Collection_1_t266055896 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1390278411(__this, method) ((  bool (*) (Collection_1_t266055896 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1390278411_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m2715613838_gshared (Collection_1_t266055896 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2715613838(__this, method) ((  bool (*) (Collection_1_t266055896 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m2715613838_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m725074873_gshared (Collection_1_t266055896 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m725074873(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t266055896 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m725074873_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2604025094_gshared (Collection_1_t266055896 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2604025094(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t266055896 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2604025094_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Add(T)
extern "C"  void Collection_1_Add_m3720538085_gshared (Collection_1_t266055896 * __this, ReflectionProbeBlendInfo_t1080597738  ___item0, const MethodInfo* method);
#define Collection_1_Add_m3720538085(__this, ___item0, method) ((  void (*) (Collection_1_t266055896 *, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))Collection_1_Add_m3720538085_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Clear()
extern "C"  void Collection_1_Clear_m1379254661_gshared (Collection_1_t266055896 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1379254661(__this, method) ((  void (*) (Collection_1_t266055896 *, const MethodInfo*))Collection_1_Clear_m1379254661_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1486555805_gshared (Collection_1_t266055896 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1486555805(__this, method) ((  void (*) (Collection_1_t266055896 *, const MethodInfo*))Collection_1_ClearItems_m1486555805_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Contains(T)
extern "C"  bool Collection_1_Contains_m1376581879_gshared (Collection_1_t266055896 * __this, ReflectionProbeBlendInfo_t1080597738  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m1376581879(__this, ___item0, method) ((  bool (*) (Collection_1_t266055896 *, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))Collection_1_Contains_m1376581879_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m906395797_gshared (Collection_1_t266055896 * __this, ReflectionProbeBlendInfoU5BU5D_t558780463* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m906395797(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t266055896 *, ReflectionProbeBlendInfoU5BU5D_t558780463*, int32_t, const MethodInfo*))Collection_1_CopyTo_m906395797_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m1404502478_gshared (Collection_1_t266055896 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1404502478(__this, method) ((  Il2CppObject* (*) (Collection_1_t266055896 *, const MethodInfo*))Collection_1_GetEnumerator_m1404502478_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m1373141281_gshared (Collection_1_t266055896 * __this, ReflectionProbeBlendInfo_t1080597738  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m1373141281(__this, ___item0, method) ((  int32_t (*) (Collection_1_t266055896 *, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))Collection_1_IndexOf_m1373141281_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3339040844_gshared (Collection_1_t266055896 * __this, int32_t ___index0, ReflectionProbeBlendInfo_t1080597738  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3339040844(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t266055896 *, int32_t, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))Collection_1_Insert_m3339040844_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1129308287_gshared (Collection_1_t266055896 * __this, int32_t ___index0, ReflectionProbeBlendInfo_t1080597738  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m1129308287(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t266055896 *, int32_t, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))Collection_1_InsertItem_m1129308287_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m262095813_gshared (Collection_1_t266055896 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m262095813(__this, method) ((  Il2CppObject* (*) (Collection_1_t266055896 *, const MethodInfo*))Collection_1_get_Items_m262095813_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Remove(T)
extern "C"  bool Collection_1_Remove_m1565100722_gshared (Collection_1_t266055896 * __this, ReflectionProbeBlendInfo_t1080597738  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1565100722(__this, ___item0, method) ((  bool (*) (Collection_1_t266055896 *, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))Collection_1_Remove_m1565100722_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1212893714_gshared (Collection_1_t266055896 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m1212893714(__this, ___index0, method) ((  void (*) (Collection_1_t266055896 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1212893714_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m259379634_gshared (Collection_1_t266055896 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m259379634(__this, ___index0, method) ((  void (*) (Collection_1_t266055896 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m259379634_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3729981562_gshared (Collection_1_t266055896 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3729981562(__this, method) ((  int32_t (*) (Collection_1_t266055896 *, const MethodInfo*))Collection_1_get_Count_m3729981562_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::get_Item(System.Int32)
extern "C"  ReflectionProbeBlendInfo_t1080597738  Collection_1_get_Item_m424671928_gshared (Collection_1_t266055896 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m424671928(__this, ___index0, method) ((  ReflectionProbeBlendInfo_t1080597738  (*) (Collection_1_t266055896 *, int32_t, const MethodInfo*))Collection_1_get_Item_m424671928_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m2287277603_gshared (Collection_1_t266055896 * __this, int32_t ___index0, ReflectionProbeBlendInfo_t1080597738  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m2287277603(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t266055896 *, int32_t, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))Collection_1_set_Item_m2287277603_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1747640822_gshared (Collection_1_t266055896 * __this, int32_t ___index0, ReflectionProbeBlendInfo_t1080597738  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1747640822(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t266055896 *, int32_t, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))Collection_1_SetItem_m1747640822_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2053213269_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2053213269(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2053213269_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::ConvertItem(System.Object)
extern "C"  ReflectionProbeBlendInfo_t1080597738  Collection_1_ConvertItem_m4196240791_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m4196240791(__this /* static, unused */, ___item0, method) ((  ReflectionProbeBlendInfo_t1080597738  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m4196240791_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m711100629_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m711100629(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m711100629_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3717540175_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3717540175(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3717540175_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m4193204144_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m4193204144(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m4193204144_gshared)(__this /* static, unused */, ___list0, method)
