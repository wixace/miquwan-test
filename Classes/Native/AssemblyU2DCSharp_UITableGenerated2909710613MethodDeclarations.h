﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UITableGenerated
struct UITableGenerated_t2909710613;
// JSVCall
struct JSVCall_t3708497963;
// UITable/OnReposition
struct OnReposition_t213079568;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.Comparison`1<UnityEngine.Transform>
struct Comparison_1_t375483973;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UITableGenerated::.ctor()
extern "C"  void UITableGenerated__ctor_m4233815334 (UITableGenerated_t2909710613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITableGenerated::UITable_UITable1(JSVCall,System.Int32)
extern "C"  bool UITableGenerated_UITable_UITable1_m14453726 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITableGenerated::UITable_columns(JSVCall)
extern "C"  void UITableGenerated_UITable_columns_m2621558603 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITableGenerated::UITable_direction(JSVCall)
extern "C"  void UITableGenerated_UITable_direction_m2950405481 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITableGenerated::UITable_sorting(JSVCall)
extern "C"  void UITableGenerated_UITable_sorting_m2144828804 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITableGenerated::UITable_pivot(JSVCall)
extern "C"  void UITableGenerated_UITable_pivot_m965874950 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITableGenerated::UITable_cellAlignment(JSVCall)
extern "C"  void UITableGenerated_UITable_cellAlignment_m618713991 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITableGenerated::UITable_hideInactive(JSVCall)
extern "C"  void UITableGenerated_UITable_hideInactive_m1328711831 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITableGenerated::UITable_keepWithinPanel(JSVCall)
extern "C"  void UITableGenerated_UITable_keepWithinPanel_m2519006068 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITableGenerated::UITable_padding(JSVCall)
extern "C"  void UITableGenerated_UITable_padding_m2932909463 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UITable/OnReposition UITableGenerated::UITable_onReposition_GetDelegate_member8_arg0(CSRepresentedObject)
extern "C"  OnReposition_t213079568 * UITableGenerated_UITable_onReposition_GetDelegate_member8_arg0_m1913149031 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITableGenerated::UITable_onReposition(JSVCall)
extern "C"  void UITableGenerated_UITable_onReposition_m66488457 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Comparison`1<UnityEngine.Transform> UITableGenerated::UITable_onCustomSort_GetDelegate_member9_arg0(CSRepresentedObject)
extern "C"  Comparison_1_t375483973 * UITableGenerated_UITable_onCustomSort_GetDelegate_member9_arg0_m3919556895 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITableGenerated::UITable_onCustomSort(JSVCall)
extern "C"  void UITableGenerated_UITable_onCustomSort_m2712020374 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITableGenerated::UITable_repositionNow(JSVCall)
extern "C"  void UITableGenerated_UITable_repositionNow_m1994729838 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITableGenerated::UITable_GetChildList(JSVCall,System.Int32)
extern "C"  bool UITableGenerated_UITable_GetChildList_m3212016779 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITableGenerated::UITable_Reposition(JSVCall,System.Int32)
extern "C"  bool UITableGenerated_UITable_Reposition_m3385411395 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITableGenerated::__Register()
extern "C"  void UITableGenerated___Register_m405135201 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UITable/OnReposition UITableGenerated::<UITable_onReposition>m__164()
extern "C"  OnReposition_t213079568 * UITableGenerated_U3CUITable_onRepositionU3Em__164_m677614287 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Comparison`1<UnityEngine.Transform> UITableGenerated::<UITable_onCustomSort>m__166()
extern "C"  Comparison_1_t375483973 * UITableGenerated_U3CUITable_onCustomSortU3Em__166_m1067101250 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITableGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UITableGenerated_ilo_attachFinalizerObject1_m3037400953 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UITableGenerated::ilo_getInt322(System.Int32)
extern "C"  int32_t UITableGenerated_ilo_getInt322_m2341209262 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UITableGenerated::ilo_getEnum3(System.Int32)
extern "C"  int32_t UITableGenerated_ilo_getEnum3_m1616328236 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UITableGenerated::ilo_setEnum4(System.Int32,System.Int32)
extern "C"  void UITableGenerated_ilo_setEnum4_m1516630244 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UITableGenerated::ilo_getBooleanS5(System.Int32)
extern "C"  bool UITableGenerated_ilo_getBooleanS5_m4192604650 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UITableGenerated::ilo_getVector2S6(System.Int32)
extern "C"  Vector2_t4282066565  UITableGenerated_ilo_getVector2S6_m1753654777 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UITableGenerated::ilo_setObject7(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UITableGenerated_ilo_setObject7_m1082908330 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
