﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,TabData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m996689236(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2248295041 *, Dictionary_2_t930971649 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,TabData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m335023117(__this, method) ((  Il2CppObject * (*) (Enumerator_t2248295041 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,TabData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2036997857(__this, method) ((  void (*) (Enumerator_t2248295041 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,TabData>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3146673770(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2248295041 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,TabData>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2180991785(__this, method) ((  Il2CppObject * (*) (Enumerator_t2248295041 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,TabData>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1037501307(__this, method) ((  Il2CppObject * (*) (Enumerator_t2248295041 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,TabData>::MoveNext()
#define Enumerator_MoveNext_m2894919117(__this, method) ((  bool (*) (Enumerator_t2248295041 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,TabData>::get_Current()
#define Enumerator_get_Current_m288196803(__this, method) ((  KeyValuePair_2_t829752355  (*) (Enumerator_t2248295041 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,TabData>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2815372634(__this, method) ((  String_t* (*) (Enumerator_t2248295041 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,TabData>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2185353470(__this, method) ((  TabData_t110553279 * (*) (Enumerator_t2248295041 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,TabData>::Reset()
#define Enumerator_Reset_m2241864102(__this, method) ((  void (*) (Enumerator_t2248295041 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,TabData>::VerifyState()
#define Enumerator_VerifyState_m4204862767(__this, method) ((  void (*) (Enumerator_t2248295041 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,TabData>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2053765335(__this, method) ((  void (*) (Enumerator_t2248295041 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,TabData>::Dispose()
#define Enumerator_Dispose_m881248502(__this, method) ((  void (*) (Enumerator_t2248295041 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
