﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RVO.Simulator/WorkerContext
struct WorkerContext_t2850943897;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.RVO.Simulator/WorkerContext::.ctor()
extern "C"  void WorkerContext__ctor_m277583858 (WorkerContext_t2850943897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
