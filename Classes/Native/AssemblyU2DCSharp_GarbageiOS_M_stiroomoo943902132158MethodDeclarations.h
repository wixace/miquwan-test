﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_stiroomoo94
struct M_stiroomoo94_t3902132158;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_stiroomoo943902132158.h"

// System.Void GarbageiOS.M_stiroomoo94::.ctor()
extern "C"  void M_stiroomoo94__ctor_m3067052389 (M_stiroomoo94_t3902132158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_stiroomoo94::M_jeyea0(System.String[],System.Int32)
extern "C"  void M_stiroomoo94_M_jeyea0_m3817288894 (M_stiroomoo94_t3902132158 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_stiroomoo94::M_stawjaiseQiqo1(System.String[],System.Int32)
extern "C"  void M_stiroomoo94_M_stawjaiseQiqo1_m3877204950 (M_stiroomoo94_t3902132158 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_stiroomoo94::M_coute2(System.String[],System.Int32)
extern "C"  void M_stiroomoo94_M_coute2_m1878764352 (M_stiroomoo94_t3902132158 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_stiroomoo94::M_zayjereha3(System.String[],System.Int32)
extern "C"  void M_stiroomoo94_M_zayjereha3_m3176480386 (M_stiroomoo94_t3902132158 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_stiroomoo94::M_sasdataZijarur4(System.String[],System.Int32)
extern "C"  void M_stiroomoo94_M_sasdataZijarur4_m616540610 (M_stiroomoo94_t3902132158 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_stiroomoo94::ilo_M_jeyea01(GarbageiOS.M_stiroomoo94,System.String[],System.Int32)
extern "C"  void M_stiroomoo94_ilo_M_jeyea01_m2461846842 (Il2CppObject * __this /* static, unused */, M_stiroomoo94_t3902132158 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_stiroomoo94::ilo_M_stawjaiseQiqo12(GarbageiOS.M_stiroomoo94,System.String[],System.Int32)
extern "C"  void M_stiroomoo94_ilo_M_stawjaiseQiqo12_m2544455217 (Il2CppObject * __this /* static, unused */, M_stiroomoo94_t3902132158 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
