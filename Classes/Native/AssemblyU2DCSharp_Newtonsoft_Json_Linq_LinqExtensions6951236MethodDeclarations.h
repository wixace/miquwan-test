﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.IJEnumerable`1<Newtonsoft.Json.Linq.JProperty>
struct IJEnumerable_1_t951749596;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JObject>
struct IEnumerable_1_t804489860;
// Newtonsoft.Json.Linq.IJEnumerable`1<Newtonsoft.Json.Linq.JToken>
struct IJEnumerable_1_t1747579902;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerable_1_t2418191612;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JProperty>
struct IEnumerable_1_t1622361306;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.String
struct String_t;
// Newtonsoft.Json.Linq.JValue
struct JValue_t3413677367;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JValue3413677367.h"
#include "mscorlib_System_Type2863145774.h"

// Newtonsoft.Json.Linq.IJEnumerable`1<Newtonsoft.Json.Linq.JProperty> Newtonsoft.Json.Linq.LinqExtensions::Properties(System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JObject>)
extern "C"  Il2CppObject* LinqExtensions_Properties_m2885902128 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.IJEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.LinqExtensions::Values(System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken>,System.Object)
extern "C"  Il2CppObject* LinqExtensions_Values_m114654391 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Il2CppObject * ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.IJEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.LinqExtensions::Values(System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken>)
extern "C"  Il2CppObject* LinqExtensions_Values_m925945257 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.IJEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.LinqExtensions::AsJEnumerable(System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken>)
extern "C"  Il2CppObject* LinqExtensions_AsJEnumerable_m552057785 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JProperty> Newtonsoft.Json.Linq.LinqExtensions::<Properties>m__378(Newtonsoft.Json.Linq.JObject)
extern "C"  Il2CppObject* LinqExtensions_U3CPropertiesU3Em__378_m1172326590 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.LinqExtensions::ilo_ArgumentNotNull1(System.Object,System.String)
extern "C"  void LinqExtensions_ilo_ArgumentNotNull1_m2892113955 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, String_t* ___parameterName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.LinqExtensions::ilo_get_Value2(Newtonsoft.Json.Linq.JValue)
extern "C"  Il2CppObject * LinqExtensions_ilo_get_Value2_m3059448804 (Il2CppObject * __this /* static, unused */, JValue_t3413677367 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.LinqExtensions::ilo_IsNullableType3(System.Type)
extern "C"  bool LinqExtensions_ilo_IsNullableType3_m1761772110 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
