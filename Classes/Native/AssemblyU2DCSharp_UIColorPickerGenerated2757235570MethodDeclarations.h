﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIColorPickerGenerated
struct UIColorPickerGenerated_t2757235570;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// UIColorPicker
struct UIColorPicker_t4217226909;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_UIColorPicker4217226909.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Void UIColorPickerGenerated::.ctor()
extern "C"  void UIColorPickerGenerated__ctor_m2579358057 (UIColorPickerGenerated_t2757235570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIColorPickerGenerated::UIColorPicker_UIColorPicker1(JSVCall,System.Int32)
extern "C"  bool UIColorPickerGenerated_UIColorPicker_UIColorPicker1_m3382186997 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPickerGenerated::UIColorPicker_current(JSVCall)
extern "C"  void UIColorPickerGenerated_UIColorPicker_current_m731868457 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPickerGenerated::UIColorPicker_value(JSVCall)
extern "C"  void UIColorPickerGenerated_UIColorPicker_value_m1743691569 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPickerGenerated::UIColorPicker_selectionWidget(JSVCall)
extern "C"  void UIColorPickerGenerated_UIColorPicker_selectionWidget_m3508061586 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPickerGenerated::UIColorPicker_onChange(JSVCall)
extern "C"  void UIColorPickerGenerated_UIColorPicker_onChange_m2805484155 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIColorPickerGenerated::UIColorPicker_Select__Color(JSVCall,System.Int32)
extern "C"  bool UIColorPickerGenerated_UIColorPicker_Select__Color_m12393960 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIColorPickerGenerated::UIColorPicker_Select__Vector2(JSVCall,System.Int32)
extern "C"  bool UIColorPickerGenerated_UIColorPicker_Select__Vector2_m2808651732 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIColorPickerGenerated::UIColorPicker_Sample__Single__Single(JSVCall,System.Int32)
extern "C"  bool UIColorPickerGenerated_UIColorPicker_Sample__Single__Single_m860112027 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPickerGenerated::__Register()
extern "C"  void UIColorPickerGenerated___Register_m4287960318 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIColorPickerGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UIColorPickerGenerated_ilo_attachFinalizerObject1_m529125526 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIColorPickerGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIColorPickerGenerated_ilo_getObject2_m1279234215 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIColorPickerGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIColorPickerGenerated_ilo_setObject3_m2437417667 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPickerGenerated::ilo_Select4(UIColorPicker,UnityEngine.Vector2)
extern "C"  void UIColorPickerGenerated_ilo_Select4_m3617367869 (Il2CppObject * __this /* static, unused */, UIColorPicker_t4217226909 * ____this0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIColorPickerGenerated::ilo_getSingle5(System.Int32)
extern "C"  float UIColorPickerGenerated_ilo_getSingle5_m3895374690 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UIColorPickerGenerated::ilo_Sample6(System.Single,System.Single)
extern "C"  Color_t4194546905  UIColorPickerGenerated_ilo_Sample6_m3958978455 (Il2CppObject * __this /* static, unused */, float ___x0, float ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
