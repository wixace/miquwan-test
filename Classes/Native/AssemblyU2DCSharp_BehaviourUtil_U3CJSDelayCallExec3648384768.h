﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BehaviourUtil/JSDelayFun3
struct JSDelayFun3_t1074711454;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BehaviourUtil/<JSDelayCallExec>c__Iterator3C
struct  U3CJSDelayCallExecU3Ec__Iterator3C_t3648384768  : public Il2CppObject
{
public:
	// System.UInt32 BehaviourUtil/<JSDelayCallExec>c__Iterator3C::<id>__0
	uint32_t ___U3CidU3E__0_0;
	// System.Single BehaviourUtil/<JSDelayCallExec>c__Iterator3C::delaytime
	float ___delaytime_1;
	// BehaviourUtil/JSDelayFun3 BehaviourUtil/<JSDelayCallExec>c__Iterator3C::act
	JSDelayFun3_t1074711454 * ___act_2;
	// System.Object BehaviourUtil/<JSDelayCallExec>c__Iterator3C::arg1
	Il2CppObject * ___arg1_3;
	// System.Object BehaviourUtil/<JSDelayCallExec>c__Iterator3C::arg2
	Il2CppObject * ___arg2_4;
	// System.Object BehaviourUtil/<JSDelayCallExec>c__Iterator3C::arg3
	Il2CppObject * ___arg3_5;
	// System.Int32 BehaviourUtil/<JSDelayCallExec>c__Iterator3C::$PC
	int32_t ___U24PC_6;
	// System.Object BehaviourUtil/<JSDelayCallExec>c__Iterator3C::$current
	Il2CppObject * ___U24current_7;
	// System.Single BehaviourUtil/<JSDelayCallExec>c__Iterator3C::<$>delaytime
	float ___U3CU24U3Edelaytime_8;
	// BehaviourUtil/JSDelayFun3 BehaviourUtil/<JSDelayCallExec>c__Iterator3C::<$>act
	JSDelayFun3_t1074711454 * ___U3CU24U3Eact_9;
	// System.Object BehaviourUtil/<JSDelayCallExec>c__Iterator3C::<$>arg1
	Il2CppObject * ___U3CU24U3Earg1_10;
	// System.Object BehaviourUtil/<JSDelayCallExec>c__Iterator3C::<$>arg2
	Il2CppObject * ___U3CU24U3Earg2_11;
	// System.Object BehaviourUtil/<JSDelayCallExec>c__Iterator3C::<$>arg3
	Il2CppObject * ___U3CU24U3Earg3_12;

public:
	inline static int32_t get_offset_of_U3CidU3E__0_0() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3C_t3648384768, ___U3CidU3E__0_0)); }
	inline uint32_t get_U3CidU3E__0_0() const { return ___U3CidU3E__0_0; }
	inline uint32_t* get_address_of_U3CidU3E__0_0() { return &___U3CidU3E__0_0; }
	inline void set_U3CidU3E__0_0(uint32_t value)
	{
		___U3CidU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_delaytime_1() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3C_t3648384768, ___delaytime_1)); }
	inline float get_delaytime_1() const { return ___delaytime_1; }
	inline float* get_address_of_delaytime_1() { return &___delaytime_1; }
	inline void set_delaytime_1(float value)
	{
		___delaytime_1 = value;
	}

	inline static int32_t get_offset_of_act_2() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3C_t3648384768, ___act_2)); }
	inline JSDelayFun3_t1074711454 * get_act_2() const { return ___act_2; }
	inline JSDelayFun3_t1074711454 ** get_address_of_act_2() { return &___act_2; }
	inline void set_act_2(JSDelayFun3_t1074711454 * value)
	{
		___act_2 = value;
		Il2CppCodeGenWriteBarrier(&___act_2, value);
	}

	inline static int32_t get_offset_of_arg1_3() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3C_t3648384768, ___arg1_3)); }
	inline Il2CppObject * get_arg1_3() const { return ___arg1_3; }
	inline Il2CppObject ** get_address_of_arg1_3() { return &___arg1_3; }
	inline void set_arg1_3(Il2CppObject * value)
	{
		___arg1_3 = value;
		Il2CppCodeGenWriteBarrier(&___arg1_3, value);
	}

	inline static int32_t get_offset_of_arg2_4() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3C_t3648384768, ___arg2_4)); }
	inline Il2CppObject * get_arg2_4() const { return ___arg2_4; }
	inline Il2CppObject ** get_address_of_arg2_4() { return &___arg2_4; }
	inline void set_arg2_4(Il2CppObject * value)
	{
		___arg2_4 = value;
		Il2CppCodeGenWriteBarrier(&___arg2_4, value);
	}

	inline static int32_t get_offset_of_arg3_5() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3C_t3648384768, ___arg3_5)); }
	inline Il2CppObject * get_arg3_5() const { return ___arg3_5; }
	inline Il2CppObject ** get_address_of_arg3_5() { return &___arg3_5; }
	inline void set_arg3_5(Il2CppObject * value)
	{
		___arg3_5 = value;
		Il2CppCodeGenWriteBarrier(&___arg3_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3C_t3648384768, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3C_t3648384768, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Edelaytime_8() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3C_t3648384768, ___U3CU24U3Edelaytime_8)); }
	inline float get_U3CU24U3Edelaytime_8() const { return ___U3CU24U3Edelaytime_8; }
	inline float* get_address_of_U3CU24U3Edelaytime_8() { return &___U3CU24U3Edelaytime_8; }
	inline void set_U3CU24U3Edelaytime_8(float value)
	{
		___U3CU24U3Edelaytime_8 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Eact_9() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3C_t3648384768, ___U3CU24U3Eact_9)); }
	inline JSDelayFun3_t1074711454 * get_U3CU24U3Eact_9() const { return ___U3CU24U3Eact_9; }
	inline JSDelayFun3_t1074711454 ** get_address_of_U3CU24U3Eact_9() { return &___U3CU24U3Eact_9; }
	inline void set_U3CU24U3Eact_9(JSDelayFun3_t1074711454 * value)
	{
		___U3CU24U3Eact_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eact_9, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Earg1_10() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3C_t3648384768, ___U3CU24U3Earg1_10)); }
	inline Il2CppObject * get_U3CU24U3Earg1_10() const { return ___U3CU24U3Earg1_10; }
	inline Il2CppObject ** get_address_of_U3CU24U3Earg1_10() { return &___U3CU24U3Earg1_10; }
	inline void set_U3CU24U3Earg1_10(Il2CppObject * value)
	{
		___U3CU24U3Earg1_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Earg1_10, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Earg2_11() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3C_t3648384768, ___U3CU24U3Earg2_11)); }
	inline Il2CppObject * get_U3CU24U3Earg2_11() const { return ___U3CU24U3Earg2_11; }
	inline Il2CppObject ** get_address_of_U3CU24U3Earg2_11() { return &___U3CU24U3Earg2_11; }
	inline void set_U3CU24U3Earg2_11(Il2CppObject * value)
	{
		___U3CU24U3Earg2_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Earg2_11, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Earg3_12() { return static_cast<int32_t>(offsetof(U3CJSDelayCallExecU3Ec__Iterator3C_t3648384768, ___U3CU24U3Earg3_12)); }
	inline Il2CppObject * get_U3CU24U3Earg3_12() const { return ___U3CU24U3Earg3_12; }
	inline Il2CppObject ** get_address_of_U3CU24U3Earg3_12() { return &___U3CU24U3Earg3_12; }
	inline void set_U3CU24U3Earg3_12(Il2CppObject * value)
	{
		___U3CU24U3Earg3_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Earg3_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
