﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CSRobOtherPlayer
struct CSRobOtherPlayer_t3654572956;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSRobData
struct  CSRobData_t815326335  : public Il2CppObject
{
public:
	// CSRobOtherPlayer CSRobData::robOtherPlayer
	CSRobOtherPlayer_t3654572956 * ___robOtherPlayer_0;

public:
	inline static int32_t get_offset_of_robOtherPlayer_0() { return static_cast<int32_t>(offsetof(CSRobData_t815326335, ___robOtherPlayer_0)); }
	inline CSRobOtherPlayer_t3654572956 * get_robOtherPlayer_0() const { return ___robOtherPlayer_0; }
	inline CSRobOtherPlayer_t3654572956 ** get_address_of_robOtherPlayer_0() { return &___robOtherPlayer_0; }
	inline void set_robOtherPlayer_0(CSRobOtherPlayer_t3654572956 * value)
	{
		___robOtherPlayer_0 = value;
		Il2CppCodeGenWriteBarrier(&___robOtherPlayer_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
