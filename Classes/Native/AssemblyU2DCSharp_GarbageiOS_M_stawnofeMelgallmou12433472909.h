﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_stawnofeMelgallmou12
struct  M_stawnofeMelgallmou12_t433472909  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_stawnofeMelgallmou12::_tookivemGedoodas
	uint32_t ____tookivemGedoodas_0;
	// System.Int32 GarbageiOS.M_stawnofeMelgallmou12::_serdrelJumay
	int32_t ____serdrelJumay_1;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_seasisparPerkearwea
	float ____seasisparPerkearwea_2;
	// System.String GarbageiOS.M_stawnofeMelgallmou12::_salljalwis
	String_t* ____salljalwis_3;
	// System.UInt32 GarbageiOS.M_stawnofeMelgallmou12::_wayraMisi
	uint32_t ____wayraMisi_4;
	// System.Boolean GarbageiOS.M_stawnofeMelgallmou12::_howkeaBasku
	bool ____howkeaBasku_5;
	// System.String GarbageiOS.M_stawnofeMelgallmou12::_rocearhi
	String_t* ____rocearhi_6;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_teasafeMawzeltu
	float ____teasafeMawzeltu_7;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_mefallki
	float ____mefallki_8;
	// System.Boolean GarbageiOS.M_stawnofeMelgallmou12::_pofiwhereRulor
	bool ____pofiwhereRulor_9;
	// System.String GarbageiOS.M_stawnofeMelgallmou12::_cusurMesasi
	String_t* ____cusurMesasi_10;
	// System.String GarbageiOS.M_stawnofeMelgallmou12::_lortebarReresel
	String_t* ____lortebarReresel_11;
	// System.Int32 GarbageiOS.M_stawnofeMelgallmou12::_basere
	int32_t ____basere_12;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_qourow
	float ____qourow_13;
	// System.Int32 GarbageiOS.M_stawnofeMelgallmou12::_tase
	int32_t ____tase_14;
	// System.Int32 GarbageiOS.M_stawnofeMelgallmou12::_fejeter
	int32_t ____fejeter_15;
	// System.Int32 GarbageiOS.M_stawnofeMelgallmou12::_drowje
	int32_t ____drowje_16;
	// System.Int32 GarbageiOS.M_stawnofeMelgallmou12::_binewou
	int32_t ____binewou_17;
	// System.Int32 GarbageiOS.M_stawnofeMelgallmou12::_yedexearVawetea
	int32_t ____yedexearVawetea_18;
	// System.Boolean GarbageiOS.M_stawnofeMelgallmou12::_temee
	bool ____temee_19;
	// System.Int32 GarbageiOS.M_stawnofeMelgallmou12::_noudowsoJojirnu
	int32_t ____noudowsoJojirnu_20;
	// System.String GarbageiOS.M_stawnofeMelgallmou12::_reherkoDasseete
	String_t* ____reherkoDasseete_21;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_sermouVehall
	float ____sermouVehall_22;
	// System.Int32 GarbageiOS.M_stawnofeMelgallmou12::_gesoraVopirwow
	int32_t ____gesoraVopirwow_23;
	// System.Boolean GarbageiOS.M_stawnofeMelgallmou12::_seemardear
	bool ____seemardear_24;
	// System.Boolean GarbageiOS.M_stawnofeMelgallmou12::_gurtis
	bool ____gurtis_25;
	// System.UInt32 GarbageiOS.M_stawnofeMelgallmou12::_felbu
	uint32_t ____felbu_26;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_pirheamawKorsuki
	float ____pirheamawKorsuki_27;
	// System.UInt32 GarbageiOS.M_stawnofeMelgallmou12::_chunoozair
	uint32_t ____chunoozair_28;
	// System.Boolean GarbageiOS.M_stawnofeMelgallmou12::_ceaje
	bool ____ceaje_29;
	// System.Int32 GarbageiOS.M_stawnofeMelgallmou12::_leacerdre
	int32_t ____leacerdre_30;
	// System.UInt32 GarbageiOS.M_stawnofeMelgallmou12::_trugall
	uint32_t ____trugall_31;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_fairsasjou
	float ____fairsasjou_32;
	// System.String GarbageiOS.M_stawnofeMelgallmou12::_lujemtou
	String_t* ____lujemtou_33;
	// System.String GarbageiOS.M_stawnofeMelgallmou12::_melrayQarera
	String_t* ____melrayQarera_34;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_danalpooBurhehay
	float ____danalpooBurhehay_35;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_melhereRogoujor
	float ____melhereRogoujor_36;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_doucowjay
	float ____doucowjay_37;
	// System.Int32 GarbageiOS.M_stawnofeMelgallmou12::_nalltallsem
	int32_t ____nalltallsem_38;
	// System.UInt32 GarbageiOS.M_stawnofeMelgallmou12::_kisdrifaReetou
	uint32_t ____kisdrifaReetou_39;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_temramayNarpa
	float ____temramayNarpa_40;
	// System.UInt32 GarbageiOS.M_stawnofeMelgallmou12::_driskiceSerbis
	uint32_t ____driskiceSerbis_41;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_staltru
	float ____staltru_42;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_zustisair
	float ____zustisair_43;
	// System.String GarbageiOS.M_stawnofeMelgallmou12::_serebemsisCorvow
	String_t* ____serebemsisCorvow_44;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_sadeMawce
	float ____sadeMawce_45;
	// System.UInt32 GarbageiOS.M_stawnofeMelgallmou12::_vaiseNelhall
	uint32_t ____vaiseNelhall_46;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_cawferNorjirci
	float ____cawferNorjirci_47;
	// System.Int32 GarbageiOS.M_stawnofeMelgallmou12::_mounasYasir
	int32_t ____mounasYasir_48;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_guta
	float ____guta_49;
	// System.Int32 GarbageiOS.M_stawnofeMelgallmou12::_murrairray
	int32_t ____murrairray_50;
	// System.String GarbageiOS.M_stawnofeMelgallmou12::_piwurDalji
	String_t* ____piwurDalji_51;
	// System.UInt32 GarbageiOS.M_stawnofeMelgallmou12::_pesalnouMepa
	uint32_t ____pesalnouMepa_52;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_jacartral
	float ____jacartral_53;
	// System.Boolean GarbageiOS.M_stawnofeMelgallmou12::_doupeJizorsu
	bool ____doupeJizorsu_54;
	// System.String GarbageiOS.M_stawnofeMelgallmou12::_noojervar
	String_t* ____noojervar_55;
	// System.String GarbageiOS.M_stawnofeMelgallmou12::_locooner
	String_t* ____locooner_56;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_lesaChidowdou
	float ____lesaChidowdou_57;
	// System.String GarbageiOS.M_stawnofeMelgallmou12::_feedeCewheasea
	String_t* ____feedeCewheasea_58;
	// System.UInt32 GarbageiOS.M_stawnofeMelgallmou12::_sarai
	uint32_t ____sarai_59;
	// System.Int32 GarbageiOS.M_stawnofeMelgallmou12::_hajelni
	int32_t ____hajelni_60;
	// System.Int32 GarbageiOS.M_stawnofeMelgallmou12::_pawtas
	int32_t ____pawtas_61;
	// System.Boolean GarbageiOS.M_stawnofeMelgallmou12::_vetasSairnurtair
	bool ____vetasSairnurtair_62;
	// System.Boolean GarbageiOS.M_stawnofeMelgallmou12::_nurmisball
	bool ____nurmisball_63;
	// System.Int32 GarbageiOS.M_stawnofeMelgallmou12::_feretraDrisjoo
	int32_t ____feretraDrisjoo_64;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_sallmisjaZownayka
	float ____sallmisjaZownayka_65;
	// System.Boolean GarbageiOS.M_stawnofeMelgallmou12::_beartrastoDrooseebe
	bool ____beartrastoDrooseebe_66;
	// System.String GarbageiOS.M_stawnofeMelgallmou12::_gemzereKirsoo
	String_t* ____gemzereKirsoo_67;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_dearmuqur
	float ____dearmuqur_68;
	// System.Int32 GarbageiOS.M_stawnofeMelgallmou12::_yoosajall
	int32_t ____yoosajall_69;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_pasteasarQowear
	float ____pasteasarQowear_70;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_jeeqay
	float ____jeeqay_71;
	// System.Boolean GarbageiOS.M_stawnofeMelgallmou12::_delwhe
	bool ____delwhe_72;
	// System.UInt32 GarbageiOS.M_stawnofeMelgallmou12::_selqirtorHika
	uint32_t ____selqirtorHika_73;
	// System.Single GarbageiOS.M_stawnofeMelgallmou12::_neesu
	float ____neesu_74;
	// System.UInt32 GarbageiOS.M_stawnofeMelgallmou12::_jarsooJalbi
	uint32_t ____jarsooJalbi_75;
	// System.String GarbageiOS.M_stawnofeMelgallmou12::_qurbere
	String_t* ____qurbere_76;
	// System.Int32 GarbageiOS.M_stawnofeMelgallmou12::_zallnejouJairha
	int32_t ____zallnejouJairha_77;
	// System.String GarbageiOS.M_stawnofeMelgallmou12::_heedajiWorrow
	String_t* ____heedajiWorrow_78;
	// System.String GarbageiOS.M_stawnofeMelgallmou12::_naipisHemconir
	String_t* ____naipisHemconir_79;
	// System.UInt32 GarbageiOS.M_stawnofeMelgallmou12::_losarjay
	uint32_t ____losarjay_80;
	// System.Int32 GarbageiOS.M_stawnofeMelgallmou12::_morfufowMinuro
	int32_t ____morfufowMinuro_81;
	// System.UInt32 GarbageiOS.M_stawnofeMelgallmou12::_bembedowDayger
	uint32_t ____bembedowDayger_82;

public:
	inline static int32_t get_offset_of__tookivemGedoodas_0() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____tookivemGedoodas_0)); }
	inline uint32_t get__tookivemGedoodas_0() const { return ____tookivemGedoodas_0; }
	inline uint32_t* get_address_of__tookivemGedoodas_0() { return &____tookivemGedoodas_0; }
	inline void set__tookivemGedoodas_0(uint32_t value)
	{
		____tookivemGedoodas_0 = value;
	}

	inline static int32_t get_offset_of__serdrelJumay_1() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____serdrelJumay_1)); }
	inline int32_t get__serdrelJumay_1() const { return ____serdrelJumay_1; }
	inline int32_t* get_address_of__serdrelJumay_1() { return &____serdrelJumay_1; }
	inline void set__serdrelJumay_1(int32_t value)
	{
		____serdrelJumay_1 = value;
	}

	inline static int32_t get_offset_of__seasisparPerkearwea_2() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____seasisparPerkearwea_2)); }
	inline float get__seasisparPerkearwea_2() const { return ____seasisparPerkearwea_2; }
	inline float* get_address_of__seasisparPerkearwea_2() { return &____seasisparPerkearwea_2; }
	inline void set__seasisparPerkearwea_2(float value)
	{
		____seasisparPerkearwea_2 = value;
	}

	inline static int32_t get_offset_of__salljalwis_3() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____salljalwis_3)); }
	inline String_t* get__salljalwis_3() const { return ____salljalwis_3; }
	inline String_t** get_address_of__salljalwis_3() { return &____salljalwis_3; }
	inline void set__salljalwis_3(String_t* value)
	{
		____salljalwis_3 = value;
		Il2CppCodeGenWriteBarrier(&____salljalwis_3, value);
	}

	inline static int32_t get_offset_of__wayraMisi_4() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____wayraMisi_4)); }
	inline uint32_t get__wayraMisi_4() const { return ____wayraMisi_4; }
	inline uint32_t* get_address_of__wayraMisi_4() { return &____wayraMisi_4; }
	inline void set__wayraMisi_4(uint32_t value)
	{
		____wayraMisi_4 = value;
	}

	inline static int32_t get_offset_of__howkeaBasku_5() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____howkeaBasku_5)); }
	inline bool get__howkeaBasku_5() const { return ____howkeaBasku_5; }
	inline bool* get_address_of__howkeaBasku_5() { return &____howkeaBasku_5; }
	inline void set__howkeaBasku_5(bool value)
	{
		____howkeaBasku_5 = value;
	}

	inline static int32_t get_offset_of__rocearhi_6() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____rocearhi_6)); }
	inline String_t* get__rocearhi_6() const { return ____rocearhi_6; }
	inline String_t** get_address_of__rocearhi_6() { return &____rocearhi_6; }
	inline void set__rocearhi_6(String_t* value)
	{
		____rocearhi_6 = value;
		Il2CppCodeGenWriteBarrier(&____rocearhi_6, value);
	}

	inline static int32_t get_offset_of__teasafeMawzeltu_7() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____teasafeMawzeltu_7)); }
	inline float get__teasafeMawzeltu_7() const { return ____teasafeMawzeltu_7; }
	inline float* get_address_of__teasafeMawzeltu_7() { return &____teasafeMawzeltu_7; }
	inline void set__teasafeMawzeltu_7(float value)
	{
		____teasafeMawzeltu_7 = value;
	}

	inline static int32_t get_offset_of__mefallki_8() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____mefallki_8)); }
	inline float get__mefallki_8() const { return ____mefallki_8; }
	inline float* get_address_of__mefallki_8() { return &____mefallki_8; }
	inline void set__mefallki_8(float value)
	{
		____mefallki_8 = value;
	}

	inline static int32_t get_offset_of__pofiwhereRulor_9() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____pofiwhereRulor_9)); }
	inline bool get__pofiwhereRulor_9() const { return ____pofiwhereRulor_9; }
	inline bool* get_address_of__pofiwhereRulor_9() { return &____pofiwhereRulor_9; }
	inline void set__pofiwhereRulor_9(bool value)
	{
		____pofiwhereRulor_9 = value;
	}

	inline static int32_t get_offset_of__cusurMesasi_10() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____cusurMesasi_10)); }
	inline String_t* get__cusurMesasi_10() const { return ____cusurMesasi_10; }
	inline String_t** get_address_of__cusurMesasi_10() { return &____cusurMesasi_10; }
	inline void set__cusurMesasi_10(String_t* value)
	{
		____cusurMesasi_10 = value;
		Il2CppCodeGenWriteBarrier(&____cusurMesasi_10, value);
	}

	inline static int32_t get_offset_of__lortebarReresel_11() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____lortebarReresel_11)); }
	inline String_t* get__lortebarReresel_11() const { return ____lortebarReresel_11; }
	inline String_t** get_address_of__lortebarReresel_11() { return &____lortebarReresel_11; }
	inline void set__lortebarReresel_11(String_t* value)
	{
		____lortebarReresel_11 = value;
		Il2CppCodeGenWriteBarrier(&____lortebarReresel_11, value);
	}

	inline static int32_t get_offset_of__basere_12() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____basere_12)); }
	inline int32_t get__basere_12() const { return ____basere_12; }
	inline int32_t* get_address_of__basere_12() { return &____basere_12; }
	inline void set__basere_12(int32_t value)
	{
		____basere_12 = value;
	}

	inline static int32_t get_offset_of__qourow_13() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____qourow_13)); }
	inline float get__qourow_13() const { return ____qourow_13; }
	inline float* get_address_of__qourow_13() { return &____qourow_13; }
	inline void set__qourow_13(float value)
	{
		____qourow_13 = value;
	}

	inline static int32_t get_offset_of__tase_14() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____tase_14)); }
	inline int32_t get__tase_14() const { return ____tase_14; }
	inline int32_t* get_address_of__tase_14() { return &____tase_14; }
	inline void set__tase_14(int32_t value)
	{
		____tase_14 = value;
	}

	inline static int32_t get_offset_of__fejeter_15() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____fejeter_15)); }
	inline int32_t get__fejeter_15() const { return ____fejeter_15; }
	inline int32_t* get_address_of__fejeter_15() { return &____fejeter_15; }
	inline void set__fejeter_15(int32_t value)
	{
		____fejeter_15 = value;
	}

	inline static int32_t get_offset_of__drowje_16() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____drowje_16)); }
	inline int32_t get__drowje_16() const { return ____drowje_16; }
	inline int32_t* get_address_of__drowje_16() { return &____drowje_16; }
	inline void set__drowje_16(int32_t value)
	{
		____drowje_16 = value;
	}

	inline static int32_t get_offset_of__binewou_17() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____binewou_17)); }
	inline int32_t get__binewou_17() const { return ____binewou_17; }
	inline int32_t* get_address_of__binewou_17() { return &____binewou_17; }
	inline void set__binewou_17(int32_t value)
	{
		____binewou_17 = value;
	}

	inline static int32_t get_offset_of__yedexearVawetea_18() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____yedexearVawetea_18)); }
	inline int32_t get__yedexearVawetea_18() const { return ____yedexearVawetea_18; }
	inline int32_t* get_address_of__yedexearVawetea_18() { return &____yedexearVawetea_18; }
	inline void set__yedexearVawetea_18(int32_t value)
	{
		____yedexearVawetea_18 = value;
	}

	inline static int32_t get_offset_of__temee_19() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____temee_19)); }
	inline bool get__temee_19() const { return ____temee_19; }
	inline bool* get_address_of__temee_19() { return &____temee_19; }
	inline void set__temee_19(bool value)
	{
		____temee_19 = value;
	}

	inline static int32_t get_offset_of__noudowsoJojirnu_20() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____noudowsoJojirnu_20)); }
	inline int32_t get__noudowsoJojirnu_20() const { return ____noudowsoJojirnu_20; }
	inline int32_t* get_address_of__noudowsoJojirnu_20() { return &____noudowsoJojirnu_20; }
	inline void set__noudowsoJojirnu_20(int32_t value)
	{
		____noudowsoJojirnu_20 = value;
	}

	inline static int32_t get_offset_of__reherkoDasseete_21() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____reherkoDasseete_21)); }
	inline String_t* get__reherkoDasseete_21() const { return ____reherkoDasseete_21; }
	inline String_t** get_address_of__reherkoDasseete_21() { return &____reherkoDasseete_21; }
	inline void set__reherkoDasseete_21(String_t* value)
	{
		____reherkoDasseete_21 = value;
		Il2CppCodeGenWriteBarrier(&____reherkoDasseete_21, value);
	}

	inline static int32_t get_offset_of__sermouVehall_22() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____sermouVehall_22)); }
	inline float get__sermouVehall_22() const { return ____sermouVehall_22; }
	inline float* get_address_of__sermouVehall_22() { return &____sermouVehall_22; }
	inline void set__sermouVehall_22(float value)
	{
		____sermouVehall_22 = value;
	}

	inline static int32_t get_offset_of__gesoraVopirwow_23() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____gesoraVopirwow_23)); }
	inline int32_t get__gesoraVopirwow_23() const { return ____gesoraVopirwow_23; }
	inline int32_t* get_address_of__gesoraVopirwow_23() { return &____gesoraVopirwow_23; }
	inline void set__gesoraVopirwow_23(int32_t value)
	{
		____gesoraVopirwow_23 = value;
	}

	inline static int32_t get_offset_of__seemardear_24() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____seemardear_24)); }
	inline bool get__seemardear_24() const { return ____seemardear_24; }
	inline bool* get_address_of__seemardear_24() { return &____seemardear_24; }
	inline void set__seemardear_24(bool value)
	{
		____seemardear_24 = value;
	}

	inline static int32_t get_offset_of__gurtis_25() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____gurtis_25)); }
	inline bool get__gurtis_25() const { return ____gurtis_25; }
	inline bool* get_address_of__gurtis_25() { return &____gurtis_25; }
	inline void set__gurtis_25(bool value)
	{
		____gurtis_25 = value;
	}

	inline static int32_t get_offset_of__felbu_26() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____felbu_26)); }
	inline uint32_t get__felbu_26() const { return ____felbu_26; }
	inline uint32_t* get_address_of__felbu_26() { return &____felbu_26; }
	inline void set__felbu_26(uint32_t value)
	{
		____felbu_26 = value;
	}

	inline static int32_t get_offset_of__pirheamawKorsuki_27() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____pirheamawKorsuki_27)); }
	inline float get__pirheamawKorsuki_27() const { return ____pirheamawKorsuki_27; }
	inline float* get_address_of__pirheamawKorsuki_27() { return &____pirheamawKorsuki_27; }
	inline void set__pirheamawKorsuki_27(float value)
	{
		____pirheamawKorsuki_27 = value;
	}

	inline static int32_t get_offset_of__chunoozair_28() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____chunoozair_28)); }
	inline uint32_t get__chunoozair_28() const { return ____chunoozair_28; }
	inline uint32_t* get_address_of__chunoozair_28() { return &____chunoozair_28; }
	inline void set__chunoozair_28(uint32_t value)
	{
		____chunoozair_28 = value;
	}

	inline static int32_t get_offset_of__ceaje_29() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____ceaje_29)); }
	inline bool get__ceaje_29() const { return ____ceaje_29; }
	inline bool* get_address_of__ceaje_29() { return &____ceaje_29; }
	inline void set__ceaje_29(bool value)
	{
		____ceaje_29 = value;
	}

	inline static int32_t get_offset_of__leacerdre_30() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____leacerdre_30)); }
	inline int32_t get__leacerdre_30() const { return ____leacerdre_30; }
	inline int32_t* get_address_of__leacerdre_30() { return &____leacerdre_30; }
	inline void set__leacerdre_30(int32_t value)
	{
		____leacerdre_30 = value;
	}

	inline static int32_t get_offset_of__trugall_31() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____trugall_31)); }
	inline uint32_t get__trugall_31() const { return ____trugall_31; }
	inline uint32_t* get_address_of__trugall_31() { return &____trugall_31; }
	inline void set__trugall_31(uint32_t value)
	{
		____trugall_31 = value;
	}

	inline static int32_t get_offset_of__fairsasjou_32() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____fairsasjou_32)); }
	inline float get__fairsasjou_32() const { return ____fairsasjou_32; }
	inline float* get_address_of__fairsasjou_32() { return &____fairsasjou_32; }
	inline void set__fairsasjou_32(float value)
	{
		____fairsasjou_32 = value;
	}

	inline static int32_t get_offset_of__lujemtou_33() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____lujemtou_33)); }
	inline String_t* get__lujemtou_33() const { return ____lujemtou_33; }
	inline String_t** get_address_of__lujemtou_33() { return &____lujemtou_33; }
	inline void set__lujemtou_33(String_t* value)
	{
		____lujemtou_33 = value;
		Il2CppCodeGenWriteBarrier(&____lujemtou_33, value);
	}

	inline static int32_t get_offset_of__melrayQarera_34() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____melrayQarera_34)); }
	inline String_t* get__melrayQarera_34() const { return ____melrayQarera_34; }
	inline String_t** get_address_of__melrayQarera_34() { return &____melrayQarera_34; }
	inline void set__melrayQarera_34(String_t* value)
	{
		____melrayQarera_34 = value;
		Il2CppCodeGenWriteBarrier(&____melrayQarera_34, value);
	}

	inline static int32_t get_offset_of__danalpooBurhehay_35() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____danalpooBurhehay_35)); }
	inline float get__danalpooBurhehay_35() const { return ____danalpooBurhehay_35; }
	inline float* get_address_of__danalpooBurhehay_35() { return &____danalpooBurhehay_35; }
	inline void set__danalpooBurhehay_35(float value)
	{
		____danalpooBurhehay_35 = value;
	}

	inline static int32_t get_offset_of__melhereRogoujor_36() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____melhereRogoujor_36)); }
	inline float get__melhereRogoujor_36() const { return ____melhereRogoujor_36; }
	inline float* get_address_of__melhereRogoujor_36() { return &____melhereRogoujor_36; }
	inline void set__melhereRogoujor_36(float value)
	{
		____melhereRogoujor_36 = value;
	}

	inline static int32_t get_offset_of__doucowjay_37() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____doucowjay_37)); }
	inline float get__doucowjay_37() const { return ____doucowjay_37; }
	inline float* get_address_of__doucowjay_37() { return &____doucowjay_37; }
	inline void set__doucowjay_37(float value)
	{
		____doucowjay_37 = value;
	}

	inline static int32_t get_offset_of__nalltallsem_38() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____nalltallsem_38)); }
	inline int32_t get__nalltallsem_38() const { return ____nalltallsem_38; }
	inline int32_t* get_address_of__nalltallsem_38() { return &____nalltallsem_38; }
	inline void set__nalltallsem_38(int32_t value)
	{
		____nalltallsem_38 = value;
	}

	inline static int32_t get_offset_of__kisdrifaReetou_39() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____kisdrifaReetou_39)); }
	inline uint32_t get__kisdrifaReetou_39() const { return ____kisdrifaReetou_39; }
	inline uint32_t* get_address_of__kisdrifaReetou_39() { return &____kisdrifaReetou_39; }
	inline void set__kisdrifaReetou_39(uint32_t value)
	{
		____kisdrifaReetou_39 = value;
	}

	inline static int32_t get_offset_of__temramayNarpa_40() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____temramayNarpa_40)); }
	inline float get__temramayNarpa_40() const { return ____temramayNarpa_40; }
	inline float* get_address_of__temramayNarpa_40() { return &____temramayNarpa_40; }
	inline void set__temramayNarpa_40(float value)
	{
		____temramayNarpa_40 = value;
	}

	inline static int32_t get_offset_of__driskiceSerbis_41() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____driskiceSerbis_41)); }
	inline uint32_t get__driskiceSerbis_41() const { return ____driskiceSerbis_41; }
	inline uint32_t* get_address_of__driskiceSerbis_41() { return &____driskiceSerbis_41; }
	inline void set__driskiceSerbis_41(uint32_t value)
	{
		____driskiceSerbis_41 = value;
	}

	inline static int32_t get_offset_of__staltru_42() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____staltru_42)); }
	inline float get__staltru_42() const { return ____staltru_42; }
	inline float* get_address_of__staltru_42() { return &____staltru_42; }
	inline void set__staltru_42(float value)
	{
		____staltru_42 = value;
	}

	inline static int32_t get_offset_of__zustisair_43() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____zustisair_43)); }
	inline float get__zustisair_43() const { return ____zustisair_43; }
	inline float* get_address_of__zustisair_43() { return &____zustisair_43; }
	inline void set__zustisair_43(float value)
	{
		____zustisair_43 = value;
	}

	inline static int32_t get_offset_of__serebemsisCorvow_44() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____serebemsisCorvow_44)); }
	inline String_t* get__serebemsisCorvow_44() const { return ____serebemsisCorvow_44; }
	inline String_t** get_address_of__serebemsisCorvow_44() { return &____serebemsisCorvow_44; }
	inline void set__serebemsisCorvow_44(String_t* value)
	{
		____serebemsisCorvow_44 = value;
		Il2CppCodeGenWriteBarrier(&____serebemsisCorvow_44, value);
	}

	inline static int32_t get_offset_of__sadeMawce_45() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____sadeMawce_45)); }
	inline float get__sadeMawce_45() const { return ____sadeMawce_45; }
	inline float* get_address_of__sadeMawce_45() { return &____sadeMawce_45; }
	inline void set__sadeMawce_45(float value)
	{
		____sadeMawce_45 = value;
	}

	inline static int32_t get_offset_of__vaiseNelhall_46() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____vaiseNelhall_46)); }
	inline uint32_t get__vaiseNelhall_46() const { return ____vaiseNelhall_46; }
	inline uint32_t* get_address_of__vaiseNelhall_46() { return &____vaiseNelhall_46; }
	inline void set__vaiseNelhall_46(uint32_t value)
	{
		____vaiseNelhall_46 = value;
	}

	inline static int32_t get_offset_of__cawferNorjirci_47() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____cawferNorjirci_47)); }
	inline float get__cawferNorjirci_47() const { return ____cawferNorjirci_47; }
	inline float* get_address_of__cawferNorjirci_47() { return &____cawferNorjirci_47; }
	inline void set__cawferNorjirci_47(float value)
	{
		____cawferNorjirci_47 = value;
	}

	inline static int32_t get_offset_of__mounasYasir_48() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____mounasYasir_48)); }
	inline int32_t get__mounasYasir_48() const { return ____mounasYasir_48; }
	inline int32_t* get_address_of__mounasYasir_48() { return &____mounasYasir_48; }
	inline void set__mounasYasir_48(int32_t value)
	{
		____mounasYasir_48 = value;
	}

	inline static int32_t get_offset_of__guta_49() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____guta_49)); }
	inline float get__guta_49() const { return ____guta_49; }
	inline float* get_address_of__guta_49() { return &____guta_49; }
	inline void set__guta_49(float value)
	{
		____guta_49 = value;
	}

	inline static int32_t get_offset_of__murrairray_50() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____murrairray_50)); }
	inline int32_t get__murrairray_50() const { return ____murrairray_50; }
	inline int32_t* get_address_of__murrairray_50() { return &____murrairray_50; }
	inline void set__murrairray_50(int32_t value)
	{
		____murrairray_50 = value;
	}

	inline static int32_t get_offset_of__piwurDalji_51() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____piwurDalji_51)); }
	inline String_t* get__piwurDalji_51() const { return ____piwurDalji_51; }
	inline String_t** get_address_of__piwurDalji_51() { return &____piwurDalji_51; }
	inline void set__piwurDalji_51(String_t* value)
	{
		____piwurDalji_51 = value;
		Il2CppCodeGenWriteBarrier(&____piwurDalji_51, value);
	}

	inline static int32_t get_offset_of__pesalnouMepa_52() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____pesalnouMepa_52)); }
	inline uint32_t get__pesalnouMepa_52() const { return ____pesalnouMepa_52; }
	inline uint32_t* get_address_of__pesalnouMepa_52() { return &____pesalnouMepa_52; }
	inline void set__pesalnouMepa_52(uint32_t value)
	{
		____pesalnouMepa_52 = value;
	}

	inline static int32_t get_offset_of__jacartral_53() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____jacartral_53)); }
	inline float get__jacartral_53() const { return ____jacartral_53; }
	inline float* get_address_of__jacartral_53() { return &____jacartral_53; }
	inline void set__jacartral_53(float value)
	{
		____jacartral_53 = value;
	}

	inline static int32_t get_offset_of__doupeJizorsu_54() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____doupeJizorsu_54)); }
	inline bool get__doupeJizorsu_54() const { return ____doupeJizorsu_54; }
	inline bool* get_address_of__doupeJizorsu_54() { return &____doupeJizorsu_54; }
	inline void set__doupeJizorsu_54(bool value)
	{
		____doupeJizorsu_54 = value;
	}

	inline static int32_t get_offset_of__noojervar_55() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____noojervar_55)); }
	inline String_t* get__noojervar_55() const { return ____noojervar_55; }
	inline String_t** get_address_of__noojervar_55() { return &____noojervar_55; }
	inline void set__noojervar_55(String_t* value)
	{
		____noojervar_55 = value;
		Il2CppCodeGenWriteBarrier(&____noojervar_55, value);
	}

	inline static int32_t get_offset_of__locooner_56() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____locooner_56)); }
	inline String_t* get__locooner_56() const { return ____locooner_56; }
	inline String_t** get_address_of__locooner_56() { return &____locooner_56; }
	inline void set__locooner_56(String_t* value)
	{
		____locooner_56 = value;
		Il2CppCodeGenWriteBarrier(&____locooner_56, value);
	}

	inline static int32_t get_offset_of__lesaChidowdou_57() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____lesaChidowdou_57)); }
	inline float get__lesaChidowdou_57() const { return ____lesaChidowdou_57; }
	inline float* get_address_of__lesaChidowdou_57() { return &____lesaChidowdou_57; }
	inline void set__lesaChidowdou_57(float value)
	{
		____lesaChidowdou_57 = value;
	}

	inline static int32_t get_offset_of__feedeCewheasea_58() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____feedeCewheasea_58)); }
	inline String_t* get__feedeCewheasea_58() const { return ____feedeCewheasea_58; }
	inline String_t** get_address_of__feedeCewheasea_58() { return &____feedeCewheasea_58; }
	inline void set__feedeCewheasea_58(String_t* value)
	{
		____feedeCewheasea_58 = value;
		Il2CppCodeGenWriteBarrier(&____feedeCewheasea_58, value);
	}

	inline static int32_t get_offset_of__sarai_59() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____sarai_59)); }
	inline uint32_t get__sarai_59() const { return ____sarai_59; }
	inline uint32_t* get_address_of__sarai_59() { return &____sarai_59; }
	inline void set__sarai_59(uint32_t value)
	{
		____sarai_59 = value;
	}

	inline static int32_t get_offset_of__hajelni_60() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____hajelni_60)); }
	inline int32_t get__hajelni_60() const { return ____hajelni_60; }
	inline int32_t* get_address_of__hajelni_60() { return &____hajelni_60; }
	inline void set__hajelni_60(int32_t value)
	{
		____hajelni_60 = value;
	}

	inline static int32_t get_offset_of__pawtas_61() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____pawtas_61)); }
	inline int32_t get__pawtas_61() const { return ____pawtas_61; }
	inline int32_t* get_address_of__pawtas_61() { return &____pawtas_61; }
	inline void set__pawtas_61(int32_t value)
	{
		____pawtas_61 = value;
	}

	inline static int32_t get_offset_of__vetasSairnurtair_62() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____vetasSairnurtair_62)); }
	inline bool get__vetasSairnurtair_62() const { return ____vetasSairnurtair_62; }
	inline bool* get_address_of__vetasSairnurtair_62() { return &____vetasSairnurtair_62; }
	inline void set__vetasSairnurtair_62(bool value)
	{
		____vetasSairnurtair_62 = value;
	}

	inline static int32_t get_offset_of__nurmisball_63() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____nurmisball_63)); }
	inline bool get__nurmisball_63() const { return ____nurmisball_63; }
	inline bool* get_address_of__nurmisball_63() { return &____nurmisball_63; }
	inline void set__nurmisball_63(bool value)
	{
		____nurmisball_63 = value;
	}

	inline static int32_t get_offset_of__feretraDrisjoo_64() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____feretraDrisjoo_64)); }
	inline int32_t get__feretraDrisjoo_64() const { return ____feretraDrisjoo_64; }
	inline int32_t* get_address_of__feretraDrisjoo_64() { return &____feretraDrisjoo_64; }
	inline void set__feretraDrisjoo_64(int32_t value)
	{
		____feretraDrisjoo_64 = value;
	}

	inline static int32_t get_offset_of__sallmisjaZownayka_65() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____sallmisjaZownayka_65)); }
	inline float get__sallmisjaZownayka_65() const { return ____sallmisjaZownayka_65; }
	inline float* get_address_of__sallmisjaZownayka_65() { return &____sallmisjaZownayka_65; }
	inline void set__sallmisjaZownayka_65(float value)
	{
		____sallmisjaZownayka_65 = value;
	}

	inline static int32_t get_offset_of__beartrastoDrooseebe_66() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____beartrastoDrooseebe_66)); }
	inline bool get__beartrastoDrooseebe_66() const { return ____beartrastoDrooseebe_66; }
	inline bool* get_address_of__beartrastoDrooseebe_66() { return &____beartrastoDrooseebe_66; }
	inline void set__beartrastoDrooseebe_66(bool value)
	{
		____beartrastoDrooseebe_66 = value;
	}

	inline static int32_t get_offset_of__gemzereKirsoo_67() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____gemzereKirsoo_67)); }
	inline String_t* get__gemzereKirsoo_67() const { return ____gemzereKirsoo_67; }
	inline String_t** get_address_of__gemzereKirsoo_67() { return &____gemzereKirsoo_67; }
	inline void set__gemzereKirsoo_67(String_t* value)
	{
		____gemzereKirsoo_67 = value;
		Il2CppCodeGenWriteBarrier(&____gemzereKirsoo_67, value);
	}

	inline static int32_t get_offset_of__dearmuqur_68() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____dearmuqur_68)); }
	inline float get__dearmuqur_68() const { return ____dearmuqur_68; }
	inline float* get_address_of__dearmuqur_68() { return &____dearmuqur_68; }
	inline void set__dearmuqur_68(float value)
	{
		____dearmuqur_68 = value;
	}

	inline static int32_t get_offset_of__yoosajall_69() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____yoosajall_69)); }
	inline int32_t get__yoosajall_69() const { return ____yoosajall_69; }
	inline int32_t* get_address_of__yoosajall_69() { return &____yoosajall_69; }
	inline void set__yoosajall_69(int32_t value)
	{
		____yoosajall_69 = value;
	}

	inline static int32_t get_offset_of__pasteasarQowear_70() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____pasteasarQowear_70)); }
	inline float get__pasteasarQowear_70() const { return ____pasteasarQowear_70; }
	inline float* get_address_of__pasteasarQowear_70() { return &____pasteasarQowear_70; }
	inline void set__pasteasarQowear_70(float value)
	{
		____pasteasarQowear_70 = value;
	}

	inline static int32_t get_offset_of__jeeqay_71() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____jeeqay_71)); }
	inline float get__jeeqay_71() const { return ____jeeqay_71; }
	inline float* get_address_of__jeeqay_71() { return &____jeeqay_71; }
	inline void set__jeeqay_71(float value)
	{
		____jeeqay_71 = value;
	}

	inline static int32_t get_offset_of__delwhe_72() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____delwhe_72)); }
	inline bool get__delwhe_72() const { return ____delwhe_72; }
	inline bool* get_address_of__delwhe_72() { return &____delwhe_72; }
	inline void set__delwhe_72(bool value)
	{
		____delwhe_72 = value;
	}

	inline static int32_t get_offset_of__selqirtorHika_73() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____selqirtorHika_73)); }
	inline uint32_t get__selqirtorHika_73() const { return ____selqirtorHika_73; }
	inline uint32_t* get_address_of__selqirtorHika_73() { return &____selqirtorHika_73; }
	inline void set__selqirtorHika_73(uint32_t value)
	{
		____selqirtorHika_73 = value;
	}

	inline static int32_t get_offset_of__neesu_74() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____neesu_74)); }
	inline float get__neesu_74() const { return ____neesu_74; }
	inline float* get_address_of__neesu_74() { return &____neesu_74; }
	inline void set__neesu_74(float value)
	{
		____neesu_74 = value;
	}

	inline static int32_t get_offset_of__jarsooJalbi_75() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____jarsooJalbi_75)); }
	inline uint32_t get__jarsooJalbi_75() const { return ____jarsooJalbi_75; }
	inline uint32_t* get_address_of__jarsooJalbi_75() { return &____jarsooJalbi_75; }
	inline void set__jarsooJalbi_75(uint32_t value)
	{
		____jarsooJalbi_75 = value;
	}

	inline static int32_t get_offset_of__qurbere_76() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____qurbere_76)); }
	inline String_t* get__qurbere_76() const { return ____qurbere_76; }
	inline String_t** get_address_of__qurbere_76() { return &____qurbere_76; }
	inline void set__qurbere_76(String_t* value)
	{
		____qurbere_76 = value;
		Il2CppCodeGenWriteBarrier(&____qurbere_76, value);
	}

	inline static int32_t get_offset_of__zallnejouJairha_77() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____zallnejouJairha_77)); }
	inline int32_t get__zallnejouJairha_77() const { return ____zallnejouJairha_77; }
	inline int32_t* get_address_of__zallnejouJairha_77() { return &____zallnejouJairha_77; }
	inline void set__zallnejouJairha_77(int32_t value)
	{
		____zallnejouJairha_77 = value;
	}

	inline static int32_t get_offset_of__heedajiWorrow_78() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____heedajiWorrow_78)); }
	inline String_t* get__heedajiWorrow_78() const { return ____heedajiWorrow_78; }
	inline String_t** get_address_of__heedajiWorrow_78() { return &____heedajiWorrow_78; }
	inline void set__heedajiWorrow_78(String_t* value)
	{
		____heedajiWorrow_78 = value;
		Il2CppCodeGenWriteBarrier(&____heedajiWorrow_78, value);
	}

	inline static int32_t get_offset_of__naipisHemconir_79() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____naipisHemconir_79)); }
	inline String_t* get__naipisHemconir_79() const { return ____naipisHemconir_79; }
	inline String_t** get_address_of__naipisHemconir_79() { return &____naipisHemconir_79; }
	inline void set__naipisHemconir_79(String_t* value)
	{
		____naipisHemconir_79 = value;
		Il2CppCodeGenWriteBarrier(&____naipisHemconir_79, value);
	}

	inline static int32_t get_offset_of__losarjay_80() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____losarjay_80)); }
	inline uint32_t get__losarjay_80() const { return ____losarjay_80; }
	inline uint32_t* get_address_of__losarjay_80() { return &____losarjay_80; }
	inline void set__losarjay_80(uint32_t value)
	{
		____losarjay_80 = value;
	}

	inline static int32_t get_offset_of__morfufowMinuro_81() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____morfufowMinuro_81)); }
	inline int32_t get__morfufowMinuro_81() const { return ____morfufowMinuro_81; }
	inline int32_t* get_address_of__morfufowMinuro_81() { return &____morfufowMinuro_81; }
	inline void set__morfufowMinuro_81(int32_t value)
	{
		____morfufowMinuro_81 = value;
	}

	inline static int32_t get_offset_of__bembedowDayger_82() { return static_cast<int32_t>(offsetof(M_stawnofeMelgallmou12_t433472909, ____bembedowDayger_82)); }
	inline uint32_t get__bembedowDayger_82() const { return ____bembedowDayger_82; }
	inline uint32_t* get_address_of__bembedowDayger_82() { return &____bembedowDayger_82; }
	inline void set__bembedowDayger_82(uint32_t value)
	{
		____bembedowDayger_82 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
