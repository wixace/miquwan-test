﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GhostItem
struct GhostItem_t218864162;

#include "codegen/il2cpp-codegen.h"

// System.Void GhostItem::.ctor()
extern "C"  void GhostItem__ctor_m2352857481 (GhostItem_t218864162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GhostItem::Update()
extern "C"  void GhostItem_Update_m1650999972 (GhostItem_t218864162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
