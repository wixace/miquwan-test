﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_dicawWeljalpo173
struct  M_dicawWeljalpo173_t904065107  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_dicawWeljalpo173::_noola
	String_t* ____noola_0;
	// System.String GarbageiOS.M_dicawWeljalpo173::_sourejeYejall
	String_t* ____sourejeYejall_1;
	// System.Single GarbageiOS.M_dicawWeljalpo173::_cerhaw
	float ____cerhaw_2;
	// System.Int32 GarbageiOS.M_dicawWeljalpo173::_zairbor
	int32_t ____zairbor_3;
	// System.String GarbageiOS.M_dicawWeljalpo173::_jelrooTowdemje
	String_t* ____jelrooTowdemje_4;
	// System.String GarbageiOS.M_dicawWeljalpo173::_voodre
	String_t* ____voodre_5;

public:
	inline static int32_t get_offset_of__noola_0() { return static_cast<int32_t>(offsetof(M_dicawWeljalpo173_t904065107, ____noola_0)); }
	inline String_t* get__noola_0() const { return ____noola_0; }
	inline String_t** get_address_of__noola_0() { return &____noola_0; }
	inline void set__noola_0(String_t* value)
	{
		____noola_0 = value;
		Il2CppCodeGenWriteBarrier(&____noola_0, value);
	}

	inline static int32_t get_offset_of__sourejeYejall_1() { return static_cast<int32_t>(offsetof(M_dicawWeljalpo173_t904065107, ____sourejeYejall_1)); }
	inline String_t* get__sourejeYejall_1() const { return ____sourejeYejall_1; }
	inline String_t** get_address_of__sourejeYejall_1() { return &____sourejeYejall_1; }
	inline void set__sourejeYejall_1(String_t* value)
	{
		____sourejeYejall_1 = value;
		Il2CppCodeGenWriteBarrier(&____sourejeYejall_1, value);
	}

	inline static int32_t get_offset_of__cerhaw_2() { return static_cast<int32_t>(offsetof(M_dicawWeljalpo173_t904065107, ____cerhaw_2)); }
	inline float get__cerhaw_2() const { return ____cerhaw_2; }
	inline float* get_address_of__cerhaw_2() { return &____cerhaw_2; }
	inline void set__cerhaw_2(float value)
	{
		____cerhaw_2 = value;
	}

	inline static int32_t get_offset_of__zairbor_3() { return static_cast<int32_t>(offsetof(M_dicawWeljalpo173_t904065107, ____zairbor_3)); }
	inline int32_t get__zairbor_3() const { return ____zairbor_3; }
	inline int32_t* get_address_of__zairbor_3() { return &____zairbor_3; }
	inline void set__zairbor_3(int32_t value)
	{
		____zairbor_3 = value;
	}

	inline static int32_t get_offset_of__jelrooTowdemje_4() { return static_cast<int32_t>(offsetof(M_dicawWeljalpo173_t904065107, ____jelrooTowdemje_4)); }
	inline String_t* get__jelrooTowdemje_4() const { return ____jelrooTowdemje_4; }
	inline String_t** get_address_of__jelrooTowdemje_4() { return &____jelrooTowdemje_4; }
	inline void set__jelrooTowdemje_4(String_t* value)
	{
		____jelrooTowdemje_4 = value;
		Il2CppCodeGenWriteBarrier(&____jelrooTowdemje_4, value);
	}

	inline static int32_t get_offset_of__voodre_5() { return static_cast<int32_t>(offsetof(M_dicawWeljalpo173_t904065107, ____voodre_5)); }
	inline String_t* get__voodre_5() const { return ____voodre_5; }
	inline String_t** get_address_of__voodre_5() { return &____voodre_5; }
	inline void set__voodre_5(String_t* value)
	{
		____voodre_5 = value;
		Il2CppCodeGenWriteBarrier(&____voodre_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
