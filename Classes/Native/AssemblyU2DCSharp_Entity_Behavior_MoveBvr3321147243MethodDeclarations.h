﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.MoveBvr
struct MoveBvr_t3321147243;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// CombatEntity
struct CombatEntity_t684137495;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// GameMgr
struct GameMgr_t1469029542;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "System_Core_System_Action3771233898.h"

// System.Void Entity.Behavior.MoveBvr::.ctor()
extern "C"  void MoveBvr__ctor_m1059735279 (MoveBvr_t3321147243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.MoveBvr::get_id()
extern "C"  uint8_t MoveBvr_get_id_m2062087719 (MoveBvr_t3321147243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.MoveBvr::Reason()
extern "C"  void MoveBvr_Reason_m2338410681 (MoveBvr_t3321147243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.MoveBvr::Action()
extern "C"  void MoveBvr_Action_m1535117355 (MoveBvr_t3321147243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.MoveBvr::DoEntering()
extern "C"  void MoveBvr_DoEntering_m3650061290 (MoveBvr_t3321147243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.MoveBvr::SetParams(System.Object[])
extern "C"  void MoveBvr_SetParams_m3485500029 (MoveBvr_t3321147243 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.MoveBvr::DoLeaving()
extern "C"  void MoveBvr_DoLeaving_m2363769462 (MoveBvr_t3321147243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.MoveBvr::Pause()
extern "C"  void MoveBvr_Pause_m1113861251 (MoveBvr_t3321147243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.MoveBvr::Play()
extern "C"  void MoveBvr_Play_m3509181097 (MoveBvr_t3321147243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.MoveBvr::ilo_get_entity1(Entity.Behavior.IBehavior)
extern "C"  CombatEntity_t684137495 * MoveBvr_ilo_get_entity1_m4284887331 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.MoveBvr::ilo_DispatchEvent2(CEvent.ZEvent)
extern "C"  void MoveBvr_ilo_DispatchEvent2_m4229397409 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.MoveBvr::ilo_TranBehavior3(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void MoveBvr_ilo_TranBehavior3_m1057368228 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___id1, ObjectU5BU5D_t1108656482* ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Entity.Behavior.MoveBvr::ilo_get_isBattleStart4(GameMgr)
extern "C"  bool MoveBvr_ilo_get_isBattleStart4_m3528243099 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.MoveBvr::ilo_UpdateTargetPos5(CombatEntity,UnityEngine.Vector3,System.Action)
extern "C"  void MoveBvr_ilo_UpdateTargetPos5_m2042720310 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, Vector3_t4282066566  ___targetPos1, Action_t3771233898 * ___OnTargetCall2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.MoveBvr::ilo_Play6(Entity.Behavior.IBehavior)
extern "C"  void MoveBvr_ilo_Play6_m1399546988 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
