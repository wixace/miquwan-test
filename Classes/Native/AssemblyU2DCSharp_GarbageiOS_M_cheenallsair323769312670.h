﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_cheenallsair32
struct  M_cheenallsair32_t3769312670  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_cheenallsair32::_dairtearjasMarser
	float ____dairtearjasMarser_0;
	// System.String GarbageiOS.M_cheenallsair32::_tahar
	String_t* ____tahar_1;
	// System.Single GarbageiOS.M_cheenallsair32::_tikiNonaw
	float ____tikiNonaw_2;
	// System.Single GarbageiOS.M_cheenallsair32::_stayjona
	float ____stayjona_3;
	// System.Int32 GarbageiOS.M_cheenallsair32::_nuhearBearfearpu
	int32_t ____nuhearBearfearpu_4;
	// System.Int32 GarbageiOS.M_cheenallsair32::_cohairBairri
	int32_t ____cohairBairri_5;
	// System.String GarbageiOS.M_cheenallsair32::_stousoobas
	String_t* ____stousoobas_6;
	// System.Single GarbageiOS.M_cheenallsair32::_whirroWheze
	float ____whirroWheze_7;
	// System.Boolean GarbageiOS.M_cheenallsair32::_chassawru
	bool ____chassawru_8;
	// System.Single GarbageiOS.M_cheenallsair32::_vaygallkee
	float ____vaygallkee_9;
	// System.UInt32 GarbageiOS.M_cheenallsair32::_noobea
	uint32_t ____noobea_10;
	// System.String GarbageiOS.M_cheenallsair32::_tacha
	String_t* ____tacha_11;
	// System.Int32 GarbageiOS.M_cheenallsair32::_pemgai
	int32_t ____pemgai_12;
	// System.UInt32 GarbageiOS.M_cheenallsair32::_xakai
	uint32_t ____xakai_13;
	// System.Int32 GarbageiOS.M_cheenallsair32::_douke
	int32_t ____douke_14;
	// System.Single GarbageiOS.M_cheenallsair32::_halli
	float ____halli_15;
	// System.UInt32 GarbageiOS.M_cheenallsair32::_sercawdri
	uint32_t ____sercawdri_16;
	// System.Single GarbageiOS.M_cheenallsair32::_zalpelWhehe
	float ____zalpelWhehe_17;
	// System.Boolean GarbageiOS.M_cheenallsair32::_merebujowRucemjor
	bool ____merebujowRucemjor_18;
	// System.Boolean GarbageiOS.M_cheenallsair32::_wodoYemi
	bool ____wodoYemi_19;
	// System.String GarbageiOS.M_cheenallsair32::_telvaw
	String_t* ____telvaw_20;
	// System.Int32 GarbageiOS.M_cheenallsair32::_tredri
	int32_t ____tredri_21;
	// System.Single GarbageiOS.M_cheenallsair32::_vurnalForrowter
	float ____vurnalForrowter_22;
	// System.Boolean GarbageiOS.M_cheenallsair32::_coujallMawcawcall
	bool ____coujallMawcawcall_23;
	// System.Single GarbageiOS.M_cheenallsair32::_morlal
	float ____morlal_24;
	// System.UInt32 GarbageiOS.M_cheenallsair32::_dawtouLaije
	uint32_t ____dawtouLaije_25;
	// System.Int32 GarbageiOS.M_cheenallsair32::_wajaysurXerbar
	int32_t ____wajaysurXerbar_26;
	// System.UInt32 GarbageiOS.M_cheenallsair32::_pemcecaw
	uint32_t ____pemcecaw_27;
	// System.UInt32 GarbageiOS.M_cheenallsair32::_kisxuree
	uint32_t ____kisxuree_28;
	// System.UInt32 GarbageiOS.M_cheenallsair32::_jasurTusaybo
	uint32_t ____jasurTusaybo_29;
	// System.Single GarbageiOS.M_cheenallsair32::_whisuye
	float ____whisuye_30;
	// System.UInt32 GarbageiOS.M_cheenallsair32::_qairwalCearfoolear
	uint32_t ____qairwalCearfoolear_31;
	// System.Int32 GarbageiOS.M_cheenallsair32::_berehel
	int32_t ____berehel_32;
	// System.Int32 GarbageiOS.M_cheenallsair32::_nesisMispanor
	int32_t ____nesisMispanor_33;
	// System.Single GarbageiOS.M_cheenallsair32::_nefallCurye
	float ____nefallCurye_34;
	// System.Int32 GarbageiOS.M_cheenallsair32::_tawkexereMumecu
	int32_t ____tawkexereMumecu_35;
	// System.Single GarbageiOS.M_cheenallsair32::_cakitiXawhayza
	float ____cakitiXawhayza_36;
	// System.Int32 GarbageiOS.M_cheenallsair32::_jipalkaWoraw
	int32_t ____jipalkaWoraw_37;
	// System.Single GarbageiOS.M_cheenallsair32::_lisxirci
	float ____lisxirci_38;
	// System.Int32 GarbageiOS.M_cheenallsair32::_poonookaSerejis
	int32_t ____poonookaSerejis_39;
	// System.Boolean GarbageiOS.M_cheenallsair32::_pourunay
	bool ____pourunay_40;
	// System.Single GarbageiOS.M_cheenallsair32::_poxu
	float ____poxu_41;
	// System.UInt32 GarbageiOS.M_cheenallsair32::_beartremnoo
	uint32_t ____beartremnoo_42;
	// System.Int32 GarbageiOS.M_cheenallsair32::_jejerbaGiwallbel
	int32_t ____jejerbaGiwallbel_43;
	// System.Boolean GarbageiOS.M_cheenallsair32::_cataPelbefa
	bool ____cataPelbefa_44;
	// System.Single GarbageiOS.M_cheenallsair32::_carjo
	float ____carjo_45;
	// System.UInt32 GarbageiOS.M_cheenallsair32::_haicair
	uint32_t ____haicair_46;
	// System.UInt32 GarbageiOS.M_cheenallsair32::_lertetaSedou
	uint32_t ____lertetaSedou_47;
	// System.Int32 GarbageiOS.M_cheenallsair32::_gide
	int32_t ____gide_48;
	// System.Single GarbageiOS.M_cheenallsair32::_teasiyeFigir
	float ____teasiyeFigir_49;
	// System.Single GarbageiOS.M_cheenallsair32::_zostai
	float ____zostai_50;
	// System.Single GarbageiOS.M_cheenallsair32::_genoore
	float ____genoore_51;
	// System.UInt32 GarbageiOS.M_cheenallsair32::_heremis
	uint32_t ____heremis_52;
	// System.Boolean GarbageiOS.M_cheenallsair32::_wheruZabee
	bool ____wheruZabee_53;
	// System.Int32 GarbageiOS.M_cheenallsair32::_jerwalfaWaluse
	int32_t ____jerwalfaWaluse_54;
	// System.Int32 GarbageiOS.M_cheenallsair32::_mearsapi
	int32_t ____mearsapi_55;
	// System.Int32 GarbageiOS.M_cheenallsair32::_raltrerWupel
	int32_t ____raltrerWupel_56;
	// System.Int32 GarbageiOS.M_cheenallsair32::_cedrefair
	int32_t ____cedrefair_57;
	// System.Single GarbageiOS.M_cheenallsair32::_dalnurberPixi
	float ____dalnurberPixi_58;
	// System.UInt32 GarbageiOS.M_cheenallsair32::_mounugu
	uint32_t ____mounugu_59;
	// System.UInt32 GarbageiOS.M_cheenallsair32::_dererouSormairnu
	uint32_t ____dererouSormairnu_60;
	// System.Int32 GarbageiOS.M_cheenallsair32::_beejou
	int32_t ____beejou_61;
	// System.UInt32 GarbageiOS.M_cheenallsair32::_sofeeRole
	uint32_t ____sofeeRole_62;
	// System.Single GarbageiOS.M_cheenallsair32::_raysaszalCeteepor
	float ____raysaszalCeteepor_63;
	// System.UInt32 GarbageiOS.M_cheenallsair32::_saldrirjar
	uint32_t ____saldrirjar_64;
	// System.String GarbageiOS.M_cheenallsair32::_sasteamay
	String_t* ____sasteamay_65;
	// System.Int32 GarbageiOS.M_cheenallsair32::_hucaraTuve
	int32_t ____hucaraTuve_66;
	// System.Single GarbageiOS.M_cheenallsair32::_riskaslea
	float ____riskaslea_67;
	// System.Int32 GarbageiOS.M_cheenallsair32::_qeredembearBallcaw
	int32_t ____qeredembearBallcaw_68;
	// System.UInt32 GarbageiOS.M_cheenallsair32::_qursico
	uint32_t ____qursico_69;
	// System.Boolean GarbageiOS.M_cheenallsair32::_jahal
	bool ____jahal_70;
	// System.Boolean GarbageiOS.M_cheenallsair32::_drupouChawne
	bool ____drupouChawne_71;
	// System.Int32 GarbageiOS.M_cheenallsair32::_nelsorwur
	int32_t ____nelsorwur_72;
	// System.String GarbageiOS.M_cheenallsair32::_tideamem
	String_t* ____tideamem_73;
	// System.UInt32 GarbageiOS.M_cheenallsair32::_pafaircea
	uint32_t ____pafaircea_74;
	// System.Int32 GarbageiOS.M_cheenallsair32::_deafirMemmir
	int32_t ____deafirMemmir_75;
	// System.Int32 GarbageiOS.M_cheenallsair32::_nusu
	int32_t ____nusu_76;
	// System.Boolean GarbageiOS.M_cheenallsair32::_talltroodiNaci
	bool ____talltroodiNaci_77;
	// System.Boolean GarbageiOS.M_cheenallsair32::_tajacha
	bool ____tajacha_78;

public:
	inline static int32_t get_offset_of__dairtearjasMarser_0() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____dairtearjasMarser_0)); }
	inline float get__dairtearjasMarser_0() const { return ____dairtearjasMarser_0; }
	inline float* get_address_of__dairtearjasMarser_0() { return &____dairtearjasMarser_0; }
	inline void set__dairtearjasMarser_0(float value)
	{
		____dairtearjasMarser_0 = value;
	}

	inline static int32_t get_offset_of__tahar_1() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____tahar_1)); }
	inline String_t* get__tahar_1() const { return ____tahar_1; }
	inline String_t** get_address_of__tahar_1() { return &____tahar_1; }
	inline void set__tahar_1(String_t* value)
	{
		____tahar_1 = value;
		Il2CppCodeGenWriteBarrier(&____tahar_1, value);
	}

	inline static int32_t get_offset_of__tikiNonaw_2() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____tikiNonaw_2)); }
	inline float get__tikiNonaw_2() const { return ____tikiNonaw_2; }
	inline float* get_address_of__tikiNonaw_2() { return &____tikiNonaw_2; }
	inline void set__tikiNonaw_2(float value)
	{
		____tikiNonaw_2 = value;
	}

	inline static int32_t get_offset_of__stayjona_3() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____stayjona_3)); }
	inline float get__stayjona_3() const { return ____stayjona_3; }
	inline float* get_address_of__stayjona_3() { return &____stayjona_3; }
	inline void set__stayjona_3(float value)
	{
		____stayjona_3 = value;
	}

	inline static int32_t get_offset_of__nuhearBearfearpu_4() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____nuhearBearfearpu_4)); }
	inline int32_t get__nuhearBearfearpu_4() const { return ____nuhearBearfearpu_4; }
	inline int32_t* get_address_of__nuhearBearfearpu_4() { return &____nuhearBearfearpu_4; }
	inline void set__nuhearBearfearpu_4(int32_t value)
	{
		____nuhearBearfearpu_4 = value;
	}

	inline static int32_t get_offset_of__cohairBairri_5() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____cohairBairri_5)); }
	inline int32_t get__cohairBairri_5() const { return ____cohairBairri_5; }
	inline int32_t* get_address_of__cohairBairri_5() { return &____cohairBairri_5; }
	inline void set__cohairBairri_5(int32_t value)
	{
		____cohairBairri_5 = value;
	}

	inline static int32_t get_offset_of__stousoobas_6() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____stousoobas_6)); }
	inline String_t* get__stousoobas_6() const { return ____stousoobas_6; }
	inline String_t** get_address_of__stousoobas_6() { return &____stousoobas_6; }
	inline void set__stousoobas_6(String_t* value)
	{
		____stousoobas_6 = value;
		Il2CppCodeGenWriteBarrier(&____stousoobas_6, value);
	}

	inline static int32_t get_offset_of__whirroWheze_7() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____whirroWheze_7)); }
	inline float get__whirroWheze_7() const { return ____whirroWheze_7; }
	inline float* get_address_of__whirroWheze_7() { return &____whirroWheze_7; }
	inline void set__whirroWheze_7(float value)
	{
		____whirroWheze_7 = value;
	}

	inline static int32_t get_offset_of__chassawru_8() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____chassawru_8)); }
	inline bool get__chassawru_8() const { return ____chassawru_8; }
	inline bool* get_address_of__chassawru_8() { return &____chassawru_8; }
	inline void set__chassawru_8(bool value)
	{
		____chassawru_8 = value;
	}

	inline static int32_t get_offset_of__vaygallkee_9() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____vaygallkee_9)); }
	inline float get__vaygallkee_9() const { return ____vaygallkee_9; }
	inline float* get_address_of__vaygallkee_9() { return &____vaygallkee_9; }
	inline void set__vaygallkee_9(float value)
	{
		____vaygallkee_9 = value;
	}

	inline static int32_t get_offset_of__noobea_10() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____noobea_10)); }
	inline uint32_t get__noobea_10() const { return ____noobea_10; }
	inline uint32_t* get_address_of__noobea_10() { return &____noobea_10; }
	inline void set__noobea_10(uint32_t value)
	{
		____noobea_10 = value;
	}

	inline static int32_t get_offset_of__tacha_11() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____tacha_11)); }
	inline String_t* get__tacha_11() const { return ____tacha_11; }
	inline String_t** get_address_of__tacha_11() { return &____tacha_11; }
	inline void set__tacha_11(String_t* value)
	{
		____tacha_11 = value;
		Il2CppCodeGenWriteBarrier(&____tacha_11, value);
	}

	inline static int32_t get_offset_of__pemgai_12() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____pemgai_12)); }
	inline int32_t get__pemgai_12() const { return ____pemgai_12; }
	inline int32_t* get_address_of__pemgai_12() { return &____pemgai_12; }
	inline void set__pemgai_12(int32_t value)
	{
		____pemgai_12 = value;
	}

	inline static int32_t get_offset_of__xakai_13() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____xakai_13)); }
	inline uint32_t get__xakai_13() const { return ____xakai_13; }
	inline uint32_t* get_address_of__xakai_13() { return &____xakai_13; }
	inline void set__xakai_13(uint32_t value)
	{
		____xakai_13 = value;
	}

	inline static int32_t get_offset_of__douke_14() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____douke_14)); }
	inline int32_t get__douke_14() const { return ____douke_14; }
	inline int32_t* get_address_of__douke_14() { return &____douke_14; }
	inline void set__douke_14(int32_t value)
	{
		____douke_14 = value;
	}

	inline static int32_t get_offset_of__halli_15() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____halli_15)); }
	inline float get__halli_15() const { return ____halli_15; }
	inline float* get_address_of__halli_15() { return &____halli_15; }
	inline void set__halli_15(float value)
	{
		____halli_15 = value;
	}

	inline static int32_t get_offset_of__sercawdri_16() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____sercawdri_16)); }
	inline uint32_t get__sercawdri_16() const { return ____sercawdri_16; }
	inline uint32_t* get_address_of__sercawdri_16() { return &____sercawdri_16; }
	inline void set__sercawdri_16(uint32_t value)
	{
		____sercawdri_16 = value;
	}

	inline static int32_t get_offset_of__zalpelWhehe_17() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____zalpelWhehe_17)); }
	inline float get__zalpelWhehe_17() const { return ____zalpelWhehe_17; }
	inline float* get_address_of__zalpelWhehe_17() { return &____zalpelWhehe_17; }
	inline void set__zalpelWhehe_17(float value)
	{
		____zalpelWhehe_17 = value;
	}

	inline static int32_t get_offset_of__merebujowRucemjor_18() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____merebujowRucemjor_18)); }
	inline bool get__merebujowRucemjor_18() const { return ____merebujowRucemjor_18; }
	inline bool* get_address_of__merebujowRucemjor_18() { return &____merebujowRucemjor_18; }
	inline void set__merebujowRucemjor_18(bool value)
	{
		____merebujowRucemjor_18 = value;
	}

	inline static int32_t get_offset_of__wodoYemi_19() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____wodoYemi_19)); }
	inline bool get__wodoYemi_19() const { return ____wodoYemi_19; }
	inline bool* get_address_of__wodoYemi_19() { return &____wodoYemi_19; }
	inline void set__wodoYemi_19(bool value)
	{
		____wodoYemi_19 = value;
	}

	inline static int32_t get_offset_of__telvaw_20() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____telvaw_20)); }
	inline String_t* get__telvaw_20() const { return ____telvaw_20; }
	inline String_t** get_address_of__telvaw_20() { return &____telvaw_20; }
	inline void set__telvaw_20(String_t* value)
	{
		____telvaw_20 = value;
		Il2CppCodeGenWriteBarrier(&____telvaw_20, value);
	}

	inline static int32_t get_offset_of__tredri_21() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____tredri_21)); }
	inline int32_t get__tredri_21() const { return ____tredri_21; }
	inline int32_t* get_address_of__tredri_21() { return &____tredri_21; }
	inline void set__tredri_21(int32_t value)
	{
		____tredri_21 = value;
	}

	inline static int32_t get_offset_of__vurnalForrowter_22() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____vurnalForrowter_22)); }
	inline float get__vurnalForrowter_22() const { return ____vurnalForrowter_22; }
	inline float* get_address_of__vurnalForrowter_22() { return &____vurnalForrowter_22; }
	inline void set__vurnalForrowter_22(float value)
	{
		____vurnalForrowter_22 = value;
	}

	inline static int32_t get_offset_of__coujallMawcawcall_23() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____coujallMawcawcall_23)); }
	inline bool get__coujallMawcawcall_23() const { return ____coujallMawcawcall_23; }
	inline bool* get_address_of__coujallMawcawcall_23() { return &____coujallMawcawcall_23; }
	inline void set__coujallMawcawcall_23(bool value)
	{
		____coujallMawcawcall_23 = value;
	}

	inline static int32_t get_offset_of__morlal_24() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____morlal_24)); }
	inline float get__morlal_24() const { return ____morlal_24; }
	inline float* get_address_of__morlal_24() { return &____morlal_24; }
	inline void set__morlal_24(float value)
	{
		____morlal_24 = value;
	}

	inline static int32_t get_offset_of__dawtouLaije_25() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____dawtouLaije_25)); }
	inline uint32_t get__dawtouLaije_25() const { return ____dawtouLaije_25; }
	inline uint32_t* get_address_of__dawtouLaije_25() { return &____dawtouLaije_25; }
	inline void set__dawtouLaije_25(uint32_t value)
	{
		____dawtouLaije_25 = value;
	}

	inline static int32_t get_offset_of__wajaysurXerbar_26() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____wajaysurXerbar_26)); }
	inline int32_t get__wajaysurXerbar_26() const { return ____wajaysurXerbar_26; }
	inline int32_t* get_address_of__wajaysurXerbar_26() { return &____wajaysurXerbar_26; }
	inline void set__wajaysurXerbar_26(int32_t value)
	{
		____wajaysurXerbar_26 = value;
	}

	inline static int32_t get_offset_of__pemcecaw_27() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____pemcecaw_27)); }
	inline uint32_t get__pemcecaw_27() const { return ____pemcecaw_27; }
	inline uint32_t* get_address_of__pemcecaw_27() { return &____pemcecaw_27; }
	inline void set__pemcecaw_27(uint32_t value)
	{
		____pemcecaw_27 = value;
	}

	inline static int32_t get_offset_of__kisxuree_28() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____kisxuree_28)); }
	inline uint32_t get__kisxuree_28() const { return ____kisxuree_28; }
	inline uint32_t* get_address_of__kisxuree_28() { return &____kisxuree_28; }
	inline void set__kisxuree_28(uint32_t value)
	{
		____kisxuree_28 = value;
	}

	inline static int32_t get_offset_of__jasurTusaybo_29() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____jasurTusaybo_29)); }
	inline uint32_t get__jasurTusaybo_29() const { return ____jasurTusaybo_29; }
	inline uint32_t* get_address_of__jasurTusaybo_29() { return &____jasurTusaybo_29; }
	inline void set__jasurTusaybo_29(uint32_t value)
	{
		____jasurTusaybo_29 = value;
	}

	inline static int32_t get_offset_of__whisuye_30() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____whisuye_30)); }
	inline float get__whisuye_30() const { return ____whisuye_30; }
	inline float* get_address_of__whisuye_30() { return &____whisuye_30; }
	inline void set__whisuye_30(float value)
	{
		____whisuye_30 = value;
	}

	inline static int32_t get_offset_of__qairwalCearfoolear_31() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____qairwalCearfoolear_31)); }
	inline uint32_t get__qairwalCearfoolear_31() const { return ____qairwalCearfoolear_31; }
	inline uint32_t* get_address_of__qairwalCearfoolear_31() { return &____qairwalCearfoolear_31; }
	inline void set__qairwalCearfoolear_31(uint32_t value)
	{
		____qairwalCearfoolear_31 = value;
	}

	inline static int32_t get_offset_of__berehel_32() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____berehel_32)); }
	inline int32_t get__berehel_32() const { return ____berehel_32; }
	inline int32_t* get_address_of__berehel_32() { return &____berehel_32; }
	inline void set__berehel_32(int32_t value)
	{
		____berehel_32 = value;
	}

	inline static int32_t get_offset_of__nesisMispanor_33() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____nesisMispanor_33)); }
	inline int32_t get__nesisMispanor_33() const { return ____nesisMispanor_33; }
	inline int32_t* get_address_of__nesisMispanor_33() { return &____nesisMispanor_33; }
	inline void set__nesisMispanor_33(int32_t value)
	{
		____nesisMispanor_33 = value;
	}

	inline static int32_t get_offset_of__nefallCurye_34() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____nefallCurye_34)); }
	inline float get__nefallCurye_34() const { return ____nefallCurye_34; }
	inline float* get_address_of__nefallCurye_34() { return &____nefallCurye_34; }
	inline void set__nefallCurye_34(float value)
	{
		____nefallCurye_34 = value;
	}

	inline static int32_t get_offset_of__tawkexereMumecu_35() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____tawkexereMumecu_35)); }
	inline int32_t get__tawkexereMumecu_35() const { return ____tawkexereMumecu_35; }
	inline int32_t* get_address_of__tawkexereMumecu_35() { return &____tawkexereMumecu_35; }
	inline void set__tawkexereMumecu_35(int32_t value)
	{
		____tawkexereMumecu_35 = value;
	}

	inline static int32_t get_offset_of__cakitiXawhayza_36() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____cakitiXawhayza_36)); }
	inline float get__cakitiXawhayza_36() const { return ____cakitiXawhayza_36; }
	inline float* get_address_of__cakitiXawhayza_36() { return &____cakitiXawhayza_36; }
	inline void set__cakitiXawhayza_36(float value)
	{
		____cakitiXawhayza_36 = value;
	}

	inline static int32_t get_offset_of__jipalkaWoraw_37() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____jipalkaWoraw_37)); }
	inline int32_t get__jipalkaWoraw_37() const { return ____jipalkaWoraw_37; }
	inline int32_t* get_address_of__jipalkaWoraw_37() { return &____jipalkaWoraw_37; }
	inline void set__jipalkaWoraw_37(int32_t value)
	{
		____jipalkaWoraw_37 = value;
	}

	inline static int32_t get_offset_of__lisxirci_38() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____lisxirci_38)); }
	inline float get__lisxirci_38() const { return ____lisxirci_38; }
	inline float* get_address_of__lisxirci_38() { return &____lisxirci_38; }
	inline void set__lisxirci_38(float value)
	{
		____lisxirci_38 = value;
	}

	inline static int32_t get_offset_of__poonookaSerejis_39() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____poonookaSerejis_39)); }
	inline int32_t get__poonookaSerejis_39() const { return ____poonookaSerejis_39; }
	inline int32_t* get_address_of__poonookaSerejis_39() { return &____poonookaSerejis_39; }
	inline void set__poonookaSerejis_39(int32_t value)
	{
		____poonookaSerejis_39 = value;
	}

	inline static int32_t get_offset_of__pourunay_40() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____pourunay_40)); }
	inline bool get__pourunay_40() const { return ____pourunay_40; }
	inline bool* get_address_of__pourunay_40() { return &____pourunay_40; }
	inline void set__pourunay_40(bool value)
	{
		____pourunay_40 = value;
	}

	inline static int32_t get_offset_of__poxu_41() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____poxu_41)); }
	inline float get__poxu_41() const { return ____poxu_41; }
	inline float* get_address_of__poxu_41() { return &____poxu_41; }
	inline void set__poxu_41(float value)
	{
		____poxu_41 = value;
	}

	inline static int32_t get_offset_of__beartremnoo_42() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____beartremnoo_42)); }
	inline uint32_t get__beartremnoo_42() const { return ____beartremnoo_42; }
	inline uint32_t* get_address_of__beartremnoo_42() { return &____beartremnoo_42; }
	inline void set__beartremnoo_42(uint32_t value)
	{
		____beartremnoo_42 = value;
	}

	inline static int32_t get_offset_of__jejerbaGiwallbel_43() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____jejerbaGiwallbel_43)); }
	inline int32_t get__jejerbaGiwallbel_43() const { return ____jejerbaGiwallbel_43; }
	inline int32_t* get_address_of__jejerbaGiwallbel_43() { return &____jejerbaGiwallbel_43; }
	inline void set__jejerbaGiwallbel_43(int32_t value)
	{
		____jejerbaGiwallbel_43 = value;
	}

	inline static int32_t get_offset_of__cataPelbefa_44() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____cataPelbefa_44)); }
	inline bool get__cataPelbefa_44() const { return ____cataPelbefa_44; }
	inline bool* get_address_of__cataPelbefa_44() { return &____cataPelbefa_44; }
	inline void set__cataPelbefa_44(bool value)
	{
		____cataPelbefa_44 = value;
	}

	inline static int32_t get_offset_of__carjo_45() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____carjo_45)); }
	inline float get__carjo_45() const { return ____carjo_45; }
	inline float* get_address_of__carjo_45() { return &____carjo_45; }
	inline void set__carjo_45(float value)
	{
		____carjo_45 = value;
	}

	inline static int32_t get_offset_of__haicair_46() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____haicair_46)); }
	inline uint32_t get__haicair_46() const { return ____haicair_46; }
	inline uint32_t* get_address_of__haicair_46() { return &____haicair_46; }
	inline void set__haicair_46(uint32_t value)
	{
		____haicair_46 = value;
	}

	inline static int32_t get_offset_of__lertetaSedou_47() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____lertetaSedou_47)); }
	inline uint32_t get__lertetaSedou_47() const { return ____lertetaSedou_47; }
	inline uint32_t* get_address_of__lertetaSedou_47() { return &____lertetaSedou_47; }
	inline void set__lertetaSedou_47(uint32_t value)
	{
		____lertetaSedou_47 = value;
	}

	inline static int32_t get_offset_of__gide_48() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____gide_48)); }
	inline int32_t get__gide_48() const { return ____gide_48; }
	inline int32_t* get_address_of__gide_48() { return &____gide_48; }
	inline void set__gide_48(int32_t value)
	{
		____gide_48 = value;
	}

	inline static int32_t get_offset_of__teasiyeFigir_49() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____teasiyeFigir_49)); }
	inline float get__teasiyeFigir_49() const { return ____teasiyeFigir_49; }
	inline float* get_address_of__teasiyeFigir_49() { return &____teasiyeFigir_49; }
	inline void set__teasiyeFigir_49(float value)
	{
		____teasiyeFigir_49 = value;
	}

	inline static int32_t get_offset_of__zostai_50() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____zostai_50)); }
	inline float get__zostai_50() const { return ____zostai_50; }
	inline float* get_address_of__zostai_50() { return &____zostai_50; }
	inline void set__zostai_50(float value)
	{
		____zostai_50 = value;
	}

	inline static int32_t get_offset_of__genoore_51() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____genoore_51)); }
	inline float get__genoore_51() const { return ____genoore_51; }
	inline float* get_address_of__genoore_51() { return &____genoore_51; }
	inline void set__genoore_51(float value)
	{
		____genoore_51 = value;
	}

	inline static int32_t get_offset_of__heremis_52() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____heremis_52)); }
	inline uint32_t get__heremis_52() const { return ____heremis_52; }
	inline uint32_t* get_address_of__heremis_52() { return &____heremis_52; }
	inline void set__heremis_52(uint32_t value)
	{
		____heremis_52 = value;
	}

	inline static int32_t get_offset_of__wheruZabee_53() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____wheruZabee_53)); }
	inline bool get__wheruZabee_53() const { return ____wheruZabee_53; }
	inline bool* get_address_of__wheruZabee_53() { return &____wheruZabee_53; }
	inline void set__wheruZabee_53(bool value)
	{
		____wheruZabee_53 = value;
	}

	inline static int32_t get_offset_of__jerwalfaWaluse_54() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____jerwalfaWaluse_54)); }
	inline int32_t get__jerwalfaWaluse_54() const { return ____jerwalfaWaluse_54; }
	inline int32_t* get_address_of__jerwalfaWaluse_54() { return &____jerwalfaWaluse_54; }
	inline void set__jerwalfaWaluse_54(int32_t value)
	{
		____jerwalfaWaluse_54 = value;
	}

	inline static int32_t get_offset_of__mearsapi_55() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____mearsapi_55)); }
	inline int32_t get__mearsapi_55() const { return ____mearsapi_55; }
	inline int32_t* get_address_of__mearsapi_55() { return &____mearsapi_55; }
	inline void set__mearsapi_55(int32_t value)
	{
		____mearsapi_55 = value;
	}

	inline static int32_t get_offset_of__raltrerWupel_56() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____raltrerWupel_56)); }
	inline int32_t get__raltrerWupel_56() const { return ____raltrerWupel_56; }
	inline int32_t* get_address_of__raltrerWupel_56() { return &____raltrerWupel_56; }
	inline void set__raltrerWupel_56(int32_t value)
	{
		____raltrerWupel_56 = value;
	}

	inline static int32_t get_offset_of__cedrefair_57() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____cedrefair_57)); }
	inline int32_t get__cedrefair_57() const { return ____cedrefair_57; }
	inline int32_t* get_address_of__cedrefair_57() { return &____cedrefair_57; }
	inline void set__cedrefair_57(int32_t value)
	{
		____cedrefair_57 = value;
	}

	inline static int32_t get_offset_of__dalnurberPixi_58() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____dalnurberPixi_58)); }
	inline float get__dalnurberPixi_58() const { return ____dalnurberPixi_58; }
	inline float* get_address_of__dalnurberPixi_58() { return &____dalnurberPixi_58; }
	inline void set__dalnurberPixi_58(float value)
	{
		____dalnurberPixi_58 = value;
	}

	inline static int32_t get_offset_of__mounugu_59() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____mounugu_59)); }
	inline uint32_t get__mounugu_59() const { return ____mounugu_59; }
	inline uint32_t* get_address_of__mounugu_59() { return &____mounugu_59; }
	inline void set__mounugu_59(uint32_t value)
	{
		____mounugu_59 = value;
	}

	inline static int32_t get_offset_of__dererouSormairnu_60() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____dererouSormairnu_60)); }
	inline uint32_t get__dererouSormairnu_60() const { return ____dererouSormairnu_60; }
	inline uint32_t* get_address_of__dererouSormairnu_60() { return &____dererouSormairnu_60; }
	inline void set__dererouSormairnu_60(uint32_t value)
	{
		____dererouSormairnu_60 = value;
	}

	inline static int32_t get_offset_of__beejou_61() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____beejou_61)); }
	inline int32_t get__beejou_61() const { return ____beejou_61; }
	inline int32_t* get_address_of__beejou_61() { return &____beejou_61; }
	inline void set__beejou_61(int32_t value)
	{
		____beejou_61 = value;
	}

	inline static int32_t get_offset_of__sofeeRole_62() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____sofeeRole_62)); }
	inline uint32_t get__sofeeRole_62() const { return ____sofeeRole_62; }
	inline uint32_t* get_address_of__sofeeRole_62() { return &____sofeeRole_62; }
	inline void set__sofeeRole_62(uint32_t value)
	{
		____sofeeRole_62 = value;
	}

	inline static int32_t get_offset_of__raysaszalCeteepor_63() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____raysaszalCeteepor_63)); }
	inline float get__raysaszalCeteepor_63() const { return ____raysaszalCeteepor_63; }
	inline float* get_address_of__raysaszalCeteepor_63() { return &____raysaszalCeteepor_63; }
	inline void set__raysaszalCeteepor_63(float value)
	{
		____raysaszalCeteepor_63 = value;
	}

	inline static int32_t get_offset_of__saldrirjar_64() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____saldrirjar_64)); }
	inline uint32_t get__saldrirjar_64() const { return ____saldrirjar_64; }
	inline uint32_t* get_address_of__saldrirjar_64() { return &____saldrirjar_64; }
	inline void set__saldrirjar_64(uint32_t value)
	{
		____saldrirjar_64 = value;
	}

	inline static int32_t get_offset_of__sasteamay_65() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____sasteamay_65)); }
	inline String_t* get__sasteamay_65() const { return ____sasteamay_65; }
	inline String_t** get_address_of__sasteamay_65() { return &____sasteamay_65; }
	inline void set__sasteamay_65(String_t* value)
	{
		____sasteamay_65 = value;
		Il2CppCodeGenWriteBarrier(&____sasteamay_65, value);
	}

	inline static int32_t get_offset_of__hucaraTuve_66() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____hucaraTuve_66)); }
	inline int32_t get__hucaraTuve_66() const { return ____hucaraTuve_66; }
	inline int32_t* get_address_of__hucaraTuve_66() { return &____hucaraTuve_66; }
	inline void set__hucaraTuve_66(int32_t value)
	{
		____hucaraTuve_66 = value;
	}

	inline static int32_t get_offset_of__riskaslea_67() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____riskaslea_67)); }
	inline float get__riskaslea_67() const { return ____riskaslea_67; }
	inline float* get_address_of__riskaslea_67() { return &____riskaslea_67; }
	inline void set__riskaslea_67(float value)
	{
		____riskaslea_67 = value;
	}

	inline static int32_t get_offset_of__qeredembearBallcaw_68() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____qeredembearBallcaw_68)); }
	inline int32_t get__qeredembearBallcaw_68() const { return ____qeredembearBallcaw_68; }
	inline int32_t* get_address_of__qeredembearBallcaw_68() { return &____qeredembearBallcaw_68; }
	inline void set__qeredembearBallcaw_68(int32_t value)
	{
		____qeredembearBallcaw_68 = value;
	}

	inline static int32_t get_offset_of__qursico_69() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____qursico_69)); }
	inline uint32_t get__qursico_69() const { return ____qursico_69; }
	inline uint32_t* get_address_of__qursico_69() { return &____qursico_69; }
	inline void set__qursico_69(uint32_t value)
	{
		____qursico_69 = value;
	}

	inline static int32_t get_offset_of__jahal_70() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____jahal_70)); }
	inline bool get__jahal_70() const { return ____jahal_70; }
	inline bool* get_address_of__jahal_70() { return &____jahal_70; }
	inline void set__jahal_70(bool value)
	{
		____jahal_70 = value;
	}

	inline static int32_t get_offset_of__drupouChawne_71() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____drupouChawne_71)); }
	inline bool get__drupouChawne_71() const { return ____drupouChawne_71; }
	inline bool* get_address_of__drupouChawne_71() { return &____drupouChawne_71; }
	inline void set__drupouChawne_71(bool value)
	{
		____drupouChawne_71 = value;
	}

	inline static int32_t get_offset_of__nelsorwur_72() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____nelsorwur_72)); }
	inline int32_t get__nelsorwur_72() const { return ____nelsorwur_72; }
	inline int32_t* get_address_of__nelsorwur_72() { return &____nelsorwur_72; }
	inline void set__nelsorwur_72(int32_t value)
	{
		____nelsorwur_72 = value;
	}

	inline static int32_t get_offset_of__tideamem_73() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____tideamem_73)); }
	inline String_t* get__tideamem_73() const { return ____tideamem_73; }
	inline String_t** get_address_of__tideamem_73() { return &____tideamem_73; }
	inline void set__tideamem_73(String_t* value)
	{
		____tideamem_73 = value;
		Il2CppCodeGenWriteBarrier(&____tideamem_73, value);
	}

	inline static int32_t get_offset_of__pafaircea_74() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____pafaircea_74)); }
	inline uint32_t get__pafaircea_74() const { return ____pafaircea_74; }
	inline uint32_t* get_address_of__pafaircea_74() { return &____pafaircea_74; }
	inline void set__pafaircea_74(uint32_t value)
	{
		____pafaircea_74 = value;
	}

	inline static int32_t get_offset_of__deafirMemmir_75() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____deafirMemmir_75)); }
	inline int32_t get__deafirMemmir_75() const { return ____deafirMemmir_75; }
	inline int32_t* get_address_of__deafirMemmir_75() { return &____deafirMemmir_75; }
	inline void set__deafirMemmir_75(int32_t value)
	{
		____deafirMemmir_75 = value;
	}

	inline static int32_t get_offset_of__nusu_76() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____nusu_76)); }
	inline int32_t get__nusu_76() const { return ____nusu_76; }
	inline int32_t* get_address_of__nusu_76() { return &____nusu_76; }
	inline void set__nusu_76(int32_t value)
	{
		____nusu_76 = value;
	}

	inline static int32_t get_offset_of__talltroodiNaci_77() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____talltroodiNaci_77)); }
	inline bool get__talltroodiNaci_77() const { return ____talltroodiNaci_77; }
	inline bool* get_address_of__talltroodiNaci_77() { return &____talltroodiNaci_77; }
	inline void set__talltroodiNaci_77(bool value)
	{
		____talltroodiNaci_77 = value;
	}

	inline static int32_t get_offset_of__tajacha_78() { return static_cast<int32_t>(offsetof(M_cheenallsair32_t3769312670, ____tajacha_78)); }
	inline bool get__tajacha_78() const { return ____tajacha_78; }
	inline bool* get_address_of__tajacha_78() { return &____tajacha_78; }
	inline void set__tajacha_78(bool value)
	{
		____tajacha_78 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
