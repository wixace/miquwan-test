﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t1322387783;
// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t2526458961;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_MaterialPropertyBlock1322387783.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"

// System.Void UnityEngine.MaterialPropertyBlock::.ctor()
extern "C"  void MaterialPropertyBlock__ctor_m2730424616 (MaterialPropertyBlock_t1322387783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::InitBlock()
extern "C"  void MaterialPropertyBlock_InitBlock_m863070691 (MaterialPropertyBlock_t1322387783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::DestroyBlock()
extern "C"  void MaterialPropertyBlock_DestroyBlock_m3380306959 (MaterialPropertyBlock_t1322387783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::Finalize()
extern "C"  void MaterialPropertyBlock_Finalize_m4091590202 (MaterialPropertyBlock_t1322387783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.MaterialPropertyBlock::get_isEmpty()
extern "C"  bool MaterialPropertyBlock_get_isEmpty_m1941159270 (MaterialPropertyBlock_t1322387783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::SetFloat(System.String,System.Single)
extern "C"  void MaterialPropertyBlock_SetFloat_m2974916209 (MaterialPropertyBlock_t1322387783 * __this, String_t* ___name0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::SetFloat(System.Int32,System.Single)
extern "C"  void MaterialPropertyBlock_SetFloat_m1897010476 (MaterialPropertyBlock_t1322387783 * __this, int32_t ___nameID0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::SetVector(System.String,UnityEngine.Vector4)
extern "C"  void MaterialPropertyBlock_SetVector_m1001277001 (MaterialPropertyBlock_t1322387783 * __this, String_t* ___name0, Vector4_t4282066567  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::SetVector(System.Int32,UnityEngine.Vector4)
extern "C"  void MaterialPropertyBlock_SetVector_m4256287566 (MaterialPropertyBlock_t1322387783 * __this, int32_t ___nameID0, Vector4_t4282066567  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::INTERNAL_CALL_SetVector(UnityEngine.MaterialPropertyBlock,System.Int32,UnityEngine.Vector4&)
extern "C"  void MaterialPropertyBlock_INTERNAL_CALL_SetVector_m4135485679 (Il2CppObject * __this /* static, unused */, MaterialPropertyBlock_t1322387783 * ___self0, int32_t ___nameID1, Vector4_t4282066567 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::SetColor(System.String,UnityEngine.Color)
extern "C"  void MaterialPropertyBlock_SetColor_m2503099333 (MaterialPropertyBlock_t1322387783 * __this, String_t* ___name0, Color_t4194546905  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::SetColor(System.Int32,UnityEngine.Color)
extern "C"  void MaterialPropertyBlock_SetColor_m1736386094 (MaterialPropertyBlock_t1322387783 * __this, int32_t ___nameID0, Color_t4194546905  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::INTERNAL_CALL_SetColor(UnityEngine.MaterialPropertyBlock,System.Int32,UnityEngine.Color&)
extern "C"  void MaterialPropertyBlock_INTERNAL_CALL_SetColor_m2507266801 (Il2CppObject * __this /* static, unused */, MaterialPropertyBlock_t1322387783 * ___self0, int32_t ___nameID1, Color_t4194546905 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::SetMatrix(System.String,UnityEngine.Matrix4x4)
extern "C"  void MaterialPropertyBlock_SetMatrix_m2705223373 (MaterialPropertyBlock_t1322387783 * __this, String_t* ___name0, Matrix4x4_t1651859333  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::SetMatrix(System.Int32,UnityEngine.Matrix4x4)
extern "C"  void MaterialPropertyBlock_SetMatrix_m3538974158 (MaterialPropertyBlock_t1322387783 * __this, int32_t ___nameID0, Matrix4x4_t1651859333  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::INTERNAL_CALL_SetMatrix(UnityEngine.MaterialPropertyBlock,System.Int32,UnityEngine.Matrix4x4&)
extern "C"  void MaterialPropertyBlock_INTERNAL_CALL_SetMatrix_m2801839535 (Il2CppObject * __this /* static, unused */, MaterialPropertyBlock_t1322387783 * ___self0, int32_t ___nameID1, Matrix4x4_t1651859333 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::SetTexture(System.String,UnityEngine.Texture)
extern "C"  void MaterialPropertyBlock_SetTexture_m1524740821 (MaterialPropertyBlock_t1322387783 * __this, String_t* ___name0, Texture_t2526458961 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::SetTexture(System.Int32,UnityEngine.Texture)
extern "C"  void MaterialPropertyBlock_SetTexture_m1343437550 (MaterialPropertyBlock_t1322387783 * __this, int32_t ___nameID0, Texture_t2526458961 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.MaterialPropertyBlock::GetFloat(System.String)
extern "C"  float MaterialPropertyBlock_GetFloat_m3185216908 (MaterialPropertyBlock_t1322387783 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.MaterialPropertyBlock::GetFloat(System.Int32)
extern "C"  float MaterialPropertyBlock_GetFloat_m1750584711 (MaterialPropertyBlock_t1322387783 * __this, int32_t ___nameID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.MaterialPropertyBlock::GetVector(System.String)
extern "C"  Vector4_t4282066567  MaterialPropertyBlock_GetVector_m3451592042 (MaterialPropertyBlock_t1322387783 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.MaterialPropertyBlock::GetVector(System.Int32)
extern "C"  Vector4_t4282066567  MaterialPropertyBlock_GetVector_m1482082793 (MaterialPropertyBlock_t1322387783 * __this, int32_t ___nameID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::INTERNAL_CALL_GetVector(UnityEngine.MaterialPropertyBlock,System.Int32,UnityEngine.Vector4&)
extern "C"  void MaterialPropertyBlock_INTERNAL_CALL_GetVector_m3396251003 (Il2CppObject * __this /* static, unused */, MaterialPropertyBlock_t1322387783 * ___self0, int32_t ___nameID1, Vector4_t4282066567 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.MaterialPropertyBlock::GetMatrix(System.String)
extern "C"  Matrix4x4_t1651859333  MaterialPropertyBlock_GetMatrix_m2215378858 (MaterialPropertyBlock_t1322387783 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.MaterialPropertyBlock::GetMatrix(System.Int32)
extern "C"  Matrix4x4_t1651859333  MaterialPropertyBlock_GetMatrix_m2134941609 (MaterialPropertyBlock_t1322387783 * __this, int32_t ___nameID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::INTERNAL_CALL_GetMatrix(UnityEngine.MaterialPropertyBlock,System.Int32,UnityEngine.Matrix4x4&)
extern "C"  void MaterialPropertyBlock_INTERNAL_CALL_GetMatrix_m1066919739 (Il2CppObject * __this /* static, unused */, MaterialPropertyBlock_t1322387783 * ___self0, int32_t ___nameID1, Matrix4x4_t1651859333 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.MaterialPropertyBlock::GetTexture(System.String)
extern "C"  Texture_t2526458961 * MaterialPropertyBlock_GetTexture_m3196960688 (MaterialPropertyBlock_t1322387783 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.MaterialPropertyBlock::GetTexture(System.Int32)
extern "C"  Texture_t2526458961 * MaterialPropertyBlock_GetTexture_m2166605539 (MaterialPropertyBlock_t1322387783 * __this, int32_t ___nameID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::Clear()
extern "C"  void MaterialPropertyBlock_Clear_m136557907 (MaterialPropertyBlock_t1322387783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
