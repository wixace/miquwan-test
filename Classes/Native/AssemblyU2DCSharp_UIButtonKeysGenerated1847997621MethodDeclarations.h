﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIButtonKeysGenerated
struct UIButtonKeysGenerated_t1847997621;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UIButtonKeysGenerated::.ctor()
extern "C"  void UIButtonKeysGenerated__ctor_m48775254 (UIButtonKeysGenerated_t1847997621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonKeysGenerated::UIButtonKeys_UIButtonKeys1(JSVCall,System.Int32)
extern "C"  bool UIButtonKeysGenerated_UIButtonKeys_UIButtonKeys1_m1500758812 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonKeysGenerated::UIButtonKeys_selectOnClick(JSVCall)
extern "C"  void UIButtonKeysGenerated_UIButtonKeys_selectOnClick_m1925984209 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonKeysGenerated::UIButtonKeys_selectOnUp(JSVCall)
extern "C"  void UIButtonKeysGenerated_UIButtonKeys_selectOnUp_m3022452568 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonKeysGenerated::UIButtonKeys_selectOnDown(JSVCall)
extern "C"  void UIButtonKeysGenerated_UIButtonKeys_selectOnDown_m672426481 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonKeysGenerated::UIButtonKeys_selectOnLeft(JSVCall)
extern "C"  void UIButtonKeysGenerated_UIButtonKeys_selectOnLeft_m633663532 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonKeysGenerated::UIButtonKeys_selectOnRight(JSVCall)
extern "C"  void UIButtonKeysGenerated_UIButtonKeys_selectOnRight_m1391139261 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonKeysGenerated::UIButtonKeys_Upgrade(JSVCall,System.Int32)
extern "C"  bool UIButtonKeysGenerated_UIButtonKeys_Upgrade_m4142129377 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonKeysGenerated::__Register()
extern "C"  void UIButtonKeysGenerated___Register_m3164734001 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIButtonKeysGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UIButtonKeysGenerated_ilo_getObject1_m2718950944 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIButtonKeysGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIButtonKeysGenerated_ilo_setObject2_m3564578777 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIButtonKeysGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIButtonKeysGenerated_ilo_getObject3_m2603518097 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
