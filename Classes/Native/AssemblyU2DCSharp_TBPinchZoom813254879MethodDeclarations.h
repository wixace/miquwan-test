﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TBPinchZoom
struct TBPinchZoom_t813254879;
// PinchGesture
struct PinchGesture_t1502590799;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_PinchGesture1502590799.h"
#include "AssemblyU2DCSharp_TBPinchZoom813254879.h"

// System.Void TBPinchZoom::.ctor()
extern "C"  void TBPinchZoom__ctor_m245153132 (TBPinchZoom_t813254879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TBPinchZoom::get_DefaultPos()
extern "C"  Vector3_t4282066566  TBPinchZoom_get_DefaultPos_m1050768442 (TBPinchZoom_t813254879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPinchZoom::set_DefaultPos(UnityEngine.Vector3)
extern "C"  void TBPinchZoom_set_DefaultPos_m1143025 (TBPinchZoom_t813254879 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TBPinchZoom::get_DefaultFov()
extern "C"  float TBPinchZoom_get_DefaultFov_m794281569 (TBPinchZoom_t813254879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPinchZoom::set_DefaultFov(System.Single)
extern "C"  void TBPinchZoom_set_DefaultFov_m231520554 (TBPinchZoom_t813254879 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TBPinchZoom::get_DefaultOrthoSize()
extern "C"  float TBPinchZoom_get_DefaultOrthoSize_m577222701 (TBPinchZoom_t813254879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPinchZoom::set_DefaultOrthoSize(System.Single)
extern "C"  void TBPinchZoom_set_DefaultOrthoSize_m3423986398 (TBPinchZoom_t813254879 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TBPinchZoom::get_IdealZoomAmount()
extern "C"  float TBPinchZoom_get_IdealZoomAmount_m204432429 (TBPinchZoom_t813254879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPinchZoom::set_IdealZoomAmount(System.Single)
extern "C"  void TBPinchZoom_set_IdealZoomAmount_m609836638 (TBPinchZoom_t813254879 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TBPinchZoom::get_ZoomAmount()
extern "C"  float TBPinchZoom_get_ZoomAmount_m1217125856 (TBPinchZoom_t813254879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPinchZoom::set_ZoomAmount(System.Single)
extern "C"  void TBPinchZoom_set_ZoomAmount_m2201815051 (TBPinchZoom_t813254879 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TBPinchZoom::get_CameraFov()
extern "C"  float TBPinchZoom_get_CameraFov_m1667650133 (TBPinchZoom_t813254879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPinchZoom::set_CameraFov(System.Single)
extern "C"  void TBPinchZoom_set_CameraFov_m3007545654 (TBPinchZoom_t813254879 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TBPinchZoom::get_ZoomPercent()
extern "C"  float TBPinchZoom_get_ZoomPercent_m3110338143 (TBPinchZoom_t813254879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPinchZoom::Start()
extern "C"  void TBPinchZoom_Start_m3487258220 (TBPinchZoom_t813254879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPinchZoom::Update()
extern "C"  void TBPinchZoom_Update_m736674593 (TBPinchZoom_t813254879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPinchZoom::SetDefaults()
extern "C"  void TBPinchZoom_SetDefaults_m1974780830 (TBPinchZoom_t813254879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPinchZoom::OnPinch(PinchGesture)
extern "C"  void TBPinchZoom_OnPinch_m971908150 (TBPinchZoom_t813254879 * __this, PinchGesture_t1502590799 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TBPinchZoom::ilo_get_IdealZoomAmount1(TBPinchZoom)
extern "C"  float TBPinchZoom_ilo_get_IdealZoomAmount1_m4271226266 (Il2CppObject * __this /* static, unused */, TBPinchZoom_t813254879 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPinchZoom::ilo_set_DefaultFov2(TBPinchZoom,System.Single)
extern "C"  void TBPinchZoom_ilo_set_DefaultFov2_m3350966104 (Il2CppObject * __this /* static, unused */, TBPinchZoom_t813254879 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPinchZoom::ilo_set_DefaultOrthoSize3(TBPinchZoom,System.Single)
extern "C"  void TBPinchZoom_ilo_set_DefaultOrthoSize3_m2362879811 (Il2CppObject * __this /* static, unused */, TBPinchZoom_t813254879 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
