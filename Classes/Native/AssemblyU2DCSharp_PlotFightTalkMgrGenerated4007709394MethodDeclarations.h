﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlotFightTalkMgrGenerated
struct PlotFightTalkMgrGenerated_t4007709394;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void PlotFightTalkMgrGenerated::.ctor()
extern "C"  void PlotFightTalkMgrGenerated__ctor_m1091929817 (PlotFightTalkMgrGenerated_t4007709394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlotFightTalkMgrGenerated::PlotFightTalkMgr_PlotFightTalkMgr1(JSVCall,System.Int32)
extern "C"  bool PlotFightTalkMgrGenerated_PlotFightTalkMgr_PlotFightTalkMgr1_m1739243545 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgrGenerated::PlotFightTalkMgr_TRIGGER_PLOT(JSVCall)
extern "C"  void PlotFightTalkMgrGenerated_PlotFightTalkMgr_TRIGGER_PLOT_m555595270 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlotFightTalkMgrGenerated::PlotFightTalkMgr_Clear(JSVCall,System.Int32)
extern "C"  bool PlotFightTalkMgrGenerated_PlotFightTalkMgr_Clear_m3194865394 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlotFightTalkMgrGenerated::PlotFightTalkMgr_OnTrigger__ZEvent(JSVCall,System.Int32)
extern "C"  bool PlotFightTalkMgrGenerated_PlotFightTalkMgr_OnTrigger__ZEvent_m999345598 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgrGenerated::__Register()
extern "C"  void PlotFightTalkMgrGenerated___Register_m2774226318 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgrGenerated::ilo_setStringS1(System.Int32,System.String)
extern "C"  void PlotFightTalkMgrGenerated_ilo_setStringS1_m4203028770 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlotFightTalkMgrGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * PlotFightTalkMgrGenerated_ilo_getObject2_m839047981 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
