﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UISavedOptionGenerated
struct UISavedOptionGenerated_t2861079495;
// JSVCall
struct JSVCall_t3708497963;
// UISavedOption
struct UISavedOption_t3282279080;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_UISavedOption3282279080.h"

// System.Void UISavedOptionGenerated::.ctor()
extern "C"  void UISavedOptionGenerated__ctor_m2852710580 (UISavedOptionGenerated_t2861079495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISavedOptionGenerated::UISavedOption_UISavedOption1(JSVCall,System.Int32)
extern "C"  bool UISavedOptionGenerated_UISavedOption_UISavedOption1_m170508852 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISavedOptionGenerated::UISavedOption_keyName(JSVCall)
extern "C"  void UISavedOptionGenerated_UISavedOption_keyName_m1337311202 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISavedOptionGenerated::UISavedOption_SaveProgress(JSVCall,System.Int32)
extern "C"  bool UISavedOptionGenerated_UISavedOption_SaveProgress_m2735117909 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISavedOptionGenerated::UISavedOption_SaveSelection(JSVCall,System.Int32)
extern "C"  bool UISavedOptionGenerated_UISavedOption_SaveSelection_m3990792614 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISavedOptionGenerated::UISavedOption_SaveState(JSVCall,System.Int32)
extern "C"  bool UISavedOptionGenerated_UISavedOption_SaveState_m2620139371 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISavedOptionGenerated::__Register()
extern "C"  void UISavedOptionGenerated___Register_m3625051283 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISavedOptionGenerated::ilo_SaveProgress1(UISavedOption)
extern "C"  void UISavedOptionGenerated_ilo_SaveProgress1_m829594404 (Il2CppObject * __this /* static, unused */, UISavedOption_t3282279080 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
