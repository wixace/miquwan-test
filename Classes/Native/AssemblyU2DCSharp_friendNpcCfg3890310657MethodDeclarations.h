﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// friendNpcCfg
struct friendNpcCfg_t3890310657;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void friendNpcCfg::.ctor()
extern "C"  void friendNpcCfg__ctor_m1877718458 (friendNpcCfg_t3890310657 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void friendNpcCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void friendNpcCfg_Init_m4153044875 (friendNpcCfg_t3890310657 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
