﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t3870600107;
// Pathfinding.DebugUtility
struct DebugUtility_t1152695055;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.DebugUtility
struct  DebugUtility_t1152695055  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Material Pathfinding.DebugUtility::defaultMaterial
	Material_t3870600107 * ___defaultMaterial_2;
	// System.Single Pathfinding.DebugUtility::offset
	float ___offset_4;
	// System.Boolean Pathfinding.DebugUtility::optimizeMeshes
	bool ___optimizeMeshes_5;

public:
	inline static int32_t get_offset_of_defaultMaterial_2() { return static_cast<int32_t>(offsetof(DebugUtility_t1152695055, ___defaultMaterial_2)); }
	inline Material_t3870600107 * get_defaultMaterial_2() const { return ___defaultMaterial_2; }
	inline Material_t3870600107 ** get_address_of_defaultMaterial_2() { return &___defaultMaterial_2; }
	inline void set_defaultMaterial_2(Material_t3870600107 * value)
	{
		___defaultMaterial_2 = value;
		Il2CppCodeGenWriteBarrier(&___defaultMaterial_2, value);
	}

	inline static int32_t get_offset_of_offset_4() { return static_cast<int32_t>(offsetof(DebugUtility_t1152695055, ___offset_4)); }
	inline float get_offset_4() const { return ___offset_4; }
	inline float* get_address_of_offset_4() { return &___offset_4; }
	inline void set_offset_4(float value)
	{
		___offset_4 = value;
	}

	inline static int32_t get_offset_of_optimizeMeshes_5() { return static_cast<int32_t>(offsetof(DebugUtility_t1152695055, ___optimizeMeshes_5)); }
	inline bool get_optimizeMeshes_5() const { return ___optimizeMeshes_5; }
	inline bool* get_address_of_optimizeMeshes_5() { return &___optimizeMeshes_5; }
	inline void set_optimizeMeshes_5(bool value)
	{
		___optimizeMeshes_5 = value;
	}
};

struct DebugUtility_t1152695055_StaticFields
{
public:
	// Pathfinding.DebugUtility Pathfinding.DebugUtility::active
	DebugUtility_t1152695055 * ___active_3;

public:
	inline static int32_t get_offset_of_active_3() { return static_cast<int32_t>(offsetof(DebugUtility_t1152695055_StaticFields, ___active_3)); }
	inline DebugUtility_t1152695055 * get_active_3() const { return ___active_3; }
	inline DebugUtility_t1152695055 ** get_address_of_active_3() { return &___active_3; }
	inline void set_active_3(DebugUtility_t1152695055 * value)
	{
		___active_3 = value;
		Il2CppCodeGenWriteBarrier(&___active_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
