﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ContinuousGestureRecognizer_1_ge2766260145.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragRecognizer
struct  DragRecognizer_t2550591000  : public ContinuousGestureRecognizer_1_t2766260145
{
public:
	// System.Single DragRecognizer::MoveTolerance
	float ___MoveTolerance_19;
	// System.Boolean DragRecognizer::ApplySameDirectionConstraint
	bool ___ApplySameDirectionConstraint_20;

public:
	inline static int32_t get_offset_of_MoveTolerance_19() { return static_cast<int32_t>(offsetof(DragRecognizer_t2550591000, ___MoveTolerance_19)); }
	inline float get_MoveTolerance_19() const { return ___MoveTolerance_19; }
	inline float* get_address_of_MoveTolerance_19() { return &___MoveTolerance_19; }
	inline void set_MoveTolerance_19(float value)
	{
		___MoveTolerance_19 = value;
	}

	inline static int32_t get_offset_of_ApplySameDirectionConstraint_20() { return static_cast<int32_t>(offsetof(DragRecognizer_t2550591000, ___ApplySameDirectionConstraint_20)); }
	inline bool get_ApplySameDirectionConstraint_20() const { return ___ApplySameDirectionConstraint_20; }
	inline bool* get_address_of_ApplySameDirectionConstraint_20() { return &___ApplySameDirectionConstraint_20; }
	inline void set_ApplySameDirectionConstraint_20(bool value)
	{
		___ApplySameDirectionConstraint_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
