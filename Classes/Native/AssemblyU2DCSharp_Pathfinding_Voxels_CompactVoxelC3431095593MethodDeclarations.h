﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Voxels.CompactVoxelCell
struct CompactVoxelCell_t3431095593;
struct CompactVoxelCell_t3431095593_marshaled_pinvoke;
struct CompactVoxelCell_t3431095593_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_CompactVoxelC3431095593.h"

// System.Void Pathfinding.Voxels.CompactVoxelCell::.ctor(System.UInt32,System.UInt32)
extern "C"  void CompactVoxelCell__ctor_m371062239 (CompactVoxelCell_t3431095593 * __this, uint32_t ___i0, uint32_t ___c1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct CompactVoxelCell_t3431095593;
struct CompactVoxelCell_t3431095593_marshaled_pinvoke;

extern "C" void CompactVoxelCell_t3431095593_marshal_pinvoke(const CompactVoxelCell_t3431095593& unmarshaled, CompactVoxelCell_t3431095593_marshaled_pinvoke& marshaled);
extern "C" void CompactVoxelCell_t3431095593_marshal_pinvoke_back(const CompactVoxelCell_t3431095593_marshaled_pinvoke& marshaled, CompactVoxelCell_t3431095593& unmarshaled);
extern "C" void CompactVoxelCell_t3431095593_marshal_pinvoke_cleanup(CompactVoxelCell_t3431095593_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct CompactVoxelCell_t3431095593;
struct CompactVoxelCell_t3431095593_marshaled_com;

extern "C" void CompactVoxelCell_t3431095593_marshal_com(const CompactVoxelCell_t3431095593& unmarshaled, CompactVoxelCell_t3431095593_marshaled_com& marshaled);
extern "C" void CompactVoxelCell_t3431095593_marshal_com_back(const CompactVoxelCell_t3431095593_marshaled_com& marshaled, CompactVoxelCell_t3431095593& unmarshaled);
extern "C" void CompactVoxelCell_t3431095593_marshal_com_cleanup(CompactVoxelCell_t3431095593_marshaled_com& marshaled);
