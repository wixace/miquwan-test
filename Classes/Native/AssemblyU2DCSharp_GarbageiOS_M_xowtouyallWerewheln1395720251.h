﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_xowtouyallWerewhelnis75
struct  M_xowtouyallWerewhelnis75_t1395720251  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_xowtouyallWerewhelnis75::_meaqoutallRelkastir
	int32_t ____meaqoutallRelkastir_0;
	// System.Boolean GarbageiOS.M_xowtouyallWerewhelnis75::_lonibeWesayloo
	bool ____lonibeWesayloo_1;
	// System.UInt32 GarbageiOS.M_xowtouyallWerewhelnis75::_parallkem
	uint32_t ____parallkem_2;
	// System.UInt32 GarbageiOS.M_xowtouyallWerewhelnis75::_rallrowZiscu
	uint32_t ____rallrowZiscu_3;
	// System.UInt32 GarbageiOS.M_xowtouyallWerewhelnis75::_peedallCapotir
	uint32_t ____peedallCapotir_4;
	// System.UInt32 GarbageiOS.M_xowtouyallWerewhelnis75::_rearceaJenall
	uint32_t ____rearceaJenall_5;
	// System.Single GarbageiOS.M_xowtouyallWerewhelnis75::_drudem
	float ____drudem_6;
	// System.Single GarbageiOS.M_xowtouyallWerewhelnis75::_subearjas
	float ____subearjas_7;
	// System.Int32 GarbageiOS.M_xowtouyallWerewhelnis75::_mayvouToosa
	int32_t ____mayvouToosa_8;
	// System.Int32 GarbageiOS.M_xowtouyallWerewhelnis75::_wasaFircee
	int32_t ____wasaFircee_9;
	// System.Int32 GarbageiOS.M_xowtouyallWerewhelnis75::_lafuboo
	int32_t ____lafuboo_10;
	// System.Boolean GarbageiOS.M_xowtouyallWerewhelnis75::_bimai
	bool ____bimai_11;
	// System.Int32 GarbageiOS.M_xowtouyallWerewhelnis75::_drairnetisDubewel
	int32_t ____drairnetisDubewel_12;
	// System.Boolean GarbageiOS.M_xowtouyallWerewhelnis75::_rurbiderNojebal
	bool ____rurbiderNojebal_13;
	// System.Single GarbageiOS.M_xowtouyallWerewhelnis75::_treteda
	float ____treteda_14;
	// System.Int32 GarbageiOS.M_xowtouyallWerewhelnis75::_gezisPouka
	int32_t ____gezisPouka_15;
	// System.Single GarbageiOS.M_xowtouyallWerewhelnis75::_whalzawmu
	float ____whalzawmu_16;
	// System.UInt32 GarbageiOS.M_xowtouyallWerewhelnis75::_rawforstay
	uint32_t ____rawforstay_17;
	// System.Int32 GarbageiOS.M_xowtouyallWerewhelnis75::_sereca
	int32_t ____sereca_18;
	// System.Single GarbageiOS.M_xowtouyallWerewhelnis75::_qoumirra
	float ____qoumirra_19;
	// System.String GarbageiOS.M_xowtouyallWerewhelnis75::_noudraVallfapea
	String_t* ____noudraVallfapea_20;
	// System.Int32 GarbageiOS.M_xowtouyallWerewhelnis75::_kouwasseCoude
	int32_t ____kouwasseCoude_21;
	// System.Boolean GarbageiOS.M_xowtouyallWerewhelnis75::_laqaifair
	bool ____laqaifair_22;
	// System.UInt32 GarbageiOS.M_xowtouyallWerewhelnis75::_risjas
	uint32_t ____risjas_23;
	// System.UInt32 GarbageiOS.M_xowtouyallWerewhelnis75::_betelvu
	uint32_t ____betelvu_24;
	// System.Boolean GarbageiOS.M_xowtouyallWerewhelnis75::_cojarse
	bool ____cojarse_25;
	// System.String GarbageiOS.M_xowtouyallWerewhelnis75::_mircheyo
	String_t* ____mircheyo_26;
	// System.Int32 GarbageiOS.M_xowtouyallWerewhelnis75::_resasyawWasbouchi
	int32_t ____resasyawWasbouchi_27;
	// System.UInt32 GarbageiOS.M_xowtouyallWerewhelnis75::_rallwhe
	uint32_t ____rallwhe_28;
	// System.Int32 GarbageiOS.M_xowtouyallWerewhelnis75::_noude
	int32_t ____noude_29;
	// System.UInt32 GarbageiOS.M_xowtouyallWerewhelnis75::_persirbeTokirto
	uint32_t ____persirbeTokirto_30;
	// System.Int32 GarbageiOS.M_xowtouyallWerewhelnis75::_rayyurere
	int32_t ____rayyurere_31;
	// System.String GarbageiOS.M_xowtouyallWerewhelnis75::_cajemNeechelga
	String_t* ____cajemNeechelga_32;
	// System.Single GarbageiOS.M_xowtouyallWerewhelnis75::_dotayMadrisa
	float ____dotayMadrisa_33;
	// System.Boolean GarbageiOS.M_xowtouyallWerewhelnis75::_serenisyeCorcheartere
	bool ____serenisyeCorcheartere_34;
	// System.UInt32 GarbageiOS.M_xowtouyallWerewhelnis75::_tearcemtai
	uint32_t ____tearcemtai_35;
	// System.Int32 GarbageiOS.M_xowtouyallWerewhelnis75::_jertetu
	int32_t ____jertetu_36;
	// System.Int32 GarbageiOS.M_xowtouyallWerewhelnis75::_xahirgoBerai
	int32_t ____xahirgoBerai_37;
	// System.UInt32 GarbageiOS.M_xowtouyallWerewhelnis75::_dasow
	uint32_t ____dasow_38;
	// System.String GarbageiOS.M_xowtouyallWerewhelnis75::_sooyall
	String_t* ____sooyall_39;
	// System.Single GarbageiOS.M_xowtouyallWerewhelnis75::_peapouxarKeekaraw
	float ____peapouxarKeekaraw_40;
	// System.Int32 GarbageiOS.M_xowtouyallWerewhelnis75::_hearceaTowo
	int32_t ____hearceaTowo_41;
	// System.UInt32 GarbageiOS.M_xowtouyallWerewhelnis75::_druwu
	uint32_t ____druwu_42;
	// System.String GarbageiOS.M_xowtouyallWerewhelnis75::_merpoGermasju
	String_t* ____merpoGermasju_43;

public:
	inline static int32_t get_offset_of__meaqoutallRelkastir_0() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____meaqoutallRelkastir_0)); }
	inline int32_t get__meaqoutallRelkastir_0() const { return ____meaqoutallRelkastir_0; }
	inline int32_t* get_address_of__meaqoutallRelkastir_0() { return &____meaqoutallRelkastir_0; }
	inline void set__meaqoutallRelkastir_0(int32_t value)
	{
		____meaqoutallRelkastir_0 = value;
	}

	inline static int32_t get_offset_of__lonibeWesayloo_1() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____lonibeWesayloo_1)); }
	inline bool get__lonibeWesayloo_1() const { return ____lonibeWesayloo_1; }
	inline bool* get_address_of__lonibeWesayloo_1() { return &____lonibeWesayloo_1; }
	inline void set__lonibeWesayloo_1(bool value)
	{
		____lonibeWesayloo_1 = value;
	}

	inline static int32_t get_offset_of__parallkem_2() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____parallkem_2)); }
	inline uint32_t get__parallkem_2() const { return ____parallkem_2; }
	inline uint32_t* get_address_of__parallkem_2() { return &____parallkem_2; }
	inline void set__parallkem_2(uint32_t value)
	{
		____parallkem_2 = value;
	}

	inline static int32_t get_offset_of__rallrowZiscu_3() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____rallrowZiscu_3)); }
	inline uint32_t get__rallrowZiscu_3() const { return ____rallrowZiscu_3; }
	inline uint32_t* get_address_of__rallrowZiscu_3() { return &____rallrowZiscu_3; }
	inline void set__rallrowZiscu_3(uint32_t value)
	{
		____rallrowZiscu_3 = value;
	}

	inline static int32_t get_offset_of__peedallCapotir_4() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____peedallCapotir_4)); }
	inline uint32_t get__peedallCapotir_4() const { return ____peedallCapotir_4; }
	inline uint32_t* get_address_of__peedallCapotir_4() { return &____peedallCapotir_4; }
	inline void set__peedallCapotir_4(uint32_t value)
	{
		____peedallCapotir_4 = value;
	}

	inline static int32_t get_offset_of__rearceaJenall_5() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____rearceaJenall_5)); }
	inline uint32_t get__rearceaJenall_5() const { return ____rearceaJenall_5; }
	inline uint32_t* get_address_of__rearceaJenall_5() { return &____rearceaJenall_5; }
	inline void set__rearceaJenall_5(uint32_t value)
	{
		____rearceaJenall_5 = value;
	}

	inline static int32_t get_offset_of__drudem_6() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____drudem_6)); }
	inline float get__drudem_6() const { return ____drudem_6; }
	inline float* get_address_of__drudem_6() { return &____drudem_6; }
	inline void set__drudem_6(float value)
	{
		____drudem_6 = value;
	}

	inline static int32_t get_offset_of__subearjas_7() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____subearjas_7)); }
	inline float get__subearjas_7() const { return ____subearjas_7; }
	inline float* get_address_of__subearjas_7() { return &____subearjas_7; }
	inline void set__subearjas_7(float value)
	{
		____subearjas_7 = value;
	}

	inline static int32_t get_offset_of__mayvouToosa_8() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____mayvouToosa_8)); }
	inline int32_t get__mayvouToosa_8() const { return ____mayvouToosa_8; }
	inline int32_t* get_address_of__mayvouToosa_8() { return &____mayvouToosa_8; }
	inline void set__mayvouToosa_8(int32_t value)
	{
		____mayvouToosa_8 = value;
	}

	inline static int32_t get_offset_of__wasaFircee_9() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____wasaFircee_9)); }
	inline int32_t get__wasaFircee_9() const { return ____wasaFircee_9; }
	inline int32_t* get_address_of__wasaFircee_9() { return &____wasaFircee_9; }
	inline void set__wasaFircee_9(int32_t value)
	{
		____wasaFircee_9 = value;
	}

	inline static int32_t get_offset_of__lafuboo_10() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____lafuboo_10)); }
	inline int32_t get__lafuboo_10() const { return ____lafuboo_10; }
	inline int32_t* get_address_of__lafuboo_10() { return &____lafuboo_10; }
	inline void set__lafuboo_10(int32_t value)
	{
		____lafuboo_10 = value;
	}

	inline static int32_t get_offset_of__bimai_11() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____bimai_11)); }
	inline bool get__bimai_11() const { return ____bimai_11; }
	inline bool* get_address_of__bimai_11() { return &____bimai_11; }
	inline void set__bimai_11(bool value)
	{
		____bimai_11 = value;
	}

	inline static int32_t get_offset_of__drairnetisDubewel_12() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____drairnetisDubewel_12)); }
	inline int32_t get__drairnetisDubewel_12() const { return ____drairnetisDubewel_12; }
	inline int32_t* get_address_of__drairnetisDubewel_12() { return &____drairnetisDubewel_12; }
	inline void set__drairnetisDubewel_12(int32_t value)
	{
		____drairnetisDubewel_12 = value;
	}

	inline static int32_t get_offset_of__rurbiderNojebal_13() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____rurbiderNojebal_13)); }
	inline bool get__rurbiderNojebal_13() const { return ____rurbiderNojebal_13; }
	inline bool* get_address_of__rurbiderNojebal_13() { return &____rurbiderNojebal_13; }
	inline void set__rurbiderNojebal_13(bool value)
	{
		____rurbiderNojebal_13 = value;
	}

	inline static int32_t get_offset_of__treteda_14() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____treteda_14)); }
	inline float get__treteda_14() const { return ____treteda_14; }
	inline float* get_address_of__treteda_14() { return &____treteda_14; }
	inline void set__treteda_14(float value)
	{
		____treteda_14 = value;
	}

	inline static int32_t get_offset_of__gezisPouka_15() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____gezisPouka_15)); }
	inline int32_t get__gezisPouka_15() const { return ____gezisPouka_15; }
	inline int32_t* get_address_of__gezisPouka_15() { return &____gezisPouka_15; }
	inline void set__gezisPouka_15(int32_t value)
	{
		____gezisPouka_15 = value;
	}

	inline static int32_t get_offset_of__whalzawmu_16() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____whalzawmu_16)); }
	inline float get__whalzawmu_16() const { return ____whalzawmu_16; }
	inline float* get_address_of__whalzawmu_16() { return &____whalzawmu_16; }
	inline void set__whalzawmu_16(float value)
	{
		____whalzawmu_16 = value;
	}

	inline static int32_t get_offset_of__rawforstay_17() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____rawforstay_17)); }
	inline uint32_t get__rawforstay_17() const { return ____rawforstay_17; }
	inline uint32_t* get_address_of__rawforstay_17() { return &____rawforstay_17; }
	inline void set__rawforstay_17(uint32_t value)
	{
		____rawforstay_17 = value;
	}

	inline static int32_t get_offset_of__sereca_18() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____sereca_18)); }
	inline int32_t get__sereca_18() const { return ____sereca_18; }
	inline int32_t* get_address_of__sereca_18() { return &____sereca_18; }
	inline void set__sereca_18(int32_t value)
	{
		____sereca_18 = value;
	}

	inline static int32_t get_offset_of__qoumirra_19() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____qoumirra_19)); }
	inline float get__qoumirra_19() const { return ____qoumirra_19; }
	inline float* get_address_of__qoumirra_19() { return &____qoumirra_19; }
	inline void set__qoumirra_19(float value)
	{
		____qoumirra_19 = value;
	}

	inline static int32_t get_offset_of__noudraVallfapea_20() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____noudraVallfapea_20)); }
	inline String_t* get__noudraVallfapea_20() const { return ____noudraVallfapea_20; }
	inline String_t** get_address_of__noudraVallfapea_20() { return &____noudraVallfapea_20; }
	inline void set__noudraVallfapea_20(String_t* value)
	{
		____noudraVallfapea_20 = value;
		Il2CppCodeGenWriteBarrier(&____noudraVallfapea_20, value);
	}

	inline static int32_t get_offset_of__kouwasseCoude_21() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____kouwasseCoude_21)); }
	inline int32_t get__kouwasseCoude_21() const { return ____kouwasseCoude_21; }
	inline int32_t* get_address_of__kouwasseCoude_21() { return &____kouwasseCoude_21; }
	inline void set__kouwasseCoude_21(int32_t value)
	{
		____kouwasseCoude_21 = value;
	}

	inline static int32_t get_offset_of__laqaifair_22() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____laqaifair_22)); }
	inline bool get__laqaifair_22() const { return ____laqaifair_22; }
	inline bool* get_address_of__laqaifair_22() { return &____laqaifair_22; }
	inline void set__laqaifair_22(bool value)
	{
		____laqaifair_22 = value;
	}

	inline static int32_t get_offset_of__risjas_23() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____risjas_23)); }
	inline uint32_t get__risjas_23() const { return ____risjas_23; }
	inline uint32_t* get_address_of__risjas_23() { return &____risjas_23; }
	inline void set__risjas_23(uint32_t value)
	{
		____risjas_23 = value;
	}

	inline static int32_t get_offset_of__betelvu_24() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____betelvu_24)); }
	inline uint32_t get__betelvu_24() const { return ____betelvu_24; }
	inline uint32_t* get_address_of__betelvu_24() { return &____betelvu_24; }
	inline void set__betelvu_24(uint32_t value)
	{
		____betelvu_24 = value;
	}

	inline static int32_t get_offset_of__cojarse_25() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____cojarse_25)); }
	inline bool get__cojarse_25() const { return ____cojarse_25; }
	inline bool* get_address_of__cojarse_25() { return &____cojarse_25; }
	inline void set__cojarse_25(bool value)
	{
		____cojarse_25 = value;
	}

	inline static int32_t get_offset_of__mircheyo_26() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____mircheyo_26)); }
	inline String_t* get__mircheyo_26() const { return ____mircheyo_26; }
	inline String_t** get_address_of__mircheyo_26() { return &____mircheyo_26; }
	inline void set__mircheyo_26(String_t* value)
	{
		____mircheyo_26 = value;
		Il2CppCodeGenWriteBarrier(&____mircheyo_26, value);
	}

	inline static int32_t get_offset_of__resasyawWasbouchi_27() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____resasyawWasbouchi_27)); }
	inline int32_t get__resasyawWasbouchi_27() const { return ____resasyawWasbouchi_27; }
	inline int32_t* get_address_of__resasyawWasbouchi_27() { return &____resasyawWasbouchi_27; }
	inline void set__resasyawWasbouchi_27(int32_t value)
	{
		____resasyawWasbouchi_27 = value;
	}

	inline static int32_t get_offset_of__rallwhe_28() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____rallwhe_28)); }
	inline uint32_t get__rallwhe_28() const { return ____rallwhe_28; }
	inline uint32_t* get_address_of__rallwhe_28() { return &____rallwhe_28; }
	inline void set__rallwhe_28(uint32_t value)
	{
		____rallwhe_28 = value;
	}

	inline static int32_t get_offset_of__noude_29() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____noude_29)); }
	inline int32_t get__noude_29() const { return ____noude_29; }
	inline int32_t* get_address_of__noude_29() { return &____noude_29; }
	inline void set__noude_29(int32_t value)
	{
		____noude_29 = value;
	}

	inline static int32_t get_offset_of__persirbeTokirto_30() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____persirbeTokirto_30)); }
	inline uint32_t get__persirbeTokirto_30() const { return ____persirbeTokirto_30; }
	inline uint32_t* get_address_of__persirbeTokirto_30() { return &____persirbeTokirto_30; }
	inline void set__persirbeTokirto_30(uint32_t value)
	{
		____persirbeTokirto_30 = value;
	}

	inline static int32_t get_offset_of__rayyurere_31() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____rayyurere_31)); }
	inline int32_t get__rayyurere_31() const { return ____rayyurere_31; }
	inline int32_t* get_address_of__rayyurere_31() { return &____rayyurere_31; }
	inline void set__rayyurere_31(int32_t value)
	{
		____rayyurere_31 = value;
	}

	inline static int32_t get_offset_of__cajemNeechelga_32() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____cajemNeechelga_32)); }
	inline String_t* get__cajemNeechelga_32() const { return ____cajemNeechelga_32; }
	inline String_t** get_address_of__cajemNeechelga_32() { return &____cajemNeechelga_32; }
	inline void set__cajemNeechelga_32(String_t* value)
	{
		____cajemNeechelga_32 = value;
		Il2CppCodeGenWriteBarrier(&____cajemNeechelga_32, value);
	}

	inline static int32_t get_offset_of__dotayMadrisa_33() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____dotayMadrisa_33)); }
	inline float get__dotayMadrisa_33() const { return ____dotayMadrisa_33; }
	inline float* get_address_of__dotayMadrisa_33() { return &____dotayMadrisa_33; }
	inline void set__dotayMadrisa_33(float value)
	{
		____dotayMadrisa_33 = value;
	}

	inline static int32_t get_offset_of__serenisyeCorcheartere_34() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____serenisyeCorcheartere_34)); }
	inline bool get__serenisyeCorcheartere_34() const { return ____serenisyeCorcheartere_34; }
	inline bool* get_address_of__serenisyeCorcheartere_34() { return &____serenisyeCorcheartere_34; }
	inline void set__serenisyeCorcheartere_34(bool value)
	{
		____serenisyeCorcheartere_34 = value;
	}

	inline static int32_t get_offset_of__tearcemtai_35() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____tearcemtai_35)); }
	inline uint32_t get__tearcemtai_35() const { return ____tearcemtai_35; }
	inline uint32_t* get_address_of__tearcemtai_35() { return &____tearcemtai_35; }
	inline void set__tearcemtai_35(uint32_t value)
	{
		____tearcemtai_35 = value;
	}

	inline static int32_t get_offset_of__jertetu_36() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____jertetu_36)); }
	inline int32_t get__jertetu_36() const { return ____jertetu_36; }
	inline int32_t* get_address_of__jertetu_36() { return &____jertetu_36; }
	inline void set__jertetu_36(int32_t value)
	{
		____jertetu_36 = value;
	}

	inline static int32_t get_offset_of__xahirgoBerai_37() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____xahirgoBerai_37)); }
	inline int32_t get__xahirgoBerai_37() const { return ____xahirgoBerai_37; }
	inline int32_t* get_address_of__xahirgoBerai_37() { return &____xahirgoBerai_37; }
	inline void set__xahirgoBerai_37(int32_t value)
	{
		____xahirgoBerai_37 = value;
	}

	inline static int32_t get_offset_of__dasow_38() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____dasow_38)); }
	inline uint32_t get__dasow_38() const { return ____dasow_38; }
	inline uint32_t* get_address_of__dasow_38() { return &____dasow_38; }
	inline void set__dasow_38(uint32_t value)
	{
		____dasow_38 = value;
	}

	inline static int32_t get_offset_of__sooyall_39() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____sooyall_39)); }
	inline String_t* get__sooyall_39() const { return ____sooyall_39; }
	inline String_t** get_address_of__sooyall_39() { return &____sooyall_39; }
	inline void set__sooyall_39(String_t* value)
	{
		____sooyall_39 = value;
		Il2CppCodeGenWriteBarrier(&____sooyall_39, value);
	}

	inline static int32_t get_offset_of__peapouxarKeekaraw_40() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____peapouxarKeekaraw_40)); }
	inline float get__peapouxarKeekaraw_40() const { return ____peapouxarKeekaraw_40; }
	inline float* get_address_of__peapouxarKeekaraw_40() { return &____peapouxarKeekaraw_40; }
	inline void set__peapouxarKeekaraw_40(float value)
	{
		____peapouxarKeekaraw_40 = value;
	}

	inline static int32_t get_offset_of__hearceaTowo_41() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____hearceaTowo_41)); }
	inline int32_t get__hearceaTowo_41() const { return ____hearceaTowo_41; }
	inline int32_t* get_address_of__hearceaTowo_41() { return &____hearceaTowo_41; }
	inline void set__hearceaTowo_41(int32_t value)
	{
		____hearceaTowo_41 = value;
	}

	inline static int32_t get_offset_of__druwu_42() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____druwu_42)); }
	inline uint32_t get__druwu_42() const { return ____druwu_42; }
	inline uint32_t* get_address_of__druwu_42() { return &____druwu_42; }
	inline void set__druwu_42(uint32_t value)
	{
		____druwu_42 = value;
	}

	inline static int32_t get_offset_of__merpoGermasju_43() { return static_cast<int32_t>(offsetof(M_xowtouyallWerewhelnis75_t1395720251, ____merpoGermasju_43)); }
	inline String_t* get__merpoGermasju_43() const { return ____merpoGermasju_43; }
	inline String_t** get_address_of__merpoGermasju_43() { return &____merpoGermasju_43; }
	inline void set__merpoGermasju_43(String_t* value)
	{
		____merpoGermasju_43 = value;
		Il2CppCodeGenWriteBarrier(&____merpoGermasju_43, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
