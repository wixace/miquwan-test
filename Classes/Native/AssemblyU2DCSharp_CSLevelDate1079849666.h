﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CSCheckPointUnit
struct CSCheckPointUnit_t3129216924;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSLevelDate
struct  CSLevelDate_t1079849666  : public Il2CppObject
{
public:
	// CSCheckPointUnit CSLevelDate::checkpointUnit
	CSCheckPointUnit_t3129216924 * ___checkpointUnit_0;

public:
	inline static int32_t get_offset_of_checkpointUnit_0() { return static_cast<int32_t>(offsetof(CSLevelDate_t1079849666, ___checkpointUnit_0)); }
	inline CSCheckPointUnit_t3129216924 * get_checkpointUnit_0() const { return ___checkpointUnit_0; }
	inline CSCheckPointUnit_t3129216924 ** get_address_of_checkpointUnit_0() { return &___checkpointUnit_0; }
	inline void set_checkpointUnit_0(CSCheckPointUnit_t3129216924 * value)
	{
		___checkpointUnit_0 = value;
		Il2CppCodeGenWriteBarrier(&___checkpointUnit_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
