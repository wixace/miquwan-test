﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Filelist`1<FileInfoCrc>
struct Filelist_1_t2494800866;
// Filelist`1<FileInfoRes>
struct Filelist_1_t2494814894;
// HttpDownLoad
struct HttpDownLoad_t2310403440;
// ABCheckUpdate
struct ABCheckUpdate_t527832336;
// System.Collections.Generic.List`1<FileInfoRes>
struct List_1_t2585245350;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// AssetDownMgr/SetBytes
struct SetBytes_t2856136722;
// System.Action
struct Action_t3771233898;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ABCheckUpdate
struct  ABCheckUpdate_t527832336  : public Il2CppObject
{
public:
	// Filelist`1<FileInfoCrc> ABCheckUpdate::clientCrcList
	Filelist_1_t2494800866 * ___clientCrcList_5;
	// Filelist`1<FileInfoCrc> ABCheckUpdate::serverCrcList
	Filelist_1_t2494800866 * ___serverCrcList_6;
	// Filelist`1<FileInfoRes> ABCheckUpdate::clientList
	Filelist_1_t2494814894 * ___clientList_7;
	// Filelist`1<FileInfoRes> ABCheckUpdate::serverList
	Filelist_1_t2494814894 * ___serverList_8;
	// Filelist`1<FileInfoRes> ABCheckUpdate::clientPersistentList
	Filelist_1_t2494814894 * ___clientPersistentList_9;
	// Filelist`1<FileInfoRes> ABCheckUpdate::clientStreamingList
	Filelist_1_t2494814894 * ___clientStreamingList_10;
	// HttpDownLoad ABCheckUpdate::httpDownLoad
	HttpDownLoad_t2310403440 * ___httpDownLoad_11;
	// System.String ABCheckUpdate::updateTargetVS
	String_t* ___updateTargetVS_12;
	// System.Boolean ABCheckUpdate::isRestart
	bool ___isRestart_13;
	// System.String ABCheckUpdate::clientVer
	String_t* ___clientVer_14;
	// System.UInt32 ABCheckUpdate::curBytes
	uint32_t ___curBytes_16;
	// System.Int32 ABCheckUpdate::downedNum
	int32_t ___downedNum_17;
	// System.Collections.Generic.List`1<FileInfoRes> ABCheckUpdate::removeList
	List_1_t2585245350 * ___removeList_18;
	// System.Text.StringBuilder ABCheckUpdate::sb
	StringBuilder_t243639308 * ___sb_19;
	// System.String ABCheckUpdate::serverVer
	String_t* ___serverVer_20;
	// System.UInt32 ABCheckUpdate::totalBytes
	uint32_t ___totalBytes_21;
	// System.Collections.Generic.List`1<FileInfoRes> ABCheckUpdate::updateList
	List_1_t2585245350 * ___updateList_22;
	// System.Int32 ABCheckUpdate::downFileNum
	int32_t ___downFileNum_23;
	// System.Int32 ABCheckUpdate::downFileCount
	int32_t ___downFileCount_24;
	// System.Boolean ABCheckUpdate::isloadServer
	bool ___isloadServer_25;
	// System.Boolean ABCheckUpdate::isRepairing
	bool ___isRepairing_26;
	// System.Int32 ABCheckUpdate::localAssetCount
	int32_t ___localAssetCount_27;
	// System.Int32 ABCheckUpdate::errorAssetCount
	int32_t ___errorAssetCount_28;
	// System.Boolean ABCheckUpdate::<isPkged>k__BackingField
	bool ___U3CisPkgedU3Ek__BackingField_29;
	// System.Boolean ABCheckUpdate::<isVerifyed>k__BackingField
	bool ___U3CisVerifyedU3Ek__BackingField_30;
	// System.Single ABCheckUpdate::<VerifyProgress>k__BackingField
	float ___U3CVerifyProgressU3Ek__BackingField_31;
	// System.Boolean ABCheckUpdate::<isStartDown>k__BackingField
	bool ___U3CisStartDownU3Ek__BackingField_32;

public:
	inline static int32_t get_offset_of_clientCrcList_5() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___clientCrcList_5)); }
	inline Filelist_1_t2494800866 * get_clientCrcList_5() const { return ___clientCrcList_5; }
	inline Filelist_1_t2494800866 ** get_address_of_clientCrcList_5() { return &___clientCrcList_5; }
	inline void set_clientCrcList_5(Filelist_1_t2494800866 * value)
	{
		___clientCrcList_5 = value;
		Il2CppCodeGenWriteBarrier(&___clientCrcList_5, value);
	}

	inline static int32_t get_offset_of_serverCrcList_6() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___serverCrcList_6)); }
	inline Filelist_1_t2494800866 * get_serverCrcList_6() const { return ___serverCrcList_6; }
	inline Filelist_1_t2494800866 ** get_address_of_serverCrcList_6() { return &___serverCrcList_6; }
	inline void set_serverCrcList_6(Filelist_1_t2494800866 * value)
	{
		___serverCrcList_6 = value;
		Il2CppCodeGenWriteBarrier(&___serverCrcList_6, value);
	}

	inline static int32_t get_offset_of_clientList_7() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___clientList_7)); }
	inline Filelist_1_t2494814894 * get_clientList_7() const { return ___clientList_7; }
	inline Filelist_1_t2494814894 ** get_address_of_clientList_7() { return &___clientList_7; }
	inline void set_clientList_7(Filelist_1_t2494814894 * value)
	{
		___clientList_7 = value;
		Il2CppCodeGenWriteBarrier(&___clientList_7, value);
	}

	inline static int32_t get_offset_of_serverList_8() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___serverList_8)); }
	inline Filelist_1_t2494814894 * get_serverList_8() const { return ___serverList_8; }
	inline Filelist_1_t2494814894 ** get_address_of_serverList_8() { return &___serverList_8; }
	inline void set_serverList_8(Filelist_1_t2494814894 * value)
	{
		___serverList_8 = value;
		Il2CppCodeGenWriteBarrier(&___serverList_8, value);
	}

	inline static int32_t get_offset_of_clientPersistentList_9() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___clientPersistentList_9)); }
	inline Filelist_1_t2494814894 * get_clientPersistentList_9() const { return ___clientPersistentList_9; }
	inline Filelist_1_t2494814894 ** get_address_of_clientPersistentList_9() { return &___clientPersistentList_9; }
	inline void set_clientPersistentList_9(Filelist_1_t2494814894 * value)
	{
		___clientPersistentList_9 = value;
		Il2CppCodeGenWriteBarrier(&___clientPersistentList_9, value);
	}

	inline static int32_t get_offset_of_clientStreamingList_10() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___clientStreamingList_10)); }
	inline Filelist_1_t2494814894 * get_clientStreamingList_10() const { return ___clientStreamingList_10; }
	inline Filelist_1_t2494814894 ** get_address_of_clientStreamingList_10() { return &___clientStreamingList_10; }
	inline void set_clientStreamingList_10(Filelist_1_t2494814894 * value)
	{
		___clientStreamingList_10 = value;
		Il2CppCodeGenWriteBarrier(&___clientStreamingList_10, value);
	}

	inline static int32_t get_offset_of_httpDownLoad_11() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___httpDownLoad_11)); }
	inline HttpDownLoad_t2310403440 * get_httpDownLoad_11() const { return ___httpDownLoad_11; }
	inline HttpDownLoad_t2310403440 ** get_address_of_httpDownLoad_11() { return &___httpDownLoad_11; }
	inline void set_httpDownLoad_11(HttpDownLoad_t2310403440 * value)
	{
		___httpDownLoad_11 = value;
		Il2CppCodeGenWriteBarrier(&___httpDownLoad_11, value);
	}

	inline static int32_t get_offset_of_updateTargetVS_12() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___updateTargetVS_12)); }
	inline String_t* get_updateTargetVS_12() const { return ___updateTargetVS_12; }
	inline String_t** get_address_of_updateTargetVS_12() { return &___updateTargetVS_12; }
	inline void set_updateTargetVS_12(String_t* value)
	{
		___updateTargetVS_12 = value;
		Il2CppCodeGenWriteBarrier(&___updateTargetVS_12, value);
	}

	inline static int32_t get_offset_of_isRestart_13() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___isRestart_13)); }
	inline bool get_isRestart_13() const { return ___isRestart_13; }
	inline bool* get_address_of_isRestart_13() { return &___isRestart_13; }
	inline void set_isRestart_13(bool value)
	{
		___isRestart_13 = value;
	}

	inline static int32_t get_offset_of_clientVer_14() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___clientVer_14)); }
	inline String_t* get_clientVer_14() const { return ___clientVer_14; }
	inline String_t** get_address_of_clientVer_14() { return &___clientVer_14; }
	inline void set_clientVer_14(String_t* value)
	{
		___clientVer_14 = value;
		Il2CppCodeGenWriteBarrier(&___clientVer_14, value);
	}

	inline static int32_t get_offset_of_curBytes_16() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___curBytes_16)); }
	inline uint32_t get_curBytes_16() const { return ___curBytes_16; }
	inline uint32_t* get_address_of_curBytes_16() { return &___curBytes_16; }
	inline void set_curBytes_16(uint32_t value)
	{
		___curBytes_16 = value;
	}

	inline static int32_t get_offset_of_downedNum_17() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___downedNum_17)); }
	inline int32_t get_downedNum_17() const { return ___downedNum_17; }
	inline int32_t* get_address_of_downedNum_17() { return &___downedNum_17; }
	inline void set_downedNum_17(int32_t value)
	{
		___downedNum_17 = value;
	}

	inline static int32_t get_offset_of_removeList_18() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___removeList_18)); }
	inline List_1_t2585245350 * get_removeList_18() const { return ___removeList_18; }
	inline List_1_t2585245350 ** get_address_of_removeList_18() { return &___removeList_18; }
	inline void set_removeList_18(List_1_t2585245350 * value)
	{
		___removeList_18 = value;
		Il2CppCodeGenWriteBarrier(&___removeList_18, value);
	}

	inline static int32_t get_offset_of_sb_19() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___sb_19)); }
	inline StringBuilder_t243639308 * get_sb_19() const { return ___sb_19; }
	inline StringBuilder_t243639308 ** get_address_of_sb_19() { return &___sb_19; }
	inline void set_sb_19(StringBuilder_t243639308 * value)
	{
		___sb_19 = value;
		Il2CppCodeGenWriteBarrier(&___sb_19, value);
	}

	inline static int32_t get_offset_of_serverVer_20() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___serverVer_20)); }
	inline String_t* get_serverVer_20() const { return ___serverVer_20; }
	inline String_t** get_address_of_serverVer_20() { return &___serverVer_20; }
	inline void set_serverVer_20(String_t* value)
	{
		___serverVer_20 = value;
		Il2CppCodeGenWriteBarrier(&___serverVer_20, value);
	}

	inline static int32_t get_offset_of_totalBytes_21() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___totalBytes_21)); }
	inline uint32_t get_totalBytes_21() const { return ___totalBytes_21; }
	inline uint32_t* get_address_of_totalBytes_21() { return &___totalBytes_21; }
	inline void set_totalBytes_21(uint32_t value)
	{
		___totalBytes_21 = value;
	}

	inline static int32_t get_offset_of_updateList_22() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___updateList_22)); }
	inline List_1_t2585245350 * get_updateList_22() const { return ___updateList_22; }
	inline List_1_t2585245350 ** get_address_of_updateList_22() { return &___updateList_22; }
	inline void set_updateList_22(List_1_t2585245350 * value)
	{
		___updateList_22 = value;
		Il2CppCodeGenWriteBarrier(&___updateList_22, value);
	}

	inline static int32_t get_offset_of_downFileNum_23() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___downFileNum_23)); }
	inline int32_t get_downFileNum_23() const { return ___downFileNum_23; }
	inline int32_t* get_address_of_downFileNum_23() { return &___downFileNum_23; }
	inline void set_downFileNum_23(int32_t value)
	{
		___downFileNum_23 = value;
	}

	inline static int32_t get_offset_of_downFileCount_24() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___downFileCount_24)); }
	inline int32_t get_downFileCount_24() const { return ___downFileCount_24; }
	inline int32_t* get_address_of_downFileCount_24() { return &___downFileCount_24; }
	inline void set_downFileCount_24(int32_t value)
	{
		___downFileCount_24 = value;
	}

	inline static int32_t get_offset_of_isloadServer_25() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___isloadServer_25)); }
	inline bool get_isloadServer_25() const { return ___isloadServer_25; }
	inline bool* get_address_of_isloadServer_25() { return &___isloadServer_25; }
	inline void set_isloadServer_25(bool value)
	{
		___isloadServer_25 = value;
	}

	inline static int32_t get_offset_of_isRepairing_26() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___isRepairing_26)); }
	inline bool get_isRepairing_26() const { return ___isRepairing_26; }
	inline bool* get_address_of_isRepairing_26() { return &___isRepairing_26; }
	inline void set_isRepairing_26(bool value)
	{
		___isRepairing_26 = value;
	}

	inline static int32_t get_offset_of_localAssetCount_27() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___localAssetCount_27)); }
	inline int32_t get_localAssetCount_27() const { return ___localAssetCount_27; }
	inline int32_t* get_address_of_localAssetCount_27() { return &___localAssetCount_27; }
	inline void set_localAssetCount_27(int32_t value)
	{
		___localAssetCount_27 = value;
	}

	inline static int32_t get_offset_of_errorAssetCount_28() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___errorAssetCount_28)); }
	inline int32_t get_errorAssetCount_28() const { return ___errorAssetCount_28; }
	inline int32_t* get_address_of_errorAssetCount_28() { return &___errorAssetCount_28; }
	inline void set_errorAssetCount_28(int32_t value)
	{
		___errorAssetCount_28 = value;
	}

	inline static int32_t get_offset_of_U3CisPkgedU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___U3CisPkgedU3Ek__BackingField_29)); }
	inline bool get_U3CisPkgedU3Ek__BackingField_29() const { return ___U3CisPkgedU3Ek__BackingField_29; }
	inline bool* get_address_of_U3CisPkgedU3Ek__BackingField_29() { return &___U3CisPkgedU3Ek__BackingField_29; }
	inline void set_U3CisPkgedU3Ek__BackingField_29(bool value)
	{
		___U3CisPkgedU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CisVerifyedU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___U3CisVerifyedU3Ek__BackingField_30)); }
	inline bool get_U3CisVerifyedU3Ek__BackingField_30() const { return ___U3CisVerifyedU3Ek__BackingField_30; }
	inline bool* get_address_of_U3CisVerifyedU3Ek__BackingField_30() { return &___U3CisVerifyedU3Ek__BackingField_30; }
	inline void set_U3CisVerifyedU3Ek__BackingField_30(bool value)
	{
		___U3CisVerifyedU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CVerifyProgressU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___U3CVerifyProgressU3Ek__BackingField_31)); }
	inline float get_U3CVerifyProgressU3Ek__BackingField_31() const { return ___U3CVerifyProgressU3Ek__BackingField_31; }
	inline float* get_address_of_U3CVerifyProgressU3Ek__BackingField_31() { return &___U3CVerifyProgressU3Ek__BackingField_31; }
	inline void set_U3CVerifyProgressU3Ek__BackingField_31(float value)
	{
		___U3CVerifyProgressU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CisStartDownU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336, ___U3CisStartDownU3Ek__BackingField_32)); }
	inline bool get_U3CisStartDownU3Ek__BackingField_32() const { return ___U3CisStartDownU3Ek__BackingField_32; }
	inline bool* get_address_of_U3CisStartDownU3Ek__BackingField_32() { return &___U3CisStartDownU3Ek__BackingField_32; }
	inline void set_U3CisStartDownU3Ek__BackingField_32(bool value)
	{
		___U3CisStartDownU3Ek__BackingField_32 = value;
	}
};

struct ABCheckUpdate_t527832336_StaticFields
{
public:
	// ABCheckUpdate ABCheckUpdate::_instance
	ABCheckUpdate_t527832336 * ____instance_15;
	// AssetDownMgr/SetBytes ABCheckUpdate::<>f__am$cache1C
	SetBytes_t2856136722 * ___U3CU3Ef__amU24cache1C_33;
	// System.Action ABCheckUpdate::<>f__am$cache1D
	Action_t3771233898 * ___U3CU3Ef__amU24cache1D_34;
	// System.Action ABCheckUpdate::<>f__am$cache1E
	Action_t3771233898 * ___U3CU3Ef__amU24cache1E_35;

public:
	inline static int32_t get_offset_of__instance_15() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336_StaticFields, ____instance_15)); }
	inline ABCheckUpdate_t527832336 * get__instance_15() const { return ____instance_15; }
	inline ABCheckUpdate_t527832336 ** get_address_of__instance_15() { return &____instance_15; }
	inline void set__instance_15(ABCheckUpdate_t527832336 * value)
	{
		____instance_15 = value;
		Il2CppCodeGenWriteBarrier(&____instance_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1C_33() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336_StaticFields, ___U3CU3Ef__amU24cache1C_33)); }
	inline SetBytes_t2856136722 * get_U3CU3Ef__amU24cache1C_33() const { return ___U3CU3Ef__amU24cache1C_33; }
	inline SetBytes_t2856136722 ** get_address_of_U3CU3Ef__amU24cache1C_33() { return &___U3CU3Ef__amU24cache1C_33; }
	inline void set_U3CU3Ef__amU24cache1C_33(SetBytes_t2856136722 * value)
	{
		___U3CU3Ef__amU24cache1C_33 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1C_33, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1D_34() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336_StaticFields, ___U3CU3Ef__amU24cache1D_34)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache1D_34() const { return ___U3CU3Ef__amU24cache1D_34; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache1D_34() { return &___U3CU3Ef__amU24cache1D_34; }
	inline void set_U3CU3Ef__amU24cache1D_34(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache1D_34 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1D_34, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1E_35() { return static_cast<int32_t>(offsetof(ABCheckUpdate_t527832336_StaticFields, ___U3CU3Ef__amU24cache1E_35)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache1E_35() const { return ___U3CU3Ef__amU24cache1E_35; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache1E_35() { return &___U3CU3Ef__amU24cache1E_35; }
	inline void set_U3CU3Ef__amU24cache1E_35(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache1E_35 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1E_35, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
