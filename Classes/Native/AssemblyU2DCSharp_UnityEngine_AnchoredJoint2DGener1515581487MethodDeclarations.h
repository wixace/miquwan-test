﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AnchoredJoint2DGenerated
struct UnityEngine_AnchoredJoint2DGenerated_t1515581487;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine_AnchoredJoint2DGenerated::.ctor()
extern "C"  void UnityEngine_AnchoredJoint2DGenerated__ctor_m235208524 (UnityEngine_AnchoredJoint2DGenerated_t1515581487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnchoredJoint2DGenerated::AnchoredJoint2D_AnchoredJoint2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnchoredJoint2DGenerated_AnchoredJoint2D_AnchoredJoint2D1_m1915740316 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnchoredJoint2DGenerated::AnchoredJoint2D_anchor(JSVCall)
extern "C"  void UnityEngine_AnchoredJoint2DGenerated_AnchoredJoint2D_anchor_m340056131 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnchoredJoint2DGenerated::AnchoredJoint2D_connectedAnchor(JSVCall)
extern "C"  void UnityEngine_AnchoredJoint2DGenerated_AnchoredJoint2D_connectedAnchor_m423072406 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnchoredJoint2DGenerated::AnchoredJoint2D_autoConfigureConnectedAnchor(JSVCall)
extern "C"  void UnityEngine_AnchoredJoint2DGenerated_AnchoredJoint2D_autoConfigureConnectedAnchor_m523962769 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnchoredJoint2DGenerated::__Register()
extern "C"  void UnityEngine_AnchoredJoint2DGenerated___Register_m529804539 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnchoredJoint2DGenerated::ilo_setVector2S1(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_AnchoredJoint2DGenerated_ilo_setVector2S1_m3372026843 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_AnchoredJoint2DGenerated::ilo_getVector2S2(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_AnchoredJoint2DGenerated_ilo_getVector2S2_m3698629775 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnchoredJoint2DGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void UnityEngine_AnchoredJoint2DGenerated_ilo_setBooleanS3_m1598021627 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
