﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct List_1_t4002166762;
// System.Collections.Generic.IEnumerable`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct IEnumerable_1_t1639926871;
// System.Collections.Generic.IEnumerator`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct IEnumerator_1_t250878963;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct ICollection_1_t3528571197;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct ReadOnlyCollection_1_t4191058746;
// Mihua.Assets.SubAssetMgr/ErrorInfo[]
struct ErrorInfoU5BU5D_t2864912703;
// System.Predicate`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct Predicate_1_t2245038093;
// System.Collections.Generic.IComparer`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct IComparer_1_t914027956;
// System.Comparison`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct Comparison_1_t1350342397;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr_ErrorIn2633981210.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4021839532.h"

// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor()
extern "C"  void List_1__ctor_m2244991173_gshared (List_1_t4002166762 * __this, const MethodInfo* method);
#define List_1__ctor_m2244991173(__this, method) ((  void (*) (List_1_t4002166762 *, const MethodInfo*))List_1__ctor_m2244991173_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m154580785_gshared (List_1_t4002166762 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m154580785(__this, ___collection0, method) ((  void (*) (List_1_t4002166762 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m154580785_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m433584351_gshared (List_1_t4002166762 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m433584351(__this, ___capacity0, method) ((  void (*) (List_1_t4002166762 *, int32_t, const MethodInfo*))List_1__ctor_m433584351_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::.cctor()
extern "C"  void List_1__cctor_m805319903_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m805319903(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m805319903_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2063014424_gshared (List_1_t4002166762 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2063014424(__this, method) ((  Il2CppObject* (*) (List_1_t4002166762 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2063014424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3265546358_gshared (List_1_t4002166762 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3265546358(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4002166762 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3265546358_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3937113989_gshared (List_1_t4002166762 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3937113989(__this, method) ((  Il2CppObject * (*) (List_1_t4002166762 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3937113989_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m985119576_gshared (List_1_t4002166762 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m985119576(__this, ___item0, method) ((  int32_t (*) (List_1_t4002166762 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m985119576_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m408448104_gshared (List_1_t4002166762 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m408448104(__this, ___item0, method) ((  bool (*) (List_1_t4002166762 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m408448104_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m108692912_gshared (List_1_t4002166762 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m108692912(__this, ___item0, method) ((  int32_t (*) (List_1_t4002166762 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m108692912_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3344484899_gshared (List_1_t4002166762 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3344484899(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4002166762 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3344484899_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m2557354533_gshared (List_1_t4002166762 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m2557354533(__this, ___item0, method) ((  void (*) (List_1_t4002166762 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m2557354533_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m576060009_gshared (List_1_t4002166762 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m576060009(__this, method) ((  bool (*) (List_1_t4002166762 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m576060009_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m430092788_gshared (List_1_t4002166762 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m430092788(__this, method) ((  bool (*) (List_1_t4002166762 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m430092788_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2674157734_gshared (List_1_t4002166762 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2674157734(__this, method) ((  Il2CppObject * (*) (List_1_t4002166762 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2674157734_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m715360727_gshared (List_1_t4002166762 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m715360727(__this, method) ((  bool (*) (List_1_t4002166762 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m715360727_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m754179650_gshared (List_1_t4002166762 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m754179650(__this, method) ((  bool (*) (List_1_t4002166762 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m754179650_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2540751021_gshared (List_1_t4002166762 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2540751021(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t4002166762 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2540751021_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m2028403002_gshared (List_1_t4002166762 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m2028403002(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4002166762 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m2028403002_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::Add(T)
extern "C"  void List_1_Add_m1939351989_gshared (List_1_t4002166762 * __this, ErrorInfo_t2633981210  ___item0, const MethodInfo* method);
#define List_1_Add_m1939351989(__this, ___item0, method) ((  void (*) (List_1_t4002166762 *, ErrorInfo_t2633981210 , const MethodInfo*))List_1_Add_m1939351989_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1380399596_gshared (List_1_t4002166762 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1380399596(__this, ___newCount0, method) ((  void (*) (List_1_t4002166762 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1380399596_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m3243884155_gshared (List_1_t4002166762 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m3243884155(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t4002166762 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m3243884155_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2935283946_gshared (List_1_t4002166762 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2935283946(__this, ___collection0, method) ((  void (*) (List_1_t4002166762 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2935283946_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2196256170_gshared (List_1_t4002166762 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2196256170(__this, ___enumerable0, method) ((  void (*) (List_1_t4002166762 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2196256170_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m667178669_gshared (List_1_t4002166762 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m667178669(__this, ___collection0, method) ((  void (*) (List_1_t4002166762 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m667178669_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t4191058746 * List_1_AsReadOnly_m1558520888_gshared (List_1_t4002166762 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1558520888(__this, method) ((  ReadOnlyCollection_1_t4191058746 * (*) (List_1_t4002166762 *, const MethodInfo*))List_1_AsReadOnly_m1558520888_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m3094293403_gshared (List_1_t4002166762 * __this, ErrorInfo_t2633981210  ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m3094293403(__this, ___item0, method) ((  int32_t (*) (List_1_t4002166762 *, ErrorInfo_t2633981210 , const MethodInfo*))List_1_BinarySearch_m3094293403_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::Clear()
extern "C"  void List_1_Clear_m3820840121_gshared (List_1_t4002166762 * __this, const MethodInfo* method);
#define List_1_Clear_m3820840121(__this, method) ((  void (*) (List_1_t4002166762 *, const MethodInfo*))List_1_Clear_m3820840121_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::Contains(T)
extern "C"  bool List_1_Contains_m3743518635_gshared (List_1_t4002166762 * __this, ErrorInfo_t2633981210  ___item0, const MethodInfo* method);
#define List_1_Contains_m3743518635(__this, ___item0, method) ((  bool (*) (List_1_t4002166762 *, ErrorInfo_t2633981210 , const MethodInfo*))List_1_Contains_m3743518635_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3556453345_gshared (List_1_t4002166762 * __this, ErrorInfoU5BU5D_t2864912703* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m3556453345(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4002166762 *, ErrorInfoU5BU5D_t2864912703*, int32_t, const MethodInfo*))List_1_CopyTo_m3556453345_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::Find(System.Predicate`1<T>)
extern "C"  ErrorInfo_t2633981210  List_1_Find_m268504005_gshared (List_1_t4002166762 * __this, Predicate_1_t2245038093 * ___match0, const MethodInfo* method);
#define List_1_Find_m268504005(__this, ___match0, method) ((  ErrorInfo_t2633981210  (*) (List_1_t4002166762 *, Predicate_1_t2245038093 *, const MethodInfo*))List_1_Find_m268504005_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m4013146146_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2245038093 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m4013146146(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2245038093 *, const MethodInfo*))List_1_CheckMatch_m4013146146_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3261504255_gshared (List_1_t4002166762 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2245038093 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3261504255(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t4002166762 *, int32_t, int32_t, Predicate_1_t2245038093 *, const MethodInfo*))List_1_GetIndex_m3261504255_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::GetEnumerator()
extern "C"  Enumerator_t4021839532  List_1_GetEnumerator_m2420376424_gshared (List_1_t4002166762 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m2420376424(__this, method) ((  Enumerator_t4021839532  (*) (List_1_t4002166762 *, const MethodInfo*))List_1_GetEnumerator_m2420376424_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1463483501_gshared (List_1_t4002166762 * __this, ErrorInfo_t2633981210  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m1463483501(__this, ___item0, method) ((  int32_t (*) (List_1_t4002166762 *, ErrorInfo_t2633981210 , const MethodInfo*))List_1_IndexOf_m1463483501_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3827142968_gshared (List_1_t4002166762 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3827142968(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t4002166762 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3827142968_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3302820529_gshared (List_1_t4002166762 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3302820529(__this, ___index0, method) ((  void (*) (List_1_t4002166762 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3302820529_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3364144792_gshared (List_1_t4002166762 * __this, int32_t ___index0, ErrorInfo_t2633981210  ___item1, const MethodInfo* method);
#define List_1_Insert_m3364144792(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4002166762 *, int32_t, ErrorInfo_t2633981210 , const MethodInfo*))List_1_Insert_m3364144792_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3835994381_gshared (List_1_t4002166762 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3835994381(__this, ___collection0, method) ((  void (*) (List_1_t4002166762 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3835994381_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::Remove(T)
extern "C"  bool List_1_Remove_m3694935654_gshared (List_1_t4002166762 * __this, ErrorInfo_t2633981210  ___item0, const MethodInfo* method);
#define List_1_Remove_m3694935654(__this, ___item0, method) ((  bool (*) (List_1_t4002166762 *, ErrorInfo_t2633981210 , const MethodInfo*))List_1_Remove_m3694935654_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1993278576_gshared (List_1_t4002166762 * __this, Predicate_1_t2245038093 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1993278576(__this, ___match0, method) ((  int32_t (*) (List_1_t4002166762 *, Predicate_1_t2245038093 *, const MethodInfo*))List_1_RemoveAll_m1993278576_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m1432605511_gshared (List_1_t4002166762 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m1432605511(__this, ___index0, method) ((  void (*) (List_1_t4002166762 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1432605511_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m1511372929_gshared (List_1_t4002166762 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m1511372929(__this, ___index0, ___count1, method) ((  void (*) (List_1_t4002166762 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1511372929_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::Reverse()
extern "C"  void List_1_Reverse_m1293005134_gshared (List_1_t4002166762 * __this, const MethodInfo* method);
#define List_1_Reverse_m1293005134(__this, method) ((  void (*) (List_1_t4002166762 *, const MethodInfo*))List_1_Reverse_m1293005134_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::Sort()
extern "C"  void List_1_Sort_m2939797780_gshared (List_1_t4002166762 * __this, const MethodInfo* method);
#define List_1_Sort_m2939797780(__this, method) ((  void (*) (List_1_t4002166762 *, const MethodInfo*))List_1_Sort_m2939797780_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1971374928_gshared (List_1_t4002166762 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m1971374928(__this, ___comparer0, method) ((  void (*) (List_1_t4002166762 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1971374928_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1751713383_gshared (List_1_t4002166762 * __this, Comparison_1_t1350342397 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1751713383(__this, ___comparison0, method) ((  void (*) (List_1_t4002166762 *, Comparison_1_t1350342397 *, const MethodInfo*))List_1_Sort_m1751713383_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::ToArray()
extern "C"  ErrorInfoU5BU5D_t2864912703* List_1_ToArray_m3379467687_gshared (List_1_t4002166762 * __this, const MethodInfo* method);
#define List_1_ToArray_m3379467687(__this, method) ((  ErrorInfoU5BU5D_t2864912703* (*) (List_1_t4002166762 *, const MethodInfo*))List_1_ToArray_m3379467687_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1028702637_gshared (List_1_t4002166762 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1028702637(__this, method) ((  void (*) (List_1_t4002166762 *, const MethodInfo*))List_1_TrimExcess_m1028702637_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m4017944093_gshared (List_1_t4002166762 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m4017944093(__this, method) ((  int32_t (*) (List_1_t4002166762 *, const MethodInfo*))List_1_get_Capacity_m4017944093_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m2885555710_gshared (List_1_t4002166762 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m2885555710(__this, ___value0, method) ((  void (*) (List_1_t4002166762 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2885555710_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Count()
extern "C"  int32_t List_1_get_Count_m3600957655_gshared (List_1_t4002166762 * __this, const MethodInfo* method);
#define List_1_get_Count_m3600957655(__this, method) ((  int32_t (*) (List_1_t4002166762 *, const MethodInfo*))List_1_get_Count_m3600957655_gshared)(__this, method)
// T System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Item(System.Int32)
extern "C"  ErrorInfo_t2633981210  List_1_get_Item_m3112894878_gshared (List_1_t4002166762 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3112894878(__this, ___index0, method) ((  ErrorInfo_t2633981210  (*) (List_1_t4002166762 *, int32_t, const MethodInfo*))List_1_get_Item_m3112894878_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m642367855_gshared (List_1_t4002166762 * __this, int32_t ___index0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
#define List_1_set_Item_m642367855(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4002166762 *, int32_t, ErrorInfo_t2633981210 , const MethodInfo*))List_1_set_Item_m642367855_gshared)(__this, ___index0, ___value1, method)
