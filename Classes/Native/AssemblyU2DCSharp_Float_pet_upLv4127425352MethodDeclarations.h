﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_pet_upLv
struct Float_pet_upLv_t4127425352;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// FloatTextUnit
struct FloatTextUnit_t2362298029;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"

// System.Void Float_pet_upLv::.ctor()
extern "C"  void Float_pet_upLv__ctor_m3439163987 (Float_pet_upLv_t4127425352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_pet_upLv::FloatID()
extern "C"  int32_t Float_pet_upLv_FloatID_m1659650719 (Float_pet_upLv_t4127425352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_pet_upLv::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_pet_upLv_OnAwake_m1550472463 (Float_pet_upLv_t4127425352 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_pet_upLv::OnDestroy()
extern "C"  void Float_pet_upLv_OnDestroy_m1377037772 (Float_pet_upLv_t4127425352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_pet_upLv::Init()
extern "C"  void Float_pet_upLv_Init_m2556329377 (Float_pet_upLv_t4127425352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_pet_upLv::SetFile(System.Object[])
extern "C"  void Float_pet_upLv_SetFile_m711898851 (Float_pet_upLv_t4127425352 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_pet_upLv::ilo_OnDestroy1(FloatTextUnit)
extern "C"  void Float_pet_upLv_ilo_OnDestroy1_m1113156523 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
