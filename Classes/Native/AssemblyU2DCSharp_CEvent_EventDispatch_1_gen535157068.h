﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<CEvent.ZEvent>>>
struct Dictionary_2_t3303518232;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CEvent.EventDispatch`1<CEvent.ZEvent>
struct  EventDispatch_1_t535157068  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<CEvent.EventFunc`1<T>>> CEvent.EventDispatch`1::dicEvents
	Dictionary_2_t3303518232 * ___dicEvents_0;

public:
	inline static int32_t get_offset_of_dicEvents_0() { return static_cast<int32_t>(offsetof(EventDispatch_1_t535157068, ___dicEvents_0)); }
	inline Dictionary_2_t3303518232 * get_dicEvents_0() const { return ___dicEvents_0; }
	inline Dictionary_2_t3303518232 ** get_address_of_dicEvents_0() { return &___dicEvents_0; }
	inline void set_dicEvents_0(Dictionary_2_t3303518232 * value)
	{
		___dicEvents_0 = value;
		Il2CppCodeGenWriteBarrier(&___dicEvents_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
