﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.GraphNode/<UpdateRecursiveG>c__AnonStorey109
struct U3CUpdateRecursiveGU3Ec__AnonStorey109_t2868289120;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.GraphNode/<UpdateRecursiveG>c__AnonStorey109::.ctor()
extern "C"  void U3CUpdateRecursiveGU3Ec__AnonStorey109__ctor_m2030615611 (U3CUpdateRecursiveGU3Ec__AnonStorey109_t2868289120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode/<UpdateRecursiveG>c__AnonStorey109::<>m__33B(Pathfinding.GraphNode)
extern "C"  void U3CUpdateRecursiveGU3Ec__AnonStorey109_U3CU3Em__33B_m4089789292 (U3CUpdateRecursiveGU3Ec__AnonStorey109_t2868289120 * __this, GraphNode_t23612370 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
