﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23374811140MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,ProductsCfgMgr/ProductInfo>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m4147075597(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2025190314 *, String_t*, ProductInfo_t1305991238 , const MethodInfo*))KeyValuePair_2__ctor_m3143780667_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,ProductsCfgMgr/ProductInfo>::get_Key()
#define KeyValuePair_2_get_Key_m2462179131(__this, method) ((  String_t* (*) (KeyValuePair_2_t2025190314 *, const MethodInfo*))KeyValuePair_2_get_Key_m2376238157_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,ProductsCfgMgr/ProductInfo>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m623455228(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2025190314 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m3324835854_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,ProductsCfgMgr/ProductInfo>::get_Value()
#define KeyValuePair_2_get_Value_m3594427963(__this, method) ((  ProductInfo_t1305991238  (*) (KeyValuePair_2_t2025190314 *, const MethodInfo*))KeyValuePair_2_get_Value_m2609530573_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,ProductsCfgMgr/ProductInfo>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3369476604(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2025190314 *, ProductInfo_t1305991238 , const MethodInfo*))KeyValuePair_2_set_Value_m1813091598_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,ProductsCfgMgr/ProductInfo>::ToString()
#define KeyValuePair_2_ToString_m2449788710(__this, method) ((  String_t* (*) (KeyValuePair_2_t2025190314 *, const MethodInfo*))KeyValuePair_2_ToString_m4080585812_gshared)(__this, method)
