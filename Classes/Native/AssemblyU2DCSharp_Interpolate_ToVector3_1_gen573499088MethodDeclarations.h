﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Interpolate/ToVector3`1<System.Object>
struct ToVector3_1_t573499088;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void Interpolate/ToVector3`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ToVector3_1__ctor_m4161488491_gshared (ToVector3_1_t573499088 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define ToVector3_1__ctor_m4161488491(__this, ___object0, ___method1, method) ((  void (*) (ToVector3_1_t573499088 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ToVector3_1__ctor_m4161488491_gshared)(__this, ___object0, ___method1, method)
// UnityEngine.Vector3 Interpolate/ToVector3`1<System.Object>::Invoke(T)
extern "C"  Vector3_t4282066566  ToVector3_1_Invoke_m1249282367_gshared (ToVector3_1_t573499088 * __this, Il2CppObject * ___v0, const MethodInfo* method);
#define ToVector3_1_Invoke_m1249282367(__this, ___v0, method) ((  Vector3_t4282066566  (*) (ToVector3_1_t573499088 *, Il2CppObject *, const MethodInfo*))ToVector3_1_Invoke_m1249282367_gshared)(__this, ___v0, method)
// System.IAsyncResult Interpolate/ToVector3`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ToVector3_1_BeginInvoke_m774210118_gshared (ToVector3_1_t573499088 * __this, Il2CppObject * ___v0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define ToVector3_1_BeginInvoke_m774210118(__this, ___v0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (ToVector3_1_t573499088 *, Il2CppObject *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))ToVector3_1_BeginInvoke_m774210118_gshared)(__this, ___v0, ___callback1, ___object2, method)
// UnityEngine.Vector3 Interpolate/ToVector3`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Vector3_t4282066566  ToVector3_1_EndInvoke_m3857074165_gshared (ToVector3_1_t573499088 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define ToVector3_1_EndInvoke_m3857074165(__this, ___result0, method) ((  Vector3_t4282066566  (*) (ToVector3_1_t573499088 *, Il2CppObject *, const MethodInfo*))ToVector3_1_EndInvoke_m3857074165_gshared)(__this, ___result0, method)
