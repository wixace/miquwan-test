﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._6d4ca4cddafc0c868209551665d0df3b
struct _6d4ca4cddafc0c868209551665d0df3b_t2364966169;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._6d4ca4cddafc0c868209551665d0df3b::.ctor()
extern "C"  void _6d4ca4cddafc0c868209551665d0df3b__ctor_m1719553332 (_6d4ca4cddafc0c868209551665d0df3b_t2364966169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6d4ca4cddafc0c868209551665d0df3b::_6d4ca4cddafc0c868209551665d0df3bm2(System.Int32)
extern "C"  int32_t _6d4ca4cddafc0c868209551665d0df3b__6d4ca4cddafc0c868209551665d0df3bm2_m4161188601 (_6d4ca4cddafc0c868209551665d0df3b_t2364966169 * __this, int32_t ____6d4ca4cddafc0c868209551665d0df3ba0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6d4ca4cddafc0c868209551665d0df3b::_6d4ca4cddafc0c868209551665d0df3bm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _6d4ca4cddafc0c868209551665d0df3b__6d4ca4cddafc0c868209551665d0df3bm_m652492765 (_6d4ca4cddafc0c868209551665d0df3b_t2364966169 * __this, int32_t ____6d4ca4cddafc0c868209551665d0df3ba0, int32_t ____6d4ca4cddafc0c868209551665d0df3b661, int32_t ____6d4ca4cddafc0c868209551665d0df3bc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
