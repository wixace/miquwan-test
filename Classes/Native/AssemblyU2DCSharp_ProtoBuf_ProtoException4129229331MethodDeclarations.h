﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.ProtoException
struct ProtoException_t4129229331;
// System.String
struct String_t;
// System.Exception
struct Exception_t3991598821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Exception3991598821.h"

// System.Void ProtoBuf.ProtoException::.ctor()
extern "C"  void ProtoException__ctor_m1371283073 (ProtoException_t4129229331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoException::.ctor(System.String)
extern "C"  void ProtoException__ctor_m76077601 (ProtoException_t4129229331 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoException::.ctor(System.String,System.Exception)
extern "C"  void ProtoException__ctor_m1082675509 (ProtoException_t4129229331 * __this, String_t* ___message0, Exception_t3991598821 * ___innerException1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
