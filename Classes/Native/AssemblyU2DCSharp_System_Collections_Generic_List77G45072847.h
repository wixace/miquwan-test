﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConstructorID
struct ConstructorID_t3348888181;
// PropertyID
struct PropertyID_t1067426256;
// MethodID
struct MethodID_t3916401116;
// JSDataExchangeMgr/DGetV`1<System.Object[]>
struct DGetV_1_t986307823;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System_Collections_Generic_List77Generated
struct  System_Collections_Generic_List77Generated_t45072847  : public Il2CppObject
{
public:

public:
};

struct System_Collections_Generic_List77Generated_t45072847_StaticFields
{
public:
	// ConstructorID System_Collections_Generic_List77Generated::constructorID0
	ConstructorID_t3348888181 * ___constructorID0_0;
	// ConstructorID System_Collections_Generic_List77Generated::constructorID1
	ConstructorID_t3348888181 * ___constructorID1_1;
	// ConstructorID System_Collections_Generic_List77Generated::constructorID2
	ConstructorID_t3348888181 * ___constructorID2_2;
	// PropertyID System_Collections_Generic_List77Generated::propertyID0
	PropertyID_t1067426256 * ___propertyID0_3;
	// PropertyID System_Collections_Generic_List77Generated::propertyID1
	PropertyID_t1067426256 * ___propertyID1_4;
	// PropertyID System_Collections_Generic_List77Generated::propertyID2
	PropertyID_t1067426256 * ___propertyID2_5;
	// MethodID System_Collections_Generic_List77Generated::methodID0
	MethodID_t3916401116 * ___methodID0_6;
	// MethodID System_Collections_Generic_List77Generated::methodID1
	MethodID_t3916401116 * ___methodID1_7;
	// MethodID System_Collections_Generic_List77Generated::methodID2
	MethodID_t3916401116 * ___methodID2_8;
	// MethodID System_Collections_Generic_List77Generated::methodID3
	MethodID_t3916401116 * ___methodID3_9;
	// MethodID System_Collections_Generic_List77Generated::methodID4
	MethodID_t3916401116 * ___methodID4_10;
	// MethodID System_Collections_Generic_List77Generated::methodID5
	MethodID_t3916401116 * ___methodID5_11;
	// MethodID System_Collections_Generic_List77Generated::methodID6
	MethodID_t3916401116 * ___methodID6_12;
	// MethodID System_Collections_Generic_List77Generated::methodID7
	MethodID_t3916401116 * ___methodID7_13;
	// MethodID System_Collections_Generic_List77Generated::methodID8
	MethodID_t3916401116 * ___methodID8_14;
	// MethodID System_Collections_Generic_List77Generated::methodID9
	MethodID_t3916401116 * ___methodID9_15;
	// MethodID System_Collections_Generic_List77Generated::methodID10
	MethodID_t3916401116 * ___methodID10_16;
	// MethodID System_Collections_Generic_List77Generated::methodID11
	MethodID_t3916401116 * ___methodID11_17;
	// MethodID System_Collections_Generic_List77Generated::methodID12
	MethodID_t3916401116 * ___methodID12_18;
	// MethodID System_Collections_Generic_List77Generated::methodID13
	MethodID_t3916401116 * ___methodID13_19;
	// MethodID System_Collections_Generic_List77Generated::methodID14
	MethodID_t3916401116 * ___methodID14_20;
	// MethodID System_Collections_Generic_List77Generated::methodID15
	MethodID_t3916401116 * ___methodID15_21;
	// MethodID System_Collections_Generic_List77Generated::methodID16
	MethodID_t3916401116 * ___methodID16_22;
	// MethodID System_Collections_Generic_List77Generated::methodID17
	MethodID_t3916401116 * ___methodID17_23;
	// MethodID System_Collections_Generic_List77Generated::methodID18
	MethodID_t3916401116 * ___methodID18_24;
	// MethodID System_Collections_Generic_List77Generated::methodID19
	MethodID_t3916401116 * ___methodID19_25;
	// MethodID System_Collections_Generic_List77Generated::methodID20
	MethodID_t3916401116 * ___methodID20_26;
	// MethodID System_Collections_Generic_List77Generated::methodID21
	MethodID_t3916401116 * ___methodID21_27;
	// MethodID System_Collections_Generic_List77Generated::methodID22
	MethodID_t3916401116 * ___methodID22_28;
	// MethodID System_Collections_Generic_List77Generated::methodID23
	MethodID_t3916401116 * ___methodID23_29;
	// MethodID System_Collections_Generic_List77Generated::methodID24
	MethodID_t3916401116 * ___methodID24_30;
	// MethodID System_Collections_Generic_List77Generated::methodID25
	MethodID_t3916401116 * ___methodID25_31;
	// MethodID System_Collections_Generic_List77Generated::methodID26
	MethodID_t3916401116 * ___methodID26_32;
	// MethodID System_Collections_Generic_List77Generated::methodID27
	MethodID_t3916401116 * ___methodID27_33;
	// MethodID System_Collections_Generic_List77Generated::methodID28
	MethodID_t3916401116 * ___methodID28_34;
	// MethodID System_Collections_Generic_List77Generated::methodID29
	MethodID_t3916401116 * ___methodID29_35;
	// MethodID System_Collections_Generic_List77Generated::methodID30
	MethodID_t3916401116 * ___methodID30_36;
	// MethodID System_Collections_Generic_List77Generated::methodID31
	MethodID_t3916401116 * ___methodID31_37;
	// MethodID System_Collections_Generic_List77Generated::methodID32
	MethodID_t3916401116 * ___methodID32_38;
	// MethodID System_Collections_Generic_List77Generated::methodID33
	MethodID_t3916401116 * ___methodID33_39;
	// MethodID System_Collections_Generic_List77Generated::methodID34
	MethodID_t3916401116 * ___methodID34_40;
	// MethodID System_Collections_Generic_List77Generated::methodID35
	MethodID_t3916401116 * ___methodID35_41;
	// MethodID System_Collections_Generic_List77Generated::methodID36
	MethodID_t3916401116 * ___methodID36_42;
	// MethodID System_Collections_Generic_List77Generated::methodID37
	MethodID_t3916401116 * ___methodID37_43;
	// MethodID System_Collections_Generic_List77Generated::methodID38
	MethodID_t3916401116 * ___methodID38_44;
	// MethodID System_Collections_Generic_List77Generated::methodID39
	MethodID_t3916401116 * ___methodID39_45;
	// MethodID System_Collections_Generic_List77Generated::methodID40
	MethodID_t3916401116 * ___methodID40_46;
	// MethodID System_Collections_Generic_List77Generated::methodID41
	MethodID_t3916401116 * ___methodID41_47;
	// MethodID System_Collections_Generic_List77Generated::methodID42
	MethodID_t3916401116 * ___methodID42_48;
	// MethodID System_Collections_Generic_List77Generated::methodID43
	MethodID_t3916401116 * ___methodID43_49;
	// MethodID System_Collections_Generic_List77Generated::methodID44
	MethodID_t3916401116 * ___methodID44_50;
	// MethodID System_Collections_Generic_List77Generated::methodID45
	MethodID_t3916401116 * ___methodID45_51;
	// JSDataExchangeMgr/DGetV`1<System.Object[]> System_Collections_Generic_List77Generated::<>f__am$cache34
	DGetV_1_t986307823 * ___U3CU3Ef__amU24cache34_52;
	// JSDataExchangeMgr/DGetV`1<System.Object[]> System_Collections_Generic_List77Generated::<>f__am$cache35
	DGetV_1_t986307823 * ___U3CU3Ef__amU24cache35_53;
	// JSDataExchangeMgr/DGetV`1<System.Object[]> System_Collections_Generic_List77Generated::<>f__am$cache36
	DGetV_1_t986307823 * ___U3CU3Ef__amU24cache36_54;

public:
	inline static int32_t get_offset_of_constructorID0_0() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___constructorID0_0)); }
	inline ConstructorID_t3348888181 * get_constructorID0_0() const { return ___constructorID0_0; }
	inline ConstructorID_t3348888181 ** get_address_of_constructorID0_0() { return &___constructorID0_0; }
	inline void set_constructorID0_0(ConstructorID_t3348888181 * value)
	{
		___constructorID0_0 = value;
		Il2CppCodeGenWriteBarrier(&___constructorID0_0, value);
	}

	inline static int32_t get_offset_of_constructorID1_1() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___constructorID1_1)); }
	inline ConstructorID_t3348888181 * get_constructorID1_1() const { return ___constructorID1_1; }
	inline ConstructorID_t3348888181 ** get_address_of_constructorID1_1() { return &___constructorID1_1; }
	inline void set_constructorID1_1(ConstructorID_t3348888181 * value)
	{
		___constructorID1_1 = value;
		Il2CppCodeGenWriteBarrier(&___constructorID1_1, value);
	}

	inline static int32_t get_offset_of_constructorID2_2() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___constructorID2_2)); }
	inline ConstructorID_t3348888181 * get_constructorID2_2() const { return ___constructorID2_2; }
	inline ConstructorID_t3348888181 ** get_address_of_constructorID2_2() { return &___constructorID2_2; }
	inline void set_constructorID2_2(ConstructorID_t3348888181 * value)
	{
		___constructorID2_2 = value;
		Il2CppCodeGenWriteBarrier(&___constructorID2_2, value);
	}

	inline static int32_t get_offset_of_propertyID0_3() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___propertyID0_3)); }
	inline PropertyID_t1067426256 * get_propertyID0_3() const { return ___propertyID0_3; }
	inline PropertyID_t1067426256 ** get_address_of_propertyID0_3() { return &___propertyID0_3; }
	inline void set_propertyID0_3(PropertyID_t1067426256 * value)
	{
		___propertyID0_3 = value;
		Il2CppCodeGenWriteBarrier(&___propertyID0_3, value);
	}

	inline static int32_t get_offset_of_propertyID1_4() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___propertyID1_4)); }
	inline PropertyID_t1067426256 * get_propertyID1_4() const { return ___propertyID1_4; }
	inline PropertyID_t1067426256 ** get_address_of_propertyID1_4() { return &___propertyID1_4; }
	inline void set_propertyID1_4(PropertyID_t1067426256 * value)
	{
		___propertyID1_4 = value;
		Il2CppCodeGenWriteBarrier(&___propertyID1_4, value);
	}

	inline static int32_t get_offset_of_propertyID2_5() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___propertyID2_5)); }
	inline PropertyID_t1067426256 * get_propertyID2_5() const { return ___propertyID2_5; }
	inline PropertyID_t1067426256 ** get_address_of_propertyID2_5() { return &___propertyID2_5; }
	inline void set_propertyID2_5(PropertyID_t1067426256 * value)
	{
		___propertyID2_5 = value;
		Il2CppCodeGenWriteBarrier(&___propertyID2_5, value);
	}

	inline static int32_t get_offset_of_methodID0_6() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID0_6)); }
	inline MethodID_t3916401116 * get_methodID0_6() const { return ___methodID0_6; }
	inline MethodID_t3916401116 ** get_address_of_methodID0_6() { return &___methodID0_6; }
	inline void set_methodID0_6(MethodID_t3916401116 * value)
	{
		___methodID0_6 = value;
		Il2CppCodeGenWriteBarrier(&___methodID0_6, value);
	}

	inline static int32_t get_offset_of_methodID1_7() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID1_7)); }
	inline MethodID_t3916401116 * get_methodID1_7() const { return ___methodID1_7; }
	inline MethodID_t3916401116 ** get_address_of_methodID1_7() { return &___methodID1_7; }
	inline void set_methodID1_7(MethodID_t3916401116 * value)
	{
		___methodID1_7 = value;
		Il2CppCodeGenWriteBarrier(&___methodID1_7, value);
	}

	inline static int32_t get_offset_of_methodID2_8() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID2_8)); }
	inline MethodID_t3916401116 * get_methodID2_8() const { return ___methodID2_8; }
	inline MethodID_t3916401116 ** get_address_of_methodID2_8() { return &___methodID2_8; }
	inline void set_methodID2_8(MethodID_t3916401116 * value)
	{
		___methodID2_8 = value;
		Il2CppCodeGenWriteBarrier(&___methodID2_8, value);
	}

	inline static int32_t get_offset_of_methodID3_9() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID3_9)); }
	inline MethodID_t3916401116 * get_methodID3_9() const { return ___methodID3_9; }
	inline MethodID_t3916401116 ** get_address_of_methodID3_9() { return &___methodID3_9; }
	inline void set_methodID3_9(MethodID_t3916401116 * value)
	{
		___methodID3_9 = value;
		Il2CppCodeGenWriteBarrier(&___methodID3_9, value);
	}

	inline static int32_t get_offset_of_methodID4_10() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID4_10)); }
	inline MethodID_t3916401116 * get_methodID4_10() const { return ___methodID4_10; }
	inline MethodID_t3916401116 ** get_address_of_methodID4_10() { return &___methodID4_10; }
	inline void set_methodID4_10(MethodID_t3916401116 * value)
	{
		___methodID4_10 = value;
		Il2CppCodeGenWriteBarrier(&___methodID4_10, value);
	}

	inline static int32_t get_offset_of_methodID5_11() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID5_11)); }
	inline MethodID_t3916401116 * get_methodID5_11() const { return ___methodID5_11; }
	inline MethodID_t3916401116 ** get_address_of_methodID5_11() { return &___methodID5_11; }
	inline void set_methodID5_11(MethodID_t3916401116 * value)
	{
		___methodID5_11 = value;
		Il2CppCodeGenWriteBarrier(&___methodID5_11, value);
	}

	inline static int32_t get_offset_of_methodID6_12() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID6_12)); }
	inline MethodID_t3916401116 * get_methodID6_12() const { return ___methodID6_12; }
	inline MethodID_t3916401116 ** get_address_of_methodID6_12() { return &___methodID6_12; }
	inline void set_methodID6_12(MethodID_t3916401116 * value)
	{
		___methodID6_12 = value;
		Il2CppCodeGenWriteBarrier(&___methodID6_12, value);
	}

	inline static int32_t get_offset_of_methodID7_13() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID7_13)); }
	inline MethodID_t3916401116 * get_methodID7_13() const { return ___methodID7_13; }
	inline MethodID_t3916401116 ** get_address_of_methodID7_13() { return &___methodID7_13; }
	inline void set_methodID7_13(MethodID_t3916401116 * value)
	{
		___methodID7_13 = value;
		Il2CppCodeGenWriteBarrier(&___methodID7_13, value);
	}

	inline static int32_t get_offset_of_methodID8_14() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID8_14)); }
	inline MethodID_t3916401116 * get_methodID8_14() const { return ___methodID8_14; }
	inline MethodID_t3916401116 ** get_address_of_methodID8_14() { return &___methodID8_14; }
	inline void set_methodID8_14(MethodID_t3916401116 * value)
	{
		___methodID8_14 = value;
		Il2CppCodeGenWriteBarrier(&___methodID8_14, value);
	}

	inline static int32_t get_offset_of_methodID9_15() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID9_15)); }
	inline MethodID_t3916401116 * get_methodID9_15() const { return ___methodID9_15; }
	inline MethodID_t3916401116 ** get_address_of_methodID9_15() { return &___methodID9_15; }
	inline void set_methodID9_15(MethodID_t3916401116 * value)
	{
		___methodID9_15 = value;
		Il2CppCodeGenWriteBarrier(&___methodID9_15, value);
	}

	inline static int32_t get_offset_of_methodID10_16() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID10_16)); }
	inline MethodID_t3916401116 * get_methodID10_16() const { return ___methodID10_16; }
	inline MethodID_t3916401116 ** get_address_of_methodID10_16() { return &___methodID10_16; }
	inline void set_methodID10_16(MethodID_t3916401116 * value)
	{
		___methodID10_16 = value;
		Il2CppCodeGenWriteBarrier(&___methodID10_16, value);
	}

	inline static int32_t get_offset_of_methodID11_17() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID11_17)); }
	inline MethodID_t3916401116 * get_methodID11_17() const { return ___methodID11_17; }
	inline MethodID_t3916401116 ** get_address_of_methodID11_17() { return &___methodID11_17; }
	inline void set_methodID11_17(MethodID_t3916401116 * value)
	{
		___methodID11_17 = value;
		Il2CppCodeGenWriteBarrier(&___methodID11_17, value);
	}

	inline static int32_t get_offset_of_methodID12_18() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID12_18)); }
	inline MethodID_t3916401116 * get_methodID12_18() const { return ___methodID12_18; }
	inline MethodID_t3916401116 ** get_address_of_methodID12_18() { return &___methodID12_18; }
	inline void set_methodID12_18(MethodID_t3916401116 * value)
	{
		___methodID12_18 = value;
		Il2CppCodeGenWriteBarrier(&___methodID12_18, value);
	}

	inline static int32_t get_offset_of_methodID13_19() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID13_19)); }
	inline MethodID_t3916401116 * get_methodID13_19() const { return ___methodID13_19; }
	inline MethodID_t3916401116 ** get_address_of_methodID13_19() { return &___methodID13_19; }
	inline void set_methodID13_19(MethodID_t3916401116 * value)
	{
		___methodID13_19 = value;
		Il2CppCodeGenWriteBarrier(&___methodID13_19, value);
	}

	inline static int32_t get_offset_of_methodID14_20() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID14_20)); }
	inline MethodID_t3916401116 * get_methodID14_20() const { return ___methodID14_20; }
	inline MethodID_t3916401116 ** get_address_of_methodID14_20() { return &___methodID14_20; }
	inline void set_methodID14_20(MethodID_t3916401116 * value)
	{
		___methodID14_20 = value;
		Il2CppCodeGenWriteBarrier(&___methodID14_20, value);
	}

	inline static int32_t get_offset_of_methodID15_21() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID15_21)); }
	inline MethodID_t3916401116 * get_methodID15_21() const { return ___methodID15_21; }
	inline MethodID_t3916401116 ** get_address_of_methodID15_21() { return &___methodID15_21; }
	inline void set_methodID15_21(MethodID_t3916401116 * value)
	{
		___methodID15_21 = value;
		Il2CppCodeGenWriteBarrier(&___methodID15_21, value);
	}

	inline static int32_t get_offset_of_methodID16_22() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID16_22)); }
	inline MethodID_t3916401116 * get_methodID16_22() const { return ___methodID16_22; }
	inline MethodID_t3916401116 ** get_address_of_methodID16_22() { return &___methodID16_22; }
	inline void set_methodID16_22(MethodID_t3916401116 * value)
	{
		___methodID16_22 = value;
		Il2CppCodeGenWriteBarrier(&___methodID16_22, value);
	}

	inline static int32_t get_offset_of_methodID17_23() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID17_23)); }
	inline MethodID_t3916401116 * get_methodID17_23() const { return ___methodID17_23; }
	inline MethodID_t3916401116 ** get_address_of_methodID17_23() { return &___methodID17_23; }
	inline void set_methodID17_23(MethodID_t3916401116 * value)
	{
		___methodID17_23 = value;
		Il2CppCodeGenWriteBarrier(&___methodID17_23, value);
	}

	inline static int32_t get_offset_of_methodID18_24() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID18_24)); }
	inline MethodID_t3916401116 * get_methodID18_24() const { return ___methodID18_24; }
	inline MethodID_t3916401116 ** get_address_of_methodID18_24() { return &___methodID18_24; }
	inline void set_methodID18_24(MethodID_t3916401116 * value)
	{
		___methodID18_24 = value;
		Il2CppCodeGenWriteBarrier(&___methodID18_24, value);
	}

	inline static int32_t get_offset_of_methodID19_25() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID19_25)); }
	inline MethodID_t3916401116 * get_methodID19_25() const { return ___methodID19_25; }
	inline MethodID_t3916401116 ** get_address_of_methodID19_25() { return &___methodID19_25; }
	inline void set_methodID19_25(MethodID_t3916401116 * value)
	{
		___methodID19_25 = value;
		Il2CppCodeGenWriteBarrier(&___methodID19_25, value);
	}

	inline static int32_t get_offset_of_methodID20_26() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID20_26)); }
	inline MethodID_t3916401116 * get_methodID20_26() const { return ___methodID20_26; }
	inline MethodID_t3916401116 ** get_address_of_methodID20_26() { return &___methodID20_26; }
	inline void set_methodID20_26(MethodID_t3916401116 * value)
	{
		___methodID20_26 = value;
		Il2CppCodeGenWriteBarrier(&___methodID20_26, value);
	}

	inline static int32_t get_offset_of_methodID21_27() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID21_27)); }
	inline MethodID_t3916401116 * get_methodID21_27() const { return ___methodID21_27; }
	inline MethodID_t3916401116 ** get_address_of_methodID21_27() { return &___methodID21_27; }
	inline void set_methodID21_27(MethodID_t3916401116 * value)
	{
		___methodID21_27 = value;
		Il2CppCodeGenWriteBarrier(&___methodID21_27, value);
	}

	inline static int32_t get_offset_of_methodID22_28() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID22_28)); }
	inline MethodID_t3916401116 * get_methodID22_28() const { return ___methodID22_28; }
	inline MethodID_t3916401116 ** get_address_of_methodID22_28() { return &___methodID22_28; }
	inline void set_methodID22_28(MethodID_t3916401116 * value)
	{
		___methodID22_28 = value;
		Il2CppCodeGenWriteBarrier(&___methodID22_28, value);
	}

	inline static int32_t get_offset_of_methodID23_29() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID23_29)); }
	inline MethodID_t3916401116 * get_methodID23_29() const { return ___methodID23_29; }
	inline MethodID_t3916401116 ** get_address_of_methodID23_29() { return &___methodID23_29; }
	inline void set_methodID23_29(MethodID_t3916401116 * value)
	{
		___methodID23_29 = value;
		Il2CppCodeGenWriteBarrier(&___methodID23_29, value);
	}

	inline static int32_t get_offset_of_methodID24_30() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID24_30)); }
	inline MethodID_t3916401116 * get_methodID24_30() const { return ___methodID24_30; }
	inline MethodID_t3916401116 ** get_address_of_methodID24_30() { return &___methodID24_30; }
	inline void set_methodID24_30(MethodID_t3916401116 * value)
	{
		___methodID24_30 = value;
		Il2CppCodeGenWriteBarrier(&___methodID24_30, value);
	}

	inline static int32_t get_offset_of_methodID25_31() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID25_31)); }
	inline MethodID_t3916401116 * get_methodID25_31() const { return ___methodID25_31; }
	inline MethodID_t3916401116 ** get_address_of_methodID25_31() { return &___methodID25_31; }
	inline void set_methodID25_31(MethodID_t3916401116 * value)
	{
		___methodID25_31 = value;
		Il2CppCodeGenWriteBarrier(&___methodID25_31, value);
	}

	inline static int32_t get_offset_of_methodID26_32() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID26_32)); }
	inline MethodID_t3916401116 * get_methodID26_32() const { return ___methodID26_32; }
	inline MethodID_t3916401116 ** get_address_of_methodID26_32() { return &___methodID26_32; }
	inline void set_methodID26_32(MethodID_t3916401116 * value)
	{
		___methodID26_32 = value;
		Il2CppCodeGenWriteBarrier(&___methodID26_32, value);
	}

	inline static int32_t get_offset_of_methodID27_33() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID27_33)); }
	inline MethodID_t3916401116 * get_methodID27_33() const { return ___methodID27_33; }
	inline MethodID_t3916401116 ** get_address_of_methodID27_33() { return &___methodID27_33; }
	inline void set_methodID27_33(MethodID_t3916401116 * value)
	{
		___methodID27_33 = value;
		Il2CppCodeGenWriteBarrier(&___methodID27_33, value);
	}

	inline static int32_t get_offset_of_methodID28_34() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID28_34)); }
	inline MethodID_t3916401116 * get_methodID28_34() const { return ___methodID28_34; }
	inline MethodID_t3916401116 ** get_address_of_methodID28_34() { return &___methodID28_34; }
	inline void set_methodID28_34(MethodID_t3916401116 * value)
	{
		___methodID28_34 = value;
		Il2CppCodeGenWriteBarrier(&___methodID28_34, value);
	}

	inline static int32_t get_offset_of_methodID29_35() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID29_35)); }
	inline MethodID_t3916401116 * get_methodID29_35() const { return ___methodID29_35; }
	inline MethodID_t3916401116 ** get_address_of_methodID29_35() { return &___methodID29_35; }
	inline void set_methodID29_35(MethodID_t3916401116 * value)
	{
		___methodID29_35 = value;
		Il2CppCodeGenWriteBarrier(&___methodID29_35, value);
	}

	inline static int32_t get_offset_of_methodID30_36() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID30_36)); }
	inline MethodID_t3916401116 * get_methodID30_36() const { return ___methodID30_36; }
	inline MethodID_t3916401116 ** get_address_of_methodID30_36() { return &___methodID30_36; }
	inline void set_methodID30_36(MethodID_t3916401116 * value)
	{
		___methodID30_36 = value;
		Il2CppCodeGenWriteBarrier(&___methodID30_36, value);
	}

	inline static int32_t get_offset_of_methodID31_37() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID31_37)); }
	inline MethodID_t3916401116 * get_methodID31_37() const { return ___methodID31_37; }
	inline MethodID_t3916401116 ** get_address_of_methodID31_37() { return &___methodID31_37; }
	inline void set_methodID31_37(MethodID_t3916401116 * value)
	{
		___methodID31_37 = value;
		Il2CppCodeGenWriteBarrier(&___methodID31_37, value);
	}

	inline static int32_t get_offset_of_methodID32_38() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID32_38)); }
	inline MethodID_t3916401116 * get_methodID32_38() const { return ___methodID32_38; }
	inline MethodID_t3916401116 ** get_address_of_methodID32_38() { return &___methodID32_38; }
	inline void set_methodID32_38(MethodID_t3916401116 * value)
	{
		___methodID32_38 = value;
		Il2CppCodeGenWriteBarrier(&___methodID32_38, value);
	}

	inline static int32_t get_offset_of_methodID33_39() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID33_39)); }
	inline MethodID_t3916401116 * get_methodID33_39() const { return ___methodID33_39; }
	inline MethodID_t3916401116 ** get_address_of_methodID33_39() { return &___methodID33_39; }
	inline void set_methodID33_39(MethodID_t3916401116 * value)
	{
		___methodID33_39 = value;
		Il2CppCodeGenWriteBarrier(&___methodID33_39, value);
	}

	inline static int32_t get_offset_of_methodID34_40() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID34_40)); }
	inline MethodID_t3916401116 * get_methodID34_40() const { return ___methodID34_40; }
	inline MethodID_t3916401116 ** get_address_of_methodID34_40() { return &___methodID34_40; }
	inline void set_methodID34_40(MethodID_t3916401116 * value)
	{
		___methodID34_40 = value;
		Il2CppCodeGenWriteBarrier(&___methodID34_40, value);
	}

	inline static int32_t get_offset_of_methodID35_41() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID35_41)); }
	inline MethodID_t3916401116 * get_methodID35_41() const { return ___methodID35_41; }
	inline MethodID_t3916401116 ** get_address_of_methodID35_41() { return &___methodID35_41; }
	inline void set_methodID35_41(MethodID_t3916401116 * value)
	{
		___methodID35_41 = value;
		Il2CppCodeGenWriteBarrier(&___methodID35_41, value);
	}

	inline static int32_t get_offset_of_methodID36_42() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID36_42)); }
	inline MethodID_t3916401116 * get_methodID36_42() const { return ___methodID36_42; }
	inline MethodID_t3916401116 ** get_address_of_methodID36_42() { return &___methodID36_42; }
	inline void set_methodID36_42(MethodID_t3916401116 * value)
	{
		___methodID36_42 = value;
		Il2CppCodeGenWriteBarrier(&___methodID36_42, value);
	}

	inline static int32_t get_offset_of_methodID37_43() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID37_43)); }
	inline MethodID_t3916401116 * get_methodID37_43() const { return ___methodID37_43; }
	inline MethodID_t3916401116 ** get_address_of_methodID37_43() { return &___methodID37_43; }
	inline void set_methodID37_43(MethodID_t3916401116 * value)
	{
		___methodID37_43 = value;
		Il2CppCodeGenWriteBarrier(&___methodID37_43, value);
	}

	inline static int32_t get_offset_of_methodID38_44() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID38_44)); }
	inline MethodID_t3916401116 * get_methodID38_44() const { return ___methodID38_44; }
	inline MethodID_t3916401116 ** get_address_of_methodID38_44() { return &___methodID38_44; }
	inline void set_methodID38_44(MethodID_t3916401116 * value)
	{
		___methodID38_44 = value;
		Il2CppCodeGenWriteBarrier(&___methodID38_44, value);
	}

	inline static int32_t get_offset_of_methodID39_45() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID39_45)); }
	inline MethodID_t3916401116 * get_methodID39_45() const { return ___methodID39_45; }
	inline MethodID_t3916401116 ** get_address_of_methodID39_45() { return &___methodID39_45; }
	inline void set_methodID39_45(MethodID_t3916401116 * value)
	{
		___methodID39_45 = value;
		Il2CppCodeGenWriteBarrier(&___methodID39_45, value);
	}

	inline static int32_t get_offset_of_methodID40_46() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID40_46)); }
	inline MethodID_t3916401116 * get_methodID40_46() const { return ___methodID40_46; }
	inline MethodID_t3916401116 ** get_address_of_methodID40_46() { return &___methodID40_46; }
	inline void set_methodID40_46(MethodID_t3916401116 * value)
	{
		___methodID40_46 = value;
		Il2CppCodeGenWriteBarrier(&___methodID40_46, value);
	}

	inline static int32_t get_offset_of_methodID41_47() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID41_47)); }
	inline MethodID_t3916401116 * get_methodID41_47() const { return ___methodID41_47; }
	inline MethodID_t3916401116 ** get_address_of_methodID41_47() { return &___methodID41_47; }
	inline void set_methodID41_47(MethodID_t3916401116 * value)
	{
		___methodID41_47 = value;
		Il2CppCodeGenWriteBarrier(&___methodID41_47, value);
	}

	inline static int32_t get_offset_of_methodID42_48() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID42_48)); }
	inline MethodID_t3916401116 * get_methodID42_48() const { return ___methodID42_48; }
	inline MethodID_t3916401116 ** get_address_of_methodID42_48() { return &___methodID42_48; }
	inline void set_methodID42_48(MethodID_t3916401116 * value)
	{
		___methodID42_48 = value;
		Il2CppCodeGenWriteBarrier(&___methodID42_48, value);
	}

	inline static int32_t get_offset_of_methodID43_49() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID43_49)); }
	inline MethodID_t3916401116 * get_methodID43_49() const { return ___methodID43_49; }
	inline MethodID_t3916401116 ** get_address_of_methodID43_49() { return &___methodID43_49; }
	inline void set_methodID43_49(MethodID_t3916401116 * value)
	{
		___methodID43_49 = value;
		Il2CppCodeGenWriteBarrier(&___methodID43_49, value);
	}

	inline static int32_t get_offset_of_methodID44_50() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID44_50)); }
	inline MethodID_t3916401116 * get_methodID44_50() const { return ___methodID44_50; }
	inline MethodID_t3916401116 ** get_address_of_methodID44_50() { return &___methodID44_50; }
	inline void set_methodID44_50(MethodID_t3916401116 * value)
	{
		___methodID44_50 = value;
		Il2CppCodeGenWriteBarrier(&___methodID44_50, value);
	}

	inline static int32_t get_offset_of_methodID45_51() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___methodID45_51)); }
	inline MethodID_t3916401116 * get_methodID45_51() const { return ___methodID45_51; }
	inline MethodID_t3916401116 ** get_address_of_methodID45_51() { return &___methodID45_51; }
	inline void set_methodID45_51(MethodID_t3916401116 * value)
	{
		___methodID45_51 = value;
		Il2CppCodeGenWriteBarrier(&___methodID45_51, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache34_52() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___U3CU3Ef__amU24cache34_52)); }
	inline DGetV_1_t986307823 * get_U3CU3Ef__amU24cache34_52() const { return ___U3CU3Ef__amU24cache34_52; }
	inline DGetV_1_t986307823 ** get_address_of_U3CU3Ef__amU24cache34_52() { return &___U3CU3Ef__amU24cache34_52; }
	inline void set_U3CU3Ef__amU24cache34_52(DGetV_1_t986307823 * value)
	{
		___U3CU3Ef__amU24cache34_52 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache34_52, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache35_53() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___U3CU3Ef__amU24cache35_53)); }
	inline DGetV_1_t986307823 * get_U3CU3Ef__amU24cache35_53() const { return ___U3CU3Ef__amU24cache35_53; }
	inline DGetV_1_t986307823 ** get_address_of_U3CU3Ef__amU24cache35_53() { return &___U3CU3Ef__amU24cache35_53; }
	inline void set_U3CU3Ef__amU24cache35_53(DGetV_1_t986307823 * value)
	{
		___U3CU3Ef__amU24cache35_53 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache35_53, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache36_54() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77Generated_t45072847_StaticFields, ___U3CU3Ef__amU24cache36_54)); }
	inline DGetV_1_t986307823 * get_U3CU3Ef__amU24cache36_54() const { return ___U3CU3Ef__amU24cache36_54; }
	inline DGetV_1_t986307823 ** get_address_of_U3CU3Ef__amU24cache36_54() { return &___U3CU3Ef__amU24cache36_54; }
	inline void set_U3CU3Ef__amU24cache36_54(DGetV_1_t986307823 * value)
	{
		___U3CU3Ef__amU24cache36_54 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache36_54, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
