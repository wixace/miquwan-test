﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RVO.Sampled.Agent
struct Agent_t1290054243;
// System.Collections.Generic.List`1<Pathfinding.RVO.ObstacleVertex>
struct List_1_t1243525355;
// Pathfinding.RVO.ObstacleVertex
struct ObstacleVertex_t4170307099;
// Pathfinding.RVO.Simulator/WorkerContext
struct WorkerContext_t2850943897;
// Pathfinding.RVO.Sampled.Agent/VO[]
struct VOU5BU5D_t2243217837;
// Pathfinding.RVO.RVOQuadtree
struct RVOQuadtree_t4291712126;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_RVOLayer1386980398.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_Sampled_Agent1290054243.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_ObstacleVertex4170307099.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_Simulator_Worker2850943897.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_RVOQuadtree4291712126.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_Sampled_Agent_VO2757914308.h"

// System.Void Pathfinding.RVO.Sampled.Agent::.ctor(UnityEngine.Vector3)
extern "C"  void Agent__ctor_m4141411442 (Agent_t1290054243 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::.cctor()
extern "C"  void Agent__cctor_m1758506118 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RVO.Sampled.Agent::get_Position()
extern "C"  Vector3_t4282066566  Agent_get_Position_m1744073877 (Agent_t1290054243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::set_Position(UnityEngine.Vector3)
extern "C"  void Agent_set_Position_m4181175222 (Agent_t1290054243 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RVO.Sampled.Agent::get_InterpolatedPosition()
extern "C"  Vector3_t4282066566  Agent_get_InterpolatedPosition_m1610524664 (Agent_t1290054243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RVO.Sampled.Agent::get_DesiredVelocity()
extern "C"  Vector3_t4282066566  Agent_get_DesiredVelocity_m1052966349 (Agent_t1290054243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::set_DesiredVelocity(UnityEngine.Vector3)
extern "C"  void Agent_set_DesiredVelocity_m404908570 (Agent_t1290054243 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::Teleport(UnityEngine.Vector3)
extern "C"  void Agent_Teleport_m3053891601 (Agent_t1290054243 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::SetYPosition(System.Single)
extern "C"  void Agent_SetYPosition_m4205147630 (Agent_t1290054243 * __this, float ___yCoordinate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RVO.RVOLayer Pathfinding.RVO.Sampled.Agent::get_Layer()
extern "C"  int32_t Agent_get_Layer_m2355573079 (Agent_t1290054243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::set_Layer(Pathfinding.RVO.RVOLayer)
extern "C"  void Agent_set_Layer_m4049898534 (Agent_t1290054243 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RVO.RVOLayer Pathfinding.RVO.Sampled.Agent::get_CollidesWith()
extern "C"  int32_t Agent_get_CollidesWith_m294348215 (Agent_t1290054243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::set_CollidesWith(Pathfinding.RVO.RVOLayer)
extern "C"  void Agent_set_CollidesWith_m748688962 (Agent_t1290054243 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RVO.Sampled.Agent::get_Locked()
extern "C"  bool Agent_get_Locked_m3447230332 (Agent_t1290054243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::set_Locked(System.Boolean)
extern "C"  void Agent_set_Locked_m3071167899 (Agent_t1290054243 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RVO.Sampled.Agent::get_Radius()
extern "C"  float Agent_get_Radius_m3801905644 (Agent_t1290054243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::set_Radius(System.Single)
extern "C"  void Agent_set_Radius_m1820482175 (Agent_t1290054243 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RVO.Sampled.Agent::get_Height()
extern "C"  float Agent_get_Height_m2949631297 (Agent_t1290054243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::set_Height(System.Single)
extern "C"  void Agent_set_Height_m3509705034 (Agent_t1290054243 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RVO.Sampled.Agent::get_MaxSpeed()
extern "C"  float Agent_get_MaxSpeed_m1888746525 (Agent_t1290054243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::set_MaxSpeed(System.Single)
extern "C"  void Agent_set_MaxSpeed_m3938050030 (Agent_t1290054243 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RVO.Sampled.Agent::get_NeighbourDist()
extern "C"  float Agent_get_NeighbourDist_m1455959211 (Agent_t1290054243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::set_NeighbourDist(System.Single)
extern "C"  void Agent_set_NeighbourDist_m1571194784 (Agent_t1290054243 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RVO.Sampled.Agent::get_AgentTimeHorizon()
extern "C"  float Agent_get_AgentTimeHorizon_m1219288611 (Agent_t1290054243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::set_AgentTimeHorizon(System.Single)
extern "C"  void Agent_set_AgentTimeHorizon_m633059240 (Agent_t1290054243 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RVO.Sampled.Agent::get_ObstacleTimeHorizon()
extern "C"  float Agent_get_ObstacleTimeHorizon_m676501767 (Agent_t1290054243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::set_ObstacleTimeHorizon(System.Single)
extern "C"  void Agent_set_ObstacleTimeHorizon_m3587801284 (Agent_t1290054243 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RVO.Sampled.Agent::get_Velocity()
extern "C"  Vector3_t4282066566  Agent_get_Velocity_m2688416585 (Agent_t1290054243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::set_Velocity(UnityEngine.Vector3)
extern "C"  void Agent_set_Velocity_m1984897154 (Agent_t1290054243 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RVO.Sampled.Agent::get_DebugDraw()
extern "C"  bool Agent_get_DebugDraw_m1621370343 (Agent_t1290054243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::set_DebugDraw(System.Boolean)
extern "C"  void Agent_set_DebugDraw_m3036932214 (Agent_t1290054243 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.RVO.Sampled.Agent::get_MaxNeighbours()
extern "C"  int32_t Agent_get_MaxNeighbours_m4040087748 (Agent_t1290054243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::set_MaxNeighbours(System.Int32)
extern "C"  void Agent_set_MaxNeighbours_m417729939 (Agent_t1290054243 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Pathfinding.RVO.ObstacleVertex> Pathfinding.RVO.Sampled.Agent::get_NeighbourObstacles()
extern "C"  List_1_t1243525355 * Agent_get_NeighbourObstacles_m3783466778 (Agent_t1290054243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::BufferSwitch()
extern "C"  void Agent_BufferSwitch_m169363185 (Agent_t1290054243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::Update()
extern "C"  void Agent_Update_m3967464422 (Agent_t1290054243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::Interpolate(System.Single)
extern "C"  void Agent_Interpolate_m3316936837 (Agent_t1290054243 * __this, float ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::CalculateNeighbours()
extern "C"  void Agent_CalculateNeighbours_m387620641 (Agent_t1290054243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RVO.Sampled.Agent::Sqr(System.Single)
extern "C"  float Agent_Sqr_m2664352774 (Agent_t1290054243 * __this, float ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RVO.Sampled.Agent::InsertAgentNeighbour(Pathfinding.RVO.Sampled.Agent,System.Single)
extern "C"  float Agent_InsertAgentNeighbour_m544150257 (Agent_t1290054243 * __this, Agent_t1290054243 * ___agent0, float ___rangeSq1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::InsertObstacleNeighbour(Pathfinding.RVO.ObstacleVertex,System.Single)
extern "C"  void Agent_InsertObstacleNeighbour_m3316585375 (Agent_t1290054243 * __this, ObstacleVertex_t4170307099 * ___ob10, float ___rangeSq1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RVO.Sampled.Agent::To3D(UnityEngine.Vector2)
extern "C"  Vector3_t4282066566  Agent_To3D_m3176091915 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::DrawCircle(UnityEngine.Vector2,System.Single,UnityEngine.Color)
extern "C"  void Agent_DrawCircle_m1433610382 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ____p0, float ___radius1, Color_t4194546905  ___col2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::DrawCircle(UnityEngine.Vector2,System.Single,System.Single,System.Single,UnityEngine.Color)
extern "C"  void Agent_DrawCircle_m2258266648 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ____p0, float ___radius1, float ___a02, float ___a13, Color_t4194546905  ___col4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::DrawVO(UnityEngine.Vector2,System.Single,UnityEngine.Vector2)
extern "C"  void Agent_DrawVO_m846107705 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___circleCenter0, float ___radius1, Vector2_t4282066565  ___origin2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::DrawCross(UnityEngine.Vector2,System.Single)
extern "C"  void Agent_DrawCross_m2535493566 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___p0, float ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::DrawCross(UnityEngine.Vector2,UnityEngine.Color,System.Single)
extern "C"  void Agent_DrawCross_m3294407134 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___p0, Color_t4194546905  ___col1, float ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::CalculateVelocity(Pathfinding.RVO.Simulator/WorkerContext)
extern "C"  void Agent_CalculateVelocity_m3899185713 (Agent_t1290054243 * __this, WorkerContext_t2850943897 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Pathfinding.RVO.Sampled.Agent::Rainbow(System.Single)
extern "C"  Color_t4194546905  Agent_Rainbow_m472969033 (Il2CppObject * __this /* static, unused */, float ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Pathfinding.RVO.Sampled.Agent::Trace(Pathfinding.RVO.Sampled.Agent/VO[],System.Int32,UnityEngine.Vector2,System.Single,System.Single&)
extern "C"  Vector2_t4282066565  Agent_Trace_m3441471926 (Agent_t1290054243 * __this, VOU5BU5D_t2243217837* ___vos0, int32_t ___voCount1, Vector2_t4282066565  ___p2, float ___cutoff3, float* ___score4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RVO.Sampled.Agent::IntersectionFactor(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&)
extern "C"  bool Agent_IntersectionFactor_m1744435026 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start10, Vector2_t4282066565  ___dir11, Vector2_t4282066565  ___start22, Vector2_t4282066565  ___dir23, float* ___factor4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RVO.Sampled.Agent::ilo_get_Position1(Pathfinding.RVO.Sampled.Agent)
extern "C"  Vector3_t4282066566  Agent_ilo_get_Position1_m2222354403 (Il2CppObject * __this /* static, unused */, Agent_t1290054243 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RVO.Sampled.Agent::ilo_get_AgentTimeHorizon2(Pathfinding.RVO.Sampled.Agent)
extern "C"  float Agent_ilo_get_AgentTimeHorizon2_m3182076624 (Il2CppObject * __this /* static, unused */, Agent_t1290054243 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.RVO.Sampled.Agent::ilo_get_MaxNeighbours3(Pathfinding.RVO.Sampled.Agent)
extern "C"  int32_t Agent_ilo_get_MaxNeighbours3_m3059452938 (Il2CppObject * __this /* static, unused */, Agent_t1290054243 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RVO.Sampled.Agent::ilo_get_DesiredVelocity4(Pathfinding.RVO.Sampled.Agent)
extern "C"  Vector3_t4282066566  Agent_ilo_get_DesiredVelocity4_m381586930 (Il2CppObject * __this /* static, unused */, Agent_t1290054243 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RVO.RVOLayer Pathfinding.RVO.Sampled.Agent::ilo_get_CollidesWith5(Pathfinding.RVO.Sampled.Agent)
extern "C"  int32_t Agent_ilo_get_CollidesWith5_m198258561 (Il2CppObject * __this /* static, unused */, Agent_t1290054243 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::ilo_Query6(Pathfinding.RVO.RVOQuadtree,UnityEngine.Vector2,System.Single,Pathfinding.RVO.Sampled.Agent)
extern "C"  void Agent_ilo_Query6_m2335173458 (Il2CppObject * __this /* static, unused */, RVOQuadtree_t4291712126 * ____this0, Vector2_t4282066565  ___p1, float ___radius2, Agent_t1290054243 * ___agent3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::ilo_DrawCircle7(UnityEngine.Vector2,System.Single,System.Single,System.Single,UnityEngine.Color)
extern "C"  void Agent_ilo_DrawCircle7_m1112190868 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ____p0, float ___radius1, float ___a02, float ___a13, Color_t4194546905  ___col4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::ilo_DrawVO8(UnityEngine.Vector2,System.Single,UnityEngine.Vector2)
extern "C"  void Agent_ilo_DrawVO8_m3045409734 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___circleCenter0, float ___radius1, Vector2_t4282066565  ___origin2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.RVO.Sampled.Agent::ilo_To3D9(UnityEngine.Vector2)
extern "C"  Vector3_t4282066566  Agent_ilo_To3D9_m3991724097 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Pathfinding.RVO.Sampled.Agent::ilo_Trace10(Pathfinding.RVO.Sampled.Agent,Pathfinding.RVO.Sampled.Agent/VO[],System.Int32,UnityEngine.Vector2,System.Single,System.Single&)
extern "C"  Vector2_t4282066565  Agent_ilo_Trace10_m3035477754 (Il2CppObject * __this /* static, unused */, Agent_t1290054243 * ____this0, VOU5BU5D_t2243217837* ___vos1, int32_t ___voCount2, Vector2_t4282066565  ___p3, float ___cutoff4, float* ___score5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RVO.Sampled.Agent::ilo_get_DebugDraw11(Pathfinding.RVO.Sampled.Agent)
extern "C"  bool Agent_ilo_get_DebugDraw11_m1858102810 (Il2CppObject * __this /* static, unused */, Agent_t1290054243 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::ilo_DrawCross12(UnityEngine.Vector2,UnityEngine.Color,System.Single)
extern "C"  void Agent_ilo_DrawCross12_m708484752 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___p0, Color_t4194546905  ___col1, float ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RVO.Sampled.Agent::ilo_ScalarSample13(Pathfinding.RVO.Sampled.Agent/VO&,UnityEngine.Vector2)
extern "C"  float Agent_ilo_ScalarSample13_m3788391912 (Il2CppObject * __this /* static, unused */, VO_t2757914308 * ____this0, Vector2_t4282066565  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Pathfinding.RVO.Sampled.Agent::ilo_Rainbow14(System.Single)
extern "C"  Color_t4194546905  Agent_ilo_Rainbow14_m2672418233 (Il2CppObject * __this /* static, unused */, float ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent::ilo_DrawCross15(UnityEngine.Vector2,System.Single)
extern "C"  void Agent_ilo_DrawCross15_m4200748557 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___p0, float ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
