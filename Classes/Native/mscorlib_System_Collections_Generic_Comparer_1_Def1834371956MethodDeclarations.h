﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.ClipperLib.IntPoint>
struct DefaultComparer_t1834371956;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.ClipperLib.IntPoint>::.ctor()
extern "C"  void DefaultComparer__ctor_m3954951729_gshared (DefaultComparer_t1834371956 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3954951729(__this, method) ((  void (*) (DefaultComparer_t1834371956 *, const MethodInfo*))DefaultComparer__ctor_m3954951729_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.ClipperLib.IntPoint>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m2771127710_gshared (DefaultComparer_t1834371956 * __this, IntPoint_t3326126179  ___x0, IntPoint_t3326126179  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m2771127710(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t1834371956 *, IntPoint_t3326126179 , IntPoint_t3326126179 , const MethodInfo*))DefaultComparer_Compare_m2771127710_gshared)(__this, ___x0, ___y1, method)
