﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.IO.FileStream
struct FileStream_t2141505868;
// System.IO.StreamWriter
struct StreamWriter_t2705123075;
// System.Action`3<System.String,LogLevel,System.Boolean>
struct Action_3_t3557919653;
// System.Object
struct Il2CppObject;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t1894914657;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LogWriter
struct  LogWriter_t4068659383  : public Il2CppObject
{
public:
	// System.String LogWriter::m_logPath
	String_t* ___m_logPath_0;
	// System.String LogWriter::m_logFileName
	String_t* ___m_logFileName_1;
	// System.String LogWriter::m_logFilePath
	String_t* ___m_logFilePath_2;
	// System.IO.FileStream LogWriter::m_fs
	FileStream_t2141505868 * ___m_fs_3;
	// System.IO.StreamWriter LogWriter::m_sw
	StreamWriter_t2705123075 * ___m_sw_4;
	// System.Action`3<System.String,LogLevel,System.Boolean> LogWriter::m_logWriter
	Action_3_t3557919653 * ___m_logWriter_5;

public:
	inline static int32_t get_offset_of_m_logPath_0() { return static_cast<int32_t>(offsetof(LogWriter_t4068659383, ___m_logPath_0)); }
	inline String_t* get_m_logPath_0() const { return ___m_logPath_0; }
	inline String_t** get_address_of_m_logPath_0() { return &___m_logPath_0; }
	inline void set_m_logPath_0(String_t* value)
	{
		___m_logPath_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_logPath_0, value);
	}

	inline static int32_t get_offset_of_m_logFileName_1() { return static_cast<int32_t>(offsetof(LogWriter_t4068659383, ___m_logFileName_1)); }
	inline String_t* get_m_logFileName_1() const { return ___m_logFileName_1; }
	inline String_t** get_address_of_m_logFileName_1() { return &___m_logFileName_1; }
	inline void set_m_logFileName_1(String_t* value)
	{
		___m_logFileName_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_logFileName_1, value);
	}

	inline static int32_t get_offset_of_m_logFilePath_2() { return static_cast<int32_t>(offsetof(LogWriter_t4068659383, ___m_logFilePath_2)); }
	inline String_t* get_m_logFilePath_2() const { return ___m_logFilePath_2; }
	inline String_t** get_address_of_m_logFilePath_2() { return &___m_logFilePath_2; }
	inline void set_m_logFilePath_2(String_t* value)
	{
		___m_logFilePath_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_logFilePath_2, value);
	}

	inline static int32_t get_offset_of_m_fs_3() { return static_cast<int32_t>(offsetof(LogWriter_t4068659383, ___m_fs_3)); }
	inline FileStream_t2141505868 * get_m_fs_3() const { return ___m_fs_3; }
	inline FileStream_t2141505868 ** get_address_of_m_fs_3() { return &___m_fs_3; }
	inline void set_m_fs_3(FileStream_t2141505868 * value)
	{
		___m_fs_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_fs_3, value);
	}

	inline static int32_t get_offset_of_m_sw_4() { return static_cast<int32_t>(offsetof(LogWriter_t4068659383, ___m_sw_4)); }
	inline StreamWriter_t2705123075 * get_m_sw_4() const { return ___m_sw_4; }
	inline StreamWriter_t2705123075 ** get_address_of_m_sw_4() { return &___m_sw_4; }
	inline void set_m_sw_4(StreamWriter_t2705123075 * value)
	{
		___m_sw_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_sw_4, value);
	}

	inline static int32_t get_offset_of_m_logWriter_5() { return static_cast<int32_t>(offsetof(LogWriter_t4068659383, ___m_logWriter_5)); }
	inline Action_3_t3557919653 * get_m_logWriter_5() const { return ___m_logWriter_5; }
	inline Action_3_t3557919653 ** get_address_of_m_logWriter_5() { return &___m_logWriter_5; }
	inline void set_m_logWriter_5(Action_3_t3557919653 * value)
	{
		___m_logWriter_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_logWriter_5, value);
	}
};

struct LogWriter_t4068659383_StaticFields
{
public:
	// System.Object LogWriter::m_locker
	Il2CppObject * ___m_locker_6;
	// System.Net.Security.RemoteCertificateValidationCallback LogWriter::<>f__am$cache7
	RemoteCertificateValidationCallback_t1894914657 * ___U3CU3Ef__amU24cache7_7;

public:
	inline static int32_t get_offset_of_m_locker_6() { return static_cast<int32_t>(offsetof(LogWriter_t4068659383_StaticFields, ___m_locker_6)); }
	inline Il2CppObject * get_m_locker_6() const { return ___m_locker_6; }
	inline Il2CppObject ** get_address_of_m_locker_6() { return &___m_locker_6; }
	inline void set_m_locker_6(Il2CppObject * value)
	{
		___m_locker_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_locker_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(LogWriter_t4068659383_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline RemoteCertificateValidationCallback_t1894914657 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline RemoteCertificateValidationCallback_t1894914657 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(RemoteCertificateValidationCallback_t1894914657 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
