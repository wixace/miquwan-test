﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameObjectVisitor/condition
struct condition_t3833526985;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void GameObjectVisitor/condition::.ctor(System.Object,System.IntPtr)
extern "C"  void condition__ctor_m3330453536 (condition_t3833526985 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameObjectVisitor/condition::Invoke(UnityEngine.GameObject)
extern "C"  bool condition_Invoke_m3687976254 (condition_t3833526985 * __this, GameObject_t3674682005 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult GameObjectVisitor/condition::BeginInvoke(UnityEngine.GameObject,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * condition_BeginInvoke_m3734449781 (condition_t3833526985 * __this, GameObject_t3674682005 * ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameObjectVisitor/condition::EndInvoke(System.IAsyncResult)
extern "C"  bool condition_EndInvoke_m1985970940 (condition_t3833526985 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
