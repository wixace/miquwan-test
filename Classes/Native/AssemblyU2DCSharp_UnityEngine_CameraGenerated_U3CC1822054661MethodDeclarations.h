﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_CameraGenerated/<Camera_onPreCull_GetDelegate_member0_arg0>c__AnonStoreyDE
struct U3CCamera_onPreCull_GetDelegate_member0_arg0U3Ec__AnonStoreyDE_t1822054661;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"

// System.Void UnityEngine_CameraGenerated/<Camera_onPreCull_GetDelegate_member0_arg0>c__AnonStoreyDE::.ctor()
extern "C"  void U3CCamera_onPreCull_GetDelegate_member0_arg0U3Ec__AnonStoreyDE__ctor_m4093145398 (U3CCamera_onPreCull_GetDelegate_member0_arg0U3Ec__AnonStoreyDE_t1822054661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated/<Camera_onPreCull_GetDelegate_member0_arg0>c__AnonStoreyDE::<>m__190(UnityEngine.Camera)
extern "C"  void U3CCamera_onPreCull_GetDelegate_member0_arg0U3Ec__AnonStoreyDE_U3CU3Em__190_m1080330287 (U3CCamera_onPreCull_GetDelegate_member0_arg0U3Ec__AnonStoreyDE_t1822054661 * __this, Camera_t2727095145 * ___cam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
