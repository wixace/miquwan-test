﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.BufferPool
struct BufferPool_t1280157896;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void ProtoBuf.BufferPool::.ctor()
extern "C"  void BufferPool__ctor_m119271276 (BufferPool_t1280157896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.BufferPool::.cctor()
extern "C"  void BufferPool__cctor_m3215313345 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.BufferPool::Flush()
extern "C"  void BufferPool_Flush_m203218574 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ProtoBuf.BufferPool::GetBuffer()
extern "C"  ByteU5BU5D_t4260760469* BufferPool_GetBuffer_m3679722666 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.BufferPool::ResizeAndFlushLeft(System.Byte[]&,System.Int32,System.Int32,System.Int32)
extern "C"  void BufferPool_ResizeAndFlushLeft_m3973971700 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469** ___buffer0, int32_t ___toFitAtLeastBytes1, int32_t ___copyFromIndex2, int32_t ___copyBytes3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.BufferPool::ReleaseBufferToPool(System.Byte[]&)
extern "C"  void BufferPool_ReleaseBufferToPool_m605610923 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469** ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.BufferPool::ilo_ReleaseBufferToPool1(System.Byte[]&)
extern "C"  void BufferPool_ilo_ReleaseBufferToPool1_m3710091617 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469** ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
