﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.AstarData
struct AstarData_t3283402719;
// AstarPath
struct AstarPath_t4090270936;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// Pathfinding.Serialization.SerializeSettings
struct SerializeSettings_t2480699453;
// Pathfinding.Serialization.AstarSerializer
struct AstarSerializer_t1384811327;
// System.Type
struct Type_t;
// System.String
struct String_t;
// Pathfinding.NavGraph
struct NavGraph_t1254319713;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// Pathfinding.NavGraph[]
struct NavGraphU5BU5D_t850130684;
// System.Collections.IEnumerable
struct IEnumerable_t3464557803;
// Pathfinding.UserConnection[]
struct UserConnectionU5BU5D_t328197926;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Serialization_Serial2480699453.h"
#include "AssemblyU2DCSharp_Pathfinding_Serialization_AstarS1384811327.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_Pathfinding_NavGraph1254319713.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_Guid3584625871.h"
#include "AssemblyU2DCSharp_Pathfinding_AstarData3283402719.h"

// System.Void Pathfinding.AstarData::.ctor()
extern "C"  void AstarData__ctor_m3337821240 (AstarData_t3283402719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AstarPath Pathfinding.AstarData::get_active()
extern "C"  AstarPath_t4090270936 * AstarData_get_active_m3879711648 (AstarData_t3283402719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pathfinding.AstarData::GetData()
extern "C"  ByteU5BU5D_t4260760469* AstarData_GetData_m1180271392 (AstarData_t3283402719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::SetData(System.Byte[],System.UInt32)
extern "C"  void AstarData_SetData_m3900295355 (AstarData_t3283402719 * __this, ByteU5BU5D_t4260760469* ___data0, uint32_t ___checksum1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::Awake()
extern "C"  void AstarData_Awake_m3575426459 (AstarData_t3283402719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::UpdateShortcuts()
extern "C"  void AstarData_UpdateShortcuts_m3771353146 (AstarData_t3283402719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::LoadFromCache()
extern "C"  void AstarData_LoadFromCache_m1941804936 (AstarData_t3283402719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pathfinding.AstarData::SerializeGraphs()
extern "C"  ByteU5BU5D_t4260760469* AstarData_SerializeGraphs_m2533749413 (AstarData_t3283402719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pathfinding.AstarData::SerializeGraphs(Pathfinding.Serialization.SerializeSettings)
extern "C"  ByteU5BU5D_t4260760469* AstarData_SerializeGraphs_m1784120690 (AstarData_t3283402719 * __this, SerializeSettings_t2480699453 * ___settings0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pathfinding.AstarData::SerializeGraphs(Pathfinding.Serialization.SerializeSettings,System.UInt32&)
extern "C"  ByteU5BU5D_t4260760469* AstarData_SerializeGraphs_m3715167334 (AstarData_t3283402719 * __this, SerializeSettings_t2480699453 * ___settings0, uint32_t* ___checksum1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::SerializeGraphsPart(Pathfinding.Serialization.AstarSerializer)
extern "C"  void AstarData_SerializeGraphsPart_m3840615527 (AstarData_t3283402719 * __this, AstarSerializer_t1384811327 * ___sr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::DeserializeGraphs()
extern "C"  void AstarData_DeserializeGraphs_m1406254618 (AstarData_t3283402719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::ClearGraphs()
extern "C"  void AstarData_ClearGraphs_m1542155944 (AstarData_t3283402719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::OnDestroy()
extern "C"  void AstarData_OnDestroy_m854332721 (AstarData_t3283402719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::DeserializeGraphs(System.Byte[])
extern "C"  void AstarData_DeserializeGraphs_m263420911 (AstarData_t3283402719 * __this, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::DeserializeGraphsAdditive(System.Byte[])
extern "C"  void AstarData_DeserializeGraphsAdditive_m2417725315 (AstarData_t3283402719 * __this, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::DeserializeGraphsPart(Pathfinding.Serialization.AstarSerializer)
extern "C"  void AstarData_DeserializeGraphsPart_m783154792 (AstarData_t3283402719 * __this, AstarSerializer_t1384811327 * ___sr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::DeserializeGraphsPartAdditive(Pathfinding.Serialization.AstarSerializer)
extern "C"  void AstarData_DeserializeGraphsPartAdditive_m3018772220 (AstarData_t3283402719 * __this, AstarSerializer_t1384811327 * ___sr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::FindGraphTypes()
extern "C"  void AstarData_FindGraphTypes_m4193949104 (AstarData_t3283402719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Pathfinding.AstarData::GetGraphType(System.String)
extern "C"  Type_t * AstarData_GetGraphType_m2346344158 (AstarData_t3283402719 * __this, String_t* ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NavGraph Pathfinding.AstarData::CreateGraph(System.String)
extern "C"  NavGraph_t1254319713 * AstarData_CreateGraph_m2442166130 (AstarData_t3283402719 * __this, String_t* ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NavGraph Pathfinding.AstarData::CreateGraph(System.Type)
extern "C"  NavGraph_t1254319713 * AstarData_CreateGraph_m1103051753 (AstarData_t3283402719 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NavGraph Pathfinding.AstarData::AddGraph(System.String)
extern "C"  NavGraph_t1254319713 * AstarData_AddGraph_m2895305201 (AstarData_t3283402719 * __this, String_t* ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NavGraph Pathfinding.AstarData::AddGraph(System.Type)
extern "C"  NavGraph_t1254319713 * AstarData_AddGraph_m1782852136 (AstarData_t3283402719 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::AddGraph(Pathfinding.NavGraph)
extern "C"  void AstarData_AddGraph_m2927217668 (AstarData_t3283402719 * __this, NavGraph_t1254319713 * ___graph0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.AstarData::RemoveGraph(Pathfinding.NavGraph)
extern "C"  bool AstarData_RemoveGraph_m1104433407 (AstarData_t3283402719 * __this, NavGraph_t1254319713 * ___graph0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NavGraph Pathfinding.AstarData::GetGraph(Pathfinding.GraphNode)
extern "C"  NavGraph_t1254319713 * AstarData_GetGraph_m1109118992 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.GraphNode Pathfinding.AstarData::GetNode(System.Int32,System.Int32)
extern "C"  GraphNode_t23612370 * AstarData_GetNode_m893209323 (AstarData_t3283402719 * __this, int32_t ___graphIndex0, int32_t ___nodeIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.GraphNode Pathfinding.AstarData::GetNode(System.Int32,System.Int32,Pathfinding.NavGraph[])
extern "C"  GraphNode_t23612370 * AstarData_GetNode_m3517177124 (AstarData_t3283402719 * __this, int32_t ___graphIndex0, int32_t ___nodeIndex1, NavGraphU5BU5D_t850130684* ___graphs2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NavGraph Pathfinding.AstarData::FindGraphOfType(System.Type)
extern "C"  NavGraph_t1254319713 * AstarData_FindGraphOfType_m341905269 (AstarData_t3283402719 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerable Pathfinding.AstarData::FindGraphsOfType(System.Type)
extern "C"  Il2CppObject * AstarData_FindGraphsOfType_m3942088770 (AstarData_t3283402719 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerable Pathfinding.AstarData::GetUpdateableGraphs()
extern "C"  Il2CppObject * AstarData_GetUpdateableGraphs_m3803278776 (AstarData_t3283402719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerable Pathfinding.AstarData::GetRaycastableGraphs()
extern "C"  Il2CppObject * AstarData_GetRaycastableGraphs_m1984395418 (AstarData_t3283402719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.AstarData::GetGraphIndex(Pathfinding.NavGraph)
extern "C"  int32_t AstarData_GetGraphIndex_m2416648841 (AstarData_t3283402719 * __this, NavGraph_t1254319713 * ___graph0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.AstarData::GuidToIndex(Pathfinding.Util.Guid)
extern "C"  int32_t AstarData_GuidToIndex_m627989685 (AstarData_t3283402719 * __this, Guid_t3584625871  ___guid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NavGraph Pathfinding.AstarData::GuidToGraph(Pathfinding.Util.Guid)
extern "C"  NavGraph_t1254319713 * AstarData_GuidToGraph_m2058383679 (AstarData_t3283402719 * __this, Guid_t3584625871  ___guid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::ilo_LoadFromCache1(Pathfinding.AstarData)
extern "C"  void AstarData_ilo_LoadFromCache1_m1158095169 (Il2CppObject * __this /* static, unused */, AstarData_t3283402719 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::ilo_DeserializeGraphs2(Pathfinding.AstarData)
extern "C"  void AstarData_ilo_DeserializeGraphs2_m2752405170 (Il2CppObject * __this /* static, unused */, AstarData_t3283402719 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NavGraph Pathfinding.AstarData::ilo_FindGraphOfType3(Pathfinding.AstarData,System.Type)
extern "C"  NavGraph_t1254319713 * AstarData_ilo_FindGraphOfType3_m3649661806 (Il2CppObject * __this /* static, unused */, AstarData_t3283402719 * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Serialization.SerializeSettings Pathfinding.AstarData::ilo_get_Settings4()
extern "C"  SerializeSettings_t2480699453 * AstarData_ilo_get_Settings4_m160668387 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pathfinding.AstarData::ilo_SerializeGraphs5(Pathfinding.AstarData,Pathfinding.Serialization.SerializeSettings,System.UInt32&)
extern "C"  ByteU5BU5D_t4260760469* AstarData_ilo_SerializeGraphs5_m3425799917 (Il2CppObject * __this /* static, unused */, AstarData_t3283402719 * ____this0, SerializeSettings_t2480699453 * ___settings1, uint32_t* ___checksum2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::ilo_OpenSerialize6(Pathfinding.Serialization.AstarSerializer)
extern "C"  void AstarData_ilo_OpenSerialize6_m3987002710 (Il2CppObject * __this /* static, unused */, AstarSerializer_t1384811327 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::ilo_SerializeGraphsPart7(Pathfinding.AstarData,Pathfinding.Serialization.AstarSerializer)
extern "C"  void AstarData_ilo_SerializeGraphsPart7_m4232365680 (Il2CppObject * __this /* static, unused */, AstarData_t3283402719 * ____this0, AstarSerializer_t1384811327 * ___sr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::ilo_SerializeExtraInfo8(Pathfinding.Serialization.AstarSerializer)
extern "C"  void AstarData_ilo_SerializeExtraInfo8_m148614136 (Il2CppObject * __this /* static, unused */, AstarSerializer_t1384811327 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::ilo_DeserializeGraphs9(Pathfinding.AstarData,System.Byte[])
extern "C"  void AstarData_ilo_DeserializeGraphs9_m3191181454 (Il2CppObject * __this /* static, unused */, AstarData_t3283402719 * ____this0, ByteU5BU5D_t4260760469* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::ilo_OnDestroy10(Pathfinding.NavGraph)
extern "C"  void AstarData_ilo_OnDestroy10_m4027237704 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::ilo_UpdateShortcuts11(Pathfinding.AstarData)
extern "C"  void AstarData_ilo_UpdateShortcuts11_m2732545624 (Il2CppObject * __this /* static, unused */, AstarData_t3283402719 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::ilo_ClearGraphs12(Pathfinding.AstarData)
extern "C"  void AstarData_ilo_ClearGraphs12_m935539849 (Il2CppObject * __this /* static, unused */, AstarData_t3283402719 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::ilo_DeserializeGraphsPart13(Pathfinding.AstarData,Pathfinding.Serialization.AstarSerializer)
extern "C"  void AstarData_ilo_DeserializeGraphsPart13_m638946034 (Il2CppObject * __this /* static, unused */, AstarData_t3283402719 * ____this0, AstarSerializer_t1384811327 * ___sr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::ilo_DeserializeGraphsPartAdditive14(Pathfinding.AstarData,Pathfinding.Serialization.AstarSerializer)
extern "C"  void AstarData_ilo_DeserializeGraphsPartAdditive14_m853345445 (Il2CppObject * __this /* static, unused */, AstarData_t3283402719 * ____this0, AstarSerializer_t1384811327 * ___sr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::ilo_CloseDeserialize15(Pathfinding.Serialization.AstarSerializer)
extern "C"  void AstarData_ilo_CloseDeserialize15_m1244191723 (Il2CppObject * __this /* static, unused */, AstarSerializer_t1384811327 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NavGraph[] Pathfinding.AstarData::ilo_DeserializeGraphs16(Pathfinding.Serialization.AstarSerializer)
extern "C"  NavGraphU5BU5D_t850130684* AstarData_ilo_DeserializeGraphs16_m2355772259 (Il2CppObject * __this /* static, unused */, AstarSerializer_t1384811327 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::ilo_DeserializeExtraInfo17(Pathfinding.Serialization.AstarSerializer)
extern "C"  void AstarData_ilo_DeserializeExtraInfo17_m4217338609 (Il2CppObject * __this /* static, unused */, AstarSerializer_t1384811327 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.UserConnection[] Pathfinding.AstarData::ilo_DeserializeUserConnections18(Pathfinding.Serialization.AstarSerializer)
extern "C"  UserConnectionU5BU5D_t328197926* AstarData_ilo_DeserializeUserConnections18_m3740523756 (Il2CppObject * __this /* static, unused */, AstarSerializer_t1384811327 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::ilo_PostDeserialization19(Pathfinding.Serialization.AstarSerializer)
extern "C"  void AstarData_ilo_PostDeserialization19_m1899353019 (Il2CppObject * __this /* static, unused */, AstarSerializer_t1384811327 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Util.Guid Pathfinding.AstarData::ilo_get_guid20(Pathfinding.NavGraph)
extern "C"  Guid_t3584625871  AstarData_ilo_get_guid20_m2319264498 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::ilo_set_guid21(Pathfinding.NavGraph,Pathfinding.Util.Guid)
extern "C"  void AstarData_ilo_set_guid21_m4051622032 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ____this0, Guid_t3584625871  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NavGraph Pathfinding.AstarData::ilo_CreateGraph22(Pathfinding.AstarData,System.Type)
extern "C"  NavGraph_t1254319713 * AstarData_ilo_CreateGraph22_m3840102869 (Il2CppObject * __this /* static, unused */, AstarData_t3283402719 * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::ilo_AddGraph23(Pathfinding.AstarData,Pathfinding.NavGraph)
extern "C"  void AstarData_ilo_AddGraph23_m4005604575 (Il2CppObject * __this /* static, unused */, AstarData_t3283402719 * ____this0, NavGraph_t1254319713 * ___graph1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AstarData::ilo_Awake24(Pathfinding.NavGraph)
extern "C"  void AstarData_ilo_Awake24_m3022562453 (Il2CppObject * __this /* static, unused */, NavGraph_t1254319713 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
