﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.RegularExpressions.Regex
struct Regex_t2161232213;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Networking.UnityWebRequest
struct  UnityWebRequest_t327863158  : public Il2CppObject
{
public:
	// System.IntPtr UnityEngine.Experimental.Networking.UnityWebRequest::m_Ptr
	IntPtr_t ___m_Ptr_0;
	// System.Boolean UnityEngine.Experimental.Networking.UnityWebRequest::<disposeDownloadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3;
	// System.Boolean UnityEngine.Experimental.Networking.UnityWebRequest::<disposeUploadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UnityWebRequest_t327863158, ___m_Ptr_0)); }
	inline IntPtr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline IntPtr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(IntPtr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UnityWebRequest_t327863158, ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3)); }
	inline bool get_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3() const { return ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3() { return &___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3; }
	inline void set_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3(bool value)
	{
		___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(UnityWebRequest_t327863158, ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4)); }
	inline bool get_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4() const { return ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4() { return &___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4; }
	inline void set_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4(bool value)
	{
		___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4 = value;
	}
};

struct UnityWebRequest_t327863158_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex UnityEngine.Experimental.Networking.UnityWebRequest::domainRegex
	Regex_t2161232213 * ___domainRegex_1;
	// System.String[] UnityEngine.Experimental.Networking.UnityWebRequest::forbiddenHeaderKeys
	StringU5BU5D_t4054002952* ___forbiddenHeaderKeys_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> UnityEngine.Experimental.Networking.UnityWebRequest::<>f__switch$map1
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map1_5;

public:
	inline static int32_t get_offset_of_domainRegex_1() { return static_cast<int32_t>(offsetof(UnityWebRequest_t327863158_StaticFields, ___domainRegex_1)); }
	inline Regex_t2161232213 * get_domainRegex_1() const { return ___domainRegex_1; }
	inline Regex_t2161232213 ** get_address_of_domainRegex_1() { return &___domainRegex_1; }
	inline void set_domainRegex_1(Regex_t2161232213 * value)
	{
		___domainRegex_1 = value;
		Il2CppCodeGenWriteBarrier(&___domainRegex_1, value);
	}

	inline static int32_t get_offset_of_forbiddenHeaderKeys_2() { return static_cast<int32_t>(offsetof(UnityWebRequest_t327863158_StaticFields, ___forbiddenHeaderKeys_2)); }
	inline StringU5BU5D_t4054002952* get_forbiddenHeaderKeys_2() const { return ___forbiddenHeaderKeys_2; }
	inline StringU5BU5D_t4054002952** get_address_of_forbiddenHeaderKeys_2() { return &___forbiddenHeaderKeys_2; }
	inline void set_forbiddenHeaderKeys_2(StringU5BU5D_t4054002952* value)
	{
		___forbiddenHeaderKeys_2 = value;
		Il2CppCodeGenWriteBarrier(&___forbiddenHeaderKeys_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_5() { return static_cast<int32_t>(offsetof(UnityWebRequest_t327863158_StaticFields, ___U3CU3Ef__switchU24map1_5)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map1_5() const { return ___U3CU3Ef__switchU24map1_5; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map1_5() { return &___U3CU3Ef__switchU24map1_5; }
	inline void set_U3CU3Ef__switchU24map1_5(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map1_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: UnityEngine.Experimental.Networking.UnityWebRequest
struct UnityWebRequest_t327863158_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4;
};
// Native definition for marshalling of: UnityEngine.Experimental.Networking.UnityWebRequest
struct UnityWebRequest_t327863158_marshaled_com
{
	intptr_t ___m_Ptr_0;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_3;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_4;
};
