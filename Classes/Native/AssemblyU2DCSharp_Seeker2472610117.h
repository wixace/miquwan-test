﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.StartEndModifier
struct StartEndModifier_t2926562694;
// Pathfinding.TagMask
struct TagMask_t3803327112;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// OnPathDelegate
struct OnPathDelegate_t598607977;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// System.Collections.Generic.List`1<Pathfinding.GraphNode>
struct List_1_t1391797922;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// System.Collections.Generic.List`1<Pathfinding.IPathModifier>
struct List_1_t2456908887;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Seeker
struct  Seeker_t2472610117  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean Seeker::drawGizmos
	bool ___drawGizmos_2;
	// System.Boolean Seeker::detailedGizmos
	bool ___detailedGizmos_3;
	// System.Boolean Seeker::saveGetNearestHints
	bool ___saveGetNearestHints_4;
	// Pathfinding.StartEndModifier Seeker::startEndModifier
	StartEndModifier_t2926562694 * ___startEndModifier_5;
	// Pathfinding.TagMask Seeker::traversableTags
	TagMask_t3803327112 * ___traversableTags_6;
	// System.Int32[] Seeker::tagPenalties
	Int32U5BU5D_t3230847821* ___tagPenalties_7;
	// OnPathDelegate Seeker::pathCallback
	OnPathDelegate_t598607977 * ___pathCallback_8;
	// OnPathDelegate Seeker::preProcessPath
	OnPathDelegate_t598607977 * ___preProcessPath_9;
	// OnPathDelegate Seeker::postProcessOriginalPath
	OnPathDelegate_t598607977 * ___postProcessOriginalPath_10;
	// OnPathDelegate Seeker::postProcessPath
	OnPathDelegate_t598607977 * ___postProcessPath_11;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Seeker::lastCompletedVectorPath
	List_1_t1355284822 * ___lastCompletedVectorPath_12;
	// System.Collections.Generic.List`1<Pathfinding.GraphNode> Seeker::lastCompletedNodePath
	List_1_t1391797922 * ___lastCompletedNodePath_13;
	// Pathfinding.Path Seeker::path
	Path_t1974241691 * ___path_14;
	// Pathfinding.Path Seeker::prevPath
	Path_t1974241691 * ___prevPath_15;
	// Pathfinding.GraphNode Seeker::startHint
	GraphNode_t23612370 * ___startHint_16;
	// Pathfinding.GraphNode Seeker::endHint
	GraphNode_t23612370 * ___endHint_17;
	// OnPathDelegate Seeker::onPathDelegate
	OnPathDelegate_t598607977 * ___onPathDelegate_18;
	// OnPathDelegate Seeker::onPartialPathDelegate
	OnPathDelegate_t598607977 * ___onPartialPathDelegate_19;
	// OnPathDelegate Seeker::tmpPathCallback
	OnPathDelegate_t598607977 * ___tmpPathCallback_20;
	// System.UInt32 Seeker::lastPathID
	uint32_t ___lastPathID_21;
	// System.Collections.Generic.List`1<Pathfinding.IPathModifier> Seeker::modifiers
	List_1_t2456908887 * ___modifiers_22;

public:
	inline static int32_t get_offset_of_drawGizmos_2() { return static_cast<int32_t>(offsetof(Seeker_t2472610117, ___drawGizmos_2)); }
	inline bool get_drawGizmos_2() const { return ___drawGizmos_2; }
	inline bool* get_address_of_drawGizmos_2() { return &___drawGizmos_2; }
	inline void set_drawGizmos_2(bool value)
	{
		___drawGizmos_2 = value;
	}

	inline static int32_t get_offset_of_detailedGizmos_3() { return static_cast<int32_t>(offsetof(Seeker_t2472610117, ___detailedGizmos_3)); }
	inline bool get_detailedGizmos_3() const { return ___detailedGizmos_3; }
	inline bool* get_address_of_detailedGizmos_3() { return &___detailedGizmos_3; }
	inline void set_detailedGizmos_3(bool value)
	{
		___detailedGizmos_3 = value;
	}

	inline static int32_t get_offset_of_saveGetNearestHints_4() { return static_cast<int32_t>(offsetof(Seeker_t2472610117, ___saveGetNearestHints_4)); }
	inline bool get_saveGetNearestHints_4() const { return ___saveGetNearestHints_4; }
	inline bool* get_address_of_saveGetNearestHints_4() { return &___saveGetNearestHints_4; }
	inline void set_saveGetNearestHints_4(bool value)
	{
		___saveGetNearestHints_4 = value;
	}

	inline static int32_t get_offset_of_startEndModifier_5() { return static_cast<int32_t>(offsetof(Seeker_t2472610117, ___startEndModifier_5)); }
	inline StartEndModifier_t2926562694 * get_startEndModifier_5() const { return ___startEndModifier_5; }
	inline StartEndModifier_t2926562694 ** get_address_of_startEndModifier_5() { return &___startEndModifier_5; }
	inline void set_startEndModifier_5(StartEndModifier_t2926562694 * value)
	{
		___startEndModifier_5 = value;
		Il2CppCodeGenWriteBarrier(&___startEndModifier_5, value);
	}

	inline static int32_t get_offset_of_traversableTags_6() { return static_cast<int32_t>(offsetof(Seeker_t2472610117, ___traversableTags_6)); }
	inline TagMask_t3803327112 * get_traversableTags_6() const { return ___traversableTags_6; }
	inline TagMask_t3803327112 ** get_address_of_traversableTags_6() { return &___traversableTags_6; }
	inline void set_traversableTags_6(TagMask_t3803327112 * value)
	{
		___traversableTags_6 = value;
		Il2CppCodeGenWriteBarrier(&___traversableTags_6, value);
	}

	inline static int32_t get_offset_of_tagPenalties_7() { return static_cast<int32_t>(offsetof(Seeker_t2472610117, ___tagPenalties_7)); }
	inline Int32U5BU5D_t3230847821* get_tagPenalties_7() const { return ___tagPenalties_7; }
	inline Int32U5BU5D_t3230847821** get_address_of_tagPenalties_7() { return &___tagPenalties_7; }
	inline void set_tagPenalties_7(Int32U5BU5D_t3230847821* value)
	{
		___tagPenalties_7 = value;
		Il2CppCodeGenWriteBarrier(&___tagPenalties_7, value);
	}

	inline static int32_t get_offset_of_pathCallback_8() { return static_cast<int32_t>(offsetof(Seeker_t2472610117, ___pathCallback_8)); }
	inline OnPathDelegate_t598607977 * get_pathCallback_8() const { return ___pathCallback_8; }
	inline OnPathDelegate_t598607977 ** get_address_of_pathCallback_8() { return &___pathCallback_8; }
	inline void set_pathCallback_8(OnPathDelegate_t598607977 * value)
	{
		___pathCallback_8 = value;
		Il2CppCodeGenWriteBarrier(&___pathCallback_8, value);
	}

	inline static int32_t get_offset_of_preProcessPath_9() { return static_cast<int32_t>(offsetof(Seeker_t2472610117, ___preProcessPath_9)); }
	inline OnPathDelegate_t598607977 * get_preProcessPath_9() const { return ___preProcessPath_9; }
	inline OnPathDelegate_t598607977 ** get_address_of_preProcessPath_9() { return &___preProcessPath_9; }
	inline void set_preProcessPath_9(OnPathDelegate_t598607977 * value)
	{
		___preProcessPath_9 = value;
		Il2CppCodeGenWriteBarrier(&___preProcessPath_9, value);
	}

	inline static int32_t get_offset_of_postProcessOriginalPath_10() { return static_cast<int32_t>(offsetof(Seeker_t2472610117, ___postProcessOriginalPath_10)); }
	inline OnPathDelegate_t598607977 * get_postProcessOriginalPath_10() const { return ___postProcessOriginalPath_10; }
	inline OnPathDelegate_t598607977 ** get_address_of_postProcessOriginalPath_10() { return &___postProcessOriginalPath_10; }
	inline void set_postProcessOriginalPath_10(OnPathDelegate_t598607977 * value)
	{
		___postProcessOriginalPath_10 = value;
		Il2CppCodeGenWriteBarrier(&___postProcessOriginalPath_10, value);
	}

	inline static int32_t get_offset_of_postProcessPath_11() { return static_cast<int32_t>(offsetof(Seeker_t2472610117, ___postProcessPath_11)); }
	inline OnPathDelegate_t598607977 * get_postProcessPath_11() const { return ___postProcessPath_11; }
	inline OnPathDelegate_t598607977 ** get_address_of_postProcessPath_11() { return &___postProcessPath_11; }
	inline void set_postProcessPath_11(OnPathDelegate_t598607977 * value)
	{
		___postProcessPath_11 = value;
		Il2CppCodeGenWriteBarrier(&___postProcessPath_11, value);
	}

	inline static int32_t get_offset_of_lastCompletedVectorPath_12() { return static_cast<int32_t>(offsetof(Seeker_t2472610117, ___lastCompletedVectorPath_12)); }
	inline List_1_t1355284822 * get_lastCompletedVectorPath_12() const { return ___lastCompletedVectorPath_12; }
	inline List_1_t1355284822 ** get_address_of_lastCompletedVectorPath_12() { return &___lastCompletedVectorPath_12; }
	inline void set_lastCompletedVectorPath_12(List_1_t1355284822 * value)
	{
		___lastCompletedVectorPath_12 = value;
		Il2CppCodeGenWriteBarrier(&___lastCompletedVectorPath_12, value);
	}

	inline static int32_t get_offset_of_lastCompletedNodePath_13() { return static_cast<int32_t>(offsetof(Seeker_t2472610117, ___lastCompletedNodePath_13)); }
	inline List_1_t1391797922 * get_lastCompletedNodePath_13() const { return ___lastCompletedNodePath_13; }
	inline List_1_t1391797922 ** get_address_of_lastCompletedNodePath_13() { return &___lastCompletedNodePath_13; }
	inline void set_lastCompletedNodePath_13(List_1_t1391797922 * value)
	{
		___lastCompletedNodePath_13 = value;
		Il2CppCodeGenWriteBarrier(&___lastCompletedNodePath_13, value);
	}

	inline static int32_t get_offset_of_path_14() { return static_cast<int32_t>(offsetof(Seeker_t2472610117, ___path_14)); }
	inline Path_t1974241691 * get_path_14() const { return ___path_14; }
	inline Path_t1974241691 ** get_address_of_path_14() { return &___path_14; }
	inline void set_path_14(Path_t1974241691 * value)
	{
		___path_14 = value;
		Il2CppCodeGenWriteBarrier(&___path_14, value);
	}

	inline static int32_t get_offset_of_prevPath_15() { return static_cast<int32_t>(offsetof(Seeker_t2472610117, ___prevPath_15)); }
	inline Path_t1974241691 * get_prevPath_15() const { return ___prevPath_15; }
	inline Path_t1974241691 ** get_address_of_prevPath_15() { return &___prevPath_15; }
	inline void set_prevPath_15(Path_t1974241691 * value)
	{
		___prevPath_15 = value;
		Il2CppCodeGenWriteBarrier(&___prevPath_15, value);
	}

	inline static int32_t get_offset_of_startHint_16() { return static_cast<int32_t>(offsetof(Seeker_t2472610117, ___startHint_16)); }
	inline GraphNode_t23612370 * get_startHint_16() const { return ___startHint_16; }
	inline GraphNode_t23612370 ** get_address_of_startHint_16() { return &___startHint_16; }
	inline void set_startHint_16(GraphNode_t23612370 * value)
	{
		___startHint_16 = value;
		Il2CppCodeGenWriteBarrier(&___startHint_16, value);
	}

	inline static int32_t get_offset_of_endHint_17() { return static_cast<int32_t>(offsetof(Seeker_t2472610117, ___endHint_17)); }
	inline GraphNode_t23612370 * get_endHint_17() const { return ___endHint_17; }
	inline GraphNode_t23612370 ** get_address_of_endHint_17() { return &___endHint_17; }
	inline void set_endHint_17(GraphNode_t23612370 * value)
	{
		___endHint_17 = value;
		Il2CppCodeGenWriteBarrier(&___endHint_17, value);
	}

	inline static int32_t get_offset_of_onPathDelegate_18() { return static_cast<int32_t>(offsetof(Seeker_t2472610117, ___onPathDelegate_18)); }
	inline OnPathDelegate_t598607977 * get_onPathDelegate_18() const { return ___onPathDelegate_18; }
	inline OnPathDelegate_t598607977 ** get_address_of_onPathDelegate_18() { return &___onPathDelegate_18; }
	inline void set_onPathDelegate_18(OnPathDelegate_t598607977 * value)
	{
		___onPathDelegate_18 = value;
		Il2CppCodeGenWriteBarrier(&___onPathDelegate_18, value);
	}

	inline static int32_t get_offset_of_onPartialPathDelegate_19() { return static_cast<int32_t>(offsetof(Seeker_t2472610117, ___onPartialPathDelegate_19)); }
	inline OnPathDelegate_t598607977 * get_onPartialPathDelegate_19() const { return ___onPartialPathDelegate_19; }
	inline OnPathDelegate_t598607977 ** get_address_of_onPartialPathDelegate_19() { return &___onPartialPathDelegate_19; }
	inline void set_onPartialPathDelegate_19(OnPathDelegate_t598607977 * value)
	{
		___onPartialPathDelegate_19 = value;
		Il2CppCodeGenWriteBarrier(&___onPartialPathDelegate_19, value);
	}

	inline static int32_t get_offset_of_tmpPathCallback_20() { return static_cast<int32_t>(offsetof(Seeker_t2472610117, ___tmpPathCallback_20)); }
	inline OnPathDelegate_t598607977 * get_tmpPathCallback_20() const { return ___tmpPathCallback_20; }
	inline OnPathDelegate_t598607977 ** get_address_of_tmpPathCallback_20() { return &___tmpPathCallback_20; }
	inline void set_tmpPathCallback_20(OnPathDelegate_t598607977 * value)
	{
		___tmpPathCallback_20 = value;
		Il2CppCodeGenWriteBarrier(&___tmpPathCallback_20, value);
	}

	inline static int32_t get_offset_of_lastPathID_21() { return static_cast<int32_t>(offsetof(Seeker_t2472610117, ___lastPathID_21)); }
	inline uint32_t get_lastPathID_21() const { return ___lastPathID_21; }
	inline uint32_t* get_address_of_lastPathID_21() { return &___lastPathID_21; }
	inline void set_lastPathID_21(uint32_t value)
	{
		___lastPathID_21 = value;
	}

	inline static int32_t get_offset_of_modifiers_22() { return static_cast<int32_t>(offsetof(Seeker_t2472610117, ___modifiers_22)); }
	inline List_1_t2456908887 * get_modifiers_22() const { return ___modifiers_22; }
	inline List_1_t2456908887 ** get_address_of_modifiers_22() { return &___modifiers_22; }
	inline void set_modifiers_22(List_1_t2456908887 * value)
	{
		___modifiers_22 = value;
		Il2CppCodeGenWriteBarrier(&___modifiers_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
