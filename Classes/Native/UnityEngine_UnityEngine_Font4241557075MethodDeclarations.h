﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Font
struct Font_t4241557075;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Action`1<UnityEngine.Font>
struct Action_1_t342405915;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.CharacterInfo[]
struct CharacterInfoU5BU5D_t4214273472;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Font4241557075.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_FontStyle3350479768.h"
#include "UnityEngine_UnityEngine_CharacterInfo2481726445.h"

// System.Void UnityEngine.Font::.ctor()
extern "C"  void Font__ctor_m4065512190 (Font_t4241557075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Font::.ctor(System.String)
extern "C"  void Font__ctor_m3564445892 (Font_t4241557075 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Font::.ctor(System.String[],System.Int32)
extern "C"  void Font__ctor_m3509589301 (Font_t4241557075 * __this, StringU5BU5D_t4054002952* ___names0, int32_t ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Font::add_textureRebuilt(System.Action`1<UnityEngine.Font>)
extern "C"  void Font_add_textureRebuilt_m3323945916 (Il2CppObject * __this /* static, unused */, Action_1_t342405915 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Font::remove_textureRebuilt(System.Action`1<UnityEngine.Font>)
extern "C"  void Font_remove_textureRebuilt_m2804643593 (Il2CppObject * __this /* static, unused */, Action_1_t342405915 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UnityEngine.Font::GetOSInstalledFontNames()
extern "C"  StringU5BU5D_t4054002952* Font_GetOSInstalledFontNames_m1206366452 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Font::Internal_CreateFont(UnityEngine.Font,System.String)
extern "C"  void Font_Internal_CreateFont_m873871775 (Il2CppObject * __this /* static, unused */, Font_t4241557075 * ____font0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Font::Internal_CreateDynamicFont(UnityEngine.Font,System.String[],System.Int32)
extern "C"  void Font_Internal_CreateDynamicFont_m1337986509 (Il2CppObject * __this /* static, unused */, Font_t4241557075 * ____font0, StringU5BU5D_t4054002952* ____names1, int32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Font UnityEngine.Font::CreateDynamicFontFromOSFont(System.String,System.Int32)
extern "C"  Font_t4241557075 * Font_CreateDynamicFontFromOSFont_m3850697175 (Il2CppObject * __this /* static, unused */, String_t* ___fontname0, int32_t ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Font UnityEngine.Font::CreateDynamicFontFromOSFont(System.String[],System.Int32)
extern "C"  Font_t4241557075 * Font_CreateDynamicFontFromOSFont_m3023696761 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t4054002952* ___fontnames0, int32_t ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.Font::get_material()
extern "C"  Material_t3870600107 * Font_get_material_m2407307367 (Font_t4241557075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Font::set_material(UnityEngine.Material)
extern "C"  void Font_set_material_m1425248940 (Font_t4241557075 * __this, Material_t3870600107 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Font::HasCharacter(System.Char)
extern "C"  bool Font_HasCharacter_m2480770466 (Font_t4241557075 * __this, Il2CppChar ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UnityEngine.Font::get_fontNames()
extern "C"  StringU5BU5D_t4054002952* Font_get_fontNames_m2853508907 (Font_t4241557075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Font::set_fontNames(System.String[])
extern "C"  void Font_set_fontNames_m866407368 (Font_t4241557075 * __this, StringU5BU5D_t4054002952* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CharacterInfo[] UnityEngine.Font::get_characterInfo()
extern "C"  CharacterInfoU5BU5D_t4214273472* Font_get_characterInfo_m4037324889 (Font_t4241557075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Font::set_characterInfo(UnityEngine.CharacterInfo[])
extern "C"  void Font_set_characterInfo_m1606534618 (Font_t4241557075 * __this, CharacterInfoU5BU5D_t4214273472* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Font::RequestCharactersInTexture(System.String,System.Int32,UnityEngine.FontStyle)
extern "C"  void Font_RequestCharactersInTexture_m2452368057 (Font_t4241557075 * __this, String_t* ___characters0, int32_t ___size1, int32_t ___style2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Font::RequestCharactersInTexture(System.String,System.Int32)
extern "C"  void Font_RequestCharactersInTexture_m2556079800 (Font_t4241557075 * __this, String_t* ___characters0, int32_t ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Font::RequestCharactersInTexture(System.String)
extern "C"  void Font_RequestCharactersInTexture_m601617535 (Font_t4241557075 * __this, String_t* ___characters0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Font::InvokeTextureRebuilt_Internal(UnityEngine.Font)
extern "C"  void Font_InvokeTextureRebuilt_Internal_m1357223658 (Il2CppObject * __this /* static, unused */, Font_t4241557075 * ___font0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Font::GetMaxVertsForString(System.String)
extern "C"  int32_t Font_GetMaxVertsForString_m3070809654 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Font::GetCharacterInfo(System.Char,UnityEngine.CharacterInfo&,System.Int32,UnityEngine.FontStyle)
extern "C"  bool Font_GetCharacterInfo_m3124990824 (Font_t4241557075 * __this, Il2CppChar ___ch0, CharacterInfo_t2481726445 * ___info1, int32_t ___size2, int32_t ___style3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Font::GetCharacterInfo(System.Char,UnityEngine.CharacterInfo&,System.Int32)
extern "C"  bool Font_GetCharacterInfo_m3989947559 (Font_t4241557075 * __this, Il2CppChar ___ch0, CharacterInfo_t2481726445 * ___info1, int32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Font::GetCharacterInfo(System.Char,UnityEngine.CharacterInfo&)
extern "C"  bool Font_GetCharacterInfo_m2173494832 (Font_t4241557075 * __this, Il2CppChar ___ch0, CharacterInfo_t2481726445 * ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Font::get_dynamic()
extern "C"  bool Font_get_dynamic_m3880144684 (Font_t4241557075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Font::get_ascent()
extern "C"  int32_t Font_get_ascent_m2144661577 (Font_t4241557075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Font::get_lineHeight()
extern "C"  int32_t Font_get_lineHeight_m2690133610 (Font_t4241557075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Font::get_fontSize()
extern "C"  int32_t Font_get_fontSize_m3025810271 (Font_t4241557075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
