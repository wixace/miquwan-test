﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.PathModifier
struct PathModifier_t2857972114;
// Seeker
struct Seeker_t2472610117;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.IPathModifier
struct IPathModifier_t1088723335;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Seeker2472610117.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"

// System.Void Pathfinding.PathModifier::.ctor()
extern "C"  void PathModifier__ctor_m2431809813 (PathModifier_t2857972114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.PathModifier::get_Priority()
extern "C"  int32_t PathModifier_get_Priority_m2579558602 (PathModifier_t2857972114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PathModifier::set_Priority(System.Int32)
extern "C"  void PathModifier_set_Priority_m917362049 (PathModifier_t2857972114 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PathModifier::Awake(Seeker)
extern "C"  void PathModifier_Awake_m70840979 (PathModifier_t2857972114 * __this, Seeker_t2472610117 * ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PathModifier::OnDestroy(Seeker)
extern "C"  void PathModifier_OnDestroy_m94698281 (PathModifier_t2857972114 * __this, Seeker_t2472610117 * ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PathModifier::ApplyOriginal(Pathfinding.Path)
extern "C"  void PathModifier_ApplyOriginal_m2304348003 (PathModifier_t2857972114 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PathModifier::PreProcess(Pathfinding.Path)
extern "C"  void PathModifier_PreProcess_m1296570348 (PathModifier_t2857972114 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PathModifier::ilo_RegisterModifier1(Seeker,Pathfinding.IPathModifier)
extern "C"  void PathModifier_ilo_RegisterModifier1_m3275034659 (Il2CppObject * __this /* static, unused */, Seeker_t2472610117 * ____this0, Il2CppObject * ___mod1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
