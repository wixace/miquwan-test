﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSEngine
struct JSEngine_t2847479019;
// System.Action
struct Action_t3771233898;
// JSAssetMgr
struct JSAssetMgr_t2634428785;
// System.Object
struct Il2CppObject;
// IZUpdate
struct IZUpdate_t3482043738;
// JSFileLoader
struct JSFileLoader_t1433707224;
// JSMgr/OnInitJSEngine
struct OnInitJSEngine_t2416054170;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSEngine2847479019.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_JSAssetMgr2634428785.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSFileLoader1433707224.h"
#include "AssemblyU2DCSharp_JSMgr_OnInitJSEngine2416054170.h"

// System.Void JSEngine::.ctor()
extern "C"  void JSEngine__ctor_m992543248 (JSEngine_t2847479019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSEngine::.cctor()
extern "C"  void JSEngine__cctor_m221973405 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSEngine::get_initSuccess()
extern "C"  bool JSEngine_get_initSuccess_m1987649220 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSEngine::set_initSuccess(System.Boolean)
extern "C"  void JSEngine_set_initSuccess_m3565085819 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSEngine::get_initFail()
extern "C"  bool JSEngine_get_initFail_m2990099999 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSEngine::set_initFail(System.Boolean)
extern "C"  void JSEngine_set_initFail_m1763734038 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSEngine::OnInitJSEngine(System.Boolean)
extern "C"  void JSEngine_OnInitJSEngine_m435790181 (JSEngine_t2847479019 * __this, bool ___bSuccess0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSEngine::OnInitJSEngine()
extern "C"  void JSEngine_OnInitJSEngine_m445793390 (JSEngine_t2847479019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSEngine::ZUpdate()
extern "C"  void JSEngine_ZUpdate_m4214818065 (JSEngine_t2847479019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSEngine::get_LoadProgress()
extern "C"  float JSEngine_get_LoadProgress_m2806998012 (JSEngine_t2847479019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSEngine::get_IsLoadSuccess()
extern "C"  bool JSEngine_get_IsLoadSuccess_m783782532 (JSEngine_t2847479019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSEngine::LoadScriptsSuccess()
extern "C"  void JSEngine_LoadScriptsSuccess_m1724685333 (JSEngine_t2847479019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSEngine::FirstInit(JSEngine)
extern "C"  void JSEngine_FirstInit_m4013832195 (Il2CppObject * __this /* static, unused */, JSEngine_t2847479019 * ___jse0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSEngine::OnAwake()
extern "C"  void JSEngine_OnAwake_m728284500 (JSEngine_t2847479019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSEngine::Update()
extern "C"  void JSEngine_Update_m2430931709 (JSEngine_t2847479019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSEngine::DoThreadSafeAction(System.Action)
extern "C"  void JSEngine_DoThreadSafeAction_m1194308241 (JSEngine_t2847479019 * __this, Action_t3771233898 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSEngine::UpdateThreadSafeActions()
extern "C"  void JSEngine_UpdateThreadSafeActions_m2619098411 (JSEngine_t2847479019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSEngine::LateUpdate()
extern "C"  void JSEngine_LateUpdate_m1414206019 (JSEngine_t2847479019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSEngine::OnDestroy()
extern "C"  void JSEngine_OnDestroy_m730550025 (JSEngine_t2847479019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSEngine::ilo_get_isLoaded1(JSAssetMgr)
extern "C"  bool JSEngine_ilo_get_isLoaded1_m3327436623 (Il2CppObject * __this /* static, unused */, JSAssetMgr_t2634428785 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSEngine::ilo_Debug2(System.Object,System.Boolean,System.Int32)
extern "C"  void JSEngine_ilo_Debug2_m444172328 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, int32_t ___user2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSEngine::ilo_AddUpdate3(IZUpdate)
extern "C"  void JSEngine_ilo_AddUpdate3_m1958592630 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSEngine::ilo_set_initFail4(System.Boolean)
extern "C"  void JSEngine_ilo_set_initFail4_m663265147 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSEngine::ilo_get_initSuccess5()
extern "C"  bool JSEngine_ilo_get_initSuccess5_m257214086 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSEngine::ilo_RemoveUpdate6(IZUpdate)
extern "C"  void JSEngine_ilo_RemoveUpdate6_m3691682090 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSEngine::ilo_set_initSuccess7(System.Boolean)
extern "C"  void JSEngine_ilo_set_initSuccess7_m2280980543 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSEngine::ilo_InitJSEngine8(JSFileLoader,JSMgr/OnInitJSEngine)
extern "C"  bool JSEngine_ilo_InitJSEngine8_m192038464 (Il2CppObject * __this /* static, unused */, JSFileLoader_t1433707224 * ___jsLoader0, OnInitJSEngine_t2416054170 * ___onInitJSEngine1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSEngine::ilo_UpdateThreadSafeActions9(JSEngine)
extern "C"  void JSEngine_ilo_UpdateThreadSafeActions9_m3469238648 (Il2CppObject * __this /* static, unused */, JSEngine_t2847479019 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSEngine::ilo_gc10()
extern "C"  void JSEngine_ilo_gc10_m2804998338 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
