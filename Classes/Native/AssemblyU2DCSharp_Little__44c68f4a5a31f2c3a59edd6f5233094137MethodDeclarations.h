﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._44c68f4a5a31f2c3a59edd6f5f68868c
struct _44c68f4a5a31f2c3a59edd6f5f68868c_t233094137;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._44c68f4a5a31f2c3a59edd6f5f68868c::.ctor()
extern "C"  void _44c68f4a5a31f2c3a59edd6f5f68868c__ctor_m1332728404 (_44c68f4a5a31f2c3a59edd6f5f68868c_t233094137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._44c68f4a5a31f2c3a59edd6f5f68868c::_44c68f4a5a31f2c3a59edd6f5f68868cm2(System.Int32)
extern "C"  int32_t _44c68f4a5a31f2c3a59edd6f5f68868c__44c68f4a5a31f2c3a59edd6f5f68868cm2_m416564985 (_44c68f4a5a31f2c3a59edd6f5f68868c_t233094137 * __this, int32_t ____44c68f4a5a31f2c3a59edd6f5f68868ca0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._44c68f4a5a31f2c3a59edd6f5f68868c::_44c68f4a5a31f2c3a59edd6f5f68868cm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _44c68f4a5a31f2c3a59edd6f5f68868c__44c68f4a5a31f2c3a59edd6f5f68868cm_m986000349 (_44c68f4a5a31f2c3a59edd6f5f68868c_t233094137 * __this, int32_t ____44c68f4a5a31f2c3a59edd6f5f68868ca0, int32_t ____44c68f4a5a31f2c3a59edd6f5f68868c221, int32_t ____44c68f4a5a31f2c3a59edd6f5f68868cc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
