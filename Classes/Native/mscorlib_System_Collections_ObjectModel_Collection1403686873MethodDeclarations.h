﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_Collection3356274529MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::.ctor()
#define Collection_1__ctor_m3452078379(__this, method) ((  void (*) (Collection_1_t1403686873 *, const MethodInfo*))Collection_1__ctor_m1690372513_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1164338242(__this, method) ((  bool (*) (Collection_1_t1403686873 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1624016570_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m2199761423(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1403686873 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1285013187_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2635226590(__this, method) ((  Il2CppObject * (*) (Collection_1_t1403686873 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2828471038_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m2827493151(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1403686873 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1708617267_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m4216430977(__this, ___value0, method) ((  bool (*) (Collection_1_t1403686873 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m504494585_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m2320870903(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1403686873 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1652511499_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m1961362666(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1403686873 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2187188150_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m712241150(__this, ___value0, method) ((  void (*) (Collection_1_t1403686873 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2625864114_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1076673083(__this, method) ((  bool (*) (Collection_1_t1403686873 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1302589123_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1478750189(__this, method) ((  Il2CppObject * (*) (Collection_1_t1403686873 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1873829551_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m3113010608(__this, method) ((  bool (*) (Collection_1_t1403686873 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2813777960_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3463922505(__this, method) ((  bool (*) (Collection_1_t1403686873 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1376059857_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m510368308(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1403686873 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2224513142_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m4287798849(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1403686873 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2262756877_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::Add(T)
#define Collection_1_Add_m3187894538(__this, ___item0, method) ((  void (*) (Collection_1_t1403686873 *, LinkedResource_t2218228715 *, const MethodInfo*))Collection_1_Add_m321765054_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::Clear()
#define Collection_1_Clear_m2047173888(__this, method) ((  void (*) (Collection_1_t1403686873 *, const MethodInfo*))Collection_1_Clear_m3391473100_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::ClearItems()
#define Collection_1_ClearItems_m1439187308(__this, method) ((  void (*) (Collection_1_t1403686873 *, const MethodInfo*))Collection_1_ClearItems_m2738199222_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::Contains(T)
#define Collection_1_Contains_m431401714(__this, ___item0, method) ((  bool (*) (Collection_1_t1403686873 *, LinkedResource_t2218228715 *, const MethodInfo*))Collection_1_Contains_m1050871674_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m455664250(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1403686873 *, LinkedResourceU5BU5D_t4016027658*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1746187054_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::GetEnumerator()
#define Collection_1_GetEnumerator_m194743296(__this, method) ((  Il2CppObject* (*) (Collection_1_t1403686873 *, const MethodInfo*))Collection_1_GetEnumerator_m625631581_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::IndexOf(T)
#define Collection_1_IndexOf_m3840354246(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1403686873 *, LinkedResource_t2218228715 *, const MethodInfo*))Collection_1_IndexOf_m3101447730_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::Insert(System.Int32,T)
#define Collection_1_Insert_m1756450673(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1403686873 *, int32_t, LinkedResource_t2218228715 *, const MethodInfo*))Collection_1_Insert_m1208073509_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m3013468737(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1403686873 *, int32_t, LinkedResource_t2218228715 *, const MethodInfo*))Collection_1_InsertItem_m714854616_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::get_Items()
#define Collection_1_get_Items_m1640979904(__this, method) ((  Il2CppObject* (*) (Collection_1_t1403686873 *, const MethodInfo*))Collection_1_get_Items_m1477178336_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::Remove(T)
#define Collection_1_Remove_m169705325(__this, ___item0, method) ((  bool (*) (Collection_1_t1403686873 *, LinkedResource_t2218228715 *, const MethodInfo*))Collection_1_Remove_m2181520885_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m3925270839(__this, ___index0, method) ((  void (*) (Collection_1_t1403686873 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3376893675_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m812927489(__this, ___index0, method) ((  void (*) (Collection_1_t1403686873 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1099170891_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::get_Count()
#define Collection_1_get_Count_m1357188063(__this, method) ((  int32_t (*) (Collection_1_t1403686873 *, const MethodInfo*))Collection_1_get_Count_m1472906633_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::get_Item(System.Int32)
#define Collection_1_get_Item_m3390534621(__this, ___index0, method) ((  LinkedResource_t2218228715 * (*) (Collection_1_t1403686873 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2356360623_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m1836546056(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1403686873 *, int32_t, LinkedResource_t2218228715 *, const MethodInfo*))Collection_1_set_Item_m3127068860_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m3190757032(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1403686873 *, int32_t, LinkedResource_t2218228715 *, const MethodInfo*))Collection_1_SetItem_m112162877_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m1342457082(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1993492338_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m2592730172(__this /* static, unused */, ___item0, method) ((  LinkedResource_t2218228715 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1655469326_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m579916090(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m651250670_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m112800138(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2469749778_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m1946915157(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3893865421_gshared)(__this /* static, unused */, ___list0, method)
