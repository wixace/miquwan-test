﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RVO.ObstacleVertex
struct ObstacleVertex_t4170307099;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.RVO.ObstacleVertex::.ctor()
extern "C"  void ObstacleVertex__ctor_m2386090779 (ObstacleVertex_t4170307099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
