﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._e02bd93ba135557cd22a7b30699c821d
struct _e02bd93ba135557cd22a7b30699c821d_t3321154965;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__e02bd93ba135557cd22a7b303321154965.h"

// System.Void Little._e02bd93ba135557cd22a7b30699c821d::.ctor()
extern "C"  void _e02bd93ba135557cd22a7b30699c821d__ctor_m1722011704 (_e02bd93ba135557cd22a7b30699c821d_t3321154965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e02bd93ba135557cd22a7b30699c821d::_e02bd93ba135557cd22a7b30699c821dm2(System.Int32)
extern "C"  int32_t _e02bd93ba135557cd22a7b30699c821d__e02bd93ba135557cd22a7b30699c821dm2_m1616118137 (_e02bd93ba135557cd22a7b30699c821d_t3321154965 * __this, int32_t ____e02bd93ba135557cd22a7b30699c821da0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e02bd93ba135557cd22a7b30699c821d::_e02bd93ba135557cd22a7b30699c821dm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _e02bd93ba135557cd22a7b30699c821d__e02bd93ba135557cd22a7b30699c821dm_m3811852637 (_e02bd93ba135557cd22a7b30699c821d_t3321154965 * __this, int32_t ____e02bd93ba135557cd22a7b30699c821da0, int32_t ____e02bd93ba135557cd22a7b30699c821d531, int32_t ____e02bd93ba135557cd22a7b30699c821dc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e02bd93ba135557cd22a7b30699c821d::ilo__e02bd93ba135557cd22a7b30699c821dm21(Little._e02bd93ba135557cd22a7b30699c821d,System.Int32)
extern "C"  int32_t _e02bd93ba135557cd22a7b30699c821d_ilo__e02bd93ba135557cd22a7b30699c821dm21_m283796732 (Il2CppObject * __this /* static, unused */, _e02bd93ba135557cd22a7b30699c821d_t3321154965 * ____this0, int32_t ____e02bd93ba135557cd22a7b30699c821da1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
