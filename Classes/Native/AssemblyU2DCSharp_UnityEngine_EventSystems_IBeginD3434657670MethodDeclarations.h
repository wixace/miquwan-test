﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_IBeginDragHandlerGenerated
struct UnityEngine_EventSystems_IBeginDragHandlerGenerated_t3434657670;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_EventSystems_IBeginDragHandlerGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_IBeginDragHandlerGenerated__ctor_m3448689317 (UnityEngine_EventSystems_IBeginDragHandlerGenerated_t3434657670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_IBeginDragHandlerGenerated::IBeginDragHandler_OnBeginDrag__PointerEventData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_IBeginDragHandlerGenerated_IBeginDragHandler_OnBeginDrag__PointerEventData_m976602755 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_IBeginDragHandlerGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_IBeginDragHandlerGenerated___Register_m1410697538 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
