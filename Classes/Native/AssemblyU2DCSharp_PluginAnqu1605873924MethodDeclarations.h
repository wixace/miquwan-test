﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginAnqu
struct PluginAnqu_t1605873924;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Char[]
struct CharU5BU5D_t3324145743;
// VersionMgr
struct VersionMgr_t1322950208;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// System.Object
struct Il2CppObject;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginAnqu1605873924.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"

// System.Void PluginAnqu::.ctor()
extern "C"  void PluginAnqu__ctor_m2454768663 (PluginAnqu_t1605873924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::Init()
extern "C"  void PluginAnqu_Init_m2386027357 (PluginAnqu_t1605873924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginAnqu::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginAnqu_ReqSDKHttpLogin_m889421868 (PluginAnqu_t1605873924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginAnqu::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginAnqu_IsLoginSuccess_m3645941340 (PluginAnqu_t1605873924 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::OpenUserLogin()
extern "C"  void PluginAnqu_OpenUserLogin_m1453582185 (PluginAnqu_t1605873924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::RoleEnterGame(CEvent.ZEvent)
extern "C"  void PluginAnqu_RoleEnterGame_m4250848210 (PluginAnqu_t1605873924 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::UserPay(CEvent.ZEvent)
extern "C"  void PluginAnqu_UserPay_m473510889 (PluginAnqu_t1605873924 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::OnInitSuccess(System.String)
extern "C"  void PluginAnqu_OnInitSuccess_m2595749881 (PluginAnqu_t1605873924 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::OnInitFail(System.String)
extern "C"  void PluginAnqu_OnInitFail_m647568360 (PluginAnqu_t1605873924 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::OnLoginSuccess(System.String)
extern "C"  void PluginAnqu_OnLoginSuccess_m3548547036 (PluginAnqu_t1605873924 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::OnLoginFail(System.String)
extern "C"  void PluginAnqu_OnLoginFail_m1524586213 (PluginAnqu_t1605873924 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::OnUserSwitchSuccess(System.String)
extern "C"  void PluginAnqu_OnUserSwitchSuccess_m3336862792 (PluginAnqu_t1605873924 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::OnUserSwitchFail(System.String)
extern "C"  void PluginAnqu_OnUserSwitchFail_m1350277625 (PluginAnqu_t1605873924 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::OnPaySuccess(System.String)
extern "C"  void PluginAnqu_OnPaySuccess_m232861979 (PluginAnqu_t1605873924 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::OnPayFail(System.String)
extern "C"  void PluginAnqu_OnPayFail_m232279558 (PluginAnqu_t1605873924 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::OnExitGameSuccess(System.String)
extern "C"  void PluginAnqu_OnExitGameSuccess_m3704618905 (PluginAnqu_t1605873924 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::OnExitGameFail(System.String)
extern "C"  void PluginAnqu_OnExitGameFail_m4150935624 (PluginAnqu_t1605873924 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::OnLogoutSuccess(System.String)
extern "C"  void PluginAnqu_OnLogoutSuccess_m2634318387 (PluginAnqu_t1605873924 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::OnLogoutFail(System.String)
extern "C"  void PluginAnqu_OnLogoutFail_m4125381614 (PluginAnqu_t1605873924 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::initAnqu(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginAnqu_initAnqu_m2945081792 (PluginAnqu_t1605873924 * __this, String_t* ___appid0, String_t* ___appkey1, String_t* ___channelid2, String_t* ___privatekey3, String_t* ___cpid4, String_t* ___curversion5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::loginAnqu()
extern "C"  void PluginAnqu_loginAnqu_m225737455 (PluginAnqu_t1605873924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::logoutAnqu()
extern "C"  void PluginAnqu_logoutAnqu_m784759336 (PluginAnqu_t1605873924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::uploadExtinfo(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginAnqu_uploadExtinfo_m1019560687 (PluginAnqu_t1605873924 * __this, String_t* ___serverid0, String_t* ___servername1, String_t* ___roleid2, String_t* ___rolename3, String_t* ___level4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginAnqu_pay_m734967945 (PluginAnqu_t1605873924 * __this, String_t* ___money0, String_t* ___subject1, String_t* ___body2, String_t* ___orderid3, String_t* ___pid4, String_t* ___rname5, String_t* ___servername6, String_t* ___mpext7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::<OnLogoutSuccess>m__40D()
extern "C"  void PluginAnqu_U3COnLogoutSuccessU3Em__40D_m2661442320 (PluginAnqu_t1605873924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] PluginAnqu::ilo_SplitStr1(System.String,System.Char[])
extern "C"  StringU5BU5D_t4054002952* PluginAnqu_ilo_SplitStr1_m3615155836 (Il2CppObject * __this /* static, unused */, String_t* ___sFile0, CharU5BU5D_t3324145743* ___separator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginAnqu::ilo_GetMD52(System.String)
extern "C"  String_t* PluginAnqu_ilo_GetMD52_m4107188409 (Il2CppObject * __this /* static, unused */, String_t* ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginAnqu::ilo_get_isSdkLogin3(VersionMgr)
extern "C"  bool PluginAnqu_ilo_get_isSdkLogin3_m599464863 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginAnqu::ilo_get_Instance4()
extern "C"  VersionMgr_t1322950208 * PluginAnqu_ilo_get_Instance4_m562269915 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginAnqu::ilo_get_ProductsCfgMgr5()
extern "C"  ProductsCfgMgr_t2493714872 * PluginAnqu_ilo_get_ProductsCfgMgr5_m2519048817 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::ilo_pay6(PluginAnqu,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginAnqu_ilo_pay6_m3624266826 (Il2CppObject * __this /* static, unused */, PluginAnqu_t1605873924 * ____this0, String_t* ___money1, String_t* ___subject2, String_t* ___body3, String_t* ___orderid4, String_t* ___pid5, String_t* ___rname6, String_t* ___servername7, String_t* ___mpext8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::ilo_Log7(System.Object,System.Boolean)
extern "C"  void PluginAnqu_ilo_Log7_m1579583964 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::ilo_ReqSDKHttpLogin8(PluginsSdkMgr)
extern "C"  void PluginAnqu_ilo_ReqSDKHttpLogin8_m3072309185 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::ilo_DispatchEvent9(CEvent.ZEvent)
extern "C"  void PluginAnqu_ilo_DispatchEvent9_m1226403810 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAnqu::ilo_OpenUserLogin10(PluginAnqu)
extern "C"  void PluginAnqu_ilo_OpenUserLogin10_m3160910961 (Il2CppObject * __this /* static, unused */, PluginAnqu_t1605873924 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
