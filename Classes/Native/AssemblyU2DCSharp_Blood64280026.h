﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;
// UISprite
struct UISprite_t661437049;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Blood
struct  Blood_t64280026  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean Blood::isHide
	bool ___isHide_2;
	// UnityEngine.Transform Blood::targetTf
	Transform_t1659122786 * ___targetTf_3;
	// UnityEngine.Transform Blood::mTf
	Transform_t1659122786 * ___mTf_4;
	// System.Boolean Blood::isvisible
	bool ___isvisible_6;
	// UISprite Blood::hp_sp
	UISprite_t661437049 * ___hp_sp_7;
	// UISprite Blood::dutytype_sp
	UISprite_t661437049 * ___dutytype_sp_8;
	// System.Single Blood::fiveSecond
	float ___fiveSecond_9;
	// UnityEngine.Transform Blood::Obj_HP
	Transform_t1659122786 * ___Obj_HP_10;
	// UnityEngine.Transform Blood::focusTr
	Transform_t1659122786 * ___focusTr_11;

public:
	inline static int32_t get_offset_of_isHide_2() { return static_cast<int32_t>(offsetof(Blood_t64280026, ___isHide_2)); }
	inline bool get_isHide_2() const { return ___isHide_2; }
	inline bool* get_address_of_isHide_2() { return &___isHide_2; }
	inline void set_isHide_2(bool value)
	{
		___isHide_2 = value;
	}

	inline static int32_t get_offset_of_targetTf_3() { return static_cast<int32_t>(offsetof(Blood_t64280026, ___targetTf_3)); }
	inline Transform_t1659122786 * get_targetTf_3() const { return ___targetTf_3; }
	inline Transform_t1659122786 ** get_address_of_targetTf_3() { return &___targetTf_3; }
	inline void set_targetTf_3(Transform_t1659122786 * value)
	{
		___targetTf_3 = value;
		Il2CppCodeGenWriteBarrier(&___targetTf_3, value);
	}

	inline static int32_t get_offset_of_mTf_4() { return static_cast<int32_t>(offsetof(Blood_t64280026, ___mTf_4)); }
	inline Transform_t1659122786 * get_mTf_4() const { return ___mTf_4; }
	inline Transform_t1659122786 ** get_address_of_mTf_4() { return &___mTf_4; }
	inline void set_mTf_4(Transform_t1659122786 * value)
	{
		___mTf_4 = value;
		Il2CppCodeGenWriteBarrier(&___mTf_4, value);
	}

	inline static int32_t get_offset_of_isvisible_6() { return static_cast<int32_t>(offsetof(Blood_t64280026, ___isvisible_6)); }
	inline bool get_isvisible_6() const { return ___isvisible_6; }
	inline bool* get_address_of_isvisible_6() { return &___isvisible_6; }
	inline void set_isvisible_6(bool value)
	{
		___isvisible_6 = value;
	}

	inline static int32_t get_offset_of_hp_sp_7() { return static_cast<int32_t>(offsetof(Blood_t64280026, ___hp_sp_7)); }
	inline UISprite_t661437049 * get_hp_sp_7() const { return ___hp_sp_7; }
	inline UISprite_t661437049 ** get_address_of_hp_sp_7() { return &___hp_sp_7; }
	inline void set_hp_sp_7(UISprite_t661437049 * value)
	{
		___hp_sp_7 = value;
		Il2CppCodeGenWriteBarrier(&___hp_sp_7, value);
	}

	inline static int32_t get_offset_of_dutytype_sp_8() { return static_cast<int32_t>(offsetof(Blood_t64280026, ___dutytype_sp_8)); }
	inline UISprite_t661437049 * get_dutytype_sp_8() const { return ___dutytype_sp_8; }
	inline UISprite_t661437049 ** get_address_of_dutytype_sp_8() { return &___dutytype_sp_8; }
	inline void set_dutytype_sp_8(UISprite_t661437049 * value)
	{
		___dutytype_sp_8 = value;
		Il2CppCodeGenWriteBarrier(&___dutytype_sp_8, value);
	}

	inline static int32_t get_offset_of_fiveSecond_9() { return static_cast<int32_t>(offsetof(Blood_t64280026, ___fiveSecond_9)); }
	inline float get_fiveSecond_9() const { return ___fiveSecond_9; }
	inline float* get_address_of_fiveSecond_9() { return &___fiveSecond_9; }
	inline void set_fiveSecond_9(float value)
	{
		___fiveSecond_9 = value;
	}

	inline static int32_t get_offset_of_Obj_HP_10() { return static_cast<int32_t>(offsetof(Blood_t64280026, ___Obj_HP_10)); }
	inline Transform_t1659122786 * get_Obj_HP_10() const { return ___Obj_HP_10; }
	inline Transform_t1659122786 ** get_address_of_Obj_HP_10() { return &___Obj_HP_10; }
	inline void set_Obj_HP_10(Transform_t1659122786 * value)
	{
		___Obj_HP_10 = value;
		Il2CppCodeGenWriteBarrier(&___Obj_HP_10, value);
	}

	inline static int32_t get_offset_of_focusTr_11() { return static_cast<int32_t>(offsetof(Blood_t64280026, ___focusTr_11)); }
	inline Transform_t1659122786 * get_focusTr_11() const { return ___focusTr_11; }
	inline Transform_t1659122786 ** get_address_of_focusTr_11() { return &___focusTr_11; }
	inline void set_focusTr_11(Transform_t1659122786 * value)
	{
		___focusTr_11 = value;
		Il2CppCodeGenWriteBarrier(&___focusTr_11, value);
	}
};

struct Blood_t64280026_StaticFields
{
public:
	// UnityEngine.Vector3 Blood::offset
	Vector3_t4282066566  ___offset_5;

public:
	inline static int32_t get_offset_of_offset_5() { return static_cast<int32_t>(offsetof(Blood_t64280026_StaticFields, ___offset_5)); }
	inline Vector3_t4282066566  get_offset_5() const { return ___offset_5; }
	inline Vector3_t4282066566 * get_address_of_offset_5() { return &___offset_5; }
	inline void set_offset_5(Vector3_t4282066566  value)
	{
		___offset_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
