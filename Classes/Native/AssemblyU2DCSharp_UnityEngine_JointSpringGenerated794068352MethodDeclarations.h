﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_JointSpringGenerated
struct UnityEngine_JointSpringGenerated_t794068352;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_JointSpringGenerated::.ctor()
extern "C"  void UnityEngine_JointSpringGenerated__ctor_m1458544923 (UnityEngine_JointSpringGenerated_t794068352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointSpringGenerated::.cctor()
extern "C"  void UnityEngine_JointSpringGenerated__cctor_m1783123442 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_JointSpringGenerated::JointSpring_JointSpring1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_JointSpringGenerated_JointSpring_JointSpring1_m916965903 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointSpringGenerated::JointSpring_spring(JSVCall)
extern "C"  void UnityEngine_JointSpringGenerated_JointSpring_spring_m1291718217 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointSpringGenerated::JointSpring_damper(JSVCall)
extern "C"  void UnityEngine_JointSpringGenerated_JointSpring_damper_m2643328201 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointSpringGenerated::JointSpring_targetPosition(JSVCall)
extern "C"  void UnityEngine_JointSpringGenerated_JointSpring_targetPosition_m3180497180 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointSpringGenerated::__Register()
extern "C"  void UnityEngine_JointSpringGenerated___Register_m4133573772 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointSpringGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_JointSpringGenerated_ilo_addJSCSRel1_m3082159703 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointSpringGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void UnityEngine_JointSpringGenerated_ilo_setSingle2_m4190547450 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_JointSpringGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_JointSpringGenerated_ilo_getSingle3_m4043584366 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointSpringGenerated::ilo_changeJSObj4(System.Int32,System.Object)
extern "C"  void UnityEngine_JointSpringGenerated_ilo_changeJSObj4_m3869398705 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
