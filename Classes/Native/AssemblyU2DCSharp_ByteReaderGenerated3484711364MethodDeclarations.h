﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ByteReaderGenerated
struct ByteReaderGenerated_t3484711364;
// JSVCall
struct JSVCall_t3708497963;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void ByteReaderGenerated::.ctor()
extern "C"  void ByteReaderGenerated__ctor_m4221326887 (ByteReaderGenerated_t3484711364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ByteReaderGenerated::ByteReader_ByteReader1(JSVCall,System.Int32)
extern "C"  bool ByteReaderGenerated_ByteReader_ByteReader1_m1371729611 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ByteReaderGenerated::ByteReader_ByteReader2(JSVCall,System.Int32)
extern "C"  bool ByteReaderGenerated_ByteReader_ByteReader2_m2616494092 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteReaderGenerated::ByteReader_canRead(JSVCall)
extern "C"  void ByteReaderGenerated_ByteReader_canRead_m1496391896 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ByteReaderGenerated::ByteReader_ReadCSV(JSVCall,System.Int32)
extern "C"  bool ByteReaderGenerated_ByteReader_ReadCSV_m3144477749 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ByteReaderGenerated::ByteReader_ReadDictionary(JSVCall,System.Int32)
extern "C"  bool ByteReaderGenerated_ByteReader_ReadDictionary_m910633417 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ByteReaderGenerated::ByteReader_ReadLine__Boolean(JSVCall,System.Int32)
extern "C"  bool ByteReaderGenerated_ByteReader_ReadLine__Boolean_m4116875491 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ByteReaderGenerated::ByteReader_ReadLine(JSVCall,System.Int32)
extern "C"  bool ByteReaderGenerated_ByteReader_ReadLine_m3573420071 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ByteReaderGenerated::ByteReader_Open__String(JSVCall,System.Int32)
extern "C"  bool ByteReaderGenerated_ByteReader_Open__String_m4185643288 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteReaderGenerated::__Register()
extern "C"  void ByteReaderGenerated___Register_m1322772224 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ByteReaderGenerated::<ByteReader_ByteReader1>m__18()
extern "C"  ByteU5BU5D_t4260760469* ByteReaderGenerated_U3CByteReader_ByteReader1U3Em__18_m2043642579 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ByteReaderGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool ByteReaderGenerated_ilo_attachFinalizerObject1_m1301703344 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ByteReaderGenerated::ilo_getBooleanS2(System.Int32)
extern "C"  bool ByteReaderGenerated_ilo_getBooleanS2_m1290172190 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ByteReaderGenerated::ilo_getStringS3(System.Int32)
extern "C"  String_t* ByteReaderGenerated_ilo_getStringS3_m1327671309 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
