﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.FixedJoint
struct FixedJoint_t571739706;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.FixedJoint::.ctor()
extern "C"  void FixedJoint__ctor_m1609409079 (FixedJoint_t571739706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
