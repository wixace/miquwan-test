﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "System_System_Net_Sockets_AddressFamily3770679850.h"

// System.String iOSipv6::getIPv6(System.String,System.String)
extern "C"  String_t* iOSipv6_getIPv6_m1716917565 (Il2CppObject * __this /* static, unused */, String_t* ___host0, String_t* ___port1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String iOSipv6::GetIPv6(System.String,System.String)
extern "C"  String_t* iOSipv6_GetIPv6_m3731818845 (Il2CppObject * __this /* static, unused */, String_t* ___host0, String_t* ___port1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iOSipv6::getIPType(System.String,System.String,System.String&,System.Net.Sockets.AddressFamily&)
extern "C"  void iOSipv6_getIPType_m4179540682 (Il2CppObject * __this /* static, unused */, String_t* ___serverIp0, String_t* ___serverPorts1, String_t** ___newServerIp2, int32_t* ___mIPType3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
