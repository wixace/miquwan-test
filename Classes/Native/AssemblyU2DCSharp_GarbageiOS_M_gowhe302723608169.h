﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_gowhe30
struct  M_gowhe30_t2723608169  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_gowhe30::_tardrallke
	String_t* ____tardrallke_0;
	// System.Single GarbageiOS.M_gowhe30::_trajasrereQaircu
	float ____trajasrereQaircu_1;
	// System.Int32 GarbageiOS.M_gowhe30::_salchaLisla
	int32_t ____salchaLisla_2;
	// System.UInt32 GarbageiOS.M_gowhe30::_nete
	uint32_t ____nete_3;
	// System.Single GarbageiOS.M_gowhe30::_carpooKerqor
	float ____carpooKerqor_4;
	// System.Int32 GarbageiOS.M_gowhe30::_basgear
	int32_t ____basgear_5;
	// System.UInt32 GarbageiOS.M_gowhe30::_barje
	uint32_t ____barje_6;
	// System.Single GarbageiOS.M_gowhe30::_whortairCuchairsto
	float ____whortairCuchairsto_7;
	// System.Int32 GarbageiOS.M_gowhe30::_hetearlasBoofarbou
	int32_t ____hetearlasBoofarbou_8;
	// System.Int32 GarbageiOS.M_gowhe30::_kesosee
	int32_t ____kesosee_9;
	// System.UInt32 GarbageiOS.M_gowhe30::_drisyerlem
	uint32_t ____drisyerlem_10;
	// System.Int32 GarbageiOS.M_gowhe30::_dasi
	int32_t ____dasi_11;
	// System.String GarbageiOS.M_gowhe30::_raijugairDawga
	String_t* ____raijugairDawga_12;
	// System.UInt32 GarbageiOS.M_gowhe30::_melbalmas
	uint32_t ____melbalmas_13;

public:
	inline static int32_t get_offset_of__tardrallke_0() { return static_cast<int32_t>(offsetof(M_gowhe30_t2723608169, ____tardrallke_0)); }
	inline String_t* get__tardrallke_0() const { return ____tardrallke_0; }
	inline String_t** get_address_of__tardrallke_0() { return &____tardrallke_0; }
	inline void set__tardrallke_0(String_t* value)
	{
		____tardrallke_0 = value;
		Il2CppCodeGenWriteBarrier(&____tardrallke_0, value);
	}

	inline static int32_t get_offset_of__trajasrereQaircu_1() { return static_cast<int32_t>(offsetof(M_gowhe30_t2723608169, ____trajasrereQaircu_1)); }
	inline float get__trajasrereQaircu_1() const { return ____trajasrereQaircu_1; }
	inline float* get_address_of__trajasrereQaircu_1() { return &____trajasrereQaircu_1; }
	inline void set__trajasrereQaircu_1(float value)
	{
		____trajasrereQaircu_1 = value;
	}

	inline static int32_t get_offset_of__salchaLisla_2() { return static_cast<int32_t>(offsetof(M_gowhe30_t2723608169, ____salchaLisla_2)); }
	inline int32_t get__salchaLisla_2() const { return ____salchaLisla_2; }
	inline int32_t* get_address_of__salchaLisla_2() { return &____salchaLisla_2; }
	inline void set__salchaLisla_2(int32_t value)
	{
		____salchaLisla_2 = value;
	}

	inline static int32_t get_offset_of__nete_3() { return static_cast<int32_t>(offsetof(M_gowhe30_t2723608169, ____nete_3)); }
	inline uint32_t get__nete_3() const { return ____nete_3; }
	inline uint32_t* get_address_of__nete_3() { return &____nete_3; }
	inline void set__nete_3(uint32_t value)
	{
		____nete_3 = value;
	}

	inline static int32_t get_offset_of__carpooKerqor_4() { return static_cast<int32_t>(offsetof(M_gowhe30_t2723608169, ____carpooKerqor_4)); }
	inline float get__carpooKerqor_4() const { return ____carpooKerqor_4; }
	inline float* get_address_of__carpooKerqor_4() { return &____carpooKerqor_4; }
	inline void set__carpooKerqor_4(float value)
	{
		____carpooKerqor_4 = value;
	}

	inline static int32_t get_offset_of__basgear_5() { return static_cast<int32_t>(offsetof(M_gowhe30_t2723608169, ____basgear_5)); }
	inline int32_t get__basgear_5() const { return ____basgear_5; }
	inline int32_t* get_address_of__basgear_5() { return &____basgear_5; }
	inline void set__basgear_5(int32_t value)
	{
		____basgear_5 = value;
	}

	inline static int32_t get_offset_of__barje_6() { return static_cast<int32_t>(offsetof(M_gowhe30_t2723608169, ____barje_6)); }
	inline uint32_t get__barje_6() const { return ____barje_6; }
	inline uint32_t* get_address_of__barje_6() { return &____barje_6; }
	inline void set__barje_6(uint32_t value)
	{
		____barje_6 = value;
	}

	inline static int32_t get_offset_of__whortairCuchairsto_7() { return static_cast<int32_t>(offsetof(M_gowhe30_t2723608169, ____whortairCuchairsto_7)); }
	inline float get__whortairCuchairsto_7() const { return ____whortairCuchairsto_7; }
	inline float* get_address_of__whortairCuchairsto_7() { return &____whortairCuchairsto_7; }
	inline void set__whortairCuchairsto_7(float value)
	{
		____whortairCuchairsto_7 = value;
	}

	inline static int32_t get_offset_of__hetearlasBoofarbou_8() { return static_cast<int32_t>(offsetof(M_gowhe30_t2723608169, ____hetearlasBoofarbou_8)); }
	inline int32_t get__hetearlasBoofarbou_8() const { return ____hetearlasBoofarbou_8; }
	inline int32_t* get_address_of__hetearlasBoofarbou_8() { return &____hetearlasBoofarbou_8; }
	inline void set__hetearlasBoofarbou_8(int32_t value)
	{
		____hetearlasBoofarbou_8 = value;
	}

	inline static int32_t get_offset_of__kesosee_9() { return static_cast<int32_t>(offsetof(M_gowhe30_t2723608169, ____kesosee_9)); }
	inline int32_t get__kesosee_9() const { return ____kesosee_9; }
	inline int32_t* get_address_of__kesosee_9() { return &____kesosee_9; }
	inline void set__kesosee_9(int32_t value)
	{
		____kesosee_9 = value;
	}

	inline static int32_t get_offset_of__drisyerlem_10() { return static_cast<int32_t>(offsetof(M_gowhe30_t2723608169, ____drisyerlem_10)); }
	inline uint32_t get__drisyerlem_10() const { return ____drisyerlem_10; }
	inline uint32_t* get_address_of__drisyerlem_10() { return &____drisyerlem_10; }
	inline void set__drisyerlem_10(uint32_t value)
	{
		____drisyerlem_10 = value;
	}

	inline static int32_t get_offset_of__dasi_11() { return static_cast<int32_t>(offsetof(M_gowhe30_t2723608169, ____dasi_11)); }
	inline int32_t get__dasi_11() const { return ____dasi_11; }
	inline int32_t* get_address_of__dasi_11() { return &____dasi_11; }
	inline void set__dasi_11(int32_t value)
	{
		____dasi_11 = value;
	}

	inline static int32_t get_offset_of__raijugairDawga_12() { return static_cast<int32_t>(offsetof(M_gowhe30_t2723608169, ____raijugairDawga_12)); }
	inline String_t* get__raijugairDawga_12() const { return ____raijugairDawga_12; }
	inline String_t** get_address_of__raijugairDawga_12() { return &____raijugairDawga_12; }
	inline void set__raijugairDawga_12(String_t* value)
	{
		____raijugairDawga_12 = value;
		Il2CppCodeGenWriteBarrier(&____raijugairDawga_12, value);
	}

	inline static int32_t get_offset_of__melbalmas_13() { return static_cast<int32_t>(offsetof(M_gowhe30_t2723608169, ____melbalmas_13)); }
	inline uint32_t get__melbalmas_13() const { return ____melbalmas_13; }
	inline uint32_t* get_address_of__melbalmas_13() { return &____melbalmas_13; }
	inline void set__melbalmas_13(uint32_t value)
	{
		____melbalmas_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
