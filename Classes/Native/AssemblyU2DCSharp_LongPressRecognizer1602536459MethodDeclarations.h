﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LongPressRecognizer
struct LongPressRecognizer_t1602536459;
// System.String
struct String_t;
// LongPressGesture
struct LongPressGesture_t2876118082;
// FingerGestures/IFingerList
struct IFingerList_t1026994100;
// Gesture
struct Gesture_t1589572905;
// GestureRecognizer
struct GestureRecognizer_t3512875949;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LongPressGesture2876118082.h"
#include "AssemblyU2DCSharp_GestureRecognitionState3604239971.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_Gesture1589572905.h"
#include "AssemblyU2DCSharp_GestureRecognizer3512875949.h"

// System.Void LongPressRecognizer::.ctor()
extern "C"  void LongPressRecognizer__ctor_m819833792 (LongPressRecognizer_t1602536459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LongPressRecognizer::GetDefaultEventMessageName()
extern "C"  String_t* LongPressRecognizer_GetDefaultEventMessageName_m2224261412 (LongPressRecognizer_t1602536459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LongPressRecognizer::OnBegin(LongPressGesture,FingerGestures/IFingerList)
extern "C"  void LongPressRecognizer_OnBegin_m872283692 (LongPressRecognizer_t1602536459 * __this, LongPressGesture_t2876118082 * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GestureRecognitionState LongPressRecognizer::OnRecognize(LongPressGesture,FingerGestures/IFingerList)
extern "C"  int32_t LongPressRecognizer_OnRecognize_m3065439831 (LongPressRecognizer_t1602536459 * __this, LongPressGesture_t2876118082 * ___gesture0, Il2CppObject * ___touches1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 LongPressRecognizer::ilo_get_Position1(Gesture)
extern "C"  Vector2_t4282066565  LongPressRecognizer_ilo_get_Position1_m1275897060 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LongPressRecognizer::ilo_get_Count2(FingerGestures/IFingerList)
extern "C"  int32_t LongPressRecognizer_ilo_get_Count2_m2873583873 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LongPressRecognizer::ilo_get_RequiredFingerCount3(GestureRecognizer)
extern "C"  int32_t LongPressRecognizer_ilo_get_RequiredFingerCount3_m2207653799 (Il2CppObject * __this /* static, unused */, GestureRecognizer_t3512875949 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
