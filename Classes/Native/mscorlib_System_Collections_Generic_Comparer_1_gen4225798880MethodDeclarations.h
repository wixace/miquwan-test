﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<Pathfinding.Int2>
struct Comparer_1_t4225798880;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.Comparer`1<Pathfinding.Int2>::.ctor()
extern "C"  void Comparer_1__ctor_m3509899666_gshared (Comparer_1_t4225798880 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m3509899666(__this, method) ((  void (*) (Comparer_1_t4225798880 *, const MethodInfo*))Comparer_1__ctor_m3509899666_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<Pathfinding.Int2>::.cctor()
extern "C"  void Comparer_1__cctor_m950611035_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m950611035(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m950611035_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<Pathfinding.Int2>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2158721847_gshared (Comparer_1_t4225798880 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m2158721847(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t4225798880 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m2158721847_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Pathfinding.Int2>::get_Default()
extern "C"  Comparer_1_t4225798880 * Comparer_1_get_Default_m3130393914_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m3130393914(__this /* static, unused */, method) ((  Comparer_1_t4225798880 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m3130393914_gshared)(__this /* static, unused */, method)
