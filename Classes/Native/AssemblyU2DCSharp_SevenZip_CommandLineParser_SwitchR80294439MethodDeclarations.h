﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.CommandLineParser.SwitchResult
struct SwitchResult_t80294439;

#include "codegen/il2cpp-codegen.h"

// System.Void SevenZip.CommandLineParser.SwitchResult::.ctor()
extern "C"  void SwitchResult__ctor_m3660004048 (SwitchResult_t80294439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
