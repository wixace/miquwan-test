﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua_Asset_ABLoadOperation_ABOperationGenerated
struct Mihua_Asset_ABLoadOperation_ABOperationGenerated_t4291834174;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// Mihua.Asset.ABLoadOperation.ABOperation
struct ABOperation_t1894014760;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_ABOp1894014760.h"

// System.Void Mihua_Asset_ABLoadOperation_ABOperationGenerated::.ctor()
extern "C"  void Mihua_Asset_ABLoadOperation_ABOperationGenerated__ctor_m3040287517 (Mihua_Asset_ABLoadOperation_ABOperationGenerated_t4291834174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_ABOperationGenerated::ABOperation_Current(JSVCall)
extern "C"  void Mihua_Asset_ABLoadOperation_ABOperationGenerated_ABOperation_Current_m2095143436 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_ABOperationGenerated::ABOperation_progress(JSVCall)
extern "C"  void Mihua_Asset_ABLoadOperation_ABOperationGenerated_ABOperation_progress_m3798644762 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_ABOperationGenerated::ABOperation_CacheAsset(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_ABOperationGenerated_ABOperation_CacheAsset_m542971570 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_ABOperationGenerated::ABOperation_Clear(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_ABOperationGenerated_ABOperation_Clear_m3651168299 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_ABOperationGenerated::ABOperation_ClearOnLoad(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_ABOperationGenerated_ABOperation_ClearOnLoad_m162444912 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_ABOperationGenerated::ABOperation_IsDone(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_ABOperationGenerated_ABOperation_IsDone_m3020032240 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_ABOperationGenerated::ABOperation_MoveNext(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_ABOperationGenerated_ABOperation_MoveNext_m1917548744 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_ABOperationGenerated::ABOperation_Reset(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_ABOperationGenerated_ABOperation_Reset_m3883712941 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_ABOperationGenerated::__Register()
extern "C"  void Mihua_Asset_ABLoadOperation_ABOperationGenerated___Register_m2669661130 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mihua_Asset_ABLoadOperation_ABOperationGenerated::ilo_get_Current1(Mihua.Asset.ABLoadOperation.ABOperation)
extern "C"  Il2CppObject * Mihua_Asset_ABLoadOperation_ABOperationGenerated_ilo_get_Current1_m263615710 (Il2CppObject * __this /* static, unused */, ABOperation_t1894014760 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mihua_Asset_ABLoadOperation_ABOperationGenerated::ilo_get_progress2(Mihua.Asset.ABLoadOperation.ABOperation)
extern "C"  float Mihua_Asset_ABLoadOperation_ABOperationGenerated_ilo_get_progress2_m1305667702 (Il2CppObject * __this /* static, unused */, ABOperation_t1894014760 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_ABOperationGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void Mihua_Asset_ABLoadOperation_ABOperationGenerated_ilo_setSingle3_m2966112569 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_ABOperationGenerated::ilo_CacheAsset4(Mihua.Asset.ABLoadOperation.ABOperation)
extern "C"  void Mihua_Asset_ABLoadOperation_ABOperationGenerated_ilo_CacheAsset4_m2160326624 (Il2CppObject * __this /* static, unused */, ABOperation_t1894014760 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_ABOperationGenerated::ilo_ClearOnLoad5(Mihua.Asset.ABLoadOperation.ABOperation)
extern "C"  void Mihua_Asset_ABLoadOperation_ABOperationGenerated_ilo_ClearOnLoad5_m2540618033 (Il2CppObject * __this /* static, unused */, ABOperation_t1894014760 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_ABOperationGenerated::ilo_setBooleanS6(System.Int32,System.Boolean)
extern "C"  void Mihua_Asset_ABLoadOperation_ABOperationGenerated_ilo_setBooleanS6_m1188403689 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
