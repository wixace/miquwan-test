﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ByteWriteArray
struct ByteWriteArray_t2165677314;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;
// System.IO.MemoryStream
struct MemoryStream_t418716369;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ByteWriteArray2165677314.h"

// System.Void ByteWriteArray::.ctor()
extern "C"  void ByteWriteArray__ctor_m1066643929 (ByteWriteArray_t2165677314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ByteWriteArray::inverse(System.Byte[])
extern "C"  ByteU5BU5D_t4260760469* ByteWriteArray_inverse_m1950496204 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteWriteArray::WriteBoolean(System.Boolean)
extern "C"  void ByteWriteArray_WriteBoolean_m3614865867 (ByteWriteArray_t2165677314 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteWriteArray::WriteByte(System.Byte)
extern "C"  void ByteWriteArray_WriteByte_m446052589 (ByteWriteArray_t2165677314 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteWriteArray::WriteBytes(System.Byte[])
extern "C"  void ByteWriteArray_WriteBytes_m406620050 (ByteWriteArray_t2165677314 * __this, ByteU5BU5D_t4260760469* ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteWriteArray::WriteBytes(System.Byte[],System.Int32,System.Int32)
extern "C"  void ByteWriteArray_WriteBytes_m1501506162 (ByteWriteArray_t2165677314 * __this, ByteU5BU5D_t4260760469* ___bytes0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteWriteArray::WriteDouble(System.Double)
extern "C"  void ByteWriteArray_WriteDouble_m1928093595 (ByteWriteArray_t2165677314 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteWriteArray::WriteFloat(System.Single)
extern "C"  void ByteWriteArray_WriteFloat_m1960240995 (ByteWriteArray_t2165677314 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteWriteArray::WriteUInt32(System.UInt32)
extern "C"  void ByteWriteArray_WriteUInt32_m1977411851 (ByteWriteArray_t2165677314 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteWriteArray::WriteInt(System.Int32)
extern "C"  void ByteWriteArray_WriteInt_m843419564 (ByteWriteArray_t2165677314 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteWriteArray::WriteLong(System.Int64)
extern "C"  void ByteWriteArray_WriteLong_m887286276 (ByteWriteArray_t2165677314 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteWriteArray::WriteULong(System.UInt64)
extern "C"  void ByteWriteArray_WriteULong_m695515998 (ByteWriteArray_t2165677314 * __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteWriteArray::WriteShort(System.Int16)
extern "C"  void ByteWriteArray_WriteShort_m45702067 (ByteWriteArray_t2165677314 * __this, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteWriteArray::WriteShort(System.Int32)
extern "C"  void ByteWriteArray_WriteShort_m45703865 (ByteWriteArray_t2165677314 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteWriteArray::WriteUTF(System.String)
extern "C"  void ByteWriteArray_WriteUTF_m2936356687 (ByteWriteArray_t2165677314 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ByteWriteArray::get_Buffer()
extern "C"  ByteU5BU5D_t4260760469* ByteWriteArray_get_Buffer_m2680474558 (ByteWriteArray_t2165677314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ByteWriteArray::get_Length()
extern "C"  int32_t ByteWriteArray_get_Length_m867764744 (ByteWriteArray_t2165677314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.MemoryStream ByteWriteArray::get_MemoryStream()
extern "C"  MemoryStream_t418716369 * ByteWriteArray_get_MemoryStream_m4097933128 (ByteWriteArray_t2165677314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ByteWriteArray::get_Postion()
extern "C"  int32_t ByteWriteArray_get_Postion_m3244101352 (ByteWriteArray_t2165677314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteWriteArray::set_Postion(System.Int32)
extern "C"  void ByteWriteArray_set_Postion_m533937811 (ByteWriteArray_t2165677314 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ByteWriteArray::ilo_WriteLong1(ByteWriteArray,System.Int64)
extern "C"  void ByteWriteArray_ilo_WriteLong1_m4141723910 (Il2CppObject * __this /* static, unused */, ByteWriteArray_t2165677314 * ____this0, int64_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
