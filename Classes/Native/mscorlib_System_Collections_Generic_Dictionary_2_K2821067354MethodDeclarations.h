﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1926057300MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m646518434(__this, ___host0, method) ((  void (*) (Enumerator_t2821067354 *, Dictionary_2_t2206131300 *, const MethodInfo*))Enumerator__ctor_m3226065526_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m465954111(__this, method) ((  Il2CppObject * (*) (Enumerator_t2821067354 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m398777451_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1470191827(__this, method) ((  void (*) (Enumerator_t2821067354 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m415193215_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::Dispose()
#define Enumerator_Dispose_m3240995268(__this, method) ((  void (*) (Enumerator_t2821067354 *, const MethodInfo*))Enumerator_Dispose_m620874392_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::MoveNext()
#define Enumerator_MoveNext_m3809612150(__this, method) ((  bool (*) (Enumerator_t2821067354 *, const MethodInfo*))Enumerator_MoveNext_m2458851563_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Entity.Behavior.EBehaviorID,Entity.Behavior.IBehavior>::get_Current()
#define Enumerator_get_Current_m3308560858(__this, method) ((  uint8_t (*) (Enumerator_t2821067354 *, const MethodInfo*))Enumerator_get_Current_m2450791945_gshared)(__this, method)
