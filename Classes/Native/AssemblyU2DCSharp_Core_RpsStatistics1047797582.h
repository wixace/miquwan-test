﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[][][]
struct Int32U5BU5DU5BU5DU5BU5D_t2084672417;
// System.Int32[,]
struct Int32U5BU2CU5D_t3230847822;
// System.Collections.Generic.List`1<Core.RpsResult>
struct List_1_t1847780376;
// System.Collections.Generic.List`1<Core.RpsChoice>
struct List_1_t1420982684;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.RpsStatistics
struct  RpsStatistics_t1047797582  : public Il2CppObject
{
public:
	// System.Int32[][][] Core.RpsStatistics::ResultsA
	Int32U5BU5DU5BU5DU5BU5D_t2084672417* ___ResultsA_1;
	// System.Int32[,] Core.RpsStatistics::ResultB
	Int32U5BU2CU5D_t3230847822* ___ResultB_2;
	// System.Int32 Core.RpsStatistics::Round
	int32_t ___Round_3;
	// System.Int32 Core.RpsStatistics::PlayerWins
	int32_t ___PlayerWins_4;
	// System.Int32 Core.RpsStatistics::BotWins
	int32_t ___BotWins_5;
	// System.Int32 Core.RpsStatistics::Ties
	int32_t ___Ties_6;
	// System.Collections.Generic.List`1<Core.RpsResult> Core.RpsStatistics::Results
	List_1_t1847780376 * ___Results_7;
	// System.Collections.Generic.List`1<Core.RpsChoice> Core.RpsStatistics::PlayerChoices
	List_1_t1420982684 * ___PlayerChoices_8;
	// System.Collections.Generic.List`1<Core.RpsChoice> Core.RpsStatistics::BotChoices
	List_1_t1420982684 * ___BotChoices_9;

public:
	inline static int32_t get_offset_of_ResultsA_1() { return static_cast<int32_t>(offsetof(RpsStatistics_t1047797582, ___ResultsA_1)); }
	inline Int32U5BU5DU5BU5DU5BU5D_t2084672417* get_ResultsA_1() const { return ___ResultsA_1; }
	inline Int32U5BU5DU5BU5DU5BU5D_t2084672417** get_address_of_ResultsA_1() { return &___ResultsA_1; }
	inline void set_ResultsA_1(Int32U5BU5DU5BU5DU5BU5D_t2084672417* value)
	{
		___ResultsA_1 = value;
		Il2CppCodeGenWriteBarrier(&___ResultsA_1, value);
	}

	inline static int32_t get_offset_of_ResultB_2() { return static_cast<int32_t>(offsetof(RpsStatistics_t1047797582, ___ResultB_2)); }
	inline Int32U5BU2CU5D_t3230847822* get_ResultB_2() const { return ___ResultB_2; }
	inline Int32U5BU2CU5D_t3230847822** get_address_of_ResultB_2() { return &___ResultB_2; }
	inline void set_ResultB_2(Int32U5BU2CU5D_t3230847822* value)
	{
		___ResultB_2 = value;
		Il2CppCodeGenWriteBarrier(&___ResultB_2, value);
	}

	inline static int32_t get_offset_of_Round_3() { return static_cast<int32_t>(offsetof(RpsStatistics_t1047797582, ___Round_3)); }
	inline int32_t get_Round_3() const { return ___Round_3; }
	inline int32_t* get_address_of_Round_3() { return &___Round_3; }
	inline void set_Round_3(int32_t value)
	{
		___Round_3 = value;
	}

	inline static int32_t get_offset_of_PlayerWins_4() { return static_cast<int32_t>(offsetof(RpsStatistics_t1047797582, ___PlayerWins_4)); }
	inline int32_t get_PlayerWins_4() const { return ___PlayerWins_4; }
	inline int32_t* get_address_of_PlayerWins_4() { return &___PlayerWins_4; }
	inline void set_PlayerWins_4(int32_t value)
	{
		___PlayerWins_4 = value;
	}

	inline static int32_t get_offset_of_BotWins_5() { return static_cast<int32_t>(offsetof(RpsStatistics_t1047797582, ___BotWins_5)); }
	inline int32_t get_BotWins_5() const { return ___BotWins_5; }
	inline int32_t* get_address_of_BotWins_5() { return &___BotWins_5; }
	inline void set_BotWins_5(int32_t value)
	{
		___BotWins_5 = value;
	}

	inline static int32_t get_offset_of_Ties_6() { return static_cast<int32_t>(offsetof(RpsStatistics_t1047797582, ___Ties_6)); }
	inline int32_t get_Ties_6() const { return ___Ties_6; }
	inline int32_t* get_address_of_Ties_6() { return &___Ties_6; }
	inline void set_Ties_6(int32_t value)
	{
		___Ties_6 = value;
	}

	inline static int32_t get_offset_of_Results_7() { return static_cast<int32_t>(offsetof(RpsStatistics_t1047797582, ___Results_7)); }
	inline List_1_t1847780376 * get_Results_7() const { return ___Results_7; }
	inline List_1_t1847780376 ** get_address_of_Results_7() { return &___Results_7; }
	inline void set_Results_7(List_1_t1847780376 * value)
	{
		___Results_7 = value;
		Il2CppCodeGenWriteBarrier(&___Results_7, value);
	}

	inline static int32_t get_offset_of_PlayerChoices_8() { return static_cast<int32_t>(offsetof(RpsStatistics_t1047797582, ___PlayerChoices_8)); }
	inline List_1_t1420982684 * get_PlayerChoices_8() const { return ___PlayerChoices_8; }
	inline List_1_t1420982684 ** get_address_of_PlayerChoices_8() { return &___PlayerChoices_8; }
	inline void set_PlayerChoices_8(List_1_t1420982684 * value)
	{
		___PlayerChoices_8 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerChoices_8, value);
	}

	inline static int32_t get_offset_of_BotChoices_9() { return static_cast<int32_t>(offsetof(RpsStatistics_t1047797582, ___BotChoices_9)); }
	inline List_1_t1420982684 * get_BotChoices_9() const { return ___BotChoices_9; }
	inline List_1_t1420982684 ** get_address_of_BotChoices_9() { return &___BotChoices_9; }
	inline void set_BotChoices_9(List_1_t1420982684 * value)
	{
		___BotChoices_9 = value;
		Il2CppCodeGenWriteBarrier(&___BotChoices_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
