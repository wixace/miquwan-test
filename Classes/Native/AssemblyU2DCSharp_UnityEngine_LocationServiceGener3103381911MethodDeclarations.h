﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_LocationServiceGenerated
struct UnityEngine_LocationServiceGenerated_t3103381911;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_LocationServiceGenerated::.ctor()
extern "C"  void UnityEngine_LocationServiceGenerated__ctor_m2358061796 (UnityEngine_LocationServiceGenerated_t3103381911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LocationServiceGenerated::LocationService_LocationService1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LocationServiceGenerated_LocationService_LocationService1_m43731156 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LocationServiceGenerated::LocationService_isEnabledByUser(JSVCall)
extern "C"  void UnityEngine_LocationServiceGenerated_LocationService_isEnabledByUser_m3233888907 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LocationServiceGenerated::LocationService_status(JSVCall)
extern "C"  void UnityEngine_LocationServiceGenerated_LocationService_status_m994800854 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LocationServiceGenerated::LocationService_lastData(JSVCall)
extern "C"  void UnityEngine_LocationServiceGenerated_LocationService_lastData_m1567542440 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LocationServiceGenerated::LocationService_Start__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LocationServiceGenerated_LocationService_Start__Single__Single_m3857164337 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LocationServiceGenerated::LocationService_Start__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LocationServiceGenerated_LocationService_Start__Single_m555274729 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LocationServiceGenerated::LocationService_Start(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LocationServiceGenerated_LocationService_Start_m4010488737 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LocationServiceGenerated::LocationService_Stop(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LocationServiceGenerated_LocationService_Stop_m2183690789 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LocationServiceGenerated::__Register()
extern "C"  void UnityEngine_LocationServiceGenerated___Register_m3654254691 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_LocationServiceGenerated::ilo_getSingle1(System.Int32)
extern "C"  float UnityEngine_LocationServiceGenerated_ilo_getSingle1_m655505091 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
