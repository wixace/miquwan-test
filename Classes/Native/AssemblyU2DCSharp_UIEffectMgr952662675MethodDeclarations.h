﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEffectMgr
struct UIEffectMgr_t952662675;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;
// System.String
struct String_t;
// Mihua.Asset.OnLoadAsset
struct OnLoadAsset_t4181543125;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_Asset_OnLoadAsset4181543125.h"

// System.Void UIEffectMgr::.ctor()
extern "C"  void UIEffectMgr__ctor_m3245880888 (UIEffectMgr_t952662675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffectMgr::Init()
extern "C"  void UIEffectMgr_Init_m56242460 (UIEffectMgr_t952662675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UIEffectMgr::get_effectParent()
extern "C"  GameObject_t3674682005 * UIEffectMgr_get_effectParent_m447821051 (UIEffectMgr_t952662675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffectMgr::set_effectParent(UnityEngine.GameObject)
extern "C"  void UIEffectMgr_set_effectParent_m3746694204 (UIEffectMgr_t952662675 * __this, GameObject_t3674682005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffectMgr::InitParent()
extern "C"  void UIEffectMgr_InitParent_m137845542 (UIEffectMgr_t952662675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffectMgr::PlayUIEffect(System.Int32)
extern "C"  void UIEffectMgr_PlayUIEffect_m1457248534 (UIEffectMgr_t952662675 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffectMgr::Clear(System.Int32,System.Single)
extern "C"  void UIEffectMgr_Clear_m2178943769 (UIEffectMgr_t952662675 * __this, int32_t ___id0, float ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffectMgr::Clear(System.Int32)
extern "C"  void UIEffectMgr_Clear_m3963300660 (UIEffectMgr_t952662675 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffectMgr::Clear()
extern "C"  void UIEffectMgr_Clear_m652014179 (UIEffectMgr_t952662675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Asset.ABLoadOperation.AssetOperation UIEffectMgr::ilo_LoadAssetAsync1(System.String,Mihua.Asset.OnLoadAsset)
extern "C"  AssetOperation_t778728221 * UIEffectMgr_ilo_LoadAssetAsync1_m2007392273 (Il2CppObject * __this /* static, unused */, String_t* ___assetName0, OnLoadAsset_t4181543125 * ___onLoadAsset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
