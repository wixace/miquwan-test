﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpinWithMouseGenerated
struct SpinWithMouseGenerated_t3327272146;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void SpinWithMouseGenerated::.ctor()
extern "C"  void SpinWithMouseGenerated__ctor_m2366725129 (SpinWithMouseGenerated_t3327272146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SpinWithMouseGenerated::SpinWithMouse_SpinWithMouse1(JSVCall,System.Int32)
extern "C"  bool SpinWithMouseGenerated_SpinWithMouse_SpinWithMouse1_m2588581909 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpinWithMouseGenerated::SpinWithMouse_target(JSVCall)
extern "C"  void SpinWithMouseGenerated_SpinWithMouse_target_m2578505689 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpinWithMouseGenerated::SpinWithMouse_speed(JSVCall)
extern "C"  void SpinWithMouseGenerated_SpinWithMouse_speed_m3747836891 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpinWithMouseGenerated::__Register()
extern "C"  void SpinWithMouseGenerated___Register_m1751465566 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SpinWithMouseGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool SpinWithMouseGenerated_ilo_attachFinalizerObject1_m2045258230 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
