﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._acff34dfb4527fe607374a68647e7589
struct _acff34dfb4527fe607374a68647e7589_t1459240658;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._acff34dfb4527fe607374a68647e7589::.ctor()
extern "C"  void _acff34dfb4527fe607374a68647e7589__ctor_m3417142171 (_acff34dfb4527fe607374a68647e7589_t1459240658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._acff34dfb4527fe607374a68647e7589::_acff34dfb4527fe607374a68647e7589m2(System.Int32)
extern "C"  int32_t _acff34dfb4527fe607374a68647e7589__acff34dfb4527fe607374a68647e7589m2_m3597471833 (_acff34dfb4527fe607374a68647e7589_t1459240658 * __this, int32_t ____acff34dfb4527fe607374a68647e7589a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._acff34dfb4527fe607374a68647e7589::_acff34dfb4527fe607374a68647e7589m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _acff34dfb4527fe607374a68647e7589__acff34dfb4527fe607374a68647e7589m_m3421482621 (_acff34dfb4527fe607374a68647e7589_t1459240658 * __this, int32_t ____acff34dfb4527fe607374a68647e7589a0, int32_t ____acff34dfb4527fe607374a68647e758981, int32_t ____acff34dfb4527fe607374a68647e7589c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
