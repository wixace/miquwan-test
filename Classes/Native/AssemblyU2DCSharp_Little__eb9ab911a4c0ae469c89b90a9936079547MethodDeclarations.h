﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._eb9ab911a4c0ae469c89b90a95bc96c6
struct _eb9ab911a4c0ae469c89b90a95bc96c6_t936079547;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__eb9ab911a4c0ae469c89b90a9936079547.h"

// System.Void Little._eb9ab911a4c0ae469c89b90a95bc96c6::.ctor()
extern "C"  void _eb9ab911a4c0ae469c89b90a95bc96c6__ctor_m2876987346 (_eb9ab911a4c0ae469c89b90a95bc96c6_t936079547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._eb9ab911a4c0ae469c89b90a95bc96c6::_eb9ab911a4c0ae469c89b90a95bc96c6m2(System.Int32)
extern "C"  int32_t _eb9ab911a4c0ae469c89b90a95bc96c6__eb9ab911a4c0ae469c89b90a95bc96c6m2_m982974393 (_eb9ab911a4c0ae469c89b90a95bc96c6_t936079547 * __this, int32_t ____eb9ab911a4c0ae469c89b90a95bc96c6a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._eb9ab911a4c0ae469c89b90a95bc96c6::_eb9ab911a4c0ae469c89b90a95bc96c6m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _eb9ab911a4c0ae469c89b90a95bc96c6__eb9ab911a4c0ae469c89b90a95bc96c6m_m3456092957 (_eb9ab911a4c0ae469c89b90a95bc96c6_t936079547 * __this, int32_t ____eb9ab911a4c0ae469c89b90a95bc96c6a0, int32_t ____eb9ab911a4c0ae469c89b90a95bc96c6781, int32_t ____eb9ab911a4c0ae469c89b90a95bc96c6c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._eb9ab911a4c0ae469c89b90a95bc96c6::ilo__eb9ab911a4c0ae469c89b90a95bc96c6m21(Little._eb9ab911a4c0ae469c89b90a95bc96c6,System.Int32)
extern "C"  int32_t _eb9ab911a4c0ae469c89b90a95bc96c6_ilo__eb9ab911a4c0ae469c89b90a95bc96c6m21_m898752226 (Il2CppObject * __this /* static, unused */, _eb9ab911a4c0ae469c89b90a95bc96c6_t936079547 * ____this0, int32_t ____eb9ab911a4c0ae469c89b90a95bc96c6a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
