﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.ProtoMemberAttribute
struct ProtoMemberAttribute_t2007578278;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoMemberAttribute2007578278.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ProtoBuf_DataFormat274207885.h"
#include "AssemblyU2DCSharp_ProtoBuf_MemberSerializationOpti2662421652.h"

// System.Void ProtoBuf.ProtoMemberAttribute::.ctor(System.Int32)
extern "C"  void ProtoMemberAttribute__ctor_m1750649247 (ProtoMemberAttribute_t2007578278 * __this, int32_t ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoMemberAttribute::.ctor(System.Int32,System.Boolean)
extern "C"  void ProtoMemberAttribute__ctor_m2293577726 (ProtoMemberAttribute_t2007578278 * __this, int32_t ___tag0, bool ___forced1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoMemberAttribute::CompareTo(System.Object)
extern "C"  int32_t ProtoMemberAttribute_CompareTo_m2771520538 (ProtoMemberAttribute_t2007578278 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoMemberAttribute::CompareTo(ProtoBuf.ProtoMemberAttribute)
extern "C"  int32_t ProtoMemberAttribute_CompareTo_m915315995 (ProtoMemberAttribute_t2007578278 * __this, ProtoMemberAttribute_t2007578278 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.ProtoMemberAttribute::get_Name()
extern "C"  String_t* ProtoMemberAttribute_get_Name_m2495778247 (ProtoMemberAttribute_t2007578278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoMemberAttribute::set_Name(System.String)
extern "C"  void ProtoMemberAttribute_set_Name_m1981364676 (ProtoMemberAttribute_t2007578278 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.DataFormat ProtoBuf.ProtoMemberAttribute::get_DataFormat()
extern "C"  int32_t ProtoMemberAttribute_get_DataFormat_m2464505841 (ProtoMemberAttribute_t2007578278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoMemberAttribute::set_DataFormat(ProtoBuf.DataFormat)
extern "C"  void ProtoMemberAttribute_set_DataFormat_m2421890970 (ProtoMemberAttribute_t2007578278 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoMemberAttribute::get_Tag()
extern "C"  int32_t ProtoMemberAttribute_get_Tag_m836540075 (ProtoMemberAttribute_t2007578278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoMemberAttribute::Rebase(System.Int32)
extern "C"  void ProtoMemberAttribute_Rebase_m2359851467 (ProtoMemberAttribute_t2007578278 * __this, int32_t ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoMemberAttribute::get_IsRequired()
extern "C"  bool ProtoMemberAttribute_get_IsRequired_m2335660468 (ProtoMemberAttribute_t2007578278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoMemberAttribute::set_IsRequired(System.Boolean)
extern "C"  void ProtoMemberAttribute_set_IsRequired_m3591487955 (ProtoMemberAttribute_t2007578278 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoMemberAttribute::get_IsPacked()
extern "C"  bool ProtoMemberAttribute_get_IsPacked_m4203531597 (ProtoMemberAttribute_t2007578278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoMemberAttribute::set_IsPacked(System.Boolean)
extern "C"  void ProtoMemberAttribute_set_IsPacked_m2842148780 (ProtoMemberAttribute_t2007578278 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoMemberAttribute::get_OverwriteList()
extern "C"  bool ProtoMemberAttribute_get_OverwriteList_m3786527040 (ProtoMemberAttribute_t2007578278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoMemberAttribute::set_OverwriteList(System.Boolean)
extern "C"  void ProtoMemberAttribute_set_OverwriteList_m3911964687 (ProtoMemberAttribute_t2007578278 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoMemberAttribute::get_AsReference()
extern "C"  bool ProtoMemberAttribute_get_AsReference_m1358982960 (ProtoMemberAttribute_t2007578278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoMemberAttribute::set_AsReference(System.Boolean)
extern "C"  void ProtoMemberAttribute_set_AsReference_m548915199 (ProtoMemberAttribute_t2007578278 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoMemberAttribute::get_AsReferenceHasValue()
extern "C"  bool ProtoMemberAttribute_get_AsReferenceHasValue_m3712641415 (ProtoMemberAttribute_t2007578278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoMemberAttribute::set_AsReferenceHasValue(System.Boolean)
extern "C"  void ProtoMemberAttribute_set_AsReferenceHasValue_m221546006 (ProtoMemberAttribute_t2007578278 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoMemberAttribute::get_DynamicType()
extern "C"  bool ProtoMemberAttribute_get_DynamicType_m974518288 (ProtoMemberAttribute_t2007578278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoMemberAttribute::set_DynamicType(System.Boolean)
extern "C"  void ProtoMemberAttribute_set_DynamicType_m830228703 (ProtoMemberAttribute_t2007578278 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.MemberSerializationOptions ProtoBuf.ProtoMemberAttribute::get_Options()
extern "C"  int32_t ProtoMemberAttribute_get_Options_m3025725673 (ProtoMemberAttribute_t2007578278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoMemberAttribute::set_Options(ProtoBuf.MemberSerializationOptions)
extern "C"  void ProtoMemberAttribute_set_Options_m89112282 (ProtoMemberAttribute_t2007578278 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoMemberAttribute::ilo_CompareTo1(ProtoBuf.ProtoMemberAttribute,ProtoBuf.ProtoMemberAttribute)
extern "C"  int32_t ProtoMemberAttribute_ilo_CompareTo1_m4034061464 (Il2CppObject * __this /* static, unused */, ProtoMemberAttribute_t2007578278 * ____this0, ProtoMemberAttribute_t2007578278 * ___other1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
