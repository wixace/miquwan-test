﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Poly2Tri.AdvancingFrontNode
struct AdvancingFrontNode_t688967424;
// Pathfinding.Poly2Tri.TriangulationPoint
struct TriangulationPoint_t3810082933;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3810082933.h"

// System.Void Pathfinding.Poly2Tri.AdvancingFrontNode::.ctor(Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  void AdvancingFrontNode__ctor_m294490163 (AdvancingFrontNode_t688967424 * __this, TriangulationPoint_t3810082933 * ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Poly2Tri.AdvancingFrontNode::get_HasNext()
extern "C"  bool AdvancingFrontNode_get_HasNext_m1764143671 (AdvancingFrontNode_t688967424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Poly2Tri.AdvancingFrontNode::get_HasPrev()
extern "C"  bool AdvancingFrontNode_get_HasPrev_m1832843639 (AdvancingFrontNode_t688967424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
