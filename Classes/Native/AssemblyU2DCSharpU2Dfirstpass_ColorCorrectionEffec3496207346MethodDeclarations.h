﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ColorCorrectionEffect
struct ColorCorrectionEffect_t3496207346;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void ColorCorrectionEffect::.ctor()
extern "C"  void ColorCorrectionEffect__ctor_m2923302789 (ColorCorrectionEffect_t3496207346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorCorrectionEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void ColorCorrectionEffect_OnRenderImage_m2530883001 (ColorCorrectionEffect_t3496207346 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
