﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_StackPool_1_gen2103378743MethodDeclarations.h"

// System.Void Pathfinding.Util.StackPool`1<Pathfinding.GraphNode>::.cctor()
#define StackPool_1__cctor_m1155734019(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))StackPool_1__cctor_m3413873323_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.Stack`1<T> Pathfinding.Util.StackPool`1<Pathfinding.GraphNode>::Claim()
#define StackPool_1_Claim_m1643053595(__this /* static, unused */, method) ((  Stack_1_t3122173294 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))StackPool_1_Claim_m3952253811_gshared)(__this /* static, unused */, method)
// System.Void Pathfinding.Util.StackPool`1<Pathfinding.GraphNode>::Warmup(System.Int32)
#define StackPool_1_Warmup_m2622816523(__this /* static, unused */, ___count0, method) ((  void (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))StackPool_1_Warmup_m4025859507_gshared)(__this /* static, unused */, ___count0, method)
// System.Void Pathfinding.Util.StackPool`1<Pathfinding.GraphNode>::Release(System.Collections.Generic.Stack`1<T>)
#define StackPool_1_Release_m3677675783(__this /* static, unused */, ___stack0, method) ((  void (*) (Il2CppObject * /* static, unused */, Stack_1_t3122173294 *, const MethodInfo*))StackPool_1_Release_m206937007_gshared)(__this /* static, unused */, ___stack0, method)
// System.Void Pathfinding.Util.StackPool`1<Pathfinding.GraphNode>::Clear()
#define StackPool_1_Clear_m2585217813(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))StackPool_1_Clear_m3350797677_gshared)(__this /* static, unused */, method)
// System.Int32 Pathfinding.Util.StackPool`1<Pathfinding.GraphNode>::GetSize()
#define StackPool_1_GetSize_m986198669(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))StackPool_1_GetSize_m247741669_gshared)(__this /* static, unused */, method)
