﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,npcAICfg>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1819947767(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1844374148 *, int32_t, npcAICfg_t1948330203 *, const MethodInfo*))KeyValuePair_2__ctor_m11197230_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,npcAICfg>::get_Key()
#define KeyValuePair_2_get_Key_m1274650257(__this, method) ((  int32_t (*) (KeyValuePair_2_t1844374148 *, const MethodInfo*))KeyValuePair_2_get_Key_m494458106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,npcAICfg>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2860574930(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1844374148 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m4229413435_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,npcAICfg>::get_Value()
#define KeyValuePair_2_get_Value_m3268557941(__this, method) ((  npcAICfg_t1948330203 * (*) (KeyValuePair_2_t1844374148 *, const MethodInfo*))KeyValuePair_2_get_Value_m1563175098_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,npcAICfg>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m800555986(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1844374148 *, npcAICfg_t1948330203 *, const MethodInfo*))KeyValuePair_2_set_Value_m1296398523_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,npcAICfg>::ToString()
#define KeyValuePair_2_ToString_m2766338486(__this, method) ((  String_t* (*) (KeyValuePair_2_t1844374148 *, const MethodInfo*))KeyValuePair_2_ToString_m491888647_gshared)(__this, method)
