﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UILocalizeGenerated
struct UILocalizeGenerated_t308520274;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UILocalizeGenerated::.ctor()
extern "C"  void UILocalizeGenerated__ctor_m613042777 (UILocalizeGenerated_t308520274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UILocalizeGenerated::UILocalize_UILocalize1(JSVCall,System.Int32)
extern "C"  bool UILocalizeGenerated_UILocalize_UILocalize1_m1460682009 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILocalizeGenerated::UILocalize_key(JSVCall)
extern "C"  void UILocalizeGenerated_UILocalize_key_m3347979487 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILocalizeGenerated::UILocalize_value(JSVCall)
extern "C"  void UILocalizeGenerated_UILocalize_value_m2999265293 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILocalizeGenerated::__Register()
extern "C"  void UILocalizeGenerated___Register_m1708442126 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UILocalizeGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UILocalizeGenerated_ilo_getObject1_m2730979453 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILocalizeGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UILocalizeGenerated_ilo_addJSCSRel2_m4142222614 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UILocalizeGenerated::ilo_getStringS3(System.Int32)
extern "C"  String_t* UILocalizeGenerated_ilo_getStringS3_m470911679 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
