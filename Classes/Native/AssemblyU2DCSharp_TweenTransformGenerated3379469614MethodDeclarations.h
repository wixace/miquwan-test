﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweenTransformGenerated
struct TweenTransformGenerated_t3379469614;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// TweenTransform
struct TweenTransform_t1891506529;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"

// System.Void TweenTransformGenerated::.ctor()
extern "C"  void TweenTransformGenerated__ctor_m2549265917 (TweenTransformGenerated_t3379469614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenTransformGenerated::TweenTransform_TweenTransform1(JSVCall,System.Int32)
extern "C"  bool TweenTransformGenerated_TweenTransform_TweenTransform1_m1706003829 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenTransformGenerated::TweenTransform_from(JSVCall)
extern "C"  void TweenTransformGenerated_TweenTransform_from_m1293159620 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenTransformGenerated::TweenTransform_to(JSVCall)
extern "C"  void TweenTransformGenerated_TweenTransform_to_m3665296147 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenTransformGenerated::TweenTransform_parentWhenFinished(JSVCall)
extern "C"  void TweenTransformGenerated_TweenTransform_parentWhenFinished_m2043192408 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenTransformGenerated::TweenTransform_Begin__GameObject__Single__Transform__Transform(JSVCall,System.Int32)
extern "C"  bool TweenTransformGenerated_TweenTransform_Begin__GameObject__Single__Transform__Transform_m327492391 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenTransformGenerated::TweenTransform_Begin__GameObject__Single__Transform(JSVCall,System.Int32)
extern "C"  bool TweenTransformGenerated_TweenTransform_Begin__GameObject__Single__Transform_m911243175 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenTransformGenerated::__Register()
extern "C"  void TweenTransformGenerated___Register_m2178022634 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TweenTransformGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t TweenTransformGenerated_ilo_getObject1_m3515239001 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TweenTransformGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t TweenTransformGenerated_ilo_setObject2_m4133906450 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TweenTransformGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * TweenTransformGenerated_ilo_getObject3_m1201088074 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TweenTransformGenerated::ilo_getSingle4(System.Int32)
extern "C"  float TweenTransformGenerated_ilo_getSingle4_m1581225205 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TweenTransform TweenTransformGenerated::ilo_Begin5(UnityEngine.GameObject,System.Single,UnityEngine.Transform)
extern "C"  TweenTransform_t1891506529 * TweenTransformGenerated_ilo_Begin5_m1148517470 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, float ___duration1, Transform_t1659122786 * ___to2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
