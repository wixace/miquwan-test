﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// JSDataExchangeMgr/DGetV`1<System.Single[]>
struct DGetV_1_t2194215330;
// JSDataExchangeMgr/DGetV`1<UnityEngine.AudioClip/PCMReaderCallback>
struct DGetV_1_t4256480239;
// JSDataExchangeMgr/DGetV`1<UnityEngine.AudioClip/PCMSetPositionCallback>
struct DGetV_1_t4121926307;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine_AudioClipGenerated
struct  UnityEngine_AudioClipGenerated_t2058178385  : public Il2CppObject
{
public:

public:
};

struct UnityEngine_AudioClipGenerated_t2058178385_StaticFields
{
public:
	// JSDataExchangeMgr/DGetV`1<System.Single[]> UnityEngine_AudioClipGenerated::<>f__am$cache0
	DGetV_1_t2194215330 * ___U3CU3Ef__amU24cache0_0;
	// JSDataExchangeMgr/DGetV`1<System.Single[]> UnityEngine_AudioClipGenerated::<>f__am$cache1
	DGetV_1_t2194215330 * ___U3CU3Ef__amU24cache1_1;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.AudioClip/PCMReaderCallback> UnityEngine_AudioClipGenerated::<>f__am$cache2
	DGetV_1_t4256480239 * ___U3CU3Ef__amU24cache2_2;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.AudioClip/PCMSetPositionCallback> UnityEngine_AudioClipGenerated::<>f__am$cache3
	DGetV_1_t4121926307 * ___U3CU3Ef__amU24cache3_3;
	// JSDataExchangeMgr/DGetV`1<UnityEngine.AudioClip/PCMReaderCallback> UnityEngine_AudioClipGenerated::<>f__am$cache4
	DGetV_1_t4256480239 * ___U3CU3Ef__amU24cache4_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(UnityEngine_AudioClipGenerated_t2058178385_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline DGetV_1_t2194215330 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline DGetV_1_t2194215330 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(DGetV_1_t2194215330 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(UnityEngine_AudioClipGenerated_t2058178385_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline DGetV_1_t2194215330 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline DGetV_1_t2194215330 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(DGetV_1_t2194215330 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(UnityEngine_AudioClipGenerated_t2058178385_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline DGetV_1_t4256480239 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline DGetV_1_t4256480239 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(DGetV_1_t4256480239 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(UnityEngine_AudioClipGenerated_t2058178385_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline DGetV_1_t4121926307 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline DGetV_1_t4121926307 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(DGetV_1_t4121926307 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(UnityEngine_AudioClipGenerated_t2058178385_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline DGetV_1_t4256480239 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline DGetV_1_t4256480239 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(DGetV_1_t4256480239 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
