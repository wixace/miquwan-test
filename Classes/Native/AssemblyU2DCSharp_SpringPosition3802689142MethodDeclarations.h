﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpringPosition
struct SpringPosition_t3802689142;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UIScrollView
struct UIScrollView_t2113479878;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_SpringPosition3802689142.h"
#include "AssemblyU2DCSharp_UIScrollView2113479878.h"

// System.Void SpringPosition::.ctor()
extern "C"  void SpringPosition__ctor_m685982437 (SpringPosition_t3802689142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPosition::Start()
extern "C"  void SpringPosition_Start_m3928087525 (SpringPosition_t3802689142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPosition::Update()
extern "C"  void SpringPosition_Update_m1517481160 (SpringPosition_t3802689142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPosition::NotifyListeners()
extern "C"  void SpringPosition_NotifyListeners_m1211524345 (SpringPosition_t3802689142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SpringPosition SpringPosition::Begin(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  SpringPosition_t3802689142 * SpringPosition_Begin_m639842037 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, Vector3_t4282066566  ___pos1, float ___strength2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SpringPosition::ilo_SpringLerp1(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern "C"  Vector3_t4282066566  SpringPosition_ilo_SpringLerp1_m711813843 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___from0, Vector3_t4282066566  ___to1, float ___strength2, float ___deltaTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPosition::ilo_NotifyListeners2(SpringPosition)
extern "C"  void SpringPosition_ilo_NotifyListeners2_m522136504 (Il2CppObject * __this /* static, unused */, SpringPosition_t3802689142 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPosition::ilo_UpdateScrollbars3(UIScrollView,System.Boolean)
extern "C"  void SpringPosition_ilo_UpdateScrollbars3_m3221291190 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, bool ___recalculateBounds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
