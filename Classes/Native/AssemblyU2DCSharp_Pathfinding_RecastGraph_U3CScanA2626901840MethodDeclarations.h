﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RecastGraph/<ScanAllTiles>c__AnonStorey119
struct U3CScanAllTilesU3Ec__AnonStorey119_t2626901840;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.RecastGraph/<ScanAllTiles>c__AnonStorey119::.ctor()
extern "C"  void U3CScanAllTilesU3Ec__AnonStorey119__ctor_m4273452875 (U3CScanAllTilesU3Ec__AnonStorey119_t2626901840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RecastGraph/<ScanAllTiles>c__AnonStorey119::<>m__34E(Pathfinding.GraphNode)
extern "C"  bool U3CScanAllTilesU3Ec__AnonStorey119_U3CU3Em__34E_m2729141990 (U3CScanAllTilesU3Ec__AnonStorey119_t2626901840 * __this, GraphNode_t23612370 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
