﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICenterOnChildGenerated/<UICenterOnChild_onFinished_GetDelegate_member2_arg0>c__AnonStoreyB2
struct U3CUICenterOnChild_onFinished_GetDelegate_member2_arg0U3Ec__AnonStoreyB2_t577649945;

#include "codegen/il2cpp-codegen.h"

// System.Void UICenterOnChildGenerated/<UICenterOnChild_onFinished_GetDelegate_member2_arg0>c__AnonStoreyB2::.ctor()
extern "C"  void U3CUICenterOnChild_onFinished_GetDelegate_member2_arg0U3Ec__AnonStoreyB2__ctor_m1404223602 (U3CUICenterOnChild_onFinished_GetDelegate_member2_arg0U3Ec__AnonStoreyB2_t577649945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICenterOnChildGenerated/<UICenterOnChild_onFinished_GetDelegate_member2_arg0>c__AnonStoreyB2::<>m__128()
extern "C"  void U3CUICenterOnChild_onFinished_GetDelegate_member2_arg0U3Ec__AnonStoreyB2_U3CU3Em__128_m3073701630 (U3CUICenterOnChild_onFinished_GetDelegate_member2_arg0U3Ec__AnonStoreyB2_t577649945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
