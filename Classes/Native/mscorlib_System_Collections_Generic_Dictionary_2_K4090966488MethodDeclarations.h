﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>
struct Dictionary_2_t3476030434;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4090966488.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m720258193_gshared (Enumerator_t4090966488 * __this, Dictionary_2_t3476030434 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m720258193(__this, ___host0, method) ((  void (*) (Enumerator_t4090966488 *, Dictionary_2_t3476030434 *, const MethodInfo*))Enumerator__ctor_m720258193_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1204350970_gshared (Enumerator_t4090966488 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1204350970(__this, method) ((  Il2CppObject * (*) (Enumerator_t4090966488 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1204350970_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1673950084_gshared (Enumerator_t4090966488 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1673950084(__this, method) ((  void (*) (Enumerator_t4090966488 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1673950084_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m2293055859_gshared (Enumerator_t4090966488 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2293055859(__this, method) ((  void (*) (Enumerator_t4090966488 *, const MethodInfo*))Enumerator_Dispose_m2293055859_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3284189236_gshared (Enumerator_t4090966488 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3284189236(__this, method) ((  bool (*) (Enumerator_t4090966488 *, const MethodInfo*))Enumerator_MoveNext_m3284189236_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2260084452_gshared (Enumerator_t4090966488 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2260084452(__this, method) ((  Il2CppObject * (*) (Enumerator_t4090966488 *, const MethodInfo*))Enumerator_get_Current_m2260084452_gshared)(__this, method)
