﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Bson.BsonArray
struct BsonArray_t438274503;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// Newtonsoft.Json.Bson.BsonToken
struct BsonToken_t455725415;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Bson.BsonToken>
struct IEnumerator_1_t2367590464;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonToken455725415.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonType2455132538.h"

// System.Void Newtonsoft.Json.Bson.BsonArray::.ctor()
extern "C"  void BsonArray__ctor_m1146848283 (BsonArray_t438274503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Newtonsoft.Json.Bson.BsonArray::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * BsonArray_System_Collections_IEnumerable_GetEnumerator_m2190878326 (BsonArray_t438274503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonArray::Add(Newtonsoft.Json.Bson.BsonToken)
extern "C"  void BsonArray_Add_m432186298 (BsonArray_t438274503 * __this, BsonToken_t455725415 * ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonArray::get_Type()
extern "C"  int8_t BsonArray_get_Type_m887748748 (BsonArray_t438274503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Bson.BsonToken> Newtonsoft.Json.Bson.BsonArray::GetEnumerator()
extern "C"  Il2CppObject* BsonArray_GetEnumerator_m1120742503 (BsonArray_t438274503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonArray::ilo_set_Parent1(Newtonsoft.Json.Bson.BsonToken,Newtonsoft.Json.Bson.BsonToken)
extern "C"  void BsonArray_ilo_set_Parent1_m1290261686 (Il2CppObject * __this /* static, unused */, BsonToken_t455725415 * ____this0, BsonToken_t455725415 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
