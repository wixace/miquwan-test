﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSAssetMgr
struct JSAssetMgr_t2634428785;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.AssetBundle
struct AssetBundle_t2070959688;
// UnityEngine.TextAsset
struct TextAsset_t3836129977;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// JSEngine
struct JSEngine_t2847479019;
// System.Object
struct Il2CppObject;
// UnityEngine.Coroutine
struct Coroutine_t3621161934;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AssetBundle2070959688.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_JSEngine2847479019.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void JSAssetMgr::.ctor()
extern "C"  void JSAssetMgr__ctor_m681014858 (JSAssetMgr_t2634428785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSAssetMgr::get_isLoaded()
extern "C"  bool JSAssetMgr_get_isLoaded_m2914698470 (JSAssetMgr_t2634428785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSAssetMgr::set_isLoaded(System.Boolean)
extern "C"  void JSAssetMgr_set_isLoaded_m362683805 (JSAssetMgr_t2634428785 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSAssetMgr::get_loadProgress()
extern "C"  float JSAssetMgr_get_loadProgress_m1594333282 (JSAssetMgr_t2634428785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSAssetMgr::InitEngine()
extern "C"  void JSAssetMgr_InitEngine_m3083747820 (JSAssetMgr_t2634428785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSAssetMgr::LoadJs()
extern "C"  void JSAssetMgr_LoadJs_m1886204041 (JSAssetMgr_t2634428785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSAssetMgr::get_IsLoaded()
extern "C"  bool JSAssetMgr_get_IsLoaded_m613196038 (JSAssetMgr_t2634428785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator JSAssetMgr::RunLoadJavascript()
extern "C"  Il2CppObject * JSAssetMgr_RunLoadJavascript_m3728202878 (JSAssetMgr_t2634428785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSAssetMgr::LoadJavaScriptData(UnityEngine.AssetBundle)
extern "C"  int32_t JSAssetMgr_LoadJavaScriptData_m3940097042 (JSAssetMgr_t2634428785 * __this, AssetBundle_t2070959688 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSAssetMgr::LoadJavaScriptData()
extern "C"  int32_t JSAssetMgr_LoadJavaScriptData_m3289855525 (JSAssetMgr_t2634428785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TextAsset JSAssetMgr::GetTextAsset(System.String)
extern "C"  TextAsset_t3836129977 * JSAssetMgr_GetTextAsset_m1449066260 (JSAssetMgr_t2634428785 * __this, String_t* ___txtAddress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] JSAssetMgr::GetJsBytes(System.String)
extern "C"  ByteU5BU5D_t4260760469* JSAssetMgr_GetJsBytes_m2191963058 (JSAssetMgr_t2634428785 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSAssetMgr::Clear()
extern "C"  void JSAssetMgr_Clear_m2382115445 (JSAssetMgr_t2634428785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject JSAssetMgr::ilo_Instantiate1(UnityEngine.GameObject)
extern "C"  GameObject_t3674682005 * JSAssetMgr_ilo_Instantiate1_m3807264929 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSAssetMgr::ilo_OnAwake2(JSEngine)
extern "C"  void JSAssetMgr_ilo_OnAwake2_m1845017262 (Il2CppObject * __this /* static, unused */, JSEngine_t2847479019 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSAssetMgr::ilo_Log3(System.Object,System.Boolean)
extern "C"  void JSAssetMgr_ilo_Log3_m1468282949 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine JSAssetMgr::ilo_StartCoroutine4(System.Collections.IEnumerator)
extern "C"  Coroutine_t3621161934 * JSAssetMgr_ilo_StartCoroutine4_m4191908172 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___routine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
