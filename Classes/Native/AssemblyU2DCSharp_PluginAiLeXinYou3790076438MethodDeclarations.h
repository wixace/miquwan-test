﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginAiLeXinYou
struct PluginAiLeXinYou_t3790076438;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionInfo
struct VersionInfo_t2356638086;
// VersionMgr
struct VersionMgr_t1322950208;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Mihua.SDK.SdkwwwCallBack
struct SdkwwwCallBack_t3004559384;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// Mihua.SDK.CreatRoleInfo
struct CreatRoleInfo_t2858846543;
// Mihua.SDK.EnterGameInfo
struct EnterGameInfo_t1897532954;
// Mihua.SDK.LvUpInfo
struct LvUpInfo_t411687177;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginAiLeXinYou3790076438.h"
#include "AssemblyU2DCSharp_FloatTextMgr630384591.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "UnityEngine_UnityEngine_WWWForm461342257.h"
#include "AssemblyU2DCSharp_Mihua_SDK_SdkwwwCallBack3004559384.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginAiLeXinYou::.ctor()
extern "C"  void PluginAiLeXinYou__ctor_m4044760389 (PluginAiLeXinYou_t3790076438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::Init()
extern "C"  void PluginAiLeXinYou_Init_m636202095 (PluginAiLeXinYou_t3790076438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginAiLeXinYou::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginAiLeXinYou_ReqSDKHttpLogin_m1343902298 (PluginAiLeXinYou_t3790076438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginAiLeXinYou::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginAiLeXinYou_IsLoginSuccess_m3121791598 (PluginAiLeXinYou_t3790076438 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::OpenUserLogin()
extern "C"  void PluginAiLeXinYou_OpenUserLogin_m2404135575 (PluginAiLeXinYou_t3790076438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::UserPay(CEvent.ZEvent)
extern "C"  void PluginAiLeXinYou_UserPay_m4180449531 (PluginAiLeXinYou_t3790076438 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginAiLeXinYou_SignCallBack_m2592900116 (PluginAiLeXinYou_t3790076438 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginAiLeXinYou::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginAiLeXinYou_BuildOrderParam2WWWForm_m2776802684 (PluginAiLeXinYou_t3790076438 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::CreateRole(CEvent.ZEvent)
extern "C"  void PluginAiLeXinYou_CreateRole_m740432938 (PluginAiLeXinYou_t3790076438 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::EnterGame(CEvent.ZEvent)
extern "C"  void PluginAiLeXinYou_EnterGame_m3834124238 (PluginAiLeXinYou_t3790076438 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginAiLeXinYou_RoleUpgrade_m4141883122 (PluginAiLeXinYou_t3790076438 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::OnLoginSuccess(System.String)
extern "C"  void PluginAiLeXinYou_OnLoginSuccess_m867740298 (PluginAiLeXinYou_t3790076438 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::OnLogoutSuccess(System.String)
extern "C"  void PluginAiLeXinYou_OnLogoutSuccess_m1133688133 (PluginAiLeXinYou_t3790076438 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::OnLogout(System.String)
extern "C"  void PluginAiLeXinYou_OnLogout_m2483805594 (PluginAiLeXinYou_t3790076438 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::SwitchAccount(System.String)
extern "C"  void PluginAiLeXinYou_SwitchAccount_m3745253894 (PluginAiLeXinYou_t3790076438 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::initSdk(System.String,System.String)
extern "C"  void PluginAiLeXinYou_initSdk_m3686688529 (PluginAiLeXinYou_t3790076438 * __this, String_t* ___appId0, String_t* ___gid1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::login(System.String,System.String)
extern "C"  void PluginAiLeXinYou_login_m479511698 (PluginAiLeXinYou_t3790076438 * __this, String_t* ___appId0, String_t* ___gid1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::logout()
extern "C"  void PluginAiLeXinYou_logout_m3201671881 (PluginAiLeXinYou_t3790076438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::updateRoleInfo(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginAiLeXinYou_updateRoleInfo_m3744453950 (PluginAiLeXinYou_t3790076438 * __this, String_t* ___roleID0, String_t* ___gid1, String_t* ___serverID2, String_t* ___roleName3, String_t* ___roleLv4, String_t* ___professional5, String_t* ___conversion6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginAiLeXinYou_pay_m976346143 (PluginAiLeXinYou_t3790076438 * __this, String_t* ___appId0, String_t* ___gId1, String_t* ___sId2, String_t* ___userId3, String_t* ___roleName4, String_t* ___amount5, String_t* ___orderId6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::<OnLogoutSuccess>m__407()
extern "C"  void PluginAiLeXinYou_U3COnLogoutSuccessU3Em__407_m3871398897 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::<OnLogout>m__408()
extern "C"  void PluginAiLeXinYou_U3COnLogoutU3Em__408_m2486488775 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::<SwitchAccount>m__409()
extern "C"  void PluginAiLeXinYou_U3CSwitchAccountU3Em__409_m3560488180 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginAiLeXinYou_ilo_AddEventListener1_m355677967 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginAiLeXinYou::ilo_get_currentVS2(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginAiLeXinYou_ilo_get_currentVS2_m1424656452 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::ilo_initSdk3(PluginAiLeXinYou,System.String,System.String)
extern "C"  void PluginAiLeXinYou_ilo_initSdk3_m2701685885 (Il2CppObject * __this /* static, unused */, PluginAiLeXinYou_t3790076438 * ____this0, String_t* ___appId1, String_t* ___gid2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginAiLeXinYou::ilo_get_isSdkLogin4(VersionMgr)
extern "C"  bool PluginAiLeXinYou_ilo_get_isSdkLogin4_m1237289038 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::ilo_login5(PluginAiLeXinYou,System.String,System.String)
extern "C"  void PluginAiLeXinYou_ilo_login5_m1017987328 (Il2CppObject * __this /* static, unused */, PluginAiLeXinYou_t3790076438 * ____this0, String_t* ___appId1, String_t* ___gid2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::ilo_FloatText6(FloatTextMgr,FLOAT_TEXT_ID,System.Int32,System.Object[])
extern "C"  void PluginAiLeXinYou_ilo_FloatText6_m245186481 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, int32_t ___textId1, int32_t ___offsetY2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::ilo_Request7(System.String,UnityEngine.WWWForm,Mihua.SDK.SdkwwwCallBack)
extern "C"  void PluginAiLeXinYou_ilo_Request7_m2241766090 (Il2CppObject * __this /* static, unused */, String_t* ___url0, WWWForm_t461342257 * ___wwwForm1, SdkwwwCallBack_t3004559384 * ___callBack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::ilo_Log8(System.Object,System.Boolean)
extern "C"  void PluginAiLeXinYou_ilo_Log8_m3129627439 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::ilo_LogError9(System.Object,System.Boolean)
extern "C"  void PluginAiLeXinYou_ilo_LogError9_m113489038 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken PluginAiLeXinYou::ilo_get_Item10(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * PluginAiLeXinYou_ilo_get_Item10_m2122406596 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr PluginAiLeXinYou::ilo_get_FloatTextMgr11()
extern "C"  FloatTextMgr_t630384591 * PluginAiLeXinYou_ilo_get_FloatTextMgr11_m3068794232 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginAiLeXinYou::ilo_get_ProductsCfgMgr12()
extern "C"  ProductsCfgMgr_t2493714872 * PluginAiLeXinYou_ilo_get_ProductsCfgMgr12_m860476441 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.CreatRoleInfo PluginAiLeXinYou::ilo_Parse13(System.Object[])
extern "C"  CreatRoleInfo_t2858846543 * PluginAiLeXinYou_ilo_Parse13_m2891228509 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::ilo_updateRoleInfo14(PluginAiLeXinYou,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginAiLeXinYou_ilo_updateRoleInfo14_m1025630224 (Il2CppObject * __this /* static, unused */, PluginAiLeXinYou_t3790076438 * ____this0, String_t* ___roleID1, String_t* ___gid2, String_t* ___serverID3, String_t* ___roleName4, String_t* ___roleLv5, String_t* ___professional6, String_t* ___conversion7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.EnterGameInfo PluginAiLeXinYou::ilo_Parse15(System.Object[])
extern "C"  EnterGameInfo_t1897532954 * PluginAiLeXinYou_ilo_Parse15_m764068208 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.LvUpInfo PluginAiLeXinYou::ilo_Parse16(System.Object[])
extern "C"  LvUpInfo_t411687177 * PluginAiLeXinYou_ilo_Parse16_m817483434 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::ilo_ReqSDKHttpLogin17(PluginsSdkMgr)
extern "C"  void PluginAiLeXinYou_ilo_ReqSDKHttpLogin17_m2924559961 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLeXinYou::ilo_Logout18(System.Action)
extern "C"  void PluginAiLeXinYou_ilo_Logout18_m3582192026 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginAiLeXinYou::ilo_get_PluginsSdkMgr19()
extern "C"  PluginsSdkMgr_t3884624670 * PluginAiLeXinYou_ilo_get_PluginsSdkMgr19_m2124184760 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
