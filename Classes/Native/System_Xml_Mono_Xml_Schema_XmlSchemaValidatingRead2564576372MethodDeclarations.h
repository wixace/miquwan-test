﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XmlSchemaValidatingReader
struct XmlSchemaValidatingReader_t2564576372;
// System.Xml.XmlReader
struct XmlReader_t4123196108;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t4229224207;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t4231404781;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_t4090188264;
// System.String
struct String_t;
// System.Xml.XmlNameTable
struct XmlNameTable_t1216706026;
// System.Xml.XmlParserContext
struct XmlParserContext_t1291067127;
// System.Xml.Schema.IXmlSchemaInfo
struct IXmlSchemaInfo_t4085280001;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2133315502;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t3060492794;
// System.Xml.Schema.XmlSchemaAttribute
struct XmlSchemaAttribute_t2904598248;
// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_t3664762632;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlReader4123196108.h"
#include "System_Xml_System_Xml_XmlReaderSettings4229224207.h"
#include "System_Xml_System_Xml_Schema_ValidationEventHandle4231404781.h"
#include "mscorlib_System_String7231557.h"
#include "System_Xml_System_Xml_XmlNodeType992114213.h"
#include "System_Xml_System_Xml_ReadState352099245.h"
#include "System_Xml_System_Xml_XmlSpace557686381.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidity1280069984.h"

// System.Void Mono.Xml.Schema.XmlSchemaValidatingReader::.ctor(System.Xml.XmlReader,System.Xml.XmlReaderSettings)
extern "C"  void XmlSchemaValidatingReader__ctor_m691346149 (XmlSchemaValidatingReader_t2564576372 * __this, XmlReader_t4123196108 * ___reader0, XmlReaderSettings_t4229224207 * ___settings1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.Schema.XmlSchemaValidatingReader::.cctor()
extern "C"  void XmlSchemaValidatingReader__cctor_m4173766767 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.Schema.XmlSchemaValidatingReader::add_ValidationEventHandler(System.Xml.Schema.ValidationEventHandler)
extern "C"  void XmlSchemaValidatingReader_add_ValidationEventHandler_m1828180237 (XmlSchemaValidatingReader_t2564576372 * __this, ValidationEventHandler_t4231404781 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.Schema.XmlSchemaValidatingReader::remove_ValidationEventHandler(System.Xml.Schema.ValidationEventHandler)
extern "C"  void XmlSchemaValidatingReader_remove_ValidationEventHandler_m3743586848 (XmlSchemaValidatingReader_t2564576372 * __this, ValidationEventHandler_t4231404781 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Xml.Schema.XmlSchemaValidatingReader::System.Xml.IXmlLineInfo.get_LineNumber()
extern "C"  int32_t XmlSchemaValidatingReader_System_Xml_IXmlLineInfo_get_LineNumber_m3771107378 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Xml.Schema.XmlSchemaValidatingReader::System.Xml.IXmlLineInfo.get_LinePosition()
extern "C"  int32_t XmlSchemaValidatingReader_System_Xml_IXmlLineInfo_get_LinePosition_m856384530 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.Schema.XmlSchemaValidatingReader::System.Xml.IXmlLineInfo.HasLineInfo()
extern "C"  bool XmlSchemaValidatingReader_System_Xml_IXmlLineInfo_HasLineInfo_m967312312 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaType Mono.Xml.Schema.XmlSchemaValidatingReader::get_ElementSchemaType()
extern "C"  XmlSchemaType_t4090188264 * XmlSchemaValidatingReader_get_ElementSchemaType_m3306897986 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.Schema.XmlSchemaValidatingReader::ResetStateOnRead()
extern "C"  void XmlSchemaValidatingReader_ResetStateOnRead_m3235463293 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Xml.Schema.XmlSchemaValidatingReader::get_LineNumber()
extern "C"  int32_t XmlSchemaValidatingReader_get_LineNumber_m2964676234 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Xml.Schema.XmlSchemaValidatingReader::get_LinePosition()
extern "C"  int32_t XmlSchemaValidatingReader_get_LinePosition_m3265135722 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaType Mono.Xml.Schema.XmlSchemaValidatingReader::get_SchemaType()
extern "C"  XmlSchemaType_t4090188264 * XmlSchemaValidatingReader_get_SchemaType_m4266999698 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.Schema.XmlSchemaValidatingReader::LookupPrefix(System.String)
extern "C"  String_t* XmlSchemaValidatingReader_LookupPrefix_m2636920691 (XmlSchemaValidatingReader_t2564576372 * __this, String_t* ___ns0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Xml.Schema.XmlSchemaValidatingReader::get_AttributeCount()
extern "C"  int32_t XmlSchemaValidatingReader_get_AttributeCount_m2929101440 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.Schema.XmlSchemaValidatingReader::get_BaseURI()
extern "C"  String_t* XmlSchemaValidatingReader_get_BaseURI_m3370098673 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.Schema.XmlSchemaValidatingReader::get_CanResolveEntity()
extern "C"  bool XmlSchemaValidatingReader_get_CanResolveEntity_m1875096134 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Xml.Schema.XmlSchemaValidatingReader::get_Depth()
extern "C"  int32_t XmlSchemaValidatingReader_get_Depth_m3421489944 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.Schema.XmlSchemaValidatingReader::get_EOF()
extern "C"  bool XmlSchemaValidatingReader_get_EOF_m2373356343 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.Schema.XmlSchemaValidatingReader::get_HasValue()
extern "C"  bool XmlSchemaValidatingReader_get_HasValue_m1864009278 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.Schema.XmlSchemaValidatingReader::get_IsDefault()
extern "C"  bool XmlSchemaValidatingReader_get_IsDefault_m161762738 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.Schema.XmlSchemaValidatingReader::get_IsEmptyElement()
extern "C"  bool XmlSchemaValidatingReader_get_IsEmptyElement_m2985969024 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.Schema.XmlSchemaValidatingReader::get_Item(System.String)
extern "C"  String_t* XmlSchemaValidatingReader_get_Item_m1555937987 (XmlSchemaValidatingReader_t2564576372 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.Schema.XmlSchemaValidatingReader::get_Item(System.String,System.String)
extern "C"  String_t* XmlSchemaValidatingReader_get_Item_m3150664127 (XmlSchemaValidatingReader_t2564576372 * __this, String_t* ___localName0, String_t* ___ns1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.Schema.XmlSchemaValidatingReader::get_LocalName()
extern "C"  String_t* XmlSchemaValidatingReader_get_LocalName_m334321580 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.Schema.XmlSchemaValidatingReader::get_Name()
extern "C"  String_t* XmlSchemaValidatingReader_get_Name_m809295895 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.Schema.XmlSchemaValidatingReader::get_NamespaceURI()
extern "C"  String_t* XmlSchemaValidatingReader_get_NamespaceURI_m2592756509 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNameTable Mono.Xml.Schema.XmlSchemaValidatingReader::get_NameTable()
extern "C"  XmlNameTable_t1216706026 * XmlSchemaValidatingReader_get_NameTable_m1234412519 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeType Mono.Xml.Schema.XmlSchemaValidatingReader::get_NodeType()
extern "C"  int32_t XmlSchemaValidatingReader_get_NodeType_m3651591511 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlParserContext Mono.Xml.Schema.XmlSchemaValidatingReader::get_ParserContext()
extern "C"  XmlParserContext_t1291067127 * XmlSchemaValidatingReader_get_ParserContext_m2096182407 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.Schema.XmlSchemaValidatingReader::get_Prefix()
extern "C"  String_t* XmlSchemaValidatingReader_get_Prefix_m1511492510 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.ReadState Mono.Xml.Schema.XmlSchemaValidatingReader::get_ReadState()
extern "C"  int32_t XmlSchemaValidatingReader_get_ReadState_m921305178 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.IXmlSchemaInfo Mono.Xml.Schema.XmlSchemaValidatingReader::get_SchemaInfo()
extern "C"  Il2CppObject * XmlSchemaValidatingReader_get_SchemaInfo_m3294553953 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.Schema.XmlSchemaValidatingReader::get_Value()
extern "C"  String_t* XmlSchemaValidatingReader_get_Value_m2123042887 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.Schema.XmlSchemaValidatingReader::get_XmlLang()
extern "C"  String_t* XmlSchemaValidatingReader_get_XmlLang_m3373318971 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlSpace Mono.Xml.Schema.XmlSchemaValidatingReader::get_XmlSpace()
extern "C"  int32_t XmlSchemaValidatingReader_get_XmlSpace_m376334000 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.Schema.XmlSchemaValidatingReader::Close()
extern "C"  void XmlSchemaValidatingReader_Close_m3662163988 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.Schema.XmlSchemaValidatingReader::GetAttribute(System.String)
extern "C"  String_t* XmlSchemaValidatingReader_GetAttribute_m3579135225 (XmlSchemaValidatingReader_t2564576372 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlQualifiedName Mono.Xml.Schema.XmlSchemaValidatingReader::SplitQName(System.String)
extern "C"  XmlQualifiedName_t2133315502 * XmlSchemaValidatingReader_SplitQName_m2973945863 (XmlSchemaValidatingReader_t2564576372 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.Schema.XmlSchemaValidatingReader::GetAttribute(System.String,System.String)
extern "C"  String_t* XmlSchemaValidatingReader_GetAttribute_m464635253 (XmlSchemaValidatingReader_t2564576372 * __this, String_t* ___localName0, String_t* ___ns1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.Schema.XmlSchemaValidatingReader::GetDefaultAttribute(System.String,System.String)
extern "C"  String_t* XmlSchemaValidatingReader_GetDefaultAttribute_m4087507886 (XmlSchemaValidatingReader_t2564576372 * __this, String_t* ___localName0, String_t* ___ns1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Xml.Schema.XmlSchemaValidatingReader::FindDefaultAttribute(System.String,System.String)
extern "C"  int32_t XmlSchemaValidatingReader_FindDefaultAttribute_m2001039270 (XmlSchemaValidatingReader_t2564576372 * __this, String_t* ___localName0, String_t* ___ns1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.Schema.XmlSchemaValidatingReader::LookupNamespace(System.String)
extern "C"  String_t* XmlSchemaValidatingReader_LookupNamespace_m2319146434 (XmlSchemaValidatingReader_t2564576372 * __this, String_t* ___prefix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.Schema.XmlSchemaValidatingReader::MoveToAttribute(System.Int32)
extern "C"  void XmlSchemaValidatingReader_MoveToAttribute_m198971261 (XmlSchemaValidatingReader_t2564576372 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.Schema.XmlSchemaValidatingReader::MoveToAttribute(System.String)
extern "C"  bool XmlSchemaValidatingReader_MoveToAttribute_m3459574094 (XmlSchemaValidatingReader_t2564576372 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.Schema.XmlSchemaValidatingReader::MoveToAttribute(System.String,System.String)
extern "C"  bool XmlSchemaValidatingReader_MoveToAttribute_m138195210 (XmlSchemaValidatingReader_t2564576372 * __this, String_t* ___localName0, String_t* ___ns1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.Schema.XmlSchemaValidatingReader::MoveToDefaultAttribute(System.String,System.String)
extern "C"  bool XmlSchemaValidatingReader_MoveToDefaultAttribute_m3793239673 (XmlSchemaValidatingReader_t2564576372 * __this, String_t* ___localName0, String_t* ___ns1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.Schema.XmlSchemaValidatingReader::MoveToElement()
extern "C"  bool XmlSchemaValidatingReader_MoveToElement_m2785230932 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.Schema.XmlSchemaValidatingReader::MoveToFirstAttribute()
extern "C"  bool XmlSchemaValidatingReader_MoveToFirstAttribute_m2954409110 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.Schema.XmlSchemaValidatingReader::MoveToNextAttribute()
extern "C"  bool XmlSchemaValidatingReader_MoveToNextAttribute_m1601248705 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.Schema.XmlSchemaValidatingReader::Read()
extern "C"  bool XmlSchemaValidatingReader_Read_m3900300692 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.Schema.XmlSchemaValidatingReader::ReadAttributeValue()
extern "C"  bool XmlSchemaValidatingReader_ReadAttributeValue_m3498386281 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.Schema.XmlSchemaValidatingReader::ResolveEntity()
extern "C"  void XmlSchemaValidatingReader_ResolveEntity_m4122695243 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.Schema.XmlSchemaValidatingReader::get_IsNil()
extern "C"  bool XmlSchemaValidatingReader_get_IsNil_m462898562 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaSimpleType Mono.Xml.Schema.XmlSchemaValidatingReader::get_MemberType()
extern "C"  XmlSchemaSimpleType_t3060492794 * XmlSchemaValidatingReader_get_MemberType_m652103581 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaAttribute Mono.Xml.Schema.XmlSchemaValidatingReader::get_SchemaAttribute()
extern "C"  XmlSchemaAttribute_t2904598248 * XmlSchemaValidatingReader_get_SchemaAttribute_m3626349092 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaElement Mono.Xml.Schema.XmlSchemaValidatingReader::get_SchemaElement()
extern "C"  XmlSchemaElement_t3664762632 * XmlSchemaValidatingReader_get_SchemaElement_m1158137828 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaValidity Mono.Xml.Schema.XmlSchemaValidatingReader::get_Validity()
extern "C"  int32_t XmlSchemaValidatingReader_get_Validity_m2998705153 (XmlSchemaValidatingReader_t2564576372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
