﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.LocalAvoidance
struct LocalAvoidance_t2375621135;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.LocalAvoidanceMover
struct  LocalAvoidanceMover_t2872971530  : public MonoBehaviour_t667441552
{
public:
	// System.Single Pathfinding.LocalAvoidanceMover::targetPointDist
	float ___targetPointDist_2;
	// System.Single Pathfinding.LocalAvoidanceMover::speed
	float ___speed_3;
	// UnityEngine.Vector3 Pathfinding.LocalAvoidanceMover::targetPoint
	Vector3_t4282066566  ___targetPoint_4;
	// Pathfinding.LocalAvoidance Pathfinding.LocalAvoidanceMover::controller
	LocalAvoidance_t2375621135 * ___controller_5;

public:
	inline static int32_t get_offset_of_targetPointDist_2() { return static_cast<int32_t>(offsetof(LocalAvoidanceMover_t2872971530, ___targetPointDist_2)); }
	inline float get_targetPointDist_2() const { return ___targetPointDist_2; }
	inline float* get_address_of_targetPointDist_2() { return &___targetPointDist_2; }
	inline void set_targetPointDist_2(float value)
	{
		___targetPointDist_2 = value;
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(LocalAvoidanceMover_t2872971530, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_targetPoint_4() { return static_cast<int32_t>(offsetof(LocalAvoidanceMover_t2872971530, ___targetPoint_4)); }
	inline Vector3_t4282066566  get_targetPoint_4() const { return ___targetPoint_4; }
	inline Vector3_t4282066566 * get_address_of_targetPoint_4() { return &___targetPoint_4; }
	inline void set_targetPoint_4(Vector3_t4282066566  value)
	{
		___targetPoint_4 = value;
	}

	inline static int32_t get_offset_of_controller_5() { return static_cast<int32_t>(offsetof(LocalAvoidanceMover_t2872971530, ___controller_5)); }
	inline LocalAvoidance_t2375621135 * get_controller_5() const { return ___controller_5; }
	inline LocalAvoidance_t2375621135 ** get_address_of_controller_5() { return &___controller_5; }
	inline void set_controller_5(LocalAvoidance_t2375621135 * value)
	{
		___controller_5 = value;
		Il2CppCodeGenWriteBarrier(&___controller_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
