﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.CollectionUtils/<CreateDictionaryWrapper>c__AnonStorey139
struct U3CCreateDictionaryWrapperU3Ec__AnonStorey139_t3658630804;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t2570496278;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"

// System.Void Newtonsoft.Json.Utilities.CollectionUtils/<CreateDictionaryWrapper>c__AnonStorey139::.ctor()
extern "C"  void U3CCreateDictionaryWrapperU3Ec__AnonStorey139__ctor_m3756307287 (U3CCreateDictionaryWrapperU3Ec__AnonStorey139_t3658630804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.CollectionUtils/<CreateDictionaryWrapper>c__AnonStorey139::<>m__395(System.Type,System.Collections.Generic.IList`1<System.Object>)
extern "C"  Il2CppObject * U3CCreateDictionaryWrapperU3Ec__AnonStorey139_U3CU3Em__395_m3133926166 (U3CCreateDictionaryWrapperU3Ec__AnonStorey139_t3658630804 * __this, Type_t * ___t0, Il2CppObject* ___a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
