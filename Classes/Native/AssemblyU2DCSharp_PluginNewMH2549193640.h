﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginNewMH
struct  PluginNewMH_t2549193640  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginNewMH::password
	String_t* ___password_3;
	// System.String PluginNewMH::username
	String_t* ___username_4;

public:
	inline static int32_t get_offset_of_password_3() { return static_cast<int32_t>(offsetof(PluginNewMH_t2549193640, ___password_3)); }
	inline String_t* get_password_3() const { return ___password_3; }
	inline String_t** get_address_of_password_3() { return &___password_3; }
	inline void set_password_3(String_t* value)
	{
		___password_3 = value;
		Il2CppCodeGenWriteBarrier(&___password_3, value);
	}

	inline static int32_t get_offset_of_username_4() { return static_cast<int32_t>(offsetof(PluginNewMH_t2549193640, ___username_4)); }
	inline String_t* get_username_4() const { return ___username_4; }
	inline String_t** get_address_of_username_4() { return &___username_4; }
	inline void set_username_4(String_t* value)
	{
		___username_4 = value;
		Il2CppCodeGenWriteBarrier(&___username_4, value);
	}
};

struct PluginNewMH_t2549193640_StaticFields
{
public:
	// System.String PluginNewMH::appKey
	String_t* ___appKey_2;

public:
	inline static int32_t get_offset_of_appKey_2() { return static_cast<int32_t>(offsetof(PluginNewMH_t2549193640_StaticFields, ___appKey_2)); }
	inline String_t* get_appKey_2() const { return ___appKey_2; }
	inline String_t** get_address_of_appKey_2() { return &___appKey_2; }
	inline void set_appKey_2(String_t* value)
	{
		___appKey_2 = value;
		Il2CppCodeGenWriteBarrier(&___appKey_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
