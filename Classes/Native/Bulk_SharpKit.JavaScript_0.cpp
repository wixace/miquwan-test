﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// SharpKit.JavaScript.JsDelegateAttribute
struct JsDelegateAttribute_t2962628132;
// SharpKit.JavaScript.JsFieldAttribute
struct JsFieldAttribute_t3190902431;
// SharpKit.JavaScript.JsMethodAttribute
struct JsMethodAttribute_t1501046856;
// System.String
struct String_t;
// System.Type
struct Type_t;
// SharpKit.JavaScript.JsObjectBase
struct JsObjectBase_t555886221;
// System.Object
struct Il2CppObject;
// SharpKit.JavaScript.JsPropertyAttribute
struct JsPropertyAttribute_t1787918292;
// SharpKit.JavaScript.JsString
struct JsString_t4170003438;
// SharpKit.JavaScript.JsTypeAttribute
struct JsTypeAttribute_t3342267407;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "SharpKit_JavaScript_U3CModuleU3E86524790.h"
#include "SharpKit_JavaScript_U3CModuleU3E86524790MethodDeclarations.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsDelegate2962628132.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsDelegate2962628132MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"
#include "mscorlib_System_Nullable_1_gen560925241MethodDeclarations.h"
#include "mscorlib_System_Attribute2523058482MethodDeclarations.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsFieldAtt3190902431.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsFieldAtt3190902431MethodDeclarations.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsMethodAt1501046856.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsMethodAt1501046856MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsMode3080509312.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsMode3080509312MethodDeclarations.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsObjectBas555886221.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsObjectBas555886221MethodDeclarations.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsProperty1787918292.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsProperty1787918292MethodDeclarations.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsString4170003438.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsString4170003438MethodDeclarations.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsTypeAttr3342267407.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsTypeAttr3342267407MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3164635835.h"
#include "mscorlib_System_Nullable_1_gen3164635835MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SharpKit.JavaScript.JsDelegateAttribute::set_NativeDelegates(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsDelegateAttribute_set_NativeDelegates_m1853445002_MetadataUsageId;
extern "C"  void JsDelegateAttribute_set_NativeDelegates_m1853445002 (JsDelegateAttribute_t2962628132 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsDelegateAttribute_set_NativeDelegates_m1853445002_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__NativeDelegates_0(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsDelegateAttribute::.ctor()
extern "C"  void JsDelegateAttribute__ctor_m2868386651 (JsDelegateAttribute_t2962628132 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsFieldAttribute::set_Export(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsFieldAttribute_set_Export_m3144860058_MetadataUsageId;
extern "C"  void JsFieldAttribute_set_Export_m3144860058 (JsFieldAttribute_t3190902431 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsFieldAttribute_set_Export_m3144860058_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__Export_0(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsFieldAttribute::.ctor()
extern "C"  void JsFieldAttribute__ctor_m1798584018 (JsFieldAttribute_t3190902431 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsMethodAttribute::set_NativeParams(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsMethodAttribute_set_NativeParams_m3635227870_MetadataUsageId;
extern "C"  void JsMethodAttribute_set_NativeParams_m3635227870 (JsMethodAttribute_t1501046856 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsMethodAttribute_set_NativeParams_m3635227870_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__NativeParams_0(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsMethodAttribute::set_TargetMethod(System.String)
extern "C"  void JsMethodAttribute_set_TargetMethod_m120501446 (JsMethodAttribute_t1501046856 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTargetMethodU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsMethodAttribute::set_TargetType(System.Type)
extern "C"  void JsMethodAttribute_set_TargetType_m3033924100 (JsMethodAttribute_t1501046856 * __this, Type_t * ___value0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___value0;
		__this->set_U3CTargetTypeU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsMethodAttribute::set_Name(System.String)
extern "C"  void JsMethodAttribute_set_Name_m1404384749 (JsMethodAttribute_t1501046856 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsMethodAttribute::set_NativeOverloads(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsMethodAttribute_set_NativeOverloads_m329298961_MetadataUsageId;
extern "C"  void JsMethodAttribute_set_NativeOverloads_m329298961 (JsMethodAttribute_t1501046856 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsMethodAttribute_set_NativeOverloads_m329298961_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__NativeOverloads_1(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsMethodAttribute::set_IgnoreGenericArguments(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsMethodAttribute_set_IgnoreGenericArguments_m3456535634_MetadataUsageId;
extern "C"  void JsMethodAttribute_set_IgnoreGenericArguments_m3456535634 (JsMethodAttribute_t1501046856 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsMethodAttribute_set_IgnoreGenericArguments_m3456535634_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__IgnoreGenericArguments_2(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsMethodAttribute::set_Global(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsMethodAttribute_set_Global_m735858372_MetadataUsageId;
extern "C"  void JsMethodAttribute_set_Global_m735858372 (JsMethodAttribute_t1501046856 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsMethodAttribute_set_Global_m735858372_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__Global_3(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsMethodAttribute::set_Export(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsMethodAttribute_set_Export_m2722372821_MetadataUsageId;
extern "C"  void JsMethodAttribute_set_Export_m2722372821 (JsMethodAttribute_t1501046856 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsMethodAttribute_set_Export_m2722372821_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__Export_4(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsMethodAttribute::.ctor()
extern "C"  void JsMethodAttribute__ctor_m1275070327 (JsMethodAttribute_t1501046856 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 SharpKit.JavaScript.JsObjectBase::GetHashCode()
extern "C"  int32_t JsObjectBase_GetHashCode_m1584299747 (JsObjectBase_t555886221 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Object_GetHashCode_m500842593(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Boolean SharpKit.JavaScript.JsObjectBase::Equals(System.Object)
extern "C"  bool JsObjectBase_Equals_m2732936447 (JsObjectBase_t555886221 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Il2CppObject * L_0 = ___obj0;
		bool L_1 = Object_Equals_m2558036873(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000b;
	}

IL_000b:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.String SharpKit.JavaScript.JsObjectBase::ToString()
extern "C"  String_t* JsObjectBase_ToString_m2391168879 (JsObjectBase_t555886221 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = Object_ToString_m2286807767(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void SharpKit.JavaScript.JsObjectBase::.ctor()
extern "C"  void JsObjectBase__ctor_m587599652 (JsObjectBase_t555886221 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsPropertyAttribute::set_NativeField(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsPropertyAttribute_set_NativeField_m348301126_MetadataUsageId;
extern "C"  void JsPropertyAttribute_set_NativeField_m348301126 (JsPropertyAttribute_t1787918292 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsPropertyAttribute_set_NativeField_m348301126_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__NativeField_0(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsPropertyAttribute::set_Export(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsPropertyAttribute_set_Export_m4029745185_MetadataUsageId;
extern "C"  void JsPropertyAttribute_set_Export_m4029745185 (JsPropertyAttribute_t1787918292 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsPropertyAttribute_set_Export_m4029745185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__Export_1(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsPropertyAttribute::set_Name(System.String)
extern "C"  void JsPropertyAttribute_set_Name_m4012174881 (JsPropertyAttribute_t1787918292 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsPropertyAttribute::set_TargetProperty(System.String)
extern "C"  void JsPropertyAttribute_set_TargetProperty_m326317830 (JsPropertyAttribute_t1787918292 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTargetPropertyU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsPropertyAttribute::set_TargetType(System.Type)
extern "C"  void JsPropertyAttribute_set_TargetType_m612794424 (JsPropertyAttribute_t1787918292 * __this, Type_t * ___value0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___value0;
		__this->set_U3CTargetTypeU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsPropertyAttribute::.ctor()
extern "C"  void JsPropertyAttribute__ctor_m1982769067 (JsPropertyAttribute_t1787918292 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsString::.ctor(System.Object)
extern "C"  void JsString__ctor_m1240413969 (JsString_t4170003438 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		JsObjectBase__ctor_m587599652(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___obj0;
		V_0 = (bool)((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		__this->set__Value_0(L_3);
	}

IL_001c:
	{
		return;
	}
}
// SharpKit.JavaScript.JsString SharpKit.JavaScript.JsString::op_Implicit(System.String)
extern Il2CppClass* JsString_t4170003438_il2cpp_TypeInfo_var;
extern const uint32_t JsString_op_Implicit_m542190724_MetadataUsageId;
extern "C"  JsString_t4170003438 * JsString_op_Implicit_m542190724 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsString_op_Implicit_m542190724_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JsString_t4170003438 * V_0 = NULL;
	{
		String_t* L_0 = ___s0;
		JsString_t4170003438 * L_1 = (JsString_t4170003438 *)il2cpp_codegen_object_new(JsString_t4170003438_il2cpp_TypeInfo_var);
		JsString__ctor_m1240413969(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000a;
	}

IL_000a:
	{
		JsString_t4170003438 * L_2 = V_0;
		return L_2;
	}
}
// System.String SharpKit.JavaScript.JsString::op_Implicit(SharpKit.JavaScript.JsString)
extern "C"  String_t* JsString_op_Implicit_m3144283048 (Il2CppObject * __this /* static, unused */, JsString_t4170003438 * ___s0, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		JsString_t4170003438 * L_0 = ___s0;
		NullCheck(L_0);
		String_t* L_1 = L_0->get__Value_0();
		V_0 = L_1;
		goto IL_000a;
	}

IL_000a:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// System.String SharpKit.JavaScript.JsString::ToString()
extern "C"  String_t* JsString_ToString_m219910224 (JsString_t4170003438 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get__Value_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::.ctor()
extern "C"  void JsTypeAttribute__ctor_m1821362512 (JsTypeAttribute_t3342267407 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::.ctor(SharpKit.JavaScript.JsMode)
extern "C"  void JsTypeAttribute__ctor_m3970655397 (JsTypeAttribute_t3342267407 * __this, int32_t ___mode0, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___mode0;
		JsTypeAttribute_set_Mode_m4122075689(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_TargetType(System.Type)
extern "C"  void JsTypeAttribute_set_TargetType_m4137901405 (JsTypeAttribute_t3342267407 * __this, Type_t * ___value0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___value0;
		__this->set_U3CTargetTypeU3Ek__BackingField_21(L_0);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_NativeParams(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsTypeAttribute_set_NativeParams_m2413822245_MetadataUsageId;
extern "C"  void JsTypeAttribute_set_NativeParams_m2413822245 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsTypeAttribute_set_NativeParams_m2413822245_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__NativeParams_0(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_NativeDelegates(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsTypeAttribute_set_NativeDelegates_m4268830719_MetadataUsageId;
extern "C"  void JsTypeAttribute_set_NativeDelegates_m4268830719 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsTypeAttribute_set_NativeDelegates_m4268830719_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__NativeDelegates_1(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_OmitCasts(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsTypeAttribute_set_OmitCasts_m3509269971_MetadataUsageId;
extern "C"  void JsTypeAttribute_set_OmitCasts_m3509269971 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsTypeAttribute_set_OmitCasts_m3509269971_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__OmitCasts_2(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_PropertiesAsFields(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsTypeAttribute_set_PropertiesAsFields_m1232173702_MetadataUsageId;
extern "C"  void JsTypeAttribute_set_PropertiesAsFields_m1232173702 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsTypeAttribute_set_PropertiesAsFields_m1232173702_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__PropertiesAsFields_3(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_AutomaticPropertiesAsFields(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsTypeAttribute_set_AutomaticPropertiesAsFields_m150870065_MetadataUsageId;
extern "C"  void JsTypeAttribute_set_AutomaticPropertiesAsFields_m150870065 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsTypeAttribute_set_AutomaticPropertiesAsFields_m150870065_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__AutomaticPropertiesAsFields_4(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_NativeEnumerator(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsTypeAttribute_set_NativeEnumerator_m3276593123_MetadataUsageId;
extern "C"  void JsTypeAttribute_set_NativeEnumerator_m3276593123 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsTypeAttribute_set_NativeEnumerator_m3276593123_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__NativeEnumerator_5(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_NativeArrayEnumerator(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsTypeAttribute_set_NativeArrayEnumerator_m1119156910_MetadataUsageId;
extern "C"  void JsTypeAttribute_set_NativeArrayEnumerator_m1119156910 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsTypeAttribute_set_NativeArrayEnumerator_m1119156910_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__NativeArrayEnumerator_6(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_NativeConstructors(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsTypeAttribute_set_NativeConstructors_m3656212504_MetadataUsageId;
extern "C"  void JsTypeAttribute_set_NativeConstructors_m3656212504 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsTypeAttribute_set_NativeConstructors_m3656212504_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__NativeConstructors_7(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_NativeOverloads(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsTypeAttribute_set_NativeOverloads_m397256298_MetadataUsageId;
extern "C"  void JsTypeAttribute_set_NativeOverloads_m397256298 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsTypeAttribute_set_NativeOverloads_m397256298_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__NativeOverloads_8(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_NativeOperatorOverloads(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsTypeAttribute_set_NativeOperatorOverloads_m568415526_MetadataUsageId;
extern "C"  void JsTypeAttribute_set_NativeOperatorOverloads_m568415526 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsTypeAttribute_set_NativeOperatorOverloads_m568415526_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__NativeOperatorOverloads_9(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_Native(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsTypeAttribute_set_Native_m1509132479_MetadataUsageId;
extern "C"  void JsTypeAttribute_set_Native_m1509132479 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsTypeAttribute_set_Native_m1509132479_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__Native_10(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_GlobalObject(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsTypeAttribute_set_GlobalObject_m1207130986_MetadataUsageId;
extern "C"  void JsTypeAttribute_set_GlobalObject_m1207130986 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsTypeAttribute_set_GlobalObject_m1207130986_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__GlobalObject_11(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_NativeFunctions(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsTypeAttribute_set_NativeFunctions_m333621772_MetadataUsageId;
extern "C"  void JsTypeAttribute_set_NativeFunctions_m333621772 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsTypeAttribute_set_NativeFunctions_m333621772_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__NativeFunctions_12(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_NativeJsons(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsTypeAttribute_set_NativeJsons_m799553404_MetadataUsageId;
extern "C"  void JsTypeAttribute_set_NativeJsons_m799553404 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsTypeAttribute_set_NativeJsons_m799553404_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__NativeJsons_13(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_Export(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsTypeAttribute_set_Export_m2619437660_MetadataUsageId;
extern "C"  void JsTypeAttribute_set_Export_m2619437660 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsTypeAttribute_set_Export_m2619437660_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__Export_14(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_Name(System.String)
extern "C"  void JsTypeAttribute_set_Name_m3869255110 (JsTypeAttribute_t3342267407 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_22(L_0);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_IgnoreGenericMethodArguments(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsTypeAttribute_set_IgnoreGenericMethodArguments_m1144442296_MetadataUsageId;
extern "C"  void JsTypeAttribute_set_IgnoreGenericMethodArguments_m1144442296 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsTypeAttribute_set_IgnoreGenericMethodArguments_m1144442296_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__IgnoreGenericMethodArguments_16(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_Mode(SharpKit.JavaScript.JsMode)
extern const MethodInfo* Nullable_1__ctor_m2297815667_MethodInfo_var;
extern const uint32_t JsTypeAttribute_set_Mode_m4122075689_MetadataUsageId;
extern "C"  void JsTypeAttribute_set_Mode_m4122075689 (JsTypeAttribute_t3342267407 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsTypeAttribute_set_Mode_m4122075689_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		Nullable_1_t3164635835  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2297815667(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2297815667_MethodInfo_var);
		__this->set__Mode_17(L_1);
		JsTypeAttribute_ApplyJsMode_m1729871304(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_InlineFields(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsTypeAttribute_set_InlineFields_m597255514_MetadataUsageId;
extern "C"  void JsTypeAttribute_set_InlineFields_m597255514 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsTypeAttribute_set_InlineFields_m597255514_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__InlineFields_18(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::set_NativeCasts(System.Boolean)
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsTypeAttribute_set_NativeCasts_m1613578757_MetadataUsageId;
extern "C"  void JsTypeAttribute_set_NativeCasts_m1613578757 (JsTypeAttribute_t3342267407 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsTypeAttribute_set_NativeCasts_m1613578757_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		Nullable_1_t560925241  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Nullable_1__ctor_m2898070853(&L_1, L_0, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__NativeCasts_20(L_1);
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::GoNative()
extern const MethodInfo* Nullable_1_get_HasValue_m1646262914_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsTypeAttribute_GoNative_m4128470995_MetadataUsageId;
extern "C"  void JsTypeAttribute_GoNative_m4128470995 (JsTypeAttribute_t3342267407 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsTypeAttribute_GoNative_m4128470995_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Nullable_1_t560925241 * L_0 = __this->get_address_of__NativeOverloads_8();
		bool L_1 = Nullable_1_get_HasValue_m1646262914(L_0, /*hidden argument*/Nullable_1_get_HasValue_m1646262914_MethodInfo_var);
		V_0 = L_1;
		bool L_2 = V_0;
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		JsTypeAttribute_set_NativeOverloads_m397256298(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0018:
	{
		Nullable_1_t560925241 * L_3 = __this->get_address_of__NativeDelegates_1();
		bool L_4 = Nullable_1_get_HasValue_m1646262914(L_3, /*hidden argument*/Nullable_1_get_HasValue_m1646262914_MethodInfo_var);
		V_0 = L_4;
		bool L_5 = V_0;
		if (L_5)
		{
			goto IL_002f;
		}
	}
	{
		JsTypeAttribute_set_NativeDelegates_m4268830719(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_002f:
	{
		Nullable_1_t560925241 * L_6 = __this->get_address_of__AutomaticPropertiesAsFields_4();
		bool L_7 = Nullable_1_get_HasValue_m1646262914(L_6, /*hidden argument*/Nullable_1_get_HasValue_m1646262914_MethodInfo_var);
		V_0 = L_7;
		bool L_8 = V_0;
		if (L_8)
		{
			goto IL_0046;
		}
	}
	{
		JsTypeAttribute_set_AutomaticPropertiesAsFields_m150870065(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0046:
	{
		Nullable_1_t560925241 * L_9 = __this->get_address_of__NativeConstructors_7();
		bool L_10 = Nullable_1_get_HasValue_m1646262914(L_9, /*hidden argument*/Nullable_1_get_HasValue_m1646262914_MethodInfo_var);
		V_0 = L_10;
		bool L_11 = V_0;
		if (L_11)
		{
			goto IL_005d;
		}
	}
	{
		JsTypeAttribute_set_NativeConstructors_m3656212504(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_005d:
	{
		Nullable_1_t560925241 * L_12 = __this->get_address_of__NativeEnumerator_5();
		bool L_13 = Nullable_1_get_HasValue_m1646262914(L_12, /*hidden argument*/Nullable_1_get_HasValue_m1646262914_MethodInfo_var);
		V_0 = L_13;
		bool L_14 = V_0;
		if (L_14)
		{
			goto IL_0074;
		}
	}
	{
		JsTypeAttribute_set_NativeEnumerator_m3276593123(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0074:
	{
		Nullable_1_t560925241 * L_15 = __this->get_address_of__NativeFunctions_12();
		bool L_16 = Nullable_1_get_HasValue_m1646262914(L_15, /*hidden argument*/Nullable_1_get_HasValue_m1646262914_MethodInfo_var);
		V_0 = L_16;
		bool L_17 = V_0;
		if (L_17)
		{
			goto IL_008b;
		}
	}
	{
		JsTypeAttribute_set_NativeFunctions_m333621772(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_008b:
	{
		Nullable_1_t560925241 * L_18 = __this->get_address_of__NativeJsons_13();
		bool L_19 = Nullable_1_get_HasValue_m1646262914(L_18, /*hidden argument*/Nullable_1_get_HasValue_m1646262914_MethodInfo_var);
		V_0 = L_19;
		bool L_20 = V_0;
		if (L_20)
		{
			goto IL_00a2;
		}
	}
	{
		JsTypeAttribute_set_NativeJsons_m799553404(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_00a2:
	{
		Nullable_1_t560925241 * L_21 = __this->get_address_of__IgnoreGenericTypeArguments_15();
		bool L_22 = Nullable_1_get_HasValue_m1646262914(L_21, /*hidden argument*/Nullable_1_get_HasValue_m1646262914_MethodInfo_var);
		V_0 = L_22;
		bool L_23 = V_0;
		if (L_23)
		{
			goto IL_00bd;
		}
	}
	{
		Nullable_1_t560925241  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Nullable_1__ctor_m2898070853(&L_24, (bool)1, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__IgnoreGenericTypeArguments_15(L_24);
	}

IL_00bd:
	{
		Nullable_1_t560925241 * L_25 = __this->get_address_of__IgnoreGenericMethodArguments_16();
		bool L_26 = Nullable_1_get_HasValue_m1646262914(L_25, /*hidden argument*/Nullable_1_get_HasValue_m1646262914_MethodInfo_var);
		V_0 = L_26;
		bool L_27 = V_0;
		if (L_27)
		{
			goto IL_00d4;
		}
	}
	{
		JsTypeAttribute_set_IgnoreGenericMethodArguments_m1144442296(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_00d4:
	{
		Nullable_1_t560925241 * L_28 = __this->get_address_of__InlineFields_18();
		bool L_29 = Nullable_1_get_HasValue_m1646262914(L_28, /*hidden argument*/Nullable_1_get_HasValue_m1646262914_MethodInfo_var);
		V_0 = L_29;
		bool L_30 = V_0;
		if (L_30)
		{
			goto IL_00eb;
		}
	}
	{
		JsTypeAttribute_set_InlineFields_m597255514(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_00eb:
	{
		return;
	}
}
// System.Void SharpKit.JavaScript.JsTypeAttribute::ApplyJsMode()
extern const MethodInfo* Nullable_1_GetValueOrDefault_m3171265427_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m2124201516_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m1646262914_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m2898070853_MethodInfo_var;
extern const uint32_t JsTypeAttribute_ApplyJsMode_m1729871304_MetadataUsageId;
extern "C"  void JsTypeAttribute_ApplyJsMode_m1729871304 (JsTypeAttribute_t3342267407 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsTypeAttribute_ApplyJsMode_m1729871304_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Nullable_1_t3164635835  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	int32_t G_B3_0 = 0;
	int32_t G_B8_0 = 0;
	int32_t G_B13_0 = 0;
	int32_t G_B18_0 = 0;
	int32_t G_B31_0 = 0;
	{
		Nullable_1_t3164635835  L_0 = __this->get__Mode_17();
		V_0 = L_0;
		int32_t L_1 = Nullable_1_GetValueOrDefault_m3171265427((&V_0), /*hidden argument*/Nullable_1_GetValueOrDefault_m3171265427_MethodInfo_var);
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		bool L_2 = Nullable_1_get_HasValue_m2124201516((&V_0), /*hidden argument*/Nullable_1_get_HasValue_m2124201516_MethodInfo_var);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001b;
	}

IL_001a:
	{
		G_B3_0 = 0;
	}

IL_001b:
	{
		V_1 = (bool)((((int32_t)G_B3_0) == ((int32_t)0))? 1 : 0);
		bool L_3 = V_1;
		if (L_3)
		{
			goto IL_0038;
		}
	}
	{
		JsTypeAttribute_set_GlobalObject_m1207130986(__this, (bool)1, /*hidden argument*/NULL);
		JsTypeAttribute_GoNative_m4128470995(__this, /*hidden argument*/NULL);
		goto IL_0179;
	}

IL_0038:
	{
		Nullable_1_t3164635835  L_4 = __this->get__Mode_17();
		V_0 = L_4;
		int32_t L_5 = Nullable_1_GetValueOrDefault_m3171265427((&V_0), /*hidden argument*/Nullable_1_GetValueOrDefault_m3171265427_MethodInfo_var);
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_0052;
		}
	}
	{
		bool L_6 = Nullable_1_get_HasValue_m2124201516((&V_0), /*hidden argument*/Nullable_1_get_HasValue_m2124201516_MethodInfo_var);
		G_B8_0 = ((int32_t)(L_6));
		goto IL_0053;
	}

IL_0052:
	{
		G_B8_0 = 0;
	}

IL_0053:
	{
		V_1 = (bool)((((int32_t)G_B8_0) == ((int32_t)0))? 1 : 0);
		bool L_7 = V_1;
		if (L_7)
		{
			goto IL_0070;
		}
	}
	{
		JsTypeAttribute_set_Native_m1509132479(__this, (bool)1, /*hidden argument*/NULL);
		JsTypeAttribute_GoNative_m4128470995(__this, /*hidden argument*/NULL);
		goto IL_0179;
	}

IL_0070:
	{
		Nullable_1_t3164635835  L_8 = __this->get__Mode_17();
		V_0 = L_8;
		int32_t L_9 = Nullable_1_GetValueOrDefault_m3171265427((&V_0), /*hidden argument*/Nullable_1_GetValueOrDefault_m3171265427_MethodInfo_var);
		if ((!(((uint32_t)L_9) == ((uint32_t)2))))
		{
			goto IL_008a;
		}
	}
	{
		bool L_10 = Nullable_1_get_HasValue_m2124201516((&V_0), /*hidden argument*/Nullable_1_get_HasValue_m2124201516_MethodInfo_var);
		G_B13_0 = ((int32_t)(L_10));
		goto IL_008b;
	}

IL_008a:
	{
		G_B13_0 = 0;
	}

IL_008b:
	{
		V_1 = (bool)((((int32_t)G_B13_0) == ((int32_t)0))? 1 : 0);
		bool L_11 = V_1;
		if (L_11)
		{
			goto IL_0099;
		}
	}
	{
		goto IL_0179;
	}

IL_0099:
	{
		Nullable_1_t3164635835  L_12 = __this->get__Mode_17();
		V_0 = L_12;
		int32_t L_13 = Nullable_1_GetValueOrDefault_m3171265427((&V_0), /*hidden argument*/Nullable_1_GetValueOrDefault_m3171265427_MethodInfo_var);
		if ((!(((uint32_t)L_13) == ((uint32_t)4))))
		{
			goto IL_00b3;
		}
	}
	{
		bool L_14 = Nullable_1_get_HasValue_m2124201516((&V_0), /*hidden argument*/Nullable_1_get_HasValue_m2124201516_MethodInfo_var);
		G_B18_0 = ((int32_t)(L_14));
		goto IL_00b4;
	}

IL_00b3:
	{
		G_B18_0 = 0;
	}

IL_00b4:
	{
		V_1 = (bool)((((int32_t)G_B18_0) == ((int32_t)0))? 1 : 0);
		bool L_15 = V_1;
		if (L_15)
		{
			goto IL_011f;
		}
	}
	{
		Nullable_1_t560925241 * L_16 = __this->get_address_of__Native_10();
		bool L_17 = Nullable_1_get_HasValue_m1646262914(L_16, /*hidden argument*/Nullable_1_get_HasValue_m1646262914_MethodInfo_var);
		V_1 = L_17;
		bool L_18 = V_1;
		if (L_18)
		{
			goto IL_00d3;
		}
	}
	{
		JsTypeAttribute_set_Native_m1509132479(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_00d3:
	{
		Nullable_1_t560925241 * L_19 = __this->get_address_of__NativeConstructors_7();
		bool L_20 = Nullable_1_get_HasValue_m1646262914(L_19, /*hidden argument*/Nullable_1_get_HasValue_m1646262914_MethodInfo_var);
		V_1 = L_20;
		bool L_21 = V_1;
		if (L_21)
		{
			goto IL_00ea;
		}
	}
	{
		JsTypeAttribute_set_NativeConstructors_m3656212504(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_00ea:
	{
		Nullable_1_t560925241 * L_22 = __this->get_address_of__InlineFields_18();
		bool L_23 = Nullable_1_get_HasValue_m1646262914(L_22, /*hidden argument*/Nullable_1_get_HasValue_m1646262914_MethodInfo_var);
		V_1 = L_23;
		bool L_24 = V_1;
		if (L_24)
		{
			goto IL_0101;
		}
	}
	{
		JsTypeAttribute_set_InlineFields_m597255514(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_0101:
	{
		Nullable_1_t560925241 * L_25 = __this->get_address_of__OmitDefaultConstructor_19();
		bool L_26 = Nullable_1_get_HasValue_m1646262914(L_25, /*hidden argument*/Nullable_1_get_HasValue_m1646262914_MethodInfo_var);
		V_1 = L_26;
		bool L_27 = V_1;
		if (L_27)
		{
			goto IL_011c;
		}
	}
	{
		Nullable_1_t560925241  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Nullable_1__ctor_m2898070853(&L_28, (bool)1, /*hidden argument*/Nullable_1__ctor_m2898070853_MethodInfo_var);
		__this->set__OmitDefaultConstructor_19(L_28);
	}

IL_011c:
	{
		goto IL_0179;
	}

IL_011f:
	{
		Nullable_1_t3164635835  L_29 = __this->get__Mode_17();
		V_0 = L_29;
		int32_t L_30 = Nullable_1_GetValueOrDefault_m3171265427((&V_0), /*hidden argument*/Nullable_1_GetValueOrDefault_m3171265427_MethodInfo_var);
		if ((!(((uint32_t)L_30) == ((uint32_t)3))))
		{
			goto IL_0139;
		}
	}
	{
		bool L_31 = Nullable_1_get_HasValue_m2124201516((&V_0), /*hidden argument*/Nullable_1_get_HasValue_m2124201516_MethodInfo_var);
		G_B31_0 = ((int32_t)(L_31));
		goto IL_013a;
	}

IL_0139:
	{
		G_B31_0 = 0;
	}

IL_013a:
	{
		V_1 = (bool)((((int32_t)G_B31_0) == ((int32_t)0))? 1 : 0);
		bool L_32 = V_1;
		if (L_32)
		{
			goto IL_0179;
		}
	}
	{
		JsTypeAttribute_set_Native_m1509132479(__this, (bool)1, /*hidden argument*/NULL);
		JsTypeAttribute_set_Export_m2619437660(__this, (bool)0, /*hidden argument*/NULL);
		JsTypeAttribute_set_PropertiesAsFields_m1232173702(__this, (bool)1, /*hidden argument*/NULL);
		JsTypeAttribute_GoNative_m4128470995(__this, /*hidden argument*/NULL);
		Nullable_1_t560925241 * L_33 = __this->get_address_of__OmitCasts_2();
		bool L_34 = Nullable_1_get_HasValue_m1646262914(L_33, /*hidden argument*/Nullable_1_get_HasValue_m1646262914_MethodInfo_var);
		V_1 = L_34;
		bool L_35 = V_1;
		if (L_35)
		{
			goto IL_0178;
		}
	}
	{
		JsTypeAttribute_set_OmitCasts_m3509269971(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0178:
	{
	}

IL_0179:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
