﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2984635997.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_4202293321.h"

// System.Void System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitDecoder>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3026946156_gshared (InternalEnumerator_1_t2984635997 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3026946156(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2984635997 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3026946156_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitDecoder>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1143741876_gshared (InternalEnumerator_1_t2984635997 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1143741876(__this, method) ((  void (*) (InternalEnumerator_1_t2984635997 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1143741876_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitDecoder>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m129594144_gshared (InternalEnumerator_1_t2984635997 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m129594144(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2984635997 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m129594144_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitDecoder>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m781740355_gshared (InternalEnumerator_1_t2984635997 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m781740355(__this, method) ((  void (*) (InternalEnumerator_1_t2984635997 *, const MethodInfo*))InternalEnumerator_1_Dispose_m781740355_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitDecoder>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3571847200_gshared (InternalEnumerator_1_t2984635997 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3571847200(__this, method) ((  bool (*) (InternalEnumerator_1_t2984635997 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3571847200_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitDecoder>::get_Current()
extern "C"  BitDecoder_t4202293321  InternalEnumerator_1_get_Current_m2243560755_gshared (InternalEnumerator_1_t2984635997 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2243560755(__this, method) ((  BitDecoder_t4202293321  (*) (InternalEnumerator_1_t2984635997 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2243560755_gshared)(__this, method)
