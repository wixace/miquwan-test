﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UISpriteGenerated
struct UISpriteGenerated_t3448223510;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// UISprite
struct UISprite_t661437049;
// UIAtlas
struct UIAtlas_t281921111;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_UISprite661437049.h"
#include "AssemblyU2DCSharp_UIAtlas281921111.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"

// System.Void UISpriteGenerated::.ctor()
extern "C"  void UISpriteGenerated__ctor_m2421788437 (UISpriteGenerated_t3448223510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteGenerated::UISprite_UISprite1(JSVCall,System.Int32)
extern "C"  bool UISpriteGenerated_UISprite_UISprite1_m2825679325 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteGenerated::UISprite_Data(JSVCall)
extern "C"  void UISpriteGenerated_UISprite_Data_m3362556964 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteGenerated::UISprite_IsMathConvert(JSVCall)
extern "C"  void UISpriteGenerated_UISprite_IsMathConvert_m1955800733 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteGenerated::UISprite_material(JSVCall)
extern "C"  void UISpriteGenerated_UISprite_material_m2554654567 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteGenerated::UISprite_atlas(JSVCall)
extern "C"  void UISpriteGenerated_UISprite_atlas_m184654643 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteGenerated::UISprite_spriteName(JSVCall)
extern "C"  void UISpriteGenerated_UISprite_spriteName_m1389893054 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteGenerated::UISprite_isValid(JSVCall)
extern "C"  void UISpriteGenerated_UISprite_isValid_m1973620908 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteGenerated::UISprite_border(JSVCall)
extern "C"  void UISpriteGenerated_UISprite_border_m2493816834 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteGenerated::UISprite_pixelSize(JSVCall)
extern "C"  void UISpriteGenerated_UISprite_pixelSize_m3200974743 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteGenerated::UISprite_minWidth(JSVCall)
extern "C"  void UISpriteGenerated_UISprite_minWidth_m3796788378 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteGenerated::UISprite_minHeight(JSVCall)
extern "C"  void UISpriteGenerated_UISprite_minHeight_m2391766373 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteGenerated::UISprite_drawingDimensions(JSVCall)
extern "C"  void UISpriteGenerated_UISprite_drawingDimensions_m733239603 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteGenerated::UISprite_premultipliedAlpha(JSVCall)
extern "C"  void UISpriteGenerated_UISprite_premultipliedAlpha_m3394055494 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteGenerated::UISprite_GetAtlasSprite(JSVCall,System.Int32)
extern "C"  bool UISpriteGenerated_UISprite_GetAtlasSprite_m766688823 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteGenerated::UISprite_MakePixelPerfect(JSVCall,System.Int32)
extern "C"  bool UISpriteGenerated_UISprite_MakePixelPerfect_m2650404530 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteGenerated::UISprite_OnFill__BetterListT1_Vector3__BetterListT1_Vector2__BetterListT1_Color32(JSVCall,System.Int32)
extern "C"  bool UISpriteGenerated_UISprite_OnFill__BetterListT1_Vector3__BetterListT1_Vector2__BetterListT1_Color32_m2868490212 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteGenerated::__Register()
extern "C"  void UISpriteGenerated___Register_m1304367826 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UISpriteGenerated_ilo_attachFinalizerObject1_m4105800322 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteGenerated::ilo_setWhatever2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  void UISpriteGenerated_ilo_setWhatever2_m1825827461 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void UISpriteGenerated_ilo_setBooleanS3_m3732099332 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteGenerated::ilo_getBooleanS4(System.Int32)
extern "C"  bool UISpriteGenerated_ilo_getBooleanS4_m1872538098 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UISpriteGenerated::ilo_setObject5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UISpriteGenerated_ilo_setObject5_m973336125 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteGenerated::ilo_set_atlas6(UISprite,UIAtlas)
extern "C"  void UISpriteGenerated_ilo_set_atlas6_m742121862 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, UIAtlas_t281921111 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UISpriteGenerated::ilo_get_border7(UISprite)
extern "C"  Vector4_t4282066567  UISpriteGenerated_ilo_get_border7_m2860899170 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UISpriteGenerated::ilo_get_pixelSize8(UISprite)
extern "C"  float UISpriteGenerated_ilo_get_pixelSize8_m1347841527 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteGenerated::ilo_setInt329(System.Int32,System.Int32)
extern "C"  void UISpriteGenerated_ilo_setInt329_m589772377 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteGenerated::ilo_get_premultipliedAlpha10(UISprite)
extern "C"  bool UISpriteGenerated_ilo_get_premultipliedAlpha10_m1693251877 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteGenerated::ilo_MakePixelPerfect11(UISprite)
extern "C"  void UISpriteGenerated_ilo_MakePixelPerfect11_m1438187710 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UISpriteGenerated::ilo_getObject12(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UISpriteGenerated_ilo_getObject12_m3709855954 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
