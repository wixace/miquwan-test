﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_QualitySettingsGenerated
struct UnityEngine_QualitySettingsGenerated_t2333672149;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_QualitySettingsGenerated::.ctor()
extern "C"  void UnityEngine_QualitySettingsGenerated__ctor_m2587199334 (UnityEngine_QualitySettingsGenerated_t2333672149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QualitySettingsGenerated::QualitySettings_QualitySettings1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QualitySettingsGenerated_QualitySettings_QualitySettings1_m3160321422 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_names(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_names_m860244984 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_pixelLightCount(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_pixelLightCount_m3195801569 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_shadowProjection(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_shadowProjection_m2408933853 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_shadowCascades(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_shadowCascades_m575916909 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_shadowDistance(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_shadowDistance_m1181342839 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_shadowNearPlaneOffset(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_shadowNearPlaneOffset_m4045373081 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_shadowCascade2Split(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_shadowCascade2Split_m3904486340 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_shadowCascade4Split(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_shadowCascade4Split_m2624622662 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_masterTextureLimit(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_masterTextureLimit_m2527955562 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_anisotropicFiltering(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_anisotropicFiltering_m1358379699 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_lodBias(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_lodBias_m2182326118 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_maximumLODLevel(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_maximumLODLevel_m4231929373 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_particleRaycastBudget(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_particleRaycastBudget_m3984271640 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_softVegetation(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_softVegetation_m4214007860 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_realtimeReflectionProbes(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_realtimeReflectionProbes_m3857680467 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_billboardsFaceCameraPosition(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_billboardsFaceCameraPosition_m240499469 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_maxQueuedFrames(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_maxQueuedFrames_m2680591075 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_vSyncCount(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_vSyncCount_m2957951854 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_antiAliasing(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_antiAliasing_m896283896 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_desiredColorSpace(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_desiredColorSpace_m2515350083 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_activeColorSpace(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_activeColorSpace_m690038691 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_blendWeights(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_blendWeights_m626623330 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_asyncUploadTimeSlice(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_asyncUploadTimeSlice_m3440989924 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::QualitySettings_asyncUploadBufferSize(JSVCall)
extern "C"  void UnityEngine_QualitySettingsGenerated_QualitySettings_asyncUploadBufferSize_m598770722 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QualitySettingsGenerated::QualitySettings_DecreaseLevel__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QualitySettingsGenerated_QualitySettings_DecreaseLevel__Boolean_m1099471361 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QualitySettingsGenerated::QualitySettings_DecreaseLevel(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QualitySettingsGenerated_QualitySettings_DecreaseLevel_m3786450249 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QualitySettingsGenerated::QualitySettings_GetQualityLevel(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QualitySettingsGenerated_QualitySettings_GetQualityLevel_m2124671422 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QualitySettingsGenerated::QualitySettings_IncreaseLevel__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QualitySettingsGenerated_QualitySettings_IncreaseLevel__Boolean_m527666597 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QualitySettingsGenerated::QualitySettings_IncreaseLevel(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QualitySettingsGenerated_QualitySettings_IncreaseLevel_m168446245 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QualitySettingsGenerated::QualitySettings_SetQualityLevel__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QualitySettingsGenerated_QualitySettings_SetQualityLevel__Int32__Boolean_m2247094180 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QualitySettingsGenerated::QualitySettings_SetQualityLevel__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QualitySettingsGenerated_QualitySettings_SetQualityLevel__Int32_m1526389830 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::__Register()
extern "C"  void UnityEngine_QualitySettingsGenerated___Register_m4040598817 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_QualitySettingsGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_QualitySettingsGenerated_ilo_getObject1_m3771859052 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QualitySettingsGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_QualitySettingsGenerated_ilo_attachFinalizerObject2_m448871802 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::ilo_setStringS3(System.Int32,System.String)
extern "C"  void UnityEngine_QualitySettingsGenerated_ilo_setStringS3_m1773430833 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_QualitySettingsGenerated::ilo_getInt324(System.Int32)
extern "C"  int32_t UnityEngine_QualitySettingsGenerated_ilo_getInt324_m1720356464 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::ilo_setEnum5(System.Int32,System.Int32)
extern "C"  void UnityEngine_QualitySettingsGenerated_ilo_setEnum5_m4227373699 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::ilo_setInt326(System.Int32,System.Int32)
extern "C"  void UnityEngine_QualitySettingsGenerated_ilo_setInt326_m1262761483 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_QualitySettingsGenerated::ilo_getSingle7(System.Int32)
extern "C"  float UnityEngine_QualitySettingsGenerated_ilo_getSingle7_m747150599 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::ilo_setVector3S8(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_QualitySettingsGenerated_ilo_setVector3S8_m3328133480 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::ilo_setSingle9(System.Int32,System.Single)
extern "C"  void UnityEngine_QualitySettingsGenerated_ilo_setSingle9_m79416150 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QualitySettingsGenerated::ilo_setBooleanS10(System.Int32,System.Boolean)
extern "C"  void UnityEngine_QualitySettingsGenerated_ilo_setBooleanS10_m1012352719 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QualitySettingsGenerated::ilo_getBooleanS11(System.Int32)
extern "C"  bool UnityEngine_QualitySettingsGenerated_ilo_getBooleanS11_m3302258159 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
