﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RichPathPart
struct RichPathPart_t2520985130;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.RichPathPart::.ctor()
extern "C"  void RichPathPart__ctor_m1377773437 (RichPathPart_t2520985130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichPathPart::Pathfinding.Util.IAstarPooledObject.OnEnterPool()
extern "C"  void RichPathPart_Pathfinding_Util_IAstarPooledObject_OnEnterPool_m1641567560 (RichPathPart_t2520985130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
