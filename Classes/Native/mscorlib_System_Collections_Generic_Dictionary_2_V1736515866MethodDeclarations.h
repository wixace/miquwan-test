﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va746493984MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Mesh,UnityEngine.Vector3[]>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m3433958601(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1736515866 *, Dictionary_2_t3035910153 *, const MethodInfo*))ValueCollection__ctor_m4177258586_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Mesh,UnityEngine.Vector3[]>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1365701193(__this, ___item0, method) ((  void (*) (ValueCollection_t1736515866 *, Vector3U5BU5D_t215400611*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1528225944_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Mesh,UnityEngine.Vector3[]>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m498822034(__this, method) ((  void (*) (ValueCollection_t1736515866 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2827278689_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Mesh,UnityEngine.Vector3[]>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1868611649(__this, ___item0, method) ((  bool (*) (ValueCollection_t1736515866 *, Vector3U5BU5D_t215400611*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2015709838_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Mesh,UnityEngine.Vector3[]>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3158242598(__this, ___item0, method) ((  bool (*) (ValueCollection_t1736515866 *, Vector3U5BU5D_t215400611*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3578506931_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Mesh,UnityEngine.Vector3[]>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3151823570(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1736515866 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4041606511_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Mesh,UnityEngine.Vector3[]>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1029062166(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1736515866 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1709700389_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Mesh,UnityEngine.Vector3[]>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1481323685(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1736515866 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2843387488_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Mesh,UnityEngine.Vector3[]>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m143787508(__this, method) ((  bool (*) (ValueCollection_t1736515866 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m290885697_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Mesh,UnityEngine.Vector3[]>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4279933396(__this, method) ((  bool (*) (ValueCollection_t1736515866 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1930063777_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Mesh,UnityEngine.Vector3[]>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3173834438(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1736515866 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1874108365_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Mesh,UnityEngine.Vector3[]>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1706108624(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1736515866 *, Vector3U5BU5DU5BU5D_t3570135410*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1735386657_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Mesh,UnityEngine.Vector3[]>::GetEnumerator()
#define ValueCollection_GetEnumerator_m4051730105(__this, method) ((  Enumerator_t967743561  (*) (ValueCollection_t1736515866 *, const MethodInfo*))ValueCollection_GetEnumerator_m1204216004_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Mesh,UnityEngine.Vector3[]>::get_Count()
#define ValueCollection_get_Count_m1834861070(__this, method) ((  int32_t (*) (ValueCollection_t1736515866 *, const MethodInfo*))ValueCollection_get_Count_m2709231847_gshared)(__this, method)
