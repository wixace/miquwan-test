﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// T4MPlantObjSC
struct T4MPlantObjSC_t1634250665;

#include "codegen/il2cpp-codegen.h"

// System.Void T4MPlantObjSC::.ctor()
extern "C"  void T4MPlantObjSC__ctor_m321011682 (T4MPlantObjSC_t1634250665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
