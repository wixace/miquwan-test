﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIModelDisplayMgr/<CreatHeroModel>c__AnonStorey160
struct U3CCreatHeroModelU3Ec__AnonStorey160_t3837456612;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"

// System.Void UIModelDisplayMgr/<CreatHeroModel>c__AnonStorey160::.ctor()
extern "C"  void U3CCreatHeroModelU3Ec__AnonStorey160__ctor_m1350266935 (U3CCreatHeroModelU3Ec__AnonStorey160_t3837456612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplayMgr/<CreatHeroModel>c__AnonStorey160::<>m__3FD(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  void U3CCreatHeroModelU3Ec__AnonStorey160_U3CU3Em__3FD_m2907911738 (U3CCreatHeroModelU3Ec__AnonStorey160_t3837456612 * __this, AssetOperation_t778728221 * ___ao0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
