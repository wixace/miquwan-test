﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_NavMeshTriangulationGenerated
struct UnityEngine_NavMeshTriangulationGenerated_t2117342248;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_NavMeshTriangulationGenerated::.ctor()
extern "C"  void UnityEngine_NavMeshTriangulationGenerated__ctor_m3892940355 (UnityEngine_NavMeshTriangulationGenerated_t2117342248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshTriangulationGenerated::.cctor()
extern "C"  void UnityEngine_NavMeshTriangulationGenerated__cctor_m4234937802 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshTriangulationGenerated::NavMeshTriangulation_NavMeshTriangulation1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshTriangulationGenerated_NavMeshTriangulation_NavMeshTriangulation1_m1419087279 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshTriangulationGenerated::NavMeshTriangulation_vertices(JSVCall)
extern "C"  void UnityEngine_NavMeshTriangulationGenerated_NavMeshTriangulation_vertices_m2059192013 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshTriangulationGenerated::NavMeshTriangulation_indices(JSVCall)
extern "C"  void UnityEngine_NavMeshTriangulationGenerated_NavMeshTriangulation_indices_m3544305823 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshTriangulationGenerated::NavMeshTriangulation_areas(JSVCall)
extern "C"  void UnityEngine_NavMeshTriangulationGenerated_NavMeshTriangulation_areas_m3043736992 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshTriangulationGenerated::__Register()
extern "C"  void UnityEngine_NavMeshTriangulationGenerated___Register_m3719651428 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine_NavMeshTriangulationGenerated::<NavMeshTriangulation_vertices>m__27F()
extern "C"  Vector3U5BU5D_t215400611* UnityEngine_NavMeshTriangulationGenerated_U3CNavMeshTriangulation_verticesU3Em__27F_m1654247950 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] UnityEngine_NavMeshTriangulationGenerated::<NavMeshTriangulation_indices>m__280()
extern "C"  Int32U5BU5D_t3230847821* UnityEngine_NavMeshTriangulationGenerated_U3CNavMeshTriangulation_indicesU3Em__280_m2997689837 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] UnityEngine_NavMeshTriangulationGenerated::<NavMeshTriangulation_areas>m__281()
extern "C"  Int32U5BU5D_t3230847821* UnityEngine_NavMeshTriangulationGenerated_U3CNavMeshTriangulation_areasU3Em__281_m2290173103 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_NavMeshTriangulationGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_NavMeshTriangulationGenerated_ilo_getObject1_m3284201811 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshTriangulationGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_NavMeshTriangulationGenerated_ilo_attachFinalizerObject2_m1935896405 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshTriangulationGenerated::ilo_moveSaveID2Arr3(System.Int32)
extern "C"  void UnityEngine_NavMeshTriangulationGenerated_ilo_moveSaveID2Arr3_m1798681370 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshTriangulationGenerated::ilo_setArrayS4(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_NavMeshTriangulationGenerated_ilo_setArrayS4_m4098148707 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshTriangulationGenerated::ilo_changeJSObj5(System.Int32,System.Object)
extern "C"  void UnityEngine_NavMeshTriangulationGenerated_ilo_changeJSObj5_m442558218 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_NavMeshTriangulationGenerated::ilo_getInt326(System.Int32)
extern "C"  int32_t UnityEngine_NavMeshTriangulationGenerated_ilo_getInt326_m147667243 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_NavMeshTriangulationGenerated::ilo_getArrayLength7(System.Int32)
extern "C"  int32_t UnityEngine_NavMeshTriangulationGenerated_ilo_getArrayLength7_m1882641019 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
