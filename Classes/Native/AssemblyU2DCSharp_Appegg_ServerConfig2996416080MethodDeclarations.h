﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Appegg/ServerConfig
struct ServerConfig_t2996416080;

#include "codegen/il2cpp-codegen.h"

// System.Void Appegg/ServerConfig::.ctor()
extern "C"  void ServerConfig__ctor_m226197275 (ServerConfig_t2996416080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
