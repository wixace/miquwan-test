﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"

// System.Void Mihua.SDK.PayInfo::.ctor()
extern "C"  void PayInfo__ctor_m2410273439 (PayInfo_t1775308120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.PayInfo Mihua.SDK.PayInfo::Parse(System.Object[])
extern "C"  PayInfo_t1775308120 * PayInfo_Parse_m2150087515 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___payOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
