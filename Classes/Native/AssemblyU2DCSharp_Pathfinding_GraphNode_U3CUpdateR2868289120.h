﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.PathHandler
struct PathHandler_t918952263;
// Pathfinding.PathNode
struct PathNode_t417131581;
// Pathfinding.Path
struct Path_t1974241691;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.GraphNode/<UpdateRecursiveG>c__AnonStorey109
struct  U3CUpdateRecursiveGU3Ec__AnonStorey109_t2868289120  : public Il2CppObject
{
public:
	// Pathfinding.PathHandler Pathfinding.GraphNode/<UpdateRecursiveG>c__AnonStorey109::handler
	PathHandler_t918952263 * ___handler_0;
	// Pathfinding.PathNode Pathfinding.GraphNode/<UpdateRecursiveG>c__AnonStorey109::pathNode
	PathNode_t417131581 * ___pathNode_1;
	// Pathfinding.Path Pathfinding.GraphNode/<UpdateRecursiveG>c__AnonStorey109::path
	Path_t1974241691 * ___path_2;

public:
	inline static int32_t get_offset_of_handler_0() { return static_cast<int32_t>(offsetof(U3CUpdateRecursiveGU3Ec__AnonStorey109_t2868289120, ___handler_0)); }
	inline PathHandler_t918952263 * get_handler_0() const { return ___handler_0; }
	inline PathHandler_t918952263 ** get_address_of_handler_0() { return &___handler_0; }
	inline void set_handler_0(PathHandler_t918952263 * value)
	{
		___handler_0 = value;
		Il2CppCodeGenWriteBarrier(&___handler_0, value);
	}

	inline static int32_t get_offset_of_pathNode_1() { return static_cast<int32_t>(offsetof(U3CUpdateRecursiveGU3Ec__AnonStorey109_t2868289120, ___pathNode_1)); }
	inline PathNode_t417131581 * get_pathNode_1() const { return ___pathNode_1; }
	inline PathNode_t417131581 ** get_address_of_pathNode_1() { return &___pathNode_1; }
	inline void set_pathNode_1(PathNode_t417131581 * value)
	{
		___pathNode_1 = value;
		Il2CppCodeGenWriteBarrier(&___pathNode_1, value);
	}

	inline static int32_t get_offset_of_path_2() { return static_cast<int32_t>(offsetof(U3CUpdateRecursiveGU3Ec__AnonStorey109_t2868289120, ___path_2)); }
	inline Path_t1974241691 * get_path_2() const { return ___path_2; }
	inline Path_t1974241691 ** get_address_of_path_2() { return &___path_2; }
	inline void set_path_2(Path_t1974241691 * value)
	{
		___path_2 = value;
		Il2CppCodeGenWriteBarrier(&___path_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
