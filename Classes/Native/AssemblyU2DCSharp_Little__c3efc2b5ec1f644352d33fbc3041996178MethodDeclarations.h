﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._c3efc2b5ec1f644352d33fbc5d9b29b9
struct _c3efc2b5ec1f644352d33fbc5d9b29b9_t3041996178;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__c3efc2b5ec1f644352d33fbc3041996178.h"

// System.Void Little._c3efc2b5ec1f644352d33fbc5d9b29b9::.ctor()
extern "C"  void _c3efc2b5ec1f644352d33fbc5d9b29b9__ctor_m2891390171 (_c3efc2b5ec1f644352d33fbc5d9b29b9_t3041996178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c3efc2b5ec1f644352d33fbc5d9b29b9::_c3efc2b5ec1f644352d33fbc5d9b29b9m2(System.Int32)
extern "C"  int32_t _c3efc2b5ec1f644352d33fbc5d9b29b9__c3efc2b5ec1f644352d33fbc5d9b29b9m2_m3027277913 (_c3efc2b5ec1f644352d33fbc5d9b29b9_t3041996178 * __this, int32_t ____c3efc2b5ec1f644352d33fbc5d9b29b9a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c3efc2b5ec1f644352d33fbc5d9b29b9::_c3efc2b5ec1f644352d33fbc5d9b29b9m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _c3efc2b5ec1f644352d33fbc5d9b29b9__c3efc2b5ec1f644352d33fbc5d9b29b9m_m460662397 (_c3efc2b5ec1f644352d33fbc5d9b29b9_t3041996178 * __this, int32_t ____c3efc2b5ec1f644352d33fbc5d9b29b9a0, int32_t ____c3efc2b5ec1f644352d33fbc5d9b29b9541, int32_t ____c3efc2b5ec1f644352d33fbc5d9b29b9c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c3efc2b5ec1f644352d33fbc5d9b29b9::ilo__c3efc2b5ec1f644352d33fbc5d9b29b9m21(Little._c3efc2b5ec1f644352d33fbc5d9b29b9,System.Int32)
extern "C"  int32_t _c3efc2b5ec1f644352d33fbc5d9b29b9_ilo__c3efc2b5ec1f644352d33fbc5d9b29b9m21_m2612083609 (Il2CppObject * __this /* static, unused */, _c3efc2b5ec1f644352d33fbc5d9b29b9_t3041996178 * ____this0, int32_t ____c3efc2b5ec1f644352d33fbc5d9b29b9a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
