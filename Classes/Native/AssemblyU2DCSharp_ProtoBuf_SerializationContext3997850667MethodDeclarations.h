﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.SerializationContext
struct SerializationContext_t3997850667;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_SerializationContext3997850667.h"

// System.Void ProtoBuf.SerializationContext::.ctor()
extern "C"  void SerializationContext__ctor_m3875330409 (SerializationContext_t3997850667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.SerializationContext::.cctor()
extern "C"  void SerializationContext__cctor_m3689029476 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.SerializationContext::Freeze()
extern "C"  void SerializationContext_Freeze_m3038178578 (SerializationContext_t3997850667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.SerializationContext::ThrowIfFrozen()
extern "C"  void SerializationContext_ThrowIfFrozen_m1420616426 (SerializationContext_t3997850667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.SerializationContext::get_Context()
extern "C"  Il2CppObject * SerializationContext_get_Context_m103427970 (SerializationContext_t3997850667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.SerializationContext::set_Context(System.Object)
extern "C"  void SerializationContext_set_Context_m2379955675 (SerializationContext_t3997850667 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.SerializationContext ProtoBuf.SerializationContext::get_Default()
extern "C"  SerializationContext_t3997850667 * SerializationContext_get_Default_m3768040880 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.SerializationContext::ilo_ThrowIfFrozen1(ProtoBuf.SerializationContext)
extern "C"  void SerializationContext_ilo_ThrowIfFrozen1_m3771441108 (Il2CppObject * __this /* static, unused */, SerializationContext_t3997850667 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
