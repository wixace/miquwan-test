﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t2376705138;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t3986041494;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Skin
struct  Skin_t2578845  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Texture2D[] Skin::skins
	Texture2DU5BU5D_t2376705138* ___skins_2;
	// UnityEngine.SkinnedMeshRenderer Skin::mrenderer
	SkinnedMeshRenderer_t3986041494 * ___mrenderer_3;

public:
	inline static int32_t get_offset_of_skins_2() { return static_cast<int32_t>(offsetof(Skin_t2578845, ___skins_2)); }
	inline Texture2DU5BU5D_t2376705138* get_skins_2() const { return ___skins_2; }
	inline Texture2DU5BU5D_t2376705138** get_address_of_skins_2() { return &___skins_2; }
	inline void set_skins_2(Texture2DU5BU5D_t2376705138* value)
	{
		___skins_2 = value;
		Il2CppCodeGenWriteBarrier(&___skins_2, value);
	}

	inline static int32_t get_offset_of_mrenderer_3() { return static_cast<int32_t>(offsetof(Skin_t2578845, ___mrenderer_3)); }
	inline SkinnedMeshRenderer_t3986041494 * get_mrenderer_3() const { return ___mrenderer_3; }
	inline SkinnedMeshRenderer_t3986041494 ** get_address_of_mrenderer_3() { return &___mrenderer_3; }
	inline void set_mrenderer_3(SkinnedMeshRenderer_t3986041494 * value)
	{
		___mrenderer_3 = value;
		Il2CppCodeGenWriteBarrier(&___mrenderer_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
