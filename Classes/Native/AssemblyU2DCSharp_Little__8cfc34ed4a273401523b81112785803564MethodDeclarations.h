﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._8cfc34ed4a273401523b81116536e786
struct _8cfc34ed4a273401523b81116536e786_t2785803564;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__8cfc34ed4a273401523b81112785803564.h"

// System.Void Little._8cfc34ed4a273401523b81116536e786::.ctor()
extern "C"  void _8cfc34ed4a273401523b81116536e786__ctor_m1132673665 (_8cfc34ed4a273401523b81116536e786_t2785803564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8cfc34ed4a273401523b81116536e786::_8cfc34ed4a273401523b81116536e786m2(System.Int32)
extern "C"  int32_t _8cfc34ed4a273401523b81116536e786__8cfc34ed4a273401523b81116536e786m2_m3288506905 (_8cfc34ed4a273401523b81116536e786_t2785803564 * __this, int32_t ____8cfc34ed4a273401523b81116536e786a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8cfc34ed4a273401523b81116536e786::_8cfc34ed4a273401523b81116536e786m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _8cfc34ed4a273401523b81116536e786__8cfc34ed4a273401523b81116536e786m_m1527616701 (_8cfc34ed4a273401523b81116536e786_t2785803564 * __this, int32_t ____8cfc34ed4a273401523b81116536e786a0, int32_t ____8cfc34ed4a273401523b81116536e786271, int32_t ____8cfc34ed4a273401523b81116536e786c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8cfc34ed4a273401523b81116536e786::ilo__8cfc34ed4a273401523b81116536e786m21(Little._8cfc34ed4a273401523b81116536e786,System.Int32)
extern "C"  int32_t _8cfc34ed4a273401523b81116536e786_ilo__8cfc34ed4a273401523b81116536e786m21_m1770114163 (Il2CppObject * __this /* static, unused */, _8cfc34ed4a273401523b81116536e786_t2785803564 * ____this0, int32_t ____8cfc34ed4a273401523b81116536e786a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
