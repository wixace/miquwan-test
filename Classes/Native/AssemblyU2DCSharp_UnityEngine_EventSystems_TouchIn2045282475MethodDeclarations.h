﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_TouchInputModuleGenerated
struct UnityEngine_EventSystems_TouchInputModuleGenerated_t2045282475;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine_EventSystems_TouchInputModuleGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_TouchInputModuleGenerated__ctor_m1958651984 (UnityEngine_EventSystems_TouchInputModuleGenerated_t2045282475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_TouchInputModuleGenerated::TouchInputModule_forceModuleActive(JSVCall)
extern "C"  void UnityEngine_EventSystems_TouchInputModuleGenerated_TouchInputModule_forceModuleActive_m4235864094 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_TouchInputModuleGenerated::TouchInputModule_DeactivateModule(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_TouchInputModuleGenerated_TouchInputModule_DeactivateModule_m4124378002 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_TouchInputModuleGenerated::TouchInputModule_IsModuleSupported(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_TouchInputModuleGenerated_TouchInputModule_IsModuleSupported_m3793228232 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_TouchInputModuleGenerated::TouchInputModule_Process(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_TouchInputModuleGenerated_TouchInputModule_Process_m2347955871 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_TouchInputModuleGenerated::TouchInputModule_ShouldActivateModule(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_TouchInputModuleGenerated_TouchInputModule_ShouldActivateModule_m530753380 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_TouchInputModuleGenerated::TouchInputModule_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_TouchInputModuleGenerated_TouchInputModule_ToString_m2635289086 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_TouchInputModuleGenerated::TouchInputModule_UpdateModule(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_TouchInputModuleGenerated_TouchInputModule_UpdateModule_m659141351 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_TouchInputModuleGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_TouchInputModuleGenerated___Register_m1850705527 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_TouchInputModuleGenerated::ilo_getBooleanS1(System.Int32)
extern "C"  bool UnityEngine_EventSystems_TouchInputModuleGenerated_ilo_getBooleanS1_m2216954236 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_TouchInputModuleGenerated::ilo_setBooleanS2(System.Int32,System.Boolean)
extern "C"  void UnityEngine_EventSystems_TouchInputModuleGenerated_ilo_setBooleanS2_m3117021792 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_TouchInputModuleGenerated::ilo_setStringS3(System.Int32,System.String)
extern "C"  void UnityEngine_EventSystems_TouchInputModuleGenerated_ilo_setStringS3_m1704254491 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
