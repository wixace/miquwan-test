﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_MessageType2182326183.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pomelo.DotNetClient.Message
struct  Message_t686303821  : public Il2CppObject
{
public:
	// Pomelo.DotNetClient.MessageType Pomelo.DotNetClient.Message::type
	int32_t ___type_0;
	// System.String Pomelo.DotNetClient.Message::route
	String_t* ___route_1;
	// System.UInt32 Pomelo.DotNetClient.Message::id
	uint32_t ___id_2;
	// Newtonsoft.Json.Linq.JObject Pomelo.DotNetClient.Message::data
	JObject_t1798544199 * ___data_3;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(Message_t686303821, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_route_1() { return static_cast<int32_t>(offsetof(Message_t686303821, ___route_1)); }
	inline String_t* get_route_1() const { return ___route_1; }
	inline String_t** get_address_of_route_1() { return &___route_1; }
	inline void set_route_1(String_t* value)
	{
		___route_1 = value;
		Il2CppCodeGenWriteBarrier(&___route_1, value);
	}

	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(Message_t686303821, ___id_2)); }
	inline uint32_t get_id_2() const { return ___id_2; }
	inline uint32_t* get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(uint32_t value)
	{
		___id_2 = value;
	}

	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(Message_t686303821, ___data_3)); }
	inline JObject_t1798544199 * get_data_3() const { return ___data_3; }
	inline JObject_t1798544199 ** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(JObject_t1798544199 * value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier(&___data_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
