﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.Int64Serializer
struct Int64Serializer_t3534721049;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"

// System.Void ProtoBuf.Serializers.Int64Serializer::.ctor(ProtoBuf.Meta.TypeModel)
extern "C"  void Int64Serializer__ctor_m70722525 (Int64Serializer_t3534721049 * __this, TypeModel_t2730011105 * ___model0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.Int64Serializer::.cctor()
extern "C"  void Int64Serializer__cctor_m3790772001 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.Int64Serializer::ProtoBuf.Serializers.IProtoSerializer.get_RequiresOldValue()
extern "C"  bool Int64Serializer_ProtoBuf_Serializers_IProtoSerializer_get_RequiresOldValue_m3479926970 (Int64Serializer_t3534721049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.Int64Serializer::ProtoBuf.Serializers.IProtoSerializer.get_ReturnsValue()
extern "C"  bool Int64Serializer_ProtoBuf_Serializers_IProtoSerializer_get_ReturnsValue_m2850347088 (Int64Serializer_t3534721049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.Int64Serializer::get_ExpectedType()
extern "C"  Type_t * Int64Serializer_get_ExpectedType_m4171191789 (Int64Serializer_t3534721049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.Int64Serializer::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * Int64Serializer_Read_m4047626093 (Int64Serializer_t3534721049 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.Int64Serializer::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void Int64Serializer_Write_m722353283 (Int64Serializer_t3534721049 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.Int64Serializer::ilo_WriteInt641(System.Int64,ProtoBuf.ProtoWriter)
extern "C"  void Int64Serializer_ilo_WriteInt641_m1405994690 (Il2CppObject * __this /* static, unused */, int64_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
