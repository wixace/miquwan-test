﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEventListenerGenerated/<UIEventListener_onHover_GetDelegate_member4_arg0>c__AnonStoreyB8
struct U3CUIEventListener_onHover_GetDelegate_member4_arg0U3Ec__AnonStoreyB8_t3237322667;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UIEventListenerGenerated/<UIEventListener_onHover_GetDelegate_member4_arg0>c__AnonStoreyB8::.ctor()
extern "C"  void U3CUIEventListener_onHover_GetDelegate_member4_arg0U3Ec__AnonStoreyB8__ctor_m528972112 (U3CUIEventListener_onHover_GetDelegate_member4_arg0U3Ec__AnonStoreyB8_t3237322667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated/<UIEventListener_onHover_GetDelegate_member4_arg0>c__AnonStoreyB8::<>m__134(UnityEngine.GameObject,System.Boolean)
extern "C"  void U3CUIEventListener_onHover_GetDelegate_member4_arg0U3Ec__AnonStoreyB8_U3CU3Em__134_m3919355690 (U3CUIEventListener_onHover_GetDelegate_member4_arg0U3Ec__AnonStoreyB8_t3237322667 * __this, GameObject_t3674682005 * ___go0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
