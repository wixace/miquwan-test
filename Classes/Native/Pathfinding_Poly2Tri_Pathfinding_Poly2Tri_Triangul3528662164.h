﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Pathfinding.Poly2Tri.DelaunayTriangle>
struct List_1_t4203289139;
// System.Collections.Generic.List`1<Pathfinding.Poly2Tri.TriangulationPoint>
struct List_1_t883301189;
// Pathfinding.Poly2Tri.TriangulationDebugContext
struct TriangulationDebugContext_t3598090913;
// Pathfinding.Poly2Tri.Triangulatable
struct Triangulatable_t923279207;

#include "mscorlib_System_Object4170816371.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul1170581608.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Poly2Tri.TriangulationContext
struct  TriangulationContext_t3528662164  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<Pathfinding.Poly2Tri.DelaunayTriangle> Pathfinding.Poly2Tri.TriangulationContext::Triangles
	List_1_t4203289139 * ___Triangles_0;
	// System.Collections.Generic.List`1<Pathfinding.Poly2Tri.TriangulationPoint> Pathfinding.Poly2Tri.TriangulationContext::Points
	List_1_t883301189 * ___Points_1;
	// Pathfinding.Poly2Tri.TriangulationDebugContext Pathfinding.Poly2Tri.TriangulationContext::<DebugContext>k__BackingField
	TriangulationDebugContext_t3598090913 * ___U3CDebugContextU3Ek__BackingField_2;
	// Pathfinding.Poly2Tri.TriangulationMode Pathfinding.Poly2Tri.TriangulationContext::<TriangulationMode>k__BackingField
	int32_t ___U3CTriangulationModeU3Ek__BackingField_3;
	// Pathfinding.Poly2Tri.Triangulatable Pathfinding.Poly2Tri.TriangulationContext::<Triangulatable>k__BackingField
	Il2CppObject * ___U3CTriangulatableU3Ek__BackingField_4;
	// System.Int32 Pathfinding.Poly2Tri.TriangulationContext::<StepCount>k__BackingField
	int32_t ___U3CStepCountU3Ek__BackingField_5;
	// System.Boolean Pathfinding.Poly2Tri.TriangulationContext::<IsDebugEnabled>k__BackingField
	bool ___U3CIsDebugEnabledU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_Triangles_0() { return static_cast<int32_t>(offsetof(TriangulationContext_t3528662164, ___Triangles_0)); }
	inline List_1_t4203289139 * get_Triangles_0() const { return ___Triangles_0; }
	inline List_1_t4203289139 ** get_address_of_Triangles_0() { return &___Triangles_0; }
	inline void set_Triangles_0(List_1_t4203289139 * value)
	{
		___Triangles_0 = value;
		Il2CppCodeGenWriteBarrier(&___Triangles_0, value);
	}

	inline static int32_t get_offset_of_Points_1() { return static_cast<int32_t>(offsetof(TriangulationContext_t3528662164, ___Points_1)); }
	inline List_1_t883301189 * get_Points_1() const { return ___Points_1; }
	inline List_1_t883301189 ** get_address_of_Points_1() { return &___Points_1; }
	inline void set_Points_1(List_1_t883301189 * value)
	{
		___Points_1 = value;
		Il2CppCodeGenWriteBarrier(&___Points_1, value);
	}

	inline static int32_t get_offset_of_U3CDebugContextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TriangulationContext_t3528662164, ___U3CDebugContextU3Ek__BackingField_2)); }
	inline TriangulationDebugContext_t3598090913 * get_U3CDebugContextU3Ek__BackingField_2() const { return ___U3CDebugContextU3Ek__BackingField_2; }
	inline TriangulationDebugContext_t3598090913 ** get_address_of_U3CDebugContextU3Ek__BackingField_2() { return &___U3CDebugContextU3Ek__BackingField_2; }
	inline void set_U3CDebugContextU3Ek__BackingField_2(TriangulationDebugContext_t3598090913 * value)
	{
		___U3CDebugContextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDebugContextU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CTriangulationModeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TriangulationContext_t3528662164, ___U3CTriangulationModeU3Ek__BackingField_3)); }
	inline int32_t get_U3CTriangulationModeU3Ek__BackingField_3() const { return ___U3CTriangulationModeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CTriangulationModeU3Ek__BackingField_3() { return &___U3CTriangulationModeU3Ek__BackingField_3; }
	inline void set_U3CTriangulationModeU3Ek__BackingField_3(int32_t value)
	{
		___U3CTriangulationModeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CTriangulatableU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TriangulationContext_t3528662164, ___U3CTriangulatableU3Ek__BackingField_4)); }
	inline Il2CppObject * get_U3CTriangulatableU3Ek__BackingField_4() const { return ___U3CTriangulatableU3Ek__BackingField_4; }
	inline Il2CppObject ** get_address_of_U3CTriangulatableU3Ek__BackingField_4() { return &___U3CTriangulatableU3Ek__BackingField_4; }
	inline void set_U3CTriangulatableU3Ek__BackingField_4(Il2CppObject * value)
	{
		___U3CTriangulatableU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTriangulatableU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CStepCountU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TriangulationContext_t3528662164, ___U3CStepCountU3Ek__BackingField_5)); }
	inline int32_t get_U3CStepCountU3Ek__BackingField_5() const { return ___U3CStepCountU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CStepCountU3Ek__BackingField_5() { return &___U3CStepCountU3Ek__BackingField_5; }
	inline void set_U3CStepCountU3Ek__BackingField_5(int32_t value)
	{
		___U3CStepCountU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CIsDebugEnabledU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TriangulationContext_t3528662164, ___U3CIsDebugEnabledU3Ek__BackingField_6)); }
	inline bool get_U3CIsDebugEnabledU3Ek__BackingField_6() const { return ___U3CIsDebugEnabledU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsDebugEnabledU3Ek__BackingField_6() { return &___U3CIsDebugEnabledU3Ek__BackingField_6; }
	inline void set_U3CIsDebugEnabledU3Ek__BackingField_6(bool value)
	{
		___U3CIsDebugEnabledU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
