﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>
struct ReadOnlyCollection_1_t2036672360;
// System.Collections.Generic.IList`1<Core.RpsResult>
struct IList_1_t3174242027;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Core.RpsResult[]
struct RpsResultU5BU5D_t1451851929;
// System.Collections.Generic.IEnumerator`1<Core.RpsResult>
struct IEnumerator_1_t2391459873;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Core_RpsResult479594824.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2550904493_gshared (ReadOnlyCollection_1_t2036672360 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2550904493(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2036672360 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2550904493_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2661992983_gshared (ReadOnlyCollection_1_t2036672360 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2661992983(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2036672360 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2661992983_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2924094867_gshared (ReadOnlyCollection_1_t2036672360 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2924094867(__this, method) ((  void (*) (ReadOnlyCollection_1_t2036672360 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2924094867_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1228856382_gshared (ReadOnlyCollection_1_t2036672360 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1228856382(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2036672360 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1228856382_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1555400384_gshared (ReadOnlyCollection_1_t2036672360 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1555400384(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2036672360 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1555400384_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3397676548_gshared (ReadOnlyCollection_1_t2036672360 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3397676548(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2036672360 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3397676548_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3735475498_gshared (ReadOnlyCollection_1_t2036672360 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3735475498(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2036672360 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3735475498_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1624573333_gshared (ReadOnlyCollection_1_t2036672360 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1624573333(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2036672360 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1624573333_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m285286095_gshared (ReadOnlyCollection_1_t2036672360 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m285286095(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2036672360 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m285286095_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2192863452_gshared (ReadOnlyCollection_1_t2036672360 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2192863452(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2036672360 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2192863452_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m602238955_gshared (ReadOnlyCollection_1_t2036672360 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m602238955(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2036672360 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m602238955_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3744527922_gshared (ReadOnlyCollection_1_t2036672360 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3744527922(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2036672360 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3744527922_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2302406634_gshared (ReadOnlyCollection_1_t2036672360 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2302406634(__this, method) ((  void (*) (ReadOnlyCollection_1_t2036672360 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2302406634_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2049751118_gshared (ReadOnlyCollection_1_t2036672360 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2049751118(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2036672360 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2049751118_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m63357834_gshared (ReadOnlyCollection_1_t2036672360 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m63357834(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2036672360 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m63357834_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1249802749_gshared (ReadOnlyCollection_1_t2036672360 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1249802749(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2036672360 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1249802749_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2386933771_gshared (ReadOnlyCollection_1_t2036672360 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2386933771(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2036672360 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2386933771_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2565858573_gshared (ReadOnlyCollection_1_t2036672360 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2565858573(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2036672360 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2565858573_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2580485198_gshared (ReadOnlyCollection_1_t2036672360 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2580485198(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2036672360 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2580485198_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1853257536_gshared (ReadOnlyCollection_1_t2036672360 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1853257536(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2036672360 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1853257536_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1694513725_gshared (ReadOnlyCollection_1_t2036672360 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1694513725(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2036672360 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1694513725_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2309785884_gshared (ReadOnlyCollection_1_t2036672360 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2309785884(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2036672360 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2309785884_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3929643207_gshared (ReadOnlyCollection_1_t2036672360 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3929643207(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2036672360 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3929643207_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3378518676_gshared (ReadOnlyCollection_1_t2036672360 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3378518676(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2036672360 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3378518676_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1754452997_gshared (ReadOnlyCollection_1_t2036672360 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1754452997(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2036672360 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m1754452997_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2352493127_gshared (ReadOnlyCollection_1_t2036672360 * __this, RpsResultU5BU5D_t1451851929* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m2352493127(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2036672360 *, RpsResultU5BU5D_t1451851929*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m2352493127_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m1190253532_gshared (ReadOnlyCollection_1_t2036672360 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1190253532(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2036672360 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1190253532_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2121471571_gshared (ReadOnlyCollection_1_t2036672360 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2121471571(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2036672360 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2121471571_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1158416776_gshared (ReadOnlyCollection_1_t2036672360 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1158416776(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2036672360 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1158416776_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsResult>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m1349633002_gshared (ReadOnlyCollection_1_t2036672360 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1349633002(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2036672360 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1349633002_gshared)(__this, ___index0, method)
