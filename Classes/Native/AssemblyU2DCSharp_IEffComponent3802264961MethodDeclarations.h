﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IEffComponent
struct IEffComponent_t3802264961;
// EffectCtrl
struct EffectCtrl_t3708787644;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_EffectCtrl3708787644.h"

// System.Void IEffComponent::.ctor()
extern "C"  void IEffComponent__ctor_m3414035722 (IEffComponent_t3802264961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EffectCtrl IEffComponent::get_effCtrl()
extern "C"  EffectCtrl_t3708787644 * IEffComponent_get_effCtrl_m2505811728 (IEffComponent_t3802264961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IEffComponent::set_effCtrl(EffectCtrl)
extern "C"  void IEffComponent_set_effCtrl_m3089382047 (IEffComponent_t3802264961 * __this, EffectCtrl_t3708787644 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
