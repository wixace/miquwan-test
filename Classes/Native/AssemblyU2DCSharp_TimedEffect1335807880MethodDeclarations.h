﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimedEffect
struct TimedEffect_t1335807880;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_TimedEffect1335807880.h"

// System.Void TimedEffect::.ctor()
extern "C"  void TimedEffect__ctor_m3737756899 (TimedEffect_t1335807880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedEffect::set_lasttime(System.Single)
extern "C"  void TimedEffect_set_lasttime_m1911071178 (TimedEffect_t1335807880 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TimedEffect::get_lasttime()
extern "C"  float TimedEffect_get_lasttime_m4271991233 (TimedEffect_t1335807880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedEffect::set_delaytime(System.Single)
extern "C"  void TimedEffect_set_delaytime_m4080503191 (TimedEffect_t1335807880 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TimedEffect::get_delaytime()
extern "C"  float TimedEffect_get_delaytime_m1460093012 (TimedEffect_t1335807880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedEffect::Start()
extern "C"  void TimedEffect_Start_m2684894691 (TimedEffect_t1335807880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedEffect::SetData(System.Action)
extern "C"  void TimedEffect_SetData_m2151196784 (TimedEffect_t1335807880 * __this, Action_t3771233898 * ___timeOutCall0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedEffect::OnDestroy()
extern "C"  void TimedEffect_OnDestroy_m4121448540 (TimedEffect_t1335807880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedEffect::Update()
extern "C"  void TimedEffect_Update_m1633208970 (TimedEffect_t1335807880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedEffect::DestroyGameObjcet()
extern "C"  void TimedEffect_DestroyGameObjcet_m4200889960 (TimedEffect_t1335807880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TimedEffect::ilo_get_delaytime1(TimedEffect)
extern "C"  float TimedEffect_ilo_get_delaytime1_m434706424 (Il2CppObject * __this /* static, unused */, TimedEffect_t1335807880 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
