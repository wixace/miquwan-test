﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraShotGameSpeedCfg
struct  CameraShotGameSpeedCfg_t308624942  : public Il2CppObject
{
public:
	// System.Int32 CameraShotGameSpeedCfg::_id
	int32_t ____id_0;
	// System.Single CameraShotGameSpeedCfg::_speed
	float ____speed_1;
	// ProtoBuf.IExtension CameraShotGameSpeedCfg::extensionObject
	Il2CppObject * ___extensionObject_2;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(CameraShotGameSpeedCfg_t308624942, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__speed_1() { return static_cast<int32_t>(offsetof(CameraShotGameSpeedCfg_t308624942, ____speed_1)); }
	inline float get__speed_1() const { return ____speed_1; }
	inline float* get_address_of__speed_1() { return &____speed_1; }
	inline void set__speed_1(float value)
	{
		____speed_1 = value;
	}

	inline static int32_t get_offset_of_extensionObject_2() { return static_cast<int32_t>(offsetof(CameraShotGameSpeedCfg_t308624942, ___extensionObject_2)); }
	inline Il2CppObject * get_extensionObject_2() const { return ___extensionObject_2; }
	inline Il2CppObject ** get_address_of_extensionObject_2() { return &___extensionObject_2; }
	inline void set_extensionObject_2(Il2CppObject * value)
	{
		___extensionObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObject_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
