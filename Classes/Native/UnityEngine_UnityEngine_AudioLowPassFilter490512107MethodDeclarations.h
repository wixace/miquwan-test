﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AudioLowPassFilter
struct AudioLowPassFilter_t490512107;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3667593487;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AnimationCurve3667593487.h"

// System.Void UnityEngine.AudioLowPassFilter::.ctor()
extern "C"  void AudioLowPassFilter__ctor_m1350154150 (AudioLowPassFilter_t490512107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioLowPassFilter::get_cutoffFrequency()
extern "C"  float AudioLowPassFilter_get_cutoffFrequency_m1278378910 (AudioLowPassFilter_t490512107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioLowPassFilter::set_cutoffFrequency(System.Single)
extern "C"  void AudioLowPassFilter_set_cutoffFrequency_m4010346549 (AudioLowPassFilter_t490512107 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationCurve UnityEngine.AudioLowPassFilter::get_customCutoffCurve()
extern "C"  AnimationCurve_t3667593487 * AudioLowPassFilter_get_customCutoffCurve_m2066355903 (AudioLowPassFilter_t490512107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioLowPassFilter::set_customCutoffCurve(UnityEngine.AnimationCurve)
extern "C"  void AudioLowPassFilter_set_customCutoffCurve_m83580630 (AudioLowPassFilter_t490512107 * __this, AnimationCurve_t3667593487 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioLowPassFilter::get_lowpassResonanceQ()
extern "C"  float AudioLowPassFilter_get_lowpassResonanceQ_m2725038839 (AudioLowPassFilter_t490512107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioLowPassFilter::set_lowpassResonanceQ(System.Single)
extern "C"  void AudioLowPassFilter_set_lowpassResonanceQ_m3534520188 (AudioLowPassFilter_t490512107 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
