﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CombatAttPlus/FightPlusAtt
struct  FightPlusAtt_t4246110079  : public Il2CppObject
{
public:
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_attack_per
	uint32_t ___ac_attack_per_0;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_attack_per
	uint32_t ____attack_per_1;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_phy_def_per
	uint32_t ___ac_phy_def_per_2;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_phy_def_per
	uint32_t ____phy_def_per_3;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_mag_def_per
	uint32_t ___ac_mag_def_per_4;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_mag_def_per
	uint32_t ____mag_def_per_5;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_hp_per
	uint32_t ___ac_hp_per_6;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_hp_per
	uint32_t ____hp_per_7;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_hitrate_per
	uint32_t ___ac_hitrate_per_8;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_hitrate_per
	uint32_t ____hitrate_per_9;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_dodge_per
	uint32_t ___ac_dodge_per_10;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_dodge_per
	uint32_t ____dodge_per_11;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_destroy_per
	uint32_t ___ac_destroy_per_12;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_destroy_per
	uint32_t ____destroy_per_13;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_block_per
	uint32_t ___ac_block_per_14;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_block_per
	uint32_t ____block_per_15;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_crit_per
	uint32_t ___ac_crit_per_16;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_crit_per
	uint32_t ____crit_per_17;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_anticrit_per
	uint32_t ___ac_anticrit_per_18;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_anticrit_per
	uint32_t ____anticrit_per_19;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_attack
	uint32_t ___ac_attack_20;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_attack
	uint32_t ____attack_21;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_phy_def
	uint32_t ___ac_phy_def_22;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_phy_def
	uint32_t ____phy_def_23;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_mag_def
	uint32_t ___ac_mag_def_24;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_mag_def
	uint32_t ____mag_def_25;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_hp
	uint32_t ___ac_hp_26;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_hp
	uint32_t ____hp_27;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_hitrate
	uint32_t ___ac_hitrate_28;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_hitrate
	uint32_t ____hitrate_29;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_dodge
	uint32_t ___ac_dodge_30;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_dodge
	uint32_t ____dodge_31;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_destroy
	uint32_t ___ac_destroy_32;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_destroy
	uint32_t ____destroy_33;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_block
	uint32_t ___ac_block_34;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_block
	uint32_t ____block_35;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_crit
	uint32_t ___ac_crit_36;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_crit
	uint32_t ____crit_37;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_anticrit
	uint32_t ___ac_anticrit_38;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_anticrit
	uint32_t ____anticrit_39;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_damage_bonus
	uint32_t ___ac_damage_bonus_40;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_damage_bonus
	uint32_t ____damage_bonus_41;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_damage_reduce
	uint32_t ___ac_damage_reduce_42;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_damage_reduce
	uint32_t ____damage_reduce_43;
	// System.UInt32 CombatAttPlus/FightPlusAtt::ac_crit_hurt
	uint32_t ___ac_crit_hurt_44;
	// System.UInt32 CombatAttPlus/FightPlusAtt::_crit_hurt
	uint32_t ____crit_hurt_45;

public:
	inline static int32_t get_offset_of_ac_attack_per_0() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_attack_per_0)); }
	inline uint32_t get_ac_attack_per_0() const { return ___ac_attack_per_0; }
	inline uint32_t* get_address_of_ac_attack_per_0() { return &___ac_attack_per_0; }
	inline void set_ac_attack_per_0(uint32_t value)
	{
		___ac_attack_per_0 = value;
	}

	inline static int32_t get_offset_of__attack_per_1() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____attack_per_1)); }
	inline uint32_t get__attack_per_1() const { return ____attack_per_1; }
	inline uint32_t* get_address_of__attack_per_1() { return &____attack_per_1; }
	inline void set__attack_per_1(uint32_t value)
	{
		____attack_per_1 = value;
	}

	inline static int32_t get_offset_of_ac_phy_def_per_2() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_phy_def_per_2)); }
	inline uint32_t get_ac_phy_def_per_2() const { return ___ac_phy_def_per_2; }
	inline uint32_t* get_address_of_ac_phy_def_per_2() { return &___ac_phy_def_per_2; }
	inline void set_ac_phy_def_per_2(uint32_t value)
	{
		___ac_phy_def_per_2 = value;
	}

	inline static int32_t get_offset_of__phy_def_per_3() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____phy_def_per_3)); }
	inline uint32_t get__phy_def_per_3() const { return ____phy_def_per_3; }
	inline uint32_t* get_address_of__phy_def_per_3() { return &____phy_def_per_3; }
	inline void set__phy_def_per_3(uint32_t value)
	{
		____phy_def_per_3 = value;
	}

	inline static int32_t get_offset_of_ac_mag_def_per_4() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_mag_def_per_4)); }
	inline uint32_t get_ac_mag_def_per_4() const { return ___ac_mag_def_per_4; }
	inline uint32_t* get_address_of_ac_mag_def_per_4() { return &___ac_mag_def_per_4; }
	inline void set_ac_mag_def_per_4(uint32_t value)
	{
		___ac_mag_def_per_4 = value;
	}

	inline static int32_t get_offset_of__mag_def_per_5() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____mag_def_per_5)); }
	inline uint32_t get__mag_def_per_5() const { return ____mag_def_per_5; }
	inline uint32_t* get_address_of__mag_def_per_5() { return &____mag_def_per_5; }
	inline void set__mag_def_per_5(uint32_t value)
	{
		____mag_def_per_5 = value;
	}

	inline static int32_t get_offset_of_ac_hp_per_6() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_hp_per_6)); }
	inline uint32_t get_ac_hp_per_6() const { return ___ac_hp_per_6; }
	inline uint32_t* get_address_of_ac_hp_per_6() { return &___ac_hp_per_6; }
	inline void set_ac_hp_per_6(uint32_t value)
	{
		___ac_hp_per_6 = value;
	}

	inline static int32_t get_offset_of__hp_per_7() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____hp_per_7)); }
	inline uint32_t get__hp_per_7() const { return ____hp_per_7; }
	inline uint32_t* get_address_of__hp_per_7() { return &____hp_per_7; }
	inline void set__hp_per_7(uint32_t value)
	{
		____hp_per_7 = value;
	}

	inline static int32_t get_offset_of_ac_hitrate_per_8() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_hitrate_per_8)); }
	inline uint32_t get_ac_hitrate_per_8() const { return ___ac_hitrate_per_8; }
	inline uint32_t* get_address_of_ac_hitrate_per_8() { return &___ac_hitrate_per_8; }
	inline void set_ac_hitrate_per_8(uint32_t value)
	{
		___ac_hitrate_per_8 = value;
	}

	inline static int32_t get_offset_of__hitrate_per_9() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____hitrate_per_9)); }
	inline uint32_t get__hitrate_per_9() const { return ____hitrate_per_9; }
	inline uint32_t* get_address_of__hitrate_per_9() { return &____hitrate_per_9; }
	inline void set__hitrate_per_9(uint32_t value)
	{
		____hitrate_per_9 = value;
	}

	inline static int32_t get_offset_of_ac_dodge_per_10() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_dodge_per_10)); }
	inline uint32_t get_ac_dodge_per_10() const { return ___ac_dodge_per_10; }
	inline uint32_t* get_address_of_ac_dodge_per_10() { return &___ac_dodge_per_10; }
	inline void set_ac_dodge_per_10(uint32_t value)
	{
		___ac_dodge_per_10 = value;
	}

	inline static int32_t get_offset_of__dodge_per_11() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____dodge_per_11)); }
	inline uint32_t get__dodge_per_11() const { return ____dodge_per_11; }
	inline uint32_t* get_address_of__dodge_per_11() { return &____dodge_per_11; }
	inline void set__dodge_per_11(uint32_t value)
	{
		____dodge_per_11 = value;
	}

	inline static int32_t get_offset_of_ac_destroy_per_12() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_destroy_per_12)); }
	inline uint32_t get_ac_destroy_per_12() const { return ___ac_destroy_per_12; }
	inline uint32_t* get_address_of_ac_destroy_per_12() { return &___ac_destroy_per_12; }
	inline void set_ac_destroy_per_12(uint32_t value)
	{
		___ac_destroy_per_12 = value;
	}

	inline static int32_t get_offset_of__destroy_per_13() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____destroy_per_13)); }
	inline uint32_t get__destroy_per_13() const { return ____destroy_per_13; }
	inline uint32_t* get_address_of__destroy_per_13() { return &____destroy_per_13; }
	inline void set__destroy_per_13(uint32_t value)
	{
		____destroy_per_13 = value;
	}

	inline static int32_t get_offset_of_ac_block_per_14() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_block_per_14)); }
	inline uint32_t get_ac_block_per_14() const { return ___ac_block_per_14; }
	inline uint32_t* get_address_of_ac_block_per_14() { return &___ac_block_per_14; }
	inline void set_ac_block_per_14(uint32_t value)
	{
		___ac_block_per_14 = value;
	}

	inline static int32_t get_offset_of__block_per_15() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____block_per_15)); }
	inline uint32_t get__block_per_15() const { return ____block_per_15; }
	inline uint32_t* get_address_of__block_per_15() { return &____block_per_15; }
	inline void set__block_per_15(uint32_t value)
	{
		____block_per_15 = value;
	}

	inline static int32_t get_offset_of_ac_crit_per_16() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_crit_per_16)); }
	inline uint32_t get_ac_crit_per_16() const { return ___ac_crit_per_16; }
	inline uint32_t* get_address_of_ac_crit_per_16() { return &___ac_crit_per_16; }
	inline void set_ac_crit_per_16(uint32_t value)
	{
		___ac_crit_per_16 = value;
	}

	inline static int32_t get_offset_of__crit_per_17() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____crit_per_17)); }
	inline uint32_t get__crit_per_17() const { return ____crit_per_17; }
	inline uint32_t* get_address_of__crit_per_17() { return &____crit_per_17; }
	inline void set__crit_per_17(uint32_t value)
	{
		____crit_per_17 = value;
	}

	inline static int32_t get_offset_of_ac_anticrit_per_18() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_anticrit_per_18)); }
	inline uint32_t get_ac_anticrit_per_18() const { return ___ac_anticrit_per_18; }
	inline uint32_t* get_address_of_ac_anticrit_per_18() { return &___ac_anticrit_per_18; }
	inline void set_ac_anticrit_per_18(uint32_t value)
	{
		___ac_anticrit_per_18 = value;
	}

	inline static int32_t get_offset_of__anticrit_per_19() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____anticrit_per_19)); }
	inline uint32_t get__anticrit_per_19() const { return ____anticrit_per_19; }
	inline uint32_t* get_address_of__anticrit_per_19() { return &____anticrit_per_19; }
	inline void set__anticrit_per_19(uint32_t value)
	{
		____anticrit_per_19 = value;
	}

	inline static int32_t get_offset_of_ac_attack_20() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_attack_20)); }
	inline uint32_t get_ac_attack_20() const { return ___ac_attack_20; }
	inline uint32_t* get_address_of_ac_attack_20() { return &___ac_attack_20; }
	inline void set_ac_attack_20(uint32_t value)
	{
		___ac_attack_20 = value;
	}

	inline static int32_t get_offset_of__attack_21() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____attack_21)); }
	inline uint32_t get__attack_21() const { return ____attack_21; }
	inline uint32_t* get_address_of__attack_21() { return &____attack_21; }
	inline void set__attack_21(uint32_t value)
	{
		____attack_21 = value;
	}

	inline static int32_t get_offset_of_ac_phy_def_22() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_phy_def_22)); }
	inline uint32_t get_ac_phy_def_22() const { return ___ac_phy_def_22; }
	inline uint32_t* get_address_of_ac_phy_def_22() { return &___ac_phy_def_22; }
	inline void set_ac_phy_def_22(uint32_t value)
	{
		___ac_phy_def_22 = value;
	}

	inline static int32_t get_offset_of__phy_def_23() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____phy_def_23)); }
	inline uint32_t get__phy_def_23() const { return ____phy_def_23; }
	inline uint32_t* get_address_of__phy_def_23() { return &____phy_def_23; }
	inline void set__phy_def_23(uint32_t value)
	{
		____phy_def_23 = value;
	}

	inline static int32_t get_offset_of_ac_mag_def_24() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_mag_def_24)); }
	inline uint32_t get_ac_mag_def_24() const { return ___ac_mag_def_24; }
	inline uint32_t* get_address_of_ac_mag_def_24() { return &___ac_mag_def_24; }
	inline void set_ac_mag_def_24(uint32_t value)
	{
		___ac_mag_def_24 = value;
	}

	inline static int32_t get_offset_of__mag_def_25() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____mag_def_25)); }
	inline uint32_t get__mag_def_25() const { return ____mag_def_25; }
	inline uint32_t* get_address_of__mag_def_25() { return &____mag_def_25; }
	inline void set__mag_def_25(uint32_t value)
	{
		____mag_def_25 = value;
	}

	inline static int32_t get_offset_of_ac_hp_26() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_hp_26)); }
	inline uint32_t get_ac_hp_26() const { return ___ac_hp_26; }
	inline uint32_t* get_address_of_ac_hp_26() { return &___ac_hp_26; }
	inline void set_ac_hp_26(uint32_t value)
	{
		___ac_hp_26 = value;
	}

	inline static int32_t get_offset_of__hp_27() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____hp_27)); }
	inline uint32_t get__hp_27() const { return ____hp_27; }
	inline uint32_t* get_address_of__hp_27() { return &____hp_27; }
	inline void set__hp_27(uint32_t value)
	{
		____hp_27 = value;
	}

	inline static int32_t get_offset_of_ac_hitrate_28() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_hitrate_28)); }
	inline uint32_t get_ac_hitrate_28() const { return ___ac_hitrate_28; }
	inline uint32_t* get_address_of_ac_hitrate_28() { return &___ac_hitrate_28; }
	inline void set_ac_hitrate_28(uint32_t value)
	{
		___ac_hitrate_28 = value;
	}

	inline static int32_t get_offset_of__hitrate_29() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____hitrate_29)); }
	inline uint32_t get__hitrate_29() const { return ____hitrate_29; }
	inline uint32_t* get_address_of__hitrate_29() { return &____hitrate_29; }
	inline void set__hitrate_29(uint32_t value)
	{
		____hitrate_29 = value;
	}

	inline static int32_t get_offset_of_ac_dodge_30() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_dodge_30)); }
	inline uint32_t get_ac_dodge_30() const { return ___ac_dodge_30; }
	inline uint32_t* get_address_of_ac_dodge_30() { return &___ac_dodge_30; }
	inline void set_ac_dodge_30(uint32_t value)
	{
		___ac_dodge_30 = value;
	}

	inline static int32_t get_offset_of__dodge_31() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____dodge_31)); }
	inline uint32_t get__dodge_31() const { return ____dodge_31; }
	inline uint32_t* get_address_of__dodge_31() { return &____dodge_31; }
	inline void set__dodge_31(uint32_t value)
	{
		____dodge_31 = value;
	}

	inline static int32_t get_offset_of_ac_destroy_32() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_destroy_32)); }
	inline uint32_t get_ac_destroy_32() const { return ___ac_destroy_32; }
	inline uint32_t* get_address_of_ac_destroy_32() { return &___ac_destroy_32; }
	inline void set_ac_destroy_32(uint32_t value)
	{
		___ac_destroy_32 = value;
	}

	inline static int32_t get_offset_of__destroy_33() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____destroy_33)); }
	inline uint32_t get__destroy_33() const { return ____destroy_33; }
	inline uint32_t* get_address_of__destroy_33() { return &____destroy_33; }
	inline void set__destroy_33(uint32_t value)
	{
		____destroy_33 = value;
	}

	inline static int32_t get_offset_of_ac_block_34() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_block_34)); }
	inline uint32_t get_ac_block_34() const { return ___ac_block_34; }
	inline uint32_t* get_address_of_ac_block_34() { return &___ac_block_34; }
	inline void set_ac_block_34(uint32_t value)
	{
		___ac_block_34 = value;
	}

	inline static int32_t get_offset_of__block_35() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____block_35)); }
	inline uint32_t get__block_35() const { return ____block_35; }
	inline uint32_t* get_address_of__block_35() { return &____block_35; }
	inline void set__block_35(uint32_t value)
	{
		____block_35 = value;
	}

	inline static int32_t get_offset_of_ac_crit_36() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_crit_36)); }
	inline uint32_t get_ac_crit_36() const { return ___ac_crit_36; }
	inline uint32_t* get_address_of_ac_crit_36() { return &___ac_crit_36; }
	inline void set_ac_crit_36(uint32_t value)
	{
		___ac_crit_36 = value;
	}

	inline static int32_t get_offset_of__crit_37() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____crit_37)); }
	inline uint32_t get__crit_37() const { return ____crit_37; }
	inline uint32_t* get_address_of__crit_37() { return &____crit_37; }
	inline void set__crit_37(uint32_t value)
	{
		____crit_37 = value;
	}

	inline static int32_t get_offset_of_ac_anticrit_38() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_anticrit_38)); }
	inline uint32_t get_ac_anticrit_38() const { return ___ac_anticrit_38; }
	inline uint32_t* get_address_of_ac_anticrit_38() { return &___ac_anticrit_38; }
	inline void set_ac_anticrit_38(uint32_t value)
	{
		___ac_anticrit_38 = value;
	}

	inline static int32_t get_offset_of__anticrit_39() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____anticrit_39)); }
	inline uint32_t get__anticrit_39() const { return ____anticrit_39; }
	inline uint32_t* get_address_of__anticrit_39() { return &____anticrit_39; }
	inline void set__anticrit_39(uint32_t value)
	{
		____anticrit_39 = value;
	}

	inline static int32_t get_offset_of_ac_damage_bonus_40() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_damage_bonus_40)); }
	inline uint32_t get_ac_damage_bonus_40() const { return ___ac_damage_bonus_40; }
	inline uint32_t* get_address_of_ac_damage_bonus_40() { return &___ac_damage_bonus_40; }
	inline void set_ac_damage_bonus_40(uint32_t value)
	{
		___ac_damage_bonus_40 = value;
	}

	inline static int32_t get_offset_of__damage_bonus_41() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____damage_bonus_41)); }
	inline uint32_t get__damage_bonus_41() const { return ____damage_bonus_41; }
	inline uint32_t* get_address_of__damage_bonus_41() { return &____damage_bonus_41; }
	inline void set__damage_bonus_41(uint32_t value)
	{
		____damage_bonus_41 = value;
	}

	inline static int32_t get_offset_of_ac_damage_reduce_42() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_damage_reduce_42)); }
	inline uint32_t get_ac_damage_reduce_42() const { return ___ac_damage_reduce_42; }
	inline uint32_t* get_address_of_ac_damage_reduce_42() { return &___ac_damage_reduce_42; }
	inline void set_ac_damage_reduce_42(uint32_t value)
	{
		___ac_damage_reduce_42 = value;
	}

	inline static int32_t get_offset_of__damage_reduce_43() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____damage_reduce_43)); }
	inline uint32_t get__damage_reduce_43() const { return ____damage_reduce_43; }
	inline uint32_t* get_address_of__damage_reduce_43() { return &____damage_reduce_43; }
	inline void set__damage_reduce_43(uint32_t value)
	{
		____damage_reduce_43 = value;
	}

	inline static int32_t get_offset_of_ac_crit_hurt_44() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ___ac_crit_hurt_44)); }
	inline uint32_t get_ac_crit_hurt_44() const { return ___ac_crit_hurt_44; }
	inline uint32_t* get_address_of_ac_crit_hurt_44() { return &___ac_crit_hurt_44; }
	inline void set_ac_crit_hurt_44(uint32_t value)
	{
		___ac_crit_hurt_44 = value;
	}

	inline static int32_t get_offset_of__crit_hurt_45() { return static_cast<int32_t>(offsetof(FightPlusAtt_t4246110079, ____crit_hurt_45)); }
	inline uint32_t get__crit_hurt_45() const { return ____crit_hurt_45; }
	inline uint32_t* get_address_of__crit_hurt_45() { return &____crit_hurt_45; }
	inline void set__crit_hurt_45(uint32_t value)
	{
		____crit_hurt_45 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
