﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Animation
struct Animation_t1724966010;
// UnityEngine.AnimationClip
struct AnimationClip_t2007702890;
// System.String
struct String_t;
// UnityEngine.AnimationState
struct AnimationState_t3682323633;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AnimationClip2007702890.h"
#include "UnityEngine_UnityEngine_WrapMode1491636113.h"
#include "UnityEngine_UnityEngine_Animation1724966010.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_PlayMode1155122555.h"
#include "UnityEngine_UnityEngine_QueueMode3161759562.h"
#include "UnityEngine_UnityEngine_AnimationCullingType2281967210.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"

// System.Void UnityEngine.Animation::.ctor()
extern "C"  void Animation__ctor_m3319815637 (Animation_t1724966010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationClip UnityEngine.Animation::get_clip()
extern "C"  AnimationClip_t2007702890 * Animation_get_clip_m980560434 (Animation_t1724966010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::set_clip(UnityEngine.AnimationClip)
extern "C"  void Animation_set_clip_m1663398585 (Animation_t1724966010 * __this, AnimationClip_t2007702890 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animation::get_playAutomatically()
extern "C"  bool Animation_get_playAutomatically_m2078183967 (Animation_t1724966010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::set_playAutomatically(System.Boolean)
extern "C"  void Animation_set_playAutomatically_m2860443004 (Animation_t1724966010 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WrapMode UnityEngine.Animation::get_wrapMode()
extern "C"  int32_t Animation_get_wrapMode_m2808937466 (Animation_t1724966010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::set_wrapMode(UnityEngine.WrapMode)
extern "C"  void Animation_set_wrapMode_m414703029 (Animation_t1724966010 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::Stop()
extern "C"  void Animation_Stop_m3675770961 (Animation_t1724966010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::INTERNAL_CALL_Stop(UnityEngine.Animation)
extern "C"  void Animation_INTERNAL_CALL_Stop_m1370047957 (Il2CppObject * __this /* static, unused */, Animation_t1724966010 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::Stop(System.String)
extern "C"  void Animation_Stop_m2745735761 (Animation_t1724966010 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::Internal_StopByName(System.String)
extern "C"  void Animation_Internal_StopByName_m322505897 (Animation_t1724966010 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::Rewind(System.String)
extern "C"  void Animation_Rewind_m502732792 (Animation_t1724966010 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::Internal_RewindByName(System.String)
extern "C"  void Animation_Internal_RewindByName_m3555379856 (Animation_t1724966010 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::Rewind()
extern "C"  void Animation_Rewind_m7024458 (Animation_t1724966010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::INTERNAL_CALL_Rewind(UnityEngine.Animation)
extern "C"  void Animation_INTERNAL_CALL_Rewind_m4178761788 (Il2CppObject * __this /* static, unused */, Animation_t1724966010 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::Sample()
extern "C"  void Animation_Sample_m2214901881 (Animation_t1724966010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::INTERNAL_CALL_Sample(UnityEngine.Animation)
extern "C"  void Animation_INTERNAL_CALL_Sample_m579220077 (Il2CppObject * __this /* static, unused */, Animation_t1724966010 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animation::get_isPlaying()
extern "C"  bool Animation_get_isPlaying_m3295833780 (Animation_t1724966010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animation::IsPlaying(System.String)
extern "C"  bool Animation_IsPlaying_m1471515685 (Animation_t1724966010 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationState UnityEngine.Animation::get_Item(System.String)
extern "C"  AnimationState_t3682323633 * Animation_get_Item_m2669576386 (Animation_t1724966010 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animation::Play()
extern "C"  bool Animation_Play_m4273654237 (Animation_t1724966010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animation::Play(UnityEngine.PlayMode)
extern "C"  bool Animation_Play_m2860559471 (Animation_t1724966010 * __this, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animation::Play(System.String,UnityEngine.PlayMode)
extern "C"  bool Animation_Play_m1881292147 (Animation_t1724966010 * __this, String_t* ___animation0, int32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animation::Play(System.String)
extern "C"  bool Animation_Play_m900498501 (Animation_t1724966010 * __this, String_t* ___animation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::CrossFade(System.String,System.Single,UnityEngine.PlayMode)
extern "C"  void Animation_CrossFade_m216828000 (Animation_t1724966010 * __this, String_t* ___animation0, float ___fadeLength1, int32_t ___mode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::CrossFade(System.String,System.Single)
extern "C"  void Animation_CrossFade_m2600902456 (Animation_t1724966010 * __this, String_t* ___animation0, float ___fadeLength1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::CrossFade(System.String)
extern "C"  void Animation_CrossFade_m1762644371 (Animation_t1724966010 * __this, String_t* ___animation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::Blend(System.String,System.Single,System.Single)
extern "C"  void Animation_Blend_m2812291720 (Animation_t1724966010 * __this, String_t* ___animation0, float ___targetWeight1, float ___fadeLength2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::Blend(System.String,System.Single)
extern "C"  void Animation_Blend_m2255251683 (Animation_t1724966010 * __this, String_t* ___animation0, float ___targetWeight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::Blend(System.String)
extern "C"  void Animation_Blend_m1153110654 (Animation_t1724966010 * __this, String_t* ___animation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationState UnityEngine.Animation::CrossFadeQueued(System.String,System.Single,UnityEngine.QueueMode,UnityEngine.PlayMode)
extern "C"  AnimationState_t3682323633 * Animation_CrossFadeQueued_m2368758351 (Animation_t1724966010 * __this, String_t* ___animation0, float ___fadeLength1, int32_t ___queue2, int32_t ___mode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationState UnityEngine.Animation::CrossFadeQueued(System.String,System.Single,UnityEngine.QueueMode)
extern "C"  AnimationState_t3682323633 * Animation_CrossFadeQueued_m3690565097 (Animation_t1724966010 * __this, String_t* ___animation0, float ___fadeLength1, int32_t ___queue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationState UnityEngine.Animation::CrossFadeQueued(System.String,System.Single)
extern "C"  AnimationState_t3682323633 * Animation_CrossFadeQueued_m1844255962 (Animation_t1724966010 * __this, String_t* ___animation0, float ___fadeLength1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationState UnityEngine.Animation::CrossFadeQueued(System.String)
extern "C"  AnimationState_t3682323633 * Animation_CrossFadeQueued_m2686448309 (Animation_t1724966010 * __this, String_t* ___animation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationState UnityEngine.Animation::PlayQueued(System.String,UnityEngine.QueueMode,UnityEngine.PlayMode)
extern "C"  AnimationState_t3682323633 * Animation_PlayQueued_m3518837842 (Animation_t1724966010 * __this, String_t* ___animation0, int32_t ___queue1, int32_t ___mode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationState UnityEngine.Animation::PlayQueued(System.String,UnityEngine.QueueMode)
extern "C"  AnimationState_t3682323633 * Animation_PlayQueued_m771891206 (Animation_t1724966010 * __this, String_t* ___animation0, int32_t ___queue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationState UnityEngine.Animation::PlayQueued(System.String)
extern "C"  AnimationState_t3682323633 * Animation_PlayQueued_m1810244279 (Animation_t1724966010 * __this, String_t* ___animation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::AddClip(UnityEngine.AnimationClip,System.String)
extern "C"  void Animation_AddClip_m3358255085 (Animation_t1724966010 * __this, AnimationClip_t2007702890 * ___clip0, String_t* ___newName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::AddClip(UnityEngine.AnimationClip,System.String,System.Int32,System.Int32,System.Boolean)
extern "C"  void Animation_AddClip_m770980048 (Animation_t1724966010 * __this, AnimationClip_t2007702890 * ___clip0, String_t* ___newName1, int32_t ___firstFrame2, int32_t ___lastFrame3, bool ___addLoopFrame4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::AddClip(UnityEngine.AnimationClip,System.String,System.Int32,System.Int32)
extern "C"  void Animation_AddClip_m2718334733 (Animation_t1724966010 * __this, AnimationClip_t2007702890 * ___clip0, String_t* ___newName1, int32_t ___firstFrame2, int32_t ___lastFrame3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::RemoveClip(UnityEngine.AnimationClip)
extern "C"  void Animation_RemoveClip_m2777156818 (Animation_t1724966010 * __this, AnimationClip_t2007702890 * ___clip0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::RemoveClip(System.String)
extern "C"  void Animation_RemoveClip_m2702382239 (Animation_t1724966010 * __this, String_t* ___clipName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animation::GetClipCount()
extern "C"  int32_t Animation_GetClipCount_m1017109752 (Animation_t1724966010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::RemoveClip2(System.String)
extern "C"  void Animation_RemoveClip2_m71002097 (Animation_t1724966010 * __this, String_t* ___clipName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animation::PlayDefaultAnimation(UnityEngine.PlayMode)
extern "C"  bool Animation_PlayDefaultAnimation_m170678930 (Animation_t1724966010 * __this, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::SyncLayer(System.Int32)
extern "C"  void Animation_SyncLayer_m51562426 (Animation_t1724966010 * __this, int32_t ___layer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::INTERNAL_CALL_SyncLayer(UnityEngine.Animation,System.Int32)
extern "C"  void Animation_INTERNAL_CALL_SyncLayer_m1147862428 (Il2CppObject * __this /* static, unused */, Animation_t1724966010 * ___self0, int32_t ___layer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityEngine.Animation::GetEnumerator()
extern "C"  Il2CppObject * Animation_GetEnumerator_m3015582503 (Animation_t1724966010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationState UnityEngine.Animation::GetState(System.String)
extern "C"  AnimationState_t3682323633 * Animation_GetState_m528810595 (Animation_t1724966010 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationState UnityEngine.Animation::GetStateAtIndex(System.Int32)
extern "C"  AnimationState_t3682323633 * Animation_GetStateAtIndex_m513456147 (Animation_t1724966010 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animation::GetStateCount()
extern "C"  int32_t Animation_GetStateCount_m3537607527 (Animation_t1724966010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationClip UnityEngine.Animation::GetClip(System.String)
extern "C"  AnimationClip_t2007702890 * Animation_GetClip_m1181634579 (Animation_t1724966010 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animation::get_animatePhysics()
extern "C"  bool Animation_get_animatePhysics_m3652384248 (Animation_t1724966010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::set_animatePhysics(System.Boolean)
extern "C"  void Animation_set_animatePhysics_m4247714569 (Animation_t1724966010 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationCullingType UnityEngine.Animation::get_cullingType()
extern "C"  int32_t Animation_get_cullingType_m2049692326 (Animation_t1724966010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::set_cullingType(UnityEngine.AnimationCullingType)
extern "C"  void Animation_set_cullingType_m190671619 (Animation_t1724966010 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UnityEngine.Animation::get_localBounds()
extern "C"  Bounds_t2711641849  Animation_get_localBounds_m4013467501 (Animation_t1724966010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::set_localBounds(UnityEngine.Bounds)
extern "C"  void Animation_set_localBounds_m2595405578 (Animation_t1724966010 * __this, Bounds_t2711641849  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::INTERNAL_get_localBounds(UnityEngine.Bounds&)
extern "C"  void Animation_INTERNAL_get_localBounds_m885326644 (Animation_t1724966010 * __this, Bounds_t2711641849 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::INTERNAL_set_localBounds(UnityEngine.Bounds&)
extern "C"  void Animation_INTERNAL_set_localBounds_m2814093224 (Animation_t1724966010 * __this, Bounds_t2711641849 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
