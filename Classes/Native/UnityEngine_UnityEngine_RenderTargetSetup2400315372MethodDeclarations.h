﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.RenderTargetSetup
struct RenderTargetSetup_t2400315372;
struct RenderTargetSetup_t2400315372_marshaled_pinvoke;
struct RenderTargetSetup_t2400315372_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct RenderTargetSetup_t2400315372;
struct RenderTargetSetup_t2400315372_marshaled_pinvoke;

extern "C" void RenderTargetSetup_t2400315372_marshal_pinvoke(const RenderTargetSetup_t2400315372& unmarshaled, RenderTargetSetup_t2400315372_marshaled_pinvoke& marshaled);
extern "C" void RenderTargetSetup_t2400315372_marshal_pinvoke_back(const RenderTargetSetup_t2400315372_marshaled_pinvoke& marshaled, RenderTargetSetup_t2400315372& unmarshaled);
extern "C" void RenderTargetSetup_t2400315372_marshal_pinvoke_cleanup(RenderTargetSetup_t2400315372_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct RenderTargetSetup_t2400315372;
struct RenderTargetSetup_t2400315372_marshaled_com;

extern "C" void RenderTargetSetup_t2400315372_marshal_com(const RenderTargetSetup_t2400315372& unmarshaled, RenderTargetSetup_t2400315372_marshaled_com& marshaled);
extern "C" void RenderTargetSetup_t2400315372_marshal_com_back(const RenderTargetSetup_t2400315372_marshaled_com& marshaled, RenderTargetSetup_t2400315372& unmarshaled);
extern "C" void RenderTargetSetup_t2400315372_marshal_com_cleanup(RenderTargetSetup_t2400315372_marshaled_com& marshaled);
