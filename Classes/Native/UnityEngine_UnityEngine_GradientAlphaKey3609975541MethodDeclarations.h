﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GradientAlphaKey
struct GradientAlphaKey_t3609975541;
struct GradientAlphaKey_t3609975541_marshaled_pinvoke;
struct GradientAlphaKey_t3609975541_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GradientAlphaKey3609975541.h"

// System.Void UnityEngine.GradientAlphaKey::.ctor(System.Single,System.Single)
extern "C"  void GradientAlphaKey__ctor_m3417325748 (GradientAlphaKey_t3609975541 * __this, float ___alpha0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct GradientAlphaKey_t3609975541;
struct GradientAlphaKey_t3609975541_marshaled_pinvoke;

extern "C" void GradientAlphaKey_t3609975541_marshal_pinvoke(const GradientAlphaKey_t3609975541& unmarshaled, GradientAlphaKey_t3609975541_marshaled_pinvoke& marshaled);
extern "C" void GradientAlphaKey_t3609975541_marshal_pinvoke_back(const GradientAlphaKey_t3609975541_marshaled_pinvoke& marshaled, GradientAlphaKey_t3609975541& unmarshaled);
extern "C" void GradientAlphaKey_t3609975541_marshal_pinvoke_cleanup(GradientAlphaKey_t3609975541_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct GradientAlphaKey_t3609975541;
struct GradientAlphaKey_t3609975541_marshaled_com;

extern "C" void GradientAlphaKey_t3609975541_marshal_com(const GradientAlphaKey_t3609975541& unmarshaled, GradientAlphaKey_t3609975541_marshaled_com& marshaled);
extern "C" void GradientAlphaKey_t3609975541_marshal_com_back(const GradientAlphaKey_t3609975541_marshaled_com& marshaled, GradientAlphaKey_t3609975541& unmarshaled);
extern "C" void GradientAlphaKey_t3609975541_marshal_com_cleanup(GradientAlphaKey_t3609975541_marshaled_com& marshaled);
