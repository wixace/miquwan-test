﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_cifouVurda13
struct  M_cifouVurda13_t2175699772  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_cifouVurda13::_maferexairTurfaker
	int32_t ____maferexairTurfaker_0;
	// System.Int32 GarbageiOS.M_cifouVurda13::_morloociVairyo
	int32_t ____morloociVairyo_1;
	// System.UInt32 GarbageiOS.M_cifouVurda13::_lijeTochermas
	uint32_t ____lijeTochermas_2;
	// System.UInt32 GarbageiOS.M_cifouVurda13::_raskivorReacusir
	uint32_t ____raskivorReacusir_3;
	// System.Boolean GarbageiOS.M_cifouVurda13::_serdar
	bool ____serdar_4;
	// System.Single GarbageiOS.M_cifouVurda13::_sturfehawRerdouray
	float ____sturfehawRerdouray_5;

public:
	inline static int32_t get_offset_of__maferexairTurfaker_0() { return static_cast<int32_t>(offsetof(M_cifouVurda13_t2175699772, ____maferexairTurfaker_0)); }
	inline int32_t get__maferexairTurfaker_0() const { return ____maferexairTurfaker_0; }
	inline int32_t* get_address_of__maferexairTurfaker_0() { return &____maferexairTurfaker_0; }
	inline void set__maferexairTurfaker_0(int32_t value)
	{
		____maferexairTurfaker_0 = value;
	}

	inline static int32_t get_offset_of__morloociVairyo_1() { return static_cast<int32_t>(offsetof(M_cifouVurda13_t2175699772, ____morloociVairyo_1)); }
	inline int32_t get__morloociVairyo_1() const { return ____morloociVairyo_1; }
	inline int32_t* get_address_of__morloociVairyo_1() { return &____morloociVairyo_1; }
	inline void set__morloociVairyo_1(int32_t value)
	{
		____morloociVairyo_1 = value;
	}

	inline static int32_t get_offset_of__lijeTochermas_2() { return static_cast<int32_t>(offsetof(M_cifouVurda13_t2175699772, ____lijeTochermas_2)); }
	inline uint32_t get__lijeTochermas_2() const { return ____lijeTochermas_2; }
	inline uint32_t* get_address_of__lijeTochermas_2() { return &____lijeTochermas_2; }
	inline void set__lijeTochermas_2(uint32_t value)
	{
		____lijeTochermas_2 = value;
	}

	inline static int32_t get_offset_of__raskivorReacusir_3() { return static_cast<int32_t>(offsetof(M_cifouVurda13_t2175699772, ____raskivorReacusir_3)); }
	inline uint32_t get__raskivorReacusir_3() const { return ____raskivorReacusir_3; }
	inline uint32_t* get_address_of__raskivorReacusir_3() { return &____raskivorReacusir_3; }
	inline void set__raskivorReacusir_3(uint32_t value)
	{
		____raskivorReacusir_3 = value;
	}

	inline static int32_t get_offset_of__serdar_4() { return static_cast<int32_t>(offsetof(M_cifouVurda13_t2175699772, ____serdar_4)); }
	inline bool get__serdar_4() const { return ____serdar_4; }
	inline bool* get_address_of__serdar_4() { return &____serdar_4; }
	inline void set__serdar_4(bool value)
	{
		____serdar_4 = value;
	}

	inline static int32_t get_offset_of__sturfehawRerdouray_5() { return static_cast<int32_t>(offsetof(M_cifouVurda13_t2175699772, ____sturfehawRerdouray_5)); }
	inline float get__sturfehawRerdouray_5() const { return ____sturfehawRerdouray_5; }
	inline float* get_address_of__sturfehawRerdouray_5() { return &____sturfehawRerdouray_5; }
	inline void set__sturfehawRerdouray_5(float value)
	{
		____sturfehawRerdouray_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
