﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_fetooPibo119
struct  M_fetooPibo119_t3744537518  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_fetooPibo119::_toumeMici
	uint32_t ____toumeMici_0;
	// System.Int32 GarbageiOS.M_fetooPibo119::_pelresu
	int32_t ____pelresu_1;
	// System.String GarbageiOS.M_fetooPibo119::_reejoGistallvou
	String_t* ____reejoGistallvou_2;
	// System.Int32 GarbageiOS.M_fetooPibo119::_hibeSaireve
	int32_t ____hibeSaireve_3;
	// System.UInt32 GarbageiOS.M_fetooPibo119::_harsawNowjou
	uint32_t ____harsawNowjou_4;
	// System.Single GarbageiOS.M_fetooPibo119::_halfai
	float ____halfai_5;
	// System.Single GarbageiOS.M_fetooPibo119::_kesaRomelner
	float ____kesaRomelner_6;
	// System.Int32 GarbageiOS.M_fetooPibo119::_whawgis
	int32_t ____whawgis_7;
	// System.UInt32 GarbageiOS.M_fetooPibo119::_hocuDebajou
	uint32_t ____hocuDebajou_8;
	// System.Boolean GarbageiOS.M_fetooPibo119::_hakeSoudowme
	bool ____hakeSoudowme_9;
	// System.Single GarbageiOS.M_fetooPibo119::_saji
	float ____saji_10;
	// System.Single GarbageiOS.M_fetooPibo119::_manereka
	float ____manereka_11;
	// System.UInt32 GarbageiOS.M_fetooPibo119::_mairhasbawRearmur
	uint32_t ____mairhasbawRearmur_12;
	// System.Int32 GarbageiOS.M_fetooPibo119::_jasur
	int32_t ____jasur_13;
	// System.Boolean GarbageiOS.M_fetooPibo119::_hounahe
	bool ____hounahe_14;
	// System.Boolean GarbageiOS.M_fetooPibo119::_hasi
	bool ____hasi_15;
	// System.Boolean GarbageiOS.M_fetooPibo119::_draykookal
	bool ____draykookal_16;
	// System.Boolean GarbageiOS.M_fetooPibo119::_dretucuPasfaysea
	bool ____dretucuPasfaysea_17;
	// System.UInt32 GarbageiOS.M_fetooPibo119::_tiserePemtowhou
	uint32_t ____tiserePemtowhou_18;
	// System.Boolean GarbageiOS.M_fetooPibo119::_maidirgiZairze
	bool ____maidirgiZairze_19;
	// System.Boolean GarbageiOS.M_fetooPibo119::_kuzer
	bool ____kuzer_20;
	// System.UInt32 GarbageiOS.M_fetooPibo119::_drouyurBostas
	uint32_t ____drouyurBostas_21;
	// System.Int32 GarbageiOS.M_fetooPibo119::_xucasallWidrelea
	int32_t ____xucasallWidrelea_22;
	// System.Boolean GarbageiOS.M_fetooPibo119::_ricemwarTelar
	bool ____ricemwarTelar_23;
	// System.Int32 GarbageiOS.M_fetooPibo119::_nisorrair
	int32_t ____nisorrair_24;
	// System.Int32 GarbageiOS.M_fetooPibo119::_draiballay
	int32_t ____draiballay_25;
	// System.Boolean GarbageiOS.M_fetooPibo119::_mairbejooRacur
	bool ____mairbejooRacur_26;
	// System.Int32 GarbageiOS.M_fetooPibo119::_fegaHusawhee
	int32_t ____fegaHusawhee_27;
	// System.UInt32 GarbageiOS.M_fetooPibo119::_narkamall
	uint32_t ____narkamall_28;
	// System.Int32 GarbageiOS.M_fetooPibo119::_pelmar
	int32_t ____pelmar_29;
	// System.Boolean GarbageiOS.M_fetooPibo119::_fawmaydiMorxa
	bool ____fawmaydiMorxa_30;
	// System.String GarbageiOS.M_fetooPibo119::_mirervallLawwhu
	String_t* ____mirervallLawwhu_31;
	// System.Single GarbageiOS.M_fetooPibo119::_headearporKakeefee
	float ____headearporKakeefee_32;
	// System.Boolean GarbageiOS.M_fetooPibo119::_gounefouKawmehay
	bool ____gounefouKawmehay_33;
	// System.Single GarbageiOS.M_fetooPibo119::_touchaipawWhirhallpaw
	float ____touchaipawWhirhallpaw_34;
	// System.UInt32 GarbageiOS.M_fetooPibo119::_pemmisbelCawner
	uint32_t ____pemmisbelCawner_35;
	// System.Boolean GarbageiOS.M_fetooPibo119::_sipena
	bool ____sipena_36;

public:
	inline static int32_t get_offset_of__toumeMici_0() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____toumeMici_0)); }
	inline uint32_t get__toumeMici_0() const { return ____toumeMici_0; }
	inline uint32_t* get_address_of__toumeMici_0() { return &____toumeMici_0; }
	inline void set__toumeMici_0(uint32_t value)
	{
		____toumeMici_0 = value;
	}

	inline static int32_t get_offset_of__pelresu_1() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____pelresu_1)); }
	inline int32_t get__pelresu_1() const { return ____pelresu_1; }
	inline int32_t* get_address_of__pelresu_1() { return &____pelresu_1; }
	inline void set__pelresu_1(int32_t value)
	{
		____pelresu_1 = value;
	}

	inline static int32_t get_offset_of__reejoGistallvou_2() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____reejoGistallvou_2)); }
	inline String_t* get__reejoGistallvou_2() const { return ____reejoGistallvou_2; }
	inline String_t** get_address_of__reejoGistallvou_2() { return &____reejoGistallvou_2; }
	inline void set__reejoGistallvou_2(String_t* value)
	{
		____reejoGistallvou_2 = value;
		Il2CppCodeGenWriteBarrier(&____reejoGistallvou_2, value);
	}

	inline static int32_t get_offset_of__hibeSaireve_3() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____hibeSaireve_3)); }
	inline int32_t get__hibeSaireve_3() const { return ____hibeSaireve_3; }
	inline int32_t* get_address_of__hibeSaireve_3() { return &____hibeSaireve_3; }
	inline void set__hibeSaireve_3(int32_t value)
	{
		____hibeSaireve_3 = value;
	}

	inline static int32_t get_offset_of__harsawNowjou_4() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____harsawNowjou_4)); }
	inline uint32_t get__harsawNowjou_4() const { return ____harsawNowjou_4; }
	inline uint32_t* get_address_of__harsawNowjou_4() { return &____harsawNowjou_4; }
	inline void set__harsawNowjou_4(uint32_t value)
	{
		____harsawNowjou_4 = value;
	}

	inline static int32_t get_offset_of__halfai_5() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____halfai_5)); }
	inline float get__halfai_5() const { return ____halfai_5; }
	inline float* get_address_of__halfai_5() { return &____halfai_5; }
	inline void set__halfai_5(float value)
	{
		____halfai_5 = value;
	}

	inline static int32_t get_offset_of__kesaRomelner_6() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____kesaRomelner_6)); }
	inline float get__kesaRomelner_6() const { return ____kesaRomelner_6; }
	inline float* get_address_of__kesaRomelner_6() { return &____kesaRomelner_6; }
	inline void set__kesaRomelner_6(float value)
	{
		____kesaRomelner_6 = value;
	}

	inline static int32_t get_offset_of__whawgis_7() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____whawgis_7)); }
	inline int32_t get__whawgis_7() const { return ____whawgis_7; }
	inline int32_t* get_address_of__whawgis_7() { return &____whawgis_7; }
	inline void set__whawgis_7(int32_t value)
	{
		____whawgis_7 = value;
	}

	inline static int32_t get_offset_of__hocuDebajou_8() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____hocuDebajou_8)); }
	inline uint32_t get__hocuDebajou_8() const { return ____hocuDebajou_8; }
	inline uint32_t* get_address_of__hocuDebajou_8() { return &____hocuDebajou_8; }
	inline void set__hocuDebajou_8(uint32_t value)
	{
		____hocuDebajou_8 = value;
	}

	inline static int32_t get_offset_of__hakeSoudowme_9() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____hakeSoudowme_9)); }
	inline bool get__hakeSoudowme_9() const { return ____hakeSoudowme_9; }
	inline bool* get_address_of__hakeSoudowme_9() { return &____hakeSoudowme_9; }
	inline void set__hakeSoudowme_9(bool value)
	{
		____hakeSoudowme_9 = value;
	}

	inline static int32_t get_offset_of__saji_10() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____saji_10)); }
	inline float get__saji_10() const { return ____saji_10; }
	inline float* get_address_of__saji_10() { return &____saji_10; }
	inline void set__saji_10(float value)
	{
		____saji_10 = value;
	}

	inline static int32_t get_offset_of__manereka_11() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____manereka_11)); }
	inline float get__manereka_11() const { return ____manereka_11; }
	inline float* get_address_of__manereka_11() { return &____manereka_11; }
	inline void set__manereka_11(float value)
	{
		____manereka_11 = value;
	}

	inline static int32_t get_offset_of__mairhasbawRearmur_12() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____mairhasbawRearmur_12)); }
	inline uint32_t get__mairhasbawRearmur_12() const { return ____mairhasbawRearmur_12; }
	inline uint32_t* get_address_of__mairhasbawRearmur_12() { return &____mairhasbawRearmur_12; }
	inline void set__mairhasbawRearmur_12(uint32_t value)
	{
		____mairhasbawRearmur_12 = value;
	}

	inline static int32_t get_offset_of__jasur_13() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____jasur_13)); }
	inline int32_t get__jasur_13() const { return ____jasur_13; }
	inline int32_t* get_address_of__jasur_13() { return &____jasur_13; }
	inline void set__jasur_13(int32_t value)
	{
		____jasur_13 = value;
	}

	inline static int32_t get_offset_of__hounahe_14() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____hounahe_14)); }
	inline bool get__hounahe_14() const { return ____hounahe_14; }
	inline bool* get_address_of__hounahe_14() { return &____hounahe_14; }
	inline void set__hounahe_14(bool value)
	{
		____hounahe_14 = value;
	}

	inline static int32_t get_offset_of__hasi_15() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____hasi_15)); }
	inline bool get__hasi_15() const { return ____hasi_15; }
	inline bool* get_address_of__hasi_15() { return &____hasi_15; }
	inline void set__hasi_15(bool value)
	{
		____hasi_15 = value;
	}

	inline static int32_t get_offset_of__draykookal_16() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____draykookal_16)); }
	inline bool get__draykookal_16() const { return ____draykookal_16; }
	inline bool* get_address_of__draykookal_16() { return &____draykookal_16; }
	inline void set__draykookal_16(bool value)
	{
		____draykookal_16 = value;
	}

	inline static int32_t get_offset_of__dretucuPasfaysea_17() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____dretucuPasfaysea_17)); }
	inline bool get__dretucuPasfaysea_17() const { return ____dretucuPasfaysea_17; }
	inline bool* get_address_of__dretucuPasfaysea_17() { return &____dretucuPasfaysea_17; }
	inline void set__dretucuPasfaysea_17(bool value)
	{
		____dretucuPasfaysea_17 = value;
	}

	inline static int32_t get_offset_of__tiserePemtowhou_18() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____tiserePemtowhou_18)); }
	inline uint32_t get__tiserePemtowhou_18() const { return ____tiserePemtowhou_18; }
	inline uint32_t* get_address_of__tiserePemtowhou_18() { return &____tiserePemtowhou_18; }
	inline void set__tiserePemtowhou_18(uint32_t value)
	{
		____tiserePemtowhou_18 = value;
	}

	inline static int32_t get_offset_of__maidirgiZairze_19() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____maidirgiZairze_19)); }
	inline bool get__maidirgiZairze_19() const { return ____maidirgiZairze_19; }
	inline bool* get_address_of__maidirgiZairze_19() { return &____maidirgiZairze_19; }
	inline void set__maidirgiZairze_19(bool value)
	{
		____maidirgiZairze_19 = value;
	}

	inline static int32_t get_offset_of__kuzer_20() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____kuzer_20)); }
	inline bool get__kuzer_20() const { return ____kuzer_20; }
	inline bool* get_address_of__kuzer_20() { return &____kuzer_20; }
	inline void set__kuzer_20(bool value)
	{
		____kuzer_20 = value;
	}

	inline static int32_t get_offset_of__drouyurBostas_21() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____drouyurBostas_21)); }
	inline uint32_t get__drouyurBostas_21() const { return ____drouyurBostas_21; }
	inline uint32_t* get_address_of__drouyurBostas_21() { return &____drouyurBostas_21; }
	inline void set__drouyurBostas_21(uint32_t value)
	{
		____drouyurBostas_21 = value;
	}

	inline static int32_t get_offset_of__xucasallWidrelea_22() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____xucasallWidrelea_22)); }
	inline int32_t get__xucasallWidrelea_22() const { return ____xucasallWidrelea_22; }
	inline int32_t* get_address_of__xucasallWidrelea_22() { return &____xucasallWidrelea_22; }
	inline void set__xucasallWidrelea_22(int32_t value)
	{
		____xucasallWidrelea_22 = value;
	}

	inline static int32_t get_offset_of__ricemwarTelar_23() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____ricemwarTelar_23)); }
	inline bool get__ricemwarTelar_23() const { return ____ricemwarTelar_23; }
	inline bool* get_address_of__ricemwarTelar_23() { return &____ricemwarTelar_23; }
	inline void set__ricemwarTelar_23(bool value)
	{
		____ricemwarTelar_23 = value;
	}

	inline static int32_t get_offset_of__nisorrair_24() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____nisorrair_24)); }
	inline int32_t get__nisorrair_24() const { return ____nisorrair_24; }
	inline int32_t* get_address_of__nisorrair_24() { return &____nisorrair_24; }
	inline void set__nisorrair_24(int32_t value)
	{
		____nisorrair_24 = value;
	}

	inline static int32_t get_offset_of__draiballay_25() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____draiballay_25)); }
	inline int32_t get__draiballay_25() const { return ____draiballay_25; }
	inline int32_t* get_address_of__draiballay_25() { return &____draiballay_25; }
	inline void set__draiballay_25(int32_t value)
	{
		____draiballay_25 = value;
	}

	inline static int32_t get_offset_of__mairbejooRacur_26() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____mairbejooRacur_26)); }
	inline bool get__mairbejooRacur_26() const { return ____mairbejooRacur_26; }
	inline bool* get_address_of__mairbejooRacur_26() { return &____mairbejooRacur_26; }
	inline void set__mairbejooRacur_26(bool value)
	{
		____mairbejooRacur_26 = value;
	}

	inline static int32_t get_offset_of__fegaHusawhee_27() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____fegaHusawhee_27)); }
	inline int32_t get__fegaHusawhee_27() const { return ____fegaHusawhee_27; }
	inline int32_t* get_address_of__fegaHusawhee_27() { return &____fegaHusawhee_27; }
	inline void set__fegaHusawhee_27(int32_t value)
	{
		____fegaHusawhee_27 = value;
	}

	inline static int32_t get_offset_of__narkamall_28() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____narkamall_28)); }
	inline uint32_t get__narkamall_28() const { return ____narkamall_28; }
	inline uint32_t* get_address_of__narkamall_28() { return &____narkamall_28; }
	inline void set__narkamall_28(uint32_t value)
	{
		____narkamall_28 = value;
	}

	inline static int32_t get_offset_of__pelmar_29() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____pelmar_29)); }
	inline int32_t get__pelmar_29() const { return ____pelmar_29; }
	inline int32_t* get_address_of__pelmar_29() { return &____pelmar_29; }
	inline void set__pelmar_29(int32_t value)
	{
		____pelmar_29 = value;
	}

	inline static int32_t get_offset_of__fawmaydiMorxa_30() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____fawmaydiMorxa_30)); }
	inline bool get__fawmaydiMorxa_30() const { return ____fawmaydiMorxa_30; }
	inline bool* get_address_of__fawmaydiMorxa_30() { return &____fawmaydiMorxa_30; }
	inline void set__fawmaydiMorxa_30(bool value)
	{
		____fawmaydiMorxa_30 = value;
	}

	inline static int32_t get_offset_of__mirervallLawwhu_31() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____mirervallLawwhu_31)); }
	inline String_t* get__mirervallLawwhu_31() const { return ____mirervallLawwhu_31; }
	inline String_t** get_address_of__mirervallLawwhu_31() { return &____mirervallLawwhu_31; }
	inline void set__mirervallLawwhu_31(String_t* value)
	{
		____mirervallLawwhu_31 = value;
		Il2CppCodeGenWriteBarrier(&____mirervallLawwhu_31, value);
	}

	inline static int32_t get_offset_of__headearporKakeefee_32() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____headearporKakeefee_32)); }
	inline float get__headearporKakeefee_32() const { return ____headearporKakeefee_32; }
	inline float* get_address_of__headearporKakeefee_32() { return &____headearporKakeefee_32; }
	inline void set__headearporKakeefee_32(float value)
	{
		____headearporKakeefee_32 = value;
	}

	inline static int32_t get_offset_of__gounefouKawmehay_33() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____gounefouKawmehay_33)); }
	inline bool get__gounefouKawmehay_33() const { return ____gounefouKawmehay_33; }
	inline bool* get_address_of__gounefouKawmehay_33() { return &____gounefouKawmehay_33; }
	inline void set__gounefouKawmehay_33(bool value)
	{
		____gounefouKawmehay_33 = value;
	}

	inline static int32_t get_offset_of__touchaipawWhirhallpaw_34() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____touchaipawWhirhallpaw_34)); }
	inline float get__touchaipawWhirhallpaw_34() const { return ____touchaipawWhirhallpaw_34; }
	inline float* get_address_of__touchaipawWhirhallpaw_34() { return &____touchaipawWhirhallpaw_34; }
	inline void set__touchaipawWhirhallpaw_34(float value)
	{
		____touchaipawWhirhallpaw_34 = value;
	}

	inline static int32_t get_offset_of__pemmisbelCawner_35() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____pemmisbelCawner_35)); }
	inline uint32_t get__pemmisbelCawner_35() const { return ____pemmisbelCawner_35; }
	inline uint32_t* get_address_of__pemmisbelCawner_35() { return &____pemmisbelCawner_35; }
	inline void set__pemmisbelCawner_35(uint32_t value)
	{
		____pemmisbelCawner_35 = value;
	}

	inline static int32_t get_offset_of__sipena_36() { return static_cast<int32_t>(offsetof(M_fetooPibo119_t3744537518, ____sipena_36)); }
	inline bool get__sipena_36() const { return ____sipena_36; }
	inline bool* get_address_of__sipena_36() { return &____sipena_36; }
	inline void set__sipena_36(bool value)
	{
		____sipena_36 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
