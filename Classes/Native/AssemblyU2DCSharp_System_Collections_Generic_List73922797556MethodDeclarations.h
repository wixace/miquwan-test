﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_Sort__ComparisonT1_T>c__AnonStorey96
struct U3CListA1_Sort__ComparisonT1_TU3Ec__AnonStorey96_t3922797556;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_Sort__ComparisonT1_T>c__AnonStorey96::.ctor()
extern "C"  void U3CListA1_Sort__ComparisonT1_TU3Ec__AnonStorey96__ctor_m329854455 (U3CListA1_Sort__ComparisonT1_TU3Ec__AnonStorey96_t3922797556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Collections_Generic_List77Generated/<ListA1_Sort__ComparisonT1_T>c__AnonStorey96::<>m__BB()
extern "C"  Il2CppObject * U3CListA1_Sort__ComparisonT1_TU3Ec__AnonStorey96_U3CU3Em__BB_m2211755701 (U3CListA1_Sort__ComparisonT1_TU3Ec__AnonStorey96_t3922797556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
