﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1244034627;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Filelist`1<System.Object>
struct  Filelist_1_t1153604171  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.String> Filelist`1::filelist
	List_1_t1375417109 * ___filelist_0;
	// System.Collections.Generic.List`1<T> Filelist`1::resdata
	List_1_t1244034627 * ___resdata_1;
	// System.Text.StringBuilder Filelist`1::sb
	StringBuilder_t243639308 * ___sb_2;

public:
	inline static int32_t get_offset_of_filelist_0() { return static_cast<int32_t>(offsetof(Filelist_1_t1153604171, ___filelist_0)); }
	inline List_1_t1375417109 * get_filelist_0() const { return ___filelist_0; }
	inline List_1_t1375417109 ** get_address_of_filelist_0() { return &___filelist_0; }
	inline void set_filelist_0(List_1_t1375417109 * value)
	{
		___filelist_0 = value;
		Il2CppCodeGenWriteBarrier(&___filelist_0, value);
	}

	inline static int32_t get_offset_of_resdata_1() { return static_cast<int32_t>(offsetof(Filelist_1_t1153604171, ___resdata_1)); }
	inline List_1_t1244034627 * get_resdata_1() const { return ___resdata_1; }
	inline List_1_t1244034627 ** get_address_of_resdata_1() { return &___resdata_1; }
	inline void set_resdata_1(List_1_t1244034627 * value)
	{
		___resdata_1 = value;
		Il2CppCodeGenWriteBarrier(&___resdata_1, value);
	}

	inline static int32_t get_offset_of_sb_2() { return static_cast<int32_t>(offsetof(Filelist_1_t1153604171, ___sb_2)); }
	inline StringBuilder_t243639308 * get_sb_2() const { return ___sb_2; }
	inline StringBuilder_t243639308 ** get_address_of_sb_2() { return &___sb_2; }
	inline void set_sb_2(StringBuilder_t243639308 * value)
	{
		___sb_2 = value;
		Il2CppCodeGenWriteBarrier(&___sb_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
