﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpringPositionGenerated
struct SpringPositionGenerated_t1264201657;
// JSVCall
struct JSVCall_t3708497963;
// SpringPosition/OnFinished
struct OnFinished_t2399837386;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;
// SpringPosition
struct SpringPosition_t3802689142;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void SpringPositionGenerated::.ctor()
extern "C"  void SpringPositionGenerated__ctor_m2496384978 (SpringPositionGenerated_t1264201657 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SpringPositionGenerated::SpringPosition_SpringPosition1(JSVCall,System.Int32)
extern "C"  bool SpringPositionGenerated_SpringPosition_SpringPosition1_m2881587808 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPositionGenerated::SpringPosition_current(JSVCall)
extern "C"  void SpringPositionGenerated_SpringPosition_current_m3048706533 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPositionGenerated::SpringPosition_target(JSVCall)
extern "C"  void SpringPositionGenerated_SpringPosition_target_m3080048221 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPositionGenerated::SpringPosition_strength(JSVCall)
extern "C"  void SpringPositionGenerated_SpringPosition_strength_m422298541 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPositionGenerated::SpringPosition_worldSpace(JSVCall)
extern "C"  void SpringPositionGenerated_SpringPosition_worldSpace_m2013871802 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPositionGenerated::SpringPosition_ignoreTimeScale(JSVCall)
extern "C"  void SpringPositionGenerated_SpringPosition_ignoreTimeScale_m1385768659 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPositionGenerated::SpringPosition_updateScrollView(JSVCall)
extern "C"  void SpringPositionGenerated_SpringPosition_updateScrollView_m3421954163 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SpringPosition/OnFinished SpringPositionGenerated::SpringPosition_onFinished_GetDelegate_member6_arg0(CSRepresentedObject)
extern "C"  OnFinished_t2399837386 * SpringPositionGenerated_SpringPosition_onFinished_GetDelegate_member6_arg0_m667265421 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPositionGenerated::SpringPosition_onFinished(JSVCall)
extern "C"  void SpringPositionGenerated_SpringPosition_onFinished_m3871511677 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPositionGenerated::SpringPosition_callWhenFinished(JSVCall)
extern "C"  void SpringPositionGenerated_SpringPosition_callWhenFinished_m3402235588 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SpringPositionGenerated::SpringPosition_Begin__GameObject__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool SpringPositionGenerated_SpringPosition_Begin__GameObject__Vector3__Single_m1785213179 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPositionGenerated::__Register()
extern "C"  void SpringPositionGenerated___Register_m3107504181 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SpringPosition/OnFinished SpringPositionGenerated::<SpringPosition_onFinished>m__9C()
extern "C"  OnFinished_t2399837386 * SpringPositionGenerated_U3CSpringPosition_onFinishedU3Em__9C_m2387978368 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SpringPositionGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool SpringPositionGenerated_ilo_attachFinalizerObject1_m2969455717 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SpringPositionGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * SpringPositionGenerated_ilo_getObject2_m3811941076 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPositionGenerated::ilo_setVector3S3(System.Int32,UnityEngine.Vector3)
extern "C"  void SpringPositionGenerated_ilo_setVector3S3_m19707575 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPositionGenerated::ilo_setBooleanS4(System.Int32,System.Boolean)
extern "C"  void SpringPositionGenerated_ilo_setBooleanS4_m2711144352 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPositionGenerated::ilo_setStringS5(System.Int32,System.String)
extern "C"  void SpringPositionGenerated_ilo_setStringS5_m573813663 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SpringPositionGenerated::ilo_getStringS6(System.Int32)
extern "C"  String_t* SpringPositionGenerated_ilo_getStringS6_m2743768251 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SpringPositionGenerated::ilo_getVector3S7(System.Int32)
extern "C"  Vector3_t4282066566  SpringPositionGenerated_ilo_getVector3S7_m662435834 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SpringPosition SpringPositionGenerated::ilo_Begin8(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  SpringPosition_t3802689142 * SpringPositionGenerated_ilo_Begin8_m1352269661 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, Vector3_t4282066566  ___pos1, float ___strength2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SpringPositionGenerated::ilo_isFunctionS9(System.Int32)
extern "C"  bool SpringPositionGenerated_ilo_isFunctionS9_m4182245898 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
