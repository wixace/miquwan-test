﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1190435706MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,JSMgr/JS_CS_Rel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2172589010(__this, ___dictionary0, method) ((  void (*) (Enumerator_t573723111 *, Dictionary_2_t3551367015 *, const MethodInfo*))Enumerator__ctor_m2377115088_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,JSMgr/JS_CS_Rel>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m387419929(__this, method) ((  Il2CppObject * (*) (Enumerator_t573723111 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1037642267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,JSMgr/JS_CS_Rel>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m764509603(__this, method) ((  void (*) (Enumerator_t573723111 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2809374949_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,JSMgr/JS_CS_Rel>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3120223578(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t573723111 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2434214620_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,JSMgr/JS_CS_Rel>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3170587957(__this, method) ((  Il2CppObject * (*) (Enumerator_t573723111 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3735627447_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,JSMgr/JS_CS_Rel>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2851650183(__this, method) ((  Il2CppObject * (*) (Enumerator_t573723111 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m393753481_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,JSMgr/JS_CS_Rel>::MoveNext()
#define Enumerator_MoveNext_m2646090236(__this, method) ((  bool (*) (Enumerator_t573723111 *, const MethodInfo*))Enumerator_MoveNext_m1213995029_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,JSMgr/JS_CS_Rel>::get_Current()
#define Enumerator_get_Current_m1102635057(__this, method) ((  KeyValuePair_2_t3450147721  (*) (Enumerator_t573723111 *, const MethodInfo*))Enumerator_get_Current_m1399860359_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,JSMgr/JS_CS_Rel>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2821931548(__this, method) ((  int32_t (*) (Enumerator_t573723111 *, const MethodInfo*))Enumerator_get_CurrentKey_m1767398110_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,JSMgr/JS_CS_Rel>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1859225052(__this, method) ((  JS_CS_Rel_t3554103776 * (*) (Enumerator_t573723111 *, const MethodInfo*))Enumerator_get_CurrentValue_m3384846750_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,JSMgr/JS_CS_Rel>::Reset()
#define Enumerator_Reset_m4063892260(__this, method) ((  void (*) (Enumerator_t573723111 *, const MethodInfo*))Enumerator_Reset_m1080084514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,JSMgr/JS_CS_Rel>::VerifyState()
#define Enumerator_VerifyState_m2543024173(__this, method) ((  void (*) (Enumerator_t573723111 *, const MethodInfo*))Enumerator_VerifyState_m2404513451_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,JSMgr/JS_CS_Rel>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2754710613(__this, method) ((  void (*) (Enumerator_t573723111 *, const MethodInfo*))Enumerator_VerifyCurrent_m2789892947_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,JSMgr/JS_CS_Rel>::Dispose()
#define Enumerator_Dispose_m3798618868(__this, method) ((  void (*) (Enumerator_t573723111 *, const MethodInfo*))Enumerator_Dispose_m1102561394_gshared)(__this, method)
