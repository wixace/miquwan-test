﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HeroMgr
struct HeroMgr_t2475965342;
// Hero
struct Hero_t2245658;
// CSHeroUnit
struct CSHeroUnit_t3764358446;
// Guide
struct Guide_t69159644;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<CSHeroUnit>
struct List_1_t837576702;
// JSCLevelHeroPointConfig
struct JSCLevelHeroPointConfig_t561690542;
// CameraSmoothFollow
struct CameraSmoothFollow_t2624612068;
// UnityEngine.Transform
struct Transform_t1659122786;
// CombatEntity
struct CombatEntity_t684137495;
// System.Collections.Generic.List`1<Hero>
struct List_1_t1370431210;
// System.Action
struct Action_t3771233898;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// JSCLevelHeroConfig
struct JSCLevelHeroConfig_t1953226502;
// LevelConfigManager
struct LevelConfigManager_t657947911;
// CSHeroData
struct CSHeroData_t3763839828;
// System.Object
struct Il2CppObject;
// herosCfg
struct herosCfg_t3676934635;
// TargetMgr
struct TargetMgr_t1188374183;
// SceneMgr
struct SceneMgr_t3584105996;
// TeamSkillMgr
struct TeamSkillMgr_t2030650276;
// System.String
struct String_t;
// CSCheckPointUnit
struct CSCheckPointUnit_t3129216924;
// GameMgr
struct GameMgr_t1469029542;
// JSCLevelMonsterConfig
struct JSCLevelMonsterConfig_t1924079698;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.Collections.Generic.List`1<JSCLevelHeroPointConfig>
struct List_1_t1929876094;
// HeroMgr/LineupPos
struct LineupPos_t2885729524;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// IZUpdate
struct IZUpdate_t3482043738;
// FightCtrl
struct FightCtrl_t648967803;
// NpcMgr
struct NpcMgr_t2339534743;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// HatredCtrl
struct HatredCtrl_t891253697;
// AnimationRunner
struct AnimationRunner_t1015409588;
// BuffCtrl
struct BuffCtrl_t2836564350;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Hero2245658.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_JSCLevelHeroPointConfig561690542.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_CSHeroUnit3764358446.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_HERO_ELEMENT2692579223.h"
#include "AssemblyU2DCSharp_HERO_COUNTRY1018279985.h"
#include "AssemblyU2DCSharp_LevelConfigManager657947911.h"
#include "AssemblyU2DCSharp_CSHeroData3763839828.h"
#include "AssemblyU2DCSharp_HeroMgr2475965342.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitType643359572.h"
#include "AssemblyU2DCSharp_HERO_TYPE820012127.h"
#include "AssemblyU2DCSharp_TargetMgr1188374183.h"
#include "AssemblyU2DCSharp_CameraSmoothFollow2624612068.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_TeamSkillMgr2030650276.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "AssemblyU2DCSharp_JSCLevelMonsterConfig1924079698.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_JSCLevelHeroConfig1953226502.h"
#include "AssemblyU2DCSharp_HeroMgr_LineupPos2885729524.h"
#include "AssemblyU2DCSharp_NpcMgr2339534743.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_HatredCtrl891253697.h"

// System.Void HeroMgr::.ctor()
extern "C"  void HeroMgr__ctor_m2541430157 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr HeroMgr::get_instance()
extern "C"  HeroMgr_t2475965342 * HeroMgr_get_instance_m157119232 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::set_isWaitTargetReached(System.Boolean)
extern "C"  void HeroMgr_set_isWaitTargetReached_m1430264871 (HeroMgr_t2475965342 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgr::get_isWaitTargetReached()
extern "C"  bool HeroMgr_get_isWaitTargetReached_m270499928 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgr::get_isUsingActiveSkill()
extern "C"  bool HeroMgr_get_isUsingActiveSkill_m2893469277 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::set_captain(Hero)
extern "C"  void HeroMgr_set_captain_m2999688312 (HeroMgr_t2475965342 * __this, Hero_t2245658 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero HeroMgr::get_captain()
extern "C"  Hero_t2245658 * HeroMgr_get_captain_m2238793193 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::Init()
extern "C"  void HeroMgr_Init_m172065575 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSHeroUnit HeroMgr::GetHeroUnit(System.UInt32)
extern "C"  CSHeroUnit_t3764358446 * HeroMgr_GetHeroUnit_m827533484 (HeroMgr_t2475965342 * __this, uint32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero HeroMgr::GetCaptain()
extern "C"  Hero_t2245658 * HeroMgr_GetCaptain_m2856624770 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Guide HeroMgr::CreateGuide(UnityEngine.GameObject)
extern "C"  Guide_t69159644 * HeroMgr_CreateGuide_m2953309626 (HeroMgr_t2475965342 * __this, GameObject_t3674682005 * ___parentObj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::CreateHeros(System.Collections.Generic.List`1<CSHeroUnit>,System.Int32)
extern "C"  void HeroMgr_CreateHeros_m2302456511 (HeroMgr_t2475965342 * __this, List_1_t837576702 * ___heroUnitList0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::CreateGadHero()
extern "C"  void HeroMgr_CreateGadHero_m4127363059 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::CreatGuideNpcs(System.Int32)
extern "C"  void HeroMgr_CreatGuideNpcs_m2942249197 (HeroMgr_t2475965342 * __this, int32_t ___round0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero HeroMgr::CreatGuideHeroNpc(System.Int32,JSCLevelHeroPointConfig)
extern "C"  Hero_t2245658 * HeroMgr_CreatGuideHeroNpc_m750189105 (HeroMgr_t2475965342 * __this, int32_t ___id0, JSCLevelHeroPointConfig_t561690542 * ___pointConfig1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroMgr::HeroListSort(Hero,Hero)
extern "C"  int32_t HeroMgr_HeroListSort_m2073817575 (HeroMgr_t2475965342 * __this, Hero_t2245658 * ___hero10, Hero_t2245658 * ___hero21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CameraSmoothFollow HeroMgr::get_CameraSmoothFollow()
extern "C"  CameraSmoothFollow_t2624612068 * HeroMgr_get_CameraSmoothFollow_m4237420203 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform HeroMgr::GetCameraTarger()
extern "C"  Transform_t1659122786 * HeroMgr_GetCameraTarger_m2967512595 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::SetCameraTargetObjPoint(CombatEntity,System.Boolean)
extern "C"  void HeroMgr_SetCameraTargetObjPoint_m3234469144 (HeroMgr_t2475965342 * __this, CombatEntity_t684137495 * ___catainHero0, bool ___isFollowHero1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero HeroMgr::GetHeroByMaxDaistance(CombatEntity)
extern "C"  Hero_t2245658 * HeroMgr_GetHeroByMaxDaistance_m3041263958 (HeroMgr_t2475965342 * __this, CombatEntity_t684137495 * ___catainHero0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero HeroMgr::CreateHero(CSHeroUnit,System.Boolean)
extern "C"  Hero_t2245658 * HeroMgr_CreateHero_m1493739521 (HeroMgr_t2475965342 * __this, CSHeroUnit_t3764358446 * ___unit0, bool ___isCatain1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSHeroUnit> HeroMgr::GetHeroUnits()
extern "C"  List_1_t837576702 * HeroMgr_GetHeroUnits_m2707622101 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ChangeHeroToDead(System.Int32)
extern "C"  void HeroMgr_ChangeHeroToDead_m3913557521 (HeroMgr_t2475965342 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSHeroUnit HeroMgr::GetHeroUnit(System.Collections.Generic.List`1<CSHeroUnit>,System.Int32)
extern "C"  CSHeroUnit_t3764358446 * HeroMgr_GetHeroUnit_m3849585221 (HeroMgr_t2475965342 * __this, List_1_t837576702 * ___units0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::HeroListSort(System.Collections.Generic.List`1<Hero>)
extern "C"  void HeroMgr_HeroListSort_m1827789983 (HeroMgr_t2475965342 * __this, List_1_t1370431210 * ___herolist0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::SetHeroPos()
extern "C"  void HeroMgr_SetHeroPos_m562382031 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::UpdateTargetPos(UnityEngine.Vector3,System.Boolean,System.Boolean,Entity.Behavior.EBehaviorID)
extern "C"  void HeroMgr_UpdateTargetPos_m1330970207 (HeroMgr_t2475965342 * __this, Vector3_t4282066566  ___targetPos0, bool ___isDoubleClick1, bool ___isFixedSpeed2, uint8_t ___bev3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::InitPlaceList(UnityEngine.Vector3)
extern "C"  void HeroMgr_InitPlaceList_m286081369 (HeroMgr_t2475965342 * __this, Vector3_t4282066566  ___vec0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HeroMgr::GetPlacePosByIndex(System.Int32,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  HeroMgr_GetPlacePosByIndex_m932111033 (HeroMgr_t2475965342 * __this, int32_t ___i0, Vector3_t4282066566  ___vec1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::RefreshPlace(UnityEngine.Vector3)
extern "C"  void HeroMgr_RefreshPlace_m3605801718 (HeroMgr_t2475965342 * __this, Vector3_t4282066566  ___vec0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ResetPlace()
extern "C"  void HeroMgr_ResetPlace_m4239646863 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::EnterBattle()
extern "C"  void HeroMgr_EnterBattle_m1541814491 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::EnterHeroBattle(Hero)
extern "C"  void HeroMgr_EnterHeroBattle_m2191911707 (HeroMgr_t2475965342 * __this, Hero_t2245658 * ___hero0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::LeaveBattleReq(System.Action)
extern "C"  void HeroMgr_LeaveBattleReq_m3167664279 (HeroMgr_t2475965342 * __this, Action_t3771233898 * ___callBack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::LeaveBattleReqOnWin(System.Action)
extern "C"  void HeroMgr_LeaveBattleReqOnWin_m681891204 (HeroMgr_t2475965342 * __this, Action_t3771233898 * ___callBack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgr::isCanLeaveBattle()
extern "C"  bool HeroMgr_isCanLeaveBattle_m4062421932 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::LeaveBattle()
extern "C"  void HeroMgr_LeaveBattle_m1084895674 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HeroMgr::GetAutoTarget(System.Collections.Generic.List`1<CombatEntity>)
extern "C"  Vector3_t4282066566  HeroMgr_GetAutoTarget_m392094446 (HeroMgr_t2475965342 * __this, List_1_t2052323047 * ___monsters0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgr::AllHerosIsAIMoveEndReached()
extern "C"  bool HeroMgr_AllHerosIsAIMoveEndReached_m2402218421 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::EnterAutoMode(System.Boolean)
extern "C"  void HeroMgr_EnterAutoMode_m638454220 (HeroMgr_t2475965342 * __this, bool ___isMarshalMove0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HeroMgr::MathfMarshalPos(Hero,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  HeroMgr_MathfMarshalPos_m1333657797 (HeroMgr_t2475965342 * __this, Hero_t2245658 * ___hero0, Vector3_t4282066566  ___pointPosition1, Vector3_t4282066566  ___eulerAngle2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ZUpdate()
extern "C"  void HeroMgr_ZUpdate_m2341485902 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgr::EnterAutoNext()
extern "C"  bool HeroMgr_EnterAutoNext_m50179353 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HeroMgr::GetMarshalPointNext()
extern "C"  GameObject_t3674682005 * HeroMgr_GetMarshalPointNext_m4090795131 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HeroMgr::GetMinMarshalPoint(System.Int32&)
extern "C"  GameObject_t3674682005 * HeroMgr_GetMinMarshalPoint_m803034955 (HeroMgr_t2475965342 * __this, int32_t* ___marshalIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgr::IsTrueHerosEndPoint()
extern "C"  bool HeroMgr_IsTrueHerosEndPoint_m1750150325 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgr::IsTrueMarshalIndex()
extern "C"  bool HeroMgr_IsTrueMarshalIndex_m1535839279 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity HeroMgr::FindTarget_AutoFight()
extern "C"  CombatEntity_t684137495 * HeroMgr_FindTarget_AutoFight_m3796644733 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgr::IsAtkRangeMonster(Hero,System.Single)
extern "C"  bool HeroMgr_IsAtkRangeMonster_m458082101 (HeroMgr_t2475965342 * __this, Hero_t2245658 * ___hero0, float ___range1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::LeaveAutoMode()
extern "C"  void HeroMgr_LeaveAutoMode_m590846516 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ChangeCaptain()
extern "C"  void HeroMgr_ChangeCaptain_m2990978303 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero HeroMgr::GetHero(System.Int32)
extern "C"  Hero_t2245658 * HeroMgr_GetHero_m3188633743 (HeroMgr_t2475965342 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero HeroMgr::GetHurtMaxHero()
extern "C"  Hero_t2245658 * HeroMgr_GetHurtMaxHero_m3175042701 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero HeroMgr::GetCureHero()
extern "C"  Hero_t2245658 * HeroMgr_GetCureHero_m3205402947 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero HeroMgr::GetRemoteHero()
extern "C"  Hero_t2245658 * HeroMgr_GetRemoteHero_m4071088612 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero HeroMgr::GetHPMinHero()
extern "C"  Hero_t2245658 * HeroMgr_GetHPMinHero_m2520374146 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero HeroMgr::GetNearestHero(CombatEntity)
extern "C"  Hero_t2245658 * HeroMgr_GetNearestHero_m2110131359 (HeroMgr_t2475965342 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero HeroMgr::GetNearestHero(UnityEngine.Vector3)
extern "C"  Hero_t2245658 * HeroMgr_GetNearestHero_m2979041987 (HeroMgr_t2475965342 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero HeroMgr::GetDefenseHero(CombatEntity)
extern "C"  Hero_t2245658 * HeroMgr_GetDefenseHero_m1450538497 (HeroMgr_t2475965342 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::AddHero(CombatEntity)
extern "C"  void HeroMgr_AddHero_m2329492719 (HeroMgr_t2475965342 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> HeroMgr::GetAllHero()
extern "C"  List_1_t2052323047 * HeroMgr_GetAllHero_m1169994226 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero HeroMgr::GetHeroByID(System.Int32)
extern "C"  Hero_t2245658 * HeroMgr_GetHeroByID_m2901230177 (HeroMgr_t2475965342 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero HeroMgr::GetHeroBySex(System.Int32)
extern "C"  Hero_t2245658 * HeroMgr_GetHeroBySex_m3963585892 (HeroMgr_t2475965342 * __this, int32_t ___sexType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Hero> HeroMgr::GetAllHeros()
extern "C"  List_1_t1370431210 * HeroMgr_GetAllHeros_m2375196134 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroMgr::GetHeroIntelligenceNum(System.Int32)
extern "C"  int32_t HeroMgr_GetHeroIntelligenceNum_m3710991761 (HeroMgr_t2475965342 * __this, int32_t ___intelligence0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgr::IsThreeElement(HERO_ELEMENT)
extern "C"  bool HeroMgr_IsThreeElement_m910083700 (HeroMgr_t2475965342 * __this, int32_t ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgr::IsThreeCountry(HERO_COUNTRY)
extern "C"  bool HeroMgr_IsThreeCountry_m1593951284 (HeroMgr_t2475965342 * __this, int32_t ___contyr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgr::IsAllElement()
extern "C"  bool HeroMgr_IsAllElement_m2949881992 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::Dead(CombatEntity)
extern "C"  void HeroMgr_Dead_m2031325508 (HeroMgr_t2475965342 * __this, CombatEntity_t684137495 * ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::FocusAttack(CombatEntity)
extern "C"  void HeroMgr_FocusAttack_m3083867188 (HeroMgr_t2475965342 * __this, CombatEntity_t684137495 * ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity HeroMgr::GetTotalDamageHero()
extern "C"  CombatEntity_t684137495 * HeroMgr_GetTotalDamageHero_m3969118920 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::Win()
extern "C"  void HeroMgr_Win_m988164615 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::Lose()
extern "C"  void HeroMgr_Lose_m259160044 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::Clear()
extern "C"  void HeroMgr_Clear_m4242530744 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::AddBuffOnTeamSkill()
extern "C"  void HeroMgr_AddBuffOnTeamSkill_m1340286200 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::RemoveBuffOnTeamEnd()
extern "C"  void HeroMgr_RemoveBuffOnTeamEnd_m4284952659 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity HeroMgr::GetMainTarget()
extern "C"  CombatEntity_t684137495 * HeroMgr_GetMainTarget_m3747568721 (HeroMgr_t2475965342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HeroMgr::GetFlyHeroPos(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  HeroMgr_GetFlyHeroPos_m2200860599 (HeroMgr_t2475965342 * __this, Vector3_t4282066566  ___vector30, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::SetHeroModelView(System.Boolean)
extern "C"  void HeroMgr_SetHeroModelView_m1332774816 (HeroMgr_t2475965342 * __this, bool ___isShow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroMgr::<HeroListSort>m__3D2(Hero,Hero)
extern "C"  int32_t HeroMgr_U3CHeroListSortU3Em__3D2_m1718197213 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ___x0, Hero_t2245658 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroMgr::<GetMinMarshalPoint>m__3D3(UnityEngine.GameObject,UnityEngine.GameObject)
extern "C"  int32_t HeroMgr_U3CGetMinMarshalPointU3Em__3D3_m841598292 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___obj10, GameObject_t3674682005 * ___obj21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroMgr::<FindTarget_AutoFight>m__3D4(CombatEntity,CombatEntity)
extern "C"  int32_t HeroMgr_U3CFindTarget_AutoFightU3Em__3D4_m4270213297 (HeroMgr_t2475965342 * __this, CombatEntity_t684137495 * ___combatEntity10, CombatEntity_t684137495 * ___combatEntity21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSCLevelHeroConfig HeroMgr::ilo_GetHeroConfig1(LevelConfigManager,System.Int32)
extern "C"  JSCLevelHeroConfig_t1953226502 * HeroMgr_ilo_GetHeroConfig1_m3618479436 (Il2CppObject * __this /* static, unused */, LevelConfigManager_t657947911 * ____this0, int32_t ___level1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSHeroUnit> HeroMgr::ilo_GetFightingHeroUntis2(CSHeroData,System.Int32)
extern "C"  List_1_t837576702 * HeroMgr_ilo_GetFightingHeroUntis2_m4106772579 (Il2CppObject * __this /* static, unused */, CSHeroData_t3763839828 * ____this0, int32_t ___fuctionType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_EnterAutoMode3(HeroMgr,System.Boolean)
extern "C"  void HeroMgr_ilo_EnterAutoMode3_m1908952124 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, bool ___isMarshalMove1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero HeroMgr::ilo_CreateHero4(HeroMgr,CSHeroUnit,System.Boolean)
extern "C"  Hero_t2245658 * HeroMgr_ilo_CreateHero4_m1067191936 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, CSHeroUnit_t3764358446 * ___unit1, bool ___isCatain2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_GadInit5(Hero)
extern "C"  void HeroMgr_ilo_GadInit5_m20595051 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSCLevelHeroConfig HeroMgr::ilo_GetGuideHeroConfig6(LevelConfigManager,System.Int32)
extern "C"  JSCLevelHeroConfig_t1953226502 * HeroMgr_ilo_GetGuideHeroConfig6_m446905595 (Il2CppObject * __this /* static, unused */, LevelConfigManager_t657947911 * ____this0, int32_t ___level1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HeroMgr::ilo_Instantiate7(UnityEngine.GameObject)
extern "C"  GameObject_t3674682005 * HeroMgr_ilo_Instantiate7_m1305383802 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_LogError8(System.Object,System.Boolean)
extern "C"  void HeroMgr_ilo_LogError8_m2431871125 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_OnTriggerFun9(Hero,System.Int32)
extern "C"  void HeroMgr_ilo_OnTriggerFun9_m4285309158 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, int32_t ___effectId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr HeroMgr::ilo_get_instance10()
extern "C"  HeroMgr_t2475965342 * HeroMgr_ilo_get_instance10_m2105518418 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitType HeroMgr::ilo_get_unitType11(CombatEntity)
extern "C"  int32_t HeroMgr_ilo_get_unitType11_m175489935 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HERO_TYPE HeroMgr::ilo_get_heromajor12(CombatEntity)
extern "C"  int32_t HeroMgr_ilo_get_heromajor12_m3952429388 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroMgr::ilo_get_atkPower13(CombatEntity)
extern "C"  float HeroMgr_ilo_get_atkPower13_m1622279263 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroMgr::ilo_get_damageType14(CombatEntity)
extern "C"  int32_t HeroMgr_ilo_get_damageType14_m2294492314 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// herosCfg HeroMgr::ilo_get_config15(CSHeroUnit)
extern "C"  herosCfg_t3676934635 * HeroMgr_ilo_get_config15_m1475448185 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero HeroMgr::ilo_get_captain16(HeroMgr)
extern "C"  Hero_t2245658 * HeroMgr_ilo_get_captain16_m3305372793 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TargetMgr HeroMgr::ilo_get_instance17()
extern "C"  TargetMgr_t1188374183 * HeroMgr_ilo_get_instance17_m1205078946 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SceneMgr HeroMgr::ilo_get_SceneMgr18()
extern "C"  SceneMgr_t3584105996 * HeroMgr_ilo_get_SceneMgr18_m2396960725 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HeroMgr::ilo_FromAndToCenterlinePoint19(TargetMgr,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t4282066566  HeroMgr_ilo_FromAndToCenterlinePoint19_m305597855 (Il2CppObject * __this /* static, unused */, TargetMgr_t1188374183 * ____this0, Vector3_t4282066566  ___form1, Vector3_t4282066566  ___to2, float ___distance3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TeamSkillMgr HeroMgr::ilo_get_TeamSkillMgr20()
extern "C"  TeamSkillMgr_t2030650276 * HeroMgr_ilo_get_TeamSkillMgr20_m1362446684 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CameraSmoothFollow HeroMgr::ilo_get_CameraSmoothFollow21(HeroMgr)
extern "C"  CameraSmoothFollow_t2624612068 * HeroMgr_ilo_get_CameraSmoothFollow21_m3521587543 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_ChangeTarget22(CameraSmoothFollow,UnityEngine.Transform)
extern "C"  void HeroMgr_ilo_ChangeTarget22_m28454718 (Il2CppObject * __this /* static, unused */, CameraSmoothFollow_t2624612068 * ____this0, Transform_t1659122786 * ____target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgr::ilo_get_isUseingSkill23(TeamSkillMgr)
extern "C"  bool HeroMgr_ilo_get_isUseingSkill23_m1934205420 (Il2CppObject * __this /* static, unused */, TeamSkillMgr_t2030650276 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HeroMgr::ilo_get_ResourcesName24(CSHeroUnit)
extern "C"  String_t* HeroMgr_ilo_get_ResourcesName24_m1479509878 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_Create25(Hero,CSHeroUnit)
extern "C"  void HeroMgr_ilo_Create25_m2761939785 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, CSHeroUnit_t3764358446 * ___hcfg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSCheckPointUnit HeroMgr::ilo_get_checkPoint26(GameMgr)
extern "C"  CSCheckPointUnit_t3129216924 * HeroMgr_ilo_get_checkPoint26_m75649646 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_set_z27(JSCLevelMonsterConfig,System.Single)
extern "C"  void HeroMgr_ilo_set_z27_m41554539 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_set_rot28(JSCLevelMonsterConfig,System.Single)
extern "C"  void HeroMgr_ilo_set_rot28_m1523864973 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_InitPos29(Hero,UnityEngine.Vector3,UnityEngine.Quaternion,System.Boolean,JSCLevelHeroPointConfig)
extern "C"  void HeroMgr_ilo_InitPos29_m1918786374 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, Vector3_t4282066566  ___pos1, Quaternion_t1553702882  ___rot2, bool ___isTrue3, JSCLevelHeroPointConfig_t561690542 * ___lhcfg4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_DispatchEvent30(CEvent.ZEvent)
extern "C"  void HeroMgr_ilo_DispatchEvent30_m460336614 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<JSCLevelHeroPointConfig> HeroMgr::ilo_get_pointList31(JSCLevelHeroConfig)
extern "C"  List_1_t1929876094 * HeroMgr_ilo_get_pointList31_m1632430180 (Il2CppObject * __this /* static, unused */, JSCLevelHeroConfig_t1953226502 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_Rigester32(HeroMgr/LineupPos)
extern "C"  void HeroMgr_ilo_Rigester32_m2443709746 (Il2CppObject * __this /* static, unused */, LineupPos_t2885729524 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HeroMgr::ilo_get_Pos33(HeroMgr/LineupPos)
extern "C"  Vector3_t4282066566  HeroMgr_ilo_get_Pos33_m1285795201 (Il2CppObject * __this /* static, unused */, LineupPos_t2885729524 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HeroMgr::ilo_get_position34(CombatEntity)
extern "C"  Vector3_t4282066566  HeroMgr_ilo_get_position34_m4177514828 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl HeroMgr::ilo_get_bvrCtrl35(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * HeroMgr_ilo_get_bvrCtrl35_m171805569 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroMgr::ilo_get_y36(JSCLevelHeroPointConfig)
extern "C"  float HeroMgr_ilo_get_y36_m1552061677 (Il2CppObject * __this /* static, unused */, JSCLevelHeroPointConfig_t561690542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroMgr::ilo_get_spacing37(JSCLevelHeroConfig)
extern "C"  float HeroMgr_ilo_get_spacing37_m605154876 (Il2CppObject * __this /* static, unused */, JSCLevelHeroConfig_t1953226502 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_set_Pos38(HeroMgr/LineupPos,UnityEngine.Vector3)
extern "C"  void HeroMgr_ilo_set_Pos38_m3851564701 (Il2CppObject * __this /* static, unused */, LineupPos_t2885729524 * ____this0, Vector3_t4282066566  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HeroMgr::ilo_GetPlacePosByIndex39(HeroMgr,System.Int32,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  HeroMgr_ilo_GetPlacePosByIndex39_m3945782020 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, int32_t ___i1, Vector3_t4282066566  ___vec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_LeaveBattle40(HeroMgr)
extern "C"  void HeroMgr_ilo_LeaveBattle40_m2763596753 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_AddUpdate41(IZUpdate)
extern "C"  void HeroMgr_ilo_AddUpdate41_m4224586181 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_set_IsBattling42(Hero,System.Boolean)
extern "C"  void HeroMgr_ilo_set_IsBattling42_m4126321491 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightCtrl HeroMgr::ilo_get_fightCtrl43(CombatEntity)
extern "C"  FightCtrl_t648967803 * HeroMgr_ilo_get_fightCtrl43_m1079135786 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroMgr::ilo_get_curRound44(NpcMgr)
extern "C"  int32_t HeroMgr_ilo_get_curRound44_m1454348572 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_TranBehavior45(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void HeroMgr_ilo_TranBehavior45_m4048142508 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___id1, ObjectU5BU5D_t1108656482* ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_LogWarning46(System.Object,System.Boolean)
extern "C"  void HeroMgr_ilo_LogWarning46_m2074309933 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_RemoveUpdate47(IZUpdate)
extern "C"  void HeroMgr_ilo_RemoveUpdate47_m1673547360 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_set_unitType48(CombatEntity,EntityEnum.UnitType)
extern "C"  void HeroMgr_ilo_set_unitType48_m1452189494 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_SetCameraTargetObjPoint49(HeroMgr,CombatEntity,System.Boolean)
extern "C"  void HeroMgr_ilo_SetCameraTargetObjPoint49_m2545475252 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, CombatEntity_t684137495 * ___catainHero1, bool ___isFollowHero2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroMgr::ilo_get_id50(CombatEntity)
extern "C"  int32_t HeroMgr_ilo_get_id50_m3743205604 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroMgr::ilo_get_totalDamage51(CombatEntity)
extern "C"  float HeroMgr_ilo_get_totalDamage51_m2678847187 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroMgr::ilo_get_dutytype52(CombatEntity)
extern "C"  int32_t HeroMgr_ilo_get_dutytype52_m3894340187 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HERO_ELEMENT HeroMgr::ilo_get_element53(CombatEntity)
extern "C"  int32_t HeroMgr_ilo_get_element53_m1315601464 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_AddHatred54(HatredCtrl,CombatEntity,System.Single)
extern "C"  void HeroMgr_ilo_AddHatred54_m3009563623 (Il2CppObject * __this /* static, unused */, HatredCtrl_t891253697 * ____this0, CombatEntity_t684137495 * ___entity1, float ___hatred2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NpcMgr HeroMgr::ilo_get_instance55()
extern "C"  NpcMgr_t2339534743 * HeroMgr_ilo_get_instance55_m1362675490 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> HeroMgr::ilo_GetAllFriendNpc56(NpcMgr)
extern "C"  List_1_t2052323047 * HeroMgr_ilo_GetAllFriendNpc56_m3236474020 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_LookAt57(CombatEntity,UnityEngine.Vector3)
extern "C"  void HeroMgr_ilo_LookAt57_m1697030714 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, Vector3_t4282066566  ___pos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnimationRunner HeroMgr::ilo_get_characterAnim58(CombatEntity)
extern "C"  AnimationRunner_t1015409588 * HeroMgr_ilo_get_characterAnim58_m1428189684 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_Clear59(CombatEntity)
extern "C"  void HeroMgr_ilo_Clear59_m4012092050 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BuffCtrl HeroMgr::ilo_get_buffCtrl60(CombatEntity)
extern "C"  BuffCtrl_t2836564350 * HeroMgr_ilo_get_buffCtrl60_m1213734965 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr::ilo_set_isNotSelected61(CombatEntity,System.Boolean)
extern "C"  void HeroMgr_ilo_set_isNotSelected61_m3620430202 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroMgr::ilo_get_atkType62(CombatEntity)
extern "C"  int32_t HeroMgr_ilo_get_atkType62_m671202132 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
