﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// checkpointCfg
struct checkpointCfg_t2816107964;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void checkpointCfg::.ctor()
extern "C"  void checkpointCfg__ctor_m3551571759 (checkpointCfg_t2816107964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void checkpointCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void checkpointCfg_Init_m1711484406 (checkpointCfg_t2816107964 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
