﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CombatEntity
struct CombatEntity_t684137495;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitStateBase
struct  UnitStateBase_t1040218558  : public Il2CppObject
{
public:
	// CombatEntity UnitStateBase::entity
	CombatEntity_t684137495 * ___entity_0;
	// System.Single UnitStateBase::doTime
	float ___doTime_1;
	// System.String UnitStateBase::aniName
	String_t* ___aniName_2;
	// System.Boolean UnitStateBase::isMoveEnd
	bool ___isMoveEnd_3;
	// System.Single UnitStateBase::doDelayTime
	float ___doDelayTime_4;

public:
	inline static int32_t get_offset_of_entity_0() { return static_cast<int32_t>(offsetof(UnitStateBase_t1040218558, ___entity_0)); }
	inline CombatEntity_t684137495 * get_entity_0() const { return ___entity_0; }
	inline CombatEntity_t684137495 ** get_address_of_entity_0() { return &___entity_0; }
	inline void set_entity_0(CombatEntity_t684137495 * value)
	{
		___entity_0 = value;
		Il2CppCodeGenWriteBarrier(&___entity_0, value);
	}

	inline static int32_t get_offset_of_doTime_1() { return static_cast<int32_t>(offsetof(UnitStateBase_t1040218558, ___doTime_1)); }
	inline float get_doTime_1() const { return ___doTime_1; }
	inline float* get_address_of_doTime_1() { return &___doTime_1; }
	inline void set_doTime_1(float value)
	{
		___doTime_1 = value;
	}

	inline static int32_t get_offset_of_aniName_2() { return static_cast<int32_t>(offsetof(UnitStateBase_t1040218558, ___aniName_2)); }
	inline String_t* get_aniName_2() const { return ___aniName_2; }
	inline String_t** get_address_of_aniName_2() { return &___aniName_2; }
	inline void set_aniName_2(String_t* value)
	{
		___aniName_2 = value;
		Il2CppCodeGenWriteBarrier(&___aniName_2, value);
	}

	inline static int32_t get_offset_of_isMoveEnd_3() { return static_cast<int32_t>(offsetof(UnitStateBase_t1040218558, ___isMoveEnd_3)); }
	inline bool get_isMoveEnd_3() const { return ___isMoveEnd_3; }
	inline bool* get_address_of_isMoveEnd_3() { return &___isMoveEnd_3; }
	inline void set_isMoveEnd_3(bool value)
	{
		___isMoveEnd_3 = value;
	}

	inline static int32_t get_offset_of_doDelayTime_4() { return static_cast<int32_t>(offsetof(UnitStateBase_t1040218558, ___doDelayTime_4)); }
	inline float get_doDelayTime_4() const { return ___doDelayTime_4; }
	inline float* get_address_of_doDelayTime_4() { return &___doDelayTime_4; }
	inline void set_doDelayTime_4(float value)
	{
		___doDelayTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
