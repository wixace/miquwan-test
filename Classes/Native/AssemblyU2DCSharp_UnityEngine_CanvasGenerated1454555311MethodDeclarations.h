﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_CanvasGenerated
struct UnityEngine_CanvasGenerated_t1454555311;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_CanvasGenerated::.ctor()
extern "C"  void UnityEngine_CanvasGenerated__ctor_m2777825180 (UnityEngine_CanvasGenerated_t1454555311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CanvasGenerated::Canvas_Canvas1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CanvasGenerated_Canvas_Canvas1_m425127958 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::Canvas_renderMode(JSVCall)
extern "C"  void UnityEngine_CanvasGenerated_Canvas_renderMode_m3521814413 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::Canvas_isRootCanvas(JSVCall)
extern "C"  void UnityEngine_CanvasGenerated_Canvas_isRootCanvas_m3386137250 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::Canvas_worldCamera(JSVCall)
extern "C"  void UnityEngine_CanvasGenerated_Canvas_worldCamera_m486105039 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::Canvas_pixelRect(JSVCall)
extern "C"  void UnityEngine_CanvasGenerated_Canvas_pixelRect_m3530277116 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::Canvas_scaleFactor(JSVCall)
extern "C"  void UnityEngine_CanvasGenerated_Canvas_scaleFactor_m1483105325 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::Canvas_referencePixelsPerUnit(JSVCall)
extern "C"  void UnityEngine_CanvasGenerated_Canvas_referencePixelsPerUnit_m464292509 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::Canvas_overridePixelPerfect(JSVCall)
extern "C"  void UnityEngine_CanvasGenerated_Canvas_overridePixelPerfect_m2524464275 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::Canvas_pixelPerfect(JSVCall)
extern "C"  void UnityEngine_CanvasGenerated_Canvas_pixelPerfect_m1523890431 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::Canvas_planeDistance(JSVCall)
extern "C"  void UnityEngine_CanvasGenerated_Canvas_planeDistance_m3091411957 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::Canvas_renderOrder(JSVCall)
extern "C"  void UnityEngine_CanvasGenerated_Canvas_renderOrder_m3220505326 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::Canvas_overrideSorting(JSVCall)
extern "C"  void UnityEngine_CanvasGenerated_Canvas_overrideSorting_m1353558574 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::Canvas_sortingOrder(JSVCall)
extern "C"  void UnityEngine_CanvasGenerated_Canvas_sortingOrder_m1478519516 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::Canvas_targetDisplay(JSVCall)
extern "C"  void UnityEngine_CanvasGenerated_Canvas_targetDisplay_m24135797 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::Canvas_sortingGridNormalizedSize(JSVCall)
extern "C"  void UnityEngine_CanvasGenerated_Canvas_sortingGridNormalizedSize_m2151733348 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::Canvas_sortingLayerID(JSVCall)
extern "C"  void UnityEngine_CanvasGenerated_Canvas_sortingLayerID_m2812892350 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::Canvas_cachedSortingLayerValue(JSVCall)
extern "C"  void UnityEngine_CanvasGenerated_Canvas_cachedSortingLayerValue_m3497767748 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::Canvas_sortingLayerName(JSVCall)
extern "C"  void UnityEngine_CanvasGenerated_Canvas_sortingLayerName_m1124401262 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CanvasGenerated::Canvas_ForceUpdateCanvases(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CanvasGenerated_Canvas_ForceUpdateCanvases_m2516572471 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CanvasGenerated::Canvas_GetDefaultCanvasMaterial(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CanvasGenerated_Canvas_GetDefaultCanvasMaterial_m749037231 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::__Register()
extern "C"  void UnityEngine_CanvasGenerated___Register_m4140998315 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::ilo_setEnum1(System.Int32,System.Int32)
extern "C"  void UnityEngine_CanvasGenerated_ilo_setEnum1_m1857215805 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CanvasGenerated::ilo_getEnum2(System.Int32)
extern "C"  int32_t UnityEngine_CanvasGenerated_ilo_getEnum2_m3763299033 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void UnityEngine_CanvasGenerated_ilo_setBooleanS3_m4079641675 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CanvasGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_CanvasGenerated_ilo_setObject4_m588105557 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void UnityEngine_CanvasGenerated_ilo_setSingle5_m2893702492 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_CanvasGenerated::ilo_getSingle6(System.Int32)
extern "C"  float UnityEngine_CanvasGenerated_ilo_getSingle6_m2866343736 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CanvasGenerated::ilo_getBooleanS7(System.Int32)
extern "C"  bool UnityEngine_CanvasGenerated_ilo_getBooleanS7_m2672181582 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CanvasGenerated::ilo_setInt328(System.Int32,System.Int32)
extern "C"  void UnityEngine_CanvasGenerated_ilo_setInt328_m3065244499 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CanvasGenerated::ilo_getInt329(System.Int32)
extern "C"  int32_t UnityEngine_CanvasGenerated_ilo_getInt329_m1630221127 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
