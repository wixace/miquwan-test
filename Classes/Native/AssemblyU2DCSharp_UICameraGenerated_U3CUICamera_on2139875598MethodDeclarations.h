﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICameraGenerated/<UICamera_onDoubleClick_GetDelegate_member43_arg0>c__AnonStoreyA4
struct U3CUICamera_onDoubleClick_GetDelegate_member43_arg0U3Ec__AnonStoreyA4_t2139875598;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UICameraGenerated/<UICamera_onDoubleClick_GetDelegate_member43_arg0>c__AnonStoreyA4::.ctor()
extern "C"  void U3CUICamera_onDoubleClick_GetDelegate_member43_arg0U3Ec__AnonStoreyA4__ctor_m924924445 (U3CUICamera_onDoubleClick_GetDelegate_member43_arg0U3Ec__AnonStoreyA4_t2139875598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated/<UICamera_onDoubleClick_GetDelegate_member43_arg0>c__AnonStoreyA4::<>m__10C(UnityEngine.GameObject)
extern "C"  void U3CUICamera_onDoubleClick_GetDelegate_member43_arg0U3Ec__AnonStoreyA4_U3CU3Em__10C_m1474021752 (U3CUICamera_onDoubleClick_GetDelegate_member43_arg0U3Ec__AnonStoreyA4_t2139875598 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
