﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_lerelaimuSosay145
struct  M_lerelaimuSosay145_t2035771315  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_lerelaimuSosay145::_steso
	uint32_t ____steso_0;
	// System.Boolean GarbageiOS.M_lerelaimuSosay145::_dooleXoogal
	bool ____dooleXoogal_1;
	// System.Boolean GarbageiOS.M_lerelaimuSosay145::_dayraiwaNachi
	bool ____dayraiwaNachi_2;
	// System.Boolean GarbageiOS.M_lerelaimuSosay145::_mairnalhaJemu
	bool ____mairnalhaJemu_3;
	// System.Boolean GarbageiOS.M_lerelaimuSosay145::_baskirne
	bool ____baskirne_4;
	// System.UInt32 GarbageiOS.M_lerelaimuSosay145::_hucelRouror
	uint32_t ____hucelRouror_5;
	// System.UInt32 GarbageiOS.M_lerelaimuSosay145::_furru
	uint32_t ____furru_6;
	// System.UInt32 GarbageiOS.M_lerelaimuSosay145::_meyamoVestou
	uint32_t ____meyamoVestou_7;
	// System.Boolean GarbageiOS.M_lerelaimuSosay145::_xemfava
	bool ____xemfava_8;
	// System.Boolean GarbageiOS.M_lerelaimuSosay145::_nadasMomai
	bool ____nadasMomai_9;

public:
	inline static int32_t get_offset_of__steso_0() { return static_cast<int32_t>(offsetof(M_lerelaimuSosay145_t2035771315, ____steso_0)); }
	inline uint32_t get__steso_0() const { return ____steso_0; }
	inline uint32_t* get_address_of__steso_0() { return &____steso_0; }
	inline void set__steso_0(uint32_t value)
	{
		____steso_0 = value;
	}

	inline static int32_t get_offset_of__dooleXoogal_1() { return static_cast<int32_t>(offsetof(M_lerelaimuSosay145_t2035771315, ____dooleXoogal_1)); }
	inline bool get__dooleXoogal_1() const { return ____dooleXoogal_1; }
	inline bool* get_address_of__dooleXoogal_1() { return &____dooleXoogal_1; }
	inline void set__dooleXoogal_1(bool value)
	{
		____dooleXoogal_1 = value;
	}

	inline static int32_t get_offset_of__dayraiwaNachi_2() { return static_cast<int32_t>(offsetof(M_lerelaimuSosay145_t2035771315, ____dayraiwaNachi_2)); }
	inline bool get__dayraiwaNachi_2() const { return ____dayraiwaNachi_2; }
	inline bool* get_address_of__dayraiwaNachi_2() { return &____dayraiwaNachi_2; }
	inline void set__dayraiwaNachi_2(bool value)
	{
		____dayraiwaNachi_2 = value;
	}

	inline static int32_t get_offset_of__mairnalhaJemu_3() { return static_cast<int32_t>(offsetof(M_lerelaimuSosay145_t2035771315, ____mairnalhaJemu_3)); }
	inline bool get__mairnalhaJemu_3() const { return ____mairnalhaJemu_3; }
	inline bool* get_address_of__mairnalhaJemu_3() { return &____mairnalhaJemu_3; }
	inline void set__mairnalhaJemu_3(bool value)
	{
		____mairnalhaJemu_3 = value;
	}

	inline static int32_t get_offset_of__baskirne_4() { return static_cast<int32_t>(offsetof(M_lerelaimuSosay145_t2035771315, ____baskirne_4)); }
	inline bool get__baskirne_4() const { return ____baskirne_4; }
	inline bool* get_address_of__baskirne_4() { return &____baskirne_4; }
	inline void set__baskirne_4(bool value)
	{
		____baskirne_4 = value;
	}

	inline static int32_t get_offset_of__hucelRouror_5() { return static_cast<int32_t>(offsetof(M_lerelaimuSosay145_t2035771315, ____hucelRouror_5)); }
	inline uint32_t get__hucelRouror_5() const { return ____hucelRouror_5; }
	inline uint32_t* get_address_of__hucelRouror_5() { return &____hucelRouror_5; }
	inline void set__hucelRouror_5(uint32_t value)
	{
		____hucelRouror_5 = value;
	}

	inline static int32_t get_offset_of__furru_6() { return static_cast<int32_t>(offsetof(M_lerelaimuSosay145_t2035771315, ____furru_6)); }
	inline uint32_t get__furru_6() const { return ____furru_6; }
	inline uint32_t* get_address_of__furru_6() { return &____furru_6; }
	inline void set__furru_6(uint32_t value)
	{
		____furru_6 = value;
	}

	inline static int32_t get_offset_of__meyamoVestou_7() { return static_cast<int32_t>(offsetof(M_lerelaimuSosay145_t2035771315, ____meyamoVestou_7)); }
	inline uint32_t get__meyamoVestou_7() const { return ____meyamoVestou_7; }
	inline uint32_t* get_address_of__meyamoVestou_7() { return &____meyamoVestou_7; }
	inline void set__meyamoVestou_7(uint32_t value)
	{
		____meyamoVestou_7 = value;
	}

	inline static int32_t get_offset_of__xemfava_8() { return static_cast<int32_t>(offsetof(M_lerelaimuSosay145_t2035771315, ____xemfava_8)); }
	inline bool get__xemfava_8() const { return ____xemfava_8; }
	inline bool* get_address_of__xemfava_8() { return &____xemfava_8; }
	inline void set__xemfava_8(bool value)
	{
		____xemfava_8 = value;
	}

	inline static int32_t get_offset_of__nadasMomai_9() { return static_cast<int32_t>(offsetof(M_lerelaimuSosay145_t2035771315, ____nadasMomai_9)); }
	inline bool get__nadasMomai_9() const { return ____nadasMomai_9; }
	inline bool* get_address_of__nadasMomai_9() { return &____nadasMomai_9; }
	inline void set__nadasMomai_9(bool value)
	{
		____nadasMomai_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
