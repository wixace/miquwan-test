﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._105105725aab512a55c3eca9cd73fc9b
struct _105105725aab512a55c3eca9cd73fc9b_t654465045;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__105105725aab512a55c3eca9c654465045.h"

// System.Void Little._105105725aab512a55c3eca9cd73fc9b::.ctor()
extern "C"  void _105105725aab512a55c3eca9cd73fc9b__ctor_m2682918328 (_105105725aab512a55c3eca9cd73fc9b_t654465045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._105105725aab512a55c3eca9cd73fc9b::_105105725aab512a55c3eca9cd73fc9bm2(System.Int32)
extern "C"  int32_t _105105725aab512a55c3eca9cd73fc9b__105105725aab512a55c3eca9cd73fc9bm2_m1934881145 (_105105725aab512a55c3eca9cd73fc9b_t654465045 * __this, int32_t ____105105725aab512a55c3eca9cd73fc9ba0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._105105725aab512a55c3eca9cd73fc9b::_105105725aab512a55c3eca9cd73fc9bm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _105105725aab512a55c3eca9cd73fc9b__105105725aab512a55c3eca9cd73fc9bm_m2600620381 (_105105725aab512a55c3eca9cd73fc9b_t654465045 * __this, int32_t ____105105725aab512a55c3eca9cd73fc9ba0, int32_t ____105105725aab512a55c3eca9cd73fc9b591, int32_t ____105105725aab512a55c3eca9cd73fc9bc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._105105725aab512a55c3eca9cd73fc9b::ilo__105105725aab512a55c3eca9cd73fc9bm21(Little._105105725aab512a55c3eca9cd73fc9b,System.Int32)
extern "C"  int32_t _105105725aab512a55c3eca9cd73fc9b_ilo__105105725aab512a55c3eca9cd73fc9bm21_m3783835516 (Il2CppObject * __this /* static, unused */, _105105725aab512a55c3eca9cd73fc9b_t654465045 * ____this0, int32_t ____105105725aab512a55c3eca9cd73fc9ba1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
