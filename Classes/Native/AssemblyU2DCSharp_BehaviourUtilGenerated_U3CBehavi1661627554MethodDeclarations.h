﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtilGenerated/<BehaviourUtil_DelayCallT3__Single__ActionT3_T1_T2_T3__T1__T2__T3>c__AnonStorey46
struct U3CBehaviourUtil_DelayCallT3__Single__ActionT3_T1_T2_T3__T1__T2__T3U3Ec__AnonStorey46_t1661627554;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtilGenerated/<BehaviourUtil_DelayCallT3__Single__ActionT3_T1_T2_T3__T1__T2__T3>c__AnonStorey46::.ctor()
extern "C"  void U3CBehaviourUtil_DelayCallT3__Single__ActionT3_T1_T2_T3__T1__T2__T3U3Ec__AnonStorey46__ctor_m2567850041 (U3CBehaviourUtil_DelayCallT3__Single__ActionT3_T1_T2_T3__T1__T2__T3U3Ec__AnonStorey46_t1661627554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BehaviourUtilGenerated/<BehaviourUtil_DelayCallT3__Single__ActionT3_T1_T2_T3__T1__T2__T3>c__AnonStorey46::<>m__5()
extern "C"  Il2CppObject * U3CBehaviourUtil_DelayCallT3__Single__ActionT3_T1_T2_T3__T1__T2__T3U3Ec__AnonStorey46_U3CU3Em__5_m3242363274 (U3CBehaviourUtil_DelayCallT3__Single__ActionT3_T1_T2_T3__T1__T2__T3U3Ec__AnonStorey46_t1661627554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
