﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._6a66de2b393decca12c0cc62ed48595c
struct _6a66de2b393decca12c0cc62ed48595c_t3858868583;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._6a66de2b393decca12c0cc62ed48595c::.ctor()
extern "C"  void _6a66de2b393decca12c0cc62ed48595c__ctor_m3424395430 (_6a66de2b393decca12c0cc62ed48595c_t3858868583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6a66de2b393decca12c0cc62ed48595c::_6a66de2b393decca12c0cc62ed48595cm2(System.Int32)
extern "C"  int32_t _6a66de2b393decca12c0cc62ed48595c__6a66de2b393decca12c0cc62ed48595cm2_m1758781497 (_6a66de2b393decca12c0cc62ed48595c_t3858868583 * __this, int32_t ____6a66de2b393decca12c0cc62ed48595ca0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6a66de2b393decca12c0cc62ed48595c::_6a66de2b393decca12c0cc62ed48595cm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _6a66de2b393decca12c0cc62ed48595c__6a66de2b393decca12c0cc62ed48595cm_m1137146525 (_6a66de2b393decca12c0cc62ed48595c_t3858868583 * __this, int32_t ____6a66de2b393decca12c0cc62ed48595ca0, int32_t ____6a66de2b393decca12c0cc62ed48595c351, int32_t ____6a66de2b393decca12c0cc62ed48595cc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
