﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.Assets.SubAssetMgr/ErrorInfo
struct ErrorInfo_t2633981210;
struct ErrorInfo_t2633981210_marshaled_pinvoke;
struct ErrorInfo_t2633981210_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct ErrorInfo_t2633981210;
struct ErrorInfo_t2633981210_marshaled_pinvoke;

extern "C" void ErrorInfo_t2633981210_marshal_pinvoke(const ErrorInfo_t2633981210& unmarshaled, ErrorInfo_t2633981210_marshaled_pinvoke& marshaled);
extern "C" void ErrorInfo_t2633981210_marshal_pinvoke_back(const ErrorInfo_t2633981210_marshaled_pinvoke& marshaled, ErrorInfo_t2633981210& unmarshaled);
extern "C" void ErrorInfo_t2633981210_marshal_pinvoke_cleanup(ErrorInfo_t2633981210_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ErrorInfo_t2633981210;
struct ErrorInfo_t2633981210_marshaled_com;

extern "C" void ErrorInfo_t2633981210_marshal_com(const ErrorInfo_t2633981210& unmarshaled, ErrorInfo_t2633981210_marshaled_com& marshaled);
extern "C" void ErrorInfo_t2633981210_marshal_com_back(const ErrorInfo_t2633981210_marshaled_com& marshaled, ErrorInfo_t2633981210& unmarshaled);
extern "C" void ErrorInfo_t2633981210_marshal_com_cleanup(ErrorInfo_t2633981210_marshaled_com& marshaled);
