﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUISettings
struct GUISettings_t4075193652;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Void UnityEngine.GUISettings::.ctor()
extern "C"  void GUISettings__ctor_m1563013787 (GUISettings_t4075193652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUISettings::get_doubleClickSelectsWord()
extern "C"  bool GUISettings_get_doubleClickSelectsWord_m1623472694 (GUISettings_t4075193652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUISettings::set_doubleClickSelectsWord(System.Boolean)
extern "C"  void GUISettings_set_doubleClickSelectsWord_m2674184007 (GUISettings_t4075193652 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUISettings::get_tripleClickSelectsLine()
extern "C"  bool GUISettings_get_tripleClickSelectsLine_m1315895277 (GUISettings_t4075193652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUISettings::set_tripleClickSelectsLine(System.Boolean)
extern "C"  void GUISettings_set_tripleClickSelectsLine_m2497298622 (GUISettings_t4075193652 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.GUISettings::get_cursorColor()
extern "C"  Color_t4194546905  GUISettings_get_cursorColor_m3617656164 (GUISettings_t4075193652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUISettings::set_cursorColor(UnityEngine.Color)
extern "C"  void GUISettings_set_cursorColor_m1579776541 (GUISettings_t4075193652 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUISettings::get_cursorFlashSpeed()
extern "C"  float GUISettings_get_cursorFlashSpeed_m3510903059 (GUISettings_t4075193652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUISettings::set_cursorFlashSpeed(System.Single)
extern "C"  void GUISettings_set_cursorFlashSpeed_m4246307704 (GUISettings_t4075193652 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.GUISettings::get_selectionColor()
extern "C"  Color_t4194546905  GUISettings_get_selectionColor_m3945610946 (GUISettings_t4075193652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUISettings::set_selectionColor(UnityEngine.Color)
extern "C"  void GUISettings_set_selectionColor_m1635052777 (GUISettings_t4075193652 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUISettings::Internal_GetCursorFlashSpeed()
extern "C"  float GUISettings_Internal_GetCursorFlashSpeed_m2094683006 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
