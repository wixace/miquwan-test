﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_4_gen2668625267MethodDeclarations.h"

// System.Void System.Action`4<UnityEngine.Transform,System.Int32,System.String,System.Single>::.ctor(System.Object,System.IntPtr)
#define Action_4__ctor_m322493149(__this, ___object0, ___method1, method) ((  void (*) (Action_4_t2423482054 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_4__ctor_m2555180776_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`4<UnityEngine.Transform,System.Int32,System.String,System.Single>::Invoke(T1,T2,T3,T4)
#define Action_4_Invoke_m1972008573(__this, ___arg10, ___arg21, ___arg32, ___arg43, method) ((  void (*) (Action_4_t2423482054 *, Transform_t1659122786 *, int32_t, String_t*, float, const MethodInfo*))Action_4_Invoke_m1661067458_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, method)
// System.IAsyncResult System.Action`4<UnityEngine.Transform,System.Int32,System.String,System.Single>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
#define Action_4_BeginInvoke_m376930438(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method) ((  Il2CppObject * (*) (Action_4_t2423482054 *, Transform_t1659122786 *, int32_t, String_t*, float, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_4_BeginInvoke_m3540796875_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method)
// System.Void System.Action`4<UnityEngine.Transform,System.Int32,System.String,System.Single>::EndInvoke(System.IAsyncResult)
#define Action_4_EndInvoke_m2059154525(__this, ___result0, method) ((  void (*) (Action_4_t2423482054 *, Il2CppObject *, const MethodInfo*))Action_4_EndInvoke_m3997179128_gshared)(__this, ___result0, method)
