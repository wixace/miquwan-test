﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.CDoubleStream
struct CDoubleStream_t2241547222;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_SeekOrigin4120335598.h"

// System.Void SevenZip.CDoubleStream::.ctor()
extern "C"  void CDoubleStream__ctor_m909993921 (CDoubleStream_t2241547222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.CDoubleStream::get_CanRead()
extern "C"  bool CDoubleStream_get_CanRead_m3570090632 (CDoubleStream_t2241547222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.CDoubleStream::get_CanWrite()
extern "C"  bool CDoubleStream_get_CanWrite_m3821280719 (CDoubleStream_t2241547222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.CDoubleStream::get_CanSeek()
extern "C"  bool CDoubleStream_get_CanSeek_m3598845674 (CDoubleStream_t2241547222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 SevenZip.CDoubleStream::get_Length()
extern "C"  int64_t CDoubleStream_get_Length_m3505518113 (CDoubleStream_t2241547222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 SevenZip.CDoubleStream::get_Position()
extern "C"  int64_t CDoubleStream_get_Position_m1770047780 (CDoubleStream_t2241547222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.CDoubleStream::set_Position(System.Int64)
extern "C"  void CDoubleStream_set_Position_m3633466779 (CDoubleStream_t2241547222 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.CDoubleStream::Flush()
extern "C"  void CDoubleStream_Flush_m993941219 (CDoubleStream_t2241547222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SevenZip.CDoubleStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t CDoubleStream_Read_m43304706 (CDoubleStream_t2241547222 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.CDoubleStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void CDoubleStream_Write_m19697963 (CDoubleStream_t2241547222 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 SevenZip.CDoubleStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t CDoubleStream_Seek_m4217211171 (CDoubleStream_t2241547222 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.CDoubleStream::SetLength(System.Int64)
extern "C"  void CDoubleStream_SetLength_m2189669401 (CDoubleStream_t2241547222 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
