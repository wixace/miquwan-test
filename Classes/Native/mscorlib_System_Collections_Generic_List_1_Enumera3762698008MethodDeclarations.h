﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>
struct List_1_t3743025238;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3762698008.h"
#include "UnityEngine_UnityEngine_MeshSubsetCombineUtility_S2374839686.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m928859289_gshared (Enumerator_t3762698008 * __this, List_1_t3743025238 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m928859289(__this, ___l0, method) ((  void (*) (Enumerator_t3762698008 *, List_1_t3743025238 *, const MethodInfo*))Enumerator__ctor_m928859289_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2589824025_gshared (Enumerator_t3762698008 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2589824025(__this, method) ((  void (*) (Enumerator_t3762698008 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2589824025_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1036374479_gshared (Enumerator_t3762698008 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1036374479(__this, method) ((  Il2CppObject * (*) (Enumerator_t3762698008 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1036374479_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Dispose()
extern "C"  void Enumerator_Dispose_m2737686718_gshared (Enumerator_t3762698008 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2737686718(__this, method) ((  void (*) (Enumerator_t3762698008 *, const MethodInfo*))Enumerator_Dispose_m2737686718_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3132291319_gshared (Enumerator_t3762698008 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3132291319(__this, method) ((  void (*) (Enumerator_t3762698008 *, const MethodInfo*))Enumerator_VerifyState_m3132291319_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1478214729_gshared (Enumerator_t3762698008 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1478214729(__this, method) ((  bool (*) (Enumerator_t3762698008 *, const MethodInfo*))Enumerator_MoveNext_m1478214729_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::get_Current()
extern "C"  SubMeshInstance_t2374839686  Enumerator_get_Current_m2806722576_gshared (Enumerator_t3762698008 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2806722576(__this, method) ((  SubMeshInstance_t2374839686  (*) (Enumerator_t3762698008 *, const MethodInfo*))Enumerator_get_Current_m2806722576_gshared)(__this, method)
