﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1603662595.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_Inter2821319919.h"

// System.Void System.Array/InternalEnumerator`1<Pathfinding.LocalAvoidance/IntersectionPair>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2877098420_gshared (InternalEnumerator_1_t1603662595 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2877098420(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1603662595 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2877098420_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3691333484_gshared (InternalEnumerator_1_t1603662595 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3691333484(__this, method) ((  void (*) (InternalEnumerator_1_t1603662595 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3691333484_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Pathfinding.LocalAvoidance/IntersectionPair>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4289038178_gshared (InternalEnumerator_1_t1603662595 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4289038178(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1603662595 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4289038178_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.LocalAvoidance/IntersectionPair>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4111819915_gshared (InternalEnumerator_1_t1603662595 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m4111819915(__this, method) ((  void (*) (InternalEnumerator_1_t1603662595 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4111819915_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.LocalAvoidance/IntersectionPair>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m11728668_gshared (InternalEnumerator_1_t1603662595 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m11728668(__this, method) ((  bool (*) (InternalEnumerator_1_t1603662595 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11728668_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Pathfinding.LocalAvoidance/IntersectionPair>::get_Current()
extern "C"  IntersectionPair_t2821319919  InternalEnumerator_1_get_Current_m916862941_gshared (InternalEnumerator_1_t1603662595 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m916862941(__this, method) ((  IntersectionPair_t2821319919  (*) (InternalEnumerator_1_t1603662595 *, const MethodInfo*))InternalEnumerator_1_get_Current_m916862941_gshared)(__this, method)
