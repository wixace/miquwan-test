﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman
struct DeflaterHuffman_t3769756376;
// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterPending
struct DeflaterPending_t1829109954;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1829109954.h"

// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::.cctor()
extern "C"  void DeflaterHuffman__cctor_m2829483505 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::.ctor(ICSharpCode.SharpZipLib.Zip.Compression.DeflaterPending)
extern "C"  void DeflaterHuffman__ctor_m211969164 (DeflaterHuffman_t3769756376 * __this, DeflaterPending_t1829109954 * ___pending0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::Reset()
extern "C"  void DeflaterHuffman_Reset_m1909678057 (DeflaterHuffman_t3769756376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::SendAllTrees(System.Int32)
extern "C"  void DeflaterHuffman_SendAllTrees_m1062228213 (DeflaterHuffman_t3769756376 * __this, int32_t ___blTreeCodes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::CompressBlock()
extern "C"  void DeflaterHuffman_CompressBlock_m2252554469 (DeflaterHuffman_t3769756376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::FlushStoredBlock(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern "C"  void DeflaterHuffman_FlushStoredBlock_m2383394690 (DeflaterHuffman_t3769756376 * __this, ByteU5BU5D_t4260760469* ___stored0, int32_t ___storedOffset1, int32_t ___storedLength2, bool ___lastBlock3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::FlushBlock(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern "C"  void DeflaterHuffman_FlushBlock_m3510668965 (DeflaterHuffman_t3769756376 * __this, ByteU5BU5D_t4260760469* ___stored0, int32_t ___storedOffset1, int32_t ___storedLength2, bool ___lastBlock3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::IsFull()
extern "C"  bool DeflaterHuffman_IsFull_m2958018863 (DeflaterHuffman_t3769756376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::TallyLit(System.Int32)
extern "C"  bool DeflaterHuffman_TallyLit_m1601839602 (DeflaterHuffman_t3769756376 * __this, int32_t ___literal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::TallyDist(System.Int32,System.Int32)
extern "C"  bool DeflaterHuffman_TallyDist_m4230142696 (DeflaterHuffman_t3769756376 * __this, int32_t ___distance0, int32_t ___length1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::BitReverse(System.Int32)
extern "C"  int16_t DeflaterHuffman_BitReverse_m2340978984 (Il2CppObject * __this /* static, unused */, int32_t ___toReverse0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::Lcode(System.Int32)
extern "C"  int32_t DeflaterHuffman_Lcode_m114808528 (Il2CppObject * __this /* static, unused */, int32_t ___length0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::Dcode(System.Int32)
extern "C"  int32_t DeflaterHuffman_Dcode_m2575616200 (Il2CppObject * __this /* static, unused */, int32_t ___distance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
