﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.ClipperLib.Clipper
struct Clipper_t636949239;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>
struct List_1_t1767529987;
// Pathfinding.ClipperLib.PolyTree
struct PolyTree_t3336435468;
// Pathfinding.ClipperLib.OutRec
struct OutRec_t3245805284;
// Pathfinding.ClipperLib.OutPt
struct OutPt_t3947633180;
// Pathfinding.ClipperLib.TEdge
struct TEdge_t3950806139;
// Pathfinding.ClipperLib.IntersectNode
struct IntersectNode_t4106323947;
// System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>
struct List_1_t399344435;
// Pathfinding.ClipperLib.Join
struct Join_t3970117804;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Clip3693001772.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Poly3967424203.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Poly3336435468.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_OutR3245805284.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_OutP3947633180.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_TEdg3950806139.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Direc297621929.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Inte4106323947.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Join3970117804.h"

// System.Void Pathfinding.ClipperLib.Clipper::.ctor(System.Int32)
extern "C"  void Clipper__ctor_m3661235519 (Clipper_t636949239 * __this, int32_t ___InitOptions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::Clear()
extern "C"  void Clipper_Clear_m1082589465 (Clipper_t636949239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::Reset()
extern "C"  void Clipper_Reset_m1322889115 (Clipper_t636949239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::get_ReverseSolution()
extern "C"  bool Clipper_get_ReverseSolution_m2875557634 (Clipper_t636949239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::set_ReverseSolution(System.Boolean)
extern "C"  void Clipper_set_ReverseSolution_m3191234529 (Clipper_t636949239 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::get_StrictlySimple()
extern "C"  bool Clipper_get_StrictlySimple_m1330170723 (Clipper_t636949239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::set_StrictlySimple(System.Boolean)
extern "C"  void Clipper_set_StrictlySimple_m4072491954 (Clipper_t636949239 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::InsertScanbeam(System.Int64)
extern "C"  void Clipper_InsertScanbeam_m2447896813 (Clipper_t636949239 * __this, int64_t ___Y0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::Execute(Pathfinding.ClipperLib.ClipType,System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>,Pathfinding.ClipperLib.PolyFillType,Pathfinding.ClipperLib.PolyFillType)
extern "C"  bool Clipper_Execute_m264922624 (Clipper_t636949239 * __this, int32_t ___clipType0, List_1_t1767529987 * ___solution1, int32_t ___subjFillType2, int32_t ___clipFillType3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::Execute(Pathfinding.ClipperLib.ClipType,Pathfinding.ClipperLib.PolyTree,Pathfinding.ClipperLib.PolyFillType,Pathfinding.ClipperLib.PolyFillType)
extern "C"  bool Clipper_Execute_m4064679803 (Clipper_t636949239 * __this, int32_t ___clipType0, PolyTree_t3336435468 * ___polytree1, int32_t ___subjFillType2, int32_t ___clipFillType3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::FixHoleLinkage(Pathfinding.ClipperLib.OutRec)
extern "C"  void Clipper_FixHoleLinkage_m891396734 (Clipper_t636949239 * __this, OutRec_t3245805284 * ___outRec0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::ExecuteInternal()
extern "C"  bool Clipper_ExecuteInternal_m87382082 (Clipper_t636949239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Pathfinding.ClipperLib.Clipper::PopScanbeam()
extern "C"  int64_t Clipper_PopScanbeam_m2240798930 (Clipper_t636949239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::DisposeAllPolyPts()
extern "C"  void Clipper_DisposeAllPolyPts_m4187081581 (Clipper_t636949239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::DisposeOutRec(System.Int32)
extern "C"  void Clipper_DisposeOutRec_m4039825918 (Clipper_t636949239 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::DisposeOutPts(Pathfinding.ClipperLib.OutPt)
extern "C"  void Clipper_DisposeOutPts_m3153880070 (Clipper_t636949239 * __this, OutPt_t3947633180 * ___pp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::AddJoin(Pathfinding.ClipperLib.OutPt,Pathfinding.ClipperLib.OutPt,Pathfinding.ClipperLib.IntPoint)
extern "C"  void Clipper_AddJoin_m2260685358 (Clipper_t636949239 * __this, OutPt_t3947633180 * ___Op10, OutPt_t3947633180 * ___Op21, IntPoint_t3326126179  ___OffPt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::AddGhostJoin(Pathfinding.ClipperLib.OutPt,Pathfinding.ClipperLib.IntPoint)
extern "C"  void Clipper_AddGhostJoin_m1097100551 (Clipper_t636949239 * __this, OutPt_t3947633180 * ___Op0, IntPoint_t3326126179  ___OffPt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::InsertLocalMinimaIntoAEL(System.Int64)
extern "C"  void Clipper_InsertLocalMinimaIntoAEL_m2894934835 (Clipper_t636949239 * __this, int64_t ___botY0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::InsertEdgeIntoAEL(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge)
extern "C"  void Clipper_InsertEdgeIntoAEL_m1577452744 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___edge0, TEdge_t3950806139 * ___startEdge1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::E2InsertsBeforeE1(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge)
extern "C"  bool Clipper_E2InsertsBeforeE1_m3305521566 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e10, TEdge_t3950806139 * ___e21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::IsEvenOddFillType(Pathfinding.ClipperLib.TEdge)
extern "C"  bool Clipper_IsEvenOddFillType_m201048083 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___edge0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::IsEvenOddAltFillType(Pathfinding.ClipperLib.TEdge)
extern "C"  bool Clipper_IsEvenOddAltFillType_m1541475944 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___edge0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::IsContributing(Pathfinding.ClipperLib.TEdge)
extern "C"  bool Clipper_IsContributing_m3580620193 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___edge0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::SetWindingCount(Pathfinding.ClipperLib.TEdge)
extern "C"  void Clipper_SetWindingCount_m167856286 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___edge0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::AddEdgeToSEL(Pathfinding.ClipperLib.TEdge)
extern "C"  void Clipper_AddEdgeToSEL_m213197330 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___edge0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::CopyAELToSEL()
extern "C"  void Clipper_CopyAELToSEL_m3766598946 (Clipper_t636949239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::SwapPositionsInAEL(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge)
extern "C"  void Clipper_SwapPositionsInAEL_m423077188 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___edge10, TEdge_t3950806139 * ___edge21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::SwapPositionsInSEL(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge)
extern "C"  void Clipper_SwapPositionsInSEL_m2241560178 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___edge10, TEdge_t3950806139 * ___edge21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::AddLocalMaxPoly(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.IntPoint)
extern "C"  void Clipper_AddLocalMaxPoly_m913303731 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e10, TEdge_t3950806139 * ___e21, IntPoint_t3326126179  ___pt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ClipperLib.OutPt Pathfinding.ClipperLib.Clipper::AddLocalMinPoly(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.IntPoint)
extern "C"  OutPt_t3947633180 * Clipper_AddLocalMinPoly_m2497180132 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e10, TEdge_t3950806139 * ___e21, IntPoint_t3326126179  ___pt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ClipperLib.OutRec Pathfinding.ClipperLib.Clipper::CreateOutRec()
extern "C"  OutRec_t3245805284 * Clipper_CreateOutRec_m1755291545 (Clipper_t636949239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ClipperLib.OutPt Pathfinding.ClipperLib.Clipper::AddOutPt(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.IntPoint)
extern "C"  OutPt_t3947633180 * Clipper_AddOutPt_m3632434112 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e0, IntPoint_t3326126179  ___pt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::HorzSegmentsOverlap(Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint)
extern "C"  bool Clipper_HorzSegmentsOverlap_m2752565594 (Clipper_t636949239 * __this, IntPoint_t3326126179  ___Pt1a0, IntPoint_t3326126179  ___Pt1b1, IntPoint_t3326126179  ___Pt2a2, IntPoint_t3326126179  ___Pt2b3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::SetHoleState(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.OutRec)
extern "C"  void Clipper_SetHoleState_m1938498398 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e0, OutRec_t3245805284 * ___outRec1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Pathfinding.ClipperLib.Clipper::GetDx(Pathfinding.ClipperLib.IntPoint,Pathfinding.ClipperLib.IntPoint)
extern "C"  double Clipper_GetDx_m3219334801 (Clipper_t636949239 * __this, IntPoint_t3326126179  ___pt10, IntPoint_t3326126179  ___pt21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::FirstIsBottomPt(Pathfinding.ClipperLib.OutPt,Pathfinding.ClipperLib.OutPt)
extern "C"  bool Clipper_FirstIsBottomPt_m2070132493 (Clipper_t636949239 * __this, OutPt_t3947633180 * ___btmPt10, OutPt_t3947633180 * ___btmPt21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ClipperLib.OutPt Pathfinding.ClipperLib.Clipper::GetBottomPt(Pathfinding.ClipperLib.OutPt)
extern "C"  OutPt_t3947633180 * Clipper_GetBottomPt_m1952022412 (Clipper_t636949239 * __this, OutPt_t3947633180 * ___pp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ClipperLib.OutRec Pathfinding.ClipperLib.Clipper::GetLowermostRec(Pathfinding.ClipperLib.OutRec,Pathfinding.ClipperLib.OutRec)
extern "C"  OutRec_t3245805284 * Clipper_GetLowermostRec_m3541598937 (Clipper_t636949239 * __this, OutRec_t3245805284 * ___outRec10, OutRec_t3245805284 * ___outRec21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::Param1RightOfParam2(Pathfinding.ClipperLib.OutRec,Pathfinding.ClipperLib.OutRec)
extern "C"  bool Clipper_Param1RightOfParam2_m667347646 (Clipper_t636949239 * __this, OutRec_t3245805284 * ___outRec10, OutRec_t3245805284 * ___outRec21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ClipperLib.OutRec Pathfinding.ClipperLib.Clipper::GetOutRec(System.Int32)
extern "C"  OutRec_t3245805284 * Clipper_GetOutRec_m918585808 (Clipper_t636949239 * __this, int32_t ___idx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::AppendPolygon(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge)
extern "C"  void Clipper_AppendPolygon_m921090842 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e10, TEdge_t3950806139 * ___e21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::ReversePolyPtLinks(Pathfinding.ClipperLib.OutPt)
extern "C"  void Clipper_ReversePolyPtLinks_m59152439 (Clipper_t636949239 * __this, OutPt_t3947633180 * ___pp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::SwapSides(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge)
extern "C"  void Clipper_SwapSides_m1446163601 (Il2CppObject * __this /* static, unused */, TEdge_t3950806139 * ___edge10, TEdge_t3950806139 * ___edge21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::SwapPolyIndexes(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge)
extern "C"  void Clipper_SwapPolyIndexes_m109539833 (Il2CppObject * __this /* static, unused */, TEdge_t3950806139 * ___edge10, TEdge_t3950806139 * ___edge21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::IntersectEdges(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.IntPoint,System.Boolean)
extern "C"  void Clipper_IntersectEdges_m1787610885 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e10, TEdge_t3950806139 * ___e21, IntPoint_t3326126179  ___pt2, bool ___protect3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::DeleteFromAEL(Pathfinding.ClipperLib.TEdge)
extern "C"  void Clipper_DeleteFromAEL_m2256743194 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::DeleteFromSEL(Pathfinding.ClipperLib.TEdge)
extern "C"  void Clipper_DeleteFromSEL_m1605264172 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::UpdateEdgeIntoAEL(Pathfinding.ClipperLib.TEdge&)
extern "C"  void Clipper_UpdateEdgeIntoAEL_m1906260707 (Clipper_t636949239 * __this, TEdge_t3950806139 ** ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::ProcessHorizontals(System.Boolean)
extern "C"  void Clipper_ProcessHorizontals_m563259309 (Clipper_t636949239 * __this, bool ___isTopOfScanbeam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::GetHorzDirection(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.Direction&,System.Int64&,System.Int64&)
extern "C"  void Clipper_GetHorzDirection_m834255180 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___HorzEdge0, int32_t* ___Dir1, int64_t* ___Left2, int64_t* ___Right3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::PrepareHorzJoins(Pathfinding.ClipperLib.TEdge,System.Boolean)
extern "C"  void Clipper_PrepareHorzJoins_m1033324665 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___horzEdge0, bool ___isTopOfScanbeam1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::ProcessHorizontal(Pathfinding.ClipperLib.TEdge,System.Boolean)
extern "C"  void Clipper_ProcessHorizontal_m2266688323 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___horzEdge0, bool ___isTopOfScanbeam1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ClipperLib.TEdge Pathfinding.ClipperLib.Clipper::GetNextInAEL(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.Direction)
extern "C"  TEdge_t3950806139 * Clipper_GetNextInAEL_m1569553480 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e0, int32_t ___Direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::IsMaxima(Pathfinding.ClipperLib.TEdge,System.Double)
extern "C"  bool Clipper_IsMaxima_m2272585228 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e0, double ___Y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::IsIntermediate(Pathfinding.ClipperLib.TEdge,System.Double)
extern "C"  bool Clipper_IsIntermediate_m743671532 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e0, double ___Y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ClipperLib.TEdge Pathfinding.ClipperLib.Clipper::GetMaximaPair(Pathfinding.ClipperLib.TEdge)
extern "C"  TEdge_t3950806139 * Clipper_GetMaximaPair_m3274930736 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::ProcessIntersections(System.Int64,System.Int64)
extern "C"  bool Clipper_ProcessIntersections_m661125977 (Clipper_t636949239 * __this, int64_t ___botY0, int64_t ___topY1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::BuildIntersectList(System.Int64,System.Int64)
extern "C"  void Clipper_BuildIntersectList_m563933121 (Clipper_t636949239 * __this, int64_t ___botY0, int64_t ___topY1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::EdgesAdjacent(Pathfinding.ClipperLib.IntersectNode)
extern "C"  bool Clipper_EdgesAdjacent_m1627663155 (Clipper_t636949239 * __this, IntersectNode_t4106323947 * ___inode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::FixupIntersectionOrder()
extern "C"  bool Clipper_FixupIntersectionOrder_m1801248039 (Clipper_t636949239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::ProcessIntersectList()
extern "C"  void Clipper_ProcessIntersectList_m4219475268 (Clipper_t636949239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Pathfinding.ClipperLib.Clipper::Round(System.Double)
extern "C"  int64_t Clipper_Round_m1914183711 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Pathfinding.ClipperLib.Clipper::TopX(Pathfinding.ClipperLib.TEdge,System.Int64)
extern "C"  int64_t Clipper_TopX_m1760470797 (Il2CppObject * __this /* static, unused */, TEdge_t3950806139 * ___edge0, int64_t ___currentY1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::InsertIntersectNode(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.IntPoint)
extern "C"  void Clipper_InsertIntersectNode_m3406234641 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e10, TEdge_t3950806139 * ___e21, IntPoint_t3326126179  ___pt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::SwapIntersectNodes(Pathfinding.ClipperLib.IntersectNode,Pathfinding.ClipperLib.IntersectNode)
extern "C"  void Clipper_SwapIntersectNodes_m33861867 (Clipper_t636949239 * __this, IntersectNode_t4106323947 * ___int10, IntersectNode_t4106323947 * ___int21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::IntersectPoint(Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.TEdge,Pathfinding.ClipperLib.IntPoint&)
extern "C"  bool Clipper_IntersectPoint_m1806336746 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___edge10, TEdge_t3950806139 * ___edge21, IntPoint_t3326126179 * ___ip2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::DisposeIntersectNodes()
extern "C"  void Clipper_DisposeIntersectNodes_m1202039965 (Clipper_t636949239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::ProcessEdgesAtTopOfScanbeam(System.Int64)
extern "C"  void Clipper_ProcessEdgesAtTopOfScanbeam_m897821276 (Clipper_t636949239 * __this, int64_t ___topY0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::DoMaxima(Pathfinding.ClipperLib.TEdge)
extern "C"  void Clipper_DoMaxima_m606392597 (Clipper_t636949239 * __this, TEdge_t3950806139 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::Orientation(System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>)
extern "C"  bool Clipper_Orientation_m1168645137 (Il2CppObject * __this /* static, unused */, List_1_t399344435 * ___poly0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.ClipperLib.Clipper::PointCount(Pathfinding.ClipperLib.OutPt)
extern "C"  int32_t Clipper_PointCount_m3418340581 (Clipper_t636949239 * __this, OutPt_t3947633180 * ___pts0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::BuildResult(System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>)
extern "C"  void Clipper_BuildResult_m1163037098 (Clipper_t636949239 * __this, List_1_t1767529987 * ___polyg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::BuildResult2(Pathfinding.ClipperLib.PolyTree)
extern "C"  void Clipper_BuildResult2_m2430771551 (Clipper_t636949239 * __this, PolyTree_t3336435468 * ___polytree0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::FixupOutPolygon(Pathfinding.ClipperLib.OutRec)
extern "C"  void Clipper_FixupOutPolygon_m3468612540 (Clipper_t636949239 * __this, OutRec_t3245805284 * ___outRec0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ClipperLib.OutPt Pathfinding.ClipperLib.Clipper::DupOutPt(Pathfinding.ClipperLib.OutPt,System.Boolean)
extern "C"  OutPt_t3947633180 * Clipper_DupOutPt_m4193918363 (Clipper_t636949239 * __this, OutPt_t3947633180 * ___outPt0, bool ___InsertAfter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::GetOverlap(System.Int64,System.Int64,System.Int64,System.Int64,System.Int64&,System.Int64&)
extern "C"  bool Clipper_GetOverlap_m1667878347 (Clipper_t636949239 * __this, int64_t ___a10, int64_t ___a21, int64_t ___b12, int64_t ___b23, int64_t* ___Left4, int64_t* ___Right5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::JoinHorz(Pathfinding.ClipperLib.OutPt,Pathfinding.ClipperLib.OutPt,Pathfinding.ClipperLib.OutPt,Pathfinding.ClipperLib.OutPt,Pathfinding.ClipperLib.IntPoint,System.Boolean)
extern "C"  bool Clipper_JoinHorz_m1354690147 (Clipper_t636949239 * __this, OutPt_t3947633180 * ___op10, OutPt_t3947633180 * ___op1b1, OutPt_t3947633180 * ___op22, OutPt_t3947633180 * ___op2b3, IntPoint_t3326126179  ___Pt4, bool ___DiscardLeft5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::JoinPoints(Pathfinding.ClipperLib.Join,Pathfinding.ClipperLib.OutPt&,Pathfinding.ClipperLib.OutPt&)
extern "C"  bool Clipper_JoinPoints_m49902773 (Clipper_t636949239 * __this, Join_t3970117804 * ___j0, OutPt_t3947633180 ** ___p11, OutPt_t3947633180 ** ___p22, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Clipper::Poly2ContainsPoly1(Pathfinding.ClipperLib.OutPt,Pathfinding.ClipperLib.OutPt,System.Boolean)
extern "C"  bool Clipper_Poly2ContainsPoly1_m2873516521 (Clipper_t636949239 * __this, OutPt_t3947633180 * ___outPt10, OutPt_t3947633180 * ___outPt21, bool ___UseFullRange2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::FixupFirstLefts1(Pathfinding.ClipperLib.OutRec,Pathfinding.ClipperLib.OutRec)
extern "C"  void Clipper_FixupFirstLefts1_m1187386151 (Clipper_t636949239 * __this, OutRec_t3245805284 * ___OldOutRec0, OutRec_t3245805284 * ___NewOutRec1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::FixupFirstLefts2(Pathfinding.ClipperLib.OutRec,Pathfinding.ClipperLib.OutRec)
extern "C"  void Clipper_FixupFirstLefts2_m3674505926 (Clipper_t636949239 * __this, OutRec_t3245805284 * ___OldOutRec0, OutRec_t3245805284 * ___NewOutRec1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::JoinCommonEdges()
extern "C"  void Clipper_JoinCommonEdges_m1930505133 (Clipper_t636949239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::UpdateOutPtIdxs(Pathfinding.ClipperLib.OutRec)
extern "C"  void Clipper_UpdateOutPtIdxs_m3611366233 (Clipper_t636949239 * __this, OutRec_t3245805284 * ___outrec0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Clipper::DoSimplePolygons()
extern "C"  void Clipper_DoSimplePolygons_m2818696524 (Clipper_t636949239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Pathfinding.ClipperLib.Clipper::Area(System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>)
extern "C"  double Clipper_Area_m791992023 (Il2CppObject * __this /* static, unused */, List_1_t399344435 * ___poly0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Pathfinding.ClipperLib.Clipper::Area(Pathfinding.ClipperLib.OutRec)
extern "C"  double Clipper_Area_m2380044382 (Clipper_t636949239 * __this, OutRec_t3245805284 * ___outRec0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
