﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FingerEventDetector_1_FingerEven2484147800MethodDeclarations.h"

// System.Void FingerEventDetector`1/FingerEventHandler<FingerUpEvent>::.ctor(System.Object,System.IntPtr)
#define FingerEventHandler__ctor_m1805809595(__this, ___object0, ___method1, method) ((  void (*) (FingerEventHandler_t1406346203 *, Il2CppObject *, IntPtr_t, const MethodInfo*))FingerEventHandler__ctor_m425058771_gshared)(__this, ___object0, ___method1, method)
// System.Void FingerEventDetector`1/FingerEventHandler<FingerUpEvent>::Invoke(T)
#define FingerEventHandler_Invoke_m2432903209(__this, ___eventData0, method) ((  void (*) (FingerEventHandler_t1406346203 *, FingerUpEvent_t3093014774 *, const MethodInfo*))FingerEventHandler_Invoke_m4182135569_gshared)(__this, ___eventData0, method)
// System.IAsyncResult FingerEventDetector`1/FingerEventHandler<FingerUpEvent>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define FingerEventHandler_BeginInvoke_m261457022(__this, ___eventData0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (FingerEventHandler_t1406346203 *, FingerUpEvent_t3093014774 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))FingerEventHandler_BeginInvoke_m1896720230_gshared)(__this, ___eventData0, ___callback1, ___object2, method)
// System.Void FingerEventDetector`1/FingerEventHandler<FingerUpEvent>::EndInvoke(System.IAsyncResult)
#define FingerEventHandler_EndInvoke_m3997499211(__this, ___result0, method) ((  void (*) (FingerEventHandler_t1406346203 *, Il2CppObject *, const MethodInfo*))FingerEventHandler_EndInvoke_m934360419_gshared)(__this, ___result0, method)
