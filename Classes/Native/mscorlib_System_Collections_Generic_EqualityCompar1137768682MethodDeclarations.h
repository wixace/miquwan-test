﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/VOLine>
struct EqualityComparer_1_t1137768682;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/VOLine>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3523305976_gshared (EqualityComparer_1_t1137768682 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m3523305976(__this, method) ((  void (*) (EqualityComparer_1_t1137768682 *, const MethodInfo*))EqualityComparer_1__ctor_m3523305976_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/VOLine>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1366206645_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m1366206645(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m1366206645_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m493727953_gshared (EqualityComparer_1_t1137768682 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m493727953(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t1137768682 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m493727953_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/VOLine>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1896216665_gshared (EqualityComparer_1_t1137768682 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1896216665(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t1137768682 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1896216665_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Pathfinding.LocalAvoidance/VOLine>::get_Default()
extern "C"  EqualityComparer_1_t1137768682 * EqualityComparer_1_get_Default_m1896591010_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m1896591010(__this /* static, unused */, method) ((  EqualityComparer_1_t1137768682 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m1896591010_gshared)(__this /* static, unused */, method)
