﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Schema.JsonSchemaBuilder/<MapType>c__AnonStorey12D
struct U3CMapTypeU3Ec__AnonStorey12D_t1908624459;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22834614897.h"

// System.Void Newtonsoft.Json.Schema.JsonSchemaBuilder/<MapType>c__AnonStorey12D::.ctor()
extern "C"  void U3CMapTypeU3Ec__AnonStorey12D__ctor_m66242736 (U3CMapTypeU3Ec__AnonStorey12D_t1908624459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchemaBuilder/<MapType>c__AnonStorey12D::<>m__37A(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Schema.JsonSchemaType>)
extern "C"  bool U3CMapTypeU3Ec__AnonStorey12D_U3CU3Em__37A_m1121880556 (U3CMapTypeU3Ec__AnonStorey12D_t1908624459 * __this, KeyValuePair_2_t2834614897  ___kv0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
