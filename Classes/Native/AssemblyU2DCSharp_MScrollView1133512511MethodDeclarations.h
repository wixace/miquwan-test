﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MScrollView
struct MScrollView_t1133512511;
// MScrollView/OnMoveCallBackFun
struct OnMoveCallBackFun_t3535987578;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MScrollView_MoveWay3474941550.h"
#include "AssemblyU2DCSharp_MScrollView_OnMoveCallBackFun3535987578.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_MScrollView1133512511.h"

// System.Void MScrollView::.ctor()
extern "C"  void MScrollView__ctor_m835070732 (MScrollView_t1133512511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollView::Awake()
extern "C"  void MScrollView_Awake_m1072675951 (MScrollView_t1133512511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollView::Register(MScrollView/MoveWay,MScrollView/OnMoveCallBackFun)
extern "C"  void MScrollView_Register_m3711155771 (MScrollView_t1133512511 * __this, int32_t ___key0, OnMoveCallBackFun_t3535987578 * ___call1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollView::unRegister(MScrollView/MoveWay)
extern "C"  void MScrollView_unRegister_m2690434320 (MScrollView_t1133512511 * __this, int32_t ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollView::OnDrag(UnityEngine.Vector2)
extern "C"  void MScrollView_OnDrag_m2301981679 (MScrollView_t1133512511 * __this, Vector2_t4282066565  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollView::OnPress()
extern "C"  void MScrollView_OnPress_m9505422 (MScrollView_t1133512511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollView::Reste()
extern "C"  void MScrollView_Reste_m2776903419 (MScrollView_t1133512511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollView::SetTargePosition(System.Int32,System.Boolean)
extern "C"  void MScrollView_SetTargePosition_m1142321354 (MScrollView_t1133512511 * __this, int32_t ___index0, bool ___isAim1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollView::Update()
extern "C"  void MScrollView_Update_m1844251009 (MScrollView_t1133512511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollView::OnClick()
extern "C"  void MScrollView_OnClick_m1188294291 (MScrollView_t1133512511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollView::MoveLeft()
extern "C"  void MScrollView_MoveLeft_m1300905648 (MScrollView_t1133512511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollView::MoveRight()
extern "C"  void MScrollView_MoveRight_m2818579893 (MScrollView_t1133512511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollView::ToLeft()
extern "C"  void MScrollView_ToLeft_m2825096090 (MScrollView_t1133512511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollView::ToRight()
extern "C"  void MScrollView_ToRight_m2823843339 (MScrollView_t1133512511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MScrollView::ilo_ToLeft1(MScrollView)
extern "C"  void MScrollView_ilo_ToLeft1_m2934089901 (Il2CppObject * __this /* static, unused */, MScrollView_t1133512511 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MScrollView::ilo_Invoke2(MScrollView/OnMoveCallBackFun)
extern "C"  bool MScrollView_ilo_Invoke2_m3876984595 (Il2CppObject * __this /* static, unused */, OnMoveCallBackFun_t3535987578 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
