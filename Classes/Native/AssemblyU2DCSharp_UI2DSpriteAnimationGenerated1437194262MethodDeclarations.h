﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI2DSpriteAnimationGenerated
struct UI2DSpriteAnimationGenerated_t1437194262;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900;
// UI2DSpriteAnimation
struct UI2DSpriteAnimation_t3878779257;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_UI2DSpriteAnimation3878779257.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UI2DSpriteAnimationGenerated::.ctor()
extern "C"  void UI2DSpriteAnimationGenerated__ctor_m31040325 (UI2DSpriteAnimationGenerated_t1437194262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UI2DSpriteAnimationGenerated::UI2DSpriteAnimation_UI2DSpriteAnimation1(JSVCall,System.Int32)
extern "C"  bool UI2DSpriteAnimationGenerated_UI2DSpriteAnimation_UI2DSpriteAnimation1_m3406229089 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteAnimationGenerated::UI2DSpriteAnimation_ignoreTimeScale(JSVCall)
extern "C"  void UI2DSpriteAnimationGenerated_UI2DSpriteAnimation_ignoreTimeScale_m1310316255 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteAnimationGenerated::UI2DSpriteAnimation_loop(JSVCall)
extern "C"  void UI2DSpriteAnimationGenerated_UI2DSpriteAnimation_loop_m359698462 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteAnimationGenerated::UI2DSpriteAnimation_frames(JSVCall)
extern "C"  void UI2DSpriteAnimationGenerated_UI2DSpriteAnimation_frames_m4048565596 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteAnimationGenerated::UI2DSpriteAnimation_isPlaying(JSVCall)
extern "C"  void UI2DSpriteAnimationGenerated_UI2DSpriteAnimation_isPlaying_m1593152294 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteAnimationGenerated::UI2DSpriteAnimation_framesPerSecond(JSVCall)
extern "C"  void UI2DSpriteAnimationGenerated_UI2DSpriteAnimation_framesPerSecond_m1553979839 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UI2DSpriteAnimationGenerated::UI2DSpriteAnimation_Pause(JSVCall,System.Int32)
extern "C"  bool UI2DSpriteAnimationGenerated_UI2DSpriteAnimation_Pause_m288184303 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UI2DSpriteAnimationGenerated::UI2DSpriteAnimation_Play(JSVCall,System.Int32)
extern "C"  bool UI2DSpriteAnimationGenerated_UI2DSpriteAnimation_Play_m2305467101 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UI2DSpriteAnimationGenerated::UI2DSpriteAnimation_ResetToBeginning(JSVCall,System.Int32)
extern "C"  bool UI2DSpriteAnimationGenerated_UI2DSpriteAnimation_ResetToBeginning_m3605321276 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteAnimationGenerated::__Register()
extern "C"  void UI2DSpriteAnimationGenerated___Register_m1058282658 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite[] UI2DSpriteAnimationGenerated::<UI2DSpriteAnimation_frames>m__FD()
extern "C"  SpriteU5BU5D_t2761310900* UI2DSpriteAnimationGenerated_U3CUI2DSpriteAnimation_framesU3Em__FD_m1620883641 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UI2DSpriteAnimationGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UI2DSpriteAnimationGenerated_ilo_getObject1_m4227384301 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UI2DSpriteAnimationGenerated::ilo_getBooleanS2(System.Int32)
extern "C"  bool UI2DSpriteAnimationGenerated_ilo_getBooleanS2_m533673000 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteAnimationGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void UI2DSpriteAnimationGenerated_ilo_setBooleanS3_m4084384052 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteAnimationGenerated::ilo_moveSaveID2Arr4(System.Int32)
extern "C"  void UI2DSpriteAnimationGenerated_ilo_moveSaveID2Arr4_m1407390685 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteAnimationGenerated::ilo_setInt325(System.Int32,System.Int32)
extern "C"  void UI2DSpriteAnimationGenerated_ilo_setInt325_m262818477 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UI2DSpriteAnimationGenerated::ilo_getInt326(System.Int32)
extern "C"  int32_t UI2DSpriteAnimationGenerated_ilo_getInt326_m593734481 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteAnimationGenerated::ilo_set_framesPerSecond7(UI2DSpriteAnimation,System.Int32)
extern "C"  void UI2DSpriteAnimationGenerated_ilo_set_framesPerSecond7_m3875495097 (Il2CppObject * __this /* static, unused */, UI2DSpriteAnimation_t3878779257 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteAnimationGenerated::ilo_Play8(UI2DSpriteAnimation)
extern "C"  void UI2DSpriteAnimationGenerated_ilo_Play8_m2864964549 (Il2CppObject * __this /* static, unused */, UI2DSpriteAnimation_t3878779257 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UI2DSpriteAnimationGenerated::ilo_getArrayLength9(System.Int32)
extern "C"  int32_t UI2DSpriteAnimationGenerated_ilo_getArrayLength9_m3599983779 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UI2DSpriteAnimationGenerated::ilo_getElement10(System.Int32,System.Int32)
extern "C"  int32_t UI2DSpriteAnimationGenerated_ilo_getElement10_m3554996577 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UI2DSpriteAnimationGenerated::ilo_getObject11(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UI2DSpriteAnimationGenerated_ilo_getObject11_m129322743 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
