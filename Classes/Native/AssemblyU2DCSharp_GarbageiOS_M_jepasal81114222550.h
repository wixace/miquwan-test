﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_jepasal8
struct  M_jepasal8_t1114222550  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_jepasal8::_serele
	bool ____serele_0;
	// System.String GarbageiOS.M_jepasal8::_rirsere
	String_t* ____rirsere_1;
	// System.Boolean GarbageiOS.M_jepasal8::_pateakuTrarsel
	bool ____pateakuTrarsel_2;
	// System.Boolean GarbageiOS.M_jepasal8::_reardocearSurdarre
	bool ____reardocearSurdarre_3;
	// System.Boolean GarbageiOS.M_jepasal8::_mousir
	bool ____mousir_4;
	// System.UInt32 GarbageiOS.M_jepasal8::_deafeeCepertir
	uint32_t ____deafeeCepertir_5;
	// System.UInt32 GarbageiOS.M_jepasal8::_rawtece
	uint32_t ____rawtece_6;
	// System.Boolean GarbageiOS.M_jepasal8::_wuzurrir
	bool ____wuzurrir_7;
	// System.Int32 GarbageiOS.M_jepasal8::_neeserWairzeecoo
	int32_t ____neeserWairzeecoo_8;
	// System.Int32 GarbageiOS.M_jepasal8::_laryee
	int32_t ____laryee_9;
	// System.Int32 GarbageiOS.M_jepasal8::_tawsa
	int32_t ____tawsa_10;
	// System.Single GarbageiOS.M_jepasal8::_nursairMasnu
	float ____nursairMasnu_11;
	// System.Boolean GarbageiOS.M_jepasal8::_mootase
	bool ____mootase_12;
	// System.String GarbageiOS.M_jepasal8::_jayparta
	String_t* ____jayparta_13;
	// System.Single GarbageiOS.M_jepasal8::_vairis
	float ____vairis_14;
	// System.Int32 GarbageiOS.M_jepasal8::_qudezaBoustischa
	int32_t ____qudezaBoustischa_15;
	// System.Single GarbageiOS.M_jepasal8::_moubobaDasjumou
	float ____moubobaDasjumou_16;
	// System.Single GarbageiOS.M_jepasal8::_dijaBamow
	float ____dijaBamow_17;
	// System.Boolean GarbageiOS.M_jepasal8::_cashaw
	bool ____cashaw_18;
	// System.Single GarbageiOS.M_jepasal8::_lamarca
	float ____lamarca_19;
	// System.Boolean GarbageiOS.M_jepasal8::_taysequ
	bool ____taysequ_20;
	// System.Boolean GarbageiOS.M_jepasal8::_pasuBerepowzaw
	bool ____pasuBerepowzaw_21;
	// System.Int32 GarbageiOS.M_jepasal8::_sisel
	int32_t ____sisel_22;
	// System.Boolean GarbageiOS.M_jepasal8::_misra
	bool ____misra_23;
	// System.String GarbageiOS.M_jepasal8::_gouwhekeFape
	String_t* ____gouwhekeFape_24;
	// System.String GarbageiOS.M_jepasal8::_caife
	String_t* ____caife_25;
	// System.Single GarbageiOS.M_jepasal8::_whecee
	float ____whecee_26;
	// System.Boolean GarbageiOS.M_jepasal8::_dawnouke
	bool ____dawnouke_27;
	// System.Boolean GarbageiOS.M_jepasal8::_loocurkairSisnay
	bool ____loocurkairSisnay_28;
	// System.Single GarbageiOS.M_jepasal8::_neltemvea
	float ____neltemvea_29;
	// System.String GarbageiOS.M_jepasal8::_rerlairsallWhasgeta
	String_t* ____rerlairsallWhasgeta_30;
	// System.Boolean GarbageiOS.M_jepasal8::_keataselHagea
	bool ____keataselHagea_31;
	// System.Boolean GarbageiOS.M_jepasal8::_nirtai
	bool ____nirtai_32;
	// System.Boolean GarbageiOS.M_jepasal8::_certee
	bool ____certee_33;
	// System.Boolean GarbageiOS.M_jepasal8::_chowforallStayrarbea
	bool ____chowforallStayrarbea_34;
	// System.Boolean GarbageiOS.M_jepasal8::_zozeBornirra
	bool ____zozeBornirra_35;
	// System.Int32 GarbageiOS.M_jepasal8::_cazere
	int32_t ____cazere_36;
	// System.UInt32 GarbageiOS.M_jepasal8::_chorsur
	uint32_t ____chorsur_37;
	// System.UInt32 GarbageiOS.M_jepasal8::_dalawmouCijourer
	uint32_t ____dalawmouCijourer_38;
	// System.Boolean GarbageiOS.M_jepasal8::_yempalSuso
	bool ____yempalSuso_39;
	// System.Boolean GarbageiOS.M_jepasal8::_wejekeTikoxe
	bool ____wejekeTikoxe_40;
	// System.Int32 GarbageiOS.M_jepasal8::_dahairji
	int32_t ____dahairji_41;
	// System.String GarbageiOS.M_jepasal8::_nirou
	String_t* ____nirou_42;
	// System.Int32 GarbageiOS.M_jepasal8::_narbonirToboonou
	int32_t ____narbonirToboonou_43;
	// System.Single GarbageiOS.M_jepasal8::_sisrerbar
	float ____sisrerbar_44;
	// System.Single GarbageiOS.M_jepasal8::_tadoRurvair
	float ____tadoRurvair_45;
	// System.UInt32 GarbageiOS.M_jepasal8::_jallfeCoge
	uint32_t ____jallfeCoge_46;
	// System.String GarbageiOS.M_jepasal8::_gasehaiGisbemday
	String_t* ____gasehaiGisbemday_47;
	// System.String GarbageiOS.M_jepasal8::_mirnastairCheesoopu
	String_t* ____mirnastairCheesoopu_48;
	// System.Int32 GarbageiOS.M_jepasal8::_kepesairReremar
	int32_t ____kepesairReremar_49;
	// System.Boolean GarbageiOS.M_jepasal8::_gepembeaJixa
	bool ____gepembeaJixa_50;
	// System.String GarbageiOS.M_jepasal8::_hehaChorowca
	String_t* ____hehaChorowca_51;
	// System.Int32 GarbageiOS.M_jepasal8::_trecuSujersta
	int32_t ____trecuSujersta_52;
	// System.Int32 GarbageiOS.M_jepasal8::_fasha
	int32_t ____fasha_53;
	// System.String GarbageiOS.M_jepasal8::_papirfearTearhi
	String_t* ____papirfearTearhi_54;
	// System.Single GarbageiOS.M_jepasal8::_gursta
	float ____gursta_55;
	// System.Int32 GarbageiOS.M_jepasal8::_sejitrou
	int32_t ____sejitrou_56;

public:
	inline static int32_t get_offset_of__serele_0() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____serele_0)); }
	inline bool get__serele_0() const { return ____serele_0; }
	inline bool* get_address_of__serele_0() { return &____serele_0; }
	inline void set__serele_0(bool value)
	{
		____serele_0 = value;
	}

	inline static int32_t get_offset_of__rirsere_1() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____rirsere_1)); }
	inline String_t* get__rirsere_1() const { return ____rirsere_1; }
	inline String_t** get_address_of__rirsere_1() { return &____rirsere_1; }
	inline void set__rirsere_1(String_t* value)
	{
		____rirsere_1 = value;
		Il2CppCodeGenWriteBarrier(&____rirsere_1, value);
	}

	inline static int32_t get_offset_of__pateakuTrarsel_2() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____pateakuTrarsel_2)); }
	inline bool get__pateakuTrarsel_2() const { return ____pateakuTrarsel_2; }
	inline bool* get_address_of__pateakuTrarsel_2() { return &____pateakuTrarsel_2; }
	inline void set__pateakuTrarsel_2(bool value)
	{
		____pateakuTrarsel_2 = value;
	}

	inline static int32_t get_offset_of__reardocearSurdarre_3() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____reardocearSurdarre_3)); }
	inline bool get__reardocearSurdarre_3() const { return ____reardocearSurdarre_3; }
	inline bool* get_address_of__reardocearSurdarre_3() { return &____reardocearSurdarre_3; }
	inline void set__reardocearSurdarre_3(bool value)
	{
		____reardocearSurdarre_3 = value;
	}

	inline static int32_t get_offset_of__mousir_4() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____mousir_4)); }
	inline bool get__mousir_4() const { return ____mousir_4; }
	inline bool* get_address_of__mousir_4() { return &____mousir_4; }
	inline void set__mousir_4(bool value)
	{
		____mousir_4 = value;
	}

	inline static int32_t get_offset_of__deafeeCepertir_5() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____deafeeCepertir_5)); }
	inline uint32_t get__deafeeCepertir_5() const { return ____deafeeCepertir_5; }
	inline uint32_t* get_address_of__deafeeCepertir_5() { return &____deafeeCepertir_5; }
	inline void set__deafeeCepertir_5(uint32_t value)
	{
		____deafeeCepertir_5 = value;
	}

	inline static int32_t get_offset_of__rawtece_6() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____rawtece_6)); }
	inline uint32_t get__rawtece_6() const { return ____rawtece_6; }
	inline uint32_t* get_address_of__rawtece_6() { return &____rawtece_6; }
	inline void set__rawtece_6(uint32_t value)
	{
		____rawtece_6 = value;
	}

	inline static int32_t get_offset_of__wuzurrir_7() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____wuzurrir_7)); }
	inline bool get__wuzurrir_7() const { return ____wuzurrir_7; }
	inline bool* get_address_of__wuzurrir_7() { return &____wuzurrir_7; }
	inline void set__wuzurrir_7(bool value)
	{
		____wuzurrir_7 = value;
	}

	inline static int32_t get_offset_of__neeserWairzeecoo_8() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____neeserWairzeecoo_8)); }
	inline int32_t get__neeserWairzeecoo_8() const { return ____neeserWairzeecoo_8; }
	inline int32_t* get_address_of__neeserWairzeecoo_8() { return &____neeserWairzeecoo_8; }
	inline void set__neeserWairzeecoo_8(int32_t value)
	{
		____neeserWairzeecoo_8 = value;
	}

	inline static int32_t get_offset_of__laryee_9() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____laryee_9)); }
	inline int32_t get__laryee_9() const { return ____laryee_9; }
	inline int32_t* get_address_of__laryee_9() { return &____laryee_9; }
	inline void set__laryee_9(int32_t value)
	{
		____laryee_9 = value;
	}

	inline static int32_t get_offset_of__tawsa_10() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____tawsa_10)); }
	inline int32_t get__tawsa_10() const { return ____tawsa_10; }
	inline int32_t* get_address_of__tawsa_10() { return &____tawsa_10; }
	inline void set__tawsa_10(int32_t value)
	{
		____tawsa_10 = value;
	}

	inline static int32_t get_offset_of__nursairMasnu_11() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____nursairMasnu_11)); }
	inline float get__nursairMasnu_11() const { return ____nursairMasnu_11; }
	inline float* get_address_of__nursairMasnu_11() { return &____nursairMasnu_11; }
	inline void set__nursairMasnu_11(float value)
	{
		____nursairMasnu_11 = value;
	}

	inline static int32_t get_offset_of__mootase_12() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____mootase_12)); }
	inline bool get__mootase_12() const { return ____mootase_12; }
	inline bool* get_address_of__mootase_12() { return &____mootase_12; }
	inline void set__mootase_12(bool value)
	{
		____mootase_12 = value;
	}

	inline static int32_t get_offset_of__jayparta_13() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____jayparta_13)); }
	inline String_t* get__jayparta_13() const { return ____jayparta_13; }
	inline String_t** get_address_of__jayparta_13() { return &____jayparta_13; }
	inline void set__jayparta_13(String_t* value)
	{
		____jayparta_13 = value;
		Il2CppCodeGenWriteBarrier(&____jayparta_13, value);
	}

	inline static int32_t get_offset_of__vairis_14() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____vairis_14)); }
	inline float get__vairis_14() const { return ____vairis_14; }
	inline float* get_address_of__vairis_14() { return &____vairis_14; }
	inline void set__vairis_14(float value)
	{
		____vairis_14 = value;
	}

	inline static int32_t get_offset_of__qudezaBoustischa_15() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____qudezaBoustischa_15)); }
	inline int32_t get__qudezaBoustischa_15() const { return ____qudezaBoustischa_15; }
	inline int32_t* get_address_of__qudezaBoustischa_15() { return &____qudezaBoustischa_15; }
	inline void set__qudezaBoustischa_15(int32_t value)
	{
		____qudezaBoustischa_15 = value;
	}

	inline static int32_t get_offset_of__moubobaDasjumou_16() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____moubobaDasjumou_16)); }
	inline float get__moubobaDasjumou_16() const { return ____moubobaDasjumou_16; }
	inline float* get_address_of__moubobaDasjumou_16() { return &____moubobaDasjumou_16; }
	inline void set__moubobaDasjumou_16(float value)
	{
		____moubobaDasjumou_16 = value;
	}

	inline static int32_t get_offset_of__dijaBamow_17() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____dijaBamow_17)); }
	inline float get__dijaBamow_17() const { return ____dijaBamow_17; }
	inline float* get_address_of__dijaBamow_17() { return &____dijaBamow_17; }
	inline void set__dijaBamow_17(float value)
	{
		____dijaBamow_17 = value;
	}

	inline static int32_t get_offset_of__cashaw_18() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____cashaw_18)); }
	inline bool get__cashaw_18() const { return ____cashaw_18; }
	inline bool* get_address_of__cashaw_18() { return &____cashaw_18; }
	inline void set__cashaw_18(bool value)
	{
		____cashaw_18 = value;
	}

	inline static int32_t get_offset_of__lamarca_19() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____lamarca_19)); }
	inline float get__lamarca_19() const { return ____lamarca_19; }
	inline float* get_address_of__lamarca_19() { return &____lamarca_19; }
	inline void set__lamarca_19(float value)
	{
		____lamarca_19 = value;
	}

	inline static int32_t get_offset_of__taysequ_20() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____taysequ_20)); }
	inline bool get__taysequ_20() const { return ____taysequ_20; }
	inline bool* get_address_of__taysequ_20() { return &____taysequ_20; }
	inline void set__taysequ_20(bool value)
	{
		____taysequ_20 = value;
	}

	inline static int32_t get_offset_of__pasuBerepowzaw_21() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____pasuBerepowzaw_21)); }
	inline bool get__pasuBerepowzaw_21() const { return ____pasuBerepowzaw_21; }
	inline bool* get_address_of__pasuBerepowzaw_21() { return &____pasuBerepowzaw_21; }
	inline void set__pasuBerepowzaw_21(bool value)
	{
		____pasuBerepowzaw_21 = value;
	}

	inline static int32_t get_offset_of__sisel_22() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____sisel_22)); }
	inline int32_t get__sisel_22() const { return ____sisel_22; }
	inline int32_t* get_address_of__sisel_22() { return &____sisel_22; }
	inline void set__sisel_22(int32_t value)
	{
		____sisel_22 = value;
	}

	inline static int32_t get_offset_of__misra_23() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____misra_23)); }
	inline bool get__misra_23() const { return ____misra_23; }
	inline bool* get_address_of__misra_23() { return &____misra_23; }
	inline void set__misra_23(bool value)
	{
		____misra_23 = value;
	}

	inline static int32_t get_offset_of__gouwhekeFape_24() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____gouwhekeFape_24)); }
	inline String_t* get__gouwhekeFape_24() const { return ____gouwhekeFape_24; }
	inline String_t** get_address_of__gouwhekeFape_24() { return &____gouwhekeFape_24; }
	inline void set__gouwhekeFape_24(String_t* value)
	{
		____gouwhekeFape_24 = value;
		Il2CppCodeGenWriteBarrier(&____gouwhekeFape_24, value);
	}

	inline static int32_t get_offset_of__caife_25() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____caife_25)); }
	inline String_t* get__caife_25() const { return ____caife_25; }
	inline String_t** get_address_of__caife_25() { return &____caife_25; }
	inline void set__caife_25(String_t* value)
	{
		____caife_25 = value;
		Il2CppCodeGenWriteBarrier(&____caife_25, value);
	}

	inline static int32_t get_offset_of__whecee_26() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____whecee_26)); }
	inline float get__whecee_26() const { return ____whecee_26; }
	inline float* get_address_of__whecee_26() { return &____whecee_26; }
	inline void set__whecee_26(float value)
	{
		____whecee_26 = value;
	}

	inline static int32_t get_offset_of__dawnouke_27() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____dawnouke_27)); }
	inline bool get__dawnouke_27() const { return ____dawnouke_27; }
	inline bool* get_address_of__dawnouke_27() { return &____dawnouke_27; }
	inline void set__dawnouke_27(bool value)
	{
		____dawnouke_27 = value;
	}

	inline static int32_t get_offset_of__loocurkairSisnay_28() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____loocurkairSisnay_28)); }
	inline bool get__loocurkairSisnay_28() const { return ____loocurkairSisnay_28; }
	inline bool* get_address_of__loocurkairSisnay_28() { return &____loocurkairSisnay_28; }
	inline void set__loocurkairSisnay_28(bool value)
	{
		____loocurkairSisnay_28 = value;
	}

	inline static int32_t get_offset_of__neltemvea_29() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____neltemvea_29)); }
	inline float get__neltemvea_29() const { return ____neltemvea_29; }
	inline float* get_address_of__neltemvea_29() { return &____neltemvea_29; }
	inline void set__neltemvea_29(float value)
	{
		____neltemvea_29 = value;
	}

	inline static int32_t get_offset_of__rerlairsallWhasgeta_30() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____rerlairsallWhasgeta_30)); }
	inline String_t* get__rerlairsallWhasgeta_30() const { return ____rerlairsallWhasgeta_30; }
	inline String_t** get_address_of__rerlairsallWhasgeta_30() { return &____rerlairsallWhasgeta_30; }
	inline void set__rerlairsallWhasgeta_30(String_t* value)
	{
		____rerlairsallWhasgeta_30 = value;
		Il2CppCodeGenWriteBarrier(&____rerlairsallWhasgeta_30, value);
	}

	inline static int32_t get_offset_of__keataselHagea_31() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____keataselHagea_31)); }
	inline bool get__keataselHagea_31() const { return ____keataselHagea_31; }
	inline bool* get_address_of__keataselHagea_31() { return &____keataselHagea_31; }
	inline void set__keataselHagea_31(bool value)
	{
		____keataselHagea_31 = value;
	}

	inline static int32_t get_offset_of__nirtai_32() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____nirtai_32)); }
	inline bool get__nirtai_32() const { return ____nirtai_32; }
	inline bool* get_address_of__nirtai_32() { return &____nirtai_32; }
	inline void set__nirtai_32(bool value)
	{
		____nirtai_32 = value;
	}

	inline static int32_t get_offset_of__certee_33() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____certee_33)); }
	inline bool get__certee_33() const { return ____certee_33; }
	inline bool* get_address_of__certee_33() { return &____certee_33; }
	inline void set__certee_33(bool value)
	{
		____certee_33 = value;
	}

	inline static int32_t get_offset_of__chowforallStayrarbea_34() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____chowforallStayrarbea_34)); }
	inline bool get__chowforallStayrarbea_34() const { return ____chowforallStayrarbea_34; }
	inline bool* get_address_of__chowforallStayrarbea_34() { return &____chowforallStayrarbea_34; }
	inline void set__chowforallStayrarbea_34(bool value)
	{
		____chowforallStayrarbea_34 = value;
	}

	inline static int32_t get_offset_of__zozeBornirra_35() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____zozeBornirra_35)); }
	inline bool get__zozeBornirra_35() const { return ____zozeBornirra_35; }
	inline bool* get_address_of__zozeBornirra_35() { return &____zozeBornirra_35; }
	inline void set__zozeBornirra_35(bool value)
	{
		____zozeBornirra_35 = value;
	}

	inline static int32_t get_offset_of__cazere_36() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____cazere_36)); }
	inline int32_t get__cazere_36() const { return ____cazere_36; }
	inline int32_t* get_address_of__cazere_36() { return &____cazere_36; }
	inline void set__cazere_36(int32_t value)
	{
		____cazere_36 = value;
	}

	inline static int32_t get_offset_of__chorsur_37() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____chorsur_37)); }
	inline uint32_t get__chorsur_37() const { return ____chorsur_37; }
	inline uint32_t* get_address_of__chorsur_37() { return &____chorsur_37; }
	inline void set__chorsur_37(uint32_t value)
	{
		____chorsur_37 = value;
	}

	inline static int32_t get_offset_of__dalawmouCijourer_38() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____dalawmouCijourer_38)); }
	inline uint32_t get__dalawmouCijourer_38() const { return ____dalawmouCijourer_38; }
	inline uint32_t* get_address_of__dalawmouCijourer_38() { return &____dalawmouCijourer_38; }
	inline void set__dalawmouCijourer_38(uint32_t value)
	{
		____dalawmouCijourer_38 = value;
	}

	inline static int32_t get_offset_of__yempalSuso_39() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____yempalSuso_39)); }
	inline bool get__yempalSuso_39() const { return ____yempalSuso_39; }
	inline bool* get_address_of__yempalSuso_39() { return &____yempalSuso_39; }
	inline void set__yempalSuso_39(bool value)
	{
		____yempalSuso_39 = value;
	}

	inline static int32_t get_offset_of__wejekeTikoxe_40() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____wejekeTikoxe_40)); }
	inline bool get__wejekeTikoxe_40() const { return ____wejekeTikoxe_40; }
	inline bool* get_address_of__wejekeTikoxe_40() { return &____wejekeTikoxe_40; }
	inline void set__wejekeTikoxe_40(bool value)
	{
		____wejekeTikoxe_40 = value;
	}

	inline static int32_t get_offset_of__dahairji_41() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____dahairji_41)); }
	inline int32_t get__dahairji_41() const { return ____dahairji_41; }
	inline int32_t* get_address_of__dahairji_41() { return &____dahairji_41; }
	inline void set__dahairji_41(int32_t value)
	{
		____dahairji_41 = value;
	}

	inline static int32_t get_offset_of__nirou_42() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____nirou_42)); }
	inline String_t* get__nirou_42() const { return ____nirou_42; }
	inline String_t** get_address_of__nirou_42() { return &____nirou_42; }
	inline void set__nirou_42(String_t* value)
	{
		____nirou_42 = value;
		Il2CppCodeGenWriteBarrier(&____nirou_42, value);
	}

	inline static int32_t get_offset_of__narbonirToboonou_43() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____narbonirToboonou_43)); }
	inline int32_t get__narbonirToboonou_43() const { return ____narbonirToboonou_43; }
	inline int32_t* get_address_of__narbonirToboonou_43() { return &____narbonirToboonou_43; }
	inline void set__narbonirToboonou_43(int32_t value)
	{
		____narbonirToboonou_43 = value;
	}

	inline static int32_t get_offset_of__sisrerbar_44() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____sisrerbar_44)); }
	inline float get__sisrerbar_44() const { return ____sisrerbar_44; }
	inline float* get_address_of__sisrerbar_44() { return &____sisrerbar_44; }
	inline void set__sisrerbar_44(float value)
	{
		____sisrerbar_44 = value;
	}

	inline static int32_t get_offset_of__tadoRurvair_45() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____tadoRurvair_45)); }
	inline float get__tadoRurvair_45() const { return ____tadoRurvair_45; }
	inline float* get_address_of__tadoRurvair_45() { return &____tadoRurvair_45; }
	inline void set__tadoRurvair_45(float value)
	{
		____tadoRurvair_45 = value;
	}

	inline static int32_t get_offset_of__jallfeCoge_46() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____jallfeCoge_46)); }
	inline uint32_t get__jallfeCoge_46() const { return ____jallfeCoge_46; }
	inline uint32_t* get_address_of__jallfeCoge_46() { return &____jallfeCoge_46; }
	inline void set__jallfeCoge_46(uint32_t value)
	{
		____jallfeCoge_46 = value;
	}

	inline static int32_t get_offset_of__gasehaiGisbemday_47() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____gasehaiGisbemday_47)); }
	inline String_t* get__gasehaiGisbemday_47() const { return ____gasehaiGisbemday_47; }
	inline String_t** get_address_of__gasehaiGisbemday_47() { return &____gasehaiGisbemday_47; }
	inline void set__gasehaiGisbemday_47(String_t* value)
	{
		____gasehaiGisbemday_47 = value;
		Il2CppCodeGenWriteBarrier(&____gasehaiGisbemday_47, value);
	}

	inline static int32_t get_offset_of__mirnastairCheesoopu_48() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____mirnastairCheesoopu_48)); }
	inline String_t* get__mirnastairCheesoopu_48() const { return ____mirnastairCheesoopu_48; }
	inline String_t** get_address_of__mirnastairCheesoopu_48() { return &____mirnastairCheesoopu_48; }
	inline void set__mirnastairCheesoopu_48(String_t* value)
	{
		____mirnastairCheesoopu_48 = value;
		Il2CppCodeGenWriteBarrier(&____mirnastairCheesoopu_48, value);
	}

	inline static int32_t get_offset_of__kepesairReremar_49() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____kepesairReremar_49)); }
	inline int32_t get__kepesairReremar_49() const { return ____kepesairReremar_49; }
	inline int32_t* get_address_of__kepesairReremar_49() { return &____kepesairReremar_49; }
	inline void set__kepesairReremar_49(int32_t value)
	{
		____kepesairReremar_49 = value;
	}

	inline static int32_t get_offset_of__gepembeaJixa_50() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____gepembeaJixa_50)); }
	inline bool get__gepembeaJixa_50() const { return ____gepembeaJixa_50; }
	inline bool* get_address_of__gepembeaJixa_50() { return &____gepembeaJixa_50; }
	inline void set__gepembeaJixa_50(bool value)
	{
		____gepembeaJixa_50 = value;
	}

	inline static int32_t get_offset_of__hehaChorowca_51() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____hehaChorowca_51)); }
	inline String_t* get__hehaChorowca_51() const { return ____hehaChorowca_51; }
	inline String_t** get_address_of__hehaChorowca_51() { return &____hehaChorowca_51; }
	inline void set__hehaChorowca_51(String_t* value)
	{
		____hehaChorowca_51 = value;
		Il2CppCodeGenWriteBarrier(&____hehaChorowca_51, value);
	}

	inline static int32_t get_offset_of__trecuSujersta_52() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____trecuSujersta_52)); }
	inline int32_t get__trecuSujersta_52() const { return ____trecuSujersta_52; }
	inline int32_t* get_address_of__trecuSujersta_52() { return &____trecuSujersta_52; }
	inline void set__trecuSujersta_52(int32_t value)
	{
		____trecuSujersta_52 = value;
	}

	inline static int32_t get_offset_of__fasha_53() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____fasha_53)); }
	inline int32_t get__fasha_53() const { return ____fasha_53; }
	inline int32_t* get_address_of__fasha_53() { return &____fasha_53; }
	inline void set__fasha_53(int32_t value)
	{
		____fasha_53 = value;
	}

	inline static int32_t get_offset_of__papirfearTearhi_54() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____papirfearTearhi_54)); }
	inline String_t* get__papirfearTearhi_54() const { return ____papirfearTearhi_54; }
	inline String_t** get_address_of__papirfearTearhi_54() { return &____papirfearTearhi_54; }
	inline void set__papirfearTearhi_54(String_t* value)
	{
		____papirfearTearhi_54 = value;
		Il2CppCodeGenWriteBarrier(&____papirfearTearhi_54, value);
	}

	inline static int32_t get_offset_of__gursta_55() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____gursta_55)); }
	inline float get__gursta_55() const { return ____gursta_55; }
	inline float* get_address_of__gursta_55() { return &____gursta_55; }
	inline void set__gursta_55(float value)
	{
		____gursta_55 = value;
	}

	inline static int32_t get_offset_of__sejitrou_56() { return static_cast<int32_t>(offsetof(M_jepasal8_t1114222550, ____sejitrou_56)); }
	inline int32_t get__sejitrou_56() const { return ____sejitrou_56; }
	inline int32_t* get_address_of__sejitrou_56() { return &____sejitrou_56; }
	inline void set__sejitrou_56(int32_t value)
	{
		____sejitrou_56 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
