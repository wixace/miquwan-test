﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_riheahiTealelfe207
struct  M_riheahiTealelfe207_t1391804857  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_riheahiTealelfe207::_mipeday
	int32_t ____mipeday_0;
	// System.String GarbageiOS.M_riheahiTealelfe207::_dircenoSasowmow
	String_t* ____dircenoSasowmow_1;
	// System.String GarbageiOS.M_riheahiTealelfe207::_stoodisgasGerewownu
	String_t* ____stoodisgasGerewownu_2;
	// System.UInt32 GarbageiOS.M_riheahiTealelfe207::_yeesi
	uint32_t ____yeesi_3;
	// System.UInt32 GarbageiOS.M_riheahiTealelfe207::_tirsoobear
	uint32_t ____tirsoobear_4;
	// System.UInt32 GarbageiOS.M_riheahiTealelfe207::_seho
	uint32_t ____seho_5;
	// System.UInt32 GarbageiOS.M_riheahiTealelfe207::_pelsetou
	uint32_t ____pelsetou_6;
	// System.Boolean GarbageiOS.M_riheahiTealelfe207::_gownea
	bool ____gownea_7;
	// System.Boolean GarbageiOS.M_riheahiTealelfe207::_chote
	bool ____chote_8;
	// System.UInt32 GarbageiOS.M_riheahiTealelfe207::_tema
	uint32_t ____tema_9;
	// System.Int32 GarbageiOS.M_riheahiTealelfe207::_seenai
	int32_t ____seenai_10;
	// System.Int32 GarbageiOS.M_riheahiTealelfe207::_naroodre
	int32_t ____naroodre_11;
	// System.Single GarbageiOS.M_riheahiTealelfe207::_neardai
	float ____neardai_12;
	// System.Int32 GarbageiOS.M_riheahiTealelfe207::_qenorga
	int32_t ____qenorga_13;
	// System.Boolean GarbageiOS.M_riheahiTealelfe207::_geexi
	bool ____geexi_14;
	// System.String GarbageiOS.M_riheahiTealelfe207::_vawceaXede
	String_t* ____vawceaXede_15;
	// System.Single GarbageiOS.M_riheahiTealelfe207::_kurcuwaiXowsoo
	float ____kurcuwaiXowsoo_16;
	// System.UInt32 GarbageiOS.M_riheahiTealelfe207::_huhearbeeLelroopou
	uint32_t ____huhearbeeLelroopou_17;
	// System.Single GarbageiOS.M_riheahiTealelfe207::_horjur
	float ____horjur_18;
	// System.UInt32 GarbageiOS.M_riheahiTealelfe207::_salltarTeawal
	uint32_t ____salltarTeawal_19;
	// System.Single GarbageiOS.M_riheahiTealelfe207::_yaidoo
	float ____yaidoo_20;
	// System.UInt32 GarbageiOS.M_riheahiTealelfe207::_toucai
	uint32_t ____toucai_21;
	// System.UInt32 GarbageiOS.M_riheahiTealelfe207::_hocarurWisto
	uint32_t ____hocarurWisto_22;
	// System.Int32 GarbageiOS.M_riheahiTealelfe207::_pelpaye
	int32_t ____pelpaye_23;
	// System.String GarbageiOS.M_riheahiTealelfe207::_lanayTeagaipear
	String_t* ____lanayTeagaipear_24;
	// System.Single GarbageiOS.M_riheahiTealelfe207::_baytemte
	float ____baytemte_25;
	// System.UInt32 GarbageiOS.M_riheahiTealelfe207::_soru
	uint32_t ____soru_26;
	// System.Int32 GarbageiOS.M_riheahiTealelfe207::_stokawjeFejihe
	int32_t ____stokawjeFejihe_27;
	// System.Boolean GarbageiOS.M_riheahiTealelfe207::_sterebairSisfisi
	bool ____sterebairSisfisi_28;
	// System.Int32 GarbageiOS.M_riheahiTealelfe207::_whayyee
	int32_t ____whayyee_29;
	// System.String GarbageiOS.M_riheahiTealelfe207::_bouval
	String_t* ____bouval_30;
	// System.Single GarbageiOS.M_riheahiTealelfe207::_gacur
	float ____gacur_31;
	// System.Single GarbageiOS.M_riheahiTealelfe207::_whispall
	float ____whispall_32;
	// System.String GarbageiOS.M_riheahiTealelfe207::_kacu
	String_t* ____kacu_33;
	// System.UInt32 GarbageiOS.M_riheahiTealelfe207::_pairpe
	uint32_t ____pairpe_34;
	// System.Boolean GarbageiOS.M_riheahiTealelfe207::_maswaharPircowlal
	bool ____maswaharPircowlal_35;
	// System.Single GarbageiOS.M_riheahiTealelfe207::_recaga
	float ____recaga_36;
	// System.UInt32 GarbageiOS.M_riheahiTealelfe207::_lefudeCisaw
	uint32_t ____lefudeCisaw_37;
	// System.Single GarbageiOS.M_riheahiTealelfe207::_jemheeHounemgere
	float ____jemheeHounemgere_38;
	// System.Single GarbageiOS.M_riheahiTealelfe207::_sisrisaMojeanis
	float ____sisrisaMojeanis_39;
	// System.String GarbageiOS.M_riheahiTealelfe207::_podall
	String_t* ____podall_40;
	// System.Boolean GarbageiOS.M_riheahiTealelfe207::_baymacaiNaltis
	bool ____baymacaiNaltis_41;
	// System.Boolean GarbageiOS.M_riheahiTealelfe207::_qismelTallmowsi
	bool ____qismelTallmowsi_42;
	// System.UInt32 GarbageiOS.M_riheahiTealelfe207::_cajurNerere
	uint32_t ____cajurNerere_43;
	// System.Single GarbageiOS.M_riheahiTealelfe207::_cerhairlemYistem
	float ____cerhairlemYistem_44;
	// System.String GarbageiOS.M_riheahiTealelfe207::_pelxa
	String_t* ____pelxa_45;
	// System.String GarbageiOS.M_riheahiTealelfe207::_mamayWhasi
	String_t* ____mamayWhasi_46;
	// System.Boolean GarbageiOS.M_riheahiTealelfe207::_wapenaQoobi
	bool ____wapenaQoobi_47;
	// System.Int32 GarbageiOS.M_riheahiTealelfe207::_kokeawouRahea
	int32_t ____kokeawouRahea_48;
	// System.Int32 GarbageiOS.M_riheahiTealelfe207::_winow
	int32_t ____winow_49;
	// System.Boolean GarbageiOS.M_riheahiTealelfe207::_gimerCecemmer
	bool ____gimerCecemmer_50;

public:
	inline static int32_t get_offset_of__mipeday_0() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____mipeday_0)); }
	inline int32_t get__mipeday_0() const { return ____mipeday_0; }
	inline int32_t* get_address_of__mipeday_0() { return &____mipeday_0; }
	inline void set__mipeday_0(int32_t value)
	{
		____mipeday_0 = value;
	}

	inline static int32_t get_offset_of__dircenoSasowmow_1() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____dircenoSasowmow_1)); }
	inline String_t* get__dircenoSasowmow_1() const { return ____dircenoSasowmow_1; }
	inline String_t** get_address_of__dircenoSasowmow_1() { return &____dircenoSasowmow_1; }
	inline void set__dircenoSasowmow_1(String_t* value)
	{
		____dircenoSasowmow_1 = value;
		Il2CppCodeGenWriteBarrier(&____dircenoSasowmow_1, value);
	}

	inline static int32_t get_offset_of__stoodisgasGerewownu_2() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____stoodisgasGerewownu_2)); }
	inline String_t* get__stoodisgasGerewownu_2() const { return ____stoodisgasGerewownu_2; }
	inline String_t** get_address_of__stoodisgasGerewownu_2() { return &____stoodisgasGerewownu_2; }
	inline void set__stoodisgasGerewownu_2(String_t* value)
	{
		____stoodisgasGerewownu_2 = value;
		Il2CppCodeGenWriteBarrier(&____stoodisgasGerewownu_2, value);
	}

	inline static int32_t get_offset_of__yeesi_3() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____yeesi_3)); }
	inline uint32_t get__yeesi_3() const { return ____yeesi_3; }
	inline uint32_t* get_address_of__yeesi_3() { return &____yeesi_3; }
	inline void set__yeesi_3(uint32_t value)
	{
		____yeesi_3 = value;
	}

	inline static int32_t get_offset_of__tirsoobear_4() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____tirsoobear_4)); }
	inline uint32_t get__tirsoobear_4() const { return ____tirsoobear_4; }
	inline uint32_t* get_address_of__tirsoobear_4() { return &____tirsoobear_4; }
	inline void set__tirsoobear_4(uint32_t value)
	{
		____tirsoobear_4 = value;
	}

	inline static int32_t get_offset_of__seho_5() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____seho_5)); }
	inline uint32_t get__seho_5() const { return ____seho_5; }
	inline uint32_t* get_address_of__seho_5() { return &____seho_5; }
	inline void set__seho_5(uint32_t value)
	{
		____seho_5 = value;
	}

	inline static int32_t get_offset_of__pelsetou_6() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____pelsetou_6)); }
	inline uint32_t get__pelsetou_6() const { return ____pelsetou_6; }
	inline uint32_t* get_address_of__pelsetou_6() { return &____pelsetou_6; }
	inline void set__pelsetou_6(uint32_t value)
	{
		____pelsetou_6 = value;
	}

	inline static int32_t get_offset_of__gownea_7() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____gownea_7)); }
	inline bool get__gownea_7() const { return ____gownea_7; }
	inline bool* get_address_of__gownea_7() { return &____gownea_7; }
	inline void set__gownea_7(bool value)
	{
		____gownea_7 = value;
	}

	inline static int32_t get_offset_of__chote_8() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____chote_8)); }
	inline bool get__chote_8() const { return ____chote_8; }
	inline bool* get_address_of__chote_8() { return &____chote_8; }
	inline void set__chote_8(bool value)
	{
		____chote_8 = value;
	}

	inline static int32_t get_offset_of__tema_9() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____tema_9)); }
	inline uint32_t get__tema_9() const { return ____tema_9; }
	inline uint32_t* get_address_of__tema_9() { return &____tema_9; }
	inline void set__tema_9(uint32_t value)
	{
		____tema_9 = value;
	}

	inline static int32_t get_offset_of__seenai_10() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____seenai_10)); }
	inline int32_t get__seenai_10() const { return ____seenai_10; }
	inline int32_t* get_address_of__seenai_10() { return &____seenai_10; }
	inline void set__seenai_10(int32_t value)
	{
		____seenai_10 = value;
	}

	inline static int32_t get_offset_of__naroodre_11() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____naroodre_11)); }
	inline int32_t get__naroodre_11() const { return ____naroodre_11; }
	inline int32_t* get_address_of__naroodre_11() { return &____naroodre_11; }
	inline void set__naroodre_11(int32_t value)
	{
		____naroodre_11 = value;
	}

	inline static int32_t get_offset_of__neardai_12() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____neardai_12)); }
	inline float get__neardai_12() const { return ____neardai_12; }
	inline float* get_address_of__neardai_12() { return &____neardai_12; }
	inline void set__neardai_12(float value)
	{
		____neardai_12 = value;
	}

	inline static int32_t get_offset_of__qenorga_13() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____qenorga_13)); }
	inline int32_t get__qenorga_13() const { return ____qenorga_13; }
	inline int32_t* get_address_of__qenorga_13() { return &____qenorga_13; }
	inline void set__qenorga_13(int32_t value)
	{
		____qenorga_13 = value;
	}

	inline static int32_t get_offset_of__geexi_14() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____geexi_14)); }
	inline bool get__geexi_14() const { return ____geexi_14; }
	inline bool* get_address_of__geexi_14() { return &____geexi_14; }
	inline void set__geexi_14(bool value)
	{
		____geexi_14 = value;
	}

	inline static int32_t get_offset_of__vawceaXede_15() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____vawceaXede_15)); }
	inline String_t* get__vawceaXede_15() const { return ____vawceaXede_15; }
	inline String_t** get_address_of__vawceaXede_15() { return &____vawceaXede_15; }
	inline void set__vawceaXede_15(String_t* value)
	{
		____vawceaXede_15 = value;
		Il2CppCodeGenWriteBarrier(&____vawceaXede_15, value);
	}

	inline static int32_t get_offset_of__kurcuwaiXowsoo_16() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____kurcuwaiXowsoo_16)); }
	inline float get__kurcuwaiXowsoo_16() const { return ____kurcuwaiXowsoo_16; }
	inline float* get_address_of__kurcuwaiXowsoo_16() { return &____kurcuwaiXowsoo_16; }
	inline void set__kurcuwaiXowsoo_16(float value)
	{
		____kurcuwaiXowsoo_16 = value;
	}

	inline static int32_t get_offset_of__huhearbeeLelroopou_17() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____huhearbeeLelroopou_17)); }
	inline uint32_t get__huhearbeeLelroopou_17() const { return ____huhearbeeLelroopou_17; }
	inline uint32_t* get_address_of__huhearbeeLelroopou_17() { return &____huhearbeeLelroopou_17; }
	inline void set__huhearbeeLelroopou_17(uint32_t value)
	{
		____huhearbeeLelroopou_17 = value;
	}

	inline static int32_t get_offset_of__horjur_18() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____horjur_18)); }
	inline float get__horjur_18() const { return ____horjur_18; }
	inline float* get_address_of__horjur_18() { return &____horjur_18; }
	inline void set__horjur_18(float value)
	{
		____horjur_18 = value;
	}

	inline static int32_t get_offset_of__salltarTeawal_19() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____salltarTeawal_19)); }
	inline uint32_t get__salltarTeawal_19() const { return ____salltarTeawal_19; }
	inline uint32_t* get_address_of__salltarTeawal_19() { return &____salltarTeawal_19; }
	inline void set__salltarTeawal_19(uint32_t value)
	{
		____salltarTeawal_19 = value;
	}

	inline static int32_t get_offset_of__yaidoo_20() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____yaidoo_20)); }
	inline float get__yaidoo_20() const { return ____yaidoo_20; }
	inline float* get_address_of__yaidoo_20() { return &____yaidoo_20; }
	inline void set__yaidoo_20(float value)
	{
		____yaidoo_20 = value;
	}

	inline static int32_t get_offset_of__toucai_21() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____toucai_21)); }
	inline uint32_t get__toucai_21() const { return ____toucai_21; }
	inline uint32_t* get_address_of__toucai_21() { return &____toucai_21; }
	inline void set__toucai_21(uint32_t value)
	{
		____toucai_21 = value;
	}

	inline static int32_t get_offset_of__hocarurWisto_22() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____hocarurWisto_22)); }
	inline uint32_t get__hocarurWisto_22() const { return ____hocarurWisto_22; }
	inline uint32_t* get_address_of__hocarurWisto_22() { return &____hocarurWisto_22; }
	inline void set__hocarurWisto_22(uint32_t value)
	{
		____hocarurWisto_22 = value;
	}

	inline static int32_t get_offset_of__pelpaye_23() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____pelpaye_23)); }
	inline int32_t get__pelpaye_23() const { return ____pelpaye_23; }
	inline int32_t* get_address_of__pelpaye_23() { return &____pelpaye_23; }
	inline void set__pelpaye_23(int32_t value)
	{
		____pelpaye_23 = value;
	}

	inline static int32_t get_offset_of__lanayTeagaipear_24() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____lanayTeagaipear_24)); }
	inline String_t* get__lanayTeagaipear_24() const { return ____lanayTeagaipear_24; }
	inline String_t** get_address_of__lanayTeagaipear_24() { return &____lanayTeagaipear_24; }
	inline void set__lanayTeagaipear_24(String_t* value)
	{
		____lanayTeagaipear_24 = value;
		Il2CppCodeGenWriteBarrier(&____lanayTeagaipear_24, value);
	}

	inline static int32_t get_offset_of__baytemte_25() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____baytemte_25)); }
	inline float get__baytemte_25() const { return ____baytemte_25; }
	inline float* get_address_of__baytemte_25() { return &____baytemte_25; }
	inline void set__baytemte_25(float value)
	{
		____baytemte_25 = value;
	}

	inline static int32_t get_offset_of__soru_26() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____soru_26)); }
	inline uint32_t get__soru_26() const { return ____soru_26; }
	inline uint32_t* get_address_of__soru_26() { return &____soru_26; }
	inline void set__soru_26(uint32_t value)
	{
		____soru_26 = value;
	}

	inline static int32_t get_offset_of__stokawjeFejihe_27() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____stokawjeFejihe_27)); }
	inline int32_t get__stokawjeFejihe_27() const { return ____stokawjeFejihe_27; }
	inline int32_t* get_address_of__stokawjeFejihe_27() { return &____stokawjeFejihe_27; }
	inline void set__stokawjeFejihe_27(int32_t value)
	{
		____stokawjeFejihe_27 = value;
	}

	inline static int32_t get_offset_of__sterebairSisfisi_28() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____sterebairSisfisi_28)); }
	inline bool get__sterebairSisfisi_28() const { return ____sterebairSisfisi_28; }
	inline bool* get_address_of__sterebairSisfisi_28() { return &____sterebairSisfisi_28; }
	inline void set__sterebairSisfisi_28(bool value)
	{
		____sterebairSisfisi_28 = value;
	}

	inline static int32_t get_offset_of__whayyee_29() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____whayyee_29)); }
	inline int32_t get__whayyee_29() const { return ____whayyee_29; }
	inline int32_t* get_address_of__whayyee_29() { return &____whayyee_29; }
	inline void set__whayyee_29(int32_t value)
	{
		____whayyee_29 = value;
	}

	inline static int32_t get_offset_of__bouval_30() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____bouval_30)); }
	inline String_t* get__bouval_30() const { return ____bouval_30; }
	inline String_t** get_address_of__bouval_30() { return &____bouval_30; }
	inline void set__bouval_30(String_t* value)
	{
		____bouval_30 = value;
		Il2CppCodeGenWriteBarrier(&____bouval_30, value);
	}

	inline static int32_t get_offset_of__gacur_31() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____gacur_31)); }
	inline float get__gacur_31() const { return ____gacur_31; }
	inline float* get_address_of__gacur_31() { return &____gacur_31; }
	inline void set__gacur_31(float value)
	{
		____gacur_31 = value;
	}

	inline static int32_t get_offset_of__whispall_32() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____whispall_32)); }
	inline float get__whispall_32() const { return ____whispall_32; }
	inline float* get_address_of__whispall_32() { return &____whispall_32; }
	inline void set__whispall_32(float value)
	{
		____whispall_32 = value;
	}

	inline static int32_t get_offset_of__kacu_33() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____kacu_33)); }
	inline String_t* get__kacu_33() const { return ____kacu_33; }
	inline String_t** get_address_of__kacu_33() { return &____kacu_33; }
	inline void set__kacu_33(String_t* value)
	{
		____kacu_33 = value;
		Il2CppCodeGenWriteBarrier(&____kacu_33, value);
	}

	inline static int32_t get_offset_of__pairpe_34() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____pairpe_34)); }
	inline uint32_t get__pairpe_34() const { return ____pairpe_34; }
	inline uint32_t* get_address_of__pairpe_34() { return &____pairpe_34; }
	inline void set__pairpe_34(uint32_t value)
	{
		____pairpe_34 = value;
	}

	inline static int32_t get_offset_of__maswaharPircowlal_35() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____maswaharPircowlal_35)); }
	inline bool get__maswaharPircowlal_35() const { return ____maswaharPircowlal_35; }
	inline bool* get_address_of__maswaharPircowlal_35() { return &____maswaharPircowlal_35; }
	inline void set__maswaharPircowlal_35(bool value)
	{
		____maswaharPircowlal_35 = value;
	}

	inline static int32_t get_offset_of__recaga_36() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____recaga_36)); }
	inline float get__recaga_36() const { return ____recaga_36; }
	inline float* get_address_of__recaga_36() { return &____recaga_36; }
	inline void set__recaga_36(float value)
	{
		____recaga_36 = value;
	}

	inline static int32_t get_offset_of__lefudeCisaw_37() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____lefudeCisaw_37)); }
	inline uint32_t get__lefudeCisaw_37() const { return ____lefudeCisaw_37; }
	inline uint32_t* get_address_of__lefudeCisaw_37() { return &____lefudeCisaw_37; }
	inline void set__lefudeCisaw_37(uint32_t value)
	{
		____lefudeCisaw_37 = value;
	}

	inline static int32_t get_offset_of__jemheeHounemgere_38() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____jemheeHounemgere_38)); }
	inline float get__jemheeHounemgere_38() const { return ____jemheeHounemgere_38; }
	inline float* get_address_of__jemheeHounemgere_38() { return &____jemheeHounemgere_38; }
	inline void set__jemheeHounemgere_38(float value)
	{
		____jemheeHounemgere_38 = value;
	}

	inline static int32_t get_offset_of__sisrisaMojeanis_39() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____sisrisaMojeanis_39)); }
	inline float get__sisrisaMojeanis_39() const { return ____sisrisaMojeanis_39; }
	inline float* get_address_of__sisrisaMojeanis_39() { return &____sisrisaMojeanis_39; }
	inline void set__sisrisaMojeanis_39(float value)
	{
		____sisrisaMojeanis_39 = value;
	}

	inline static int32_t get_offset_of__podall_40() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____podall_40)); }
	inline String_t* get__podall_40() const { return ____podall_40; }
	inline String_t** get_address_of__podall_40() { return &____podall_40; }
	inline void set__podall_40(String_t* value)
	{
		____podall_40 = value;
		Il2CppCodeGenWriteBarrier(&____podall_40, value);
	}

	inline static int32_t get_offset_of__baymacaiNaltis_41() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____baymacaiNaltis_41)); }
	inline bool get__baymacaiNaltis_41() const { return ____baymacaiNaltis_41; }
	inline bool* get_address_of__baymacaiNaltis_41() { return &____baymacaiNaltis_41; }
	inline void set__baymacaiNaltis_41(bool value)
	{
		____baymacaiNaltis_41 = value;
	}

	inline static int32_t get_offset_of__qismelTallmowsi_42() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____qismelTallmowsi_42)); }
	inline bool get__qismelTallmowsi_42() const { return ____qismelTallmowsi_42; }
	inline bool* get_address_of__qismelTallmowsi_42() { return &____qismelTallmowsi_42; }
	inline void set__qismelTallmowsi_42(bool value)
	{
		____qismelTallmowsi_42 = value;
	}

	inline static int32_t get_offset_of__cajurNerere_43() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____cajurNerere_43)); }
	inline uint32_t get__cajurNerere_43() const { return ____cajurNerere_43; }
	inline uint32_t* get_address_of__cajurNerere_43() { return &____cajurNerere_43; }
	inline void set__cajurNerere_43(uint32_t value)
	{
		____cajurNerere_43 = value;
	}

	inline static int32_t get_offset_of__cerhairlemYistem_44() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____cerhairlemYistem_44)); }
	inline float get__cerhairlemYistem_44() const { return ____cerhairlemYistem_44; }
	inline float* get_address_of__cerhairlemYistem_44() { return &____cerhairlemYistem_44; }
	inline void set__cerhairlemYistem_44(float value)
	{
		____cerhairlemYistem_44 = value;
	}

	inline static int32_t get_offset_of__pelxa_45() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____pelxa_45)); }
	inline String_t* get__pelxa_45() const { return ____pelxa_45; }
	inline String_t** get_address_of__pelxa_45() { return &____pelxa_45; }
	inline void set__pelxa_45(String_t* value)
	{
		____pelxa_45 = value;
		Il2CppCodeGenWriteBarrier(&____pelxa_45, value);
	}

	inline static int32_t get_offset_of__mamayWhasi_46() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____mamayWhasi_46)); }
	inline String_t* get__mamayWhasi_46() const { return ____mamayWhasi_46; }
	inline String_t** get_address_of__mamayWhasi_46() { return &____mamayWhasi_46; }
	inline void set__mamayWhasi_46(String_t* value)
	{
		____mamayWhasi_46 = value;
		Il2CppCodeGenWriteBarrier(&____mamayWhasi_46, value);
	}

	inline static int32_t get_offset_of__wapenaQoobi_47() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____wapenaQoobi_47)); }
	inline bool get__wapenaQoobi_47() const { return ____wapenaQoobi_47; }
	inline bool* get_address_of__wapenaQoobi_47() { return &____wapenaQoobi_47; }
	inline void set__wapenaQoobi_47(bool value)
	{
		____wapenaQoobi_47 = value;
	}

	inline static int32_t get_offset_of__kokeawouRahea_48() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____kokeawouRahea_48)); }
	inline int32_t get__kokeawouRahea_48() const { return ____kokeawouRahea_48; }
	inline int32_t* get_address_of__kokeawouRahea_48() { return &____kokeawouRahea_48; }
	inline void set__kokeawouRahea_48(int32_t value)
	{
		____kokeawouRahea_48 = value;
	}

	inline static int32_t get_offset_of__winow_49() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____winow_49)); }
	inline int32_t get__winow_49() const { return ____winow_49; }
	inline int32_t* get_address_of__winow_49() { return &____winow_49; }
	inline void set__winow_49(int32_t value)
	{
		____winow_49 = value;
	}

	inline static int32_t get_offset_of__gimerCecemmer_50() { return static_cast<int32_t>(offsetof(M_riheahiTealelfe207_t1391804857, ____gimerCecemmer_50)); }
	inline bool get__gimerCecemmer_50() const { return ____gimerCecemmer_50; }
	inline bool* get_address_of__gimerCecemmer_50() { return &____gimerCecemmer_50; }
	inline void set__gimerCecemmer_50(bool value)
	{
		____gimerCecemmer_50 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
