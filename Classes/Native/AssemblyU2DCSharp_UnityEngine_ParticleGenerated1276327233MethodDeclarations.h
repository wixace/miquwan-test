﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ParticleGenerated
struct UnityEngine_ParticleGenerated_t1276327233;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_ParticleGenerated::.ctor()
extern "C"  void UnityEngine_ParticleGenerated__ctor_m2858005834 (UnityEngine_ParticleGenerated_t1276327233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleGenerated::.cctor()
extern "C"  void UnityEngine_ParticleGenerated__cctor_m2216738723 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleGenerated::Particle_Particle1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleGenerated_Particle_Particle1_m2089090472 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleGenerated::Particle_position(JSVCall)
extern "C"  void UnityEngine_ParticleGenerated_Particle_position_m1563287613 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleGenerated::Particle_velocity(JSVCall)
extern "C"  void UnityEngine_ParticleGenerated_Particle_velocity_m3633653513 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleGenerated::Particle_energy(JSVCall)
extern "C"  void UnityEngine_ParticleGenerated_Particle_energy_m3505312190 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleGenerated::Particle_startEnergy(JSVCall)
extern "C"  void UnityEngine_ParticleGenerated_Particle_startEnergy_m3053113724 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleGenerated::Particle_size(JSVCall)
extern "C"  void UnityEngine_ParticleGenerated_Particle_size_m368946277 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleGenerated::Particle_rotation(JSVCall)
extern "C"  void UnityEngine_ParticleGenerated_Particle_rotation_m1231993224 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleGenerated::Particle_angularVelocity(JSVCall)
extern "C"  void UnityEngine_ParticleGenerated_Particle_angularVelocity_m3558983111 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleGenerated::Particle_color(JSVCall)
extern "C"  void UnityEngine_ParticleGenerated_Particle_color_m1298950083 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleGenerated::__Register()
extern "C"  void UnityEngine_ParticleGenerated___Register_m2790753725 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_ParticleGenerated_ilo_addJSCSRel1_m3598053126 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_ParticleGenerated::ilo_getVector3S2(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_ParticleGenerated_ilo_getVector3S2_m212847549 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleGenerated::ilo_changeJSObj3(System.Int32,System.Object)
extern "C"  void UnityEngine_ParticleGenerated_ilo_changeJSObj3_m1989123937 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleGenerated::ilo_setSingle4(System.Int32,System.Single)
extern "C"  void UnityEngine_ParticleGenerated_ilo_setSingle4_m3099540589 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_ParticleGenerated::ilo_getSingle5(System.Int32)
extern "C"  float UnityEngine_ParticleGenerated_ilo_getSingle5_m1196731145 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ParticleGenerated::ilo_setObject6(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_ParticleGenerated_ilo_setObject6_m1791634025 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_ParticleGenerated::ilo_getObject7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_ParticleGenerated_ilo_getObject7_m2953936417 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
