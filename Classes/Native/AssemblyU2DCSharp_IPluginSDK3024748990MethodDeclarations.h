﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPluginSDK
struct IPluginSDK_t3024748990;

#include "codegen/il2cpp-codegen.h"

// System.Void IPluginSDK::.ctor()
extern "C"  void IPluginSDK__ctor_m2998185117 (IPluginSDK_t3024748990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
