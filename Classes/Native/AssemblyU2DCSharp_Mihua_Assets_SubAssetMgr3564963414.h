﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Filelist`1<FileInfoCrc>
struct Filelist_1_t2494800866;
// System.Collections.Generic.List`1<FileInfoRes>
struct List_1_t2585245350;
// System.Collections.Generic.Dictionary`2<System.String,FileInfoRes>
struct Dictionary_2_t2037478168;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;
// System.Collections.Generic.List`1<Mihua.Net.UrlLoader>
struct List_1_t3858915048;
// System.Collections.Generic.Dictionary`2<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>
struct Dictionary_2_t3454399580;
// System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct List_1_t4002166762;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mihua.Assets.SubAssetMgr
struct  SubAssetMgr_t3564963414  : public Il2CppObject
{
public:
	// Filelist`1<FileInfoCrc> Mihua.Assets.SubAssetMgr::fileCrcList
	Filelist_1_t2494800866 * ___fileCrcList_6;
	// System.Collections.Generic.List`1<FileInfoRes> Mihua.Assets.SubAssetMgr::waitList
	List_1_t2585245350 * ___waitList_7;
	// System.Collections.Generic.Dictionary`2<System.String,FileInfoRes> Mihua.Assets.SubAssetMgr::downDic
	Dictionary_2_t2037478168 * ___downDic_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mihua.Assets.SubAssetMgr::assetDic
	Dictionary_2_t1974256870 * ___assetDic_9;
	// System.UInt32 Mihua.Assets.SubAssetMgr::totalBytes
	uint32_t ___totalBytes_10;
	// System.UInt32 Mihua.Assets.SubAssetMgr::curBytes
	uint32_t ___curBytes_11;
	// System.UInt32 Mihua.Assets.SubAssetMgr::loadedBytes
	uint32_t ___loadedBytes_12;
	// System.UInt32 Mihua.Assets.SubAssetMgr::loadingBytes
	uint32_t ___loadingBytes_13;
	// System.Collections.Generic.List`1<Mihua.Net.UrlLoader> Mihua.Assets.SubAssetMgr::m_WWWList
	List_1_t3858915048 * ___m_WWWList_14;
	// System.Collections.Generic.Dictionary`2<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo> Mihua.Assets.SubAssetMgr::m_errorDic
	Dictionary_2_t3454399580 * ___m_errorDic_15;
	// System.Collections.Generic.List`1<Mihua.Assets.SubAssetMgr/ErrorInfo> Mihua.Assets.SubAssetMgr::m_errorList
	List_1_t4002166762 * ___m_errorList_16;
	// System.Collections.Generic.List`1<System.String> Mihua.Assets.SubAssetMgr::m_loadingList
	List_1_t1375417109 * ___m_loadingList_17;
	// System.Int32 Mihua.Assets.SubAssetMgr::maxLoadingCount
	int32_t ___maxLoadingCount_18;
	// System.Int32 Mihua.Assets.SubAssetMgr::curLoadingCount
	int32_t ___curLoadingCount_19;
	// System.Boolean Mihua.Assets.SubAssetMgr::_isPause
	bool ____isPause_20;
	// System.Boolean Mihua.Assets.SubAssetMgr::<isForce>k__BackingField
	bool ___U3CisForceU3Ek__BackingField_21;
	// System.Boolean Mihua.Assets.SubAssetMgr::<isStart>k__BackingField
	bool ___U3CisStartU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_fileCrcList_6() { return static_cast<int32_t>(offsetof(SubAssetMgr_t3564963414, ___fileCrcList_6)); }
	inline Filelist_1_t2494800866 * get_fileCrcList_6() const { return ___fileCrcList_6; }
	inline Filelist_1_t2494800866 ** get_address_of_fileCrcList_6() { return &___fileCrcList_6; }
	inline void set_fileCrcList_6(Filelist_1_t2494800866 * value)
	{
		___fileCrcList_6 = value;
		Il2CppCodeGenWriteBarrier(&___fileCrcList_6, value);
	}

	inline static int32_t get_offset_of_waitList_7() { return static_cast<int32_t>(offsetof(SubAssetMgr_t3564963414, ___waitList_7)); }
	inline List_1_t2585245350 * get_waitList_7() const { return ___waitList_7; }
	inline List_1_t2585245350 ** get_address_of_waitList_7() { return &___waitList_7; }
	inline void set_waitList_7(List_1_t2585245350 * value)
	{
		___waitList_7 = value;
		Il2CppCodeGenWriteBarrier(&___waitList_7, value);
	}

	inline static int32_t get_offset_of_downDic_8() { return static_cast<int32_t>(offsetof(SubAssetMgr_t3564963414, ___downDic_8)); }
	inline Dictionary_2_t2037478168 * get_downDic_8() const { return ___downDic_8; }
	inline Dictionary_2_t2037478168 ** get_address_of_downDic_8() { return &___downDic_8; }
	inline void set_downDic_8(Dictionary_2_t2037478168 * value)
	{
		___downDic_8 = value;
		Il2CppCodeGenWriteBarrier(&___downDic_8, value);
	}

	inline static int32_t get_offset_of_assetDic_9() { return static_cast<int32_t>(offsetof(SubAssetMgr_t3564963414, ___assetDic_9)); }
	inline Dictionary_2_t1974256870 * get_assetDic_9() const { return ___assetDic_9; }
	inline Dictionary_2_t1974256870 ** get_address_of_assetDic_9() { return &___assetDic_9; }
	inline void set_assetDic_9(Dictionary_2_t1974256870 * value)
	{
		___assetDic_9 = value;
		Il2CppCodeGenWriteBarrier(&___assetDic_9, value);
	}

	inline static int32_t get_offset_of_totalBytes_10() { return static_cast<int32_t>(offsetof(SubAssetMgr_t3564963414, ___totalBytes_10)); }
	inline uint32_t get_totalBytes_10() const { return ___totalBytes_10; }
	inline uint32_t* get_address_of_totalBytes_10() { return &___totalBytes_10; }
	inline void set_totalBytes_10(uint32_t value)
	{
		___totalBytes_10 = value;
	}

	inline static int32_t get_offset_of_curBytes_11() { return static_cast<int32_t>(offsetof(SubAssetMgr_t3564963414, ___curBytes_11)); }
	inline uint32_t get_curBytes_11() const { return ___curBytes_11; }
	inline uint32_t* get_address_of_curBytes_11() { return &___curBytes_11; }
	inline void set_curBytes_11(uint32_t value)
	{
		___curBytes_11 = value;
	}

	inline static int32_t get_offset_of_loadedBytes_12() { return static_cast<int32_t>(offsetof(SubAssetMgr_t3564963414, ___loadedBytes_12)); }
	inline uint32_t get_loadedBytes_12() const { return ___loadedBytes_12; }
	inline uint32_t* get_address_of_loadedBytes_12() { return &___loadedBytes_12; }
	inline void set_loadedBytes_12(uint32_t value)
	{
		___loadedBytes_12 = value;
	}

	inline static int32_t get_offset_of_loadingBytes_13() { return static_cast<int32_t>(offsetof(SubAssetMgr_t3564963414, ___loadingBytes_13)); }
	inline uint32_t get_loadingBytes_13() const { return ___loadingBytes_13; }
	inline uint32_t* get_address_of_loadingBytes_13() { return &___loadingBytes_13; }
	inline void set_loadingBytes_13(uint32_t value)
	{
		___loadingBytes_13 = value;
	}

	inline static int32_t get_offset_of_m_WWWList_14() { return static_cast<int32_t>(offsetof(SubAssetMgr_t3564963414, ___m_WWWList_14)); }
	inline List_1_t3858915048 * get_m_WWWList_14() const { return ___m_WWWList_14; }
	inline List_1_t3858915048 ** get_address_of_m_WWWList_14() { return &___m_WWWList_14; }
	inline void set_m_WWWList_14(List_1_t3858915048 * value)
	{
		___m_WWWList_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_WWWList_14, value);
	}

	inline static int32_t get_offset_of_m_errorDic_15() { return static_cast<int32_t>(offsetof(SubAssetMgr_t3564963414, ___m_errorDic_15)); }
	inline Dictionary_2_t3454399580 * get_m_errorDic_15() const { return ___m_errorDic_15; }
	inline Dictionary_2_t3454399580 ** get_address_of_m_errorDic_15() { return &___m_errorDic_15; }
	inline void set_m_errorDic_15(Dictionary_2_t3454399580 * value)
	{
		___m_errorDic_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_errorDic_15, value);
	}

	inline static int32_t get_offset_of_m_errorList_16() { return static_cast<int32_t>(offsetof(SubAssetMgr_t3564963414, ___m_errorList_16)); }
	inline List_1_t4002166762 * get_m_errorList_16() const { return ___m_errorList_16; }
	inline List_1_t4002166762 ** get_address_of_m_errorList_16() { return &___m_errorList_16; }
	inline void set_m_errorList_16(List_1_t4002166762 * value)
	{
		___m_errorList_16 = value;
		Il2CppCodeGenWriteBarrier(&___m_errorList_16, value);
	}

	inline static int32_t get_offset_of_m_loadingList_17() { return static_cast<int32_t>(offsetof(SubAssetMgr_t3564963414, ___m_loadingList_17)); }
	inline List_1_t1375417109 * get_m_loadingList_17() const { return ___m_loadingList_17; }
	inline List_1_t1375417109 ** get_address_of_m_loadingList_17() { return &___m_loadingList_17; }
	inline void set_m_loadingList_17(List_1_t1375417109 * value)
	{
		___m_loadingList_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_loadingList_17, value);
	}

	inline static int32_t get_offset_of_maxLoadingCount_18() { return static_cast<int32_t>(offsetof(SubAssetMgr_t3564963414, ___maxLoadingCount_18)); }
	inline int32_t get_maxLoadingCount_18() const { return ___maxLoadingCount_18; }
	inline int32_t* get_address_of_maxLoadingCount_18() { return &___maxLoadingCount_18; }
	inline void set_maxLoadingCount_18(int32_t value)
	{
		___maxLoadingCount_18 = value;
	}

	inline static int32_t get_offset_of_curLoadingCount_19() { return static_cast<int32_t>(offsetof(SubAssetMgr_t3564963414, ___curLoadingCount_19)); }
	inline int32_t get_curLoadingCount_19() const { return ___curLoadingCount_19; }
	inline int32_t* get_address_of_curLoadingCount_19() { return &___curLoadingCount_19; }
	inline void set_curLoadingCount_19(int32_t value)
	{
		___curLoadingCount_19 = value;
	}

	inline static int32_t get_offset_of__isPause_20() { return static_cast<int32_t>(offsetof(SubAssetMgr_t3564963414, ____isPause_20)); }
	inline bool get__isPause_20() const { return ____isPause_20; }
	inline bool* get_address_of__isPause_20() { return &____isPause_20; }
	inline void set__isPause_20(bool value)
	{
		____isPause_20 = value;
	}

	inline static int32_t get_offset_of_U3CisForceU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(SubAssetMgr_t3564963414, ___U3CisForceU3Ek__BackingField_21)); }
	inline bool get_U3CisForceU3Ek__BackingField_21() const { return ___U3CisForceU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CisForceU3Ek__BackingField_21() { return &___U3CisForceU3Ek__BackingField_21; }
	inline void set_U3CisForceU3Ek__BackingField_21(bool value)
	{
		___U3CisForceU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CisStartU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(SubAssetMgr_t3564963414, ___U3CisStartU3Ek__BackingField_22)); }
	inline bool get_U3CisStartU3Ek__BackingField_22() const { return ___U3CisStartU3Ek__BackingField_22; }
	inline bool* get_address_of_U3CisStartU3Ek__BackingField_22() { return &___U3CisStartU3Ek__BackingField_22; }
	inline void set_U3CisStartU3Ek__BackingField_22(bool value)
	{
		___U3CisStartU3Ek__BackingField_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
