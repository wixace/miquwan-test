﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>
struct Dictionary_2_t3590202184;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En612558280.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23488982890.h"
#include "AssemblyU2DCSharp_SoundStatus3592938945.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,SoundStatus>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1661222131_gshared (Enumerator_t612558280 * __this, Dictionary_2_t3590202184 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1661222131(__this, ___dictionary0, method) ((  void (*) (Enumerator_t612558280 *, Dictionary_2_t3590202184 *, const MethodInfo*))Enumerator__ctor_m1661222131_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,SoundStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m761105048_gshared (Enumerator_t612558280 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m761105048(__this, method) ((  Il2CppObject * (*) (Enumerator_t612558280 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m761105048_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,SoundStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m440910306_gshared (Enumerator_t612558280 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m440910306(__this, method) ((  void (*) (Enumerator_t612558280 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m440910306_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,SoundStatus>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2546973401_gshared (Enumerator_t612558280 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2546973401(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t612558280 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2546973401_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,SoundStatus>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1171289460_gshared (Enumerator_t612558280 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1171289460(__this, method) ((  Il2CppObject * (*) (Enumerator_t612558280 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1171289460_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,SoundStatus>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1376175878_gshared (Enumerator_t612558280 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1376175878(__this, method) ((  Il2CppObject * (*) (Enumerator_t612558280 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1376175878_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,SoundStatus>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3802715922_gshared (Enumerator_t612558280 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3802715922(__this, method) ((  bool (*) (Enumerator_t612558280 *, const MethodInfo*))Enumerator_MoveNext_m3802715922_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,SoundStatus>::get_Current()
extern "C"  KeyValuePair_2_t3488982890  Enumerator_get_Current_m3594576362_gshared (Enumerator_t612558280 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3594576362(__this, method) ((  KeyValuePair_2_t3488982890  (*) (Enumerator_t612558280 *, const MethodInfo*))Enumerator_get_Current_m3594576362_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,SoundStatus>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m1344561051_gshared (Enumerator_t612558280 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1344561051(__this, method) ((  int32_t (*) (Enumerator_t612558280 *, const MethodInfo*))Enumerator_get_CurrentKey_m1344561051_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,SoundStatus>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m3305277083_gshared (Enumerator_t612558280 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m3305277083(__this, method) ((  int32_t (*) (Enumerator_t612558280 *, const MethodInfo*))Enumerator_get_CurrentValue_m3305277083_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,SoundStatus>::Reset()
extern "C"  void Enumerator_Reset_m3581614789_gshared (Enumerator_t612558280 * __this, const MethodInfo* method);
#define Enumerator_Reset_m3581614789(__this, method) ((  void (*) (Enumerator_t612558280 *, const MethodInfo*))Enumerator_Reset_m3581614789_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,SoundStatus>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3598615310_gshared (Enumerator_t612558280 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3598615310(__this, method) ((  void (*) (Enumerator_t612558280 *, const MethodInfo*))Enumerator_VerifyState_m3598615310_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,SoundStatus>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m3565511414_gshared (Enumerator_t612558280 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m3565511414(__this, method) ((  void (*) (Enumerator_t612558280 *, const MethodInfo*))Enumerator_VerifyCurrent_m3565511414_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,SoundStatus>::Dispose()
extern "C"  void Enumerator_Dispose_m4186437205_gshared (Enumerator_t612558280 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m4186437205(__this, method) ((  void (*) (Enumerator_t612558280 *, const MethodInfo*))Enumerator_Dispose_m4186437205_gshared)(__this, method)
