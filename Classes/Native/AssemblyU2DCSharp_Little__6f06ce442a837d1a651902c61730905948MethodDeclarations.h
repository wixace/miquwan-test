﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._6f06ce442a837d1a651902c60787dc47
struct _6f06ce442a837d1a651902c60787dc47_t1730905948;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__6f06ce442a837d1a651902c61730905948.h"

// System.Void Little._6f06ce442a837d1a651902c60787dc47::.ctor()
extern "C"  void _6f06ce442a837d1a651902c60787dc47__ctor_m3120834129 (_6f06ce442a837d1a651902c60787dc47_t1730905948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6f06ce442a837d1a651902c60787dc47::_6f06ce442a837d1a651902c60787dc47m2(System.Int32)
extern "C"  int32_t _6f06ce442a837d1a651902c60787dc47__6f06ce442a837d1a651902c60787dc47m2_m3304911897 (_6f06ce442a837d1a651902c60787dc47_t1730905948 * __this, int32_t ____6f06ce442a837d1a651902c60787dc47a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6f06ce442a837d1a651902c60787dc47::_6f06ce442a837d1a651902c60787dc47m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _6f06ce442a837d1a651902c60787dc47__6f06ce442a837d1a651902c60787dc47m_m603161277 (_6f06ce442a837d1a651902c60787dc47_t1730905948 * __this, int32_t ____6f06ce442a837d1a651902c60787dc47a0, int32_t ____6f06ce442a837d1a651902c60787dc4781, int32_t ____6f06ce442a837d1a651902c60787dc47c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6f06ce442a837d1a651902c60787dc47::ilo__6f06ce442a837d1a651902c60787dc47m21(Little._6f06ce442a837d1a651902c60787dc47,System.Int32)
extern "C"  int32_t _6f06ce442a837d1a651902c60787dc47_ilo__6f06ce442a837d1a651902c60787dc47m21_m1179200675 (Il2CppObject * __this /* static, unused */, _6f06ce442a837d1a651902c60787dc47_t1730905948 * ____this0, int32_t ____6f06ce442a837d1a651902c60787dc47a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
