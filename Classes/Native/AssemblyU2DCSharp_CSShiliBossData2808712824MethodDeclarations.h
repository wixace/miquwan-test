﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSShiliBossData
struct CSShiliBossData_t2808712824;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void CSShiliBossData::.ctor()
extern "C"  void CSShiliBossData__ctor_m2953510899 (CSShiliBossData_t2808712824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSShiliBossData::LoadData(System.String)
extern "C"  void CSShiliBossData_LoadData_m893872129 (CSShiliBossData_t2808712824 * __this, String_t* ___jsonStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSShiliBossData::UnLoadData()
extern "C"  void CSShiliBossData_UnLoadData_m2729576474 (CSShiliBossData_t2808712824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CSShiliBossData::ilo_DeserializeObject1(System.String)
extern "C"  Il2CppObject * CSShiliBossData_ilo_DeserializeObject1_m1235483168 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
