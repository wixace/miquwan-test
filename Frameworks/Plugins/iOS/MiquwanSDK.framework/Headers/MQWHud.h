//
//  MQWHud.h
//  MiquwanSDK
//
//  Created by MiQuWang on 2019/11/20.
//  Copyright © 2019 LQX. All rights reserved.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@interface MQWHud : NSObject

@property (nonatomic, strong)UIImage *hudImg;

/// 提示时长-默认1.5s
@property (nonatomic, assign)NSTimeInterval tipTime;

+ (void)show;

+ (void)showInfo:(NSString *)info;
+ (void)showAccount:(NSString *)account password:(NSString *)password;
+ (void)showAlertMsg:(NSString *)msg;
+ (void)showAlert:(nullable NSString *)tit body:(NSString *)body;
+ (void)showAlert:(nullable NSString *)tit body:(NSString *)body senderTit:(NSString *)senderTit;

+ (void)showWithMassge:(nullable NSString *)massge;
+ (void)dismiss ;

+ (void)hiddenBall:(BOOL)hidden;
@end

NS_ASSUME_NONNULL_END
