﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Exception
struct Exception_t3991598821;

#include "Pathfinding_Ionic_Zip_Reduced_Pathfinding_Ionic_Zi3652130261.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Ionic.Zip.ZipErrorEventArgs
struct  ZipErrorEventArgs_t2001130006  : public ZipProgressEventArgs_t3652130261
{
public:
	// System.Exception Pathfinding.Ionic.Zip.ZipErrorEventArgs::_exc
	Exception_t3991598821 * ____exc_8;

public:
	inline static int32_t get_offset_of__exc_8() { return static_cast<int32_t>(offsetof(ZipErrorEventArgs_t2001130006, ____exc_8)); }
	inline Exception_t3991598821 * get__exc_8() const { return ____exc_8; }
	inline Exception_t3991598821 ** get_address_of__exc_8() { return &____exc_8; }
	inline void set__exc_8(Exception_t3991598821 * value)
	{
		____exc_8 = value;
		Il2CppCodeGenWriteBarrier(&____exc_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
