﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_derepairBousoxel156
struct  M_derepairBousoxel156_t14639945  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_derepairBousoxel156::_drourair
	int32_t ____drourair_0;
	// System.Single GarbageiOS.M_derepairBousoxel156::_kirlaw
	float ____kirlaw_1;
	// System.Single GarbageiOS.M_derepairBousoxel156::_tiszeaNarresa
	float ____tiszeaNarresa_2;
	// System.Single GarbageiOS.M_derepairBousoxel156::_towwherfa
	float ____towwherfa_3;
	// System.String GarbageiOS.M_derepairBousoxel156::_chelcas
	String_t* ____chelcas_4;
	// System.Int32 GarbageiOS.M_derepairBousoxel156::_guja
	int32_t ____guja_5;
	// System.Int32 GarbageiOS.M_derepairBousoxel156::_stopu
	int32_t ____stopu_6;
	// System.Int32 GarbageiOS.M_derepairBousoxel156::_tallchacall
	int32_t ____tallchacall_7;
	// System.Int32 GarbageiOS.M_derepairBousoxel156::_talltowKairdaili
	int32_t ____talltowKairdaili_8;
	// System.Single GarbageiOS.M_derepairBousoxel156::_tirheQoubousee
	float ____tirheQoubousee_9;
	// System.UInt32 GarbageiOS.M_derepairBousoxel156::_kaywhooBayvidree
	uint32_t ____kaywhooBayvidree_10;
	// System.Int32 GarbageiOS.M_derepairBousoxel156::_lemeNesere
	int32_t ____lemeNesere_11;
	// System.UInt32 GarbageiOS.M_derepairBousoxel156::_hemraKerai
	uint32_t ____hemraKerai_12;
	// System.UInt32 GarbageiOS.M_derepairBousoxel156::_jeace
	uint32_t ____jeace_13;
	// System.Boolean GarbageiOS.M_derepairBousoxel156::_sumerseMearnerete
	bool ____sumerseMearnerete_14;
	// System.String GarbageiOS.M_derepairBousoxel156::_verejiKoowou
	String_t* ____verejiKoowou_15;
	// System.Single GarbageiOS.M_derepairBousoxel156::_tetisar
	float ____tetisar_16;
	// System.Boolean GarbageiOS.M_derepairBousoxel156::_cowceKairxirkoo
	bool ____cowceKairxirkoo_17;
	// System.Boolean GarbageiOS.M_derepairBousoxel156::_lanafaStavea
	bool ____lanafaStavea_18;
	// System.Single GarbageiOS.M_derepairBousoxel156::_morniroJowzay
	float ____morniroJowzay_19;
	// System.UInt32 GarbageiOS.M_derepairBousoxel156::_ferejerzuPalorro
	uint32_t ____ferejerzuPalorro_20;
	// System.Int32 GarbageiOS.M_derepairBousoxel156::_tercalaw
	int32_t ____tercalaw_21;
	// System.Boolean GarbageiOS.M_derepairBousoxel156::_whaseDemas
	bool ____whaseDemas_22;
	// System.Int32 GarbageiOS.M_derepairBousoxel156::_realor
	int32_t ____realor_23;
	// System.Int32 GarbageiOS.M_derepairBousoxel156::_jeparsa
	int32_t ____jeparsa_24;
	// System.UInt32 GarbageiOS.M_derepairBousoxel156::_sapooHoryu
	uint32_t ____sapooHoryu_25;
	// System.Boolean GarbageiOS.M_derepairBousoxel156::_bassawsa
	bool ____bassawsa_26;
	// System.UInt32 GarbageiOS.M_derepairBousoxel156::_tedoterePihu
	uint32_t ____tedoterePihu_27;
	// System.String GarbageiOS.M_derepairBousoxel156::_sidrarqea
	String_t* ____sidrarqea_28;
	// System.Int32 GarbageiOS.M_derepairBousoxel156::_jaymelSistaw
	int32_t ____jaymelSistaw_29;
	// System.UInt32 GarbageiOS.M_derepairBousoxel156::_topel
	uint32_t ____topel_30;
	// System.UInt32 GarbageiOS.M_derepairBousoxel156::_donerawVutall
	uint32_t ____donerawVutall_31;
	// System.String GarbageiOS.M_derepairBousoxel156::_whechereWhorsihar
	String_t* ____whechereWhorsihar_32;
	// System.UInt32 GarbageiOS.M_derepairBousoxel156::_semerRouca
	uint32_t ____semerRouca_33;
	// System.Int32 GarbageiOS.M_derepairBousoxel156::_jela
	int32_t ____jela_34;
	// System.Boolean GarbageiOS.M_derepairBousoxel156::_semade
	bool ____semade_35;
	// System.Boolean GarbageiOS.M_derepairBousoxel156::_katair
	bool ____katair_36;
	// System.UInt32 GarbageiOS.M_derepairBousoxel156::_nabeber
	uint32_t ____nabeber_37;
	// System.Boolean GarbageiOS.M_derepairBousoxel156::_distasCurairhow
	bool ____distasCurairhow_38;
	// System.String GarbageiOS.M_derepairBousoxel156::_sayheCahirtee
	String_t* ____sayheCahirtee_39;
	// System.Boolean GarbageiOS.M_derepairBousoxel156::_sika
	bool ____sika_40;
	// System.UInt32 GarbageiOS.M_derepairBousoxel156::_memtal
	uint32_t ____memtal_41;
	// System.Int32 GarbageiOS.M_derepairBousoxel156::_hawjutelWigoxe
	int32_t ____hawjutelWigoxe_42;
	// System.Int32 GarbageiOS.M_derepairBousoxel156::_qerji
	int32_t ____qerji_43;
	// System.String GarbageiOS.M_derepairBousoxel156::_qistawmai
	String_t* ____qistawmai_44;
	// System.Boolean GarbageiOS.M_derepairBousoxel156::_soorall
	bool ____soorall_45;
	// System.Single GarbageiOS.M_derepairBousoxel156::_dradraiNadaryi
	float ____dradraiNadaryi_46;
	// System.String GarbageiOS.M_derepairBousoxel156::_daijimearTayar
	String_t* ____daijimearTayar_47;
	// System.Int32 GarbageiOS.M_derepairBousoxel156::_midearea
	int32_t ____midearea_48;
	// System.String GarbageiOS.M_derepairBousoxel156::_kerneeSorwoo
	String_t* ____kerneeSorwoo_49;
	// System.Int32 GarbageiOS.M_derepairBousoxel156::_larjirou
	int32_t ____larjirou_50;
	// System.UInt32 GarbageiOS.M_derepairBousoxel156::_sapiMasheesea
	uint32_t ____sapiMasheesea_51;
	// System.UInt32 GarbageiOS.M_derepairBousoxel156::_liceepa
	uint32_t ____liceepa_52;
	// System.UInt32 GarbageiOS.M_derepairBousoxel156::_treremirpem
	uint32_t ____treremirpem_53;
	// System.Boolean GarbageiOS.M_derepairBousoxel156::_serair
	bool ____serair_54;
	// System.UInt32 GarbageiOS.M_derepairBousoxel156::_neepafiKairrecel
	uint32_t ____neepafiKairrecel_55;

public:
	inline static int32_t get_offset_of__drourair_0() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____drourair_0)); }
	inline int32_t get__drourair_0() const { return ____drourair_0; }
	inline int32_t* get_address_of__drourair_0() { return &____drourair_0; }
	inline void set__drourair_0(int32_t value)
	{
		____drourair_0 = value;
	}

	inline static int32_t get_offset_of__kirlaw_1() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____kirlaw_1)); }
	inline float get__kirlaw_1() const { return ____kirlaw_1; }
	inline float* get_address_of__kirlaw_1() { return &____kirlaw_1; }
	inline void set__kirlaw_1(float value)
	{
		____kirlaw_1 = value;
	}

	inline static int32_t get_offset_of__tiszeaNarresa_2() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____tiszeaNarresa_2)); }
	inline float get__tiszeaNarresa_2() const { return ____tiszeaNarresa_2; }
	inline float* get_address_of__tiszeaNarresa_2() { return &____tiszeaNarresa_2; }
	inline void set__tiszeaNarresa_2(float value)
	{
		____tiszeaNarresa_2 = value;
	}

	inline static int32_t get_offset_of__towwherfa_3() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____towwherfa_3)); }
	inline float get__towwherfa_3() const { return ____towwherfa_3; }
	inline float* get_address_of__towwherfa_3() { return &____towwherfa_3; }
	inline void set__towwherfa_3(float value)
	{
		____towwherfa_3 = value;
	}

	inline static int32_t get_offset_of__chelcas_4() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____chelcas_4)); }
	inline String_t* get__chelcas_4() const { return ____chelcas_4; }
	inline String_t** get_address_of__chelcas_4() { return &____chelcas_4; }
	inline void set__chelcas_4(String_t* value)
	{
		____chelcas_4 = value;
		Il2CppCodeGenWriteBarrier(&____chelcas_4, value);
	}

	inline static int32_t get_offset_of__guja_5() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____guja_5)); }
	inline int32_t get__guja_5() const { return ____guja_5; }
	inline int32_t* get_address_of__guja_5() { return &____guja_5; }
	inline void set__guja_5(int32_t value)
	{
		____guja_5 = value;
	}

	inline static int32_t get_offset_of__stopu_6() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____stopu_6)); }
	inline int32_t get__stopu_6() const { return ____stopu_6; }
	inline int32_t* get_address_of__stopu_6() { return &____stopu_6; }
	inline void set__stopu_6(int32_t value)
	{
		____stopu_6 = value;
	}

	inline static int32_t get_offset_of__tallchacall_7() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____tallchacall_7)); }
	inline int32_t get__tallchacall_7() const { return ____tallchacall_7; }
	inline int32_t* get_address_of__tallchacall_7() { return &____tallchacall_7; }
	inline void set__tallchacall_7(int32_t value)
	{
		____tallchacall_7 = value;
	}

	inline static int32_t get_offset_of__talltowKairdaili_8() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____talltowKairdaili_8)); }
	inline int32_t get__talltowKairdaili_8() const { return ____talltowKairdaili_8; }
	inline int32_t* get_address_of__talltowKairdaili_8() { return &____talltowKairdaili_8; }
	inline void set__talltowKairdaili_8(int32_t value)
	{
		____talltowKairdaili_8 = value;
	}

	inline static int32_t get_offset_of__tirheQoubousee_9() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____tirheQoubousee_9)); }
	inline float get__tirheQoubousee_9() const { return ____tirheQoubousee_9; }
	inline float* get_address_of__tirheQoubousee_9() { return &____tirheQoubousee_9; }
	inline void set__tirheQoubousee_9(float value)
	{
		____tirheQoubousee_9 = value;
	}

	inline static int32_t get_offset_of__kaywhooBayvidree_10() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____kaywhooBayvidree_10)); }
	inline uint32_t get__kaywhooBayvidree_10() const { return ____kaywhooBayvidree_10; }
	inline uint32_t* get_address_of__kaywhooBayvidree_10() { return &____kaywhooBayvidree_10; }
	inline void set__kaywhooBayvidree_10(uint32_t value)
	{
		____kaywhooBayvidree_10 = value;
	}

	inline static int32_t get_offset_of__lemeNesere_11() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____lemeNesere_11)); }
	inline int32_t get__lemeNesere_11() const { return ____lemeNesere_11; }
	inline int32_t* get_address_of__lemeNesere_11() { return &____lemeNesere_11; }
	inline void set__lemeNesere_11(int32_t value)
	{
		____lemeNesere_11 = value;
	}

	inline static int32_t get_offset_of__hemraKerai_12() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____hemraKerai_12)); }
	inline uint32_t get__hemraKerai_12() const { return ____hemraKerai_12; }
	inline uint32_t* get_address_of__hemraKerai_12() { return &____hemraKerai_12; }
	inline void set__hemraKerai_12(uint32_t value)
	{
		____hemraKerai_12 = value;
	}

	inline static int32_t get_offset_of__jeace_13() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____jeace_13)); }
	inline uint32_t get__jeace_13() const { return ____jeace_13; }
	inline uint32_t* get_address_of__jeace_13() { return &____jeace_13; }
	inline void set__jeace_13(uint32_t value)
	{
		____jeace_13 = value;
	}

	inline static int32_t get_offset_of__sumerseMearnerete_14() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____sumerseMearnerete_14)); }
	inline bool get__sumerseMearnerete_14() const { return ____sumerseMearnerete_14; }
	inline bool* get_address_of__sumerseMearnerete_14() { return &____sumerseMearnerete_14; }
	inline void set__sumerseMearnerete_14(bool value)
	{
		____sumerseMearnerete_14 = value;
	}

	inline static int32_t get_offset_of__verejiKoowou_15() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____verejiKoowou_15)); }
	inline String_t* get__verejiKoowou_15() const { return ____verejiKoowou_15; }
	inline String_t** get_address_of__verejiKoowou_15() { return &____verejiKoowou_15; }
	inline void set__verejiKoowou_15(String_t* value)
	{
		____verejiKoowou_15 = value;
		Il2CppCodeGenWriteBarrier(&____verejiKoowou_15, value);
	}

	inline static int32_t get_offset_of__tetisar_16() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____tetisar_16)); }
	inline float get__tetisar_16() const { return ____tetisar_16; }
	inline float* get_address_of__tetisar_16() { return &____tetisar_16; }
	inline void set__tetisar_16(float value)
	{
		____tetisar_16 = value;
	}

	inline static int32_t get_offset_of__cowceKairxirkoo_17() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____cowceKairxirkoo_17)); }
	inline bool get__cowceKairxirkoo_17() const { return ____cowceKairxirkoo_17; }
	inline bool* get_address_of__cowceKairxirkoo_17() { return &____cowceKairxirkoo_17; }
	inline void set__cowceKairxirkoo_17(bool value)
	{
		____cowceKairxirkoo_17 = value;
	}

	inline static int32_t get_offset_of__lanafaStavea_18() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____lanafaStavea_18)); }
	inline bool get__lanafaStavea_18() const { return ____lanafaStavea_18; }
	inline bool* get_address_of__lanafaStavea_18() { return &____lanafaStavea_18; }
	inline void set__lanafaStavea_18(bool value)
	{
		____lanafaStavea_18 = value;
	}

	inline static int32_t get_offset_of__morniroJowzay_19() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____morniroJowzay_19)); }
	inline float get__morniroJowzay_19() const { return ____morniroJowzay_19; }
	inline float* get_address_of__morniroJowzay_19() { return &____morniroJowzay_19; }
	inline void set__morniroJowzay_19(float value)
	{
		____morniroJowzay_19 = value;
	}

	inline static int32_t get_offset_of__ferejerzuPalorro_20() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____ferejerzuPalorro_20)); }
	inline uint32_t get__ferejerzuPalorro_20() const { return ____ferejerzuPalorro_20; }
	inline uint32_t* get_address_of__ferejerzuPalorro_20() { return &____ferejerzuPalorro_20; }
	inline void set__ferejerzuPalorro_20(uint32_t value)
	{
		____ferejerzuPalorro_20 = value;
	}

	inline static int32_t get_offset_of__tercalaw_21() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____tercalaw_21)); }
	inline int32_t get__tercalaw_21() const { return ____tercalaw_21; }
	inline int32_t* get_address_of__tercalaw_21() { return &____tercalaw_21; }
	inline void set__tercalaw_21(int32_t value)
	{
		____tercalaw_21 = value;
	}

	inline static int32_t get_offset_of__whaseDemas_22() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____whaseDemas_22)); }
	inline bool get__whaseDemas_22() const { return ____whaseDemas_22; }
	inline bool* get_address_of__whaseDemas_22() { return &____whaseDemas_22; }
	inline void set__whaseDemas_22(bool value)
	{
		____whaseDemas_22 = value;
	}

	inline static int32_t get_offset_of__realor_23() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____realor_23)); }
	inline int32_t get__realor_23() const { return ____realor_23; }
	inline int32_t* get_address_of__realor_23() { return &____realor_23; }
	inline void set__realor_23(int32_t value)
	{
		____realor_23 = value;
	}

	inline static int32_t get_offset_of__jeparsa_24() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____jeparsa_24)); }
	inline int32_t get__jeparsa_24() const { return ____jeparsa_24; }
	inline int32_t* get_address_of__jeparsa_24() { return &____jeparsa_24; }
	inline void set__jeparsa_24(int32_t value)
	{
		____jeparsa_24 = value;
	}

	inline static int32_t get_offset_of__sapooHoryu_25() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____sapooHoryu_25)); }
	inline uint32_t get__sapooHoryu_25() const { return ____sapooHoryu_25; }
	inline uint32_t* get_address_of__sapooHoryu_25() { return &____sapooHoryu_25; }
	inline void set__sapooHoryu_25(uint32_t value)
	{
		____sapooHoryu_25 = value;
	}

	inline static int32_t get_offset_of__bassawsa_26() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____bassawsa_26)); }
	inline bool get__bassawsa_26() const { return ____bassawsa_26; }
	inline bool* get_address_of__bassawsa_26() { return &____bassawsa_26; }
	inline void set__bassawsa_26(bool value)
	{
		____bassawsa_26 = value;
	}

	inline static int32_t get_offset_of__tedoterePihu_27() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____tedoterePihu_27)); }
	inline uint32_t get__tedoterePihu_27() const { return ____tedoterePihu_27; }
	inline uint32_t* get_address_of__tedoterePihu_27() { return &____tedoterePihu_27; }
	inline void set__tedoterePihu_27(uint32_t value)
	{
		____tedoterePihu_27 = value;
	}

	inline static int32_t get_offset_of__sidrarqea_28() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____sidrarqea_28)); }
	inline String_t* get__sidrarqea_28() const { return ____sidrarqea_28; }
	inline String_t** get_address_of__sidrarqea_28() { return &____sidrarqea_28; }
	inline void set__sidrarqea_28(String_t* value)
	{
		____sidrarqea_28 = value;
		Il2CppCodeGenWriteBarrier(&____sidrarqea_28, value);
	}

	inline static int32_t get_offset_of__jaymelSistaw_29() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____jaymelSistaw_29)); }
	inline int32_t get__jaymelSistaw_29() const { return ____jaymelSistaw_29; }
	inline int32_t* get_address_of__jaymelSistaw_29() { return &____jaymelSistaw_29; }
	inline void set__jaymelSistaw_29(int32_t value)
	{
		____jaymelSistaw_29 = value;
	}

	inline static int32_t get_offset_of__topel_30() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____topel_30)); }
	inline uint32_t get__topel_30() const { return ____topel_30; }
	inline uint32_t* get_address_of__topel_30() { return &____topel_30; }
	inline void set__topel_30(uint32_t value)
	{
		____topel_30 = value;
	}

	inline static int32_t get_offset_of__donerawVutall_31() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____donerawVutall_31)); }
	inline uint32_t get__donerawVutall_31() const { return ____donerawVutall_31; }
	inline uint32_t* get_address_of__donerawVutall_31() { return &____donerawVutall_31; }
	inline void set__donerawVutall_31(uint32_t value)
	{
		____donerawVutall_31 = value;
	}

	inline static int32_t get_offset_of__whechereWhorsihar_32() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____whechereWhorsihar_32)); }
	inline String_t* get__whechereWhorsihar_32() const { return ____whechereWhorsihar_32; }
	inline String_t** get_address_of__whechereWhorsihar_32() { return &____whechereWhorsihar_32; }
	inline void set__whechereWhorsihar_32(String_t* value)
	{
		____whechereWhorsihar_32 = value;
		Il2CppCodeGenWriteBarrier(&____whechereWhorsihar_32, value);
	}

	inline static int32_t get_offset_of__semerRouca_33() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____semerRouca_33)); }
	inline uint32_t get__semerRouca_33() const { return ____semerRouca_33; }
	inline uint32_t* get_address_of__semerRouca_33() { return &____semerRouca_33; }
	inline void set__semerRouca_33(uint32_t value)
	{
		____semerRouca_33 = value;
	}

	inline static int32_t get_offset_of__jela_34() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____jela_34)); }
	inline int32_t get__jela_34() const { return ____jela_34; }
	inline int32_t* get_address_of__jela_34() { return &____jela_34; }
	inline void set__jela_34(int32_t value)
	{
		____jela_34 = value;
	}

	inline static int32_t get_offset_of__semade_35() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____semade_35)); }
	inline bool get__semade_35() const { return ____semade_35; }
	inline bool* get_address_of__semade_35() { return &____semade_35; }
	inline void set__semade_35(bool value)
	{
		____semade_35 = value;
	}

	inline static int32_t get_offset_of__katair_36() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____katair_36)); }
	inline bool get__katair_36() const { return ____katair_36; }
	inline bool* get_address_of__katair_36() { return &____katair_36; }
	inline void set__katair_36(bool value)
	{
		____katair_36 = value;
	}

	inline static int32_t get_offset_of__nabeber_37() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____nabeber_37)); }
	inline uint32_t get__nabeber_37() const { return ____nabeber_37; }
	inline uint32_t* get_address_of__nabeber_37() { return &____nabeber_37; }
	inline void set__nabeber_37(uint32_t value)
	{
		____nabeber_37 = value;
	}

	inline static int32_t get_offset_of__distasCurairhow_38() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____distasCurairhow_38)); }
	inline bool get__distasCurairhow_38() const { return ____distasCurairhow_38; }
	inline bool* get_address_of__distasCurairhow_38() { return &____distasCurairhow_38; }
	inline void set__distasCurairhow_38(bool value)
	{
		____distasCurairhow_38 = value;
	}

	inline static int32_t get_offset_of__sayheCahirtee_39() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____sayheCahirtee_39)); }
	inline String_t* get__sayheCahirtee_39() const { return ____sayheCahirtee_39; }
	inline String_t** get_address_of__sayheCahirtee_39() { return &____sayheCahirtee_39; }
	inline void set__sayheCahirtee_39(String_t* value)
	{
		____sayheCahirtee_39 = value;
		Il2CppCodeGenWriteBarrier(&____sayheCahirtee_39, value);
	}

	inline static int32_t get_offset_of__sika_40() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____sika_40)); }
	inline bool get__sika_40() const { return ____sika_40; }
	inline bool* get_address_of__sika_40() { return &____sika_40; }
	inline void set__sika_40(bool value)
	{
		____sika_40 = value;
	}

	inline static int32_t get_offset_of__memtal_41() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____memtal_41)); }
	inline uint32_t get__memtal_41() const { return ____memtal_41; }
	inline uint32_t* get_address_of__memtal_41() { return &____memtal_41; }
	inline void set__memtal_41(uint32_t value)
	{
		____memtal_41 = value;
	}

	inline static int32_t get_offset_of__hawjutelWigoxe_42() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____hawjutelWigoxe_42)); }
	inline int32_t get__hawjutelWigoxe_42() const { return ____hawjutelWigoxe_42; }
	inline int32_t* get_address_of__hawjutelWigoxe_42() { return &____hawjutelWigoxe_42; }
	inline void set__hawjutelWigoxe_42(int32_t value)
	{
		____hawjutelWigoxe_42 = value;
	}

	inline static int32_t get_offset_of__qerji_43() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____qerji_43)); }
	inline int32_t get__qerji_43() const { return ____qerji_43; }
	inline int32_t* get_address_of__qerji_43() { return &____qerji_43; }
	inline void set__qerji_43(int32_t value)
	{
		____qerji_43 = value;
	}

	inline static int32_t get_offset_of__qistawmai_44() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____qistawmai_44)); }
	inline String_t* get__qistawmai_44() const { return ____qistawmai_44; }
	inline String_t** get_address_of__qistawmai_44() { return &____qistawmai_44; }
	inline void set__qistawmai_44(String_t* value)
	{
		____qistawmai_44 = value;
		Il2CppCodeGenWriteBarrier(&____qistawmai_44, value);
	}

	inline static int32_t get_offset_of__soorall_45() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____soorall_45)); }
	inline bool get__soorall_45() const { return ____soorall_45; }
	inline bool* get_address_of__soorall_45() { return &____soorall_45; }
	inline void set__soorall_45(bool value)
	{
		____soorall_45 = value;
	}

	inline static int32_t get_offset_of__dradraiNadaryi_46() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____dradraiNadaryi_46)); }
	inline float get__dradraiNadaryi_46() const { return ____dradraiNadaryi_46; }
	inline float* get_address_of__dradraiNadaryi_46() { return &____dradraiNadaryi_46; }
	inline void set__dradraiNadaryi_46(float value)
	{
		____dradraiNadaryi_46 = value;
	}

	inline static int32_t get_offset_of__daijimearTayar_47() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____daijimearTayar_47)); }
	inline String_t* get__daijimearTayar_47() const { return ____daijimearTayar_47; }
	inline String_t** get_address_of__daijimearTayar_47() { return &____daijimearTayar_47; }
	inline void set__daijimearTayar_47(String_t* value)
	{
		____daijimearTayar_47 = value;
		Il2CppCodeGenWriteBarrier(&____daijimearTayar_47, value);
	}

	inline static int32_t get_offset_of__midearea_48() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____midearea_48)); }
	inline int32_t get__midearea_48() const { return ____midearea_48; }
	inline int32_t* get_address_of__midearea_48() { return &____midearea_48; }
	inline void set__midearea_48(int32_t value)
	{
		____midearea_48 = value;
	}

	inline static int32_t get_offset_of__kerneeSorwoo_49() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____kerneeSorwoo_49)); }
	inline String_t* get__kerneeSorwoo_49() const { return ____kerneeSorwoo_49; }
	inline String_t** get_address_of__kerneeSorwoo_49() { return &____kerneeSorwoo_49; }
	inline void set__kerneeSorwoo_49(String_t* value)
	{
		____kerneeSorwoo_49 = value;
		Il2CppCodeGenWriteBarrier(&____kerneeSorwoo_49, value);
	}

	inline static int32_t get_offset_of__larjirou_50() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____larjirou_50)); }
	inline int32_t get__larjirou_50() const { return ____larjirou_50; }
	inline int32_t* get_address_of__larjirou_50() { return &____larjirou_50; }
	inline void set__larjirou_50(int32_t value)
	{
		____larjirou_50 = value;
	}

	inline static int32_t get_offset_of__sapiMasheesea_51() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____sapiMasheesea_51)); }
	inline uint32_t get__sapiMasheesea_51() const { return ____sapiMasheesea_51; }
	inline uint32_t* get_address_of__sapiMasheesea_51() { return &____sapiMasheesea_51; }
	inline void set__sapiMasheesea_51(uint32_t value)
	{
		____sapiMasheesea_51 = value;
	}

	inline static int32_t get_offset_of__liceepa_52() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____liceepa_52)); }
	inline uint32_t get__liceepa_52() const { return ____liceepa_52; }
	inline uint32_t* get_address_of__liceepa_52() { return &____liceepa_52; }
	inline void set__liceepa_52(uint32_t value)
	{
		____liceepa_52 = value;
	}

	inline static int32_t get_offset_of__treremirpem_53() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____treremirpem_53)); }
	inline uint32_t get__treremirpem_53() const { return ____treremirpem_53; }
	inline uint32_t* get_address_of__treremirpem_53() { return &____treremirpem_53; }
	inline void set__treremirpem_53(uint32_t value)
	{
		____treremirpem_53 = value;
	}

	inline static int32_t get_offset_of__serair_54() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____serair_54)); }
	inline bool get__serair_54() const { return ____serair_54; }
	inline bool* get_address_of__serair_54() { return &____serair_54; }
	inline void set__serair_54(bool value)
	{
		____serair_54 = value;
	}

	inline static int32_t get_offset_of__neepafiKairrecel_55() { return static_cast<int32_t>(offsetof(M_derepairBousoxel156_t14639945, ____neepafiKairrecel_55)); }
	inline uint32_t get__neepafiKairrecel_55() const { return ____neepafiKairrecel_55; }
	inline uint32_t* get_address_of__neepafiKairrecel_55() { return &____neepafiKairrecel_55; }
	inline void set__neepafiKairrecel_55(uint32_t value)
	{
		____neepafiKairrecel_55 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
