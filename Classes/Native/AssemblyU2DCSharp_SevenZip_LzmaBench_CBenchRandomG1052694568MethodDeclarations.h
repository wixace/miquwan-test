﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.LzmaBench/CBenchRandomGenerator
struct CBenchRandomGenerator_t1052694568;

#include "codegen/il2cpp-codegen.h"

// System.Void SevenZip.LzmaBench/CBenchRandomGenerator::.ctor()
extern "C"  void CBenchRandomGenerator__ctor_m407909235 (CBenchRandomGenerator_t1052694568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench/CBenchRandomGenerator::Set(System.UInt32)
extern "C"  void CBenchRandomGenerator_Set_m2607586151 (CBenchRandomGenerator_t1052694568 * __this, uint32_t ___bufferSize0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.LzmaBench/CBenchRandomGenerator::GetRndBit()
extern "C"  uint32_t CBenchRandomGenerator_GetRndBit_m2924308945 (CBenchRandomGenerator_t1052694568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.LzmaBench/CBenchRandomGenerator::GetLogRandBits(System.Int32)
extern "C"  uint32_t CBenchRandomGenerator_GetLogRandBits_m1629317206 (CBenchRandomGenerator_t1052694568 * __this, int32_t ___numBits0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.LzmaBench/CBenchRandomGenerator::GetOffset()
extern "C"  uint32_t CBenchRandomGenerator_GetOffset_m3583142047 (CBenchRandomGenerator_t1052694568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.LzmaBench/CBenchRandomGenerator::GetLen1()
extern "C"  uint32_t CBenchRandomGenerator_GetLen1_m3796415816 (CBenchRandomGenerator_t1052694568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.LzmaBench/CBenchRandomGenerator::GetLen2()
extern "C"  uint32_t CBenchRandomGenerator_GetLen2_m3796416777 (CBenchRandomGenerator_t1052694568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench/CBenchRandomGenerator::Generate()
extern "C"  void CBenchRandomGenerator_Generate_m3821638566 (CBenchRandomGenerator_t1052694568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
