﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2759590984MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m729709837(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2124762191 *, Dictionary_2_t807438799 *, const MethodInfo*))Enumerator__ctor_m753268849_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1211742964(__this, method) ((  Il2CppObject * (*) (Enumerator_t2124762191 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m67213840_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2623038856(__this, method) ((  void (*) (Enumerator_t2124762191 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2131366820_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1047876945(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2124762191 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2985981037_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4186304720(__this, method) ((  Il2CppObject * (*) (Enumerator_t2124762191 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3671657196_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3997883234(__this, method) ((  Il2CppObject * (*) (Enumerator_t2124762191 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3342851710_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::MoveNext()
#define Enumerator_MoveNext_m3695985652(__this, method) ((  bool (*) (Enumerator_t2124762191 *, const MethodInfo*))Enumerator_MoveNext_m56045584_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::get_Current()
#define Enumerator_get_Current_m1841146428(__this, method) ((  KeyValuePair_2_t706219505  (*) (Enumerator_t2124762191 *, const MethodInfo*))Enumerator_get_Current_m2618191008_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m111332161(__this, method) ((  int32_t (*) (Enumerator_t2124762191 *, const MethodInfo*))Enumerator_get_CurrentKey_m3308353117_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3542051365(__this, method) ((  OnMoveCallBackFun_t3535987578 * (*) (Enumerator_t2124762191 *, const MethodInfo*))Enumerator_get_CurrentValue_m1303323201_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::Reset()
#define Enumerator_Reset_m1589236703(__this, method) ((  void (*) (Enumerator_t2124762191 *, const MethodInfo*))Enumerator_Reset_m1707899203_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::VerifyState()
#define Enumerator_VerifyState_m3111157928(__this, method) ((  void (*) (Enumerator_t2124762191 *, const MethodInfo*))Enumerator_VerifyState_m3106048780_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3270402576(__this, method) ((  void (*) (Enumerator_t2124762191 *, const MethodInfo*))Enumerator_VerifyCurrent_m2655478644_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::Dispose()
#define Enumerator_Dispose_m771543279(__this, method) ((  void (*) (Enumerator_t2124762191 *, const MethodInfo*))Enumerator_Dispose_m3137056083_gshared)(__this, method)
