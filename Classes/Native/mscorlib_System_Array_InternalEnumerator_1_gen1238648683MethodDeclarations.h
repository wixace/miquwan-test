﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1238648683.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_TreeInstance2456306007.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.TreeInstance>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1374864539_gshared (InternalEnumerator_1_t1238648683 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1374864539(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1238648683 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1374864539_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TreeInstance>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m138001701_gshared (InternalEnumerator_1_t1238648683 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m138001701(__this, method) ((  void (*) (InternalEnumerator_1_t1238648683 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m138001701_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TreeInstance>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m215469713_gshared (InternalEnumerator_1_t1238648683 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m215469713(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1238648683 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m215469713_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TreeInstance>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m426066994_gshared (InternalEnumerator_1_t1238648683 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m426066994(__this, method) ((  void (*) (InternalEnumerator_1_t1238648683 *, const MethodInfo*))InternalEnumerator_1_Dispose_m426066994_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TreeInstance>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1328973457_gshared (InternalEnumerator_1_t1238648683 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1328973457(__this, method) ((  bool (*) (InternalEnumerator_1_t1238648683 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1328973457_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.TreeInstance>::get_Current()
extern "C"  TreeInstance_t2456306007  InternalEnumerator_1_get_Current_m2828677730_gshared (InternalEnumerator_1_t1238648683 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2828677730(__this, method) ((  TreeInstance_t2456306007  (*) (InternalEnumerator_1_t1238648683 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2828677730_gshared)(__this, method)
