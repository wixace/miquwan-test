﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_lerelaimuSosay145
struct M_lerelaimuSosay145_t2035771315;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_lerelaimuSosay1452035771315.h"

// System.Void GarbageiOS.M_lerelaimuSosay145::.ctor()
extern "C"  void M_lerelaimuSosay145__ctor_m1760172816 (M_lerelaimuSosay145_t2035771315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lerelaimuSosay145::M_mairteargallGairyis0(System.String[],System.Int32)
extern "C"  void M_lerelaimuSosay145_M_mairteargallGairyis0_m340226630 (M_lerelaimuSosay145_t2035771315 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lerelaimuSosay145::M_toopearre1(System.String[],System.Int32)
extern "C"  void M_lerelaimuSosay145_M_toopearre1_m3079300321 (M_lerelaimuSosay145_t2035771315 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lerelaimuSosay145::ilo_M_mairteargallGairyis01(GarbageiOS.M_lerelaimuSosay145,System.String[],System.Int32)
extern "C"  void M_lerelaimuSosay145_ilo_M_mairteargallGairyis01_m2181002295 (Il2CppObject * __this /* static, unused */, M_lerelaimuSosay145_t2035771315 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
