﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._92e15e4e19374c12873356e869ae0f93
struct _92e15e4e19374c12873356e869ae0f93_t398374821;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._92e15e4e19374c12873356e869ae0f93::.ctor()
extern "C"  void _92e15e4e19374c12873356e869ae0f93__ctor_m1828206632 (_92e15e4e19374c12873356e869ae0f93_t398374821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._92e15e4e19374c12873356e869ae0f93::_92e15e4e19374c12873356e869ae0f93m2(System.Int32)
extern "C"  int32_t _92e15e4e19374c12873356e869ae0f93__92e15e4e19374c12873356e869ae0f93m2_m1843611513 (_92e15e4e19374c12873356e869ae0f93_t398374821 * __this, int32_t ____92e15e4e19374c12873356e869ae0f93a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._92e15e4e19374c12873356e869ae0f93::_92e15e4e19374c12873356e869ae0f93m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _92e15e4e19374c12873356e869ae0f93__92e15e4e19374c12873356e869ae0f93m_m1363131229 (_92e15e4e19374c12873356e869ae0f93_t398374821 * __this, int32_t ____92e15e4e19374c12873356e869ae0f93a0, int32_t ____92e15e4e19374c12873356e869ae0f93651, int32_t ____92e15e4e19374c12873356e869ae0f93c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
