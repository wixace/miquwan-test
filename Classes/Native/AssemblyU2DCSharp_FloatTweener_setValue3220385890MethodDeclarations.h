﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FloatTweener/setValue
struct setValue_t3220385890;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void FloatTweener/setValue::.ctor(System.Object,System.IntPtr)
extern "C"  void setValue__ctor_m1327415097 (setValue_t3220385890 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTweener/setValue::Invoke(System.Single)
extern "C"  void setValue_Invoke_m412659640 (setValue_t3220385890 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult FloatTweener/setValue::BeginInvoke(System.Single,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * setValue_BeginInvoke_m2665466067 (setValue_t3220385890 * __this, float ___value0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTweener/setValue::EndInvoke(System.IAsyncResult)
extern "C"  void setValue_EndInvoke_m1235006921 (setValue_t3220385890 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
