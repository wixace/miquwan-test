﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayMove
struct ReplayMove_t780044408;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;
// ReplayBase
struct ReplayBase_t779703160;
// CombatEntity
struct CombatEntity_t684137495;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ReplayBase779703160.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "System_Core_System_Action3771233898.h"

// System.Void ReplayMove::.ctor()
extern "C"  void ReplayMove__ctor_m1637029667 (ReplayMove_t780044408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMove::.ctor(System.Object[])
extern "C"  void ReplayMove__ctor_m1323487791 (ReplayMove_t780044408 * __this, ObjectU5BU5D_t1108656482* ___datas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] ReplayMove::ParserJsonStr(System.String)
extern "C"  StringU5BU5D_t4054002952* ReplayMove_ParserJsonStr_m1686584152 (ReplayMove_t780044408 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayMove::GetJsonStr()
extern "C"  String_t* ReplayMove_GetJsonStr_m2506076695 (ReplayMove_t780044408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMove::Execute()
extern "C"  void ReplayMove_Execute_m4078869110 (ReplayMove_t780044408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMove::ilo_Execute1(ReplayBase)
extern "C"  void ReplayMove_ilo_Execute1_m281675608 (Il2CppObject * __this /* static, unused */, ReplayBase_t779703160 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReplayMove::ilo_get_isDeath2(CombatEntity)
extern "C"  bool ReplayMove_ilo_get_isDeath2_m2498112930 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity ReplayMove::ilo_get_Entity3(ReplayBase)
extern "C"  CombatEntity_t684137495 * ReplayMove_ilo_get_Entity3_m2277598391 (Il2CppObject * __this /* static, unused */, ReplayBase_t779703160 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayMove::ilo_UpdateTargetPos4(CombatEntity,UnityEngine.Vector3,System.Action)
extern "C"  void ReplayMove_ilo_UpdateTargetPos4_m3358569089 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, Vector3_t4282066566  ___targetPos1, Action_t3771233898 * ___OnTargetCall2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
