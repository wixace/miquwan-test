﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Int32>
struct U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Int32>::.ctor()
extern "C"  void U3CDelayCallExecU3Ec__Iterator3F_1__ctor_m2000337995_gshared (U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator3F_1__ctor_m2000337995(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator3F_1__ctor_m2000337995_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Int32>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator3F_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2064039473_gshared (U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator3F_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2064039473(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator3F_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2064039473_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator3F_1_System_Collections_IEnumerator_get_Current_m1446331333_gshared (U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator3F_1_System_Collections_IEnumerator_get_Current_m1446331333(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator3F_1_System_Collections_IEnumerator_get_Current_m1446331333_gshared)(__this, method)
// System.Boolean BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Int32>::MoveNext()
extern "C"  bool U3CDelayCallExecU3Ec__Iterator3F_1_MoveNext_m2137000113_gshared (U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator3F_1_MoveNext_m2137000113(__this, method) ((  bool (*) (U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator3F_1_MoveNext_m2137000113_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Int32>::Dispose()
extern "C"  void U3CDelayCallExecU3Ec__Iterator3F_1_Dispose_m2372714952_gshared (U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator3F_1_Dispose_m2372714952(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator3F_1_Dispose_m2372714952_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator3F`1<System.Int32>::Reset()
extern "C"  void U3CDelayCallExecU3Ec__Iterator3F_1_Reset_m3941738232_gshared (U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator3F_1_Reset_m3941738232(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator3F_1_t1764912606 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator3F_1_Reset_m3941738232_gshared)(__this, method)
