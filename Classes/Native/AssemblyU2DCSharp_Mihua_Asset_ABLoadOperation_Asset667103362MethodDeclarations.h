﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.Asset.ABLoadOperation.AssetOperationLoaded
struct AssetOperationLoaded_t667103362;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t3071478659;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Mihua.Asset.ABLoadOperation.AssetOperationLoaded::.ctor(System.String)
extern "C"  void AssetOperationLoaded__ctor_m3144309797 (AssetOperationLoaded_t667103362 * __this, String_t* ___assetName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object Mihua.Asset.ABLoadOperation.AssetOperationLoaded::GetAsset()
extern "C"  Object_t3071478659 * AssetOperationLoaded_GetAsset_m2365040872 (AssetOperationLoaded_t667103362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Asset.ABLoadOperation.AssetOperationLoaded::IsDone()
extern "C"  bool AssetOperationLoaded_IsDone_m3142804647 (AssetOperationLoaded_t667103362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mihua.Asset.ABLoadOperation.AssetOperationLoaded::get_progress()
extern "C"  float AssetOperationLoaded_get_progress_m645438921 (AssetOperationLoaded_t667103362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.ABLoadOperation.AssetOperationLoaded::Clear()
extern "C"  void AssetOperationLoaded_Clear_m2069914152 (AssetOperationLoaded_t667103362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
