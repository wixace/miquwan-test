﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_FindIndex_GetDelegate_member17_arg0>c__AnonStorey87`1<System.Object>
struct U3CListA1_FindIndex_GetDelegate_member17_arg0U3Ec__AnonStorey87_1_t697222579;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_FindIndex_GetDelegate_member17_arg0>c__AnonStorey87`1<System.Object>::.ctor()
extern "C"  void U3CListA1_FindIndex_GetDelegate_member17_arg0U3Ec__AnonStorey87_1__ctor_m287948226_gshared (U3CListA1_FindIndex_GetDelegate_member17_arg0U3Ec__AnonStorey87_1_t697222579 * __this, const MethodInfo* method);
#define U3CListA1_FindIndex_GetDelegate_member17_arg0U3Ec__AnonStorey87_1__ctor_m287948226(__this, method) ((  void (*) (U3CListA1_FindIndex_GetDelegate_member17_arg0U3Ec__AnonStorey87_1_t697222579 *, const MethodInfo*))U3CListA1_FindIndex_GetDelegate_member17_arg0U3Ec__AnonStorey87_1__ctor_m287948226_gshared)(__this, method)
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_FindIndex_GetDelegate_member17_arg0>c__AnonStorey87`1<System.Object>::<>m__AC(T)
extern "C"  bool U3CListA1_FindIndex_GetDelegate_member17_arg0U3Ec__AnonStorey87_1_U3CU3Em__AC_m2938542109_gshared (U3CListA1_FindIndex_GetDelegate_member17_arg0U3Ec__AnonStorey87_1_t697222579 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CListA1_FindIndex_GetDelegate_member17_arg0U3Ec__AnonStorey87_1_U3CU3Em__AC_m2938542109(__this, ___obj0, method) ((  bool (*) (U3CListA1_FindIndex_GetDelegate_member17_arg0U3Ec__AnonStorey87_1_t697222579 *, Il2CppObject *, const MethodInfo*))U3CListA1_FindIndex_GetDelegate_member17_arg0U3Ec__AnonStorey87_1_U3CU3Em__AC_m2938542109_gshared)(__this, ___obj0, method)
