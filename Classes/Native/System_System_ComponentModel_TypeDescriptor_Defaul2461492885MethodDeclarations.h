﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.TypeDescriptor/DefaultTypeDescriptionProvider
struct DefaultTypeDescriptionProvider_t2461492885;

#include "codegen/il2cpp-codegen.h"

// System.Void System.ComponentModel.TypeDescriptor/DefaultTypeDescriptionProvider::.ctor()
extern "C"  void DefaultTypeDescriptionProvider__ctor_m3636893909 (DefaultTypeDescriptionProvider_t2461492885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
