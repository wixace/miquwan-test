﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Mail.SmtpFailedRecipientsException
struct SmtpFailedRecipientsException_t645487992;
// System.String
struct String_t;
// System.Net.Mail.SmtpFailedRecipientException[]
struct SmtpFailedRecipientExceptionU5BU5D_t3524271232;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"

// System.Void System.Net.Mail.SmtpFailedRecipientsException::.ctor()
extern "C"  void SmtpFailedRecipientsException__ctor_m358195727 (SmtpFailedRecipientsException_t645487992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.SmtpFailedRecipientsException::.ctor(System.String,System.Net.Mail.SmtpFailedRecipientException[])
extern "C"  void SmtpFailedRecipientsException__ctor_m3146987745 (SmtpFailedRecipientsException_t645487992 * __this, String_t* ___message0, SmtpFailedRecipientExceptionU5BU5D_t3524271232* ___innerExceptions1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.SmtpFailedRecipientsException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SmtpFailedRecipientsException__ctor_m2306133712 (SmtpFailedRecipientsException_t645487992 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.SmtpFailedRecipientsException::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SmtpFailedRecipientsException_System_Runtime_Serialization_ISerializable_GetObjectData_m635629212 (SmtpFailedRecipientsException_t645487992 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.SmtpFailedRecipientsException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SmtpFailedRecipientsException_GetObjectData_m3735176749 (SmtpFailedRecipientsException_t645487992 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
