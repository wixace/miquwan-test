﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t291504320;

#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Float_fight_tip
struct  Float_fight_tip_t4275741097  : public FloatTextUnit_t2362298029
{
public:
	// UILabel Float_fight_tip::LabelFile
	UILabel_t291504320 * ___LabelFile_3;

public:
	inline static int32_t get_offset_of_LabelFile_3() { return static_cast<int32_t>(offsetof(Float_fight_tip_t4275741097, ___LabelFile_3)); }
	inline UILabel_t291504320 * get_LabelFile_3() const { return ___LabelFile_3; }
	inline UILabel_t291504320 ** get_address_of_LabelFile_3() { return &___LabelFile_3; }
	inline void set_LabelFile_3(UILabel_t291504320 * value)
	{
		___LabelFile_3 = value;
		Il2CppCodeGenWriteBarrier(&___LabelFile_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
