﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ContrastStretchEffect
struct ContrastStretchEffect_t443831540;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;
// UnityEngine.Texture
struct Texture_t2526458961;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"

// System.Void ContrastStretchEffect::.ctor()
extern "C"  void ContrastStretchEffect__ctor_m162504899 (ContrastStretchEffect_t443831540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material ContrastStretchEffect::get_materialLum()
extern "C"  Material_t3870600107 * ContrastStretchEffect_get_materialLum_m1594234236 (ContrastStretchEffect_t443831540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material ContrastStretchEffect::get_materialReduce()
extern "C"  Material_t3870600107 * ContrastStretchEffect_get_materialReduce_m450810896 (ContrastStretchEffect_t443831540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material ContrastStretchEffect::get_materialAdapt()
extern "C"  Material_t3870600107 * ContrastStretchEffect_get_materialAdapt_m1382578010 (ContrastStretchEffect_t443831540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material ContrastStretchEffect::get_materialApply()
extern "C"  Material_t3870600107 * ContrastStretchEffect_get_materialApply_m1739866278 (ContrastStretchEffect_t443831540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContrastStretchEffect::Start()
extern "C"  void ContrastStretchEffect_Start_m3404609987 (ContrastStretchEffect_t443831540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContrastStretchEffect::OnEnable()
extern "C"  void ContrastStretchEffect_OnEnable_m2826882019 (ContrastStretchEffect_t443831540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContrastStretchEffect::OnDisable()
extern "C"  void ContrastStretchEffect_OnDisable_m2174933930 (ContrastStretchEffect_t443831540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContrastStretchEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void ContrastStretchEffect_OnRenderImage_m3152364987 (ContrastStretchEffect_t443831540 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContrastStretchEffect::CalculateAdaptation(UnityEngine.Texture)
extern "C"  void ContrastStretchEffect_CalculateAdaptation_m2954666100 (ContrastStretchEffect_t443831540 * __this, Texture_t2526458961 * ___curTexture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
