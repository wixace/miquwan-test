﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14C`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_t3091917058;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"

// System.Void Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14C`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1__ctor_m2618729866_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_t3091917058 * __this, const MethodInfo* method);
#define U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1__ctor_m2618729866(__this, method) ((  void (*) (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_t3091917058 *, const MethodInfo*))U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1__ctor_m2618729866_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.StringUtils/<ForgivingCaseSensitiveFind>c__AnonStorey14C`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::<>m__3B8(TSource)
extern "C"  void U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_U3CU3Em__3B8_m2785372651_gshared (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_t3091917058 * __this, KeyValuePair_2_t1944668977  ___itm0, const MethodInfo* method);
#define U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_U3CU3Em__3B8_m2785372651(__this, ___itm0, method) ((  void (*) (U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_t3091917058 *, KeyValuePair_2_t1944668977 , const MethodInfo*))U3CForgivingCaseSensitiveFindU3Ec__AnonStorey14C_1_U3CU3Em__3B8_m2785372651_gshared)(__this, ___itm0, method)
