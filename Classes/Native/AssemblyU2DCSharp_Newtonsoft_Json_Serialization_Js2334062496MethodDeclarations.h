﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<CreateAndPopulateList>c__AnonStorey134
struct U3CCreateAndPopulateListU3Ec__AnonStorey134_t2334062496;
// System.Collections.IList
struct IList_t1751339649;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<CreateAndPopulateList>c__AnonStorey134::.ctor()
extern "C"  void U3CCreateAndPopulateListU3Ec__AnonStorey134__ctor_m914599163 (U3CCreateAndPopulateListU3Ec__AnonStorey134_t2334062496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<CreateAndPopulateList>c__AnonStorey134::<>m__389(System.Collections.IList,System.Boolean)
extern "C"  void U3CCreateAndPopulateListU3Ec__AnonStorey134_U3CU3Em__389_m2128688218 (U3CCreateAndPopulateListU3Ec__AnonStorey134_t2334062496 * __this, Il2CppObject * ___l0, bool ___isTemporaryListReference1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
