﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FingerGestures
struct FingerGestures_t2907604723;
// Gesture/EventHandler
struct EventHandler_t2814654102;
// FingerEventDetector`1/FingerEventHandler<FingerEvent>
struct FingerEventHandler_t2860637046;
// FingerGestures/EventHandler
struct EventHandler_t2445892876;
// Gesture
struct Gesture_t1589572905;
// FingerEvent
struct FingerEvent_t252338321;
// FingerClusterManager
struct FingerClusterManager_t3376029756;
// FGInputProvider
struct FGInputProvider_t1238597786;
// FingerGestures/Finger
struct Finger_t182428197;
// FingerGestures/IFingerList
struct IFingerList_t1026994100;
// System.Collections.Generic.List`1<GestureRecognizer>
struct List_1_t586094205;
// GestureRecognizer
struct GestureRecognizer_t3512875949;
// FingerGestures/GlobalTouchFilterDelegate
struct GlobalTouchFilterDelegate_t316184925;
// UnityEngine.Transform
struct Transform_t1659122786;
// System.String
struct String_t;
// FingerGestures/FingerList
struct FingerList_t1886137443;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Gesture_EventHandler2814654102.h"
#include "AssemblyU2DCSharp_FingerGestures_EventHandler2445892876.h"
#include "AssemblyU2DCSharp_Gesture1589572905.h"
#include "AssemblyU2DCSharp_FingerEvent252338321.h"
#include "UnityEngine_UnityEngine_RuntimePlatform3050318497.h"
#include "AssemblyU2DCSharp_FGInputProvider1238597786.h"
#include "AssemblyU2DCSharp_GestureRecognizer3512875949.h"
#include "AssemblyU2DCSharp_FingerGestures_GlobalTouchFilterD316184925.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_FingerGestures_SwipeDirection1218055201.h"
#include "AssemblyU2DCSharp_FingerGestures_Finger182428197.h"
#include "AssemblyU2DCSharp_DistanceUnit3471059769.h"
#include "AssemblyU2DCSharp_FingerGestures2907604723.h"
#include "AssemblyU2DCSharp_FingerGestures_FingerList1886137443.h"

// System.Void FingerGestures::.ctor()
extern "C"  void FingerGestures__ctor_m594831112 (FingerGestures_t2907604723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::.cctor()
extern "C"  void FingerGestures__cctor_m777799077 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::add_OnGestureEvent(Gesture/EventHandler)
extern "C"  void FingerGestures_add_OnGestureEvent_m3115828692 (Il2CppObject * __this /* static, unused */, EventHandler_t2814654102 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::remove_OnGestureEvent(Gesture/EventHandler)
extern "C"  void FingerGestures_remove_OnGestureEvent_m3963595579 (Il2CppObject * __this /* static, unused */, EventHandler_t2814654102 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::add_OnFingerEvent(FingerEventDetector`1/FingerEventHandler<FingerEvent>)
extern "C"  void FingerGestures_add_OnFingerEvent_m320121231 (Il2CppObject * __this /* static, unused */, FingerEventHandler_t2860637046 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::remove_OnFingerEvent(FingerEventDetector`1/FingerEventHandler<FingerEvent>)
extern "C"  void FingerGestures_remove_OnFingerEvent_m110441206 (Il2CppObject * __this /* static, unused */, FingerEventHandler_t2860637046 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::add_OnInputProviderChanged(FingerGestures/EventHandler)
extern "C"  void FingerGestures_add_OnInputProviderChanged_m177774964 (Il2CppObject * __this /* static, unused */, EventHandler_t2445892876 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::remove_OnInputProviderChanged(FingerGestures/EventHandler)
extern "C"  void FingerGestures_remove_OnInputProviderChanged_m3596247341 (Il2CppObject * __this /* static, unused */, EventHandler_t2445892876 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::FireEvent(Gesture)
extern "C"  void FingerGestures_FireEvent_m3288616575 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::FireEvent(FingerEvent)
extern "C"  void FingerGestures_FireEvent_m3269593239 (Il2CppObject * __this /* static, unused */, FingerEvent_t252338321 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerClusterManager FingerGestures::get_DefaultClusterManager()
extern "C"  FingerClusterManager_t3376029756 * FingerGestures_get_DefaultClusterManager_m3075425936 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures FingerGestures::get_Instance()
extern "C"  FingerGestures_t2907604723 * FingerGestures_get_Instance_m590289156 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::Init()
extern "C"  void FingerGestures_Init_m2880218700 (FingerGestures_t2907604723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FGInputProvider FingerGestures::get_InputProvider()
extern "C"  FGInputProvider_t1238597786 * FingerGestures_get_InputProvider_m3869525887 (FingerGestures_t2907604723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FingerGestures::IsTouchScreenPlatform(UnityEngine.RuntimePlatform)
extern "C"  bool FingerGestures_IsTouchScreenPlatform_m570670552 (Il2CppObject * __this /* static, unused */, int32_t ___platform0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::InitInputProvider()
extern "C"  void FingerGestures_InitInputProvider_m3634993809 (FingerGestures_t2907604723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::InstallInputProvider(FGInputProvider)
extern "C"  void FingerGestures_InstallInputProvider_m4289455452 (FingerGestures_t2907604723 * __this, FGInputProvider_t1238597786 * ___inputProviderPrefab0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FingerGestures::get_MaxFingers()
extern "C"  int32_t FingerGestures_get_MaxFingers_m3541252729 (FingerGestures_t2907604723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/Finger FingerGestures::GetFinger(System.Int32)
extern "C"  Finger_t182428197 * FingerGestures_GetFinger_m3441520136 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/IFingerList FingerGestures::get_Touches()
extern "C"  Il2CppObject * FingerGestures_get_Touches_m183703553 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GestureRecognizer> FingerGestures::get_RegisteredGestureRecognizers()
extern "C"  List_1_t586094205 * FingerGestures_get_RegisteredGestureRecognizers_m1537119707 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::Register(GestureRecognizer)
extern "C"  void FingerGestures_Register_m385012582 (Il2CppObject * __this /* static, unused */, GestureRecognizer_t3512875949 * ___recognizer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::Unregister(GestureRecognizer)
extern "C"  void FingerGestures_Unregister_m182540269 (Il2CppObject * __this /* static, unused */, GestureRecognizer_t3512875949 * ___recognizer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::Awake()
extern "C"  void FingerGestures_Awake_m832436331 (FingerGestures_t2907604723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::Start()
extern "C"  void FingerGestures_Start_m3836936200 (FingerGestures_t2907604723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::OnEnable()
extern "C"  void FingerGestures_OnEnable_m1650172798 (FingerGestures_t2907604723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::CheckInit()
extern "C"  void FingerGestures_CheckInit_m754777566 (FingerGestures_t2907604723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::Update()
extern "C"  void FingerGestures_Update_m2986757381 (FingerGestures_t2907604723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::InitFingers(System.Int32)
extern "C"  void FingerGestures_InitFingers_m1105655345 (FingerGestures_t2907604723 * __this, int32_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::UpdateFingers()
extern "C"  void FingerGestures_UpdateFingers_m2783221767 (FingerGestures_t2907604723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/GlobalTouchFilterDelegate FingerGestures::get_GlobalTouchFilter()
extern "C"  GlobalTouchFilterDelegate_t316184925 * FingerGestures_get_GlobalTouchFilter_m3331615665 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::set_GlobalTouchFilter(FingerGestures/GlobalTouchFilterDelegate)
extern "C"  void FingerGestures_set_GlobalTouchFilter_m2171098112 (Il2CppObject * __this /* static, unused */, GlobalTouchFilterDelegate_t316184925 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FingerGestures::ShouldProcessTouch(System.Int32,UnityEngine.Vector2)
extern "C"  bool FingerGestures_ShouldProcessTouch_m2389199256 (FingerGestures_t2907604723 * __this, int32_t ___fingerIndex0, Vector2_t4282066565  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform FingerGestures::CreateNode(System.String,UnityEngine.Transform)
extern "C"  Transform_t1659122786 * FingerGestures_CreateNode_m3982322433 (FingerGestures_t2907604723 * __this, String_t* ___name0, Transform_t1659122786 * ___parent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::InitNodes()
extern "C"  void FingerGestures_InitNodes_m3712761063 (FingerGestures_t2907604723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/SwipeDirection FingerGestures::GetSwipeDirection(UnityEngine.Vector2,System.Single)
extern "C"  int32_t FingerGestures_GetSwipeDirection_m1893702352 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___dir0, float ___tolerance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/SwipeDirection FingerGestures::GetSwipeDirection(UnityEngine.Vector2)
extern "C"  int32_t FingerGestures_GetSwipeDirection_m3081140011 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___dir0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FingerGestures::UsingUnityRemote()
extern "C"  bool FingerGestures_UsingUnityRemote_m3473835431 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FingerGestures::AllFingersMoving(FingerGestures/Finger,FingerGestures/Finger)
extern "C"  bool FingerGestures_AllFingersMoving_m2446867405 (Il2CppObject * __this /* static, unused */, Finger_t182428197 * ___finger00, Finger_t182428197 * ___finger11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FingerGestures::FingersMovedInOppositeDirections(FingerGestures/Finger,FingerGestures/Finger,System.Single)
extern "C"  bool FingerGestures_FingersMovedInOppositeDirections_m2661933120 (Il2CppObject * __this /* static, unused */, Finger_t182428197 * ___finger00, Finger_t182428197 * ___finger11, float ___minDOT2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FingerGestures::SignedAngle(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float FingerGestures_SignedAngle_m291979933 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___from0, Vector2_t4282066565  ___to1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FingerGestures::NormalizeAngle360(System.Single)
extern "C"  float FingerGestures_NormalizeAngle360_m1488664074 (Il2CppObject * __this /* static, unused */, float ___angleInDegrees0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FingerGestures::get_ScreenDPI()
extern "C"  float FingerGestures_get_ScreenDPI_m2675506818 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::set_ScreenDPI(System.Single)
extern "C"  void FingerGestures_set_ScreenDPI_m3417007121 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FingerGestures::Convert(System.Single,DistanceUnit,DistanceUnit)
extern "C"  float FingerGestures_Convert_m378999262 (Il2CppObject * __this /* static, unused */, float ___distance0, int32_t ___fromUnit1, int32_t ___toUnit2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 FingerGestures::Convert(UnityEngine.Vector2,DistanceUnit,DistanceUnit)
extern "C"  Vector2_t4282066565  FingerGestures_Convert_m2352351804 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___v0, int32_t ___fromUnit1, int32_t ___toUnit2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::ilo_InitInputProvider1(FingerGestures)
extern "C"  void FingerGestures_ilo_InitInputProvider1_m3007898882 (Il2CppObject * __this /* static, unused */, FingerGestures_t2907604723 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FingerGestures::ilo_IsTouchScreenPlatform2(UnityEngine.RuntimePlatform)
extern "C"  bool FingerGestures_ilo_IsTouchScreenPlatform2_m1234818557 (Il2CppObject * __this /* static, unused */, int32_t ___platform0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FingerGestures::ilo_get_MaxFingers3(FingerGestures)
extern "C"  int32_t FingerGestures_ilo_get_MaxFingers3_m2681794582 (Il2CppObject * __this /* static, unused */, FingerGestures_t2907604723 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerGestures::ilo_Clear4(FingerGestures/FingerList)
extern "C"  void FingerGestures_ilo_Clear4_m3589917433 (Il2CppObject * __this /* static, unused */, FingerList_t1886137443 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FingerGestures::ilo_Invoke5(FingerGestures/GlobalTouchFilterDelegate,System.Int32,UnityEngine.Vector2)
extern "C"  bool FingerGestures_ilo_Invoke5_m3050031244 (Il2CppObject * __this /* static, unused */, GlobalTouchFilterDelegate_t316184925 * ____this0, int32_t ___fingerIndex1, Vector2_t4282066565  ___position2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform FingerGestures::ilo_CreateNode6(FingerGestures,System.String,UnityEngine.Transform)
extern "C"  Transform_t1659122786 * FingerGestures_ilo_CreateNode6_m3977377773 (Il2CppObject * __this /* static, unused */, FingerGestures_t2907604723 * ____this0, String_t* ___name1, Transform_t1659122786 * ___parent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FingerGestures::ilo_NormalizeAngle3607(System.Single)
extern "C"  float FingerGestures_ilo_NormalizeAngle3607_m1367943456 (Il2CppObject * __this /* static, unused */, float ___angleInDegrees0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FingerGestures::ilo_get_IsMoving8(FingerGestures/Finger)
extern "C"  bool FingerGestures_ilo_get_IsMoving8_m2646013015 (Il2CppObject * __this /* static, unused */, Finger_t182428197 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
