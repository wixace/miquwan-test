﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Xml_XmlElementGenerated
struct System_Xml_XmlElementGenerated_t2206854610;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Xml_XmlElementGenerated::.ctor()
extern "C"  void System_Xml_XmlElementGenerated__ctor_m663362313 (System_Xml_XmlElementGenerated_t2206854610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlElementGenerated::XmlElement_Attributes(JSVCall)
extern "C"  void System_Xml_XmlElementGenerated_XmlElement_Attributes_m1979928495 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlElementGenerated::XmlElement_HasAttributes(JSVCall)
extern "C"  void System_Xml_XmlElementGenerated_XmlElement_HasAttributes_m1166205813 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlElementGenerated::XmlElement_InnerText(JSVCall)
extern "C"  void System_Xml_XmlElementGenerated_XmlElement_InnerText_m1102174883 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlElementGenerated::XmlElement_InnerXml(JSVCall)
extern "C"  void System_Xml_XmlElementGenerated_XmlElement_InnerXml_m2987817573 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlElementGenerated::XmlElement_IsEmpty(JSVCall)
extern "C"  void System_Xml_XmlElementGenerated_XmlElement_IsEmpty_m1479803139 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlElementGenerated::XmlElement_LocalName(JSVCall)
extern "C"  void System_Xml_XmlElementGenerated_XmlElement_LocalName_m271738896 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlElementGenerated::XmlElement_Name(JSVCall)
extern "C"  void System_Xml_XmlElementGenerated_XmlElement_Name_m1094144251 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlElementGenerated::XmlElement_NamespaceURI(JSVCall)
extern "C"  void System_Xml_XmlElementGenerated_XmlElement_NamespaceURI_m1469255733 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlElementGenerated::XmlElement_NextSibling(JSVCall)
extern "C"  void System_Xml_XmlElementGenerated_XmlElement_NextSibling_m194315383 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlElementGenerated::XmlElement_NodeType(JSVCall)
extern "C"  void System_Xml_XmlElementGenerated_XmlElement_NodeType_m3214874186 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlElementGenerated::XmlElement_OwnerDocument(JSVCall)
extern "C"  void System_Xml_XmlElementGenerated_XmlElement_OwnerDocument_m294516408 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlElementGenerated::XmlElement_Prefix(JSVCall)
extern "C"  void System_Xml_XmlElementGenerated_XmlElement_Prefix_m1044465876 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlElementGenerated::XmlElement_ParentNode(JSVCall)
extern "C"  void System_Xml_XmlElementGenerated_XmlElement_ParentNode_m200894170 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlElementGenerated::XmlElement_SchemaInfo(JSVCall)
extern "C"  void System_Xml_XmlElementGenerated_XmlElement_SchemaInfo_m3197062679 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_CloneNode__Boolean(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_CloneNode__Boolean_m1618064774 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_GetAttribute__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_GetAttribute__String__String_m1595122245 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_GetAttribute__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_GetAttribute__String_m3142719476 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_GetAttributeNode__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_GetAttributeNode__String__String_m1426719207 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_GetAttributeNode__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_GetAttributeNode__String_m1957378454 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_GetElementsByTagName__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_GetElementsByTagName__String__String_m39229216 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_GetElementsByTagName__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_GetElementsByTagName__String_m3816363983 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_HasAttribute__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_HasAttribute__String__String_m416038529 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_HasAttribute__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_HasAttribute__String_m957919280 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_RemoveAll(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_RemoveAll_m1088503586 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_RemoveAllAttributes(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_RemoveAllAttributes_m2854592537 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_RemoveAttribute__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_RemoveAttribute__String__String_m4255960959 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_RemoveAttribute__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_RemoveAttribute__String_m1650532142 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_RemoveAttributeAt__Int32(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_RemoveAttributeAt__Int32_m3310237280 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_RemoveAttributeNode__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_RemoveAttributeNode__String__String_m2088108577 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_RemoveAttributeNode__XmlAttribute(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_RemoveAttributeNode__XmlAttribute_m2605035140 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_SetAttribute__String__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_SetAttribute__String__String__String_m443506954 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_SetAttribute__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_SetAttribute__String__String_m3003607993 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_SetAttributeNode__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_SetAttributeNode__String__String_m2387865947 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_SetAttributeNode__XmlAttribute(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_SetAttributeNode__XmlAttribute_m1684677694 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_WriteContentTo__XmlWriter(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_WriteContentTo__XmlWriter_m2772394810 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::XmlElement_WriteTo__XmlWriter(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_XmlElement_WriteTo__XmlWriter_m4264100013 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlElementGenerated::__Register()
extern "C"  void System_Xml_XmlElementGenerated___Register_m2257313630 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlElementGenerated::ilo_setBooleanS1(System.Int32,System.Boolean)
extern "C"  void System_Xml_XmlElementGenerated_ilo_setBooleanS1_m2876632762 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlElementGenerated::ilo_setStringS2(System.Int32,System.String)
extern "C"  void System_Xml_XmlElementGenerated_ilo_setStringS2_m2517058003 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System_Xml_XmlElementGenerated::ilo_getStringS3(System.Int32)
extern "C"  String_t* System_Xml_XmlElementGenerated_ilo_getStringS3_m2183410889 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Xml_XmlElementGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_Xml_XmlElementGenerated_ilo_setObject4_m2584071524 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlElementGenerated::ilo_getBooleanS5(System.Int32)
extern "C"  bool System_Xml_XmlElementGenerated_ilo_getBooleanS5_m1006444967 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Xml_XmlElementGenerated::ilo_getInt326(System.Int32)
extern "C"  int32_t System_Xml_XmlElementGenerated_ilo_getInt326_m272430229 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Xml_XmlElementGenerated::ilo_getObject7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_Xml_XmlElementGenerated_ilo_getObject7_m4159220236 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
