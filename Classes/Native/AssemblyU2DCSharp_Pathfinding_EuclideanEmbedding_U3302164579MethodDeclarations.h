﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120
struct U3CRecalculateCostsU3Ec__AnonStorey120_t3302164579;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120::.ctor()
extern "C"  void U3CRecalculateCostsU3Ec__AnonStorey120__ctor_m1823208552 (U3CRecalculateCostsU3Ec__AnonStorey120_t3302164579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.EuclideanEmbedding/<RecalculateCosts>c__AnonStorey120::<>m__352(System.Int32)
extern "C"  void U3CRecalculateCostsU3Ec__AnonStorey120_U3CU3Em__352_m3761019058 (U3CRecalculateCostsU3Ec__AnonStorey120_t3302164579 * __this, int32_t ___k0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
