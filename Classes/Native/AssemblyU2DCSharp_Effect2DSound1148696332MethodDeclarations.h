﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Effect2DSound
struct Effect2DSound_t1148696332;
// SoundObj
struct SoundObj_t1807286664;
// UnityEngine.AudioSource
struct AudioSource_t1740077639;
// SoundCfg
struct SoundCfg_t1807275253;
// UnityEngine.AudioClip
struct AudioClip_t794140988;
// ISound
struct ISound_t2170003014;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SoundCfg1807275253.h"
#include "UnityEngine_UnityEngine_AudioClip794140988.h"
#include "AssemblyU2DCSharp_SoundObj1807286664.h"
#include "AssemblyU2DCSharp_Effect2DSound1148696332.h"
#include "AssemblyU2DCSharp_ISound2170003014.h"

// System.Void Effect2DSound::.ctor()
extern "C"  void Effect2DSound__ctor_m2876364767 (Effect2DSound_t1148696332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SoundObj Effect2DSound::GetAudio()
extern "C"  SoundObj_t1807286664 * Effect2DSound_GetAudio_m1927745296 (Effect2DSound_t1148696332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SoundObj Effect2DSound::GetObjByID(System.Int32)
extern "C"  SoundObj_t1807286664 * Effect2DSound_GetObjByID_m4085025364 (Effect2DSound_t1148696332 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Effect2DSound::IsPlaying(System.Int32)
extern "C"  bool Effect2DSound_IsPlaying_m738079014 (Effect2DSound_t1148696332 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Effect2DSound::Pause()
extern "C"  void Effect2DSound_Pause_m2930490739 (Effect2DSound_t1148696332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioSource Effect2DSound::Play(SoundCfg,UnityEngine.AudioClip)
extern "C"  AudioSource_t1740077639 * Effect2DSound_Play_m2916230728 (Effect2DSound_t1148696332 * __this, SoundCfg_t1807275253 * ___soundcfg0, AudioClip_t794140988 * ___clip1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Effect2DSound::RePlay()
extern "C"  void Effect2DSound_RePlay_m2325783244 (Effect2DSound_t1148696332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Effect2DSound::SetAudio(SoundObj)
extern "C"  void Effect2DSound_SetAudio_m2102379153 (Effect2DSound_t1148696332 * __this, SoundObj_t1807286664 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Effect2DSound::SetPitch(System.Single,System.Int32)
extern "C"  void Effect2DSound_SetPitch_m3651212111 (Effect2DSound_t1148696332 * __this, float ___pitch0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Effect2DSound::Stop()
extern "C"  void Effect2DSound_Stop_m197782791 (Effect2DSound_t1148696332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Effect2DSound::StopHero()
extern "C"  void Effect2DSound_StopHero_m1166811041 (Effect2DSound_t1148696332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Effect2DSound::StopByID(System.Int32)
extern "C"  void Effect2DSound_StopByID_m3253540138 (Effect2DSound_t1148696332 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Effect2DSound::StopLoopSound()
extern "C"  void Effect2DSound_StopLoopSound_m892599718 (Effect2DSound_t1148696332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Effect2DSound::Update()
extern "C"  void Effect2DSound_Update_m699856654 (Effect2DSound_t1148696332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Effect2DSound::get_mute()
extern "C"  bool Effect2DSound_get_mute_m351024147 (Effect2DSound_t1148696332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Effect2DSound::set_mute(System.Boolean)
extern "C"  void Effect2DSound_set_mute_m1628950514 (Effect2DSound_t1148696332 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SoundObj Effect2DSound::ilo_GetObjByID1(Effect2DSound,System.Int32)
extern "C"  SoundObj_t1807286664 * Effect2DSound_ilo_GetObjByID1_m1202138382 (Il2CppObject * __this /* static, unused */, Effect2DSound_t1148696332 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Effect2DSound::ilo_get_volume2(ISound)
extern "C"  float Effect2DSound_ilo_get_volume2_m3035131327 (Il2CppObject * __this /* static, unused */, ISound_t2170003014 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Effect2DSound::ilo_get_mute3(Effect2DSound)
extern "C"  bool Effect2DSound_ilo_get_mute3_m1266187511 (Il2CppObject * __this /* static, unused */, Effect2DSound_t1148696332 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Effect2DSound::ilo_SetAudio4(Effect2DSound,SoundObj)
extern "C"  void Effect2DSound_ilo_SetAudio4_m699374882 (Il2CppObject * __this /* static, unused */, Effect2DSound_t1148696332 * ____this0, SoundObj_t1807286664 * ___obj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Effect2DSound::ilo_get_mute5(ISound)
extern "C"  bool Effect2DSound_ilo_get_mute5_m2868397963 (Il2CppObject * __this /* static, unused */, ISound_t2170003014 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Effect2DSound::ilo_set_mute6(ISound,System.Boolean)
extern "C"  void Effect2DSound_ilo_set_mute6_m1909019929 (Il2CppObject * __this /* static, unused */, ISound_t2170003014 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
