﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_muyoJayzi189
struct M_muyoJayzi189_t3307371183;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_muyoJayzi1893307371183.h"

// System.Void GarbageiOS.M_muyoJayzi189::.ctor()
extern "C"  void M_muyoJayzi189__ctor_m2436385892 (M_muyoJayzi189_t3307371183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_muyoJayzi189::M_tolurdair0(System.String[],System.Int32)
extern "C"  void M_muyoJayzi189_M_tolurdair0_m2934787781 (M_muyoJayzi189_t3307371183 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_muyoJayzi189::M_lijaDruri1(System.String[],System.Int32)
extern "C"  void M_muyoJayzi189_M_lijaDruri1_m788271184 (M_muyoJayzi189_t3307371183 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_muyoJayzi189::ilo_M_tolurdair01(GarbageiOS.M_muyoJayzi189,System.String[],System.Int32)
extern "C"  void M_muyoJayzi189_ilo_M_tolurdair01_m3722685668 (Il2CppObject * __this /* static, unused */, M_muyoJayzi189_t3307371183 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
